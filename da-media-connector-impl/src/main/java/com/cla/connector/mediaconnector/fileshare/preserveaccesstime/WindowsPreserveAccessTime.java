package com.cla.connector.mediaconnector.fileshare.preserveaccesstime;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.sun.jna.Memory;
import com.sun.jna.platform.win32.*;
import com.sun.jna.ptr.PointerByReference;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static com.sun.jna.platform.win32.WinNT.*;

public class WindowsPreserveAccessTime {

    private static final Logger logger = LoggerFactory.getLogger(WindowsPreserveAccessTime.class);

    private static final FILETIME UNCHANGED_ACCESS_TIME;
    private static final int DEFAULT_DW_DESIRED_ACCESS_FLAGS = WinNT.ACCESS_SYSTEM_SECURITY | WinNT.GENERIC_READ | WinNT.FILE_WRITE_ATTRIBUTES;
    private static boolean doubleCheckDirectory = true;
    private static boolean forceIsDirectoryByAttributes = true;

    private static List<String> fileShareMapErrorIgnore = new ArrayList<>();

    static { // Initialize special value for unchanged access time
        UNCHANGED_ACCESS_TIME = new FILETIME();
        UNCHANGED_ACCESS_TIME.dwLowDateTime = 0xFFFFFFFF;
        UNCHANGED_ACCESS_TIME.dwHighDateTime = 0xFFFFFFFF;
    }

    public static void setDoubleCheckDirectory(boolean doubleCheckDirectoryParam) {
        doubleCheckDirectory = doubleCheckDirectoryParam;
    }

    public static void setForceIsDirectoryByAttributes(boolean forceIsDirectoryByAttributesParam) {
        forceIsDirectoryByAttributes = forceIsDirectoryByAttributesParam;
    }

    public static void fetchAttributes(ClaFilePropertiesDto props, boolean isImpersonateBeforeAction) throws IOException {
        preserveAccessTimeAction(props.getFileName(), new AttributesHandleAction(props), isImpersonateBeforeAction);
    }

    public static void fetchAcls(ClaFilePropertiesDto props, boolean isImpersonateBeforeAction) throws IOException {
        preserveAccessTimeAction(props.getFileName(), new AclHandleAction(props), isImpersonateBeforeAction);
    }

    private static long fileTimeInMillis(LARGE_INTEGER time) {
        return FILETIME.filetimeToDate(time.getHigh().intValue(), time.getLow().intValue()).getTime();
    }

    public static void preserveAccessTimeAction(String path, HandleAction handleAction, boolean isImpersonateBeforeAction, int... accessFlags) throws IOException {
        doPrivliged(() -> {
            HandleWrapper handleWrapper = null;
            WinNT.HANDLE hFile = null;
            try {
                hFile = getFileHandle(path, accessFlags);
                if (hFile != WinNT.INVALID_HANDLE_VALUE) {
                    handleWrapper = new HandleWrapper(hFile, false);
                } else {
                    hFile = getDirHandle(path, accessFlags);
                    if (hFile != WinNT.INVALID_HANDLE_VALUE) {
                        boolean isDirectory = true;
                        if (doubleCheckDirectory) {
                            int attrib = getFileAttributesByHandle(hFile);
                            if (attrib == 0) {
                                logger.warn("Failed to get dirHandle attributes for {}. Reason {}", path, Kernel32Util.getLastErrorMessage());
                            } else {
                                if ((attrib & 16 /* WinNT.FILE_ATTRIBUTE_DIRECTORY */) == 0) {
                                    logger.debug("Found non-directory attributes of a dirHandle {}. Attributes={}", path, attrib);
                                    if (forceIsDirectoryByAttributes) {
                                        isDirectory = false;
                                    }
                                }
                            }
                        }
                        handleWrapper = new HandleWrapper(hFile, isDirectory);
                    }
                }
                if (handleWrapper == null) {
                    throw new NoSuchFileException(path, null, Kernel32Util.getLastErrorMessage());
                }
                preserveLastAccessTime(hFile, path);
                handleAction.execute(handleWrapper);
            } finally {
                Kernel32.INSTANCE.CloseHandle(hFile);
            }
        }, isImpersonateBeforeAction);
    }

    private static int getFileAttributesByHandle(HANDLE handle) {
        Memory p = new Memory(FILE_BASIC_INFO.sizeOf());
        if (false == Kernel32.INSTANCE.GetFileInformationByHandleEx(handle, WinBase.FileBasicInfo, p, new DWORD(p.size()))) {
            return 0;
        }
        FILE_BASIC_INFO fbi = new FILE_BASIC_INFO(p);
        return fbi.FileAttributes;
    }

    private static HANDLE getDirHandle(String path, int... accessFlags) {
        int dwDesiredAccess = getDwDesiredAccess(accessFlags);
        return Kernel32.INSTANCE.CreateFile(
                path,
                dwDesiredAccess,
                WinNT.FILE_SHARE_READ,
                new WinBase.SECURITY_ATTRIBUTES(),
                WinNT.OPEN_EXISTING,
                WinNT.FILE_FLAG_BACKUP_SEMANTICS,
                null);
    }

    private static int getDwDesiredAccess(int[] accessFlags) {
        int dwDesiredAccess = DEFAULT_DW_DESIRED_ACCESS_FLAGS;
        if (accessFlags.length > 0) {
            for (int accessFlag : accessFlags) {
                dwDesiredAccess |= accessFlag;
            }
        }
        return dwDesiredAccess;
    }

    private static HANDLE getFileHandle(String path, int... accessFlags) {
        return Kernel32.INSTANCE.CreateFile(
                path,
                getDwDesiredAccess(accessFlags),
                WinNT.FILE_SHARE_READ,
                new WinBase.SECURITY_ATTRIBUTES(),
                WinNT.OPEN_EXISTING,
                WinNT.FILE_ATTRIBUTE_NORMAL,
                null);
    }

    static void preserveLastAccessTime(HANDLE hFile, String path) {
        int rc = Kernel32.INSTANCE.SetFileTime(hFile, null, UNCHANGED_ACCESS_TIME, null);
        if (rc != 1) {
            logger.error("Failed to preserve access time for ({}), reason {}", path, Kernel32Util.getLastErrorMessage());
        } else if (logger.isTraceEnabled()) {
            logger.trace("Access time will be preserved for ({}): {}", path, Kernel32Util.getLastErrorMessage());
        }
    }

    private static boolean canOpenFile(String path, int dwDesiredAccess, int dwFlagsAndAttributes) {
        HANDLE hnd = Kernel32.INSTANCE.CreateFile(
                path,
                dwDesiredAccess,
                WinNT.FILE_SHARE_READ,
                new WinBase.SECURITY_ATTRIBUTES(),
                WinNT.OPEN_EXISTING,
                dwFlagsAndAttributes,
                null);
        boolean res = (hnd != WinNT.INVALID_HANDLE_VALUE);
        if (res) {
            Kernel32.INSTANCE.CloseHandle(hnd);
        }
        return res;
    }

    public static boolean isDirectory(Path path, boolean isImpersonateBeforeAction, boolean ignoreAccessErrors) {
        boolean[] isDirectory = new boolean[]{false};
        try {
            doPrivliged(() -> {
                String absolutePath = path.toFile().getAbsolutePath();
                WinNT.HANDLE hFile = null;
                try {
                    hFile = getFileHandle(absolutePath);
                    if (hFile != WinNT.INVALID_HANDLE_VALUE) {
                        isDirectory[0] = false;
                    } else {
                        hFile = getDirHandle(absolutePath);
                        if (hFile != WinNT.INVALID_HANDLE_VALUE) {
                            isDirectory[0] = true;
                            if (doubleCheckDirectory) {
                                int attrib = getFileAttributesByHandle(hFile);
                                if (attrib == 0) {
                                    logger.warn("Failed to get dirHandle attributes for {}. Reason {}", path, Kernel32Util.getLastErrorMessage());
                                } else {
                                    if ((attrib & 16 /* WinNT.FILE_ATTRIBUTE_DIRECTORY */) == 0) {
                                        logger.debug("Found non-directory attributes of a dirHandle {}. Attributes={}", path, attrib);
                                        if (forceIsDirectoryByAttributes) {
                                            isDirectory[0] = false;
                                        }
                                    }
                                }
                            }
                        } else {
                            String errMsg = Kernel32Util.getLastErrorMessage();
                            logger.warn("Failed to check isDirectory for {}. Reason {}", path, errMsg);
                            if (logger.isTraceEnabled()) {
                                boolean fileNoSec = canOpenFile(absolutePath, WinNT.GENERIC_READ | WinNT.FILE_WRITE_ATTRIBUTES, WinNT.FILE_ATTRIBUTE_NORMAL);
                                boolean dirNoSec = canOpenFile(absolutePath, WinNT.GENERIC_READ | WinNT.FILE_WRITE_ATTRIBUTES, WinNT.FILE_FLAG_BACKUP_SEMANTICS);
                                boolean fileNoSecWA = canOpenFile(absolutePath, WinNT.GENERIC_READ, WinNT.FILE_ATTRIBUTE_NORMAL);
                                boolean dirNoSecWA = canOpenFile(absolutePath, WinNT.GENERIC_READ, WinNT.FILE_FLAG_BACKUP_SEMANTICS);
                                boolean fileReadData = canOpenFile(absolutePath, WinNT.FILE_READ_DATA, WinNT.FILE_ATTRIBUTE_NORMAL);
                                boolean dirReadData = canOpenFile(absolutePath, WinNT.FILE_READ_DATA, WinNT.FILE_FLAG_BACKUP_SEMANTICS);
                                logger.trace("Detailed access status: file GR+WA={}, directory GR+WA={}, file GR={}, directory GR={}, file RD={}, directory RD={}",
                                        okFailed(fileNoSec), okFailed(dirNoSec), okFailed(fileNoSecWA), okFailed(dirNoSecWA), okFailed(fileReadData), okFailed(dirReadData));
                            }
                            if (!ignoreAccessErrors) {
                                boolean shouldTrowException = true;
                                for (String err : fileShareMapErrorIgnore) {
                                    if (errMsg.contains(err)) {
                                        shouldTrowException = false;
                                    }
                                }

                                if (shouldTrowException) {
                                    throw new AccessException(errMsg + " for " + absolutePath);
                                }
                            }
                        }
                    }
                    if (hFile != WinNT.INVALID_HANDLE_VALUE) {
                        preserveLastAccessTime(hFile, absolutePath);
                    }
                } finally {
                    Kernel32.INSTANCE.CloseHandle(hFile);
                }
            }, isImpersonateBeforeAction);
        } catch (IOException e) {
            logger.error("Caught IO exception", e);
        }
        if (logger.isTraceEnabled()) {
            logger.trace("**** isDirectory {} for ({})", isDirectory[0], path);
        }
        return isDirectory[0];
    }

    private static String okFailed(final boolean status) {
        return status ? "ok" : "failed";
    }

    public static long fileSize(String filename, boolean isImpersonateBeforeAction) {
        long[] size = new long[]{0};
        try {
            doPrivliged(() -> {
                HANDLE handle = getFileHandle(filename);
                try {
                    preserveLastAccessTime(handle, filename);
                    Memory pFsi = new Memory(FILE_STANDARD_INFO.sizeOf());
                    boolean succeeded = Kernel32.INSTANCE.GetFileInformationByHandleEx(handle, WinBase.FileStandardInfo, pFsi, new DWORD(pFsi.size()));
                    if (!succeeded) {
                        size[0] = 0;
                        logger.error("Failed to get file size {}", Kernel32Util.getLastErrorMessage());
                    }
                    FILE_STANDARD_INFO fsi = new FILE_STANDARD_INFO(pFsi);
                    size[0] = fsi.EndOfFile.getValue();
                } finally {
                    Kernel32.INSTANCE.CloseHandle(handle);
                }
            }, isImpersonateBeforeAction);
        } catch (IOException e) {
            logger.error("Failed to execute fileSize ", e);
        }
        if (logger.isTraceEnabled()) {
            logger.info("**** fileSize {} for ({})", size[0], filename);
        }
        return size[0];
    }


    private static class AclHandleAction implements HandleAction {

        private final ClaFilePropertiesDto props;

        AclHandleAction(ClaFilePropertiesDto props) {
            this.props = props;
        }

        @Override
        public void execute(HandleWrapper handleWrapper) throws IOException {
            HANDLE handle = handleWrapper.handle;
            PointerByReference ppsidOwner = new PointerByReference();
            PointerByReference ppsidGroup = new PointerByReference();
            PointerByReference ppDacl = new PointerByReference();
            PointerByReference ppSacl = new PointerByReference();
            PointerByReference ppSecurityDescriptor = new PointerByReference();
            try {
                int infoType = WinNT.OWNER_SECURITY_INFORMATION
                        | WinNT.GROUP_SECURITY_INFORMATION
                        | WinNT.DACL_SECURITY_INFORMATION
                        | WinNT.SACL_SECURITY_INFORMATION;

                int rc = Advapi32.INSTANCE.GetSecurityInfo(
                        handle,
                        AccCtrl.SE_OBJECT_TYPE.SE_FILE_OBJECT,
                        infoType,
                        ppsidOwner,
                        ppsidGroup,
                        ppDacl,
                        ppSacl,
                        ppSecurityDescriptor);
                if (rc != 0) {
                    throw new IOException(String.format("Failed to get security info for (%s), reason: %s", props.getFileName(), Kernel32Util.getLastErrorMessage()));
                }
                props.setOwnerName(getAccountBySid(new PSID(ppsidOwner.getValue())).fqn);
                ACL pAcl = new ACL(ppDacl.getValue());
                ACCESS_ACEStructure[] aceStructures = pAcl.getACEStructures();
                for (ACCESS_ACEStructure aceStructure : aceStructures) {
                    boolean deny = aceStructure.AceType == WinNT.ACCESS_DENIED_ACE_TYPE;
                    Advapi32Util.Account accountBySid = getAccountBySid(aceStructure);
                    String principalName = StringUtils.isBlank(accountBySid.domain) ? "\\" + accountBySid.fqn : accountBySid.fqn;
                    if (deny && (aceStructure.Mask & WinNT.FILE_READ_DATA) == WinNT.FILE_READ_DATA) {
                        props.addDeniedReadPrincipalName(principalName);
                        if (logger.isTraceEnabled()) {
                            logger.trace("**** Added for ({}) to denied read principals {}", props.getFileName(), principalName);
                        }
                    } else if ((aceStructure.Mask & WinNT.FILE_READ_DATA) == WinNT.FILE_READ_DATA) {
                        props.addAllowedReadPrincipalName(principalName);
                        if (logger.isTraceEnabled()) {
                            logger.trace("**** Added for ({}) to allowed read principals {}", props.getFileName(), principalName);
                        }
                    }
                    if (deny && (aceStructure.Mask & WinNT.FILE_WRITE_DATA) == WinNT.FILE_WRITE_DATA) {
                        props.addDeniedWritePrincipalName(principalName);
                        if (logger.isTraceEnabled()) {
                            logger.trace("**** Added for ({}) to denied write principals {}", props.getFileName(), principalName);
                        }
                    } else if ((aceStructure.Mask & WinNT.FILE_WRITE_DATA) == WinNT.FILE_WRITE_DATA) {
                        props.addAllowedWritePrincipalName(principalName);
                        if (logger.isTraceEnabled()) {
                            logger.trace("**** Added for ({}) to allowed write principals {}", props.getFileName(), principalName);
                        }
                    }
                }
                props.calculateAclSignature();
            } finally {
                try {
                    Kernel32Util.freeLocalMemory(ppSecurityDescriptor.getValue());
                } catch (Throwable t) {
                    logger.warn("Failed to free ppSecurityDescriptor memory, might cause memory issues", t);
                }
            }
        }

        private Advapi32Util.Account getAccountBySid(ACCESS_ACEStructure aceStructure) {
            return getAccountBySid(aceStructure.getSID());
        }

        private Advapi32Util.Account getAccountBySid(PSID sid) {
            try {
                return Advapi32Util.getAccountBySid(sid);
            } catch (Exception e) {
                logger.trace("Failed to get account details for {}, reason: {}", sid, e.getMessage());
                Advapi32Util.Account account = new Advapi32Util.Account();
                account.fqn = sid.getSidString();
                account.domain = "UNRESOLVED";
                account.name = "UNRESOLVED";
                return account;
            }
        }
    }

    private static class AttributesHandleAction implements HandleAction {

        private final ClaFilePropertiesDto props;

        AttributesHandleAction(ClaFilePropertiesDto props) {
            this.props = props;
        }

        @Override
        public void execute(HandleWrapper handleWrapper) throws IOException {
            HANDLE handle = handleWrapper.handle;
            Memory pFbi = new Memory(WinBase.FILE_BASIC_INFO.sizeOf());
            boolean succeeded = Kernel32.INSTANCE.GetFileInformationByHandleEx(handle, WinBase.FileBasicInfo, pFbi, new WinDef.DWORD(pFbi.size()));
            if (!succeeded) {
                throw new IOException(String.format("Failed to get file basic info for (%s), reason: %s", props.getFileName(), Kernel32Util.getLastErrorMessage()));
            }
            WinBase.FILE_BASIC_INFO fbi = new WinBase.FILE_BASIC_INFO(pFbi);
            props.setModTimeMilli(fileTimeInMillis(fbi.LastWriteTime));
            props.setAccessTimeMilli(fileTimeInMillis(fbi.LastAccessTime));
            props.setCreationTimeMilli(fileTimeInMillis(fbi.CreationTime));
            props.setOffline((fbi.FileAttributes & WinNT.FILE_ATTRIBUTE_OFFLINE) == WinNT.FILE_ATTRIBUTE_OFFLINE);
            Memory pFsi = new Memory(WinBase.FILE_STANDARD_INFO.sizeOf());
            succeeded = Kernel32.INSTANCE.GetFileInformationByHandleEx(handle, WinBase.FileStandardInfo, pFsi, new DWORD(pFsi.size()));
            if (!succeeded) {
                throw new IOException(String.format("Failed to get file standard info (e.g. size) for (%s), reason: %s", props.getFileName(), Kernel32Util.getLastErrorMessage()));
            }
            WinBase.FILE_STANDARD_INFO fsi = new WinBase.FILE_STANDARD_INFO(pFsi);
            props.setFileSize(fsi.EndOfFile.getValue());
            // TODO Itai: [DAMAIN-3075] demutexify when adding support for access-time-preservation of PST files
            props.setFolder(handleWrapper.directory);
        }
    }

    public static void doPrivliged(IOAction action, boolean isImpersonateBeforeAction) throws IOException {
        if (!isImpersonateBeforeAction) {
            // Allow override impersonation logic
            action.execute();
            return;
        }
        long ts = System.currentTimeMillis();
        WinNT.HANDLEByReference phToken = new WinNT.HANDLEByReference();
        WinNT.HANDLEByReference phTokenDuplicate = new WinNT.HANDLEByReference();
        try {
            boolean impersontating = false;
            if (!Advapi32.INSTANCE.OpenThreadToken(
                    Kernel32.INSTANCE.GetCurrentThread(),
                    WinNT.TOKEN_ADJUST_PRIVILEGES,
                    false,
                    phToken)) {

                int lastErrorCode = Kernel32.INSTANCE.GetLastError();
                throwIfFailed(String.format("OpenThreadToken - expected error code to be %d (W32Errors.ERROR_NO_TOKEN) but got %d", W32Errors.ERROR_NO_TOKEN, lastErrorCode),
                        W32Errors.ERROR_NO_TOKEN == lastErrorCode);

                // OpenThreadToken may fail with W32Errors.ERROR_NO_TOKEN if current thread is anonymous.  When this happens,
                // we need to open the process token to duplicate it, then set our thread token.
                throwIfFailed("OpenProcessToken",
                        Advapi32.INSTANCE.OpenProcessToken(Kernel32.INSTANCE.GetCurrentProcess(), WinNT.TOKEN_DUPLICATE, phToken));

                // Process token opened, now duplicate
                throwIfFailed("DuplicateTokenEx",
                        Advapi32.INSTANCE.DuplicateTokenEx(
                                phToken.getValue(),
                                WinNT.TOKEN_ADJUST_PRIVILEGES | WinNT.TOKEN_IMPERSONATE,
                                null,
                                WinNT.SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation,
                                WinNT.TOKEN_TYPE.TokenImpersonation,
                                phTokenDuplicate));

                // And set thread token.
                throwIfFailed("SetThreadToken", Advapi32.INSTANCE.SetThreadToken(null, phTokenDuplicate.getValue()));

                impersontating = true;
            }

            // Which token to adjust depends on whether we had to impersonate or not.
            WinNT.HANDLE tokenAdjust = impersontating ? phTokenDuplicate.getValue() : phToken.getValue();

            WinNT.TOKEN_PRIVILEGES tp = new WinNT.TOKEN_PRIVILEGES(1);
            WinNT.LUID pLuid = new WinNT.LUID();

            throwIfFailed("LookupPrivilegeValue - SE_RESTORE_NAME",
                    Advapi32.INSTANCE.LookupPrivilegeValue(null, WinNT.SE_SECURITY_NAME, pLuid));

            tp.Privileges[0] = new WinNT.LUID_AND_ATTRIBUTES(pLuid, new WinDef.DWORD(WinNT.SE_PRIVILEGE_ENABLED));
            throwIfFailed("AdjustTokenPrivileges",
                    Advapi32.INSTANCE.AdjustTokenPrivileges(tokenAdjust, false, tp, 0, null, null));

            throwIfFailed("LookupPrivilegeValue - SE_RESTORE_NAME",
                    Advapi32.INSTANCE.LookupPrivilegeValue(null, WinNT.SE_RESTORE_NAME, pLuid));
            tp.Privileges[0] = new WinNT.LUID_AND_ATTRIBUTES(pLuid, new DWORD(WinNT.SE_PRIVILEGE_ENABLED));

            throwIfFailed("AdjustTokenPrivileges",
                    Advapi32.INSTANCE.AdjustTokenPrivileges(tokenAdjust, false, tp, 0, null, null));

            IOException exp = null;
            try {
                action.execute();
            } catch (IOException e) {
                exp = e;
            }

            if (impersontating) {
                throwIfFailed("SetThreadToken",
                        Advapi32.INSTANCE.SetThreadToken(null, null));
            } else {
                tp.Privileges[0] = new WinNT.LUID_AND_ATTRIBUTES(pLuid, new DWORD(0));
                throwIfFailed("RevertTokenPrivileges",
                        Advapi32.INSTANCE.AdjustTokenPrivileges(tokenAdjust, false, tp, 0, null, null));
            }
            if (exp != null) {
                throw exp;
            }
        } finally {
            Kernel32Util.closeHandleRefs(phToken, phTokenDuplicate);
        }
        if (logger.isTraceEnabled()) {
            ts = System.currentTimeMillis() - ts;
            logger.trace("**** doPrivileged - () took {} ms", action.getClass().getSimpleName(), ts);
        }
    }

    private static void throwIfFailed(String message, boolean success) throws FileSystemException {
        if (!success) {
            throw new FileSystemException(String.format("%s: %s", message, Kernel32Util.getLastErrorMessage()));
        }
    }


    public interface HandleAction {
        void execute(HandleWrapper handle) throws IOException;
    }

    public interface IOAction {
        void execute() throws IOException;
    }


    public static class HandleWrapper {

        public final HANDLE handle;
        public final boolean directory;

        private HandleWrapper(HANDLE handle, boolean directory) {
            this.handle = handle;
            this.directory = directory;
        }
    }

    public static void setFileShareMapErrorIgnore(List<String> errorIgnore) {
        if (errorIgnore != null) {
            fileShareMapErrorIgnore = errorIgnore;
        }
    }
}
