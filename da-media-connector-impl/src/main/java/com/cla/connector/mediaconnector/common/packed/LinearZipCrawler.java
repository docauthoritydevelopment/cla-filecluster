package com.cla.connector.mediaconnector.common.packed;

import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.utils.FileNamingUtils;
import com.google.common.io.ByteStreams;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Enumeration;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.cla.connector.utils.pckg.PackagePathResolver.PACKAGE_SEPARATOR;

public class LinearZipCrawler implements PackageCrawler {
	private static final Logger logger = LoggerFactory.getLogger(LinearZipCrawler.class);
	private static ZipPackageResourceCache packageResourceCache = new ZipPackageResourceCache(60*1000, 30*1000);

	@Override
	public void crawl(ClaFilePropertiesDto packageFileProperties, Predicate<? super String> fileTypePredicate, Consumer<ClaFilePropertiesDto> packageEntryConsumer) throws PackageCrawlException {
		try (ZipFile zipFile = new ZipFile(packageFileProperties.getFileName())) {
			if (needsPassword(zipFile)) {
				throw new PackageCrawlException(ProcessingErrorType.PACKAGE_CRAWL_PASSWORD_PROTECTED, String.format("Given zip is password protected, %s", packageFileProperties.getFileName()));
			}

			for (ZipArchiveEntry archiveEntry : Collections.list(zipFile.getEntries())) {
				if(!fileTypePredicate.test(archiveEntry.getName()))
					continue;
				ClaFilePropertiesDto claFilePropertiesDto = resolveFileDtoProperties(packageFileProperties, archiveEntry);
				packageEntryConsumer.accept(claFilePropertiesDto);
			}
		} catch (IOException e) {
			logger.error("Exception while crawling a ZIP file " + packageFileProperties.getFileName(), e);
			throw new PackageCrawlException("Exception while crawling a ZIP file " + packageFileProperties.getFileName(), e);
		}
	}

	@Override
	public FileContentDto extractPackagedEntry(String packageFile, String entryId) {
		try {
			ZipFile zipFile = packageResourceCache.loadPackageFile(packageFile);
			ZipArchiveEntry entry = zipFile.getEntry(entryId);
			InputStream inputStream = zipFile.getInputStream(entry);
			return new FileContentDto(ByteStreams.toByteArray(inputStream), entryId);
		} catch (IOException e) {
			logger.error("Exception while extracting entry {} from a ZIP file {}", entryId, packageFile);
			throw new RuntimeException("Exception while extracting entry "+ entryId +" from a ZIP file " + packageFile, e);
		}
	}

	@Override
	public ClaFilePropertiesDto extractPackagedEntryProperties(ClaFilePropertiesDto packageFileProperties, String entryId) {
		String packageRootPath = FileNamingUtils.getPackageRootPath(packageFileProperties.getFileName());
		ZipFile zipFile = packageResourceCache.loadPackageFile(packageRootPath);
		ZipArchiveEntry entry = zipFile.getEntry(entryId);
		ClaFilePropertiesDto filePropertiesDto = resolveFileDtoProperties(packageFileProperties, entry);
		return filePropertiesDto;
	}

	private static boolean needsPassword(ZipFile zipFile) {
		Enumeration<ZipArchiveEntry> entries = zipFile.getEntries();
		while (entries.hasMoreElements()) {
			ZipArchiveEntry currentZipArchiveEntry = entries.nextElement();
			if (currentZipArchiveEntry.getGeneralPurposeBit().usesEncryption()) {
				return true;
			}
		}
		return false;
	}

	private static ClaFilePropertiesDto resolveFileDtoProperties(ClaFilePropertiesDto packageFileProps, ZipArchiveEntry packageEntry) {
		String name = buildName(packageFileProps.getFileName(), packageEntry);
		ClaFilePropertiesDto fileProp = new ClaFilePropertiesDto(name, packageEntry.getSize());
		fileProp.setFilePath(Paths.get(packageEntry.getName()));
		fileProp.setMediaItemId(name);
		fileProp.setOwnerName(packageFileProps.getOwnerName());
		fileProp.setCreationTimeMilli(packageEntry.getCreationTime() == null ? packageFileProps.getCreationTimeMilli() : packageEntry.getCreationTime().toMillis());
		fileProp.setModTimeMilli(packageEntry.getLastModifiedDate().getTime());
		fileProp.setFolder(packageEntry.isDirectory());
		fileProp.setFile(!packageEntry.isDirectory());
		fileProp.addAllowedReadPrincipalNames(packageFileProps.getAclReadAllowed());
		fileProp.addDeniedReadPrincipalNames(packageFileProps.getAclReadDenied());
		fileProp.addAllowedWritePrincipalNames(packageFileProps.getAclWriteAllowed());
		fileProp.addDeniedWritePrincipalNames(packageFileProps.getAclWriteDenied());
		fileProp.setAclSignature(fileProp.calculateAclSignature());
		fileProp.setAclInheritanceType(AclInheritanceType.FOLDER);
		return fileProp;
	}

	//TODO: [Costa] maybe abolish this method and just concat zip name + @ + entryName ? (this method was taken as-is from the old implementation)
	private static String buildName(String zipPath, ZipArchiveEntry zipArchiveEntry) {
		String relativePath = zipArchiveEntry.getName();
		int lastSeparatorIdx = relativePath.lastIndexOf("/");
		relativePath = lastSeparatorIdx == relativePath.length() - 1 ? relativePath.substring(0, lastSeparatorIdx) : relativePath;
		String separator = zipArchiveEntry.isDirectory() ? "" : "\\";
		return zipPath + PACKAGE_SEPARATOR + separator + relativePath.replaceAll("/", "\\\\");
	}
}
