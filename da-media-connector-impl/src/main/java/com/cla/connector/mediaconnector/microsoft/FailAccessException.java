package com.cla.connector.mediaconnector.microsoft;

public class FailAccessException extends RuntimeException {

    public FailAccessException() {
    }

    public FailAccessException(String message) {
        super(message);
    }

    public FailAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailAccessException(Throwable cause) {
        super(cause);
    }

    public FailAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
