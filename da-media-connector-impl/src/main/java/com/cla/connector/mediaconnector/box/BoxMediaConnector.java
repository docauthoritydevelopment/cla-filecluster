package com.cla.connector.mediaconnector.box;

import com.box.sdk.*;
import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.file.*;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static java.util.Optional.*;



/**
 * Media connector for box servers *
 */
public class BoxMediaConnector extends AbstractMediaConnector {
	//region ...member fields
	private static final Logger logger = LoggerFactory.getLogger(BoxMediaConnector.class);

	private BoxAPIConnection api;
	private String jwt;
	private int boxThrottlingRetries;
	private int boxParallelScan;
	int MAX_CACHE_ENTRIES = 100;
	private final static MediaType BOX = MediaType.BOX; //used to shorten code

	/**
	 * This field is used to inject the folder/file unique box id into the D.A. folder/file naming convention.
	 * Using this separator(indicator) it is possible to extract the folder ID needed to directly access the folder.
	 * And yes, it is a hack.
	 */
	public static final  String MEDIA_ENTITY_ID_SEPARATOR = "#MEID#";

	private static String PATH_SEPARATOR = BOX.getPathSeparator();

	private static String[] FOLDER_INFO_PROPERTIES_RETRIEVE = {"type", "id", "sequence_id", "etag", "name", "created_at", "modified_at","size", "path_collection", "created_by", "modified_by",
			"content_created_at", "content_modified_at", "owned_by", "shared_link", "parent",
			"item_status", "item_collection", "permissions"};

	final BoxEvent.Type[] BOX_CHANGE_LOG_EVENT_TYPES = {BoxEvent.Type.DELETE, BoxEvent.Type.UNDELETE, BoxEvent.Type.UPLOAD,	BoxEvent.Type.EDIT,	BoxEvent.Type.RENAME,
			BoxEvent.Type.COPY, BoxEvent.Type.MOVE, BoxEvent.Type.ITEM_COPY, BoxEvent.Type.ITEM_CREATE, BoxEvent.Type.ITEM_MODIFY, BoxEvent.Type.ITEM_MOVE, BoxEvent.Type.ITEM_RENAME,
			BoxEvent.Type.UNSHARE, BoxEvent.Type.COLLABORATION_ROLE_CHANGE, BoxEvent.Type.COLLABORATION_REMOVE, BoxEvent.Type.COLLABORATION_ACCEPT, BoxEvent.Type.CHANGE_FOLDER_PERMISSION};

	private LoadingCache<String, BoxFile> fileLoadingCache =
			CacheBuilder.newBuilder()
					.maximumSize(500)
					.expireAfterAccess(60, TimeUnit.SECONDS)
					.build(new CacheLoader<String, BoxFile>() {
						@Override
						public BoxFile load(String itemId) throws Exception {
							return callWithRetry(3, 50, ()->new BoxFile(getApi(), itemId));
						}
					});
	private LoadingCache<BoxFile, BoxFile.Info> fileInfoLoadingCache =
			CacheBuilder.newBuilder()
					.maximumSize(500)
					.expireAfterAccess(60, TimeUnit.SECONDS)
					.build(new CacheLoader<BoxFile, BoxFile.Info>() {
						@Override
						public BoxFile.Info load(BoxFile file) throws Exception {
							return callWithRetry(3, 50, file::getInfo);
						}
					});

	private Cache<String, Collection<BoxCollaboration.Info>> folderCollaborationCache = CacheBuilder.newBuilder().maximumSize(500).expireAfterAccess(60, TimeUnit.SECONDS).build();
	private Cache<String, BoxResourceIterable<BoxCollaboration.Info>> fileCollaborationCache = CacheBuilder.newBuilder().maximumSize(500).expireAfterAccess(60, TimeUnit.SECONDS).build();

	private Consumer<Map<String, String>> eventStreamPositionConsumer;


	public BoxMediaConnector(String jwt, int boxThrottlingRetries, int boxParallelScan) {
		this.jwt = jwt;
		this.boxParallelScan = boxParallelScan;
		this.boxThrottlingRetries = boxThrottlingRetries;
	}

	public BoxAPIConnection getApi() {
		if(api == null || api.needsRefresh()) {
			api = callWithRetry(2, 200, this::refreshApiConnection);
		}
		return api;
	}

	//region ...implemented public methods
	@Override
	public ClaFilePropertiesDto getFileAttributes(String fileId) throws FileNotFoundException {
		return getMediaItemAttributes(fileId, true);
	}

	@Override
	public MediaType getMediaType() {
		return BOX;
	}

	@Override
	public void streamMediaItems(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
								 Consumer<DirListingPayload> directoryListingConsumer,
								 Consumer<String> createScanTaskConsumer, Predicate<Pair<Long, Long>> scanActivePredicate,
								 ProgressTracker filePropsProgressTracker) {
		String rootMediaEntityId = resolveScanRootMediaEntityId(scanParams);
		BoxFolder rootFolder = new BoxFolder(getApi(), rootMediaEntityId);
		Consumer<ClaFilePropertiesDto> consumerReporter = fp->{
			filePropertiesConsumer.accept(fp);
			if(fp.isFile()) filePropsProgressTracker.incrementProgress(1L);
		};
		Executor fileDiscoverExecutor = new ThreadPoolExecutor(this.boxParallelScan, this.boxParallelScan, 0, TimeUnit.MINUTES, new SynchronousQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());
		final Predicate<? super String> fileTypesPredicate = FileTypeUtils.createFileTypesPredicate(scanParams.getScanTypeSpecification());
		crawl(rootFolder, consumerReporter, fileDiscoverExecutor, fileTypesPredicate);
	}

	@Override
	public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
										   Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
										   Consumer<DirListingPayload> directoryListingConsumer,
										   Consumer<String> createScanTaskConsumer,
										   Predicate<Pair<Long, Long>> scanActivePredicate,
										   ProgressTracker filePropsProgressTracker) {

		String entityId = resolveScanRootMediaEntityId(scanParams);

		Predicate<? super String> fileTypesPredicate = FileTypeUtils.createFileTypesPredicate(scanParams.getScanTypeSpecification());
		BoxCrawlRecursiveAction action = (BoxCrawlRecursiveAction) BoxCrawlRecursiveAction.Builder.create()
				.withMediaConnector(this)
				.withPath(entityId)
				.withMediaEntityId(entityId)
				.withCreateScanTaskConsumer(createScanTaskConsumer)
				.withDirListingConsumer(directoryListingConsumer)
				.withFilePropsConsumer(filePropertiesConsumer)
				.withFileTypesPredicate(fileTypesPredicate)
				.withJobStatePredicate(scanActivePredicate)
				.withProgressTracker(filePropsProgressTracker)
				.withScanParams(scanParams)
				.build();

		forkJoinPool.invoke(action);
	}

	@Override
	public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
		return true; //TODO
	}

	@Override
	public void fetchAcls(ClaFilePropertiesDto claFileProp) {

	}

	@Override
	public List<ServerResourceDto> testConnection() {
		BoxUser.Info userInfo = BoxUser.getCurrentUser(getApi()).getInfo();
		ServerResourceDto serverResourceDto = new ServerResourceDto();
		serverResourceDto.setFullName(userInfo.getLogin());//returning the Service Account login as the name
		List<ServerResourceDto> subFolders = browseSubFolders(null);
		subFolders.clear();
		subFolders.add(serverResourceDto);
		return subFolders;
	}

	@Override
	public String getIdentifier() {
		return null;
	}

	@Override
	public ClaFilePropertiesDto getMediaItemAttributes(@NotNull String mediaItemId) throws FileNotFoundException {
		return getMediaItemAttributes(mediaItemId, false);
	}

	@Override
	public ClaFilePropertiesDto getMediaItemAttributes(String mediaItemId, boolean fetchAcls) throws FileNotFoundException {
		BoxFile file = fileLoadingCache.getUnchecked(mediaItemId);
		ClaFilePropertiesDto fileProperties = resolveFileProperties(file, fetchAcls);
		return fileProperties;
	}

	@Override
	public FileContentDto getFileContent(String fileId, boolean forUserDownload) {
		BoxFile file = new BoxFile(getApi(), fileId);
		try (ByteArrayOutputStream outstream = new ByteArrayOutputStream()){
			file.download(outstream);
			byte[] bytes = outstream.toByteArray();
			return new FileContentDto(bytes, fileId);
		} catch (IOException e) {
			logger.error("Couldn`t download file.",e);
		}
		return null;
	}

	@Override
	public InputStream getInputStream(ClaFilePropertiesDto props) {
		FileContentDto fileContent = getFileContent(props.getMediaItemId(), false);
		return fileContent != null ?
				fileContent.getInputStream()
				: null;
	}

	@Override
	public void concurrentStreamMediaChangeLog(
			ForkJoinPool forkJoinPool, ScanTaskParameters scanParams, String realPath, Long runId,
			long startChangeLogPosition, Consumer<MediaChangeLogDto> changeConsumer,
			Consumer<String> createScanTaskConsumer, Predicate<? super String> fileTypesPredicate) {

		streamMediaChangeLog(
				scanParams, realPath, runId, startChangeLogPosition, changeConsumer, createScanTaskConsumer,
				fileTypesPredicate);
	}

	@Override
	public void streamMediaChangeLog(
			ScanTaskParameters scanParams, String realPath, Long runId, long startChangeLogTimeMillis,
			Consumer<MediaChangeLogDto> changeConsumer, Consumer<String> createScanTaskConsumer,
			Predicate<? super String> fileTypesPredicate) {

		Map<String, String> keyValuePairs = scanParams.getKeyValuePairs();
		String scanDate = keyValuePairs.get("box-delta-position.lastScanDate.RF" + scanParams.getRootFolderId().toString());
		String streamPosition = keyValuePairs.get("box-delta-position.position.RF" + scanParams.getRootFolderId().toString());

		long startMillis = Strings.isNullOrEmpty(scanDate) ? startChangeLogTimeMillis : Long.parseLong(scanDate);
		Date startDate = new Date(startMillis);
		Date endDate = new Date(System.currentTimeMillis());
		List<MediaChangeLogDto> changedFolders = new ArrayList<>();

		logger.debug("Streaming change log for items under {}, for date range:{} to {} at stream position {}", realPath, startDate, endDate, streamPosition);
		final String initialPosition = Strings.emptyToNull( streamPosition);
		DeletionParentPathResolver deletionParentPathResolver = new DeletionParentPathResolver(t -> getBoxEvents(startDate, endDate, initialPosition, t), () -> getApi());

		EventLog eventLog = getBoxEvents(startDate, endDate, initialPosition, BOX_CHANGE_LOG_EVENT_TYPES);
		String nextStreamPosition = eventLog.getNextStreamPosition();
		HashSet<BoxEvent.Type> types = new HashSet<>(Arrays.asList(BOX_CHANGE_LOG_EVENT_TYPES));
		while (!Objects.equals(nextStreamPosition, streamPosition)) {
			for (BoxEvent changeEvent : eventLog){
				if(!types.contains(changeEvent.getType()))//guarding against the inconsistent filtering on API
					continue;
				Optional<MediaChangeLogDto> changeLogOpt = convertChangeItemToMediaChangeLogDto(changeEvent, deletionParentPathResolver);
				if(!changeLogOpt.isPresent()){
					continue; //if the item is already missing, means that it was deleted later. thus so skipping current event
				}
				MediaChangeLogDto changeLogDto = changeLogOpt.get();
				boolean shouldSkipFileType = !(changeLogDto.isFile() && fileTypesPredicate.test(changeLogDto.getFileName()));
				if(!isItemBelongToRootFolder(changeLogDto.getFileName(), scanParams) || shouldSkipFileType) {
					continue;
				}
				changeConsumer.accept(changeLogDto); //Accepting all changes. The appserver will delete or create new data as needed
				if(changeLogDto.isFolder() && DiffType.RENAMED == changeLogDto.getEventType()) {
					changedFolders.add(changeLogDto);
				}
			}
			eventLog = getBoxEvents(startDate, endDate, nextStreamPosition);
			streamPosition = eventLog.getStreamPosition();
			nextStreamPosition = eventLog.getNextStreamPosition();
		}
		String lastEventPosition = nextStreamPosition;
		eventStreamPositionConsumer.accept(new HashMap<String,String>(){{ put("box-delta-position.position.RF" + scanParams.getRootFolderId(),
				lastEventPosition); }});
		logger.info("Saved BOX last stream position for RF {} at {}", scanParams.getPath(), nextStreamPosition);

		/*logger.debug("Re-crawling {} RENAMED/MOVED folders", changedFolders.size());
		Consumer<ClaFilePropertiesDto> logConsumerWrapper = getChangeLogConsumerWrapper(changeConsumer);
		changedFolders.forEach(folder->streamMediaItems(scanParams.copy().setRootFolderMediaEntityId(folder.getMediaItemId()),
				logConsumerWrapper, dlp -> {}, s -> {}, null, dummyTracker));*/
	}
	//endregion

	public void setLastEventStreamPositionConsumer(Consumer<Map<String, String>> consumer) {
		this.eventStreamPositionConsumer = consumer;
	}

	public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
		BoxFolder parentFolder =  Strings.isNullOrEmpty(folderMediaEntityId) ? BoxFolder.getRootFolder(getApi())
				: new BoxFolder(getApi(), folderMediaEntityId);
		try {//this section checks whether we got path or entityID. Path will cause 404 exception.
			parentFolder.getInfo();
		}catch(BoxAPIResponseException be){
			if(be.getResponseCode() == 404){//handling as path
				String entityIdFromPath = resolveEntityIdFromPath(folderMediaEntityId);
				parentFolder = new BoxFolder(getApi(), entityIdFromPath);
			}
		}
		List<ServerResourceDto> result = StreamSupport.stream(parentFolder.getChildren(FOLDER_INFO_PROPERTIES_RETRIEVE).spliterator(), false)
				.filter(i -> i instanceof BoxFolder.Info)
				.map(folderInfo -> new ServerResourceDto(BOX.toNormalizedFolderPath(resolvePath(folderInfo).toString()),
						folderInfo.getName(), ServerResourceType.FOLDER, folderInfo.getID()))
				.collect(Collectors.toList());
		return result;
	}

	public List<BoxCrawlRecursiveAction> expandFolder(String folderId, ScanTaskParameters scanParams,
												   Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
												   Consumer<DirListingPayload> directoryListingConsumer,
												   Consumer<String> createScanTaskConsumer,
												   Predicate<Pair<Long, Long>> scanActivePredicate,
												   ProgressTracker filePropsProgressTracker){
		List<BoxCrawlRecursiveAction> actions =	Collections.synchronizedList(new ArrayList<>());
		Executor folderDiscoverExecutor = new ThreadPoolExecutor(this.boxParallelScan, this.boxParallelScan, 0, TimeUnit.MINUTES, new SynchronousQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());
		Executor fileDiscoverExecutor = new ThreadPoolExecutor(this.boxParallelScan, this.boxParallelScan, 0, TimeUnit.MINUTES, new SynchronousQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());

		logger.debug("Stream files from Box folder, id={}", folderId);
		BoxFolder folder = callWithRetry(3, 300, ()-> new BoxFolder(getApi(), folderId));
		Iterable<BoxItem.Info> folderChildren = callWithRetry(3, 200, () ->folder.getChildren(FOLDER_INFO_PROPERTIES_RETRIEVE));
		Predicate<? super String> fileTypesPredicate = FileTypeUtils.createFileTypesPredicate(scanParams.getScanTypeSpecification());
		for (BoxItem.Info itemInfo : folderChildren) {
			logger.trace("discovered item: "+ itemInfo.getName());
			if(itemInfo instanceof BoxFile.Info && fileTypesPredicate.test(itemInfo.getName())){
				fileInfoLoadingCache.put(((BoxFile.Info) itemInfo).getResource(), (BoxFile.Info) itemInfo);
				fileDiscoverExecutor.execute(()-> {
					ClaFilePropertiesDto filePropertiesDto = callWithRetry(3, 500, ()->{
						BoxFile file = new BoxFile(getApi(), itemInfo.getID());
						return resolveFileProperties(file, false);
					});
					filePropertiesConsumer.accept(filePropertiesDto);
					filePropsProgressTracker.incrementProgress(1);
				});
			}
			if (itemInfo instanceof BoxFolder.Info) {
				folderDiscoverExecutor.execute(()-> {
					BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
					ClaFilePropertiesDto folderPropsDto = callWithRetry(3, 400,()->{
						BoxFolder discoveredFolder = new BoxFolder(getApi(), folderInfo.getID());
						return resolveFolderProperties(discoveredFolder, folderInfo, true);
					});
					filePropertiesConsumer.accept(folderPropsDto);
					BoxCrawlRecursiveAction action = (BoxCrawlRecursiveAction) BoxCrawlRecursiveAction.Builder.create()
							.withMediaConnector(this)
							.withPath(BOX.toNormalizedFolderPath(resolvePath(folderInfo).toString()))
							.withMediaEntityId(folderInfo.getID())
							.withCreateScanTaskConsumer(createScanTaskConsumer)
							.withDirListingConsumer(directoryListingConsumer)
							.withFilePropsConsumer(filePropertiesConsumer)
							.withJobStatePredicate(scanActivePredicate)
							.withProgressTracker(filePropsProgressTracker)
							.withScanParams(scanParams)
							.build();
					actions.add(action);
				});
			}
		}

		try {
			((ThreadPoolExecutor) fileDiscoverExecutor).shutdown();
			((ThreadPoolExecutor) fileDiscoverExecutor).awaitTermination(1,TimeUnit.MINUTES );
			((ThreadPoolExecutor) folderDiscoverExecutor).shutdown();
			((ThreadPoolExecutor) folderDiscoverExecutor).awaitTermination(1,TimeUnit.MINUTES );
		} catch (InterruptedException e) {
			logger.error("error awaiting crawl termination", e);
		}
		return actions;
	}

	/**
	 * it is a hacky combina with implanting the mediaItemId into the path.
	 * Please see {@link #resolveEntityIdFromPath} and {@link #MEDIA_ENTITY_ID_SEPARATOR} for more info.
	 * @param path the folder hierarchy path
	 * @param mediaEntityId the unique BOX id
	 * @return concatenation with dedicated separator
	 */
	public String constructPathWithMediaEntityId(String path, String mediaEntityId){
		return StringUtils.stripEnd(path, BOX.getPathSeparator()) + MEDIA_ENTITY_ID_SEPARATOR + mediaEntityId;
	}

	private BoxAPIConnection refreshApiConnection() {
		if(api == null)
		{
			IAccessTokenCache accessTokenCache = new InMemoryLRUAccessTokenCache(MAX_CACHE_ENTRIES);
			try {
				logger.info("Connecting to BOX");
				Reader reader = new StringReader(this.jwt);
				BoxConfig boxConfig = BoxConfig.readFrom(reader);
				api = BoxDeveloperEditionAPIConnection.getAppEnterpriseConnection(boxConfig, accessTokenCache);
				api.setMaxRequestAttempts(this.boxThrottlingRetries);
				//api.setRequestInterceptor(new BoxRequestInterceptor());
			} catch (Exception e) {
				logger.error("error loading BOX config.", e);
			}
		}
		else {
			logger.info("Refreshing BOX connection.");
			api.refresh();
		}

		BoxUser.Info userInfo = BoxUser.getCurrentUser(api).getInfo();
		logger.info("Logged into BOX account with user name:{} login:{}", userInfo.getName(), userInfo.getLogin());
		return api;
	}

	private void crawl(BoxFolder folder, Consumer<ClaFilePropertiesDto> filePropertiesConsumer, Executor fileDiscoverExecutor, Predicate<? super String> fileTypesPredicate) {
		for (BoxItem.Info itemInfo : folder.getChildren(FOLDER_INFO_PROPERTIES_RETRIEVE)) {
			logger.trace("discovered item: "+ itemInfo.getName());
			if(itemInfo instanceof BoxFile.Info && fileTypesPredicate.test(itemInfo.getName())){
				fileInfoLoadingCache.put(((BoxFile.Info) itemInfo).getResource(), (BoxFile.Info) itemInfo);
				fileDiscoverExecutor.execute(()->callWithRetry(3, 300, ()->{
					BoxFile file = new BoxFile(getApi(), itemInfo.getID());
					ClaFilePropertiesDto filePropertiesDto = resolveFileProperties(file, false);
					filePropertiesConsumer.accept(filePropertiesDto);
					return Void.class;}));
			}
			if (itemInfo instanceof BoxFolder.Info) {
				BoxFolder discoveredFolder = callWithRetry(3, 300, ()->{
					BoxFolder inspectedFolder = new BoxFolder(getApi(), itemInfo.getID());
					ClaFilePropertiesDto folderPropsDto = resolveFolderProperties(inspectedFolder, (BoxFolder.Info) itemInfo, true);
					filePropertiesConsumer.accept(folderPropsDto);
					return inspectedFolder;
				});
				crawl(discoveredFolder, filePropertiesConsumer, fileDiscoverExecutor, fileTypesPredicate);
			}
		}
	}

	private ClaFilePropertiesDto resolveFileProperties(BoxFile file, boolean fetchAcl){
		//BoxFile.Info info = fileCollaborationCache.getIfPresent(file.getID());
		BoxFile.Info info = fileInfoLoadingCache.getUnchecked(file);
		ClaFilePropertiesDto itemProperties = resolveProperties(info);

		itemProperties.setType(FileTypeUtils.getFileType(itemProperties.getFileName()));
		if(fetchAcl){
			resolveFileAcl(itemProperties, file);
		}
		itemProperties.setAclSignature(itemProperties.calculateAclSignature());
		return itemProperties;
	}

	private ClaFilePropertiesDto resolveFolderProperties(BoxFolder discoveredFolder, BoxFolder.Info folderInfo, boolean fetchAcl){
		ClaFilePropertiesDto itemProperties = resolveProperties(folderInfo);
		if(fetchAcl) {
			resolveFolderAcl(itemProperties, discoveredFolder);
		}
		itemProperties.setAclSignature(itemProperties.calculateAclSignature());
		return itemProperties;
	}

	private void resolveFolderAcl(ClaFilePropertiesDto itemProperties, BoxFolder discoveredFolder) {
		try {
			Collection<BoxCollaboration.Info> folderCollaborations = folderCollaborationCache.get(discoveredFolder.getID(), () -> discoveredFolder.getCollaborations());
			//Collection<BoxCollaboration.Info> folderCollaborations = discoveredFolder.getCollaborations();
			folderCollaborations.forEach(fa->{
				if(fa.getStatus()==BoxCollaboration.Status.ACCEPTED) {
					BoxCollaborator.Info accessibleBy = fa.getAccessibleBy();
					switch (fa.getRole()){
						case CO_OWNER:
						case EDITOR:
						case OWNER:
						case PREVIEWER_UPLOADER:
						case UPLOADER:
						case VIEWER_UPLOADER:
							itemProperties.addAllowedWritePrincipalName(resolveAclName(accessibleBy));
							itemProperties.addAllowedReadPrincipalName(resolveAclName(accessibleBy));
							break;
						case VIEWER:
						case PREVIEWER:
							itemProperties.addAllowedReadPrincipalName(resolveAclName(accessibleBy));
							break;
					}
				}
			});
		} catch (Exception e) {
			throw new RuntimeException("could not resolve ACL for Box folder id=" + (discoveredFolder != null ? discoveredFolder.getID() : null), e);
		}
	}

	private void resolveFileAcl(ClaFilePropertiesDto fileProperties, BoxFile file) {
		try {
			BoxResourceIterable<BoxCollaboration.Info> allFileCollaborations = fileCollaborationCache.get(file.getID(),() -> file.getAllFileCollaborations());
			allFileCollaborations.forEach(fa->{
				if(fa.getStatus()==BoxCollaboration.Status.ACCEPTED) {
					BoxCollaborator.Info accessibleBy = fa.getAccessibleBy();
					switch (fa.getRole()){
						case CO_OWNER:
						case EDITOR:
						case OWNER:
							fileProperties.addAllowedWritePrincipalName(resolveAclName(accessibleBy));
							fileProperties.addAllowedReadPrincipalName(resolveAclName(accessibleBy));
							break;
						case VIEWER:
							fileProperties.addAllowedReadPrincipalName(resolveAclName(accessibleBy));
							break;
					}
				}
			});
		} catch (Exception e) {
			throw new RuntimeException("could not resolve ACL for box file id=" + (file != null ? file.getID() : null), e);
		}
	}

	private static ClaFilePropertiesDto resolveProperties(BoxItem.Info info){
		boolean isFolder = info instanceof BoxFolder.Info;
		Path fullPath = resolvePath(info);
		String rawPath = fullPath.toString();
		String normalizedPath = isFolder ?
				//NOTE: stripping the / from the folder because of the bug on the server side, see DAMAIN-6224
				StringUtils.stripEnd(BOX.toNormalizedFolderPath(rawPath), PATH_SEPARATOR) :
				BOX.toNormalizedFilePath(rawPath);
		ClaFilePropertiesDto fileProp = new ClaFilePropertiesDto(normalizedPath, info.getSize());
		fileProp.setFilePath(fullPath);
		fileProp.setMediaItemId(info.getID());
		fileProp.setOwnerName(info.getOwnedBy().getName());
		fileProp.setCreationTimeMilli(info.getCreatedAt().getTime());
		fileProp.setModTimeMilli(info.getModifiedAt().getTime());
		fileProp.setFolder(isFolder);
		fileProp.setFile(!isFolder);
		//EnumSet<BoxFile.Permission> permissions = info.getPermissions();
		fileProp.setAclInheritanceType(AclInheritanceType.FOLDER);
		return fileProp;
	}

	private static Path resolvePath(BoxItem.Info info) {
		List<BoxFolder.Info> pathCollection = info.getPathCollection();
		List<String> pathItems =  pathCollection != null ? pathCollection.stream().map(p -> p.getName()).collect(Collectors.toList()) :
									new ArrayList<>();
		pathItems.add(info.getName());
		return Paths.get("", pathItems.toArray(new String[0]));
	}

	/**
	 * Teh recursive action does have the original scan request params but id doesn`t have media entity id for the target subfolder.
	 * So in order to access the actions target folder, i inject the folder ID in the end of the path and separate it by the #MEID# string.
	 * Yeah, I know what you are thinking but it is not my fault, it happens because of legacy constrains
	 * on using the PATH as the unique identifier of entities, event that it is not true for non FILESHARE stores (which use some unique key id).
	 * **/
	private String resolveScanRootMediaEntityId(ScanTaskParameters scanParams){
		String entityId;
		String path = scanParams.getPath();
		if(path.contains(MEDIA_ENTITY_ID_SEPARATOR)){
			entityId = path.substring(path.lastIndexOf(MEDIA_ENTITY_ID_SEPARATOR) + MEDIA_ENTITY_ID_SEPARATOR.length());
		}
		else if (Strings.isNullOrEmpty(scanParams.getRootFolderMediaEntityId())) {
			entityId = resolveEntityIdFromPath(path);
		} else {
			entityId = scanParams.getRootFolderMediaEntityId();
		}
		return entityId;
	}

	private String resolveEntityIdFromPath(String path) {
		logger.info("resolving folder mediaEntityId for Box path={}", path);
		String[] dirs = path.split(Pattern.quote(PATH_SEPARATOR));
		LinkedBlockingDeque<String> pathItems = new LinkedBlockingDeque<>(Arrays.asList(dirs));
		BoxFolder currentFolder =  BoxFolder.getRootFolder(getApi());
		List<ServerResourceDto> subFolders = Arrays.asList(
				new ServerResourceDto(null, currentFolder.getInfo().getName(), ServerResourceType.FOLDER ,currentFolder.getID()));
		String entityIdFromPath = resolveEntityIdFromPath(pathItems, subFolders);
		logger.info("resolved Box folder mediaEntityId={}", entityIdFromPath);
		return entityIdFromPath;
	}

	private String resolveEntityIdFromPath(LinkedBlockingDeque<String> pathItems, List<ServerResourceDto> subFolders) {
		String pathItem = pathItems.pop();
		if(subFolders.stream().anyMatch(s->s.getName().equals(pathItem))){
			ServerResourceDto serverResourceDto = subFolders.stream().filter(s -> s.getName().equals(pathItem)).findFirst().get();
			if(pathItems.isEmpty())
				return serverResourceDto.getId();
			BoxFolder currentFolder = new BoxFolder(api, serverResourceDto.getId());
			subFolders = browseSubFolders(currentFolder.getID());
			return resolveEntityIdFromPath(pathItems, subFolders);
		}
		throw new InvalidPathException(pathItem.concat(PATH_SEPARATOR), "Invalid BOX path");
	}

	public String resolveAclName(BoxCollaborator.Info collaboratorInfo) {
		return collaboratorInfo instanceof BoxGroup.Info ? collaboratorInfo.getName() : collaboratorInfo.getLogin();
	}


	/**
	 * the event type JSONs look in following way:
	 * <pre>
	 * {@code
	 *EventType: COLLABORATION_ROLE_CHANGE
	 * {
	 * 	"folder_id": "51996170034",
	 * 	"folder_name": "Extraction",
	 * 	"user_id": "3877154061",
	 * 	"user_name": "tomer-jwt-test1",
	 * 	"parent": {
	 * 		"type": "folder",
	 * 		"name": "All Files",
	 * 		"id": "0"
	 * 	},
	 * 	"owned_by": {
	 * 		"type": "user",
	 * 		"id": "3874784031",
	 * 		"name": "Tomer W.",
	 * 		"login": "tomer.weinberger@docauthority.com"
	 * 	}
	 * }
	 *}
	 * {@code
	 * EventType: RENAME/COPY/UPLOAD/MOVE/DELETE Item
	 * {
	 * 	"item_type": "folder",
	 * 	"item_id": "62620564824",
	 * 	"item_name": "f1",
	 * 	"parent": {
	 * 		"type": "folder",
	 * 		"name": "CostaTheKiller",
	 * 		"id": "52515758438"
	 * 	},
	 * 	"owned_by": {
	 * 		"type": "user",
	 * 		"id": "3874784031",
	 * 		"name": "Tomer W.",
	 * 		"login": "tomer.weinberger@docauthority.com"
	 * 	}
	 * }
	 * }
	 * </pre>
	 */
	private Optional<MediaChangeLogDto> convertChangeItemToMediaChangeLogDto(BoxEvent event, DeletionParentPathResolver deletionParentPathResolver) {
		DiffType diff = convertToDiffType(event.getType());
		JsonObject eventSourceJSON = event.getSourceJSON();
		String folderId = ofNullable(eventSourceJSON.get("folder_id")).map(JsonValue::asString).orElse(null);
		//the folder_id property is present only in COLLABORATION_... events
		String itemId = diff == DiffType.ACL_UPDATED ?
								folderId :
								eventSourceJSON.get("item_id").asString();
		//item_type is not present on ACL_UPDATED, but the ACLs are on folder level
		boolean isFolder = (diff == DiffType.ACL_UPDATED) || eventSourceJSON.get("item_type").asString().equals("folder");
		String itemType = isFolder ? "folder" : "file";

		if(diff == DiffType.DELETED){
			MediaChangeLogDto deletedChange = resolveDeletionChangeLogDto(event, deletionParentPathResolver);
			return ofNullable(deletedChange);
		}
		try {
			ClaFilePropertiesDto filePropertiesDto = null;
			switch(itemType) {
				case "file":
					BoxFile boxFile = new BoxFile(getApi(), itemId);
					filePropertiesDto = resolveFileProperties(boxFile, false);
					break;
				case "folder":
					BoxFolder folder = new BoxFolder(getApi(), itemId);
					BoxFolder.Info folderInfo = callWithRetry(2, 100, ()->folder.getInfo(FOLDER_INFO_PROPERTIES_RETRIEVE));
					filePropertiesDto = resolveFolderProperties(folder, folderInfo, true);
				break;
			}
			MediaChangeLogDto mediaChangeLogDto = new MediaChangeLogDto(itemId, diff, filePropertiesDto);
			logger.trace("Box event: {}", eventSourceJSON.toString());
			return ofNullable( mediaChangeLogDto);
		} catch (UncheckedExecutionException | NoSuchElementException e) {
			if(e instanceof NoSuchElementException || e.getCause() instanceof NoSuchElementException)
				logger.warn("Box {} #{} is missing. Probably was deleted in a later event.", itemType, itemId);
			return empty();
		}
	}

	private MediaChangeLogDto resolveDeletionChangeLogDto(BoxEvent event, DeletionParentPathResolver deletionParentPathResolver) {
		JsonObject json = event.getSourceJSON();
		boolean isFolder = json.get("item_type").asString().equals("folder");
		String itemId = json.get("item_id").asString();
		String folderIdToResolve = isFolder ?
									itemId :
									deletionParentPathResolver.extractParentId(json);
		String folderPath = deletionParentPathResolver.resolveFolderPath(folderIdToResolve);
		String fullPath = isFolder ?
							folderPath :
							BOX.toNormalizedFilePath(Paths.get(folderPath,
									deletionParentPathResolver.extractName(json)).toString());
		ClaFilePropertiesDto deletedItemProps = new ClaFilePropertiesDto(fullPath, 0L);
		deletedItemProps.setFilePath(Paths.get(fullPath));
		deletedItemProps.setFile(!isFolder);
		deletedItemProps.setFolder(isFolder);
		deletedItemProps.setMediaItemId(itemId);
		return new MediaChangeLogDto(itemId, DiffType.DELETED, deletedItemProps);
	}

	private static DiffType convertToDiffType(BoxEvent.Type type) {
		switch (type){
			case DELETE:
				return DiffType.DELETED;
			case UPLOAD:
			case COPY:
			case ITEM_COPY:
				return DiffType.NEW;
			case UNDELETE:
				return DiffType.UNDELETED;
			case EDIT:
			case ITEM_MODIFY:
				return DiffType.CONTENT_UPDATED;
			case RENAME:
			case ITEM_RENAME:
			case MOVE:
			case ITEM_MOVE:
				return DiffType.RENAMED;
			case COLLABORATION_ROLE_CHANGE:
			case COLLABORATION_REMOVE:
			case COLLABORATION_ACCEPT:
			case CHANGE_FOLDER_PERMISSION:
			case UNSHARE:
			case ITEM_SHARED_UNSHARE:
			case ITEM_SHARED_UPDATE:
				return DiffType.ACL_UPDATED;
			default:
				return null;
		}
	}

	private boolean isItemBelongToRootFolder(String filename, ScanTaskParameters scanParams) {
		//following separation logic is needed in order to terminate the folder/url with appropriate terminator
		String rootFolderPath = scanParams.getPath() +
								(scanParams.getPath().endsWith(PATH_SEPARATOR) ? "" : PATH_SEPARATOR);
		return 	filename.startsWith(rootFolderPath);
	}

	private EventLog getBoxEvents(Date startDate, Date endDate, String streamPosition, BoxEvent.Type...types){
		logger.debug("Streaming change log for items for date range:{} to {} at stream position {}", startDate, endDate, streamPosition);
		EventLog events = EventLog.getEnterpriseEvents(getApi(), streamPosition, startDate, endDate);
		logger.debug("Retrieved change log of {} items. Next stream position {}", events.getSize(), events.getNextStreamPosition());
		return events;
	}
	//endregion

	private <T> T callWithRetry(int retry, int sleepMs, Callable<T> function){
		int nextSleep = 0;
		Exception lastException = null;
		for(int i=0; i<retry; i++){
			try {
				Thread.sleep(nextSleep);
				return function.call();
			}catch(Exception e){
				if(e instanceof BoxAPIResponseException && ((BoxAPIResponseException)e).getResponseCode() == 404){
					throw new NoSuchElementException();
				}
				logger.error("Error while performing action. Try #" + i, e);
				nextSleep = sleepMs;
				lastException = e;
			}
		}
		throw new RuntimeException(lastException);
	}

	private class DeletionParentPathResolver {
		final Map<String,String> deletedFoldersAndTheirParents = new HashMap<>();//pairs of (deleted-folder,parent)
		final Map<String,String> foldersPaths = new HashMap<>();//the resolved path of the folder
		final Set<String> alreadyResolvedFoldersPaths = new HashSet<>();//memoization
		final Supplier<BoxAPIConnection> apiSupplier;

		public DeletionParentPathResolver(Function<BoxEvent.Type[],EventLog> eventsProvider, Supplier<BoxAPIConnection> apiConnectionSupplier) {
			apiSupplier = apiConnectionSupplier;
			EventLog changes = eventsProvider.apply(new BoxEvent.Type[]{BoxEvent.Type.DELETE});
			for (BoxEvent deletionEvent : changes) {
				if(deletionEvent.getType() != BoxEvent.Type.DELETE)
					continue;//sometimes the API event type filter doesn`t work, so i filter manually
				JsonObject deletionJSON = deletionEvent.getSourceJSON();
				if(deletionJSON.get("item_type").asString().equals("folder")) {
					String folderId = deletionJSON.get("item_id").asString();
					deletedFoldersAndTheirParents.put(folderId, extractParentId(deletionJSON));
					foldersPaths.put(folderId, extractName(deletionJSON));
				}
			}
			deletedFoldersAndTheirParents.keySet().forEach(this::resolveFolderPath);
		}

		private String extractName(JsonObject eventSourceJSON) {
			return eventSourceJSON.get("item_name").asString();
		}

		public String extractParentId(JsonObject eventSourceJSON){
			return eventSourceJSON.get("parent").asObject().get("id").asString();
		}

		public String resolveFolderPath(String folderId) {
			if(alreadyResolvedFoldersPaths.contains(folderId)){//using memoization to avoid same work twice
				return foldersPaths.get(folderId);
			}

			String parentId = deletedFoldersAndTheirParents.get(folderId);
			if(parentId == null){//a not deleted parent folder (not in deleted folders list) of a file
				BoxFolder boxFolder = new BoxFolder(apiSupplier.get(), folderId);
				String path = resolvePath(boxFolder.getInfo()).toString();
				return processPath(folderId, path);
			}

			if (isDeletedFolder(parentId)) {// a deleted parent folder. will be recursively resolved first
				String parentFolderPath = resolveFolderPath(parentId);
				return processPath(folderId, parentFolderPath, foldersPaths.get(folderId));
			}
			//arrived to a not-deleted parent folder of the deleted folder
			BoxFolder boxFolder = new BoxFolder(apiSupplier.get(), parentId);
			Path path = resolvePath(boxFolder.getInfo());
			return processPath(folderId, path.toString(), foldersPaths.get(folderId));
		}

		private String processPath(String folderId, String... folders){
			String path = Paths.get("", folders).toString();
			String normalizedFolderPath = BOX.toNormalizedFolderPath(path);
			foldersPaths.put(folderId, normalizedFolderPath);
			alreadyResolvedFoldersPaths.add(folderId);
			return normalizedFolderPath;
		}

		public boolean isDeletedFolder(String folderId){
			return deletedFoldersAndTheirParents.containsKey(folderId);
		}
	}

	private static class BoxRequestInterceptor implements RequestInterceptor {
		private static final Logger logger = LoggerFactory.getLogger(BoxRequestInterceptor.class);
		private static AtomicInteger requestsCounter = new AtomicInteger(0);

		@Override
		public BoxAPIResponse onRequest(BoxAPIRequest request) {
			logger.debug("Box api ["+request.getUrl()+"] calls count=" + requestsCounter.incrementAndGet());
			return null;
		}
	}
}
