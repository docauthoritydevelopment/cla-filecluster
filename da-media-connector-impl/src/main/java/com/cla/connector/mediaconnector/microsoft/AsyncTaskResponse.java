package com.cla.connector.mediaconnector.microsoft;

import java.util.Objects;

public class AsyncTaskResponse<T, E extends Exception> {

    private E exception;

    private T response;

    AsyncTaskResponse(T response) {
        this.response = response;
    }

    AsyncTaskResponse(E ex) {
        this.exception = ex;
    }

    public E getException() {
        return exception;
    }

    public T getResponse() {
        return response;
    }

    public boolean isSucceeded() {
        return exception == null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AsyncTaskResponse<?, ?> that = (AsyncTaskResponse<?, ?>) o;
        return Objects.equals(exception, that.exception) &&
                Objects.equals(response, that.response);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exception, response);
    }

    @Override
    public String toString() {
        return "AsyncTaskResponse{" +
                "exception=" + exception +
                ", response=" + response +
                '}';
    }
}
