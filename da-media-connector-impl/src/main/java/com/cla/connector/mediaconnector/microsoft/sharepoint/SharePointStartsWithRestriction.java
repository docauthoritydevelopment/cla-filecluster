package com.cla.connector.mediaconnector.microsoft.sharepoint;

import com.independentsoft.share.Util;
import com.independentsoft.share.queryoptions.IFilterRestriction;

/**
 * Created by uri on 05/10/2016.
 */
public class SharePointStartsWithRestriction implements IFilterRestriction {

    private String propertyName;
    private String value;

    public SharePointStartsWithRestriction(String propertyName, String value) {
        if(propertyName == null) {
            throw new IllegalArgumentException("propertyName");
        } else if(value == null) {
            throw new IllegalArgumentException("value");
        } else {
            this.propertyName = propertyName;
            this.value = value;
        }
    }

    public String toString() {
        return "startswith(" + Util.encodeUrlInputStream(this.propertyName) + ", '" + this.value + "')";
    }

}
