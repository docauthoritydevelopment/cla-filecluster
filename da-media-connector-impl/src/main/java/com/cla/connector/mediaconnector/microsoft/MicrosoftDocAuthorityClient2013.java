package com.cla.connector.mediaconnector.microsoft;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.cla.connector.utils.CharacterFilterInputStream;
import com.google.common.collect.Lists;
import com.independentsoft.share.ServiceException;
import com.independentsoft.share.Util;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MicrosoftDocAuthorityClient2013 extends MicrosoftDocAuthorityClient {
    private static final Logger logger = LoggerFactory.getLogger(MicrosoftDocAuthorityClient2013.class);

    MicrosoftDocAuthorityClient2013(String userName, String domain, String password, String userAgent, String url,
                                    int maxRetries, MSConnectionConfig connectionConfig, String... charsToFilter) {

        super(userName, domain, password, userAgent, url, maxRetries, connectionConfig, MicrosoftDocAuthorityClient.VERSION_2013, false, charsToFilter);
    }

    @Override
    public List<ClaFilePropertiesDto> getFilesWithMediaItemId(String subSite, String folder, String listId, int pageIdx, int pageSize) throws ServiceException {
        List<ClaFilePropertiesDto> results = super.getFilesWithMediaItemId(subSite, folder, listId, pageIdx, pageSize);
        List<ClaFilePropertiesDto> resultsCompletion = getFilesDataResultCompletion(subSite, folder, pageIdx, pageSize, results);
        Map<String, ClaFilePropertiesDto> mediaItemIdMap =
                results.stream()
                        .collect(Collectors.toMap(ClaFilePropertiesDto::getMediaItemId, Function.identity()));

        resultsCompletion.forEach(file -> {
            ClaFilePropertiesDto dto = mediaItemIdMap.get(file.getMediaItemId());
            if (dto == null) {
                logger.warn("No file matching was found in previous query. Skipping");
                return;
            }
            dto.setOwnerName(file.getOwnerName());
            dto.setCreationTimeMilli(file.getCreationTimeMilli());
            dto.setModTimeMilli(file.getModTimeMilli());
            dto.setFileSize(file.getFileSize());
        });


        return results;
    }

    @NotNull
    private List<ClaFilePropertiesDto> getFilesDataResultCompletion(String subSite, String folder, int pageIdx, int pageSize, List<ClaFilePropertiesDto> results) throws ServiceException {
        List<ClaFilePropertiesDto> resultsCompletion = null;
        try {
            resultsCompletion = getFilesWithMediaItemId2013(subSite, folder, pageIdx, pageSize);
            Map<String, ClaFilePropertiesDto> filenamePropMap = resultsCompletion.stream()
                    .collect(Collectors.toMap(dto -> dto.getFileName().toLowerCase(), Function.identity()));


            List<ClaFilePropertiesDto> dataMissing = Lists.newArrayList();
            results.forEach(file -> {
                ClaFilePropertiesDto toEnrich = filenamePropMap.get(file.getFileName().toLowerCase());
                if (toEnrich != null) {
                    if (file.getMediaItemId() == null) {
                        logger.warn("{} has no media-item-id - skipping", file.getFileName());
                    } else {
                        toEnrich.setMediaItemId(file.getMediaItemId());
                    }
                } else {
                    dataMissing.add(file);
                }
            });

            if (dataMissing.size() > 0) {
                logger.debug("Processing un-matched file(s) (count={})", dataMissing.size());
                resultsCompletion.addAll(getFileDataCompletionOneByOne(subSite, dataMissing));
            }
        } catch (DataAccessException e) {
            logger.error("Failed to obtain data-completion for " + folder + ", falling-back (list)", e);
            if (resultsCompletion == null ){
                resultsCompletion = getFileDataCompletionOneByOne(subSite, results);
            } else {
                resultsCompletion.addAll(getFileDataCompletionOneByOne(subSite, results));
            }
        }
        return resultsCompletion;
    }

    private List<ClaFilePropertiesDto> getFileDataCompletionOneByOne(String subSite, Collection<ClaFilePropertiesDto> fileProps) {
        return fileProps.stream()
                .map(dto -> {
                    String itemId = dto.getMediaItemId();
                    try {
                        return getFileWithMediaItemId_ListMethod(subSite, dto.getMediaItemId());
                    } catch (ServiceException e) {
                        logger.error("Failed to retrieve additional data (list method) for item: " + itemId + ", Filename: " + dto.getFileName(), e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public ClaFilePropertiesDto getFilesAttributes(String subSite, String fileName) throws ServiceException {
        ClaFilePropertiesDto prop = super.getFilesAttributes(subSite, fileName);
        try {
            ClaFilePropertiesDto additional = getFileAttributes2013(fileName);
            prop.setCreationTimeMilli(additional.getCreationTimeMilli());
            prop.setModTimeMilli(additional.getModTimeMilli());
            prop.setFileSize(additional.getFileSize());
        } catch (Exception e) {
            logger.error("Failed to enrich additional data", e);
        }
        return prop;
    }

    private ClaFilePropertiesDto getFileAttributes2013(String filename) throws ServiceException {
        logger.debug("Retrieving file {}", filename);
        String apiUrl = "/GetFileByServerRelativeUrl('" + Util.encodeUrl(filename) + "')";
        return sendRequest(apiUrl, (is) -> {
            try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charsToFilter)) {
                return SharePointParseUtils.convertFileItemFileProperty2013(streamFilter);
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Failed to parse response XML", e);
                if (logger.isDebugEnabled()) {
                    sendRequest(apiUrl, (respInStream) -> {
                        try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(respInStream, charsToFilter)) {
                            logger.warn("Failed call response: {}", IOUtils.toString(streamFilter, "UTF-8"));
                        } catch (IOException e1) {
                            logger.error("Failed to dump ms-response", e1);
                        }

                        return null;
                    });
                }
                throw new ServiceException("Failed to parse response XML", e, apiUrl);
            }
        });
    }

    protected List<ClaFilePropertiesDto> getFilesWithMediaItemId2013(String subSite, String folder, int pageIdx, int pageSize) throws ServiceException {
        logger.debug("Listing files under {} (page index: {}, page size: {})", folder, pageIdx, pageSize);
//        "/_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl("/Shared Documents/qwe") + "')/Files?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties&$expand=ListItemAllFields,Properties"
        String apiUrl = "/GetFolderByServerRelativeUrl('" + Util.encodeUrl(folder) + "')/Files?$expand=Author&$orderby=ServerRelativeUrl&$top=" + pageSize + "&$skip=" + (pageIdx * pageSize);
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charsToFilter)) {
                List<ClaFilePropertiesDto> files = SharePointParseUtils.convertFileItemListToFileList2013(streamFilter);
                files.forEach(file -> file.setFileName(convertFileRefToFileUrl(file.getFileName())));
                return files;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Failed to parse response XML", e);
                if (logger.isDebugEnabled()) {
                    sendRequest(apiUrl, (respInStream) -> {
                        try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(respInStream, charsToFilter)) {
                            logger.warn("Failed call response: {}", IOUtils.toString(streamFilter, "UTF-8"));
                        } catch (IOException e1) {
                            logger.error("Failed to dump ms-response", e1);
                        }

                        return null;
                    });
                }
                throw new ServiceException("Failed to parse response XML", e, apiUrl);
            }
        });
    }

    private ClaFilePropertiesDto getFileWithMediaItemId_ListMethod(String subSite, String mediaItemId) throws ServiceException {
        logger.debug("Retrieving files data - as a list item {}", mediaItemId);
        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId);
        if (StringUtils.isBlank(key.getListId()) || StringUtils.isBlank(key.getItemId())) {
            throw new RuntimeException("Invalid media item id (expected 'GUID/Integer'");
        }
        // expand fetch size to include margin from beginning and end (items vs files)
        String apiUrl = "/lists('" + key.getListId() + "')/items(" + key.getItemId() + ")?$expand=file";
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charsToFilter)) {
                ClaFilePropertiesDto file = SharePointParseUtils.convertFileItemListToFileList_ListMethod(streamFilter);
                file.setMediaItemId(SharePointParseUtils.calculateMediaItemId(subSite, mediaItemId));
                return file;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Failed to parse response XML", e);
                if (logger.isDebugEnabled()) {
                    sendRequest(apiUrl, (respInStream) -> {
                        try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(respInStream, charsToFilter)) {
                            logger.warn("Failed call response: {}", IOUtils.toString(streamFilter, "UTF-8"));
                        } catch (IOException e1) {
                            logger.error("Failed to dump ms-response", e1);
                        }

                        return null;
                    });
                }
                throw new ServiceException("Failed to parse response XML", e, apiUrl);
            }
        });
    }
}
