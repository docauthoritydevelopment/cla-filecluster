package com.cla.connector.mediaconnector.labeling;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@SuppressWarnings("unused")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RawLabelingError {
    private SetFileLabelOp fileLabel;
    private String errorMessage;
    private String errorType;

    @JsonProperty("file_label")
    public SetFileLabelOp getFileLabel() {
        return fileLabel;
    }

    public void setFileLabel(SetFileLabelOp fileLabel) {
        this.fileLabel = fileLabel;
    }

    @JsonProperty("error_message")
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @JsonProperty("error_type")
    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }
}
