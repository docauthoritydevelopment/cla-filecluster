package com.cla.connector.mediaconnector.microsoft;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.TimeUnit;

@Builder
@Getter
@ToString
@EqualsAndHashCode
public class MSConnectionConfig {
    @Builder.Default private double backoffMultiplier = 2.0;
    @Builder.Default private int serviceClientTimeout = 20000;
    @Builder.Default private int connectionTimeout = 10000;
    @Builder.Default private int socketTimeout = 10000;
    @Builder.Default private int connectionRequestTimeout = 10000;
    @Builder.Default private int apiCallBarrier = -1;
    private int apiCallBarrierTTS;
    @Builder.Default private int serviceClientTimeoutRetryInterval = 1000;
    @Builder.Default private int connectionPoolCloseStaleConnectionTTL = (int) TimeUnit.MINUTES.toMinutes(10);
    @Builder.Default private int maxConnectionsPerHost = 100;
}
