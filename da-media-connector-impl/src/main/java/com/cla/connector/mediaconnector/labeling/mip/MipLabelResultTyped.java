package com.cla.connector.mediaconnector.labeling.mip;

import com.cla.connector.mediaconnector.labeling.LabelResult;

/**
 * Created by: yael
 * Created on: 7/14/2019
 */
public class MipLabelResultTyped extends MipLabelResult implements LabelResult {

    public MipLabelResultTyped() {
    }

    public MipLabelResultTyped(MipLabelResult other) {
        setCreatedOn(other.getCreatedOn());
        setId(other.getId());
        setName(other.getName());
        setSensitivity(other.getSensitivity());
        setTooltip(other.getTooltip());
        setDescription(other.getDescription());
        setActive(other.isActive());
    }
}
