package com.cla.connector.mediaconnector.common.packed;

import org.apache.commons.compress.archivers.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ZipPackageResourceCache extends PackageResourceCache<ZipFile> {
	private final static Logger logger = LoggerFactory.getLogger(ZipPackageResourceCache.class);

	public ZipPackageResourceCache(int expireAfterIdle, int cleanUpPeriod) {
		super(expireAfterIdle, cleanUpPeriod,
			fileName-> {
				try {
					logger.debug("opening and caching ZIP package file {}", fileName);
					return new ZipFile(fileName);
				} catch (Throwable e) {
					throw new RuntimeException(e);
				}
		}, (ZipFile z)-> {
					try {
						z.close();
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				});
	}
}
