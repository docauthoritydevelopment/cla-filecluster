package com.cla.connector.mediaconnector.google;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Media connector for box servers
 * Created by uri on 08/08/2016.
 */
public class GoogleMediaConnector extends AbstractMediaConnector {

    @Override
    public ClaFilePropertiesDto getFileAttributes(String fileId){
        //TODO
        return null;
    }

    @Override
    public InputStream getInputStream(ClaFilePropertiesDto props){
        //TODO
        return null;
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.GOOGLE;
    }

    @Override
    public void streamMediaItems(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                 Consumer<DirListingPayload> directoryListingConsumer,
                                 Consumer<String> createScanTaskConsumer,
                                 Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {

    }

    @Override
    public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
                                           Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                           Consumer<DirListingPayload> directoryListingConsumer,
                                           Consumer<String> createScanTaskConsumer,
                                           Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {

    }

    @Override
    public ClaFilePropertiesDto getMediaItemAttributes(@NotNull String mediaItemId){
        return null;
    }

    @Override
    public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
        return true; //TODO
    }

    @Override
    public void fetchAcls(ClaFilePropertiesDto claFileProp) {

    }

    @Override
    public List<ServerResourceDto> testConnection() {
		return null;
    }

    @Override
    public String getIdentifier() {
        return null;
    }

    @Override
    public ClaFilePropertiesDto getMediaItemAttributes(String mediaItemId, boolean fetchAcls){
        return null;
    }

    @Override
    public FileContentDto getFileContent(String filename, boolean forUserDownload){
        return null;
    }

	@Override
	public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
		return null;
	}
}
