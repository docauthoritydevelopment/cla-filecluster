package com.cla.connector.mediaconnector.exchange;

import com.cla.connector.domain.dto.media.Exchange365ConnectionParametersDto;
import com.cla.connector.mediaconnector.exchange.auth.MSGraphAuthConfig;
import com.cla.connector.mediaconnector.exchange.throttling.MsGraphThrottlingParams;

/**
 * Created By Itai Marko
 */
public final class Exchange365Params {

    private final Exchange365ConnectionParametersDto connectionParamsDto;
    private final MSGraphAuthConfig authConfig;
    private final MsGraphThrottlingParams throttlingParams;


    public Exchange365Params(Exchange365ConnectionParametersDto connectionParamsDto,
                             MSGraphAuthConfig authConfig,
                             MsGraphThrottlingParams throttlingParams) {

        this.connectionParamsDto = connectionParamsDto;
        this.authConfig = authConfig;
        this.throttlingParams = throttlingParams;
    }

    Exchange365ConnectionParametersDto getConnectionParamsDto() {
        return connectionParamsDto;
    }

    MSGraphAuthConfig getAuthConfig() {
        return authConfig;
    }

    MsGraphThrottlingParams getThrottlingParams() {
        return throttlingParams;
    }
}
