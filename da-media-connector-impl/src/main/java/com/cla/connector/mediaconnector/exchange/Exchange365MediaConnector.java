package com.cla.connector.mediaconnector.exchange;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.mail.MsGraphItemLocator;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by: yael
 * Created on: 2/4/2019
 */
public class Exchange365MediaConnector extends AbstractMediaConnector {

    private static final Logger logger = LoggerFactory.getLogger(Exchange365MediaConnector.class);

    private MsGraphFacade graphFacade;

    private Exchange365Params exchange365Params;

    public Exchange365MediaConnector(Exchange365Params exchange365Params) {
        this.exchange365Params = exchange365Params;
        this.graphFacade = new MsGraphFacade(exchange365Params);
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.EXCHANGE_365;
    }

    @Override
    public Pair<FileContentDto, ClaFilePropertiesDto> getItemDtoAndContent(String mediaItemId, boolean forUserDownload) {
        if (forUserDownload) {
            return graphFacade.getContentForDownload(mediaItemId);
        } else {
            return graphFacade.getItemDtoAndContent(mediaItemId);
        }
    }

    @Override
    public Map<String, Pair<FileContentDto, ClaFilePropertiesDto>> getContainerFileDataList(
            String fileIdentifier, Set<String> expectedContainedFilesIdentifiers) {

        return graphFacade.getEmailAndAttachmentsFileData(fileIdentifier, expectedContainedFilesIdentifiers);
    }

    @Override
    public ClaFilePropertiesDto getFileAttributes(String mediaItemId) {
        logger.warn("Exchange365MediaConnector.getFileAttributes() called instead of getItemDtoAndContent(). mediaItemId: {}", mediaItemId);
        return graphFacade.getItemDtoAndContent(mediaItemId).getValue();
    }

    @Override
    public FileContentDto getFileContent(String mediaItemId, boolean forUserDownload) {
        logger.warn("Exchange365MediaConnector.getFileContent() called instead of getItemDtoAndContent(). mediaItemId: {}", mediaItemId);
        if (forUserDownload) {
            return graphFacade.getContentForDownload(mediaItemId).getKey();
        } else {
            return graphFacade.getItemDtoAndContent(mediaItemId).getKey();
        }
    }

    @Override
    public ClaFilePropertiesDto getMediaItemAttributes(String mediaItemId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public InputStream getInputStream(ClaFilePropertiesDto props) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void streamMediaChangeLog(
            ScanTaskParameters scanParams, String realPath, Long runId, long startChangeLogPosition,
            Consumer<MediaChangeLogDto> changeConsumer, Consumer<String> createScanTaskConsumer,
            Predicate<? super String> fileTypesPredicate) {

        String mailboxUPN = scanParams.getMailboxUPN();
        if (Strings.isNullOrEmpty(mailboxUPN)) {
            throw new IllegalArgumentException("mailboxUPN must be specified. Got [" + mailboxUPN + "]");
        }

        String syncState = scanParams.getSyncState();   // should be null for first scan/force rescan
        Long fromDateLong = scanParams.getFromDateFilter();
        Instant fromDate = fromDateLong == null ? null: Instant.ofEpochMilli(fromDateLong);
        String folderId = scanParams.getPath() == null ?
                null : // should be null for folder-structure, and folderId for folder's-contents
                MsGraphItemLocator.fromString(scanParams.getPath()).getItemId().getUid();
        String nextSyncState;
        if (folderId == null) { // Indicates a folder-structure sync
            nextSyncState = graphFacade.streamFolderStructureChanges(
                    mailboxUPN, syncState, runId, changeConsumer, createScanTaskConsumer);
        } else { // Indicates a folder's-contents sync
            nextSyncState = graphFacade.streamFolderContentsChanges(
                    mailboxUPN, folderId, syncState, fromDate, runId, changeConsumer, fileTypesPredicate);
        }
        // Passing the next syncState to ScanConsumer.finalizeJob() on the scanParams.
        // This is done to avoid refactoring all media connectors to return a value here.
        // Apologies for the quick & dirty solution..
        scanParams.setSyncState(nextSyncState);
    }

    @Override
    public void concurrentStreamMediaChangeLog(
            ForkJoinPool forkJoinPool, ScanTaskParameters scanParams, String realPath, Long runId,
            long startChangeLogPosition, Consumer<MediaChangeLogDto> changeConsumer,
            Consumer<String> createScanTaskConsumer, Predicate<? super String> fileTypesPredicate) {

        logger.info("Delegating concurrentStreamMediaChangeLog() call for Exchange 365 to streamMediaChangeLog()");
        streamMediaChangeLog(
                scanParams, realPath, runId, startChangeLogPosition, changeConsumer, createScanTaskConsumer,
                fileTypesPredicate);
    }

    @Override
    public void streamMediaItems(
            ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
            Consumer<DirListingPayload> directoryListingConsumer, Consumer<String> createScanTaskConsumer,
            Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {

        throw new UnsupportedOperationException(
                "streamMediaItems() isn't supported for Exchange 365. only streamMediaChangeLog() is supported");
    }

    @Override
    public void concurrentStreamMediaItems(
            ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
            Consumer<ClaFilePropertiesDto> filePropertiesConsumer, Consumer<DirListingPayload> directoryListingConsumer,
            Consumer<String> createScanTaskConsumer, Predicate<Pair<Long, Long>> scanActivePredicate,
            ProgressTracker filePropsProgressTracker) {

        throw new UnsupportedOperationException(
                "concurrentStreamMediaItems() isn't supported for Exchange 365. only streamMediaChangeLog() is supported");
    }

    @Override
    public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
        return true; //TODO Itai: implement this (DAMAIN-6774)
    }

    @Override
    public void fetchAcls(ClaFilePropertiesDto claFileProp) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ServerResourceDto> testConnection() {
        if (graphFacade.testConnection()) {
            return ImmutableList.of(new ServerResourceDto(exchange365Params.getConnectionParamsDto().getApplicationId(),
                    exchange365Params.getConnectionParamsDto().getTenantId()));
        }
        return null;
    }

    @Override
    public String getIdentifier() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
        throw new UnsupportedOperationException();
    }
}
