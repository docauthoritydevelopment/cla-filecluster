package com.cla.connector.mediaconnector.fileshare;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.media.scan.RecursiveActionBase;
import com.cla.connector.media.scan.RecursiveActionBuilderBase;
import com.cla.connector.mediaconnector.common.packed.PackageCrawlException;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils.getMediaProperties;
import static com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils.isDirectory;
import static com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils.scanPackageFileIfExist;

public class StreamMediaRecursiveAction extends RecursiveActionBase<Path> {

    private final static Logger logger = LoggerFactory.getLogger(StreamMediaRecursiveAction.class);

    private boolean preserveAccessedTime;
    private boolean impersonateBeforeAction;

    private StreamMediaRecursiveAction(boolean preserveAccessedTime, boolean impersonateBeforeAction) {
        this.preserveAccessedTime = preserveAccessedTime;
        this.impersonateBeforeAction = impersonateBeforeAction;
    }

    @Override
    protected void compute() {
		try {
			logger.debug("Start computing forked path {}.", path);
			List<Path> pathSkipList = scanParams.getSkipPaths() == null ? Lists.newArrayList() : scanParams.getSkipPaths().stream()
                    .map(Paths::get)
                    .collect(Collectors.toList());

			if (scanActivePredicate != null && !scanActivePredicate.test(Pair.of(scanParams.getJobId(), scanParams.getJobStateIdentifier()))) {
                logger.info("scanActivePredicate returned false for job {} failed . Aborting", scanParams.getJobId());
                return;
            }

            if (!isDirectory(path, preserveAccessedTime, impersonateBeforeAction, scanParams.isIgnoreAccessErrors())) {
                logger.warn("Expected folder, but got file: (). Aborting", path);
                return;
            }

            String baseName = path.getFileName() != null ? path.getFileName().toString() : "";
            if (MediaConnectorUtils.isFolderExcluded(path, baseName, scanParams.getExcludedRules(), pathSkipList, scanParams.getSkipDirnames(), scanParams.getRunId())) {
                logger.debug("Skipping path {}", path.toString());
            } else {
                ClaFilePropertiesDto mediaProperties = getMediaProperties(path, preserveAccessedTime, impersonateBeforeAction); // get the folder properties
				filePropsConsumer.accept(mediaProperties);
				DirListingPayload dirListing = DirListingPayload.create().setFolderPath(path.toString()); // create dir listing payload to be filled with file/sub-folder paths
				List<Path> files = Lists.newArrayList();
				List<Path> folders = Lists.newArrayList();
				long startTime = timeSource.currentTimeMillis();

				try (Stream<Path> pathStream = Files.list(path)) {
					logger.debug("List dir {}. Elapsed time {}ms", path, timeSource.millisSince(startTime));
					pathStream.forEach(subPath -> {
						if (dirListingConsumer != null) { // add path to dir listing, if exists
							dirListing.addPath(subPath.toString());
						}
						if (isDirectory(subPath, preserveAccessedTime, impersonateBeforeAction, scanParams.isIgnoreAccessErrors())) {
							folders.add(subPath);
						} else {
							files.add(subPath);
						}
					});

					for (Path filePath : files) {
						ClaFilePropertiesDto fileProperties = getMediaProperties(filePath, preserveAccessedTime, impersonateBeforeAction);
						filePropsConsumer.accept(fileProperties);
						try {
                            scanPackageFileIfExist(fileProperties, filePropsConsumer, fileTypesPredicate);
                        } catch (PackageCrawlException e) {
                            fileProperties.addError(MediaConnectorUtils.createScanError(
                                    "Failed to scan package file contents.", e,
                                    fileProperties.getFileName(), scanParams.getRunId()));
                        }
					}

					if (folders.size() > 0) {
						if (scanParams.isFstLevelOnly()) {
							logger.trace("fst level scan only, ask for task creation for folders");
							folders.forEach(f -> {
                                createScanTaskConsumer.accept(f.toString());
                                // consume folder properties
                                ClaFilePropertiesDto mediaPropertiesSubFolder =
                                        FileShareMediaUtils.getMediaProperties(f, preserveAccessedTime, impersonateBeforeAction);
                                filePropsConsumer.accept(mediaPropertiesSubFolder);
                            });
						} else {
							List<StreamMediaRecursiveAction> subtasks = createSubtasks(folders);
							invokeAll(subtasks);
						}
					}
                } catch (IOException | UncheckedIOException | InvalidPathException ioe) {
                    // TODO Itai: [DAMAIN-3075] [DAMAIN-3219] demutexify when adding support for fork-join scanning of PST files
                    ClaFilePropertiesDto mediaErrProperties = ClaFilePropertiesDto.create();
                    ClaFilePropertiesDto.copyProperties(mediaProperties, mediaErrProperties);
                    mediaErrProperties.setFolder(true);
                    mediaErrProperties.addError(
                            MediaConnectorUtils.createScanError("Failed to list folder contents.",
                                    ioe, path.toString(), scanParams.getRunId())
                    );
                    if (ioe instanceof AccessDeniedException) {
                        mediaErrProperties.setInaccessible(true);
                        filePropsConsumer.accept(mediaErrProperties);
                    }
                }

                logger.debug("Traversing dir {}. Elapsed time {}ms", path, timeSource.millisSince(startTime));

                if (dirListingConsumer != null) {
                    dirListingConsumer.accept(dirListing);
                }
            }
        } catch (Exception e) {
            logger.warn("Exception while traversing dir {}. Error={}", path, e.getMessage(), e);
            throw e;
        } catch (Throwable t) {
            logger.warn("Throwable while traversing dir {}. Error={}", path, t);
            throw new RuntimeException(t.getMessage());
        }
    }





	private List<StreamMediaRecursiveAction> createSubtasks(List<Path> folders) {
        return folders.stream()
                .map(folderPath -> Builder.create()
                    .setImpersonateBeforeAction(impersonateBeforeAction)
                    .setPreserveAccessedTime(preserveAccessedTime)
                    .withPath(folderPath)
                    .withDirListingConsumer(dirListingConsumer)
                    .withFilePropsConsumer(filePropsConsumer)
                    .withScanParams(scanParams)
                    .withFileTypesPredicate(fileTypesPredicate)
                    .withJobStatePredicate(scanActivePredicate)
                    .withProgressTracker(filePropsProgressTracker)
                    .build())
                .map(action -> (StreamMediaRecursiveAction) action)
                .collect(Collectors.toList());
    }

    public static class Builder extends RecursiveActionBuilderBase<StreamMediaRecursiveAction, Path> {

        private boolean preserveAccessedTime = false;
        private boolean impersonateBeforeAction = true;

        public static Builder create() {
            return new Builder();
        }

        Builder setPreserveAccessedTime(boolean preserveAccessedTime) {
            this.preserveAccessedTime = preserveAccessedTime;
            return this;
        }

        Builder setImpersonateBeforeAction(boolean impersonateBeforeAction) {
            this.impersonateBeforeAction = impersonateBeforeAction;
            return this;
        }

        public StreamMediaRecursiveAction build() {
            return super.build(new StreamMediaRecursiveAction(preserveAccessedTime, impersonateBeforeAction));
        }
    }
}
