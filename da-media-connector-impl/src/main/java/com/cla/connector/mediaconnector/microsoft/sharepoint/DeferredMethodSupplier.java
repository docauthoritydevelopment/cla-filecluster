package com.cla.connector.mediaconnector.microsoft.sharepoint;

public interface DeferredMethodSupplier<T, E extends Exception> {

    T invoke() throws E;
}