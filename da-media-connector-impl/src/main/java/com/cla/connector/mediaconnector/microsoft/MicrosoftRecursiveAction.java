package com.cla.connector.mediaconnector.microsoft;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.media.scan.RecursiveActionBase;
import com.cla.connector.media.scan.RecursiveActionBuilderBase;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by oren on 2/4/2018.
 */
public class MicrosoftRecursiveAction extends RecursiveActionBase<String> {

    private static final Logger logger = LoggerFactory.getLogger(RecursiveActionBase.class.getName());

    private MicrosoftConnectorBase microsoftConnectorBase;

    private String listId;

    private String subSite;

    @Override
    protected void compute() {
        List<String> folders;
        try {
            folders = microsoftConnectorBase.concurrentStreamFilesAndSubFolders(path, scanParams, fileTypesPredicate,
                    filePropsConsumer, scanActivePredicate, filePropsProgressTracker, listId, subSite);
        } catch (Exception e) {
            logger.error("Failed to stream files and folders under {}", path, e);
            if (!subFolderTask) { // This is our initial task then fail the scan
                throw e;
            }
            // Otherwise set as error folder scan
            ClaFilePropertiesDto errDto = ClaFilePropertiesDto.create()
                    .addError(microsoftConnectorBase.createScanError("Error while processing a top folder " + path, e,
                            SharePointParseUtils.applySiteMark(path, subSite), scanParams.getRunId()));
            filePropsConsumer.accept(errDto);
            return;
        }

        List<MicrosoftRecursiveAction> tasks = folders.stream()
                .map(this::assemblePath)
                .map(folder -> {
                    if (scanParams.isFstLevelOnly()) {
                        createScanTaskConsumer.accept(folder);
                        logger.trace("fst level scan only, ask for task creation for folder {}", folder);
                        return null;
                    } else {
                        return (MicrosoftRecursiveAction) MicrosoftRecursiveAction.Builder.create()
                                .withMicrosoftConnectorBase(microsoftConnectorBase)
                                .withListId(listId)
                                .withSubSite(subSite)
                                .withPath(folder)
                                .withCreateScanTaskConsumer(createScanTaskConsumer)
                                .withDirListingConsumer(dirListingConsumer)
                                .withFilePropsConsumer(filePropsConsumer)
                                .withFileTypesPredicate(fileTypesPredicate)
                                .withJobStatePredicate(scanActivePredicate)
                                .withProgressTracker(filePropsProgressTracker)
                                .withScanParams(scanParams)
                                .asSubFolderTask()
                                .build();
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (tasks != null && tasks.size() > 0) {
            invokeAll(tasks);
        }
    }

    private String assemblePath(String folderName) {
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(path);
        if (key.getSite() == null) { // no sub site
            return SharePointParseUtils.normalizePath(path + "/" + folderName);
        }

        String temp = key.getPath().replaceAll("[/]+$", "") + SharePointParseUtils.normalizePath(folderName);
        return Optional.ofNullable(subSite)
                .map(site -> SharePointParseUtils.applySiteMark(temp, site))
                .orElse(StringUtils.EMPTY);
    }

    public static class Builder extends RecursiveActionBuilderBase<MicrosoftRecursiveAction, String> {

        private MicrosoftConnectorBase microsoftConnectorBase;

        private String listId;

        private String subSite;

        public static Builder create() {
            return new Builder();
        }

        public Builder withMicrosoftConnectorBase(MicrosoftConnectorBase microsoftConnectorBase) {
            this.microsoftConnectorBase = microsoftConnectorBase;
            return this;
        }

        public Builder withListId(String listId) {
            this.listId = listId;
            return this;
        }

        public Builder withSubSite(String subSite) {
            this.subSite = subSite;
            return this;
        }

        @Override
        public MicrosoftRecursiveAction build() {
            MicrosoftRecursiveAction action = new MicrosoftRecursiveAction();
            action.microsoftConnectorBase = microsoftConnectorBase;
            action.listId = listId;
            action.subSite = subSite;
            return super.build(action);
        }
    }
}
