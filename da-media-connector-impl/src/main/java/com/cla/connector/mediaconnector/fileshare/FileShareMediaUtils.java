package com.cla.connector.mediaconnector.fileshare;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.common.packed.PackageCrawlException;
import com.cla.connector.mediaconnector.common.packed.PackageCrawler;
import com.cla.connector.mediaconnector.common.packed.PackageCrawlerFactory;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.mediaconnector.fileshare.preserveaccesstime.WindowsPreserveAccessTime;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.google.common.collect.Lists;
import com.sun.jna.platform.win32.WinNT;
import org.apache.commons.exec.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileShareMediaUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileShareMediaUtils.class);
    private static final String ATTRIBUTES_KEY = "attributes";
    private static final String DOS_ATTRIBUTES = "dos:*";

    public static void concurrentStreamMediaItems(ForkJoinPool forkJoinPool,
                                                  Predicate<? super String> fileTypesPredicate,
                                                  final Path path,
                                                  final ScanTaskParameters scanParams,
                                                  Consumer<ClaFilePropertiesDto> filePropsConsumer,
                                                  Consumer<DirListingPayload> dirListingConsumer,
                                                  Consumer<String> createScanTaskConsumer,
                                                  Predicate<Pair<Long, Long>> scanActivePredicate,
                                                  ProgressTracker filePropsProgressTracker,
                                                  boolean isPreserveAccessedTime,
                                                  boolean impersonateBeforeAction) {

        logger.trace("start concurrentStreamMediaItems for path {}", path.toString());

        StreamMediaRecursiveAction action = (StreamMediaRecursiveAction) StreamMediaRecursiveAction.Builder.create()
                .setImpersonateBeforeAction(impersonateBeforeAction)
                .setPreserveAccessedTime(isPreserveAccessedTime)
                .withPath(path)
                .withFileTypesPredicate(fileTypesPredicate)
                .withJobStatePredicate(scanActivePredicate)
                .withScanParams(scanParams)
                .withFilePropsConsumer(filePropsConsumer)
                .withDirListingConsumer(dirListingConsumer)
                .withCreateScanTaskConsumer(createScanTaskConsumer)
                .withProgressTracker(filePropsProgressTracker)
                .build();

        forkJoinPool.invoke(action);
    }

    public static void streamMediaItems(final Path path,
                                        final ScanTaskParameters scanParams,
                                        Consumer<ClaFilePropertiesDto> filePropsConsumer,
                                        Consumer<DirListingPayload> dirListingConsumer,
                                        Consumer<String> createScanTaskConsumer,
                                        final Predicate<Pair<Long, Long>> scanActivePredicate,
                                        boolean isPreserveAccessedTime,
                                        boolean impersonateBeforeAction) {
        String pathName = path.toString();
        logger.trace("start streamMediaItems for path {}", pathName);

        List<FolderExcludeRuleDto> excludedRules = scanParams.getExcludedRules();
        List<String> skipPaths = scanParams.getSkipPaths();
        List<Path> pathSkipList = skipPaths == null ? Lists.newArrayList() : skipPaths.stream()
                .map(Paths::get)
                .collect(Collectors.toList());
        List<String> skipDirnames = scanParams.getSkipDirnames();
        Long runId = scanParams.getRunId();

        if (scanActivePredicate != null && !scanActivePredicate.test(Pair.of(scanParams.getJobId(), scanParams.getJobStateIdentifier()))) {
            logger.info("scanActivePredicate returned false for job {} failed . Aborting", scanParams.getJobId());
            return;
        }

        if (isServerOnlyPath(path, scanParams)) {
            List<String> topLevelShares = getServerShares(pathName);
            // get the folder properties
            ClaFilePropertiesDto mediaProperties = ClaFilePropertiesDto.create().setPath(path);
            // create dir listing payload to be filled with file/sub-folder paths
            final DirListingPayload dirListing = DirListingPayload.create().setFolderPath(pathName);
            // consume folder properties
            filePropsConsumer.accept(mediaProperties);

            // if there are shares inside
            if (topLevelShares != null) {
                topLevelShares.stream().map(share -> Paths.get(share)).forEach(subPath -> {
                    // add path to dir listing, if exists
                    if (dirListingConsumer != null) {
                        dirListing.addPath(subPath.toString());
                    }
                    // go down the tree recursively
                    logger.debug("Streaming media items for identified top-level-share {} ...", subPath.toString());
                    streamMediaItems(subPath, scanParams, filePropsConsumer, dirListingConsumer,
                            createScanTaskConsumer, scanActivePredicate, isPreserveAccessedTime, impersonateBeforeAction);
                });
            }

            if (dirListingConsumer != null) {
                dirListingConsumer.accept(dirListing);
            }

        } else if (isDirectory(path, isPreserveAccessedTime, impersonateBeforeAction, scanParams.isIgnoreAccessErrors())) {
            String baseName = path.getFileName() != null ? path.getFileName().toString() : "";
            if (MediaConnectorUtils.isFolderExcluded(path, baseName, excludedRules, pathSkipList, skipDirnames, runId)) {
                logger.debug("Skipping path {}", pathName);
            } else {
                // get the folder properties
                ClaFilePropertiesDto mediaProperties = FileShareMediaUtils.getMediaProperties(path, isPreserveAccessedTime, impersonateBeforeAction);
                // create dir listing payload to be filled with file/sub-folder paths
                final DirListingPayload dirListing = DirListingPayload.create().setFolderPath(pathName);

                try (Stream<Path> pathStream = Files.list(path)) {
                    // consume folder properties
                    filePropsConsumer.accept(mediaProperties);
                    // if there are files/folders inside
                    if (pathStream != null) {
                        pathStream.forEach(subPath -> {
                            // add path to dir listing, if exists
                            if (dirListingConsumer != null) {
                                dirListing.addPath(subPath.toString());
                            }
                            if (scanParams.isFstLevelOnly() && subPath.toFile().isDirectory()) {
                                createScanTaskConsumer.accept(subPath.toString());
                                logger.trace("fst level scan only, ask for task creation for folder {}", subPath.toString());
                                // consume folder properties
                                ClaFilePropertiesDto mediaPropertiesSubFolder =
                                        FileShareMediaUtils.getMediaProperties(subPath, isPreserveAccessedTime, impersonateBeforeAction);
                                filePropsConsumer.accept(mediaPropertiesSubFolder);
                            } else {
                                // go down the tree recursively
                                streamMediaItems(subPath, scanParams, filePropsConsumer, dirListingConsumer,
                                        createScanTaskConsumer, scanActivePredicate,
                                        isPreserveAccessedTime, impersonateBeforeAction);
                            }
                        });
                    }
                } catch (IOException | UncheckedIOException | InvalidPathException ioe) {
                    ClaFilePropertiesDto mediaErrProperties = ClaFilePropertiesDto.create();
                    ClaFilePropertiesDto.copyProperties(mediaProperties, mediaErrProperties);
                    mediaErrProperties.setFolder(true);
                    mediaErrProperties.addError(
                            MediaConnectorUtils.createScanError("Failed to list folder contents.", ioe, pathName, runId)
                    );
                    if (ioe instanceof AccessDeniedException) {
                        mediaErrProperties.setInaccessible(true);
                        filePropsConsumer.accept(mediaErrProperties);
                    }
                }

                if (dirListingConsumer != null) {
                    dirListingConsumer.accept(dirListing);
                }
            }
        } else {
            ClaFilePropertiesDto mediaProperties = FileShareMediaUtils.getMediaProperties(path, isPreserveAccessedTime, impersonateBeforeAction);
            filePropsConsumer.accept(mediaProperties);
			try {//sorry for this ugly block but as an ancient proverb says- "when in Rome...."
				scanPackageFileIfExist(mediaProperties, filePropsConsumer, FileTypeUtils.createFileTypesPredicate(scanParams.getScanTypeSpecification()));
			} catch (PackageCrawlException e) {
				mediaProperties.addError(MediaConnectorUtils.createScanError("Failed to scan package file contents.", e, mediaProperties.getFileName(), runId));
			}
		}
    }

    public static boolean isDirectory(Path path, boolean isPreserveAccessedTime, boolean impersonateBeforeAction, boolean ignoreAccessErrors) {
        return isPreserveAccessedTime ? WindowsPreserveAccessTime.isDirectory(path, impersonateBeforeAction, ignoreAccessErrors) : Files.isDirectory(path);
    }

    @SuppressWarnings("unused")
    private static boolean isServerOnlyPath(Path path, ScanTaskParameters scanParams) {
//        String name = FilenameUtils.normalizeNoEndSeparator(path.toString());
//        String basename = FilenameUtils.getBaseName(name);
//        if (expandServerOnlyUrl &&
//                name != null &&
//                expandServerDummyBasename.equals(basename)) {
//            String normPath = FilenameUtils.getFullPathNoEndSeparator(name);
//            // \\servername\_
//            boolean res = (normPath != null &&
//                    normPath.startsWith("\\\\") &&
//                    FilenameUtils.getFullPathNoEndSeparator(normPath) == null);
//            if (res && logger.isDebugEnabled()) {
//                logger.debug("Suspect path {} to be a server-only URL", normPath);
//            }
//            return res;
//        }
        return false;
    }


    /**
     * Get file/folder properties
     *
     * @param pathStr path string
     * @return ClaFilePropertiesDto
     */
    public static Optional<ClaFilePropertiesDto> getMediaProperties(final String pathStr, boolean isPreserveAccessedTime, boolean impersonateBeforeAction) {
        Path path = FileSystems.getDefault().getPath(pathStr);
        return Optional.of(getMediaProperties(path, isPreserveAccessedTime, impersonateBeforeAction));
    }

    /**
     * Get file/folder properties
     *
     * @param path path object
     * @return ClaFilePropertiesDto
     */
    public static ClaFilePropertiesDto getMediaProperties(final Path path, boolean isPreserveAccessedTime, boolean impersonateBeforeAction) {
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create().setPath(path);
        if (logger.isTraceEnabled()) {
            logger.trace("**** Fetching properties & ACLs for ({})", props.getFileName());
        }
        try {
            if (isPreserveAccessedTime) {
                WindowsPreserveAccessTime.fetchAttributes(props, impersonateBeforeAction);
                WindowsPreserveAccessTime.fetchAcls(props, impersonateBeforeAction);
            } else {
                fetchAcls(props);
                fetchAttributes(props);
            }
            if (logger.isTraceEnabled()) {
                logger.trace("**** properties for {}:\n{}", path, props);
                logger.trace("**** ACLs for {}:\n{}", path, toAclStr(props));
            }
        } catch (final NoSuchFileException e) {
            logger.warn("Cannot get ACLS or BasicFileAttributes for path= {}. No Such File (or no permissions to see file). {}", path.toString(), e.getReason());
            ScanErrorDto scanErrorDto = MediaConnectorUtils.createScanError(e, path.toString(), null);
            props.addError(scanErrorDto);
            return props;
        } catch (final IOException e) {
            logger.error("Cannot get ACLS or BasicFileAttributes for path= {}. Error: {} (class: {})", path.toString(), e.getMessage(), e.getClass());
            ScanErrorDto scanErrorDto = MediaConnectorUtils.createScanError(e, path.toString(), null);
            props.addError(scanErrorDto);
            return props;
        } catch (Exception e) {
            logger.warn("Cannot get ACLS or BasicFileAttributes for path= {}, reason: {}", path.toString(), e.getMessage());
            ScanErrorDto scanErrorDto = MediaConnectorUtils.createScanError(e, path.toString(), null);
            props.addError(scanErrorDto);
            return props;
        }
        return props;
    }

    @SuppressWarnings("StringBufferReplaceableByString")
    private static String toAclStr(ClaFilePropertiesDto props) {
        return new StringBuilder()
                .append('\n')
                .append("-------------- Allowed Read Principals --------------")
                .append('\n')
                .append(props.getAclReadAllowed().stream().collect(Collectors.joining("\n")))
                .append('\n')
                .append("-------------- Denied Read Principals --------------")
                .append('\n')
                .append(props.getAclReadDenied().stream().collect(Collectors.joining("\n")))
                .append('\n')
                .append("-------------- Allowed Write Principals --------------")
                .append('\n')
                .append(props.getAclWriteAllowed().stream().collect(Collectors.joining("\n")))
                .append('\n')
                .append("-------------- Denied Write Principals --------------")
                .append('\n')
                .append(props.getAclWriteDenied().stream().collect(Collectors.joining("\n")))
                .append('\n')
                .toString();
    }

    public static void fetchAttributes(ClaFilePropertiesDto props) throws IOException {
        Map<String, Object> attributesMap = Files.readAttributes(props.getFilePath(), DOS_ATTRIBUTES);
        if (attributesMap.containsKey(ATTRIBUTES_KEY)) {
            int attributesMask = (int) attributesMap.get(ATTRIBUTES_KEY);
            props.setOffline((attributesMask & WinNT.FILE_ATTRIBUTE_OFFLINE) == WinNT.FILE_ATTRIBUTE_OFFLINE);
        }
        props.setFileSize((Long) attributesMap.get("size"));
        props.setModTimeMilli(((FileTime) attributesMap.get("lastModifiedTime")).toMillis());
        props.setAccessTimeMilli(((FileTime) attributesMap.get("lastAccessTime")).toMillis());
        props.setCreationTimeMilli(((FileTime) attributesMap.get("creationTime")).toMillis());
        props.setFolder((boolean) attributesMap.get("isDirectory"));
    }

    @SuppressWarnings("SpellCheckingInspection")
    public static void fetchAcls(ClaFilePropertiesDto props) {

        Path path = props.getFilePath();

        final AclFileAttributeView afav = Files.getFileAttributeView(path, AclFileAttributeView.class);
        if (afav != null) {
            List<AclEntry> acl = null;
            try {
                props.setOwnerName(afav.getOwner().getName());
                acl = afav.getAcl();
            } catch (final NoSuchFileException e) {
                logger.warn("Cannot get owner for path={}. Error({}): {}",
                        path.toString(), e.getClass().getName(), e.getMessage());
            } catch (final IOException e) {
                logger.error("Cannot get owner for path={}. Error({}): {}",
                        path.toString(), e.getClass().getName(), e.getMessage());
                ScanErrorDto scanErrorDto = MediaConnectorUtils.createScanError("Failed parse owner/ACL for path: " + path.toString(),
                        e, path.toString(), null);
                scanErrorDto.setSeverity(ScanErrorDto.ErrorSeverity.WARNING);
                props.addError(scanErrorDto);
            }
            if (acl != null) {
                try {
                    for (final AclEntry ae : acl) {
                        final Set<AclEntryFlag> aefs = ae.flags();
                        final Set<AclEntryPermission> aeps = ae.permissions();

                        if (logger.isTraceEnabled()) {
                            for (final AclEntryFlag aef : aefs) {
                                logger.trace("\t\taef: " + aef.name());
                            }
                        }

                        for (final AclEntryPermission aep : aeps) {
                            String principalName = ae.principal().getName();
                            if (aep == AclEntryPermission.READ_DATA) {
                                if (ae.type() == AclEntryType.ALLOW) {
                                    props.addAllowedReadPrincipalName(principalName);
                                } else if (ae.type() == AclEntryType.DENY) {
                                    props.addDeniedReadPrincipalName(principalName);
                                }
                            } else if (aep == AclEntryPermission.WRITE_DATA) {
                                if (ae.type() == AclEntryType.ALLOW) {
                                    props.addAllowedWritePrincipalName(principalName);
                                } else if (ae.type() == AclEntryType.DENY) {
                                    props.addDeniedWritePrincipalName(principalName);
                                }
                            }
                        }
                    }

                    props.calculateAclSignature();
                } catch (Exception e) {
                    logger.error("Failed parse ACL for path: " + path.toString(), e);
                    ScanErrorDto scanErrorDto = MediaConnectorUtils.createScanError("Failed parse ACL for path: " + path.toString(),
                            e, path.toString(), null);
                    scanErrorDto.setSeverity(ScanErrorDto.ErrorSeverity.WARNING);
                    props.addError(scanErrorDto);
                }
            }
        }
    }

    private static List<String> getServerShares(String serverPath) {
        try {
            return executeNetView(FilenameUtils.getPath(serverPath));
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    private static List<String> executeNetView(String serverPath) throws IOException, InterruptedException {
        List<String> result = new ArrayList<>();
        String commandLine = "net view";
        if (serverPath != null) {
            commandLine = "net view " + serverPath.replaceAll("/", "\\");
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        long time = System.currentTimeMillis();
        executeCommandLine(commandLine, outputStream);
        time = System.currentTimeMillis() - time;
        String output = new String(outputStream.toByteArray());
        boolean resultStart = false;
        for (String line : StringUtils.split(output, '\n')) {
            if (line.startsWith("The command completed successfully")) {
                break;
            }
            if (resultStart) {
                String folder = line.replaceFirst("\\s.+", "").replace("\r", "");
                result.add(serverPath + "\\" + folder);
            }
            if (line.startsWith("------------------")) {
                resultStart = true;
            }
        }
        if (logger.isDebugEnabled()) {
            if (result.isEmpty()) {
                logger.debug("Found no Net view shares (run for {}ms)", time);
            } else {
                logger.debug("Net view on {} results in: {} (run for {}ms)", serverPath, result.stream().collect(Collectors.joining(",")), time);
            }
        }
        return result;
    }

    private static void executeCommandLine(String line, ByteArrayOutputStream outputStream) throws IOException, InterruptedException {
        DefaultExecutor executor = new DefaultExecutor();
        CommandLine cmdLine = CommandLine.parse(line);
        ExecuteWatchdog watchdog = new ExecuteWatchdog(30000);
        executor.setWatchdog(watchdog);
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        executor.setStreamHandler(streamHandler);
        executor.execute(cmdLine, resultHandler);
        resultHandler.waitFor();
    }

    public static boolean isDirectoryExists(String pathStr, boolean isPreserveAccessedTime, boolean impersonateBeforeAction, boolean ignoreAccessErrors) {
        final Path path = Paths.get(pathStr);
        boolean result = isPreserveAccessedTime ?
                (Files.exists(path) && WindowsPreserveAccessTime.isDirectory(path, impersonateBeforeAction, ignoreAccessErrors)) :
                Files.exists(path);

        if (logger.isTraceEnabled()) {
            Boolean otherRes = null;
            if (isPreserveAccessedTime) {
                otherRes = Files.exists(path);
            }
            Boolean resourceUtilsRes = null;
            Boolean fileAbsolute = null;
            if (!result) {
                try {
                    File f = ResourceUtils.getFile(pathStr);
                    resourceUtilsRes = f.exists();
                } catch (Exception e) {
                    resourceUtilsRes = false;
                }
                try {
                    File file = new File(pathStr);
                    fileAbsolute = file.getAbsoluteFile().exists();
                } catch (Exception e) {
                    fileAbsolute = false;
                }
            }
            logger.trace("directory for path {} isPreserveAccessedTime {} " +
                            "impersonateBeforeAction {} result {} other {} resourceUtilsRes {} fileAbsolute {}",
                    pathStr, isPreserveAccessedTime, impersonateBeforeAction, result, otherRes, resourceUtilsRes, fileAbsolute);
        }
        return result;
    }

    public static void createTempFolderIfNeeded(String tempFolder) {
        if (!Files.isDirectory(Paths.get(tempFolder))) {
            try {
                logger.info("Creating missing tempFolder: {} ...", tempFolder);
                Files.createDirectories(Paths.get(tempFolder));
            } catch (IOException e) {
                logger.error("Could not create tempFolder: {}. Error: {}", tempFolder, e);
                throw new RuntimeException(e);
            }
        }
    }

    public static void cleanFolder(String folderName) {
        if (Files.isDirectory(Paths.get(folderName))) {
            logger.info("Delete content of folder {}", folderName);
            try {
                FileUtils.cleanDirectory(new File(folderName));
            } catch (IOException e) {
                logger.error("Could not clean content of folder: {}", folderName, e);
            }
        }
    }

	public static void scanPackageFileIfExist(ClaFilePropertiesDto fileProperties, Consumer<ClaFilePropertiesDto> filePropsConsumer, Predicate<? super String> fileTypesPredicate) throws PackageCrawlException {
		if (	fileProperties == null ||
				!fileProperties.isFile() ||
				!fileTypesPredicate.test(fileProperties.getFileName())	||
				!FileNamingUtils.isPackage(fileProperties.getFileName())) {
			return;
		}
		Optional<PackageCrawler> packageCrawler = PackageCrawlerFactory.getPackageCrawler(fileProperties);
		if (packageCrawler.isPresent()) {
            packageCrawler.get().crawl(fileProperties, fileTypesPredicate, filePropsConsumer);
        }
	}

	public static List<String> getSharePermissions(String scriptPath, String computer, String share) throws Exception {
        List<String> result = new ArrayList<>();
        StringBuilder error = new StringBuilder();
        String command = "powershell.exe -ExecutionPolicy Unrestricted \"" + scriptPath + "\" " + computer + " \"'" + share + "'\"";

        logger.trace(command);

        Process powerShellProcess = null;
        try {
            try {
                powerShellProcess = Runtime.getRuntime().exec(command);
                powerShellProcess.waitFor();
            } finally {
                if (powerShellProcess != null) {
                    powerShellProcess.getOutputStream().close();
                }
            }

            String line;
            try (BufferedReader stdout = new BufferedReader(new InputStreamReader(
                    powerShellProcess.getInputStream()))) {
                while ((line = stdout.readLine()) != null) {
                    result.add(line);
                }
            }

            try (BufferedReader stderr = new BufferedReader(new InputStreamReader(
                    powerShellProcess.getErrorStream()))) {
                while ((line = stderr.readLine()) != null) {
                    error.append(line);
                }
            }
        } finally {
            if (powerShellProcess != null) {
                powerShellProcess.destroy();
            }
        }

        if (!error.toString().isEmpty()) {
            logger.error("error calling script to get share permissions for machine {} and share {} error {}"
                    , computer, share, error.toString());
            throw new RuntimeException(error.toString());
        }

        return result;
    }
}
