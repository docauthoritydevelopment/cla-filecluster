package com.cla.connector.mediaconnector.microsoft.sharepoint;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

@SuppressWarnings("WeakerAccess")
public class StreamListData {
    private String path;
    private final ScanTaskParameters scanParams;
    private final String listId;
    private String libName;
    private final String subSite;
    private final Consumer<ClaFilePropertiesDto> filePropertiesDtoConsumer;
    private final Consumer<String> createScanTaskConsumer;
    private final Predicate<Pair<Long, Long>> scanActivePredicate;
    private final ProgressTracker filePropsProgressTracker;

    public StreamListData(String path, ScanTaskParameters scanParams, String listId, String libName, String subSite,
                          Consumer<ClaFilePropertiesDto> filePropertiesDtoConsumer, Consumer<String> createScanTaskConsumer,
                          Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {
        this.path = path;
        this.scanParams = scanParams;
        this.listId = listId;
        this.subSite = subSite;
        this.filePropertiesDtoConsumer = filePropertiesDtoConsumer;
        this.createScanTaskConsumer = createScanTaskConsumer;
        this.scanActivePredicate = scanActivePredicate;
        this.filePropsProgressTracker = filePropsProgressTracker;
        this.libName = libName;
    }

    public String getPath() {
        return path;
    }

    public ScanTaskParameters getScanParams() {
        return scanParams;
    }

    public String getListId() {
        return listId;
    }

    public String getLibName() {
        return libName;
    }

    public String getSubSite() {
        return subSite;
    }

    public Consumer<ClaFilePropertiesDto> getFilePropertiesDtoConsumer() {
        return filePropertiesDtoConsumer;
    }

    public Consumer<String> getCreateScanTaskConsumer() {
        return createScanTaskConsumer;
    }

    public Predicate<Pair<Long, Long>> getScanActivePredicate() {
        return scanActivePredicate;
    }

    public ProgressTracker getFilePropsProgressTracker() {
        return filePropsProgressTracker;
    }

    public void appendToPath(String addendum) {
        this.path += addendum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreamListData that = (StreamListData) o;
        return Objects.equals(path, that.path) &&
                Objects.equals(scanParams, that.scanParams) &&
                Objects.equals(listId, that.listId) &&
                Objects.equals(libName, that.libName) &&
                Objects.equals(subSite, that.subSite) &&
                Objects.equals(filePropertiesDtoConsumer, that.filePropertiesDtoConsumer) &&
                Objects.equals(createScanTaskConsumer, that.createScanTaskConsumer) &&
                Objects.equals(scanActivePredicate, that.scanActivePredicate) &&
                Objects.equals(filePropsProgressTracker, that.filePropsProgressTracker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, scanParams, listId, libName, subSite, filePropertiesDtoConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
    }
}
