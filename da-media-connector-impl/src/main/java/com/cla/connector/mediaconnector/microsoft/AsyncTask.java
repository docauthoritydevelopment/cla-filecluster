package com.cla.connector.mediaconnector.microsoft;

/**
 * Encapsulate task to be performed in back ground
 * @param <T> Task return type
 * @param <E> Exception which may be thrown.
 */
public interface AsyncTask<T, E extends Exception> {

    T execute() throws E;
}
