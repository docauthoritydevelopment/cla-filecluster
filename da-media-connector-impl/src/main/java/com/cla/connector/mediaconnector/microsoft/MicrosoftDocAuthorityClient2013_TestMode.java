package com.cla.connector.mediaconnector.microsoft;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.independentsoft.share.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

public class MicrosoftDocAuthorityClient2013_TestMode extends MicrosoftDocAuthorityClient2013 {
    private static final Logger logger = LoggerFactory.getLogger(MicrosoftDocAuthorityClient2013_TestMode.class);

    private String folderToFail;

    MicrosoftDocAuthorityClient2013_TestMode(String userName, String domain, String password, String userAgent, String url,
                                             int maxRetries, MSConnectionConfig connectionConfig, String folderToFailOn, String... charsToFilter) {
        super(userName, domain, password, userAgent, url, maxRetries, connectionConfig, charsToFilter);

        this.folderToFail = Optional.ofNullable(folderToFailOn)
                .map(SharePointParseUtils::removeUnneededDoubleSlashes)
                .map(folder -> {
                    if (folder.startsWith(url)) {
                        folder = super.basePath + "/" + folder.substring(url.length());
                    }
                    return folder;
                })
                .map(SharePointParseUtils::normalizePath)
                .orElse(null);
    }

    @Override
    public List<ClaFilePropertiesDto> getFilesWithMediaItemId2013(String subSite, String folder, int pageIdx, int pageSize) throws ServiceException {
        if(SharePointParseUtils.pathsEquals(folderToFail, folder)) {
            logger.info("***** Simulating failure on folder {}", folder);
            throw new DataAccessException("Test mode: Failed to get files metadata (completion) for " + folder) {};
        } else {
            return super.getFilesWithMediaItemId2013(subSite, folder, pageIdx, pageSize);
        }
    }
}