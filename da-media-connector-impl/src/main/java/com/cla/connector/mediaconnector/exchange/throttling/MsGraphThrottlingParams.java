package com.cla.connector.mediaconnector.exchange.throttling;

import com.cla.connector.mediaconnector.ThrottlingStartledTurtle;

/**
 * Created By Itai Marko
 */
public class MsGraphThrottlingParams {

    private final ThrottlingStartledTurtle throttler;
    private final int maxRetriesOnThrottling;

    public MsGraphThrottlingParams(ThrottlingStartledTurtle throttler, int maxRetriesOnThrottling) {
        this.throttler = throttler;
        this.maxRetriesOnThrottling = maxRetriesOnThrottling;
    }

    ThrottlingStartledTurtle getThrottler() {
        return throttler;
    }

    /**
     * Maximum number of attempts to retry sending a call to Exchange 365 MS-Graph API.
     * Zero or negative values indicated to retry forever.
     */
    Integer getMaxRetriesOnThrottling() {
        return maxRetriesOnThrottling;
    }
}
