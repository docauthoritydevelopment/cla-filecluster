package com.cla.connector.mediaconnector.mail;

import com.cla.connector.utils.FileNamingUtils;
import com.google.common.base.Strings;

public interface MailRelated {
    public static final String EMPTY_SUBJECT = "(no subject)";
    public static final int maxFileNameSize = 64;
    public static final String DEFAULT_HASH = "52652228";

    public static String changeSubjectToFileName(String subject, String mailIdentifier) {
        String pathifiedSubject = FileNamingUtils.replaceIllegalPathChars(subject);
        if (Strings.isNullOrEmpty(pathifiedSubject)) {
            pathifiedSubject = MailRelated.EMPTY_SUBJECT;
        }
        if (pathifiedSubject.length() > maxFileNameSize) {
            pathifiedSubject = pathifiedSubject.substring(0, maxFileNameSize);
        }
        String uniqueId = getHashAdditionToFileName(mailIdentifier);
        return pathifiedSubject + uniqueId;
    }

    public static String getHashAdditionToFileName(String mailIdentifier) {
        return " " + (Strings.isNullOrEmpty(mailIdentifier) ? DEFAULT_HASH : HashUtils.getCRC32(mailIdentifier));
    }

    public static String getSubject(String subject) {
        if (Strings.isNullOrEmpty(subject)) {
            return null;
        }
        if (Strings.isNullOrEmpty(subject.trim())) {
            return null;
        }
        return subject.trim();
    }

    public static String addHashToBaseName(String baseName, String mailIdentifier) {
        String extension = FileNamingUtils.getFilenameExtension(baseName);
        baseName = baseName.substring(0, baseName.length()-(extension.length()+1)) + MailRelated.getHashAdditionToFileName(mailIdentifier);
        baseName += (extension == null || extension.isEmpty()) ? "" : "." + extension;
        return baseName;
    }
}
