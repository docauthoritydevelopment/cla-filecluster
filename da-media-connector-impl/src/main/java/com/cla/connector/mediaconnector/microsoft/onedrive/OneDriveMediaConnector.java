package com.cla.connector.mediaconnector.microsoft.onedrive;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.file.ServerResourceType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.microsoft.*;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.google.common.collect.Lists;
import com.independentsoft.share.Folder;
import com.independentsoft.share.PrincipalType;
import com.independentsoft.share.ServiceException;
import com.independentsoft.share.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by oren on 12/25/2017.
 */
public class OneDriveMediaConnector extends MicrosoftConnectorBase {
    private static final Logger logger = LoggerFactory.getLogger(OneDriveMediaConnector.class);

    private static final String URL_TEMPLATE = "https://<domain>-my.sharepoint.com/";
    private static final String SITE_URL_TEMPLATE = "personal/<normalized user name>";
    private static final String DRIVE_PREFIX = "/Documents";
    private static final String SUBSITE_REGEX = "personal/[^/]+";


    private static final String USERNAME_SUFFIX = ".onmicrosoft.com";

    private String urlPrefix;

    private String defaultSubsite;

    protected OneDriveMediaConnector(SharePointConnectionParametersDto sharePointConnectionDetailsDto,
                                     MSAppInfo appInfo,
                                     int maxRetries,
                                     int pageSize,
                                     long maxSupportFileSize,
                                     MSConnectionConfig connectionConfig,
                                     boolean doInit,
                                     List<String> foldersToFilter,
                                     boolean isSpecialCharsSupported,
                                     String... charsToFilter) {

        super(sharePointConnectionDetailsDto.getUsername(), sharePointConnectionDetailsDto.getPassword(), appInfo,
                pageSize, maxRetries, maxSupportFileSize, connectionConfig,
                null, isSpecialCharsSupported, foldersToFilter);

        String username = sharePointConnectionDetailsDto.getUsername();
        String domain = StringUtils.isBlank(sharePointConnectionDetailsDto.getDomain()) ? extractDomainFromUser(username) : sharePointConnectionDetailsDto.getDomain();
        if (domain == null) {
            throw new IllegalArgumentException("Domain must be present");
        }

        String normalizedUsername = username.replaceAll("@", "_").replaceAll("\\.", "_");
        this.urlPrefix = URL_TEMPLATE.replace("<domain>", domain);
        defaultSubsite = SITE_URL_TEMPLATE.replace("<normalized user name>", normalizedUsername);

        if (doInit) {
            init(domain, defaultSubsite, urlPrefix, appInfo, charsToFilter);
        }
    }

    private String normalizeUsername(String username) {
        return username.replaceAll("@", "_").replaceAll("\\.", "_");
    }

    private String extractDomainFromUser(String username) { // experimental - Might username end with diff suffix??
//        eg: user@docauthority.onmicrosoft.com
        int atIdx = username.indexOf("@");
        if (atIdx > -1 && username.endsWith(USERNAME_SUFFIX)) {
            String domain = username.substring(atIdx + 1, username.length() - USERNAME_SUFFIX.length());
            logger.trace("Normalized username {}", domain);
            return domain;
        }

        logger.warn("Failed to extract domain from user {}", username);
        return null;
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.ONE_DRIVE;
    }

    @Override
    public void streamMediaItems(ScanTaskParameters scanParams,
                                 Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                 Consumer<DirListingPayload> directoryListingConsumer,
                                 Consumer<String> createScanTaskConsumer,
                                 Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {
        String path = scanParams.getMailboxUPN();
        if (!path.endsWith("/")) {
            path += "/";
        }

        String actualPath = shortPathToActual(path);
        logger.trace("Converted path {} to actual OD path {}", path, actualPath);
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(actualPath);
        logger.trace("Using following key {} to process folder data", key);
        TopFolderDataResult topFolderDataResult = processTopFolderData(scanParams.getRunId(), filePropertiesConsumer,
                key, actualPath);
        if (!topFolderDataResult.success) {
            abortScan(scanParams, filePropertiesConsumer, path, topFolderDataResult);
            return;
        }

        Predicate<? super String> fileTypesPredicate = FileTypeUtils.createFileTypesPredicate(scanParams.getScanTypeSpecification());
        Consumer<ClaFilePropertiesDto> customizedFilePropConsumer = getCustomFilePropConsumer(filePropertiesConsumer);
        streamFilesAndSubFolders(actualPath, topFolderDataResult.listId, key.getSite(), scanParams, fileTypesPredicate,
                customizedFilePropConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
        scanParams.setSyncState(String.valueOf(scanParams.getStartScanTime()));
    }

    private void abortScan(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer, String path, TopFolderDataResult topFolderDataResult) {
        logger.error(
                String.format("Aborting scan for run %d, upn: %s, reason: %s", scanParams.getRunId(),
                        scanParams.getMailboxUPN(), topFolderDataResult.message), topFolderDataResult.error);
        ClaFilePropertiesDto errDto = ClaFilePropertiesDto.create()
                .addError(createScanError(topFolderDataResult.message, topFolderDataResult.error, path,
                        scanParams.getRunId()));
        filePropertiesConsumer.accept(errDto);
    }

    private TopFolderDataResult processTopFolderData(Long runId, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                                     MSItemKey key, String path) {
        String listId = null;
        String folderPath = convertFileNameIfNeeded(key.getPath());
        Exception exp = null;
        try {
            listId = getFolderProperties(key.getSite(), folderPath).getListId();
            logger.trace("For key:{}, retrieved listId={}", key, listId);
        } catch (Exception e) {
            if (FailAccessException.class.isInstance(e) || DataAccessException.class.isInstance(e)) {
                logger.error("Failed to process top folder data.", e);
                throw (RuntimeException)e;
            } else {
                logger.error("Failed to process top folder data {} but ignoring",e.getClass(), e);
                exp = e;
            }
        }

        if (listId == null) {
            return TopFolderDataResult.fail(String.format("Path not found %s", path));
        }
        if (exp != null) {
            return TopFolderDataResult.fail(String.format("Failed to retrieve list id for path %s", path), exp);
        }

        processFolderData(runId, filePropertiesConsumer, path, key.getSite(), folderPath, listId);
        return TopFolderDataResult.success(listId);
    }

    @Override
    public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool,
                                           ScanTaskParameters scanParams,
                                           Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                           Consumer<DirListingPayload> directoryListingConsumer,
                                           Consumer<String> createScanTaskConsumer,
                                           Predicate<Pair<Long, Long>> scanActivePredicate,
                                           ProgressTracker filePropsProgressTracker) {

        scanParams.setSyncState(String.valueOf(scanParams.getStartScanTime()));
        String path = shortPathToActual(scanParams.getMailboxUPN());

        if (!path.endsWith("/")) {
            path += "/";
        }
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(path);

        Consumer<ClaFilePropertiesDto> customizedConsumer = getCustomFilePropConsumer(filePropertiesConsumer);

        TopFolderDataResult topFolderDataResult = processTopFolderData(scanParams.getRunId(), customizedConsumer, key, path);
        if (!topFolderDataResult.success) {
            abortScan(scanParams, filePropertiesConsumer, path, topFolderDataResult);
            return;
        }

        scanParams.setSyncState(String.valueOf(scanParams.getStartScanTime()));
        final Predicate<? super String> fileTypesPredicate = FileTypeUtils.createFileTypesPredicate(scanParams.getScanTypeSpecification());

        MicrosoftRecursiveAction action = (MicrosoftRecursiveAction) MicrosoftRecursiveAction.Builder.create()
                .withListId(topFolderDataResult.listId)
                .withSubSite(key.getSite())
                .withMicrosoftConnectorBase(this)
                .withPath(path)
                .withCreateScanTaskConsumer(createScanTaskConsumer)
                .withDirListingConsumer(directoryListingConsumer)
                .withFilePropsConsumer(customizedConsumer)
                .withFileTypesPredicate(fileTypesPredicate)
                .withJobStatePredicate(scanActivePredicate)
                .withProgressTracker(filePropsProgressTracker)
                .withScanParams(scanParams)
                .build();

        forkJoinPool.invoke(action);
    }

    public void streamMediaChangeLog(
            ScanTaskParameters scanParams, String realPath, Long runId, long startChangeLogPosition,
            Consumer<MediaChangeLogDto> changeConsumer, Consumer<String> createScanTaskConsumer,
            Predicate<? super String> fileTypesPredicate) {

        String path = shortPathToActual(scanParams.getMailboxUPN());

        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(path);

        Consumer<MediaChangeLogDto> customConsumer = getCustomFilePropConsumer(changeConsumer);
        logger.debug("Streaming changes for path: {} and site: {}", path, key.getSite());
        super.streamMediaChangeLogForSite(scanParams, path, key.getSite(), Long.valueOf(scanParams.getSyncState()), customConsumer);
    }

    private <T extends ClaFilePropertiesDto> Consumer<T> getCustomFilePropConsumer(Consumer<T> filePropertiesConsumer) {
        return (item) -> {
            Optional.ofNullable(item.getFileName())
                    .filter(filename -> filename.startsWith(urlPrefix))
                    .ifPresent(filename -> item.setFileName(actualPathToShort(item.getFileName())));
            filePropertiesConsumer.accept(item);
        };
    }

    private String shortPathToActual(String path) {
        path = StringUtils.removeStart(path, "/");
        String localPath = StringUtils.substringAfter(path, "/");
        String user = StringUtils.substringBefore(path, "/");
        user = user.replaceAll("@", "_").replaceAll("\\.", "_");
        user = StringUtils.removeStart(user, "{");
        user = StringUtils.removeEnd(user, "}");
        return new StringBuilder(urlPrefix)
                .append("{personal}/")
                .append("{").append(user).append("}")
                .append("/Documents/")
                .append(localPath)
                .toString();
    }

    private String actualPathToShort(String path) {
        String chopped = StringUtils.removeStartIgnoreCase(path, urlPrefix + "{personal}/");
        String user = StringUtils.substringBefore(chopped, "/");
        user = StringUtils.removeStart(user, "{");
        user = StringUtils.removeEnd(user, "}");
        String localPath = StringUtils.substringAfter(chopped, "/");
        localPath = StringUtils.removeStartIgnoreCase(localPath, "documents/");
        return "/" + user + "/" + localPath;

    }

    @Override
    public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
        return true; // TODO workaround. until supporting root folder accessibility check on a list of paths
//        final String compatiblePath = convertFileNameIfNeeded(path);
//        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(compatiblePath);
//        if (key.getSite() != null) {
//            try {
//                execAsyncTask(() -> service.getFolder(key.getSite(), key.getPath()));
//                return true;
//            } catch (Exception e) {
//                logger.error("Failed to get folder {}", path, e);
//            }
//        }
//        return false;
    }


    @Override
    public ClaFilePropertiesDto getFileAttributes(String mediaItemId) throws FileNotFoundException {
        ClaFilePropertiesDto fileAttr = super.getFileAttributes(mediaItemId);
        String path = SharePointParseUtils.normalizePath(fileAttr.getFileName());
        String subsite = extractUserDrive(path);
        String markedPath = SharePointParseUtils.applySiteMark(path, subsite);
        String strippedPrefix = StringUtils.stripEnd(urlPrefix, "/");
        if (!markedPath.startsWith(strippedPrefix)) {
            markedPath = strippedPrefix + markedPath;
        }
        fileAttr.setFileName(actualPathToShort(markedPath));
        return fileAttr;
    }

    @Override
    protected String convertFileNameIfNeeded(String fileName) {

        fileName = SharePointParseUtils.normalizePath(fileName);
        String url = SharePointParseUtils.normalizePath(urlPrefix);
        String convertedFilename = fileName.startsWith(url) ? fileName.substring(url.length()) : fileName;
        if (!convertedFilename.startsWith("/")) {
            convertedFilename = "/" + convertedFilename;
        }

        return convertedFilename;
    }

    @Override
    public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
        String path = Optional.ofNullable(folderMediaEntityId).orElse("/");
        if ("/".equals(path)) {
            return initialBrowse();
        } else {
            return browseSiteFolders(folderMediaEntityId);
        }
    }

    private List<ServerResourceDto> initialBrowse() {
        try {
            return service.getUsers(null)
                    .stream()
                    .filter(user -> PrincipalType.USER.equals(user.getType()))
                    .map(User::getEmail)
                    .filter(StringUtils::isNotEmpty)
                    .map(this::toServerResourceDto)
                    .collect(Collectors.toList());
        } catch (ServiceException e) {
            logger.error("unable to retrieve users", e);
            String defaultUrl = SharePointParseUtils.applySiteMark(urlPrefix + defaultSubsite + DRIVE_PREFIX, defaultSubsite);
            return listCoreSite(defaultUrl, userName);
        }
    }

    private ServerResourceDto toServerResourceDto(String userEmail) {
        String url = urlPrefix +
                SITE_URL_TEMPLATE.replace("<normalized user name>", normalizeUsername(userEmail)) +
                DRIVE_PREFIX;
        String subSite = extractUserDrive(url);
        url = SharePointParseUtils.applySiteMark(url, subSite);
        ServerResourceDto coreSite = new ServerResourceDto(url, userEmail);
        coreSite.setHasChildren(true);
        coreSite.setAccessible(true);
        coreSite.setType(ServerResourceType.SITE);
        return coreSite;
    }

    List<ServerResourceDto> browseSiteFolders(String path) {
        String compatibleFolderName = convertFileNameIfNeeded(path);
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(compatibleFolderName);
        List<Folder> folders;
        try {
            folders = execAsyncTask(() -> service.getFolders(key.getSite(), key.getPath()));
        } catch (Exception e) {
            logger.error("Failed to get folders under {}", path, e);
            throw new RuntimeException(e);
        }
        return folders.stream()
                .filter(folder -> !folder.getName().startsWith("_"))
                .map(folder -> {
                    String folderPath = StringUtils.stripEnd(urlPrefix, "/") + folder.getServerRelativeUrl();
                    String markedFolderPath = SharePointParseUtils.applySiteMark(folderPath, key.getSite());
                    ServerResourceDto dto = new ServerResourceDto(markedFolderPath, folder.getName());
                    dto.setHasChildren(folder.getItemCount() > 0);
                    return dto;
                })
                .collect(Collectors.toList());
    }

    private String extractUserDrive(String path) {
        Matcher matcher = Pattern.compile(SUBSITE_REGEX).matcher(path);
        if (matcher.find()) {
            return matcher.group(0);
        }
        logger.error("could not extract sub-site from: {}", path);
        return null;
    }

    /*List<ServerResourceDto> listFilesAsServerResources(String path) {
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(path);
        List<ServerResourceDto> files = super.listFilesAsServerResources(key.getSite(), key.getPath(), maxRetries);
        files.forEach(file -> {
            String truncatedFullFileName = file.getFullName().substring(urlPrefix.length() + 1); // compensate added '/'
            if (truncatedFullFileName.startsWith("//")) {
                truncatedFullFileName = truncatedFullFileName.substring(1);
            }
            file.setFullName(truncatedFullFileName);
        });
        return files;
    }*/

    @Override
    protected String extractLibraryBasePath(String subSite, com.independentsoft.share.List list) {
        return null;
    }

    @Override
    protected String createBaseUri(boolean includeBasePath) {
        return null;
    }

    @Override
    protected List<ServerResourceDto> browseSubSiteFolders(String relPath, String subSite) {
        return Lists.newArrayList();
    }

    @Override
    protected List<ServerResourceDto> listSubSites(String updatedPath) {
        return Lists.newArrayList();
    }

    @Override
    protected List<ServerResourceDto> listSubSites(MSItemKey key, String basePath) {
        return Lists.newArrayList();
    }

    public InputStream getInputStream(String mediaItemId) throws FileNotFoundException {
        return getInputStreamForMediaItemId(mediaItemId);
    }

    public static OneDriveMediaConnectorBuilder builder() {
        return new OneDriveMediaConnectorBuilder();
    }

    public static class OneDriveMediaConnectorBuilder extends MicrosoftConnectorBaseBuilder<OneDriveMediaConnector, OneDriveMediaConnectorBuilder> {
        private boolean doInit = true;

        public OneDriveMediaConnectorBuilder withDoInit(boolean doInit) {
            this.doInit = doInit;
            return getThis();
        }

        @Override
        protected OneDriveMediaConnectorBuilder getThis() {
            return this;
        }

        @Override
        public OneDriveMediaConnector build() {
            return new OneDriveMediaConnector(
                    sharePointConnectionParametersDto,
                    appInfo,
                    maxRetries,
                    pageSize,
                    maxFileSize,
                    connectionConfig,
                    doInit,
                    foldersToFilter,
                    isSpecialCharsSupported,
                    charsToFilter);
        }
    }

    private static class TopFolderDataResult {


        public final boolean success;
        public final String listId;
        public final String message;
        public final Exception error;

        private TopFolderDataResult(boolean success, String listId, String message, Exception error) {
            this.success = success;
            this.listId = listId;
            this.message = message;
            this.error = error;
        }

        public static TopFolderDataResult fail(String message, Exception e) {
            return new TopFolderDataResult(false, null, message, e);
        }

        public static TopFolderDataResult fail(String message) {
            return new TopFolderDataResult(false, null, message, null);
        }

        public static TopFolderDataResult success(String listId) {
            return new TopFolderDataResult(true, listId, null, null);
        }

    }
}

