package com.cla.connector.mediaconnector.labeling.mip;

import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MIPConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MIPConnectionParametersDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.mediaconnector.MediaConnector;
import com.cla.connector.mediaconnector.labeling.RawLabelingError;
import com.cla.connector.mediaconnector.labeling.SetFileLabelOp;
import com.google.common.collect.ImmutableList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class MipMediaConnector implements MediaConnector {

    private static final Logger logger = LoggerFactory.getLogger(MipMediaConnector.class);

    private static final String MIP_OAUTH_TOKEN = "MIP_OAUTH_TOKEN";
    private static final String GRANT_TYPE = "grant_type";
    private static final String CLIENT_ID = "client_id";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";
    private static final String RESOURCE = "resource";

    private static final int timeBetweenConnectMillis = 30000;
    private static final int maxRetries = 3;

    private final String mipLoginUrl;
    private RestTemplate restTemplate;
    private MIPConnectionDetailsDto connectionDetails;
    private long connectionDetailsRefreshed = 0L;
    private String proxyUrlPrefix;
    private ReentrantLock lock = new ReentrantLock();
    private long latestInit = 0;

    public MipMediaConnector(String mipLoginUrl, MIPConnectionDetailsDto connectionDetails) {
        this.restTemplate = new RestTemplate();
        this.connectionDetails = connectionDetails;
        setProxyUrlPrefix(connectionDetails.getUrl());
        this.mipLoginUrl = mipLoginUrl;
    }

    public void init(){
        if (latestInit < System.currentTimeMillis() - timeBetweenConnectMillis) {
            if (lock.tryLock()) {
                logger.trace("Initializing MIP connector.");
                latestInit = System.currentTimeMillis();
                try {
                    MipLoginResponse loginResponse = loginMip();
                    initEngine(loginResponse.getAccessToken());
                } catch (Exception e) {
                    logger.error("Initialization error.", e);
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    public MipLabelResult[] getFileLabel(String path) {
        logger.trace("Retrieving MIP labels for path {}.", path);
        String getLabelUrl = proxyUrlPrefix + "labels";
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(getLabelUrl)
                .queryParam("filePath", path);
        return (MipLabelResult[])doWithRetry(
                builder.build(false).toUriString(), null, MipLabelResult[].class);
    }

    public Collection<RawLabelingError> setFileLabels(List<SetFileLabelOp> setFileLabels) {
        logger.trace("Applying {} labels.", setFileLabels.size());
        String setLabelsUrl = proxyUrlPrefix + "labels";
        RawLabelingError[] res = (RawLabelingError[])doWithRetry(setLabelsUrl, setFileLabels, RawLabelingError[].class);
        return res == null ? null : Arrays.asList(res);
    }

    public RawLabelingError[] setFileLabel(SetFileLabelOp setFileLabel) {
        logger.trace("Applying MIP label {}", setFileLabel.toString());
        String setLabelsUrl = proxyUrlPrefix + "labels";
        return (RawLabelingError[])doWithRetry(setLabelsUrl, ImmutableList.of(setFileLabel), RawLabelingError[].class);
    }

    public Collection<MipLabelResult> getAllLabels(){
        logger.trace("Retrieving list of available labels from MIP.");
        String getLabelsUrl = proxyUrlPrefix + "labels/all";
        MipLabelResult[] body = (MipLabelResult[])doWithRetry(getLabelsUrl, null, MipLabelResult[].class);
        return body == null ? null : Arrays.asList(body);
    }



    @Override
    public List<ServerResourceDto> testConnection() {
        try {
            logger.debug("Testing connection to MIP.");
            Collection<MipLabelResult> allLabels = getAllLabels();
            if (allLabels != null){
                logger.debug("Test succeeded.");
                return ImmutableList.of(new ServerResourceDto(connectionDetails.getUsername(), connectionDetails.getName()));
            }
        } catch (Exception e) {
            logger.debug("Test failed: " + e.getMessage());
        }
        return null;
    }

    //region ------------- private methods ---------------------------
    private MipLoginResponse loginMip() {
        MIPConnectionParametersDto params = connectionDetails.getMIPConnectionParametersDto();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();

        body.add(GRANT_TYPE, PASSWORD);
        body.add(USERNAME, connectionDetails.getUsername());
        body.add(CLIENT_ID, params.getClientId());
        body.add(PASSWORD, params.getPassword());
        body.add(RESOURCE, params.getResource());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);
        MipLoginResponse mipLoginResponse = restTemplate.postForObject(mipLoginUrl, request, MipLoginResponse.class);
        if (mipLoginResponse != null) {
            logger.info("Logged into MIP: " + mipLoginResponse.toString());
        }
        return mipLoginResponse;
    }

    private void initEngine(String accessToken) {
        MIPConnectionParametersDto params = connectionDetails.getMIPConnectionParametersDto();
        String initEngineUrl = proxyUrlPrefix + "/engine/init";
        HttpHeaders headers = new HttpHeaders();
        headers.put(MIP_OAUTH_TOKEN, Collections.singletonList(accessToken));
        headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);
        MipProxyInitRequest initRequest = new MipProxyInitRequest(params.getClientId(), "MipSdk-Sample-Apps", "1.0.0");
        HttpEntity<MipProxyInitRequest> request = new HttpEntity<>(initRequest, headers);
        ResponseEntity<Void> voidResponseEntity = restTemplate.postForEntity(initEngineUrl, request, Void.class);
        logger.info("MIP Proxy Engine initiated. Status: " + voidResponseEntity.getStatusCode());
    }

    private Object doWithRetry(String url, Object request, Class result) {
        Object body = null;
        int retry = 0;
        do {
            try {
                @SuppressWarnings("unchecked")
                ResponseEntity<Object> entity = request == null ?
                        restTemplate.getForEntity(url, result)
                        : restTemplate.postForEntity(url, request, result);
                body = entity.getBody();
                return body;
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode().equals(HttpStatus.FORBIDDEN)) {
                    init();
                }
                if (result.equals(RawLabelingError[].class)) {
                    body = returnErrResult(e.getMessage());
                }
            } catch (Exception e) {
                if (result.equals(RawLabelingError[].class)) {
                    body = returnErrResult(e.getMessage());
                }
                return body;
            }
            ++retry;
        } while (retry < maxRetries);
        return body;
    }

    private RawLabelingError[] returnErrResult(String errorMessage) {
        RawLabelingError err = new RawLabelingError();
        err.setErrorMessage(errorMessage);
        RawLabelingError[] result = new RawLabelingError[1];
        result[0] = err;
        return result;
    }
    //endregion

    //region --------------- getters and setters --------------------------


    private void setProxyUrlPrefix(String proxyUrlPrefix) {
        this.proxyUrlPrefix = proxyUrlPrefix;
        if (!proxyUrlPrefix.endsWith("/")){
            this.proxyUrlPrefix += "/";
        }
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.MIP;
    }

    public long getConnectionDetailsRefreshed() {
        return connectionDetailsRefreshed;
    }

    public void setConnectionDetailsRefreshed(long connectionDetailsRefreshed) {
        this.connectionDetailsRefreshed = connectionDetailsRefreshed;
    }
    //endregion

}
