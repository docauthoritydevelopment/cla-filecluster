package com.cla.connector.mediaconnector.labeling;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@SuppressWarnings("unused")
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class SetFileLabelOp {
    private String filePath;
    private String labelName;
    private String newFilePath;
    private String description;

    @JsonProperty("file_path")
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @JsonProperty("label_name")
    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    @JsonProperty("new_file_path")
    public String getNewFilePath() {
        return newFilePath;
    }

    public void setNewFilePath(String newFilePath) {
        this.newFilePath = newFilePath;
    }

}
