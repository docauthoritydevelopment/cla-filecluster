package com.cla.connector.mediaconnector.exchange.auth;

import com.cla.connector.domain.dto.media.Exchange365ConnectionParametersDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import com.microsoft.graph.authentication.IAuthenticationProvider;
import com.microsoft.graph.http.IHttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import static org.springframework.http.HttpStatus.*;

public class MSGraphAuthenticationProvider implements IAuthenticationProvider {
    private static final Logger logger = LoggerFactory.getLogger(MSGraphAuthenticationProvider.class);
    private static final List<HttpStatus> RETRYABLE_STATUSES = Lists.newArrayList(
            // client
            PROXY_AUTHENTICATION_REQUIRED, REQUEST_TIMEOUT, CONFLICT, GONE, PRECONDITION_FAILED, EXPECTATION_FAILED,
            I_AM_A_TEAPOT, UNPROCESSABLE_ENTITY, LOCKED, FAILED_DEPENDENCY, TOO_MANY_REQUESTS, UNAVAILABLE_FOR_LEGAL_REASONS,

            // server
            INTERNAL_SERVER_ERROR, BAD_GATEWAY, SERVICE_UNAVAILABLE, GATEWAY_TIMEOUT, VARIANT_ALSO_NEGOTIATES,
            INSUFFICIENT_STORAGE, NOT_EXTENDED,
            NETWORK_AUTHENTICATION_REQUIRED
    );

    private static final String AUTH_URL = "https://login.microsoftonline.com/<tenant_id>/oauth2/v2.0/token/";
    private static final String AUTH_SCOPE = "https://graph.microsoft.com/.default";
    private static final String GRANT_TYPE = "client_credentials";

    private final RestTemplate REST_TEMPLATE;
    private final RetryTemplate RETRY_TEMPLATE;

    private final MSGraphAuthConfig config;

    private final String authEndpoint;
    private final String tenantId;
    private final String clientId;
    private final String clientSecret;

    private final AtomicBoolean fetchActive = new AtomicBoolean();
    private final ReentrantLock lock = new ReentrantLock();
    private volatile String accessToken;
    private volatile String tokenType;
    private volatile Instant tokenProximatelyExpiryTimestamp;

    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    public MSGraphAuthenticationProvider(Exchange365ConnectionParametersDto connectionParametersDto,
                                         MSGraphAuthConfig config) {

        this.tenantId = connectionParametersDto.getTenantId();
        this.clientId = connectionParametersDto.getApplicationId();
        this.clientSecret = connectionParametersDto.getPassword();
        this.authEndpoint = Optional.ofNullable(connectionParametersDto.getAdUrlOverride())
                .orElse(AUTH_URL);

        this.config = config;

        REST_TEMPLATE = initRestTemplate(config.readTimeout, config.connectTimeout);
        RETRY_TEMPLATE = initRetryTemplate();
        tokenProximatelyExpiryTimestamp = Instant.now()
                .minus(1, ChronoUnit.SECONDS); // Make sure to login for the first time.

    }

    private RetryTemplate initRetryTemplate() {
        final RetryTemplate rt = new RetryTemplate();
        final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(config.maxAttempts);
        final FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(TimeUnit.MILLISECONDS.toMillis(config.retrySleepInterval));
        rt.setRetryPolicy(retryPolicy);
        rt.setBackOffPolicy(backOffPolicy);
        return rt;
    }

    private RestTemplate initRestTemplate(int readTimeout, int connectTimeout) {
        SimpleClientHttpRequestFactory reqFactory = new SimpleClientHttpRequestFactory();
        reqFactory.setConnectTimeout(connectTimeout);
        reqFactory.setReadTimeout(readTimeout);
        reqFactory.setOutputStreaming(false); // https://stackoverflow.com/a/31649238

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(reqFactory);
        restTemplate.setErrorHandler(new MSGraphOAuthErrorHandler());

        return restTemplate;
    }

    @Override
    public void authenticateRequest(IHttpRequest request) {
        if (isAccessTokenRefreshNeeded(false)) { // first login/Immediate refresh needed
            logger.info("New session - fetching access token");
            fetchAndSetAccessToken();
        }
        if (logger.isTraceEnabled()) {
            logger.trace("Authorizing using token '{}'", getObfuscatedToken());
        }
        request.addHeader(HttpHeaders.AUTHORIZATION, accessToken);

        if (!fetchActive.get() && isAccessTokenRefreshNeeded(true)) {// Fetch new token before current expires
            fetchActive.set(true);
            executorService.execute(() -> {
                logger.info("Refreshing access token");
                fetchAndSetAccessToken();
            });
        } else if(logger.isDebugEnabled()) {
            logger.debug("No need for new token; fetchActive={}", fetchActive.get());
        }
    }

    private String getObfuscatedToken() {
        return accessToken.substring(0, tokenType.length() + 5) + "..." + accessToken.substring(accessToken.length() - 5);
    }

    private boolean isAccessTokenRefreshNeeded(boolean isPrefetch) {
        Instant now = Instant.now();
        if (isPrefetch) {
            now = now.plus(config.prefetchTokenBeforeExpiry, ChronoUnit.MILLIS);
        }

        boolean refreshNeeded = now.isAfter(tokenProximatelyExpiryTimestamp);
        if (logger.isTraceEnabled()) {
            logger.trace("Current token expires on {}, prefetch {} ms, refresh needed: {}",
                    tokenProximatelyExpiryTimestamp.toString(), config.prefetchTokenBeforeExpiry, refreshNeeded);
        }
        return refreshNeeded;
    }

    private void fetchAndSetAccessToken() {
        lock.lock();
        fetchActive.set(true);
        try {
            if (!isAccessTokenRefreshNeeded(true)) {
                logger.trace("Token already refreshed, aborting.");
                return;
            }

            logger.info("Obtaining new access token");
            OAuthResponse res = RETRY_TEMPLATE.execute(
                    this::loginWithRetry,
                    rc -> {
                        Throwable t = Optional.of(rc.getLastThrowable())
                                .filter(lastThrow -> lastThrow.getCause() != null)
                                .map(Throwable::getCause)
                                .orElse(rc.getLastThrowable());
                        logger.error("Failed to fetch access token", t);
                        throw t instanceof RuntimeException ? (RuntimeException) t : new RuntimeException(t);
                    }
            );

            accessToken = res.tokenType + " " + res.accessToken;
            tokenType = res.tokenType;
            logger.trace("Setting new access token '{}' (type {})", getObfuscatedToken(), res.tokenType);
            int proximateExpiry = (int) (res.expiresIn * 0.99); // make the token expire a bit sooner
            tokenProximatelyExpiryTimestamp = Instant.now().plus(proximateExpiry, ChronoUnit.SECONDS);
        } finally {
            fetchActive.set(false);
            lock.unlock();
        }
    }

    private OAuthResponse loginWithRetry(RetryContext retryContext) {
        if (retryContext.getRetryCount() > 0) {
            if (!isRetryable(retryContext.getLastThrowable())) {
                retryContext.setExhaustedOnly();
                logger.info("Aborted retries (Not retryable error)");
            }
        }
        logger.info("Fetch access token (attempt {}/{})", retryContext.getRetryCount()+1, config.maxAttempts);
        return getToken();
    }

    private boolean isRetryable(Throwable lastThrowable) {
        if (lastThrowable == null || lastThrowable instanceof TimeoutException) {
            return true;
        }

        if (lastThrowable instanceof HttpStatusCodeException) {
            HttpStatus code = ((HttpStatusCodeException) lastThrowable).getStatusCode();
            return RETRYABLE_STATUSES.contains(code);
        }

        return false;
    }

    private OAuthResponse getToken() {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("scope", AUTH_SCOPE);
        params.add("grant_type", GRANT_TYPE);
        params.add("client_id", clientId);
        params.add("client_secret", clientSecret);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Lists.newArrayList(MediaType.APPLICATION_JSON));

        HttpEntity<MultiValueMap<String, String>> body = new HttpEntity<>(params, headers);
        ResponseEntity<OAuthResponse> response;
        logger.info("Obtaining access token for tenant={} client={}", tenantId, clientId);
        try {
            response = REST_TEMPLATE.postForEntity(authEndpoint.replaceAll("<tenant_id>", tenantId), body, OAuthResponse.class);
        } catch (HttpStatusCodeException e) {
            logger.error("Failed to obtain access token (response-body={})", e.getResponseBodyAsString(), e);
            throw e;
        } catch (Exception e) {
            logger.error("Failed to obtain access token", e);
            throw new RuntimeException(e);
        }

        return response.getBody();
    }

    private static class OAuthResponse implements Serializable {
        private String tokenType;
        private String extExpiresIn;
        private int expiresIn;
        private String accessToken;

        public OAuthResponse(@JsonProperty(value = "token_type", required = true) String tokenType,
                             @JsonProperty(value = "ext_expires_in") String extExpiresIn,
                             @JsonProperty(value = "expires_in", required = true) int expiresIn,
                             @JsonProperty(value = "access_token", required = true) String accessToken) {

            this.tokenType = tokenType;
            this.extExpiresIn = extExpiresIn;
            this.expiresIn = expiresIn;
            this.accessToken = accessToken;
        }

        @Override
        public String toString() {
            return "OAuthResponse{" +
                    "tokenType='" + tokenType + '\'' +
                    ", extExpiresIn='" + extExpiresIn + '\'' +
                    ", expiresIn='" + expiresIn + '\'' +
                    ", accessToken='" + accessToken + '\'' +
                    '}';
        }
    }
}