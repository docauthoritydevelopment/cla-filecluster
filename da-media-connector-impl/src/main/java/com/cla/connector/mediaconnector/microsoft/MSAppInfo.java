package com.cla.connector.mediaconnector.microsoft;

public class MSAppInfo {

    public final String company;
    public final String appName;
    public final String version;

    public MSAppInfo(String company, String appName, String version) {
        this.company = company;
        this.appName = appName;
        this.version = version;
    }

    @Override
    public String toString() {
        return "MSAppInfo{" +
                "company='" + company + '\'' +
                ", appName='" + appName + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}

