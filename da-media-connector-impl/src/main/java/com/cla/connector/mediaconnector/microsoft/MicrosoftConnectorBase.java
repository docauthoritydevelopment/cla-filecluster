package com.cla.connector.mediaconnector.microsoft;

import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.*;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.exceptions.MediaConnectionException;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointExtendedFolder;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointListItem;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointRoleAssignment;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Converters;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.independentsoft.share.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.security.AccessControlException;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by oren on 12/27/2017.
 */
public abstract class MicrosoftConnectorBase extends AbstractMediaConnector {

    private static final Logger logger = LoggerFactory.getLogger(MicrosoftConnectorBase.class);

    private static final String UNKNOWN_USER = "Unknown";

    protected Set<String> detectedSubSites = Sets.newHashSet();


    //TODO 2.0 - set
    protected MicrosoftDocAuthorityClient microsoftDocAuthorityClient;

    protected int pageSize;
    protected int maxRetries;
    protected long maxFileSize;

    protected Service service;

    protected String userName;
    protected String password;
    protected String url;
    protected String domain;
    protected String basePath;
    protected String domainEndpoint;

    protected MSConnectionConfig connectionConfig;

    protected List<String> foldersToFilter;
    protected String folderToFail;

    private AtomicInteger clientCallCounter = new AtomicInteger();
    private final AtomicInteger spApiCallCounter = new AtomicInteger();
    protected MSAppInfo appInfo;
    protected String userAgent;

    protected int spInstanceVersion;
    protected boolean isSpecialCharsSupported;

    private ExecutorService resourceExecutor = Executors.newCachedThreadPool();

    protected MicrosoftConnectorBase(String userName,
                                     String password,
                                     MSAppInfo appInfo,
                                     int pageSize,
                                     int maxRetries,
                                     long maxFileSize,
                                     MSConnectionConfig connectionConfig,
                                     String folderToFail,
                                     boolean isSpecialCharsSupported,
                                     List<String> foldersToFilter) {

        this.userName = userName;
        this.password = password;
        this.appInfo = appInfo;
        this.pageSize = pageSize;
        this.maxRetries = maxRetries;
        this.maxFileSize = maxFileSize;
        this.connectionConfig = connectionConfig;
        this.folderToFail = folderToFail;
        this.foldersToFilter = foldersToFilter.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());

        this.isSpecialCharsSupported = isSpecialCharsSupported;
    }

    protected void init(String domain, String defaultSubSite, String url, MSAppInfo appInfo, String... charsToFilter) {
        try {
            this.url = new java.net.URL(url).toString();
        } catch (MalformedURLException e) {
            logger.error("Bad url", e);
            throw new RuntimeException("Bad url: " + url, e);
        }
        this.domain = domain;
        microsoftDocAuthorityClient = getCompatibleMSClient(domain, url, defaultSubSite, appInfo,
                connectionConfig, isSpecialCharsSupported, charsToFilter);
        initConnectionPool();
    }

    private MicrosoftDocAuthorityClient getCompatibleMSClient(String domain,
                                                              String url,
                                                              String defaultSubSite,
                                                              MSAppInfo appInfo,
                                                              MSConnectionConfig connectionConfig,
                                                              boolean specialCharsSupported,
                                                              String[] charsToFilter) {

        Optional.ofNullable(SharePointParseUtils.parseUserAgent(appInfo))
                .ifPresent(ua -> this.userAgent = ua);

        MicrosoftDocAuthorityClient temp = new MicrosoftDocAuthorityClient(userName, domain, password, this.userAgent,
                url, maxRetries, connectionConfig, specialCharsSupported, charsToFilter);
        try {
            final MicrosoftDocAuthorityClient verTemp = temp;
            spInstanceVersion = execAsyncTask(verTemp::getVersion);
            logger.info("Detected version: {}", spInstanceVersion);
            if (MicrosoftDocAuthorityClient.VERSION_2013 == spInstanceVersion) {
                temp = StringUtils.isEmpty(folderToFail)
                        ? new MicrosoftDocAuthorityClient2013(userName, domain, password, userAgent, url, maxRetries,
                        connectionConfig, charsToFilter)
                        : new MicrosoftDocAuthorityClient2013_TestMode(userName, domain, password, userAgent, url,
                        maxRetries, connectionConfig, folderToFail, charsToFilter);
                logger.info("Initialized 2013 ms-client");
            }
        } catch (Exception e) {
            logger.error("Failed to get connector version, falling back to default (2016)", e);
            spInstanceVersion = MicrosoftDocAuthorityClient.VERSION_DEFAULT;
        }

        if (domain != null) {
            logger.debug("Create {} connector (service) on {} with user {} at domain {}", getMediaType().name(), url, userName, domain);
            service = new Service(url, defaultSubSite, spInstanceVersion, isSpecialCharsSupported, userName, password,
                    domain, connectionConfig.getConnectionTimeout(), connectionConfig.getSocketTimeout());
        } else {
            logger.debug("Create {} connector (service) on {} with user {}", getMediaType().name(), url, userName);
            service = new Service(url, defaultSubSite, spInstanceVersion, isSpecialCharsSupported, userName, password,
                    null, connectionConfig.getConnectionTimeout(), connectionConfig.getSocketTimeout());
        }
        MicrosoftDocAuthorityClient finalTemp = temp;
        service.setAppSignInHeadersRetriever(_httpClient -> finalTemp.getAppSignInHeaders(_httpClient));

        Optional.ofNullable(userAgent)
                .ifPresent(service::setUserAgent);
        temp.setJShareService(service);
        return temp;
    }


    private void initConnectionPool() {
        PoolingHttpClientConnectionManager poolMan = (PoolingHttpClientConnectionManager) service.getClientConnectionManager();
        poolMan.setMaxTotal(connectionConfig.getMaxConnectionsPerHost() * 10);
        poolMan.setDefaultMaxPerRoute(connectionConfig.getMaxConnectionsPerHost());
        microsoftDocAuthorityClient.setClientConnectionManager(poolMan);
    }

    /**
     * Concurrent
     */
    List<String> concurrentStreamFilesAndSubFolders(String path,
                                                    ScanTaskParameters scanParams,
                                                    Predicate<? super String> fileTypesPredicate,
                                                    Consumer<ClaFilePropertiesDto> fileConsumer,
                                                    Predicate<Pair<Long, Long>> scanActivePredicate,
                                                    ProgressTracker filePropsProgressTracker,
                                                    String listId,
                                                    String subSite) {

        if (scanActivePredicate != null && !scanActivePredicate.test(Pair.of(scanParams.getJobId(), scanParams.getJobStateIdentifier()))) {
            logger.info("scanActivePredicate returned false for job {} failed . Aborting", scanParams.getJobId());
            return Lists.newArrayList();
        }

        String folderPath = convertFileNameIfNeeded(path);
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(folderPath);
        folderPath = key.getPath();
        if (MediaConnectorUtils.isFolderExcluded(path, folderPath, scanParams.getExcludedRules(), scanParams.getSkipDirnames(), scanParams.getRunId())) {
            logger.debug("Path {} excluded - skipping", path);
            return Collections.emptyList();
        }

        Long runId = scanParams.getRunId();
        try {
            processFolderData(runId, fileConsumer, path, subSite, folderPath, listId);
        } catch (Exception e) {
            logger.error("Failed to process folder data", e);
            ClaFilePropertiesDto errDto = ClaFilePropertiesDto.create()
                    .addError(createScanError("Error while processing top folder " + path, e, SharePointParseUtils.applySiteMark(path, subSite), runId));
            fileConsumer.accept(errDto);

        }
        if (!isSubSitePath(key)) {
            try {
                streamFiles(runId, folderPath, subSite, listId, fileTypesPredicate, fileConsumer);
            } catch (Exception e) {
                logger.error("Failed to stream files under folder {} ({})", path, folderPath, e);
                ClaFilePropertiesDto errDto = ClaFilePropertiesDto.create()
                        .addError(createScanError("Error while processing folder " + path, e, SharePointParseUtils.applySiteMark(path, subSite), runId));
                fileConsumer.accept(errDto);
            }
        }
        // we want to call this every once in a while to make sure
        // we send scan progress even if we skip files
        filePropsProgressTracker.incrementProgress(0);

        if (subSite != null) {
            path = SharePointParseUtils.splitPathAndSubsite(path).getPath();
        }
        if (!path.endsWith("/")) {
            path += "/";
        }
        return getFoldersUnderSite(subSite, path, folderPath)
                .stream()
                .map(ServerResourceDto::getName)
                .collect(Collectors.toList());
    }

    protected boolean isSubSitePath(MSItemKey itemKey) {
        String path = SharePointParseUtils.normalizePath(itemKey.getPath());
        return Optional.ofNullable(itemKey.getSite())
                .map(SharePointParseUtils::normalizePath)
                .map(path::endsWith)
                .orElse(false);
    }

    private List<ServerResourceDto> getFoldersUnderSite(String subSite, String fullPath, String relPath) {
        List<ServerResourceDto> folders;
        if (isSubSitePath(MSItemKey.path(subSite, fullPath))) {
            relPath = SharePointParseUtils.applySiteMark(relPath, subSite);
            folders = listSubSites(relPath);
            try {
                List<ServerResourceDto> temp = browseSubSiteFolders(relPath, subSite);
                folders.addAll(temp);
            } catch (Exception e) {
                logger.error("Failed to list sub site's folders under path=" + fullPath + ", relPath=" + relPath, e);
            }
        } else {
            folders = listFoldersUnderLibrary(fullPath, subSite);
        }
        return folders;
    }

    protected void processFolderData(Long runId,
                                     Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                     String path,
                                     String subSite,
                                     String folderPath,
                                     String listId) {

        logger.info("Processing {}", path);
        logger.debug(Arrays.toString(Thread.currentThread().getStackTrace()));
        SharePointExtendedFolder fetchedFolder = null;
        try {
            fetchedFolder = getFolderProperties(subSite, SharePointParseUtils.removeUnneededDoubleSlashes(folderPath));
        } catch (FileNotFoundException | AccessControlException ex) {
            ClaFilePropertiesDto propsDto = ClaFilePropertiesDto.create().addError(
                    createScanError("Error while processing folder " + path, ex, SharePointParseUtils.applySiteMark(path, subSite), runId));
            filePropertiesConsumer.accept(propsDto);
            logger.error("Failed to fetch root folder " + path + " subsite=" + subSite, ex);
            String msg = Optional.ofNullable(ex.getMessage()).orElse("");
            if (msg.contains("403") || msg.contains("429")) {
                logger.error("Failed to process top folder data for path {}, probably throttling.", path, ex);
                //throw new AccessControlException("Failed to process top folder data (" + path + " )" + ex.getMessage() + ") probably throttle control");
            } else {
                logger.error("Failed to fetch folder for path {}.", path, ex);
                //throw new RuntimeException("Failed to fetch folder " + path, ex);
            }
        }
        if (fetchedFolder != null) {
            if (listId == null) {
                listId = fetchedFolder.getListId();
            }
            processFolderData(fetchedFolder, filePropertiesConsumer, path, subSite, folderPath, listId);
        }
    }

    private void processFolderData(SharePointExtendedFolder folder2Process,
                                   Consumer<ClaFilePropertiesDto> filePropertiesConsumer, String path, String subSite,
                                   String folderPath, String listId) {

        String folderMediaId;
        String folderName = folder2Process.getName();
        logger.debug("Received folder {}", path);

        AclInheritanceType aclInheritanceType = null;
        if (listId != null) {
            folderMediaId = SharePointParseUtils.splitMediaItemIdAndSite(listId).getListId();
            logger.debug("Extract from  listId {} folderMediaId {}", listId, folderMediaId);
            String itemId = null;
            try {
                itemId = execAsyncTask(() -> microsoftDocAuthorityClient.getFolderMediaItemId(subSite, folderPath));
                if (itemId != null) {
                    folderMediaId = SharePointParseUtils.calculateMediaItemId(null, listId, itemId);
                }
            } catch (Exception e) {
                logger.debug("Failed to get folder-media-item-id for folder {}. using listId {}, subsite: {}, err={}", folderPath, listId, subSite, e.getMessage());
            }
            if (itemId != null) {
                aclInheritanceType = getAclInheritanceType(subSite, folderPath, listId, itemId);
            }
        } else if (folderName != null) {
            logger.debug("No list id - setting media item id to be folder's name for folder {}", folderName);
            folderMediaId = folderName;
        } else {
            logger.warn("error processing folder, could not get folder data - {}", path);
            return;
        }

        // TODO Itai: [DAMAIN-3075] demutexify this (and other uses of setFolder() in this package when adding support for scanning PST files in SharePoint/OneDrive
        ClaFilePropertiesDto folderDto = ClaFilePropertiesDto.create()
                .setFolder(true);
        String temp = folderMediaId;
        folderMediaId = Optional.ofNullable(subSite)
                .map(site -> temp.equals(folderName) ? SharePointParseUtils.applySiteMark(temp, site) : SharePointParseUtils.calculateMediaItemId(site, temp))
                .orElse(folderMediaId);
        folderDto.setMediaItemId(folderMediaId);

        folderDto.setCreationTimeMilli(
                Optional.of(folder2Process.getCreationTime())
                        .orElseGet(() -> {
                            logger.warn("No creation time was received for {}. Setting 0.", folder2Process.getServerRelativeUrl());
                            return 0L;
                        }));
        folderDto.setModTimeMilli(
                Optional.of(folder2Process.getLastModifiedTime())
                        .orElseGet(() -> {
                            logger.warn("No last-modification time was received for {}. Setting 0.", folder2Process.getServerRelativeUrl());
                            return 0L;
                        }));
        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        folderDto.setFileName(SharePointParseUtils.applySiteMark(path, subSite));
        folderDto.setAclInheritanceType(aclInheritanceType);
        folderDto.setTitle(folder2Process.getListTitle());
        filePropertiesConsumer.accept(folderDto);
    }

    private AclInheritanceType getAclInheritanceType(String subSite, String folderPath, String listId, String itemId) {
        String strippedListId = SharePointParseUtils.splitMediaItemIdAndSite(listId).getListId();
        try {
            String tempItemId = itemId;
            SharePointListItem item = execAsyncTask(() -> microsoftDocAuthorityClient.getListItem(subSite, strippedListId, tempItemId));
            return item.isListItemHavingUniqueAcls() ? AclInheritanceType.NONE : AclInheritanceType.FOLDER;
        } catch (Exception e) {// TODO Oren: handle runtime/timeout excetions
            if (logger.isDebugEnabled()) {
                logger.debug("Failed to get folder-media-item-id for folder " + folderPath + ". using listId " + listId, e);
            } else {
                logger.warn("Failed to get folder-media-item-id for folder {}. using listId {}", folderPath, listId);
            }
        }
        return null;
    }

    protected int streamFilesAndSubFolders(String path,
                                           String listId,
                                           String subSite,
                                           ScanTaskParameters scanParams,
                                           Predicate<? super String> fileTypesPredicate,
                                           Consumer<ClaFilePropertiesDto> fileConsumer,
                                           Consumer<String> createScanTaskConsumer,
                                           Predicate<Pair<Long, Long>> scanActivePredicate,
                                           ProgressTracker filePropsProgressTracker) {


        if (scanActivePredicate != null && !scanActivePredicate.test(Pair.of(scanParams.getJobId(), scanParams.getJobStateIdentifier()))) {
            logger.info("scanActivePredicate returned false for job {} failed . Aborting", scanParams.getJobId());
            return 0;
        }
        String plainPath = SharePointParseUtils.splitPathAndSubsite(path).getPath();
        Long runId = scanParams.getRunId();
        String folderPath = convertFileNameIfNeeded(path);
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(folderPath);
        folderPath = key.getPath();
        if (MediaConnectorUtils.isFolderExcluded(plainPath, folderPath, scanParams.getExcludedRules(), scanParams.getSkipDirnames(), scanParams.getRunId())) {
            logger.debug("Path {} excluded - skipping", path);
            return 0;
        }
        processFolderData(runId, fileConsumer, plainPath, subSite, folderPath, listId);
        int totalFilesStreamed = 0;
        try {
            totalFilesStreamed = streamFiles(runId, folderPath, subSite, listId, fileTypesPredicate, fileConsumer);
        } catch (Exception e) {
            logger.error("Failed to stream files under folder {} ({})", path, folderPath);
            ScanErrorDto scanErrorDto = createScanError("Failed to stream files under folder " + path, e, path, scanParams.getRunId());
            fileConsumer.accept(ClaFilePropertiesDto.create()
                    .setFolder(true)
                    .addError(scanErrorDto));
        }

        // we want to call this every once in a while to make sure
        // we send scan progress even if we skip files
        filePropsProgressTracker.incrementProgress(0);

        if (!plainPath.endsWith("/")) {
            plainPath += "/";
        }
        List<ServerResourceDto> folders;

        try {
            folders = listFoldersUnderLibrary(plainPath, subSite); // TODO Oren: make pageable
        } catch (Exception e) {
            logger.error("Failed to list folders under path {}", path);
            ClaFilePropertiesDto errDto = ClaFilePropertiesDto.create()
                    .addError(createScanError("Failed to list folders under folder " + path, e, SharePointParseUtils.applySiteMark(plainPath, subSite), runId));
            fileConsumer.accept(errDto);
            folders = Lists.newArrayList();
        }

        for (ServerResourceDto folder : folders) {
            String seperator = path.endsWith("/") || folder.getName().startsWith("/") ? "" : "/";
            String tempPath = path + seperator + folder.getName();
            if (scanParams.isFstLevelOnly()) {
                createScanTaskConsumer.accept(tempPath);
                logger.trace("fst level scan only, ask for task creation for folder {}", tempPath);
            } else if (Optional.ofNullable(folder.getHasChildren()).orElse(true)) {
                totalFilesStreamed += streamFilesAndSubFolders(tempPath, listId, subSite, scanParams, fileTypesPredicate,
                        fileConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
            }
        }

        return totalFilesStreamed;
    }

    protected int streamFiles(Long runId, String path, String subSite, String listId, Predicate<? super String> fileTypesPredicate, Consumer<ClaFilePropertiesDto> fileConsumer) {
        logger.debug("Fetching files under {}", path);

        int scanCount = 0;
        int pageIndex = 0;
        List<ClaFilePropertiesDto> files;
        do {
            // TODO: opStateService.checkIfStopRequested();
            // retrieve items from SharePoint page by page and consume
            if (listId != null) {
                listId = SharePointParseUtils.splitMediaItemIdAndSite(listId).getListId();
            }
            try {
                files = listFilesUnderFolder(subSite, path, listId, pageSize, pageIndex);
                scanCount += files.size();
                files.stream()
                        .filter(dto -> fileTypesPredicate.test(dto.getFileName()))
                        .peek(dto -> {
                            if (subSite != null) {
                                dto.setFileName(SharePointParseUtils.applySiteMark(dto.getFileName(), subSite));//TODO Oren Remove when moving to media-item-id
                            }
                        })
                        .forEach(fileConsumer);
            } catch (AccessControlException e) {
                logger.error("Failed to list files " + (pageIndex * pageSize) + " to " + ((pageIndex + 1) * pageSize - 1) + " under path " + path, e);
                throw e;
            } catch (Exception e) {
                logger.error("Failed to list files " + (pageIndex * pageSize) + " to " + ((pageIndex + 1) * pageSize - 1) + " under path " + path + ", skipping to next batch", e);
                ClaFilePropertiesDto errDto = ClaFilePropertiesDto.create().addError(
                        createScanError("Error while streaming files (page=" + pageIndex + ") under folder " + path, e, SharePointParseUtils.applySiteMark(path, subSite), runId));
                fileConsumer.accept(errDto);
                files = Lists.newArrayList();
            }
            pageIndex++;
        } while (files.size() == pageSize);

        return scanCount;
    }

    private List<ClaFilePropertiesDto> listFilesUnderFolder(String subSite, String folder, String listId, int pageSize, int pageIdx) {
        logger.debug("Listing files under {} (page index: {}, page size: {})", folder, pageIdx, pageSize);
        String folderName = convertFileNameIfNeeded(folder);
        try {
            return execAsyncTask(() -> microsoftDocAuthorityClient.getFilesWithMediaItemId(
                    subSite,
                    folderName,
                    listId,
                    pageIdx,
                    pageSize));
        } catch (Exception e) {
            logger.error("Failed to get files", e);
            if (e.getMessage() != null && e.getMessage().contains("403")) {
                throw new AccessControlException("Failed to list all files under folder " + folder + " (page index: " + pageIdx + ", page size: " + pageSize + ") probably throttle control");
            } else {
                throw new RuntimeException("Failed to list all files under folder " + folder, e);
            }
        }
    }

    @Override
    public List<ServerResourceDto> testConnection() {
        logger.debug("Testing connection for user {} under domain {}", userName, domain);
        try {
            return testConnectionImpl();
        } catch (Exception e) {
            logger.info("Connection test failed by :{} ({})", e.getMessage());
            if (e.getMessage() != null) {
                switch (e.getMessage()) {
                    case "401 Unauthorized":
                        throw new MediaConnectionException("Connection test failed: User Unauthorized", BadRequestType.TEST_FAILED);
                    case "403 Forbidden":
                        throw new MediaConnectionException("Connection test failed: User Forbidden", BadRequestType.TEST_FAILED);
                    case "404 Not Found":
                        throw new MediaConnectionException("Connection test failed: URL not found", BadRequestType.TEST_FAILED);
                }
            }
            throw new MediaConnectionException("Connection test failed: " + e.getMessage(), BadRequestType.TEST_FAILED);
        }
    }

    protected List<ServerResourceDto> testConnectionImpl() throws InterruptedException, FileNotFoundException, TimeoutException, ExecutionException {
        execAsyncTask(() -> service.getCurrentUser());
        return Lists.newArrayList();
    }

    protected List<ServerResourceDto> listFoldersUnderLibrary(String libraryFullPath, String subSite) {
        return listFoldersUnderLibrary(libraryFullPath, subSite, true);
    }

    private List<ServerResourceDto> listFoldersUnderLibrary(String libraryFullPath, String subSite, boolean isFirstAttempt) {
        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/small library
        logger.debug("List folders under library: {}, sub site: {}", libraryFullPath, subSite);
        if (!libraryFullPath.endsWith("/")) {
            libraryFullPath += "/";
        }
        try {
            String pathSubset = convertFileNameIfNeeded(libraryFullPath);
            List<Folder> folders = execAsyncTask(() -> service.getFolders(subSite, pathSubset));
            logger.trace("Retrieved {} folders under: {}", folders.size(), libraryFullPath);
            List<ServerResourceDto> result = new ArrayList<>();
            for (Folder folder : folders) {
                String name = folder.getName();
                if (isFolderFiltered(name)) {
                    //Internal folder
                    logger.trace("{} is irrelevant folder", name);
                    continue;
                }
                String fullName = libraryFullPath + name;
                ServerResourceDto serverResourceDto = new ServerResourceDto(fullName, name);
                serverResourceDto.setHasChildren(folder.getItemCount() > 0);
                serverResourceDto.setType(ServerResourceType.FOLDER);
                result.add(serverResourceDto);
            }

            return result;
        } catch (TimeoutException e) {
            if (isFirstAttempt) {
                logger.warn("Failed to list folders under " + libraryFullPath + ", retrying", e);
                suspendTimeoutRetryInterval();
                return listFoldersUnderLibrary(libraryFullPath, subSite, false);
            }
            logger.error("Failed to list folders under " + libraryFullPath, e);
            throw createTimeoutAccessControlException(e, "Failed to list folders under library " + libraryFullPath);
        } catch (Exception e) {
            logger.error("Failed to list folders at {} under sharePoint connector", libraryFullPath, e);
            if (e.getMessage().contains("403")) {
                throw new AccessControlException("Failed to list folders under library " + libraryFullPath + ": (" + e.getMessage() + ") probably throttle control");
            } else {
                throw new MediaConnectionException("Failed to list folders at " + libraryFullPath + " under sharePoint: " + e.getMessage(), BadRequestType.OPERATION_FAILED);
            }
        }
    }

    protected boolean isFolderFiltered(String folderName) {
        return StringUtils.isEmpty(folderName)
                || folderName.startsWith("_")
                || folderName.startsWith("/_")
                || foldersToFilter.contains(folderName.toLowerCase());
    }

    @Override
    public ClaFilePropertiesDto getFileAttributes(String mediaItemId) throws FileNotFoundException {
        return getFileAttributes(mediaItemId, true);
    }

    protected ClaFilePropertiesDto getFileAttributes(String mediaItemId, boolean isFirstAttempt) throws FileNotFoundException {
        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId);
        SharePointListItem listItem;
        ClaFilePropertiesDto attr;
        String fileName = null;
        String convertedFileName = null;
        String subSite = key.getSite();
        try {
            listItem = Optional.ofNullable(execAsyncTask(() -> microsoftDocAuthorityClient.getListItem(key)))
                    .orElseThrow(() -> new FileNotFoundException("No item with id " + mediaItemId));

            fileName = listItem.getFileRef();
            final String temp = convertedFileName = convertFileNameIfNeeded(fileName);
            attr = execAsyncTask(() -> microsoftDocAuthorityClient.getFilesAttributes(subSite, temp));
            if (attr == null) {
                throw new FileNotFoundException("File not found");
            }
            attr.setMediaItemId(mediaItemId);
            attr.setFileName(fileName);
            final String tempConvertedFileName = convertedFileName;
            String owner = Optional.ofNullable(attr.getOwnerName())
                    .orElseGet(() -> getOwner(subSite, tempConvertedFileName));
            owner = Optional.ofNullable(owner)
                    .map(SharePointParseUtils::convertLoginName)
                    .orElse(UNKNOWN_USER);
            attr.setOwnerName(SharePointParseUtils.convertLoginName(owner));
            attr.setType(FileTypeUtils.getFileType(attr.getFileName()));

        } catch (FileNotFoundException e) {
            throw e;
        } catch (TimeoutException te) {
            if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                return getFileAttributes(mediaItemId, false);
            }
            throw new AccessControlException("Failed to obtain file attributes: " + te.getMessage());

        } catch (Exception e) {
            logger.error("Failed to get file {} attributes from sharePoint", fileName, e);
            if (e.getMessage().contains("403")) {
                throw new AccessControlException("Failed to get file " + fileName + " attributes from sharePoint: (" + e.getMessage() + ") probably throttle control");
            } else {
//                throw new FileNotFoundException("Failed to get file " + fileName + " attributes from sharePoint: " + e.getMessage());
                attr = ClaFilePropertiesDto.create();
                attr.setFileName(fileName);
            }
        }

        if (Optional.ofNullable(convertedFileName).isPresent()) {
            attr.setAclInheritanceType(getFileInheritanceType(subSite, convertedFileName, true));
        }
        fetchAcls(attr);
        return attr;
    }

    private String getOwner(String subSite, String convertedFileName) {
        try {
            return execAsyncTask(() -> microsoftDocAuthorityClient.getFileOwner(subSite, convertedFileName));
        } catch (Exception e) {
            logger.error("Failed to get file's owner", e);
        }
        return null;
    }

    private AclInheritanceType getFileInheritanceType(String subSite, String convertedFileName, boolean isFirstAttempt) {
        try {
            return execAsyncTask(() -> microsoftDocAuthorityClient.getFileAclInheritanceType(subSite, convertedFileName));
        } catch (TimeoutException e) {
            if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                return getFileInheritanceType(subSite, convertedFileName, false);
            }
//            throw new AccessControlException("Failed to obtain file's inheritance type: " + e.getMessage());
        } catch (Exception e) {
            logger.error("Failed to obtain file's inheritance type: " + convertedFileName, e);
        }
        return null;
    }

    private AccessControlException createTimeoutAccessControlException(TimeoutException te, String s) {
        String msg = Optional.ofNullable(te.getMessage())
                .orElse("Request timeout");
        throw new AccessControlException(s + ": " + msg);
    }

    @SuppressWarnings("unused")
    private User getFileAuthor(String subSite, String fileName/*, boolean isFirstAttempt*/) {
        try {
            return execAsyncTask(() -> service.getFileAuthor(subSite, fileName));
        } catch (TimeoutException te) {
            /*if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                return getFileAuthor(fileName, false);
            }*/
            throw new AccessControlException("Failed to obtain file attributes: " + te.getMessage());
        } catch (InterruptedException | FileNotFoundException | ExecutionException e) {
            logger.error("Failed to get file {} attributes from sharePoint", fileName, e);
            throw new RuntimeException("Failed to get file " + fileName + " attributes from sharePoint " + e.getMessage());
        }
    }

    public ClaFilePropertiesDto getMediaItemAttributes(@NotNull String mediaItemId) {
        return getMediaItemAttributesWithTimeout(mediaItemId, true);
    }

    private ClaFilePropertiesDto getMediaItemAttributesWithTimeout(@NotNull String mediaItemId, boolean isFirstAttempt) {
        if (mediaItemId == null) {
            throw new RuntimeException("null media item ID - can't get media item attributes");
        }

        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId);
        try {
            if (key.getItemId() != null) {
                SharePointListItem listItem = execAsyncTask(() -> microsoftDocAuthorityClient.getListItem(key));
                if (listItem == null) {
                    return null; //File was not found in the server
                }
                return convertToClaFilePropertiesDto(key.getSite(), key.getListId(), listItem);
            } else if (key.getListId().startsWith("site:")) {
                logger.warn("For now - fetching media item attributes from site is not supported");
                ClaFilePropertiesDto claFilePropertiesDto = ClaFilePropertiesDto.create();
                claFilePropertiesDto.setMediaItemId(mediaItemId);
                claFilePropertiesDto.setFolder(true);
                return claFilePropertiesDto;
            } else {
                ClaFilePropertiesDto claFilePropertiesDto = ClaFilePropertiesDto.create();
                claFilePropertiesDto.setMediaItemId(mediaItemId);
                com.independentsoft.share.List sharePointList = execAsyncTask(() -> service.getList(key.getSite(), key.getListId()));
                String libraryBasePath = extractLibraryBasePath(key.getSite(), sharePointList);
                String siteUrlPart = Optional.ofNullable(key.getSite())
                        .map(val -> val + "/")
                        .orElse("");
                String fullName = SharePointParseUtils.removeUnneededDoubleSlashes(createBaseUri(true) + "/" + siteUrlPart + libraryBasePath);
                claFilePropertiesDto.setFileName(fullName);
                claFilePropertiesDto.setFolder(true);
                return claFilePropertiesDto;
            }
        } catch (TimeoutException te) {
            if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                return getMediaItemAttributesWithTimeout(mediaItemId, false);
            }

            throw createTimeoutAccessControlException(te, "Failed to obtain file content stream");
        } catch (Exception e) {
            logger.error("Failed to get sharePoint list item properties by MediaItemId {}", mediaItemId, e);
            /*resetService();
            throw new RuntimeException("Failed to get sharePoint list item properties by MediaItemId " + mediaItemId, e);*/
            ClaFilePropertiesDto dto = ClaFilePropertiesDto.create()
                    .setFolder(mediaItemId.contains("/"));
            dto.setMediaItemId(mediaItemId);
            return dto;
        }
    }

    protected ClaFilePropertiesDto convertToClaFilePropertiesDto(String subSite, String listId, SharePointListItem listItem) {
        ClaFilePropertiesDto filePropertyDto = ClaFilePropertiesDto.create();
        enrichClaFileProperties(subSite, listId, listItem, filePropertyDto);
        return filePropertyDto;
    }

    private MediaChangeLogDto convertToMediaChangeLogDto(String subSite, String listId, String listItemId, SharePointListItem listItem, DiffType diff) {
        logger.trace("Converting changelog to dto (list {} listItemId {})", listId, listItemId);
        MediaChangeLogDto mediaChangeLogDto = MediaChangeLogDto.create();
        mediaChangeLogDto.setEventType(diff);
        if (DiffType.DELETED.equals(diff)) {
            mediaChangeLogDto.setMediaItemId(SharePointParseUtils.calculateMediaItemId(subSite, listId, listItemId));
        } else {
            enrichClaFileProperties(subSite, listId, listItem, mediaChangeLogDto);
        }
        logger.trace("Received diff type {} for li {}", diff, listItem == null ? null : listItem.getFileRef());
        return mediaChangeLogDto;
    }

    private void enrichClaFileProperties(String subSite, String listId, SharePointListItem listItem, ClaFilePropertiesDto filePropertyDto) {
        String filePath = microsoftDocAuthorityClient.convertFileRefToFileUrl(listItem.getFileRef());
        filePath = SharePointParseUtils.applySiteMark(filePath, subSite);
        filePropertyDto.setFileName(filePath);
        String listItemId = listItem.getId();
        filePropertyDto.setMediaItemId(SharePointParseUtils.calculateMediaItemId(subSite, listId, listItemId));
        AclInheritanceType aclInheritanceType = listItem.isListItemHavingUniqueAcls() ? AclInheritanceType.NONE : AclInheritanceType.FOLDER;
        filePropertyDto.setAclInheritanceType(aclInheritanceType);
        if (FileSystemObjectType.FOLDER.equals(listItem.getFileSystemObjectType())) {
            filePropertyDto.setFolder(true);
        }
        if (listItem.getModified() != null) {
            long modTimeMill = SharePointParseUtils.convertDateTimeToMilliseconds(listItem.getModified());
            filePropertyDto.setModTimeMilli(modTimeMill);
        }
//        if (listItem.getAccess() != null) { // Todo: support access time
//            long accessTimeMill = SharePointParseUtils.convertDateTimeToMilliseconds(listItem.getAccess());
//            filePropertyDto.setAccessTimeMilli(accessTimeMill);
//        }
        String owner = Optional.ofNullable(listItem.getLoginName())
                .map(SharePointParseUtils::convertLoginName)
                .orElse(UNKNOWN_USER);
        filePropertyDto.setOwnerName(owner);
        filePropertyDto.setFileSize(listItem.getSize());
    }

    @Override
    public InputStream getInputStream(ClaFilePropertiesDto props) throws FileNotFoundException {
        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(props.getMediaItemId());
        return getInputStreamForFileName(props.getFileName(), key.getSite(), true);
    }

    protected InputStream getInputStreamForMediaItemId(String mediaItemId) throws FileNotFoundException {
        String fileName;
        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId);
        try {
            fileName = getFileRef(mediaItemId);
        } catch (FileNotFoundException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Failed to resolve file-reference for item id: " + mediaItemId, e);
            throw new FileNotFoundException(e.getMessage());
        }
        return getInputStreamForFileName(fileName, key.getSite(), true);

    }

    @Override
    public FileContentDto getFileContent(String mediaItemId, boolean forUserDownload) throws FileNotFoundException {
        return getFileContentInner(mediaItemId, true, forUserDownload);
    }

    protected FileContentDto getFileContentInner(String mediaItemId, boolean isFirstAttempt, boolean forUserDownload)
            throws FileNotFoundException {

        String fileName = null;
        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId);
        Long length;
        try {
            final String temp = fileName = getFileRef(mediaItemId);
            length = execAsyncTask(() -> service.getFile(key.getSite(), temp).getLength());
            logger.debug("File {} is {} bytes long", fileName, length);
            if (length > maxFileSize) {
                String err = String.format("File %s is too big (%d bytes); max supported size is up to %d bytes",
                        fileName, length, maxFileSize);
                throw new IllegalArgumentException(err);
            }
        } catch (RuntimeException e) {
            throw e;
        } catch (TimeoutException te) {
            if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                return getFileContentInner(mediaItemId, false, forUserDownload);
            }
            throw createTimeoutAccessControlException(te, "Failed to obtain file content stream");
        } catch (Exception e) {
            String resolvedFileName = Optional.ofNullable(fileName)
                    .orElse("Couldn't resolve filename for item-id " + mediaItemId);
            logger.warn("Failed to get file InputStream from SharePoint item id {} ( fileName {})", mediaItemId, resolvedFileName, e);
            if (e.getMessage().contains("403")) {
                throw new AccessControlException("Failed to get file InputStream from SharePoint file " + fileName + " (" + e.getMessage() + ") probably throttle control");
            } else {
//                throw new FileNotFoundException("Failed to get file InputStream from SharePoint file " + fileName + " (" + e.getMessage() + ")");
                logger.info("Setting file length as 0");
                length = 0L;
            }
        }

        try {
            InputStream inputStream = new ByteArrayInputStream(IOUtils.toByteArray(getInputStreamForFileName(fileName, key.getSite(), true)));
            return new FileContentDto(inputStream, length);
        } catch (Exception e) {
            logger.error("Couldn't resolve input-stream for subsite=" + key.getSite() + " path=" + key.getPath(), e);
            throw new FileNotFoundException("Couldn't resolve input-stream for " + fileName);
        }
    }

    private String getFileRef(String mediaItemId) throws FileNotFoundException, InterruptedException, ExecutionException, TimeoutException {
        return Optional.ofNullable(
                execAsyncTask(() -> microsoftDocAuthorityClient.getListItem(SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId))))
                .map(SharePointListItem::getFileRef)
                .orElseThrow(() -> new FileNotFoundException("No item with id " + mediaItemId));
    }

    protected InputStream getInputStreamForFileName(String fileName, String subSite, boolean isFirstAttempt) throws FileNotFoundException {
        final String parsedFileName = convertFileNameIfNeeded(fileName);
        logger.debug("Getting file input stream for file={} (basePath={}, subSite={})", fileName, basePath, subSite);
        logger.debug("Attempting to obtain input-stream for {}", parsedFileName);
        try (InputStream inputStream = execAsyncTask(() -> service.getFileStream(subSite, parsedFileName))) {
            InputStream output = new ByteArrayInputStream(IOUtils.toByteArray(inputStream));
            logger.debug("Input-stream for {} obtained successfully", fileName);
            return output;
        } catch (TimeoutException te) {
            if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                return getInputStreamForFileName(fileName, subSite, false);
            }
            throw createTimeoutAccessControlException(te, "Failed to obtain file input stream: ");
        } catch (Exception e) {
            logger.error("Failed to open input-stream on microsoft file {}", fileName, e);
            if (e.getMessage().contains("403")) {
                throw new AccessControlException("Failed to get file InputStream from microsoft file " + fileName + " (" + e.getMessage() + ") probably throttle control");
            } else {
                throw new FileNotFoundException("Failed to open stream to file " + fileName);
            }
        }
    }

    @Override
    public void fetchAcls(ClaFilePropertiesDto claFileProp) {
        String mediaItemId = claFileProp.getMediaItemId();
        MSItemKey key = SharePointParseUtils.splitMediaItemIdAndSite(mediaItemId);
        try {
            if (key.getItemId() != null) {
                //ListItem
                List<SharePointRoleAssignment> listItemPermissions = execAsyncTask(() -> microsoftDocAuthorityClient.getListItemPermissions(key));
                addListItemPermissionsToClaFileProperties(claFileProp, listItemPermissions);
                logger.debug("Fetched r:{},w:{} ACLs for {}", claFileProp.getAclReadAllowed().size(), claFileProp.getAclWriteAllowed().size(), mediaItemId);
            } else if (mediaItemId.startsWith("site:")) {
                logger.debug("For now - fetching ACL's from site is not supported");
            } else {
                List<SharePointRoleAssignment> listPermissions = execAsyncTask(() -> microsoftDocAuthorityClient.getListPermissions(key.getSite(), key.getListId()));
                logger.debug("List: {} ) has {} permissions", claFileProp, listPermissions.size());
                addListItemPermissionsToClaFileProperties(claFileProp, listPermissions);
            }
            claFileProp.calculateAclSignature();
        } catch (Exception e) {
            logger.error("Failed to fetch ACL's from SharePoint list item: {}", mediaItemId, e);
        }
    }

    protected void addListItemPermissionsToClaFileProperties(ClaFilePropertiesDto claFileProp, List<SharePointRoleAssignment> listItemPermissions) {
        for (SharePointRoleAssignment listItemPermission : listItemPermissions) {
            String loginName = listItemPermission.getLoginName();
            switch (listItemPermission.getAclType()) {
                case WRITE_TYPE:
                    claFileProp.getAclWriteAllowed().add(loginName);
                case READ_TYPE:
                    claFileProp.getAclReadAllowed().add(loginName);
                    break;
                case DENY_READ_TYPE:
                    claFileProp.getAclReadDenied().add(loginName);
                case DENY_WRITE_TYPE:
                    claFileProp.getAclWriteDenied().add(loginName);
                    break;
            }
        }

        Optional.ofNullable(claFileProp.getOwnerName())
                .map(SharePointParseUtils::convertLoginName)
                .ifPresent(owner -> {
                    if (!claFileProp.getAclWriteAllowed().contains(owner)) {
                        claFileProp.getAclWriteAllowed().add(owner);
                    }
                    if (!claFileProp.getAclReadAllowed().contains(owner)) {
                        claFileProp.getAclReadAllowed().add(owner);
                    }
                });
    }

    @Override
    public String getIdentifier() {
        return String.valueOf(this.hashCode());
    }

    protected void streamMediaChangeLogForSite(ScanTaskParameters scanParams, String realPath, String subSite, long startChangeLogPosition, Consumer<MediaChangeLogDto> changeConsumer) {
        logger.debug("Streaming change log: getting changes under {}", realPath);
        List<Change> changes;
        try {
            startChangeLogPosition = scanParams.getSyncState() == null ? startChangeLogPosition :
                    Long.valueOf(scanParams.getSyncState());
            changes = getChangeLog(startChangeLogPosition, subSite, true);
            // Every time set the correct sync state (which is the max time)
            changes.stream()
                    .map(Change::getTime)
                    .mapToLong(Date::getTime)
                    .max()
                    .ifPresent(maxTime -> {
                        logger.debug("Streaming change log: Changed max time to {} for path {}", maxTime, realPath);
                        scanParams.setSyncState(String.valueOf(maxTime));
                    });
        } catch (Exception e) {
            logger.error("Failed to get change-log for path: {} and subSite: {}", realPath, subSite);
            ScanErrorDto errorDto = createScanError("Failed to stream change-log", e, realPath, scanParams.getRunId());
            MediaChangeLogDto changeLogDto = (MediaChangeLogDto) MediaChangeLogDto.create().addError(errorDto);
            changeLogDto.setChangeLogPosition(Long.toString(startChangeLogPosition));
            changeConsumer.accept(changeLogDto);
            return; // Since changes are fetched for site, on not found, there's no need to attempt crawling sub-sites.
        }

        logger.debug("Streaming change log: got total of {} changes", changes.size());
        String temp = convertFileNameIfNeeded(realPath);
        if (!temp.endsWith("/")) {
            temp += "/";
        }
        final String rootFolderPrefix = SharePointParseUtils.splitPathAndSubsite(temp).getPath().toLowerCase();
        final Predicate<String> isBelongedToRootFolder = (itemPath -> itemPath.toLowerCase().startsWith(rootFolderPrefix));

        Consumer<ClaFilePropertiesDto> logConsumerWrapper = getChangeLogConsumerWrapper(changeConsumer);
        long changeTimeToCompare = startChangeLogPosition;
        List<MediaChangeLogDto> changeItems = changes.stream()
                .filter(item -> item.getTime().getTime() < scanParams.getStartScanTime() &&
                        item.getTime().getTime() > changeTimeToCompare)
                .map(item -> (ChangeItem) item)
                .map(item -> convertChangeItemToMediaChangeLogDto(subSite, item, isBelongedToRootFolder, true))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        logger.debug("Streaming change log: filtered to {} relevant changes ", changeItems.size());

        changeItems.forEach(changeConsumer);

       /* changeItems.stream()
                .filter(changedFoldersOnly())
                .forEach(folder -> streamMediaItems(
                        scanParams.copy()
                                .setRootFolderMediaEntityId(folder.getMediaItemId())
                                .setPath(folder.getFileName()),
                        logConsumerWrapper, dlp -> {
                        }, s -> {
                        }, null, dummyTracker));*/

        streamSubSitesChanges(scanParams, realPath, subSite, startChangeLogPosition, changeConsumer, isBelongedToRootFolder);
    }

    private Predicate<? super MediaChangeLogDto> changedFoldersOnly() {
        return change -> change.isFolder() && DiffType.RENAMED.equals(change.getEventType());
    }

    private void streamSubSitesChanges(ScanTaskParameters scanParams,
                                       String realPath,
                                       String subSite,
                                       long startChangeLogPosition,
                                       Consumer<MediaChangeLogDto> changeConsumer,
                                       Predicate<String> isBelongedToRootFolder) {

        List<ServerResourceDto> subSites = listSubSites(MSItemKey.path(subSite, realPath), basePath);
        String basePathLookup = ((StringUtils.isEmpty(basePath) || "/".equals(basePath)) ? url : basePath)
                .toLowerCase();

        boolean shouldCrawl = false;
        if (subSites.size() > 0) {
            String sitePath = extractSubSiteRelativePath(subSites.get(0), basePathLookup);
            shouldCrawl = isBelongedToRootFolder.test(SharePointParseUtils.normalizePath(basePath + sitePath) + "/");
        }

        if (shouldCrawl) {
            subSites.stream()
                    .map(dto -> extractSubSiteRelativePath(dto, basePathLookup))
                    .map(subSiteToCrawl -> subSiteToCrawl.substring(1))
                    .forEach(subSiteToCrawl -> streamMediaChangeLogForSite(scanParams, realPath, subSiteToCrawl, startChangeLogPosition, changeConsumer));
        }
    }

    private String extractSubSiteRelativePath(ServerResourceDto resourceDto, String basePathLookup) {
        String subSitePath = resourceDto.getFullName().toLowerCase();
        subSitePath = SharePointParseUtils.splitPathAndSubsite(subSitePath).getPath();
        subSitePath = subSitePath.substring(subSitePath.toLowerCase().indexOf(basePathLookup) + basePathLookup.length());
        return SharePointParseUtils.normalizePath(subSitePath);
    }

    private MediaChangeLogDto convertChangeItemToMediaChangeLogDto(String subSite, ChangeItem item, Predicate<String> isBelongedToRootFolder, boolean isFirstAttempt) {
        try {
            String itemId = Integer.toString(item.getItemId());
            DiffType diff = SharePointParseUtils.convertToDiffType(item.getType());
            if (!DiffType.DELETED.equals(diff)) {
                logger.trace("Retrieving changed-list-item data for item: {} under subSite: {}", itemId, subSite);
                SharePointListItem li = execAsyncTask(() -> microsoftDocAuthorityClient.getListItem(
                        subSite, item.getListId(), itemId));

                if (li == null) {
                    logger.info("Sharepoint list-item not found for itemId: {} and listId: {}, dropping change {}",
                            diff, itemId, item.getListId());
                    return null;
                }

                if (diff == DiffType.CONTENT_UPDATED && li.getFileSystemObjectType() == FileSystemObjectType.FOLDER) {
                    logger.info("Not relevant change {} for {}, dropping it", diff, li);
                    return null;
                }

                if (DiffType.RENAMED.equals(diff) || isBelongedToRootFolder.test(li.getFileRef())) {
                    logger.info("Preparing Change {} for file {}", diff, li.getFileRef());
                    return convertToMediaChangeLogDto(subSite, item.getListId(), itemId, li, diff);
                }

                logger.info("Not relevant change {} for {}, dropping it", diff, li.getFileRef());
                return null;
            }

            logger.trace("Preparing {} change for itemId: {}", diff, item.getItemId());
            return convertToMediaChangeLogDto(subSite, item.getListId(), itemId, null, diff);
        } catch (TimeoutException e) {
            if (isFirstAttempt) {
                suspendTimeoutRetryInterval();
                convertChangeItemToMediaChangeLogDto(subSite, item, isBelongedToRootFolder, false);
            }
            throw createTimeoutAccessControlException(e, "Failed to stream change log");
        } catch (Exception e) {
            logger.error("Failed to retrieve list item (list: {}, item-id: {})", item.getListId(), item.getItemId());
            String msg = Optional.ofNullable(e.getMessage()).orElse("");
            if (msg.contains("403") || msg.contains("429")) {
                throw new AccessControlException("Failed to stream change log: (" + e.getMessage() + ") probably throttle control");
            } else {
                throw new RuntimeException("Failed to stream change log: (" + e.getMessage() + ")");
            }
        }
    }

    @NotNull
    private List<Change> getChangeLog(long startChangeLogPosition, String subSite, boolean isFirstAttempt) throws FileNotFoundException {
        logger.info("Acquiring change log");
        ChangeQuery query = new ChangeQuery();
        query.setItem(true);
        query.setDelete(true);
        query.setMove(true);
        query.setAdd(true);
        query.setUpdate(true);
        query.setRestore(true);
        query.setRename(true);
        query.setRoleAssignmentAdd(true);
        query.setRoleAssignmentDelete(true);

        // TODO oren: limit size and use paging

        try {
            logger.trace("Retrieving site id");
            String siteId = execAsyncTask(() -> service.getSite(subSite).getId());
            logger.debug("Retrieved site id {}", siteId);
            query.setChangeTokenStart(new ChangeToken(ChangeTokenScope.SITE, siteId, new Date(startChangeLogPosition)));
            logger.debug("Fetching changes beginning from {}", query.getChangeTokenStart().getChangeTime());
            return execAsyncTask(() -> service.getChanges(subSite, query));
        } catch (TimeoutException te) {
            if (isFirstAttempt) {
                logger.warn("Failed to retrieve MS change-list - retrying");
                suspendTimeoutRetryInterval();
                return getChangeLog(startChangeLogPosition, subSite, false);
            }
            throw createTimeoutAccessControlException(te, "Failed to stream change log");
        } catch (Exception e) {
            logger.error("Failed to get MS change-list items", e);
            if (e.getMessage().contains("403") || e.getMessage().contains("429")) {
                throw new AccessControlException("Failed to stream change log: (" + e.getMessage() + ") probably throttle control (subSite=" + subSite + ", basePath=" + basePath + ")");
            } else if (e.getMessage().contains("404")) {
                throw new FileNotFoundException("Failed to fetch changes for site " + url + ", subSite=" + subSite);
            } else {
                throw new RuntimeException("Failed to stream change log: (" + e.getMessage() + ")(subSite=" + subSite + ", basePath=" + basePath + ")");
            }
        }
    }

    @Override
    public void concurrentStreamMediaChangeLog(
            ForkJoinPool forkJoinPool, ScanTaskParameters scanParams, String realPath, Long runId,
            long startChangeLogPosition, Consumer<MediaChangeLogDto> changeConsumer,
            Consumer<String> createScanTaskConsumer, Predicate<? super String> fileTypesPredicate) {

        logger.debug("Streaming (concurrent - not supported) change log for items under {}", realPath);
        streamMediaChangeLog(
                scanParams, realPath, runId, startChangeLogPosition, changeConsumer, createScanTaskConsumer,
                fileTypesPredicate);
    }

    protected <T> T execAsyncTask(Callable<T> task)
            throws InterruptedException, FileNotFoundException, ExecutionException, TimeoutException {

        regulate();

        int refNum = clientCallCounter.getAndIncrement();
        logger.debug("execAsyncTask: Before executing async call (outer) - refNum: {}", refNum);

        Callable<T> wrapper = () -> {
            logger.debug("execAsyncTask: Before executing async call (inner) - refNum: {}", refNum);
            T result = task.call();
            logger.debug("execAsyncTask: After executing async call (inner) - refNum: {}", refNum);
            return result;
        };

        Future<T> future = resourceExecutor.submit(wrapper);
        try {
            logger.debug("execAsyncTask: Waiting for async execution to end (outer) - refNum: {}", refNum);
            T result = future.get(connectionConfig.getServiceClientTimeout(), TimeUnit.MILLISECONDS);
            logger.debug("execAsyncTask: Async call execution done (outer) - refNum: {}", refNum);
            return result;
        } catch (TimeoutException e) {
            if (logger.isTraceEnabled()) {
                logger.trace("execAsyncTask: Async task execution was aborted due timeout ({}), refNum={}, trace={}",
                        connectionConfig.getServiceClientTimeout(),
                        refNum,
                        Arrays.toString(Thread.currentThread().getStackTrace()));
            }
            future.cancel(true);
            throw e;
        } catch (ExecutionException e) {
            // ExecutionException wraps the actual exception since we run in a thread
            // in itself not interesting, inner exception is the real issue
            if (e.getCause() instanceof FileNotFoundException) {
                throw (FileNotFoundException) e.getCause();
            } else if (e.getCause() instanceof RuntimeException) {
                throw (RuntimeException)e.getCause();
            } else {
                throw e;
            }
        }
    }

    @SuppressWarnings("unused")
    private Stream<MediaChangeLogDto> getChangesInFolder(String folder, String fullPath, long fromTime) {
        int pageIdx = 0;
        List<MediaChangeLogDto> changesInFolder = Lists.newArrayList();
        List<MediaChangeLogDto> batchChanges;
        do {
            try {
                batchChanges = microsoftDocAuthorityClient.getChangeLogForFolder(folder, fromTime, pageIdx, pageSize);
                changesInFolder.addAll(batchChanges);
            } catch (ServiceException e) {
                logger.error("Failed to retrieve data from change log for folder {}", fullPath);
                throw new RuntimeException("Failed to stream change log for folder " + fullPath, e);
            }
            pageIdx++;
        } while (batchChanges.size() == pageSize);
        logger.debug("Received {} changed items under {}", changesInFolder.size(), fullPath);

        changesInFolder.stream()
                .filter(chg -> chg.getEventType() == DiffType.NEW || chg.getEventType() == DiffType.RENAMED)
                .forEach(chg -> {
                    try {
                        microsoftDocAuthorityClient.enrichMediaChangeLogWithListItemData(chg);
                    } catch (ServiceException | FileNotFoundException e) {
                        logger.error("Failed to retrieve metadata for media with item id: {}", chg.getMediaItemId(), e);
                        throw new RuntimeException("Failed to retrieve metadata for media with item id: " + chg.getMediaItemId(), e);
                    }
                });
        return changesInFolder.stream();
    }

    protected List<ServerResourceDto> listCoreSite(String fullName, String name) {
        logger.trace("Listing core site (fullname: {}, name: {})", fullName, name);
        ServerResourceDto coreSite = new ServerResourceDto(fullName, name);
        coreSite.setHasChildren(true);
        coreSite.setAccessible(true);
        coreSite.setType(ServerResourceType.SITE);
        return Lists.newArrayList(coreSite);
    }

    protected List<ServerResourceDto> listFilesAsServerResources(String subSite, String path, int retries) {
        try {
            List<File> files = execAsyncTask(() -> service.getFiles(subSite, path));
            List<ServerResourceDto> result = new ArrayList<>();
            for (File file : files) {
                String fullName = path + "/" + file.getName();
                ServerResourceDto e = new ServerResourceDto(fullName, file.getName());
                e.setType(ServerResourceType.FILE);
                e.setHasChildren(false);
                result.add(e);
            }
            return result;
        } catch (Exception e) {
            if (retries > 0) {
                logger.error("Failed to list files under sharePoint connector at path {} - retrying", path, e);
                return listFilesAsServerResources(subSite, path, retries - 1);
            }
            logger.error("Failed to list files under sharePoint connector at path {}", path, e);
            throw new RuntimeException("Failed to list files under sharePoint connector: " + e.getMessage());
        }
    }

    protected ScanErrorDto createScanError(String text, Exception ex, String path, Long runId) {
        String exceptionText = Converters.getExceptionText(ex);
        ScanErrorDto scanErrorDto = ScanErrorDto.create(ScanErrorDto.ErrorSeverity.CRITICAL);
        scanErrorDto.setRunId(runId)
                .setPath(path)
                .setExceptionText(exceptionText)
                .setText(text);
        return scanErrorDto;
    }

    protected abstract String convertFileNameIfNeeded(String folder);

    protected abstract String extractLibraryBasePath(String subSite, com.independentsoft.share.List list);

    protected abstract String createBaseUri(boolean includeBasePath);

    /**
     * @param path - includes the basePath (/test/basic)
     * @return list ID
     * @throws FileNotFoundException when path doesn't exist
     */
    public SharePointExtendedFolder getFolderProperties(String subSite, String path) throws FileNotFoundException {
        return getFolderProperties(subSite, path, true);
    }

    private SharePointExtendedFolder getFolderProperties(String subSite, String path, boolean isFirstAttempt) throws FileNotFoundException {
        try {
            return execAsyncTask(() -> microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(subSite, path));
        } catch (TimeoutException e) {
            if (isFirstAttempt) {
                logger.warn("Attempt to retrieve folder {} failed - retrying", path);
                return getFolderProperties(subSite, path, false);
            }
//            throw createTimeoutAccessControlException(te, "Failed to process top folder data");
            logger.error("Failed to process top folder data.", e);
            throw new AccessControlException(e.getMessage());
            //throw new AccessControlException("Failed to process top folder data: " + te.getMessage());
        } catch (Exception e) {
            if (FailAccessException.class.isInstance(e) || DataAccessException.class.isInstance(e)) {
                logger.error("Failed to process top folder data.", e);
                throw (RuntimeException)e;
            } else {
                throw new FileNotFoundException("Failed to find path [" + path + "] due to error [" + e.getMessage() + "]");
            }
        }
    }


    protected boolean isEmptyPath(String basePath) {
        return "/".equals(basePath) || StringUtils.isEmpty(basePath);
    }

    private void suspendTimeoutRetryInterval() {
        try {
            TimeUnit.MILLISECONDS.sleep(connectionConfig.getServiceClientTimeoutRetryInterval());
        } catch (InterruptedException e) {
            logger.debug("Interrupted while suspending between retries ({})", Arrays.toString(e.getStackTrace()));
        }
    }

    protected void closedResourceStaleConnections() {
        HttpClientConnectionManager connManager = microsoftDocAuthorityClient.getClientConnectionManager();
        connManager.closeExpiredConnections();
        connManager.closeIdleConnections(connectionConfig.getConnectionPoolCloseStaleConnectionTTL(), TimeUnit.MILLISECONDS);
    }

    private void regulate() {
        logger.trace("Regulating MS api-call");
        int callQty = spApiCallCounter.incrementAndGet();
        if (connectionConfig.getApiCallBarrier() > 0 && callQty >= connectionConfig.getApiCallBarrier()) {
            synchronized (spApiCallCounter) {
                if (connectionConfig.getApiCallBarrier() > 0 && callQty >= connectionConfig.getApiCallBarrier()) {
                    logger.debug("API calls quantity reached its barrier ({}), pausing for {} seconds",
                            connectionConfig.getApiCallBarrier(), connectionConfig.getApiCallBarrierTTS() / 1000);

                    try {
                        TimeUnit.MILLISECONDS.sleep(connectionConfig.getApiCallBarrierTTS());
                    } catch (InterruptedException e) {
                        logger.warn("Interrupted while pausing");
                    }
                    spApiCallCounter.set(0);
                    // closedResourceStaleConnections();
                    logger.trace("MS-conn-regulator api call counter was reset");
                }
            }
        }
    }

    protected abstract List<ServerResourceDto> browseSubSiteFolders(String relPath, String subSite) throws Exception;

    protected abstract List<ServerResourceDto> listSubSites(String updatedPath);

    protected abstract List<ServerResourceDto> listSubSites(MSItemKey key, String basePath);


    /**
     * @param <E> Type of entity being built.
     * @param <T> Type of entity builder.
     */
    public static abstract class MicrosoftConnectorBaseBuilder<E extends MicrosoftConnectorBase, T extends MicrosoftConnectorBaseBuilder<E, T>> {
        protected SharePointConnectionParametersDto sharePointConnectionParametersDto;
        protected int pageSize = 1000;
        protected int maxRetries = 2;
        protected long maxFileSize = 20_000_000;
        protected String[] charsToFilter = new String[0];
        protected MSConnectionConfig connectionConfig;
        protected String folderToFail;
        protected List<String> foldersToFilter = Lists.newArrayList();
        protected MSAppInfo appInfo;
        protected String version;
        protected boolean isSpecialCharsSupported;

        public T withAppInfo(MSAppInfo appInfo) {
            this.appInfo = appInfo;
            return getThis();
        }

        public T withConnectionConfig(MSConnectionConfig config) {
            this.connectionConfig = config;
            return getThis();
        }

        public T withSharePointConnectionParametersDto(SharePointConnectionParametersDto sharePointConnectionParametersDto) {
            this.sharePointConnectionParametersDto = sharePointConnectionParametersDto;
            return getThis();
        }

        public T withPageSize(int pageSize) {
            this.pageSize = pageSize;
            return getThis();
        }

        public T withMaxRetries(int maxRetries) {
            this.maxRetries = maxRetries;
            return getThis();
        }

        public T withMaxFileSize(long maxFileSize) {
            this.maxFileSize = maxFileSize;
            return getThis();
        }

        public T withCharactersToFilter(String... charsToFilter) {
            this.charsToFilter = charsToFilter;
            return getThis();
        }


        public T withFoldersToFilter(List<String> folders) {
            this.foldersToFilter = Optional.ofNullable(folders)
                    .orElseGet(Lists::newArrayList);
            return getThis();
        }

        public T withFolderToFail(String folder) {
            this.folderToFail = folder;
            return getThis();
        }

        public T withIsSpecialCharactersSupported(boolean isSpecialCharsSupported) {
            this.isSpecialCharsSupported = isSpecialCharsSupported;
            return getThis();
        }

        protected abstract T getThis();

        public abstract E build();
    }
}
