package com.cla.connector.mediaconnector.microsoft.sharepoint;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StreamSiteData {
    private ScanTaskParameters scanParams;
    private String path;
    private String subSite;
    private Consumer<ClaFilePropertiesDto> consumer;
    private Consumer<String> createScanTaskConsumer;
    private Predicate<Pair<Long, Long>> scanActivePredicate;
    private ProgressTracker filePropsProgressTracker;
    private int currentDepth;

    StreamSiteData(ScanTaskParameters scanParams, String path, String subSite, Consumer<ClaFilePropertiesDto> consumer, Consumer<String> createScanTaskConsumer, Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker, int currentDepth) {
        this.scanParams = scanParams;
        this.path = path;
        this.subSite = subSite;
        this.consumer = consumer;
        this.createScanTaskConsumer = createScanTaskConsumer;
        this.scanActivePredicate = scanActivePredicate;
        this.filePropsProgressTracker = filePropsProgressTracker;
        this.currentDepth = currentDepth;
    }

    public ScanTaskParameters getScanParams() {
        return scanParams;
    }

    public String getPath() {
        return path;
    }

    public String getSubSite() {
        return subSite;
    }

    public Consumer<ClaFilePropertiesDto> getConsumer() {
        return consumer;
    }

    public Consumer<String> getCreateScanTaskConsumer() {
        return createScanTaskConsumer;
    }

    public Predicate<Pair<Long, Long>> getScanActivePredicate() {
        return scanActivePredicate;
    }

    public ProgressTracker getFilePropsProgressTracker() {
        return filePropsProgressTracker;
    }

    public int getCurrentDepth() {
        return currentDepth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreamSiteData that = (StreamSiteData) o;
        return currentDepth == that.currentDepth &&
                Objects.equals(scanParams, that.scanParams) &&
                Objects.equals(path, that.path) &&
                Objects.equals(subSite, that.subSite) &&
                Objects.equals(consumer, that.consumer) &&
                Objects.equals(createScanTaskConsumer, that.createScanTaskConsumer) &&
                Objects.equals(scanActivePredicate, that.scanActivePredicate) &&
                Objects.equals(filePropsProgressTracker, that.filePropsProgressTracker);
    }

    @Override
    public int hashCode() {
        return Objects.hash(scanParams, path, subSite, consumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker, currentDepth);
    }

    @Override
    public String toString() {
        return "StreamSiteData{" +
                "scanParams=" + scanParams +
                ", path='" + path + '\'' +
                ", subSite='" + subSite + '\'' +
                ", consumer=" + consumer +
                ", createScanTaskConsumer=" + createScanTaskConsumer +
                ", scanActivePredicate=" + scanActivePredicate +
                ", filePropsProgressTracker=" + filePropsProgressTracker +
                ", currentDepth=" + currentDepth +
                '}';
    }
}
