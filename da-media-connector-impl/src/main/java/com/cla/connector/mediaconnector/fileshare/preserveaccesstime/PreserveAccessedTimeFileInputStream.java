package com.cla.connector.mediaconnector.fileshare.preserveaccesstime;

import com.sun.jna.platform.win32.*;
import com.sun.jna.ptr.IntByReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class PreserveAccessedTimeFileInputStream extends InputStream {

    private static final Logger logger = LoggerFactory.getLogger(PreserveAccessedTimeFileInputStream.class);

    private static final int IO_READ_BUF_SIZE = 1024 * 32;

    private WinBase.OVERLAPPED overlapped = new WinBase.OVERLAPPED();
    private final String path;
    private WinNT.HANDLE handle;
    private boolean handleInitialized = false;
    private boolean open;
    private byte[] intByteArray = new byte[IO_READ_BUF_SIZE];
    private int readCount = 0;
    private int readPos = 0;
    private boolean isImpersonateBeforeAction;

    public PreserveAccessedTimeFileInputStream(String path, boolean isImpersonateBeforeAction) {
        this.path = path;
        this.open = true;
        this.isImpersonateBeforeAction = isImpersonateBeforeAction;
    }

    @Override
    public synchronized int read() throws IOException {

        ensureOpen();
        initHandleIfNeeded(isImpersonateBeforeAction);

        if (readPos >= readCount) { // Do read if pos reached IO_READ_BUF_SIZE
            long ts = System.currentTimeMillis();

            IntByReference lpNumberOfBytesRead = new IntByReference();
            boolean succeeded = Kernel32.INSTANCE.ReadFile(handle, intByteArray, intByteArray.length, lpNumberOfBytesRead, overlapped);
            readPos = 0;
            readCount = lpNumberOfBytesRead.getValue();
            if (logger.isTraceEnabled()) {
                logger.trace("**** read() of {} took {} ms", readCount, System.currentTimeMillis() - ts);
            }
            overlapped.Offset += readCount;
            if (readCount == 0 || !succeeded) {
                return -1;
            }
        }

        return (intByteArray[readPos++] & 0xff);
    }

    private void initHandleIfNeeded(boolean isImpersonateBeforeAction) throws IOException {
        if (!handleInitialized) {
            WindowsPreserveAccessTime.doPrivliged(() -> {
                this.handle = Kernel32.INSTANCE.CreateFile(
                        path,
                        WinNT.GENERIC_READ | WinNT.FILE_WRITE_ATTRIBUTES,
                        WinNT.FILE_SHARE_READ,
                        null,
                        WinNT.OPEN_EXISTING,
                        0,
                        null);
                ensureValidHandle();
                WindowsPreserveAccessTime.preserveLastAccessTime(this.handle, path);
                handleInitialized = true;
                readCount = 0;
                readPos = 0;
                logger.trace("**** Opened PreserveAccessedTimeFileInputStream for ({})", path);
            }, isImpersonateBeforeAction);
        }
    }

    @Override
    public synchronized void close() throws IOException {
        if (!open) {
            return;
        }
        if (handleInitialized) {
            open = false;
            boolean succeeded = Kernel32.INSTANCE.CloseHandle(handle);
            if (!succeeded) {
                throw new IOException(String.format("Failed to close file (%s), reason: %s", path, Kernel32Util.getLastErrorMessage()));
            }
            logger.trace("**** Closed PreserveAccessedTimeFileInputStream for ({})", path);
        }
    }

    private void ensureOpen() throws IOException {
        if (!open) {
            throw new IOException("Stream is closed");
        }
    }

    private void ensureValidHandle() throws IOException {
        if (handle == WinNT.INVALID_HANDLE_VALUE) {
            throw new IOException(String.format("Failed to open file (%s), reason: %s", path, Kernel32Util.getLastErrorMessage()));
        }
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public synchronized void reset() throws IOException {
        overlapped.Offset = 0;
        overlapped.OffsetHigh = 0;
        readPos = 0;
        readCount = 0;
    }

    public int getReadPos() {
        return readPos;
    }

    public int getReadCount() {
        return readCount;
    }

    public WinBase.OVERLAPPED getOverlapped() {
        return overlapped;
    }

    public WinNT.HANDLE getHandle() {
        return handle;
    }
}