package com.cla.connector.mediaconnector.exchange;

import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.AttachmentInfo;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.connector.domain.dto.media.mail.MsGraphItemLocator;
import com.cla.connector.mediaconnector.mail.MailRelated;
import com.cla.connector.utils.FileTypeUtils;
import com.google.common.base.Charsets;
import com.microsoft.graph.models.extensions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By Itai Marko
 */
final class MsGraphDtoConversionUtils {

    static Exchange365ItemDto dtoForMailFolder(MailFolder folder, DiffType diffType, String mailboxUPN) {
        MsGraphItemLocator mailFolderLocator = MsGraphItemLocator.forFolder(mailboxUPN, folder.id);
        Exchange365ItemDto dto = dtoForGraphEntity(mailFolderLocator, diffType);
        if (folder.parentFolderId != null) { // parentFolderId is null when deleting a folder
            dto.setParentFolderId(MsGraphItemLocator.forFolder(mailboxUPN, folder.parentFolderId).toString());
        }
        dto.setFileName(folder.displayName);
        dto.setFolder(true);

        return dto;
    }

    static Exchange365ItemDto dtoForEmailMessage(Message message, DiffType diffType, String mailboxUPN) {
        MsGraphItemLocator emailMessageLocator = MsGraphItemLocator.forEmailMessage(mailboxUPN, message.id);
        Exchange365ItemDto dto = dtoForGraphEntity(emailMessageLocator, diffType);
        // Replace any illegal path characters in the email's subject
        String pathifiedSubject = MailRelated.changeSubjectToFileName(message.subject, message.id);
        dto.setFileName(pathifiedSubject);
        dto.setType(FileType.MAIL);
        dto.setEmailSubject(MailRelated.getSubject(message.subject));
        populateMessageFields(dto, message);
        return dto;
    }

    static Exchange365ItemDto dtoForEmailFileAttachment(
            FileAttachment fileAttachment, Message containingMessage, String mailboxUpn) {

        MsGraphItemLocator fileAttachmentLocator =
                MsGraphItemLocator.forEmailFileAttachment(mailboxUpn, fileAttachment.id, containingMessage.id);
        Exchange365ItemDto dto = dtoForGraphEntity(
                fileAttachmentLocator, DiffType.CREATED_UPDATED);// This dto is created only for ingest -> never deleted
        dto.setParentFolderId(MsGraphItemLocator.forFolder(mailboxUpn, containingMessage.parentFolderId).toString());
        dto.setFileName(MailRelated.addHashToBaseName(fileAttachment.name, fileAttachment.id));
        dto.setType(FileTypeUtils.getFileType(fileAttachment.name));
        dto.setEmailSubject(fileAttachment.name);
        populateMessageFields(dto, containingMessage);
        // override file size and modification time, since populateMessageFields from the mail
        // and not from the attachment
        dto.setFileSize(fileAttachment.size == null ? null : fileAttachment.size.longValue());
        dto.setModTimeMilli(fileAttachment.lastModifiedDateTime.toInstant().toEpochMilli());
        return dto;
    }

    static AttachmentInfo attachmentInfoForGraphAttachment(Attachment graphAttachment,
                                                           String containingMessageId, String mailboxUPN) {
        MsGraphItemLocator fileAttachmentLocator =
                MsGraphItemLocator.forEmailFileAttachment(mailboxUPN, graphAttachment.id, containingMessageId);
        String attachmentMediaEntityId = fileAttachmentLocator.toString();
        String fileName = MailRelated.addHashToBaseName(graphAttachment.name, graphAttachment.id);
        return new AttachmentInfo(
                attachmentMediaEntityId, fileName, graphAttachment.size, graphAttachment.contentType, graphAttachment.name);
    }

    private static void populateMessageFields(Exchange365ItemDto dto, Message message) {
        if (message.parentFolderId != null) {
            dto.setParentFolderId(MsGraphItemLocator.forFolder(dto.getMailboxUpn(), message.parentFolderId).toString());
        }
        dto.setFolder(false);

        if (message.sentDateTime != null) {
            dto.setSentDate(message.sentDateTime.toInstant().toEpochMilli());
        }
        if (message.createdDateTime != null) {
            dto.setCreationTimeMilli(message.createdDateTime.toInstant().toEpochMilli());
        }
        if (message.lastModifiedDateTime != null) {
            dto.setModTimeMilli(message.lastModifiedDateTime.toInstant().toEpochMilli());
        }
        if (message.body != null) {
            dto.setFileSize((long) message.body.content.getBytes(Charsets.UTF_8).length);
        }

        if (message.sender != null) {
            EmailAddress sender = emailAddressForGraphAddress(message.sender.emailAddress);
            dto.setSender(sender);
        }

        List<Recipient> toRecipients = message.toRecipients == null ? new ArrayList<>() : message.toRecipients;
        List<Recipient> ccRecipients = message.ccRecipients == null ? new ArrayList<>() : message.ccRecipients;
        List<Recipient> bccRecipients = message.bccRecipients == null ? new ArrayList<>() : message.bccRecipients;
        List<EmailAddress> recipients =
                new ArrayList<>(toRecipients.size() + ccRecipients.size() + bccRecipients.size());
        toRecipients.forEach(recipient -> recipients.add(emailAddressForGraphAddress(recipient.emailAddress)));
        ccRecipients.forEach(recipient -> recipients.add(emailAddressForGraphAddress(recipient.emailAddress)));
        bccRecipients.forEach(recipient -> recipients.add(emailAddressForGraphAddress(recipient.emailAddress)));
        dto.setRecipients(recipients);
    }

    private static Exchange365ItemDto dtoForGraphEntity(MsGraphItemLocator entityLocator, DiffType diffType) {
        Exchange365ItemDto changeLogDto = Exchange365ItemDto.create(entityLocator.toString(), diffType);
        String mailboxUPN = entityLocator.getMailboxUpn();
        changeLogDto.setMailboxUpn(mailboxUPN);
        changeLogDto.setAclInheritanceType(AclInheritanceType.NONE);
        changeLogDto.setOwnerName(mailboxUPN);
        changeLogDto.addAllowedReadPrincipalName(mailboxUPN);
        changeLogDto.addAllowedWritePrincipalName(mailboxUPN);
        return changeLogDto;
    }

    private static EmailAddress emailAddressForGraphAddress(
            com.microsoft.graph.models.extensions.EmailAddress graphAddress) {
        return new EmailAddress(graphAddress.name, graphAddress.address);
    }

    private MsGraphDtoConversionUtils() {}
}
