package com.cla.connector.mediaconnector.microsoft;

import java.util.Objects;

/**
 * Note that path either is used or list-id/item-id are used
 */
public class MSItemKey {

    private String site;
    private String listId;
    private String itemId;
    private String path;
    private String basePathAddendum;

    private MSItemKey(String site, String path) {
        this.site = site;
        this.path = path;
    }

    private MSItemKey(String site, String listId, String itemId) {
        this(null, site, listId, itemId);
    }

    private MSItemKey(String basePathAddendum, String site, String listId, String itemId) {
        this.site = site;
        this.listId = listId;
        this.itemId = itemId;
        this.basePathAddendum = basePathAddendum;
    }

    public static MSItemKey listItem(String listId, String itemId) {
        return listItem(null, listId, itemId);
    }

    public static MSItemKey listItem(String site, String listId, String itemId) {
        return new MSItemKey(site, listId, itemId);
    }

    public static MSItemKey listItem(String basePathAddendum, String site, String listId, String itemId) {
        return new MSItemKey(basePathAddendum, site, listId, itemId);
    }

    public static MSItemKey path(String site, String path) {
        return new MSItemKey(site, path);
    }

    public String getSite() {
        return site;
    }

    public String getListId() {
        return listId;
    }

    public String getItemId() {
        return itemId;
    }

    public String getPath() {
        return path;
    }

    public String getBasePathAddendum() {
        return basePathAddendum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MSItemKey that = (MSItemKey) o;
        return Objects.equals(site, that.site) &&
                Objects.equals(listId, that.listId) &&
                Objects.equals(itemId, that.itemId) &&
                Objects.equals(basePathAddendum, that.basePathAddendum) &&
                Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(basePathAddendum, site, listId, itemId);
    }

    @Override
    public String toString() {
        return "MSItemKey{" +
                "site='" + site + '\'' +
                ", listId='" + listId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", path='" + path + '\'' +
                ", basePathAddendum='" + basePathAddendum + '\'' +
                '}';
    }
}