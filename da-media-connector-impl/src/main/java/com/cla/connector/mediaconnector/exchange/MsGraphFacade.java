package com.cla.connector.mediaconnector.exchange;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.media.*;
import com.cla.connector.domain.dto.media.mail.MsGraphItemLocator;
import com.cla.connector.mediaconnector.exchange.auth.MSGraphAuthenticationProvider;
import com.cla.connector.mediaconnector.exchange.throttling.MsGraphThrottlingHttpProvider;
import com.cla.connector.domain.dto.media.mail.MailItemType;
import com.cla.connector.utils.MailRelatedUtils;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.microsoft.graph.core.DefaultClientConfig;
import com.microsoft.graph.core.IClientConfig;
import com.microsoft.graph.core.IConnectionConfig;
import com.microsoft.graph.http.GraphError;
import com.microsoft.graph.http.GraphServiceException;
import com.microsoft.graph.http.IHttpProvider;
import com.microsoft.graph.logger.LoggerLevel;
import com.microsoft.graph.models.extensions.*;
import com.microsoft.graph.models.generated.BodyType;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.options.QueryOption;
import com.microsoft.graph.requests.extensions.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.cla.connector.domain.dto.media.mail.MailItemType.EMAIL;
import static com.cla.connector.mediaconnector.exchange.Exchange365Constants.Message.READ_FLAG_CHANGE_INDICATORS;
import static com.cla.connector.mediaconnector.exchange.Exchange365Constants.REMOVAL_INDICATOR;
import static com.cla.connector.mediaconnector.exchange.MsGraphDtoConversionUtils.*;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Created By Itai Marko
 */
class MsGraphFacade {

    private static final Logger logger = LoggerFactory.getLogger(MsGraphFacade.class);

    private static final ZoneId utcZoneId = ZoneId.of("UTC");

    @SuppressWarnings("SpellCheckingInspection")
    private static final DateTimeFormatter dateOnlyFormatter =
            DateTimeFormatter.ofPattern("uuuu-MM-dd").withZone(utcZoneId);

    private static final String MAILBOX_NOT_ENABLED_FOR_REST_API = "MailboxNotEnabledForRESTAPI";

    private static final String MAILBOX_NOT_SUPPORTED_FOR_REST_API = "MailboxNotSupportedForRESTAPI";

    private final IGraphServiceClient graphClient;

    MsGraphFacade(Exchange365Params exchange365Params) {

        Exchange365ConnectionParametersDto connectionParams = exchange365Params.getConnectionParamsDto();
        MSGraphAuthenticationProvider authProvider =
                new MSGraphAuthenticationProvider(connectionParams, exchange365Params.getAuthConfig());

        IClientConfig graphClientConfig = DefaultClientConfig.createWithAuthenticationProvider(authProvider);
        IHttpProvider internalHttpProvider = graphClientConfig.getHttpProvider();
        IConnectionConfig connectionConfig = internalHttpProvider.getConnectionConfig();
        connectionConfig.setConnectTimeout(connectionParams.getConnectionConnectTimeout());
        connectionConfig.setReadTimeout(connectionParams.getConnectionReadTimeout());

        MsGraphThrottlingHttpProvider throttlingHttpProviderDecorator =
                new MsGraphThrottlingHttpProvider(internalHttpProvider, exchange365Params.getThrottlingParams());

        this.graphClient = GraphServiceClient
                .builder()
                .authenticationProvider(authProvider)
                .httpProvider(throttlingHttpProviderDecorator)
                .serializer(graphClientConfig.getSerializer())
                .executors(graphClientConfig.getExecutors())
                .logger(graphClientConfig.getLogger())
                .buildClient();

        graphClient.getLogger().setLoggingLevel(LoggerLevel.DEBUG); // TODO Itai: wire the logger to our SLF4J/logback
    }

    public boolean testConnection() {
        try {
            graphClient.users().buildRequest().get();
            return true;
        } catch (Exception e) {
            logger.error("failed test connection", e);
            return false;
        }
    }

    String streamFolderStructureChanges(
            String mailboxUPN, String syncState, Long runId,
            Consumer<MediaChangeLogDto> changeConsumer, Consumer<String> createScanTaskConsumer) {
        try {
            // Create folder-structure deltas request builder
            IMailFolderDeltaCollectionRequestBuilder foldersDeltaRequestBuilder
                    = getFoldersDeltaRequestBuilder(mailboxUPN, syncState, runId);

            // Stream folder-structure changes, page by page
            List<Exchange365ItemDto> folderChanges = new ArrayList<>();
            IMailFolderDeltaCollectionPage folderDeltaPage = null;
            while (foldersDeltaRequestBuilder != null) {
                IMailFolderDeltaCollectionRequest foldersDeltaRequest = foldersDeltaRequestBuilder.buildRequest();
                folderDeltaPage = foldersDeltaRequest.get(); // TODO Itai: handle possible ClientException
                foldersDeltaRequestBuilder = folderDeltaPage.getNextPage();
                streamFolderStructureChangesPage(folderDeltaPage, mailboxUPN, folderChanges::add, createScanTaskConsumer);
            }

            sendFolderStructureMessage(mailboxUPN, changeConsumer, folderChanges);

            // Get next SyncState (deltaLink) from the last page
            if (folderDeltaPage == null) {
                throw new RuntimeException("folderDeltaPage is unexpectedly null while streaming folder-structure " +
                        "changes for mailbox: " + mailboxUPN + ", in runId: " + runId);
            }
            String nextSyncState = folderDeltaPage.deltaLink();
            if (nextSyncState == null) {
                logger.error("The nextSyncState is unexpectedly null after streaming the last folder-structure delta " +
                        "page for mailbox: {}, in runId: {}, with original syncState: {}", mailboxUPN, runId, syncState);
            }
            return nextSyncState;
        } catch (GraphServiceException e) {
            GraphError graphServiceError = e.getServiceError();
            if (graphServiceError != null && (
                    MAILBOX_NOT_ENABLED_FOR_REST_API.equals(graphServiceError.code) ||
                            MAILBOX_NOT_SUPPORTED_FOR_REST_API.equals(graphServiceError.code))) {
                logger.error("Mailbox {} is inaccessible. Got MS-Graph service error code: {} with error message: {}",
                        mailboxUPN, graphServiceError.code, graphServiceError.message, e);
                reportScanError(changeConsumer, null, runId, e);
                return null; // TODO Itai: This is just a quick fix to not fail the entire map job (DAMAIN-7283). Need to handle multi-task map jobs properly
            } else {
                throw e;
            }
        }
    }

    private IMailFolderDeltaCollectionRequestBuilder getFoldersDeltaRequestBuilder(
            String mailboxUPN, String syncState, Long runId) {

        IMailFolderDeltaCollectionRequestBuilder foldersDeltaRequestBuilder;
        if (syncState == null) {
            logger.info("Streaming folder-structure changes for mailbox: {}, in runId: {}, since the beginning of time",
                    mailboxUPN, runId);
            foldersDeltaRequestBuilder = graphClient.users(mailboxUPN).mailFolders().delta();
        } else {
            logger.info("Streaming folder-structure changes for mailbox: {}, in runId: {}, since syncState: {}",
                    mailboxUPN, runId, syncState);
            foldersDeltaRequestBuilder = graphClient.users(mailboxUPN).mailFolders().delta(syncState);
        }
        return foldersDeltaRequestBuilder;
    }

    private void streamFolderStructureChangesPage(
            IMailFolderDeltaCollectionPage folderDeltaPage, String mailboxUPN,
            Consumer<Exchange365ItemDto> changeConsumer, Consumer<String> createScanTaskConsumer) {

        List<MailFolder> changedFolders = folderDeltaPage.getCurrentPage();
        logger.debug("Got a page of {} changed mailFolders", changedFolders.size());
        for (MailFolder changedFolder : changedFolders) {
            DiffType diffType = detectDiffType(changedFolder);
            switch (diffType) {
                case CREATED_UPDATED:
                    logger.info("Sending a request to create a folder-contents task for folder: {}, in mailbox: {}," +
                            " folder Id: {}", changedFolder.displayName, mailboxUPN, changedFolder.id);
                    createScanTaskConsumer.accept(MsGraphItemLocator.forFolder(mailboxUPN, changedFolder.id).toString());
                    // fall through
                case DELETED:
                    Exchange365ItemDto changeLogDto =
                            MsGraphDtoConversionUtils.dtoForMailFolder(changedFolder, diffType, mailboxUPN);
                    logger.info("Consuming folder-structure diffType: {} of Exchange365ItemDto: {}", diffType, changeLogDto);
                    changeConsumer.accept(changeLogDto);
                    break;
                case SIMILAR:
                    logger.error("Unexpected READ_CHANGED detected for mailFolder: {}, in mailbox: {}, with ID: {}, " +
                                    "raw json: {}",
                            changedFolder.displayName, mailboxUPN, changedFolder.id, changedFolder.getRawObject());
                    break;
                default:
                    logger.error("Unexpected '{}' DiffType detected for mailFolder: {}, in mailbox: {}, with ID: {}," +
                                    " raw json: {}",
                            diffType, changedFolder.displayName, mailboxUPN, changedFolder.id,
                            changedFolder.getRawObject());
            }
        }
    }

    private void sendFolderStructureMessage(String mailboxUPN, Consumer<MediaChangeLogDto> changeConsumer, List<Exchange365ItemDto> folderChanges) {
        if (!folderChanges.isEmpty()) {
            Exchange365FoldersChangesDto foldersChangesDto = new Exchange365FoldersChangesDto(folderChanges);
            foldersChangesDto.setFolder(true);
            foldersChangesDto.setMailboxUPN(mailboxUPN);
            changeConsumer.accept(foldersChangesDto);
        }
    }

    String streamFolderContentsChanges(
            String mailboxUPN, String folderId, String syncState, Instant fromDate, Long runId,
            Consumer<MediaChangeLogDto> changeConsumer, Predicate<? super String> fileTypesPredicate) {

        // Create folder-contents deltas request builder
        IMessageDeltaCollectionRequestBuilder messageDeltaRequestBuilder =
                getMessageDeltaRequestBuilder(mailboxUPN, folderId, syncState, runId);

        // Create query parameters
        List<Option> options = new ArrayList<>(2);
        options.add(getExpandAttachmentsQueryParam());
        getFromFilterQueryParam(fromDate).ifPresent(options::add);

        // Stream folder-contents changes, page by page
        IMessageDeltaCollectionPage messageDeltaPage = null;
        while(messageDeltaRequestBuilder != null) {
            try {
                IMessageDeltaCollectionRequest messageDeltaRequest = messageDeltaRequestBuilder.buildRequest(options);
                messageDeltaPage = messageDeltaRequest.get();
                messageDeltaRequestBuilder = messageDeltaPage.getNextPage();
                streamFolderContentsChangesPage(messageDeltaPage, mailboxUPN, changeConsumer, fileTypesPredicate);
            } catch (Exception e) {
                logger.error("Failed processing change log page for Exchange mailbox {} and folder {}", mailboxUPN, folderId, e);
                reportScanError(changeConsumer, folderId, runId, e);
            }
        }

        // Get the last SyncState (deltaLink) from the last page
        if (messageDeltaPage == null) {
            throw new RuntimeException("messageDeltaPage is unexpectedly null while streaming folder-contents " +
                    "changes for mailbox: " + mailboxUPN + ", in runId: " + runId);
        }
        String nextSyncState = messageDeltaPage.deltaLink();
        if (nextSyncState == null) {
            logger.error("The nextSyncState is unexpectedly null after streaming the last folder-contents delta " +
                    "page for mailbox: {}, in runId: {}, with original syncState: {}", mailboxUPN, runId, syncState);
        }
        return nextSyncState;
    }

    private IMessageDeltaCollectionRequestBuilder getMessageDeltaRequestBuilder(
            String mailboxUPN, String folderId, String syncState, Long runId) {

        IMessageDeltaCollectionRequestBuilder messageDeltaRequestBuilder;
        if (syncState == null) {
            logger.info("Streaming folder-contents changes for mailbox: {}, in runId: {}, folderId: {}, since the " +
                    "beginning of time", mailboxUPN, runId, folderId);
            messageDeltaRequestBuilder = graphClient.users(mailboxUPN).mailFolders(folderId).messages().delta();
        } else {
            logger.info("Streaming folder-contents changes for mailbox: {}, in runId: {}, folderId: {}, " +
                    "since syncState: {}", mailboxUPN, runId, folderId, syncState);
            messageDeltaRequestBuilder = graphClient.users(mailboxUPN).mailFolders(folderId).messages().delta(syncState);
        }
        return messageDeltaRequestBuilder;
    }

    private QueryOption getExpandAttachmentsQueryParam() {
        // TODO Itai: find a way to filter out the contentBytes
        return new QueryOption( // TODO Itai: move below to consts
                "$expand", "attachments(%24select%3Did%2C%20name%2C%20size%2C%20isInline%2C%20lastModifiedDateTime)");
    }

    private Optional<QueryOption> getFromFilterQueryParam(Instant fromDate) {
        if (fromDate == null) {
            return Optional.empty();
        }

        String fromStr = LocalDateTime.ofInstant(fromDate, utcZoneId).format(dateOnlyFormatter);
        // TODO Itai: move below to consts
        String filterValue = "ReceivedDateTime%20ge%20" + fromStr;
        return Optional.of(new QueryOption("$filter", filterValue));
    }

    private void reportScanError(Consumer<MediaChangeLogDto> changeConsumer, String mediaItemId, Long runId, Exception e) {

        MediaChangeLogDto errorMsg = MediaChangeLogDto.create();
        errorMsg.setFolder(true);
        errorMsg.setMediaItemId(mediaItemId);
        errorMsg.addError(MediaConnectorUtils.createScanError(e, mediaItemId, runId));
        changeConsumer.accept(errorMsg);
    }

    private void streamFolderContentsChangesPage(
            IMessageDeltaCollectionPage messageDeltaPage, String mailboxUPN, Consumer<MediaChangeLogDto> changeConsumer,
            Predicate<? super String> fileTypesPredicate) {

        List<Message> changedMessages = messageDeltaPage.getCurrentPage();
        logger.debug("Got a page of {} mailFolder's changed messages, delta link {}", changedMessages.size(), messageDeltaPage.deltaLink());
        for (Message changedMessage : changedMessages) {
            DiffType diffType = detectDiffType(changedMessage);
            switch (diffType) {
                case CREATED_UPDATED:
                case DELETED:
                    Exchange365ItemDto changeLogDto = dtoForEmailMessage(changedMessage, diffType, mailboxUPN);
                    String messageMid = changeLogDto.getMediaItemId();
                    logger.info("Consuming message change diffType: {} of Exchange365ItemDto: {}", diffType, changeLogDto);
                    List<AttachmentInfo> attachmentInfos =
                            getMessageAttachmentsStream(changedMessage)
                                    .filter(attachment ->
                                            shouldScanAttachment(
                                                    attachment, changedMessage, messageMid, mailboxUPN, fileTypesPredicate))
                                    .map(attachment ->
                                            attachmentInfoForGraphAttachment(attachment, changedMessage.id, mailboxUPN))
                                    .collect(Collectors.toList());
                    changeLogDto.setAttachmentInfos(attachmentInfos);
                    changeConsumer.accept(changeLogDto);
                    break;
                case SIMILAR:
                    logger.debug("Skipping message with detected READ_CHANGED message: {}, with ID: {}, raw json: {}",
                            changedMessage.subject, changedMessage.id, changedMessage.getRawObject());
                    break;
                default:
                    logger.error("Unexpected '{}' DiffType detected for mailFolder: {}, with ID: {}, raw json: {}",
                            diffType, changedMessage.subject, changedMessage.id, changedMessage.getRawObject());
            }
        }
    }

    private boolean shouldScanAttachment(
            Attachment attachment, Message containingMessage, String messageMid, String mailboxUPN,
            Predicate<? super String> fileTypesPredicate) {

        if (!(attachment instanceof FileAttachment)) {
            logger.debug("Skipping attachment: {}, of class: {}, of containingMessage: {}, of mailbox: {}, containingMessage Id: {}, attachmentId: {}", attachment.name, attachment.getClass(), containingMessage.subject, mailboxUPN, messageMid, attachment.id);
            return false;
        }
        if (!fileTypesPredicate.test(attachment.name)) {
            logger.debug("Skipping attachment: {}, of ignored file type. Attachment's containingMessage: {}, of mailbox: {}, containingMessage Id: {}, attachmentId: {}", attachment.name, containingMessage.subject, mailboxUPN, messageMid, attachment.id);
            return false;
        }

        return true;
    }

    private Stream<Attachment> getMessageAttachmentsStream(Message message) {
        if (message.attachments == null) {
            return Stream.empty();
        }

        MsGraphAttachmentsPagingSpliterator spliterator = new MsGraphAttachmentsPagingSpliterator(
                message.attachments,
                pageSize -> logRetrievedAttachmentsPageSize(pageSize, message));

        return StreamSupport.stream(spliterator, false);
    }

    private void logRetrievedAttachmentsPageSize(int retrievedAttachmentsPageSize, Message containingMessage) {
        logger.debug("Downloaded a page of {} attachments of message: {}, message Id: {}",
                retrievedAttachmentsPageSize, containingMessage.subject, containingMessage.id);
    }

    Pair<FileContentDto, ClaFilePropertiesDto> getContentForDownload(String itemId) {
        MsGraphItemLocator graphItemLocator = MsGraphItemLocator.fromString(itemId);
        MailItemType type = graphItemLocator.getType();
        switch (type) {
            case EMAIL:
                Message message = getEmailMessageByGraphId(graphItemLocator, true);
                Exchange365ItemDto emailMessageItemDto = getEmailMessageItemDto(message, graphItemLocator.getMailboxUpn());
                FileContentDto emailMessageContentDto = getEmailMessageContentDtoForDownload(message, emailMessageItemDto);
                return Pair.of(emailMessageContentDto, emailMessageItemDto);
            case ATTACHMENT:
                FileAttachment fileAttachment = getEmailFileAttachmentByGraphId(graphItemLocator);
                FileContentDto attachmentFileContentDto =
                        new FileContentDto(fileAttachment.contentBytes, fileAttachment.name);
                Exchange365ItemDto attachmentItemDto = getFileAttachmentItemDto(fileAttachment, graphItemLocator);
                return Pair.of(attachmentFileContentDto, attachmentItemDto);
            default:
                throw new IllegalArgumentException(
                        "MsGraphFacade.getItemDtoAndContent() got itemId with invalid type: " + type + ", itemId: " + itemId);
        }
    }

    Pair<FileContentDto, ClaFilePropertiesDto> getItemDtoAndContent(String itemId) {
        MsGraphItemLocator graphItemLocator = MsGraphItemLocator.fromString(itemId);
        MailItemType type = graphItemLocator.getType();
        switch (type) {
            case EMAIL:
                Message message = getEmailMessageByGraphId(graphItemLocator, false);
                FileContentDto emailMessageContentDto = getEmailMessageContentDto(message);
                Exchange365ItemDto emailMessageItemDto = getEmailMessageItemDto(message, graphItemLocator.getMailboxUpn());
                return Pair.of(emailMessageContentDto, emailMessageItemDto);
            case ATTACHMENT:
                FileAttachment fileAttachment = getEmailFileAttachmentByGraphId(graphItemLocator);
                FileContentDto attachmentFileContentDto =
                        new FileContentDto(fileAttachment.contentBytes, fileAttachment.name);
                Exchange365ItemDto attachmentItemDto = getFileAttachmentItemDto(fileAttachment, graphItemLocator);
                return Pair.of(attachmentFileContentDto, attachmentItemDto);
            default:
                throw new IllegalArgumentException(
                        "MsGraphFacade.getItemDtoAndContent() got itemId with invalid type: " + type + ", itemId: " + itemId);
        }
    }

    Map<String, Pair<FileContentDto, ClaFilePropertiesDto>> getEmailAndAttachmentsFileData(
            String emailMessageId, Set<String> expectedAttachmentsIds) {

        MsGraphItemLocator graphEmailMessageLocator = MsGraphItemLocator.fromString(emailMessageId);

        MailItemType itemType = graphEmailMessageLocator.getType();
        checkArgument(itemType == EMAIL,
                "MsGraphFacade.getEmailAndAttachmentsFileData() got emailMessageId with invalid itemType: %s, emailMessageId: %s", itemType, emailMessageId);

        String mailboxUpn = graphEmailMessageLocator.getMailboxUpn();
        Message messageWithAttachments = getEmailMessageByGraphId(graphEmailMessageLocator, true); // TODO Itai: Possible performance improvement: request only the attachments that were scanned (the ones in the expectedAttachmentsIds Set)

        List<Pair<FileContentDto, ClaFilePropertiesDto>> fileDataList =
                getMessageAttachmentsStream(messageWithAttachments)
                        .filter(attachment ->
                                shouldIngestAttachment(attachment, messageWithAttachments, emailMessageId))
                        .map(attachment ->
                                (FileAttachment) attachment) // This is safe after the filter above
                        .map(fileAttachment ->
                                getFileAttachmentData(fileAttachment, messageWithAttachments, mailboxUpn))
                        .collect(
                                Collectors.toList());

        Map<String, Pair<FileContentDto, ClaFilePropertiesDto>> fileDataByIdMap =
                new HashMap<>(fileDataList.size() + 1);
        for (Pair<FileContentDto, ClaFilePropertiesDto> fileData : fileDataList) {
            ClaFilePropertiesDto attachmentPayload = fileData.getValue();
            String attachmentMid = attachmentPayload.getMediaItemId();
            if (expectedAttachmentsIds.contains(attachmentMid)) {
                fileDataByIdMap.put(attachmentMid, fileData);
            } else {
                // This means an attachment was added to an email after it was mapped and before it was ingested
                logger.debug("Skipping unexpected attachment: {} of email: {}, in mailbox: {}, attachmentId: {}",
                        attachmentPayload.getFileName(), messageWithAttachments.subject, mailboxUpn, attachmentMid);
            }
        }

        FileContentDto emailMessageContentDto = getEmailMessageContentDto(messageWithAttachments);
        Exchange365ItemDto emailMessageItemDto = getEmailMessageItemDto(messageWithAttachments, mailboxUpn);
        fileDataByIdMap.put(emailMessageId, Pair.of(emailMessageContentDto, emailMessageItemDto));

        return fileDataByIdMap;
    }

    private boolean shouldIngestAttachment(Attachment attachment, Message containingMessage, String containingMessageId) {
        if (attachment instanceof FileAttachment) {
            return true;
        }
        logger.debug("Skipping non FileAttachment attachment: {}, of type:  {}, of message: {}, message Id: {}, attachmentId: {}", attachment.name, attachment.getClass(), containingMessage.subject, containingMessageId, attachment.id);
        return false;
    }

    private Pair<FileContentDto, ClaFilePropertiesDto> getFileAttachmentData(
            FileAttachment attachment, Message containingMessage, String mailboxUpn) {

        FileContentDto attachmentFileContentDto = new FileContentDto(attachment.contentBytes, attachment.name);
        Exchange365ItemDto attachmentItemDto = dtoForEmailFileAttachment(attachment, containingMessage, mailboxUpn);
        return Pair.of(attachmentFileContentDto, attachmentItemDto);
    }

    private Message getEmailMessageByGraphId(MsGraphItemLocator graphItemLocator, boolean includeAttachments) {
        String mailboxUpn = graphItemLocator.getMailboxUpn();
        String messageUid = graphItemLocator.getItemId().getUid();
        return getEmailMessage(mailboxUpn, messageUid, includeAttachments);
    }

    private FileContentDto getEmailMessageContentDto(Message message) {
        String subject = message.subject;
        String content = message.body.content;
        // TODO Itai: the following assumes UTF_8 encoding. Encoding should be easy to detect (at least for html)
        byte[] contentBytes = content.getBytes(StandardCharsets.UTF_8);
        return new FileContentDto(contentBytes, subject);
    }

    private FileContentDto getEmailMessageContentDtoForDownload(Message message, Exchange365ItemDto emailMessageItemDto) {
        try {
            MimeMessage mimeMessage = MailRelatedUtils.getMimeMessage();
            mimeMessage.setSubject(emailMessageItemDto.getEmailSubject());
            mimeMessage.setSentDate(new Date(emailMessageItemDto.getSentDate()));

            if (emailMessageItemDto.getSender() != null) {
                InternetAddress fromMailbox = new InternetAddress();
                String senderEmailAddress = emailMessageItemDto.getSender().getAddress();
                fromMailbox.setAddress(senderEmailAddress);
                String senderName = emailMessageItemDto.getSender().getName();
                if (!senderName.isEmpty()) {
                    fromMailbox.setPersonal(senderName);
                } else {
                    fromMailbox.setPersonal(senderEmailAddress);
                }
                mimeMessage.setFrom(fromMailbox);
            }

            if (message.toRecipients != null) {
                for (Recipient recipient : message.toRecipients) {
                    InternetAddress recipientAddress = new InternetAddress(recipient.emailAddress.address, recipient.emailAddress.name);
                    mimeMessage.setRecipient(javax.mail.Message.RecipientType.TO, recipientAddress);
                }
            }
            if (message.ccRecipients != null) {
                for (Recipient recipient : message.ccRecipients) {
                    InternetAddress recipientAddress = new InternetAddress(recipient.emailAddress.address, recipient.emailAddress.name);
                    mimeMessage.setRecipient(javax.mail.Message.RecipientType.CC, recipientAddress);
                }
            }
            if (message.bccRecipients != null) {
                for (Recipient recipient : message.bccRecipients) {
                    InternetAddress recipientAddress = new InternetAddress(recipient.emailAddress.address, recipient.emailAddress.name);
                    mimeMessage.setRecipient(javax.mail.Message.RecipientType.BCC, recipientAddress);
                }
            }

            setContentToMimeMessage(message, mimeMessage);
            byte[] contentBytes = MailRelatedUtils.getMimeMessageBytes(mimeMessage);
            return new FileContentDto(contentBytes, emailMessageItemDto.getEmailSubject());
        } catch (Exception e) {
            logger.error("problem creating mime message", e);
            throw new RuntimeException(e);
        }
    }

    private void setContentToMimeMessage(Message message, MimeMessage mimeMessage) throws MessagingException, IOException {
        MimeMultipart rootMultipart = new MimeMultipart();
        MimeBodyPart contentBodyPart = new MimeBodyPart();
        MimeMultipart contentMultipart = new MimeMultipart();
        contentBodyPart.setContent(contentMultipart);

        String msgBody = null;
        if (message.body != null) {
            msgBody = message.body.content.trim();
            if (!msgBody.isEmpty() && message.body.contentType.equals(BodyType.TEXT)) {
                MimeBodyPart textBodyPart = new MimeBodyPart();
                textBodyPart.setText(msgBody);
                contentMultipart.addBodyPart(textBodyPart);
            }
            if (!msgBody.isEmpty() && message.body.contentType.equals(BodyType.HTML)) {
                MimeBodyPart htmlBodyPart = new MimeBodyPart();
                htmlBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(msgBody, "text/html")));
                contentMultipart.addBodyPart(htmlBodyPart);
            }
        }
        if (Strings.isNullOrEmpty(msgBody)) {
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText("<<Empty Email Body>>");
            textBodyPart.addHeaderLine("Content-Type: text/plain; charset=\"utf-8\"");
            textBodyPart.addHeaderLine("Content-Transfer-Encoding: quoted-printable");
            contentMultipart.addBodyPart(textBodyPart);
        }
        rootMultipart.addBodyPart(contentBodyPart);

        getMessageAttachmentsStream(message)
                .map(attachment ->
                        (FileAttachment) attachment)
                .forEach(attachment -> {
                    try {
                        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                        String mimeTag = attachment.contentType;
                        if (Strings.isNullOrEmpty(mimeTag)) {
                            mimeTag = URLConnection.guessContentTypeFromName(attachment.name);
                        }
                        if (Strings.isNullOrEmpty(mimeTag)) {
                            mimeTag = "application/octet-stream";
                        }
                        DataSource source = new ByteArrayDataSource(attachment.contentBytes, mimeTag);
                        attachmentBodyPart.setDataHandler(new DataHandler(source));
                        attachmentBodyPart.setContentID(attachment.contentId);
                        attachmentBodyPart.setFileName(attachment.name);
                        rootMultipart.addBodyPart(attachmentBodyPart);
                    } catch (Exception e) {
                        logger.error("problem adding attachment to mime message", e);
                    }
                });

        mimeMessage.setContent(rootMultipart);
    }

    private Exchange365ItemDto getEmailMessageItemDto(Message message, String mailboxUpn) {
        DiffType diffType = detectDiffType(message); // TODO Itai: this is just because we're sending the same dto from scan and from ingest..
        return dtoForEmailMessage(message, diffType, mailboxUpn);
    }

    private Exchange365ItemDto getFileAttachmentItemDto(
            FileAttachment fileAttachment, MsGraphItemLocator attachmentItemLocator) {

        String containingEmailUid = attachmentItemLocator.getParentItemId().getUid(); // TODO Itai: validate parent is email type
        String mailboxUpn = attachmentItemLocator.getMailboxUpn();
        Message containingMessage = getEmailMessage(mailboxUpn, containingEmailUid, false); // TODO Itai: this is another call to graph just to get the sender and recipients.. Aaahhhh!!!
        return dtoForEmailFileAttachment(fileAttachment, containingMessage, mailboxUpn);
    }

    private Message getEmailMessage(String mailboxUpn, String messageUid, boolean includeAttachments) {
        logger.debug("Fetching Exchange email message using Ms-Graph from mailbox: {}, includeAttachments: {}, messageId: {}"
                , mailboxUpn, includeAttachments, messageUid);
        HeaderOption preferHtmlBodyHeader = new HeaderOption("Prefer", "outlook.body-content-type=\"html\"");
        IMessageRequest messageRequest = graphClient
                .users(mailboxUpn)
                .messages(messageUid)
                .buildRequest(Collections.singletonList(preferHtmlBodyHeader));
        if (includeAttachments) {
            messageRequest = messageRequest.expand("attachments"); // TODO Itai: move to constants
        }
        Message message = messageRequest.get(); // TODO Itai: handle possible ClientException
        logger.debug("Fetched Exchange email message using Ms-Graph from mailbox: {}, message.subject: {}, messageId: {}"
                , mailboxUpn, message.subject, message.id);
        return message;
    }

    private FileAttachment getEmailFileAttachmentByGraphId(MsGraphItemLocator graphItemLocator) {
        Attachment attachment = getEmailAttachmentByGraphId(graphItemLocator);
        if (attachment instanceof FileAttachment) {
            return (FileAttachment) attachment;
        } else { // Non FileAttachments were supposed to get filtered out in the scan in streamMessageAttachmentsPage()
            throw new RuntimeException("Unsupported attachment type of attachment: " + attachment.name + ",  type: " +
                    attachment.getClass() + ", attachmentId: " + attachment.id + ", Locator: " + graphItemLocator);
        }
    }

    private Attachment getEmailAttachmentByGraphId(MsGraphItemLocator graphItemLocator) {
        String mailboxUpn = graphItemLocator.getMailboxUpn();
        String attachmentUid = graphItemLocator.getItemId().getUid();
        String containingEmailUid = graphItemLocator.getParentItemId().getUid(); // TODO Itai: validate parent is email type
        logger.debug(
                "Fetching Exchange attachment using MS-Graph from mailbox {}, attachmentId: {}, containing messageId: {}" +
                        ", Locator: {}", mailboxUpn, attachmentUid, containingEmailUid, graphItemLocator);

        Attachment attachment = graphClient
                .users(mailboxUpn)
                .messages(containingEmailUid)
                .attachments(attachmentUid)
                .buildRequest()
                .get(); // TODO Itai: handle possible ClientException
        logger.debug(
                "Fetched Exchange attachment using MS-Graph from mailbox {}, attachment name: {}, attachmentId: {}" +
                        ", containing messageId: {}",  mailboxUpn, attachment.name, attachmentUid, containingEmailUid);
        return attachment;
    }

    private static DiffType detectDiffType(Entity entity) {
        Objects.requireNonNull(entity);
        JsonObject rawObject = entity.getRawObject();

        if (rawObject.has(REMOVAL_INDICATOR)) {
            return DiffType.DELETED;
        }

        Set<String> keySet = rawObject.keySet();
        if (keySet.size() == READ_FLAG_CHANGE_INDICATORS.size() && keySet.containsAll(READ_FLAG_CHANGE_INDICATORS)) {
            logger.debug("Detected a read-flag change by READ_FLAG_CHANGE_INDICATORS heuristic." +
                    " Entity Id: {}, entity oDataType: {}", entity.id, entity.oDataType);
            return DiffType.SIMILAR;
        }

        return DiffType.CREATED_UPDATED;
    }
}
