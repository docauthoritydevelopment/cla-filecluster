package com.cla.connector.mediaconnector.mail;

import com.google.common.base.Charsets;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Created by: yael
 * Created on: 3/6/2019
 */
public class HashUtils {

    public static String getCRC32(final String text) {
        return String.valueOf(getCRC32(text.getBytes(Charsets.UTF_8)));
    }

    private static long getCRC32(final byte[] bytes) {
        Checksum checksum = new CRC32();

        // update the current checksum with the specified array of bytes
        checksum.update(bytes, 0, bytes.length);

        // get the current checksum value
        return checksum.getValue();
    }
}
