package com.cla.connector.mediaconnector.fileshare;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.mediaconnector.common.packed.PackageCrawler;
import com.cla.connector.mediaconnector.common.packed.PackageCrawlerFactory;
import com.cla.connector.mediaconnector.fileshare.preserveaccesstime.PreserveAccessedTimeFileInputStream;
import com.cla.connector.mediaconnector.fileshare.preserveaccesstime.WindowsPreserveAccessTime;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.MediaConnectorUtils;
import com.cla.connector.utils.Pair;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Connection wrapper for file servers
 * Created by uri on 08/08/2016.
 */
public class FileShareMediaConnector extends AbstractMediaConnector {

    private static final Logger logger = LoggerFactory.getLogger(FileShareMediaConnector.class);

    private boolean preserveAccessedTime;
    private boolean impersonateBeforeAction;
    private String pathToSharePermissionScript;

	private LoadingCache<String, ClaFilePropertiesDto> filePropertiesCache;

    public FileShareMediaConnector(boolean preserveAccessedTime, boolean impersonateBeforeAction, String pathToSharePermissionScript) {
        this.preserveAccessedTime = preserveAccessedTime;
        this.impersonateBeforeAction = impersonateBeforeAction;
        this.pathToSharePermissionScript = pathToSharePermissionScript;
		filePropertiesCache = CacheBuilder.newBuilder()
				.maximumSize(50)
				.expireAfterAccess(120, TimeUnit.SECONDS)
				.build(new CacheLoader<String, ClaFilePropertiesDto>() {
					@Override
					public ClaFilePropertiesDto load(String fileFullName) {
						Optional<ClaFilePropertiesDto> mediaProperties = FileShareMediaUtils.getMediaProperties(fileFullName, preserveAccessedTime, impersonateBeforeAction);
						return mediaProperties.orElse(null);
					}
				});
    }

    @Override
    public ClaFilePropertiesDto getFileAttributes(String fileFullName) {
		if (!FileNamingUtils.isPackageEntry(fileFullName)) {
			return filePropertiesCache.getUnchecked(fileFullName);
		}
		//handling package file
		String packageRootPath = FileNamingUtils.getPackageRootPath(fileFullName);
		String packageEntryPath = FileNamingUtils.getPackageEntryPath(fileFullName);
		Optional<PackageCrawler> packageCrawler = PackageCrawlerFactory.getPackageCrawler(packageRootPath);
		ClaFilePropertiesDto packageFileProps = filePropertiesCache.getUnchecked(packageRootPath);
		return packageCrawler.
                map(packageCrawler1 -> packageCrawler1.extractPackagedEntryProperties(packageFileProps, packageEntryPath))
                .orElse(null);
	}

    @Override
    public ClaFilePropertiesDto getMediaItemAttributes(@NotNull String mediaItemId) {
        return FileShareMediaUtils.getMediaProperties(mediaItemId, preserveAccessedTime, impersonateBeforeAction).orElse(null);
    }

    @Override
    public void streamMediaItems(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                 Consumer<DirListingPayload> directoryListingConsumer,
                                 Consumer<String> createScanTaskConsumer, Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {

        streamMediaItems(false, null, scanParams, filePropertiesConsumer, directoryListingConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
    }

    @Override
    public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
                                           Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                           Consumer<DirListingPayload> directoryListingConsumer,
                                           Consumer<String> createScanTaskConsumer,
                                           Predicate<Pair<Long, Long>> scanActivePredicate,
                                           ProgressTracker filePropsProgressTracker) {

        streamMediaItems(true, forkJoinPool, scanParams, filePropertiesConsumer, directoryListingConsumer,
                createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
    }

    @Override
    public MediaType getMediaType() {
        return MediaType.FILE_SHARE;
    }

    @Override
    public InputStream getInputStream(ClaFilePropertiesDto props) throws IOException {
        return getInputStream(props.getFileName());
    }

    private InputStream getInputStream(String filename) throws IOException {
        if (preserveAccessedTime) {
            logger.debug("Opening PreserveAccessedTimeFileInputStream for filename {}", filename);
            return new PreserveAccessedTimeFileInputStream(filename, impersonateBeforeAction);
        }

        return new FileInputStream(filename);
    }

    private void streamMediaItems(boolean concurrent, ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
                                  Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                  Consumer<DirListingPayload> directoryListingConsumer,
                                  Consumer<String> createScanTaskConsumer,
                                  Predicate<Pair<Long, Long>> scanActivePredicate,
                                  ProgressTracker filePropsProgressTracker){
        Predicate<? super String> fileTypesPredicate = MediaConnectorUtils.createFileTypePredicate(scanParams);
        Path url = Paths.get(scanParams.getPath());
        if (concurrent){
            FileShareMediaUtils.concurrentStreamMediaItems(forkJoinPool, fileTypesPredicate, url, scanParams,
                    filePropertiesConsumer, directoryListingConsumer, createScanTaskConsumer,
                    scanActivePredicate, filePropsProgressTracker, preserveAccessedTime, impersonateBeforeAction);
        }
        else{
            FileShareMediaUtils.streamMediaItems(url, scanParams,
                    filePropertiesConsumer, directoryListingConsumer, createScanTaskConsumer,
                    scanActivePredicate, preserveAccessedTime, impersonateBeforeAction);
        }
    }

    @SuppressWarnings("SpellCheckingInspection")
    private FileContentDto fetchZipEntryContentByPreservedInputStream(PreserveAccessedTimeFileInputStream patfis, String fileName) throws IOException {
        String entryName = FileNamingUtils.getPackageEntryPath(fileName);
        try (ZipArchiveInputStream zipArchiveInputStream = new ZipArchiveInputStream(patfis)) {
            ZipArchiveEntry nextZipEntry = zipArchiveInputStream.getNextZipEntry();
            while (nextZipEntry != null) {
                if (nextZipEntry.getName().equals(entryName)) {
                    byte[] content = new byte[(int) nextZipEntry.getSize()];
                    int offset = 0;
                    while (offset < content.length) {
                        offset += zipArchiveInputStream.read(content, offset, content.length - offset);
                    }
                    return new FileContentDto(new ByteArrayInputStream(content), nextZipEntry.getSize(), nextZipEntry.getName());
                }
                nextZipEntry = zipArchiveInputStream.getNextZipEntry();
            }
        }

        throw new FileNotFoundException(String.format("Did not find in zip entry %s", fileName));
    }

    private FileContentDto fetchPackageEntryContentByFileName(String filename) {
        String actualZipPath = FileNamingUtils.getPackageRootPath(filename);
        String entryName = FileNamingUtils.getPackageEntryPath(filename);
		Optional<PackageCrawler> packageCrawler = PackageCrawlerFactory.getPackageCrawler(actualZipPath);
        return packageCrawler.map(cr -> cr.extractPackagedEntry(actualZipPath, entryName)).orElse(null);
    }

    @Override
    public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
        return FileShareMediaUtils.isDirectoryExists(path, preserveAccessedTime, impersonateBeforeAction, ignoreAccessErrors);
    }

    @Override
    public void fetchAcls(ClaFilePropertiesDto claFileProp) {
        FileShareMediaUtils.fetchAcls(claFileProp);
    }

    @Override
    public List<ServerResourceDto> testConnection() {
		return null;
    }

    @Override
    public String getIdentifier() {
        return null;
    }

    @Override
    public FileContentDto getFileContent(String filename, boolean forUserDownload) throws IOException {
        if (FileNamingUtils.isPackageEntry(filename)) {
            return getPackageEntryContent(filename);
        }

        InputStream inputStream = getInputStream(filename);

        long length = 0;
        try {
            length = (int) (preserveAccessedTime ? WindowsPreserveAccessTime.fileSize(filename, impersonateBeforeAction) : Files.size(Paths.get(filename)));
        } catch (IOException e) {
            logger.warn("Could not get size for file {}. {}", filename, e.getMessage());
        }
        return new FileContentDto(inputStream, length, filename);
    }

	@Override
	public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
		return null;
	}

	private FileContentDto getPackageEntryContent(String filename) throws IOException {
        if (preserveAccessedTime) {
            //noinspection SpellCheckingInspection
            PreserveAccessedTimeFileInputStream patfis = new PreserveAccessedTimeFileInputStream(
                    FileNamingUtils.getPackageRootPath(filename), impersonateBeforeAction);
            return fetchZipEntryContentByPreservedInputStream(patfis, filename);
        }

        return fetchPackageEntryContentByFileName(filename);
    }

    // e.g  rootFolderPath = "//172.31.41.248/Enron/FullExtraction/j111";
    @Override
    public List<String> getSharePermissions(String rootFolderPath) throws Exception {
        Pair<String, String> res = getComputerShareFromPath(rootFolderPath);

        if (res == null) {
            logger.error("cannot get share permission for {}, failed to parse path", rootFolderPath);
            throw new RuntimeException("cannot get share permission, failed to parse path");
        }

        return FileShareMediaUtils.getSharePermissions(pathToSharePermissionScript, res.getKey(), res.getValue());
    }

    static Pair<String, String> getComputerShareFromPath(String rootFolderPath) {
        String path = FilenameUtils.separatorsToWindows(rootFolderPath);
        if (!path.startsWith("\\\\")) {
            return null;
        }
        path = path.replace("\\\\", "\\");
        if (path.startsWith("\\")) {
            path = path.substring(1);
        }
        String[] parts = path.split("\\\\");
        String computer = null;
        String share = null;
        if (parts.length > 1) {
            computer = parts[0];
            share = parts[1];

            if (computer.contains(":")) {
                computer = computer.substring(0, computer.indexOf(":"));
            }
        }

        if (Strings.isNullOrEmpty(computer) || Strings.isNullOrEmpty(share)) {
            return null;
        }
        return Pair.of(computer, share);
    }
}
