package com.cla.connector.mediaconnector.labeling.mip;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@SuppressWarnings("unused")
@Getter
@Setter
@ToString
public class MipLabelResult {
    private String createdOn;
    private String id;
    private String name;
    private String sensitivity;
    private String tooltip;
    private String description;
    private boolean active;

    @JsonProperty("created_on")
    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("is_active")
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
