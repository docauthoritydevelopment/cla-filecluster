package com.cla.connector.mediaconnector.exchange.auth;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class MSGraphAuthConfig {
    public final int maxAttempts;
    public final int retrySleepInterval;
    public final int prefetchTokenBeforeExpiry;
    public final int readTimeout;
    public final int connectTimeout;
}
