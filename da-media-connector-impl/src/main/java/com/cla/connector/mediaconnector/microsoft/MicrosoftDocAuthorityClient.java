package com.cla.connector.mediaconnector.microsoft;

import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.mediaconnector.exception.RateLimitException;
import com.cla.connector.mediaconnector.microsoft.sharepoint.*;
import com.cla.connector.utils.CharacterFilterInputStream;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.independentsoft.share.*;
import com.independentsoft.share.queryoptions.Expand;
import com.independentsoft.share.queryoptions.IQueryOption;
import com.independentsoft.share.queryoptions.Select;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.jdom2.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.ExceptionClassifierRetryPolicy;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import javax.validation.constraints.NotNull;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 30/11/2016.
 */
public class MicrosoftDocAuthorityClient {
    private static final String VERSION_HEADER = "MicrosoftSharePointTeamServices";
    static final int VERSION_DEFAULT = 2016;
    static final int VERSION_2013 = 2013;
    private static final String API_PATH = "/_api/web";
    private static final String SP_ONLINE_DOMAIN = "SharepointOnline".toLowerCase();
    private static final String EXPIRES_ON = "expires_on";
    private static SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    static {
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
    }

    // 2013>=
    private static final String GET_FILE_RESTRICTED_CHARS = "/GetFileByServerRelativeUrl('";

    // 2013<
    private static final String GET_FILE_ALL_CHARS = "/GetFileByServerRelativePath(decodedurl='";

    protected final String getFileApiCall;
    protected final String getFolderApiCall = "/GetFolderByServerRelativeUrl('";

    private final RetryTemplate retryTemplate;

    private CloseableHttpClient httpclient;

    private String formDigest;
    private long formDigestExpiration;

    private static final Logger logger = LoggerFactory.getLogger(MicrosoftDocAuthorityClient.class);

    private String securityToken = null;

    private Header[] signInHeaders = null;

    private URI endpoint;
    private String userName;
    private String domain;
    private String password;
    private String host;
    private String port;
    String basePath;
    private HttpClientConnectionManager clientConnectionManager;
    private String scheme;
    private String url;
    private int maxRetries;
    private boolean isSharePointOnline = false;
    private Service jshareService = null;

    private MSConnectionConfig connectionConfig;

    List<String> charsToFilter;

    private String userAgent = "JShare 1.0, www.independentsoft.com";

    protected MicrosoftDocAuthorityClient(String userName, String domain, String password,
                                          String userAgent, String url, int maxRetries,
                                          MSConnectionConfig connectionConfig,
                                          boolean isSpecialCharsSupported,
                                          String... charsToFilter) {
        this(userName, domain, password, userAgent, url, maxRetries, connectionConfig, VERSION_DEFAULT,
                isSpecialCharsSupported, charsToFilter);
    }

    protected MicrosoftDocAuthorityClient(String userName, String domain, String password,
                                          String userAgent, String url, int maxRetries,
                                          MSConnectionConfig connectionConfig,
                                          int version, boolean isSpecialCharsSupported, String... charsToFilter) {
        this.userName = userName;
        this.domain = domain;
        this.password = password;
        this.url = url;
        this.maxRetries = maxRetries;
        this.connectionConfig = connectionConfig;
        initEndpointParameters(url);
        this.charsToFilter = Lists.newArrayList(charsToFilter);

        getFileApiCall = isSpecialCharsSupported && VERSION_DEFAULT <= version ? GET_FILE_ALL_CHARS : GET_FILE_RESTRICTED_CHARS;

        Optional.ofNullable(userAgent)
                .ifPresent(ua -> this.userAgent = ua);

        retryTemplate = configRetryPolicy(maxRetries, connectionConfig);

        if (logger.isDebugEnabled()) {
            logger.debug("Creating MicrosoftDocAuthorityClient({},{},x,{},{},..) hash={}", userName, domain, userAgent, url, Integer.toHexString(hashCode()));
        }
    }

    private RetryTemplate configRetryPolicy(int maxRetries, MSConnectionConfig connectionConfig) {
        RetryTemplate retryTemplate = new RetryTemplate();
        ExceptionClassifierRetryPolicy exRetryPolicy = new ExceptionClassifierRetryPolicy();
        SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy(maxRetries);
        exRetryPolicy.setPolicyMap(new HashMap<Class<? extends Throwable>, RetryPolicy>() {{
            put(RateLimitException.class, simpleRetryPolicy);
            put(ConnectTimeoutException.class, simpleRetryPolicy);
            put(SocketTimeoutException.class, simpleRetryPolicy);
            put(IOException.class, simpleRetryPolicy);
            put(Exception.class, new NeverRetryPolicy());
        }});
        ExponentialBackOffPolicy exponentialBackOffPolicy = new ExponentialBackOffPolicy();
        exponentialBackOffPolicy.setMultiplier(connectionConfig.getBackoffMultiplier());
        exponentialBackOffPolicy.setInitialInterval(connectionConfig.getServiceClientTimeoutRetryInterval());
        retryTemplate.setBackOffPolicy(exponentialBackOffPolicy);
        retryTemplate.setRetryPolicy(exRetryPolicy);
        return retryTemplate;
    }

    private void initEndpointParameters(String url) {
        try {
            URL url1 = new URL(url);
            endpoint = url1.toURI();
            host = endpoint.getHost();
            port = endpoint.getPort() != -1 ? Integer.toString(endpoint.getPort()) : null;
            basePath = endpoint.getPath();
            scheme = endpoint.getScheme();
        } catch (MalformedURLException | URISyntaxException e) {
            throw new RuntimeException("Malformed URL for SharePoint connector " + url);
        }

        charsToFilter = Optional.ofNullable(charsToFilter).orElseGet(Lists::newArrayList);
        this.charsToFilter = Lists.newArrayList(charsToFilter);
    }

    private CloseableHttpResponse sendLegacyRequestToSharePoint(String api, int retries) throws IOException {
        logger.info("Calling  createHttpClientIfNeeded");
        createHttpClientIfNeeded();
        String uri = createLegacyApiUri(api);
        HttpGet httpget = new HttpGet(uri);
//        httpget.setHeader("Content-Type", "application/json;odata=verbose");
        httpget.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        httpget.setHeader("Accept-Encoding", "gzip, deflate, sdch");
        try {
            CloseableHttpResponse response = httpclient.execute(httpget, Service.getContext(url, userName));
            logger.debug("Response {} for url: {}", response.getStatusLine(), httpget.getRequestLine());
            if (logger.isTraceEnabled()) {
                logger.trace("Response headers: ", Stream.of(response.getAllHeaders()).map(header ->
                        header.getName() + ":" + header.getValue() +
                                Stream.of(header.getElements()).map(el -> el.getName() + ":" + el.getValue())
                                        .collect(Collectors.joining(",", "[", "]")))
                        .collect(Collectors.joining(";")));
            }
            return response;
        } catch (IOException e) {
            logger.warn("Failed to send request to URL: {}", uri, e);
            if (retries > 0) {
                return sendLegacyRequestToSharePoint(api, retries - 1);
            }
            throw e;
        }
    }

    private CloseableHttpResponse doSendRequestToSharePoint(String api, String subSite) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("Calling share point api {} for subsite {} (hash={})", api, subSite, Integer.toHexString(hashCode()));
        }
        createHttpClientIfNeeded();
        String uri = createApiUri(api, subSite);
        HttpGet httpget = new HttpGet(uri);
        httpget.setHeader("Content-Type", "application/json;odata=verbose");
        httpget.setHeader("Accept-Encoding", "gzip");
        if (isSharePointOnline) {
            if (signInHeaders == null || isExpiring(signInHeaders)) {
                signInHeaders = getSignInSpOnlineAppTokenHeaders(httpclient, userName, password, host);
                if (logger.isDebugEnabled()) {
                    logger.debug("Replacing expiring signInHeaders {}", signInHeaders == null ? "null" : Stream.of(signInHeaders).map(e -> e.getName() + ":" + e.getValue()).collect(Collectors.joining(";")));
                }
            }
            Stream.of(signInHeaders).forEach(h -> httpget.setHeader(h));
//            httpget.setHeaders(signInHeaders);
            if (logger.isTraceEnabled()) {
                String reqHdrsStr = Stream.of(httpget.getAllHeaders()).map(h -> h.getName() + ":" + h.getValue() + ":" +
                        Stream.of(h.getElements()).map(e -> e.getName())).collect(Collectors.joining(";"));
                logger.trace("sendRequestToSharePoint: preparing call to {} with headers {} (#{})", uri, reqHdrsStr, Integer.toHexString(hashCode()));
            }
        } else {
            if (signInHeaders != null) {
                Stream.of(signInHeaders).forEach(h -> httpget.setHeader(h));
//            httpget.setHeaders(signInHeaders);
            }
        }

        long start = System.currentTimeMillis();
        CloseableHttpResponse response = httpclient.execute(httpget, Service.getContext(url, userName));
        long duration = System.currentTimeMillis() - start;
        logger.debug("Response {} in {} ms for url: {}", response.getStatusLine(), duration, httpget.getRequestLine());
        handleCommonErrorStatusCodes(uri, response, false);
        return response;
    }

    private boolean isExpiring(Header[] signInHeaders) {
        String expiresStr = Stream.of(signInHeaders).filter(h -> h.getName().equals(EXPIRES_ON))
                .map(Header::getValue).findFirst().orElse(null);
        if (expiresStr == null) {
            logger.error("Missing expire_on header: {}", Stream.of(signInHeaders).map(h -> h.getName() + ":" + h.getValue()).collect(Collectors.joining(";")));
            return false;
        }
        Long expiresOn = Long.valueOf(expiresStr);
        long nowSec = System.currentTimeMillis()/1000;
        boolean res = (expiresOn - nowSec <= 10);
        if (res) {  // Allows 10 sec grace
            logger.debug("SignIn headers expiring: expire_on={} now={}", expiresOn, nowSec);
        }
        return res;
    }

    private void handleCommonErrorStatusCodes(String uri, CloseableHttpResponse response, boolean handle404) {
        int statusCode = response.getStatusLine().getStatusCode();
        switch (statusCode) {
            case 429:
                printErrorBody(response);
                throw new RateLimitException("Request denied at uri [" + uri + "]. 429 too many request at the same time");
            case 403:
                printErrorBody(response);
                throw new FailAccessException("Failed to access sharePoint at uri [" + uri + "] action was forbidden (403) " +
                        "from user " + userName);
            case 400:
                printErrorBody(response);
                throw new FailAccessException("Failed to access sharePoint at uri [" + uri + "]. bad request (400)");
            case 500:
                printErrorBody(response);
                throw new DataAccessException("Failed to access sharePoint at uri [" + uri + "]. " +
                        "internal server error (500): User cannot be found.");
            case 503:
                printErrorBody(response);
                throw new FailAccessException("Failed to access sharePoint at uri [" + uri + "]. Service unavailable (503)");
            case 404:
                if (handle404) {
                    throw new RuntimeException("Requested entity not found on SharePoint at uri [" + uri + "]. 404 not found");
                }
            default:
                if (statusCode != 200) {
                    logger.debug("Got status line: {}", response.getStatusLine());
                }
        }
    }

    private void printErrorBody(CloseableHttpResponse response) {
        if (response.getEntity() != null) {
            try {
                String msg = EntityUtils.toString(response.getEntity());
                logger.error(msg);
            } catch (IOException e) {
                logger.error("Failed to extract error body from error message", e);
            }
        }
    }

    private void createHttpClientIfNeeded() {
        isSharePointOnline = (!Strings.isNullOrEmpty(domain)) && domain.toLowerCase().equals(SP_ONLINE_DOMAIN);
        if (httpclient == null) {
            httpclient = createHttpClient();
        }
    }

    public void terminateHttpClient() {
        Optional.ofNullable(httpclient)
                .ifPresent(cli -> {
                    try {
                        cli.close();
                    } catch (IOException e) {
                        logger.error("Terminate http-client failed", e);
                    }
                });
    }

    private CloseableHttpClient createHttpClient() {
        long start = System.currentTimeMillis();
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        isSharePointOnline = (!Strings.isNullOrEmpty(domain)) && domain.toLowerCase().equals(SP_ONLINE_DOMAIN);

        Credentials credentials = (domain != null) ?
                (isSharePointOnline ?
                        null :
                        (new NTCredentials(userName, password, endpoint.toString(), domain))) :
                (new UsernamePasswordCredentials(userName, password));
        if (logger.isTraceEnabled()) {
            logger.trace("setting http credentials: domain={} user={} pass-len={} endpoint={} (#)",
                    (domain == null ? "null" : domain), userName, password.length(), endpoint==null?"null":endpoint.toString(), Integer.toHexString(hashCode()));
        }
        if (credentials != null) {
            credsProvider.setCredentials(new AuthScope(AuthScope.ANY), credentials);
        }
        HttpClientBuilder custom = HttpClients.custom();
        if (clientConnectionManager != null) {
            custom.setConnectionManager(clientConnectionManager);
        } else {
            //TODO
            PoolingHttpClientConnectionManager connPool = new PoolingHttpClientConnectionManager();
            connPool.setMaxTotal(200);
            connPool.setDefaultMaxPerRoute(200);
            custom.setConnectionManager(connPool);
        }
        RequestConfig config = RequestConfig.custom()
                .setTargetPreferredAuthSchemes(Arrays.asList(AuthSchemes.NTLM, AuthSchemes.KERBEROS, AuthSchemes.BASIC, AuthSchemes.DIGEST, AuthSchemes.SPNEGO))
                .setConnectTimeout(connectionConfig.getConnectionTimeout())
                .setConnectionRequestTimeout(connectionConfig.getConnectionRequestTimeout())
                .setSocketTimeout(connectionConfig.getSocketTimeout())
                .build();
        custom.setDefaultCredentialsProvider(credsProvider);
        BasicCookieStore cookieStore = new BasicCookieStore();
        custom.setDefaultCookieStore(cookieStore);
        custom.setRedirectStrategy(new LaxRedirectStrategy());
        custom.setUserAgent(userAgent);
        CloseableHttpClient httpClient = custom
                .setDefaultRequestConfig(config)
                .build();

        if (this.userName != null && this.userName.endsWith(".onmicrosoft.com")) {
            securityToken = fetchSecurityToken(httpClient);
        }
        if (isSharePointOnline) {
            /*
             * Sharepoint online registered application:
             *  client_id = username in the form of {{clientId}}@{{resourceId}}/{{tenantId}}   e.g 249a670e-aab8-43fd-9136-6ef0706e72c4@00000003-0000-0ff1-ce00-000000000000/5476672f-3933-4f33-bfb5-f73ad48a0112
             *  client_secret = password
             *
             *  grant_type:client_credentials
             *  resource:{{clientId}}/host@{{realm}}
             */
            logger.debug("Looking for SP Online registered app authentication: username={} domain={} host={} jshareService={}", userName, domain, host, jshareService==null?"null":"not-null");
            signInHeaders = getSignInSpOnlineAppTokenHeaders(httpClient, userName, password, host);
        } else if (securityToken != null) {
            signInHeaders = getSignInCookies(httpClient, securityToken);
        }

        logger.debug("Created SharePoint Http Client for user {} in {} ms", this.userName, System.currentTimeMillis() - start);
        return httpClient;
    }

    private Header[] getSignInSpOnlineAppTokenHeaders(CloseableHttpClient httpClient, String userName, String clientSecret, String host) {
        String body = null;
        String authRequestBody = "grant_type=client_credentials"
                + "&client_id=<client_id>@<tenant_id>"
                + "&client_secret=<client_secret>"
                + "&resource=<resource_id>/<host>@<tenant_id>";
        String authRequestUrl = "https://accounts.accesscontrol.windows.net/<tenant_id>/tokens/OAuth/2";

        // Username assumed to be in the form: application_id@resource_id/tenant_id
        String[] sp = userName.split("@",3);
        if (sp.length != 2) {
            logger.warn("Username {} should be in the form <client_id>@<resource_id>/<tenant_id>");
            return null;
        }
        String clientId = sp[0];
        String resourceTenantId = sp[1];
        sp = resourceTenantId.split("/",3);
        if (sp.length != 2) {
            logger.warn("Username {} should be in the form <client_id>@<resource_id>/<tenant_id>");
            return null;
        }
        String resourceId = sp[0];
        String tenantId = sp[1];

        authRequestBody = authRequestBody.replaceAll("<client_id>", clientId)
                .replaceAll("<tenant_id>", tenantId)
                .replaceAll("<host>", host)
                .replaceAll("<resource_id>", resourceId);
        logger.trace("Auth body={}", authRequestBody);
        try {
            authRequestBody = authRequestBody.replaceAll("<client_secret>", URLEncoder.encode(clientSecret, "UTF-8"));
        } catch (UnsupportedEncodingException e) {}
        authRequestUrl = authRequestUrl.replaceAll("<tenant_id>", tenantId);

        HttpResponse httpResponse = null;
        try {
            HttpPost request = new HttpPost(authRequestUrl);
            request.setEntity(new StringEntity(authRequestBody, ContentType.APPLICATION_FORM_URLENCODED));
            request.addHeader("Accept","application/xml;application/json;");
            request.setHeader("Content-Type", ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
            request.setHeader("Content-Language", "en-US");

            httpResponse = httpClient.execute(request, Service.getContext(url, userName));
            body = EntityUtils.toString(httpResponse.getEntity());
            if (logger.isTraceEnabled()) {
                String reqHdrsStr = Stream.of(request.getAllHeaders()).map(h->h.getName()+":"+h.getValue()).collect(Collectors.joining(";"));
                String respHdrsStr = Stream.of(httpResponse.getAllHeaders()).map(h->h.getName()+":"+h.getValue()).collect(Collectors.joining(";"));
                logger.trace("getSignInSpOnlineAppTokenHeaders: (#{}) status={}\nauthUrl={}\nrequestHeaders={}\nresponseHeaders={}\nrespbody={}",
                        Integer.toHexString(hashCode()), httpResponse.getStatusLine(), authRequestUrl, reqHdrsStr, respHdrsStr, body);
            }
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                JsonObject jsonBody = (new JsonParser()).parse(body).getAsJsonObject();
                String accessToken = jsonBody.get("access_token").getAsString();
                String tokenType = jsonBody.get("token_type").getAsString();
                String resource = jsonBody.get("resource").getAsString();
                BigInteger expiresOn = jsonBody.get(EXPIRES_ON).getAsBigInteger();
//              DEBUG:
//                logger.error("\n==============> Facing expiration in 2 minutes. Was {}\n", expiresOn);
//                expiresOn = BigInteger.valueOf(System.currentTimeMillis()/1000 + 2 * 60);
                if (logger.isDebugEnabled()) {
                    logger.debug("getSignInSpOnlineAppTokenHeaders: accessTokenLen={}, tokenType={}, expiresOn={} ({}), resource={}",
                            accessToken.length(), tokenType, expiresOn, sdf.format(new java.util.Date(expiresOn.longValue() * 1000)), resource);
                }
                //    Authorization:{{token_type1}} {{access_token1}}
                //    resource:{{clientId}}/docauthority.sharepoint.com
                //    expires_on:{{expires_on}}
                Header[] headers = {
                        (new BasicHeader("Authorization", tokenType + " " + accessToken))
                        , (new BasicHeader("resource", clientId + "/" + host))
                        , (new BasicHeader(EXPIRES_ON, expiresOn.toString()))
                };
                return headers;
            } else {
                logger.debug("getSignInSpOnlineAppTokenHeaders: status={}\nauthUrl={}\nrespbody={}", httpResponse.getStatusLine(), authRequestUrl, body);
                return null;
            }
        } catch (Exception e) {
            logger.error("Failed to getSignInSpOnlineAppTokenHeaders response {}", body, e);
            throw new RuntimeException("Failed to getSignInSpOnlineAppTokenHeaders SharePoint token XML response", e);
        } finally {
            if (httpResponse != null && httpResponse.getEntity() != null) {
                try {
                    httpResponse.getEntity().getContent().close();
                } catch (IOException e) {
                    logger.warn("getSignInSpOnlineAppTokenHeaders: Failed to close input-stream", e);
                }
            }
        }
    }

    private String createLegacyApiUri(String api) {
        return scheme + "://" + host + basePath + "/_vti_bin" + api;
    }

    public String createApiUri(String api, String subSite) {
        if (!StringUtils.isEmpty(subSite) && !subSite.startsWith("/")) {
            subSite = "/" + subSite;
        }
        return SharePointParseUtils.removeUnneededDoubleSlashes(endpoint.toString() + subSite + API_PATH + api);
    }

    /**
     * <html><head><title>Object moved</title></head><body>
     * <h2>Object moved to <a href="https://login.microsoftonline.com/login.srf?wa=wsignin1%2E0&amp;rpsnv=4&amp;ct=1481011588&amp;rver=6%2E1%2E6206%2E0&amp;wp=MBI&amp;wreply=https%3A%2F%2Fdocauthority%2Esharepoint%2Ecom%2F%5Fforms%2Fdefault%2Easpx%3Fapr%3D1&amp;lc=1033&amp;id=500046&amp;guests=1">here</a>.</h2>
     * </body></html>
     */
    private Header[] getSignInCookies(HttpClient httpClient, String securityToken) {
        logger.debug("Get SignInCookies");
        HttpResponse httpResponse = null;
        try {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(scheme).append("://").append(host);
            if (port != null) {
                stringBuilder.append(":").append(port);
            }
            stringBuilder.append("/_forms/default.aspx?wa=wsignin1.0");
            String uri = stringBuilder.toString();
            HttpPost request = new HttpPost(uri);
            ContentType contentType = ContentType.create("application/x-www-form-urlencoded", "utf-8");
            HttpEntity entity = new StringEntity(securityToken, contentType);
            request.setEntity(entity);
            httpResponse = httpClient.execute(request, Service.getContext(url, userName));
            Header[] headers = httpResponse.getHeaders("Set-Cookie");
            String body = EntityUtils.toString(httpResponse.getEntity());
            if (headers.length == 0) {
                logger.debug(body);
                throw new RuntimeException("Unable to sign in: no cookies returned in response (code: " + httpResponse.getStatusLine() + " body: " + body + ")");
            } else {
                boolean rtFaFound = false;
                for (Header header : headers) {
                    logger.debug("Got Set-Cookie: {}", header.getValue());
                    if (header.getValue() != null && header.getValue().startsWith("rtFa")) {
                        rtFaFound = true;
                    }
                }
                if (!rtFaFound) {
                    logger.warn("RtFa cookie missing from SharePoint 365 authentication");
                    logger.debug(body);
                }
            }
            return headers;
            //            stringBuilder.append("/_forms/login.srf?wa=wsignin1.0");
//            RequestEntity<String> requestEntity = null;
//            requestEntity = new RequestEntity<>(securityToken, HttpMethod.POST, new URI(uri.toString()));
//            ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);
//            List<String> cookies = responseEntity.getHeaders().get("Set-Cookie");
//            if (CollectionUtils.isEmpty(cookies)) {
//                throw new RuntimeException("Unable to sign in: no cookies returned in response (code: "+responseEntity.getStatusCode()+" body: "+responseEntity.getBody()+")");
//            }
//            httpget.setHeader("Cookie", Joiner.on(';').join(signInCookies));
//            return cookies;
        } catch (Exception e) {
            throw new RuntimeException("Failed to get signIn cookies (" + url + ")", e);
        } finally {
            if (httpResponse != null && httpResponse.getEntity() != null) {
                try {
                    httpResponse.getEntity().getContent().close();
                } catch (IOException e) {
                    logger.warn("getSignInCookies: Failed to close input-stream. url={}", url, e);
                }
            }
        }
    }

    private String fetchSecurityToken(HttpClient httpClient) {
        String body = null;
        String authRequestBody = "<S:Envelope xmlns:S=\"http://www.w3.org/2003/05/soap-envelope\"" +
                " xmlns:wsa=\"http://www.w3.org/2005/08/addressing\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\"" +
                " xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\"" +
                " xmlns:wst=\"http://schemas.xmlsoap.org/ws/2005/02/trust\"" +
                " xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
                "<S:Header><wsa:Action S:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</wsa:Action>" +
                "<wsa:To S:mustUnderstand=\"1\">https://login.microsoftonline.com/rst2.srf</wsa:To>" +
                "<wsse:Security><wsse:UsernameToken wsu:Id=\"user\">" +
                "<wsse:Username>" + this.userName + "</wsse:Username>" +
                "<wsse:Password>" + this.password + "</wsse:Password></wsse:UsernameToken>" + "</wsse:Security>" +
                "</S:Header>" + "<S:Body><wst:RequestSecurityToken Id=\"RST0\">" +
                "<wst:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</wst:RequestType><wsp:AppliesTo>" +
                "<wsa:EndpointReference><wsa:Address>" + url + "</wsa:Address></wsa:EndpointReference>" +
                "</wsp:AppliesTo><wsp:PolicyReference URI=\"MBI\" />" +
                "</wst:RequestSecurityToken></S:Body>" + "</S:Envelope>";
        HttpPost request = new HttpPost("https://login.microsoftonline.com/extSTS.srf");
        request.setEntity(new StringEntity(authRequestBody, ContentType.TEXT_XML));
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(request, Service.getContext(url, userName));
            body = EntityUtils.toString(httpResponse.getEntity());
            if (logger.isTraceEnabled()) {
                String reqHdrsStr = Stream.of(request.getAllHeaders()).map(h->h.getName()+":"+h.getValue()).collect(Collectors.joining(";"));
                String respHdrsStr = Stream.of(httpResponse.getAllHeaders()).map(h->h.getName()+":"+h.getValue()).collect(Collectors.joining(";"));
                logger.trace("fetchSecurityToken: (#{}) status={}\nrequestHeaders={}\nresponseHeaders={}\nbody={}",
                        Integer.toHexString(hashCode()), httpResponse.getStatusLine(), reqHdrsStr, respHdrsStr, body);
            }
            return SharePointParseUtils.extractToken(body);
        } catch (URISyntaxException e) {
            throw new RuntimeException("Failed to create valid URI", e);
        } catch (Exception e) {
            logger.error("Failed to parse XML response {}", body, e);
            throw new RuntimeException("Failed to parse SharePoint token XML response", e);
        } finally {
            if (httpResponse != null && httpResponse.getEntity() != null) {
                try {
                    httpResponse.getEntity().getContent().close();
                } catch (IOException e) {
                    logger.warn("fetchSecurityToken: Failed to close input-stream", e);
                }
            }
        }
    }

    @SuppressWarnings("unused")
    HttpClientConnectionManager getClientConnectionManager() {
        return clientConnectionManager;
    }

    void setClientConnectionManager(HttpClientConnectionManager clientConnectionManager) {
        this.clientConnectionManager = clientConnectionManager;
    }

    @SuppressWarnings("SameParameterValue")
    private CloseableHttpResponse sendRequestToSharePoint(String api) throws Exception {
        return sendRequestToSharePoint(api, StringUtils.EMPTY);
    }

    private CloseableHttpResponse sendRequestToSharePoint(String api, String subSite) throws Exception {
        String subsite = Optional.ofNullable(subSite)
                .orElse(StringUtils.EMPTY);
        return retryTemplate.execute(context -> {
            if (context.getRetryCount() > 0) {
                logger.debug("Retry {}, executing api {} for subSite: {}",
                        context.getRetryCount(), api, subSite, context.getLastThrowable());
            }
            return doSendRequestToSharePoint(api, subsite);
        });
    }

    private CloseableHttpResponse sendLegacyRequestToSharePoint(String api) throws IOException {
        return sendLegacyRequestToSharePoint(api, maxRetries);
    }


    /**
     * Fetch item id.
     * This is not media item id, it's the secondary part (item id withing the list).
     * mediaItemId format "listId/itemId"
     */
    public String getFolderMediaItemId(String subSite, String path) throws ServiceException {
        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/_api/Web/
        // GetFolderByServerRelativeUrl('/sites/test/Shared%20Documents/personal')/ListItemAllFields?$select=id
        String s1 = SharePointParseUtils.encodeUrlWithSlash(path);
        String baseUrl = getFolderApiCall + s1 + "')/ListItemAllFields";
        String apiUrl = baseUrl + "?$select=id";
        String response;
        try {
            response = sendRequest(apiUrl, subSite, SharePointParseUtils::extractMainId);
        } catch (RuntimeException e) {
            logger.debug("Failed to get folder metadata item ID, apiURL: {}, subSite: {}, path: {}", apiUrl, subSite, path);
            throw e;
        }
        return response;
    }

    public SharePointListItemPage getListItems(String apiUrl) throws ServiceException {
        return sendRequest(apiUrl, is -> {
            try {
                return SharePointParseUtils.parseSharePointListItems(is);
            } catch (Exception e) {
                logger.error("Failed to parse XML: {}");
                throw new ServiceException("Failed to parse XML", e, apiUrl);
            }
        });
    }

    public SharePointListItemPage getListItems(String listId, List<IQueryOption> queryOptions) throws
            ServiceException {
        String apiUrl = "/lists('" + listId + "')/items" + SharePointParseUtils.createQueryOptionsUrl(queryOptions);
        return getListItems(apiUrl);
    }

    void enrichMediaChangeLogWithListItemData(MediaChangeLogDto log) throws ServiceException, FileNotFoundException {
        String[] idParts = log.getMediaItemId().split("/");
        final SharePointListItem listItem = getListItem(null, idParts[0], idParts[1]);
        if (listItem == null) {
            return;
        }
        log.setFolder(FileSystemObjectType.FOLDER.equals(listItem.getFileSystemObjectType()));
        log.setCreationTimeMilli(SharePointParseUtils.getTimeInMillis(listItem.getCreated(), true));
        log.setModTimeMilli(SharePointParseUtils.getTimeInMillis(listItem.getModified(), true));
        log.setOwnerName(listItem.getLoginName());
        log.setFileName(convertFileRefToFileUrl(listItem.getFileRef()));
        log.setFileSize(listItem.getSize());
    }

    public SharePointListItem getListItem(String subSite, String listId, String itemId) throws
            ServiceException, FileNotFoundException {
        return getListItem(MSItemKey.listItem(subSite, listId, itemId));
    }

    public SharePointListItem getListItem(MSItemKey listItemKey) throws ServiceException, FileNotFoundException {
        List<IQueryOption> queryOptions = new ArrayList<>();
        addDefaultListItemQueryOptions(queryOptions);
        String apiUrl = "/lists('" + listItemKey.getListId() + "')/items(" + listItemKey.getItemId() + ")" + SharePointParseUtils.createQueryOptionsUrl(queryOptions);
        try (CloseableHttpResponse response = sendRequestToSharePoint(apiUrl, listItemKey.getSite())) {
            if (response.getStatusLine().getStatusCode() != 200) {
                logger.debug("Got status line: {}", response.getStatusLine());
            }
            if (response.getStatusLine().getStatusCode() == 404) {
                return null; //File not found.
            }
            InputStream content = response.getEntity().getContent();
            CharacterFilterInputStream charFilter = new CharacterFilterInputStream(content, charsToFilter);
            return SharePointParseUtils.parseSharePointListItem(charFilter);
        } catch (IOException e) {
            logger.error("Failed to execute HTTP client request to {}", apiUrl, e);
            throw new ServiceException("Failed to execute HTTP client request to:" + apiUrl, e, apiUrl);
        } catch (Exception e) {
            logger.error("Failed to parse XML", e);
            throw new FileNotFoundException(e.getMessage());
        }
    }

    public void addDefaultListItemQueryOptions(Collection<IQueryOption> queryOptions) {
        queryOptions.add(new Expand("File/Author,File/ServerRelativeUrl,FieldValuesAsText"));
        queryOptions.add(new Select("*", "HasUniqueRoleAssignments"));
    }

    public List<SharePointRoleAssignment> getListPermissions(String subSite, String listId) throws ServiceException {
        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/_api/web/lists('97b2f9ca-6440-4909-9034-48cb272ca838')/RoleAssignments?$expand=Member,RoleDefinitionBindings&$select=Member/loginName,RoleDefinitionBindings/roletypekind
        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/_api/web/lists('97b2f9ca-6440-4909-9034-48cb272ca838')/roleAssignments
//        String url = "_api/web/lists(\'" + listId + "\')/RoleAssignments?$expand=Member";
//        String url = "_api/web/lists(\'" + listId + "\')/roleAssignments?$expand=Member,RoleDefinitionBindings&$select=Member/loginName,RoleDefinitionBindings/roletypekind";
        String baseUrl = "/lists('" + listId + "')/roleAssignments";
        String apiUrl = baseUrl + "?$expand=Member,RoleDefinitionBindings&$select=Member/loginName,RoleDefinitionBindings/roletypekind";
        try (CloseableHttpResponse response = sendRequestToSharePoint(apiUrl, subSite)) {
            logger.trace("Got status line: {} (#{})", response.getStatusLine(), Integer.toHexString(hashCode()));
            InputStream contentInputStream = response.getEntity().getContent();
            if (logger.isTraceEnabled()) {
                String content = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
                logger.trace("Received xml: {}", content);
                contentInputStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
            }
            return SharePointParseUtils.parseRoleAssignments(contentInputStream);
        } catch (IOException e) {
            logger.error("Failed to execute HTTP client request to {}", apiUrl, e);
            throw new ServiceException("Failed to execute HTTP client request to:" + apiUrl, e, apiUrl);
        } catch (Exception e) {
            throw new ServiceException("Failed to parse XML", e, apiUrl);
        }
    }

    public List<SharePointRoleAssignment> getListItemPermissions(MSItemKey listItemKey) throws ServiceException {
        String baseUrl = "/lists('" + listItemKey.getListId() + "')/items(" + listItemKey.getItemId() + ")/RoleAssignments";
        String apiUrl = baseUrl + "?$expand=Member,RoleDefinitionBindings&$select=Member/loginName,RoleDefinitionBindings/roletypekind";
        try (CloseableHttpResponse response = sendRequestToSharePoint(apiUrl, listItemKey.getSite())) {
            logger.trace("Got status line: {}", response.getStatusLine());
            InputStream contentInputStream = response.getEntity().getContent();
            if (logger.isTraceEnabled()) {
                String content = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);
                logger.trace("Received xml: {}", content);
                contentInputStream = new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
            }
            return SharePointParseUtils.parseRoleAssignments(contentInputStream);
        } catch (IOException e) {
            logger.error("Failed to execute HTTP client request to {}", apiUrl, e);
            throw new ServiceException("Failed to execute HTTP client request to:" + apiUrl, e, apiUrl);
        } catch (Exception e) {
            logger.error("Failed to execute HTTP client request to {}", apiUrl, e);
            throw new ServiceException("Failed to parse XML", e, apiUrl);
        }
    }

    @SuppressWarnings("unused")
    public long getLibraryItemCount(String subSite, String library) throws FileNotFoundException {
        String libraryTitle = getLibraryTitle(subSite, library);
        logger.debug("Get library {} (internal: {}) item count", library, libraryTitle);
        try (CloseableHttpResponse response = sendLegacyRequestToSharePoint("/ListData.svc/" + libraryTitle + "/$count")) {
            logger.debug("Got status line: {}", response.getStatusLine());
            InputStream content = response.getEntity().getContent();
            String result = IOUtils.toString(content, "UTF-8");
            logger.debug("Got {} items in the library {}", result, library);
            return Long.valueOf(result);
        } catch (IOException e) {
            throw new FileNotFoundException("Failed to get library " + library + " item count");
        }
    }

    /**
     * @param path - includes the basePath (/test/basic)
     */
    private String getLibraryTitle(String subSite, String path) throws FileNotFoundException {
        try {
            return getSharePointExtendedFolderDetails(subSite, path).getListTitle();
        } catch (ServiceException e) {
            throw new FileNotFoundException("Failed to find path" + path + " due to " + e.getMessage());
        }
    }

    /**
     * Path includes the basePath
     * * https://docauthority.sharepoint.com/tests/_api/web/GetFolderByServerRelativeUrl('/tests/Missoula%20SubZone/CitySub')?$expand=Folders,ListItemAllFields&$select*
     */
    public List<SharePointExtendedFolder> listFolders(String subSite, String path) throws ServiceException {
        String encodedPath = SharePointParseUtils.encodeUrlWithSlash(path);
        String apiUrl = getFolderApiCall + encodedPath + "')/Folders?$expand=Properties";
        return sendRequest(apiUrl, subSite, (is) -> {
            try {
                return SharePointParseUtils.parseFolders(is);
            } catch (Exception e) {
                logger.error("Failed to parse data");
                throw new ServiceException("Failed to parse XML", e, apiUrl);
            }
        });
    }

    /**
     * @param path - includes the basePath (/test/basic)
     */
    protected SharePointExtendedFolder getSharePointExtendedFolderDetails(String subSite, String path) throws
            ServiceException {
        String encodedPath = SharePointParseUtils.encodeUrlWithSlash(path);
        String apiUrl = getFolderApiCall + encodedPath + "')/Properties";
        return sendRequest(apiUrl, subSite, (is) -> {
            try {
                return SharePointParseUtils.parseFolderProperties(is);
            } catch (Exception e) {
                if (RuntimeException.class.isInstance(e)) {
                    // we want to throw real exception where relevant
                    // and not wrap it so we can fail the scan if needed
                    throw (RuntimeException)e;
                } else {
                    logger.error("Failed to parse data");
                    throw new ServiceException("Failed to parse response", e, apiUrl);
                }
            }
        });
    }

    <T> T sendRequest(String apiUrl, SharePointServiceFunction<InputStream, T> responseHandler) throws
            ServiceException {
        return sendRequest(apiUrl, "", responseHandler);
    }

    <T> T sendRequest(String apiUrl, String subSite, SharePointServiceFunction<InputStream, T> responseHandler) throws
            ServiceException {
        return sendRequest(apiUrl, subSite, responseHandler, false);
    }

    private <T> T
    sendRequest(String apiUrl, String subSite, SharePointServiceFunction<InputStream, T> responseHandler,
                boolean handle404) throws ServiceException {
        try (CloseableHttpResponse response = sendRequestToSharePoint(apiUrl, subSite)) {
            handleCommonErrorStatusCodes(apiUrl, response, handle404);
            return responseHandler.apply(response.getEntity().getContent());
        } catch (Exception e) {
            if (e instanceof RuntimeException) {
                // we want to throw real exception where relevant
                // and not wrap it so we can fail the scan if needed
                throw (RuntimeException)e;
            } else {
                logger.error("Failed to execute HTTP client request to {} on path {}", apiUrl, subSite, e);
                throw new ServiceException("Failed to execute HTTP client request to:" + apiUrl, e, apiUrl);
            }
        }
    }

    public String convertFileRefToFileUrl(String value) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(scheme).append("://").append(host);
        if (port != null) {
            stringBuilder.append(":").append(port);
        }
        if (!value.startsWith("/")) {
            stringBuilder.append("/");
        }
        stringBuilder.append(value);
        return stringBuilder.toString();
    }

    public ClaFilePropertiesDto getFilesAttributes(String subSite, String fileName) throws ServiceException {
        logger.debug("Retrieving attributes for file {}", fileName);
        String apiUrl = getFileApiCall + Util.encodeUrl(fileName) + "')?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties&$expand=ListItemAllFields,Properties";
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charsToFilter)) {
                return SharePointParseUtils.convertFileItemFileProperty(streamFilter);
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Failed to parse response XML", e);
                throw new ServiceException(String.format("Failed to parse response XML for subSite: %s", subSite)
                        , e, apiUrl);
            }
        });
    }

    public List<ClaFilePropertiesDto> getFilesWithMediaItemId(String subSite, String folder, String listId,
                                                              int pageIdx, int pageSize) throws ServiceException {
        logger.debug("Listing files under {} (page index: {}, page size: {})", folder, pageIdx, pageSize);
//        "/_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl("/Shared Documents/qwe") + "')/Files?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties&$expand=ListItemAllFields,Properties"
        String apiUrl = getFolderApiCall + Util.encodeUrl(folder) + "')/Files?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties&$expand=ListItemAllFields,Properties&$top=" + pageSize + "&$skip=" + (pageIdx * pageSize);
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charsToFilter)) {
                List<ClaFilePropertiesDto> files = SharePointParseUtils.convertFileItemListToFileList(streamFilter);
                files.stream()
                        .peek(file -> file.setFileName(convertFileRefToFileUrl(file.getFileName())))
                        .filter(file -> !file.getMediaItemId().contains("/"))
                        .forEach(file ->
                                file.setMediaItemId(
                                        Optional.ofNullable(listId)
                                                .map(lid -> SharePointParseUtils.calculateMediaItemId(subSite, lid, file.getMediaItemId()))
                                                .orElse(file.getMediaItemId())));

                return files;
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Failed to parse response XML", e);
                throw new ServiceException(String.format("Failed to parse response XML for " +
                        "subSite: %s, folder: %s, listId; %s", subSite, folder, listId), e, apiUrl);
            }
        });
    }

    AclInheritanceType getFileAclInheritanceType(String subSite, String filename) throws ServiceException {
        logger.debug("Retrieving file's {} Acl Inheritance Type", filename);
//        "/_api/web/GetFolderByServerRelativeUrl('" + Util.encodeUrl("/Shared Documents/qwe") + "')/Files?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties&$expand=ListItemAllFields,Properties"
        String apiUrl = getFileApiCall + Util.encodeUrl(filename) + "')?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties,ListItemAllFields/HasUniqueRoleAssignments&$expand=ListItemAllFields,RoleDefinitionBindings,Properties";
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charsToFilter)) {
                ClaFilePropertiesDto file = SharePointParseUtils.convertFileItemFileProperty(streamFilter);
                return file.getAclInheritanceType();
            } catch (RuntimeException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Failed to parse response XML", e);
                throw new ServiceException("Failed to parse response XML", e, apiUrl);
            }
        });
    }

    private String getFormDigestValue() throws ServiceException {
        if (formDigest != null && formDigestExpiration > System.currentTimeMillis()) { // valid token
            return formDigest;
        }
        logger.debug("Obtaining form-digest token");
        HttpPost post = new HttpPost(url + "/_api/contextinfo");
        post.setHeader("Accept", "application/json;odata=verbose");
        logger.info("Calling  createHttpClientIfNeeded()");
        createHttpClientIfNeeded();
        String contextInfo;

        try (CloseableHttpResponse response = httpclient.execute(post, Service.getContext(url, userName))) {
            contextInfo = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
        } catch (IOException ioe) {
            logger.error("Failed to retrieve form digest value", ioe);
            throw new ServiceException("Failed to retrieve form digest value", ioe, "contextInfo");
        }

        int beginIdx = contextInfo.indexOf("FormDigestValue");
        if (beginIdx == -1) {
            throw new RuntimeException("Failed to extract form digest value (Property not found)");
        }
        beginIdx += "FormDigestValue".length() + 3;

        String digestValue = contextInfo.substring(beginIdx, contextInfo.indexOf("\"", beginIdx));
        int expirationIndex = contextInfo.indexOf("FormDigestTimeoutSeconds") + "FormDigestTimeoutSeconds".length() + 2;
        String expirationOfst = contextInfo.substring(expirationIndex, contextInfo.indexOf(',', expirationIndex));
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, Integer.parseInt(expirationOfst));

        this.formDigest = digestValue;
        this.formDigestExpiration = cal.getTimeInMillis();

        return this.formDigest;
    }

    List<MediaChangeLogDto> getChangeLogForFolder(String folder, long fromDate, int pageIdx, int pageSize) throws
            ServiceException {
        String fromDateStr;
        try {
            fromDateStr = URLEncoder.encode(Instant.ofEpochMilli(fromDate).toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("Failed to encode to UTF-8", e);
            throw new RuntimeException(e);
        }
        logger.debug("Listing changes for {} since {} (page index: {}, page size: {})", folder, fromDate, pageIdx, pageSize);
        String uri = String.format("%s/_api/web/GetFolderByServerRelativeUrl('%s')/getchanges?$select=*&$top=%d&$skip=%d&$orderby.desc=Time&$expand=ListItemAllFields,Properties&$filter=%sdatetime'%s'",
                url,
                Util.encodeUrl(folder),
                pageSize,
                pageSize * pageIdx,
                Util.encodeUrl("Time ge "),
                fromDateStr);

        HttpPost post = new HttpPost(uri);
        post.setHeader("X-RequestDigest", getFormDigestValue());
        post.setHeader("Content-Type", "application/json;odata=verbose");

        ChangeQuery query = new ChangeQuery();
        query.setItem(true);
        query.setDelete(true);
        query.setMove(true);
        query.setAdd(true);
        query.setUpdate(true);
        query.setRestore(true);
        query.setRename(true);
        query.setRoleAssignmentAdd(true);
        query.setRoleAssignmentDelete(true);

        BasicHttpEntity body = new BasicHttpEntity();
        body.setContent(new ByteArrayInputStream(query.toString().getBytes(StandardCharsets.UTF_8)));
        post.setEntity(body);

        try (CloseableHttpResponse response = httpclient.execute(post, Service.getContext(url, userName));
             InputStream is = response.getEntity().getContent()) {

            handleCommonErrorStatusCodes(post.getURI().toString(), response, true);
            return SharePointParseUtils.convertToMediaChangeLog(is);
        } catch (Exception e) {
            if (RuntimeException.class.isInstance(e)) {
                // we want to throw real exception where relevant
                // and not wrap it so we can fail the scan if needed
                throw (RuntimeException)e;
            } else {
                throw new ServiceException("Failed to retrieve change log", e, post.getURI().toString());
            }
        }
    }

    public int getVersion() {
        try (CloseableHttpResponse response = sendRequestToSharePoint("")) {
            Header headers[] = response.getHeaders(VERSION_HEADER);
            Optional<String> versionHeader = Arrays.stream(Objects.requireNonNull(headers))
                    .map(Header::getValue)
                    .filter(Objects::nonNull)
                    .findAny();

            if (!versionHeader.isPresent()) {
                logger.error("Couldn't determine SP version: - header not found - returning default {}", VERSION_DEFAULT);
                return VERSION_DEFAULT;
            }

            String val = versionHeader.get();
            int dotIdx = val.indexOf(".");
            if (dotIdx == -1) {
                logger.error("Invalid header value - using default {}", VERSION_DEFAULT);
                return VERSION_DEFAULT;
            }
            switch (Integer.valueOf(val.substring(0, dotIdx))) {
                case 14:
                    return 2010;
                case 15:
                    return VERSION_2013;
                case 16:
                    return VERSION_DEFAULT;
                default:
                    logger.error("Received unknown version identifier: {}. Using default({})", val, VERSION_DEFAULT);
                    return VERSION_DEFAULT;
            }
        } catch (Exception e) {
            logger.error("Couldn't determine SP version, using default: " + VERSION_DEFAULT, e);
            return VERSION_DEFAULT;
        }
    }

    String getFileOwner(String subSite, String fileName) throws ServiceException {
        String api = getFileApiCall + Util.encodeUrl(fileName) + "')/Author";
        return sendRequest(api, subSite, is -> {
            CharacterFilterInputStream filteredStream = new CharacterFilterInputStream(is, charsToFilter);
            try {
                ClaFilePropertiesDto prop = SharePointParseUtils.convertFileItemFileProperty2013(filteredStream);
                return prop.getOwnerName();
            } catch (Exception e) {
                logger.error("Failed to get file owner", e);
            }
            return null;
        });
    }

    public List<ServerResourceDto> listSubSitesUnderSubSite(@NotNull String subSite) throws ServiceException {
        if (SharePointParseUtils.pathsEquals(basePath, subSite)) {
            subSite = null;
        } else if (!(StringUtils.EMPTY.equals(basePath) || "/".equals(basePath)) && subSite.contains(basePath)) {
            subSite = subSite.replace(basePath, "");
        }
        final String siteTemp = subSite;
        String api = "/webs";
        return sendRequest(api, subSite, is -> {
            CharacterFilterInputStream cfis = new CharacterFilterInputStream(is, charsToFilter);
            try {
                String sitesXml = IOUtils.toString(cfis, StandardCharsets.UTF_8);
                logger.trace("Received XML: {}", sitesXml);
                List<ServerResourceDto> sites = SharePointParseUtils.extractSubSites(new ByteArrayInputStream(sitesXml.getBytes(StandardCharsets.UTF_8)));
                sites.forEach(site -> site.setFullName(convertFileRefToFileUrl(site.getFullName())));
                return sites;
            } catch (Exception e) {
                logger.error("Failed to list sub-sites under " + siteTemp, e);
                return Lists.newArrayList();
            }
        });
    }


    // http://ec2-35-165-166-205.us-west-2.compute.amazonaws.com:8080/sanuk/ChiefDataOffice/_api/web/webs
    public List<ServerResourceDto> listSubSites(String parentSite) throws ServiceException {
//        ==> http://ec2-35-165-166-205.us-west-2.compute.amazonaws.com:8080/mang-path/site/<parentSite>/_api/web/webs
        String subSite = parentSite.substring(basePath.length());
        return listSubSitesUnderSubSite(subSite);
    }

    public List<ServerResourceDto> listSubSitesFolders(String subSite, String relPath) throws ServiceException {
        //http://ec2-35-165-166-205.us-west-2.compute.amazonaws.com:8080/path/site/<subsite>/_api/web/folders
        String api = "/folders";
        return sendRequest(api, subSite, is -> {
            try {
                String content = IOUtils.toString(new CharacterFilterInputStream(is, charsToFilter), StandardCharsets.UTF_8);
                logger.trace("Received xml: {}", content);
                List<ServerResourceDto> folders = SharePointParseUtils.extractSubSiteFolders(new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8)));
                folders.forEach(folder -> folder.setFullName(convertFileRefToFileUrl(folder.getFullName())));
                return folders;
            } catch (Exception e) {
                logger.error("Failed to list folders under sub-site {}", relPath);
                return Lists.newArrayList();
            }
        });
    }

    public boolean isValidSubSiteEndpoint(String path) {
        boolean isValid;
        try {
            isValid = sendRequest("", path, is -> true, true);
            logger.debug("Path {} is a valid site");
        } catch (Exception e) {
            logger.debug("Path {} isn't a valid site {}", e.getMessage());
            isValid = false;
        }
        return isValid;
    }

    public ClaFilePropertiesDto getFileAcl(String subSite, String filename) throws ServiceException {
        String path = filename.substring(0, filename.lastIndexOf("/") + 1);
        String apiUrl = getFolderApiCall + Util.encodeUrl(path) + "')/Files('" + Util.encodeUrl(filename.substring(path.length())) + "')?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties,ListItemAllFields/HasUniqueRoleAssignments&$expand=ListItemAllFields,RoleDefinitionBindings,Properties";
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream cFIS = new CharacterFilterInputStream(is, charsToFilter)) {
                return SharePointParseUtils.convertFileItemFileProperty(cFIS);
            } catch (IOException | JDOMException e) {
                logger.error("Failed to fetch file acls");
                throw new RuntimeException(e);
            }
        });
    }

    public ClaFilePropertiesDto getFileMediaItemId(String subSite, String path) throws ServiceException {
        String apiUrl = getFileApiCall + Util.encodeUrl(path) + "')?$select=ListItemAllFields/ID,ListItemAllFields/FileRef,Properties&$expand=ListItemAllFields,Properties";
        return sendRequest(apiUrl, subSite, (is) -> {
            try (CharacterFilterInputStream cFIS = new CharacterFilterInputStream(is, charsToFilter)) {
                ClaFilePropertiesDto props = SharePointParseUtils.extractFileMediaItemId(cFIS);
                Optional.ofNullable(subSite)
                        .ifPresent(site -> props.setMediaItemId(
                                SharePointParseUtils.calculateMediaItemId(site, props.getMediaItemId())));

                return props;
            } catch (IOException | JDOMException e) {
                logger.error("Failed to fetch file acls");
                throw new RuntimeException(e);
            }
        });
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setJShareService(Service service) {
        this.jshareService = service;
    }

    public Header[] getAppSignInHeaders(CloseableHttpClient hc) {
        if (!isSharePointOnline) {
            return null;
        }
        if (signInHeaders == null || isExpiring(signInHeaders)) {
            signInHeaders = getSignInSpOnlineAppTokenHeaders(hc, userName, password, host);
        }
        return signInHeaders;
    }
}

/*

    private final boolean followPostRedirects = true;
    private String objectId = "1d2df786-8ef7-437a-acc3-c4153bca2d22";

    private final static String AUTHORITY_URL2 = "https://login.windows.net/common";

    public static final String AUTHORITY_URL = "https://login.microsoftonline.com/common";
    public static final String AUTHORIZATION_ENDPOINT = "/oauth2/authorize";
    public static final String TOKEN_ENDPOINT = "/oauth2/token";
    // Update these two constants with the values for your application:
    public static String CLIENT_ID = "217f4822-5b1d-432a-8ad3-fcdca6824ffe";
    public static final String REDIRECT_URI = "com.microsoft.graph://connect/oauth/redirect";
    public static final String MICROSOFT_GRAPH_API_ENDPOINT_RESOURCE_ID = "https://graph.microsoft.com/";
*/