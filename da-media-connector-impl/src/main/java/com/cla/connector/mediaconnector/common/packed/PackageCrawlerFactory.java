package com.cla.connector.mediaconnector.common.packed;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.utils.FileNamingUtils;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;


/**
 * Provides package files (i.e. zip, arj ...) crawlers
 */
public class PackageCrawlerFactory {

    private static final Logger logger = LoggerFactory.getLogger(PackageCrawlerFactory.class);
    private static Map<String, Supplier<PackageCrawler>> sequentialPackageCrawlerSupplierRegistry = ImmutableMap.of("zip", LinearZipCrawler::new);

    public static Optional<PackageCrawler> getPackageCrawler(String path) {
        Optional<Supplier<PackageCrawler>> packageCrawlerCreator = getPackageCrawler(sequentialPackageCrawlerSupplierRegistry, path);
        Supplier<PackageCrawler> packageCrawlerSupplier = packageCrawlerCreator.orElse(null);
        return packageCrawlerCreator.isPresent() ? Optional.of(packageCrawlerSupplier.get()) : Optional.empty();
    }

    private static <T> Optional<T> getPackageCrawler(Map<String, T> packageCrawlerRegistry, String path) {
		Preconditions.checkNotNull(path, "path argument cannot be null");
		String filenameExtension = FileNamingUtils.getFilenameExtension(path);
		final T crawler = packageCrawlerRegistry.get(filenameExtension.toLowerCase());
		if (crawler == null){
		    logger.warn("Package crawler not found for file:" + path);
        }
        return Optional.ofNullable(crawler);
    }

    public static Optional<PackageCrawler> getPackageCrawler(ClaFilePropertiesDto parentDetails){
    	return getPackageCrawler(parentDetails.getFileName());
	}
}
