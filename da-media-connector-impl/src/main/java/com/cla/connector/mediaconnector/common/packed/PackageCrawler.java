package com.cla.connector.mediaconnector.common.packed;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 *  Provide functionality to crawl inside a package file
 **/
public interface PackageCrawler {
	void crawl(ClaFilePropertiesDto packageFileProperties, Predicate<? super String> fileTypesPredicate, Consumer<ClaFilePropertiesDto> packageEntryConsumer) throws PackageCrawlException;

    FileContentDto extractPackagedEntry(String packageFile, String entryId);
    ClaFilePropertiesDto extractPackagedEntryProperties(ClaFilePropertiesDto packageFileProperties, String entryId);
}
