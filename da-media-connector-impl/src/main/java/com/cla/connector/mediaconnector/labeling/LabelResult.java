package com.cla.connector.mediaconnector.labeling;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by: yael
 * Created on: 7/11/2019
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@type")
public interface LabelResult {
}
