package com.cla.connector.mediaconnector.box;

import com.cla.connector.media.scan.RecursiveActionBase;
import com.cla.connector.media.scan.RecursiveActionBuilderBase;

import java.util.List;

public class BoxCrawlRecursiveAction extends RecursiveActionBase<String> {
	private BoxMediaConnector boxMediaConnector;

	@Override
	protected void compute() {
		List<BoxCrawlRecursiveAction> boxCrawlRecursiveActions = boxMediaConnector.expandFolder(mediaEntityId, scanParams, filePropsConsumer,
				dirListingConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
		if(scanParams.isFstLevelOnly()){
			boxCrawlRecursiveActions.stream().map(rec -> boxMediaConnector.constructPathWithMediaEntityId(rec.getPath(), rec.getMediaEntityId()))
					.forEach(createScanTaskConsumer::accept);
		}else{
			invokeAll(boxCrawlRecursiveActions);
		}
	}

	public static class Builder extends RecursiveActionBuilderBase<BoxCrawlRecursiveAction, String> {
		private BoxMediaConnector boxMediaConnector;
		public static Builder create() {
			return new Builder();
		}

		public Builder withMediaConnector(BoxMediaConnector boxMediaConnector) {
			this.boxMediaConnector = boxMediaConnector;
			return this;
		}

		@Override
		public BoxCrawlRecursiveAction build() {
			BoxCrawlRecursiveAction action = new BoxCrawlRecursiveAction();
			action.boxMediaConnector = boxMediaConnector;
			//action.listId = listId;
			return super.build(action);
		}
	}
}
