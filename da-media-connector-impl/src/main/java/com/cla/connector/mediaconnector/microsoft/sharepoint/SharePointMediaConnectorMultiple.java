package com.cla.connector.mediaconnector.microsoft.sharepoint;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.microsoft.MSAppInfo;
import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;


public class SharePointMediaConnectorMultiple extends SharePointMediaConnector {
    private static final String SCAN_MODE_ANNOUNCEMENT = "***~~~******~~~******~~~*** MULTI-MAP MODE - mapping {} times ***~~~******~~~******~~~***";

    private static final Logger logger = LoggerFactory.getLogger(SharePointMediaConnectorMultiple.class);

    private int timesToRescan;


    SharePointMediaConnectorMultiple(SharePointConnectionParametersDto sharePointConnectionDetailsDto, MSAppInfo msAppInfo,
                                     int maxRetries, int pageSize, long maxSupportFileSize, MSConnectionConfig connectionConfig,
                                     int siteCrawlMaxDepth, String folderToFail, int timesToRescan, List<String> foldersToFilter,
                                     boolean isSpecialCharsSupported, String... charsToFilter) {

        super(sharePointConnectionDetailsDto,
                msAppInfo,
                maxRetries,
                pageSize,
                maxSupportFileSize,
                connectionConfig,
                siteCrawlMaxDepth,
                folderToFail,
                foldersToFilter,
                isSpecialCharsSupported,
                charsToFilter);
        this.timesToRescan = timesToRescan;
    }

    @Override
    public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
                                           Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                           Consumer<DirListingPayload> directoryListingConsumer,
                                           Consumer<String> createScanTaskConsumer,
                                           Predicate<Pair<Long, Long>> scanActivePredicate,
                                           ProgressTracker filePropsProgressTracker) {

        logger.error(SCAN_MODE_ANNOUNCEMENT, timesToRescan);

        for (int iter = 0; iter < timesToRescan; iter++) {
            logger.info("Mapping files iteration {} out of {}", iter+1, timesToRescan);
            super.concurrentStreamMediaItems(forkJoinPool,
                    scanParams,
                    filePropertiesConsumer,
                    directoryListingConsumer,
                    createScanTaskConsumer,
                    scanActivePredicate,
                    filePropsProgressTracker);
        }

    }

    @Override
    public void streamMediaItems(ScanTaskParameters scanParams,
                                 Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                                 Consumer<DirListingPayload> directoryListingConsumer,
                                 Consumer<String> createScanTaskConsumer,
                                 Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {

        logger.error(SCAN_MODE_ANNOUNCEMENT, timesToRescan);

        for (int iter = 0; iter < timesToRescan; iter++) {
            logger.info("Mapping files iteration {} out of {}", iter+1, timesToRescan);
            super.streamMediaItems(scanParams,
                    filePropertiesConsumer,
                    directoryListingConsumer,
                    createScanTaskConsumer,
                    scanActivePredicate,
                    filePropsProgressTracker);
        }
    }
}
