package com.cla.connector.mediaconnector.exchange.auth;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class MSGraphOAuthErrorHandler implements ResponseErrorHandler {

    private static final Logger logger = LoggerFactory.getLogger(MSGraphOAuthErrorHandler.class);
    private static final byte[] EMPTY_BYTEARRAY = new byte[0];

    @Override
    public boolean hasError(@NonNull ClientHttpResponse response) throws IOException {
        return !response.getStatusCode().is2xxSuccessful();
    }

    @Override
    public void handleError(@NonNull ClientHttpResponse response) throws IOException {
        logger.trace("Handling error {}", response.getStatusText());
        InputStream responseBody = response.getBody();
        byte[] body = Optional.of(responseBody)
                .map(input -> {
                    try {
                        return IOUtils.toByteArray(input);
                    } catch (IOException e) {
                        logger.error("Failed to get response body");
                        return null;
                    }
                })
                .orElse(EMPTY_BYTEARRAY);
        logger.trace("Error body size {}", body.length);
        throw (response.getStatusCode().is5xxServerError())
                ? new HttpServerErrorException(response.getStatusCode(), response.getStatusText(), response.getHeaders(), body, null)
                : new HttpClientErrorException(response.getStatusCode(), response.getStatusText(), response.getHeaders(), body, null);
    }
}