package com.cla.connector.mediaconnector.common.packed;

import com.cla.connector.domain.dto.error.ProcessingErrorType;

public class PackageCrawlException extends Exception {

    private final ProcessingErrorType processingErrorType;

    public PackageCrawlException(String message, Throwable cause) {
        super(message, cause);
        this.processingErrorType = ProcessingErrorType.PACKAGE_CRAWL_INTERNAL_ERROR;
    }

    public PackageCrawlException(ProcessingErrorType processingErrorType, String message) {
        super(message);
        this.processingErrorType = processingErrorType;
    }

    public ProcessingErrorType getProcessingErrorType() {
        return processingErrorType;
    }

}
