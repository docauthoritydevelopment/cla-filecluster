package com.cla.connector.mediaconnector.exchange;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created By Itai Marko
 */
public final class Exchange365Constants {

    /**
     * Indicates the entity was removed if present in the json of the entity
     */
    static final String REMOVAL_INDICATOR = "@removed";

    /**
     * Contains constants related to MS-Graph Message fields
     */
    public static class Message {

        /**
         * The key name of the ID json entity
         */
        public static final String ID = "id";

        /**
         * The key name of the Parent-Folder-ID json entity
         */
        static final String PARENT_FOLDER_ID = "parentFolderId";

        /**
         * The key name of the @oDataType json entity
         */
        static final String O_DATA_TYPE = "@odata.type";

        /**
         * The key name of the isRead json entity
         */
        static final String IS_READ = "isRead";

        /**
         * A list of message fields that:
         * <br/>
         * if are the only ones present, imply that the entity only had its read flag changed,
         * <br/>
         * and if others are present, imply the entity was created or updated
         */
        static final Set<String> READ_FLAG_CHANGE_INDICATORS =
                Collections.unmodifiableSet(new HashSet<>(Arrays.asList(ID, PARENT_FOLDER_ID, O_DATA_TYPE, IS_READ)));

        private Message() {}

    }

    private Exchange365Constants() {
    }
}
