package com.cla.connector.mediaconnector.labeling.mip;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@SuppressWarnings("unused")
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class MipProxyInitRequest {
    private String clientId;
    private String appName;
    private String appVersion;

    @JsonProperty("client_id")
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @JsonProperty("app_name")
    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    @JsonProperty("app_version")
    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

}
