package com.cla.connector.mediaconnector.common.packed;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalNotification;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

public class PackageResourceCache<T> {
	private final static Logger logger = LoggerFactory.getLogger(PackageResourceCache.class);
	private final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(r -> {
		Thread t = new Thread(r);
		t.setDaemon(true);
		return t;
	});
	private final LoadingCache<String, T> fileLoadingCache;

	public PackageResourceCache(int expireAfterIdle, int cleanUpPeriod, Function<String,T> loadAction, Consumer<T> removeAction) {
		fileLoadingCache =
				CacheBuilder.newBuilder()
						.maximumSize(1000)
						.expireAfterAccess(expireAfterIdle, TimeUnit.MILLISECONDS)
						.removalListener((RemovalNotification<String, T> cacheEntry) -> {
							try {
								logger.debug("closing and evicting from cache a package file " +  cacheEntry.getKey());
								removeAction.accept(cacheEntry.getValue());
							} catch (Throwable e) {
								logger.error("could not close package file:" + cacheEntry.getKey(), e);
							}
						})
						.build(new CacheLoader<String, T>() {
							@Override
							public T load(String fullFileName) throws Exception {
								try {
									return loadAction.apply(fullFileName);
								} catch (Throwable e) {
									logger.error("could not open package file:" + fullFileName, e);
									throw e;
								}
							}
						});


		scheduledExecutorService.scheduleAtFixedRate(()-> {
					try {

						fileLoadingCache.cleanUp();
						if(fileLoadingCache.size() > 0)
							logger.debug("current open package files number: " + fileLoadingCache.size() );
					} catch (Throwable e) {
						logger.error("could not clean up the opened package files cache", e);
					}
				}, 50, cleanUpPeriod,TimeUnit.MILLISECONDS);

	}

	public T loadPackageFile(String fullFileName){
		return fileLoadingCache.getUnchecked(fullFileName);
	}

	public long size(){
		return fileLoadingCache.size();
	}
}
