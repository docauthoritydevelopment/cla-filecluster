package com.cla.connector.domain.dto.media;

/**
 * Created by uri on 07/08/2016.
 */
public class SharePointConnectionParametersDto implements ConnectionParametersDto{
    private String url;
    private String username;
    private String password;
    private String domain;

    private Boolean sharePointOnline;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean getSharePointOnline() {
        return sharePointOnline;
    }

    public void setSharePointOnline(Boolean sharePointOnline) {
        this.sharePointOnline = sharePointOnline;
    }

    @Override
    public String toString() {
        return "SharePointConnectionParametersDto{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", domain='" + domain + '\'' +
                '}';
    }

    public SharePointConnectionParametersDto copy() {
        SharePointConnectionParametersDto copy = new SharePointConnectionParametersDto();
        copy.url = this.url;
        copy.username = this.username;
        copy.password = this.password;
        copy.domain = this.domain;
        copy.sharePointOnline = this.sharePointOnline;
        return copy;
    }

}
