package com.cla.connector.domain.dto.media;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class MIPConnectionParametersDto implements ConnectionParametersDto {

    private String password;
    private String clientId;
    private String resource;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }
}
