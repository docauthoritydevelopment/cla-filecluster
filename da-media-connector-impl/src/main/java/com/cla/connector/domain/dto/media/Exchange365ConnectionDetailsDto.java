package com.cla.connector.domain.dto.media;

/**
 * Created by: yael
 * Created on: 2/4/2019
 */
public class Exchange365ConnectionDetailsDto extends MediaConnectionDetailsDto {

    private Exchange365ConnectionParametersDto exchangeConnectionParametersDto;

    public Exchange365ConnectionDetailsDto() {
        mediaType = MediaType.EXCHANGE_365;
    }

    public Exchange365ConnectionParametersDto getExchangeConnectionParametersDto() {
        return exchangeConnectionParametersDto;
    }

    public void setExchangeConnectionParametersDto(Exchange365ConnectionParametersDto exchangeConnectionParametersDto) {
        this.exchangeConnectionParametersDto = exchangeConnectionParametersDto;
    }
}
