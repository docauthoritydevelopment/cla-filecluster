package com.cla.connector.domain.dto.media;

import com.microsoft.graph.core.DefaultConnectionConfig;

import java.net.HttpURLConnection;

/**
 * Created by: yael
 * Created on: 2/4/2019
 */
public class Exchange365ConnectionParametersDto implements ConnectionParametersDto{

    private String tenantId;
    private String applicationId;
    private String adUrlOverride;
    private String graphEndpointOverride;
    private String password;
    private int connectionConnectTimeout;
    private int connectionReadTimeout;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getAdUrlOverride() {
        return adUrlOverride;
    }

    public void setAdUrlOverride(String adUrlOverride) {
        this.adUrlOverride = adUrlOverride;
    }

    public String getGraphEndpointOverride() {
        return graphEndpointOverride;
    }

    public void setGraphEndpointOverride(String graphEndpointOverride) {
        this.graphEndpointOverride = graphEndpointOverride;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns the timeout in milliseconds to connect to MS-Graph
     * <p/>
     * This eventually is set to the MS-Graph's underlying {@link HttpURLConnection#setConnectTimeout(int)}.
     * Zero means no timeout
     * <p/>
     * Note 30,000 millis is the default of MS-Graph java sdk defined in:
     * {@link DefaultConnectionConfig#DEFAULT_CONNECT_TIMEOUT_MS}
     */
    public int getConnectionConnectTimeout() {
        return connectionConnectTimeout;
    }

    /**
     * Sets the timeout in milliseconds to connect to MS-Graph
     * <p/>
     * This eventually is set to the MS-Graph's underlying {@link HttpURLConnection#setConnectTimeout(int)}.
     * Zero means no timeout
     * <p/>
     * Note 30,000 millis is the default of MS-Graph java sdk defined in:
     * {@link DefaultConnectionConfig#DEFAULT_CONNECT_TIMEOUT_MS}
     */
    public void setConnectionConnectTimeout(int connectionTimeout) {
        this.connectionConnectTimeout = connectionTimeout;
    }

    /**
     * Returns the timeout in milliseconds to read from a connection to MS-Graph
     * <p/>
     * This eventually is set to the MS-Graph's underlying {@link HttpURLConnection#setReadTimeout(int)}.
     * Zero means no timeout
     * <p/>
     * Note 30,000 millis is the default of MS-Graph java sdk defined in:
     * {@link DefaultConnectionConfig#DEFAULT_READ_TIMEOUT_MS}
     */
    public int getConnectionReadTimeout() {
        return connectionReadTimeout;
    }

    /**
     * Sets the timeout in milliseconds to read from a connection to MS-Graph
     * <p/>
     * This eventually is set to the MS-Graph's underlying {@link HttpURLConnection#setReadTimeout(int)}.
     * Zero means no timeout
     * <p/>
     * Note 30,000 millis is the default of MS-Graph java sdk defined in:
     * {@link DefaultConnectionConfig#DEFAULT_READ_TIMEOUT_MS}
     */
    public void setConnectionReadTimeout(int connectionReadTimeout) {
        this.connectionReadTimeout = connectionReadTimeout;
    }
}
