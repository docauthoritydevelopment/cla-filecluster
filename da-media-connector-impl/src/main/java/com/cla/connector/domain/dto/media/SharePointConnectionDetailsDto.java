package com.cla.connector.domain.dto.media;


/**
 *
 * Created by uri on 29/08/2016.
 */
public class SharePointConnectionDetailsDto extends MicrosoftConnectionDetailsDtoBase {

    public SharePointConnectionDetailsDto() {
        super(MediaType.SHARE_POINT);
    }

}
