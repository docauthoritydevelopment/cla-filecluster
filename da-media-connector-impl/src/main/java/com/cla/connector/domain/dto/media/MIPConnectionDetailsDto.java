package com.cla.connector.domain.dto.media;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
public class MIPConnectionDetailsDto extends MediaConnectionDetailsDto {

    private MIPConnectionParametersDto MIPConnectionParametersDto;

    public MIPConnectionDetailsDto() {
        mediaType = MediaType.MIP;
    }

    public MIPConnectionParametersDto getMIPConnectionParametersDto() {
        return MIPConnectionParametersDto;
    }

    public void setMIPConnectionParametersDto(MIPConnectionParametersDto MIPConnectionParametersDto) {
        this.MIPConnectionParametersDto = MIPConnectionParametersDto;
    }
}
