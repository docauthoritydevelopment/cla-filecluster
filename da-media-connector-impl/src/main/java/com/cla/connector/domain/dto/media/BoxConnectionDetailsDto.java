package com.cla.connector.domain.dto.media;

/**
 * Stub for Box connection details DTO
 * Created by vladi on 2/5/2017.
 */
public class BoxConnectionDetailsDto extends MediaConnectionDetailsDto{
    public BoxConnectionDetailsDto() {
        mediaType = MediaType.BOX;
    }
    private String jwt;

	public String getJwt() {
		return jwt;
	}

	public BoxConnectionDetailsDto setJwt(String jwt) {
		this.jwt = jwt;
		return this;
	}
}
