package com.cla.connector.domain.dto.media;


/**
 * Stub for Box connection details DTO
 * Created by vladi on 2/5/2017.
 */
public class FileShareConnectionDetailsDto extends MediaConnectionDetailsDto{

    public FileShareConnectionDetailsDto() {
        mediaType = MediaType.FILE_SHARE;
    }
}
