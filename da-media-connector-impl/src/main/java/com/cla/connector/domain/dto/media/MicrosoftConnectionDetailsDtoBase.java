package com.cla.connector.domain.dto.media;


import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * Created by uri on 29/08/2016.
 */
public abstract class MicrosoftConnectionDetailsDtoBase extends MediaConnectionDetailsDto {

    private SharePointConnectionParametersDto sharePointConnectionParametersDto;

    public MicrosoftConnectionDetailsDtoBase(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public SharePointConnectionParametersDto getSharePointConnectionParametersDto() {
        return sharePointConnectionParametersDto;
    }

    public void setSharePointConnectionParametersDto(SharePointConnectionParametersDto sharePointConnectionParametersDto) {
        this.sharePointConnectionParametersDto = sharePointConnectionParametersDto;
    }

    @JsonIgnore
    public <T extends MicrosoftConnectionDetailsDtoBase> T copy(){
        T target;
        try {
            //noinspection unchecked
            target = (T)this.getClass().newInstance();
            target.setId(this.getId());
            target.setName(this.getName());
            target.setUsername(this.getUsername());
            target.setMediaType(this.getMediaType());
            target.setUrl(this.getUrl());
            target.setConnectionParametersJson(this.getConnectionParametersJson());
            target.setTimestamp(this.getTimestamp());
            if (this.getSharePointConnectionParametersDto() != null) {
                target.setSharePointConnectionParametersDto(this.getSharePointConnectionParametersDto().copy());
            }
            target.setPstCacheEnabled(this.isPstCacheEnabled());
            return target;
        } catch (Exception e) {
            throw new RuntimeException("Failed to copy object of type " + this.getClass().getCanonicalName(), e);
        }
    }
}
