package com.cla.connector.domain.dto.media;


/**
 *
 * Created by oren
 */
public class OneDriveConnectionDetailsDto extends MicrosoftConnectionDetailsDtoBase {

    public OneDriveConnectionDetailsDto() {
        super(MediaType.ONE_DRIVE);
    }

}
