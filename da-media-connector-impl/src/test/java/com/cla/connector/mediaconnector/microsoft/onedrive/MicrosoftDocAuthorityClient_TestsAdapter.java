package com.cla.connector.mediaconnector.microsoft.onedrive;

import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.mediaconnector.microsoft.MicrosoftDocAuthorityClient;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointExtendedFolder;
import com.independentsoft.share.ServiceException;

public class MicrosoftDocAuthorityClient_TestsAdapter extends MicrosoftDocAuthorityClient {

    MicrosoftDocAuthorityClient_TestsAdapter(String userName, String domain, String password, String userAgent,
                                             String url, int maxRetries, MSConnectionConfig connectionConfig,
                                             boolean isSpecialCharactersSupported, String... charsToFilter) {

        super(userName, domain, password, userAgent, url, maxRetries, connectionConfig, isSpecialCharactersSupported,
                charsToFilter);
    }

    @Override
    public SharePointExtendedFolder getSharePointExtendedFolderDetails(String subSite, String path) throws ServiceException {
        return super.getSharePointExtendedFolderDetails(subSite, path);
    }
}
