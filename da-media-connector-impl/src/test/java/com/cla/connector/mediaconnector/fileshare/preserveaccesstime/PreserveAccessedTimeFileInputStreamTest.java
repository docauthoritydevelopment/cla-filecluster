package com.cla.connector.mediaconnector.fileshare.preserveaccesstime;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PreserveAccessedTimeFileInputStreamTest {

    @Test
    public void testReset() throws Exception {

        String filename = "./src/test/resources/files/bamba.xlsx";

        PreserveAccessedTimeFileInputStream in = new PreserveAccessedTimeFileInputStream(filename, false);
        BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(in)));
        String lineBefore = bufferedReader.readLine();

        in.reset();

        bufferedReader = new BufferedReader((new InputStreamReader(in)));
        String lineAfter = bufferedReader.readLine();
        Assert.assertEquals(lineBefore, lineAfter);
        bufferedReader.close();
        in.close();
    }

    //overlapped.Internal = new BaseTSD.ULONG_PTR();
    //overlapped.InternalHigh = new BaseTSD.ULONG_PTR();
}