package com.cla.connector.mediaconnector.common.packed;

import org.apache.commons.compress.archivers.zip.ZipFile;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ZipPackageResourceCacheTest {
	private ZipPackageResourceCache zipPackageResourceCache = new ZipPackageResourceCache(100, 50);

	@Test
	public void testLoadAndCleanupZip() throws InterruptedException {
		ClassLoader classLoader = LinearZipCrawlerTest.class.getClassLoader();
		String name = "files/SampleZip.zip";
		File file = new File(classLoader.getResource(name).getFile());
		final String absolutePath = file.getAbsolutePath();
		final ZipFile zipFile = zipPackageResourceCache.loadPackageFile(absolutePath);
		assertNotNull(zipFile);
		assertEquals(1, zipPackageResourceCache.size());
		Thread.sleep(30);
		assertEquals(1,zipPackageResourceCache.size());
		zipPackageResourceCache.loadPackageFile(absolutePath);
		Thread.sleep(300);
		assertEquals(0,zipPackageResourceCache.size());
	}


	@Test
	public void testLoadAndCleanupZipError() throws InterruptedException {
		try {
			String name = "files/SampleZip.zip";
			ZipFile zipFile = zipPackageResourceCache.loadPackageFile(name);
		} catch (Exception e) {
			Throwable cause = e.getCause();
			for (int i = 0; i < 5; i++) {
				if(cause instanceof FileNotFoundException)
					return;
				else cause = cause.getCause();
			}
		}
	}
}