package com.cla.connector.mediaconnector.common.packed;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class LinearZipCrawlerTest {

	private LinearZipCrawler packageCrawler = new LinearZipCrawler();
	final String entryId = "30 SnoPUD 01042002.docx";
	private static ClaFilePropertiesDto claFilePropertiesDto = null;

	@BeforeClass
	public static void setup(){
		ClassLoader classLoader = LinearZipCrawlerTest.class.getClassLoader();
		final String name = "files/SampleZip.zip";
		File file = new File(classLoader.getResource(name).getFile());
		claFilePropertiesDto = new ClaFilePropertiesDto(file.getAbsolutePath(), 123L);
	}

	@Test
	public void crawlZipTest() throws PackageCrawlException {
		List<String> files = new ArrayList<>();
		packageCrawler.crawl(claFilePropertiesDto,
				p->true,
				e->{files.add(e.getFileName());});
		assertTrue(files.size() > 0);
	}

	@Test
	public void crawlEncryptedZipTestFail() {
		ClassLoader classLoader = getClass().getClassLoader();
		final String name = "files/SampleZipEncrypted.zip";
		List<String> files = new ArrayList<>();
		File file = new File(classLoader.getResource(name).getFile());
		try {
			packageCrawler.crawl(new ClaFilePropertiesDto(file.getAbsolutePath(), 123L),
					p->true,
					e-> files.add(e.getFileName()));
		} catch (PackageCrawlException e) {
			assertTrue(e.getProcessingErrorType() == ProcessingErrorType.PACKAGE_CRAWL_PASSWORD_PROTECTED);
		}
	}

	@Test
	public void crawlZipFilesFilterPredicateTest() throws PackageCrawlException {
		List<String> files = new ArrayList<>();
		packageCrawler.crawl(claFilePropertiesDto,
				p-> p.endsWith(".xlsx"),
				e-> files.add(e.getFileName()));
		assertEquals(2, files.size());
	}

	@Test
	public void extractPackagedEntry() {
		FileContentDto fileContentDto = packageCrawler.extractPackagedEntry(claFilePropertiesDto.getFileName(), entryId);
		assertNotNull(fileContentDto);
		assertNotNull(fileContentDto.getInputStream());
	}

	@Test
	public void extractPackagedEntryProperties() {
		ClaFilePropertiesDto packagedEntryProperties = packageCrawler.extractPackagedEntryProperties(claFilePropertiesDto, entryId);
		assertNotNull(packagedEntryProperties);
		assertTrue(packagedEntryProperties.getFileName().endsWith(entryId));
	}
}