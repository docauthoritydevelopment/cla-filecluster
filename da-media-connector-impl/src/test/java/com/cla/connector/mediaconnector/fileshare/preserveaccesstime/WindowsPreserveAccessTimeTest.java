package com.cla.connector.mediaconnector.fileshare.preserveaccesstime;

import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.google.common.base.Strings;
import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.Kernel32Util;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;
import org.apache.commons.io.IOUtils;
import org.assertj.core.util.Lists;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class WindowsPreserveAccessTimeTest {
    private static boolean accessTimeEnabled = false;

    @BeforeClass
    public static void supportAccessTime() throws IOException {
        Process process = Runtime.getRuntime().exec("../dev-scripts/fs_last_accessed.bat");
        try {
            List<String> lines = IOUtils.readLines(process.getInputStream());
            for (String line : lines) {
                System.out.println(line);
                if (line.contains("ON")) {
                    accessTimeEnabled = true;
                }
            }
        } finally {
            process.destroy();
        }

        if (!accessTimeEnabled) {
            System.out.println("Access time is not supported, enabling it");
            process = Runtime.getRuntime().exec("../dev-scripts/fs_last_accessed.bat set");
            try {
                IOUtils.readLines(process.getInputStream()).forEach(l -> System.out.println(l));
            } finally {
                process.destroy();
            }
        }
    }

    @AfterClass
    public static void revertAccessTimeSupport() throws IOException {
        if (!accessTimeEnabled) {
            System.out.println("Access time was initially not enabled, reverting it");
            Process process = Runtime.getRuntime().exec("../dev-scripts/fs_last_accessed.bat clear");
            try {
                IOUtils.readLines(process.getInputStream()).forEach(l -> System.out.println(l));
            } finally {
                process.destroy();
            }
        }

    }

    @Test
    public void testFetchAttributes() throws Exception {
        String url = "src";
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        props.setFileName(url);
        WindowsPreserveAccessTime.fetchAttributes(props, true);
        assertFalse(props.hasErrors());
        assertTrue(props.getAccessTimeMilli() > 0);
        assertTrue(props.getCreationTimeMilli() > 0);
        assertTrue(props.getModTimeMilli() > 0);
    }

    @Test(expected = NoSuchFileException.class)
    public void testFetchAttributesBadPath() throws Exception {
        WindowsPreserveAccessTime.fetchAttributes(ClaFilePropertiesDto.create().setPath(Paths.get("..\\gggggg")), true);
    }

    @Test
    public void testFetchAcl() throws Exception {
        String url = "src";
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        props.setFileName(url);
        WindowsPreserveAccessTime.fetchAcls(props, true);
        assertFalse(props.hasErrors());
        assertFalse(Strings.isNullOrEmpty(props.getAclSignature()));
        assertTrue(props.getAclReadAllowed().size() > 0);
    }

    @Test(expected = NoSuchFileException.class)
    public void testFetchAclsBadPath() throws Exception {
        WindowsPreserveAccessTime.fetchAcls(ClaFilePropertiesDto.create().setPath(Paths.get("..\\regregerg")), true);
    }

    @Test
    public void testPreserveLastAccessTimeAction() throws Exception {
        // Create a tmp file
        String fileName = "bambi.txt";
        Path p = Paths.get(fileName).toAbsolutePath();
        Files.deleteIfExists(p);
        Files.createFile(p);

        // Take last access time & wait 1 sec before accessing file through java api
        byte[] str = new String("aaaa").getBytes();
        long lastAccessTime = ((FileTime) Files.getAttribute(p, "lastAccessTime")).toMillis();
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create().setPath(p);
        Thread.sleep(1000);

        // Write to file & verify that access time is changed
        Files.write(p, str);
        FileShareMediaUtils.fetchAttributes(props);
        assertNotEquals(props.getAccessTimeMilli(), lastAccessTime);

        // Take last access time & wait 1 sec before accessing through  WindowsPreserveAccessTime api
        lastAccessTime = props.getAccessTimeMilli();
        Thread.sleep(1000);

        // Write again to file & verify access time is the same
        WindowsPreserveAccessTime.preserveAccessTimeAction(p.toString(), handle -> {
            IntByReference intByReference = new IntByReference();

            boolean success = Kernel32.INSTANCE.WriteFile(handle.handle, str, str.length, intByReference, null);
            if (!success) {
                throw new RuntimeException(Kernel32Util.getLastErrorMessage());
            }

        }, true, WinNT.GENERIC_WRITE);
        assertEquals(((FileTime) Files.getAttribute(p, "lastAccessTime")).toMillis(), lastAccessTime);

        // Delete tmp file
        Files.deleteIfExists(p);
    }

    @Test
    public void testPreserveAccessedTimeFileInputStream() throws Exception {
        // Create tmp file
        String fileName = "bambi.txt";
        Path p = Paths.get(fileName).toAbsolutePath();
        Files.deleteIfExists(p);
        Files.createFile(p);

        // Write lines to file
        List<String> writeLines = Arrays.asList("a", "b", "c");
        Files.write(p, writeLines);

        // Take last access time & wait 1 sec before accessing file again
        long lastAccessTime = ((FileTime) Files.getAttribute(p, "lastAccessTime")).toMillis();
        Thread.sleep(1000);

        // Read lines from file
        try (PreserveAccessedTimeFileInputStream preserveAccessedTimeFileInputStream = new PreserveAccessedTimeFileInputStream(p.toString(), true)) {
            List<String> readLines = IOUtils.readLines(preserveAccessedTimeFileInputStream);
            // Check last time is presereved & write = read
            assertEquals(lastAccessTime, ((FileTime) Files.getAttribute(p, "lastAccessTime")).toMillis());
            assertEquals(writeLines, readLines);
        }

        // Delete tmp file
        Files.deleteIfExists(p);
    }


    @Test
    public void testIsDirectoryTrue() throws Exception {
        String url = "src";
        Path p = Paths.get(url).toAbsolutePath();
        boolean res = WindowsPreserveAccessTime.isDirectory(p, true, false);
        assertTrue(res);
    }

    @Test
    public void testIsDirectoryFalse() throws Exception {
        String fileName = "bambi.txt";
        Path p = Paths.get(fileName).toAbsolutePath();
        Files.deleteIfExists(p);
        Files.createFile(p);
        boolean res = WindowsPreserveAccessTime.isDirectory(p, true, false);
        assertFalse(res);
        Files.deleteIfExists(p);
    }

    @Test
    public void testIsDirectoryBadPathNoException() {
        WindowsPreserveAccessTime.setFileShareMapErrorIgnore(Lists.newArrayList("The system cannot find the file specified"));
        String url = "..\\regregerg";
        Path p = Paths.get(url).toAbsolutePath();
        assertFalse(WindowsPreserveAccessTime.isDirectory(p, true, false));
        WindowsPreserveAccessTime.setFileShareMapErrorIgnore(Lists.newArrayList());
    }

    @Test(expected = AccessException.class)
    public void testIsDirectoryBadPathAccessException() {
        String url = "..\\regregerg";
        Path p = Paths.get(url).toAbsolutePath();
        WindowsPreserveAccessTime.isDirectory(p, true, false);
    }

    @Test
    public void testFileSize() throws Exception {
        String fileName = "bambi.txt";
        Path p = Paths.get(fileName).toAbsolutePath();
        Files.deleteIfExists(p);
        Files.createFile(p);
        List<String> lines = new ArrayList<>();
        lines.add("one line");
        Files.write(p, lines);
        long res = WindowsPreserveAccessTime.fileSize(fileName, true);
        assertEquals(10, res);
        Files.deleteIfExists(p);
    }
}
