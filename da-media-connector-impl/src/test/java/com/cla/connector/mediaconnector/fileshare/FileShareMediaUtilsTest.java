package com.cla.connector.mediaconnector.fileshare;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.utils.Pair;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

/**
 * Created by: yael
 * Created on: 11/29/2017
 */
public class FileShareMediaUtilsTest {

    @Test
    public void testGetMediaProperties () {
        String url = "..\\appserver\\src\\test\\resources\\files\\excel\\two";
        Optional<ClaFilePropertiesDto> res = FileShareMediaUtils.getMediaProperties(url, true, true);
        assertTrue(res.isPresent());
        assertTrue(res.get().getFileName().contains(url.substring(2)));
    }

    @Test
    public void testGetMediaPropertiesBadPath () {
        String url = "..\\appserver\\src\\test\\resources\\gggggg";
        Optional<ClaFilePropertiesDto> res = FileShareMediaUtils.getMediaProperties(url, true, true);
        assertTrue(res.isPresent());
        assertTrue(res.get().getFileName().contains(url.substring(2)));
        assertTrue(res.get().hasErrors());
    }

    @Test
    public void testStreamMediaItemsBadPath() {
        String url = "..\\appserver\\src\\test\\resources\\gggggg";
        ScanTaskParameters scanParams = createScanTaskParameters();

        Consumer<ClaFilePropertiesDto> filePropsConsumer = claFilePropertiesDto -> {};

        Consumer<DirListingPayload> dirListingConsumer = dirListingPayload -> fail("Should not have been called");

        Consumer<String> createScanTaskConsumer = s -> fail("Should not have been called");

        Predicate<Pair<Long, Long>> scanActivePredicate = (Predicate<Pair<Long, Long>>) aLong -> true;

        FileShareMediaUtils.streamMediaItems(Paths.get(url), scanParams, filePropsConsumer,
                dirListingConsumer, createScanTaskConsumer, scanActivePredicate, false, true);
    }

    @Test
    public void testStreamMediaItems() {
        String url = "..\\appserver\\src\\test\\resources\\files\\excel\\two";

        ScanTaskParameters scanParams = createScanTaskParameters();

        List<ClaFilePropertiesDto> filesProperties = new ArrayList<>();
        Consumer<ClaFilePropertiesDto> filePropsConsumer = filesProperties::add;

        Consumer<DirListingPayload> dirListingConsumer = dirListingPayload -> {
            assertEquals(dirListingPayload.getFolderPath(), url);
            assertTrue(dirListingPayload.getDirectoryList().contains(url+"\\budget98.xlsx"));
            assertEquals(2, dirListingPayload.getDirectoryList().size());
        };

        Predicate<Pair<Long, Long>> scanActivePredicate = (Predicate<Pair<Long, Long>>) aLong -> true;

        FileShareMediaUtils.streamMediaItems(Paths.get(url), scanParams, filePropsConsumer,
                dirListingConsumer, null, scanActivePredicate, false, true);

        for (ClaFilePropertiesDto f : filesProperties) {
            assertTrue(f.getFileName().contains(url.substring(2)));
            assertFalse(f.hasErrors());
        }
    }

    private ScanTaskParameters createScanTaskParameters() {
        ScanTaskParameters scanParams = ScanTaskParameters.create();
        scanParams.setRunId(5L);
        Set<String> extensionsToScan = Sets.newHashSet("pdf", "docx", "doc", "xls", "xlsx", "zip", "pst");
        List<FileType> typesToScan = Collections.singletonList(FileType.OTHER);
        ScanTypeSpecification scanTypes = new ScanTypeSpecification(typesToScan, extensionsToScan, null);
        scanParams.setScanTypeSpecification(scanTypes);
        return scanParams;
    }

    @Test
    public void testFetchAcls() {
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        String url = "..\\appserver\\src\\test\\resources\\files\\excel\\two";
        prop.setMediaItemId(url);
        FileShareMediaUtils.fetchAcls(prop);
        assertFalse(prop.hasErrors());
        assertFalse(prop.getAclSignature().isEmpty());
    }

    @Test
    public void testFetchAclsBadPath() {
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        String url = "..\\appserver\\src\\test\\gggggggg";
        prop.setMediaItemId(url);
        FileShareMediaUtils.fetchAcls(prop);
        assertFalse(prop.hasErrors());
        assertNull(prop.getAclSignature());
    }

    @Test
    public void testIsDirectoryExists() {
        assertTrue(FileShareMediaUtils.isDirectoryExists("..\\appserver\\src\\test\\resources\\files\\excel\\two", false, true, false));
    }

    @Test
    public void testIsDirectoryExistsBadPath() {
        assertFalse(FileShareMediaUtils.isDirectoryExists("..\\appserver\\src\\gggggg", false, true, false));
    }
}
