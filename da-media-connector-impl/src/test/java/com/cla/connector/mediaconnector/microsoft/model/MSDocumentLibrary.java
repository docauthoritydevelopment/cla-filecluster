package com.cla.connector.mediaconnector.microsoft.model;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointExtendedFolder;
import lombok.Builder;
import lombok.Singular;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Builder
public class MSDocumentLibrary extends com.independentsoft.share.List implements MSFileSystemItem {
    @Builder.Default
    String listId = UUID.randomUUID().toString().toUpperCase();

    String entityTypeName;
    String documentTemplateUrl;
    String title;

    String urlSegment;

    @Singular
    List<MSFile> files;

    @Singular
    List<MSFolder> folders;

    boolean crawlable;

    public String getListId() {
        return listId;
    }

    public String getId() {
        return listId;
    }

    public String getUrlSegment() {
        return urlSegment;
    }

    @Override
    public List<MSFileSystemItem> getSubFolders() {
        return getFolders().stream()
                .map(folder -> (MSFileSystemItem) folder)
                .collect(Collectors.toList());
    }

    public List<MSFile> getFiles() {
        return files;
    }

    public List<MSFolder> getFolders() {
        return folders;
    }

    @Override
    public String getEntityTypeName() {
        return entityTypeName;
    }

    @Override
    public String getDocumentTemplateUrl() {
        return documentTemplateUrl;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public SharePointExtendedFolder toSharePointExtendedFolder() {
        SharePointExtendedFolder folder = new SharePointExtendedFolder();
        folder.setListId(listId);
        return folder;
    }

    public List<ClaFilePropertiesDto> getFilesAsClaProp() {
        return toClaFilePropertiesDtos(files);
    }

    @Override
    public boolean isCrawlable() {
        return crawlable;
    }

    static List<ClaFilePropertiesDto> toClaFilePropertiesDtos(List<MSFile> files) {
        return files.stream()
                .map(msfile -> {
                    ClaFilePropertiesDto dto = ClaFilePropertiesDto.create()
                            .setFolder(false);
                    dto.setFileName(msfile.path + "/" + msfile.name);

                    return dto;
                })
                .collect(Collectors.toList());
    }

    public static class DocumentLibraryProperties {
        public final String documentTemplateUrl;
        public final String entityTypeName;
        public final String title;
        public final String urlSegment;
        public final boolean isCrawlable;

        public DocumentLibraryProperties(String documentTemplateUrl, String entityTypeName, String title, String urlSegment, boolean isCrawlable) {
            this.documentTemplateUrl = documentTemplateUrl;
            this.entityTypeName = entityTypeName;
            this.title = title;
            this.urlSegment = urlSegment;
            this.isCrawlable = isCrawlable;
        }
    }

    public static class MSDocumentLibraryBuilder {
        @SuppressWarnings("unused")
        private String entityTypeName;

        @SuppressWarnings("unused")
        private String documentTemplateUrl;

        public void entityTypeName(String entityTypeName) {
            this.entityTypeName = entityTypeName;
        }

        public void documentTemplateUrl(String documentTemplateUrl) {
            this.documentTemplateUrl = documentTemplateUrl;
        }
    }
}