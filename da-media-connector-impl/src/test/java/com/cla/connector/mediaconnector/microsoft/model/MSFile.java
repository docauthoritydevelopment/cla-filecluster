package com.cla.connector.mediaconnector.microsoft.model;

import com.independentsoft.share.File;

@lombok.Builder
public class MSFile extends File {
    String name;
    String path;
    @lombok.Builder.Default
    int length = 5;

    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public String getPath() {
        return path;
    }
}