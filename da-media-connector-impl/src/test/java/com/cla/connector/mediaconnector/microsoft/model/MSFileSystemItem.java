package com.cla.connector.mediaconnector.microsoft.model;

import java.util.List;

public interface MSFileSystemItem {

    List<MSFileSystemItem> getSubFolders();

    List<MSFile> getFiles();

    default boolean isCrawlable() {
        return true;
    }
}
