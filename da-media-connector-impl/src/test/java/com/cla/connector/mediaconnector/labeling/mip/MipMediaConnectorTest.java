package com.cla.connector.mediaconnector.labeling.mip;

import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MIPConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MIPConnectionParametersDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

@Ignore
public class MipMediaConnectorTest {

    //------------------- temporal code -------------------------------------------------
    private final String uri = "https://login.microsoftonline.com/common/oauth2/token";
    private final String clientId = "6de00d49-382d-404d-bc61-bbad3c75f1e8";
    private final String username = "costa.koren@docauthority.onmicrosoft.com";
    private final String password = "knsTNT07";
    private final String resource = "https://psor.o365syncservice.com";
    private final String proxyUrl = "http://localhost:9090/mip/api/";
    //----------------------------------------------------------------------------------

    @Test
    public void generalTest(){
        MIPConnectionDetailsDto connDto = new MIPConnectionDetailsDto();
        MIPConnectionParametersDto params = new MIPConnectionParametersDto();
        params.setClientId(clientId);
        params.setPassword(password);
        params.setResource(resource);
        connDto.setUrl(proxyUrl);
        connDto.setUsername(username);
        connDto.setMIPConnectionParametersDto(params);
        MipMediaConnector connector = new MipMediaConnector(uri, connDto);
        connector.init();
        Collection<MipLabelResult> allLabels = connector.getAllLabels();
        List<ServerResourceDto> serverResourceDtos = connector.testConnection();
        //MipLabelResult[] fileLabel = connector.getFileLabel("D:\\Enron\\metal2\\LeadHiory.xlsx");
    }

    @Test
    public void parseTest(){
        String json = "[{\"created_on\":\"2019-07-08T10:16:23.354304Z\",\"id\":\"8da9f00b-4b9d-4f19-8e04-7ca405aef4ea\",\"name\":\"Public\",\"sensitivity\":3,\"tooltip\":\"Business data that is specifically prepared and approved for public consumption.\",\"is_active\":true}]";
        ObjectMapper mapper = new ObjectMapper();
        try {
            MipLabelResult[] mipLabelResult = mapper.readValue(json, MipLabelResult[].class);
            System.out.println(mipLabelResult[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}