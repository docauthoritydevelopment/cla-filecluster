package com.cla.connector.mediaconnector.microsoft.model;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointExtendedFolder;
import com.independentsoft.share.Folder;
import lombok.Singular;
import lombok.ToString;
import org.assertj.core.util.Lists;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@ToString
public class MSFolder extends Folder implements MSFileSystemItem {
    public final String path;

    @Singular
    private List<MSFolder> subFolders;

    @Singular
    private List<MSFile> files;

    private Date createTime = new Date();
    private Date lastModTime = new Date();

    public MSFolder(String path) {
        this.path = path;
    }

    public List<Folder> getFolders() {
        return subFolders == null
                ? Lists.emptyList() : subFolders.stream().map(fold -> (Folder) fold).collect(Collectors.toList());
    }

    public List<MSFileSystemItem> getSubFolders() {
        return Optional.ofNullable(subFolders)
                .map(folders -> folders.stream()
                        .map(folder -> (MSFileSystemItem) folder)
                        .collect(Collectors.toList()))
                .orElseGet(Lists::newArrayList);
    }

    public List<MSFile> getFiles() {
        return files;
    }

    public List<ClaFilePropertiesDto> getFilesAsClaFileProp() {
        return MSDocumentLibrary.toClaFilePropertiesDtos(files);
    }

    @Override
    public String getName() {
        return path;
    }

    @Override
    public Date getCreatedTime() {
        return createTime;
    }

    @Override
    public Date getLastModifiedTime() {
        return lastModTime;
    }

    @Override
    public int getItemCount() {
        return Optional.ofNullable(subFolders).orElse(Lists.emptyList()).size()
                + Optional.ofNullable(files).orElse(Lists.emptyList()).size();
    }

    public SharePointExtendedFolder toSharePointExtendedFolder() {
        SharePointExtendedFolder folder = new SharePointExtendedFolder();
        folder.setName(path);
        return folder;
    }

    public static class Builder {
        private static String[] FILE_TYPES = {"docx", "xlsx", "pdf"};

        private String path;

        private List<MSFolder> subFolders = Lists.newArrayList();

        private int fileAmount;

        private Date createTime;
        private Date lastModTime;

        public static Builder create() {
            return new Builder();
        }

        public Builder withPath(String path) {
            this.path = path;
            return this;
        }

        public Builder withFileAmount(int amount) {
            this.fileAmount = amount;
            return this;
        }

        public Builder withSubFolders(List<MSFolder> subFolders) {
            this.subFolders = subFolders;
            return this;
        }

        public Builder withSubFolder(MSFolder folder) {
            this.subFolders.add(folder);
            return this;
        }

        @SuppressWarnings("unused")
        public Builder withCreateTime(Date date) {
            this.createTime = date;
            return this;
        }

        @SuppressWarnings("unused")
        public Builder withLastModTime(Date date) {
            this.lastModTime = date;
            return this;
        }

        public MSFolder build() {
            MSFolder folder = new MSFolder(path);
            if (subFolders != null) {
                folder.subFolders = subFolders;
            }
            folder.files =
                    IntStream.range(0, fileAmount)
                            .mapToObj(i -> UUID.randomUUID().toString() + "." + FILE_TYPES[i % FILE_TYPES.length])
                            .map(filename -> MSFile.builder()
                                    .name(filename)
                                    .path(folder.path)

                                    .build())
                            .collect(Collectors.toList());


            folder.createTime = Optional.ofNullable(createTime).orElse(new Date());
            folder.lastModTime = Optional.ofNullable(lastModTime).orElse(new Date());

            return folder;
        }
    }
}