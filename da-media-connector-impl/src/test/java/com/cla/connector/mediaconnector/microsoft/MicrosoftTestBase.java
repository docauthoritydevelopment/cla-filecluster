package com.cla.connector.mediaconnector.microsoft;

import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.mediaconnector.microsoft.onedrive.MicrosoftDocAuthorityClient_TestsAdapter;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;
import com.google.common.collect.Sets;
import com.independentsoft.share.Service;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.junit.After;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.Set;

public abstract class MicrosoftTestBase {

    protected static final String SHARE_POINT_365_USER = "ro-admin2@docauthority.onmicrosoft.com";
    protected static final String SHARE_POINT_365_PASSWORD = "doc4Sharepoint6";
    protected static final long MAX_FILE_SIZE = 20_000_000;
    protected static final int msClientHttpTimeout = 5000;

    protected final ProgressTracker filePropsProgressTracker = new DummyProgressTracker();

    @Mock
    protected Service service;

    @Mock
    protected MicrosoftDocAuthorityClient_TestsAdapter microsoftDocAuthorityClient;


    protected void initConnectorResources(MicrosoftConnectorBase connectorBase) {
        when(microsoftDocAuthorityClient.getVersion()).thenReturn(MicrosoftDocAuthorityClient.VERSION_DEFAULT);
        ReflectionTestUtils.setField(microsoftDocAuthorityClient, "clientConnectionManager", new BasicHttpClientConnectionManager());
        ReflectionTestUtils.setField(connectorBase, "service", service);
        ReflectionTestUtils.setField(connectorBase, "microsoftDocAuthorityClient", microsoftDocAuthorityClient);
    }

    protected <T extends MicrosoftConnectorBase.MicrosoftConnectorBaseBuilder<E, T>, E extends MicrosoftConnectorBase> E getEnrichedConnector(T builder, String url) {
        return builder.withSharePointConnectionParametersDto(getConnectionParameterDTO(url))
                .withConnectionConfig(getConnectionConfig())
                .withMaxRetries(1)
                .withPageSize(10)
                .withMaxFileSize(MAX_FILE_SIZE)
                .withFoldersToFilter(null)
                .build();
    }

    protected SharePointConnectionParametersDto getConnectionParameterDTO(String connectorUrl) {
        SharePointConnectionParametersDto connectionParamsDto = new SharePointConnectionParametersDto();
        connectionParamsDto.setUsername(SHARE_POINT_365_USER);
        connectionParamsDto.setPassword(SHARE_POINT_365_PASSWORD);

        Optional.ofNullable(connectorUrl)
                .ifPresent(connectionParamsDto::setUrl);

        return connectionParamsDto;
    }

    protected MSConnectionConfig getConnectionConfig() {
        return MSConnectionConfig.builder()
                .connectionTimeout(msClientHttpTimeout)
                .socketTimeout(msClientHttpTimeout)
                .serviceClientTimeout(2000)
                .connectionRequestTimeout(msClientHttpTimeout)
                .build();
    }

    @After
    public void resetMocks() {
        Mockito.reset(service);
        Mockito.reset(microsoftDocAuthorityClient);
    }

    protected ScanTaskParameters getScanTaskParams(String basePath) {
        Set<String> extensions = Sets.newHashSet("pdf", "docx", "doc", "xls", "xlsx");
        ScanTypeSpecification scanTypes = new ScanTypeSpecification(null, extensions, null);
        return ScanTaskParameters.create()
                .setPath(basePath)
                .setRunId(-1L)
                .setScanTypeSpecification(scanTypes);
    }
}
