package com.cla.connector.mediaconnector.microsoft.onedrive;

import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.mediaconnector.microsoft.MSItemKey;
import com.cla.connector.mediaconnector.microsoft.MicrosoftTestBase;
import com.cla.connector.mediaconnector.microsoft.model.MSFile;
import com.cla.connector.mediaconnector.microsoft.model.MSFolder;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointExtendedFolder;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointListItem;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointRoleAssignment;
import com.google.common.collect.Sets;
import com.independentsoft.share.*;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystemNotFoundException;
import java.util.*;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created by oren on 12/25/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class OneDriveConnectorTests extends MicrosoftTestBase {

    private String validFile1 = "/Attachments/A.xlsx";
    private String validFile2 = "/Document-test1drv.docx";

    private String user = "ro-admin2_docauthority_onmicrosoft_com";
    private String subsite = "personal/ro-admin2_docauthority_onmicrosoft_com";
    private String fullPath = "/personal/ro-admin2_docauthority_onmicrosoft_com/Documents";

    private OneDriveMediaConnector oneDriveMediaConnector;

    @Before
    public void init()  {
        OneDriveMediaConnector.OneDriveMediaConnectorBuilder builder = OneDriveMediaConnector.builder()
                .withDoInit(false);

        oneDriveMediaConnector = getEnrichedConnector(builder, null);
        initConnectorResources(oneDriveMediaConnector);
    }

    private void preStreamInit(boolean hasChildren) throws ServiceException, FileNotFoundException {
        Folder folder = new MyFolder();
        Folder folder2 = new MyFolder(hasChildren);
        List<Folder> folders = new ArrayList<>();
        folders.add(folder);
        folders.add(folder2);
        when(service.getFolders(anyString(), anyString())).thenReturn(folders);
        when(service.getFolder(anyString(), anyString())).thenReturn(new MyFolder());
        SharePointExtendedFolder msExFolder = new SharePointExtendedFolder();
        msExFolder.setListId(UUID.randomUUID().toString());
        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(anyString(), anyString())).thenReturn(msExFolder);
        List<ClaFilePropertiesDto> filesToRet = new ArrayList<>();
        ClaFilePropertiesDto dto = ClaFilePropertiesDto.create();
        dto.setFolder(false);
        dto.setModTimeMilli(100);
        dto.setFileName("Bulbul\\Akka\\BulBul.pdf");
        filesToRet.add(dto);
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), anyString(), anyString(), anyInt(), anyInt())).thenReturn(filesToRet);

        SharePointListItem item = new SharePointListItem();
        item.setFileSystemObjectType(FileSystemObjectType.FILE);
        item.setId("fghfhf");
        item.setFileRef("dfgdfgd");
        item.setListItemHavingUniqueAcls(true);
        item.setModified("2016-11-03T12:42:44Z");
        item.setSize(55L);
        item.setLoginName("ety|54y");
        when(microsoftDocAuthorityClient.getListItem(any(MSItemKey.class))).thenReturn(item);
        when(microsoftDocAuthorityClient.getListItem(any(), anyString(), anyString())).thenReturn(item);

        List<File> files = new ArrayList<>();
        files.add(MSFile.builder().build());
        files.add(MSFile.builder().build());
        when(service.getFiles(anyString(), anyString())).thenReturn(files);
    }

    @Override
    protected ScanTaskParameters getScanTaskParams(String basePath) {
        ScanTaskParameters params = super.getScanTaskParams(basePath);
        params.setMailboxUPN(params.getPath());
        params.setPath(null);
        return params;
    }

    @Ignore
    public void connectionTest() {
        SharePointConnectionParametersDto connectionParamsDto = new SharePointConnectionParametersDto();
        connectionParamsDto.setUsername(SHARE_POINT_365_USER);
        connectionParamsDto.setPassword(SHARE_POINT_365_PASSWORD);

        MSConnectionConfig config = MSConnectionConfig.builder()
                .connectionTimeout(msClientHttpTimeout)
                .socketTimeout(msClientHttpTimeout)
                .serviceClientTimeout(2000)
                .connectionRequestTimeout(msClientHttpTimeout)
                .build();

        OneDriveMediaConnector.builder()
                .withSharePointConnectionParametersDto(connectionParamsDto)
                .withConnectionConfig(config)
                .withMaxRetries(100)
                .withPageSize(10)
                .withMaxFileSize(MAX_FILE_SIZE)
                .build()
                .testConnection();
    }

    @Test
    public void testStreamMediaItems() throws ServiceException, FileNotFoundException {
        preStreamInit(false);
        List<ClaFilePropertiesDto> accumulator = Lists.newArrayList();
        oneDriveMediaConnector.streamMediaItems(getScanTaskParams("/" + user + "/Attachments"), accumulator::add, null, null, aLong -> true, filePropsProgressTracker);
        assertEquals(3, accumulator.size()); // 2 Files
    }

    @Test
    public void testConcurrentStreamMediaItems() throws ServiceException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(10);

        MSFolder sub11 = MSFolder.Builder.create()
                .withPath("sub11")
                .withFileAmount(1)
                .build();
        MSFolder sub12 = MSFolder.Builder.create()
                .withPath("sub12")
                .withFileAmount(2)
                .build();
        MSFolder sub1 = MSFolder.Builder.create()
                .withPath("sub1")
                .withFileAmount(3)
                .withSubFolders(Arrays.asList(sub11, sub12))
                .build();

        MSFolder sub2 = MSFolder.Builder.create()
                .withPath("sub2")
                .withFileAmount(4)
                .build();

        MSFolder root = MSFolder.Builder.create()
                .withPath("/root/")
                .withFileAmount(4)
                .withSubFolders(Arrays.asList(sub1, sub2))
                .build();

        ScanTaskParameters scanParams = getScanTaskParams(user + "/root");

        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(
                subsite, SharePointParseUtils.normalizePath(fullPath + root.path)))
                .thenReturn(new SharePointExtendedFolder_Tests(root));

        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(
                subsite, SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + root.path + sub1.path)))
                .thenReturn(new SharePointExtendedFolder_Tests(sub1));

        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(
                subsite, SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + root.path + sub1.path + "/" + sub11.path)))
                .thenReturn(new SharePointExtendedFolder_Tests(sub11));

        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(
                subsite, SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + root.path + sub1.path + "/" + sub12.path)))
                .thenReturn(new SharePointExtendedFolder_Tests(sub12));

        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(
                subsite, SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + root.path + sub2.path)))
                .thenReturn(new SharePointExtendedFolder_Tests(sub2));


        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path)))).thenReturn(root.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/")))).thenReturn(sub1.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub2.path + "/")))).thenReturn(sub2.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path)))).thenReturn(sub11.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub2.path + "/" + sub12.path)))).thenReturn(sub12.getFolders());

        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path)), anyString(), anyInt(), anyInt())).thenReturn(root.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path)), anyString(), anyInt(), anyInt())).thenReturn(sub1.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path)), anyString(), anyInt(), anyInt())).thenReturn(sub11.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub12.path)), anyString(), anyInt(), anyInt())).thenReturn(sub12.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub2.path)), anyString(), anyInt(), anyInt())).thenReturn(sub2.getFilesAsClaFileProp());

        List<ClaFilePropertiesDto> accumulator = Collections.synchronizedList(Lists.newArrayList());

        oneDriveMediaConnector.concurrentStreamMediaItems(
                forkJoinPool,
                scanParams,
                accumulator::add,
                null,
                null,
                aLong -> true,
                filePropsProgressTracker);

        // TODO why is the number of invocations 6 ?
        //verify(microsoftDocAuthorityClient, times(3)).getSharePointExtendedFolderDetails(subsite, SharePointParseUtils.normalizePath(fullPath + root.path));

        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path)));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/")));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub2.path + "/")));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path + "/")));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub12.path + "/")));

        List<String> fileNames = flatFileList(root);
        fileNames.add(root.path);

        assertEquals(fileNames.size(), accumulator.size());

        /*List<String> receivedFiles = accumulator.stream()
                .map(ClaFilePropertiesDto::getFileName)
                .collect(Collectors.toList());*/
    }

    @Test
    public void testConcurrentStreamMediaManyItems() throws ServiceException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(10);

        MSFolder sub11 = MSFolder.Builder.create()
                .withPath("sub11")
                .withFileAmount(100)
                .build();
        MSFolder sub12 = MSFolder.Builder.create()
                .withPath("sub12")
                .withFileAmount(222)
                .build();
        MSFolder sub1 = MSFolder.Builder.create()
                .withPath("sub1")
                .withFileAmount(12)
                .withSubFolders(Arrays.asList(sub11, sub12))
                .build();

        MSFolder root = MSFolder.Builder.create()
                .withPath("/root/")
                .withFileAmount(4)
                .withSubFolders(Collections.singletonList(sub1))
                .build();

        ScanTaskParameters scanParams = getScanTaskParams("/" + user + "/root");

        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(subsite, SharePointParseUtils.normalizePath(fullPath + root.path))).thenReturn(new SharePointExtendedFolder_Tests(root));
        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(subsite, SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path))).thenReturn(new SharePointExtendedFolder_Tests(sub1));
        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(subsite, SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path))).thenReturn(new SharePointExtendedFolder_Tests(sub11));
        when(microsoftDocAuthorityClient.getSharePointExtendedFolderDetails(subsite, SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub12.path))).thenReturn(new SharePointExtendedFolder_Tests(sub12));

        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + "/")))).thenReturn(root.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/")))).thenReturn(sub1.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path + "/")))).thenReturn(sub11.getFolders());
        when(service.getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub12.path + "/")))).thenReturn(sub12.getFolders());

        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + "/")), anyString(), anyInt(), anyInt())).thenReturn(root.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path)), anyString(), anyInt(), anyInt())).thenReturn(sub1.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path)), anyString(), anyInt(), anyInt())).thenReturn(sub11.getFilesAsClaFileProp());
        when(microsoftDocAuthorityClient.getFilesWithMediaItemId(any(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub12.path)), anyString(), anyInt(), anyInt())).thenReturn(sub12.getFilesAsClaFileProp());

        List<ClaFilePropertiesDto> accumulator = Collections.synchronizedList(Lists.newArrayList());
        oneDriveMediaConnector.concurrentStreamMediaItems(forkJoinPool, scanParams, accumulator::add, null, null, aLong -> true, filePropsProgressTracker);

        verify(microsoftDocAuthorityClient, times(3)).getSharePointExtendedFolderDetails(subsite, SharePointParseUtils.normalizePath(fullPath + root.path));

        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path)));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/")));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub11.path + "/")));
        verify(service).getFolders(anyString(), eq(SharePointParseUtils.normalizePath(fullPath + root.path + sub1.path + "/" + sub12.path + "/")));

        List<String> fileNames = flatFileList(root);
        fileNames.add("/" + user + "/root");
        assertEquals(fileNames.size(), accumulator.size());
    }

    private List<String> flatFileList(MSFolder folder) {
        List<String> files = folder.getFilesAsClaFileProp()
                .stream()
                .map(ClaFilePropertiesDto::getFileName)
                .collect(Collectors.toList());

        files.add(folder.getName());

        List<String> subFolderFiles = folder.getFolders()
                .stream()
                .map(subFolder -> (MSFolder) subFolder)
                .map(this::flatFileList)
                .flatMap(List::stream)
                .collect(Collectors.toList());

        files.addAll(subFolderFiles);
        return files;
    }

    @Test
    public void testStreamMediaItemsLimitedAmount() throws ServiceException, FileNotFoundException {
        preStreamInit(false);
        Set<String> extensions = Sets.newHashSet("pdf", "docx", "doc", "xls", "xlsx");
        ScanTypeSpecification scanTypes = new ScanTypeSpecification(null, extensions, null);
        ScanTaskParameters scanParams = ScanTaskParameters.create()
                .setMailboxUPN("/")
                .setRunId(-1L)
                .setScanTypeSpecification(scanTypes);

        List<ClaFilePropertiesDto> accumulator = Lists.newArrayList();
        oneDriveMediaConnector.streamMediaItems(scanParams, accumulator::add, null, null, aLong -> true, filePropsProgressTracker);
        assertEquals(3, accumulator.size());
    }

    @Test
    @Ignore // TODO until we fix the accessibility check
    public void testDirectoryExistence() throws ServiceException {

        when(service.getFolder(anyString(), eq(SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + "/")))).thenReturn(new Folder());
        when(service.getFolder(anyString(), eq(SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + "/Attachments/")))).thenReturn(new Folder());
        when(service.getFolder(anyString(), eq(SharePointParseUtils.removeUnneededDoubleSlashes(fullPath + "/Attachments1")))).thenThrow(new ServiceException("", new Throwable(""), ""));

        assertTrue(oneDriveMediaConnector.isDirectoryExists("/" + user + "/", false));
        assertTrue(oneDriveMediaConnector.isDirectoryExists("/" + user + "/Attachments/", false));
        assertFalse(oneDriveMediaConnector.isDirectoryExists("/" + user + "/Attachments1", false));
    }

    @Test
    public void testGetFileAttributes() throws ServiceException, FileNotFoundException {
        final int totalItems = 2;
        String dummyListId = UUID.randomUUID().toString().toUpperCase();
        String targetFiles[] = { validFile1, validFile2 };
        for (int idx=0; idx<totalItems; idx++) {
            SharePointListItem fileItem = new SharePointListItem();
            fileItem.setFileRef(fullPath + targetFiles[idx]);
            when (microsoftDocAuthorityClient.getListItem(eq(MSItemKey.listItem(dummyListId, Integer.toString(idx))))).thenReturn(fileItem);
            when (microsoftDocAuthorityClient.getFilesAttributes(any(), eq(SharePointParseUtils.normalizePath(fullPath + targetFiles[idx])))).thenReturn(new ClaFilePropertiesDto("/" + user + targetFiles[idx], null));
        }

        for (int idx=0; idx<totalItems; idx++) {
            ClaFilePropertiesDto filePropertiesDto;
            try {
                filePropertiesDto = oneDriveMediaConnector.getFileAttributes(dummyListId + "/" + idx);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
            assertEquals(SharePointParseUtils.removeUnneededDoubleSlashes("/" + user + targetFiles[idx]), filePropertiesDto.getFileName());
        }
    }

    @Test(expected = FileNotFoundException.class)
    public void testGetFileAttributesFileNotFound() throws FileNotFoundException, ServiceException {
        String listId = UUID.randomUUID().toString();
        String itemId = "123";
        String mediaItemId = listId + "/" + itemId;
        when(microsoftDocAuthorityClient.getListItem(eq(MSItemKey.listItem(listId, itemId)))).thenThrow(new FileNotFoundException("File not found"));

        oneDriveMediaConnector.getFileAttributes(mediaItemId);
    }

    @Test
    public void testGetInputStream() throws IOException, ServiceException {
        String itemId = "itemId";
        String listId = "listId";
        SharePointListItem item = new SharePointListItem();
        String fileRef = fullPath + validFile1;
        item.setFileRef(fileRef);
        item.setSize(1L);
        when(microsoftDocAuthorityClient.getListItem(eq(MSItemKey.listItem(listId, itemId)))).thenReturn(item);
        when(service.getFile(anyString(), eq(fileRef))).thenReturn(MSFile.builder().build());
        byte[] arr = new byte[5];
        InputStream inputStream = new ByteArrayInputStream(arr);
        when(service.getFileStream(anyString(), anyString())).thenReturn(inputStream);

        String mediaItemId = listId + "/" + itemId;
        ClaFilePropertiesDto fileProps = new ClaFilePropertiesDto("Sharepoint office file.docx", -1L);
        fileProps.setMediaItemId(mediaItemId);
        try (InputStream is = oneDriveMediaConnector.getInputStream(fileProps)) {
            assertNotNull(is);
        }
    }

    @Test
    public void testListSiteFolders() throws ServiceException, FileNotFoundException {
        preStreamInit(true);
        List<ServerResourceDto> folders = oneDriveMediaConnector.browseSiteFolders("/");
        assertTrue(folders.size() > 0);
        assertTrue(
                folders.stream()
                        .anyMatch(folder -> Boolean.TRUE.equals(folder.getHasChildren()))
        );
    }

    /*@Test
    public void testListFilesAsServerResources() throws ServiceException, FileNotFoundException {
        preStreamInit(false);
        String paths[] = {"/", "/Attachments"};
        Arrays.stream(paths).forEach(path -> {
            List<ServerResourceDto> files = oneDriveMediaConnector.listFilesAsServerResources(path);
            assertTrue(files.size() > 0);
        });
    }*/

    @Test
    public void testGetMediaItemAttributes() throws FileNotFoundException, ServiceException {
        preStreamInit(false);
        ClaFilePropertiesDto file = getAFile();
        file.setMediaItemId("st/am");
        ClaFilePropertiesDto fileProps = oneDriveMediaConnector.getMediaItemAttributes(file.getMediaItemId());
        assertNotNull(fileProps);
        assertFalse(fileProps.isFolder());
        assertTrue(fileProps.getModTimeMilli() > 0);
        assertEquals("54y", fileProps.getOwnerName());
    }

    private ClaFilePropertiesDto getAFile() {
        List<ClaFilePropertiesDto> accumulator = Lists.newArrayList();
        ScanTaskParameters taskParams = getScanTaskParams("/" + user + "/Attachments");
        oneDriveMediaConnector.streamMediaItems(taskParams, accumulator::add, null, null, aLong -> true, filePropsProgressTracker);
        assertTrue(accumulator.size() > 1); // root folder + files
        Optional<ClaFilePropertiesDto> fileOpt = accumulator.stream()
                .filter(item -> !item.isFolder())
                .findAny();

        return fileOpt.orElseThrow(() -> new FileSystemNotFoundException("No files found under " + taskParams.getPath()));
    }

    @Test
    public void testFetchMediaItemAcls() throws ServiceException, FileNotFoundException {
        List<SharePointRoleAssignment> listItemPermissions = new ArrayList<>();
        SharePointRoleAssignment role = new SharePointRoleAssignment();
        role.setAclType(AclType.READ_TYPE);
        role.setLoginName("gggg");
        listItemPermissions.add(role);

        role = new SharePointRoleAssignment();
        role.setAclType(AclType.WRITE_TYPE);
        role.setLoginName("gggg");
        listItemPermissions.add(role);

        role = new SharePointRoleAssignment();
        role.setAclType(AclType.DENY_READ_TYPE);
        role.setLoginName("gggg3333");
        listItemPermissions.add(role);

        role = new SharePointRoleAssignment();
        role.setAclType(AclType.DENY_WRITE_TYPE);
        role.setLoginName("gggg3333");
        listItemPermissions.add(role);

        when(microsoftDocAuthorityClient.getListItemPermissions(any(MSItemKey.class))).thenReturn(listItemPermissions);
        when(microsoftDocAuthorityClient.getListPermissions(any(), anyString())).thenReturn(listItemPermissions);

        preStreamInit(false);
        ClaFilePropertiesDto file = getAFile();
        file.setMediaItemId("st/am");
        String owner = "##file-owner##";
        file.setOwnerName(owner);
        oneDriveMediaConnector.fetchAcls(file);
        assertTrue(
                file.getAclReadAllowed().size() > 0
                        || file.getAclWriteAllowed().size() > 0
                        || file.getAclReadDenied().size() > 0
                        || file.getAclWriteDenied().size() > 0);
        assertNotNull(file.getAclSignature());
        assertTrue(file.getAclReadAllowed().contains(owner));
        assertTrue(file.getAclWriteAllowed().contains(owner));
    }

    @Test
    @Ignore // TODO TEMP - until media changelog fix
    public void testMediaChangeLog() throws ServiceException {
        String itemPath = "/" + user + "/Attachments/";
        List<MediaChangeLogDto> changeList = Lists.newArrayList();
        Consumer<MediaChangeLogDto> dummyConsumer = changeList::add;
        long scanTime = System.currentTimeMillis();

        ReflectionTestUtils.setField(oneDriveMediaConnector, "url", "");

        String dummyListId = UUID.randomUUID().toString().toUpperCase();
        MockedChangeItem newFile = new MockedChangeItem(dummyListId, 1, ChangeType.ADD, scanTime - 19);
        MockedChangeItem delFile = new MockedChangeItem(dummyListId, 2, ChangeType.DELETE_OBJECT, scanTime - 19);
        MockedChangeItem renamedFile = new MockedChangeItem(dummyListId, 3, ChangeType.RENAME, scanTime - 19);
        MockedChangeItem updatedFile = new MockedChangeItem(dummyListId, 4, ChangeType.UPDATE, scanTime - 19);

        List<Change> mockedChangeList = Arrays.asList(newFile, delFile, renamedFile, updatedFile);
        when(service.getChanges(anyString(), any(ChangeQuery.class))).thenReturn(mockedChangeList);

        ScanTaskParameters scanParams = getScanTaskParams("/" + user + "/Attachments")
                .setStartScanTime(scanTime);

        Site site = new Site();
        ReflectionTestUtils.setField(site, "id", "qqq");
        when(service.getSite(anyString())).thenReturn(site);
        mockedChangeList.stream()
                .map(item -> (MockedChangeItem) item)
                .forEach(item -> {
                    try {
                        SharePointListItem spItem = new SharePointListItem();
                        spItem.setFileSystemObjectType(FileSystemObjectType.FILE);
                        spItem.setId(Integer.toString(item.getItemId()));
                        spItem.setFileRef(SharePointParseUtils.removeUnneededDoubleSlashes(itemPath + "mock-file-name.bin"));
                        spItem.setListItemHavingUniqueAcls(true);
                        spItem.setModified("2016-11-03T12:42:44Z");
                        spItem.setSize(55L);
                        spItem.setLoginName("ety|54y");
                        when(microsoftDocAuthorityClient.getListItem(any(), eq(item.getListId()), eq(Integer.toString(item.getItemId())))).thenReturn(spItem);
                    } catch (ServiceException | FileNotFoundException e) {
                        e.printStackTrace();
                    }
                });
        Consumer<String> createScanTaskConsumer = s -> {}; // just a dummy createScanTaskConsumer
        String path = "https://docauthority-my.sharepoint.com/Attachments/";
        oneDriveMediaConnector.streamMediaChangeLog(
                scanParams, path, -1L, scanTime - 100, dummyConsumer, createScanTaskConsumer, filePath -> true);

        System.out.println(mockedChangeList.toString());
        assertEquals(mockedChangeList.size(), changeList.size());

        Map<String, MediaChangeLogDto> changeMap = SharePointParseUtils.convertToMediaChangeLogDtos(dummyListId, mockedChangeList)
                .stream()
                .collect(Collectors.toMap(MediaChangeLogDto::getMediaItemId, Function.identity()));
        assertFalse(changeList.stream()
                .anyMatch(chg -> !chg.getEventType().equals(changeMap.get(chg.getMediaItemId()).getEventType())));

    }

    @Test
    @Ignore // TODO TEMP - until media changelog fix
    public void testConcurrentMediaChangeLog() throws ServiceException {
        ForkJoinPool forkJoinPool = new ForkJoinPool(10);
        long scanTime = System.currentTimeMillis();
        String itemPath = "/" + user + "/Attachments/";
        List<MediaChangeLogDto> changeList = Collections.synchronizedList(Lists.newArrayList());
        Consumer<MediaChangeLogDto> dummyConsumer = changeList::add;
        String dummyListId = UUID.randomUUID().toString().toUpperCase();
        MockedChangeItem newFile = new MockedChangeItem(dummyListId, 1, ChangeType.ADD, scanTime - 19);
        MockedChangeItem delFile = new MockedChangeItem(dummyListId, 2, ChangeType.DELETE_OBJECT, scanTime - 19);
        MockedChangeItem renamedFile = new MockedChangeItem(dummyListId, 3, ChangeType.RENAME, scanTime - 19);
        MockedChangeItem updatedFile = new MockedChangeItem(dummyListId, 4, ChangeType.UPDATE, scanTime - 19);

        ReflectionTestUtils.setField(oneDriveMediaConnector, "url", "");

        MediaChangeLogDto subFolderNewFile = new MediaChangeLogDto(dummyListId + "/" + 13, DiffType.NEW);
        List<Change> mockedChangeList = Arrays.asList(newFile, delFile, renamedFile, updatedFile);
        Site site = new Site();
        ReflectionTestUtils.setField(site, "id", "123");

        when(service.getSite(anyString())).thenReturn(site);
        when(service.getChanges(anyString(), any(ChangeQuery.class))).thenReturn(mockedChangeList);

        mockedChangeList.stream()
                .map(item -> (MockedChangeItem) item)
                .forEach(item -> {
                    try {
                        SharePointListItem spItem = new SharePointListItem();
                        spItem.setFileSystemObjectType(FileSystemObjectType.FILE);
                        spItem.setId(Integer.toString(item.getItemId()));
                        spItem.setFileRef(SharePointParseUtils.removeUnneededDoubleSlashes(itemPath + "mock-file-name.bin"));
                        spItem.setListItemHavingUniqueAcls(true);
                        spItem.setModified("2016-11-03T12:42:44Z");
                        spItem.setSize(55L);
                        spItem.setLoginName("ety|54y");
                        when(microsoftDocAuthorityClient.getListItem(any(), eq(item.getListId()), eq(Integer.toString(item.getItemId())))).thenReturn(spItem);
                    } catch (ServiceException | FileNotFoundException e) {
                        e.printStackTrace();
                    }
                });

        String path = "https://docauthority-my.sharepoint.com/Attachments/";
        ScanTaskParameters scanParams = getScanTaskParams("/" + user + "/Attachments")
                .setStartScanTime(scanTime)
                .setPath(path);
        Consumer<String> createScanTaskConsumer = s -> {};  // just a dummy createScanTaskConsumer
        oneDriveMediaConnector.concurrentStreamMediaChangeLog(
                forkJoinPool, scanParams, path, -1L, scanTime - 100, dummyConsumer, createScanTaskConsumer, filePath -> true);

        assertEquals(mockedChangeList.size(), changeList.size());
        Map<String, MediaChangeLogDto> changeMap = SharePointParseUtils.convertToMediaChangeLogDtos(dummyListId, mockedChangeList)
                .stream()
                .collect(Collectors.toMap(MediaChangeLogDto::getMediaItemId, Function.identity()));
        changeMap.put(subFolderNewFile.getMediaItemId(), subFolderNewFile);
        assertFalse(changeList.stream()
                .anyMatch(chg -> !chg.getEventType().equals(changeMap.get(chg.getMediaItemId()).getEventType())));
    }

    private class MockedChangeItem extends ChangeItem {
        private String listId;

        private int itemId;

        private ChangeType type;

        private Date time;

        MockedChangeItem(String listId, int itemId, ChangeType type, long changeTime) {
            this.listId = listId;
            this.itemId = itemId;
            this.type = type;
            this.time = new Date(changeTime);
        }

        @Override
        public String getListId() {
            return listId;
        }

        @SuppressWarnings("unused")
        public void setListId(String listId) {
            this.listId = listId;
        }

        @Override
        public int getItemId() {
            return itemId;
        }

        @SuppressWarnings("unused")
        public void setItemId(int itemId) {
            this.itemId = itemId;
        }

        @Override
        public ChangeType getType() {
            return type;
        }

        @SuppressWarnings("unused")
        public void setType(ChangeType type) {
            this.type = type;
        }

        public Date getTime() {
            return time;
        }

        @SuppressWarnings("unused")
        public void setTime(Date changeTime) {
            this.time = changeTime;
        }
    }

    @SuppressWarnings("unused")
    private class MyFolder extends Folder {
        private boolean hasChildren = false;

        MyFolder() {}

        MyFolder(boolean hasChildren) {
            this.hasChildren = hasChildren;
        }

        public Boolean getHasChildren() {
            return hasChildren;
        }

        public int getItemCount() {
            return hasChildren ? 1 : 0;
        }

        public String getName() {
            return "fghfg";
        }
        public Date getCreatedTime() {
            return new Date();
        }
        public Date getLastModifiedTime() {
            return new Date();
        }
        public String getServerRelativeUrl() {
            return "/" + user + "A folder";
        }
    }

    private static class SharePointExtendedFolder_Tests extends SharePointExtendedFolder {
        SharePointExtendedFolder_Tests(MSFolder folder) {
            setServerRelativeUrl(folder.getName());
            setListTitle(folder.getName());
            setCreationTime(folder.getCreatedTime().getTime());
            setLastModifiedTime(folder.getLastModifiedTime().getTime());
            setListId(UUID.randomUUID().toString());
        }
    }
}
