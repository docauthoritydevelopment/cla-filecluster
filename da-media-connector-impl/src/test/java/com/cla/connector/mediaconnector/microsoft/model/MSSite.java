package com.cla.connector.mediaconnector.microsoft.model;

import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointExtendedFolder;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Singular;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Builder
public class MSSite implements MSFileSystemItem {

    private static final List<MSFile> emptyList = Collections.unmodifiableList(Lists.newArrayList());

    String connectorPath;
    String siteRelativeUrl;

    @Singular
    List<MSSite> subSites;

    @Singular
    List<MSDocumentLibrary> docLibs;

    @Override
    public List<MSFileSystemItem> getSubFolders() {
        List<MSFileSystemItem> output = Optional.ofNullable(subSites)
                .map(sites -> sites.stream().map(site -> (MSFileSystemItem) site).collect(Collectors.toList()))
                .orElseGet(Lists::newArrayList);

        Optional.ofNullable(docLibs)
                .map(libs -> libs.stream().map(lib -> ((MSFileSystemItem) lib)).collect(Collectors.toList()))
                .ifPresent(output::addAll);

        return output;
    }

    @Override
    public List<MSFile> getFiles() {
        return emptyList;
    }

    public List<MSDocumentLibrary> getDocLibs() {
        return docLibs;
    }

    public String getSiteRelativeUrl() {
        return siteRelativeUrl;
    }

    public List<MSSite> getSubSites() {
        return subSites;
    }

    public List<ServerResourceDto> getSubSitesAsServerResourceDto() {
        return subSites.stream()
                .map(MSSite::asServerResourceDto)
                .collect(Collectors.toList());
    }

    public ServerResourceDto asServerResourceDto() {
        return new ServerResourceDto(SharePointParseUtils.normalizePath(this.connectorPath + siteRelativeUrl), siteRelativeUrl);
    }

    public SharePointExtendedFolder toSharePointExtendedFolder() {
        return new SharePointExtendedFolder();
    }
}