package com.cla.connector.mediaconnector.fileshare;

import com.cla.connector.utils.Pair;
import org.junit.Test;

import static org.junit.Assert.*;

public class FileShareMediaConnectorTest {

    @Test
    public void testGetComputerShareFromPath() {
        String rootFolderPath = "\\\\localhost\\yael\\wasHebrew(Test588)kk";
        Pair<String, String> res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res != null);
        assertEquals("localhost", res.getKey());
        assertEquals("yael", res.getValue());

        rootFolderPath = "\\\\localhost:675\\yael\\wasHebrew(Test588)kk";
        res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res != null);
        assertEquals("localhost", res.getKey());
        assertEquals("yael", res.getValue());

        rootFolderPath = "\\\\172.31.41.248\\Enron\\Extraction";
        res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res != null);
        assertEquals("172.31.41.248", res.getKey());
        assertEquals("Enron", res.getValue());

        rootFolderPath = "\\\\172.31.41.248:8080\\Enron\\Extraction";
        res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res != null);
        assertEquals("172.31.41.248", res.getKey());
        assertEquals("Enron", res.getValue());

        rootFolderPath = "//172.31.41.248:8080/Enron/Extraction";
        res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res != null);
        assertEquals("172.31.41.248", res.getKey());
        assertEquals("Enron", res.getValue());

        rootFolderPath = "D:\\Enron\\encodingChk";
        res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res == null);

        rootFolderPath = "encodingChk";
        res = (FileShareMediaConnector.getComputerShareFromPath(rootFolderPath));
        assertTrue(res == null);
    }

}