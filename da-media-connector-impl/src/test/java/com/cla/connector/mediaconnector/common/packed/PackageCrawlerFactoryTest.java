package com.cla.connector.mediaconnector.common.packed;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertTrue;

public class PackageCrawlerFactoryTest {

	private static final String ZIP_PATH_FILENAME = "test\\best\\nest.zip";
	private static  final String UNKNOWN_PATH_FILENAME = "test\\best\\nest.ziv";
	private ClaFilePropertiesDto filePropertiesDtoZip = new ClaFilePropertiesDto(ZIP_PATH_FILENAME, 123L);

	@Test
	public void getZipPackageCrawlerTest() {
		Optional<PackageCrawler> packageCrawler = PackageCrawlerFactory.getPackageCrawler(ZIP_PATH_FILENAME);
		assertTrue(packageCrawler.isPresent());
		assertTrue(packageCrawler.get() instanceof LinearZipCrawler);
	}

	public void getUnknownPackageCrawlerTest() {
		Optional<PackageCrawler> packageCrawler = PackageCrawlerFactory.getPackageCrawler(UNKNOWN_PATH_FILENAME);
		assertTrue(packageCrawler.isPresent());
		assert(packageCrawler.get() instanceof LinearZipCrawler);
	}

	@Test
	public void getPackageCrawler() {
		Optional<PackageCrawler> packageCrawler = PackageCrawlerFactory.getPackageCrawler(filePropertiesDtoZip);
		assertTrue(packageCrawler.isPresent());
		assertTrue(packageCrawler.get() instanceof LinearZipCrawler);
	}
}