package util;

import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by uri on 17/09/2015.
 */
public class FacetPageImpl<T>  {

    private List<T> content = new ArrayList<T>();

    private int pageSize;
    private int pageNumber;

    private long totalAggregatedCount;
    private long pageAggregatedCount;
    private long remainingAggregatedCount;

    private boolean isLast = false;

    public FacetPageImpl() {
    }

    public FacetPageImpl(final List<T> content, int pageSize, int pageNumber, long totalAggregatedCount, long pageAggregatedCount) {
        this.totalAggregatedCount = totalAggregatedCount;
        this.pageAggregatedCount = pageAggregatedCount;
        remainingAggregatedCount = totalAggregatedCount - pageAggregatedCount;
        this.pageSize = pageSize;
        this.pageNumber = pageNumber;
        if (content == null){
            throw new IllegalArgumentException("Content must not be null!");
        }
        this.content.addAll(content);
        if (content.size() != pageSize) {
            isLast = true;
        }
    }

    public FacetPageImpl(final List<T> content, int pageSize, int pageNumber) {
        this(content,pageSize,pageNumber,0,0);
    }

    //TODO: eliminate those constructors in order to eliminate dependency on Spring and move this class to common
    public FacetPageImpl(List content, PageRequest pageRequest) {
        this(content,pageRequest.getPageSize(),pageRequest.getPageNumber() + 1); //Shift 1 for output
    }

    public FacetPageImpl(List content, PageRequest pageRequest, long pageAggregatedCount, long totalAggregatedCount) {
        this(content,pageRequest.getPageSize(),pageRequest.getPageNumber() + 1,totalAggregatedCount,pageAggregatedCount);
    }

    public List<T> getContent() {
        return content;
    }

    /*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Slice#getNumberOfElements()
	 */
    public int getNumberOfElements() {
        return content.size();
    }

    /*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Slice#isLast()
	 */
    public boolean isLast() {
        return !hasNext();
    }

    /**
     * TODO - refrain from naive approach
     * @return
     */
    private boolean hasNext() {
        return content.size() == pageSize;
    }

    /*
	 * (non-Javadoc)
	 * @see org.springframework.data.domain.Slice#hasContent()
	 */
    public boolean hasContent() {
        return !content.isEmpty();
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setIsLast(boolean isLast) {
        this.isLast = isLast;
    }

    public long getTotalAggregatedCount() {
        return totalAggregatedCount;
    }

    public long getPageAggregatedCount() {
        return pageAggregatedCount;
    }

    public long getRemainingAggregatedCount() {
        return remainingAggregatedCount;
    }

    public void setContent(List<T> content) {
        this.content = content;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setTotalAggregatedCount(long totalAggregatedCount) {
        this.totalAggregatedCount = totalAggregatedCount;
    }

    public void setPageAggregatedCount(long pageAggregatedCount) {
        this.pageAggregatedCount = pageAggregatedCount;
    }

    public void setRemainingAggregatedCount(long remainingAggregatedCount) {
        this.remainingAggregatedCount = remainingAggregatedCount;
    }

    public void setLast(boolean last) {
        isLast = last;
    }
}
