package util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by uri on 02/01/2017.
 */
public class ConversionUtils {

    private final static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES,true);
    }

    public static ObjectMapper getMapper() {
        return mapper;
    }
}
