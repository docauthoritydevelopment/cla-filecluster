package util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

/**
 * Created by uri on 19/01/2017.
 */
public class RestExceptionUtils {
    final static Logger logger = LoggerFactory.getLogger(RestExceptionUtils.class);


    public static void logRestErrorAndThrow(RestClientException e) {
        if (e instanceof HttpClientErrorException) {
            HttpClientErrorException e1 = (HttpClientErrorException) e;
            String responseBodyAsString = e1.getResponseBodyAsString();
            logger.error("Rest error: {} with message {}",e1.getStatusCode(),responseBodyAsString);
        }
        else {
            logger.error("Rest error {}",e.getMessage(),e);
        }
        throw e;
    }
}
