package util;

import org.springframework.http.*;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by uri on 28/12/2016.
 */
public class RestClient {

    private final String url;
    private final String username;
    private final String password;
    private String authCookieCache;
    private long authCookieLastUse;

    private Object authCookieLock = new Object();

    private long authCookieCacheExpiry = 120000;

    static RestClient restClient;

    public static RestClient createRestClient(String url, String username, String password) {
        restClient = new RestClient(url, username, password);
        restClient.create();
        return restClient;
    }

    public static RestClient getRestClient() {
        return restClient;
    }

    private RestClient(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public RestTemplate create() {
        final String authCookie = getAuthCookie(username, password);
        FormAuthenticatedRestTemplate restTemplate = new FormAuthenticatedRestTemplate(authCookie);
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
        return restTemplate;
    }

    private String getAuthCookie(final String username, final String password) {
        synchronized (authCookieLock) {
            if (authCookieCache != null && System.currentTimeMillis() - authCookieLastUse < authCookieCacheExpiry) {
                authCookieLastUse = System.currentTimeMillis();
                return authCookieCache;
            }
            final HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            final HttpEntity<String> requestEntity = new HttpEntity<String>("username=" + username + "&password=" + password, requestHeaders);
            String loginUrl = url + "/login";
            final ResponseEntity<String> rs = new RestTemplate().exchange(loginUrl, HttpMethod.POST, requestEntity, String.class);
            if (!rs.getStatusCode().equals(HttpStatus.OK)) {
                throw new RuntimeException("Remote file processor - Login failed: " + rs.getStatusCode());
            }
            final String authCookie = rs.getHeaders().getFirst("Set-Cookie").split(" ")[0];
            authCookieCache = authCookie;
            authCookieLastUse = System.currentTimeMillis();
            return authCookie;
        }
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
