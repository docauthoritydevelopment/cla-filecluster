package util;

import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

public class FormAuthenticatedRestTemplate extends RestTemplate {

	private final String authCookie;

	public FormAuthenticatedRestTemplate(final String authCookie) {
		this.authCookie = authCookie;
	}

	@Override
	protected ClientHttpRequest createRequest(final URI url, final HttpMethod method) throws IOException {
		final ClientHttpRequest request = super.createRequest(url, method);
		request.getHeaders().put("Cookie", Arrays.asList(authCookie));
		return request;
	}

}
