package keywords.docauthority;

import com.cla.common.domain.dto.crawler.FileCrawlingStateDto;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import util.RestClient;
import util.RestExceptionUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;


/**
 * FileCrawlerApiController
 * Created by uri on 27/12/2016.
 */
@RobotKeywords
public class CrawlerApiKeywords {

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    private static final Logger logger = LoggerFactory.getLogger(CrawlerApiKeywords.class);

    public void connectToFileCluster(String url, String username, String password) {
        System.out.println("connect to file cluster");
        RestClient restClient = RestClient.createRestClient(url, username, password);
        System.out.println("file cluster connected");
    }

    /**
     * FileCrawlerApiController.rerunOnRootFolder
     * @param rootFolderId
     */
    @RobotKeyword
    public void runOnRootFolder(long rootFolderId) {
        RestClient restClient = RestClient.getRestClient();
        RestTemplate restTemplate = restClient.create();
        String url = restClient.getUrl() + "/api/rerun/root/" + rootFolderId;
        logger.info("Run on rootFolder {} - post to URL: {}",rootFolderId,url);
        try {
            restTemplate.postForLocation(url,null);
        } catch (RestClientException e) {
            RestExceptionUtils.logRestErrorAndThrow(e);
        }
    }

    /**
     * FileCrawlerApiController.getFileCrawlingState
     */
    @RobotKeyword("get File Crawling State")
    public FileCrawlingStateDto getFileCrawlingState() {
        RestClient restClient = RestClient.getRestClient();
        RestTemplate restTemplate = restClient.create();
        String url = restClient.getUrl() + "/api/run/state/full";
        FileCrawlingStateDto result = restTemplate.getForObject(url, FileCrawlingStateDto.class);
        return result;
    }

    @RobotKeyword("collect logs to file")
    public void collectLogsToFile(String fileName) throws IOException {
        Path parent = Paths.get(fileName).getParent();
        Files.createDirectories(parent);
        RestClient restClient = RestClient.getRestClient();
        RestTemplate restTemplate = restClient.create();
        String url = restClient.getUrl() + "/api/logs/fetch";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class, "1");
        if (response.getStatusCode() == HttpStatus.OK) {
            Files.write(Paths.get(fileName), response.getBody());
        }

    }

    @RobotKeyword("Stop FileCrawler")
    public void stopFileCrawler() {
        RestClient restClient = RestClient.getRestClient();
        RestTemplate restTemplate = restClient.create();
        String url = restClient.getUrl() + "/api/run/stop";
        restTemplate.postForEntity(url,null,null);
    }
}
