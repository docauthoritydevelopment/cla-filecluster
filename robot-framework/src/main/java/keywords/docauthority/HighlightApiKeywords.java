package keywords.docauthority;

import com.cla.common.domain.dto.extraction.FileHighlightsDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.JsonDtoConversionUtils;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;
import util.RestClient;

import java.net.URI;

/**
 * Created by uri on 16/01/2017.
 */
@RobotKeywords
public class HighlightApiKeywords {

    String base ="/api/files";

    Logger logger = LoggerFactory.getLogger(HighlightApiKeywords.class);

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    @RobotKeyword("find Highlights In File")
    public FileHighlightsDto findHighlightsInFile(long fileId, String text) {
        RestClient restClient = RestClient.getRestClient();
        FilterDescriptor filterDescriptor = new FilterDescriptor("contentFilter",text,"contains");
        String filterString = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+fileId+"/highlight/list")
                .queryParam("filter", filterString)
                .build().encode().toUri();
        logger.debug("Result URL: "+url);
        FileHighlightsDto result= restClient.create().getForObject(url, FileHighlightsDto.class);
        return result;
    }

}
