package keywords.docauthority;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePartitionDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;
import util.ConversionUtils;
import util.CustomPageImpl;
import util.FacetPageImpl;
import util.RestClient;

import java.net.URI;
import java.util.Collection;

/**
 * Created by uri on 09/01/2017.
 */
@RobotKeywords
public class DateRangeKeywords {

    private String base = "/api/daterange";

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    @RobotKeyword("List Date Range Partitions")
    public Collection<DateRangePartitionDto> listDateRangePartitions() {
        RestClient restClient = RestClient.getRestClient();
        ParameterizedTypeReference<CustomPageImpl<DateRangePartitionDto>> responseType = new ParameterizedTypeReference<CustomPageImpl<DateRangePartitionDto>>() {
        };
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/partitions")
                .build().encode().toUri();
        System.out.println("Result URL: "+url);

        try {
            ResponseEntity<CustomPageImpl<DateRangePartitionDto>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
            return exchange.getBody().getContent();
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RobotKeyword("List Last Modified File Count")
    public Collection<AggregationCountItemDTO<DateRangeItemDto>>listLastModifiedFileCount(long partitionId, FilterDescriptor filterDescriptor) throws JsonProcessingException {
        RestClient restClient = RestClient.getRestClient();

        ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<DateRangeItemDto>>> responseType = new ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<DateRangeItemDto>>>(){ };
        String filterString = ConversionUtils.getMapper().writeValueAsString(filterDescriptor);

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/partitions/"+partitionId+"/filecount/lastmodified")
                .queryParam("filter", filterString)
                .build().encode().toUri();
        System.out.println("Result URL: "+url);

        ResponseEntity<FacetPageImpl<AggregationCountItemDTO<DateRangeItemDto>>> exchange = null;
        try {
            exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return exchange.getBody().getContent();
    }

}
