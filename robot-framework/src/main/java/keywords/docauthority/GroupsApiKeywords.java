package keywords.docauthority;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;
import util.ConversionUtils;
import util.FacetPageImpl;
import util.RestClient;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by uri on 02/01/2017.
 */
@RobotKeywords
public class GroupsApiKeywords {

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    @RobotKeyword
    public Collection<GroupDto> listGroupsWithFilter(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        Collection<AggregationCountItemDTO<GroupDto>> aggregationCountItemDTOS = listGroupsFileCountWithFilter(filterDescriptor);
        List<GroupDto> result = aggregationCountItemDTOS.stream().map(g -> g.getItem()).collect(Collectors.toList());
        return result;
    }

    /**
     * list files with filter  ${filter}
     */
    @RobotKeyword
    public Collection<AggregationCountItemDTO<GroupDto>>listGroupsFileCountWithFilter(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        RestClient restClient = RestClient.getRestClient();

        ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<GroupDto>>> responseType = new ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<GroupDto>>>(){ };
        String filterString = ConversionUtils.getMapper().writeValueAsString(filterDescriptor);

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + "/api/group/filecount")
                .queryParam("filter", filterString)
                .build().encode().toUri();
        //TODO - by default we should fetch more than the default 30 files
        System.out.println("List groups result URL: "+url);

        //TODO - by default we should fetch more than the default 30 files
        ResponseEntity<FacetPageImpl<AggregationCountItemDTO<GroupDto>>> exchange = null;
        try {
            exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return exchange.getBody().getContent();
    }

}
