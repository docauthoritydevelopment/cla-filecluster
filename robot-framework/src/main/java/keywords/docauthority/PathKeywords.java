package keywords.docauthority;

import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Paths;

/**
 * Created by uri on 15/01/2017.
 */
@RobotKeywords
public class PathKeywords {

    Logger logger = LoggerFactory.getLogger(PathKeywords.class);

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    @RobotKeyword
    public String normalizePath(String path) {
        logger.debug("Normalize path {}",path);
        return Paths.get(path).toAbsolutePath().normalize().toString();
    }
}
