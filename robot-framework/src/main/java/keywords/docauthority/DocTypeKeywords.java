package keywords.docauthority;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;
import util.ConversionUtils;
import util.CustomPageImpl;
import util.FacetPageImpl;
import util.RestClient;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by uri on 09/01/2017.
 */
@RobotKeywords
public class DocTypeKeywords {

    String base = "/api/doctype";

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    private Logger logger = LoggerFactory.getLogger(DocTypeKeywords.class);

    @RobotKeyword("List DocTypes")
    public Collection<DocTypeDto> listDocTypes() {
        RestClient restClient = RestClient.getRestClient();
        ParameterizedTypeReference<CustomPageImpl<DocTypeDto>> responseType = new ParameterizedTypeReference<CustomPageImpl<DocTypeDto>>() {};
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/list")
                .build().encode().toUri();
        System.out.println("Result URL: "+url);

        try {
            ResponseEntity<CustomPageImpl<DocTypeDto>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
            List<DocTypeDto> content = exchange.getBody().getContent();
            logger.debug("Found {} docTypes",content.size());
            return content;
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RobotKeyword("Add DocType Without Parent")
    public DocTypeDto addDocTypeWithoutParent(String name) {
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base;
        DocTypeDto docTypeDto = new DocTypeDto();
        docTypeDto.setName(name);
        //docTypeDto.setParentId(parentId);
        System.out.println("Create docType " + docTypeDto);
        HttpEntity<?> requestEntity = new HttpEntity<>(docTypeDto);
        ResponseEntity<DocTypeDto> exchange = restClient.create().exchange(url, HttpMethod.PUT, requestEntity, DocTypeDto.class);
        System.out.println("Got status code: " + exchange.getStatusCode());
        System.out.println("Got body : " + exchange.getBody());
        return exchange.getBody();

    }

    @RobotKeyword("Add DocType With Parent")
    public DocTypeDto addDocTypeWithParent(String name, long parentId) {
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base;
        DocTypeDto docTypeDto = new DocTypeDto();
        docTypeDto.setName(name);
        docTypeDto.setParentId(parentId);
        System.out.println("Create docType " + docTypeDto);
        HttpEntity<?> requestEntity = new HttpEntity<>(docTypeDto);
        ResponseEntity<DocTypeDto> exchange = restClient.create().exchange(url, HttpMethod.PUT, requestEntity, DocTypeDto.class);
        System.out.println("Got status code: " + exchange.getStatusCode());
        System.out.println("Got body : " + exchange.getBody());
        return exchange.getBody();

    }


    @RobotKeyword("findDocTypeByNameOnly")
    public DocTypeDto findDocTypeByNameOnly(String name) {
        Collection<DocTypeDto> fileDocTypeDtos = listDocTypes();
        for (DocTypeDto fileDocTypeDto : fileDocTypeDtos) {
            if (name.equalsIgnoreCase(fileDocTypeDto.getName())) {
                return fileDocTypeDto;
            }
        }
        throw new RuntimeException("Failed to find a tag type by name "+name);
    }


    @RobotKeyword("findDocTypeByName")
    public DocTypeDto findDocTypeByName(String name, long parentId) {
        Collection<DocTypeDto> fileDocTypeDtos = listDocTypes();
        for (DocTypeDto fileDocTypeDto : fileDocTypeDtos) {
            if (name.equalsIgnoreCase(fileDocTypeDto.getName())) {
                logger.debug("Found docType {}",fileDocTypeDto);
                return fileDocTypeDto;
            }
        }
        throw new RuntimeException("Failed to find a tag type by name "+name);
    }

    @RobotKeyword("delete doc type")
    public void deleteDocTypeByName(String name, long parentId) {
        RestClient restClient = RestClient.getRestClient();
        //logger.info("Delete doc type {} of type {}",name,parentId);
        DocTypeDto docTypeByName = findDocTypeByName(name, parentId);
        if (docTypeByName == null) {
            throw new RuntimeException("Unable to delete doc type "+name+" with parent id "+parentId+". DocType not found");
        }
      //  logger.info("Delete file tag {}",docTypeByName);
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/" + docTypeByName.getId())
                .build().encode().toUri();
        restClient.create().delete(url);
    }

    @RobotKeyword("Update DocType")
    public DocTypeDto updateDocType(DocTypeDto docTypeDto) {
        logger.info("Update docType to {}",docTypeDto);
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base+  "/" + docTypeDto.getId();
        HttpEntity<?> requestEntity = new HttpEntity<>(docTypeDto);
        ResponseEntity<DocTypeDto> exchange = restClient.create().exchange(url, HttpMethod.POST, requestEntity, DocTypeDto.class);
        System.out.println("Got status code: " + exchange.getStatusCode());
        System.out.println("Got body : " + exchange.getBody());
        return exchange.getBody();

    }

    @RobotKeyword("Move DocType")
    public DocTypeDto moveDocType(long docTypeId, long newParentId) {
        logger.info("Move docType {} to {}",docTypeId,newParentId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/move")
                .queryParam("newParentId",newParentId)
                .build().encode().toUri();

        ResponseEntity<DocTypeDto> exchange = restClient.create().exchange(url, HttpMethod.POST, null, DocTypeDto.class);
        System.out.println("Got status code: " + exchange.getStatusCode());
        System.out.println("Got body : " + exchange.getBody());
        return exchange.getBody();
    }

    @RobotKeyword("Delete docType")
    public void deleteDocType(long docTypeId) {
        logger.info("Delete doc type {} ",docTypeId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+docTypeId;
        restClient.create().delete(keywordUrl);
    }

    @RobotKeyword("Assign docType to file")
    public void assignDocTypeToFile(long docTypeId, long fileId) {
        logger.info("Assign doc type {} to file {}",docTypeId,fileId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/file/"+fileId)
                .build().encode().toUri();
        restClient.create().put(url,null);
    }

    @RobotKeyword("Assign docType to group")
    public void assignDocTypeToGroup(long docTypeId, long groupId) {
        logger.info("Assign doc type {} to group {}",docTypeId,groupId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/bid/"+groupId)
                .build().encode().toUri();
        restClient.create().put(url,null);
    }

    @RobotKeyword("Assign docType to folder")
    public void assignDocTypeToFolder(long docTypeId, long folderId) {
        logger.info("Assign doc type {} to folder {}",docTypeId,folderId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/folder/"+folderId)
                .build().encode().toUri();
        restClient.create().put(url,null);
    }

    @RobotKeyword("Remove docType from file")
    public void removeDocTypeFromFile(long docTypeId, long fileId) {
        logger.info("Remove doc type {} from file {}",docTypeId,fileId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/file/"+fileId)
                .build().encode().toUri();
        restClient.create().delete(url);
    }

    @RobotKeyword("Remove docType from group")
    public void removeDocTypeFromGroup(long docTypeId, long groupId) {
        logger.info("Remove doc type {} from group {}",docTypeId,groupId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/bid/"+groupId)
                .build().encode().toUri();
        restClient.create().delete(url);
    }

    @RobotKeyword("Remove docType from folder")
    public void removeDocTypeFromFolder(long docTypeId, long folderId) {
        logger.info("Remove doc type {} from folder {}",docTypeId,folderId);
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/"+docTypeId + "/folder/"+folderId)
                .build().encode().toUri();
        restClient.create().delete(url);
    }

    @RobotKeyword
    public Collection<DocTypeDto> listDocTypesWithFilter(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        Collection<AggregationCountItemDTO<DocTypeDto>> aggregationCountItemDTOS = listDocTypesFileCountWithFilter(filterDescriptor);
        List<DocTypeDto> result = aggregationCountItemDTOS.stream().map(g -> g.getItem()).collect(Collectors.toList());
        return result;
    }

    @RobotKeyword
    public Collection<AggregationCountItemDTO<DocTypeDto>> listDocTypesFileCountWithFilter(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        RestClient restClient = RestClient.getRestClient();

        ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<DocTypeDto>>> responseType = new ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<DocTypeDto>>>(){ };
        String filterString = ConversionUtils.getMapper().writeValueAsString(filterDescriptor);

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base +  "/filecount")
                .queryParam("filter", filterString)
                .build().encode().toUri();
        logger.info("List docTypes file count result URL: "+url);

        ResponseEntity<FacetPageImpl<AggregationCountItemDTO<DocTypeDto>>> exchange = null;
        try {
            exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return exchange.getBody().getContent();
    }

}
