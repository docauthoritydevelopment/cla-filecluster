package keywords.docauthority;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filetag.ApplyToSolrStatusDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponentsBuilder;
import util.ConversionUtils;
import util.FacetPageImpl;
import util.RestClient;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by uri on 10/01/2017.
 */
@RobotKeywords
public class TagApiKeywords {

    String base = "/api/filetag";

    Logger logger = LoggerFactory.getLogger(TagApiKeywords.class);

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    @RobotKeyword
    public Collection<AggregationCountItemDTO<FileTagDto>>findTagsFileCount(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        RestClient restClient = RestClient.getRestClient();

        ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<FileTagDto>>> responseType = new ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<FileTagDto>>>(){ };
        String filterString = ConversionUtils.getMapper().writeValueAsString(filterDescriptor);

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/filecount")
                .queryParam("filter", filterString)
                .build().encode().toUri();
        logger.info("List tags URL: {}",url);

        try {
            ResponseEntity<FacetPageImpl<AggregationCountItemDTO<FileTagDto>>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
            return exchange.getBody().getContent();
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RobotKeyword
    public Collection<AggregationCountItemDTO<FileTagTypeDto>>    findTagTypesFileCount(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        RestClient restClient = RestClient.getRestClient();

        ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<FileTagTypeDto>>> responseType = new ParameterizedTypeReference<FacetPageImpl<AggregationCountItemDTO<FileTagTypeDto>>>(){ };
        String filterString = ConversionUtils.getMapper().writeValueAsString(filterDescriptor);

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/type/filecount")
                .queryParam("filter", filterString)
                .build().encode().toUri();
        logger.info("List tag types URL: {}",url);

        try {
            ResponseEntity<FacetPageImpl<AggregationCountItemDTO<FileTagTypeDto>>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
            return exchange.getBody().getContent();
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RobotKeyword("Create tag type")
    public FileTagTypeDto createTagType(String name, boolean sensitive) {
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base + "/type/new";
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setName(name);
        fileTagTypeDto.setSensitive(sensitive);
        logger.info("Create Tag type " + fileTagTypeDto);
        HttpEntity<?> requestEntity = new HttpEntity<>(fileTagTypeDto);
        ResponseEntity<FileTagTypeDto> exchange = restClient.create().exchange(url, HttpMethod.PUT, requestEntity, FileTagTypeDto.class);
        logger.info("Got status code: " + exchange.getStatusCode());
        logger.info("Got body : " + exchange.getBody());
        return exchange.getBody();

    }

    @RobotKeyword("List Tag Type Tags")
    public Collection<FileTagDto> listTagTypeTags(long tagTypeId) {
        RestClient restClient = RestClient.getRestClient();
        ParameterizedTypeReference<List<FileTagDto>> responseType = new ParameterizedTypeReference<List<FileTagDto>>(){};
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base)
                .queryParam("typeId",tagTypeId)
                .build().encode().toUri();
        System.out.println("Result URL: "+url);

        try {
            ResponseEntity<List<FileTagDto>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
            return exchange.getBody();
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RobotKeyword("add Tag To File")
    public void addTagToFile(long fileId,long tagId) {
        logger.info("add Tag {} To File {}",tagId,fileId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+tagId+"/file/"+fileId;
        restClient.create().put(keywordUrl, null);
    }

    @RobotKeyword("Remove Tag To File")
    public void removeTagFromFile(long fileId,long tagId) {
        logger.info("remove Tag {} from File {}",tagId,fileId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+tagId+"/file/"+fileId;
        restClient.create().delete(keywordUrl);
    }


    @RobotKeyword("add Tag To Group")
    public void addTagToGroup(String groupName,long tagId) {
        logger.info("add Tag {} To groupId {}",tagId,groupName);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+tagId+"/bid/"+groupName;
        restClient.create().put(keywordUrl, null);
    }

    @RobotKeyword("Remove Tag From Group")
    public void removeTagFromGroup(long groupId,long tagId) {
        logger.info("remove Tag {} from groupId {}",tagId,groupId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+tagId+"/bid/"+groupId;
        restClient.create().delete(keywordUrl);
    }

    @RobotKeyword("add Tag To folder")
    public void addTagToFolder(long folderId,long tagId) {
        logger.info("add Tag {} To folderId {}",tagId,folderId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+tagId+"/folder/"+folderId;
        restClient.create().put(keywordUrl, null);
    }

    @RobotKeyword("Remove Tag From folder")
    public void removeTagFromFolder(long folderId,long tagId) {
        logger.info("remove Tag {} from folderId {}",tagId,folderId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/"+tagId+"/folder/"+folderId;
        restClient.create().delete(keywordUrl);
    }

    @RobotKeyword
    public void applyToSolr() {
        logger.info("Apply tags and docTypes to solr");
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ base + "/apply/solr";
        restClient.create().postForLocation(keywordUrl,null);
    }

    @RobotKeyword
    public ApplyToSolrStatusDto getApplyTagsToSolrTaskStatus() {
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base + "/apply/solr";
        ApplyToSolrStatusDto applyToSolrStatusDto = restClient.create().getForObject(url, ApplyToSolrStatusDto.class);
        return applyToSolrStatusDto;
    }

    @RobotKeyword("list Tag Types")
    public FileTagTypeDto[] listTagTypes() {
        RestClient restClient = RestClient.getRestClient();
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/type")
                .build().encode().toUri();
        System.out.println("Result URL: "+url);
        try {
            FileTagTypeDto[] tagTypes = restClient.create().getForObject(url, FileTagTypeDto[].class);
            return tagTypes;
        } catch (RestClientException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @RobotKeyword("Create File Tag")
    public FileTagDto createFileTag(String name, long typeId) {
        RestClient restClient = RestClient.getRestClient();

        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/type/"+typeId+"/new")
                .queryParam("name",name)
                .build().encode().toUri();
        logger.info("Create fileTag {} in type {}",name,typeId);
        ResponseEntity<FileTagDto> exchange = restClient.create().exchange(url, HttpMethod.PUT, null, FileTagDto.class);
        System.out.println("Got status code: " + exchange.getStatusCode());
        System.out.println("Got body : " + exchange.getBody());
        return exchange.getBody();

    }

    @RobotKeyword("delete file Tag")
    public void deleteFileTag(String name, long tagTypeId) {
        RestClient restClient = RestClient.getRestClient();
        logger.info("Delete file tag {} of type {}",name,tagTypeId);
        FileTagDto tagByName = findTagByName(name, tagTypeId);
        logger.info("Delete file tag {}",tagByName);
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/" + tagByName.getId())
                .build().encode().toUri();
        restClient.create().delete(url);
    }


    @RobotKeyword("delete fileType")
    public void deleteFileType(String name, long tagTypeId) {
        RestClient restClient = RestClient.getRestClient();
        logger.info("Delete fileType {} ",name,tagTypeId);
        FileTagTypeDto fileTypeByName = findTagTypeByName(name, tagTypeId);
        logger.info("Delete file tag {}",fileTypeByName);
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/" + fileTypeByName.getId())
                .build().encode().toUri();
        restClient.create().delete(url);
    }

 /*/
   @RobotKeyword("delete fileType")
    public void deleteFileType(String name) {
       RestClient restClient = RestClient.getRestClient();
        logger.info("Delete fileType {} ",name);
       FileTagTypeDto fileTypeByName = findTagTypeByName(name);
        logger.info("Delete fileType {}",fileTypeByName);
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/" + fileTypeByName.getName())
                .build().encode().toUri();
        restClient.create().delete(url);
    }
/*/
    @RobotKeyword("findTagByName")
    public FileTagDto findTagByName(String name, long tagTypeId) {
        Collection<FileTagDto> fileTagDtos = listTagTypeTags(tagTypeId);
        Optional<FileTagDto> first = fileTagDtos.stream().filter(f -> name.equalsIgnoreCase(f.getName())).findFirst();
        return first.get();
    }

    @RobotKeyword("findTagTypeByName")
    public FileTagTypeDto findTagTypeByName(String name, long tagTypeId) {
        FileTagTypeDto[] fileTagTypeDtos = listTagTypes();
        for (FileTagTypeDto fileTagTypeDto : fileTagTypeDtos) {
            if (name.equalsIgnoreCase(fileTagTypeDto.getName())) {
                return fileTagTypeDto;
            }
        }
        throw new RuntimeException("Failed to find a tag type by name "+name);
    }

    @RobotKeyword("findTagTypeByNameOnly")
    public FileTagTypeDto findTagTypeByNameOnly(String name) {
        FileTagTypeDto[] fileTagTypeDtos = listTagTypes();
        for (FileTagTypeDto fileTagTypeDto : fileTagTypeDtos) {
            if (name.equalsIgnoreCase(fileTagTypeDto.getName())) {
                return fileTagTypeDto;
            }
        }
        throw new RuntimeException("Failed to find a tag type by name "+name);
    }

}
