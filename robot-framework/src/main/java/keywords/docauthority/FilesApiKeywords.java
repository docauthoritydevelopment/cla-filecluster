package keywords.docauthority;

import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.group.AttachFilesToGroupDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import util.ConversionUtils;
import util.CustomPageImpl;
import util.RestClient;

import java.util.Collection;

/**
 * Created by uri on 02/01/2017.
 */
@RobotKeywords
public class FilesApiKeywords {

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    Logger logger = LoggerFactory.getLogger(FilesApiKeywords.class);

    /**
     * list files with filter  ${filter}
     */
    @RobotKeyword
    public Collection<ClaFileDto> listFilesWithFilter(FilterDescriptor filterDescriptor) throws JsonProcessingException {
        RestClient restClient = RestClient.getRestClient();
        ParameterizedTypeReference<CustomPageImpl<ClaFileDto>> responseType = new ParameterizedTypeReference<CustomPageImpl<ClaFileDto>>(){ };
        String filterString = ConversionUtils.getMapper().writeValueAsString(filterDescriptor);

        UriComponents url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + "/api/file/dn")
                .queryParam("filter", filterString)
                .build().encode();
        //TODO - by default we should fetch more than the default 30 files
        System.out.println("List files result URL: "+url.toUri());
        ResponseEntity<CustomPageImpl<ClaFileDto>> exchange = null;
        try {
            exchange = restClient.create().exchange(url.toUri(), HttpMethod.GET, null, responseType);
        } catch (RestClientException e) {
            logger.error("Rest Client exception while calling url {}",url,e);
            throw e;
        } catch (Exception e) {
            logger.error("Runtime exception while calling url {}",url,e);
            throw e;
        }

        return exchange.getBody().getContent();
    }

    @RobotKeyword("Detach file from group")
    public void detachFileFromGroup(long fileId) {
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ "/api/file/"+fileId+"/group/detach";
        restClient.create().postForLocation(keywordUrl,null);
    }

    @RobotKeyword("Attach file to group")
    public GroupDetailsDto attachFileToExistingGroup(long fileId,String groupId) {
        AttachFilesToGroupDto attachFilesToGroupDto = new AttachFilesToGroupDto();
        attachFilesToGroupDto.setFileIds(Lists.newArrayList(fileId));
        attachFilesToGroupDto.setGroupId(groupId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ "/api/file/group/attach";
        GroupDetailsDto groupDetailsDto = restClient.create().postForObject(keywordUrl, attachFilesToGroupDto, GroupDetailsDto.class);
        return groupDetailsDto;
    }

    @RobotKeyword("Analyze file and attach to group")
    public void analyzeFileAndAttachToGroup(long fileId) {
        logger.info("analyze File {} And Attach To Group",fileId);
        RestClient restClient = RestClient.getRestClient();
        String keywordUrl = restClient.getUrl()+ "/api/file/"+fileId+"/group/analyze";
        restClient.create().postForLocation(keywordUrl, null);
    }
}
