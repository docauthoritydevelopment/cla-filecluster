package keywords.docauthority;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.util.UriComponentsBuilder;
import util.RestClient;

import java.net.URI;
import java.util.List;

/**
 * Created by uri on 16/01/2017.
 */
@RobotKeywords
public class SharePointApiKeywords {

    String base ="/api/media/sharepoint";

    Logger logger = LoggerFactory.getLogger(SharePointApiKeywords.class);

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    /**
     * /connections/new
     * @return
     * @param name
     * @param userName
     * @param domain
     * @param password
     * @param sharePointUrl
     */
    @RobotKeyword("configure SharePoint Connection Details")
    public SharePointConnectionDetailsDto configureSharePointConnectionDetails(String name, String userName, String domain, String password, String sharePointUrl) {
        SharePointConnectionDetailsDto sharePointConnectionDto = createSharePointConnectionDto(name, userName, domain, password, sharePointUrl);

        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base +  "/connections/new";
        logger.debug("Create SharePoint connector " + sharePointConnectionDto);
        HttpEntity<?> requestEntity = new HttpEntity<>(sharePointConnectionDto);
        ResponseEntity<SharePointConnectionDetailsDto> exchange = restClient.create().exchange(url, HttpMethod.POST, requestEntity, SharePointConnectionDetailsDto.class);
        logger.debug("Got status code: " + exchange.getStatusCode());
        logger.debug("Got body : " + exchange.getBody());
        return exchange.getBody();
    }

    @RobotKeyword
    public TestResultDto testSharePointConnection(String name, String userName, String domain, String password, String sharePointUrl) {
        SharePointConnectionDetailsDto sharePointConnectionDto = createSharePointConnectionDto(name, userName, domain, password, sharePointUrl);
        SharePointConnectionParametersDto sharePointConnectionParametersDto = sharePointConnectionDto.getSharePointConnectionParametersDto();

        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base +  "/connections/test";
        logger.debug("Test SharePoint connector " + sharePointConnectionDto);
        HttpEntity<?> requestEntity = new HttpEntity<>(sharePointConnectionDto);
        ResponseEntity<TestResultDto> exchange = restClient.create().exchange(url, HttpMethod.POST, requestEntity, TestResultDto.class);
        logger.debug("Got status code: " + exchange.getStatusCode());
        logger.debug("Got body : " + exchange.getBody());
        return exchange.getBody();
    }

    @RobotKeyword
    public List<SharePointConnectionDetailsDto> getSharePointConnectionDetails() {
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + base + "/connections";

        ParameterizedTypeReference<List<SharePointConnectionDetailsDto>> responseType = new ParameterizedTypeReference<List<SharePointConnectionDetailsDto>>(){ };
        ResponseEntity<List<SharePointConnectionDetailsDto>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
        return exchange.getBody();
    }

    @RobotKeyword
    public SharePointConnectionDetailsDto createSharePointConnectionDto(String name, String userName, String domain, String password, String url) {
        SharePointConnectionDetailsDto sharePointConnectionDetailsDto = new SharePointConnectionDetailsDto();
        sharePointConnectionDetailsDto.setName(name);
        sharePointConnectionDetailsDto.setMediaType(MediaType.SHARE_POINT);
        sharePointConnectionDetailsDto.setPstCacheEnabled(true);
        SharePointConnectionParametersDto sharePointConnectionParametersDto = new SharePointConnectionParametersDto();
        sharePointConnectionParametersDto.setUsername(userName);
        sharePointConnectionParametersDto.setDomain(domain);
        sharePointConnectionParametersDto.setPassword(password);
        sharePointConnectionParametersDto.setUrl(url);
        sharePointConnectionDetailsDto.setSharePointConnectionParametersDto(sharePointConnectionParametersDto);
        return sharePointConnectionDetailsDto;
    }

    @RobotKeyword
    public List<ServerResourceDto> listSharePointFolders(@PathVariable long connectionId, String baseFolder) {
        RestClient restClient = RestClient.getRestClient();
        URI url = UriComponentsBuilder.fromHttpUrl(restClient.getUrl() + base + "/" + "/connection/"+connectionId+"/server/folders")
                .queryParam("id",baseFolder)
                .build().encode().toUri();

        ParameterizedTypeReference<List<ServerResourceDto>> responseType = new ParameterizedTypeReference<List<ServerResourceDto>>(){ };
        ResponseEntity<List<ServerResourceDto>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
        return exchange.getBody();
    }
}
