package keywords.docauthority;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.media.MediaType;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import util.CustomPageImpl;
import util.RestClient;

import java.nio.file.Paths;
import java.util.List;

/**
 * FileTreeApiController rest client (api prefix /api/filetree)
 * Created by uri on 29/12/2016.
 */
@RobotKeywords
public class RootFolderKeywords {

    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    private static final Logger logger = LoggerFactory.getLogger(RootFolderKeywords.class);

    /**
     * /api/filetree
     * FileTreeApiController
     *
     * @param path
     * @return
     */
    @RobotKeyword("Create File Share Root Folder with path")
    public RootFolderDto createFileShareRootFolderWithPath(String path) {
        path = Paths.get(path).toAbsolutePath().toString();
        RestClient restClient = RestClient.getRestClient();
        logger.debug("Create file share root folder with path " + path);
        String url = restClient.getUrl() + "/api/filetree/rootfolders/new";
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath(path);
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        logger.debug("Create rootFolder " + rootFolderDto);
        RootFolderDto result = restClient.create().postForObject(url, rootFolderDto, RootFolderDto.class);
        logger.debug("Created rootFolder " + result);
        return result;
    }

    @RobotKeyword("Find root folder by path")
    public RootFolderDto findRootFolderByPath(String path) {
        path = Paths.get(path).normalize().toAbsolutePath().toString();
        List<RootFolderDto> rootFolderDtos = listRootFolders();
        logger.debug("Looking for root folder by real path "+path);
        for (RootFolderDto rootFolderDto : rootFolderDtos) {
            logger.debug("RootFolder "+rootFolderDto);
            if (rootFolderDto.getRealPath().equalsIgnoreCase(path)) {
                return rootFolderDto;
            }
        }
        return null;
    }

    @RobotKeyword
    public List<RootFolderDto> listRootFolders() {
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + "/api/filetree/rootfolders";

        ParameterizedTypeReference<CustomPageImpl<RootFolderDto>> responseType = new ParameterizedTypeReference<CustomPageImpl<RootFolderDto>>(){ };
        ResponseEntity<CustomPageImpl<RootFolderDto>> exchange = restClient.create().exchange(url, HttpMethod.GET, null, responseType);
        return exchange.getBody().getContent();
    }

    @RobotKeyword
    public void deleteRootFolderById(long id) {
        System.out.println("Delete root folder by id "+id);
        RestClient restClient = RestClient.getRestClient();
        String url = restClient.getUrl() + "/api/filetree/rootfolders/"+id;
        restClient.create().delete(url);
    }

    public void deleteRootFolder(RootFolderDto rootFolderDto, boolean failIfNull) {
        if (rootFolderDto != null) {
            logger.debug("Delete root folder "+rootFolderDto);
            deleteRootFolderById(rootFolderDto.getId());
        }
        else if (failIfNull && rootFolderDto == null) {
            throw new RuntimeException("Can't delete root folder - null object passed");
        }
    }

    @RobotKeyword("Create SharePoint Root Folder")
    public RootFolderDto createSharePointRootFolder(long mediaConnectorId, String path) {
        RestClient restClient = RestClient.getRestClient();
        logger.debug("Create share point root folder with path {} on media connection {}",path,mediaConnectorId);
        String url = restClient.getUrl() + "/api/filetree/rootfolders/new";
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath(path);
        rootFolderDto.setMediaType(MediaType.SHARE_POINT);
        rootFolderDto.setMediaConnectionDetailsId(mediaConnectorId);
        logger.debug("Create rootFolder " + rootFolderDto);
        RootFolderDto result = restClient.create().postForObject(url, rootFolderDto, RootFolderDto.class);
        logger.debug("Created rootFolder " + result);
        return result;
    }

}
