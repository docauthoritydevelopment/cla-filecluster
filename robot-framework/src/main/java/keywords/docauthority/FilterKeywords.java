package keywords.docauthority;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import org.robotframework.javalib.annotation.RobotKeyword;
import org.robotframework.javalib.annotation.RobotKeywords;

/**
 * Created by uri on 02/01/2017.
 */
@RobotKeywords
public class FilterKeywords {
    public static final String ROBOT_LIBRARY_SCOPE = "GLOBAL";

    @RobotKeyword
    public FilterDescriptor createBaseFilterOnKeyValue(String key, Object value) {
        return new FilterDescriptor(key,String.valueOf(value),"EQ");
    }
}
