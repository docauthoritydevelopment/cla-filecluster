*** Settings ***

Library          keywords.docauthority.CrawlerApiKeywords
Library          keywords.docauthority.PathKeywords

*** Variables ***

#${url} =    http://localhost:8080
#${user} =   aa
#${password} =   123
#${samples_folder} =  ${CURDIR}${/}..${/}samples
${reportingTest_folder} =  ${samples_folder}${/}reportingTests
${reportingTest_Subfolder} =  ${samples_folder}${/}reportingTests${/}subFolder

*** Keywords ***

login to fileCrawler
    connectToFileCluster    ${url}  ${user}     ${password}

collect logs on failure
   Run Keyword If	'${TEST STATUS}' == 'FAIL'  collect logs

collect logs
  log  Collect logs
  collect Logs To File  ${EXECDIR}${/}logs/${TEST NAME}.zip
  log  Log file <a href=${EXECDIR}${/}logs/${TEST NAME}.zip>log file</a>  HTML

verify no scan is running
  ${state}=  get file crawling state
  Should not be true  ${state.fileCrawlerRunning}
