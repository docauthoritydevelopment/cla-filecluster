*** Settings ***
Documentation    Suite description
Suite Setup     Run Keywords
...             Login To System    AND
...             Add User           AND
...             Activate Alarms    AND
...             Check Balance
*** Test Cases ***
Test title
    [Tags]    DEBUG
    Provided precondition
    When action
    Then check expectations

*** Keywords ***
Provided precondition
    Setup system under test