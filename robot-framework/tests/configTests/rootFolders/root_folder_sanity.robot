*** Settings ***

Force Tags          Sanity  Rest
Library          keywords.docauthority.RootFolderKeywords
Resource          ../../common.robot
Resource          ../../restKeywords.robot

Test Teardown  collect logs on failure

*** Variables ***

*** Testcases ***

Test create Root folder
    [Documentation]     Test that a new root folder can be created

    delete root folder by path "${CURDIR}" if exists

    ${rootFolderDto}=   create File Share RootFolder With Path  ${CURDIR}

    Should Not Be Equal  ${null}  ${rootFolderDto.id}

    Log  ${rootFolderDto}




