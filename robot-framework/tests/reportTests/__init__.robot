*** Settings ***
Resource          ../common.robot
Resource          ../restKeywords.robot
Suite Setup       Scan sample folder
Library          keywords.docauthority.CrawlerApiKeywords

*** Keywords ***

Scan sample folder
    Log     Scan sample root folder ${reportingTest_folder}
    ${rootFolder}=  Create File Share Root Folder With Path "${reportingTest_folder}" if not exists
    runOnRootFolder  ${rootFolder.id}
    Wait Until Keyword Succeeds  2 min  5 sec  verify no scan is running
    log  Scan finished on root folder id ${rootFolder.id}.
    set global variable  ${sampleRootFolderId}  ${rootFolder.id}
    ${secs} =	Get Time	epoch
    set global variable  ${randomNumber}  ${secs}


