*** Settings ***

Force Tags          Sanity  Rest
Resource          ../common.robot
Resource          ../restKeywords.robot
Library          keywords.docauthority.CrawlerApiKeywords
Library          keywords.docauthority.GroupsApiKeywords

Test Teardown  collect logs on failure
*** Variables ***
${docTypeName}=   docType_sanity_${randomNumber}
${tagName}=  tag_sanity_${randomNumber}
${systemParentDocTypeName}=   HR
${duplicateTag}=   DuplicatedTag
${groupTagged}=  subject friday’s ferc orders on california
*** Testcases ***



Test add a new docType
   # [tags]  Dev
    ${addDocType}=  add doc type without ParentId
    verify doc type ${addDocType.id} is added to the list

Test add the same docType (known failure Test)
  #[tags]  Dev
  ${addDocType}=  add doc type without ParentId
  ${addDuplicatedDocType}=  add the same doc type name and catch exception

Test add docType to default docType
   #[tags]  Dev
    ${findDocType}=  find "${systemParentDocTypeName}" from the list
    ${addDocType}=  add doc type without ParentId to ${systemParentDocTypeName}
    verify ${findDocType.name} is added to the "${systemParentDocTypeName}"

Test add docType to new added docType
   #[tags]  Dev
    ${addDocType}=  add doc type without ParentId
    ${findDocType}=  find added "${docTypeName}" from the list
    verify ${findDocType.name} is added to the new added "${docTypeName}"
    ${addDocType}=  add doc type without ParentId
    ${findDocType}=  verify ${findDocType.name} is added to the new added "${docTypeName}"

Test delete default docType (system) used by the system
   # [tags]  Dev
    ${findDocType}=  find "${systemParentDocTypeName}" from the list
    delete used docType ${findDocType} in the system and catch exception

Test remove docType (added docType)
    # [tags]  Dev
    ${addDocType}=  add doc type without ParentId
    ${findDocType}=  find added "${docTypeName}" from the list
    ${addDocType}=  add doc type without ParentId
    ${findDocType}=  verify ${findDocType.name} is added to the new added "${docTypeName}"
    ${deleteDocType}=  Delete Doc Type ${docTypeName} ${findDocType.id}

#Test add docType to group


