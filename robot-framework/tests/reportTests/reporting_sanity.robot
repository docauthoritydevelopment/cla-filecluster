*** Settings ***

Force Tags          Sanity  Rest
Resource          ../common.robot
Resource          ../restKeywords.robot
Library          OperatingSystem

Test Teardown  collect logs on failure
Test Timeout    30s

*** Testcases ***

Test Files report with root folder filter
    #[tags]  Dev
  ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
  @{files}=  list files with filter  ${filter}
  Should not be empty  ${files}

Test File container from a list of files

   #[tags]  Dev
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{files}=  list files with filter  ${filter}
    ${firstFile}=  Set Variable  @{files}[0]
    Log Many  @{files}
    Should be equal as strings  ${firstFile.type}  EXCEL
    Should be equal  ${firstFile.baseName}  American Public Energy Agency.xls

Test file has correct group name
   # [tags]  Dev
    ${rootFolderFilter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    log  find the first group
    @{groups}=  list groups with filter  ${rootFolderFilter}
    ${firstGroup}=  Set Variable  @{groups}[0]
    log  find the first file within the first group (${firstGroup.id})
    ${firstGroupFilter}=  create base filter on key value  groupId  ${firstGroup.id}
    @{files}=  list files with filter  ${firstGroupFilter}
    ${firstFile}=  Set Variable  @{files}[1]
    Log Many  @{files}
    log  Verify that the first file in the group has the correct group name
    Should be equal   ${firstFile.groupName}  ${firstGroup.groupName}

Test file has Tags
    #[tags]  Dev
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{files}=  list files with filter  ${filter}
    ${firstFile}=  Set Variable  @{files}[0]
    @{fileTags}=  set variable  ${firstFile.fileTagDtos}
    Should Not Be Empty  ${fileTags}
    Log Many  @{fileTags}


Test verify we have at least one tagged file
#    [tags]  Dev

    ${filter}=  create base filter on key value  tags  *
    @{files}=  list files with filter  ${filter}
    ${firstFile}=  Set Variable  @{files}[0]
    @{fileTags}=  set variable  ${firstFile.fileTagDtos}
    Should Not Be Empty  ${fileTags}
    Log Many  @{fileTags}
    ${FileTagDto}=  Set Variable  @{fileTags}[0]

Test verify files has no tag

   # [tags]  Dev
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{files}=  list files with filter  ${filter}
    ${firstFile}=  Set Variable  @{files}[2]
    @{fileTags}=  set variable  ${firstFile.fileTagDtos}
    Should Be Empty   ${fileTags}
    Log Many  @{fileTags}


Test Number of scanned files
  #  [tags]  Dev
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{files}=  list files with filter  ${filter}
    ${length} =	Get Length  ${files}
    Should be equal As Integers  ${length}  8
    Log Many  @{files}


Test count number of FileGroup
   # [tags]  Dev
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{groups}=  list groups with filter  ${filter}
    Should not be empty  ${groups}
    ${length} =	Get Length	${groups}
    Should Be Equal As Integers	${length}	2
    log  number of groups ${length}

#Test verify Groups names
#    ${rootFolderFilter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
#    @{groups}=  list groups with filter  ${rootFolderFilter}
#    Should not be empty  ${groups}
#    ${firstGroup}=  Set Variable  @{groups}[0]
#    Should be equal  ${firstGroup.groupName}  subject friday’s ferc orders on california
#    ${secondGroup}=  Set Variable  @{groups}[1]
#    Should be equal  ${secondGroup.groupName}  process for the period april 23rd may 25th

Test number of files per a group

  #  [tags]  Dev
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{groups}=  list groups with filter  ${filter}
    Should not be empty  ${groups}
    Log Many  @{groups}
    ${firstGroup}=  Set Variable  @{groups}[0]
    Should Be True  ${firstGroup.numOfFiles} > 0

Test that group has a sample member
   # [tags]  Dev
    ${rootFolderFilter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    @{groups}=  list groups with filter  ${rootFolderFilter}
    Should not be empty  ${groups}
    ${firstGroup}=  Set Variable  @{groups}[0]
    Should not be empty  ${firstGroup.firstMemberName}




