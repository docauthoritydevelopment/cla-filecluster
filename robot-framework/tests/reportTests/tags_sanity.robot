*** Settings ***

Force Tags          Sanity  Rest
Resource          ../common.robot
Resource          ../restKeywords.robot
Library          keywords.docauthority.CrawlerApiKeywords
Library          keywords.docauthority.GroupsApiKeywords


Test Teardown  collect logs on failure
*** Variables ***
${tagTypeName}=   tagType_sanity ${randomNumber}
${tagName}=  tag_sanity_${randomNumber}
${systemTagTypeName}=   Regulation
${duplicateTag}=   DuplicatedTag

*** Testcases ***

Test add a new Tag Type
  #  [tags]  Dev
    ${addTagType}=  add a new Sensitive Tag Type ${tagTypeName}
    verify that tag type ${tagTypeName} with id ${addTagType.id} exists

Test add new Tag to a system Tag Type
   #[tags]  Dev
    ${findSystemTagType}=  find a system Tag Type ${systemTagTypeName}
    ${addNewTag}=  add a new file tag to ${findSystemTagType}
    verify new tag ${tagName} was added to ${findSystemTagType.id}

Test add new Tag to a new added Tag Type
  #[tags]  Dev
    Sleep    2s
    ${addTagType}=  add a new Sensitive Tag Type ${tagTypeName}
    verify that tag type ${tagTypeName} with id ${addTagType.id} exists
    ${findTagType}=  find added ${tagTypeName} which is not system tag type
    ${addNewTag}=  add tag to a new added tag type "${findTagType}"
    verify new tag was added to ${findTagType.id}

Test delete a tagType
    Sleep    2s
  #  [tags]  Dev
    ${addTagType}=  add a new Sensitive Tag Type ${tagTypeName}
    verify that tag type ${tagTypeName} with id ${addTagType.id} exists
    delete tag tag type for added ${addTagType.id}

Test delete tag from system tag type (bug in the keyword)
    Sleep    2s
    # [tags]  Dev
    ${findSystemTagType}=  find a system Tag Type ${systemTagTypeName}
    Log Many  ${systemTagTypeName}
    ${addNewTag}=  add a new file tag to ${findSystemTagType}
    verify new tag ${tagName} was added to ${findSystemTagType.id}
    delete added tag from system tag type ${findSystemTagType.id}

Test add a used TagType name in to the list of Tag types
    Sleep    2s
   #[tags]  Dev
    ${addTagType}=  add a new Sensitive Tag Type ${tagTypeName}
    verify that tag type ${tagTypeName} with id ${addTagType.id} exists
    add a duplicated tag type and catch exception


Test add a used Tag name in to the same tagType
    Sleep    2s
   # [tags]  Dev
    ${addTagType}=  add a new Sensitive Tag Type ${tagTypeName}
    verify that tag type ${tagTypeName} with id ${addTagType.id} exists
    ${addFirstTag}=  add the first ${duplicateTag} name to ${addTagType}
    verify the first tag for duplicated tag was added ${addFirstTag}
    add a duplicated tag name and catch exception


Test add tag to folder from new created Tag Type and tag name
    Sleep    2s
    [tags]  Dev
    ${addTagType}=  add a new Sensitive Tag Type ${tagTypeName}
    verify that tag type ${tagTypeName} with id ${addTagType.id} exists
    ${findTagType}=  find added ${tagTypeName} which is not system tag type
    ${addNewTag}=  add tag to a new added tag type "${findTagType}"
    ${firstGroup}=  find the firstGroup ${addNewTag}
    ${addNewTag}=  ${addNewTag} to the first found group ${firstGroup.id} ${addNewTag.id}



