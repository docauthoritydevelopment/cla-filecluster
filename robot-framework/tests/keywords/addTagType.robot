*** Settings ***
Resource          ../common.robot
Resource          ../restKeywords.robot
Library          keywords.docauthority.TagApiKeywords
Library          keywords.docauthority.GroupsApiKeywords


*** Keywords ***

add a new Sensitive Tag Type ${tagTypeName}

    log  Add a new tag type ${tagTypeName}
    ${addTagType}=  createTagType   ${tagTypeName}  True
    Log Many  ${tagTypeName}
    Log  ${addTagType}
    [return]  ${addTagType}

verify that tag type ${tagTypeName} with id ${tagTypeId} exists
    Sleep    2s
    ${newTagType} =  findTagTypeByName  ${tagTypeName}  ${tagTypeId}
    Log Many  ${newTagType}
    log  verify the new created tag was added in to the list of Tags
    Should Be Equal  ${newTagType.name}  ${tagTypeName}

find a system Tag Type ${systemTagTypeName}
    log  have the list of TagTypes in the system
    ${findSystemTagType}=  Find Tag Type By Name Only    ${systemTagTypeName}
    Log Many  ${findSystemTagType}
    [return]  ${findSystemTagType}

find added ${tagTypeName} which is not system tag type
    ${findTagType} =  Find Tag Type By Name Only  ${tagTypeName}
    Log Many  ${findTagType}
    log  verify the new created tag was added in to the list of Tags
    Should Be Equal  ${findTagType.name}  ${tagTypeName}
    [return]  ${findTagType}

add a new file tag to ${findSystemTagType}
    Sleep    2s
    log  Add a new tag ${tagName} to found SystemTagType
    ${addNewTag}=  createFileTag  ${tagName}  ${findSystemTagType.id}
    [return]  ${addNewTag}

verify new tag file "${tagName}" is added to the new added Tag Type ${addTagType.id}
    log  verify the added tag is added in to the right list
    ${verifyTagName}=  find tag by name  ${tagName}  ${addTagType.id}
    Should be equal  ${verifyTagName}  ${tagName}

verify new tag ${tagName} was added to ${findSystemTagType.id}
    log  look for the new added tag in the tagType
    ${tagInList}=  findTagByName  ${tagName}  ${findSystemTagType.id}
    Log Many  ${tagInList}
    Should be equal  ${tagInList.name}  ${tagName}

verify new tag was added to ${findTagType.id}
    log  look for the new added tag in the tagType
    ${tagInList}=  findTagByName  ${tagName}  ${findTagType.id}
    Log Many  ${tagInList}
    Should be equal  ${tagInList.name}  ${tagName}

delete tag tag type for added ${addTagType.id}
    log   delete new added tagType from the list
    Log Many  ${tagTypeName}
    Log Many  ${addTagType.id}
    ${deleteTagType}=  deleteFileType  ${tagTypeName}  ${addTagType.id}

delete added tag from system tag type ${findSystemTagType.id}
    log  delete added tag from system tag type
    Log Many  ${tagTypeName}
    Log Many  ${findSystemTagType.id}
    ${deleteTagType}=  Delete File Tag  ${tagTypeName}  ${findSystemTagType.id}

add a duplicated tag type and catch exception
    Sleep    2s
    log  add the same TagType - catch exception
    Run Keyword And Expect Error	*   createTagType   ${tagTypeName}  False

add the first ${duplicateTag} name to ${addTagType}
    log  add the first Tag (duplicate potential_ to the added list
    ${addFirstTag}=  createFileTag  ${duplicateTag}  ${addTagType.id}
    [return]  ${addFirstTag}

add tag to a new added tag type "${findTagType}"
    log  Add a new tag type ${tagTypeName}
    Log Many  ${tagName}
    Log Many  ${findTagType.id}
    ${addNewTag}=  createFileTag  ${tagName}  ${findTagType.id}
    Log Many  ${addNewTag}
    [return]  ${addNewTag}

verify the first tag for duplicated tag was added ${addFirstTag}
    log  compare between ${addFirstTag.name} and ${duplicateTag}
    Should Be Equal  ${addFirstTag.name}  ${duplicateTag}

add a duplicated tag name and catch exception
    log  add the same Tag - catch exception
    Run Keyword And Expect Error	*   createFileTag  ${duplicateTag}  ${addNewTag.id}

find the firstGroup ${addNewTag}
    ${firstGroup}=  find a list of Group folders and supply the first item
    Log Many  ${firstGroup.id}
    ${groupTag} =  Add Tag To Group  ${firstGroup.id}  ${addNewTag.id}
    Log Many  ${groupTag}
    [return]  ${firstGroup}

${addNewTag} to the first found group ${firstGroup.name} ${addNewTag.id}
    ${groupTag} =  Add Tag To Group  ${firstGroup.name}  ${addNewTag.id}
    Log Many  ${groupTag}
