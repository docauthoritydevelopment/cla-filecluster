*** Settings ***
Resource          ../common.robot
Resource          ../restKeywords.robot
Library          keywords.docauthority.DocTypeKeywords


*** Keywords ***

add doc type without ParentId
    log  Add a new doc type ${docTypeName}
    ${addDocType}=  addDocTypeWithoutParent  ${docTypeName}
    [return]  ${addDocType}

add the same doc type name and catch exception
    log  add the same Doc Type - catch exception
    Run Keyword And Expect Error	*   addDocTypeWithoutParent  ${docTypeName}

add doc type without ParentId to ${systemParentDocTypeName}
    log  Add a new doc type ${docTypeName}
    ${addDocType}=  addDocTypeWithoutParent  ${systemParentDocTypeName}
    [return]  ${addDocType}

verify doc type ${addDocType.id} is added to the list
    log  Find the new added docType from the list
    ${findDocType}=  Find Doc Type By Name  ${docTypeName}  ${addDocType.id}
    Log Many  ${findDocType}
    Should Be Equal  ${findDocType.name}  ${docTypeName}
    [return]  ${findDocType}

add doc type with ParentId (findDocType.id)
    ${addDocType}=  addDocTypeWithParent   ${docTypeName}  ${findDocType.id}
    [return]  ${addDocType}

find "${systemParentDocTypeName}" from the list
    log  Find the id of default ${systemParentDocTypeName} from the list
    ${findDocType}=  Find Doc Type By Name Only  ${systemParentDocTypeName}
    Log Many  ${findDocType}
    [return]  ${findDocType}

find added "${docTypeName}" from the list
    log  Find the id of default ${systemParentDocTypeName} from the list
    ${findDocType}=  Find Doc Type By Name Only  ${docTypeName}
    Log Many  ${findDocType}
    [return]  ${findDocType}

verify ${findDocType.name} is added to the new added "${docTypeName}"
    ${findDocType}=  Find Doc Type By Name Only  ${docTypeName}
    Log Many  ${findDocType}
    Should be equal  ${findDocType.name}  ${docTypeName}
    [return]  ${findDocType}

verify ${findDocType.name} is added to the "${systemParentDocTypeName}"
    ${findDocType}=  Find Doc Type By Name Only  ${systemParentDocTypeName}
    Log Many  ${findDocType}
    Should be equal  ${findDocType.name}  ${systemParentDocTypeName}
    [return]  ${findDocType}


Delete Doc Type ${docTypeName} ${findDocType.id}
    log  delete doc type by using ${docTypeName} ${findDocType.id}
    ${deleteDocType}=  deleteDocTypeByName  ${docTypeName}  ${findDocType.id}

delete used docType ${findDocType} in the system and catch exception
    log  delete used docType in the system
    Run Keyword And Expect Error	*   Delete Doc Type  ${systemParentDocTypeName}

