*** Settings ***

Resource            ../common.robot
Resource          addTagType.robot
Library          keywords.docauthority.RootFolderKeywords
Library          keywords.docauthority.GroupsApiKeywords

*** Keywords ***

Delete root folder by path "${path}" if exists
    ${rootFolder}=  findRootFolderByPath  ${path}
    Delete Root Folder  ${rootFolder}  ${False}


Create File Share Root Folder With Path "${path}" if not exists
    ${rootFolder}=  findRootFolderByPath  ${path}
    log  ${rootFolder}
    Run Keyword if  $rootFolder is ${None}  Create File Share Root Folder With Path  ${path}
    ${rootFolder}=  findRootFolderByPath  ${path}
    [return]  ${rootFolder}

find a list of Group folders and supply the first item
    log  run the filter to find a list of groups
    ${filter}=  create base filter on key value  rootFolderId  ${sampleRootFolderId}
    log  verify the list of groups is not empty
    @{groups}=  list groups with filter  ${filter}
    Should not be empty  ${groups}
    Log Many  @{groups}
    log  find the first group in the filter
    ${firstGroup}=  Set Variable  @{groups}[0]
    Log Many  ${firstGroup}
    [return]  ${firstGroup}