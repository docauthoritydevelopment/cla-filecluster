package da.file.simulator.scan;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import da.file.simulator.scan.generators.file.FileGeneratorBase;
import da.file.simulator.scan.generators.path.PathGenerator;

import java.util.Map;
import java.util.function.Consumer;

public abstract class ScanStrategy {
	protected SeedIterator seedIterator;

	protected PathGenerator pathGenerator;
	protected Map<FileType,FileGeneratorBase> fileGenerators;

	public ScanStrategy(PathGenerator pathGenerator,  Map<FileType,FileGeneratorBase> fileGenerators) {
		this.pathGenerator = pathGenerator;
		this.fileGenerators = fileGenerators;
	}

	public PathGenerator getPathGenerator() {
		return pathGenerator;
	}

	public Map<FileType, FileGeneratorBase> getFileGenerators() {
		return fileGenerators;
	}

	public SeedIterator getSeedIterator() {
		return seedIterator;
	}

	public ScanStrategy setSeedIterator(SeedIterator seedIterator) {
		this.seedIterator = seedIterator;
		return this;
	}

	public abstract void scan(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
							  ProgressTracker filePropsProgressTracker);

}
