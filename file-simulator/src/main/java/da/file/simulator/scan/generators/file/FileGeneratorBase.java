package da.file.simulator.scan.generators.file;


import com.cla.common.domain.dto.file.FileAclDataDto;

import com.cla.common.utils.signature.ContentSignHelper;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.utils.TimeSource;
import com.google.common.base.Splitter;
import da.file.simulator.scan.generators.acl.AclGenerator;

import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public abstract class FileGeneratorBase implements FileGenerator {

	private ScanTypeSpecification typeSpecification;

	AclGenerator aclGenerator;

	abstract public <T extends FileGeneratorBase> T setAclGenerator(AclGenerator aclGenerator);

	ContentSignHelper contentSignHelper = new ContentSignHelper(25000000);

	private int targetCount;

	public int getTargetCount() {
		return targetCount;
	}

	public FileGeneratorBase setTargetCount(int targetCount) {
		this.targetCount = targetCount;
		return this;
	}

	public void setScanParams(ScanTypeSpecification typeSpecification) {
		this.typeSpecification = typeSpecification;
	}

	@Override
	public ClaFilePropertiesDto generate(String path, boolean isFolder, int seed) {
		String root = Splitter.on(File.separator).omitEmptyStrings().splitToList(path).get(1);
		String fileName = isFolder ?
				path :
				Paths.get(path, FileGeneratorUtils.finalizeFileName(root, getFileNameBody(), getFileExtension(),
						seed)).toString();
		ClaFilePropertiesDto fileProperties = new ClaFilePropertiesDto(fileName, isFolder ? 0 : 10000L);

		FileAclDataDto fileAclDataDto = aclGenerator.generateFileAcl(seed);
		fileAclDataDto.getAclReadData().forEach(fileProperties::addAllowedReadPrincipalName);
		fileAclDataDto.getAclWriteData().forEach(fileProperties::addAllowedWritePrincipalName);
		fileAclDataDto.getAclDeniedReadData().forEach(fileProperties::addDeniedReadPrincipalName);
		fileAclDataDto.getAclDeniedWriteData().forEach(fileProperties::addDeniedWritePrincipalName);
		fileProperties.setAclSignature(fileProperties.calculateAclSignature());

		TimeSource timeSource = TimeSource.create().withSeed(seed);
		timeSource.setSimulatedDateTime(2000, 1, 1, 0, 0, 0);
		long creationTime = timeSource
				.simulateRandomDateTime(timeSource.currentTimeMillis(), 18*365, TimeUnit.DAYS).currentTimeMillis();
		long modTime = timeSource.simulateRandomDateTime(creationTime, 10, TimeUnit.DAYS).currentTimeMillis();
		long accessTime = timeSource.simulateRandomDateTime(creationTime, 14, TimeUnit.DAYS).currentTimeMillis();

		fileProperties.setCreationTimeMilli(creationTime);
		fileProperties.setAccessTimeMilli(accessTime);
		fileProperties.setModTimeMilli(modTime);
		fileProperties.setFile(!isFolder);
		fileProperties.setFolder(isFolder);
		fileProperties.setMediaItemId(fileName);
		fileProperties.setOwnerName("owner_" + seed);
		fileProperties.setClaFileId(null);
		fileProperties.setFileSize(23040L + seed);
		fileProperties.setContentSignature(contentSignHelper.sign(fileName.getBytes()));
		fileProperties.setUserContentSignature("ihAoHhR8ISTGO/GuID6TT0pJYkU=");
		fileProperties.setContentMetadataId(null);
		fileProperties.setFileName(fileName);
		fileProperties.setAclInheritanceType(null);

		fileProperties.setMime(getFileMime());
		fileProperties.setType(getFileType());

		return fileProperties;
	}

	protected abstract FileType getFileType();

	protected abstract String getFileMime();

	protected abstract String getFileExtension();

	protected abstract String getFileNameBody();

}
