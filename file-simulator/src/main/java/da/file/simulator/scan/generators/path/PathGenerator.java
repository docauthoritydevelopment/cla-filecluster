package da.file.simulator.scan.generators.path;

public abstract class PathGenerator implements Iterable<String>{
	protected int depth;
	private String rootPath;
	protected int seed;

	public PathGenerator init(String rootPath, int seed){
		this.rootPath = rootPath;
		this.seed = seed;
		prepareFolderStructure();
		return this;
	}

	public int getDepth() {
		return depth;
	}

	public abstract <T extends PathGenerator> T setDepth(int depth);

	public abstract int getNodeCount();

	protected abstract void prepareFolderStructure();

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getRootPath() {
		return rootPath;
	}

	public int getSeed() {
		return seed;
	}

	public PathGenerator setSeed(int seed) {
		this.seed = seed;
		return this;
	}
}
