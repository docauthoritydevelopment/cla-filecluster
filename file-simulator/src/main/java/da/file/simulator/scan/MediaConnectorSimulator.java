package da.file.simulator.scan;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import da.file.simulator.scan.generators.file.FileGeneratorBase;
import da.file.simulator.scan.generators.file.FileGeneratorUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;


/**
 * This class is a facade for a simulated files, through which the MediaProc. will acquire 'files' and 'folders'
 */
public class MediaConnectorSimulator extends AbstractMediaConnector {

	private ScanStrategy scanStrategy;

	public MediaConnectorSimulator(ScanStrategy scanStrategy) {
		this.scanStrategy = scanStrategy;
	}

	//region ...overriden methods from AbstractMediaConnector
	@Override
	public MediaType getMediaType() {
		return MediaType.FILE_SHARE; // so we dont get NPE 
	}

	@Override
	public ClaFilePropertiesDto getFileAttributes(String fileId) throws FileNotFoundException {
		int seed = FileGeneratorUtils.resolveSeedFromFileName(fileId);
		FileType fileType = FileTypeUtils.getFileType(fileId);
		Map<FileType, FileGeneratorBase> fileGenerators = scanStrategy.getFileGenerators();
		FileGeneratorBase fileGenerator = fileGenerators.get(fileType);
		ClaFilePropertiesDto claFilePropertiesDto = fileGenerator.generate(FilenameUtils.getFullPath(fileId), false, seed);
		return claFilePropertiesDto;
	}

	@Override
	public ClaFilePropertiesDto getMediaItemAttributes(String mediaItemId) throws FileNotFoundException {
		return getFileAttributes(mediaItemId);
	}

	@Override
	public InputStream getInputStream(ClaFilePropertiesDto props) throws IOException {
		throw new RuntimeException("This method is not implemented yet in the simulator");
	}

	@Override
	public void streamMediaItems(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
								 Consumer<DirListingPayload> directoryListingConsumer, Consumer<String> createScanTaskConsumer,
								 Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {
		streamMediaItems(false, null, scanParams, filePropertiesConsumer, directoryListingConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
	}

	@Override
	public void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
										   Consumer<ClaFilePropertiesDto> filePropertiesConsumer, Consumer<DirListingPayload> directoryListingConsumer,
										   Consumer<String> createScanTaskConsumer, Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker) {
		streamMediaItems(true, null, scanParams, filePropertiesConsumer, directoryListingConsumer, createScanTaskConsumer, scanActivePredicate, filePropsProgressTracker);
	}

	@Override
	public boolean isDirectoryExists(String path, boolean ignoreAccessErrors) {
		return true;
	}

	@Override
	public void fetchAcls(ClaFilePropertiesDto claFileProp) {

	}

	@Override
	public List<ServerResourceDto> testConnection() {
		return browseSubFolders(null);
	}

	@Override
	public String getIdentifier() {
		return "simulator";
	}

	@Override
	public FileContentDto getFileContent(String filename, boolean forUserDownload) throws IOException {
		throw new RuntimeException("This method is not implemented yet in the simulator");
	}


    @Override
	public void streamMediaChangeLog(
            ScanTaskParameters scanParams, String realPath, Long runId, long startChangeLogPosition,
            Consumer<MediaChangeLogDto> changeConsumer, Consumer<String> createScanTaskConsumer,
			Predicate<? super String> fileTypesPredicate) {

	}

	@Override
	public void concurrentStreamMediaChangeLog(
			ForkJoinPool forkJoinPool, ScanTaskParameters scanParams, String realPath, Long runId,
			long startChangeLogPosition, Consumer<MediaChangeLogDto> changeConsumer,
			Consumer<String> createScanTaskConsumer, Predicate<? super String> fileTypesPredicate) {

	}

	@Override
	public List<ServerResourceDto> browseSubFolders(String folderMediaEntityId) {
		return Arrays.asList(new ServerResourceDto());
	}

	//endregion


	private void streamMediaItems(boolean concurrent, ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
								  Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
								  Consumer<DirListingPayload> directoryListingConsumer,
								  Consumer<String> createScanTaskConsumer,
								  Predicate<Pair<Long, Long>> scanActivePredicate,
								  ProgressTracker filePropsProgressTracker){

		//plugable strategy for generating the folder/files mix.
		scanStrategy.scan(scanParams, filePropertiesConsumer, filePropsProgressTracker );
	}
}
