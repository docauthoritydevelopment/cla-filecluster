package da.file.simulator.scan.generators.path;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;

@Profile("file_simulator")
@Component("defaultPathGenerator")
@Scope("singleton")
@Lazy
public class DefaultPathGenerator extends PathGenerator {

	private List<String> folders = new ArrayList<>();

	@Override
	@Value("${simulation.path.args.depth:10}")
	public DefaultPathGenerator setDepth(int depth) {
		this.depth = depth;
		return this;
	}

	@Override
	protected void prepareFolderStructure() {
		//the moment the root path is given - a full folder list can be generated
		folders.clear();
		String currFolder = getRootPath();
		folders.add(currFolder);
		for (int i = 1; i < getDepth(); i++) {
			Path filePath = Paths.get(currFolder, "folder_" + seed++ );
			currFolder = filePath.toString();
			folders.add(currFolder);
		}
	}

	@Override
	public void setRootPath(String rootPath) {
		super.setRootPath(rootPath);

	}

	@Override
	public int getNodeCount() {
		return folders.size();
	}

	@NotNull
	@Override
	public Iterator<String> iterator() {
		return folders.iterator();
	}

	@Override
	public void forEach(Consumer<? super String> action) {
		folders.forEach(action);
	}

	@Override
	public Spliterator<String> spliterator() {
		return folders.spliterator();
	}
}
