package da.file.simulator.scan.generators.file;


import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

public interface FileGenerator {
	ClaFilePropertiesDto generate(String path, boolean isFolder, int seed);
}
