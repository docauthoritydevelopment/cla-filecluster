package da.file.simulator.scan;

import java.util.function.Supplier;

public interface SeedIterator extends Supplier<Integer> {
	SeedIterator reset();
	int getCount();
}
