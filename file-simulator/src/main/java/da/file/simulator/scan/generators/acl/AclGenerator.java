package da.file.simulator.scan.generators.acl;

import com.cla.common.domain.dto.file.FileAclDataDto;

@FunctionalInterface
public interface AclGenerator {
	FileAclDataDto generateFileAcl(int seed);
}
