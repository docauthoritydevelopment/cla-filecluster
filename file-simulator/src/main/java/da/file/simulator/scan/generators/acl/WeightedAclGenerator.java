package da.file.simulator.scan.generators.acl;

import com.cla.common.domain.dto.file.FileAclDataDto;
import com.cla.connector.domain.dto.acl.AclType;
import com.google.common.collect.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Arrays.*;

@SuppressWarnings({"unused"})
public class WeightedAclGenerator implements AclGenerator {
	private static final Logger logger = LoggerFactory.getLogger(WeightedAclGenerator.class);

	@Value("${simulation.acl.args.read-allowed-source:}")
	private String sourceReadAllowedFilePath;
	@Value("${simulation.acl.args.read-denied-source:}")
	private String sourceReadDeniedFilePath;
	@Value("${simulation.acl.args.write-allowed-source:}")
	private String sourceWriteAllowedFilePath;
	@Value("${simulation.acl.args.write-denied-source:}")
	private String sourceWriteDeniedFilePath;

	private Map<AclType, AclWeightedContainer> aclWeightedContainerMap;

	@PostConstruct
	private void init() throws IOException {
		logger.info("Initializing Weighted Acl Generator. Please note that the expected input CSV format is \"names,weight\". " +
				"Multiple names in a row should be between double-quotes.");
		Map<AclType,String> sources = new HashMap<AclType,String>(){
			{
				put(AclType.READ_TYPE, sourceReadAllowedFilePath);
				put(AclType.DENY_READ_TYPE,	sourceReadDeniedFilePath);
				put(AclType.WRITE_TYPE, sourceWriteAllowedFilePath);
				put(AclType.DENY_WRITE_TYPE, sourceWriteDeniedFilePath);
			}};
		stream(AclType.values()).filter(aclType -> sources.get(aclType) == null)
				.forEach(sources::remove);
		aclWeightedContainerMap = stream(AclType.values())
				.collect(Collectors.toMap(acl -> acl, acl -> new AclWeightedContainer()));

		for (AclType aclType: sources.keySet()) {
			String sourceFilePath = sources.get(aclType);
			logger.info("Loading ACL {} from input file: {}", aclType, sourceFilePath);
			AclWeightedContainer aclWeightedContainer = aclWeightedContainerMap.get(aclType);
			try (ICsvMapReader mapReader = new CsvMapReader(new FileReader(sourceFilePath), CsvPreference.STANDARD_PREFERENCE)) {
				String[] header = mapReader.getHeader(true);
				Map<String, Object> entriesMap;
				while ((entriesMap = mapReader.read(header, new CellProcessor[2])) != null) {
					List<String> names = stream(Optional.ofNullable(entriesMap.getOrDefault("names", ""))
							.orElse("")
							.toString()
							.split(","))
								.map(String::trim)
								.filter(s -> !s.isEmpty())
								.collect(Collectors.toList());
					int weight = Integer.parseInt((String) entriesMap.get("weight"));
					aclWeightedContainer.add(weight, names);
				}//while
			}//try
		}//for
	}

	@Override
	public FileAclDataDto generateFileAcl(int seed) {
		return new FileAclDataDto()
						.setAclReadData(aclWeightedContainerMap.get(AclType.READ_TYPE).acl(seed))
						.setAclDeniedReadData(aclWeightedContainerMap.get(AclType.DENY_READ_TYPE).acl(seed))
						.setAclWriteData(aclWeightedContainerMap.get(AclType.WRITE_TYPE).acl(seed))
						.setAclDeniedWriteData(aclWeightedContainerMap.get(AclType.DENY_WRITE_TYPE).acl(seed));
	}

	private static class AclWeightedContainer{
		private final static ImmutableList<String> emptyResult = ImmutableList.of();
		private RangeMap<Integer, List<String>> buckets = TreeRangeMap.create();
		private int totalWeight = 0;

		public void add(int weight, List<String> acls){
			buckets.put(Range.closedOpen(totalWeight, totalWeight + weight), acls);
			totalWeight += weight;
		}

		public List<String> acl(int seed) {
			if(totalWeight == 0) //no range buckets defined
				return emptyResult;
			Random random = new Random(seed);
			int randomSeed = random.nextInt(Integer.MAX_VALUE);
			return buckets.get(randomSeed % totalWeight);
		}
	}
}
