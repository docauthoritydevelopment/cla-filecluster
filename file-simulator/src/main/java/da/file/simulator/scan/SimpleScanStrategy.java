package da.file.simulator.scan;


import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import da.file.simulator.scan.generators.file.FileGeneratorBase;
import da.file.simulator.scan.generators.path.PathGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Profile("file_simulator")
@Component("simpleScanStrategy")
@Scope("singleton")
@Lazy
public class SimpleScanStrategy extends ScanStrategy {

	@Value("${simulation.scan.delayMs:50}")
	private int waitForNext;

	@Autowired
	public SimpleScanStrategy(PathGenerator pathGenerator,  Map<FileType,FileGeneratorBase> fileGenerators) {
		super(pathGenerator, fileGenerators);
	}

	@Override
	public void scan(ScanTaskParameters scanParams, Consumer<ClaFilePropertiesDto> filePropertiesConsumer, ProgressTracker filePropsProgressTracker) {
		FileGeneratorBase folderPropertiesGenerator = fileGenerators.get(FileType.WORD);//any generator will do. I just use the word as default.
		pathGenerator.init(scanParams.getPath(), seedIterator.reset().get());
		final int seedSalt = /*Math.abs(scanParams.getPath().hashCode())*/ + 0;
		Consumer<ClaFilePropertiesDto> consumerWaiter = t -> {
			filePropertiesConsumer.accept(t);
			try {
				Thread.sleep(waitForNext);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};

		Map<FileType, Integer> filesGeneratedTotal = fileGenerators.entrySet().stream().collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().getTargetCount()));
		for (String path : pathGenerator) {
			Integer seed = getSeedIterator().get() + seedSalt;
			ClaFilePropertiesDto folderProperties = folderPropertiesGenerator.generate(path, true, seed);
			consumerWaiter.accept(folderProperties);//notify listener
			for (FileType fileType : fileGenerators.keySet()) {
				FileGeneratorBase fileGenerator  = fileGenerators.get(fileType);
				for (int i = 0; filesGeneratedTotal.get(fileType) > 0 && i < fileGenerator.getTargetCount() / pathGenerator.getNodeCount(); i++) {
					int fileSeed = getSeedIterator().get() + seedSalt;
					ClaFilePropertiesDto claFilePropertiesDto = fileGenerator.generate(path, false, fileSeed);
					consumerWaiter.accept(claFilePropertiesDto);//notify listener
					filesGeneratedTotal.put(fileType, filesGeneratedTotal.get(fileType) - 1 );
				}//for
			}//foreach
		}//foreach
	}
}
