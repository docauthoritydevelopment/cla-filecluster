package da.file.simulator.scan;

import java.util.Random;

public class SimpleSeedIterator implements SeedIterator {
	private final int initialSeed;
	private int seed;
	private int counter = 0;
	private Random random;


	public SimpleSeedIterator(final int seed) {
		this.initialSeed = seed;
		reset();
	}

	@Override public Integer get() {
		counter++;
		return random.nextInt(Integer.MAX_VALUE);
	}

	@Override
	public SeedIterator reset() {
		seed = initialSeed;
		random = new Random(seed);
		counter = 0;
		return this;
	}

	@Override
	public int getCount() {
		return counter;
	}
}
