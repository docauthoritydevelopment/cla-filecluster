package da.file.simulator.scan.generators.file;

import com.cla.connector.domain.dto.file.FileType;
import da.file.simulator.scan.generators.acl.AclGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("file_simulator")
@Component("defaultExcelFileGenerator")
@Lazy
public class DefaultExcelFileGenerator extends FileGeneratorBase implements ExcelFileGenerator {

	public DefaultExcelFileGenerator() {	}

	@Autowired
	@Override
	public DefaultExcelFileGenerator setAclGenerator(AclGenerator aclGenerator) {
		this.aclGenerator = aclGenerator;
		return this;
	}

	@Override
	@Value("${simulation.excel.files.count:10}")
	public FileGeneratorBase setTargetCount(int targetCount) {
		return super.setTargetCount(targetCount);
	}

	@Override
	protected FileType getFileType() {
		return FileType.EXCEL;
	}

	@Override
	protected String getFileMime() {
		return "application/vnd.ms-excel";
	}

	@Override
	protected String getFileExtension() {
		return "xls";
	}

	@Override
	protected String getFileNameBody() {
		return "excel";
	}
}
