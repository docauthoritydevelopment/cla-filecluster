package da.file.simulator.scan.generators.file;

import com.google.common.base.Preconditions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileGeneratorUtils {
	private static final String SEED_MARKER_TEMPLATE = "[seed#%s]";
	private static final String FILE_NAME_TEMPLATE = "%s_%s."+ SEED_MARKER_TEMPLATE +".%s";
	private static final Pattern pattern = Pattern.compile("\\[seed\\#(\\d+)\\]");

	public static String finalizeFileName(String prefix, String nameBody, String fileExtension, long seed){
		return String.format(FILE_NAME_TEMPLATE, prefix, nameBody, seed, fileExtension);
	}

	public static int resolveSeedFromFileName(String fileName){
		Matcher matcher = pattern.matcher(fileName);
		Preconditions.checkArgument(matcher.find(), "Expected a simulated file name of pattern:%s. Actual name:%s", FILE_NAME_TEMPLATE, fileName);
		return Integer.parseInt(matcher.group(1));
	}
}
