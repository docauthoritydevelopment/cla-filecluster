package da.file.simulator.scan;

import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.mediaconnector.MediaConnector;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.filecluster.service.media.MediaConnectorPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.function.Consumer;


@SuppressWarnings("unchecked")
@Profile("file_simulator")
public class MediaConnectorFactorySimulator implements MediaConnectorFactory {
	private static Logger logger = LoggerFactory.getLogger(MediaConnectorFactorySimulator.class);

	private MediaConnectorSimulator mediaConnectorSimulator;

	//region ...overriden methods
	@Override
	public <T extends MediaConnector, S extends MediaConnectionDetailsDto> T getMediaConnector(S connectionDetails) {
		return (T) mediaConnectorSimulator;
	}

	@Override
	public MediaConnectorPool createMediaConnectionPool(MediaConnectionDetailsDto connectionDetails) {
		return null;
	}

	@Override
	public <T extends MediaConnector, S extends MediaConnectionDetailsDto> T createMediaConnector(S connectionDetails) {
		return (T) mediaConnectorSimulator;
	}

	@Override
	public void setKeyValuePairsConsumer(Consumer<Map<String, String>> mapConsumer) {

	}

	//endregion

	//region ...properties
	public MediaConnectorSimulator getMediaConnectorSimulator() {
		return mediaConnectorSimulator;
	}

	public MediaConnectorFactorySimulator setMediaConnectorSimulator(MediaConnectorSimulator mediaConnectorSimulator) {
		this.mediaConnectorSimulator = mediaConnectorSimulator;
		return this;
	}
	//endregion
}
