package da.file.simulator.scan.generators.acl;

import com.cla.common.domain.dto.file.FileAclDataDto;
import com.google.common.collect.ImmutableList;

import java.util.List;

public class ConstantValuesAclGenerator implements AclGenerator {
	final private List<String> allowedPrincipalNames = ImmutableList.copyOf("NTAUTHORITY\\AuthenticatedUsers,BUILTIN\\Users,BUILTIN\\Administrators,NTAUTHORITY\\SYSTEM".split(","));
	final private List<String> emptyList= ImmutableList.of();

	@Override
	public FileAclDataDto generateFileAcl(int seed) {
		FileAclDataDto fileAclDataDto = new FileAclDataDto();
		fileAclDataDto.setAclDeniedReadData(emptyList);
		fileAclDataDto.setAclDeniedWriteData(emptyList);
		fileAclDataDto.setAclReadData(allowedPrincipalNames);
		fileAclDataDto.setAclWriteData(allowedPrincipalNames);
		return fileAclDataDto;
	}
}


