package da.file.simulator.scan.generators.path;

import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Profile("file_simulator")
@Component("treePathGenerator")
@Scope("singleton")
@Lazy
public class TreePathGenerator extends PathGenerator {

	private TreeNode foldersTree;
	private List<TreeNode> flatTreeNodes = new ArrayList<>();
	private int branchingFactor;

	@Value("${simulation.path.args.branching:2}")
	public TreePathGenerator setBranchingFactor(int branchingFactor) {
		this.branchingFactor = branchingFactor;
		return this;
	}

	@Override
	@Value("${simulation.path.args.depth:5}")
	public TreePathGenerator setDepth(int depth) {
		this.depth = depth;
		return this;
	}

	@Override
	protected void prepareFolderStructure() {
		Queue<TreeNode> bfsQueue = new LinkedList<>();
		flatTreeNodes.clear();
		TreeNode treeRootNode = new TreeNode(null);
		flatTreeNodes.add(treeRootNode);
		bfsQueue.add(treeRootNode);
		for(int currDepth = 0; currDepth < depth ; currDepth++){
			while(bfsQueue.size() > 0 && bfsQueue.peek().depth == currDepth ){
				//set
				TreeNode node = bfsQueue.remove();
				node.value = "folder_d" + currDepth + "_" + seed++;
				//expand
				boolean shouldExpand = currDepth + 1 != depth;
				for(int b = 0; b < branchingFactor && shouldExpand; b++){
						TreeNode child = new TreeNode(node);
						child.depth = currDepth + 1;
						node.children.add(child);
						bfsQueue.add(child);
						flatTreeNodes.add(child);
				}
			}//while depth
		}//for
		treeRootNode.value = getRootPath();
		foldersTree = treeRootNode;
	}

	@Override
	public int getNodeCount() {
		return flatTreeNodes.size();
	}

	@NotNull
	@Override
	public Iterator<String> iterator() {
		return flatTreeNodes.stream()
				.map(TreeNode::getPath)
				.collect(Collectors.toList())
				.iterator();
	}

	private static class TreeNode {
		public TreeNode parent;
		public List<TreeNode> children = new ArrayList<>();
		public String value;
		public int depth;

		public TreeNode(TreeNode parent){this.parent=parent;}

		public String getPath(){
			TreeNode current = this;
			LinkedList<String> path = Lists.newLinkedList();
			do{
				path.addFirst(current.value);
				current = current.parent;
			}while(current != null);
			Path filePath = Paths.get(path.removeFirst(), path.toArray(new String[0]));
			return filePath.toString();
		}
	}
}
