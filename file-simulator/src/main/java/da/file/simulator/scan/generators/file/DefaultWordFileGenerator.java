package da.file.simulator.scan.generators.file;

import com.cla.connector.domain.dto.file.FileType;
import da.file.simulator.scan.generators.acl.AclGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("file_simulator")
@Component("defaultWordFileGenerator")
@Lazy
public class DefaultWordFileGenerator extends FileGeneratorBase implements WordFileGenerator {

	//private long creationTimeMilli=1526892192034L;
	//private AclGenerator aclGenerator;

	public DefaultWordFileGenerator() {	}

	@Autowired
	public DefaultWordFileGenerator setAclGenerator(AclGenerator aclGenerator) {
		this.aclGenerator = aclGenerator;
		return this;
	}

	@Override
	@Value("${simulation.word.files.count:10}")
	public FileGeneratorBase setTargetCount(int targetCount) {
		return super.setTargetCount(targetCount);
	}

	@Override
	protected FileType getFileType() {
		return FileType.WORD;
	}

	@Override
	protected String getFileMime() {
		return "application/msword";
	}

	@Override
	protected String getFileExtension() {
		return "DOC";
	}

	@Override
	protected String getFileNameBody() {
		return "doc";
	}

	//@Override
	/*public ClaFilePropertiesDto generate1(String path, boolean isFolder, int seed) {
		String root = Splitter.on(File.separator).omitEmptyStrings().splitToList(path).get(1);
		String fileName = isFolder ? path : Paths.get(path, finalizeFileName( root, "doc", "DOC", seed)).toString();
		ClaFilePropertiesDto fileProperties = new ClaFilePropertiesDto(fileName, isFolder ? 0 : 10000L);

		FileAclDataDto fileAclDataDto = aclGenerator.generateFileAcl(seed);
		fileAclDataDto.getAclReadData().forEach(fileProperties::addAllowedReadPrincipalName);
		fileAclDataDto.getAclReadData().forEach(fileProperties::addAllowedWritePrincipalName);
		fileAclDataDto.getAclDeniedReadData().forEach(fileProperties::addDeniedReadPrincipalName);
		fileAclDataDto.getAclDeniedWriteData().forEach(fileProperties::addDeniedWritePrincipalName);
		fileProperties.setAclSignature(fileProperties.calculateAclSignature());

		fileProperties.setCreationTimeMilli(creationTimeMilli + 1000 * seed );
		fileProperties.setAccessTimeMilli(creationTimeMilli + 10000 * seed);
		fileProperties.setModTimeMilli(creationTimeMilli + 20000 * seed);
		fileProperties.setFile(!isFolder);
		fileProperties.setFolder(isFolder);
		fileProperties.setMediaItemId(fileName);
		fileProperties.setOwnerName("owner_" + seed);
		fileProperties.setClaFileId(null);
		fileProperties.setFileSize(23040L * seed*seed);
		fileProperties.setContentSignature(contentSignHelper.sign(fileName.getBytes()));
		fileProperties.setUserContentSignature("ihAoHhR8ISTGO/GuID6TT0pJYkU=");
		fileProperties.setContentMetadataId(null);
		fileProperties.setFileName(fileName);
		fileProperties.setAclInheritanceType(null);
		fileProperties.setMime("application/msword");
		fileProperties.setType(FileType.WORD);

		return fileProperties;
	}*/
}
