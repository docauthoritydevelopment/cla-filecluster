package da.file.simulator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface SimValue {
	String value();
	String typePlaceHolder() default "$DOC_TYPE";
	String defaultValue() default "";
}
