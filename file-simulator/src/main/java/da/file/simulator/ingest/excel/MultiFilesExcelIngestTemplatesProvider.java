package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.messages.ExcelIngestResult;
import da.file.simulator.ingest.ExportedIngestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class MultiFilesExcelIngestTemplatesProvider implements ExcelIngestTemplateProvider {
	private static final Logger logger = LoggerFactory.getLogger(MultiFilesExcelIngestTemplatesProvider.class);
	@Value("${simulation.excel.ingest.template.args.folder}")
	private String folderPath;
	@Value("${simulation.excel.ingest.template.args.wildcard:*.*}")
	private String wildcard = "*.*";

	private List<ExcelIngestResult> templates;

	public MultiFilesExcelIngestTemplatesProvider() {
	}

	public MultiFilesExcelIngestTemplatesProvider(String folderPath) {
		this.folderPath = folderPath;
		initTemplates();
	}

	@SuppressWarnings({"UnusedDeclaration"})
	@PostConstruct
	private void initTemplates(){
		List<ExportedIngestResult> exportedIngestResults = loadExportedIngestResultsFromFolder(folderPath, wildcard);
		this.templates = exportedIngestResults.stream()
				.map(t->(ExcelIngestResult)t.getIngestResultMessage().getPayload()).collect(Collectors.toList());
		templates.forEach(t->logger.debug("loaded template file " + t.getFileName()));
	}

	@Override
	public ExcelIngestResult getInstance(int seed) {
		return templates.get(seed % templates.size());
	}

	@Override
	public int size() {
		return templates.size();
	}

	@Override
	public String getTemplateId(int seed) {
		return String.valueOf(folderPath.hashCode());
	}
}
