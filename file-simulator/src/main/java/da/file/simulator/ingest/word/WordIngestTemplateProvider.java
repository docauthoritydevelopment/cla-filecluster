package da.file.simulator.ingest.word;

import com.cla.common.domain.dto.messages.WordIngestResult;
import da.file.simulator.ingest.IngestTemplateProvider;
import org.springframework.stereotype.Component;
//tag interface to classify per doc type
public interface WordIngestTemplateProvider extends IngestTemplateProvider<WordIngestResult> {
}
