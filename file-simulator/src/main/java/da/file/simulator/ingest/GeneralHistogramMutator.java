package da.file.simulator.ingest;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class GeneralHistogramMutator extends HistogramsMutatorSkeleton<Long> {
	private static final Logger logger = LoggerFactory.getLogger(GeneralHistogramMutator.class);

	private GroupingStrategy groupingStrategy;

	public GroupingStrategy getGroupingStrategy() {
		return groupingStrategy;
	}

//	@Autowired
	public GeneralHistogramMutator setGroupingStrategy(GroupingStrategy groupingStrategy) {
		this.groupingStrategy = groupingStrategy;
		return this;
	}

	@Override
	protected ClaSuperCollection<Long> applyMutation(ClaSuperCollection<Long> claSuperCollection, int seed, String templateName) {
		Map<String, ClaHistogramCollection<Long>> histogramsMap = claSuperCollection.getCollections();
		offsetHistograms(histogramsMap, seed, templateName);
		return claSuperCollection;
	}

	private synchronized void offsetHistograms(Map<String, ClaHistogramCollection<Long>> histogramsMap, int seed, String templateName){
		int groupOffset = groupingStrategy.calculateGroupOffset(seed, templateName);
		logger.debug("resolved offset {} for seed {}", groupOffset, seed);
		histogramsMap.forEach((histName,histogramCollection) -> {
			Map<Long, Integer> histogramMap = histogramCollection.getCollection().entrySet().stream()
					.collect(Collectors.toMap(entry -> entry.getKey() + groupOffset, Map.Entry::getValue));
			histogramsMap.put(histName, new ClaHistogramCollection<>(histogramMap));
		});
	}

	protected ClaSuperCollection<Long> applyRandomMutation(ClaSuperCollection<Long> claSuperCollection, int seed) {
		Map<String, ClaHistogramCollection<Long>> histogramsMap = claSuperCollection.getCollections();
		Random random = new Random(seed);
		histogramsMap.forEach((histName,histogramCollection) -> {
			Map<Long, Integer> histogramMap = histogramCollection.getCollection();
			logger.debug("before mutation:{}:{}", histName, histogramMap.values().stream().map(String::valueOf).collect(Collectors.joining(",")));
			int rndInt = Math.abs(random.nextInt());
			int mod = rndInt % 5;
			histogramMap.forEach((key, val) -> {
				int weight = Math.abs(seed * Math.abs(random.nextInt()));
				switch (mod) {
					case 0:
					case 1:
						histogramMap.put(key, (int) (val * Float.parseFloat("" + (1 + weight % 10) + "." + (weight + 30))));
						break;
					case 2:
						histogramMap.put(key, (int) (val * Float.parseFloat("0." + (weight))));
						break;
					case 3:
						histogramMap.put(key, (int) (val * Float.parseFloat("" + (weight % 15) + "." + (weight + 70))));
						break;
					case 4:
						histogramMap.put(key, (int) (val * Float.parseFloat("0." + weight / 2)));
						break;
				}
				histogramsMap.put(histName, new ClaHistogramCollection<>(histogramMap));
				logger.debug("after mutation:{}:{}", histName, histogramMap.values().stream().map(String::valueOf).collect(Collectors.joining(",")));
			});
		});
		return claSuperCollection;
	}
}
