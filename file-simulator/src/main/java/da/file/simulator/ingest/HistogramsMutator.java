package da.file.simulator.ingest;

import com.cla.common.domain.dto.histogram.ClaSuperCollection;

public interface HistogramsMutator<T> {
	ClaSuperCollection<T> mutate(final ClaSuperCollection<T> histogramsCollectionPrototype, int seed, String templateName);
}
