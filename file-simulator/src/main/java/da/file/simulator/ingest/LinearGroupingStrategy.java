package da.file.simulator.ingest;

import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import da.file.simulator.SimValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

public class LinearGroupingStrategy implements GroupingStrategy {
	private static final Logger logger = LoggerFactory.getLogger(LinearGroupingStrategy.class);

	RangeMap<Integer, Integer> groupOffsetMap = TreeRangeMap.create();

	@SimValue(value = "${simulation.$DOC_TYPE.grouping.args.maxSizePercent}", defaultValue = "21")
	private int maxGroupSizePercent;

	@SimValue(value = "${simulation.$DOC_TYPE.grouping.args.sizeStepPercent}", defaultValue = "21")
	private int sizeStepPercent;

	@SimValue("simulation.$DOC_TYPE.files.count")
	private int totalFilesCount;

	//region ...getters-setters
	public LinearGroupingStrategy setTotalFilesCount(int totalFilesCount) {
		this.totalFilesCount = totalFilesCount;
		return this;
	}

	public LinearGroupingStrategy setMaxGroupSizePercent(int maxGroupSizePercent) {
		this.maxGroupSizePercent = maxGroupSizePercent;
		return this;
	}

	public LinearGroupingStrategy setSizeStepPercent(int sizeStepPercent) {
		this.sizeStepPercent = sizeStepPercent;
		return this;
	}
	//endregion

	@PostConstruct
	private void initBuckets() {
		int initialTotalFilesCount = totalFilesCount;
		int filesCount = totalFilesCount;
		double stepInverse = 100 - sizeStepPercent;
		int offset = 0;
		int lower = 0;
		int upper = (int) Math.floor (filesCount * (double)maxGroupSizePercent/100);
		while(filesCount > 0){
			groupOffsetMap.put(Range.closedOpen(lower, upper), offset++);
			int bucketSize = upper - lower;
			logger.info("Resolved grouping bucket [{},{}) of {} from {}", lower, upper, bucketSize ,initialTotalFilesCount);
			lower = upper;
			upper = Math.min(upper + Math.max((int) Math.floor(bucketSize * stepInverse/100), 1), initialTotalFilesCount);
			filesCount-= bucketSize;
		}
	}

	@Override
	public int calculateGroupOffset(int fileSeed, String template) {
		return groupOffsetMap.get(fileSeed % totalFilesCount);
	}
}
