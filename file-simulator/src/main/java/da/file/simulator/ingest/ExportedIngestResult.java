package da.file.simulator.ingest;

import com.cla.common.domain.dto.messages.IngestResultMessage;

import java.util.Map;

public class ExportedIngestResult {
	Map<String,Object> statistics;
	IngestResultMessage ingestResultMessage;

	public Map<String, Object> getStatistics() {
		return statistics;
	}

	public ExportedIngestResult setStatistics(Map<String, Object> statistics) {
		this.statistics = statistics;
		return this;
	}

	public IngestResultMessage getIngestResultMessage() {
		return ingestResultMessage;
	}

	public ExportedIngestResult setIngestResultMessage(IngestResultMessage ingestResultMessage) {
		this.ingestResultMessage = ingestResultMessage;
		return this;
	}
}
