package da.file.simulator.ingest;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
abstract public class PatternSearchResultsGeneratorBase {
	abstract protected Map<String, Integer> getPatternPercents();

	public static Map<String,Integer> splitString(String patternPercentsStr){
		if(patternPercentsStr != null && patternPercentsStr.length()>0){
			return Arrays.stream(patternPercentsStr.split(","))
					.map(s->{	int li = s.lastIndexOf(":");
								return new String[]{s.substring(0, li), s.substring(li + 1)};})
					.collect(Collectors.toMap(s->s[0].trim(), s->Integer.parseInt(s[1].trim())));
		}
		return new HashMap<>();
	}

	public Set<SearchPatternCountDto> generate(int seed, Collection<TextSearchPatternImpl> textSearchPatterns) {
		Random generator = new Random(seed);
		Set<SearchPatternCountDto> patternCountDtoSet = textSearchPatterns.stream()
				.filter(p -> getPatternPercents().containsKey(p.getName()))
				.map(p -> {
					int probability = getPatternPercents().get(p.getName()).intValue();
					return generator.nextInt(100) <= probability ?
							new SearchPatternCountDto(p.getId(), p.getName(), 1, Lists.newArrayList())
							: null;
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
		return patternCountDtoSet;
	}
}
