package da.file.simulator.ingest;

import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.utils.DaMessageConversionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.base.Preconditions;
import com.google.common.io.Files;
import org.apache.commons.collections4.functors.NotNullPredicate;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public interface IngestTemplateProvider<T extends IngestMessagePayload> {
	Logger logger = LoggerFactory.getLogger(IngestTemplateProvider.class);
	T getInstance(int seed);
	int size();
	String getTemplateId(int seed);

	default List<ExportedIngestResult> loadExportedIngestResultsFromFolder(String folderPath, String wildcard){
		try {
			File path = new File(folderPath);
			Preconditions.checkArgument(path.exists(), "Invalid path! The file or folder " +folderPath+ " doesn`t exist");
			if(path.isFile()){
				return deserializeTemplates(path);
			}
			FileFilter fileFilter = new WildcardFileFilter(wildcard);
			File[] files = path.listFiles(fileFilter);
			return deserializeTemplates(files);
		} catch (Exception e) {
			logger.error("error deserializing templates", e);
			return new ArrayList<>();
		}
	}

	default List<ExportedIngestResult> deserializeTemplates(File... files) {
		ObjectMapper objectMapper = new ObjectMapper();
		return Arrays.stream(files).filter(f -> f.isFile())
				.map(f -> {
					try {
						return objectMapper.readValue(f, ExportedIngestResult.class);
					} catch (IOException e) {
						logger.error("error deserializing template", e);
						return null;
					}
				}).filter(NotNullPredicate.INSTANCE::evaluate)
				.collect(Collectors.toList());
	}

	default List<T> loadTemplatesFromFolder(String folderPath, String wildcard){
		File dir = new File(folderPath);
		FileFilter fileFilter = new WildcardFileFilter(wildcard);
		File[] files = dir.listFiles(fileFilter);
		return Arrays.stream(files).filter(f -> f.isFile())
				.map(f -> resultIngestTemplateDeserializer(readFromFile(f)))
				.collect(Collectors.toList());
	}

	default String readFromFile(File file){
		try {
			return Files.asCharSource(file, Charsets.UTF_8).read();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	default T resultIngestTemplateDeserializer(String serialized) {
		T template;
		try {
			IngestResultMessage resultMessage = DaMessageConversionUtils.Ingest.deserializeResultMessage(serialized);
			template = (T) resultMessage.getPayload();
		} catch (IOException | ClassNotFoundException e) {
			template = null;
		}
		return template;
	}

}