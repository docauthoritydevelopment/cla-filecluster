package da.file.simulator.ingest.word;

import com.cla.common.domain.dto.messages.WordIngestResult;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static da.file.simulator.ingest.PatternSearchResultsGeneratorBase.splitString;

@SuppressWarnings("unused")
public class WeightedMultiFilesWordIngestTemplatesProvider implements WordIngestTemplateProvider {
	private static final Logger logger = LoggerFactory.getLogger(WeightedMultiFilesWordIngestTemplatesProvider.class);

	@Value("${simulation.word.ingest.template.args.foldersWeights:}")
	private String foldersWeightsStr;

	private RangeMap<Integer, MultiFilesWordIngestTemplatesProvider> foldersBuckets = TreeRangeMap.create();
	private int totalWeight = 0;
	private int size = 0;

	@PostConstruct
	private void init(){
		Map<String,Integer> foldersWeightsMap = splitString(foldersWeightsStr);
		for (String folder : foldersWeightsMap.keySet().stream().sorted(String::compareTo).collect(Collectors.toList())) {
			if(folder.startsWith("#")){
				logger.info("skipping path " + folder);
				continue;
			}
			Integer weight = foldersWeightsMap.get(folder);
			MultiFilesWordIngestTemplatesProvider templatesProvider = new MultiFilesWordIngestTemplatesProvider(folder);
			foldersBuckets.put(Range.closedOpen(totalWeight, totalWeight + weight), templatesProvider);
			totalWeight += weight;
			size += templatesProvider.size();
			logger.info("loaded templates from folder {}, weighted {} with {} files" , folder, weight, templatesProvider.size());
		}
	}

	@Override
	public WordIngestResult getInstance(int seed) {
		int randomSeed = getRandomSeed(seed);
		MultiFilesWordIngestTemplatesProvider templatesProvider = foldersBuckets.get(randomSeed % totalWeight);
		return templatesProvider.getInstance(randomSeed);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public String getTemplateId(int seed) {
		int randomSeed = getRandomSeed(seed);
		MultiFilesWordIngestTemplatesProvider templatesProvider = foldersBuckets.get(randomSeed % totalWeight);
		return templatesProvider.getTemplateId(randomSeed);
	}

	private static int getRandomSeed(int seed) {
		Random random = new Random(seed);
		return random.nextInt(Integer.MAX_VALUE);
	}
}
