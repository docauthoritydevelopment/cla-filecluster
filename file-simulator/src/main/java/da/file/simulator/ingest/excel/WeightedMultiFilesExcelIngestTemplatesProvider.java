package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static da.file.simulator.ingest.PatternSearchResultsGeneratorBase.splitString;

@SuppressWarnings("unused")
public class WeightedMultiFilesExcelIngestTemplatesProvider implements ExcelIngestTemplateProvider {
	private static final Logger logger = LoggerFactory.getLogger(WeightedMultiFilesExcelIngestTemplatesProvider.class);

	@Value("${simulation.excel.ingest.template.args.foldersWeights:}")
	private String foldersWeightsStr;

	private RangeMap<Integer, MultiFilesExcelIngestTemplatesProvider> foldersBuckets = TreeRangeMap.create();
	private int totalWeight = 0;
	private int size = 0;

	@PostConstruct
	private void init(){
		Map<String,Integer> foldersWeightsMap = splitString(foldersWeightsStr);
		for (String folder : foldersWeightsMap.keySet().stream().sorted(String::compareTo).collect(Collectors.toList())) {
			if(folder.startsWith("#")){
				logger.info("skipping path " + folder);
				continue;
			}
			Integer weight = foldersWeightsMap.get(folder);
			MultiFilesExcelIngestTemplatesProvider templatesProvider = new MultiFilesExcelIngestTemplatesProvider(folder);
			foldersBuckets.put(Range.closedOpen(totalWeight, totalWeight + weight), templatesProvider);
			totalWeight += weight;
			size += templatesProvider.size();
			logger.info("loaded templates from folder {}, weighted {} with {} files" , folder, weight, templatesProvider.size());
		}
	}

	@Override
	public ExcelIngestResult getInstance(int seed) {
		Random random = new Random(seed);
		int randomSeed = random.nextInt(Integer.MAX_VALUE);
		MultiFilesExcelIngestTemplatesProvider templatesProvider = foldersBuckets.get(randomSeed % totalWeight);
		return templatesProvider.getInstance(randomSeed);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public String getTemplateId(int seed) {
		Random random = new Random(seed);
		int randomSeed = random.nextInt(Integer.MAX_VALUE);
		MultiFilesExcelIngestTemplatesProvider templatesProvider = foldersBuckets.get(randomSeed % totalWeight);
		return templatesProvider.getTemplateId(randomSeed);
	}
}
