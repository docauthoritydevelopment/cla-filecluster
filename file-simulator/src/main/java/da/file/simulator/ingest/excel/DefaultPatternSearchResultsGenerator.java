package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.mediaproc.SearchPatternServiceInterface;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.ExcelSheetIngestResult;

import da.file.simulator.ingest.PatternSearchResultsGeneratorBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;


@SuppressWarnings("unused")
public class DefaultPatternSearchResultsGenerator extends PatternSearchResultsGeneratorBase implements ExcelPatternSearchResultsGenerator {
	private static final Logger logger = LoggerFactory.getLogger(DefaultPatternSearchResultsGenerator.class);

	@Autowired
	private SearchPatternServiceInterface searchPatternService;

	@Value("${simulation.excel.ingest.pattern.args.patternPercents:}")
	private String patternPercentsStr;
	private Map<String,Integer> patternPercentsMap = new HashMap<>();

	@PostConstruct
	private void init(){
		patternPercentsMap = splitString(patternPercentsStr);
	}

	/**
	 * Will generate pattern search results per each sheet of the excel. in order for the resulting values
	 * to differ between sheets - will increment the seed for every sheet.
	 * @param seed
	 * @param excelIngestResult
	 */
	@Override
	public void generate(int seed, ExcelIngestResult excelIngestResult) {
		for (ExcelSheetIngestResult sheetIngestResult : excelIngestResult.getSheets()) {
			Set<SearchPatternCountDto> patternCountDtoSet = generate(seed++, searchPatternService.getAllFileSearchPatternImpls());
			logger.debug("generated {} patterns for seed {}",patternCountDtoSet.size(), seed);
			sheetIngestResult.setSearchPatternsCounting(patternCountDtoSet);
		}
	}

	@Override
	protected Map<String, Integer> getPatternPercents() {
		return patternPercentsMap;
	}
}
