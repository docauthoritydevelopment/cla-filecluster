package da.file.simulator.ingest;

public interface GroupingStrategy {
	int calculateGroupOffset(int fileSeed, String template);
}
