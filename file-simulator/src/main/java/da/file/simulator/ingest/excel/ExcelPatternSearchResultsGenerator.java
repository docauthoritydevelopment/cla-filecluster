package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.messages.ExcelIngestResult;

public interface ExcelPatternSearchResultsGenerator {
	void generate(int seed, ExcelIngestResult excelIngestResult);
}
