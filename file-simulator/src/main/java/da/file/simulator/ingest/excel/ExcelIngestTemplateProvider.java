package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.messages.ExcelIngestResult;
import da.file.simulator.ingest.IngestTemplateProvider;

//tag interface to classify per doc type
public interface ExcelIngestTemplateProvider extends IngestTemplateProvider<ExcelIngestResult> {
}
