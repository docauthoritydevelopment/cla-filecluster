package da.file.simulator.ingest;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.*;
import com.rometools.utils.Strings;
import da.file.simulator.SimValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvMapReader;
import org.supercsv.io.ICsvMapReader;
import org.supercsv.prefs.CsvPreference;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

public class ExplicitGroupingStrategy implements GroupingStrategy {
    private static final Logger logger = LoggerFactory.getLogger(ExplicitGroupingStrategy.class);

    @SimValue("simulation.$DOC_TYPE.ingest.grouping.args.groupPercents")
    private String groupWeightsStr;

    private Map<String, RangeMap<Double, Integer>> groupsOffsetMap;
    private Map<String, Double> totalWeights = new HashMap<>();
    private Map<String, String> folderHashes = new HashMap<>();
    private Random rnd = new Random();

    //region ...getters-setters
    public ExplicitGroupingStrategy setGroupWeightsStr(String groupWeightsStr) {
        this.groupWeightsStr = groupWeightsStr.replaceAll("\\s*:\\s*\\[", ":\\[");
        return this;
    }
    //endregion

    @PostConstruct
    public void initBuckets() {
        groupsOffsetMap = new HashMap<>();
        Multimap<String, Double> weightedTemplates = splitAndParseString(groupWeightsStr);
        weightedTemplates.asMap().forEach((templateId, weights) -> {
            logger.info("building range tree for template source: {}", folderHashes.get(templateId));
            double percentSum = weights.stream().mapToDouble(Double::doubleValue).sum();
            Preconditions.checkArgument(percentSum <= 100,
                    "Invalid input: percent sum %s is larger than 100% on group %s",
                    percentSum, folderHashes.get(templateId));
            double totalWeight = 0;
            TreeRangeMap<Double, Integer> templateOffsetMap = TreeRangeMap.create();
            int offset = 0;
            for (Double weight : weights) {
                double upper = (totalWeight + weight);
                templateOffsetMap.put(Range.closedOpen(totalWeight, upper), offset);
                logger.info("assigned offset {} to range bucket [{},{}), for template source: {}",
                        offset, totalWeight, upper, folderHashes.get(templateId));
                offset++;
                totalWeight = upper;
            }
            double singleFileBucket = (100 - percentSum);
            if (singleFileBucket > 0) {
                templateOffsetMap.put(Range.closedOpen(totalWeight, totalWeight + singleFileBucket), Integer.MIN_VALUE);
                logger.info("assigned single file groups bucket of size {} for template source: {}",
                        singleFileBucket, folderHashes.get(templateId));
            }
            groupsOffsetMap.put(templateId, templateOffsetMap);
            totalWeights.put(templateId, totalWeight + singleFileBucket);
        });
    }

    @Override
    public int calculateGroupOffset(int fileSeed, String template) {
        RangeMap<Double, Integer> offsetRangeMap = groupsOffsetMap.get(template);
        Preconditions.checkNotNull(offsetRangeMap, "The template with ID:%s is not present in the offset range map!", template);
        Integer offset = offsetRangeMap.get(fileSeed % totalWeights.get(template));
        return offset == Integer.MIN_VALUE ?
                rnd.nextInt(Integer.MAX_VALUE) : offset;
    }

    private Multimap<String, Double> splitAndParseString(String weightsStr) {
        ImmutableMultimap.Builder<String, Double> builder = ImmutableMultimap.builder();
        if (Strings.isEmpty(weightsStr)) {
            return builder.build();
        }
        Map<String, String> result = Splitter.onPattern("\\],?")
                .trimResults().omitEmptyStrings()
                .withKeyValueSeparator(":[")
                .split(weightsStr);
        for (Map.Entry<String, String> entry : result.entrySet()) {
            String groupName = entry.getKey();
            if (groupName.startsWith("#")) {
                logger.info("skipping group " + groupName);
                continue;
            }
            List<Double> weights = Splitter.onPattern("\\[|,")
                    .omitEmptyStrings().trimResults()
                    .splitToList(entry.getValue())
                    .stream().map(Double::parseDouble).collect(Collectors.toList());

            String folderHash = String.valueOf(groupName.trim().hashCode());
            builder.putAll(folderHash, weights);
            folderHashes.put(folderHash, groupName);
            logger.info("assigned hash value {} to group {}", folderHash, groupName);
        }
        return builder.build();
    }

    private Map<String, Integer> loadCsv() throws IOException {
        logger.info("Initializing Explicit Grouping strategy. Please note that the expected input CSV format is \"folder,weight\".");
        logger.info("Loading Weights from input file: {}", groupWeightsStr);
        //Map<Path,Integer> paths = new HashMap<>();
        Map<String, Integer> paths = new HashMap<>();
        try (ICsvMapReader mapReader = new CsvMapReader(new FileReader(groupWeightsStr), CsvPreference.STANDARD_PREFERENCE)) {
            String[] header = mapReader.getHeader(true);
            Map<String, Object> entriesMap;
            while ((entriesMap = mapReader.read(header, new CellProcessor[2])) != null) {
                String folder = entriesMap.getOrDefault("folder", "").toString();
                if (folder.startsWith("#")) {
                    logger.info("skipping commented out row #{} [{}]", mapReader.getRowNumber(), folder);
                    continue;
                }
                //Path path = Paths.get(folder);
                //Preconditions.checkArgument(Files.exists(path), "The path %s does not exist! " +
                //		"You can skip it by putting a # as a first char!", folder);
                int weight = Integer.parseInt((String) entriesMap.get("weight"));
                //paths.put(path, weight);
                paths.put(folder, weight);
            }//while
        }//try
        return paths;
    }
}
