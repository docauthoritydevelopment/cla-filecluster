package da.file.simulator.ingest;

import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.service.IngestRequestHandler;
import com.cla.filecluster.service.IngestRequestHandlerProvider;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.util.Map;

/**
 * This class is an injection point for the simulator. It facades a ingest request handler selection.
 */
@Profile("file_simulator")
@Component
public class IngestRequestHandlerProviderSimulator implements IngestRequestHandlerProvider {
	private static Logger logger = LoggerFactory.getLogger(IngestRequestHandlerProviderSimulator.class);

	private Map<FileType,IngestRequestHandler> ingestRequestHandlersMap;

	@Autowired
	public IngestRequestHandlerProviderSimulator(Map<FileType, IngestRequestHandler> ingestRequestHandlersMap) {
		this.ingestRequestHandlersMap = ingestRequestHandlersMap;
	}

	@Override
	public IngestRequestHandler getIngestRequestHandler(IngestRequestMessage requestMessage) throws FileNotFoundException {
		FileType fileType = requestMessage.getPayload().getFileType();
		Preconditions.checkArgument(ingestRequestHandlersMap.containsKey(fileType),"No IngestRequestHandler was defined for file type=" + fileType);
		return ingestRequestHandlersMap.get(fileType);
	}
}
