package da.file.simulator.ingest.word;

import com.cla.common.domain.dto.messages.WordIngestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class MultiFilesWordIngestTemplatesProvider implements WordIngestTemplateProvider {
	private static final Logger logger = LoggerFactory.getLogger(MultiFilesWordIngestTemplatesProvider.class);
	@Value("${simulation.word.ingest.template.args.folder}")
	private String folderPath;
	@Value("${simulation.word.ingest.template.args.wildcard:*.*}")
	private String wildcard = "*.*";

	private List<WordIngestResult> templates;

	public MultiFilesWordIngestTemplatesProvider() {
	}

	public MultiFilesWordIngestTemplatesProvider(String folderPath) {
		this.folderPath = folderPath;
		initTemplates();
	}

	@SuppressWarnings({"UnusedDeclaration"})
	@PostConstruct
	private void initTemplates(){
		this.templates = loadExportedIngestResultsFromFolder(folderPath, wildcard).stream()
				.map(t->(WordIngestResult)t.getIngestResultMessage().getPayload()).collect(Collectors.toList());
		templates.forEach(t->logger.debug("loaded template file " + t.getFileName()));
	}

	@Override
	public WordIngestResult getInstance(int seed) {
		return templates.get(seed % templates.size());
	}

	@Override
	public int size() {
		return templates.size();
	}

	@Override
	public String getTemplateId(int seed) {
		return String.valueOf(folderPath.hashCode());
	}
}
