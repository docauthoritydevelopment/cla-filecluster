package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.utils.DaMessageConversionUtils;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.io.IOException;
import java.net.URL;

@SuppressWarnings("unused")
public class SingleResultExcelIngestTemplateProvider implements ExcelIngestTemplateProvider {
	private static final Logger logger = LoggerFactory.getLogger(SingleResultExcelIngestTemplateProvider.class);

	private final ExcelIngestResult ingestResultTemplate;

	public SingleResultExcelIngestTemplateProvider() {
		ExcelIngestResult template;
		try {
			URL url = Resources.getResource("excel_ingest_result_template.txt");
			oneResult = Resources.toString(url, Charsets.UTF_8).trim();
			IngestResultMessage resultMessage = DaMessageConversionUtils.Ingest.deserializeResultMessage(oneResult);
			template = (ExcelIngestResult) resultMessage.getPayload();
		} catch (IOException |ClassNotFoundException e) {
			logger.error("can`t deserialize the preloaded template", e);
			template = null;
		}
		ingestResultTemplate = template;
	}

	@Override
	public ExcelIngestResult getInstance(int seed) {
		ExcelIngestResult ingestResult = new ExcelIngestResult();
		BeanUtils.copyProperties(ingestResultTemplate, ingestResult);
		return ingestResult;
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public String getTemplateId(int seed) {
		return String.valueOf("excel_ingest_result_template.txt".hashCode());
	}
	//region ...template
	private String oneResult = "";
//endregion
}
