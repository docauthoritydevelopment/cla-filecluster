package da.file.simulator.ingest;

import com.cla.common.domain.dto.FileMetadata;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

import java.util.Date;

public abstract class SimulatorIngestRequestHandlerBase {
	protected void setMetadata(ClaFilePropertiesDto filePropertiesDto, FileMetadata metadata) {
		metadata.setAclReadAllowed(filePropertiesDto.getAclReadAllowed());
		metadata.setAclReadDenied(filePropertiesDto.getAclReadDenied());
		metadata.setAclWriteAllowed(filePropertiesDto.getAclWriteAllowed());
		metadata.setAclWriteDenied(filePropertiesDto.getAclWriteDenied());
		metadata.setCreatDate(new Date(filePropertiesDto.getCreationTimeMilli()));

		//metadata.setFsFileSize(filePropertiesDto.getFileSize()/1000);
		metadata.setOwner(filePropertiesDto.getOwnerName());
	}

	protected void delay(int wait){
		try {
			Thread.sleep(wait);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
