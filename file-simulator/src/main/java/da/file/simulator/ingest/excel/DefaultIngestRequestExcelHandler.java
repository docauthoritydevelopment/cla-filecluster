package da.file.simulator.ingest.excel;

import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.IngestRequestHandler;
import da.file.simulator.ingest.HistogramsMutator;
import da.file.simulator.ingest.SimulatorIngestRequestHandlerBase;
import da.file.simulator.scan.generators.file.ExcelFileGenerator;
import da.file.simulator.scan.generators.file.FileGeneratorUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Profile("file_simulator")
@Component("defaultIngestRequestExcelHandler")
@Lazy
public class DefaultIngestRequestExcelHandler extends SimulatorIngestRequestHandlerBase implements IngestRequestHandler {

	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Value("${simulation.excel.ingest.delayMs:50}")
	private int wait;

	@Autowired
	private ExcelIngestTemplateProvider excelIngestTemplateProvider;

	@Autowired
	private ExcelFileGenerator fileGenerator;

	@Resource(name="excelHistogramMutator")
	private HistogramsMutator<Long> histogramMutator;

	@Autowired
	private ExcelPatternSearchResultsGenerator patternSearchResultsGenerator;

	@Override
	public List<Pair<IngestTaskParameters, IngestMessagePayload>> handleIngestionRequest(
			IngestRequestMessage ingestRequestMessage) {

		String fullFilePath = ingestRequestMessage.getPayload().getFilePath();
		int seed = FileGeneratorUtils.resolveSeedFromFileName(fullFilePath);
		String templateId = excelIngestTemplateProvider.getTemplateId(seed);

		logger.trace("get template provider task {}",ingestRequestMessage.getTaskId());
		ExcelIngestResult instance = excelIngestTemplateProvider.getInstance(seed);
		ExcelIngestResult messagePayloadResult = SerializationUtils.clone(instance);
		logger.trace("generate for task {}",ingestRequestMessage.getTaskId());
		//now set some specific data
		ClaFilePropertiesDto filePropertiesDto = fileGenerator.generate(FilenameUtils.getFullPath(fullFilePath), false, seed);
		messagePayloadResult.setClaFileProperties(filePropertiesDto);
		messagePayloadResult.setFileName(filePropertiesDto.getFileName());

		logger.trace("mutate for task {}",ingestRequestMessage.getTaskId());

		Map<String, ClaSuperCollection<Long>> sheetHistogramsCollection = messagePayloadResult.getFileCollections().getCollections();
		sheetHistogramsCollection.forEach((sheetName, superCollection)->{
			ClaSuperCollection<Long> mutatedHistograms = histogramMutator.mutate(superCollection, seed, templateId);
			sheetHistogramsCollection.put(sheetName, mutatedHistograms);
		});
		messagePayloadResult.setCombinedProposedTitles(Collections.singletonList(FilenameUtils.getBaseName(fullFilePath)));
		patternSearchResultsGenerator.generate(seed, messagePayloadResult);

		ExcelWorkbookMetaData metadata = messagePayloadResult.getMetadata();
		setMetadata(filePropertiesDto, metadata);

		delay(wait);
		return Collections.singletonList(Pair.of(ingestRequestMessage.getPayload(), messagePayloadResult));
	}
}
