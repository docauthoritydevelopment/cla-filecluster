package da.file.simulator.ingest;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;

abstract public class HistogramsMutatorSkeleton<T> implements HistogramsMutator<T> {
	@Override
	public ClaSuperCollection<T> mutate(ClaSuperCollection<T> histogramsCollectionPrototype, int seed, String templateName) {
		ClaSuperCollection<T> clone = cloneHistogramCollection(histogramsCollectionPrototype);
		return applyMutation(clone, seed, templateName);
	}

	protected abstract ClaSuperCollection<T> applyMutation(ClaSuperCollection<T> claSuperCollection, int seed, String templateName);

	private ClaSuperCollection<T> cloneHistogramCollection(final ClaSuperCollection<T> histogramsCollectionPrototype) {
		ClaSuperCollection<T> clone = new ClaSuperCollection<>();
		histogramsCollectionPrototype.getCollections().forEach((collectionName,collection) -> {
			try {
				ClaHistogramCollection<T> collectionClone = new ClaHistogramCollection<>(collection.getCollection(), collection.getType());
				clone.setCollection(collectionName, collectionClone);
			} catch (Exception e) {
				// do nothing
			}
		});
		return clone;
	}
}
