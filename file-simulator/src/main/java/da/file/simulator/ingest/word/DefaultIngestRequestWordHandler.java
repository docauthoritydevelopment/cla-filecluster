package da.file.simulator.ingest.word;

import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestRequestMessage;
import com.cla.common.domain.dto.messages.IngestTaskParameters;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.IngestRequestHandler;
import da.file.simulator.ingest.HistogramsMutator;
import da.file.simulator.ingest.SimulatorIngestRequestHandlerBase;
import da.file.simulator.scan.generators.file.FileGeneratorUtils;
import da.file.simulator.scan.generators.file.WordFileGenerator;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

@Profile("file_simulator")
@Component("defaultIngestRequestWordHandler")
@Lazy
public class DefaultIngestRequestWordHandler extends SimulatorIngestRequestHandlerBase implements IngestRequestHandler {

	@Value("${simulation.word.ingest.delayMs:50}")
	private int wait;

	@Autowired
	private WordIngestTemplateProvider wordIngestTemplateProvider;

	@Autowired
	private WordFileGenerator fileGenerator;

	@Resource(name = "wordHistogramMutator")
	private HistogramsMutator<Long> histogramMutator;

	@Autowired
	private WordPatternSearchResultsGenerator patternSearchResultsGenerator;

	@Override
	public List<Pair<IngestTaskParameters, IngestMessagePayload>> handleIngestionRequest(
			IngestRequestMessage ingestRequestMessage) {

		String fullFilePath = ingestRequestMessage.getPayload().getFilePath();
		int seed = FileGeneratorUtils.resolveSeedFromFileName(fullFilePath);
		String templateId = wordIngestTemplateProvider.getTemplateId(seed);
		WordIngestResult instance = wordIngestTemplateProvider.getInstance(seed);
		WordIngestResult messagePayloadResult = SerializationUtils.clone(instance);
		//now set some specific data
		ClaFilePropertiesDto filePropertiesDto = fileGenerator.generate(FilenameUtils.getFullPath(fullFilePath), false, seed);
		messagePayloadResult.setClaFileProperties(filePropertiesDto);
		ClaSuperCollection<Long> mutatedHistograms = histogramMutator.mutate(messagePayloadResult.getLongHistograms(), seed, templateId);
		messagePayloadResult.setLongHistograms(mutatedHistograms);
		messagePayloadResult.setCombinedProposedTitles(Collections.singletonList(FilenameUtils.getBaseName(fullFilePath)));
		patternSearchResultsGenerator.generate(seed, messagePayloadResult);

		WordfileMetaData metadata = messagePayloadResult.getMetadata();
		setMetadata(filePropertiesDto, metadata);

		delay(wait);
		return Collections.singletonList(Pair.of(ingestRequestMessage.getPayload(), messagePayloadResult));
	}
}

