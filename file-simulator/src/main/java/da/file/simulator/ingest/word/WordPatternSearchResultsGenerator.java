package da.file.simulator.ingest.word;

import com.cla.common.domain.dto.messages.WordIngestResult;

public interface WordPatternSearchResultsGenerator {
	void generate(int seed, WordIngestResult wordIngestResult);
}
