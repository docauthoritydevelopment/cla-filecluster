package da.file.simulator.ingest.word;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.mediaproc.SearchPatternServiceInterface;
import com.cla.common.domain.dto.messages.WordIngestResult;
import da.file.simulator.ingest.PatternSearchResultsGeneratorBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;


@SuppressWarnings("unused")
public class DefaultPatternSearchResultsGenerator extends PatternSearchResultsGeneratorBase implements WordPatternSearchResultsGenerator {
	private static final Logger logger = LoggerFactory.getLogger(DefaultPatternSearchResultsGenerator.class);

	@Autowired
	private SearchPatternServiceInterface searchPatternService;

	@Value("${simulation.word.ingest.pattern.args.patternPercents:}")
	private String patternPercentsStr;
	private Map<String,Integer> patternPercentsMap = new HashMap<>();

	@PostConstruct
	private void init(){
		patternPercentsMap = splitString(patternPercentsStr);
	}

	@Override
	public void generate(int seed, WordIngestResult wordIngestResult) {
		Set<SearchPatternCountDto> patternCountDtoSet = generate(seed, searchPatternService.getAllFileSearchPatternImpls());
		logger.debug("generated {} patterns for seed {}",patternCountDtoSet.size(), seed);
		wordIngestResult.setSearchPatternsCounting(patternCountDtoSet);
	}

	@Override
	public Map<String, Integer> getPatternPercents() {
		return patternPercentsMap;
	}
}
