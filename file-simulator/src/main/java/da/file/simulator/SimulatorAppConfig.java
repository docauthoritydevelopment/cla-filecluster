package da.file.simulator;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.service.IngestRequestHandler;
import com.google.common.collect.Maps;
import da.file.simulator.ingest.GeneralHistogramMutator;
import da.file.simulator.ingest.GroupingStrategy;
import da.file.simulator.ingest.HistogramsMutator;
import da.file.simulator.ingest.excel.ExcelIngestTemplateProvider;
import da.file.simulator.ingest.excel.ExcelPatternSearchResultsGenerator;
import da.file.simulator.ingest.word.WordIngestTemplateProvider;
import da.file.simulator.ingest.word.WordPatternSearchResultsGenerator;
import da.file.simulator.scan.MediaConnectorFactorySimulator;
import da.file.simulator.scan.MediaConnectorSimulator;
import da.file.simulator.scan.ScanStrategy;
import da.file.simulator.scan.SimpleSeedIterator;
import da.file.simulator.scan.generators.acl.AclGenerator;
import da.file.simulator.scan.generators.file.FileGeneratorBase;
import da.file.simulator.scan.generators.path.PathGenerator;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@Profile("file_simulator")
public class SimulatorAppConfig {
	private static Logger logger = LoggerFactory.getLogger(SimulatorAppConfig.class);

	@Autowired
	private ConfigurableEnvironment env;
	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private ConfigurableListableBeanFactory beanFactory;

	// access a properties.yml file like properties
	@Bean
	@Primary
	public PropertySource injectProperties() {
		PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();
		YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();

		// Configure simulator yaml resource
		Resource yamlResource;
		if (env.containsProperty("sim.conf")) {
			String yamlFilePath = env.getProperty("sim.conf");
			yamlResource = new FileSystemResource(yamlFilePath);
			logger.info("Configured sim.conf = {}", yamlFilePath);
		} else {
			yamlResource = new ClassPathResource("file_simulator_conf.yml");
			logger.warn("No Configured sim.conf falling back to default: {}", "file_simulator_conf.yml");
		}
		yaml.setResources(yamlResource);

		propertySourcesPlaceholderConfigurer.setProperties(yaml.getObject());
		// properties need to be processed by beanfactory to be accessible after
		propertySourcesPlaceholderConfigurer.postProcessBeanFactory(beanFactory);
		PropertySource<?> propertySource = propertySourcesPlaceholderConfigurer.getAppliedPropertySources().get(PropertySourcesPlaceholderConfigurer.LOCAL_PROPERTIES_PROPERTY_SOURCE_NAME);
		env.getPropertySources().addFirst(propertySource);
		return propertySource;
	}

	@Bean
	@DependsOn("aclGenerator")
	public Map<FileType,FileGeneratorBase> fileGenerators(){
		Map<FileType,FileGeneratorBase> fileGenerators = Maps.newHashMap();
		Map<FileType,String> defaultFileGenerators = new HashMap<FileType,String>(){{
			put(FileType.WORD, "defaultWordFileGenerator");
			put(FileType.EXCEL, "defaultExcelFileGenerator");
		}};
		defaultFileGenerators.keySet().forEach(fileType -> {
			String beanName = env.getProperty("simulation." + fileType.name().toLowerCase() + ".files.generator");
			FileGeneratorBase fileGenerator = getBeanByName(beanName, defaultFileGenerators.get(fileType));
			fileGenerators.put(fileType, fileGenerator);
		});
		return fileGenerators;
	}

	@Bean
	@DependsOn("injectProperties")
	public PathGenerator pathGenerator(){
		PathGenerator generator = getBeanByName("simulation.path.generator", "treePathGenerator");
		return generator;
	}

	@Bean
	public ScanStrategy scanStrategy(PathGenerator pathGenerator, Map<FileType,FileGeneratorBase> fileGenerators){
		int seed = Integer.parseInt(env.getProperty("simulation.seed", "0")); //this is a little bit hacky because the property didn`t resolve from yaml directly
		ScanStrategy scanStrategy = getBeanByName("simulation.scan.strategy", "simpleScanStrategy", pathGenerator, fileGenerators);
		scanStrategy.setSeedIterator(new SimpleSeedIterator(seed));
		return scanStrategy;
	}

	@Bean
	public MediaConnectorFactorySimulator mediaConnectorFactorySimulator(ScanStrategy scanStrategy){
		MediaConnectorSimulator mediaConnectorSimulator = new MediaConnectorSimulator(scanStrategy);
		MediaConnectorFactorySimulator mediaConnectorFactorySimulator = new MediaConnectorFactorySimulator()
				.setMediaConnectorSimulator(mediaConnectorSimulator);
		return mediaConnectorFactorySimulator;
	}

	@Bean
	public GroupingStrategy wordGroupingStrategy() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
		GroupingStrategy linearGroupingStrategy = getClassInstanceByNameAndType(FileType.WORD, "LinearGroupingStrategy", "da.file.simulator.ingest");
		return linearGroupingStrategy;
	}

	@Bean
	public GroupingStrategy excelGroupingStrategy() throws IllegalAccessException, InstantiationException, ClassNotFoundException{
		GroupingStrategy groupingStrategy = getClassInstanceByNameAndType(FileType.EXCEL, "LinearGroupingStrategy", "da.file.simulator.ingest");
		return groupingStrategy;
	}

	@Bean
	public HistogramsMutator<Long> excelHistogramMutator() throws IllegalAccessException, ClassNotFoundException, InstantiationException {
		GeneralHistogramMutator generalHistogramMutator = new GeneralHistogramMutator();
		generalHistogramMutator.setGroupingStrategy(excelGroupingStrategy());
		return generalHistogramMutator;
	}

	@Bean
	public HistogramsMutator<Long> wordHistogramMutator(GroupingStrategy wordGroupingStrategy){
		GeneralHistogramMutator generalHistogramMutator = new GeneralHistogramMutator();
		generalHistogramMutator.setGroupingStrategy(wordGroupingStrategy);
		return generalHistogramMutator;
	}

	@Bean
	public WordIngestTemplateProvider wordIngestTemplateProvider() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
		String property = "simulation." + FileType.WORD.name().toLowerCase() + ".ingest.template.provider";
		return getClassInstanceByName(property, "SingleResultWordIngestTemplateProvider", "da.file.simulator.ingest.word");
	}

	@Bean
	public ExcelIngestTemplateProvider excelIngestTemplateProvider() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		String property = "simulation." + FileType.EXCEL.name().toLowerCase() + ".ingest.template.provider";
		return getClassInstanceByName(property, "SingleResultExcelIngestTemplateProvider", "da.file.simulator.ingest.excel");
	}

	@Bean
	public Map<FileType,IngestRequestHandler> ingestRequestHandlers(){
		Map<FileType,IngestRequestHandler> ingestRequestHandlersMap = Maps.newHashMap();
		Map<FileType,String> defaultIngestRequestHandlers = new HashMap<FileType,String>(){{
			put(FileType.WORD, "defaultIngestRequestWordHandler");
			put(FileType.EXCEL, "defaultIngestRequestExcelHandler");
		}};

		//TODO: load histogram mutator, currently only one implemented, hence - using autowire

		defaultIngestRequestHandlers.keySet().forEach(fileType -> {
			String beanName = env.getProperty("simulation." + fileType.name().toLowerCase() + ".ingest.handler");
			IngestRequestHandler ingestRequestHandler = getBeanByName(beanName, defaultIngestRequestHandlers.get(fileType));
			ingestRequestHandlersMap.put(fileType, ingestRequestHandler);
		});
		return ingestRequestHandlersMap;
	}

	@Bean
	public WordPatternSearchResultsGenerator wordPatternSearchResultsGenerator() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
		String property = "simulation." + FileType.WORD.name().toLowerCase() + ".ingest.pattern.generator";
		return getClassInstanceByName(property, "DefaultPatternSearchResultsGenerator", "da.file.simulator.ingest.word");
	}

	@Bean
	public ExcelPatternSearchResultsGenerator excelPatternSearchResultsGenerator() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
		String property = "simulation." + FileType.EXCEL.name().toLowerCase() + ".ingest.pattern.generator";
		return getClassInstanceByName(property, "DefaultPatternSearchResultsGenerator", "da.file.simulator.ingest.excel");
	}

	@Bean
	public AclGenerator aclGenerator() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
		String property = "simulation.acl.generator";
		return getClassInstanceByName(property, "ConstantValuesAclGenerator", "da.file.simulator.scan.generators.acl");
	}

	private <T> T getBeanByName(String envProperty, String defaultBean, Object... args){
		envProperty = envProperty == null ? defaultBean : envProperty;
		String beanName = env.getProperty(envProperty, defaultBean);
		logger.info("Resolved simulation argument {}={}", envProperty, beanName);
		return (T)  (args==null || args.length==0 ?  applicationContext.getBean(beanName)
				: applicationContext.getBean(beanName, args));
	}

	private <T> T getClassInstanceByName(String envProperty, String defaultClass, String packagePrefix) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
		envProperty = envProperty == null ? defaultClass : envProperty;
		String className = packagePrefix +
				(packagePrefix.endsWith(".") ? "" : "." ) +
					env.getProperty(envProperty, defaultClass);
		logger.info("Resolved simulation argument {}={}", envProperty, className);
		Class<?> clazz = Class.forName(className);
		return (T) clazz.newInstance();
	}

	private  <T> T getClassInstanceByNameAndType(FileType fileType, String defaultClass, String packagePrefix) throws IllegalAccessException, InstantiationException, ClassNotFoundException{
		String fileTypeName = fileType.name().toLowerCase();
		String property = "simulation." + fileTypeName + ".ingest.grouping.strategy";
		T classInstanceByName = getClassInstanceByName(property, defaultClass, packagePrefix);
		BeanWrapper bw =  new BeanWrapperImpl(classInstanceByName);
		List<Field> fieldsWithAnnotation = FieldUtils.getFieldsListWithAnnotation(classInstanceByName.getClass(), SimValue.class);
		for (Field field : fieldsWithAnnotation) {
			SimValue annotation = field.getAnnotation(SimValue.class);
			String fieldProp = annotation.value().replace(annotation.typePlaceHolder(), fileTypeName);
			String propertyValue = env.getProperty(fieldProp, annotation.defaultValue());
			bw.setPropertyValue(field.getName(), propertyValue);
		}
		return classInstanceByName;
	}

}
