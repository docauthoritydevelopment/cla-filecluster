@ECHO off
REM ##########################################################################
REM
REM  Run Upgrade scripts
REM
REM ##########################################################################

REM Set local scope for the variables with windows NT shell
REM if "%OS%"=="Windows_NT" setlocal
setlocal
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%

if NOT '%1' == 'execute' (
    if NOT '%1' == 'baseline' (
        if NOT '%1' == 'drop-schema' (
            if NOT '%1' == 'solr' (
                if NOT '%1' == 'info' goto usage
            )
        )
    )
)

IF NOT [%1] == [solr] (
echo.
echo DocAuthority Upgrade scripts executor
echo Start processing...
echo     action=%1
echo     database-url=%2
echo     user=%3
echo.
echo Initialising...
)

if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%~dp0

REM Add default JVM options here. You can also use JAVA_OPTS and CLA_FILECLUSTER_OPTS to pass JVM options to this script.
REM set "JMX_CONFIG_FILE=%APP_HOME%\config\jmx.properties"

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"
set "JAVA_OPTS=-Dlog4j.configurationFile=log4j2.xml"
set JAVA_EXE=java
set LIB_FOLDER=%DIRNAME%\upgradeTools\build\libs
set CLASSPATH=%LIB_FOLDER%\*

REM Execute Program
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% -classpath "%CLASSPATH%" com.cla.filecluster.scripts.RunUpgradeSqlScripts %*

:end
REM End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
REM Set variable CLA_FILECLUSTER_EXIT_CONSOLE if you need the _script_ return code instead of
REM the _cmd.exe /c_ return code!
IF  not "" == "%CLA_FILECLUSTER_EXIT_CONSOLE%" exit 1
EXIT /b 1

:mainEnd
IF "%OS%"=="Windows_NT" endlocal
GOTO finally

:usage
echo. 
echo DocAuthority Database upgrade tool
echo. 
echo Usage:
echo     %0 [action]
echo     %0 [action] [database-url] [user] [password]
echo.
echo     [action]           - The action to do: [execute,info,baseline,solr]
echo                        [execute]: run all upgrade scripts not previously executed
echo                        [solr]: run specific solr-cloud operations:
echo                                create-collection-if-needed, create-collection, delete-collections,
echo                                reload-collections, reload-collection
echo                        [info]: see a list of all pending upgrade scripts
echo                        [baseline]: run the baseline script (part of basic installation)
echo     [database-url] 	- A full JDBC url of the database: (jdbc:mysql://localhost:3306/docauthority)
goto finally

:finally
