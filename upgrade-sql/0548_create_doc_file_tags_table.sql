CREATE TABLE IF NOT EXISTS `doc_type_file_tags` (
	`doc_type_id` BIGINT(20) NOT NULL,
	`file_tag_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`doc_type_id`, `file_tag_id`),
	UNIQUE INDEX `dtft_file_tag_id_idx` (`file_tag_id`),
	CONSTRAINT `dtft_file_tag_id_fk` FOREIGN KEY (`file_tag_id`) REFERENCES `file_tag` (`id`),
	CONSTRAINT `dtft_doc_type_id_fk` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;