alter table crawl_runs MODIFY COLUMN `version` INT(11) NULL DEFAULT 0;
alter table jm_tasks MODIFY COLUMN `version` INT(11) NULL DEFAULT 0;
alter table jm_jobs MODIFY COLUMN `version` INT(11) NULL DEFAULT 0;