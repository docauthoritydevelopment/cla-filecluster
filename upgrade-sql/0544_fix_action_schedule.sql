alter TABLE `action_schedule` add column `name` VARCHAR(255) NULL DEFAULT NULL;

alter TABLE `action_schedule` add UNIQUE INDEX `name_idx` (`name`);

