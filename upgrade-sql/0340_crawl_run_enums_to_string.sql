ALTER TABLE `crawl_runs`
    CHANGE COLUMN `run_status` `run_status_o` int(11) DEFAULT NULL
  , CHANGE COLUMN `run_type` `run_type_o` int(11) DEFAULT NULL
;
ALTER TABLE `crawl_runs`
    DROP KEY `idx_runstatus`
  , DROP KEY `idx_runtype`
;
ALTER TABLE `crawl_runs`
	ADD COLUMN `run_status` char(32) NOT NULL DEFAULT 'NA'
  , ADD COLUMN `run_type` char(32) NOT NULL DEFAULT 'ROOT_FOLDER'
;
UPDATE `crawl_runs`
    SET run_status = CASE
            WHEN run_status_o = 0 THEN 'FINISHED_SUCCESSFULLY'
            WHEN run_status_o = 1 THEN 'NA'
            WHEN run_status_o = 2 THEN 'RUNNING'
            WHEN run_status_o = 3 THEN 'FAILED'
            WHEN run_status_o = 4 THEN 'STOPPED'
            WHEN run_status_o = 5 THEN 'UN_USED1'
            WHEN run_status_o = 6 THEN 'PAUSED'
    END
    , run_type = CASE
            WHEN run_type_o = 0 THEN 'ROOT_FOLDER'
            WHEN run_type_o = 1 THEN 'BIZ_LIST'
            WHEN run_type_o = 2 THEN 'SCHEDULE_GROUP'
   END
;
ALTER TABLE `crawl_runs`
    ADD KEY `idx_runstatus` (`run_status`)
  , ADD KEY `idx_runtype` (`run_type`)
;
