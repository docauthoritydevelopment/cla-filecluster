-- add time_stamp to primary key so we can partition by it
ALTER TABLE audit DROP PRIMARY KEY, add PRIMARY KEY(event_id, time_stamp);

-- Note: if you get Error Code: 1418 try running SET GLOBAL log_bin_trust_function_creators = 1;

-- create partition by timestamp and the first partition
set @partition_date = UNIX_TIMESTAMP(DATE_ADD(CURDATE(), INTERVAL 1 DAY))*1000;
set @alter_p := concat('ALTER TABLE audit PARTITION BY RANGE (time_stamp) (PARTITION p0 VALUES LESS THAN(',@partition_date,') ENGINE=InnoDB)');
prepare stmnt from @alter_p;
execute stmnt;

-- drop stored procedures
DROP FUNCTION IF EXISTS get_last_partition_audit_name;
DROP FUNCTION IF EXISTS get_last_partition_audit_date;
DROP FUNCTION IF EXISTS get_first_partition_audit_name;
DROP FUNCTION IF EXISTS get_first_partition_audit_date;

DELIMITER $$

-- create stored procedures to get last partition data
CREATE FUNCTION get_last_partition_audit_name() RETURNS varchar(50)
BEGIN

SELECT partition_name
into @last_partition_name
from information_schema.PARTITIONS
where table_schema = 'da' and table_name = 'audit'
and partition_ordinal_position = (
select max(partition_ordinal_position)
from information_schema.PARTITIONS
where table_schema = 'da' and table_name = 'audit');

RETURN (@last_partition_name);

END $$

CREATE FUNCTION get_last_partition_audit_date() RETURNS BIGINT(20)
BEGIN

select partition_description
into @last_partition_date
from information_schema.PARTITIONS
where table_schema = 'da' and table_name = 'audit'
and partition_ordinal_position = (
select max(partition_ordinal_position)
from information_schema.PARTITIONS
where table_schema = 'da' and table_name = 'audit');

RETURN (@last_partition_date);

END $$

-- create stored procedures to get first partition data

CREATE FUNCTION get_first_partition_audit_name() RETURNS varchar(50)
BEGIN

SELECT partition_name
into @fst_partition_name
from information_schema.PARTITIONS
where table_schema = 'da' and table_name = 'audit'
and partition_ordinal_position = 1;

RETURN (@fst_partition_name);

END $$

CREATE FUNCTION get_first_partition_audit_date() RETURNS BIGINT(20)
BEGIN

select partition_description
into @fst_partition_date
from information_schema.PARTITIONS
where table_schema = 'da' and table_name = 'audit'
and partition_ordinal_position = 1;

RETURN (@fst_partition_date);

END $$

DELIMITER ;

