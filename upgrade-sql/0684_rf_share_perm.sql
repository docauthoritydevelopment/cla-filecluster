CREATE TABLE `root_folder_share_permission` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`acl_entry_type` VARCHAR(255) NULL DEFAULT NULL,
	`mask` BIGINT(20) NULL DEFAULT NULL,
	`root_folder_id` BIGINT(20) NULL DEFAULT NULL,
	`text_permissions` VARCHAR(2048) NULL DEFAULT NULL,
	`user` VARCHAR(255) NULL DEFAULT NULL,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;