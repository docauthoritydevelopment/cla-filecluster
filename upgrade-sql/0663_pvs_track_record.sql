CREATE TABLE `pvs_track_record` (
	`id` BIGINT(20) NOT NULL,
	`collection_state` VARCHAR(255) NULL DEFAULT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;