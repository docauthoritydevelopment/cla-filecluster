alter table schedule_group ADD COLUMN `is_default` BIT(1) NULL DEFAULT NULL;
update schedule_group set `is_default` = b'0';
update schedule_group set `is_default` = b'1' where name='default';