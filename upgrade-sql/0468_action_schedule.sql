alter table action_execution_task_status add column `action_params` JSON NULL DEFAULT NULL;
alter table action_execution_task_status add column `entity_id` VARCHAR(255) NULL DEFAULT NULL;
alter table action_execution_task_status add column `entity_type` VARCHAR(255) NULL DEFAULT NULL;
alter table action_execution_task_status add column `filter_descriptor` JSON NULL DEFAULT NULL;
alter table action_execution_task_status add column `action_id` VARCHAR(255) NULL DEFAULT NULL;

CREATE TABLE `action_schedule` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`action_execution_task_status_id` BIGINT(20) NOT NULL,
	`active` BIT(1) NOT NULL,
	`cron_trigger_string` VARCHAR(255) NULL DEFAULT NULL,
	`last_execution_date` BIGINT(20) NOT NULL,
	`result_path` VARCHAR(255) NULL DEFAULT NULL,
	`scheduling_json` VARCHAR(4096) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name_idx` (`name`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

