ALTER TABLE `audit`
	ADD COLUMN `on_object_id` varchar(1024) DEFAULT NULL,
    ADD COLUMN `on_object_type` varchar(255) DEFAULT NULL,
    ADD COLUMN `params` varchar(1024) DEFAULT NULL,
    MODIFY COLUMN `object_id` varchar(1024) DEFAULT NULL
;
