-- Version 1.5 DB-Upgrade script
--
-- To be run after 'schema_15_initial_creation.sql' was run
-- Solr DB should be copied to a new location for dedicated 1.5 copy (to keep old 1.4 DB operational)
-- Post activation:     /api/run/dbupgrade/finalize
--      - calc folders statistics
--      - calc content statistics
--      - calc group statistics
--      - Reset CAT_FILE
--
-- Full upgrade procedure:
--    - stop 1.4 services
--    - create backup copy for DocAuthorty folder
--    - create backup copy for Solr data folder
--    - Uninstall 1.4 (only if no longer required)
--    - Install 1.5 (or copy ZIP with full env.) - set db schema to be 'da_1_5'
--    - Stop 1.5 services
--    - MySql configuration: in my.ini file set 'innodb_buffer_pool_size=1G' (restart MySql after update)
--    - Create 1.5 DB schema
--    - Run DB upgrade script
--       * If Workbench is being used, update read-timeout to a high number: Edit / Preferences / SQL Editor - read timeout -> 600000 seconds
--    - Copy / update cofig files app.prop, app-win.prop - keep DB schema set to new 1.5 schema
--    - If co-exsist with 1.4, modify ports (server, JMX, UI)
--    - If co-exsist with 1.4, set Solt core folders with different names
--    - App properties: comment the following
--          #scanner.metadata.removeOwnerQuotes=true
--          #scanner.metadata.ownerDbWithQuotes=false
--    - Start services
--    - Activate /api/run/dbupgrade/finalize
--    ...

-- Allow simple copy of the tables
ALTER TABLE da_1_5.doc_folder MODIFY COLUMN `deleted` bit(1) NOT NULL DEFAULT 0;
ALTER TABLE da_1_5.file_groups MODIFY COLUMN `count_on_last_naming` bigint(20) DEFAULT NULL;
ALTER TABLE da_1_5.file_groups MODIFY COLUMN `deleted` bit(1) NOT NULL DEFAULT 0;
ALTER TABLE da_1_5.file_groups MODIFY COLUMN `dirty` bit(1) NOT NULL DEFAULT 0;
ALTER TABLE da_1_5.file_groups MODIFY COLUMN `first_member_content_id` bigint(20) DEFAULT NULL;
ALTER TABLE da_1_5.file_groups MODIFY COLUMN `group_type` int(11) NOT NULL DEFAULT 0;
ALTER TABLE da_1_5.action_execution_task_status MODIFY COLUMN `errors_count` bigint(20) NOT NULL DEFAULT 0;
ALTER TABLE da_1_5.biz_list_item_extraction MODIFY COLUMN `agg_min_count` int(11) DEFAULT NULL;
ALTER TABLE da_1_5.users MODIFY COLUMN `password_must_be_changed` bit(1) NOT NULL DEFAULT 0;
ALTER TABLE da_1_5.users MODIFY COLUMN `user_role` int(11) DEFAULT 0;
ALTER TABLE da_1_5.users MODIFY COLUMN `user_state` int(11) DEFAULT 0;
ALTER TABLE da_1_5.users MODIFY COLUMN `user_type` int(11) DEFAULT 0;
ALTER TABLE da_1_5.users MODIFY COLUMN `deleted` bit(1) NOT NULL DEFAULT '\0';


-- Optional
-- alter table docauthority.doc_type drop column entity_list_id;
-- alter table docauthority.extraction_configuration drop column entity_list_id;
-- alter table docauthority.saved_charts drop column show_un_associated;

DROP PROCEDURE IF EXISTS update_auto_increment;
DELIMITER //
CREATE PROCEDURE update_auto_increment(IN src_tbl TEXT, IN dst_tbl TEXT)
BEGIN
    DECLARE _max_stmt VARCHAR(1024);
    DECLARE _stmt VARCHAR(1024);
    SET @inc := 0;
	
    SET @MAX_SQL := CONCAT('SELECT IFNULL(MAX(`id`), 0) + 1 INTO @inc FROM ', src_tbl);
    PREPARE _max_stmt FROM @MAX_SQL;
    EXECUTE _max_stmt;
    DEALLOCATE PREPARE _max_stmt;

    SET @SQL := CONCAT('ALTER TABLE ', dst_tbl, ' AUTO_INCREMENT = ', @inc);
    -- SELECT @SQL;
    PREPARE _stmt FROM @SQL;
    EXECUTE _stmt;
    DEALLOCATE PREPARE _stmt;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS copy_table;
DELIMITER //
CREATE PROCEDURE copy_table(IN t1 TEXT, IN src TEXT, IN dst TEXT)
BEGIN
  DECLARE stmt_flds VARCHAR(1024);
  DECLARE stmt_trn VARCHAR(1024);
  DECLARE stmt_ins VARCHAR(1024);
  DECLARE stmt_ver VARCHAR(1024);

  SET @qry := CONCAT('SELECT GROUP_CONCAT(COLUMN_NAME) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ''',
				src,''' AND TABLE_NAME = ''',t1,''' INTO @src_fields');
  -- select @qry;
  PREPARE stmt_flds FROM @qry;
  EXECUTE stmt_flds;
  DEALLOCATE PREPARE stmt_flds;
  
  set @qry = (SELECT CONCAT("TRUNCATE TABLE ", dst, ".", t1));
  -- select @qry;
  PREPARE stmt_trn FROM @qry;
  EXECUTE stmt_trn;
  DEALLOCATE PREPARE stmt_trn;

  set @qry = (SELECT CONCAT("INSERT INTO ", dst, ".", t1," (",@src_fields,") (SELECT * FROM ", src, ".", t1,")"));
  -- select @qry;
  PREPARE stmt_ins FROM @qry;
  EXECUTE stmt_ins;
  DEALLOCATE PREPARE stmt_ins;

  SET @qry := CONCAT('SELECT count(*) FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ''',
				dst,''' AND TABLE_NAME = ''',t1,''' AND COLUMN_NAME = ''id'' INTO @id_found');
  -- select @qry;
  PREPARE stmt_ver FROM @qry;
  EXECUTE stmt_ver;
  DEALLOCATE PREPARE stmt_ver;
  IF (@id_found > 0) THEN
	CALL update_auto_increment(CONCAT(src,'.',t1), CONCAT(dst,'.',t1));
  END IF;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS id_batch_query;
DELIMITER //
CREATE PROCEDURE id_batch_query(IN t1 TEXT, IN myq TEXT)
BEGIN
  DECLARE stmt_maxid VARCHAR(1024);
  DECLARE stmt_q VARCHAR(4096);

  SET @qry := CONCAT('SELECT MAX(id) FROM ',t1,' INTO @max_id');
  -- select @qry;
  PREPARE stmt_maxid FROM @qry;
  EXECUTE stmt_maxid;
  DEALLOCATE PREPARE stmt_maxid;

  SET @qry := CONCAT('SELECT MIN(id) FROM ',t1,' INTO @min_id');
  -- select @qry;
  PREPARE stmt_maxid FROM @qry;
  EXECUTE stmt_maxid;
  DEALLOCATE PREPARE stmt_maxid;

  set @from_id = @min_id - 1;

  WHILE (@from_id < @max_id) DO
      set @to_id = @from_id + 10000;

      SET @qry := CONCAT(myq, ' WHERE id > ', @from_id, ' AND id <= ', @to_id);
      -- select @qry;
      PREPARE stmt_q FROM @qry;
      EXECUTE stmt_q;
      DEALLOCATE PREPARE stmt_q;

      set @from_id = @to_id;
  END WHILE;

END //
DELIMITER ;


-- Data conversion starts here --

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- COPY identical tables
-- CALL copy_table('accounts', 'docauthority', 'da_1_5');
CALL copy_table('acl_items', 'docauthority', 'da_1_5');
CALL copy_table('aclcategory_filter_acl', 'docauthority', 'da_1_5');
CALL copy_table('act_file_action_desired', 'docauthority', 'da_1_5');
-- CALL copy_table('action_class_executor_def', 'docauthority', 'da_1_5');
-- CALL copy_table('action_definition', 'docauthority', 'da_1_5');
CALL copy_table('action_execution_task_status', 'docauthority', 'da_1_5');
CALL copy_table('action_instance', 'docauthority', 'da_1_5');
-- CALL copy_table('action_parameter_definition', 'docauthority', 'da_1_5');
CALL copy_table('biz_list', 'docauthority', 'da_1_5');
CALL copy_table('biz_list_item_extraction', 'docauthority', 'da_1_5');
CALL copy_table('bl_item_alias', 'docauthority', 'da_1_5');
CALL copy_table('bl_item_simple', 'docauthority', 'da_1_5');
CALL copy_table('business_id_tag_association', 'docauthority', 'da_1_5');
CALL copy_table('cat_fil_val', 'docauthority', 'da_1_5');
CALL copy_table('category_filters', 'docauthority', 'da_1_5');
CALL copy_table('cla_file_tag_association', 'docauthority', 'da_1_5');
-- CALL copy_table('doc_folder', 'docauthority', 'da_1_5');
-- CALL copy_table('doc_store', 'docauthority', 'da_1_5');
CALL copy_table('doc_type', 'docauthority', 'da_1_5');
CALL copy_table('doc_type_association', 'docauthority', 'da_1_5');
CALL copy_table('doc_type_risk_information', 'docauthority', 'da_1_5');
CALL copy_table('doc_type_template', 'docauthority', 'da_1_5');
CALL copy_table('err_processed_files', 'docauthority', 'da_1_5');
CALL copy_table('extracted_entities', 'docauthority', 'da_1_5');
CALL copy_table('extracted_text', 'docauthority', 'da_1_5');
CALL copy_table('extraction_configuration', 'docauthority', 'da_1_5');
CALL copy_table('file_categories', 'docauthority', 'da_1_5');
CALL copy_table('file_groups', 'docauthority', 'da_1_5');
CALL copy_table('file_tag', 'docauthority', 'da_1_5');
CALL copy_table('file_tag_type', 'docauthority', 'da_1_5');
CALL copy_table('folder_tag_association', 'docauthority', 'da_1_5');
CALL copy_table('group_unification_helper', 'docauthority', 'da_1_5');
CALL copy_table('jobs', 'docauthority', 'da_1_5');
CALL copy_table('missed_file_match', 'docauthority', 'da_1_5');
CALL copy_table('phase_details', 'docauthority', 'da_1_5');
CALL copy_table('pol_compound_policy_item', 'docauthority', 'da_1_5');
CALL copy_table('pol_compound_policy_item_policy_items', 'docauthority', 'da_1_5');
-- CALL copy_table('pol_desired_state_definition', 'docauthority', 'da_1_5');
CALL copy_table('policy_item', 'docauthority', 'da_1_5');
CALL copy_table('policy_item_action_instances', 'docauthority', 'da_1_5');
CALL copy_table('policy_item_attachment', 'docauthority', 'da_1_5');
CALL copy_table('policy_item_rules', 'docauthority', 'da_1_5');
CALL copy_table('policy_rule_instance', 'docauthority', 'da_1_5');
-- CALL copy_table('pv_analysis_data', 'docauthority', 'da_1_5');
CALL copy_table('role_authorities', 'docauthority', 'da_1_5');
CALL copy_table('roles', 'docauthority', 'da_1_5');
CALL copy_table('saved_charts', 'docauthority', 'da_1_5');
-- CALL copy_table('saved_filters', 'docauthority', 'da_1_5');
CALL copy_table('subtree', 'docauthority', 'da_1_5');
CALL copy_table('users', 'docauthority', 'da_1_5');
CALL copy_table('users_roles', 'docauthority', 'da_1_5');

-- End of identical tables copy

TRUNCATE TABLE da_1_5.`doc_store`;
INSERT INTO da_1_5.`doc_store` (
	`id`,
	`account_id`,
	`description`,
	`path_descriptor_type`
) SELECT
	`id`,
	1,
	`description`,
	`doc_store_type`
FROM docauthority.doc_store f WHERE f.id > 0;
CALL update_auto_increment('docauthority.doc_store', 'da_1_5.`doc_store`');

-- doc_folder (added 'deleted')
TRUNCATE TABLE da_1_5.`doc_folder`;
CALL id_batch_query('docauthority.doc_folder',
    'INSERT INTO da_1_5.`doc_folder` (`id`,    '
    ' 	`absolute_depth`,    '
    ' 	`bounding_subtree_id`,    '
    ' 	`depth_from_root`,    '
    ' 	`error_message`,    '
    ' 	`folder_hashed`,    '
    ' 	`name`,    '
    ' 	`num_of_all_files`,    '
    ' 	`num_of_direct_files`,    '
    ' 	`num_of_sub_folders`,    '
    ' 	`parents_info`,    '
    ' 	`path`,    '
    ' 	`real_path`,    '
    ' 	`doc_store_id`,    '
    ' 	`parent_folder_id`,    '
    ' 	`root_folder_id`    '
    ' ) SELECT * FROM docauthority.doc_folder f  ');
-- INSERT INTO da_1_5.`doc_folder` (
--	`id`,
--	`absolute_depth`,
--	`bounding_subtree_id`,
--	`depth_from_root`,
--	`error_message`,
--	`folder_hashed`,
--	`name`,
--	`num_of_all_files`,
--	`num_of_direct_files`,
--	`num_of_sub_folders`,
--	`parents_info`,
--	`path`,
--	`real_path`,
--	`doc_store_id`,
--	`parent_folder_id`,
--	`root_folder_id`
-- ) SELECT * FROM docauthority.doc_folder f WHERE f.id > 0;
CALL update_auto_increment('docauthority.doc_folder', 'da_1_5.`doc_folder`');

-- cla_files -> cla_files_md
TRUNCATE TABLE da_1_5.`cla_files_md`;
CALL id_batch_query('docauthority.cla_files',
	'INSERT INTO da_1_5.`cla_files_md` (	'
	'	`id`,	'
	'	`base_name`,	'
	'	`content_dirty`,	'
	'	`created_ondb`,	'
	'	`creation_date`,	'
	'	`initial_scan_date`,	'
	'	`deleted`,	'
	'	`dirty`,	'
	'	`dirty_wip`,	'
	'	`extension`,	'
	'	`file_name_hashed`,	'
	'	`fs_last_modified`,	'
	'	`full_path`,	'
	'	`mime`,	'
	'	`owner`,	'
	'	`size_on_media`,	'
	'	`state`,	'
	'	`type`,	'
	'	`updated_ondb`,	'
	'	`account_id`,	'
	'	`clajob_id`,	'
	'	`content_metadata_id`,	'
	'	`doc_folder_id`,	'
	'	`file_group_id`,	'
	'	`user_group_id`,	'
	'	`ingestion_summary_contentId`	'
	'	) SELECT	'
	'		id,	'
	'		base_name,	'
	'		1,	'
	'		created_ondb,	'
	'		creation_date,	'
	'		created_ondb,	'
	'		0,	'
	'		1,	'
	'		0,	'
	'		extension,	'
	'		file_name_hashed,	'
	'		fs_last_modified,	'
	'		full_path,	'
	'		mime,	'
	'		owner,	'
	'		fs_file_size,	'
	'		state,	'
	'		type,	'
	'		updated_ondb,	'
	'		account_id,	'
	'		clajob_id,	'
	'		id,	'
	'		doc_folder_id,	'
	'		file_group_id,	'
	'		file_group_id,	'
	'		id	'
	'	FROM docauthority.cla_files f 	');
-- INSERT INTO da_1_5.`cla_files_md` (
--    `id`,
--    `base_name`,
--    `content_dirty`,
--    `created_ondb`,
--    `creation_date`,
--    `initial_scan_date`,
--    `deleted`,
--    `dirty`,
--    `dirty_wip`,
--    `extension`,
--    `file_name_hashed`,
--    `fs_last_modified`,
--    `full_path`,
--    `mime`,
--    `owner`,
--    `size_on_media`,
--    `state`,
--    `type`,
--    `updated_ondb`,
--    `account_id`,
--    `clajob_id`,
--    `content_metadata_id`,
--    `doc_folder_id`,
--    `file_group_id`,
--    `user_group_id`,
--    `ingestion_summary_contentId`
--    ) SELECT
--        id,
--        base_name,
--        1,
--        created_ondb,
--        creation_date,
--        created_ondb,
--        0,
--        1,
--        0,
--        extension,
--        file_name_hashed,
--        fs_last_modified,
--        full_path,
--        mime,
--        owner,
--        fs_file_size,
--        state,
--        type,
--        updated_ondb,
--        account_id,
--        clajob_id,
--        id,
--        doc_folder_id,
--        file_group_id,
--        file_group_id,
--        id
--    FROM docauthority.cla_files f WHERE f.id > 0;
CALL update_auto_increment('docauthority.cla_files', 'da_1_5.`cla_files_md`');
UPDATE da_1_5.`cla_files_md` cf
	LEFT JOIN da_1_5.`doc_folder` df ON df.id = cf.doc_folder_id
	SET cf.root_folder_id = df.root_folder_id
	WHERE cf.root_folder_id IS NULL;

-- cla_files -> content_metadata
TRUNCATE TABLE da_1_5.`content_metadata`;

CALL id_batch_query('docauthority.cla_files',
	'INSERT INTO da_1_5.`content_metadata` (	'
	'	`id`,	'
	'	`account_id`,	'
	'   `analyze_hint`, '
	'	`app_creation_date`,	'
	'	`author`,	'
	'	`company`,	'
	'	`file_count`,	'
	'	`fs_file_size`,	'
	'	`generating_app`,	'
	'	`group_dirty`,	'
	'	`items_countl1`,	'
	'	`items_countl2`,	'
	'	`items_countl3`,	'
	'	`keywords`,	'
	'	`population_dirty`,	'
	'	`state`,	'
	'	`subject`,	'
	'	`template`,	'
	'	`title`,	'
	'	`type`,	'
	'	`file_group_id`,	'
	'	`user_group_id`	'
	'	) SELECT	'
	'		id,	'
	'		account_id,	'
	'       0,  '
	'		creation_date,	'
	'		author,	'
	'		company,	'
	'		1,	'
	'		fs_file_size,	'
	'		generating_app,	'
	'		1,	'
	'		items_countl1,	'
	'		items_countl2,	'
	'		items_countl3,	'
	'		keywords,	'
	'		1,	'
	'		state,	'
	'		subject,	'
	'		template,	'
	'		title,	'
	'		type,	'
	'		file_group_id,	'
	'		file_group_id	'
	'	FROM docauthority.cla_files f 	');

-- INSERT INTO da_1_5.`content_metadata` (
--    `id`,
--    `account_id`,
--    `app_creation_date`,
--    `author`,
--    `company`,
--    `file_count`,
--    `fs_file_size`,
--    `generating_app`,
--    `group_dirty`,
--    `items_countl1`,
--    `items_countl2`,
--    `items_countl3`,
--    `keywords`,
--    `population_dirty`,
--    `state`,
--    `subject`,
--    `template`,
--    `title`,
--    `type`,
--    `file_group_id`,
--    `user_group_id`
--    ) SELECT
--        id,
--        account_id,
--        creation_date,
--        author,
--        company,
--        1,
--        fs_file_size,
--        generating_app,
--        1,
--        items_countl1,
--        items_countl2,
--        items_countl3,
--        keywords,
--        1,
--        state,
--        subject,
--        template,
--        title,
--        type,
--        file_group_id,
--        file_group_id
--    FROM docauthority.cla_files f WHERE id > 0;
CALL update_auto_increment('docauthority.cla_files', 'da_1_5.`content_metadata`');

-- cla_files -> ingestion_summary
TRUNCATE TABLE da_1_5.`ingestion_summary`;
CALL id_batch_query('docauthority.cla_files',
	'INSERT INTO da_1_5.`ingestion_summary` (	'
	'	`content_id`,	'
	'	`proposed_titles`	'
	'	) SELECT	'
	'		id,	'
	'		proposed_titles	'
	'	FROM docauthority.cla_files f 	');

-- INSERT INTO da_1_5.`ingestion_summary` (
--    `content_id`,
--    `proposed_titles`
--    ) SELECT
--        id,
--        proposed_titles
--    FROM docauthority.cla_files f WHERE id > 0;
CALL update_auto_increment('docauthority.cla_files', 'da_1_5.`ingestion_summary`');

-- copy selected fields for: root_folder	(small table - can be done directly)
TRUNCATE TABLE da_1_5.`root_folder`;
INSERT INTO da_1_5.`root_folder` (
    `id`,
    `base_depth`,
    `file_types`,
    `folders_count`,
    `media_type`,
    `path`,
    `path_descriptor_type`,
    `real_path`,
    `rescan_active`,
    `root_folder_state`,
    `store_location`,
    `store_purpose`,
    `store_rescan_method`,
    `store_security`,
    `doc_store_include`
    ) SELECT
        id,
        base_depth,
        'WORD,EXCEL,PDF,OTHER',
        folders_count,
        0,
        path,
        'WINDOWS',
        path,
        0,
        1,
        1,
        0,
        0,
        0,
        doc_store_include
    FROM docauthority.`root_folder` WHERE id>0;
CALL update_auto_increment('docauthority.`root_folder`', 'da_1_5.`root_folder`');

-- copy selected fields for: root_folder_schedule	(small table - can be done directly)
TRUNCATE TABLE da_1_5.`root_folder_schedule`;
INSERT INTO da_1_5.`root_folder_schedule` (
    `root_folder_id`,
    `schedule_group_id`
    ) SELECT
        id,
        1
    FROM docauthority.`root_folder` WHERE id>0;
UPDATE da_1_5.`root_folder_schedule` set number = id WHERE id>0;

-- saved_filters	(small table - can be done directly)
INSERT INTO da_1_5.`saved_filters` (
    `id`,
    `account_id`,
    `filter_json`,
    `name`,
    `owner_id`
	) SELECT
	    id,
	    1,
	    filter_json,
	    name,
	    owner_id
	FROM docauthority.`saved_filters` WHERE id>0;
CALL update_auto_increment('docauthority.`saved_filters`', 'da_1_5.`saved_filters`');

-- pv_analysis_data
CALL id_batch_query('docauthority.pv_analysis_data',
	'INSERT INTO da_1_5.`pv_analysis_data` (	'
	'	`id`,	'
	'	`analysis_meadata`,	'
	'   `analyze_hint`, '
	'	`content_id`,	'
	'	`pv_collections`	'
	'	) SELECT	'
	'		id,	'
	'		analysis_meadata,	'
	'       0,  '
	'		cla_file_id,	'
	'		pv_collections	'
	'	FROM docauthority.`pv_analysis_data` 	');

-- INSERT INTO da_1_5.`pv_analysis_data` (
--	`id`,
--	`analysis_meadata`,
--	`content_id`,
--	`pv_collections`
--	) SELECT
--		id,
--		analysis_meadata,
--		cla_file_id,
--		pv_collections
--	FROM docauthority.`pv_analysis_data` WHERE id>0;
CALL update_auto_increment('docauthority.`pv_analysis_data`', 'da_1_5.`pv_analysis_data`');


-- content_metadata_search_patterns_counting	(no id field - expect direct query to be sufficient)
INSERT INTO da_1_5.`content_metadata_search_patterns_counting` (
    `content_id`,
    `pattern_count`,
    `pattern_id`,
    `pattern_name`
	) SELECT
        cla_file_id,
        pattern_count,
        pattern_id,
        pattern_name
	FROM docauthority.`cla_file_search_patterns_counting` WHERE cla_file_id>0;

TRUNCATE TABLE `license_resource`;
INSERT INTO `license_resource`
    (`id`,`issue_time`,`signature`,`value`)
VALUES
    ('crawlerExpiry',0,'nanKTqHjcpRm7dRgNEMgEj2WHLg=','1498663749000')
    ,('loginExpiry',0,'z+pRATw49FghuLCv+ybFSu/O1gA=','1498663749000')
    ,('maxFiles',0,'tN06iw8DP3qSCIJIxYdJPen5n9w=','10000000')
    ,('maxRootFolders',0,'HC21Wa73UgsJ6a8tu+Ear7Q9Ejw=','25')
    ,('serverId',0,'qMvmfIIQRjX09bp4gP5ARNDxrUg=','6442eafb')
;

update da_1_5.`cla_file_tag_association` set dirty=1 where id > 0;
update da_1_5.`folder_tag_association` set solr_dirty=1 where id > 0;
update da_1_5.`doc_type_association` set dirty=1 where id > 0;

-- INSERT INTO da_1_5.`roles`
--    (`description`, `name`)
-- VALUES
--    ('Can operate functional api like scan ingest analyze etc','operator')
--    ,('Can activate crawker api','crawler_api')
--    ,('Demo showcase','demo-showcase')
-- ;
--
-- INSERT INTO da_1_5.`role_authorities`
--    (`role_id`, `authorities`)
-- VALUES
--    (3,4)
--    ,(3,6)
--    ,(4,7)
--    ,(5,4)
-- ;

INSERT INTO da_1_5.`users`
    (`name`, `password`,`role_name`,`username`,`account_id`)
VALUES
    ('scheduleUser', '*', 'schedule', 'scheduleInternalUser',1)
;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
