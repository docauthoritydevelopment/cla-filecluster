call drop_fk_for_table('cla_files_md','doc_folder_id');
call drop_fk_for_table('cla_files_md','user_group_id');
call drop_fk_for_table('cla_files_md','content_metadata_id');
call drop_fk_for_table('cla_files_md','file_group_id');

call drop_fk_for_table('cla_file_tag_association','cla_file_id');

call drop_fk_for_table('content_metadata','file_group_id');
call drop_fk_for_table('content_metadata','user_group_id');

call drop_fk_for_table('doc_folder','parent_folder_id');
call drop_fk_for_table('doc_folder','doc_store_id');
call drop_fk_for_table('doc_folder','root_folder_id');

call drop_fk_for_table('doc_type_association','cla_file_id');
call drop_fk_for_table('doc_type_association','origin_folder_id');
call drop_fk_for_table('doc_type_association','folder_id');


call drop_fk_for_table('err_processed_files','cla_file_id');
call drop_fk_for_table('err_scanned_files','doc_folder_id');

call drop_fk_for_table('file_groups','first_member_id');

call drop_fk_for_table('folder_tag_association','doc_folder_id');
call drop_fk_for_table('folder_tag_association','origin_folder_id');


INSERT INTO hibernate_sequences (sequence_name,next_val)
SELECT 'CAT_FILE', max(id)+501 from cla_files_md
 ON DUPLICATE KEY UPDATE next_val = GREATEST(next_val,(SELECT max(id)+501 from cla_files_md));

INSERT INTO hibernate_sequences (sequence_name,next_val)
SELECT 'CONTENT_METADATA', max(id)+501 from content_metadata
 ON DUPLICATE KEY UPDATE next_val = GREATEST(next_val,(SELECT max(id)+501 from content_metadata));

INSERT INTO hibernate_sequences (sequence_name,next_val)
SELECT 'DOC_FOLDER', max(id)+501 from doc_folder
 ON DUPLICATE KEY UPDATE next_val = GREATEST(next_val,(SELECT max(id)+501 from doc_folder));

INSERT INTO hibernate_sequences (sequence_name,next_val)
SELECT 'INGESTION_ERROR', ifnull(max(id),0)+501 from err_processed_files
 ON DUPLICATE KEY UPDATE next_val = GREATEST(ifnull(next_val,0),(SELECT ifnull(max(id),0)+501 from err_processed_files));

INSERT INTO hibernate_sequences (sequence_name,next_val)
SELECT 'MISSED_FILE_MATCH', ifnull(max(id),0)+501 from missed_file_match
 ON DUPLICATE KEY UPDATE next_val = GREATEST(ifnull(next_val,0),(SELECT ifnull(max(id),0)+501 from missed_file_match));