CREATE TABLE `scheduled_operations` (
	`id` BIGINT(20) NOT NULL,
	`create_time` BIGINT(20) NULL DEFAULT NULL,
	`data` JSON NULL DEFAULT NULL,
	`operation_type` VARCHAR(255) NOT NULL,
	`start_time` BIGINT(20) NULL DEFAULT NULL,
	`step` INT(11) NOT NULL,
	`is_user_operation` BIT(1) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
