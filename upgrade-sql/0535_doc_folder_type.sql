ALTER TABLE `doc_folder` ADD COLUMN `folder_type` INT(11) NULL DEFAULT NULL;

update `doc_folder` set folder_type = 1 where folder_type is null and name like '%.zip*';

update `doc_folder` set folder_type = 0 where folder_type is null;