CREATE TABLE `workflow` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NOT NULL,
	`date_modified` BIGINT(20) NOT NULL,
	`assignee` BIGINT(20) NULL DEFAULT NULL,
	`contact_info` VARCHAR(255) NULL DEFAULT NULL,
	`current_state_due_date` BIGINT(20) NULL DEFAULT NULL,
	`date_closed` BIGINT(20) NULL DEFAULT NULL,
	`date_last_state_change` BIGINT(20) NULL DEFAULT NULL,
	`due_date` BIGINT(20) NULL DEFAULT NULL,
	`filter` JSON NULL DEFAULT NULL,
	`priority` INT(11) NULL DEFAULT NULL,
	`request_details` JSON NULL DEFAULT NULL,
	`state` INT(11) NULL DEFAULT NULL,
	`workflow_type` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `workflow_history` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NOT NULL,
	`date_modified` BIGINT(20) NOT NULL,
	`action_type` INT(11) NULL DEFAULT NULL,
	`actor` VARCHAR(255) NULL DEFAULT NULL,
	`change_description` JSON NULL DEFAULT NULL,
	`comment` VARCHAR(1000) NULL DEFAULT NULL,
	`workflow_id` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;