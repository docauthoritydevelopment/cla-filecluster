REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
REM --- if NOT [%1] == [do-convert] goto usage

setlocal EnableDelayedExpansion
set UPGRADE_TOOL=call filecluster\filecluster-upgrade-schema.bat
set SOLR_ADDRESS=localhost:2181/solr
pushd ..

mkdir filecluster\tempDir\0690_conf
xcopy /y /s solr\CatFolders filecluster\tempDir\0690_conf
copy /y filecluster\upgrade-sql\tmp_file_0690_managed-schema_CF.txt filecluster\tempDir\0690_conf\conf\managed-schema
%UPGRADE_TOOL% solr %SOLR_ADDRESS% zk-upconfig filecluster\tempDir\0690_conf\conf CatFolders_tmp
call zookeeper\bin\zkCli.cmd -server localhost:2181 set /solr/collections/CatFolders {"configName":"CatFolders_tmp"}
timeout /T 10 /NOBREAK
call zookeeper\bin\zkCli.cmd -server localhost:2181 get /solr/collections/CatFolders
%UPGRADE_TOOL% solr %SOLR_ADDRESS% reload-collection CatFolders
rmdir /s /q filecluster\tempDir\0690_conf

SET NUM_SHARDS=1
for /f "tokens=1,2" %%a in (num-shards.txt) do (
	if "X%%b" == "XLegacy" (
		SET NUM_SHARDS=%%a
	)
	if "X%%b" == "X" (
		SET NUM_SHARDS=%%a
	)
)
ECHO NUM_SHARDS=%NUM_SHARDS%
set UPGRADE_TOOL=call filecluster\filecluster-upgrade-schema.bat
set SOLR_ADDRESS=localhost:2181/solr
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed CatFolders_2 CatFolders %NUM_SHARDS%

popd
exit /B 0
