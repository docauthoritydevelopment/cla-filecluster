ALTER TABLE `err_scanned_files` ADD COLUMN `root_folder_id` BIGINT NULL;
ALTER TABLE `err_scanned_files` ADD COLUMN `date_created` BIGINT NULL;
ALTER TABLE `err_processed_files` ADD COLUMN `date_created` BIGINT NULL;

update `err_scanned_files`  set `date_created` = UNIX_TIMESTAMP(created_ondb)*1000;
update `err_processed_files`  set `date_created` = UNIX_TIMESTAMP(created_ondb)*1000;