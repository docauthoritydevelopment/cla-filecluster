REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
REM --- if NOT [%1] == [do-convert] goto usage

pushd ..
SET LOGF=installation_logs\%0.log
SET CONF_DIR=.\filecluster\config
SET NEW_CONF_DIR=.\filecluster\config_new
SET PROP_FILE=%CONF_DIR%\application.properties
IF NOT EXIST "%CONF_DIR%" (
    Echo Config dir %CONF_DIR% doesnot exist >> %LOGF%
    exit 1
)
IF NOT EXIST "%NEW_CONF_DIR%" (
    Echo New config dir %NEW_CONF_DIR% doesnot exist >> %LOGF%
)

echo.>> %PROP_FILE%
echo spring.jpa.properties.hibernate.current_session_context_class=org.springframework.orm.hibernate5.SpringSessionContext>> %PROP_FILE%
echo.>> %PROP_FILE%

REM Echo Added spring property to %PROP_FILE%>> %LOGF%
popd

goto finally

:usage
echo Usage:
echo        %0 do-reset
goto finally

:finally
exit /b 0
