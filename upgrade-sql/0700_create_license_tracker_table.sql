CREATE TABLE `license_tracker` (
  `id` varchar(255) NOT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `updated_time` bigint(20) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

