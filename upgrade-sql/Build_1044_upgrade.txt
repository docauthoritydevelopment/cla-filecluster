
mngAgent app.props:
==================
tailer.fatal.regex=OutOfMemoryError,ClusterState says we are the leader,WATCHDOG_RESTART

mediaProcessor app.props:
========================
# max number of threads for concurrent mapping - zero imlplies no concurrency (backward compatible)
scan.concurrency.max-pool-size=0
scan.concurrency.parallelism=10
scan.responses.idle-threshold-sec=1200
