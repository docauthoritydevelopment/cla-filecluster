ALTER TABLE `scheduled_operations`
	ADD COLUMN 	`state` VARCHAR(255) NULL DEFAULT NULL;

ALTER TABLE `scheduled_operations`
	ADD COLUMN `update_time` BIGINT(20) NULL DEFAULT NULL;

ALTER TABLE `scheduled_operations`
	ADD COLUMN `should_display` BIT(1) NULL DEFAULT NULL;