DROP TABLE IF EXISTS `doc_type_file_tags`;
CREATE TABLE IF NOT EXISTS `file_tag_settings` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`deleted` BIT(1) NOT NULL,
	`dirty` BIT(1) NOT NULL,
	`doc_type_id` BIGINT(20) NULL DEFAULT NULL,
	`file_tag_id` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `fts_doc_type_id_idx` (`doc_type_id`),
	INDEX `fts_file_tag_id_idx` (`file_tag_id`),
	CONSTRAINT `fts_file_tag_id_fk` FOREIGN KEY (`file_tag_id`) REFERENCES `file_tag` (`id`),
	CONSTRAINT `fts_doc_type_id_fk` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
