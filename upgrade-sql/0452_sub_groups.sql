alter table file_groups add column `has_subgroups` BIT(1) NOT NULL;
alter table file_groups add column `raw_analysis_group_id` CHAR(36) NULL DEFAULT NULL;

CREATE TABLE `refinement_rules` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NOT NULL,
	`date_modified` BIGINT(20) NOT NULL,
	`active` BIT(1) NOT NULL,
	`body` VARCHAR(2048) NULL DEFAULT NULL,
	`deleted` BIT(1) NOT NULL,
	`dirty` BIT(1) NOT NULL,
	`priority` INT(11) NOT NULL,
	`raw_analysis_group_id` CHAR(36) NULL DEFAULT NULL,
	`subgroup_id` CHAR(36) NULL DEFAULT NULL,
	`subgroup_name` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
