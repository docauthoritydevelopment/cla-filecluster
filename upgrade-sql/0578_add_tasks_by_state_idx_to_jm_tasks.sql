select if (
    exists(
        select distinct index_name from information_schema.statistics
        where table_schema = DATABASE()
        and table_name = 'jm_tasks' and index_name like 'tasks_by_state_idx'
    )
    ,'select ''index tasks_by_state_idx exists'' _______;'
    ,'create index tasks_by_state_idx on jm_tasks(`deleted`, `type`, `run_context`, `state`, `job_id`)') into @a;
PREPARE stmt1 FROM @a;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

