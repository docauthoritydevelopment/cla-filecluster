ALTER TABLE `scheduled_operations`
	ADD COLUMN `owner_id` BIGINT(20) NULL;

ALTER TABLE `scheduled_operations`
	ADD COLUMN `description` VARCHAR(255) NULL DEFAULT NULL ;