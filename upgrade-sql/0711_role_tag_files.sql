INSERT INTO `roles` (`id`,`description`,`display_name`,`name`) VALUES
     (44,'Assign tags to files via filter in batch mode','Assign tags to files in batch mode','AssignTagFilesBatch')
;


INSERT INTO `role_authorities` VALUES
   (44,43)
   ;

insert into template_roles
select template_id, 44 from template_roles where role_id in (
select id from roles where name='AssignTag');