alter table scheduled_operations add column `priority` INT(11) NULL DEFAULT NULL;
update scheduled_operations set `priority`=1 where `priority` is null;