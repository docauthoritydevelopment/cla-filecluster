-- Capture unrelated fix of the baseline script
CREATE INDEX `name_idx` ON `doc_folder` (name(255));

-- Create new tables
DROP TABLE IF EXISTS `comp_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comp_role` (
  `type` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `manager_username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_idx` (`name`),
  UNIQUE KEY `displayname_idx` (`display_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `roles_comp_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_comp_roles` (
  `comp_role_id` bigint(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`comp_role_id`,`role_id`),
  KEY `FK9vuiiuhpslacxudkfng4mcth3` (`role_id`),
  CONSTRAINT `FK96gnlemjcwj6xavwb0ww6nwjo` FOREIGN KEY (`comp_role_id`) REFERENCES `comp_role` (`id`),
  CONSTRAINT `FK9vuiiuhpslacxudkfng4mcth3` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `users_comp_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_comp_roles` (
  `user_id` bigint(20) NOT NULL,
  `comp_role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`comp_role_id`),
  KEY `FKd8x5rvmy91vb2nplccshdqf9d` (`comp_role_id`),
  CONSTRAINT `FK5amnqnuab4c7djiwyva0p9krp` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FKd8x5rvmy91vb2nplccshdqf9d` FOREIGN KEY (`comp_role_id`) REFERENCES `comp_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Fix constrains and references from other tables
ALTER TABLE `biz_role_roles` DROP FOREIGN KEY `FKnaneo07gjespvuxdgi0qtfmr5`;
ALTER TABLE `business_id_tag_association` DROP FOREIGN KEY `FKq91qvgomib0cbcdt40m1xg57x`;
ALTER TABLE `business_id_tag_association` ADD KEY `FK3uvgjo52aloeld11q9oy04wpn` (`functional_role_id`);
ALTER TABLE `business_id_tag_association` ADD CONSTRAINT `FK3uvgjo52aloeld11q9oy04wpn` FOREIGN KEY (`functional_role_id`) REFERENCES `comp_role` (`id`);

ALTER TABLE `folder_tag_association` DROP FOREIGN KEY `FKdakq7otltelswrj2hw5e68555`;
ALTER TABLE `folder_tag_association` ADD KEY `FKdyb4cvpt3lnwl5iuxp5xoqnka` (`functional_role_id`);
ALTER TABLE `folder_tag_association` ADD CONSTRAINT `FKdyb4cvpt3lnwl5iuxp5xoqnka` FOREIGN KEY (`functional_role_id`) REFERENCES `comp_role` (`id`);

ALTER TABLE `ldap_group_mapping_da_roles` DROP FOREIGN KEY `FKias2i8tv04w8g0du0ytevu0vc`;
ALTER TABLE `ldap_group_mapping_da_roles` ADD KEY `FKmwfa3are7jtnoal5uq58lornd` (`da_roles_id`);
ALTER TABLE `ldap_group_mapping_da_roles` ADD CONSTRAINT `FKmwfa3are7jtnoal5uq58lornd` FOREIGN KEY (`da_roles_id`) REFERENCES `comp_role` (`id`);

-- Populate new tables
DELETE FROM `comp_role` WHERE `id` > 0;
LOCK TABLES `comp_role` WRITE;
/*!40000 ALTER TABLE `comp_role` DISABLE KEYS */;
INSERT INTO `comp_role` VALUES
    ('BIZ_ROLE',1,'\0','Administrator','Administrator','admin',NULL)
   ,('BIZ_ROLE',2,'\0','Operator','Operator','operator',NULL)
   ,('BIZ_ROLE',3,'\0','Viewer','Viewer','viewer',NULL)
   ,('BIZ_ROLE',4,'\0','Security Manager','Security Manager','security_manager',NULL)
   ,('BIZ_ROLE',5,'\0','Data Manager','Data Manager','data_manager',NULL)
   ,('BIZ_ROLE',6,'\0','Department Trustee','Department Trustee','department_trustee',NULL)
;
/*!40000 ALTER TABLE `comp_role` ENABLE KEYS */;
UNLOCK TABLES;

DELETE FROM `roles_comp_roles` WHERE `role_id` > 0;
DELETE FROM `role_authorities` WHERE `role_id` > 0;
DELETE FROM `users_roles` WHERE `user_id` > 0;
DELETE FROM `roles` WHERE `id` > 0;
LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES
    (1,'Manage root folders and scan configuration','Manage scan configuration','MngScanConfig')
   ,(2,'Run scans','Run scans','RunScans')
   ,(3,'Manage system users','Manage users','MngRoles')
   ,(4,'Manage system roles','Manage roles','MngUsers')
   ,(5,'View users configuration','View users','ViewUsers')
   ,(6,'Assign roles to users','Assign roles','MngUserRoles')
   ,(7,'View audit trail','View audit','ViewAuditTrail')
   ,(8,'General system administration','General administration','GeneralAdmin'),
   (9,'Manage tags configuration','Manage tags','MngTagConfig')
   ,(10,'Assign tags','Assign tags','AssignTag')
   ,(11,'Assign security tags','Assign security tags','AssignSecurityTag')
   ,(12,'View tag types','View tag types','ViewTagTypes')
   ,(13,'View tag associations','View assigned tags','ViewTagAssociation')
   ,(14,'View security tag associations','View security tags','ViewSecurityTagAssociation')
   ,(15,'Configure DocTypes','Configure document types','MngDocTypeConfig')
   ,(16,'Assign DocTypes','Assign DocTypes','AssignDocType')
   ,(17,'View DocTypes hierarchy','View DocTypes tree','ViewDocTypeTree')
   ,(18,'View DocTypes associations','View assigned DocTypes','ViewDocTypeAssociation')
   ,(19,'Assign functional role','Assign functional role','AssignFunctionalRole')
   ,(20,'View functional role associations','View assigned functional role','ViewFuncRoleAssociation')
   ,(21,'Manage business lists','Manage business lists','MngBizLists')
   ,(22,'Run actions','Run actions','RunActions')
   ,(23,'View all files content','View files content','ViewContent')
   ,(24,'View all files','View files','ViewFiles')
   ,(25,'View permitted content','View permitted content','ViewPermittedContent')
   ,(26,'View permitted files','View permitted files','ViewPermittedFiles')
   ,(27,'View business list associations','View assigned list items','ViewBizListAssociation')
   ,(28,'Assign business list item','Assign list item','AssignBizList')
   ,(29,'Manage group names and content','Manage groups','MngGroups')
   ,(30,'View saved reports','View reports','ViewReports')
   ,(31,'Manage saved reports','Manage reports','MngReports')
   ,(32,'Technical support','Tech support','TechSupport')
   ;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

-- INSERT INTO users_roles (user_id, roles_id) SELECT u.id, r.id from users u left join roles r on true;

LOCK TABLES `roles_comp_roles` WRITE;
/*!40000 ALTER TABLE `roles_comp_roles` DISABLE KEYS */;
INSERT INTO `roles_comp_roles` VALUES
    (1,1),(2,1),(1,2),(2,2),(1,3),(1,4),(1,5),(1,6),(1,7)
    ,(1,8),(1,9),(4,9),(5,9),(1,10),(4,10),(6,10),(1,11)
    ,(4,11),(6,11),(1,12),(4,12),(6,12),(1,13),(4,13),(6,13)
    ,(1,14),(4,14),(6,14),(1,15),(4,15),(5,15),(1,16),(5,16)
    ,(6,16),(1,17),(5,17),(6,17),(1,18),(5,18),(6,18),(1,19)
    ,(4,19),(6,19),(1,20),(4,20),(6,20),(1,21),(5,21),(1,22)
    ,(6,22),(1,23),(1,24),(1,25),(3,25),(1,26),(3,26)
    ,(1,27),(4,27),(5,27),(6,27),(1,28),(4,28),(5,28)
    ,(6,28),(1,29),(5,29),(6,29),(1,30),(4,30),(5,30)
    ,(1,31),(4,31),(5,31),(1,32)
   ;
/*!40000 ALTER TABLE `roles_comp_roles` ENABLE KEYS */;
UNLOCK TABLES;

DELETE FROM `users_comp_roles` WHERE `user_id` > 0;
INSERT INTO `users_comp_roles` (user_id, comp_role_id) SELECT u.id, r.id from users u left join `comp_role` r on true;

LOCK TABLES `role_authorities` WRITE;
/*!40000 ALTER TABLE `role_authorities` DISABLE KEYS */;
INSERT INTO `role_authorities` VALUES
    (1,0),(2,1),(3,2),(4,3),(5,4),(6,5),(7,6),(8,7),(9,8),(10,9),(11,10)
   ,(12,11),(13,12),(14,13),(15,14),(16,15),(17,16),(18,17),(19,18)
   ,(20,19),(21,20),(22,21),(23,22),(24,23),(25,24),(26,25)
   ,(27,26),(28,27),(29,28),(30,29),(31,30),(32,31)
   ;
/*!40000 ALTER TABLE `role_authorities` ENABLE KEYS */;
UNLOCK TABLES;
