alter table mon_components CHANGE COLUMN `created_ondb` `created_ondb` BIGINT(20) NULL DEFAULT NULL;
alter table mon_components CHANGE COLUMN `last_response_date` `last_response_date` BIGINT(20) NULL DEFAULT NULL;
alter table mon_components CHANGE COLUMN `state_change_date` `state_change_date` BIGINT(20) NULL DEFAULT NULL;

update mon_components set created_ondb = UNIX_TIMESTAMP()*1000 where created_ondb > UNIX_TIMESTAMP()*1000;
update mon_components set last_response_date = UNIX_TIMESTAMP()*1000 where last_response_date > UNIX_TIMESTAMP()*1000;
update mon_components set state_change_date = UNIX_TIMESTAMP()*1000;