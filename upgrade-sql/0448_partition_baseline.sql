DROP FUNCTION IF EXISTS get_last_partition_name;
DROP FUNCTION IF EXISTS get_last_partition_date;
DROP FUNCTION IF EXISTS get_first_partition_name;
DROP FUNCTION IF EXISTS get_first_partition_date;

DELIMITER $$

-- create stored procedures to get last partition data
CREATE FUNCTION get_last_partition_name(table_name_str varchar(50)) RETURNS varchar(50)
BEGIN

SELECT partition_name
into @last_partition_name
from information_schema.PARTITIONS
where table_schema = database() and table_name = table_name_str
and partition_ordinal_position = (
select max(partition_ordinal_position)
from information_schema.PARTITIONS
where table_schema = database() and table_name = table_name_str);

RETURN (@last_partition_name);

END $$

CREATE FUNCTION get_last_partition_date(table_name_str varchar(50)) RETURNS BIGINT(20)
BEGIN

select partition_description
into @last_partition_date
from information_schema.PARTITIONS
where table_schema = database() and table_name = table_name_str
and partition_ordinal_position = (
select max(partition_ordinal_position)
from information_schema.PARTITIONS
where table_schema = database() and table_name = table_name_str);

RETURN (@last_partition_date);

END $$

-- create stored procedures to get first partition data

CREATE FUNCTION get_first_partition_name(table_name_str varchar(50)) RETURNS varchar(50)
BEGIN

SELECT partition_name
into @fst_partition_name
from information_schema.PARTITIONS
where table_schema = database() and table_name = table_name_str
and partition_ordinal_position = 1;

RETURN (@fst_partition_name);

END $$

CREATE FUNCTION get_first_partition_date(table_name_str varchar(50)) RETURNS BIGINT(20)
BEGIN

select partition_description
into @fst_partition_date
from information_schema.PARTITIONS
where table_schema = database() and table_name = table_name_str
and partition_ordinal_position = 1;

RETURN (@fst_partition_date);

END $$

DELIMITER ;

