INSERT INTO `roles` VALUES
     (38,'Manage search patterns','Manage search patterns','ManageSearchPatterns')
   ,(39,'Activate Regulations','Activate regulations','ActivateRegulations')
   ,(40,'Activate search pattern categories','Activate search pattern categories','ActivatePatternCategories')
;


INSERT INTO `role_authorities` VALUES
   (38,37),(39,38),(40,39)
   ;

insert into template_roles
select template_id, 38 from comp_role where name = 'admin';

insert into template_roles
select template_id, 38 from comp_role where name = 'tech_support';

insert into template_roles
select template_id, 39 from comp_role where name = 'admin';

insert into template_roles
select template_id, 39 from comp_role where name = 'tech_support';

insert into template_roles
select template_id, 39 from comp_role where name = 'security_manager';

insert into template_roles
select template_id, 39 from comp_role where name = 'data_manager';

insert into template_roles
select template_id, 40 from comp_role where name = 'admin';

insert into template_roles
select template_id, 40 from comp_role where name = 'tech_support';

insert into template_roles
select template_id, 40 from comp_role where name = 'security_manager';

insert into template_roles
select template_id, 40 from comp_role where name = 'data_manager';
