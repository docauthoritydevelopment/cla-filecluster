-- Convert path related fields to support 4 bytes UTF8 (extension characters)

ALTER TABLE `cla_files_md` CHANGE `base_name` `base_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `cla_files_md` CHANGE `extension` `extension` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `cla_files_md` CHANGE `media_entity_id` `media_entity_id` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `cla_files_md` CHANGE `full_path` `full_path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;

ALTER TABLE `root_folder` CHANGE `media_entity_id` `media_entity_id` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `root_folder` CHANGE `path` `path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `root_folder` CHANGE `real_path` `real_path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;

ALTER TABLE `doc_folder` CHANGE `media_entity_id` `media_entity_id` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `doc_folder` CHANGE `name` `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `doc_folder` CHANGE `path` `path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;
ALTER TABLE `doc_folder` CHANGE `real_path` `real_path` varchar(2048) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL;


DROP TABLE IF EXISTS crawl_temp_file;

DROP TABLE IF EXISTS crawl_temp_folder;

DROP TABLE IF EXISTS crawl_folder_change_log;

DROP TABLE IF EXISTS crawl_file_change_log;
