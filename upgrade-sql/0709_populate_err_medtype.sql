update err_scanned_files se set se.media_type =
(select media_type from root_folder where id = se.root_folder_id)
where se.root_folder_id > 0 and se.media_type is null;

update err_processed_files se set se.media_type =
(select media_type from root_folder where id = se.root_folder_id)
where se.root_folder_id > 0 and se.media_type is null;