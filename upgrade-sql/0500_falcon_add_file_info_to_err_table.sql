ALTER TABLE `err_processed_files`
ADD COLUMN `full_path` VARCHAR(2048) NULL DEFAULT NULL AFTER `cla_file_id`,
ADD COLUMN `content_id` bigint(20) NULL DEFAULT NULL AFTER `full_path`,
ADD COLUMN `folder_id` bigint(20) NULL DEFAULT NULL AFTER `full_path`,
ADD COLUMN `root_folder_id` bigint(20) NULL DEFAULT NULL AFTER `full_path`,
ADD COLUMN `file_type` INT(11) NULL DEFAULT NULL AFTER `full_path`;

UPDATE err_processed_files err, cla_files_md f
SET err.full_path = f.full_path, err.file_type = f.type, err.content_id = f.content_id, err.folder_id = f.folder_id, err.root_folder_id = root_folder_id
WHERE err.cla_file_id=f.id;