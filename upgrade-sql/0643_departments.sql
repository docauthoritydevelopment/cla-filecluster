CREATE TABLE `department` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`deleted` BIT(1) NOT NULL,
	`name` VARCHAR(255) NOT NULL,
	`unique_name` VARCHAR(255) NOT NULL,
	`description` VARCHAR(255) NULL,
	`parent_id` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FKmgsnnmudxrwqidn4f64q8rp4o` (`parent_id`),
	CONSTRAINT `FKmgsnnmudxrwqidn4f64q8rp4o` FOREIGN KEY (`parent_id`) REFERENCES `department` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


alter table tag_association ADD COLUMN `department_id` BIGINT(20) NULL DEFAULT NULL;

CREATE INDEX `department_idx` ON `tag_association` (department_id);

ALTER TABLE `tag_association` ADD CONSTRAINT FK_department_idx FOREIGN KEY (department_id) REFERENCES department(id);

ALTER TABLE root_folder ADD COLUMN `dirty` BIT(1) NOT NULL;
ALTER TABLE root_folder ADD COLUMN `department_id` BIGINT(20) NULL DEFAULT NULL;

INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (1,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Sales & Marketing', 'Sales & Marketing78', NULL);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (2,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Proposals & Contracts', 'Proposals & Contracts79', 1);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (3,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Newsletters & Invitations', 'Newsletters & Invitations80', 1);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (4,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Customer Satisfaction Surveys', 'Customer Satisfaction Surveys81', 1);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (5,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Tenders', 'Tenders82', 1);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (6,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Operations', 'Operations83', NULL);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (7,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Letters & Faxes', 'Letters & Faxes84', 6);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (8,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Finance & Accounting', 'Finance & Accounting85', NULL);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (9,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Bank Operations and Transfers', 'Bank Operations and Transfers86', 8);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (10,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Collection', 'Collection87', 8);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (11,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Budgets & Plans', 'Budgets & Plans88', 8);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (12,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Financial Reports', 'Financial Reports89', 8);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (13,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'HR', 'HR90', NULL);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (14,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Employment', 'Employment91', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (15,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Hiring', 'Hiring92', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (16,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Awards Review and Processing', 'Awards Review and Processing93', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (17,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Benefits', 'Benefits94', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (18,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Salary Determination', 'Salary Determination95', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (19,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Promotions', 'Promotions96', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (20,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Reassignments', 'Reassignments97', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (21,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Policy Development', 'Policy Development98', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (22,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Work Permits, Immigration & Visa', 'Work Permits, Immigration & Visa99', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (23,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Position Classification and Grading', 'Position Classification and Grading100', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (24,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Compensation', 'Compensation101', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (25,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Job Analysis', 'Job Analysis102', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (26,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Career Planning', 'Career Planning103', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (27,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Labour Relations', 'Labour Relations104', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (28,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Labor Law Compliance', 'Labor Law Compliance105', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (29,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Salary & Payroll', 'Salary & Payroll106', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (30,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Salary lists', 'Salary lists107', 13);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (31,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Legal', 'Legal108', NULL);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (32,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Confidentiality Agreements', 'Confidentiality Agreements109', 31);
INSERT INTO `department` (`id`,`date_created`,`date_modified`,`deleted`,`name`,`unique_name`,`parent_id`) VALUES (33,  UNIX_TIMESTAMP(now())*1000,  UNIX_TIMESTAMP(now())*1000, b'0', 'Service Contracts', 'Service Contracts110', 31);
