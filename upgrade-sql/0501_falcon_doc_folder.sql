alter table folder_tag_association add column origin_depth_from_root INT(11);

alter table doc_type_association add column origin_depth_from_root INT(11);

call drop_column_with_fk_and_index('doc_folder','doc_store_id');

alter table doc_folder add column `path_descriptor_type` VARCHAR(255) NULL DEFAULT NULL;

update doc_type_association a
left join doc_folder d on d.id = a.origin_folder_id
set origin_depth_from_root = d.depth_from_root
where origin_folder_id > 0;

update folder_tag_association a
left join doc_folder d on d.id = a.origin_folder_id
set origin_depth_from_root = d.depth_from_root
where origin_folder_id > 0;