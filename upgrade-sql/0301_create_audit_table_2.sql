-- Re-Create audit table for logback to persist user events.
DROP TABLE IF NOT EXISTS audit ;
CREATE TABLE `audit` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) DEFAULT NULL,
  `message` varchar(1024) DEFAULT NULL,
  `object_id` varchar(255) DEFAULT NULL,
  `object_type` varchar(255) DEFAULT NULL,
  `sub_system` varchar(255) DEFAULT NULL,
  `time_stamp` bigint(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`, `time_stamp`),
  KEY `sub_system_idx` (`sub_system`),
  KEY `username_idx` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
