ALTER TABLE `err_processed_files`
	ADD COLUMN `media_type` INT(11) NULL;

ALTER TABLE `err_processed_files`
    ADD COLUMN `extension` varchar(20) NULL;

ALTER TABLE `err_scanned_files`
	ADD COLUMN `media_type` INT(11) NULL;
