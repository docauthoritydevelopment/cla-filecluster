-- Note: if you get Error Code: 1418 try running SET GLOBAL log_bin_trust_function_creators = 1;

DROP TABLE IF EXISTS `_schema_upgrade_log`;
CREATE TABLE `_schema_upgrade_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_stamp` DATETIME(3) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `message` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP PROCEDURE IF EXISTS `CreateMissingIndex`;
DELIMITER $$

CREATE PROCEDURE `CreateMissingIndex` (
    tbl_name  VARCHAR(64),
    inx_name  VARCHAR(64),
    inx_cols  VARCHAR(128)
)
BEGIN
    DECLARE IndexIsThere INTEGER;

    SELECT COUNT(1) INTO IndexIsThere FROM (
        SELECT S.TABLE_NAME,S.INDEX_NAME,group_concat(S.COLUMN_NAME ORDER BY S.TABLE_NAME ASC,S.SEQ_IN_INDEX ASC) cols
        FROM INFORMATION_SCHEMA.STATISTICS S WHERE S.TABLE_SCHEMA=database() AND S.TABLE_NAME=tbl_name
        GROUP BY S.TABLE_NAME,S.INDEX_NAME
    ) inx_info WHERE inx_info.cols = inx_cols;

    IF IndexIsThere = 0 THEN
        SET @sqlstmt = CONCAT('ALTER TABLE ',tbl_name ,' ADD INDEX ', inx_name, ' (', inx_cols, ') ');
        PREPARE st FROM @sqlstmt;
        EXECUTE st;
        DEALLOCATE PREPARE st;
        INSERT INTO `_schema_upgrade_log` (`time_stamp`, `status`, `message`)
            SELECT now(3),2,CONCAT('+++ Created index ', inx_name, ' with columns ', inx_cols,' on table ', tbl_name,'.') Info_Msg;
    ELSE
        INSERT INTO `_schema_upgrade_log` (`time_stamp`, `status`, `message`)
            SELECT now(3),1,CONCAT('   === Index with columns ', inx_cols,' already exists on table ', tbl_name,'.') Info_Msg;
    END IF;

END $$

DELIMITER ;

-- Using the procedure:
--      CALL CreateMissingIndex('tbl1', 'inx1', 'cols');

-- Run the following on the reference DB to create the update script:
--
-- SELECT
-- 	concat('CALL CreateMissingIndex(''',S.TABLE_NAME,''',''',S.INDEX_NAME,''',''',group_concat(S.COLUMN_NAME ORDER BY S.TABLE_NAME ASC, S.SEQ_IN_INDEX ASC),''');') statements
-- FROM INFORMATION_SCHEMA.STATISTICS S
-- WHERE S.TABLE_SCHEMA=database() AND S.INDEX_NAME <> 'PRIMARY'
-- GROUP BY S.TABLE_NAME,S.INDEX_NAME ;
CALL CreateMissingIndex('aclcategory_filter_acl','FKc2ntbf35rg9n9vo5wsttp2cf5','aclcategory_filter_id');
CALL CreateMissingIndex('action_execution_task_status','IDXi0lpsaaddfqloeoq4a92xqmeh','trigger_time');
CALL CreateMissingIndex('action_instance','FKfue3443s8a0t2b8akanr6bxaj','action_id');
CALL CreateMissingIndex('action_parameter_definition','FK91o57hs57yv864blc9vgxg8ov','action_definition_id');
CALL CreateMissingIndex('act_file_action_desired','FKn4tgjuo4p1vmwy5uvhgu1ubvn','action_definition_id');
CALL CreateMissingIndex('act_file_action_desired','IDX3ctwjwu7d64dqn75mbfbpwm1d','cla_file_id');
CALL CreateMissingIndex('act_file_action_desired','IDXfakh4awiwi5nqs2nsjgtw7rrg','action_class');
CALL CreateMissingIndex('act_file_action_desired','IDXivwafeh097g1pgx34iokqleve','action_trigger_id');
CALL CreateMissingIndex('audit','sub_system_idx','sub_system');
CALL CreateMissingIndex('audit','username_idx','username');
CALL CreateMissingIndex('biz_list','name','name');
CALL CreateMissingIndex('biz_list','name_idx','name');
CALL CreateMissingIndex('biz_list_item_extraction','bl_e_bli','biz_list_id');
CALL CreateMissingIndex('biz_list_item_extraction','bl_e_dirty','dirty');
CALL CreateMissingIndex('biz_list_item_extraction','bl_e_dirty_bli','dirty,biz_list_id');
CALL CreateMissingIndex('biz_list_item_extraction','bl_e_dirty_f','dirty,cla_file_id');
CALL CreateMissingIndex('biz_list_item_extraction','bl_e_fid','cla_file_id');
CALL CreateMissingIndex('bl_item_alias','FK1o721aofhsq2ihfp5dxs9noif','entity_sid');
CALL CreateMissingIndex('bl_item_simple','entity_id_idx','entity_id');
CALL CreateMissingIndex('bl_item_simple','FK6tw85lx7d0llgrfxwn5tftl7p','biz_list_id');
CALL CreateMissingIndex('bl_item_simple','name_idx','name');
CALL CreateMissingIndex('bl_item_simple','state_idx','state');
CALL CreateMissingIndex('business_id_tag_association','association_time_stamp_idx','association_time_stamp');
CALL CreateMissingIndex('business_id_tag_association','deleted_idx','deleted');
CALL CreateMissingIndex('business_id_tag_association','FK3uvgjo52aloeld11q9oy04wpn','functional_role_id');
CALL CreateMissingIndex('business_id_tag_association','FK67sx1u2gseqoi0he7krq53lto','simple_biz_list_item_sid');
CALL CreateMissingIndex('business_id_tag_association','FKbe9utsuhastoqjk9r49yll9ml','file_tag_id');
CALL CreateMissingIndex('business_id_tag_association','FKfh0gdao0ndnvg5bmpyg3w2ic4','file_group_id');
CALL CreateMissingIndex('business_id_tag_association','FKivqglorjq2u7mkrbfkov6yjro','doc_type_id');
CALL CreateMissingIndex('business_id_tag_association','solr_dirty_idx','solr_dirty');
CALL CreateMissingIndex('category_filters','FKt2ldhdfxgrock8jc645cwgidd','category_id');
CALL CreateMissingIndex('cat_fil_val','FK7gobc5hmda0vbn59ax5548gll','cat_filter');
CALL CreateMissingIndex('cla_files_md','acctRfTypeState_idx','root_folder_id,type,state,deleted');
CALL CreateMissingIndex('cla_files_md','acctTypeState_idx','type,state,deleted');
CALL CreateMissingIndex('cla_files_md','acct_content_metadata_idx','content_metadata_id');
CALL CreateMissingIndex('cla_files_md','acct_dirty_id_idx','dirty_wip,dirty,deleted');
CALL CreateMissingIndex('cla_files_md','content_metadata_idx','content_metadata_id');
CALL CreateMissingIndex('cla_files_md','deleted_idx','deleted');
CALL CreateMissingIndex('cla_files_md','dirty_idx','dirty_wip,dirty,deleted');
CALL CreateMissingIndex('cla_files_md','fileGroup_idx','file_group_id');
CALL CreateMissingIndex('cla_files_md','fileNameHashed_idx','file_name_hashed');

-- CALL CreateMissingIndex('cla_files_md','FKe7k1h47eloyfbt5q0ndxk6nc8','doc_folder_id');
CALL CreateMissingIndex('cla_files_md','docFolderId_deleted_idx','doc_folder_id,deleted');

CALL CreateMissingIndex('cla_files_md','FK_cla_files_md_package_top_file_id','package_top_file_id');
CALL CreateMissingIndex('cla_files_md','rf_acl_sig_idx','root_folder_id,acl_signature');
CALL CreateMissingIndex('cla_files_md','rf_deleted_id_idx','root_folder_id,deleted,id');
CALL CreateMissingIndex('cla_files_md','rf_deleted_pkg_id_idx','root_folder_id,package_top_file_id,deleted,id');
CALL CreateMissingIndex('cla_files_md','rf_fileNameHashed_idx','root_folder_id,file_name_hashed');
CALL CreateMissingIndex('cla_files_md','rf_fNHashed_lm_idx','root_folder_id,file_name_hashed,fs_last_modified');
CALL CreateMissingIndex('cla_files_md','rf_fNHashed_lm_ow_acl_idx','root_folder_id,file_name_hashed,fs_last_modified,owner,acl_signature');
CALL CreateMissingIndex('cla_files_md','rf_media_itm_idx','root_folder_id,media_entity_id');
CALL CreateMissingIndex('cla_files_md','rootFolder_idx','root_folder_id');
CALL CreateMissingIndex('cla_files_md','state_del_idx','state,deleted');
CALL CreateMissingIndex('cla_files_md','state_idx','state,dirty');
CALL CreateMissingIndex('cla_files_md','type_del_idx','type,deleted');
CALL CreateMissingIndex('cla_files_md','userGroup_idx','user_group_id');
CALL CreateMissingIndex('cla_file_tag_association','association_time_stamp_idx','association_time_stamp');
CALL CreateMissingIndex('cla_file_tag_association','dirty_idx','dirty');
CALL CreateMissingIndex('cla_file_tag_association','FK53dx58sfbqvns7oliytk4igcn','cla_file_id');
CALL CreateMissingIndex('cla_file_tag_association','FK80vki2irjat1v4nkh7g0scqqj','functional_role_id');
CALL CreateMissingIndex('cla_file_tag_association','FKa0yeqeqc3iqqdc1uul20wh1lo','doc_type_id');
CALL CreateMissingIndex('cla_file_tag_association','FKgs7tbfajafisrqdi3uxg3ttnf','file_tag_id');
CALL CreateMissingIndex('cla_file_tag_association','FKpk1k16nspm5ca80xqf4h6au7e','simple_biz_list_item_sid');
CALL CreateMissingIndex('comp_role','displayname_idx','display_name,type');
CALL CreateMissingIndex('comp_role','FK3om6ew1cwvspla7e9ia75jmxf','template_id');
CALL CreateMissingIndex('comp_role','name_idx','name,type');
CALL CreateMissingIndex('comp_role_templates','displayname_idx','display_name');
CALL CreateMissingIndex('comp_role_templates','name_idx','name');
CALL CreateMissingIndex('content_metadata','contentSignatureIdx','content_signature');
CALL CreateMissingIndex('content_metadata','fileGroup_idx','file_group_id');
CALL CreateMissingIndex('content_metadata','groupDirty_idx','group_dirty');
CALL CreateMissingIndex('content_metadata','popDirty_idx','population_dirty');
CALL CreateMissingIndex('content_metadata','popDirty_joinSignatureIdz','population_dirty,content_signature');
CALL CreateMissingIndex('content_metadata','state_idx','state');
CALL CreateMissingIndex('content_metadata','typeState_idx','type,state,file_count');
CALL CreateMissingIndex('content_metadata','type_idx','type');
CALL CreateMissingIndex('content_metadata','userGroup_idx','user_group_id');
CALL CreateMissingIndex('content_metadata','user_content_signature_idx','user_content_signature');
CALL CreateMissingIndex('crawl_runs','idx_parentrun','parent_run_id');
CALL CreateMissingIndex('crawl_runs','idx_rfid','root_folder_id');
CALL CreateMissingIndex('crawl_runs','idx_runstatus','run_status');
CALL CreateMissingIndex('crawl_runs','idx_runtype','run_type');
CALL CreateMissingIndex('crawl_runs','idx_stoptime','stop_time');
CALL CreateMissingIndex('customer_data_center','name_constraint','name');
CALL CreateMissingIndex('date_range_partition_simple_date_range_items','FK3xpfcf5l5x16kq8a8dhu4fo26','date_range_partition_id');
CALL CreateMissingIndex('date_range_partition_simple_date_range_items','UK_pd4n2bi7we4rupwa3o2f1c6gj','simple_date_range_items_id');
CALL CreateMissingIndex('deb_file_hist_info','lid_idx','lid');
CALL CreateMissingIndex('deb_file_hist_info','ts_idx','ts_milli');
CALL CreateMissingIndex('deb_file_hist_info','type_idx','type');
CALL CreateMissingIndex('deb_file_hist_info','uuid_idx','uuid');
CALL CreateMissingIndex('deb_similarity_info','lid_idx1','lid1');
CALL CreateMissingIndex('deb_similarity_info','lid_idx2','lid2');
CALL CreateMissingIndex('deb_similarity_info','ts_idx','ts_milli');
CALL CreateMissingIndex('deb_similarity_info','type_idx','type');
CALL CreateMissingIndex('deb_similarity_info','uuid_idx1','uuid1');
CALL CreateMissingIndex('deb_similarity_info','uuid_idx2','uuid2');
CALL CreateMissingIndex('df_acl_items','doc_folder_id_idx','doc_folder_id');
CALL CreateMissingIndex('df_acl_items','type_idx','type');
CALL CreateMissingIndex('doc_folder','acl_signature_idx','acl_signature');
CALL CreateMissingIndex('doc_folder','deleted_depthFromRoot_idx','deleted,depth_from_root');
CALL CreateMissingIndex('doc_folder','deleted_depth_subFolders_id_idx','deleted,depth_from_root,num_of_sub_folders,id');
CALL CreateMissingIndex('doc_folder','deleted_hash_idx','deleted,folder_hashed');
CALL CreateMissingIndex('doc_folder','deleted_idx','deleted');
CALL CreateMissingIndex('doc_folder','deleted_id_idx','deleted,id');
CALL CreateMissingIndex('doc_folder','deleted_pFolder_pInfo_idx','deleted,parent_folder_id,parents_info');
CALL CreateMissingIndex('doc_folder','deleted_rootFid_idx','root_folder_id,deleted');
CALL CreateMissingIndex('doc_folder','deleted_rootFid_pkg_idx','root_folder_id,package_top_file_id,deleted');
CALL CreateMissingIndex('doc_folder','del_path_idx','deleted,path');
CALL CreateMissingIndex('doc_folder','del_real_path_idx','deleted,real_path');
CALL CreateMissingIndex('doc_folder','depthFromRoot_idx','depth_from_root');
CALL CreateMissingIndex('doc_folder','FKp9o7ek0aq6pxhd5qc9ymxj1un','doc_store_id');
CALL CreateMissingIndex('doc_folder','FKroufcc6kuctkom7toyscrtlsj','parent_folder_id');
CALL CreateMissingIndex('doc_folder','FK_doc_folder_package_top_file_id','package_top_file_id');
CALL CreateMissingIndex('doc_folder','folderHashed_idx','folder_hashed');
CALL CreateMissingIndex('doc_folder','media_item_rootFid_idx','root_folder_id,media_entity_id');
CALL CreateMissingIndex('doc_folder','name_idx','name');
CALL CreateMissingIndex('doc_folder','parentsInfo_idx','parents_info');
CALL CreateMissingIndex('doc_folder','path_idx','path');
CALL CreateMissingIndex('doc_folder','pkg_rootFid_del_idx','root_folder_id,deleted,package_top_file_id');
CALL CreateMissingIndex('doc_folder','real_path_idx','real_path');
CALL CreateMissingIndex('doc_type','doc_type_identifier_idx','doc_type_identifier');
CALL CreateMissingIndex('doc_type','FK4mlxkjyidv8b2j8o6qfeeu9gv','doc_type_risk_information_id');
CALL CreateMissingIndex('doc_type','FKflvrpp39pex8do8h40ga0njoo','biz_list_id');
CALL CreateMissingIndex('doc_type','FKo7sw6ivt7uu8sunfy34pa2s9x','template_id');
CALL CreateMissingIndex('doc_type','FKowc91h4u2v4036069rhgdct8g','parent_doc_type_id');
CALL CreateMissingIndex('doc_type','name_par_acc','name,parent_doc_type_id');
CALL CreateMissingIndex('doc_type_association','association_time_stamp_idx','association_time_stamp');
CALL CreateMissingIndex('doc_type_association','deleted_idx','deleted');
CALL CreateMissingIndex('doc_type_association','dirty_idx','dirty');
CALL CreateMissingIndex('doc_type_association','FKaobsokleygxil4im7bvixday2','cla_file_id');
CALL CreateMissingIndex('doc_type_association','FKe1jirhj049wwwhadjh7y8j0og','doc_type_id');
CALL CreateMissingIndex('doc_type_association','FKjx8pl783q8q7kukod3aw03ca2','origin_folder_id');
CALL CreateMissingIndex('doc_type_association','FKk1pkmmj2wefu58pslyb6u4wom','group_id');
CALL CreateMissingIndex('doc_type_association','FKnn3oa7r2v88f01qp23vryj1xn','folder_id');
CALL CreateMissingIndex('doc_type_association','source_idx','association_source');
CALL CreateMissingIndex('doc_type_risk_information','name_acc','name');
CALL CreateMissingIndex('doc_type_risk_information','name_idx','name');
CALL CreateMissingIndex('doc_type_template','name_acc','name');
CALL CreateMissingIndex('entity_credentials','entity_identify','entity_type,entity_id');
CALL CreateMissingIndex('err_processed_files','FK4bx245exhrewbi98ydmabkm8h','cla_file_id');
CALL CreateMissingIndex('err_processed_files','run_id_idx','run_id');
CALL CreateMissingIndex('err_scanned_files','FKe52w6emvpv700m07g0y4xeks1','doc_folder_id');
CALL CreateMissingIndex('err_scanned_files','run_id_idx','run_id');
CALL CreateMissingIndex('extracted_entities','claFileId_idx','cla_file_id');
CALL CreateMissingIndex('extracted_entities','clearFields_idx','clear_fields');
CALL CreateMissingIndex('extracted_entities','customField_idx','custom_field');
CALL CreateMissingIndex('extracted_entities','groupId_idx','group_id');
CALL CreateMissingIndex('extracted_entities','ruleParam_idx','rule_param');
CALL CreateMissingIndex('extracted_entities','ruleType_idx','rule_type');
CALL CreateMissingIndex('extracted_entities','rule_idx','rule');
CALL CreateMissingIndex('extracted_text','claFileId_idx_et','cla_file_id');
CALL CreateMissingIndex('extracted_text','groupId_idx_et','group_id');
CALL CreateMissingIndex('extracted_text','resultOrder_idx_et','result_order');
CALL CreateMissingIndex('extraction_configuration','FKo97044xt6vpy4lrsbmkegf86b','biz_list_id');
CALL CreateMissingIndex('extraction_configuration','name_idx','name');
CALL CreateMissingIndex('file_groups','deleted_idx','deleted');
CALL CreateMissingIndex('file_groups','del_nof_idx','deleted,num_of_files');
CALL CreateMissingIndex('file_groups','del_type_idx','deleted,group_type');
CALL CreateMissingIndex('file_groups','dirty_idx','dirty');
CALL CreateMissingIndex('file_groups','dirty_type_idx','dirty,group_type');
CALL CreateMissingIndex('file_groups','FKd1opto29mep0nx58cxtwr1llw','parent_group_id');
CALL CreateMissingIndex('file_groups','FKfngdk0hk53uj8wcai6rcppnit','first_member_id');
CALL CreateMissingIndex('file_groups','id_del_idx','id,deleted');
CALL CreateMissingIndex('file_groups','name_idx','name');
CALL CreateMissingIndex('file_groups','numOfFiles_idx','num_of_files');
CALL CreateMissingIndex('file_tag','FK5kk1fsbte7sji9a70j2jqmdn0','type_id');
CALL CreateMissingIndex('file_tag','FK8gylvloh0yumxxenwdfb8xwmb','doctype_id');
CALL CreateMissingIndex('file_tag','FKf5pihgbl71vyociwmv5rx1h9','parent_tag_id');
CALL CreateMissingIndex('file_tag','identifier_idx','identifier');
CALL CreateMissingIndex('file_tag','name_idx','name');
CALL CreateMissingIndex('file_tag','UK6yicw1144rghckjgx48msmr3u','identifier');
CALL CreateMissingIndex('file_tag_type','name_idx','name');
CALL CreateMissingIndex('file_tag_type','sensitive_tag_idx','sensitive_tag');
CALL CreateMissingIndex('folder_exclude_rule','rootsatch_op','root_folder_id,match_string,operator');
CALL CreateMissingIndex('folder_tag_association','association_time_stamp_idx','association_time_stamp');
CALL CreateMissingIndex('folder_tag_association','deleted_idx','deleted');
CALL CreateMissingIndex('folder_tag_association','FK2wumxam5gl1o70d6vo619sgn4','doc_folder_id');
CALL CreateMissingIndex('folder_tag_association','FK68f5gq43n2ne3e9uwgei2xjxs','simple_biz_list_item_sid');
CALL CreateMissingIndex('folder_tag_association','FK9a0rxvwkif6pdgkq1cnp426rn','doc_type_id');
CALL CreateMissingIndex('folder_tag_association','FKdyb4cvpt3lnwl5iuxp5xoqnka','functional_role_id');
CALL CreateMissingIndex('folder_tag_association','FKm0rm8fh4f6uk6njg2xxbkigk8','origin_folder_id');
CALL CreateMissingIndex('folder_tag_association','FKp5gfouygu6xe49pc5iieeag63','file_tag_id');
CALL CreateMissingIndex('folder_tag_association','implicit_idx','implicit');
CALL CreateMissingIndex('folder_tag_association','solr_dirty_idx','solr_dirty');
CALL CreateMissingIndex('group_unification_helper','id_processed_idx','id,processed');
CALL CreateMissingIndex('group_unification_helper','orig_group_idx','orig_group,processed');
CALL CreateMissingIndex('jm_jobs','item_id_idx','item_id,deleted,tasks_part_id');
CALL CreateMissingIndex('jm_jobs','run_context_idx','run_context');
CALL CreateMissingIndex('jm_jobs','state_idx','state');
CALL CreateMissingIndex('jm_jobs','type_idx','type');
CALL CreateMissingIndex('jm_jobs','type_state_idx','type,state,deleted');
CALL CreateMissingIndex('jm_tasks','job_idx','job_id,deleted');
CALL CreateMissingIndex('jm_tasks','job_item_idx','job_id,item_id');
CALL CreateMissingIndex('jm_tasks','job_run_context_idx','run_context');
CALL CreateMissingIndex('jm_tasks','job_state_idx','job_id,state,deleted');
CALL CreateMissingIndex('jm_tasks','job_type_idx','job_id,type');
CALL CreateMissingIndex('ldap_connection_details','name_idx','name');
CALL CreateMissingIndex('ldap_group_mapping','FKfoubse72xih310ovk75v8ojyi','connection_details_id');
CALL CreateMissingIndex('ldap_group_mapping','name_idx','group_name');
CALL CreateMissingIndex('ldap_group_mapping_da_roles','FKmwfa3are7jtnoal5uq58lornd','da_roles_id');
CALL CreateMissingIndex('media_connection_details','FKgsixjci72oictvagy2sp7b48d','owner_id');
CALL CreateMissingIndex('merging_groups','source_group_idx','source_group');
CALL CreateMissingIndex('merging_groups','source_target_idx','source_group,target_group');
CALL CreateMissingIndex('missed_file_match','fileid1_idx','file1_id,processed');
CALL CreateMissingIndex('missed_file_match','fileid2_idx','file2_id,processed');
CALL CreateMissingIndex('missed_file_match','id_processed_idx','id,processed');
CALL CreateMissingIndex('mon_components','FKnxiljnvyyl6qtiwf9g926m27u','customer_data_center_id');
CALL CreateMissingIndex('mon_components','idx_instance_component','instance_id,cla_component_type');
CALL CreateMissingIndex('mon_components','idx_type','component_type');
CALL CreateMissingIndex('mon_component_status','idx_component','cla_component_id');
CALL CreateMissingIndex('mon_component_status','idx_type','component_type,timestamp');
CALL CreateMissingIndex('roles','role_idx','name');
CALL CreateMissingIndex('role_authorities','UKcvyx3eb0fi8ljnq12ewiv43ej','role_id,authorities');
CALL CreateMissingIndex('root_folder','FK6swris59dnnlayivu0ivg90m2','customer_data_center_id');
CALL CreateMissingIndex('root_folder','FKg6kjxfrkxivgla25qu32woyki','media_connection_details');
CALL CreateMissingIndex('root_folder','FKj3y4ijlluvuctdxscviaa3wl0','doc_store_include');
CALL CreateMissingIndex('root_folder','mediaType_idx','media_type');
CALL CreateMissingIndex('root_folder','path_idx','path');
CALL CreateMissingIndex('root_folder_schedule','FK9t2uk03xea3an5wn8baopy83o','root_folder_id');
CALL CreateMissingIndex('root_folder_schedule','IDXsdoxxobju5onm1nfsvyo4enmx','schedule_group_id,number');
CALL CreateMissingIndex('saved_charts','FKp3nalnnsidm7ygkjg0pfyqlt2','owner_id');
CALL CreateMissingIndex('saved_filters','FKm71rfktl6njuyuijxg8fo70vl','owner_id');
CALL CreateMissingIndex('saved_filters','name_idx','name');
CALL CreateMissingIndex('schedule_group','name_constraint','name');
CALL CreateMissingIndex('subtree','doc_folder_id_idx','doc_folder_id');
CALL CreateMissingIndex('subtree','type_idx','type');
CALL CreateMissingIndex('system_settings','name_index','name');
CALL CreateMissingIndex('template_roles','FKsny5dvl1opa68qqxk1125nn2e','role_id');
CALL CreateMissingIndex('trend_chart','FK9b5qgw64fjtr0u9wjo5epf3y6','owner_id');
CALL CreateMissingIndex('trend_chart_data','tc_ts_idx','trend_chart_id,requested_time_stamp');
CALL CreateMissingIndex('users','deleted_idx','deleted');
CALL CreateMissingIndex('users','type_idx','user_type');
CALL CreateMissingIndex('users','username_idx','username');
CALL CreateMissingIndex('users_comp_roles','FKd8x5rvmy91vb2nplccshdqf9d','comp_role_id');
CALL CreateMissingIndex('users_roles','FKa62j07k5mhgifpp955h37ponj','roles_id');
CALL CreateMissingIndex('user_access_tokens','FKpdl33hbshnajawagv06iev1m2','user_id');
CALL CreateMissingIndex('user_saved_data','user_key_index','user,user_key');

-- DROP PROCEDURE IF EXISTS `CreateMissingIndex`;
INSERT INTO `_schema_upgrade_log` (`time_stamp`, `status`, `message`)
    SELECT now(3),0,'Done adding missing indexes';

-- fixed Dec-2018 replace 'da' with database() call.
DROP PROCEDURE IF EXISTS drop_column_with_fk_and_index;
DELIMITER $$
CREATE PROCEDURE `drop_column_with_fk_and_index`(
	IN `table_name` VARCHAR(50),
	IN `column_name` VARCHAR(50)
)
BEGIN
	select @count_cl:=count(k.COLUMN_NAME)
	from information_schema.COLUMNS k
	where k.TABLE_SCHEMA = database()
	  and k.COLUMN_NAME = column_name
	  and k.TABLE_NAME =table_name;

	IF @count_cl > 0 THEN
	      set @s='';
			select @db:= k.CONSTRAINT_SCHEMA,@constraint_name:=k.CONSTRAINT_NAME
			from information_schema.key_column_usage k
			where CONSTRAINT_SCHEMA = 'da'
	  		and k.COLUMN_NAME = column_name
	  		and k.TABLE_NAME =table_name;
	  		IF @constraint_name is not null THEN
			   set @s=concat(@s,'ALTER TABLE ', table_name, ' DROP FOREIGN KEY ',@constraint_name, ', DROP INDEX ',@constraint_name,', DROP COLUMN ',column_name,  ';' );
			ELSE
			   set @s= concat(@s,'ALTER TABLE ', table_name, ' DROP COLUMN ',column_name,  ';' );
			END IF;

			prepare stmt from @s;
			execute stmt;
			deallocate prepare stmt;
	END IF;

END $$
DELIMITER ;

DROP FUNCTION IF EXISTS get_last_partition_name;
DELIMITER $$
-- create stored procedures to get last partition data
CREATE FUNCTION get_last_partition_name(table_name_str varchar(50)) RETURNS varchar(50)
BEGIN
    SELECT partition_name INTO @last_partition_name
        FROM information_schema.PARTITIONS
        WHERE table_schema = database() AND table_name = table_name_str
        AND partition_ordinal_position = (
            SELECT MAX(partition_ordinal_position)
            FROM information_schema.PARTITIONS
            WHERE table_schema = database() AND table_name = table_name_str
        );
    RETURN (@last_partition_name);
END $$
DELIMITER ;

DROP FUNCTION IF EXISTS get_last_partition_date;
DELIMITER $$
CREATE FUNCTION get_last_partition_date(table_name_str varchar(50)) RETURNS BIGINT(20)
BEGIN
    SELECT partition_description INTO @last_partition_date
        FROM information_schema.PARTITIONS
        WHERE table_schema = database() AND table_name = table_name_str
        AND partition_ordinal_position = (
            SELECT MAX(partition_ordinal_position)
            FROM information_schema.PARTITIONS
            WHERE table_schema = database() AND table_name = table_name_str
        );
    RETURN (@last_partition_date);
END $$

DELIMITER ;
DROP FUNCTION IF EXISTS get_first_partition_name;
DELIMITER $$
-- create stored procedures to get first partition data
CREATE FUNCTION get_first_partition_name(table_name_str varchar(50)) RETURNS varchar(50)
BEGIN
    SELECT partition_name INTO @fst_partition_name
        FROM information_schema.PARTITIONS
        WHERE table_schema = database() AND table_name = table_name_str
        AND partition_ordinal_position = 1;
RETURN (@fst_partition_name);
END $$

DELIMITER ;

DROP FUNCTION IF EXISTS get_first_partition_date;
DELIMITER $$
CREATE FUNCTION get_first_partition_date(table_name_str varchar(50)) RETURNS BIGINT(20)
BEGIN
    SELECT partition_description INTO @fst_partition_date
        FROM information_schema.PARTITIONS
        WHERE table_schema = database() AND table_name = table_name_str
        AND partition_ordinal_position = 1;
    RETURN (@fst_partition_date);
END $$

DELIMITER ;

INSERT INTO `_schema_upgrade_log` (`time_stamp`, `status`, `message`)
    SELECT now(3),0,'Done adding missing procedures and functions';

SELECT * FROM `_schema_upgrade_log`;

