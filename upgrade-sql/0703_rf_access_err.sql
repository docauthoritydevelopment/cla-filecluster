ALTER TABLE `root_folder`
	ADD COLUMN `ignore_access_errors` BIT(1) NULL DEFAULT NULL;

ALTER TABLE `root_folder`
	ADD COLUMN `failure_getting_share_permission` BIT(1) NOT NULL;