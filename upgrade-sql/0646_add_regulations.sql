ALTER TABLE text_search_pattern  ADD COLUMN created_ondb bigint(20) NULL DEFAULT NULL;

update text_search_pattern  set created_ondb = UNIX_TIMESTAMP()*1000;

CREATE TABLE `regulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `created_ondb` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `regulation` (active,created_ondb,name) VALUES ( b'1', UNIX_TIMESTAMP(now())*1000, 'GDPR');
INSERT INTO `regulation` (active,created_ondb,name) VALUES ( b'1', UNIX_TIMESTAMP(now())*1000, 'HIPPA');

CREATE TABLE `pattern_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `created_ondb` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl33vy5j1uuulkiwa7c5v17vae` (`parent_id`),
  CONSTRAINT `FKl33vy5j1uuulkiwa7c5v17vae` FOREIGN KEY (`parent_id`) REFERENCES `pattern_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `pattern_category` (`id`,`active`,`name`,`created_ondb`) VALUES (1,1,'General',UNIX_TIMESTAMP()*1000);
INSERT INTO `pattern_category` (`id`,`active`,`name`, `parent_id`,`created_ondb`) VALUES (2,1,'General', '1',UNIX_TIMESTAMP()*1000);


CREATE TABLE `text_search_pattern_regulations` (
  `patterns_id` int(11) NOT NULL,
  `regulations_id` int(11) NOT NULL,
  PRIMARY KEY (`patterns_id`,`regulations_id`),
  KEY `FK555yiygqh15pus5x4gehhb15o` (`regulations_id`),
  CONSTRAINT `FK555yiygqh15pus5x4gehhb15o` FOREIGN KEY (`regulations_id`) REFERENCES `regulation` (`id`),
  CONSTRAINT `FKql3nrs5aetd6lqwngt1rhcf55` FOREIGN KEY (`patterns_id`) REFERENCES `text_search_pattern` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `text_search_pattern_sub_categories` (
  `patterns_id` int(11) NOT NULL,
  `sub_categories_id` int(11) NOT NULL,
  PRIMARY KEY (`patterns_id`,`sub_categories_id`),
  KEY `FKl22i44d0si2dc38k2nyd96ioe` (`sub_categories_id`),
  CONSTRAINT `FK4a28lq384e969l1xn8xu2o9fd` FOREIGN KEY (`patterns_id`) REFERENCES `text_search_pattern` (`id`),
  CONSTRAINT `FKl22i44d0si2dc38k2nyd96ioe` FOREIGN KEY (`sub_categories_id`) REFERENCES `pattern_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `text_search_pattern_sub_categories` ( `patterns_id`, `sub_categories_id`) SELECT  tsp.id, 2 FROM    text_search_pattern tsp;

update file_tag_type set hidden=1 where name in ('Pattern','Pattern-many');