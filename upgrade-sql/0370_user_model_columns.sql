ALTER TABLE `users`
     ADD COLUMN `account_non_expired` bit(1) NOT NULL DEFAULT b'1'
    ,ADD COLUMN `account_non_locked` bit(1) NOT NULL DEFAULT b'1'
    ,ADD COLUMN `login_failures` int(11) DEFAULT 0
    ,ADD COLUMN `credentials_non_expired` bit(1) NOT NULL DEFAULT b'1'
    ,ADD COLUMN `last_bad_login` datetime(3) DEFAULT NULL
    ,ADD COLUMN `last_good_login` datetime(3) DEFAULT NULL
;
