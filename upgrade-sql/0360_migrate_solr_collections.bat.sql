REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
if NOT [%1] == [do-convert] goto usage

REM
REM This file is meant to be activated manually after updated installer has finished instllation.
REM
pushd ..
SET LOGF=installation_logs\%0.log
SET CONF_DIR=.\filecluster\config
SET NEW_CONF_DIR=.\filecluster\config_new
SET PROP_FILE=%CONF_DIR%\application.properties
SET MIGRATE=call solr_cloud_config.bat migrate-collection-2017-10
SET CREATE_ALIAS=call solr_cloud_config.bat create-alias
set STOP_CMD=sc stop
REM
REM ZK_ADDESS and SHARDS should be updated for the specific system configuration
REM
SET ZK_ADDESS=localhost:2181/solr
SET SHARDS=3

IF NOT EXIST "%CONF_DIR%" (
    Echo Config dir %CONF_DIR% doesnot exist >> %LOGF%
    exit 1
)
IF NOT EXIST "%NEW_CONF_DIR%" (
    Echo New config dir %NEW_CONF_DIR% doesnot exist >> %LOGF%
)

%STOP_CMD% "DocAuthority-filecluster"

REM
REM SolrCloud collections are set with old schema. The updated configurations under .\solr are already updated to latest schema after installer completed.
REM New collections will be created and loaded with the converted data. Then application.properties will be added with reference to new collection names.
REM
%MIGRATE% %ZK_ADDESS% Extraction Extraction2 create solr\Extraction %SHARDS%
%MIGRATE% %ZK_ADDESS% PVS PVS2 create solr\PVS %SHARDS%
%MIGRATE% %ZK_ADDESS% CatFiles CatFiles2 create solr\CatFiles %SHARDS%
%MIGRATE% %ZK_ADDESS% AnalysisData AnalysisData2 create solr\AnalysisData %SHARDS%
%MIGRATE% %ZK_ADDESS% ContentMetadata ContentMetadata2 create solr\ContentMetadata %SHARDS%
%MIGRATE% %ZK_ADDESS% GrpHelper GrpHelper2 create solr\GrpHelper %SHARDS%

%CREATE_ALIAS% %ZK_ADDESS% Extraction Extraction2
%CREATE_ALIAS% %ZK_ADDESS% PVS PVS2
%CREATE_ALIAS% %ZK_ADDESS% CatFiles CatFiles2
%CREATE_ALIAS% %ZK_ADDESS% AnalysisData AnalysisData2
%CREATE_ALIAS% %ZK_ADDESS% ContentMetadata ContentMetadata2
%CREATE_ALIAS% %ZK_ADDESS% GrpHelper GrpHelper2

REM echo.>> %PROP_FILE%
REM echo fileDTO.solr.core=Extraction2>> %PROP_FILE%
REM echo pvs.solr.core=PVS2>> %PROP_FILE%
REM REM echo categoryFiles.solr.core=CatFiles2>> %PROP_FILE%
REM echo analysisData.solr.core=AnalysisData2>> %PROP_FILE%
REM echo contentMetadata.solr.core=ContentMetadata2>> %PROP_FILE%
REM echo grpHelper.solr.core=GrpHelper2>> %PROP_FILE%

REM Echo Added spring properties to %PROP_FILE%>> %LOGF%
popd

goto finally

:usage
echo Usage:
echo        %0 do-reset
goto finally

:finally
exit /b 0
