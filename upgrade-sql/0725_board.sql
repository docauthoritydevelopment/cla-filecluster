CREATE TABLE `boards` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`board_type` VARCHAR(255) NULL DEFAULT NULL,
	`description` VARCHAR(255) NULL DEFAULT NULL,
	`is_public` BIT(1) NOT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`owner_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `board_tasks` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`assignee` BIGINT(20) NOT NULL,
	`board_entity_type` VARCHAR(255) NULL DEFAULT NULL,
	`board_id` BIGINT(20) NOT NULL,
	`board_task_state` VARCHAR(255) NULL DEFAULT NULL,
	`board_task_type` VARCHAR(255) NULL DEFAULT NULL,
	`due_date` BIGINT(20) NOT NULL,
	`entity_id` VARCHAR(255) NULL DEFAULT NULL,
	`owner_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
