alter TABLE `user_view_data` add column `global_filter` BOOLEAN;

update user_view_data set global_filter = 1 where owner_id is null;
update user_view_data set global_filter = 0 where owner_id is not null;