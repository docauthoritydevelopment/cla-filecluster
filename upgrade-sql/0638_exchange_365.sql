alter table root_folder add column `mailbox_group` VARCHAR(255) NULL DEFAULT NULL;
alter table root_folder add column `from_date_scan_filter` BIGINT(20) NULL DEFAULT NULL;

CREATE TABLE `mailboxes` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`root_folder_id` BIGINT(20) NOT NULL,
	`sync_state` VARCHAR(2048) NULL DEFAULT NULL,
	`upn` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `upn_rf_uqc` (`root_folder_id`, `upn`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

update root_folder set file_types = concat(file_types , ',MAIL') where file_types like '%PST%';