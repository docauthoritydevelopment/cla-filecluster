INSERT INTO `roles` VALUES
     (41,'Manage saved filters, charts and dashboards','Manage user view data','MngUserViewData')
;


INSERT INTO `role_authorities` VALUES
   (41,40)
   ;

insert into template_roles
select template_id, 41 from template_roles where role_id in (
select id from roles where name='GeneralAdmin');