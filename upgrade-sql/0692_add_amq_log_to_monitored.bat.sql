REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
REM --- if NOT [%1] == [do-convert] goto usage

setlocal
REM Assumes that AMQ is installed on same machine as FC where upgrade scripts are activated
SET FILE=..\mngAgent\config\application.properties
SET FROM_STR=tailer.files=
SET TO_STR=%FROM_STR%../apache-activemq/data/activemq.log,
powershell -Command "(gc %FILE%) -replace '%FROM_STR%', '%TO_STR%' | Out-File -encoding ASCII %FILE%"
endlocal
exit /B 0
