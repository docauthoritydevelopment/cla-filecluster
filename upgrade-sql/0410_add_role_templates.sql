CREATE TABLE `comp_role_templates` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`deleted` BIT(1) NOT NULL,
	`description` VARCHAR(255) NULL DEFAULT NULL,
	`display_name` VARCHAR(255) NOT NULL,
	`date_created` BIGINT(20) NOT NULL,
	`date_modified` BIGINT(20) NOT NULL,
	`template_type` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name_idx` (`name`),
	UNIQUE INDEX `displayname_idx` (`display_name`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;

CREATE TABLE `template_roles` (
	`template_id` BIGINT(20) NOT NULL,
	`role_id` INT(11) NOT NULL,
	PRIMARY KEY (`template_id`, `role_id`),
	INDEX `FK9vuttttpslacxudkfng4mcth3` (`role_id`),
	CONSTRAINT `FK96gnlemjctttxavwb0ww6nwjo` FOREIGN KEY (`template_id`) REFERENCES `comp_role_templates` (`id`),
	CONSTRAINT `FK9vuiiuhpslactttfng4mcth3` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


ALTER TABLE `comp_role` ADD COLUMN `template_id` BIGINT(20) NULL AFTER `manager_username`;

insert into comp_role_templates (name, display_name, description, deleted, date_created, date_modified, template_type)
select name, display_name, description, deleted, now(), now(), 2 from comp_role;

update comp_role r set template_id = (select id from comp_role_templates where name = r.name and display_name = r.display_name);

insert into template_roles (template_id, role_id)
select c.template_id, r.role_id
from comp_role c, roles_comp_roles r
where c.id = r.comp_role_id;
