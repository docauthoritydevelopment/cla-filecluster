ALTER TABLE `customer_data_center`
	ADD COLUMN `ingest_tasks_queue_limit` INT(11) NULL DEFAULT NULL;
ALTER TABLE `customer_data_center`
	ADD COLUMN `ingest_task_retry_time` INT(11) NULL DEFAULT NULL;
ALTER TABLE `customer_data_center`
	ADD COLUMN `ingest_task_expire_time` INT(11) NULL DEFAULT NULL;