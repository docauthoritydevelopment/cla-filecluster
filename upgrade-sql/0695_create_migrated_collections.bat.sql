REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
REM --- if NOT [%1] == [do-convert] goto usage

setlocal EnableDelayedExpansion
set UPGRADE_TOOL=call filecluster\filecluster-upgrade-schema.bat
set SOLR_ADDRESS=localhost:2181/solr
pushd ..

SET NUM_SHARDS=1
for /f "tokens=1,2" %%a in (num-shards.txt) do (
	if "X%%b" == "XLegacy" (
		SET NUM_SHARDS=%%a
	)
	if "X%%b" == "X" (
		SET NUM_SHARDS=%%a
	)
)
ECHO NUM_SHARDS=%NUM_SHARDS%
set UPGRADE_TOOL=call filecluster\filecluster-upgrade-schema.bat
set SOLR_ADDRESS=localhost:2181/solr
SET OUTFILE1=filecluster\tempDir\0695_align_1.bat
SET OUTFILE2=filecluster\tempDir\0695_align_2.bat
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed CatFiles_2 CatFiles %NUM_SHARDS%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed ContentMetadata_2 ContentMetadata %NUM_SHARDS%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed BizList_2 BizList %NUM_SHARDS%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed GrpHelper_2 GrpHelper %NUM_SHARDS%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed UnmatchedContent_2 UnmatchedContent %NUM_SHARDS%

if [X%NUM_SHARDS%]==[X1] GOTO noAlignment
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards "Extraction,CatFiles_2,BizList_2" %OUTFILE1% append
Call %OUTFILE1%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards "PVS,GrpHelper_2" %OUTFILE2%
Call %OUTFILE2%

:noAlignment
popd
exit /B 0
