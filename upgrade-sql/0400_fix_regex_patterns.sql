-- Remove redundant \\ from pre defined native search patterns (force new values)
UPDATE text_search_pattern SET pattern = REPLACE(pattern, '\\\\', '\\') WHERE id > 0;
