update policies p
set p.config = JSON_ARRAY_APPEND(p.config,'$.relevantEventTypes','SCAN_DIFF_NEW'),
p.config = JSON_INSERT(p.config,'$.actionPluginsDefinitions[0].pluginInputTransformer.severityMappings.SCAN_DIFF_NEW','MEDIUM');