alter TABLE `root_folder` add column `ingest_file_types` VARCHAR(255) NULL DEFAULT NULL;

update root_folder set ingest_file_types = file_types where file_types is not null;