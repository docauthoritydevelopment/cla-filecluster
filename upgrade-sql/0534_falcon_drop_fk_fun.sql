DROP PROCEDURE IF EXISTS drop_fk_for_table;
DELIMITER //
CREATE PROCEDURE `drop_fk_for_table`(
	IN `table_name` VARCHAR(50),
	IN `column_name` VARCHAR(50)
)
BEGIN

	select @count_cl:=count(k.COLUMN_NAME)
	from information_schema.COLUMNS k
	where k.TABLE_SCHEMA = DATABASE()
	  and k.COLUMN_NAME = column_name
	  and k.TABLE_NAME =table_name;

	IF @count_cl > 0 THEN
	      set @s='';
			select @db:= k.CONSTRAINT_SCHEMA,@constraint_name:=k.CONSTRAINT_NAME
			from information_schema.key_column_usage k
			where CONSTRAINT_SCHEMA = DATABASE()
	  		and k.COLUMN_NAME = column_name
	  		and k.TABLE_NAME = table_name;
	  		IF @constraint_name is not null THEN
			   set @s=concat(@s,'ALTER TABLE ', table_name, ' DROP FOREIGN KEY ',@constraint_name, ';' );

			END IF;

			prepare stmt from @s;
			execute stmt;
			deallocate prepare stmt;
	END IF;

END  //