INSERT INTO `da`.`user_view_data`
(
`display_data`,
`display_type`,
`global_filter`,
`name`)
VALUES
('{\"chartType\":\"YEARLY_TOP_EXTENSIONS\",\"displayTypes\":[\"line\"],\"title\":\"\",\"yAxisTitle\":\"Volume\",\"valueType\":\"fileSize\",\"additionalQueryParams\":{\"valueField\" : \"SIZE\", \"take\" : 5,\"valueFunction\":\"SUM\"}}',
'DASHBOARD_WIDGET',
1,
'Capacity management highlights');
