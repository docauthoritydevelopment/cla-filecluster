alter table `trend_chart` add column `resolution_type` int(11) DEFAULT NULL;
alter table `trend_chart` add column `view_offset` int(11) DEFAULT NULL;
alter table `trend_chart` add column `view_resolution_count` int(11) DEFAULT NULL;
alter table `trend_chart` add column `view_resolution_step` int(11) DEFAULT NULL;
update `trend_chart` set resolution_type = 0 where id > 0 AND resolution_type is NULL;
