ALTER TABLE `crawl_runs` add column `run_type` int(11) DEFAULT 0;
ALTER TABLE `crawl_runs` add column `parent_run_id` bigint(20) DEFAULT NULL;
ALTER TABLE `crawl_runs` add column `has_child_runs` bit(1) DEFAULT 0;

ALTER TABLE `err_processed_files` add KEY `run_id_idx` (`run_id`);