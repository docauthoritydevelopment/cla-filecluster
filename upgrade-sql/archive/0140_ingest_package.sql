ALTER TABLE `cla_files_md` DROP FOREIGN KEY FK_cla_files_md_package_top_folder_id;
ALTER TABLE `cla_files_md` DROP COLUMN `package_top_folder_id`;
ALTER TABLE `doc_folder` DROP FOREIGN KEY FK_doc_folder_package_top_folder_id;
ALTER TABLE `doc_folder` DROP COLUMN `package_top_folder_id`;

ALTER TABLE `cla_files_md` ADD COLUMN `package_top_file_id` bigint(20) DEFAULT NULL;
ALTER TABLE `cla_files_md` ADD CONSTRAINT FK_cla_files_md_package_top_file_id FOREIGN KEY (package_top_file_id) REFERENCES cla_files_md(id);
ALTER TABLE `doc_folder` ADD COLUMN `package_top_file_id` bigint(20) DEFAULT NULL;
ALTER TABLE `doc_folder` ADD CONSTRAINT FK_doc_folder_package_top_file_id FOREIGN KEY (package_top_file_id) REFERENCES cla_files_md(id);