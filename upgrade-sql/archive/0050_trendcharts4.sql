UPDATE trend_chart 
 SET transformer_script='output.ratio = 100 - Math.min(100,Math.round((10*input.dtAny + 10*input.tt02 + 10*input.tt03 + 10*input.tt04 + 10*input.tt13 + 10*input.tt14 + 10*input.tt15 + 10*input.tt16 + 20*input.tt17) / (input.total+1))) ;'
 WHERE transformer_script='output.ratio = 100 - Math.max(100,Math.round((10*input.dtAny + 10*input.tt02 + 10*input.tt03 + 10*input.tt04 + 10*input.tt13 + 10*input.tt14 + 10*input.tt15 + 10*input.tt16 + 20*input.tt17) / (input.total+1))) ;';
