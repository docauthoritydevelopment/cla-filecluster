alter table users add column `email` varchar(255) DEFAULT NULL;
alter table users add column `last_password_change` datetime(3) DEFAULT NULL;
alter table users add column `password_must_be_changed` bit(1) NOT NULL default 0;
alter table users add column `user_role` int(11) DEFAULT 0;
alter table users add column `user_state` int(11) DEFAULT 0;
alter table users add column `user_type` int(11) DEFAULT 0;
