alter table `trend_chart` modify column `transformer_script` varchar(4096) DEFAULT NULL;
alter table `trend_chart` modify column `filters` varchar(4096) DEFAULT NULL;
alter table `trend_chart_data` modify column `value` varchar(4096) DEFAULT NULL;