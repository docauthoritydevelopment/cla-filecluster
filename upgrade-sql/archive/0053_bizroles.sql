CREATE TABLE `biz_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `biz_role_roles` (
  `biz_role_id` bigint(20) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`biz_role_id`,`roles_id`),
  KEY `FK_f6lpivum1i4gy0h6hv19aeaoq` (`roles_id`),
  CONSTRAINT `FK_f6lpivum1i4gy0h6hv19aeaoq` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FK_q1nxatwveyaa6wt2i9xgqeqk6` FOREIGN KEY (`biz_role_id`) REFERENCES `biz_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users_biz_roles` (
  `users_id` bigint(20) NOT NULL,
  `biz_roles_id` bigint(20) NOT NULL,
  PRIMARY KEY (`users_id`,`biz_roles_id`),
  KEY `FK_3klo7bc2fjwve8is5nm0tmxqi` (`biz_roles_id`),
  CONSTRAINT `FK_3klo7bc2fjwve8is5nm0tmxqi` FOREIGN KEY (`biz_roles_id`) REFERENCES `biz_role` (`id`),
  CONSTRAINT `FK_6u7beig0ujcpybqrp8rn5l07u` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table users add column `deleted` bit(1) NOT NULL default 0;
