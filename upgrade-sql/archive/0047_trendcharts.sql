CREATE TABLE `trend_chart_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `cron_trigger_string` varchar(255) DEFAULT NULL,
  `last_requested_time_stamp` bigint(20) DEFAULT NULL,
  `last_run_end_time` bigint(20) NOT NULL,
  `last_run_start_time` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_o0ib9qwcigusyj5il7h78q5iy` (`account_id`),
  CONSTRAINT `FK_o0ib9qwcigusyj5il7h78q5iy` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `trend_chart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chart_display_type` int(11) DEFAULT NULL,
  `chart_value_format_type` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `filters` varchar(2048) DEFAULT NULL,
  `hidden` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `series_type` varchar(255) DEFAULT NULL,
  `sort_type` int(11) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `view_configuration` varchar(2048) DEFAULT NULL,
  `start_absolute_time` datetime DEFAULT NULL,
  `start_relative_period` int(11) DEFAULT NULL,
  `start_relative_period_amount` int(11) DEFAULT NULL,
  `start_type` int(11) DEFAULT NULL,
  `view_resolution_ms` bigint(20) DEFAULT NULL,
  `view_sample_count` int(11) DEFAULT NULL,
  `end_absolute_time` datetime DEFAULT NULL,
  `end_relative_period` int(11) DEFAULT NULL,
  `end_relative_period_amount` int(11) DEFAULT NULL,
  `end_type` int(11) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eq9b62ybcl8y47n2kftwd92xg` (`owner_id`),
  CONSTRAINT `FK_eq9b62ybcl8y47n2kftwd92xg` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `trend_chart_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actual_time_stamp` bigint(20) NOT NULL,
  `requested_time_stamp` bigint(20) NOT NULL,
  `value` varchar(2048) DEFAULT NULL,
  `view_resolution_ms` bigint(20) DEFAULT NULL,
  `trend_chart_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tc_ts_idx` (`trend_chart_id`,`requested_time_stamp`),
  CONSTRAINT `FK_ogspcp18nf7yh7l9dqtns8ro5` FOREIGN KEY (`trend_chart_id`) REFERENCES `trend_chart` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `trend_chart_schedule` (`id`,`active`,`cron_trigger_string`,`last_requested_time_stamp`,`last_run_end_time`,`last_run_start_time`,`name`,`account_id`)
  VALUES (1,1,'0 2 1 * * *',NULL,0,0,'default',1);