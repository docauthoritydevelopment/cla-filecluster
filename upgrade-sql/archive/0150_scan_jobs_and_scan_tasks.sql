CREATE TABLE `scan_jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_ondb` datetime(3) NOT NULL,
  `status_update_date` datetime(3) NOT NULL,
  `accepting_tasks` bit(1) NOT NULL,
  `status` varchar(255) NOT NULL,
  `root_folder_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `scan_tasks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_ondb` datetime(3) NOT NULL,
  `status_update_date` datetime(3) NOT NULL,
  `status` varchar(255) NOT NULL,
  `scan_job_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9t9tfiksyf445di5hpyfyj82a` (`scan_job_id`),
  CONSTRAINT `FK_9t9tfiksyf445di5hpyfyj82a` FOREIGN KEY (`scan_job_id`) REFERENCES `scan_jobs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;