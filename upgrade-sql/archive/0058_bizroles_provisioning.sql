--Create the provisioning sysRole
INSERT INTO `roles` (`id`,`description`,`display_name`,`name`) VALUES (6,'Manage bizRoles','BizRole-provisioning','bizRole-provisioning');

--Create the default bizRoles. Assign roles
INSERT INTO `biz_role_roles` (`biz_role_id`,`roles_id`) VALUES (1,6);

