alter table cla_files_md add KEY `content_metadata_idx` (`content_metadata_id`);
alter table content_metadata add KEY `popDirty_idx` (`population_dirty`);
alter table content_metadata add KEY `acct_popDirty_idx` (`account_id`,`population_dirty`);

-- Add license repo upgrade script here ...

CREATE TABLE `doc_authority_license` (
  `id` bigint(20) NOT NULL,
  `apply_date` bigint(20) NOT NULL,
  `creation_date` bigint(20) NOT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `registered_to` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `license_resource` (
  `id` varchar(255) NOT NULL,
  `issue_time` bigint(20) NOT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `doc_authority_license` (`id`,`apply_date`,`creation_date`,`issuer`,`registered_to`,`signature`) VALUES (1467116949459,1467123535859,1467116949459,'Uri','DEMO','37d20oEjzoKWC2lXVbetADKYIjI=');

INSERT INTO `license_resource` (`id`,`issue_time`,`signature`,`value`) VALUES ('crawlerExpiry',0,'nanKTqHjcpRm7dRgNEMgEj2WHLg=','1498663749000');
INSERT INTO `license_resource` (`id`,`issue_time`,`signature`,`value`) VALUES ('loginExpiry',0,'z+pRATw49FghuLCv+ybFSu/O1gA=','1498663749000');
INSERT INTO `license_resource` (`id`,`issue_time`,`signature`,`value`) VALUES ('maxFiles',0,'tN06iw8DP3qSCIJIxYdJPen5n9w=','10000000');
INSERT INTO `license_resource` (`id`,`issue_time`,`signature`,`value`) VALUES ('maxRootFolders',0,'HC21Wa73UgsJ6a8tu+Ear7Q9Ejw=','25');
