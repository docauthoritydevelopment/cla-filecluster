update action_class_executor_def
   set id = 'IONIC_ENCRYPTION_SCRIPT_GENERATOR',
    description = 'Creates ionic encryption scripts',
    java_class_name = 'com.cla.filecluster.actions.executors.IonicEncryptionScriptGenerator',
    name = 'Creates ionic encryption scripts'
where id = 'ENCRYPTION_MANAGEMENT_SCRIPT_GENERATOR';
