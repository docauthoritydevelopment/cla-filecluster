CREATE INDEX `rf_media_itm_idx` ON `cla_files_md` (root_folder_id, media_entity_id(250));
CREATE INDEX `name_idx` ON `doc_folder' (name(255));
CREATE INDEX `media_item_rootFid_idx` ON `doc_folder` (root_folder_id, media_entity_id(250));
