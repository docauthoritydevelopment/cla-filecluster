--DocFolder
-- No changes
--RootFolder
--     private CrawlerType crawlerType;
--     private Long mediaConnectionDetailsId;
Alter TABLE `root_folder` add column `crawler_type` int(11) DEFAULT NULL;
Alter TABLE `root_folder` add column `media_connection_details_id` bigint(20) DEFAULT NULL;

--MediaConnectionDetails
CREATE TABLE `media_connection_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `connection_details_json` varchar(255) DEFAULT NULL,
  `media_authentication_type` int(11) DEFAULT NULL,
  `media_type` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_15ix4pb9au1rljf9qwy4opls5` (`owner_id`),
  CONSTRAINT `FK_15ix4pb9au1rljf9qwy4opls5` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--AclItem

--ClaFIle
--private AclInheritanceType aclInheritanceType;
ALTER TABLE `cla_files_md` add column `acl_inheritance_type` int(11) DEFAULT NULL;
--DocFolderAclItem
CREATE TABLE `df_acl_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_folder_id` bigint(20) NOT NULL,
  `entity` varchar(1024) NOT NULL,
  `level` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_idx` (`type`),
  KEY `doc_folder_id_idx` (`doc_folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Retroactively add a missing column
Alter TABLE `crawl_runs` add column `media_change_log_position` varchar(4096) DEFAULT NULL;

