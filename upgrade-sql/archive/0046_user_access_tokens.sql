
-- for the upgrade script user_id is int and not long.
CREATE TABLE `user_access_tokens` (
  `user_id` int(11) NOT NULL,
  `access_tokens` varchar(255) DEFAULT NULL,
  KEY `FK_j9rx80fwoyi35tyl1lsjlu57b` (`user_id`),
  CONSTRAINT `FK_j9rx80fwoyi35tyl1lsjlu57b` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Users table id field changed from int to long (18/07/2016)

ALTER TABLE saved_charts DROP FOREIGN KEY FK_jv91ofphv09ci149qg5925i31;
ALTER TABLE saved_filters DROP FOREIGN KEY FK_lolwslrifr1n5462t8utuguh8;
ALTER TABLE user_access_tokens DROP FOREIGN KEY FK_j9rx80fwoyi35tyl1lsjlu57b;
ALTER TABLE users_roles DROP FOREIGN KEY FK_3b2cl2u4ck187o21r4uhp6psv;

ALTER TABLE `users` CHANGE COLUMN `id` `id` BIGINT(20) NOT NULL;

ALTER TABLE `saved_charts` CHANGE COLUMN `owner_id` `owner_id` BIGINT(20);
ALTER TABLE `saved_filters` CHANGE COLUMN `owner_id` `owner_id` BIGINT(20);
ALTER TABLE `users_roles` CHANGE COLUMN `users_id` `users_id` BIGINT(20) NOT NULL;
ALTER TABLE `user_access_tokens` CHANGE COLUMN `user_id` `user_id` BIGINT(20) NOT NULL;

ALTER TABLE saved_charts ADD CONSTRAINT FK_jv91ofphv09ci149qg5925i31 FOREIGN KEY (owner_id) REFERENCES users(id);
ALTER TABLE saved_filters ADD CONSTRAINT FK_lolwslrifr1n5462t8utuguh8 FOREIGN KEY (owner_id) REFERENCES users(id);
ALTER TABLE user_access_tokens ADD CONSTRAINT FK_j9rx80fwoyi35tyl1lsjlu57b FOREIGN KEY (user_id) REFERENCES users(id);
ALTER TABLE users_roles ADD CONSTRAINT FK_3b2cl2u4ck187o21r4uhp6psv FOREIGN KEY (users_id) REFERENCES users(id);

