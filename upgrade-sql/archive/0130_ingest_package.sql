ALTER TABLE `cla_files_md` ADD COLUMN `package_top_folder_id` bigint(20) DEFAULT NULL;
ALTER TABLE `cla_files_md` ADD CONSTRAINT FK_cla_files_md_package_top_folder_id FOREIGN KEY (package_top_folder_id) REFERENCES doc_folder(id);
ALTER TABLE `doc_folder` ADD COLUMN `package_top_folder_id` bigint(20) DEFAULT NULL;
ALTER TABLE `doc_folder` ADD CONSTRAINT FK_doc_folder_package_top_folder_id FOREIGN KEY (package_top_folder_id) REFERENCES doc_folder(id);