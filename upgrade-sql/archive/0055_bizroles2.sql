alter table `biz_role` add column `display_name` varchar(255) NOT NULL default "";

-- Copy display names to the correct column
update biz_role set display_name = name where id >-1;

-- Add the new api_crawler role
INSERT INTO `roles` (`id`,`description`,`display_name`,`name`) VALUES (5,'Can activate crawler api','crawler_api','crawler_api');

--Create the default bizRoles
INSERT INTO `biz_role` (`id`,`deleted`,`description`,`display_name`,`name`) VALUES
    (1,'\0','Admin role','Admin','admin')
    ,(2,'\0','Operator role','Operator','operator')
    ,(3,'\0','View role','Viewer','viewer');

--Create the default bizRoles. Assign roles
INSERT INTO `biz_role_roles` (`biz_role_id`,`roles_id`) VALUES
    (1,1)
    ,(1,2)
    ,(1,3)
    ,(1,5)
    ,(2,3)
    ,(2,5);

--Assign the bizRoles to the users
INSERT INTO `users_biz_roles` (`users_id`,`biz_roles_id`) VALUES
    (1,1)
    ,(2,2)
    ,(3,2);
