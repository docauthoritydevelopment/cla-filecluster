-- Conditionally add missing rows to the mon_components table.
insert into `mon_components`
(active, cla_component_type, component_type, created_ondb, external_net_address, instance_id, last_response_date, local_net_address, location, state, state_change_date, customer_data_center_id)
select * from (select
1, '5' as v8, NULL as v4, now() as v1, 'http://localhost:8080/', 'AppServer', now() as v2, NULL as v5, NULL as v6, '0' as v9, now() as v3, NULL as v7
) as tmp
WHERE NOT EXISTS (select instance_id from `mon_components` WHERE instance_id = 'AppServer') limit 1;

insert into `mon_components`
(active, cla_component_type, component_type, created_ondb, external_net_address, instance_id, last_response_date, local_net_address, location, state, state_change_date, customer_data_center_id)
select * from (select
1, '6' as v8, NULL as v4, now() as v1, 'failover://tcp://localhost:3306', 'DataBase', now() as v2, NULL as v5, NULL as v6, '0' as v9, now() as v3, NULL as v7
) as tmp
WHERE NOT EXISTS (select instance_id from `mon_components` WHERE instance_id = 'DataBase') limit 1;

insert into `mon_components`
(active, cla_component_type, component_type, created_ondb, external_net_address, instance_id, last_response_date, local_net_address, location, state, state_change_date, customer_data_center_id)
select * from (select
1, '1' as v8, NULL as v4, now() as v1, 'failover://tcp://localhost:61616', 'Broker', now() as v2, NULL as v5, NULL as v6, '0' as v9, now() as v3, NULL as v7
) as tmp
WHERE NOT EXISTS (select instance_id from `mon_components` WHERE instance_id = 'Broker') limit 1;

