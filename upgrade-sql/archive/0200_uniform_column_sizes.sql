-- Convert path related fields to support 4 bytes UTF8 (extension characters)

ALTER TABLE `cla_files_md` CHANGE `base_name` `base_name` varchar(255) DEFAULT NULL;
ALTER TABLE `cla_files_md` CHANGE `extension` `extension` varchar(255) DEFAULT NULL;
ALTER TABLE `cla_files_md` CHANGE `media_entity_id` `media_entity_id` varchar(2048) DEFAULT NULL;
ALTER TABLE `cla_files_md` CHANGE `full_path` `full_path` varchar(2048) NOT NULL;

ALTER TABLE `root_folder` CHANGE `media_entity_id` `media_entity_id` varchar(2048) DEFAULT NULL;
ALTER TABLE `root_folder` CHANGE `path` `path` varchar(2048) DEFAULT NULL;
ALTER TABLE `root_folder` CHANGE `real_path` `real_path` varchar(2048) DEFAULT NULL;

ALTER TABLE `doc_folder` CHANGE `media_entity_id` `media_entity_id` varchar(2048) DEFAULT NULL;
ALTER TABLE `doc_folder` CHANGE `name` `name` varchar(255) DEFAULT NULL;
ALTER TABLE `doc_folder` CHANGE `path` `path` varchar(2048) DEFAULT NULL;
ALTER TABLE `doc_folder` CHANGE `real_path` `real_path` varchar(2048) DEFAULT NULL;
