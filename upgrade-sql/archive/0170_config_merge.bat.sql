REM Sample BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
exit 0

REM ----
SET LOGF=%0.log
SET CONF_DIR=.\config
SET NEW_CONF_DIR=.\config_new
SET PROP_FILE=%CONF_DIR%\application.properties
SET LOG_CONF1=%NEW_CONF_DIR%\log4j.properties
SET LOG_CONF2=%NEW_CONF_DIR%\log4j-fileprocessor.properties

IF NOT EXIST "%CONF_DIR%" (
    Echo Config dir %CONF_DIR% doesnot exist >> %LOGF%
    exit 1
)
IF NOT EXIST "%NEW_CONF_DIR%" (
    Echo New config dir %NEW_CONF_DIR% doesnot exist >> %LOGF%
)

echo spring.velocity.check-template-location=false >> %PROP_FILE%
echo spring.velocity.enabled=false >> %PROP_FILE%

Echo Added spring properties to %PROP_FILE% >> %LOGF%
 
IF  EXIST "%LOG_CONF1%" (
  Echo Copying %LOG_CONF1% %CONF_DIR%  >> %LOGF%
  COPY /Y %LOG_CONF1% %CONF_DIR%  >> %LOGF%
)
IF  EXIST "%LOG_CONF2%" (
  Echo Copying %LOG_CONF2% %CONF_DIR%  >> %LOGF%
  COPY /Y %LOG_CONF2% %CONF_DIR%  >> %LOGF%
)
