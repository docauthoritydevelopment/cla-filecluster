CREATE DATABASE  IF NOT EXISTS `da_1_5` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `da_1_5`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: da_1_5
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_name_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'default-account');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_items`
--

DROP TABLE IF EXISTS `acl_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cla_file_id` bigint(20) NOT NULL,
  `entity` varchar(1024) NOT NULL,
  `level` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type_idx` (`type`),
  KEY `cla_file_id_idx` (`cla_file_id`),
  KEY `entity_idx` (`entity`(255))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_items`
--

LOCK TABLES `acl_items` WRITE;
/*!40000 ALTER TABLE `acl_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aclcategory_filter_acl`
--

DROP TABLE IF EXISTS `aclcategory_filter_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aclcategory_filter_acl` (
  `aclcategory_filter_id` int(11) NOT NULL,
  `acl` varchar(255) DEFAULT NULL,
  KEY `FK_60bbwiexgmamjwyquo43egx2u` (`aclcategory_filter_id`),
  CONSTRAINT `FK_60bbwiexgmamjwyquo43egx2u` FOREIGN KEY (`aclcategory_filter_id`) REFERENCES `category_filters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aclcategory_filter_acl`
--

LOCK TABLES `aclcategory_filter_acl` WRITE;
/*!40000 ALTER TABLE `aclcategory_filter_acl` DISABLE KEYS */;
/*!40000 ALTER TABLE `aclcategory_filter_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `act_file_action_desired`
--

DROP TABLE IF EXISTS `act_file_action_desired`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `act_file_action_desired` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action_class` int(11) DEFAULT NULL,
  `action_trigger_id` bigint(20) NOT NULL,
  `cla_file_id` bigint(20) NOT NULL,
  `properties` varchar(4096) DEFAULT NULL,
  `url` varchar(4096) DEFAULT NULL,
  `action_definition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_shssku9x89u3u8smqs90xwci` (`cla_file_id`),
  KEY `UK_ci0sis7edvxh5u6dhe0q8t553` (`action_class`),
  KEY `UK_hv5bfug2sdwis4o5o7x7i1ai5` (`action_trigger_id`),
  KEY `FK_1aqcpt8u4tva11yi1a76u2jxj` (`action_definition_id`),
  CONSTRAINT `FK_1aqcpt8u4tva11yi1a76u2jxj` FOREIGN KEY (`action_definition_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `act_file_action_desired`
--

LOCK TABLES `act_file_action_desired` WRITE;
/*!40000 ALTER TABLE `act_file_action_desired` DISABLE KEYS */;
/*!40000 ALTER TABLE `act_file_action_desired` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_class_executor_def`
--

DROP TABLE IF EXISTS `action_class_executor_def`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_class_executor_def` (
  `id` varchar(255) NOT NULL,
  `action_class` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_class_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_class_executor_def`
--

LOCK TABLES `action_class_executor_def` WRITE;
/*!40000 ALTER TABLE `action_class_executor_def` DISABLE KEYS */;
INSERT INTO `action_class_executor_def` VALUES ('ACCESS_MANAGEMENT_SCRIPT_GENERATOR',0,'Creates permission altering scripts','com.cla.filecluster.actions.executors.AccessManagementScriptGenerator','Access Management Script Generator'),('DLP_POLICY_SCRIPT_GENERATOR\'',3,'Creates DLP policy altering scripts','com.cla.filecluster.actions.executors.DlpScriptGenerator','Creates DLP policy altering scripts'),('FILE_PROPERTY_MANIPULATOR_SCRIPT_GENERATOR',2,'Creates file properties altering scripts','com.cla.filecluster.actions.executors.FileTaggingScriptGenerator','File Properties Manipulation Script Generator'),('IONIC_ENCRYPTION_SCRIPT_GENERATOR',4,'Creates ionic encryption scripts','com.cla.filecluster.actions.executors.IonicEncryptionScriptGenerator','Creates ionic encryption scripts'),('REPORTING_CSV_GENERATOR',7,'Creates a CSV report on the files','com.cla.filecluster.actions.executors.ReportingCsvGenerator','Reporting CSV generator'),('STORAGE_POLICY_SCRIPT_GENERATOR',6,'Create scripts that move/copy files','com.cla.filecluster.actions.executors.StoragePolicyScriptGenerator','Storage Policy Script Generator');
/*!40000 ALTER TABLE `action_class_executor_def` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_definition`
--

DROP TABLE IF EXISTS `action_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_definition` (
  `id` varchar(255) NOT NULL,
  `action_class` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_method_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_definition`
--

LOCK TABLES `action_definition` WRITE;
/*!40000 ALTER TABLE `action_definition` DISABLE KEYS */;
INSERT INTO `action_definition` VALUES ('ADD_PERMISSION',0,'Grunt file access by user/group','addPermission','Add permission'),('ADD_PROPERTY_VALUE',2,'Add a value to the properties of a file','addPropertyValue','Add file property value'),('ALLOW_EMAIL_DLP',3,'Allow file to be sent to email (list) through DLP','allowEmail','Allow email list through DLP'),('COPY_FILE',6,'Copy a file to a different location','copyFile','Copy a file'),('DECRYPT_FILE',4,'Decrypt file','decryptFile','Decrypt file'),('DENY_EMAIL_DLP',3,'Block file to be sent to email (list) through DLP','denyEmail','Deny email list through DLP'),('ENCRYPT_FILE',4,'Encrypt file','encryptFile','Encrypt file'),('MOVE_FILE',6,'Move a file to a different location','moveFile','Move a file'),('REMOVE_PERMISSION',0,'Deny file access by user/group','removePermission','Remove permission'),('REM_PROPERTY_VALUE',2,'Remove a value from the properties of a file','remPropertyValue','Remove file property value'),('REPORT_FILE_DETAILS',7,'Add the file to a report','reportFile','Report on the file'),('SET_PROPERTY_VALUE',2,'Override the properties of a file','setPropertyValues','Set file property value');
/*!40000 ALTER TABLE `action_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_execution_task_status`
--

DROP TABLE IF EXISTS `action_execution_task_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_execution_task_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `errors_count` bigint(20) NOT NULL,
  `first_error` varchar(4096) DEFAULT NULL,
  `progress_mark` bigint(20) NOT NULL,
  `progress_max_mark` bigint(20) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `trigger_time` bigint(20) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_t7ammbfd2vkosb6fp405ut7xw` (`trigger_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_execution_task_status`
--

LOCK TABLES `action_execution_task_status` WRITE;
/*!40000 ALTER TABLE `action_execution_task_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_execution_task_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_instance`
--

DROP TABLE IF EXISTS `action_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parameters` varchar(255) DEFAULT NULL,
  `action_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eek9qnr3wvjcn2j6fxj884a7q` (`action_id`),
  CONSTRAINT `FK_eek9qnr3wvjcn2j6fxj884a7q` FOREIGN KEY (`action_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_instance`
--

LOCK TABLES `action_instance` WRITE;
/*!40000 ALTER TABLE `action_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `action_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `action_parameter_definition`
--

DROP TABLE IF EXISTS `action_parameter_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `action_parameter_definition` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `action_definition_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lgj2m0w6d7mm95prl16hk7jn` (`action_definition_id`),
  CONSTRAINT `FK_lgj2m0w6d7mm95prl16hk7jn` FOREIGN KEY (`action_definition_id`) REFERENCES `action_definition` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `action_parameter_definition`
--

LOCK TABLES `action_parameter_definition` WRITE;
/*!40000 ALTER TABLE `action_parameter_definition` DISABLE KEYS */;
INSERT INTO `action_parameter_definition` VALUES ('fields','The list of fields that will be reported on the file','fields','.*','REPORT_FILE_DETAILS'),('targetFileCopy','The copy target','target','.*','COPY_FILE'),('targetFileEnc','The encryption target','target','.*','ENCRYPT_FILE'),('targetFileMove','The move target','target','.*','MOVE_FILE'),('tempFolderEnc','encrypted file will be saved here','tempFolder','.*','ENCRYPT_FILE'),('userDlpAllow','The user to allow email for','user','.*','ALLOW_EMAIL_DLP'),('userDlpDeny','The user to block email for','user','.*','DENY_EMAIL_DLP'),('userPermAdd','The user to add permission for','user','.*','ADD_PERMISSION'),('userPermRemove','The user to deny permission for','user','.*','REMOVE_PERMISSION');
/*!40000 ALTER TABLE `action_parameter_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_phase_details`
--

DROP TABLE IF EXISTS `audit_phase_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audit_phase_details` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `count` bigint(20) NOT NULL,
  `count_max_mark` bigint(20) NOT NULL,
  `end` bigint(20) DEFAULT NULL,
  `error` varchar(4096) DEFAULT NULL,
  `errors_count` int(11) NOT NULL,
  `file_crawl_phase` varchar(255) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  `run_id` bigint(20) DEFAULT NULL,
  `start` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_phase_details`
--

LOCK TABLES `audit_phase_details` WRITE;
/*!40000 ALTER TABLE `audit_phase_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_phase_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_list`
--

DROP TABLE IF EXISTS `biz_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `biz_list_item_type` int(11) NOT NULL,
  `biz_list_solr_id` varchar(255) DEFAULT NULL,
  `biz_list_source` varchar(255) DEFAULT NULL,
  `creation_time_stamp` bigint(20) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `entity_list_encryption_type` int(11) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  `extraction_session_id` bigint(20) NOT NULL,
  `last_update_time_stamp` bigint(20) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `rules_file_name` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_acc` (`name`,`account_id`),
  KEY `name_idx` (`name`),
  KEY `FK_heiks1tibj5hvtiq29dqsr3gi` (`account_id`),
  CONSTRAINT `FK_heiks1tibj5hvtiq29dqsr3gi` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_list`
--

LOCK TABLES `biz_list` WRITE;
/*!40000 ALTER TABLE `biz_list` DISABLE KEYS */;
INSERT INTO `biz_list` VALUES (1,0,'blt.1',NULL,NULL,'predefined bizList',NULL,NULL,0,NULL,'Popular Names US','./config/extraction/extraction-rules-NameOnly.txt','NameOnly',1);
/*!40000 ALTER TABLE `biz_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_list_item_extraction`
--

DROP TABLE IF EXISTS `biz_list_item_extraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_list_item_extraction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agg_min_count` int(11) NOT NULL,
  `biz_list_id` bigint(20) NOT NULL,
  `biz_list_item_sid` varchar(255) NOT NULL,
  `biz_list_item_type` int(11) NOT NULL,
  `cla_file_id` bigint(20) NOT NULL,
  `dirty` bit(1) NOT NULL,
  `extraction_session_id` bigint(20) NOT NULL,
  `rule_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bl_e_fid` (`cla_file_id`),
  KEY `bl_e_dirty_bli` (`dirty`,`biz_list_id`),
  KEY `bl_e_bli` (`biz_list_id`),
  KEY `bl_e_dirty_f` (`dirty`,`cla_file_id`),
  KEY `bl_e_dirty` (`dirty`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_list_item_extraction`
--

LOCK TABLES `biz_list_item_extraction` WRITE;
/*!40000 ALTER TABLE `biz_list_item_extraction` DISABLE KEYS */;
/*!40000 ALTER TABLE `biz_list_item_extraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_role`
--

DROP TABLE IF EXISTS `biz_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_idx` (`name`),
  UNIQUE KEY `displayname_idx` (`display_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_role`
--

LOCK TABLES `biz_role` WRITE;
/*!40000 ALTER TABLE `biz_role` DISABLE KEYS */;
INSERT INTO `biz_role` VALUES (1,'\0','Admin role','Admin','admin'),(2,'\0','Operator role','Operator','operator'),(3,'\0','View role','Viewer','viewer');
/*!40000 ALTER TABLE `biz_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_role_roles`
--

DROP TABLE IF EXISTS `biz_role_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `biz_role_roles` (
  `biz_role_id` bigint(20) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`biz_role_id`,`roles_id`),
  KEY `FK_f6lpivum1i4gy0h6hv19aeaoq` (`roles_id`),
  CONSTRAINT `FK_f6lpivum1i4gy0h6hv19aeaoq` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `FK_q1nxatwveyaa6wt2i9xgqeqk6` FOREIGN KEY (`biz_role_id`) REFERENCES `biz_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_role_roles`
--

LOCK TABLES `biz_role_roles` WRITE;
/*!40000 ALTER TABLE `biz_role_roles` DISABLE KEYS */;
INSERT INTO `biz_role_roles` VALUES (1,1),(1,2),(1,3),(2,3),(1,4),(2,4);
/*!40000 ALTER TABLE `biz_role_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bl_item_alias`
--

DROP TABLE IF EXISTS `bl_item_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bl_item_alias` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) DEFAULT NULL,
  `encrypted_alias` varchar(255) DEFAULT NULL,
  `entity_sid` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_15ja8pg2y2po5doob4qeqft7i` (`entity_sid`),
  CONSTRAINT `FK_15ja8pg2y2po5doob4qeqft7i` FOREIGN KEY (`entity_sid`) REFERENCES `bl_item_simple` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bl_item_alias`
--

LOCK TABLES `bl_item_alias` WRITE;
/*!40000 ALTER TABLE `bl_item_alias` DISABLE KEYS */;
/*!40000 ALTER TABLE `bl_item_alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bl_item_simple`
--

DROP TABLE IF EXISTS `bl_item_simple`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bl_item_simple` (
  `dtype` varchar(31) NOT NULL,
  `sid` varchar(255) NOT NULL,
  `entity_id` varchar(255) NOT NULL,
  `import_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `source` int(11) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `encrypted_fields_json` varchar(4096) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `biz_list_id` bigint(20) NOT NULL,
  PRIMARY KEY (`sid`),
  KEY `name_idx` (`name`),
  KEY `entity_id_idx` (`entity_id`),
  KEY `state_idx` (`state`),
  KEY `FK_dk03rakxm6e63vlo854paab57` (`biz_list_id`),
  CONSTRAINT `FK_dk03rakxm6e63vlo854paab57` FOREIGN KEY (`biz_list_id`) REFERENCES `biz_list` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bl_item_simple`
--

LOCK TABLES `bl_item_simple` WRITE;
/*!40000 ALTER TABLE `bl_item_simple` DISABLE KEYS */;
INSERT INTO `bl_item_simple` VALUES ('ConsumerBizListItem','1_Aaron','Aaron',1476771747142,'Aaron',NULL,0,'{\"CNAME\":\"449A36B6689D841D\",\"ID\":\"B8C37E33DEFDE51C\",\"NAME\":\"449A36B6689D841D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Abbott','Abbott',1476771747142,'Abbott',NULL,0,'{\"CNAME\":\"93E8524F77E2EA61\",\"ID\":\"FBA9D88164F3E2D9\",\"NAME\":\"93E8524F77E2EA61\"}',NULL,NULL,1),('ConsumerBizListItem','1_Abby','Abby',1476771747142,'Abby',NULL,0,'{\"CNAME\":\"BB2E4C1498162EB6\",\"ID\":\"AA68C75C4A77C87F\",\"NAME\":\"BB2E4C1498162EB6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Abigail','Abigail',1476771747142,'Abigail',NULL,0,'{\"CNAME\":\"37F299007792A4E9\",\"ID\":\"FED33392D3A48AA1\",\"NAME\":\"37F299007792A4E9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Acevedo','Acevedo',1476771747142,'Acevedo',NULL,0,'{\"CNAME\":\"817E0801005101BA\",\"ID\":\"2387337BA1E0B024\",\"NAME\":\"817E0801005101BA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Acosta','Acosta',1476771747142,'Acosta',NULL,0,'{\"CNAME\":\"3912678BA738F220\",\"ID\":\"9246444D94F081E3\",\"NAME\":\"3912678BA738F220\"}',NULL,NULL,1),('ConsumerBizListItem','1_Adam','Adam',1476771747142,'Adam',NULL,0,'{\"CNAME\":\"1D7C2923C1684726\",\"ID\":\"D7322ED717DEDF1E\",\"NAME\":\"1D7C2923C1684726\"}',NULL,NULL,1),('ConsumerBizListItem','1_Adams','Adams',1476771747142,'Adams',NULL,0,'{\"CNAME\":\"3CC4A9A458D45578\",\"ID\":\"1587965FB4D4B5AF\",\"NAME\":\"3CC4A9A458D45578\"}',NULL,NULL,1),('ConsumerBizListItem','1_Adkins','Adkins',1476771747142,'Adkins',NULL,0,'{\"CNAME\":\"E63E9FDAB369BC31\",\"ID\":\"31B3B31A1C2F8A37\",\"NAME\":\"E63E9FDAB369BC31\"}',NULL,NULL,1),('ConsumerBizListItem','1_Aguilar','Aguilar',1476771747142,'Aguilar',NULL,0,'{\"CNAME\":\"6F5E464B6DBFA186\",\"ID\":\"1E48C4420B7073BC\",\"NAME\":\"6F5E464B6DBFA186\"}',NULL,NULL,1),('ConsumerBizListItem','1_Aguirre','Aguirre',1476771747142,'Aguirre',NULL,0,'{\"CNAME\":\"EBB3FE6806BFDAED\",\"ID\":\"7F975A56C761DB65\",\"NAME\":\"EBB3FE6806BFDAED\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alex','Alex',1476771747142,'Alex',NULL,0,'{\"CNAME\":\"534B44A19BF18D20\",\"ID\":\"F33BA15EFFA5C10E\",\"NAME\":\"534B44A19BF18D20\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alexander','Alexander',1476771747142,'Alexander',NULL,0,'{\"CNAME\":\"DD22141ACB5EA065\",\"ID\":\"6B180037ABBEBEA9\",\"NAME\":\"DD22141ACB5EA065\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alicia','Alicia',1476771747142,'Alicia',NULL,0,'{\"CNAME\":\"E94EF563867E9C9D\",\"ID\":\"766D856EF1A6B02F\",\"NAME\":\"E94EF563867E9C9D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alisha','Alisha',1476771747142,'Alisha',NULL,0,'{\"CNAME\":\"DDDF0AC818211584\",\"ID\":\"298923C8190045E9\",\"NAME\":\"DDDF0AC818211584\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alison','Alison',1476771747142,'Alison',NULL,0,'{\"CNAME\":\"38B05A6EA700FA67\",\"ID\":\"8FE2621D8E716B0\",\"NAME\":\"38B05A6EA700FA67\"}',NULL,NULL,1),('ConsumerBizListItem','1_Allen','Allen',1476771747142,'Allen',NULL,0,'{\"CNAME\":\"A34C3D45B6018D3F\",\"ID\":\"5D616DD38211EBB5\",\"NAME\":\"A34C3D45B6018D3F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Allison','Allison',1476771747142,'Allison',NULL,0,'{\"CNAME\":\"4651D80CFA79F493\",\"ID\":\"EF50C335CCA9F340\",\"NAME\":\"4651D80CFA79F493\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alvarado','Alvarado',1476771747142,'Alvarado',NULL,0,'{\"CNAME\":\"D5CCA9DF47002E1F\",\"ID\":\"3E0704B5690A2DE\",\"NAME\":\"D5CCA9DF47002E1F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Alvarez','Alvarez',1476771747142,'Alvarez',NULL,0,'{\"CNAME\":\"E438D4969C839B98\",\"ID\":\"65CC2C8205A05D73\",\"NAME\":\"E438D4969C839B98\"}',NULL,NULL,1),('ConsumerBizListItem','1_Amanda','Amanda',1476771747142,'Amanda',NULL,0,'{\"CNAME\":\"6209804952225AB3\",\"ID\":\"768281A05DA9F27\",\"NAME\":\"6209804952225AB3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Andersen','Andersen',1476771747142,'Andersen',NULL,0,'{\"CNAME\":\"C0B53054FFEFC149\",\"ID\":\"93D65641FF3F1586\",\"NAME\":\"C0B53054FFEFC149\"}',NULL,NULL,1),('ConsumerBizListItem','1_Anderson','Anderson',1476771747142,'Anderson',NULL,0,'{\"CNAME\":\"89BA023086E37A34\",\"ID\":\"CE5140DF15D046A6\",\"NAME\":\"89BA023086E37A34\"}',NULL,NULL,1),('ConsumerBizListItem','1_Andrade','Andrade',1476771747142,'Andrade',NULL,0,'{\"CNAME\":\"EC8225AF0776B3EE\",\"ID\":\"21BBC7EE20B7113\",\"NAME\":\"EC8225AF0776B3EE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Andrea','Andrea',1476771747142,'Andrea',NULL,0,'{\"CNAME\":\"1C42F9C1CA2F6544\",\"ID\":\"82B8A3434904411A\",\"NAME\":\"1C42F9C1CA2F6544\"}',NULL,NULL,1),('ConsumerBizListItem','1_Andrew','Andrew',1476771747142,'Andrew',NULL,0,'{\"CNAME\":\"D914E3ECF6CC4811\",\"ID\":\"24146DB4EB48C718\",\"NAME\":\"D914E3ECF6CC4811\"}',NULL,NULL,1),('ConsumerBizListItem','1_Andrews','Andrews',1476771747142,'Andrews',NULL,0,'{\"CNAME\":\"12E0A8A5DC75DF9B\",\"ID\":\"883E881BB4D22A7A\",\"NAME\":\"12E0A8A5DC75DF9B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Angela','Angela',1476771747142,'Angela',NULL,0,'{\"CNAME\":\"36388794BE2CF5F2\",\"ID\":\"3806734B256C27E4\",\"NAME\":\"36388794BE2CF5F2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Anna','Anna',1476771747142,'Anna',NULL,0,'{\"CNAME\":\"A70F9E38FF015AFA\",\"ID\":\"84D2004BF28A2095\",\"NAME\":\"A70F9E38FF015AFA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Anthony','Anthony',1476771747142,'Anthony',NULL,0,'{\"CNAME\":\"65FBEF05E01FAC39\",\"ID\":\"E515DF0D202AE52F\",\"NAME\":\"65FBEF05E01FAC39\"}',NULL,NULL,1),('ConsumerBizListItem','1_Archer','Archer',1476771747142,'Archer',NULL,0,'{\"CNAME\":\"97FBDF62D73918BD\",\"ID\":\"AFDEC7005CC9F143\",\"NAME\":\"97FBDF62D73918BD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Arellano','Arellano',1476771747142,'Arellano',NULL,0,'{\"CNAME\":\"B30D2A1ED1874CD9\",\"ID\":\"995E1FDA4A2B5F55\",\"NAME\":\"B30D2A1ED1874CD9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Arias','Arias',1476771747142,'Arias',NULL,0,'{\"CNAME\":\"54A9F17A5CD5C87C\",\"ID\":\"E17184BCB70DCF39\",\"NAME\":\"54A9F17A5CD5C87C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Armstrong','Armstrong',1476771747142,'Armstrong',NULL,0,'{\"CNAME\":\"D5DACC43370FDFE2\",\"ID\":\"BDB106A0560C4E46\",\"NAME\":\"D5DACC43370FDFE2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Arnold','Arnold',1476771747142,'Arnold',NULL,0,'{\"CNAME\":\"49A30D03C669A09F\",\"ID\":\"A34BACF839B92377\",\"NAME\":\"49A30D03C669A09F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Arroyo','Arroyo',1476771747142,'Arroyo',NULL,0,'{\"CNAME\":\"9DE2DC4967B054F5\",\"ID\":\"83FA5A432AE55C25\",\"NAME\":\"9DE2DC4967B054F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ashlee','Ashlee',1476771747142,'Ashlee',NULL,0,'{\"CNAME\":\"EAE021356DCAF663\",\"ID\":\"EDDB904A6DB77375\",\"NAME\":\"EAE021356DCAF663\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ashley','Ashley',1476771747142,'Ashley',NULL,0,'{\"CNAME\":\"ADFF44C5102FCA27\",\"ID\":\"6D70CB65D1521172\",\"NAME\":\"ADFF44C5102FCA27\"}',NULL,NULL,1),('ConsumerBizListItem','1_Atkins','Atkins',1476771747142,'Atkins',NULL,0,'{\"CNAME\":\"A1D7C2ED53BCE22D\",\"ID\":\"27ED0FB950B856B0\",\"NAME\":\"A1D7C2ED53BCE22D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Atkinson','Atkinson',1476771747142,'Atkinson',NULL,0,'{\"CNAME\":\"6F45C29D0F3B1891\",\"ID\":\"537D9B6C927223C7\",\"NAME\":\"6F45C29D0F3B1891\"}',NULL,NULL,1),('ConsumerBizListItem','1_Avery','Avery',1476771747142,'Avery',NULL,0,'{\"CNAME\":\"3E10E87DA0F93B98\",\"ID\":\"D736BB10D83A904A\",\"NAME\":\"3E10E87DA0F93B98\"}',NULL,NULL,1),('ConsumerBizListItem','1_Avila','Avila',1476771747142,'Avila',NULL,0,'{\"CNAME\":\"56D20F8529FAE677\",\"ID\":\"9AC403DA7947A183\",\"NAME\":\"56D20F8529FAE677\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ayala','Ayala',1476771747142,'Ayala',NULL,0,'{\"CNAME\":\"FB15EE9986158D35\",\"ID\":\"B9141AFF1412DC76\",\"NAME\":\"FB15EE9986158D35\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ayers','Ayers',1476771747142,'Ayers',NULL,0,'{\"CNAME\":\"C67B8EA4CA897DDE\",\"ID\":\"1019C8091693EF5C\",\"NAME\":\"C67B8EA4CA897DDE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bailey','Bailey',1476771747142,'Bailey',NULL,0,'{\"CNAME\":\"7D0DF4F46505D83B\",\"ID\":\"A0E2A2C563D57DF2\",\"NAME\":\"7D0DF4F46505D83B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Baird','Baird',1476771747142,'Baird',NULL,0,'{\"CNAME\":\"75BB3818B51A5DE\",\"ID\":\"1579779B98CE9EDB\",\"NAME\":\"75BB3818B51A5DE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Baldwin','Baldwin',1476771747142,'Baldwin',NULL,0,'{\"CNAME\":\"423AC084531624EC\",\"ID\":\"20B5E1CF8694AF7A\",\"NAME\":\"423AC084531624EC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ballard','Ballard',1476771747142,'Ballard',NULL,0,'{\"CNAME\":\"71E6A1C0AC2ADDB\",\"ID\":\"7CCE53CF90577442\",\"NAME\":\"71E6A1C0AC2ADDB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barajas','Barajas',1476771747142,'Barajas',NULL,0,'{\"CNAME\":\"4BFC3E6F5B1015C8\",\"ID\":\"58C54802A9FB9526\",\"NAME\":\"4BFC3E6F5B1015C8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barker','Barker',1476771747142,'Barker',NULL,0,'{\"CNAME\":\"8EC9004FFE6042B0\",\"ID\":\"5055CBF43FAC3F7E\",\"NAME\":\"8EC9004FFE6042B0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barnes','Barnes',1476771747142,'Barnes',NULL,0,'{\"CNAME\":\"19BF0AC25FA551CA\",\"ID\":\"456AC9B0D15A8B7F\",\"NAME\":\"19BF0AC25FA551CA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barnett','Barnett',1476771747142,'Barnett',NULL,0,'{\"CNAME\":\"EE67C9D291E68344\",\"ID\":\"F4DD765C12F2EF67\",\"NAME\":\"EE67C9D291E68344\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barr','Barr',1476771747142,'Barr',NULL,0,'{\"CNAME\":\"8978C98BB5A48C2F\",\"ID\":\"7FEC306D1E665BC9\",\"NAME\":\"8978C98BB5A48C2F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barrera','Barrera',1476771747142,'Barrera',NULL,0,'{\"CNAME\":\"4478033796FD7C45\",\"ID\":\"DB576A7D2453575F\",\"NAME\":\"4478033796FD7C45\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barrett','Barrett',1476771747142,'Barrett',NULL,0,'{\"CNAME\":\"5B1424225FF2A849\",\"ID\":\"53C04118DF112C13\",\"NAME\":\"5B1424225FF2A849\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barron','Barron',1476771747142,'Barron',NULL,0,'{\"CNAME\":\"2F71B74A272CAFE0\",\"ID\":\"4CA82782C5372A54\",\"NAME\":\"2F71B74A272CAFE0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barry','Barry',1476771747142,'Barry',NULL,0,'{\"CNAME\":\"F9D900B378F3389D\",\"ID\":\"D8700CBD38CC9F30\",\"NAME\":\"F9D900B378F3389D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bartlett','Bartlett',1476771747142,'Bartlett',NULL,0,'{\"CNAME\":\"9B6EC59A19941F7B\",\"ID\":\"B4D168B48157C623\",\"NAME\":\"9B6EC59A19941F7B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Barton','Barton',1476771747142,'Barton',NULL,0,'{\"CNAME\":\"3C895F944EE1CDD6\",\"ID\":\"4A08142C38DBE374\",\"NAME\":\"3C895F944EE1CDD6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bauer','Bauer',1476771747142,'Bauer',NULL,0,'{\"CNAME\":\"BA69542E8644B997\",\"ID\":\"299A23A2291E2126\",\"NAME\":\"BA69542E8644B997\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bautista','Bautista',1476771747142,'Bautista',NULL,0,'{\"CNAME\":\"C143E8E539ADC263\",\"ID\":\"A89CF525E1D9F04D\",\"NAME\":\"C143E8E539ADC263\"}',NULL,NULL,1),('ConsumerBizListItem','1_Baxter','Baxter',1476771747142,'Baxter',NULL,0,'{\"CNAME\":\"1A912CA20FB31005\",\"ID\":\"CD89FEF7FFDD490D\",\"NAME\":\"1A912CA20FB31005\"}',NULL,NULL,1),('ConsumerBizListItem','1_Beasley','Beasley',1476771747142,'Beasley',NULL,0,'{\"CNAME\":\"83861D56E24D8A79\",\"ID\":\"6C340F25839E6ACD\",\"NAME\":\"83861D56E24D8A79\"}',NULL,NULL,1),('ConsumerBizListItem','1_Beck','Beck',1476771747142,'Beck',NULL,0,'{\"CNAME\":\"C9813838145BD948\",\"ID\":\"2290A7385ED77CC5\",\"NAME\":\"C9813838145BD948\"}',NULL,NULL,1),('ConsumerBizListItem','1_Becker','Becker',1476771747142,'Becker',NULL,0,'{\"CNAME\":\"6462416CFCBBCF58\",\"ID\":\"A2137A2AE8E39B50\",\"NAME\":\"6462416CFCBBCF58\"}',NULL,NULL,1),('ConsumerBizListItem','1_Beltran','Beltran',1476771747142,'Beltran',NULL,0,'{\"CNAME\":\"F7999E2AE9A91C18\",\"ID\":\"43DD49B4FDB9BEDE\",\"NAME\":\"F7999E2AE9A91C18\"}',NULL,NULL,1),('ConsumerBizListItem','1_Benitez','Benitez',1476771747142,'Benitez',NULL,0,'{\"CNAME\":\"921FDE0433A99A6C\",\"ID\":\"31857B449C407203\",\"NAME\":\"921FDE0433A99A6C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Benjamin','Benjamin',1476771747142,'Benjamin',NULL,0,'{\"CNAME\":\"5D9F71B71B207B9E\",\"ID\":\"53ADAF494DC89EF7\",\"NAME\":\"5D9F71B71B207B9E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bennett','Bennett',1476771747142,'Bennett',NULL,0,'{\"CNAME\":\"A0734C7EC07D2579\",\"ID\":\"FC2C7C47B918D0C2\",\"NAME\":\"A0734C7EC07D2579\"}',NULL,NULL,1),('ConsumerBizListItem','1_Benson','Benson',1476771747142,'Benson',NULL,0,'{\"CNAME\":\"EBCFD5A11D7CF5BA\",\"ID\":\"DC58E3A306451C9D\",\"NAME\":\"EBCFD5A11D7CF5BA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bentley','Bentley',1476771747142,'Bentley',NULL,0,'{\"CNAME\":\"298B8014DA335621\",\"ID\":\"1B0114C51CC532ED\",\"NAME\":\"298B8014DA335621\"}',NULL,NULL,1),('ConsumerBizListItem','1_Benton','Benton',1476771747142,'Benton',NULL,0,'{\"CNAME\":\"851250C5A62CDEE4\",\"ID\":\"A1519DE5B5D44B31\",\"NAME\":\"851250C5A62CDEE4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Berg','Berg',1476771747142,'Berg',NULL,0,'{\"CNAME\":\"1E059C910ACC559\",\"ID\":\"390E982518A50E28\",\"NAME\":\"1E059C910ACC559\"}',NULL,NULL,1),('ConsumerBizListItem','1_Berger','Berger',1476771747142,'Berger',NULL,0,'{\"CNAME\":\"137FC712B0B94E91\",\"ID\":\"708F3CF8100D5E71\",\"NAME\":\"137FC712B0B94E91\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bernard','Bernard',1476771747142,'Bernard',NULL,0,'{\"CNAME\":\"78D6810E1299959F\",\"ID\":\"46072631582FC240\",\"NAME\":\"78D6810E1299959F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Berry','Berry',1476771747142,'Berry',NULL,0,'{\"CNAME\":\"5076E33378DB3B35\",\"ID\":\"8A1E808B55FDE945\",\"NAME\":\"5076E33378DB3B35\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bethany','Bethany',1476771747142,'Bethany',NULL,0,'{\"CNAME\":\"4249EC7015E8A9F8\",\"ID\":\"62DDB6C727310E7\",\"NAME\":\"4249EC7015E8A9F8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blackburn','Blackburn',1476771747142,'Blackburn',NULL,0,'{\"CNAME\":\"10D17F83D9D742B3\",\"ID\":\"522A9AE9A99880D3\",\"NAME\":\"10D17F83D9D742B3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blackwell','Blackwell',1476771747142,'Blackwell',NULL,0,'{\"CNAME\":\"20CEDCEBBB8F438B\",\"ID\":\"43BAA6762FA81BB4\",\"NAME\":\"20CEDCEBBB8F438B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blair','Blair',1476771747142,'Blair',NULL,0,'{\"CNAME\":\"695D3555929F09CC\",\"ID\":\"731C83DB8D2FF01B\",\"NAME\":\"695D3555929F09CC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blake','Blake',1476771747142,'Blake',NULL,0,'{\"CNAME\":\"3AA49EC6BFC91064\",\"ID\":\"36A16A2505369E0C\",\"NAME\":\"3AA49EC6BFC91064\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blanchard','Blanchard',1476771747142,'Blanchard',NULL,0,'{\"CNAME\":\"ECB077F4DD1FEBC3\",\"ID\":\"818F4654ED39A1C1\",\"NAME\":\"ECB077F4DD1FEBC3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blankenship','Blankenship',1476771747142,'Blankenship',NULL,0,'{\"CNAME\":\"426EE52557DFC257\",\"ID\":\"E5E63DA79FCD2BEB\",\"NAME\":\"426EE52557DFC257\"}',NULL,NULL,1),('ConsumerBizListItem','1_Blevins','Blevins',1476771747142,'Blevins',NULL,0,'{\"CNAME\":\"A5FBA68F4217051D\",\"ID\":\"9F36407EAD0629FC\",\"NAME\":\"A5FBA68F4217051D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bolton','Bolton',1476771747142,'Bolton',NULL,0,'{\"CNAME\":\"C65D33237DC2AB50\",\"ID\":\"4F16C818875D9FCB\",\"NAME\":\"C65D33237DC2AB50\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bonilla','Bonilla',1476771747142,'Bonilla',NULL,0,'{\"CNAME\":\"50A202E60B2F9F64\",\"ID\":\"D91D1B4D82419DE8\",\"NAME\":\"50A202E60B2F9F64\"}',NULL,NULL,1),('ConsumerBizListItem','1_Booker','Booker',1476771747142,'Booker',NULL,0,'{\"CNAME\":\"79F243D557026E88\",\"ID\":\"A26398DCA6F47B49\",\"NAME\":\"79F243D557026E88\"}',NULL,NULL,1),('ConsumerBizListItem','1_Boone','Boone',1476771747142,'Boone',NULL,0,'{\"CNAME\":\"D4495034EF09E08D\",\"ID\":\"B1563A78EC593375\",\"NAME\":\"D4495034EF09E08D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bowers','Bowers',1476771747142,'Bowers',NULL,0,'{\"CNAME\":\"E5EED26A407DA376\",\"ID\":\"3F5446139179452\",\"NAME\":\"E5EED26A407DA376\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bowman','Bowman',1476771747142,'Bowman',NULL,0,'{\"CNAME\":\"9AFEDD56E37131E5\",\"ID\":\"8B4066554730DDFA\",\"NAME\":\"9AFEDD56E37131E5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Boyd','Boyd',1476771747142,'Boyd',NULL,0,'{\"CNAME\":\"3C51EC0B06B6C9CC\",\"ID\":\"754DDA4B1BA34C6F\",\"NAME\":\"3C51EC0B06B6C9CC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Boyer','Boyer',1476771747142,'Boyer',NULL,0,'{\"CNAME\":\"BF0E1BD668644504\",\"ID\":\"6A2FEEF8ED6A9FE7\",\"NAME\":\"BF0E1BD668644504\"}',NULL,NULL,1),('ConsumerBizListItem','1_Boyle','Boyle',1476771747142,'Boyle',NULL,0,'{\"CNAME\":\"288A92A44C72633C\",\"ID\":\"68B1FBE7F16E4AE3\",\"NAME\":\"288A92A44C72633C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bradford','Bradford',1476771747142,'Bradford',NULL,0,'{\"CNAME\":\"B6D9F15EB4EDFAE5\",\"ID\":\"41BFD20A38BB1B0B\",\"NAME\":\"B6D9F15EB4EDFAE5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bradley','Bradley',1476771747142,'Bradley',NULL,0,'{\"CNAME\":\"8FCD96EB01E8614D\",\"ID\":\"D6723E7CD6735DF6\",\"NAME\":\"8FCD96EB01E8614D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bradshaw','Bradshaw',1476771747142,'Bradshaw',NULL,0,'{\"CNAME\":\"3DE9D4A2FE62F649\",\"ID\":\"4E2545F819E67F06\",\"NAME\":\"3DE9D4A2FE62F649\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brady','Brady',1476771747142,'Brady',NULL,0,'{\"CNAME\":\"5BF984E94F0A3177\",\"ID\":\"DB2B4182156B2F1F\",\"NAME\":\"5BF984E94F0A3177\"}',NULL,NULL,1),('ConsumerBizListItem','1_Branch','Branch',1476771747142,'Branch',NULL,0,'{\"CNAME\":\"9603A224B40D7B67\",\"ID\":\"A5E0FF62BE0B0845\",\"NAME\":\"9603A224B40D7B67\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brandi','Brandi',1476771747142,'Brandi',NULL,0,'{\"CNAME\":\"BB40981B6F5EDA22\",\"ID\":\"A016070970114070\",\"NAME\":\"BB40981B6F5EDA22\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brandon','Brandon',1476771747142,'Brandon',NULL,0,'{\"CNAME\":\"FC275AC3498D6AB0\",\"ID\":\"1E6E0A04D20F5096\",\"NAME\":\"FC275AC3498D6AB0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brandt','Brandt',1476771747142,'Brandt',NULL,0,'{\"CNAME\":\"DAA5BB9C0113A453\",\"ID\":\"C6BFF625BDB03939\",\"NAME\":\"DAA5BB9C0113A453\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brandy','Brandy',1476771747142,'Brandy',NULL,0,'{\"CNAME\":\"6C29E9CC4042D972\",\"ID\":\"C667D53ACD899A97\",\"NAME\":\"6C29E9CC4042D972\"}',NULL,NULL,1),('ConsumerBizListItem','1_Braun','Braun',1476771747142,'Braun',NULL,0,'{\"CNAME\":\"33B1929F4A9A34B0\",\"ID\":\"AACE49C7D80767CF\",\"NAME\":\"33B1929F4A9A34B0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bray','Bray',1476771747142,'Bray',NULL,0,'{\"CNAME\":\"6BEE5221C6DC4B41\",\"ID\":\"4DA04049A062F5AD\",\"NAME\":\"6BEE5221C6DC4B41\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brennan','Brennan',1476771747142,'Brennan',NULL,0,'{\"CNAME\":\"2F7CDF67387A8EAC\",\"ID\":\"AF21D0C97DB2E27E\",\"NAME\":\"2F7CDF67387A8EAC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brent','Brent',1476771747142,'Brent',NULL,0,'{\"CNAME\":\"FDDD76411252E465\",\"ID\":\"C9F95A0A5AF052BF\",\"NAME\":\"FDDD76411252E465\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brett','Brett',1476771747142,'Brett',NULL,0,'{\"CNAME\":\"C1BED8B1918CCDB4\",\"ID\":\"E58CC5CA94270ACA\",\"NAME\":\"C1BED8B1918CCDB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brewer','Brewer',1476771747142,'Brewer',NULL,0,'{\"CNAME\":\"ACA39D8232DBD69C\",\"ID\":\"B9D487A30398D42E\",\"NAME\":\"ACA39D8232DBD69C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brian','Brian',1476771747142,'Brian',NULL,0,'{\"CNAME\":\"CBD44F8B5B48A51F\",\"ID\":\"8F1D43620BC6BB58\",\"NAME\":\"CBD44F8B5B48A51F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Briggs','Briggs',1476771747142,'Briggs',NULL,0,'{\"CNAME\":\"EDFDC0FA5E2AE6E3\",\"ID\":\"2CBCA44843A86453\",\"NAME\":\"EDFDC0FA5E2AE6E3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brittany','Brittany',1476771747142,'Brittany',NULL,0,'{\"CNAME\":\"7FECAF436EB80F4B\",\"ID\":\"B59C67BF196A4758\",\"NAME\":\"7FECAF436EB80F4B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brittney','Brittney',1476771747142,'Brittney',NULL,0,'{\"CNAME\":\"FC0224AD11E12740\",\"ID\":\"20D135F0F28185B8\",\"NAME\":\"FC0224AD11E12740\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brock','Brock',1476771747142,'Brock',NULL,0,'{\"CNAME\":\"DC0109407B4C65FD\",\"ID\":\"9C3B1830513CC3B8\",\"NAME\":\"DC0109407B4C65FD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brooke','Brooke',1476771747142,'Brooke',NULL,0,'{\"CNAME\":\"111EE1E76425203D\",\"ID\":\"D6EF5F7FA914C199\",\"NAME\":\"111EE1E76425203D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Brooks','Brooks',1476771747142,'Brooks',NULL,0,'{\"CNAME\":\"CDBD33F49D2A6CEF\",\"ID\":\"E19347E1C3CA0C0B\",\"NAME\":\"CDBD33F49D2A6CEF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Browning','Browning',1476771747142,'Browning',NULL,0,'{\"CNAME\":\"1C6AC33D8CB4A39E\",\"ID\":\"DD77279F7D325EEC\",\"NAME\":\"1C6AC33D8CB4A39E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bruce','Bruce',1476771747142,'Bruce',NULL,0,'{\"CNAME\":\"E8315CAA4EB8C2A2\",\"ID\":\"EEC27C419D0FE24\",\"NAME\":\"E8315CAA4EB8C2A2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bryan','Bryan',1476771747142,'Bryan',NULL,0,'{\"CNAME\":\"7D4EF62DE50874A4\",\"ID\":\"C60D060B946D6DD6\",\"NAME\":\"7D4EF62DE50874A4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bryant','Bryant',1476771747142,'Bryant',NULL,0,'{\"CNAME\":\"E5B79BBDB797144A\",\"ID\":\"8597A6CFA74DEFCB\",\"NAME\":\"E5B79BBDB797144A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Buchanan','Buchanan',1476771747142,'Buchanan',NULL,0,'{\"CNAME\":\"3A8316A1AC06D90C\",\"ID\":\"C6036A69BE21CB66\",\"NAME\":\"3A8316A1AC06D90C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Buckley','Buckley',1476771747142,'Buckley',NULL,0,'{\"CNAME\":\"E4131DE02A20B908\",\"ID\":\"3A15C7D0BBE60300\",\"NAME\":\"E4131DE02A20B908\"}',NULL,NULL,1),('ConsumerBizListItem','1_Bullock','Bullock',1476771747142,'Bullock',NULL,0,'{\"CNAME\":\"8BC4C04DCE06B122\",\"ID\":\"3B712DE48137572F\",\"NAME\":\"8BC4C04DCE06B122\"}',NULL,NULL,1),('ConsumerBizListItem','1_Burch','Burch',1476771747142,'Burch',NULL,0,'{\"CNAME\":\"9C4964BAAC830732\",\"ID\":\"2CFD4560539F887A\",\"NAME\":\"9C4964BAAC830732\"}',NULL,NULL,1),('ConsumerBizListItem','1_Burgess','Burgess',1476771747142,'Burgess',NULL,0,'{\"CNAME\":\"9A01312E96A50A16\",\"ID\":\"C7635BFD99248A2C\",\"NAME\":\"9A01312E96A50A16\"}',NULL,NULL,1),('ConsumerBizListItem','1_Burke','Burke',1476771747142,'Burke',NULL,0,'{\"CNAME\":\"25263B46ABC397CC\",\"ID\":\"C21002F464C5FC5B\",\"NAME\":\"25263B46ABC397CC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Burnett','Burnett',1476771747142,'Burnett',NULL,0,'{\"CNAME\":\"1D06D5A76A91E775\",\"ID\":\"FFEED84C7CB1AE7B\",\"NAME\":\"1D06D5A76A91E775\"}',NULL,NULL,1),('ConsumerBizListItem','1_Burns','Burns',1476771747142,'Burns',NULL,0,'{\"CNAME\":\"46DEFCE884D1BE32\",\"ID\":\"678A1491514B7F10\",\"NAME\":\"46DEFCE884D1BE32\"}',NULL,NULL,1),('ConsumerBizListItem','1_Burton','Burton',1476771747142,'Burton',NULL,0,'{\"CNAME\":\"C189E6726D2DC699\",\"ID\":\"3FE78A8ACF5FDA99\",\"NAME\":\"C189E6726D2DC699\"}',NULL,NULL,1),('ConsumerBizListItem','1_Byrd','Byrd',1476771747142,'Byrd',NULL,0,'{\"CNAME\":\"D8CDE5F4FDCBD97B\",\"ID\":\"69A5B5995110B36A\",\"NAME\":\"D8CDE5F4FDCBD97B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cabrera','Cabrera',1476771747142,'Cabrera',NULL,0,'{\"CNAME\":\"2AACD6088B178AA1\",\"ID\":\"4A213D37242BDCAD\",\"NAME\":\"2AACD6088B178AA1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cain','Cain',1476771747142,'Cain',NULL,0,'{\"CNAME\":\"733C5E338B817C69\",\"ID\":\"FE709C654EAC84D5\",\"NAME\":\"733C5E338B817C69\"}',NULL,NULL,1),('ConsumerBizListItem','1_Calderon','Calderon',1476771747142,'Calderon',NULL,0,'{\"CNAME\":\"F98DB2B09887B074\",\"ID\":\"571E0F7E2D992E73\",\"NAME\":\"F98DB2B09887B074\"}',NULL,NULL,1),('ConsumerBizListItem','1_Caldwell','Caldwell',1476771747142,'Caldwell',NULL,0,'{\"CNAME\":\"517434089B0B7AD3\",\"ID\":\"FD06B8EA02FE5B1C\",\"NAME\":\"517434089B0B7AD3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Caleb','Caleb',1476771747142,'Caleb',NULL,0,'{\"CNAME\":\"2F0154D7DB348840\",\"ID\":\"7C9D0B1F96AEBD7B\",\"NAME\":\"2F0154D7DB348840\"}',NULL,NULL,1),('ConsumerBizListItem','1_Calhoun','Calhoun',1476771747142,'Calhoun',NULL,0,'{\"CNAME\":\"A328C6BF1A97200D\",\"ID\":\"FD2C5E4680D9A01D\",\"NAME\":\"A328C6BF1A97200D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Callahan','Callahan',1476771747142,'Callahan',NULL,0,'{\"CNAME\":\"D9FC71BA2112DB04\",\"ID\":\"47A658229EB2368A\",\"NAME\":\"D9FC71BA2112DB04\"}',NULL,NULL,1),('ConsumerBizListItem','1_Camacho','Camacho',1476771747142,'Camacho',NULL,0,'{\"CNAME\":\"15E29073B36ACE1D\",\"ID\":\"801C14F07F972422\",\"NAME\":\"15E29073B36ACE1D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cameron','Cameron',1476771747142,'Cameron',NULL,0,'{\"CNAME\":\"BB36FEAAC46D7AAA\",\"ID\":\"C3E0C62EE91DB8DC\",\"NAME\":\"BB36FEAAC46D7AAA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Campbell','Campbell',1476771747142,'Campbell',NULL,0,'{\"CNAME\":\"3F0B8D8580D70015\",\"ID\":\"184260348236F955\",\"NAME\":\"3F0B8D8580D70015\"}',NULL,NULL,1),('ConsumerBizListItem','1_Campos','Campos',1476771747142,'Campos',NULL,0,'{\"CNAME\":\"21BD32A43CBD0A5\",\"ID\":\"8248A99E81E752CB\",\"NAME\":\"21BD32A43CBD0A5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cantrell','Cantrell',1476771747142,'Cantrell',NULL,0,'{\"CNAME\":\"59119829781FC0AA\",\"ID\":\"F7F580E11D00A758\",\"NAME\":\"59119829781FC0AA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cantu','Cantu',1476771747142,'Cantu',NULL,0,'{\"CNAME\":\"3A2522CD96557B14\",\"ID\":\"8CE6790CC6A94E65\",\"NAME\":\"3A2522CD96557B14\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cardenas','Cardenas',1476771747142,'Cardenas',NULL,0,'{\"CNAME\":\"1A261D825473769C\",\"ID\":\"208E43F0E45C4C78\",\"NAME\":\"1A261D825473769C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carey','Carey',1476771747142,'Carey',NULL,0,'{\"CNAME\":\"CA8B1EDF74317452\",\"ID\":\"4588E674D3F0FAF9\",\"NAME\":\"CA8B1EDF74317452\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carl','Carl',1476771747142,'Carl',NULL,0,'{\"CNAME\":\"A0DF931E7A7F9B60\",\"ID\":\"FEAB05AA91085B7A\",\"NAME\":\"A0DF931E7A7F9B60\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carlson','Carlson',1476771747142,'Carlson',NULL,0,'{\"CNAME\":\"3EFC15D786B9219D\",\"ID\":\"8A3363ABE792DB2D\",\"NAME\":\"3EFC15D786B9219D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carney','Carney',1476771747142,'Carney',NULL,0,'{\"CNAME\":\"C3AA439727DC88CB\",\"ID\":\"A1D50185E7426CBB\",\"NAME\":\"C3AA439727DC88CB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carr','Carr',1476771747142,'Carr',NULL,0,'{\"CNAME\":\"7D7EE36ED0313BBC\",\"ID\":\"DF0AAB058CE179E4\",\"NAME\":\"7D7EE36ED0313BBC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carrie','Carrie',1476771747142,'Carrie',NULL,0,'{\"CNAME\":\"97B9308F6A1F6A85\",\"ID\":\"9C6C3783B4A7005\",\"NAME\":\"97B9308F6A1F6A85\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carrillo','Carrillo',1476771747142,'Carrillo',NULL,0,'{\"CNAME\":\"72CF1EABAB646D31\",\"ID\":\"2B38C2DF6A49B97F\",\"NAME\":\"72CF1EABAB646D31\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carroll','Carroll',1476771747142,'Carroll',NULL,0,'{\"CNAME\":\"47DB95D9998BE77D\",\"ID\":\"F197002B9A0853EC\",\"NAME\":\"47DB95D9998BE77D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Carson','Carson',1476771747142,'Carson',NULL,0,'{\"CNAME\":\"B42B26A4FE27D81A\",\"ID\":\"45F31D16B1058D58\",\"NAME\":\"B42B26A4FE27D81A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Casey','Casey',1476771747142,'Casey',NULL,0,'{\"CNAME\":\"1E21AFEBA99926D6\",\"ID\":\"55B1927FDAFEF39C\",\"NAME\":\"1E21AFEBA99926D6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cassandra','Cassandra',1476771747142,'Cassandra',NULL,0,'{\"CNAME\":\"121C60DF0C03083D\",\"ID\":\"E8B1CBD05F6E6A35\",\"NAME\":\"121C60DF0C03083D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Castaneda','Castaneda',1476771747142,'Castaneda',NULL,0,'{\"CNAME\":\"DC6F9FC8A45EBAB1\",\"ID\":\"285F89B802BCB265\",\"NAME\":\"DC6F9FC8A45EBAB1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Castillo','Castillo',1476771747142,'Castillo',NULL,0,'{\"CNAME\":\"E7A58438FA3C342C\",\"ID\":\"E22312179BF43E61\",\"NAME\":\"E7A58438FA3C342C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Castro','Castro',1476771747142,'Castro',NULL,0,'{\"CNAME\":\"848FFD503F98D236\",\"ID\":\"A8240CB8235E9C49\",\"NAME\":\"848FFD503F98D236\"}',NULL,NULL,1),('ConsumerBizListItem','1_Catherine','Catherine',1476771747142,'Catherine',NULL,0,'{\"CNAME\":\"5EB59A1C45EC8D0E\",\"ID\":\"C048B3A434E49E6\",\"NAME\":\"5EB59A1C45EC8D0E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cervantes','Cervantes',1476771747142,'Cervantes',NULL,0,'{\"CNAME\":\"3BE8C5739D8F056B\",\"ID\":\"16E6A3326DD7D868\",\"NAME\":\"3BE8C5739D8F056B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chad','Chad',1476771747142,'Chad',NULL,0,'{\"CNAME\":\"EDE79B3FBF673A9A\",\"ID\":\"884D79963BD8BC0A\",\"NAME\":\"EDE79B3FBF673A9A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chan','Chan',1476771747142,'Chan',NULL,0,'{\"CNAME\":\"26C322652770620E\",\"ID\":\"38CA89564B225940\",\"NAME\":\"26C322652770620E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chandler','Chandler',1476771747142,'Chandler',NULL,0,'{\"CNAME\":\"E7823AF140E46BB4\",\"ID\":\"ABEA47BA24142ED1\",\"NAME\":\"E7823AF140E46BB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chaney','Chaney',1476771747142,'Chaney',NULL,0,'{\"CNAME\":\"62C3C96C5C35493D\",\"ID\":\"6E7D2DA6D3953058\",\"NAME\":\"62C3C96C5C35493D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chang','Chang',1476771747142,'Chang',NULL,0,'{\"CNAME\":\"BF9017D04F72C1B5\",\"ID\":\"E26AF6AC3B1C1C\",\"NAME\":\"BF9017D04F72C1B5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chapman','Chapman',1476771747142,'Chapman',NULL,0,'{\"CNAME\":\"AC3D3C98C89DCBF5\",\"ID\":\"A3FB4FBF9A6F9CF0\",\"NAME\":\"AC3D3C98C89DCBF5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Charles','Charles',1476771747142,'Charles',NULL,0,'{\"CNAME\":\"A5410EE37744C574\",\"ID\":\"F2C9A93EEA6F38F\",\"NAME\":\"A5410EE37744C574\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chavez','Chavez',1476771747142,'Chavez',NULL,0,'{\"CNAME\":\"82DD3179733E3E7\",\"ID\":\"9DA187A7A191431D\",\"NAME\":\"82DD3179733E3E7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chelsea','Chelsea',1476771747142,'Chelsea',NULL,0,'{\"CNAME\":\"91CB315A6405BFCC\",\"ID\":\"2F29B6E3ABC6EBDE\",\"NAME\":\"91CB315A6405BFCC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chen','Chen',1476771747142,'Chen',NULL,0,'{\"CNAME\":\"A1A8887793ACFC19\",\"ID\":\"1E1D184167CA7676\",\"NAME\":\"A1A8887793ACFC19\"}',NULL,NULL,1),('ConsumerBizListItem','1_Choi','Choi',1476771747142,'Choi',NULL,0,'{\"CNAME\":\"A5AF3C3F6914C2D7\",\"ID\":\"3EB71F6293A2A31F\",\"NAME\":\"A5AF3C3F6914C2D7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Christensen','Christensen',1476771747142,'Christensen',NULL,0,'{\"CNAME\":\"83841FA37186B3C8\",\"ID\":\"A113C1ECD3CACE22\",\"NAME\":\"83841FA37186B3C8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Christian','Christian',1476771747142,'Christian',NULL,0,'{\"CNAME\":\"7FF135854376850E\",\"ID\":\"36A1694BCE9815B7\",\"NAME\":\"7FF135854376850E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Christina','Christina',1476771747142,'Christina',NULL,0,'{\"CNAME\":\"E311DD5FD4CDBBA7\",\"ID\":\"A0A0C8AAA00ADE5\",\"NAME\":\"E311DD5FD4CDBBA7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Christine','Christine',1476771747142,'Christine',NULL,0,'{\"CNAME\":\"723E1489A45D2CBA\",\"ID\":\"3473DECCCB0509FB\",\"NAME\":\"723E1489A45D2CBA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Christopher','Christopher',1476771747142,'Christopher',NULL,0,'{\"CNAME\":\"A909FFE7BE1FFE2\",\"ID\":\"1896A3BF730516DD\",\"NAME\":\"A909FFE7BE1FFE2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Chung','Chung',1476771747142,'Chung',NULL,0,'{\"CNAME\":\"D1A346DF2019A0C0\",\"ID\":\"A7D8AE4569120B5B\",\"NAME\":\"D1A346DF2019A0C0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cisneros','Cisneros',1476771747142,'Cisneros',NULL,0,'{\"CNAME\":\"251F4D7BED16FB71\",\"ID\":\"83F97F4825290BE4\",\"NAME\":\"251F4D7BED16FB71\"}',NULL,NULL,1),('ConsumerBizListItem','1_Clarke','Clarke',1476771747142,'Clarke',NULL,0,'{\"CNAME\":\"14994A56A1F7E3C3\",\"ID\":\"7D771E0E8F3633AB\",\"NAME\":\"14994A56A1F7E3C3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Clayton','Clayton',1476771747142,'Clayton',NULL,0,'{\"CNAME\":\"D0B8C94BB241F6F7\",\"ID\":\"DABD8D2CE74E782C\",\"NAME\":\"D0B8C94BB241F6F7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Clements','Clements',1476771747142,'Clements',NULL,0,'{\"CNAME\":\"9C06C8D081BA60B8\",\"ID\":\"6EB6E75FDDEC0218\",\"NAME\":\"9C06C8D081BA60B8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cobb','Cobb',1476771747142,'Cobb',NULL,0,'{\"CNAME\":\"7061D02035071395\",\"ID\":\"2AC2406E835BD49C\",\"NAME\":\"7061D02035071395\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cochran','Cochran',1476771747142,'Cochran',NULL,0,'{\"CNAME\":\"5B62F56E80093823\",\"ID\":\"F47330643AE134CA\",\"NAME\":\"5B62F56E80093823\"}',NULL,NULL,1),('ConsumerBizListItem','1_Coffey','Coffey',1476771747142,'Coffey',NULL,0,'{\"CNAME\":\"3429F0641ACB93FA\",\"ID\":\"E095E054EE94774\",\"NAME\":\"3429F0641ACB93FA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cohen','Cohen',1476771747142,'Cohen',NULL,0,'{\"CNAME\":\"1FDFADDBA13390A9\",\"ID\":\"97AF4FB322BB5C89\",\"NAME\":\"1FDFADDBA13390A9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Coleman','Coleman',1476771747142,'Coleman',NULL,0,'{\"CNAME\":\"AF8412547D623ABB\",\"ID\":\"5680522B8E2BB019\",\"NAME\":\"AF8412547D623ABB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Collier','Collier',1476771747142,'Collier',NULL,0,'{\"CNAME\":\"79F567AFA129D132\",\"ID\":\"2B6D65B9A9445C42\",\"NAME\":\"79F567AFA129D132\"}',NULL,NULL,1),('ConsumerBizListItem','1_Collins','Collins',1476771747142,'Collins',NULL,0,'{\"CNAME\":\"7CE38BF6811A96AF\",\"ID\":\"4F284803BD0966CC\",\"NAME\":\"7CE38BF6811A96AF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Combs','Combs',1476771747142,'Combs',NULL,0,'{\"CNAME\":\"161A285043E6536F\",\"ID\":\"C44E503833B64E9F\",\"NAME\":\"161A285043E6536F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Compton','Compton',1476771747142,'Compton',NULL,0,'{\"CNAME\":\"A58D805436AF781C\",\"ID\":\"82C2559140B95CCD\",\"NAME\":\"A58D805436AF781C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Conley','Conley',1476771747142,'Conley',NULL,0,'{\"CNAME\":\"9B3C9A88147B4BE2\",\"ID\":\"160C88652D47D0BE\",\"NAME\":\"9B3C9A88147B4BE2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Conner','Conner',1476771747142,'Conner',NULL,0,'{\"CNAME\":\"45EDF534397CCC21\",\"ID\":\"B20BB95AB626D93F\",\"NAME\":\"45EDF534397CCC21\"}',NULL,NULL,1),('ConsumerBizListItem','1_Conrad','Conrad',1476771747142,'Conrad',NULL,0,'{\"CNAME\":\"A66483FB374DAAEF\",\"ID\":\"52292E0C763FD027\",\"NAME\":\"A66483FB374DAAEF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Contreras','Contreras',1476771747142,'Contreras',NULL,0,'{\"CNAME\":\"AFF94B2D344CB850\",\"ID\":\"9A3D458322D70046\",\"NAME\":\"AFF94B2D344CB850\"}',NULL,NULL,1),('ConsumerBizListItem','1_Conway','Conway',1476771747142,'Conway',NULL,0,'{\"CNAME\":\"C60A4AA7A5CAAC6B\",\"ID\":\"A42A596FC71E1782\",\"NAME\":\"C60A4AA7A5CAAC6B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cooke','Cooke',1476771747142,'Cooke',NULL,0,'{\"CNAME\":\"400695F99D020AB4\",\"ID\":\"188E8B8B014829E\",\"NAME\":\"400695F99D020AB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cooley','Cooley',1476771747142,'Cooley',NULL,0,'{\"CNAME\":\"1ABEB7BC99BD2341\",\"ID\":\"9ADEB82FFFB5444E\",\"NAME\":\"1ABEB7BC99BD2341\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cooper','Cooper',1476771747142,'Cooper',NULL,0,'{\"CNAME\":\"CD21B93CFD8D6824\",\"ID\":\"AE5E3CE40E0404A4\",\"NAME\":\"CD21B93CFD8D6824\"}',NULL,NULL,1),('ConsumerBizListItem','1_Copeland','Copeland',1476771747142,'Copeland',NULL,0,'{\"CNAME\":\"D7FB54278C1363FA\",\"ID\":\"C54E7837E0CD0CED\",\"NAME\":\"D7FB54278C1363FA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cordova','Cordova',1476771747142,'Cordova',NULL,0,'{\"CNAME\":\"C0E0D6E58B463D79\",\"ID\":\"4D2E7BD33C475784\",\"NAME\":\"C0E0D6E58B463D79\"}',NULL,NULL,1),('ConsumerBizListItem','1_Corey','Corey',1476771747142,'Corey',NULL,0,'{\"CNAME\":\"679F660CDB8DE648\",\"ID\":\"FE2D010308A6B379\",\"NAME\":\"679F660CDB8DE648\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cortez','Cortez',1476771747142,'Cortez',NULL,0,'{\"CNAME\":\"E9D8FD1A46B1CB02\",\"ID\":\"7501E5D4DA87AC39\",\"NAME\":\"E9D8FD1A46B1CB02\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cory','Cory',1476771747142,'Cory',NULL,0,'{\"CNAME\":\"7729CA956C9BDB1E\",\"ID\":\"147702DB07145348\",\"NAME\":\"7729CA956C9BDB1E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Costa','Costa',1476771747142,'Costa',NULL,0,'{\"CNAME\":\"2FE44044F4021738\",\"ID\":\"491442DF5F88C6AA\",\"NAME\":\"2FE44044F4021738\"}',NULL,NULL,1),('ConsumerBizListItem','1_Courtney','Courtney',1476771747142,'Courtney',NULL,0,'{\"CNAME\":\"292903FA88D000B4\",\"ID\":\"FB2FCD534B0FF3BB\",\"NAME\":\"292903FA88D000B4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cowan','Cowan',1476771747142,'Cowan',NULL,0,'{\"CNAME\":\"43E98CE7586066E8\",\"ID\":\"B571ECEA16A98240\",\"NAME\":\"43E98CE7586066E8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Craig','Craig',1476771747142,'Craig',NULL,0,'{\"CNAME\":\"14084800449265EE\",\"ID\":\"144A3F71A03AB7C4\",\"NAME\":\"14084800449265EE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Crawford','Crawford',1476771747142,'Crawford',NULL,0,'{\"CNAME\":\"E2A6B5A40F803C4C\",\"ID\":\"4E4E53AA080247BC\",\"NAME\":\"E2A6B5A40F803C4C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Crosby','Crosby',1476771747142,'Crosby',NULL,0,'{\"CNAME\":\"98FE7775C467D52C\",\"ID\":\"A58149D355F02887\",\"NAME\":\"98FE7775C467D52C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cuevas','Cuevas',1476771747142,'Cuevas',NULL,0,'{\"CNAME\":\"4025A17E2551B92D\",\"ID\":\"7E7E69EA33848743\",\"NAME\":\"4025A17E2551B92D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cummings','Cummings',1476771747142,'Cummings',NULL,0,'{\"CNAME\":\"E341DE977FBDF2E3\",\"ID\":\"F7CADE80B7CC92B9\",\"NAME\":\"E341DE977FBDF2E3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Cunningham','Cunningham',1476771747142,'Cunningham',NULL,0,'{\"CNAME\":\"CAA47875B7CEACD1\",\"ID\":\"285AB9448D2751EE\",\"NAME\":\"CAA47875B7CEACD1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Curry','Curry',1476771747142,'Curry',NULL,0,'{\"CNAME\":\"6F374060AEBF4D4F\",\"ID\":\"A01610228FE998F5\",\"NAME\":\"6F374060AEBF4D4F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Curtis','Curtis',1476771747142,'Curtis',NULL,0,'{\"CNAME\":\"C85A6A3B872CCDC1\",\"ID\":\"33CEB07BF4EEB3DA\",\"NAME\":\"C85A6A3B872CCDC1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dalton','Dalton',1476771747142,'Dalton',NULL,0,'{\"CNAME\":\"FF8E653E4CD52545\",\"ID\":\"C8BA76C279269B1C\",\"NAME\":\"FF8E653E4CD52545\"}',NULL,NULL,1),('ConsumerBizListItem','1_Daniel','Daniel',1476771747142,'Daniel',NULL,0,'{\"CNAME\":\"AA47F8215C6F30A0\",\"ID\":\"A4D2F0D23DCC84CE\",\"NAME\":\"AA47F8215C6F30A0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Danielle','Danielle',1476771747142,'Danielle',NULL,0,'{\"CNAME\":\"D05A90B8F6326735\",\"ID\":\"3948EAD63A9F2944\",\"NAME\":\"D05A90B8F6326735\"}',NULL,NULL,1),('ConsumerBizListItem','1_Daniels','Daniels',1476771747142,'Daniels',NULL,0,'{\"CNAME\":\"7CD068F5AEB8DEFF\",\"ID\":\"6A61D423D02A1C56\",\"NAME\":\"7CD068F5AEB8DEFF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Daugherty','Daugherty',1476771747142,'Daugherty',NULL,0,'{\"CNAME\":\"8CDC0AE4F0D7EDFB\",\"ID\":\"3F67FD97162D20E6\",\"NAME\":\"8CDC0AE4F0D7EDFB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Davenport','Davenport',1476771747142,'Davenport',NULL,0,'{\"CNAME\":\"CD73469AD25E1A6F\",\"ID\":\"2715518C87599930\",\"NAME\":\"CD73469AD25E1A6F\"}',NULL,NULL,1),('ConsumerBizListItem','1_David','David',1476771747142,'David',NULL,0,'{\"CNAME\":\"172522EC1028AB78\",\"ID\":\"B24D516BB65A5A58\",\"NAME\":\"172522EC1028AB78\"}',NULL,NULL,1),('ConsumerBizListItem','1_Davidson','Davidson',1476771747142,'Davidson',NULL,0,'{\"CNAME\":\"3FFDCAD47AFC7A3C\",\"ID\":\"1D72310EDC006DAD\",\"NAME\":\"3FFDCAD47AFC7A3C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Davies','Davies',1476771747142,'Davies',NULL,0,'{\"CNAME\":\"570B7F149EF6E997\",\"ID\":\"3A029F04D76D32E7\",\"NAME\":\"570B7F149EF6E997\"}',NULL,NULL,1),('ConsumerBizListItem','1_Davila','Davila',1476771747142,'Davila',NULL,0,'{\"CNAME\":\"4A02767C8F2D6687\",\"ID\":\"43CCA4B3DE2097B9\",\"NAME\":\"4A02767C8F2D6687\"}',NULL,NULL,1),('ConsumerBizListItem','1_Davis','Davis',1476771747142,'Davis',NULL,0,'{\"CNAME\":\"1C4637C65B65C812\",\"ID\":\"68D13CF26C4B4F4F\",\"NAME\":\"1C4637C65B65C812\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dawson','Dawson',1476771747142,'Dawson',NULL,0,'{\"CNAME\":\"B36EB6A9DE96A0CF\",\"ID\":\"25DF35DE87AA441B\",\"NAME\":\"B36EB6A9DE96A0CF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Decker','Decker',1476771747142,'Decker',NULL,0,'{\"CNAME\":\"799D978330DFF449\",\"ID\":\"3210DDBEAA16948A\",\"NAME\":\"799D978330DFF449\"}',NULL,NULL,1),('ConsumerBizListItem','1_Delacruz','Delacruz',1476771747142,'Delacruz',NULL,0,'{\"CNAME\":\"473732ECF904F995\",\"ID\":\"C4851E8E264415C4\",\"NAME\":\"473732ECF904F995\"}',NULL,NULL,1),('ConsumerBizListItem','1_Deleon','Deleon',1476771747142,'Deleon',NULL,0,'{\"CNAME\":\"B932C02D9EFF422\",\"ID\":\"EB86D510361FC23B\",\"NAME\":\"B932C02D9EFF422\"}',NULL,NULL,1),('ConsumerBizListItem','1_Delgado','Delgado',1476771747142,'Delgado',NULL,0,'{\"CNAME\":\"41E2DD5F17C3524C\",\"ID\":\"310CE61C90F3A46E\",\"NAME\":\"41E2DD5F17C3524C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dennis','Dennis',1476771747142,'Dennis',NULL,0,'{\"CNAME\":\"7DAACEA5F373B4C1\",\"ID\":\"4122CB13C7A474C1\",\"NAME\":\"7DAACEA5F373B4C1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Derek','Derek',1476771747142,'Derek',NULL,0,'{\"CNAME\":\"769F2B8A75180C1E\",\"ID\":\"6C14DA109E294D1E\",\"NAME\":\"769F2B8A75180C1E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Derrick','Derrick',1476771747142,'Derrick',NULL,0,'{\"CNAME\":\"BA8869BD398D1922\",\"ID\":\"E53A0A2978C28872\",\"NAME\":\"BA8869BD398D1922\"}',NULL,NULL,1),('ConsumerBizListItem','1_Diaz','Diaz',1476771747142,'Diaz',NULL,0,'{\"CNAME\":\"99CCABED315E3609\",\"ID\":\"E034FB6B66AACC1D\",\"NAME\":\"99CCABED315E3609\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dickerson','Dickerson',1476771747142,'Dickerson',NULL,0,'{\"CNAME\":\"4B852F3FFE7D0BD2\",\"ID\":\"81DC9BDB52D04DC2\",\"NAME\":\"4B852F3FFE7D0BD2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dickson','Dickson',1476771747142,'Dickson',NULL,0,'{\"CNAME\":\"2154F28665B52947\",\"ID\":\"9996535E07258A7B\",\"NAME\":\"2154F28665B52947\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dillon','Dillon',1476771747142,'Dillon',NULL,0,'{\"CNAME\":\"36394FA9C10C8D02\",\"ID\":\"7BCCFDE7714A1EBA\",\"NAME\":\"36394FA9C10C8D02\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dixon','Dixon',1476771747142,'Dixon',NULL,0,'{\"CNAME\":\"E34DBF4925964180\",\"ID\":\"A9EB812238F75313\",\"NAME\":\"E34DBF4925964180\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dodson','Dodson',1476771747142,'Dodson',NULL,0,'{\"CNAME\":\"C49D10A40B115E4\",\"ID\":\"D38901788C533E82\",\"NAME\":\"C49D10A40B115E4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dominguez','Dominguez',1476771747142,'Dominguez',NULL,0,'{\"CNAME\":\"4578078C772307D2\",\"ID\":\"B3BA8F1BEE1238A2\",\"NAME\":\"4578078C772307D2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Donald','Donald',1476771747142,'Donald',NULL,0,'{\"CNAME\":\"D343C0F0CA763F9\",\"ID\":\"A9078E8653368C9C\",\"NAME\":\"D343C0F0CA763F9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Donaldson','Donaldson',1476771747142,'Donaldson',NULL,0,'{\"CNAME\":\"91D1AF81F3A1B01B\",\"ID\":\"1C65CEF3DFD1E00C\",\"NAME\":\"91D1AF81F3A1B01B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Donovan','Donovan',1476771747142,'Donovan',NULL,0,'{\"CNAME\":\"C6AA159EB1971E47\",\"ID\":\"2DE5D16682C3C350\",\"NAME\":\"C6AA159EB1971E47\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dorsey','Dorsey',1476771747142,'Dorsey',NULL,0,'{\"CNAME\":\"9C4B13BFAE9AB90C\",\"ID\":\"E1D5BE1C7F2F4566\",\"NAME\":\"9C4B13BFAE9AB90C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dougherty','Dougherty',1476771747142,'Dougherty',NULL,0,'{\"CNAME\":\"EE1D880B4DFCA19C\",\"ID\":\"2C89109D42178DE8\",\"NAME\":\"EE1D880B4DFCA19C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Douglas','Douglas',1476771747142,'Douglas',NULL,0,'{\"CNAME\":\"3B16DC694C38D04F\",\"ID\":\"5EAC43ACEBA42C87\",\"NAME\":\"3B16DC694C38D04F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Doyle','Doyle',1476771747142,'Doyle',NULL,0,'{\"CNAME\":\"980182B70AC2E59E\",\"ID\":\"905056C1AC1DAD14\",\"NAME\":\"980182B70AC2E59E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Drake','Drake',1476771747142,'Drake',NULL,0,'{\"CNAME\":\"3AC75A6D60271EFA\",\"ID\":\"E6D8545DAA42D5CE\",\"NAME\":\"3AC75A6D60271EFA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Duarte','Duarte',1476771747142,'Duarte',NULL,0,'{\"CNAME\":\"485BA25E02B2E5AA\",\"ID\":\"39E4973BA3321B80\",\"NAME\":\"485BA25E02B2E5AA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dudley','Dudley',1476771747142,'Dudley',NULL,0,'{\"CNAME\":\"B1CA87138047BA3B\",\"ID\":\"2BA8698B79439589\",\"NAME\":\"B1CA87138047BA3B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Duffy','Duffy',1476771747142,'Duffy',NULL,0,'{\"CNAME\":\"904BC6E21E4799CE\",\"ID\":\"81E5F81DB77C5964\",\"NAME\":\"904BC6E21E4799CE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Duke','Duke',1476771747142,'Duke',NULL,0,'{\"CNAME\":\"41CF7CF23D3D372\",\"ID\":\"838E8AFB1CA34354\",\"NAME\":\"41CF7CF23D3D372\"}',NULL,NULL,1),('ConsumerBizListItem','1_Duncan','Duncan',1476771747142,'Duncan',NULL,0,'{\"CNAME\":\"F4D677934C35431D\",\"ID\":\"A284DF1155EC3E67\",\"NAME\":\"F4D677934C35431D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dunlap','Dunlap',1476771747142,'Dunlap',NULL,0,'{\"CNAME\":\"62C02A9C5A214367\",\"ID\":\"B495CE63EDE0F4EF\",\"NAME\":\"62C02A9C5A214367\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dunn','Dunn',1476771747142,'Dunn',NULL,0,'{\"CNAME\":\"D0CD7C2FB198B7AA\",\"ID\":\"884CE4BB65D328EC\",\"NAME\":\"D0CD7C2FB198B7AA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Duran','Duran',1476771747142,'Duran',NULL,0,'{\"CNAME\":\"F35644DD55A57B18\",\"ID\":\"3BF55BBAD370A8FC\",\"NAME\":\"F35644DD55A57B18\"}',NULL,NULL,1),('ConsumerBizListItem','1_Durham','Durham',1476771747142,'Durham',NULL,0,'{\"CNAME\":\"E2AF7E5615910245\",\"ID\":\"E3251075554389FE\",\"NAME\":\"E2AF7E5615910245\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dustin','Dustin',1476771747142,'Dustin',NULL,0,'{\"CNAME\":\"4D0336A64806A7D\",\"ID\":\"CD758E8F59DFDF06\",\"NAME\":\"4D0336A64806A7D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Dyer','Dyer',1476771747142,'Dyer',NULL,0,'{\"CNAME\":\"1A2063D89FF95EF\",\"ID\":\"26588E932C7CCFA1\",\"NAME\":\"1A2063D89FF95EF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Eaton','Eaton',1476771747142,'Eaton',NULL,0,'{\"CNAME\":\"8E6E0BFAF3487255\",\"ID\":\"68A83EEB494A308F\",\"NAME\":\"8E6E0BFAF3487255\"}',NULL,NULL,1),('ConsumerBizListItem','1_Edward','Edward',1476771747142,'Edward',NULL,0,'{\"CNAME\":\"A53F3929621DBA13\",\"ID\":\"F4573FC71C731D5C\",\"NAME\":\"A53F3929621DBA13\"}',NULL,NULL,1),('ConsumerBizListItem','1_Edwards','Edwards',1476771747142,'Edwards',NULL,0,'{\"CNAME\":\"A03E0F556D555E9\",\"ID\":\"17326D10D511828F\",\"NAME\":\"A03E0F556D555E9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Elizabeth','Elizabeth',1476771747142,'Elizabeth',NULL,0,'{\"CNAME\":\"4AF09080574089CB\",\"ID\":\"DC4C44F624D600AA\",\"NAME\":\"4AF09080574089CB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Elliott','Elliott',1476771747142,'Elliott',NULL,0,'{\"CNAME\":\"C6B36FFBCA41C490\",\"ID\":\"7EB3C8BE3D411E8E\",\"NAME\":\"C6B36FFBCA41C490\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ellis','Ellis',1476771747142,'Ellis',NULL,0,'{\"CNAME\":\"7E49DADF2CC69CBA\",\"ID\":\"6C8DBA7D0DF1C4A7\",\"NAME\":\"7E49DADF2CC69CBA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ellison','Ellison',1476771747142,'Ellison',NULL,0,'{\"CNAME\":\"991519043F37B2EA\",\"ID\":\"CE2FFD21FC958D9\",\"NAME\":\"991519043F37B2EA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Emily','Emily',1476771747142,'Emily',NULL,0,'{\"CNAME\":\"B02AE5AAEFE3F709\",\"ID\":\"8E2CFDC275761EDC\",\"NAME\":\"B02AE5AAEFE3F709\"}',NULL,NULL,1),('ConsumerBizListItem','1_English','English',1476771747142,'English',NULL,0,'{\"CNAME\":\"BA0A6DDD94C73698\",\"ID\":\"B51A15F382AC9143\",\"NAME\":\"BA0A6DDD94C73698\"}',NULL,NULL,1),('ConsumerBizListItem','1_Eric','Eric',1476771747142,'Eric',NULL,0,'{\"CNAME\":\"29988429C481F219\",\"ID\":\"193002E668758EA9\",\"NAME\":\"29988429C481F219\"}',NULL,NULL,1),('ConsumerBizListItem','1_Erica','Erica',1476771747142,'Erica',NULL,0,'{\"CNAME\":\"826B5C04738BEDC\",\"ID\":\"E00406144C1E7E35\",\"NAME\":\"826B5C04738BEDC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Erickson','Erickson',1476771747142,'Erickson',NULL,0,'{\"CNAME\":\"591B252C0C8F97FF\",\"ID\":\"C850371FDA6892FB\",\"NAME\":\"591B252C0C8F97FF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Erik','Erik',1476771747142,'Erik',NULL,0,'{\"CNAME\":\"6A42DD6E7CA9A813\",\"ID\":\"C1E39D912D21C91D\",\"NAME\":\"6A42DD6E7CA9A813\"}',NULL,NULL,1),('ConsumerBizListItem','1_Erin','Erin',1476771747142,'Erin',NULL,0,'{\"CNAME\":\"5F5BE3890FA875BF\",\"ID\":\"7810CCD41BF26FAA\",\"NAME\":\"5F5BE3890FA875BF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Escobar','Escobar',1476771747142,'Escobar',NULL,0,'{\"CNAME\":\"E1EFC4C0B611956E\",\"ID\":\"F91E24DFE80012E2\",\"NAME\":\"E1EFC4C0B611956E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Esparza','Esparza',1476771747142,'Esparza',NULL,0,'{\"CNAME\":\"BE916EF9FBD89FAB\",\"ID\":\"E702E51DA2C0F5BE\",\"NAME\":\"BE916EF9FBD89FAB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Espinoza','Espinoza',1476771747142,'Espinoza',NULL,0,'{\"CNAME\":\"2048D4FC09D84F3E\",\"ID\":\"BB04AF0F7ECAEE4A\",\"NAME\":\"2048D4FC09D84F3E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Estes','Estes',1476771747142,'Estes',NULL,0,'{\"CNAME\":\"6E40D2C0B278A3AA\",\"ID\":\"FB60D411A5C5B72B\",\"NAME\":\"6E40D2C0B278A3AA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Estrada','Estrada',1476771747142,'Estrada',NULL,0,'{\"CNAME\":\"7C1397C9742D1E30\",\"ID\":\"D759175DE8EA5B1D\",\"NAME\":\"7C1397C9742D1E30\"}',NULL,NULL,1),('ConsumerBizListItem','1_Evan','Evan',1476771747142,'Evan',NULL,0,'{\"CNAME\":\"98CC7D37DC7B90C1\",\"ID\":\"AB1A4D0DD4D48A2B\",\"NAME\":\"98CC7D37DC7B90C1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Evans','Evans',1476771747142,'Evans',NULL,0,'{\"CNAME\":\"29549B8791C11D20\",\"ID\":\"995665640DC31997\",\"NAME\":\"29549B8791C11D20\"}',NULL,NULL,1),('ConsumerBizListItem','1_Everett','Everett',1476771747142,'Everett',NULL,0,'{\"CNAME\":\"D60BBB7684DFFC89\",\"ID\":\"DA11E8CD1811ACB7\",\"NAME\":\"D60BBB7684DFFC89\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ewing','Ewing',1476771747142,'Ewing',NULL,0,'{\"CNAME\":\"69F9405C134D6847\",\"ID\":\"D94E18A8ADB4CC0F\",\"NAME\":\"69F9405C134D6847\"}',NULL,NULL,1),('ConsumerBizListItem','1_Farley','Farley',1476771747142,'Farley',NULL,0,'{\"CNAME\":\"8D51D2A413808FBA\",\"ID\":\"1CECC7A77928CA81\",\"NAME\":\"8D51D2A413808FBA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Farrell','Farrell',1476771747142,'Farrell',NULL,0,'{\"CNAME\":\"FD349FC45519E68D\",\"ID\":\"18B59CE1FD616D8\",\"NAME\":\"FD349FC45519E68D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Faulkner','Faulkner',1476771747142,'Faulkner',NULL,0,'{\"CNAME\":\"4D210DCD020E5BB6\",\"ID\":\"242C100DC94F871B\",\"NAME\":\"4D210DCD020E5BB6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ferguson','Ferguson',1476771747142,'Ferguson',NULL,0,'{\"CNAME\":\"560330CBF90CD52C\",\"ID\":\"944BDD9636749A08\",\"NAME\":\"560330CBF90CD52C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fernandez','Fernandez',1476771747142,'Fernandez',NULL,0,'{\"CNAME\":\"5936442FD1DD38B2\",\"ID\":\"C0A271BC0ECB776A\",\"NAME\":\"5936442FD1DD38B2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ferrell','Ferrell',1476771747142,'Ferrell',NULL,0,'{\"CNAME\":\"85CF23E843672972\",\"ID\":\"4079016D940210B4\",\"NAME\":\"85CF23E843672972\"}',NULL,NULL,1),('ConsumerBizListItem','1_Figueroa','Figueroa',1476771747142,'Figueroa',NULL,0,'{\"CNAME\":\"707A4A998376AE4\",\"ID\":\"E9FA1F3E9E66792\",\"NAME\":\"707A4A998376AE4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Finley','Finley',1476771747142,'Finley',NULL,0,'{\"CNAME\":\"D71C483B12929CCF\",\"ID\":\"829424FFA0D3A25\",\"NAME\":\"D71C483B12929CCF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fischer','Fischer',1476771747142,'Fischer',NULL,0,'{\"CNAME\":\"A8E3638E3C0DEB4\",\"ID\":\"70222949CC0DB89A\",\"NAME\":\"A8E3638E3C0DEB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fisher','Fisher',1476771747142,'Fisher',NULL,0,'{\"CNAME\":\"2163C17CA50BB0FC\",\"ID\":\"71F6278D140AF599\",\"NAME\":\"2163C17CA50BB0FC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fitzgerald','Fitzgerald',1476771747142,'Fitzgerald',NULL,0,'{\"CNAME\":\"6DC23BE6D0D50A6B\",\"ID\":\"459A4DDCB586F24E\",\"NAME\":\"6DC23BE6D0D50A6B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fitzpatrick','Fitzpatrick',1476771747142,'Fitzpatrick',NULL,0,'{\"CNAME\":\"D53B99133799A0E5\",\"ID\":\"7C82FAB8C8F89124\",\"NAME\":\"D53B99133799A0E5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fleming','Fleming',1476771747142,'Fleming',NULL,0,'{\"CNAME\":\"F313C576332CA4C6\",\"ID\":\"84438B7AAE55A063\",\"NAME\":\"F313C576332CA4C6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fletcher','Fletcher',1476771747142,'Fletcher',NULL,0,'{\"CNAME\":\"531CF6E04BA45B3C\",\"ID\":\"DC87C13749315C72\",\"NAME\":\"531CF6E04BA45B3C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Flores','Flores',1476771747142,'Flores',NULL,0,'{\"CNAME\":\"EE384A02E22087EE\",\"ID\":\"2812E5CF6D8F21D6\",\"NAME\":\"EE384A02E22087EE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Floyd','Floyd',1476771747142,'Floyd',NULL,0,'{\"CNAME\":\"46F3ABBDFB8EE9EC\",\"ID\":\"286674E3082FEB7E\",\"NAME\":\"46F3ABBDFB8EE9EC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Flynn','Flynn',1476771747142,'Flynn',NULL,0,'{\"CNAME\":\"4B6BDB199B4E7C48\",\"ID\":\"A51FB975227D6640\",\"NAME\":\"4B6BDB199B4E7C48\"}',NULL,NULL,1),('ConsumerBizListItem','1_Foley','Foley',1476771747142,'Foley',NULL,0,'{\"CNAME\":\"DA0D7A282884A80A\",\"ID\":\"A0833C8A1817526A\",\"NAME\":\"DA0D7A282884A80A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Forbes','Forbes',1476771747142,'Forbes',NULL,0,'{\"CNAME\":\"2BE987E67D0E412D\",\"ID\":\"6F3E29A35278D71C\",\"NAME\":\"2BE987E67D0E412D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ford','Ford',1476771747142,'Ford',NULL,0,'{\"CNAME\":\"80CA06ABFA1A5104\",\"ID\":\"2DF45244F09369E1\",\"NAME\":\"80CA06ABFA1A5104\"}',NULL,NULL,1),('ConsumerBizListItem','1_Foster','Foster',1476771747142,'Foster',NULL,0,'{\"CNAME\":\"96853C0E2DD18A1E\",\"ID\":\"996009F237400660\",\"NAME\":\"96853C0E2DD18A1E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fowler','Fowler',1476771747142,'Fowler',NULL,0,'{\"CNAME\":\"CB1A676BCEE11FD\",\"ID\":\"D282EF263719AB84\",\"NAME\":\"CB1A676BCEE11FD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Francis','Francis',1476771747142,'Francis',NULL,0,'{\"CNAME\":\"D0AB7FE6C314F4FE\",\"ID\":\"5CAF41D62364D5B4\",\"NAME\":\"D0AB7FE6C314F4FE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Franco','Franco',1476771747142,'Franco',NULL,0,'{\"CNAME\":\"6DD1411A66159040\",\"ID\":\"FD5C905BCD8C3348\",\"NAME\":\"6DD1411A66159040\"}',NULL,NULL,1),('ConsumerBizListItem','1_Frank','Frank',1476771747142,'Frank',NULL,0,'{\"CNAME\":\"26253C50741FAA9C\",\"ID\":\"7940AB4746839656\",\"NAME\":\"26253C50741FAA9C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Franklin','Franklin',1476771747142,'Franklin',NULL,0,'{\"CNAME\":\"9C1F33CA0B1F1B10\",\"ID\":\"F93882CBD8FC7FB7\",\"NAME\":\"9C1F33CA0B1F1B10\"}',NULL,NULL,1),('ConsumerBizListItem','1_Frazier','Frazier',1476771747142,'Frazier',NULL,0,'{\"CNAME\":\"3BD21A42747A59DF\",\"ID\":\"A0872CC5B5CA4CC2\",\"NAME\":\"3BD21A42747A59DF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Frederick','Frederick',1476771747142,'Frederick',NULL,0,'{\"CNAME\":\"B43CF7F09CCC873C\",\"ID\":\"4476B929E30DD0C4\",\"NAME\":\"B43CF7F09CCC873C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Freeman','Freeman',1476771747142,'Freeman',NULL,0,'{\"CNAME\":\"6636E1322531BD6A\",\"ID\":\"535AB76633D94208\",\"NAME\":\"6636E1322531BD6A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Frey','Frey',1476771747142,'Frey',NULL,0,'{\"CNAME\":\"8E3241BD690F94FD\",\"ID\":\"7BB060764A818184\",\"NAME\":\"8E3241BD690F94FD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Friedman','Friedman',1476771747142,'Friedman',NULL,0,'{\"CNAME\":\"6001AEA4918E6DF\",\"ID\":\"F29B38F160F87AE8\",\"NAME\":\"6001AEA4918E6DF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fritz','Fritz',1476771747142,'Fritz',NULL,0,'{\"CNAME\":\"8C63FEED8B89EA9A\",\"ID\":\"F09696910BDD874A\",\"NAME\":\"8C63FEED8B89EA9A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Frost','Frost',1476771747142,'Frost',NULL,0,'{\"CNAME\":\"2B82477BCCCC369D\",\"ID\":\"50905D7B2216BFEC\",\"NAME\":\"2B82477BCCCC369D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Frye','Frye',1476771747142,'Frye',NULL,0,'{\"CNAME\":\"16CF03FA44315487\",\"ID\":\"DFD7468AC613286C\",\"NAME\":\"16CF03FA44315487\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fuentes','Fuentes',1476771747142,'Fuentes',NULL,0,'{\"CNAME\":\"704BF4D7BAABD091\",\"ID\":\"F3F1B7FC5A8779A9\",\"NAME\":\"704BF4D7BAABD091\"}',NULL,NULL,1),('ConsumerBizListItem','1_Fuller','Fuller',1476771747142,'Fuller',NULL,0,'{\"CNAME\":\"9CCEE4A1FCBEB21B\",\"ID\":\"93FB9D4B16AA750C\",\"NAME\":\"9CCEE4A1FCBEB21B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gaines','Gaines',1476771747142,'Gaines',NULL,0,'{\"CNAME\":\"906CC1CCB800C67\",\"ID\":\"76CF99D3614E23EA\",\"NAME\":\"906CC1CCB800C67\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gallagher','Gallagher',1476771747142,'Gallagher',NULL,0,'{\"CNAME\":\"2800CB426FD44157\",\"ID\":\"1EE3DFCD8A0645A2\",\"NAME\":\"2800CB426FD44157\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gallegos','Gallegos',1476771747142,'Gallegos',NULL,0,'{\"CNAME\":\"77FE37C61821FFD4\",\"ID\":\"2A50E9C2D6B89B95\",\"NAME\":\"77FE37C61821FFD4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Galloway','Galloway',1476771747142,'Galloway',NULL,0,'{\"CNAME\":\"2051E049D1901AA0\",\"ID\":\"F9BE311E65D81A9A\",\"NAME\":\"2051E049D1901AA0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Galvan','Galvan',1476771747142,'Galvan',NULL,0,'{\"CNAME\":\"209F03EFF7D40F90\",\"ID\":\"DC960C46C38BD16E\",\"NAME\":\"209F03EFF7D40F90\"}',NULL,NULL,1),('ConsumerBizListItem','1_Garcia','Garcia',1476771747142,'Garcia',NULL,0,'{\"CNAME\":\"BBB5FF6DC3826B99\",\"ID\":\"4671AEAF49C79268\",\"NAME\":\"BBB5FF6DC3826B99\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gardner','Gardner',1476771747142,'Gardner',NULL,0,'{\"CNAME\":\"EC5C1FB82DD32306\",\"ID\":\"BB7946E7D85C81A9\",\"NAME\":\"EC5C1FB82DD32306\"}',NULL,NULL,1),('ConsumerBizListItem','1_Garner','Garner',1476771747142,'Garner',NULL,0,'{\"CNAME\":\"2E80E32F40C5F582\",\"ID\":\"3546AB441E56FA33\",\"NAME\":\"2E80E32F40C5F582\"}',NULL,NULL,1),('ConsumerBizListItem','1_Garrett','Garrett',1476771747142,'Garrett',NULL,0,'{\"CNAME\":\"77D28DC99A347492\",\"ID\":\"C70DAF247944FE3A\",\"NAME\":\"77D28DC99A347492\"}',NULL,NULL,1),('ConsumerBizListItem','1_Garrison','Garrison',1476771747142,'Garrison',NULL,0,'{\"CNAME\":\"2DC979D8571788E6\",\"ID\":\"17FAFE5F6CE2F190\",\"NAME\":\"2DC979D8571788E6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gary','Gary',1476771747142,'Gary',NULL,0,'{\"CNAME\":\"3B083FD0AADC888\",\"ID\":\"4C22BD444899D3B6\",\"NAME\":\"3B083FD0AADC888\"}',NULL,NULL,1),('ConsumerBizListItem','1_Garza','Garza',1476771747142,'Garza',NULL,0,'{\"CNAME\":\"B19E7A3ACD0CE567\",\"ID\":\"1E9565CECC4E989\",\"NAME\":\"B19E7A3ACD0CE567\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gentry','Gentry',1476771747142,'Gentry',NULL,0,'{\"CNAME\":\"17337DE8EBA5A515\",\"ID\":\"FE51510C80BFD6E5\",\"NAME\":\"17337DE8EBA5A515\"}',NULL,NULL,1),('ConsumerBizListItem','1_George','George',1476771747142,'George',NULL,0,'{\"CNAME\":\"9B306AB04EF5E25F\",\"ID\":\"E077E1A544EEC4F0\",\"NAME\":\"9B306AB04EF5E25F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gibbs','Gibbs',1476771747142,'Gibbs',NULL,0,'{\"CNAME\":\"C39F81C1EBAD90BA\",\"ID\":\"28E209B61A52482A\",\"NAME\":\"C39F81C1EBAD90BA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gibson','Gibson',1476771747142,'Gibson',NULL,0,'{\"CNAME\":\"96401F1B20C5AB70\",\"ID\":\"FF49CC40A8890E6A\",\"NAME\":\"96401F1B20C5AB70\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gilbert','Gilbert',1476771747142,'Gilbert',NULL,0,'{\"CNAME\":\"E14C4EC12ADAD2F0\",\"ID\":\"8EDD72158CCD2A87\",\"NAME\":\"E14C4EC12ADAD2F0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Giles','Giles',1476771747142,'Giles',NULL,0,'{\"CNAME\":\"604F3E51064BB666\",\"ID\":\"9CB67FFB59554AB1\",\"NAME\":\"604F3E51064BB666\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gill','Gill',1476771747142,'Gill',NULL,0,'{\"CNAME\":\"A80BB44145C3F9A\",\"ID\":\"3D779CAE2D46CF6A\",\"NAME\":\"A80BB44145C3F9A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gillespie','Gillespie',1476771747142,'Gillespie',NULL,0,'{\"CNAME\":\"EBCB7766ED2A7960\",\"ID\":\"E48E13207341B6BF\",\"NAME\":\"EBCB7766ED2A7960\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gilmore','Gilmore',1476771747142,'Gilmore',NULL,0,'{\"CNAME\":\"177F2E6C269203ED\",\"ID\":\"5311655A15B75FA\",\"NAME\":\"177F2E6C269203ED\"}',NULL,NULL,1),('ConsumerBizListItem','1_Glenn','Glenn',1476771747142,'Glenn',NULL,0,'{\"CNAME\":\"3C784BFF199EF62E\",\"ID\":\"D10EC7C16CBE9DE8\",\"NAME\":\"3C784BFF199EF62E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Glover','Glover',1476771747142,'Glover',NULL,0,'{\"CNAME\":\"8925613CE25FD881\",\"ID\":\"4F87658EF0DE1944\",\"NAME\":\"8925613CE25FD881\"}',NULL,NULL,1),('ConsumerBizListItem','1_Golden','Golden',1476771747142,'Golden',NULL,0,'{\"CNAME\":\"C773FDEF3889BDAD\",\"ID\":\"33EBD5B07DC7E407\",\"NAME\":\"C773FDEF3889BDAD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gomez','Gomez',1476771747142,'Gomez',NULL,0,'{\"CNAME\":\"D7180FD20069768F\",\"ID\":\"5E1B18C4C6A6D316\",\"NAME\":\"D7180FD20069768F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gonzales','Gonzales',1476771747142,'Gonzales',NULL,0,'{\"CNAME\":\"5F05B6F966568941\",\"ID\":\"674BFC5F6B72706F\",\"NAME\":\"5F05B6F966568941\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gonzalez','Gonzalez',1476771747142,'Gonzalez',NULL,0,'{\"CNAME\":\"E796E897B03DFA33\",\"ID\":\"A50ABBA8132A7719\",\"NAME\":\"E796E897B03DFA33\"}',NULL,NULL,1),('ConsumerBizListItem','1_Good','Good',1476771747142,'Good',NULL,0,'{\"CNAME\":\"755F85C2723BB393\",\"ID\":\"86109D400F0ED29E\",\"NAME\":\"755F85C2723BB393\"}',NULL,NULL,1),('ConsumerBizListItem','1_Goodman','Goodman',1476771747142,'Goodman',NULL,0,'{\"CNAME\":\"68E4BEC2B0A80CC2\",\"ID\":\"82965D4ED8150294\",\"NAME\":\"68E4BEC2B0A80CC2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Goodwin','Goodwin',1476771747142,'Goodwin',NULL,0,'{\"CNAME\":\"49EF1EB2B7A1DE6F\",\"ID\":\"E55666A4AD822E0\",\"NAME\":\"49EF1EB2B7A1DE6F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gordon','Gordon',1476771747142,'Gordon',NULL,0,'{\"CNAME\":\"8FB744B51A1F14E5\",\"ID\":\"C73DFE6C630EDB4C\",\"NAME\":\"8FB744B51A1F14E5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gould','Gould',1476771747142,'Gould',NULL,0,'{\"CNAME\":\"16B65BD37AA7F2EB\",\"ID\":\"BCC0D400288793E8\",\"NAME\":\"16B65BD37AA7F2EB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Graham','Graham',1476771747142,'Graham',NULL,0,'{\"CNAME\":\"905E55B4671F18A2\",\"ID\":\"861DC9BD7F4E7DD3\",\"NAME\":\"905E55B4671F18A2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Greene','Greene',1476771747142,'Greene',NULL,0,'{\"CNAME\":\"9B97CA569BCB00DF\",\"ID\":\"3E313B9BADF12632\",\"NAME\":\"9B97CA569BCB00DF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Greer','Greer',1476771747142,'Greer',NULL,0,'{\"CNAME\":\"A50F227F8AFBA77D\",\"ID\":\"8B0DC65F996F98FD\",\"NAME\":\"A50F227F8AFBA77D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gregory','Gregory',1476771747142,'Gregory',NULL,0,'{\"CNAME\":\"E73D1F05BADAF949\",\"ID\":\"EE8374EC4E4AD797\",\"NAME\":\"E73D1F05BADAF949\"}',NULL,NULL,1),('ConsumerBizListItem','1_Griffin','Griffin',1476771747142,'Griffin',NULL,0,'{\"CNAME\":\"D5DF6F9E5BC8CF58\",\"ID\":\"AF4732711661056E\",\"NAME\":\"D5DF6F9E5BC8CF58\"}',NULL,NULL,1),('ConsumerBizListItem','1_Griffith','Griffith',1476771747142,'Griffith',NULL,0,'{\"CNAME\":\"7FD80F528B3AF163\",\"ID\":\"B8C27B7A1C450FFD\",\"NAME\":\"7FD80F528B3AF163\"}',NULL,NULL,1),('ConsumerBizListItem','1_Grimes','Grimes',1476771747142,'Grimes',NULL,0,'{\"CNAME\":\"E725FD4681B41193\",\"ID\":\"980ECD059122CE2E\",\"NAME\":\"E725FD4681B41193\"}',NULL,NULL,1),('ConsumerBizListItem','1_Guerra','Guerra',1476771747142,'Guerra',NULL,0,'{\"CNAME\":\"789A98094D698266\",\"ID\":\"C26820B8A4C1B3C2\",\"NAME\":\"789A98094D698266\"}',NULL,NULL,1),('ConsumerBizListItem','1_Guerrero','Guerrero',1476771747142,'Guerrero',NULL,0,'{\"CNAME\":\"27617D27802A9DEA\",\"ID\":\"83ADC9225E4DEB67\",\"NAME\":\"27617D27802A9DEA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Gutierrez','Gutierrez',1476771747142,'Gutierrez',NULL,0,'{\"CNAME\":\"F7EA063AB0850599\",\"ID\":\"8D3369C4C086F236\",\"NAME\":\"F7EA063AB0850599\"}',NULL,NULL,1),('ConsumerBizListItem','1_Guzman','Guzman',1476771747142,'Guzman',NULL,0,'{\"CNAME\":\"1A9293C661B533D7\",\"ID\":\"FB508EF074EE78A0\",\"NAME\":\"1A9293C661B533D7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Haas','Haas',1476771747142,'Haas',NULL,0,'{\"CNAME\":\"AB1CEF3CCACF899B\",\"ID\":\"CF9A242B70F45317\",\"NAME\":\"AB1CEF3CCACF899B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hahn','Hahn',1476771747142,'Hahn',NULL,0,'{\"CNAME\":\"95F7892ABBDCC8A4\",\"ID\":\"A9BE4C2A4041CADB\",\"NAME\":\"95F7892ABBDCC8A4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Haley','Haley',1476771747142,'Haley',NULL,0,'{\"CNAME\":\"85E0728D47091458\",\"ID\":\"9683CC5F89562EA4\",\"NAME\":\"85E0728D47091458\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hamilton','Hamilton',1476771747142,'Hamilton',NULL,0,'{\"CNAME\":\"B56683FDA85D87A0\",\"ID\":\"EFFC299A1ADDB07E\",\"NAME\":\"B56683FDA85D87A0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hammond','Hammond',1476771747142,'Hammond',NULL,0,'{\"CNAME\":\"333752ACCDC91CBE\",\"ID\":\"CF1F78FE923AFE05\",\"NAME\":\"333752ACCDC91CBE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hampton','Hampton',1476771747142,'Hampton',NULL,0,'{\"CNAME\":\"B6766B490C59A423\",\"ID\":\"6A5889BB0190D021\",\"NAME\":\"B6766B490C59A423\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hancock','Hancock',1476771747142,'Hancock',NULL,0,'{\"CNAME\":\"906C5718311FBA07\",\"ID\":\"A223C6B3710F85DF\",\"NAME\":\"906C5718311FBA07\"}',NULL,NULL,1),('ConsumerBizListItem','1_Haney','Haney',1476771747142,'Haney',NULL,0,'{\"CNAME\":\"9EA8AFDAF47DC49E\",\"ID\":\"6C1DA886822C6782\",\"NAME\":\"9EA8AFDAF47DC49E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hanna','Hanna',1476771747142,'Hanna',NULL,0,'{\"CNAME\":\"300BF52DD231262B\",\"ID\":\"E3A37AA85A14E35\",\"NAME\":\"300BF52DD231262B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hannah','Hannah',1476771747142,'Hannah',NULL,0,'{\"CNAME\":\"EB09D5E396183F4B\",\"ID\":\"D1EE59E20AD01CED\",\"NAME\":\"EB09D5E396183F4B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hansen','Hansen',1476771747142,'Hansen',NULL,0,'{\"CNAME\":\"6C5B77BC23A2BCAA\",\"ID\":\"81CA0262C82E712E\",\"NAME\":\"6C5B77BC23A2BCAA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hanson','Hanson',1476771747142,'Hanson',NULL,0,'{\"CNAME\":\"86B090FD48F953AB\",\"ID\":\"F3BD5AD57C8389A8\",\"NAME\":\"86B090FD48F953AB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hardin','Hardin',1476771747142,'Hardin',NULL,0,'{\"CNAME\":\"4A5B6CB49C9EBDF7\",\"ID\":\"E56B06C51E104919\",\"NAME\":\"4A5B6CB49C9EBDF7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harding','Harding',1476771747142,'Harding',NULL,0,'{\"CNAME\":\"F05A0776697735E5\",\"ID\":\"792C7B5AAE4A79E7\",\"NAME\":\"F05A0776697735E5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hardy','Hardy',1476771747142,'Hardy',NULL,0,'{\"CNAME\":\"CDADC9AB9E91D4A\",\"ID\":\"70FEB62B69F16E02\",\"NAME\":\"CDADC9AB9E91D4A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harmon','Harmon',1476771747142,'Harmon',NULL,0,'{\"CNAME\":\"B3DD0F04C8AB0913\",\"ID\":\"79A49B3E37626328\",\"NAME\":\"B3DD0F04C8AB0913\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harper','Harper',1476771747142,'Harper',NULL,0,'{\"CNAME\":\"F08F9065D1DCB714\",\"ID\":\"F52378E14237225A\",\"NAME\":\"F08F9065D1DCB714\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harrell','Harrell',1476771747142,'Harrell',NULL,0,'{\"CNAME\":\"13ED06A7B164F39D\",\"ID\":\"8D9A0ADB7C204239\",\"NAME\":\"13ED06A7B164F39D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harrington','Harrington',1476771747142,'Harrington',NULL,0,'{\"CNAME\":\"3927E4F1C1B8B5F3\",\"ID\":\"BAD5F33780C42F25\",\"NAME\":\"3927E4F1C1B8B5F3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harris','Harris',1476771747142,'Harris',NULL,0,'{\"CNAME\":\"65EC1E9CA4D5C2CA\",\"ID\":\"EA8FCD92D5958171\",\"NAME\":\"65EC1E9CA4D5C2CA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harrison','Harrison',1476771747142,'Harrison',NULL,0,'{\"CNAME\":\"F04DD540E2245391\",\"ID\":\"D82118376DF344B0\",\"NAME\":\"F04DD540E2245391\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hartman','Hartman',1476771747142,'Hartman',NULL,0,'{\"CNAME\":\"1BF22405CD48D61B\",\"ID\":\"4E0D67E54AD6626E\",\"NAME\":\"1BF22405CD48D61B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Harvey','Harvey',1476771747142,'Harvey',NULL,0,'{\"CNAME\":\"3D83F27F0845FAF9\",\"ID\":\"CD0DCE8FCA267BF1\",\"NAME\":\"3D83F27F0845FAF9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hatfield','Hatfield',1476771747142,'Hatfield',NULL,0,'{\"CNAME\":\"73FB108558A4CD60\",\"ID\":\"5CA3E9B122F61F8F\",\"NAME\":\"73FB108558A4CD60\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hawkins','Hawkins',1476771747142,'Hawkins',NULL,0,'{\"CNAME\":\"7AFDC127B6DBD1BB\",\"ID\":\"86E8F7AB32CFD125\",\"NAME\":\"7AFDC127B6DBD1BB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hayden','Hayden',1476771747142,'Hayden',NULL,0,'{\"CNAME\":\"A417816EC35F5E1F\",\"ID\":\"363763E5C3DC3A68\",\"NAME\":\"A417816EC35F5E1F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Haynes','Haynes',1476771747142,'Haynes',NULL,0,'{\"CNAME\":\"1D85CA4D653D46C\",\"ID\":\"4FA7C62536118CC4\",\"NAME\":\"1D85CA4D653D46C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Heath','Heath',1476771747142,'Heath',NULL,0,'{\"CNAME\":\"CD25A8CACFE77223\",\"ID\":\"C0A7566915F4F24\",\"NAME\":\"CD25A8CACFE77223\"}',NULL,NULL,1),('ConsumerBizListItem','1_Heather','Heather',1476771747142,'Heather',NULL,0,'{\"CNAME\":\"32D635528E3A388A\",\"ID\":\"2BD7F907B7F5B6BB\",\"NAME\":\"32D635528E3A388A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hebert','Hebert',1476771747142,'Hebert',NULL,0,'{\"CNAME\":\"20B1531AD25FF871\",\"ID\":\"359F38463D487E9E\",\"NAME\":\"20B1531AD25FF871\"}',NULL,NULL,1),('ConsumerBizListItem','1_Henderson','Henderson',1476771747142,'Henderson',NULL,0,'{\"CNAME\":\"853550E6A1E8981A\",\"ID\":\"7F53F8C6C730AF6A\",\"NAME\":\"853550E6A1E8981A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hendricks','Hendricks',1476771747142,'Hendricks',NULL,0,'{\"CNAME\":\"1ED7568DFD3A32B7\",\"ID\":\"C0826819636026DD\",\"NAME\":\"1ED7568DFD3A32B7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hendrix','Hendrix',1476771747142,'Hendrix',NULL,0,'{\"CNAME\":\"B1C84F8D672B5D6A\",\"ID\":\"AD3019B856147C17\",\"NAME\":\"B1C84F8D672B5D6A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Henry','Henry',1476771747142,'Henry',NULL,0,'{\"CNAME\":\"27E4180BEEDB297\",\"ID\":\"F016E59C7AD8B1D7\",\"NAME\":\"27E4180BEEDB297\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hensley','Hensley',1476771747142,'Hensley',NULL,0,'{\"CNAME\":\"92B40CA2BE7C71AD\",\"ID\":\"14D9E8007C9B41F5\",\"NAME\":\"92B40CA2BE7C71AD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Henson','Henson',1476771747142,'Henson',NULL,0,'{\"CNAME\":\"44612212E8B260DE\",\"ID\":\"966289037AD9846\",\"NAME\":\"44612212E8B260DE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Herman','Herman',1476771747142,'Herman',NULL,0,'{\"CNAME\":\"A1A6907C98994608\",\"ID\":\"215A71A12769B056\",\"NAME\":\"A1A6907C98994608\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hernandez','Hernandez',1476771747142,'Hernandez',NULL,0,'{\"CNAME\":\"F57661026A1DF28B\",\"ID\":\"D9731321EF4E063E\",\"NAME\":\"F57661026A1DF28B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Herrera','Herrera',1476771747142,'Herrera',NULL,0,'{\"CNAME\":\"A9F94B0C3BCD97BC\",\"ID\":\"602D1305678A8D5F\",\"NAME\":\"A9F94B0C3BCD97BC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Herring','Herring',1476771747142,'Herring',NULL,0,'{\"CNAME\":\"B04C31BDDF46D75\",\"ID\":\"F0DD4A99FBA6075A\",\"NAME\":\"B04C31BDDF46D75\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hess','Hess',1476771747142,'Hess',NULL,0,'{\"CNAME\":\"90C62F60544DE31B\",\"ID\":\"9701A1C165DD9420\",\"NAME\":\"90C62F60544DE31B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hester','Hester',1476771747142,'Hester',NULL,0,'{\"CNAME\":\"5C8E5AC42F469AB2\",\"ID\":\"28FC2782EA7EF51C\",\"NAME\":\"5C8E5AC42F469AB2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hickman','Hickman',1476771747142,'Hickman',NULL,0,'{\"CNAME\":\"3644841CDED5E3C5\",\"ID\":\"4EDAA105D5F53590\",\"NAME\":\"3644841CDED5E3C5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hicks','Hicks',1476771747142,'Hicks',NULL,0,'{\"CNAME\":\"D02F6041ECD83AA3\",\"ID\":\"186A157B2992E7DA\",\"NAME\":\"D02F6041ECD83AA3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Higgins','Higgins',1476771747142,'Higgins',NULL,0,'{\"CNAME\":\"E35BB7D04BE67571\",\"ID\":\"3DE2334A314A7A72\",\"NAME\":\"E35BB7D04BE67571\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hines','Hines',1476771747142,'Hines',NULL,0,'{\"CNAME\":\"D4538518F9DBAEC4\",\"ID\":\"B7087C1F4F89E63A\",\"NAME\":\"D4538518F9DBAEC4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hinton','Hinton',1476771747142,'Hinton',NULL,0,'{\"CNAME\":\"816CAAC2EF4A94BE\",\"ID\":\"46771D1F432B4234\",\"NAME\":\"816CAAC2EF4A94BE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hobbs','Hobbs',1476771747142,'Hobbs',NULL,0,'{\"CNAME\":\"15DD8A95BE4BAAC7\",\"ID\":\"6D9C547CF146054A\",\"NAME\":\"15DD8A95BE4BAAC7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hodge','Hodge',1476771747142,'Hodge',NULL,0,'{\"CNAME\":\"86284A41221510F5\",\"ID\":\"7B5B23F4AADF9513\",\"NAME\":\"86284A41221510F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hodges','Hodges',1476771747142,'Hodges',NULL,0,'{\"CNAME\":\"63CB8FBA2BAD3C93\",\"ID\":\"512C5CAD6C37EDB9\",\"NAME\":\"63CB8FBA2BAD3C93\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hoffman','Hoffman',1476771747142,'Hoffman',NULL,0,'{\"CNAME\":\"1DD88162B6ED2855\",\"ID\":\"54072F485CDB7897\",\"NAME\":\"1DD88162B6ED2855\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hogan','Hogan',1476771747142,'Hogan',NULL,0,'{\"CNAME\":\"EE4230A50643E856\",\"ID\":\"E4E946668CF2AFC\",\"NAME\":\"EE4230A50643E856\"}',NULL,NULL,1),('ConsumerBizListItem','1_Holden','Holden',1476771747142,'Holden',NULL,0,'{\"CNAME\":\"90FDC6BC8D17ADDD\",\"ID\":\"59F51FD6937412B7\",\"NAME\":\"90FDC6BC8D17ADDD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Holland','Holland',1476771747142,'Holland',NULL,0,'{\"CNAME\":\"B95F379F6AE24561\",\"ID\":\"7A674153C63CFF1A\",\"NAME\":\"B95F379F6AE24561\"}',NULL,NULL,1),('ConsumerBizListItem','1_Holloway','Holloway',1476771747142,'Holloway',NULL,0,'{\"CNAME\":\"3021F7E62553F0D6\",\"ID\":\"A60937EBA57758ED\",\"NAME\":\"3021F7E62553F0D6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Holmes','Holmes',1476771747142,'Holmes',NULL,0,'{\"CNAME\":\"4102E8103EA89D32\",\"ID\":\"23AD3E314E2A2B43\",\"NAME\":\"4102E8103EA89D32\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hooper','Hooper',1476771747142,'Hooper',NULL,0,'{\"CNAME\":\"C7180702FDEAA1DC\",\"ID\":\"9A1756FD0C741126\",\"NAME\":\"C7180702FDEAA1DC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hopkins','Hopkins',1476771747142,'Hopkins',NULL,0,'{\"CNAME\":\"99B1084A7FBDE6C9\",\"ID\":\"309928D4B100A5D7\",\"NAME\":\"99B1084A7FBDE6C9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Horne','Horne',1476771747142,'Horne',NULL,0,'{\"CNAME\":\"5BB0B0F34D94C5D4\",\"ID\":\"30C8E1CA872524FB\",\"NAME\":\"5BB0B0F34D94C5D4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Horton','Horton',1476771747142,'Horton',NULL,0,'{\"CNAME\":\"8B9E4342B4462839\",\"ID\":\"90DB9DA4FC5414AB\",\"NAME\":\"8B9E4342B4462839\"}',NULL,NULL,1),('ConsumerBizListItem','1_Howard','Howard',1476771747142,'Howard',NULL,0,'{\"CNAME\":\"DC5AB2B32D9D7804\",\"ID\":\"9AA42B31882EC039\",\"NAME\":\"DC5AB2B32D9D7804\"}',NULL,NULL,1),('ConsumerBizListItem','1_Howe','Howe',1476771747142,'Howe',NULL,0,'{\"CNAME\":\"9753DD6CB91FB56E\",\"ID\":\"ED9422357395A0D\",\"NAME\":\"9753DD6CB91FB56E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Howell','Howell',1476771747142,'Howell',NULL,0,'{\"CNAME\":\"7515D5843074A849\",\"ID\":\"856FC81623DA2150\",\"NAME\":\"7515D5843074A849\"}',NULL,NULL,1),('ConsumerBizListItem','1_Huang','Huang',1476771747142,'Huang',NULL,0,'{\"CNAME\":\"4E8F794089B6B4EF\",\"ID\":\"489D0396E6826EB0\",\"NAME\":\"4E8F794089B6B4EF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hubbard','Hubbard',1476771747142,'Hubbard',NULL,0,'{\"CNAME\":\"1D902A2E0A226B6A\",\"ID\":\"A424ED4BD3A7D6AE\",\"NAME\":\"1D902A2E0A226B6A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Huber','Huber',1476771747142,'Huber',NULL,0,'{\"CNAME\":\"3291AAAC20F3D603\",\"ID\":\"299FB2142D7DE959\",\"NAME\":\"3291AAAC20F3D603\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hudson','Hudson',1476771747142,'Hudson',NULL,0,'{\"CNAME\":\"BC7AFCB7EEF3CCFC\",\"ID\":\"970AF30E481057C4\",\"NAME\":\"BC7AFCB7EEF3CCFC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Huerta','Huerta',1476771747142,'Huerta',NULL,0,'{\"CNAME\":\"63DC3C0AE79D1D9A\",\"ID\":\"663A4DDCEACB40B\",\"NAME\":\"63DC3C0AE79D1D9A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Huffman','Huffman',1476771747142,'Huffman',NULL,0,'{\"CNAME\":\"C4BFCEA09EE404DF\",\"ID\":\"56352739F5964354\",\"NAME\":\"C4BFCEA09EE404DF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hughes','Hughes',1476771747142,'Hughes',NULL,0,'{\"CNAME\":\"697B31D5B595B8C8\",\"ID\":\"411AE1BF081D1674\",\"NAME\":\"697B31D5B595B8C8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hull','Hull',1476771747142,'Hull',NULL,0,'{\"CNAME\":\"1947B668C33BEDC0\",\"ID\":\"E11943A6031A0E61\",\"NAME\":\"1947B668C33BEDC0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Humphrey','Humphrey',1476771747142,'Humphrey',NULL,0,'{\"CNAME\":\"262D4D3F6CF9F553\",\"ID\":\"56468D5607A5AAF1\",\"NAME\":\"262D4D3F6CF9F553\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hunter','Hunter',1476771747142,'Hunter',NULL,0,'{\"CNAME\":\"6B1B36CBB04B4149\",\"ID\":\"B069B3415151FA72\",\"NAME\":\"6B1B36CBB04B4149\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hurley','Hurley',1476771747142,'Hurley',NULL,0,'{\"CNAME\":\"2C49AD8A57C62DCD\",\"ID\":\"7895FC13088EE37F\",\"NAME\":\"2C49AD8A57C62DCD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hurst','Hurst',1476771747142,'Hurst',NULL,0,'{\"CNAME\":\"30674DA7E57E4E59\",\"ID\":\"1F3202D820180A39\",\"NAME\":\"30674DA7E57E4E59\"}',NULL,NULL,1),('ConsumerBizListItem','1_Hutchinson','Hutchinson',1476771747142,'Hutchinson',NULL,0,'{\"CNAME\":\"D9910B3E0C893194\",\"ID\":\"EAA32C96F620053C\",\"NAME\":\"D9910B3E0C893194\"}',NULL,NULL,1),('ConsumerBizListItem','1_Huynh','Huynh',1476771747142,'Huynh',NULL,0,'{\"CNAME\":\"724432DE08F74E1F\",\"ID\":\"A8F8F60264024DCA\",\"NAME\":\"724432DE08F74E1F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ibarra','Ibarra',1476771747142,'Ibarra',NULL,0,'{\"CNAME\":\"5E777EAAA999428E\",\"ID\":\"DB6EBD0566994D14\",\"NAME\":\"5E777EAAA999428E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ingram','Ingram',1476771747142,'Ingram',NULL,0,'{\"CNAME\":\"22A19E5DD971E964\",\"ID\":\"540AE6B0F6AC6E15\",\"NAME\":\"22A19E5DD971E964\"}',NULL,NULL,1),('ConsumerBizListItem','1_Irwin','Irwin',1476771747142,'Irwin',NULL,0,'{\"CNAME\":\"A3A1B2FED45FF920\",\"ID\":\"F4A331B7A22D1B23\",\"NAME\":\"A3A1B2FED45FF920\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jackson','Jackson',1476771747142,'Jackson',NULL,0,'{\"CNAME\":\"B41779690B83F182\",\"ID\":\"B197FFDEF2DDC330\",\"NAME\":\"B41779690B83F182\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jacob','Jacob',1476771747142,'Jacob',NULL,0,'{\"CNAME\":\"736B19F69AACA691\",\"ID\":\"F69E505B08403AD2\",\"NAME\":\"736B19F69AACA691\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jacobs','Jacobs',1476771747142,'Jacobs',NULL,0,'{\"CNAME\":\"A0B9A4445E09FD1C\",\"ID\":\"8FB5F8BE2AA9D6C6\",\"NAME\":\"A0B9A4445E09FD1C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jacobson','Jacobson',1476771747142,'Jacobson',NULL,0,'{\"CNAME\":\"7D98F111BB6E7838\",\"ID\":\"AFE434653A898DA2\",\"NAME\":\"7D98F111BB6E7838\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jacqueline','Jacqueline',1476771747142,'Jacqueline',NULL,0,'{\"CNAME\":\"1C45154ABBF67419\",\"ID\":\"B265CE60FE4C5384\",\"NAME\":\"1C45154ABBF67419\"}',NULL,NULL,1),('ConsumerBizListItem','1_James','James',1476771747142,'James',NULL,0,'{\"CNAME\":\"B4CC344D25A2EFE5\",\"ID\":\"8FB21EE7A2207526\",\"NAME\":\"B4CC344D25A2EFE5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jamie','Jamie',1476771747142,'Jamie',NULL,0,'{\"CNAME\":\"1C138FD52DDD7713\",\"ID\":\"C930EECD01935FEE\",\"NAME\":\"1C138FD52DDD7713\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jared','Jared',1476771747142,'Jared',NULL,0,'{\"CNAME\":\"B620E68B3BF4387B\",\"ID\":\"E94F63F579E05CB4\",\"NAME\":\"B620E68B3BF4387B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jarvis','Jarvis',1476771747142,'Jarvis',NULL,0,'{\"CNAME\":\"E3BDEA4F0A488356\",\"ID\":\"2DFFBC474AA176B6\",\"NAME\":\"E3BDEA4F0A488356\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jason','Jason',1476771747142,'Jason',NULL,0,'{\"CNAME\":\"2B877B4B825B48A9\",\"ID\":\"C5CC17E395D3049B\",\"NAME\":\"2B877B4B825B48A9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jefferson','Jefferson',1476771747142,'Jefferson',NULL,0,'{\"CNAME\":\"102B4FE641956DB4\",\"ID\":\"80A8155EB153025E\",\"NAME\":\"102B4FE641956DB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jeffery','Jeffery',1476771747142,'Jeffery',NULL,0,'{\"CNAME\":\"913013028698F70C\",\"ID\":\"6786F3C62FBF9021\",\"NAME\":\"913013028698F70C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jeffrey','Jeffrey',1476771747142,'Jeffrey',NULL,0,'{\"CNAME\":\"6A27F10AEF159701\",\"ID\":\"536A76F94CF75351\",\"NAME\":\"6A27F10AEF159701\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jenkins','Jenkins',1476771747142,'Jenkins',NULL,0,'{\"CNAME\":\"AB63A76362C3972A\",\"ID\":\"3BBFDDE8842A5C44\",\"NAME\":\"AB63A76362C3972A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jenna','Jenna',1476771747142,'Jenna',NULL,0,'{\"CNAME\":\"A9726DCA1F708941\",\"ID\":\"A1BF96B7165E962\",\"NAME\":\"A9726DCA1F708941\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jennifer','Jennifer',1476771747142,'Jennifer',NULL,0,'{\"CNAME\":\"1660FE5C81C4CE64\",\"ID\":\"5E76BEF6E019B254\",\"NAME\":\"1660FE5C81C4CE64\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jennings','Jennings',1476771747142,'Jennings',NULL,0,'{\"CNAME\":\"F9436AB13C309387\",\"ID\":\"1D8BAE291B1E472\",\"NAME\":\"F9436AB13C309387\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jensen','Jensen',1476771747142,'Jensen',NULL,0,'{\"CNAME\":\"554265464B692D13\",\"ID\":\"4DCAE38EE11D3A66\",\"NAME\":\"554265464B692D13\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jeremiah','Jeremiah',1476771747142,'Jeremiah',NULL,0,'{\"CNAME\":\"4B8C9C9AD8AACF24\",\"ID\":\"1ABB1E1EA5F481B5\",\"NAME\":\"4B8C9C9AD8AACF24\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jeremy','Jeremy',1476771747142,'Jeremy',NULL,0,'{\"CNAME\":\"6967CABEFD763AC1\",\"ID\":\"7042AC7D03D3B99\",\"NAME\":\"6967CABEFD763AC1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jerry','Jerry',1476771747142,'Jerry',NULL,0,'{\"CNAME\":\"30035607EE5BB378\",\"ID\":\"95151403B0DB4F75\",\"NAME\":\"30035607EE5BB378\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jesse','Jesse',1476771747142,'Jesse',NULL,0,'{\"CNAME\":\"A1361CB85BE840D6\",\"ID\":\"AD71C82B22F4F65B\",\"NAME\":\"A1361CB85BE840D6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jessica','Jessica',1476771747142,'Jessica',NULL,0,'{\"CNAME\":\"AAE039D6AA239CFC\",\"ID\":\"FE70C36866ADD157\",\"NAME\":\"AAE039D6AA239CFC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jill','Jill',1476771747142,'Jill',NULL,0,'{\"CNAME\":\"94232713ABF350F0\",\"ID\":\"C913303F392FFC64\",\"NAME\":\"94232713ABF350F0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jimenez','Jimenez',1476771747142,'Jimenez',NULL,0,'{\"CNAME\":\"5E44852A0F2C5961\",\"ID\":\"372D3F309FEF0619\",\"NAME\":\"5E44852A0F2C5961\"}',NULL,NULL,1),('ConsumerBizListItem','1_Joel','Joel',1476771747142,'Joel',NULL,0,'{\"CNAME\":\"C000CCF225950AAC\",\"ID\":\"6D9CB7DE5E8AC30B\",\"NAME\":\"C000CCF225950AAC\"}',NULL,NULL,1),('ConsumerBizListItem','1_John','John',1476771747142,'John',NULL,0,'{\"CNAME\":\"527BD5B5D689E2C3\",\"ID\":\"245952ECFF55018\",\"NAME\":\"527BD5B5D689E2C3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Johns','Johns',1476771747142,'Johns',NULL,0,'{\"CNAME\":\"56D2453B24865C55\",\"ID\":\"CEC6F62CFB44B1BE\",\"NAME\":\"56D2453B24865C55\"}',NULL,NULL,1),('ConsumerBizListItem','1_Johnson','Johnson',1476771747142,'Johnson',NULL,0,'{\"CNAME\":\"79AB945544E5BC01\",\"ID\":\"991DE292E76F74F3\",\"NAME\":\"79AB945544E5BC01\"}',NULL,NULL,1),('ConsumerBizListItem','1_Johnston','Johnston',1476771747142,'Johnston',NULL,0,'{\"CNAME\":\"71D5930A6D05BE52\",\"ID\":\"42FFCF057E133F94\",\"NAME\":\"71D5930A6D05BE52\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jonathan','Jonathan',1476771747142,'Jonathan',NULL,0,'{\"CNAME\":\"78842815248300FA\",\"ID\":\"8B0D268963DD0CFB\",\"NAME\":\"78842815248300FA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jonathon','Jonathon',1476771747142,'Jonathon',NULL,0,'{\"CNAME\":\"77CD7711F4EB3BB1\",\"ID\":\"DAA96D9681A21445\",\"NAME\":\"77CD7711F4EB3BB1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jones','Jones',1476771747142,'Jones',NULL,0,'{\"CNAME\":\"EE7E4705DD4AC06A\",\"ID\":\"AC796A52DB3F16BB\",\"NAME\":\"EE7E4705DD4AC06A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Jordan','Jordan',1476771747142,'Jordan',NULL,0,'{\"CNAME\":\"D16D377AF76C99D2\",\"ID\":\"7D6044E95A167611\",\"NAME\":\"D16D377AF76C99D2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Joseph','Joseph',1476771747142,'Joseph',NULL,0,'{\"CNAME\":\"CB07901C53218323\",\"ID\":\"571D3A9420BFD921\",\"NAME\":\"CB07901C53218323\"}',NULL,NULL,1),('ConsumerBizListItem','1_Joshua','Joshua',1476771747142,'Joshua',NULL,0,'{\"CNAME\":\"D1133275EE2118BE\",\"ID\":\"97275A23CA44226C\",\"NAME\":\"D1133275EE2118BE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Joyce','Joyce',1476771747142,'Joyce',NULL,0,'{\"CNAME\":\"19053D1F43416AD9\",\"ID\":\"78B9CAB19959E4AF\",\"NAME\":\"19053D1F43416AD9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Juarez','Juarez',1476771747142,'Juarez',NULL,0,'{\"CNAME\":\"3511A50075304801\",\"ID\":\"83CDCEC08FBF9037\",\"NAME\":\"3511A50075304801\"}',NULL,NULL,1),('ConsumerBizListItem','1_Julie','Julie',1476771747142,'Julie',NULL,0,'{\"CNAME\":\"16F12F5E8379E22B\",\"ID\":\"DC09C97FD73D7A32\",\"NAME\":\"16F12F5E8379E22B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Justin','Justin',1476771747142,'Justin',NULL,0,'{\"CNAME\":\"53DD9C6005F3CDFC\",\"ID\":\"E816C635CAD85A60\",\"NAME\":\"53DD9C6005F3CDFC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kaiser','Kaiser',1476771747142,'Kaiser',NULL,0,'{\"CNAME\":\"80C74513E7126A4D\",\"ID\":\"4E8412AD48562E3C\",\"NAME\":\"80C74513E7126A4D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kane','Kane',1476771747142,'Kane',NULL,0,'{\"CNAME\":\"A230B06A0387F5C6\",\"ID\":\"AB541D874C7BC19A\",\"NAME\":\"A230B06A0387F5C6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kara','Kara',1476771747142,'Kara',NULL,0,'{\"CNAME\":\"D731E2C5239A8E2C\",\"ID\":\"BFFC98347EE35B3E\",\"NAME\":\"D731E2C5239A8E2C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Katherine','Katherine',1476771747142,'Katherine',NULL,0,'{\"CNAME\":\"C832493E09C0A0F1\",\"ID\":\"729C68884BD359AD\",\"NAME\":\"C832493E09C0A0F1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kathleen','Kathleen',1476771747142,'Kathleen',NULL,0,'{\"CNAME\":\"312E2D6F2BF24314\",\"ID\":\"7FB8CEB3BD59C795\",\"NAME\":\"312E2D6F2BF24314\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kathryn','Kathryn',1476771747142,'Kathryn',NULL,0,'{\"CNAME\":\"B1C7755394ED39ED\",\"ID\":\"86DF7DCFD896FCAF\",\"NAME\":\"B1C7755394ED39ED\"}',NULL,NULL,1),('ConsumerBizListItem','1_Katie','Katie',1476771747142,'Katie',NULL,0,'{\"CNAME\":\"8A52072CEE5D17CC\",\"ID\":\"6D3A1E06D6A06349\",\"NAME\":\"8A52072CEE5D17CC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kaufman','Kaufman',1476771747142,'Kaufman',NULL,0,'{\"CNAME\":\"EC202A34F44A6323\",\"ID\":\"8A146F1A3DA4700C\",\"NAME\":\"EC202A34F44A6323\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kayla','Kayla',1476771747142,'Kayla',NULL,0,'{\"CNAME\":\"16616347A7A5A764\",\"ID\":\"1BAFF70E2669E837\",\"NAME\":\"16616347A7A5A764\"}',NULL,NULL,1),('ConsumerBizListItem','1_Keith','Keith',1476771747142,'Keith',NULL,0,'{\"CNAME\":\"8DD9FA632CA161D0\",\"ID\":\"F187A23C3EE681EF\",\"NAME\":\"8DD9FA632CA161D0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Keller','Keller',1476771747142,'Keller',NULL,0,'{\"CNAME\":\"D4865FF5A7EE343A\",\"ID\":\"226D1F15ECD35F78\",\"NAME\":\"D4865FF5A7EE343A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kelley','Kelley',1476771747142,'Kelley',NULL,0,'{\"CNAME\":\"60A0ACA55D2DF93A\",\"ID\":\"E655C7716A4B3EA6\",\"NAME\":\"60A0ACA55D2DF93A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kelli','Kelli',1476771747142,'Kelli',NULL,0,'{\"CNAME\":\"3561B1026C3D33CD\",\"ID\":\"1714726C817AF504\",\"NAME\":\"3561B1026C3D33CD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kelly','Kelly',1476771747142,'Kelly',NULL,0,'{\"CNAME\":\"AE074A5692DFB7C2\",\"ID\":\"FCDF25D6E191893E\",\"NAME\":\"AE074A5692DFB7C2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kennedy','Kennedy',1476771747142,'Kennedy',NULL,0,'{\"CNAME\":\"6C64383BFD8D7B0E\",\"ID\":\"FA1E9C965314CCD7\",\"NAME\":\"6C64383BFD8D7B0E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kenneth','Kenneth',1476771747142,'Kenneth',NULL,0,'{\"CNAME\":\"7CA955BD92CA8B00\",\"ID\":\"1415DB70FE9DDB11\",\"NAME\":\"7CA955BD92CA8B00\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kent','Kent',1476771747142,'Kent',NULL,0,'{\"CNAME\":\"564F10260067A9B0\",\"ID\":\"55C567FD4395ECEF\",\"NAME\":\"564F10260067A9B0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kerr','Kerr',1476771747142,'Kerr',NULL,0,'{\"CNAME\":\"249D9C0CDEC6DF9A\",\"ID\":\"CED556CD9F9C0C83\",\"NAME\":\"249D9C0CDEC6DF9A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kevin','Kevin',1476771747142,'Kevin',NULL,0,'{\"CNAME\":\"9D5E3ECDEB4CDB7A\",\"ID\":\"6490791E7ABF6B29\",\"NAME\":\"9D5E3ECDEB4CDB7A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Khan','Khan',1476771747142,'Khan',NULL,0,'{\"CNAME\":\"9E95F6D797987B7D\",\"ID\":\"CFA5301358B9FCBE\",\"NAME\":\"9E95F6D797987B7D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kidd','Kidd',1476771747142,'Kidd',NULL,0,'{\"CNAME\":\"BBBB8331D47439E4\",\"ID\":\"5CBDFD0DFA22A3FC\",\"NAME\":\"BBBB8331D47439E4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kimberly','Kimberly',1476771747142,'Kimberly',NULL,0,'{\"CNAME\":\"B6CB572E168A99F5\",\"ID\":\"77F959F119F4FB23\",\"NAME\":\"B6CB572E168A99F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kirby','Kirby',1476771747142,'Kirby',NULL,0,'{\"CNAME\":\"99AFB6D7741C5E15\",\"ID\":\"C8CE55163055C4D\",\"NAME\":\"99AFB6D7741C5E15\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kirk','Kirk',1476771747142,'Kirk',NULL,0,'{\"CNAME\":\"73A6A32A7F6A5C7D\",\"ID\":\"49B8B4F95F02E055\",\"NAME\":\"73A6A32A7F6A5C7D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Klein','Klein',1476771747142,'Klein',NULL,0,'{\"CNAME\":\"89A114BEF2A7DDB4\",\"ID\":\"C9EBB2DED806D7F\",\"NAME\":\"89A114BEF2A7DDB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kline','Kline',1476771747142,'Kline',NULL,0,'{\"CNAME\":\"8B986951863A2BD0\",\"ID\":\"B5488AEFF4288918\",\"NAME\":\"8B986951863A2BD0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Knapp','Knapp',1476771747142,'Knapp',NULL,0,'{\"CNAME\":\"477364C6E0713BC4\",\"ID\":\"4DCF435435894A4D\",\"NAME\":\"477364C6E0713BC4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Knight','Knight',1476771747142,'Knight',NULL,0,'{\"CNAME\":\"EFEAD51028A03DB2\",\"ID\":\"EBD6D2F5D60FF9AF\",\"NAME\":\"EFEAD51028A03DB2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Knox','Knox',1476771747142,'Knox',NULL,0,'{\"CNAME\":\"EE50D26FBE3D6BD6\",\"ID\":\"1CD3882394520876\",\"NAME\":\"EE50D26FBE3D6BD6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Koch','Koch',1476771747142,'Koch',NULL,0,'{\"CNAME\":\"5290BFA958F1EA1\",\"ID\":\"41A60377BA920919\",\"NAME\":\"5290BFA958F1EA1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kramer','Kramer',1476771747142,'Kramer',NULL,0,'{\"CNAME\":\"532619426CBAAD17\",\"ID\":\"EBB71045453F3867\",\"NAME\":\"532619426CBAAD17\"}',NULL,NULL,1),('ConsumerBizListItem','1_Krause','Krause',1476771747142,'Krause',NULL,0,'{\"CNAME\":\"91DF63FB383BE6A5\",\"ID\":\"F3D014EEAD934BB\",\"NAME\":\"91DF63FB383BE6A5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kristen','Kristen',1476771747142,'Kristen',NULL,0,'{\"CNAME\":\"F52B06185D58A5EB\",\"ID\":\"2B3BF3EEE2475E03\",\"NAME\":\"F52B06185D58A5EB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kristin','Kristin',1476771747142,'Kristin',NULL,0,'{\"CNAME\":\"4BE7B6B03D32793F\",\"ID\":\"83F2550373F2F194\",\"NAME\":\"4BE7B6B03D32793F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kristina','Kristina',1476771747142,'Kristina',NULL,0,'{\"CNAME\":\"15AB465B07F1E770\",\"ID\":\"BC7316929FE1545B\",\"NAME\":\"15AB465B07F1E770\"}',NULL,NULL,1),('ConsumerBizListItem','1_Krueger','Krueger',1476771747142,'Krueger',NULL,0,'{\"CNAME\":\"8B65D8B2FFE34F0D\",\"ID\":\"490640B43519C772\",\"NAME\":\"8B65D8B2FFE34F0D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Krystal','Krystal',1476771747142,'Krystal',NULL,0,'{\"CNAME\":\"BEF8E2BCB5A951BB\",\"ID\":\"81C8727C62E800BE\",\"NAME\":\"BEF8E2BCB5A951BB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Kyle','Kyle',1476771747142,'Kyle',NULL,0,'{\"CNAME\":\"4B75751E170E00F5\",\"ID\":\"42D6C7D61481D1C2\",\"NAME\":\"4B75751E170E00F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lacey','Lacey',1476771747142,'Lacey',NULL,0,'{\"CNAME\":\"13174983A0811496\",\"ID\":\"5607FE8879E4FD26\",\"NAME\":\"13174983A0811496\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lambert','Lambert',1476771747142,'Lambert',NULL,0,'{\"CNAME\":\"D79D11E9A095A2C5\",\"ID\":\"DB1915052D15F781\",\"NAME\":\"D79D11E9A095A2C5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Landry','Landry',1476771747142,'Landry',NULL,0,'{\"CNAME\":\"EC3E2A9D920ABF07\",\"ID\":\"253F7B5D921338AF\",\"NAME\":\"EC3E2A9D920ABF07\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lara','Lara',1476771747142,'Lara',NULL,0,'{\"CNAME\":\"D3C327C84F809A53\",\"ID\":\"3FB451CA2E89B3A1\",\"NAME\":\"D3C327C84F809A53\"}',NULL,NULL,1),('ConsumerBizListItem','1_Larry','Larry',1476771747142,'Larry',NULL,0,'{\"CNAME\":\"66F4B449B3A98ABF\",\"ID\":\"DF12ECD077EFC8C2\",\"NAME\":\"66F4B449B3A98ABF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Larsen','Larsen',1476771747142,'Larsen',NULL,0,'{\"CNAME\":\"4F780382467D1C80\",\"ID\":\"471C75EE6643A109\",\"NAME\":\"4F780382467D1C80\"}',NULL,NULL,1),('ConsumerBizListItem','1_Larson','Larson',1476771747142,'Larson',NULL,0,'{\"CNAME\":\"7E9F0189BAD38CFF\",\"ID\":\"8C00DEE24C9878FE\",\"NAME\":\"7E9F0189BAD38CFF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Laura','Laura',1476771747142,'Laura',NULL,0,'{\"CNAME\":\"680E89809965EC41\",\"ID\":\"E60E81C4CBE5171C\",\"NAME\":\"680E89809965EC41\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lauren','Lauren',1476771747142,'Lauren',NULL,0,'{\"CNAME\":\"2760C7B84D4BAD6B\",\"ID\":\"5CCE8DEDE893813F\",\"NAME\":\"2760C7B84D4BAD6B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lawrence','Lawrence',1476771747142,'Lawrence',NULL,0,'{\"CNAME\":\"E02D90EA127F923D\",\"ID\":\"109D2DD3608F669C\",\"NAME\":\"E02D90EA127F923D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lawson','Lawson',1476771747142,'Lawson',NULL,0,'{\"CNAME\":\"A184897C89E41EB3\",\"ID\":\"AA486F25175CBDC3\",\"NAME\":\"A184897C89E41EB3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Leah','Leah',1476771747142,'Leah',NULL,0,'{\"CNAME\":\"7007E0CD908F10B9\",\"ID\":\"CB8ACB1DC9821BF7\",\"NAME\":\"7007E0CD908F10B9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Leblanc','Leblanc',1476771747142,'Leblanc',NULL,0,'{\"CNAME\":\"B63BAC3B49D80E1F\",\"ID\":\"99ADFF456950DD96\",\"NAME\":\"B63BAC3B49D80E1F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Leon','Leon',1476771747142,'Leon',NULL,0,'{\"CNAME\":\"5C443B2003676FA5\",\"ID\":\"8040837089CDF46\",\"NAME\":\"5C443B2003676FA5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Leonard','Leonard',1476771747142,'Leonard',NULL,0,'{\"CNAME\":\"C539EADB15CE2243\",\"ID\":\"35309226EB45EC36\",\"NAME\":\"C539EADB15CE2243\"}',NULL,NULL,1),('ConsumerBizListItem','1_Leslie','Leslie',1476771747142,'Leslie',NULL,0,'{\"CNAME\":\"21BB5BB51758EAB1\",\"ID\":\"D63FBF8C3173730F\",\"NAME\":\"21BB5BB51758EAB1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lester','Lester',1476771747142,'Lester',NULL,0,'{\"CNAME\":\"21426B290D975EC5\",\"ID\":\"748BA69D3E8D1AF8\",\"NAME\":\"21426B290D975EC5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Levine','Levine',1476771747142,'Levine',NULL,0,'{\"CNAME\":\"3229BF45CB9D7B51\",\"ID\":\"B5A1FC2085986034\",\"NAME\":\"3229BF45CB9D7B51\"}',NULL,NULL,1),('ConsumerBizListItem','1_Levy','Levy',1476771747142,'Levy',NULL,0,'{\"CNAME\":\"6639C3233DFA712E\",\"ID\":\"D1A69640D53A32A9\",\"NAME\":\"6639C3233DFA712E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lewis','Lewis',1476771747142,'Lewis',NULL,0,'{\"CNAME\":\"FA348EFCD3BB1A1F\",\"ID\":\"B60C5AB647A27045\",\"NAME\":\"FA348EFCD3BB1A1F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lindsay','Lindsay',1476771747142,'Lindsay',NULL,0,'{\"CNAME\":\"DD7405DCDF0150DA\",\"ID\":\"17E23E50BEDC63B4\",\"NAME\":\"DD7405DCDF0150DA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lindsey','Lindsey',1476771747142,'Lindsey',NULL,0,'{\"CNAME\":\"102DE5E5A79546E1\",\"ID\":\"CDA72177EBA360FF\",\"NAME\":\"102DE5E5A79546E1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lisa','Lisa',1476771747142,'Lisa',NULL,0,'{\"CNAME\":\"ED14F4A4D7ECDDB6\",\"ID\":\"1373B284BC381890\",\"NAME\":\"ED14F4A4D7ECDDB6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Livingston','Livingston',1476771747142,'Livingston',NULL,0,'{\"CNAME\":\"A79E464A15C3B095\",\"ID\":\"D010396CA8ABF6EA\",\"NAME\":\"A79E464A15C3B095\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lloyd','Lloyd',1476771747142,'Lloyd',NULL,0,'{\"CNAME\":\"344C8024766AC77D\",\"ID\":\"819C9FBFB075D62A\",\"NAME\":\"344C8024766AC77D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Logan','Logan',1476771747142,'Logan',NULL,0,'{\"CNAME\":\"3447ADFD742CDFB9\",\"ID\":\"F337D999D9AD116A\",\"NAME\":\"3447ADFD742CDFB9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lopez','Lopez',1476771747142,'Lopez',NULL,0,'{\"CNAME\":\"C5A1A98649A7874D\",\"ID\":\"89AE0FE22C47D374\",\"NAME\":\"C5A1A98649A7874D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lowe','Lowe',1476771747142,'Lowe',NULL,0,'{\"CNAME\":\"964267E48ED1FB4F\",\"ID\":\"AFF0A6A452123297\",\"NAME\":\"964267E48ED1FB4F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lozano','Lozano',1476771747142,'Lozano',NULL,0,'{\"CNAME\":\"C7441D0121C8145D\",\"ID\":\"204DA255AEA2CD4A\",\"NAME\":\"C7441D0121C8145D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lucas','Lucas',1476771747142,'Lucas',NULL,0,'{\"CNAME\":\"DC53FC4F621C80BD\",\"ID\":\"35464C848F410E55\",\"NAME\":\"DC53FC4F621C80BD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lucero','Lucero',1476771747142,'Lucero',NULL,0,'{\"CNAME\":\"21F45967F1504F4E\",\"ID\":\"C88D8D0A60977545\",\"NAME\":\"21F45967F1504F4E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Luke','Luke',1476771747142,'Luke',NULL,0,'{\"CNAME\":\"46ECBEC5EC7951CE\",\"ID\":\"6B8EBA4355174221\",\"NAME\":\"46ECBEC5EC7951CE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Luna','Luna',1476771747142,'Luna',NULL,0,'{\"CNAME\":\"BA8A48B0E34226A2\",\"ID\":\"4E6CD95227CB0C28\",\"NAME\":\"BA8A48B0E34226A2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lutz','Lutz',1476771747142,'Lutz',NULL,0,'{\"CNAME\":\"E1237D6262AB3169\",\"ID\":\"351B33587C5FDD93\",\"NAME\":\"E1237D6262AB3169\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lynn','Lynn',1476771747142,'Lynn',NULL,0,'{\"CNAME\":\"32B5B2C608BD455\",\"ID\":\"18EAD4C77C3F40DA\",\"NAME\":\"32B5B2C608BD455\"}',NULL,NULL,1),('ConsumerBizListItem','1_Lyons','Lyons',1476771747142,'Lyons',NULL,0,'{\"CNAME\":\"8565E42B4518C572\",\"ID\":\"98986C005E5DEF2D\",\"NAME\":\"8565E42B4518C572\"}',NULL,NULL,1),('ConsumerBizListItem','1_Macdonald','Macdonald',1476771747142,'Macdonald',NULL,0,'{\"CNAME\":\"37E7E2604A3EE9EC\",\"ID\":\"B2DD140336C9DF86\",\"NAME\":\"37E7E2604A3EE9EC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Macias','Macias',1476771747142,'Macias',NULL,0,'{\"CNAME\":\"BB83240B6E5A6D73\",\"ID\":\"4E9CEC1F58305645\",\"NAME\":\"BB83240B6E5A6D73\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mack','Mack',1476771747142,'Mack',NULL,0,'{\"CNAME\":\"6A07C26809D867D2\",\"ID\":\"596F713F9A7376FE\",\"NAME\":\"6A07C26809D867D2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Maddox','Maddox',1476771747142,'Maddox',NULL,0,'{\"CNAME\":\"90110A19BB45EF79\",\"ID\":\"29921001F2F04BD3\",\"NAME\":\"90110A19BB45EF79\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mahoney','Mahoney',1476771747142,'Mahoney',NULL,0,'{\"CNAME\":\"151AB86134A27789\",\"ID\":\"20C8BFAC8DE160D\",\"NAME\":\"151AB86134A27789\"}',NULL,NULL,1),('ConsumerBizListItem','1_Maldonado','Maldonado',1476771747142,'Maldonado',NULL,0,'{\"CNAME\":\"7EE17937AF0E463C\",\"ID\":\"3A20F62A0AF1AA15\",\"NAME\":\"7EE17937AF0E463C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Malone','Malone',1476771747142,'Malone',NULL,0,'{\"CNAME\":\"91383AA090C55000\",\"ID\":\"B132ECC1609BFCF3\",\"NAME\":\"91383AA090C55000\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mann','Mann',1476771747142,'Mann',NULL,0,'{\"CNAME\":\"53B606C783F6BA91\",\"ID\":\"92AF93F73FAF3CEF\",\"NAME\":\"53B606C783F6BA91\"}',NULL,NULL,1),('ConsumerBizListItem','1_Marcus','Marcus',1476771747142,'Marcus',NULL,0,'{\"CNAME\":\"49C167D7CD66DC64\",\"ID\":\"4D6E4749289C4EC5\",\"NAME\":\"49C167D7CD66DC64\"}',NULL,NULL,1),('ConsumerBizListItem','1_Margaret','Margaret',1476771747142,'Margaret',NULL,0,'{\"CNAME\":\"764B1284A3E013E3\",\"ID\":\"FACF9F743B083008\",\"NAME\":\"764B1284A3E013E3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Marquez','Marquez',1476771747142,'Marquez',NULL,0,'{\"CNAME\":\"6C9F28A7BFA28DB7\",\"ID\":\"D961E9F236177D65\",\"NAME\":\"6C9F28A7BFA28DB7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Marshall','Marshall',1476771747142,'Marshall',NULL,0,'{\"CNAME\":\"701FB90716E10ECC\",\"ID\":\"BE53EE6110493523\",\"NAME\":\"701FB90716E10ECC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Martinez','Martinez',1476771747142,'Martinez',NULL,0,'{\"CNAME\":\"D2F0C294711426F4\",\"ID\":\"18D10DC6E666EAB6\",\"NAME\":\"D2F0C294711426F4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mary','Mary',1476771747142,'Mary',NULL,0,'{\"CNAME\":\"B8E7BE5DFA2CE071\",\"ID\":\"DFA92D8F817E5B08\",\"NAME\":\"B8E7BE5DFA2CE071\"}',NULL,NULL,1),('ConsumerBizListItem','1_Massey','Massey',1476771747142,'Massey',NULL,0,'{\"CNAME\":\"1C34523403B86C6D\",\"ID\":\"655EA4BD3B5736D8\",\"NAME\":\"1C34523403B86C6D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mata','Mata',1476771747142,'Mata',NULL,0,'{\"CNAME\":\"1C99C5A7D118311C\",\"ID\":\"7949E456002B2898\",\"NAME\":\"1C99C5A7D118311C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mathews','Mathews',1476771747142,'Mathews',NULL,0,'{\"CNAME\":\"65702568F1EBE36A\",\"ID\":\"7BD28F15A49D5E58\",\"NAME\":\"65702568F1EBE36A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mathis','Mathis',1476771747142,'Mathis',NULL,0,'{\"CNAME\":\"C909BAA24666B732\",\"ID\":\"452BF208BF901322\",\"NAME\":\"C909BAA24666B732\"}',NULL,NULL,1),('ConsumerBizListItem','1_Matthew','Matthew',1476771747142,'Matthew',NULL,0,'{\"CNAME\":\"E6A5BA0842A53116\",\"ID\":\"A1AFC58C6CA9540D\",\"NAME\":\"E6A5BA0842A53116\"}',NULL,NULL,1),('ConsumerBizListItem','1_Matthews','Matthews',1476771747142,'Matthews',NULL,0,'{\"CNAME\":\"F44436329CC331D4\",\"ID\":\"D4F4805C36DC685\",\"NAME\":\"F44436329CC331D4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Maxwell','Maxwell',1476771747142,'Maxwell',NULL,0,'{\"CNAME\":\"8601F6E1028A8E8A\",\"ID\":\"70EFBA66D3D8D531\",\"NAME\":\"8601F6E1028A8E8A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mayer','Mayer',1476771747142,'Mayer',NULL,0,'{\"CNAME\":\"78DEFF6B70CA7566\",\"ID\":\"AF5AFD7F7C807171\",\"NAME\":\"78DEFF6B70CA7566\"}',NULL,NULL,1),('ConsumerBizListItem','1_Maynard','Maynard',1476771747142,'Maynard',NULL,0,'{\"CNAME\":\"82DF4F948DB1FA3C\",\"ID\":\"7E1D842D0F7EE600\",\"NAME\":\"82DF4F948DB1FA3C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcbride','Mcbride',1476771747142,'Mcbride',NULL,0,'{\"CNAME\":\"9B6D1FF8539640B2\",\"ID\":\"95D309F0B035D97F\",\"NAME\":\"9B6D1FF8539640B2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccall','Mccall',1476771747142,'Mccall',NULL,0,'{\"CNAME\":\"ECD6889B95F15270\",\"ID\":\"ED4227734ED75D34\",\"NAME\":\"ECD6889B95F15270\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccann','Mccann',1476771747142,'Mccann',NULL,0,'{\"CNAME\":\"37F4E81CB8415802\",\"ID\":\"DC5C768B5DC76A08\",\"NAME\":\"37F4E81CB8415802\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccarthy','Mccarthy',1476771747142,'Mccarthy',NULL,0,'{\"CNAME\":\"2FCEAA1D9AFE4AE\",\"ID\":\"88A199611AC2B85B\",\"NAME\":\"2FCEAA1D9AFE4AE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccarty','Mccarty',1476771747142,'Mccarty',NULL,0,'{\"CNAME\":\"BE1E11719506AD3E\",\"ID\":\"B710915795B9E9C0\",\"NAME\":\"BE1E11719506AD3E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcclain','Mcclain',1476771747142,'Mcclain',NULL,0,'{\"CNAME\":\"AB350E225A781999\",\"ID\":\"76023EDC9187CF1\",\"NAME\":\"AB350E225A781999\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcclure','Mcclure',1476771747142,'Mcclure',NULL,0,'{\"CNAME\":\"BC34A7CDFA9597C2\",\"ID\":\"277281AADA22045C\",\"NAME\":\"BC34A7CDFA9597C2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcconnell','Mcconnell',1476771747142,'Mcconnell',NULL,0,'{\"CNAME\":\"B6B377518F437C92\",\"ID\":\"228499B55310264A\",\"NAME\":\"B6B377518F437C92\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccormick','Mccormick',1476771747142,'Mccormick',NULL,0,'{\"CNAME\":\"6460782DD7DED651\",\"ID\":\"8F19793B2671094E\",\"NAME\":\"6460782DD7DED651\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccoy','Mccoy',1476771747142,'Mccoy',NULL,0,'{\"CNAME\":\"5865D1A909CACD53\",\"ID\":\"49AF6C4E558A7569\",\"NAME\":\"5865D1A909CACD53\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mccullough','Mccullough',1476771747142,'Mccullough',NULL,0,'{\"CNAME\":\"90707A96DBB6EEE0\",\"ID\":\"894B77F805BD94D2\",\"NAME\":\"90707A96DBB6EEE0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcdaniel','Mcdaniel',1476771747142,'Mcdaniel',NULL,0,'{\"CNAME\":\"B3195FAA3C6F3701\",\"ID\":\"DCA5672FF3444C7E\",\"NAME\":\"B3195FAA3C6F3701\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcdonald','Mcdonald',1476771747142,'Mcdonald',NULL,0,'{\"CNAME\":\"F336FCF927BC87FB\",\"ID\":\"BCB41CCDC4363C68\",\"NAME\":\"F336FCF927BC87FB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcdowell','Mcdowell',1476771747142,'Mcdowell',NULL,0,'{\"CNAME\":\"1135BC2FCD4F7D57\",\"ID\":\"5129A5DDCD0DCD75\",\"NAME\":\"1135BC2FCD4F7D57\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcfarland','Mcfarland',1476771747142,'Mcfarland',NULL,0,'{\"CNAME\":\"94F3C3540FBFA81F\",\"ID\":\"D72FBBCCD9FE64C3\",\"NAME\":\"94F3C3540FBFA81F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcgee','Mcgee',1476771747142,'Mcgee',NULL,0,'{\"CNAME\":\"6EAF31951D2176E6\",\"ID\":\"6A81A4FB98D149F\",\"NAME\":\"6EAF31951D2176E6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcgrath','Mcgrath',1476771747142,'Mcgrath',NULL,0,'{\"CNAME\":\"245027B440EADAC4\",\"ID\":\"EBCC77DC72360D0\",\"NAME\":\"245027B440EADAC4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcguire','Mcguire',1476771747142,'Mcguire',NULL,0,'{\"CNAME\":\"C761E938068AD151\",\"ID\":\"D88518ACBCC3D08D\",\"NAME\":\"C761E938068AD151\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcintosh','Mcintosh',1476771747142,'Mcintosh',NULL,0,'{\"CNAME\":\"787DD39CA1382A00\",\"ID\":\"309FEE4E541E51DE\",\"NAME\":\"787DD39CA1382A00\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcintyre','Mcintyre',1476771747142,'Mcintyre',NULL,0,'{\"CNAME\":\"4C75129D2DC165AD\",\"ID\":\"87EC2F451208DF97\",\"NAME\":\"4C75129D2DC165AD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mckay','Mckay',1476771747142,'Mckay',NULL,0,'{\"CNAME\":\"2C94281CA7BD540C\",\"ID\":\"9C19A2AA1D84E04B\",\"NAME\":\"2C94281CA7BD540C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mckee','Mckee',1476771747142,'Mckee',NULL,0,'{\"CNAME\":\"C2983225648BC38A\",\"ID\":\"83E8EF518174E1EB\",\"NAME\":\"C2983225648BC38A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mckenzie','Mckenzie',1476771747142,'Mckenzie',NULL,0,'{\"CNAME\":\"330AABD33B0256C6\",\"ID\":\"9E984C108157CEA7\",\"NAME\":\"330AABD33B0256C6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mckinney','Mckinney',1476771747142,'Mckinney',NULL,0,'{\"CNAME\":\"D07AB186DDA62EDF\",\"ID\":\"C559DA2BA967EB82\",\"NAME\":\"D07AB186DDA62EDF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcknight','Mcknight',1476771747142,'Mcknight',NULL,0,'{\"CNAME\":\"1499337C57912882\",\"ID\":\"1BC0249A6412EF49\",\"NAME\":\"1499337C57912882\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mclaughlin','Mclaughlin',1476771747142,'Mclaughlin',NULL,0,'{\"CNAME\":\"FEEA0505CFCBD326\",\"ID\":\"F3173935ED8AC4BF\",\"NAME\":\"FEEA0505CFCBD326\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mclean','Mclean',1476771747142,'Mclean',NULL,0,'{\"CNAME\":\"18AD91BC2C102AB7\",\"ID\":\"A368B0DE8B91CFB3\",\"NAME\":\"18AD91BC2C102AB7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcmahon','Mcmahon',1476771747142,'Mcmahon',NULL,0,'{\"CNAME\":\"9E04ED639B91F1EB\",\"ID\":\"F18A6D1CDE4B2051\",\"NAME\":\"9E04ED639B91F1EB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcmillan','Mcmillan',1476771747142,'Mcmillan',NULL,0,'{\"CNAME\":\"89B38D0B03FDDED0\",\"ID\":\"495DABFD0CA768A3\",\"NAME\":\"89B38D0B03FDDED0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcneil','Mcneil',1476771747142,'Mcneil',NULL,0,'{\"CNAME\":\"2D44FBCDB27C1804\",\"ID\":\"9597353E41E6957B\",\"NAME\":\"2D44FBCDB27C1804\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mcpherson','Mcpherson',1476771747142,'Mcpherson',NULL,0,'{\"CNAME\":\"2DF766484A3093AD\",\"ID\":\"FAAFDA66202D2344\",\"NAME\":\"2DF766484A3093AD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Megan','Megan',1476771747142,'Megan',NULL,0,'{\"CNAME\":\"99C307A7BCD9067E\",\"ID\":\"A2CC63E065705FE9\",\"NAME\":\"99C307A7BCD9067E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Meghan','Meghan',1476771747142,'Meghan',NULL,0,'{\"CNAME\":\"97E3613AEAD155D2\",\"ID\":\"A14AC55A4F27472C\",\"NAME\":\"97E3613AEAD155D2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mejia','Mejia',1476771747142,'Mejia',NULL,0,'{\"CNAME\":\"5B4A3F6CF7B30A99\",\"ID\":\"7A6A74CBE87BC600\",\"NAME\":\"5B4A3F6CF7B30A99\"}',NULL,NULL,1),('ConsumerBizListItem','1_Melendez','Melendez',1476771747142,'Melendez',NULL,0,'{\"CNAME\":\"B24C0E255D13C4C9\",\"ID\":\"52947E0ADE57A09E\",\"NAME\":\"B24C0E255D13C4C9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Melinda','Melinda',1476771747142,'Melinda',NULL,0,'{\"CNAME\":\"19B63690A34F20C9\",\"ID\":\"69D658D0B2859E32\",\"NAME\":\"19B63690A34F20C9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Melissa','Melissa',1476771747142,'Melissa',NULL,0,'{\"CNAME\":\"FF5390BDE5A4CF0A\",\"ID\":\"E9FD7C2C6623306D\",\"NAME\":\"FF5390BDE5A4CF0A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Melton','Melton',1476771747142,'Melton',NULL,0,'{\"CNAME\":\"36397819A8E877F9\",\"ID\":\"71A58E8CB75904F2\",\"NAME\":\"36397819A8E877F9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mendez','Mendez',1476771747142,'Mendez',NULL,0,'{\"CNAME\":\"9E1873C5561CD882\",\"ID\":\"962E56A8A0B0420D\",\"NAME\":\"9E1873C5561CD882\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mendoza','Mendoza',1476771747142,'Mendoza',NULL,0,'{\"CNAME\":\"CFCD7931AB079AE3\",\"ID\":\"297FA7777981F402\",\"NAME\":\"CFCD7931AB079AE3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mercado','Mercado',1476771747142,'Mercado',NULL,0,'{\"CNAME\":\"5BD053A2E3D78B78\",\"ID\":\"F60BB6BB4C96D4DF\",\"NAME\":\"5BD053A2E3D78B78\"}',NULL,NULL,1),('ConsumerBizListItem','1_Merritt','Merritt',1476771747142,'Merritt',NULL,0,'{\"CNAME\":\"31A9D83E188E5C3A\",\"ID\":\"D1DC3A8270A6F939\",\"NAME\":\"31A9D83E188E5C3A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Meyer','Meyer',1476771747142,'Meyer',NULL,0,'{\"CNAME\":\"B6273C0BA3AE37A4\",\"ID\":\"806BEAFE154032A5\",\"NAME\":\"B6273C0BA3AE37A4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Meyers','Meyers',1476771747142,'Meyers',NULL,0,'{\"CNAME\":\"CD5887A7A6549C19\",\"ID\":\"4462BF0DDBE0D0DA\",\"NAME\":\"CD5887A7A6549C19\"}',NULL,NULL,1),('ConsumerBizListItem','1_Meza','Meza',1476771747142,'Meza',NULL,0,'{\"CNAME\":\"291CDFED91F4DD0E\",\"ID\":\"C7AF0926B294E47E\",\"NAME\":\"291CDFED91F4DD0E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Michael','Michael',1476771747142,'Michael',NULL,0,'{\"CNAME\":\"ACF4539A14B3AA2\",\"ID\":\"FC528592C3858F90\",\"NAME\":\"ACF4539A14B3AA2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Michelle','Michelle',1476771747142,'Michelle',NULL,0,'{\"CNAME\":\"2345F10BB948C566\",\"ID\":\"9327969053C0068D\",\"NAME\":\"2345F10BB948C566\"}',NULL,NULL,1),('ConsumerBizListItem','1_Middleton','Middleton',1476771747142,'Middleton',NULL,0,'{\"CNAME\":\"DBA4F851FE6C5209\",\"ID\":\"A981F2B708044D6F\",\"NAME\":\"DBA4F851FE6C5209\"}',NULL,NULL,1),('ConsumerBizListItem','1_Miranda','Miranda',1476771747142,'Miranda',NULL,0,'{\"CNAME\":\"1EE1877C6655ECC7\",\"ID\":\"BC573864331A9E42\",\"NAME\":\"1EE1877C6655ECC7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mitchell','Mitchell',1476771747142,'Mitchell',NULL,0,'{\"CNAME\":\"84BF25CB5D7EA95\",\"ID\":\"97D0145823AEB8ED\",\"NAME\":\"84BF25CB5D7EA95\"}',NULL,NULL,1),('ConsumerBizListItem','1_Molina','Molina',1476771747142,'Molina',NULL,0,'{\"CNAME\":\"21EC6379D30E841A\",\"ID\":\"EFB76CFF97AAF057\",\"NAME\":\"21EC6379D30E841A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Molly','Molly',1476771747142,'Molly',NULL,0,'{\"CNAME\":\"ED6658E6F22583ED\",\"ID\":\"5C50B4DF4B176845\",\"NAME\":\"ED6658E6F22583ED\"}',NULL,NULL,1),('ConsumerBizListItem','1_Monica','Monica',1476771747142,'Monica',NULL,0,'{\"CNAME\":\"FF0D813DD5D2F64D\",\"ID\":\"46031B3D04DC9099\",\"NAME\":\"FF0D813DD5D2F64D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Monroe','Monroe',1476771747142,'Monroe',NULL,0,'{\"CNAME\":\"C36C86822C55925D\",\"ID\":\"6BA3AF5D7B2790E7\",\"NAME\":\"C36C86822C55925D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Montes','Montes',1476771747142,'Montes',NULL,0,'{\"CNAME\":\"D7BCF0975602FF8B\",\"ID\":\"771FC6F0F4B1D7D\",\"NAME\":\"D7BCF0975602FF8B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Montgomery','Montgomery',1476771747142,'Montgomery',NULL,0,'{\"CNAME\":\"867521B04F541373\",\"ID\":\"21FE5B8BA755EEAE\",\"NAME\":\"867521B04F541373\"}',NULL,NULL,1),('ConsumerBizListItem','1_Montoya','Montoya',1476771747142,'Montoya',NULL,0,'{\"CNAME\":\"E7C53599DFB70EC\",\"ID\":\"6DD4E10E3296FA63\",\"NAME\":\"E7C53599DFB70EC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mooney','Mooney',1476771747142,'Mooney',NULL,0,'{\"CNAME\":\"C5070384DA2C69D7\",\"ID\":\"42A3964579017F3C\",\"NAME\":\"C5070384DA2C69D7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Moore','Moore',1476771747142,'Moore',NULL,0,'{\"CNAME\":\"A595AAB986C3206E\",\"ID\":\"1C54985E4F95B781\",\"NAME\":\"A595AAB986C3206E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mora','Mora',1476771747142,'Mora',NULL,0,'{\"CNAME\":\"C427ED4DC3E638AD\",\"ID\":\"52C5189391854C93\",\"NAME\":\"C427ED4DC3E638AD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Morales','Morales',1476771747142,'Morales',NULL,0,'{\"CNAME\":\"5E0C550EBC5DB9F1\",\"ID\":\"7E230522657ECDC5\",\"NAME\":\"5E0C550EBC5DB9F1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Moran','Moran',1476771747142,'Moran',NULL,0,'{\"CNAME\":\"D539D9CE1D931FDD\",\"ID\":\"3C1E4BD67169B815\",\"NAME\":\"D539D9CE1D931FDD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Moreno','Moreno',1476771747142,'Moreno',NULL,0,'{\"CNAME\":\"782CA1470254D6C9\",\"ID\":\"84F0F20482CDE7E5\",\"NAME\":\"782CA1470254D6C9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Morgan','Morgan',1476771747142,'Morgan',NULL,0,'{\"CNAME\":\"DAE4A923E4AE71D\",\"ID\":\"10C272D06794D3E5\",\"NAME\":\"DAE4A923E4AE71D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Morris','Morris',1476771747142,'Morris',NULL,0,'{\"CNAME\":\"EC726FD938BBF930\",\"ID\":\"81C650CAAC28CDEF\",\"NAME\":\"EC726FD938BBF930\"}',NULL,NULL,1),('ConsumerBizListItem','1_Morrison','Morrison',1476771747142,'Morrison',NULL,0,'{\"CNAME\":\"ACBFC55415A34579\",\"ID\":\"F670EF5D2D6BDF8F\",\"NAME\":\"ACBFC55415A34579\"}',NULL,NULL,1),('ConsumerBizListItem','1_Morse','Morse',1476771747142,'Morse',NULL,0,'{\"CNAME\":\"A3110069C6DBE435\",\"ID\":\"89F03F7D02720160\",\"NAME\":\"A3110069C6DBE435\"}',NULL,NULL,1),('ConsumerBizListItem','1_Morton','Morton',1476771747142,'Morton',NULL,0,'{\"CNAME\":\"A2D6BEBF652BB9FC\",\"ID\":\"C1FEA270C48E8079\",\"NAME\":\"A2D6BEBF652BB9FC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Moses','Moses',1476771747142,'Moses',NULL,0,'{\"CNAME\":\"52BD43D37ED62EB4\",\"ID\":\"E4873AA9A05CC5ED\",\"NAME\":\"52BD43D37ED62EB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mosley','Mosley',1476771747142,'Mosley',NULL,0,'{\"CNAME\":\"A54704082C48C048\",\"ID\":\"8D420FA35754D1F1\",\"NAME\":\"A54704082C48C048\"}',NULL,NULL,1),('ConsumerBizListItem','1_Moyer','Moyer',1476771747142,'Moyer',NULL,0,'{\"CNAME\":\"4E9275DE7693E38\",\"ID\":\"7437D136770F5B35\",\"NAME\":\"4E9275DE7693E38\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mueller','Mueller',1476771747142,'Mueller',NULL,0,'{\"CNAME\":\"54AB4C9DF68F3284\",\"ID\":\"96DE2547F44254C9\",\"NAME\":\"54AB4C9DF68F3284\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mullen','Mullen',1476771747142,'Mullen',NULL,0,'{\"CNAME\":\"A093F1FE1373512A\",\"ID\":\"973A5F0CCBC4EE35\",\"NAME\":\"A093F1FE1373512A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Mullins','Mullins',1476771747142,'Mullins',NULL,0,'{\"CNAME\":\"58761D7B8CC732B3\",\"ID\":\"BE3E9D3F7D705373\",\"NAME\":\"58761D7B8CC732B3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Munoz','Munoz',1476771747142,'Munoz',NULL,0,'{\"CNAME\":\"3A119899C7DD59E4\",\"ID\":\"207F88018F722375\",\"NAME\":\"3A119899C7DD59E4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Murillo','Murillo',1476771747142,'Murillo',NULL,0,'{\"CNAME\":\"8B63B8397312105C\",\"ID\":\"B147A61C1D07C1C9\",\"NAME\":\"8B63B8397312105C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Murphy','Murphy',1476771747142,'Murphy',NULL,0,'{\"CNAME\":\"25EE0C0C5392C0FA\",\"ID\":\"9D2682367C3935DE\",\"NAME\":\"25EE0C0C5392C0FA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Murray','Murray',1476771747142,'Murray',NULL,0,'{\"CNAME\":\"39E7F45C25DFE9F7\",\"ID\":\"CC42ACC8CE334185\",\"NAME\":\"39E7F45C25DFE9F7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Myers','Myers',1476771747142,'Myers',NULL,0,'{\"CNAME\":\"979BF8440678E87F\",\"ID\":\"712A3C9878EFEAE8\",\"NAME\":\"979BF8440678E87F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nash','Nash',1476771747142,'Nash',NULL,0,'{\"CNAME\":\"CFE26A9EFB0E689B\",\"ID\":\"A40511CAD8383E5A\",\"NAME\":\"CFE26A9EFB0E689B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Natalie','Natalie',1476771747142,'Natalie',NULL,0,'{\"CNAME\":\"F9E1F3AE15198F81\",\"ID\":\"C0560792E4A3C79E\",\"NAME\":\"F9E1F3AE15198F81\"}',NULL,NULL,1),('ConsumerBizListItem','1_Natasha','Natasha',1476771747142,'Natasha',NULL,0,'{\"CNAME\":\"6275E26419211D1F\",\"ID\":\"E1696007BE4EEFB8\",\"NAME\":\"6275E26419211D1F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nathan','Nathan',1476771747142,'Nathan',NULL,0,'{\"CNAME\":\"1404834E52A4C6CA\",\"ID\":\"9A4400501FEBB2A9\",\"NAME\":\"1404834E52A4C6CA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nathaniel','Nathaniel',1476771747142,'Nathaniel',NULL,0,'{\"CNAME\":\"525EF3E7827F41BE\",\"ID\":\"7D12B66D3DF6AF8D\",\"NAME\":\"525EF3E7827F41BE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Navarro','Navarro',1476771747142,'Navarro',NULL,0,'{\"CNAME\":\"488E58FD6265BFA6\",\"ID\":\"B2531E7BB29BF22E\",\"NAME\":\"488E58FD6265BFA6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Neal','Neal',1476771747142,'Neal',NULL,0,'{\"CNAME\":\"E9504B1CFBA7C36\",\"ID\":\"E449B9317DAD920C\",\"NAME\":\"E9504B1CFBA7C36\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nelson','Nelson',1476771747142,'Nelson',NULL,0,'{\"CNAME\":\"A4E360681676C770\",\"ID\":\"4F398CB9D6BC79AE\",\"NAME\":\"A4E360681676C770\"}',NULL,NULL,1),('ConsumerBizListItem','1_Newman','Newman',1476771747142,'Newman',NULL,0,'{\"CNAME\":\"EE099107DC8C1EE\",\"ID\":\"E8D92F99EDD25E2C\",\"NAME\":\"EE099107DC8C1EE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Newton','Newton',1476771747142,'Newton',NULL,0,'{\"CNAME\":\"DA6FA909F1C0188C\",\"ID\":\"86D7C8A08B4AAA1B\",\"NAME\":\"DA6FA909F1C0188C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nguyen','Nguyen',1476771747142,'Nguyen',NULL,0,'{\"CNAME\":\"27FF2FFE376B2EDC\",\"ID\":\"158FC2DDD52EC2CF\",\"NAME\":\"27FF2FFE376B2EDC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nicholas','Nicholas',1476771747142,'Nicholas',NULL,0,'{\"CNAME\":\"532AB4D2BBCC4613\",\"ID\":\"B0F2AD44D26E1A6F\",\"NAME\":\"532AB4D2BBCC4613\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nichole','Nichole',1476771747142,'Nichole',NULL,0,'{\"CNAME\":\"1CB0231EBAA3F6F5\",\"ID\":\"CDF1035C34EC3802\",\"NAME\":\"1CB0231EBAA3F6F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nichols','Nichols',1476771747142,'Nichols',NULL,0,'{\"CNAME\":\"28043B2FD75E1A4F\",\"ID\":\"148510031349642D\",\"NAME\":\"28043B2FD75E1A4F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nicholson','Nicholson',1476771747142,'Nicholson',NULL,0,'{\"CNAME\":\"C7122AC077CA6A56\",\"ID\":\"647C722BF90A4914\",\"NAME\":\"C7122AC077CA6A56\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nicole','Nicole',1476771747142,'Nicole',NULL,0,'{\"CNAME\":\"FC63F87C08D50526\",\"ID\":\"2451041557A22145\",\"NAME\":\"FC63F87C08D50526\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nielsen','Nielsen',1476771747142,'Nielsen',NULL,0,'{\"CNAME\":\"4A5DBD140D59B7A8\",\"ID\":\"A0F3601DC6820364\",\"NAME\":\"4A5DBD140D59B7A8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nixon','Nixon',1476771747142,'Nixon',NULL,0,'{\"CNAME\":\"37A7D989ABE835F4\",\"ID\":\"757F843A169CC678\",\"NAME\":\"37A7D989ABE835F4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nolan','Nolan',1476771747142,'Nolan',NULL,0,'{\"CNAME\":\"F66C8824D7520D7F\",\"ID\":\"64F1F27BF1B4EC22\",\"NAME\":\"F66C8824D7520D7F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Norman','Norman',1476771747142,'Norman',NULL,0,'{\"CNAME\":\"9AC915832A9A1C97\",\"ID\":\"831C2F88A604A07C\",\"NAME\":\"9AC915832A9A1C97\"}',NULL,NULL,1),('ConsumerBizListItem','1_Norris','Norris',1476771747142,'Norris',NULL,0,'{\"CNAME\":\"44041E67A48E7629\",\"ID\":\"8E6BEA8E90BA87A\",\"NAME\":\"44041E67A48E7629\"}',NULL,NULL,1),('ConsumerBizListItem','1_Norton','Norton',1476771747142,'Norton',NULL,0,'{\"CNAME\":\"1B7D9167AB164F30\",\"ID\":\"2CB6B10338A7FC41\",\"NAME\":\"1B7D9167AB164F30\"}',NULL,NULL,1),('ConsumerBizListItem','1_Novak','Novak',1476771747142,'Novak',NULL,0,'{\"CNAME\":\"F4EF2C3931255364\",\"ID\":\"E6384711491713D2\",\"NAME\":\"F4EF2C3931255364\"}',NULL,NULL,1),('ConsumerBizListItem','1_Nunez','Nunez',1476771747142,'Nunez',NULL,0,'{\"CNAME\":\"D7F8F7AC0C50BB1C\",\"ID\":\"11D0E6287202FCED\",\"NAME\":\"D7F8F7AC0C50BB1C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Obrien','Obrien',1476771747142,'Obrien',NULL,0,'{\"CNAME\":\"10CD26A9686C48A3\",\"ID\":\"F50A6C02A3FC5A3A\",\"NAME\":\"10CD26A9686C48A3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ochoa','Ochoa',1476771747142,'Ochoa',NULL,0,'{\"CNAME\":\"446D8DA06145F77B\",\"ID\":\"6A81681A7AF700C6\",\"NAME\":\"446D8DA06145F77B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Oconnell','Oconnell',1476771747142,'Oconnell',NULL,0,'{\"CNAME\":\"F23B6FF5885D033C\",\"ID\":\"FDDD7938A71DB5F8\",\"NAME\":\"F23B6FF5885D033C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Oconnor','Oconnor',1476771747142,'Oconnor',NULL,0,'{\"CNAME\":\"AF9E7E526B574C73\",\"ID\":\"1EF91C212E30E14B\",\"NAME\":\"AF9E7E526B574C73\"}',NULL,NULL,1),('ConsumerBizListItem','1_Odom','Odom',1476771747142,'Odom',NULL,0,'{\"CNAME\":\"4C21317940F3EC56\",\"ID\":\"5D79099FCDF499F1\",\"NAME\":\"4C21317940F3EC56\"}',NULL,NULL,1),('ConsumerBizListItem','1_Odonnell','Odonnell',1476771747142,'Odonnell',NULL,0,'{\"CNAME\":\"A973D25D14EDEAAF\",\"ID\":\"B618C3210E934362\",\"NAME\":\"A973D25D14EDEAAF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Oliver','Oliver',1476771747142,'Oliver',NULL,0,'{\"CNAME\":\"ACAE273A5A5C88B4\",\"ID\":\"7FEA637FD6D02B8F\",\"NAME\":\"ACAE273A5A5C88B4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Olsen','Olsen',1476771747142,'Olsen',NULL,0,'{\"CNAME\":\"D141F75795C7D7A2\",\"ID\":\"AE614C557843B1DF\",\"NAME\":\"D141F75795C7D7A2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Olson','Olson',1476771747142,'Olson',NULL,0,'{\"CNAME\":\"B3E5969A22C3D2C6\",\"ID\":\"7C4EDE33A62160A1\",\"NAME\":\"B3E5969A22C3D2C6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Oneal','Oneal',1476771747142,'Oneal',NULL,0,'{\"CNAME\":\"37B286491519E3F1\",\"ID\":\"D43AB110AB2489D6\",\"NAME\":\"37B286491519E3F1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Oneill','Oneill',1476771747142,'Oneill',NULL,0,'{\"CNAME\":\"E684AFE76B94D5B5\",\"ID\":\"8303A79B1E19A194\",\"NAME\":\"E684AFE76B94D5B5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Orozco','Orozco',1476771747142,'Orozco',NULL,0,'{\"CNAME\":\"E2EA1695C607E636\",\"ID\":\"4921F95BAF824205\",\"NAME\":\"E2EA1695C607E636\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ortega','Ortega',1476771747142,'Ortega',NULL,0,'{\"CNAME\":\"4810F530CF739946\",\"ID\":\"B59A51A3C0BF9C52\",\"NAME\":\"4810F530CF739946\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ortiz','Ortiz',1476771747142,'Ortiz',NULL,0,'{\"CNAME\":\"DD1C31205BF84100\",\"ID\":\"26751BE1181460BA\",\"NAME\":\"DD1C31205BF84100\"}',NULL,NULL,1),('ConsumerBizListItem','1_Osborn','Osborn',1476771747142,'Osborn',NULL,0,'{\"CNAME\":\"487A0E6D719733E9\",\"ID\":\"E721A54A8CF18C85\",\"NAME\":\"487A0E6D719733E9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Osborne','Osborne',1476771747142,'Osborne',NULL,0,'{\"CNAME\":\"E3501017B8CADA46\",\"ID\":\"5CF21CE30208CFFF\",\"NAME\":\"E3501017B8CADA46\"}',NULL,NULL,1),('ConsumerBizListItem','1_Owen','Owen',1476771747142,'Owen',NULL,0,'{\"CNAME\":\"43996FB100428B0D\",\"ID\":\"673271CC47C1A4E7\",\"NAME\":\"43996FB100428B0D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Owens','Owens',1476771747142,'Owens',NULL,0,'{\"CNAME\":\"D786034C5653A0F5\",\"ID\":\"9FE97FFF97F08966\",\"NAME\":\"D786034C5653A0F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pacheco','Pacheco',1476771747142,'Pacheco',NULL,0,'{\"CNAME\":\"E3B9592341F871B7\",\"ID\":\"EF2A4BE5473AB0B3\",\"NAME\":\"E3B9592341F871B7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Padilla','Padilla',1476771747142,'Padilla',NULL,0,'{\"CNAME\":\"CA581FDBAC455AF6\",\"ID\":\"1E00F2F4BFCBB75\",\"NAME\":\"CA581FDBAC455AF6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Parrish','Parrish',1476771747142,'Parrish',NULL,0,'{\"CNAME\":\"AF0A68669EBB674B\",\"ID\":\"15231A7CE4BA789D\",\"NAME\":\"AF0A68669EBB674B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Parsons','Parsons',1476771747142,'Parsons',NULL,0,'{\"CNAME\":\"4119DDD04B8B7A0C\",\"ID\":\"B9F94C77652C9A76\",\"NAME\":\"4119DDD04B8B7A0C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Patel','Patel',1476771747142,'Patel',NULL,0,'{\"CNAME\":\"64A43B6CA15D128A\",\"ID\":\"375C71349B295FBE\",\"NAME\":\"64A43B6CA15D128A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Patricia','Patricia',1476771747142,'Patricia',NULL,0,'{\"CNAME\":\"823FEC7A2632EA7B\",\"ID\":\"A588A6199FEFF5BA\",\"NAME\":\"823FEC7A2632EA7B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Patrick','Patrick',1476771747142,'Patrick',NULL,0,'{\"CNAME\":\"6C84CBD30CF9350A\",\"ID\":\"2A27B8144AC02F67\",\"NAME\":\"6C84CBD30CF9350A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Patterson','Patterson',1476771747142,'Patterson',NULL,0,'{\"CNAME\":\"2B6BB87BFB1DF29F\",\"ID\":\"8BB88F80D334B186\",\"NAME\":\"2B6BB87BFB1DF29F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Patton','Patton',1476771747142,'Patton',NULL,0,'{\"CNAME\":\"EAA46B31C6844FBE\",\"ID\":\"2F039058BD48307\",\"NAME\":\"EAA46B31C6844FBE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Paul','Paul',1476771747142,'Paul',NULL,0,'{\"CNAME\":\"6C63212AB48E8401\",\"ID\":\"C59B469D724F7919\",\"NAME\":\"6C63212AB48E8401\"}',NULL,NULL,1),('ConsumerBizListItem','1_Payne','Payne',1476771747142,'Payne',NULL,0,'{\"CNAME\":\"50AC9C8856C464E3\",\"ID\":\"52D080A3E172C33F\",\"NAME\":\"50AC9C8856C464E3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pearson','Pearson',1476771747142,'Pearson',NULL,0,'{\"CNAME\":\"BB0D62B4F0C05E65\",\"ID\":\"5A142A55461D5FEF\",\"NAME\":\"BB0D62B4F0C05E65\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pena','Pena',1476771747142,'Pena',NULL,0,'{\"CNAME\":\"27E110C11BCE4198\",\"ID\":\"A941493EEEA57EDE\",\"NAME\":\"27E110C11BCE4198\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pennington','Pennington',1476771747142,'Pennington',NULL,0,'{\"CNAME\":\"71BF8C090757EE59\",\"ID\":\"A51C896C9CB81ECB\",\"NAME\":\"71BF8C090757EE59\"}',NULL,NULL,1),('ConsumerBizListItem','1_Perez','Perez',1476771747142,'Perez',NULL,0,'{\"CNAME\":\"765BA753B609D84B\",\"ID\":\"464D828B85B0BED9\",\"NAME\":\"765BA753B609D84B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Perkins','Perkins',1476771747142,'Perkins',NULL,0,'{\"CNAME\":\"1F373CF4EE4DA606\",\"ID\":\"798ED7D4EE7138D4\",\"NAME\":\"1F373CF4EE4DA606\"}',NULL,NULL,1),('ConsumerBizListItem','1_Perry','Perry',1476771747142,'Perry',NULL,0,'{\"CNAME\":\"590B659F79AAD9E9\",\"ID\":\"EFBE98067C6C73D\",\"NAME\":\"590B659F79AAD9E9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Peter','Peter',1476771747142,'Peter',NULL,0,'{\"CNAME\":\"51DC30DDC473D43A\",\"ID\":\"66BE31E4C40D6769\",\"NAME\":\"51DC30DDC473D43A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Peters','Peters',1476771747142,'Peters',NULL,0,'{\"CNAME\":\"D54BEC109FC1C2E2\",\"ID\":\"8B5700012BE65C9D\",\"NAME\":\"D54BEC109FC1C2E2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Petersen','Petersen',1476771747142,'Petersen',NULL,0,'{\"CNAME\":\"9EC7BE155E47EF11\",\"ID\":\"FC6709BFDF0572F1\",\"NAME\":\"9EC7BE155E47EF11\"}',NULL,NULL,1),('ConsumerBizListItem','1_Peterson','Peterson',1476771747142,'Peterson',NULL,0,'{\"CNAME\":\"3E6F31D8C5F341A9\",\"ID\":\"F3144CEFE89A60D6\",\"NAME\":\"3E6F31D8C5F341A9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pham','Pham',1476771747142,'Pham',NULL,0,'{\"CNAME\":\"48C4B99727972BE6\",\"ID\":\"ACAB0116C354964A\",\"NAME\":\"48C4B99727972BE6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Phelps','Phelps',1476771747142,'Phelps',NULL,0,'{\"CNAME\":\"CA515D93D5120A22\",\"ID\":\"28DC6B0E1B33769B\",\"NAME\":\"CA515D93D5120A22\"}',NULL,NULL,1),('ConsumerBizListItem','1_Philip','Philip',1476771747142,'Philip',NULL,0,'{\"CNAME\":\"7B40760B8EBBFB7D\",\"ID\":\"404DCC91B2AEAA7C\",\"NAME\":\"7B40760B8EBBFB7D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Phillip','Phillip',1476771747142,'Phillip',NULL,0,'{\"CNAME\":\"156026B915CC5097\",\"ID\":\"8725FB777F25776F\",\"NAME\":\"156026B915CC5097\"}',NULL,NULL,1),('ConsumerBizListItem','1_Phillips','Phillips',1476771747142,'Phillips',NULL,0,'{\"CNAME\":\"830BE4979A39935A\",\"ID\":\"62889E73828C756C\",\"NAME\":\"830BE4979A39935A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pineda','Pineda',1476771747142,'Pineda',NULL,0,'{\"CNAME\":\"76E7A1292BF45F85\",\"ID\":\"3E7E0224018AB3CF\",\"NAME\":\"76E7A1292BF45F85\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pittman','Pittman',1476771747142,'Pittman',NULL,0,'{\"CNAME\":\"B3A3EEDEC6636DDE\",\"ID\":\"74563BA21A90DA13\",\"NAME\":\"B3A3EEDEC6636DDE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pitts','Pitts',1476771747142,'Pitts',NULL,0,'{\"CNAME\":\"AD755CB78D7EAAB2\",\"ID\":\"515AB26C135E92ED\",\"NAME\":\"AD755CB78D7EAAB2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pollard','Pollard',1476771747142,'Pollard',NULL,0,'{\"CNAME\":\"D4C5B2CFA4BDFB03\",\"ID\":\"7137DEBD45AE4D0A\",\"NAME\":\"D4C5B2CFA4BDFB03\"}',NULL,NULL,1),('ConsumerBizListItem','1_Poole','Poole',1476771747142,'Poole',NULL,0,'{\"CNAME\":\"45125D8C1C677C8D\",\"ID\":\"25E2A30F44898B9F\",\"NAME\":\"45125D8C1C677C8D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Potts','Potts',1476771747142,'Potts',NULL,0,'{\"CNAME\":\"C37309E1B9E42EF4\",\"ID\":\"851300EE84C2B80E\",\"NAME\":\"C37309E1B9E42EF4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Powell','Powell',1476771747142,'Powell',NULL,0,'{\"CNAME\":\"A13A75D965052E29\",\"ID\":\"B29EED44276144E4\",\"NAME\":\"A13A75D965052E29\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pratt','Pratt',1476771747142,'Pratt',NULL,0,'{\"CNAME\":\"C46833287C6F8550\",\"ID\":\"270EDD69788DCE20\",\"NAME\":\"C46833287C6F8550\"}',NULL,NULL,1),('ConsumerBizListItem','1_Preston','Preston',1476771747142,'Preston',NULL,0,'{\"CNAME\":\"5F7254408C59AAE2\",\"ID\":\"4FA53BE91B4933D5\",\"NAME\":\"5F7254408C59AAE2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pruitt','Pruitt',1476771747142,'Pruitt',NULL,0,'{\"CNAME\":\"1D92D15A8108EDC6\",\"ID\":\"AB2B41C63853F0A6\",\"NAME\":\"1D92D15A8108EDC6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Pugh','Pugh',1476771747142,'Pugh',NULL,0,'{\"CNAME\":\"316FBCF20085AE9B\",\"ID\":\"C4492CBE90FBDBF8\",\"NAME\":\"316FBCF20085AE9B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Quinn','Quinn',1476771747142,'Quinn',NULL,0,'{\"CNAME\":\"F9EF1E530C617CD5\",\"ID\":\"C0D0E461DE8D0024\",\"NAME\":\"F9EF1E530C617CD5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rachael','Rachael',1476771747142,'Rachael',NULL,0,'{\"CNAME\":\"3676EFB616C47897\",\"ID\":\"70ECE1E1E0931919\",\"NAME\":\"3676EFB616C47897\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rachel','Rachel',1476771747142,'Rachel',NULL,0,'{\"CNAME\":\"8E73B27568CB3BE2\",\"ID\":\"84C6494D30851C63\",\"NAME\":\"8E73B27568CB3BE2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ramirez','Ramirez',1476771747142,'Ramirez',NULL,0,'{\"CNAME\":\"9F5CBF232CD3B866\",\"ID\":\"752D25A1F8DBFB2D\",\"NAME\":\"9F5CBF232CD3B866\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ramos','Ramos',1476771747142,'Ramos',NULL,0,'{\"CNAME\":\"DB3B992995B36A9D\",\"ID\":\"54F5F4071FACA32A\",\"NAME\":\"DB3B992995B36A9D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ramsey','Ramsey',1476771747142,'Ramsey',NULL,0,'{\"CNAME\":\"A7AA509C34AAD08C\",\"ID\":\"B3B43AEEACB25836\",\"NAME\":\"A7AA509C34AAD08C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Randall','Randall',1476771747142,'Randall',NULL,0,'{\"CNAME\":\"C524EF418B241015\",\"ID\":\"6E62A992C676F611\",\"NAME\":\"C524EF418B241015\"}',NULL,NULL,1),('ConsumerBizListItem','1_Randolph','Randolph',1476771747142,'Randolph',NULL,0,'{\"CNAME\":\"2B22E2C346E0FDF6\",\"ID\":\"955A1584AF63A546\",\"NAME\":\"2B22E2C346E0FDF6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rangel','Rangel',1476771747142,'Rangel',NULL,0,'{\"CNAME\":\"A96AA2C4D2714321\",\"ID\":\"418EF6127E442148\",\"NAME\":\"A96AA2C4D2714321\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rasmussen','Rasmussen',1476771747142,'Rasmussen',NULL,0,'{\"CNAME\":\"A087CCF2ABBC1EC4\",\"ID\":\"97D98119037C5B8A\",\"NAME\":\"A087CCF2ABBC1EC4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Raymond','Raymond',1476771747142,'Raymond',NULL,0,'{\"CNAME\":\"F2A415AA78C76218\",\"ID\":\"442CDE81694CA09A\",\"NAME\":\"F2A415AA78C76218\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rebecca','Rebecca',1476771747142,'Rebecca',NULL,0,'{\"CNAME\":\"E358BF645A205CF1\",\"ID\":\"3E15CC11F979ED25\",\"NAME\":\"E358BF645A205CF1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Reese','Reese',1476771747142,'Reese',NULL,0,'{\"CNAME\":\"CE8FBF161182F814\",\"ID\":\"54E36C5FF5F6A180\",\"NAME\":\"CE8FBF161182F814\"}',NULL,NULL,1),('ConsumerBizListItem','1_Reeves','Reeves',1476771747142,'Reeves',NULL,0,'{\"CNAME\":\"2D2F533BEEC50B23\",\"ID\":\"FC9B003BB003A298\",\"NAME\":\"2D2F533BEEC50B23\"}',NULL,NULL,1),('ConsumerBizListItem','1_Reid','Reid',1476771747142,'Reid',NULL,0,'{\"CNAME\":\"75BCAD9151580B6C\",\"ID\":\"6A5DFAC4BE150250\",\"NAME\":\"75BCAD9151580B6C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Reilly','Reilly',1476771747142,'Reilly',NULL,0,'{\"CNAME\":\"6A1FCB99D07001FD\",\"ID\":\"B3BBCCD6C008E727\",\"NAME\":\"6A1FCB99D07001FD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Reyes','Reyes',1476771747142,'Reyes',NULL,0,'{\"CNAME\":\"958A74A4695EC722\",\"ID\":\"11C484EA9305EA4C\",\"NAME\":\"958A74A4695EC722\"}',NULL,NULL,1),('ConsumerBizListItem','1_Reynolds','Reynolds',1476771747142,'Reynolds',NULL,0,'{\"CNAME\":\"AFFEF6CFF763E2BF\",\"ID\":\"A63FC8C5D915E1F1\",\"NAME\":\"AFFEF6CFF763E2BF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rhodes','Rhodes',1476771747142,'Rhodes',NULL,0,'{\"CNAME\":\"A185111D5E99BD8F\",\"ID\":\"231141B34C82AA95\",\"NAME\":\"A185111D5E99BD8F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Richard','Richard',1476771747142,'Richard',NULL,0,'{\"CNAME\":\"6AE199A93C381BF6\",\"ID\":\"D8D31BD778DA8BDD\",\"NAME\":\"6AE199A93C381BF6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Richards','Richards',1476771747142,'Richards',NULL,0,'{\"CNAME\":\"BB80B7C78CBC7101\",\"ID\":\"CEFAB442B1728A7C\",\"NAME\":\"BB80B7C78CBC7101\"}',NULL,NULL,1),('ConsumerBizListItem','1_Richardson','Richardson',1476771747142,'Richardson',NULL,0,'{\"CNAME\":\"681A917B131A549\",\"ID\":\"D2CDF047A6674CEF\",\"NAME\":\"681A917B131A549\"}',NULL,NULL,1),('ConsumerBizListItem','1_Richmond','Richmond',1476771747142,'Richmond',NULL,0,'{\"CNAME\":\"AA4BBE632574E4A9\",\"ID\":\"2612AA892D962D6F\",\"NAME\":\"AA4BBE632574E4A9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Riggs','Riggs',1476771747142,'Riggs',NULL,0,'{\"CNAME\":\"AD1A9CC5CB2B257F\",\"ID\":\"BA1B3EBA322EAB5D\",\"NAME\":\"AD1A9CC5CB2B257F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rios','Rios',1476771747142,'Rios',NULL,0,'{\"CNAME\":\"574DBED3EAC44869\",\"ID\":\"E22DD5DABDE45EDA\",\"NAME\":\"574DBED3EAC44869\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ritter','Ritter',1476771747142,'Ritter',NULL,0,'{\"CNAME\":\"C80AC5B06A8BE775\",\"ID\":\"7283518D47A05A09\",\"NAME\":\"C80AC5B06A8BE775\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rivas','Rivas',1476771747142,'Rivas',NULL,0,'{\"CNAME\":\"C7EA29DDF4466F98\",\"ID\":\"17ED8ABEDC255908\",\"NAME\":\"C7EA29DDF4466F98\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rivera','Rivera',1476771747142,'Rivera',NULL,0,'{\"CNAME\":\"96A6E41DE7919DBF\",\"ID\":\"DF9028FCB6B065E0\",\"NAME\":\"96A6E41DE7919DBF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Robbins','Robbins',1476771747142,'Robbins',NULL,0,'{\"CNAME\":\"2DF48FA4BAED8D9F\",\"ID\":\"94E4451AD2390902\",\"NAME\":\"2DF48FA4BAED8D9F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Roberson','Roberson',1476771747142,'Roberson',NULL,0,'{\"CNAME\":\"E4E6E8A86231D6C9\",\"ID\":\"8698FF92115213AB\",\"NAME\":\"E4E6E8A86231D6C9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Robert','Robert',1476771747142,'Robert',NULL,0,'{\"CNAME\":\"684C851AF59965B6\",\"ID\":\"96A93BA89A5B5C6C\",\"NAME\":\"684C851AF59965B6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Roberts','Roberts',1476771747142,'Roberts',NULL,0,'{\"CNAME\":\"E95DAFE7231707EB\",\"ID\":\"E8FD4A8A5BAB2B37\",\"NAME\":\"E95DAFE7231707EB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Robertson','Robertson',1476771747142,'Robertson',NULL,0,'{\"CNAME\":\"7E7DCB823CB8BAC7\",\"ID\":\"DB29450C3F5E97F9\",\"NAME\":\"7E7DCB823CB8BAC7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Robinson','Robinson',1476771747142,'Robinson',NULL,0,'{\"CNAME\":\"AF7A158CB1B057F5\",\"ID\":\"7EB7EABBE9BD03C2\",\"NAME\":\"AF7A158CB1B057F5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rocha','Rocha',1476771747142,'Rocha',NULL,0,'{\"CNAME\":\"5C3BC57FBC5B323C\",\"ID\":\"E5A4D6BF330F23A8\",\"NAME\":\"5C3BC57FBC5B323C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rodgers','Rodgers',1476771747142,'Rodgers',NULL,0,'{\"CNAME\":\"FC09AAF0FE75AF0E\",\"ID\":\"21BE9A4BD4F81549\",\"NAME\":\"FC09AAF0FE75AF0E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rodriguez','Rodriguez',1476771747142,'Rodriguez',NULL,0,'{\"CNAME\":\"653C92B485A7374D\",\"ID\":\"299570476C6F0309\",\"NAME\":\"653C92B485A7374D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rogers','Rogers',1476771747142,'Rogers',NULL,0,'{\"CNAME\":\"12D7CA0CE1D7ACC\",\"ID\":\"4ABE17A1C80CBDD2\",\"NAME\":\"12D7CA0CE1D7ACC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rojas','Rojas',1476771747142,'Rojas',NULL,0,'{\"CNAME\":\"F58FD4C84C06D78\",\"ID\":\"F0BDA020D2470F2E\",\"NAME\":\"F58FD4C84C06D78\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rollins','Rollins',1476771747142,'Rollins',NULL,0,'{\"CNAME\":\"DAE78F049502EBE4\",\"ID\":\"36D7534290610D9B\",\"NAME\":\"DAE78F049502EBE4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Romero','Romero',1476771747142,'Romero',NULL,0,'{\"CNAME\":\"15FA672549A3C6BB\",\"ID\":\"7DD0240CD412EFDE\",\"NAME\":\"15FA672549A3C6BB\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ronald','Ronald',1476771747142,'Ronald',NULL,0,'{\"CNAME\":\"5AF0A0FEB2094F43\",\"ID\":\"E9B73BCCD1762555\",\"NAME\":\"5AF0A0FEB2094F43\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rosales','Rosales',1476771747142,'Rosales',NULL,0,'{\"CNAME\":\"96C4CBFBD9B13425\",\"ID\":\"6709E8D64A5F4726\",\"NAME\":\"96C4CBFBD9B13425\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rosario','Rosario',1476771747142,'Rosario',NULL,0,'{\"CNAME\":\"865CC410A1B7C60A\",\"ID\":\"8C01A75941549A70\",\"NAME\":\"865CC410A1B7C60A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ross','Ross',1476771747142,'Ross',NULL,0,'{\"CNAME\":\"EDEEE8F93FDED5D7\",\"ID\":\"F63F65B503E22CB9\",\"NAME\":\"EDEEE8F93FDED5D7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Roth','Roth',1476771747142,'Roth',NULL,0,'{\"CNAME\":\"D653C1F1541D5393\",\"ID\":\"8B6A80C3CF2CBD5F\",\"NAME\":\"D653C1F1541D5393\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rowe','Rowe',1476771747142,'Rowe',NULL,0,'{\"CNAME\":\"B2D443798C352985\",\"ID\":\"4A2DDF148C5A9C42\",\"NAME\":\"B2D443798C352985\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rowland','Rowland',1476771747142,'Rowland',NULL,0,'{\"CNAME\":\"5474E5A97BC5A205\",\"ID\":\"B865367FC4C0845C\",\"NAME\":\"5474E5A97BC5A205\"}',NULL,NULL,1),('ConsumerBizListItem','1_Rubio','Rubio',1476771747142,'Rubio',NULL,0,'{\"CNAME\":\"908563CABD95885F\",\"ID\":\"365D17770080C807\",\"NAME\":\"908563CABD95885F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ruiz','Ruiz',1476771747142,'Ruiz',NULL,0,'{\"CNAME\":\"9A2A3BA925FDACE5\",\"ID\":\"393C55AEA738548D\",\"NAME\":\"9A2A3BA925FDACE5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Russell','Russell',1476771747142,'Russell',NULL,0,'{\"CNAME\":\"1E9C32B7BA6FEFA7\",\"ID\":\"6449F44A102FDE84\",\"NAME\":\"1E9C32B7BA6FEFA7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Russo','Russo',1476771747142,'Russo',NULL,0,'{\"CNAME\":\"DF4414705CAA4690\",\"ID\":\"D860BD12CE9C0268\",\"NAME\":\"DF4414705CAA4690\"}',NULL,NULL,1),('ConsumerBizListItem','1_Ryan','Ryan',1476771747142,'Ryan',NULL,0,'{\"CNAME\":\"10C7CCC7A4F0AFF0\",\"ID\":\"DB957C626A8CD7A2\",\"NAME\":\"10C7CCC7A4F0AFF0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Salas','Salas',1476771747142,'Salas',NULL,0,'{\"CNAME\":\"FC6067E380942CF2\",\"ID\":\"ECD62DE20EA67E1C\",\"NAME\":\"FC6067E380942CF2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Salazar','Salazar',1476771747142,'Salazar',NULL,0,'{\"CNAME\":\"31926F882293943B\",\"ID\":\"D25414405EB37DAE\",\"NAME\":\"31926F882293943B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Salinas','Salinas',1476771747142,'Salinas',NULL,0,'{\"CNAME\":\"EFE4C534F51BFF41\",\"ID\":\"BF201D5407A6509F\",\"NAME\":\"EFE4C534F51BFF41\"}',NULL,NULL,1),('ConsumerBizListItem','1_Samantha','Samantha',1476771747142,'Samantha',NULL,0,'{\"CNAME\":\"F01E0D7992A3B774\",\"ID\":\"52335232B118649\",\"NAME\":\"F01E0D7992A3B774\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sampson','Sampson',1476771747142,'Sampson',NULL,0,'{\"CNAME\":\"B68A89A9D6A1A538\",\"ID\":\"C8CBD669CFB2F016\",\"NAME\":\"B68A89A9D6A1A538\"}',NULL,NULL,1),('ConsumerBizListItem','1_Samuel','Samuel',1476771747142,'Samuel',NULL,0,'{\"CNAME\":\"D8AE5776067290C4\",\"ID\":\"59E0B2658E9F2E77\",\"NAME\":\"D8AE5776067290C4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sanchez','Sanchez',1476771747142,'Sanchez',NULL,0,'{\"CNAME\":\"B4A7DB11AAAB6D9\",\"ID\":\"731460A8A5CE162\",\"NAME\":\"B4A7DB11AAAB6D9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sandoval','Sandoval',1476771747142,'Sandoval',NULL,0,'{\"CNAME\":\"64D2AE6E32D2DE0B\",\"ID\":\"90599C8FDD2F6E7A\",\"NAME\":\"64D2AE6E32D2DE0B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sanford','Sanford',1476771747142,'Sanford',NULL,0,'{\"CNAME\":\"C9CF2113AF437BE7\",\"ID\":\"8D9FC2308C8F28D2\",\"NAME\":\"C9CF2113AF437BE7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Santana','Santana',1476771747142,'Santana',NULL,0,'{\"CNAME\":\"EF4355061C51F7F\",\"ID\":\"AC5DAB2E99EEE9CF\",\"NAME\":\"EF4355061C51F7F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Santiago','Santiago',1476771747142,'Santiago',NULL,0,'{\"CNAME\":\"A6F30815A43F38EC\",\"ID\":\"533A888904BD486\",\"NAME\":\"A6F30815A43F38EC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Santos','Santos',1476771747142,'Santos',NULL,0,'{\"CNAME\":\"114FDFEFD3D69799\",\"ID\":\"F39AE9FF3A81F499\",\"NAME\":\"114FDFEFD3D69799\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sara','Sara',1476771747142,'Sara',NULL,0,'{\"CNAME\":\"5BD537FC3789B548\",\"ID\":\"CD14821DAB219EA0\",\"NAME\":\"5BD537FC3789B548\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sarah','Sarah',1476771747142,'Sarah',NULL,0,'{\"CNAME\":\"9E9D7A08E048E9D6\",\"ID\":\"DC40B7120E77741D\",\"NAME\":\"9E9D7A08E048E9D6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Saunders','Saunders',1476771747142,'Saunders',NULL,0,'{\"CNAME\":\"D8323A1F539D0044\",\"ID\":\"3FAB5890D8113D0B\",\"NAME\":\"D8323A1F539D0044\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schaefer','Schaefer',1476771747142,'Schaefer',NULL,0,'{\"CNAME\":\"D28E60078EF7975F\",\"ID\":\"90E1357833654983\",\"NAME\":\"D28E60078EF7975F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schmidt','Schmidt',1476771747142,'Schmidt',NULL,0,'{\"CNAME\":\"1AE6A00FFF3841C0\",\"ID\":\"7FFD85D93A3E4DE5\",\"NAME\":\"1AE6A00FFF3841C0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schmitt','Schmitt',1476771747142,'Schmitt',NULL,0,'{\"CNAME\":\"45D77C6D437298AF\",\"ID\":\"D1E946F4E67DB4B3\",\"NAME\":\"45D77C6D437298AF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schneider','Schneider',1476771747142,'Schneider',NULL,0,'{\"CNAME\":\"BD7CDD31BA07B93B\",\"ID\":\"1F1BAA5B8EDAC74E\",\"NAME\":\"BD7CDD31BA07B93B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schroeder','Schroeder',1476771747142,'Schroeder',NULL,0,'{\"CNAME\":\"B9A96F58B15E81EF\",\"ID\":\"39027DFAD5138C9C\",\"NAME\":\"B9A96F58B15E81EF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schultz','Schultz',1476771747142,'Schultz',NULL,0,'{\"CNAME\":\"9CC49FDC6EDA9D82\",\"ID\":\"645098B086D2F9E1\",\"NAME\":\"9CC49FDC6EDA9D82\"}',NULL,NULL,1),('ConsumerBizListItem','1_Schwartz','Schwartz',1476771747142,'Schwartz',NULL,0,'{\"CNAME\":\"ECAE93167D813627\",\"ID\":\"6E79ED05BAEC2754\",\"NAME\":\"ECAE93167D813627\"}',NULL,NULL,1),('ConsumerBizListItem','1_Scott','Scott',1476771747142,'Scott',NULL,0,'{\"CNAME\":\"21F63C6E971CD913\",\"ID\":\"9B15D48A1514D82\",\"NAME\":\"21F63C6E971CD913\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sean','Sean',1476771747142,'Sean',NULL,0,'{\"CNAME\":\"9B938710211168F2\",\"ID\":\"5B6BA13F79129A74\",\"NAME\":\"9B938710211168F2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sellers','Sellers',1476771747142,'Sellers',NULL,0,'{\"CNAME\":\"D396CEBB1F0E0C0F\",\"ID\":\"F542EAE1949358E2\",\"NAME\":\"D396CEBB1F0E0C0F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Seth','Seth',1476771747142,'Seth',NULL,0,'{\"CNAME\":\"480A0EFD668E5685\",\"ID\":\"155FA09596C7E18E\",\"NAME\":\"480A0EFD668E5685\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sexton','Sexton',1476771747142,'Sexton',NULL,0,'{\"CNAME\":\"65DF6602604F27B3\",\"ID\":\"F0BBAC6FA079F1E0\",\"NAME\":\"65DF6602604F27B3\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shaffer','Shaffer',1476771747142,'Shaffer',NULL,0,'{\"CNAME\":\"6317B8E2866D276A\",\"ID\":\"FAACBCD5BF1D0189\",\"NAME\":\"6317B8E2866D276A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shah','Shah',1476771747142,'Shah',NULL,0,'{\"CNAME\":\"1630937C3D00B4F4\",\"ID\":\"F4A4DA9AA7EADFD2\",\"NAME\":\"1630937C3D00B4F4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shane','Shane',1476771747142,'Shane',NULL,0,'{\"CNAME\":\"7790FFBB26CF6409\",\"ID\":\"F5C3DD7514BF620A\",\"NAME\":\"7790FFBB26CF6409\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shannon','Shannon',1476771747142,'Shannon',NULL,0,'{\"CNAME\":\"9D2F75377AC0AB99\",\"ID\":\"17C3433FECC21B57\",\"NAME\":\"9D2F75377AC0AB99\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shaw','Shaw',1476771747142,'Shaw',NULL,0,'{\"CNAME\":\"A84DE33B6CB560A2\",\"ID\":\"F2D887E01A80E813\",\"NAME\":\"A84DE33B6CB560A2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shawn','Shawn',1476771747142,'Shawn',NULL,0,'{\"CNAME\":\"A3CCEBA83235DC95\",\"ID\":\"596DEDF4498E258E\",\"NAME\":\"A3CCEBA83235DC95\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shea','Shea',1476771747142,'Shea',NULL,0,'{\"CNAME\":\"87F9BD6508E7F875\",\"ID\":\"E087EC55DCBE7B2\",\"NAME\":\"87F9BD6508E7F875\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shelton','Shelton',1476771747142,'Shelton',NULL,0,'{\"CNAME\":\"741E2C301C083869\",\"ID\":\"14CFDB59B5BDA1FC\",\"NAME\":\"741E2C301C083869\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shepard','Shepard',1476771747142,'Shepard',NULL,0,'{\"CNAME\":\"E2EA187427A6E481\",\"ID\":\"2647C1DBA23BC0E0\",\"NAME\":\"E2EA187427A6E481\"}',NULL,NULL,1),('ConsumerBizListItem','1_Shepherd','Shepherd',1476771747142,'Shepherd',NULL,0,'{\"CNAME\":\"EE03B2BC52780914\",\"ID\":\"ED519DACC89B2BEA\",\"NAME\":\"EE03B2BC52780914\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sheppard','Sheppard',1476771747142,'Sheppard',NULL,0,'{\"CNAME\":\"CC0ED618901F68F4\",\"ID\":\"F442D33FA0683208\",\"NAME\":\"CC0ED618901F68F4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sherman','Sherman',1476771747142,'Sherman',NULL,0,'{\"CNAME\":\"C221B4A8B9B1B071\",\"ID\":\"FFACE8385ABBF94B\",\"NAME\":\"C221B4A8B9B1B071\"}',NULL,NULL,1),('ConsumerBizListItem','1_Silva','Silva',1476771747142,'Silva',NULL,0,'{\"CNAME\":\"5ECC4AA302D527A6\",\"ID\":\"4496BF24AFE7FAB6\",\"NAME\":\"5ECC4AA302D527A6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Simmons','Simmons',1476771747142,'Simmons',NULL,0,'{\"CNAME\":\"F74D6A154D7F1B1A\",\"ID\":\"AA2A77371374094F\",\"NAME\":\"F74D6A154D7F1B1A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Simon','Simon',1476771747142,'Simon',NULL,0,'{\"CNAME\":\"B30BD351371C6862\",\"ID\":\"69654D5CE089C13\",\"NAME\":\"B30BD351371C6862\"}',NULL,NULL,1),('ConsumerBizListItem','1_Simpson','Simpson',1476771747142,'Simpson',NULL,0,'{\"CNAME\":\"C81D76D6C160DB37\",\"ID\":\"E0AB531EC3121615\",\"NAME\":\"C81D76D6C160DB37\"}',NULL,NULL,1),('ConsumerBizListItem','1_Singh','Singh',1476771747142,'Singh',NULL,0,'{\"CNAME\":\"2865A5B14E5A7027\",\"ID\":\"61B1FB3F59E28C67\",\"NAME\":\"2865A5B14E5A7027\"}',NULL,NULL,1),('ConsumerBizListItem','1_Skinner','Skinner',1476771747142,'Skinner',NULL,0,'{\"CNAME\":\"F622889F42BAAFF1\",\"ID\":\"9A1DE01F893E0D25\",\"NAME\":\"F622889F42BAAFF1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sloan','Sloan',1476771747142,'Sloan',NULL,0,'{\"CNAME\":\"CDCA073392FAB3DC\",\"ID\":\"D757719ED7C2B66D\",\"NAME\":\"CDCA073392FAB3DC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Snyder','Snyder',1476771747142,'Snyder',NULL,0,'{\"CNAME\":\"36D7785547BC6F85\",\"ID\":\"DFCE06801E1A85D6\",\"NAME\":\"36D7785547BC6F85\"}',NULL,NULL,1),('ConsumerBizListItem','1_Solis','Solis',1476771747142,'Solis',NULL,0,'{\"CNAME\":\"81D7143BBFD2ACE6\",\"ID\":\"F26DAB9BF6A137C3\",\"NAME\":\"81D7143BBFD2ACE6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Solomon','Solomon',1476771747142,'Solomon',NULL,0,'{\"CNAME\":\"BBDD0E294FD18366\",\"ID\":\"85F007F8C50DD25F\",\"NAME\":\"BBDD0E294FD18366\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sosa','Sosa',1476771747142,'Sosa',NULL,0,'{\"CNAME\":\"6648EDE707275819\",\"ID\":\"D7657583058394C8\",\"NAME\":\"6648EDE707275819\"}',NULL,NULL,1),('ConsumerBizListItem','1_Soto','Soto',1476771747142,'Soto',NULL,0,'{\"CNAME\":\"9ABA14B14C1DDB1\",\"ID\":\"728F206C2A01BF57\",\"NAME\":\"9ABA14B14C1DDB1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Spence','Spence',1476771747142,'Spence',NULL,0,'{\"CNAME\":\"7A28BD6B9611C5E9\",\"ID\":\"201D7288B4C18A67\",\"NAME\":\"7A28BD6B9611C5E9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stacey','Stacey',1476771747142,'Stacey',NULL,0,'{\"CNAME\":\"B9066808309E7B22\",\"ID\":\"E8DFFF4676A47048\",\"NAME\":\"B9066808309E7B22\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stacy','Stacy',1476771747142,'Stacy',NULL,0,'{\"CNAME\":\"ECD3D37EA8BDBAB7\",\"ID\":\"57C0531E13F40B91\",\"NAME\":\"ECD3D37EA8BDBAB7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stafford','Stafford',1476771747142,'Stafford',NULL,0,'{\"CNAME\":\"C84789F69A50EAA1\",\"ID\":\"4888241374E8C62D\",\"NAME\":\"C84789F69A50EAA1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stanley','Stanley',1476771747142,'Stanley',NULL,0,'{\"CNAME\":\"C7656CE3FBB462C8\",\"ID\":\"6A15EB1C3836723\",\"NAME\":\"C7656CE3FBB462C8\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stanton','Stanton',1476771747142,'Stanton',NULL,0,'{\"CNAME\":\"772A620A2F94D86C\",\"ID\":\"59BCDA7C438BAD7D\",\"NAME\":\"772A620A2F94D86C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Steele','Steele',1476771747142,'Steele',NULL,0,'{\"CNAME\":\"78B6802EED9C7A03\",\"ID\":\"F45A1078FEB35DE7\",\"NAME\":\"78B6802EED9C7A03\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stein','Stein',1476771747142,'Stein',NULL,0,'{\"CNAME\":\"F569AB23D780A45B\",\"ID\":\"82CADB0649A3AF49\",\"NAME\":\"F569AB23D780A45B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stephanie','Stephanie',1476771747142,'Stephanie',NULL,0,'{\"CNAME\":\"59F29878F0CA6FFF\",\"ID\":\"7385DB9A3F11415B\",\"NAME\":\"59F29878F0CA6FFF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stephen','Stephen',1476771747142,'Stephen',NULL,0,'{\"CNAME\":\"7FF36797539130F7\",\"ID\":\"CD63A3EEC3319FD9\",\"NAME\":\"7FF36797539130F7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stephens','Stephens',1476771747142,'Stephens',NULL,0,'{\"CNAME\":\"2CE1008896DB6313\",\"ID\":\"8C3039BD5842DCA3\",\"NAME\":\"2CE1008896DB6313\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stephenson','Stephenson',1476771747142,'Stephenson',NULL,0,'{\"CNAME\":\"BAC40A9809C6EC0\",\"ID\":\"FF1418E8CC993FE8\",\"NAME\":\"BAC40A9809C6EC0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Steven','Steven',1476771747142,'Steven',NULL,0,'{\"CNAME\":\"6ED61D4B80BB0F81\",\"ID\":\"EB1E78328C46506B\",\"NAME\":\"6ED61D4B80BB0F81\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stevens','Stevens',1476771747142,'Stevens',NULL,0,'{\"CNAME\":\"4945AA82E88DC621\",\"ID\":\"7503CFACD12053D3\",\"NAME\":\"4945AA82E88DC621\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stevenson','Stevenson',1476771747142,'Stevenson',NULL,0,'{\"CNAME\":\"556A3546FD13A354\",\"ID\":\"49AD23D1EC9FA4BD\",\"NAME\":\"556A3546FD13A354\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stewart','Stewart',1476771747142,'Stewart',NULL,0,'{\"CNAME\":\"AE463243B033F797\",\"ID\":\"6AE07DCB33EC3B7C\",\"NAME\":\"AE463243B033F797\"}',NULL,NULL,1),('ConsumerBizListItem','1_Strickland','Strickland',1476771747142,'Strickland',NULL,0,'{\"CNAME\":\"BD06B2275315D49B\",\"ID\":\"3C947BC2F7FF007B\",\"NAME\":\"BD06B2275315D49B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Stuart','Stuart',1476771747142,'Stuart',NULL,0,'{\"CNAME\":\"407CB3436F86ACC2\",\"ID\":\"A3545BD79D31F9A7\",\"NAME\":\"407CB3436F86ACC2\"}',NULL,NULL,1),('ConsumerBizListItem','1_Suarez','Suarez',1476771747142,'Suarez',NULL,0,'{\"CNAME\":\"9158734E4B7A59BF\",\"ID\":\"D7FD118E6F226A71\",\"NAME\":\"9158734E4B7A59BF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sullivan','Sullivan',1476771747142,'Sullivan',NULL,0,'{\"CNAME\":\"A0411CDC8FA047D7\",\"ID\":\"537DE305E941FCCD\",\"NAME\":\"A0411CDC8FA047D7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Susan','Susan',1476771747142,'Susan',NULL,0,'{\"CNAME\":\"AC575E3EECF0FA41\",\"ID\":\"96C5C28BECF18E71\",\"NAME\":\"AC575E3EECF0FA41\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sutton','Sutton',1476771747142,'Sutton',NULL,0,'{\"CNAME\":\"C30223A4F74281CD\",\"ID\":\"F9D1152547C0BDE0\",\"NAME\":\"C30223A4F74281CD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Swanson','Swanson',1476771747142,'Swanson',NULL,0,'{\"CNAME\":\"573152CC51DE19DF\",\"ID\":\"4E87337F366F72DA\",\"NAME\":\"573152CC51DE19DF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Sweeney','Sweeney',1476771747142,'Sweeney',NULL,0,'{\"CNAME\":\"6CC17653018F36B7\",\"ID\":\"9FB05DD477D4AE6\",\"NAME\":\"6CC17653018F36B7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tabitha','Tabitha',1476771747142,'Tabitha',NULL,0,'{\"CNAME\":\"395B069478996236\",\"ID\":\"D072677D210AC4C0\",\"NAME\":\"395B069478996236\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tapia','Tapia',1476771747142,'Tapia',NULL,0,'{\"CNAME\":\"D4A5208B73A10817\",\"ID\":\"69D1FC78DBDA242C\",\"NAME\":\"D4A5208B73A10817\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tara','Tara',1476771747142,'Tara',NULL,0,'{\"CNAME\":\"2C842D72963554E4\",\"ID\":\"19DE10ADBAA1B2EE\",\"NAME\":\"2C842D72963554E4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tate','Tate',1476771747142,'Tate',NULL,0,'{\"CNAME\":\"C8C01893D9BC804C\",\"ID\":\"A19ACD7D2689207F\",\"NAME\":\"C8C01893D9BC804C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Taylor','Taylor',1476771747142,'Taylor',NULL,0,'{\"CNAME\":\"7D8BC5F1A8D3787D\",\"ID\":\"C164BBC9D6C72A52\",\"NAME\":\"7D8BC5F1A8D3787D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Terrell','Terrell',1476771747142,'Terrell',NULL,0,'{\"CNAME\":\"3150184A9614F8B9\",\"ID\":\"68C694DE94E6C110\",\"NAME\":\"3150184A9614F8B9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Thomas','Thomas',1476771747142,'Thomas',NULL,0,'{\"CNAME\":\"EF6E65EFC188E7DF\",\"ID\":\"D305281FAF947CA7\",\"NAME\":\"EF6E65EFC188E7DF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Thompson','Thompson',1476771747142,'Thompson',NULL,0,'{\"CNAME\":\"3A7E1B81CA8FBC0E\",\"ID\":\"FB8FEFF253BB6C83\",\"NAME\":\"3A7E1B81CA8FBC0E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Thornton','Thornton',1476771747142,'Thornton',NULL,0,'{\"CNAME\":\"DD58D6F9AAFD5DFF\",\"ID\":\"2F25F6E326ADB93C\",\"NAME\":\"DD58D6F9AAFD5DFF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Timothy','Timothy',1476771747142,'Timothy',NULL,0,'{\"CNAME\":\"ECB97D53D2D35B8B\",\"ID\":\"D4B2AEB2453BDADA\",\"NAME\":\"ECB97D53D2D35B8B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Todd','Todd',1476771747142,'Todd',NULL,0,'{\"CNAME\":\"C60BA607DAFB7A71\",\"ID\":\"958ADB57686C2FDE\",\"NAME\":\"C60BA607DAFB7A71\"}',NULL,NULL,1),('ConsumerBizListItem','1_Torres','Torres',1476771747142,'Torres',NULL,0,'{\"CNAME\":\"B50AC41EC20631C7\",\"ID\":\"7E9E346DC5FD268B\",\"NAME\":\"B50AC41EC20631C7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Townsend','Townsend',1476771747142,'Townsend',NULL,0,'{\"CNAME\":\"F0E963829CB013E9\",\"ID\":\"BA9A56CE0A9BFA26\",\"NAME\":\"F0E963829CB013E9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tran','Tran',1476771747142,'Tran',NULL,0,'{\"CNAME\":\"31509AFAEFD35FF7\",\"ID\":\"F31B20466AE89669\",\"NAME\":\"31509AFAEFD35FF7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Travis','Travis',1476771747142,'Travis',NULL,0,'{\"CNAME\":\"7985139AE9B6EFB4\",\"ID\":\"DC5D637ED5E62C36\",\"NAME\":\"7985139AE9B6EFB4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Trevino','Trevino',1476771747142,'Trevino',NULL,0,'{\"CNAME\":\"93FC2E1A97EF7A04\",\"ID\":\"44A2E0804995FAF8\",\"NAME\":\"93FC2E1A97EF7A04\"}',NULL,NULL,1),('ConsumerBizListItem','1_Trujillo','Trujillo',1476771747142,'Trujillo',NULL,0,'{\"CNAME\":\"F6E014747BA5B09B\",\"ID\":\"3214A6D842CC6959\",\"NAME\":\"F6E014747BA5B09B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tucker','Tucker',1476771747142,'Tucker',NULL,0,'{\"CNAME\":\"6243E1116E9DE008\",\"ID\":\"B4568DF26077653E\",\"NAME\":\"6243E1116E9DE008\"}',NULL,NULL,1),('ConsumerBizListItem','1_Tyler','Tyler',1476771747142,'Tyler',NULL,0,'{\"CNAME\":\"CCB4A9130F39CC55\",\"ID\":\"E1314FC026DA60D8\",\"NAME\":\"CCB4A9130F39CC55\"}',NULL,NULL,1),('ConsumerBizListItem','1_Valdez','Valdez',1476771747142,'Valdez',NULL,0,'{\"CNAME\":\"50E80C6AF2174520\",\"ID\":\"C45008212F7BDF6E\",\"NAME\":\"50E80C6AF2174520\"}',NULL,NULL,1),('ConsumerBizListItem','1_Valencia','Valencia',1476771747142,'Valencia',NULL,0,'{\"CNAME\":\"E45C3160715AA0CD\",\"ID\":\"F0FCF351DF4EB678\",\"NAME\":\"E45C3160715AA0CD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Valenzuela','Valenzuela',1476771747142,'Valenzuela',NULL,0,'{\"CNAME\":\"52F330F3389BBC44\",\"ID\":\"D523773C6B194F37\",\"NAME\":\"52F330F3389BBC44\"}',NULL,NULL,1),('ConsumerBizListItem','1_Valerie','Valerie',1476771747142,'Valerie',NULL,0,'{\"CNAME\":\"6818BAB4DA85A3A1\",\"ID\":\"C366C2C97D47B02B\",\"NAME\":\"6818BAB4DA85A3A1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vance','Vance',1476771747142,'Vance',NULL,0,'{\"CNAME\":\"E3D8D445E600ADD9\",\"ID\":\"7302E3F5E7C072AE\",\"NAME\":\"E3D8D445E600ADD9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vanessa','Vanessa',1476771747142,'Vanessa',NULL,0,'{\"CNAME\":\"282BBBFB69DA08D0\",\"ID\":\"2D1B2A5FF364606F\",\"NAME\":\"282BBBFB69DA08D0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vang','Vang',1476771747142,'Vang',NULL,0,'{\"CNAME\":\"E2DC344E217C6D8D\",\"ID\":\"68148596109E38CF\",\"NAME\":\"E2DC344E217C6D8D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vargas','Vargas',1476771747142,'Vargas',NULL,0,'{\"CNAME\":\"D49FAB26AC2DFC19\",\"ID\":\"865DFBDE8A344B44\",\"NAME\":\"D49FAB26AC2DFC19\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vasquez','Vasquez',1476771747142,'Vasquez',NULL,0,'{\"CNAME\":\"B24BBBF00B2B4FC4\",\"ID\":\"13168E6A2E6C84B4\",\"NAME\":\"B24BBBF00B2B4FC4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vaughan','Vaughan',1476771747142,'Vaughan',NULL,0,'{\"CNAME\":\"1720031491AD2F63\",\"ID\":\"CA460332316D6DA8\",\"NAME\":\"1720031491AD2F63\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vaughn','Vaughn',1476771747142,'Vaughn',NULL,0,'{\"CNAME\":\"57D413BCA681029A\",\"ID\":\"56F9F88906AEBF4A\",\"NAME\":\"57D413BCA681029A\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vazquez','Vazquez',1476771747142,'Vazquez',NULL,0,'{\"CNAME\":\"FCCFC3769CB2CAA6\",\"ID\":\"5FA9E41BFEC07257\",\"NAME\":\"FCCFC3769CB2CAA6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vega','Vega',1476771747142,'Vega',NULL,0,'{\"CNAME\":\"8253DEE45BD36758\",\"ID\":\"59FDCD96BAEB751\",\"NAME\":\"8253DEE45BD36758\"}',NULL,NULL,1),('ConsumerBizListItem','1_Velasquez','Velasquez',1476771747142,'Velasquez',NULL,0,'{\"CNAME\":\"FC61EB2B83DE90EC\",\"ID\":\"E06F967FB0D35559\",\"NAME\":\"FC61EB2B83DE90EC\"}',NULL,NULL,1),('ConsumerBizListItem','1_Velazquez','Velazquez',1476771747142,'Velazquez',NULL,0,'{\"CNAME\":\"5D00430818F45F69\",\"ID\":\"AF3303F852ABECCD\",\"NAME\":\"5D00430818F45F69\"}',NULL,NULL,1),('ConsumerBizListItem','1_Velez','Velez',1476771747142,'Velez',NULL,0,'{\"CNAME\":\"FBFFF451C6D4D407\",\"ID\":\"11F524C3FBFEECA4\",\"NAME\":\"FBFFF451C6D4D407\"}',NULL,NULL,1),('ConsumerBizListItem','1_Victoria','Victoria',1476771747142,'Victoria',NULL,0,'{\"CNAME\":\"E0E34C5AD05AAC3E\",\"ID\":\"6BE5336DB2C11973\",\"NAME\":\"E0E34C5AD05AAC3E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Villanueva','Villanueva',1476771747142,'Villanueva',NULL,0,'{\"CNAME\":\"30D956A15329A41D\",\"ID\":\"9FDB62F932ADF55A\",\"NAME\":\"30D956A15329A41D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Villarreal','Villarreal',1476771747142,'Villarreal',NULL,0,'{\"CNAME\":\"7E22E34A15506958\",\"ID\":\"D54E99A6C03704E9\",\"NAME\":\"7E22E34A15506958\"}',NULL,NULL,1),('ConsumerBizListItem','1_Villegas','Villegas',1476771747142,'Villegas',NULL,0,'{\"CNAME\":\"7D16FF50603C8C7\",\"ID\":\"FC4DDC15F9F4B4B0\",\"NAME\":\"7D16FF50603C8C7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Vincent','Vincent',1476771747142,'Vincent',NULL,0,'{\"CNAME\":\"B15AB3F829F0F897\",\"ID\":\"944626ADF9E3B76A\",\"NAME\":\"B15AB3F829F0F897\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wagner','Wagner',1476771747142,'Wagner',NULL,0,'{\"CNAME\":\"23BA6002AA3583A6\",\"ID\":\"C91591A8D461C286\",\"NAME\":\"23BA6002AA3583A6\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wallace','Wallace',1476771747142,'Wallace',NULL,0,'{\"CNAME\":\"53A82ED2B8E34E62\",\"ID\":\"73E0F7487B8E5297\",\"NAME\":\"53A82ED2B8E34E62\"}',NULL,NULL,1),('ConsumerBizListItem','1_Waller','Waller',1476771747142,'Waller',NULL,0,'{\"CNAME\":\"2C7FE2CA37BC719F\",\"ID\":\"DEA9DDB25CBF2352\",\"NAME\":\"2C7FE2CA37BC719F\"}',NULL,NULL,1),('ConsumerBizListItem','1_Walsh','Walsh',1476771747142,'Walsh',NULL,0,'{\"CNAME\":\"6CE24873A2BD26BA\",\"ID\":\"77369E37B2AA1404\",\"NAME\":\"6CE24873A2BD26BA\"}',NULL,NULL,1),('ConsumerBizListItem','1_Walter','Walter',1476771747142,'Walter',NULL,0,'{\"CNAME\":\"841D93525B9F0960\",\"ID\":\"65699726A3C601B9\",\"NAME\":\"841D93525B9F0960\"}',NULL,NULL,1),('ConsumerBizListItem','1_Walters','Walters',1476771747142,'Walters',NULL,0,'{\"CNAME\":\"AA5231A7B2BC9BD7\",\"ID\":\"609154FA35B3194\",\"NAME\":\"AA5231A7B2BC9BD7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Walton','Walton',1476771747142,'Walton',NULL,0,'{\"CNAME\":\"FD1E821721FFA7D4\",\"ID\":\"AB7314887865C426\",\"NAME\":\"FD1E821721FFA7D4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wang','Wang',1476771747142,'Wang',NULL,0,'{\"CNAME\":\"E08392BB89DEDB8E\",\"ID\":\"4DF4D434D481C5B\",\"NAME\":\"E08392BB89DEDB8E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Washington','Washington',1476771747142,'Washington',NULL,0,'{\"CNAME\":\"ED2B406EFC231BE\",\"ID\":\"FF7D0F525B3BE596\",\"NAME\":\"ED2B406EFC231BE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Watkins','Watkins',1476771747142,'Watkins',NULL,0,'{\"CNAME\":\"AEAB79582502F49C\",\"ID\":\"C44799B04A1C72E3\",\"NAME\":\"AEAB79582502F49C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Watson','Watson',1476771747142,'Watson',NULL,0,'{\"CNAME\":\"4190908D675ABC6C\",\"ID\":\"7A4E20A7BBEEB7A\",\"NAME\":\"4190908D675ABC6C\"}',NULL,NULL,1),('ConsumerBizListItem','1_Webb','Webb',1476771747142,'Webb',NULL,0,'{\"CNAME\":\"CEA6D33D7BE32043\",\"ID\":\"B432F34C5A997C8E\",\"NAME\":\"CEA6D33D7BE32043\"}',NULL,NULL,1),('ConsumerBizListItem','1_Webster','Webster',1476771747142,'Webster',NULL,0,'{\"CNAME\":\"90CEF87A29EFC802\",\"ID\":\"65FC52ED8F88C813\",\"NAME\":\"90CEF87A29EFC802\"}',NULL,NULL,1),('ConsumerBizListItem','1_Weiss','Weiss',1476771747142,'Weiss',NULL,0,'{\"CNAME\":\"D5574721179C49D5\",\"ID\":\"CB79F8FA58B91D3A\",\"NAME\":\"D5574721179C49D5\"}',NULL,NULL,1),('ConsumerBizListItem','1_Werner','Werner',1476771747142,'Werner',NULL,0,'{\"CNAME\":\"68124427CE41A87D\",\"ID\":\"E74C0D42B4433905\",\"NAME\":\"68124427CE41A87D\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wesley','Wesley',1476771747142,'Wesley',NULL,0,'{\"CNAME\":\"A09F91F8BE77E65B\",\"ID\":\"8C8A58FA97C205FF\",\"NAME\":\"A09F91F8BE77E65B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Whitaker','Whitaker',1476771747142,'Whitaker',NULL,0,'{\"CNAME\":\"67049D6C79667192\",\"ID\":\"44968AECE94F667E\",\"NAME\":\"67049D6C79667192\"}',NULL,NULL,1),('ConsumerBizListItem','1_Whitney','Whitney',1476771747142,'Whitney',NULL,0,'{\"CNAME\":\"5EFD5935913B175E\",\"ID\":\"9F6992966D4C363E\",\"NAME\":\"5EFD5935913B175E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wiggins','Wiggins',1476771747142,'Wiggins',NULL,0,'{\"CNAME\":\"24D29F5BC23F7923\",\"ID\":\"333222170AB9EDCA\",\"NAME\":\"24D29F5BC23F7923\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wilcox','Wilcox',1476771747142,'Wilcox',NULL,0,'{\"CNAME\":\"AF79D3DE450CE51E\",\"ID\":\"414E773D5B7E5C06\",\"NAME\":\"AF79D3DE450CE51E\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wiley','Wiley',1476771747142,'Wiley',NULL,0,'{\"CNAME\":\"535D33A0DD1FB211\",\"ID\":\"B139E104214A08AE\",\"NAME\":\"535D33A0DD1FB211\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wilkerson','Wilkerson',1476771747142,'Wilkerson',NULL,0,'{\"CNAME\":\"1F3B0A51B1373BFE\",\"ID\":\"950CA92A4DCF426\",\"NAME\":\"1F3B0A51B1373BFE\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wilkins','Wilkins',1476771747142,'Wilkins',NULL,0,'{\"CNAME\":\"4861463DA295EDC7\",\"ID\":\"5103C3584B063C43\",\"NAME\":\"4861463DA295EDC7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wilkinson','Wilkinson',1476771747142,'Wilkinson',NULL,0,'{\"CNAME\":\"24605FE91F62029F\",\"ID\":\"E5B294B70C9647DC\",\"NAME\":\"24605FE91F62029F\"}',NULL,NULL,1),('ConsumerBizListItem','1_William','William',1476771747142,'William',NULL,0,'{\"CNAME\":\"FD820A2B4461BDDD\",\"ID\":\"5BCE843DD76DB8C9\",\"NAME\":\"FD820A2B4461BDDD\"}',NULL,NULL,1),('ConsumerBizListItem','1_Williams','Williams',1476771747142,'Williams',NULL,0,'{\"CNAME\":\"44E7CDC8F1386A18\",\"ID\":\"139F0874F2DED2E4\",\"NAME\":\"44E7CDC8F1386A18\"}',NULL,NULL,1),('ConsumerBizListItem','1_Williamson','Williamson',1476771747142,'Williamson',NULL,0,'{\"CNAME\":\"FF25C29E683C6D23\",\"ID\":\"29530DE21430B754\",\"NAME\":\"FF25C29E683C6D23\"}',NULL,NULL,1),('ConsumerBizListItem','1_Willis','Willis',1476771747142,'Willis',NULL,0,'{\"CNAME\":\"44C1BEFEA85820E1\",\"ID\":\"15D185EAA7C954E7\",\"NAME\":\"44C1BEFEA85820E1\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wilson','Wilson',1476771747142,'Wilson',NULL,0,'{\"CNAME\":\"ABD7372BBA555775\",\"ID\":\"52D2752B150F9C35\",\"NAME\":\"ABD7372BBA555775\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wolfe','Wolfe',1476771747142,'Wolfe',NULL,0,'{\"CNAME\":\"42D6AC079958545B\",\"ID\":\"1E913E1B06EAD0B6\",\"NAME\":\"42D6AC079958545B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wong','Wong',1476771747142,'Wong',NULL,0,'{\"CNAME\":\"76F5D947149185D7\",\"ID\":\"8562AE5E28654471\",\"NAME\":\"76F5D947149185D7\"}',NULL,NULL,1),('ConsumerBizListItem','1_Woodard','Woodard',1476771747142,'Woodard',NULL,0,'{\"CNAME\":\"8FE642B5EA6F41F0\",\"ID\":\"8D55A249E6BAA5C0\",\"NAME\":\"8FE642B5EA6F41F0\"}',NULL,NULL,1),('ConsumerBizListItem','1_Woodward','Woodward',1476771747142,'Woodward',NULL,0,'{\"CNAME\":\"21DFD3BB76D9ACA4\",\"ID\":\"11108A3DBFE4636C\",\"NAME\":\"21DFD3BB76D9ACA4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Wyatt','Wyatt',1476771747142,'Wyatt',NULL,0,'{\"CNAME\":\"FBFC2161930F1857\",\"ID\":\"136F951362DAB62E\",\"NAME\":\"FBFC2161930F1857\"}',NULL,NULL,1),('ConsumerBizListItem','1_Yoder','Yoder',1476771747142,'Yoder',NULL,0,'{\"CNAME\":\"5219E19973FA6DD8\",\"ID\":\"AD4CC1FB9B068FAE\",\"NAME\":\"5219E19973FA6DD8\"}',NULL,NULL,1),('ConsumerBizListItem','1_York','York',1476771747142,'York',NULL,0,'{\"CNAME\":\"C3B29CE20CE560EF\",\"ID\":\"F22E4747DA1AA27E\",\"NAME\":\"C3B29CE20CE560EF\"}',NULL,NULL,1),('ConsumerBizListItem','1_Zachary','Zachary',1476771747142,'Zachary',NULL,0,'{\"CNAME\":\"905A74D4251FC1B\",\"ID\":\"95E6834D0A3D99E9\",\"NAME\":\"905A74D4251FC1B\"}',NULL,NULL,1),('ConsumerBizListItem','1_Zamora','Zamora',1476771747142,'Zamora',NULL,0,'{\"CNAME\":\"D19CFF9D1A41E5C9\",\"ID\":\"7AF6266CC52234B5\",\"NAME\":\"D19CFF9D1A41E5C9\"}',NULL,NULL,1),('ConsumerBizListItem','1_Zavala','Zavala',1476771747142,'Zavala',NULL,0,'{\"CNAME\":\"8A5D2F907A4CFAA4\",\"ID\":\"519C841559646593\",\"NAME\":\"8A5D2F907A4CFAA4\"}',NULL,NULL,1),('ConsumerBizListItem','1_Zhang','Zhang',1476771747142,'Zhang',NULL,0,'{\"CNAME\":\"D0CD2693B3506677\",\"ID\":\"C3395DD46C34FA7F\",\"NAME\":\"D0CD2693B3506677\"}',NULL,NULL,1),('ConsumerBizListItem','1_Zimmerman','Zimmerman',1476771747142,'Zimmerman',NULL,0,'{\"CNAME\":\"CAD244DFA5414A55\",\"ID\":\"6F2688A5FCE7D48C\",\"NAME\":\"CAD244DFA5414A55\"}',NULL,NULL,1),('ConsumerBizListItem','1_Zuniga','Zuniga',1476771747142,'Zuniga',NULL,0,'{\"CNAME\":\"F772DD197B80806D\",\"ID\":\"2D00F43F07911355\",\"NAME\":\"F772DD197B80806D\"}',NULL,NULL,1);
/*!40000 ALTER TABLE `bl_item_simple` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `business_id_tag_association`
--

DROP TABLE IF EXISTS `business_id_tag_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `business_id_tag_association` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `association_time_stamp` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `files_should_be_dirty` bit(1) NOT NULL,
  `solr_dirty` bit(1) NOT NULL,
  `compound_policy_item_id` bigint(20) DEFAULT NULL,
  `doc_type_id` bigint(20) DEFAULT NULL,
  `file_tag_id` bigint(20) DEFAULT NULL,
  `simple_biz_list_item_sid` varchar(255) DEFAULT NULL,
  `file_group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `association_time_stamp_idx` (`association_time_stamp`),
  KEY `solr_dirty_idx` (`solr_dirty`),
  KEY `deleted_idx` (`deleted`),
  KEY `FK_pt6wrfn6ey26gn12l1ks6ao8d` (`compound_policy_item_id`),
  KEY `FK_eiodnvsf11tgtbb0eggp0i85t` (`doc_type_id`),
  KEY `FK_2mdkvw11j2gpap588nem18sbj` (`file_tag_id`),
  KEY `FK_qmi8gsx5gi83d0vkxpxvmhfmw` (`simple_biz_list_item_sid`),
  KEY `FK_7x55j850t3k0l3iyhq64a763t` (`file_group_id`),
  CONSTRAINT `FK_2mdkvw11j2gpap588nem18sbj` FOREIGN KEY (`file_tag_id`) REFERENCES `file_tag` (`id`),
  CONSTRAINT `FK_7x55j850t3k0l3iyhq64a763t` FOREIGN KEY (`file_group_id`) REFERENCES `file_groups` (`id`),
  CONSTRAINT `FK_eiodnvsf11tgtbb0eggp0i85t` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`),
  CONSTRAINT `FK_pt6wrfn6ey26gn12l1ks6ao8d` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `pol_compound_policy_item` (`id`),
  CONSTRAINT `FK_qmi8gsx5gi83d0vkxpxvmhfmw` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `business_id_tag_association`
--

LOCK TABLES `business_id_tag_association` WRITE;
/*!40000 ALTER TABLE `business_id_tag_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `business_id_tag_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cat_fil_val`
--

DROP TABLE IF EXISTS `cat_fil_val`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cat_fil_val` (
  `cat_filter` int(11) NOT NULL,
  `f_values` varchar(255) DEFAULT NULL,
  KEY `FK_394673w2xdyg66780e0kemibm` (`cat_filter`),
  CONSTRAINT `FK_394673w2xdyg66780e0kemibm` FOREIGN KEY (`cat_filter`) REFERENCES `category_filters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cat_fil_val`
--

LOCK TABLES `cat_fil_val` WRITE;
/*!40000 ALTER TABLE `cat_fil_val` DISABLE KEYS */;
/*!40000 ALTER TABLE `cat_fil_val` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_filters`
--

DROP TABLE IF EXISTS `category_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_filters` (
  `filter_type` varchar(31) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8hct5sb7jlnktot1wp7aii66l` (`category_id`),
  CONSTRAINT `FK_8hct5sb7jlnktot1wp7aii66l` FOREIGN KEY (`category_id`) REFERENCES `file_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_filters`
--

LOCK TABLES `category_filters` WRITE;
/*!40000 ALTER TABLE `category_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cla_file_tag_association`
--

DROP TABLE IF EXISTS `cla_file_tag_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cla_file_tag_association` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `association_time_stamp` bigint(20) NOT NULL,
  `dirty` bit(1) NOT NULL,
  `compound_policy_item_id` bigint(20) DEFAULT NULL,
  `doc_type_id` bigint(20) DEFAULT NULL,
  `file_tag_id` bigint(20) DEFAULT NULL,
  `simple_biz_list_item_sid` varchar(255) DEFAULT NULL,
  `cla_file_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `association_time_stamp_idx` (`association_time_stamp`),
  KEY `dirty_idx` (`dirty`),
  KEY `FK_damcgwlew39d839mulfv6gycd` (`compound_policy_item_id`),
  KEY `FK_321x1e582kff2l1w7ypjpscx` (`doc_type_id`),
  KEY `FK_nmvjpl3womk11nf876rkhetvj` (`file_tag_id`),
  KEY `FK_dbgnlx2pksjsrhafclp3xj6ob` (`simple_biz_list_item_sid`),
  KEY `FK_7rps4gwenvdmt805djqaxkw1n` (`cla_file_id`),
  CONSTRAINT `FK_321x1e582kff2l1w7ypjpscx` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`),
  CONSTRAINT `FK_7rps4gwenvdmt805djqaxkw1n` FOREIGN KEY (`cla_file_id`) REFERENCES `cla_files_md` (`id`),
  CONSTRAINT `FK_damcgwlew39d839mulfv6gycd` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `pol_compound_policy_item` (`id`),
  CONSTRAINT `FK_dbgnlx2pksjsrhafclp3xj6ob` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`),
  CONSTRAINT `FK_nmvjpl3womk11nf876rkhetvj` FOREIGN KEY (`file_tag_id`) REFERENCES `file_tag` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cla_file_tag_association`
--

LOCK TABLES `cla_file_tag_association` WRITE;
/*!40000 ALTER TABLE `cla_file_tag_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `cla_file_tag_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cla_files_md`
--

DROP TABLE IF EXISTS `cla_files_md`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cla_files_md` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acl_signature` char(28) DEFAULT NULL,
  `base_name` varchar(255) DEFAULT NULL,
  `content_dirty` bit(1) NOT NULL,
  `created_ondb` datetime(3) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `deletion_date` datetime(3) DEFAULT NULL,
  `dirty` int(11) DEFAULT NULL,
  `dirty_wip` bit(1) NOT NULL,
  `extension` varchar(255) DEFAULT NULL,
  `file_name_hashed` char(32) NOT NULL,
  `fs_last_modified` datetime(3) DEFAULT NULL,
  `full_path` varchar(4096) NOT NULL,
  `initial_scan_date` datetime(3) DEFAULT NULL,
  `media_entity_id` varchar(255) DEFAULT NULL,
  `mime` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  `size_on_media` bigint(20) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `updated_ondb` datetime(3) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `file_group_id` char(36) DEFAULT NULL,
  `clajob_id` char(36) DEFAULT NULL,
  `content_metadata_id` bigint(20) DEFAULT NULL,
  `doc_folder_id` bigint(20) DEFAULT NULL,
  `ingestion_summary_contentId` bigint(20) DEFAULT NULL,
  `user_group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `acct_deleted_idx` (`account_id`,`deleted`),
  KEY `state_idx` (`state`,`dirty`),
  KEY `type_del_idx` (`type`,`deleted`),
  KEY `fileNameHashed_idx` (`file_name_hashed`),
  KEY `rf_acl_sig_idx` (`root_folder_id`,`acl_signature`),
  KEY `rf_fileNameHashed_idx` (`root_folder_id`,`file_name_hashed`),
  KEY `rf_fNHashed_lm_idx` (`root_folder_id`,`file_name_hashed`,`fs_last_modified`),
  KEY `rf_fNHashed_lm_ow_acl_idx` (`root_folder_id`,`file_name_hashed`,`fs_last_modified`,`owner`,`acl_signature`),
  KEY `fileGroup_idx` (`file_group_id`),
  KEY `userGroup_idx` (`user_group_id`),
  KEY `rootFolder_idx` (`root_folder_id`),
  KEY `rf_deleted_id_idx` (`root_folder_id`,`deleted`,`id`),
  KEY `acctTypeState_idx` (`account_id`,`type`,`state`,`deleted`),
  KEY `acctRfTypeState_idx` (`account_id`,`root_folder_id`,`type`,`state`,`deleted`),
  KEY `acct_content_metadata_idx` (`account_id`,`content_metadata_id`),
  KEY `content_metadata_idx` (`content_metadata_id`),
  KEY `dirty_idx` (`dirty_wip`,`dirty`,`deleted`),
  KEY `acct_dirty_id_idx` (`account_id`,`dirty_wip`,`dirty`,`deleted`),
  KEY `FK_75hcpcgi539yadsec4ob71im2` (`clajob_id`),
  KEY `FK_st6xwfk3j3a9t0892jickvg7m` (`doc_folder_id`),
  KEY `FK_83u5wjf1pj8mo6ldqilirdry3` (`ingestion_summary_contentId`),
  CONSTRAINT `FK_75hcpcgi539yadsec4ob71im2` FOREIGN KEY (`clajob_id`) REFERENCES `jobs` (`id`),
  CONSTRAINT `FK_83u5wjf1pj8mo6ldqilirdry3` FOREIGN KEY (`ingestion_summary_contentId`) REFERENCES `ingestion_summary` (`content_id`),
  CONSTRAINT `FK_cx3vbn520yc39rtodycvdh8u5` FOREIGN KEY (`user_group_id`) REFERENCES `file_groups` (`id`),
  CONSTRAINT `FK_ishmo5e0lwseyiuggmk7mud8p` FOREIGN KEY (`file_group_id`) REFERENCES `file_groups` (`id`),
  CONSTRAINT `FK_kn7rfddo0ws15ka51edonlni4` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FK_lu3e8cpc1g9mmsep8qrc8skj` FOREIGN KEY (`content_metadata_id`) REFERENCES `content_metadata` (`id`),
  CONSTRAINT `FK_st6xwfk3j3a9t0892jickvg7m` FOREIGN KEY (`doc_folder_id`) REFERENCES `doc_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cla_files_md`
--

LOCK TABLES `cla_files_md` WRITE;
/*!40000 ALTER TABLE `cla_files_md` DISABLE KEYS */;
/*!40000 ALTER TABLE `cla_files_md` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_metadata`
--

DROP TABLE IF EXISTS `content_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_metadata` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `analyze_hint` int(11) NOT NULL,
  `app_creation_date` datetime(3) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `content_signature` char(28) DEFAULT NULL,
  `file_count` int(11) NOT NULL,
  `fs_file_size` bigint(20) DEFAULT NULL,
  `generating_app` varchar(255) DEFAULT NULL,
  `group_dirty` bit(1) NOT NULL,
  `items_countl1` bigint(20) DEFAULT NULL,
  `items_countl2` bigint(20) DEFAULT NULL,
  `items_countl3` bigint(20) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `population_dirty` bit(1) NOT NULL,
  `sample_file_id` bigint(20) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `file_group_id` char(36) DEFAULT NULL,
  `user_group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `state_idx` (`state`),
  KEY `type_idx` (`type`),
  KEY `fileGroup_idx` (`file_group_id`),
  KEY `userGroup_idx` (`user_group_id`),
  KEY `typeState_idx` (`type`,`state`,`file_count`),
  KEY `popDirty_joinSignatureIdz` (`population_dirty`,`content_signature`),
  KEY `popDirty_idx` (`population_dirty`),
  KEY `acct_popDirty_idx` (`account_id`,`population_dirty`),
  KEY `contentSignatureIdx` (`content_signature`),
  CONSTRAINT `FK_qdb808lbkapfv2sxj6u2adhe8` FOREIGN KEY (`file_group_id`) REFERENCES `file_groups` (`id`),
  CONSTRAINT `FK_rbnt2nf1cn7ujc5somyjgphmf` FOREIGN KEY (`user_group_id`) REFERENCES `file_groups` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_metadata`
--

LOCK TABLES `content_metadata` WRITE;
/*!40000 ALTER TABLE `content_metadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_metadata_search_patterns_counting`
--

DROP TABLE IF EXISTS `content_metadata_search_patterns_counting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_metadata_search_patterns_counting` (
  `content_id` bigint(20) NOT NULL,
  `pattern_count` int(11) NOT NULL,
  `pattern_id` int(11) NOT NULL,
  `pattern_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`content_id`,`pattern_count`,`pattern_id`),
  KEY `patternId_idx` (`pattern_id`),
  KEY `patternCount_idx` (`pattern_count`),
  CONSTRAINT `FK_608io1ef4cns2dx1p3najbndk` FOREIGN KEY (`content_id`) REFERENCES `content_metadata` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_metadata_search_patterns_counting`
--

LOCK TABLES `content_metadata_search_patterns_counting` WRITE;
/*!40000 ALTER TABLE `content_metadata_search_patterns_counting` DISABLE KEYS */;
/*!40000 ALTER TABLE `content_metadata_search_patterns_counting` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `crawl_runs`
--

DROP TABLE IF EXISTS `crawl_runs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crawl_runs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `analyze_files` bit(1) NOT NULL,
  `analyzed_root_folders` varchar(4096) DEFAULT NULL,
  `deleted_files` bigint(20) NOT NULL,
  `exception_text` varchar(4096) DEFAULT NULL,
  `ingest_files` bit(1) NOT NULL,
  `ingested_root_folders` varchar(4096) DEFAULT NULL,
  `ingestion_errors` bigint(20) NOT NULL,
  `manual` bit(1) NOT NULL,
  `new_files` bigint(20) NOT NULL,
  `processed_excel_files` bigint(20) NOT NULL,
  `processed_other_files` bigint(20) NOT NULL,
  `processed_pdf_files` bigint(20) NOT NULL,
  `processed_word_files` bigint(20) NOT NULL,
  `root_folder_accessible` bit(1) NOT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  `run_config` longtext,
  `run_status` int(11) DEFAULT NULL,
  `scan_errors` bigint(20) NOT NULL,
  `scan_files` bit(1) NOT NULL,
  `schedule_group_id` bigint(20) DEFAULT NULL,
  `start_time` datetime(3) DEFAULT NULL,
  `state_string` varchar(255) DEFAULT NULL,
  `stop_time` datetime(3) DEFAULT NULL,
  `total_processed_files` bigint(20) NOT NULL,
  `updated_content_files` bigint(20) NOT NULL,
  `updated_metadata_files` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_rfid` (`root_folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crawl_runs`
--

LOCK TABLES `crawl_runs` WRITE;
/*!40000 ALTER TABLE `crawl_runs` DISABLE KEYS */;
/*!40000 ALTER TABLE `crawl_runs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date_range_partition`
--

DROP TABLE IF EXISTS `date_range_partition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_range_partition` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_range_partition`
--

LOCK TABLES `date_range_partition` WRITE;
/*!40000 ALTER TABLE `date_range_partition` DISABLE KEYS */;
INSERT INTO `date_range_partition` VALUES (1,'default','default');
/*!40000 ALTER TABLE `date_range_partition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date_range_partition_simple_date_range_items`
--

DROP TABLE IF EXISTS `date_range_partition_simple_date_range_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `date_range_partition_simple_date_range_items` (
  `date_range_partition_id` bigint(20) NOT NULL,
  `simple_date_range_items_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_pd4n2bi7we4rupwa3o2f1c6gj` (`simple_date_range_items_id`),
  KEY `FK_g66w92q1l1fnacoxvxpowe13o` (`date_range_partition_id`),
  CONSTRAINT `FK_g66w92q1l1fnacoxvxpowe13o` FOREIGN KEY (`date_range_partition_id`) REFERENCES `date_range_partition` (`id`),
  CONSTRAINT `FK_pd4n2bi7we4rupwa3o2f1c6gj` FOREIGN KEY (`simple_date_range_items_id`) REFERENCES `simple_date_range_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date_range_partition_simple_date_range_items`
--

LOCK TABLES `date_range_partition_simple_date_range_items` WRITE;
/*!40000 ALTER TABLE `date_range_partition_simple_date_range_items` DISABLE KEYS */;
INSERT INTO `date_range_partition_simple_date_range_items` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6);
/*!40000 ALTER TABLE `date_range_partition_simple_date_range_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deb_file_hist_info`
--

DROP TABLE IF EXISTS `deb_file_hist_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deb_file_hist_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `info` varchar(4096) DEFAULT NULL,
  `lid` bigint(20) DEFAULT NULL,
  `part` varchar(255) DEFAULT NULL,
  `ts_milli` bigint(20) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `uuid` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lid_idx` (`lid`),
  KEY `type_idx` (`type`),
  KEY `ts_idx` (`ts_milli`),
  KEY `uuid_idx` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deb_file_hist_info`
--

LOCK TABLES `deb_file_hist_info` WRITE;
/*!40000 ALTER TABLE `deb_file_hist_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `deb_file_hist_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deb_similarity_info`
--

DROP TABLE IF EXISTS `deb_similarity_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deb_similarity_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grouping_hint` varchar(255) DEFAULT NULL,
  `info` varchar(4096) DEFAULT NULL,
  `lid1` bigint(20) DEFAULT NULL,
  `lid2` bigint(20) DEFAULT NULL,
  `part1` varchar(255) DEFAULT NULL,
  `part2` varchar(255) DEFAULT NULL,
  `rates` varchar(1024) DEFAULT NULL,
  `ts_milli` bigint(20) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `uuid1` char(36) NOT NULL,
  `uuid2` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lid_idx1` (`lid1`),
  KEY `lid_idx2` (`lid2`),
  KEY `type_idx` (`type`),
  KEY `ts_idx` (`ts_milli`),
  KEY `uuid_idx1` (`uuid1`),
  KEY `uuid_idx2` (`uuid2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deb_similarity_info`
--

LOCK TABLES `deb_similarity_info` WRITE;
/*!40000 ALTER TABLE `deb_similarity_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `deb_similarity_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_authority_license`
--

DROP TABLE IF EXISTS `doc_authority_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_authority_license` (
  `id` bigint(20) NOT NULL,
  `apply_date` bigint(20) NOT NULL,
  `creation_date` bigint(20) NOT NULL,
  `issuer` varchar(255) DEFAULT NULL,
  `registered_to` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_authority_license`
--

LOCK TABLES `doc_authority_license` WRITE;
/*!40000 ALTER TABLE `doc_authority_license` DISABLE KEYS */;
INSERT INTO `doc_authority_license` VALUES (0,1476771746310,1476782546000,'Auto generated for APOV','- APOV -','Ey2gHRX759PwNP9e5yExkrVS8+g=');
/*!40000 ALTER TABLE `doc_authority_license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_folder`
--

DROP TABLE IF EXISTS `doc_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_folder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `absolute_depth` int(11) NOT NULL,
  `bounding_subtree_id` bigint(20) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `depth_from_root` int(11) NOT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `folder_hashed` char(32) NOT NULL,
  `media_entity_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `num_of_all_files` int(11) NOT NULL,
  `num_of_direct_files` int(11) NOT NULL,
  `num_of_sub_folders` int(11) DEFAULT NULL,
  `parents_info` varchar(255) DEFAULT NULL,
  `path` varchar(4096) DEFAULT NULL,
  `real_path` varchar(4096) DEFAULT NULL,
  `doc_store_id` bigint(20) DEFAULT NULL,
  `parent_folder_id` bigint(20) DEFAULT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `deleted_idx` (`deleted`),
  KEY `deleted_id_idx` (`deleted`,`id`),
  KEY `name_idx` (`name`),
  KEY `folderHashed_idx` (`folder_hashed`),
  KEY `deleted_hash_idx` (`deleted`,`folder_hashed`),
  KEY `deleted_rootFid_idx` (`root_folder_id`,`deleted`),
  KEY `depthFromRoot_idx` (`depth_from_root`),
  KEY `deleted_depthFromRoot_idx` (`deleted`,`depth_from_root`),
  KEY `deleted_depth_subFolders_id_idx` (`deleted`,`depth_from_root`,`num_of_sub_folders`,`id`),
  KEY `parentsInfo_idx` (`parents_info`),
  KEY `deleted_pFolder_pInfo_idx` (`parent_folder_id`,`parents_info`,`deleted`),
  KEY `FK_d5bo5sptv0av6hwd9b0gh0v73` (`doc_store_id`),
  KEY `path_idx` (`path`(255)),
  KEY `real_path_idx` (`real_path`(255)),
  KEY `del_path_idx` (`deleted`,`path`(250)),
  KEY `del_real_path_idx` (`deleted`,`real_path`(250)),
  CONSTRAINT `FK_d5bo5sptv0av6hwd9b0gh0v73` FOREIGN KEY (`doc_store_id`) REFERENCES `doc_store` (`id`),
  CONSTRAINT `FK_fuu3y3u0g8rjm4of8hvytfkhh` FOREIGN KEY (`parent_folder_id`) REFERENCES `doc_folder` (`id`),
  CONSTRAINT `FK_ikvu3xythlqjrarw1s3tgwk16` FOREIGN KEY (`root_folder_id`) REFERENCES `root_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_folder`
--

LOCK TABLES `doc_folder` WRITE;
/*!40000 ALTER TABLE `doc_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_store`
--

DROP TABLE IF EXISTS `doc_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_store` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `path_descriptor_type` varchar(255) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2f0nbhu6chfe6pra6w7j1u9p6` (`account_id`),
  CONSTRAINT `FK_2f0nbhu6chfe6pra6w7j1u9p6` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_store`
--

LOCK TABLES `doc_store` WRITE;
/*!40000 ALTER TABLE `doc_store` DISABLE KEYS */;
INSERT INTO `doc_store` VALUES (1,'Default DocStore','WINDOWS',1);
/*!40000 ALTER TABLE `doc_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_type`
--

DROP TABLE IF EXISTS `doc_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alternative_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `doc_type_identifier` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `unique_name` varchar(255) NOT NULL,
  `account_id` int(11) NOT NULL,
  `biz_list_id` bigint(20) DEFAULT NULL,
  `doc_type_risk_information_id` bigint(20) DEFAULT NULL,
  `parent_doc_type_id` bigint(20) DEFAULT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_par_acc` (`name`,`parent_doc_type_id`,`account_id`),
  KEY `doc_type_identifier_idx` (`doc_type_identifier`),
  KEY `FK_pe4r51c5vw04c3q0ysn2135s3` (`account_id`),
  KEY `FK_py85n74lrvmgnodt7anjgkt4v` (`biz_list_id`),
  KEY `FK_3j04hjv85m931mrl9btvqpjbl` (`doc_type_risk_information_id`),
  KEY `FK_34h58qm3dvto0kuuq8ayu351d` (`parent_doc_type_id`),
  KEY `FK_7lgfiwa353ckg6j8uv1v64q2y` (`template_id`),
  CONSTRAINT `FK_34h58qm3dvto0kuuq8ayu351d` FOREIGN KEY (`parent_doc_type_id`) REFERENCES `doc_type` (`id`),
  CONSTRAINT `FK_3j04hjv85m931mrl9btvqpjbl` FOREIGN KEY (`doc_type_risk_information_id`) REFERENCES `doc_type_risk_information` (`id`),
  CONSTRAINT `FK_7lgfiwa353ckg6j8uv1v64q2y` FOREIGN KEY (`template_id`) REFERENCES `doc_type_template` (`id`),
  CONSTRAINT `FK_pe4r51c5vw04c3q0ysn2135s3` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FK_py85n74lrvmgnodt7anjgkt4v` FOREIGN KEY (`biz_list_id`) REFERENCES `biz_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_type`
--

LOCK TABLES `doc_type` WRITE;
/*!40000 ALTER TABLE `doc_type` DISABLE KEYS */;
INSERT INTO `doc_type` VALUES (1,'שיווק ומכירות',NULL,'dt.1','Sales & Marketing','Sales & Marketing89',1,NULL,1,NULL,1),(2,'הצעות מחיר ומכתבי התקשרות',NULL,'dt.1.2','Proposals & Contracts','Proposals & Contracts90',1,NULL,1,1,1),(3,'תיקשור והזמנות',NULL,'dt.1.3','Newsletters & Invitations','Newsletters & Invitations91',1,NULL,1,1,1),(4,'סקרי שביעות רצון',NULL,'dt.1.4','Customer Satisfaction Surveys','Customer Satisfaction Surveys92',1,NULL,1,1,1),(5,'מכרזים',NULL,'dt.1.5','Tenders','Tenders124',1,NULL,1,1,1),(6,'תפעול',NULL,'dt.6','Operations','Operations93',1,NULL,1,NULL,1),(7,'מכתבים ופקסים',NULL,'dt.6.7','Letters & Faxes','Letters & Faxes94',1,NULL,1,6,1),(8,'חשבות',NULL,'dt.8','Finance & Accounting','Finance & Accounting95',1,NULL,1,NULL,1),(9,'פעולות בנקאיות והעברות כספיות',NULL,'dt.8.9','Bank Operations and Transfers','Bank Operations and Transfers96',1,NULL,1,8,1),(10,'גביה',NULL,'dt.8.10','Collection','Collection97',1,NULL,1,8,1),(11,'תקציבים',NULL,'dt.8.11','Budgets & Plans','Budgets & Plans101',1,NULL,1,8,1),(12,'דיווח כספי',NULL,'dt.8.12','Financial Reports','Financial Reports104',1,NULL,1,8,1),(13,'חשבוניות',NULL,'dt.8.10.13','Invoices','Invoices98',1,NULL,1,10,1),(14,'חשבונות שכר טרחה',NULL,'dt.8.10.14','Proforma Invoices','Proforma Invoices99',1,NULL,1,10,1),(15,'דיווח שעות',NULL,'dt.8.10.15','Work Hours Reports','Work Hours Reports100',1,NULL,1,10,1),(16,'תקציב לעומת ביצוע',NULL,'dt.8.11.16','Budget to Actual','Budget to Actual102',1,NULL,1,11,1),(17,'תקציב',NULL,'dt.8.11.17','Budgets','Budgets103',1,NULL,1,11,1),(18,'10-k',NULL,'dt.8.12.18','10-k','10-k105',1,NULL,1,12,1),(19,'10-q',NULL,'dt.8.12.19','10-q','10-q106',1,NULL,1,12,1),(20,'דיווחים לבורסה',NULL,'dt.8.12.20','TASE Reports','TASE Reports107',1,NULL,1,12,1),(21,'משאבי אנוש',NULL,'dt.21','HR','HR108',1,NULL,1,NULL,1),(22,'העסקה',NULL,'dt.21.22','Employment','Employment109',1,NULL,1,21,1),(23,'גיוס והשמה',NULL,'dt.21.23','Hiring','Hiring115',1,NULL,1,21,1),(24,'Awards Review and Processing',NULL,'dt.21.24','Awards Review and Processing','Awards Review and Processing138',1,NULL,1,21,1),(25,'הטבות',NULL,'dt.21.25','Benefits','Benefits139',1,NULL,1,21,1),(26,'החלטות שכר',NULL,'dt.21.26','Salary Determination','Salary Determination148',1,NULL,1,21,1),(27,'קידום',NULL,'dt.21.27','Promotions','Promotions149',1,NULL,1,21,1),(28,'מעברי תפקיד',NULL,'dt.21.28','Reassignments','Reassignments150',1,NULL,1,21,1),(29,'מדיניות משאבי אנוש',NULL,'dt.21.29','Policy Development','Policy Development151',1,NULL,1,21,1),(30,'אישורי עבודה, הגירה וויזות',NULL,'dt.21.30','Work Permits, Immigration & Visa','Work Permits Immigration & Visa152',1,NULL,1,21,1),(31,'דרוג משרות וסיווג',NULL,'dt.21.31','Position Classification and Grading','Position Classification and Grading153',1,NULL,1,21,1),(32,'תוכניות תיגמול',NULL,'dt.21.32','Compensation','Compensation154',1,NULL,1,21,1),(33,'ניתוח תפקידים',NULL,'dt.21.33','Job Analysis','Job Analysis155',1,NULL,1,21,1),(34,'ניהול קריירה',NULL,'dt.21.34','Career Planning','Career Planning156',1,NULL,1,21,1),(35,'יחסי עבודה',NULL,'dt.21.35','Labour Relations','Labour Relations157',1,NULL,1,21,1),(36,'עמידה בחוקי עבודה ותקינה',NULL,'dt.21.36','Labor Law Compliance','Labor Law Compliance158',1,NULL,1,21,1),(37,'הסכמי העסקה',NULL,'dt.21.22.37','Employment Agreements','Employment Agreements110',1,NULL,1,22,1),(38,'טפסי העברה ומכתבי פיטורין',NULL,'dt.21.22.38','Personnel Action Forms','Personnel Action Forms111',1,NULL,1,22,1),(39,'הערכות עובדים',NULL,'dt.21.22.39','Employee Evaluations','Employee Evaluations112',1,NULL,1,22,1),(40,'שינויים בתנאי העסקה',NULL,'dt.21.22.40','Employment Terms Changes','Employment Terms Changes113',1,NULL,1,22,1),(41,'מכתבי המלצה',NULL,'dt.21.22.41','Recommendation Letters','Recommendation Letters114',1,NULL,1,22,1),(42,'ראיונות עם מועמדים',NULL,'dt.21.23.42','Applicant Reports','Applicant Reports116',1,NULL,1,23,1),(43,'בדיקות רפואיות',NULL,'dt.21.23.43','Pre-Hire Physical Reports','Pre-Hire Physical Reports117',1,NULL,1,23,1),(44,'שכר',NULL,'dt.44','Salary & Payroll','Salary & Payroll118',1,NULL,1,NULL,1),(45,'רשימות משכורות',NULL,'dt.44.45','Salary lists','Salary lists119',1,NULL,1,44,1),(46,'מחלקה משפטית',NULL,'dt.46','Legal','Legal120',1,NULL,1,NULL,1),(47,'Confidentiality Agreeme',NULL,'dt.46.47','Confidentiality Agreements','Confidentiality Agreements136',1,NULL,1,46,1),(48,'Service Contra',NULL,'dt.46.48','Service Contracts','Service Contracts137',1,NULL,1,46,1),(49,'ביטוחי בריאות',NULL,'dt.21.25.49','Healthcare Insurance','Healthcare Insurance140',1,NULL,1,25,1),(50,'ביטוחי חיים ופנסיה',NULL,'dt.21.25.50','Life Insurance','Life Insurance141',1,NULL,1,25,1),(51,'קופות גמל',NULL,'dt.21.25.51','Disability Insurance','Disability Insurance142',1,NULL,1,25,1),(52,'פרישה',NULL,'dt.21.25.52','Retirement','Retirement143',1,NULL,1,25,1),(53,'ביטוחי רשות',NULL,'dt.21.25.53','Voluntary Accidental Death and Dismemberment Insurance','Voluntary Accidental Death and Dismemberment Insurance144',1,NULL,1,25,1),(54,'Leave Transfer',NULL,'dt.21.25.54','Leave Transfer','Leave Transfer145',1,NULL,1,25,1),(55,'קורסים והכשרות',NULL,'dt.21.25.55','Training','Training146',1,NULL,1,25,1),(56,'תיגמול עובדים',NULL,'dt.21.25.56','Workers Compensation','Workers Compensation147',1,NULL,1,25,1);
/*!40000 ALTER TABLE `doc_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_type_association`
--

DROP TABLE IF EXISTS `doc_type_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_type_association` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `association_source` int(11) DEFAULT NULL,
  `association_time_stamp` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `dirty` bit(1) NOT NULL,
  `files_should_be_dirty` bit(1) NOT NULL,
  `implicit` bit(1) NOT NULL,
  `doc_type_id` bigint(20) NOT NULL,
  `origin_folder_id` bigint(20) DEFAULT NULL,
  `cla_file_id` bigint(20) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  `folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `association_time_stamp_idx` (`association_time_stamp`),
  KEY `dirty_idx` (`dirty`),
  KEY `deleted_idx` (`deleted`),
  KEY `source_idx` (`association_source`),
  KEY `FK_gho3aw0rqc5xy2vq1tnn57hh0` (`doc_type_id`),
  KEY `FK_gurkgxopf3dtv4l30a76eep1e` (`origin_folder_id`),
  KEY `FK_sykd6ajhuwu1fvc92a7w7q2bb` (`cla_file_id`),
  KEY `FK_g90b6whii8j78n3uws2dai80` (`group_id`),
  KEY `FK_rm5amcr4ai54e1kfyfqcqvr6x` (`folder_id`),
  CONSTRAINT `FK_g90b6whii8j78n3uws2dai80` FOREIGN KEY (`group_id`) REFERENCES `file_groups` (`id`),
  CONSTRAINT `FK_gho3aw0rqc5xy2vq1tnn57hh0` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`),
  CONSTRAINT `FK_gurkgxopf3dtv4l30a76eep1e` FOREIGN KEY (`origin_folder_id`) REFERENCES `doc_folder` (`id`),
  CONSTRAINT `FK_rm5amcr4ai54e1kfyfqcqvr6x` FOREIGN KEY (`folder_id`) REFERENCES `doc_folder` (`id`),
  CONSTRAINT `FK_sykd6ajhuwu1fvc92a7w7q2bb` FOREIGN KEY (`cla_file_id`) REFERENCES `cla_files_md` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_type_association`
--

LOCK TABLES `doc_type_association` WRITE;
/*!40000 ALTER TABLE `doc_type_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `doc_type_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_type_risk_information`
--

DROP TABLE IF EXISTS `doc_type_risk_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_type_risk_information` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `default_cost_per_item` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_acc` (`name`,`account_id`),
  KEY `name_idx` (`name`),
  KEY `FK_15niskg56pg91cnhra8tig4t7` (`account_id`),
  CONSTRAINT `FK_15niskg56pg91cnhra8tig4t7` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_type_risk_information`
--

LOCK TABLES `doc_type_risk_information` WRITE;
/*!40000 ALTER TABLE `doc_type_risk_information` DISABLE KEYS */;
INSERT INTO `doc_type_risk_information` VALUES (1,0,'default',1),(2,0,'low',1),(3,0,'medium',1),(4,0,'high',1);
/*!40000 ALTER TABLE `doc_type_risk_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doc_type_template`
--

DROP TABLE IF EXISTS `doc_type_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doc_type_template` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_acc` (`name`,`account_id`),
  KEY `FK_cxff57ufhq9ti918fqtxeoy55` (`account_id`),
  CONSTRAINT `FK_cxff57ufhq9ti918fqtxeoy55` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doc_type_template`
--

LOCK TABLES `doc_type_template` WRITE;
/*!40000 ALTER TABLE `doc_type_template` DISABLE KEYS */;
INSERT INTO `doc_type_template` VALUES (1,'./config/defaultDocTypeTemplate.csv',1);
/*!40000 ALTER TABLE `doc_type_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `err_processed_files`
--

DROP TABLE IF EXISTS `err_processed_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `err_processed_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_ondb` datetime(3) DEFAULT NULL,
  `exception_text` varchar(255) DEFAULT NULL,
  `run_id` bigint(20) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `cla_file_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_469wngckypwc0r16ry3ga5inx` (`cla_file_id`),
  CONSTRAINT `FK_469wngckypwc0r16ry3ga5inx` FOREIGN KEY (`cla_file_id`) REFERENCES `cla_files_md` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `err_processed_files`
--

LOCK TABLES `err_processed_files` WRITE;
/*!40000 ALTER TABLE `err_processed_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `err_processed_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `err_scanned_files`
--

DROP TABLE IF EXISTS `err_scanned_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `err_scanned_files` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_ondb` datetime(3) DEFAULT NULL,
  `exception_text` varchar(255) DEFAULT NULL,
  `path` varchar(4096) DEFAULT NULL,
  `run_id` bigint(20) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `doc_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_9u5tsiu12a16t9oox9gipbgvb` (`run_id`),
  KEY `FK_r54t5i4jjfcyshauqmkse9bf4` (`doc_folder_id`),
  CONSTRAINT `FK_r54t5i4jjfcyshauqmkse9bf4` FOREIGN KEY (`doc_folder_id`) REFERENCES `doc_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `err_scanned_files`
--

LOCK TABLES `err_scanned_files` WRITE;
/*!40000 ALTER TABLE `err_scanned_files` DISABLE KEYS */;
/*!40000 ALTER TABLE `err_scanned_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extracted_entities`
--

DROP TABLE IF EXISTS `extracted_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extracted_entities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cla_file_id` bigint(20) NOT NULL,
  `cla_file_name` varchar(4096) NOT NULL,
  `cla_file_part` varchar(255) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `clear_fields` varchar(255) NOT NULL,
  `custom_field` varchar(255) NOT NULL,
  `fields_values` longtext,
  `line_number` int(11) NOT NULL,
  `rule` varchar(255) DEFAULT NULL,
  `rule_param` smallint(6) DEFAULT NULL,
  `rule_type` int(11) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `clearFields_idx` (`clear_fields`),
  KEY `rule_idx` (`rule`),
  KEY `ruleType_idx` (`rule_type`),
  KEY `ruleParam_idx` (`rule_param`),
  KEY `customField_idx` (`custom_field`),
  KEY `claFileId_idx` (`cla_file_id`),
  KEY `groupId_idx` (`group_id`),
  KEY `FK_rnc8wnu32d8v8k7mxpi8v3w9n` (`account_id`),
  CONSTRAINT `FK_rnc8wnu32d8v8k7mxpi8v3w9n` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extracted_entities`
--

LOCK TABLES `extracted_entities` WRITE;
/*!40000 ALTER TABLE `extracted_entities` DISABLE KEYS */;
/*!40000 ALTER TABLE `extracted_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extracted_text`
--

DROP TABLE IF EXISTS `extracted_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extracted_text` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cla_file_id` bigint(20) NOT NULL,
  `cla_file_name` varchar(4096) NOT NULL,
  `cla_file_part` varchar(255) DEFAULT NULL,
  `group_id` char(36) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `hashed_query_text` varchar(255) NOT NULL,
  `query_name` varchar(255) NOT NULL,
  `query_text` varchar(255) NOT NULL,
  `result_order` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `resultOrder_idx_et` (`result_order`),
  KEY `claFileId_idx_et` (`cla_file_id`),
  KEY `groupId_idx_et` (`group_id`),
  KEY `FK_r5c71fi0w88be8yt8wupevr6c` (`account_id`),
  CONSTRAINT `FK_r5c71fi0w88be8yt8wupevr6c` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extracted_text`
--

LOCK TABLES `extracted_text` WRITE;
/*!40000 ALTER TABLE `extracted_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `extracted_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extraction_configuration`
--

DROP TABLE IF EXISTS `extraction_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extraction_configuration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `extraction_purpose` int(11) DEFAULT NULL,
  `extraction_rules_file` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `search_type` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `biz_list_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`),
  KEY `FK_nr1c1a1t2g1chued1bb88bq59` (`account_id`),
  KEY `FK_pbpo8ukh2hx79r4r93k5nhhr4` (`biz_list_id`),
  CONSTRAINT `FK_nr1c1a1t2g1chued1bb88bq59` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FK_pbpo8ukh2hx79r4r93k5nhhr4` FOREIGN KEY (`biz_list_id`) REFERENCES `biz_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extraction_configuration`
--

LOCK TABLES `extraction_configuration` WRITE;
/*!40000 ALTER TABLE `extraction_configuration` DISABLE KEYS */;
INSERT INTO `extraction_configuration` VALUES (1,0,'./config/extraction/extraction-rules-NameOnly.txt','default',2,1,NULL);
/*!40000 ALTER TABLE `extraction_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_categories`
--

DROP TABLE IF EXISTS `file_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `scope` int(11) NOT NULL,
  `state` varchar(255) DEFAULT NULL,
  `static_category` bit(1) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_categories`
--

LOCK TABLES `file_categories` WRITE;
/*!40000 ALTER TABLE `file_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_groups`
--

DROP TABLE IF EXISTS `file_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_groups` (
  `id` char(36) NOT NULL,
  `count_on_last_naming` bigint(20) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `dirty` bit(1) NOT NULL,
  `file_namestv` longtext,
  `first_member_content_id` bigint(20) DEFAULT NULL,
  `first_proposed_names` longtext,
  `generated_name` varchar(255) DEFAULT NULL,
  `grand_parent_dirtv` longtext,
  `group_type` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `num_of_extraction_hit_entities` bigint(20) DEFAULT NULL,
  `num_of_extraction_hit_files` bigint(20) DEFAULT NULL,
  `num_of_files` bigint(20) DEFAULT NULL,
  `parent_dirtv` longtext,
  `proposed_names` longtext,
  `proposed_new_names` longtext,
  `second_proposed_names` longtext,
  `third_proposed_names` longtext,
  `account_id` int(11) NOT NULL,
  `first_member_id` bigint(20) DEFAULT NULL,
  `parent_group_id` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_del_idx` (`id`,`deleted`),
  KEY `numOfFiles_idx` (`num_of_files`),
  KEY `deleted_idx` (`deleted`),
  KEY `del_nof_idx` (`deleted`,`num_of_files`),
  KEY `del_type_idx` (`deleted`,`group_type`),
  KEY `dirty_type_idx` (`dirty`,`group_type`),
  KEY `dirty_idx` (`dirty`),
  KEY `account_dirty_idx` (`account_id`,`dirty`),
  KEY `name_idx` (`name`),
  KEY `FK_ih3m78btt910y473a2uhpx84r` (`first_member_id`),
  KEY `FK_72exeytbij7owr76xepmx52v` (`parent_group_id`),
  CONSTRAINT `FK_72exeytbij7owr76xepmx52v` FOREIGN KEY (`parent_group_id`) REFERENCES `file_groups` (`id`),
  CONSTRAINT `FK_ih3m78btt910y473a2uhpx84r` FOREIGN KEY (`first_member_id`) REFERENCES `cla_files_md` (`id`),
  CONSTRAINT `FK_qns2j0v3yeoxb7cjf19n5pxmd` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_groups`
--

LOCK TABLES `file_groups` WRITE;
/*!40000 ALTER TABLE `file_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_tag`
--

DROP TABLE IF EXISTS `file_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `action` int(11) DEFAULT NULL,
  `alternative_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `entity_id_context` varchar(255) DEFAULT NULL,
  `identifier` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner_id` bigint(20) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `doctype_id` bigint(20) DEFAULT NULL,
  `parent_tag_id` bigint(20) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_hw1go61s8newyypefggvqqg3b` (`account_id`,`identifier`),
  KEY `identifier_idx` (`identifier`),
  KEY `name_idx` (`name`),
  KEY `FK_c5d2e51jy1s250psxlmv9q3wg` (`doctype_id`),
  KEY `FK_q7beoo5o1kpyamn6wscewfs71` (`parent_tag_id`),
  KEY `FK_spd93fn5k12r7x22jr3dto46d` (`type_id`),
  CONSTRAINT `FK_c5d2e51jy1s250psxlmv9q3wg` FOREIGN KEY (`doctype_id`) REFERENCES `doc_type` (`id`),
  CONSTRAINT `FK_p450scyky4irl3suic54xdp33` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`),
  CONSTRAINT `FK_q7beoo5o1kpyamn6wscewfs71` FOREIGN KEY (`parent_tag_id`) REFERENCES `file_tag` (`id`),
  CONSTRAINT `FK_spd93fn5k12r7x22jr3dto46d` FOREIGN KEY (`type_id`) REFERENCES `file_tag_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_tag`
--

LOCK TABLES `file_tag` WRITE;
/*!40000 ALTER TABLE `file_tag` DISABLE KEYS */;
INSERT INTO `file_tag` VALUES (1,4,NULL,'',NULL,'1_Basel_Regulation','Basel',1,1,NULL,NULL,4),(2,4,NULL,'',NULL,'1_Check 21 Act_Regulation','Check 21 Act',1,1,NULL,NULL,4),(3,4,NULL,'',NULL,'1_COPPA_Regulation','COPPA',1,1,NULL,NULL,4),(4,4,NULL,'',NULL,'1_DIACAP_Regulation','DIACAP',1,1,NULL,NULL,4),(5,4,NULL,'',NULL,'1_EU GDPR_Regulation','EU GDPR',1,1,NULL,NULL,4),(6,4,NULL,'',NULL,'1_EAR_Regulation','EAR',1,1,NULL,NULL,4),(7,4,NULL,'',NULL,'1_FCRA_Regulation','FCRA',1,1,NULL,NULL,4),(8,4,NULL,'',NULL,'1_FDA-21 CFR_Regulation','FDA-21 CFR',1,1,NULL,NULL,4),(9,4,NULL,'',NULL,'1_FERC & NERC_Regulation','FERC & NERC',1,1,NULL,NULL,4),(10,4,NULL,'',NULL,'1_FERPA_Regulation','FERPA',1,1,NULL,NULL,4),(11,4,NULL,'',NULL,'1_FFIEC_Regulation','FFIEC',1,1,NULL,NULL,4),(12,4,NULL,'',NULL,'1_FISMA_Regulation','FISMA',1,1,NULL,NULL,4),(13,4,NULL,'',NULL,'1_GLBA_Regulation','GLBA',1,1,NULL,NULL,4),(14,4,NULL,'',NULL,'1_HIPAA_Regulation','HIPAA',1,1,NULL,NULL,4),(15,4,NULL,'',NULL,'1_ITAR_Regulation','ITAR',1,1,NULL,NULL,4),(16,4,NULL,'',NULL,'1_MITS_Regulation','MITS',1,1,NULL,NULL,4),(17,4,NULL,'',NULL,'1_PCI DSS_Regulation','PCI DSS',1,1,NULL,NULL,4),(18,4,NULL,'',NULL,'1_SOX_Regulation','SOX',1,1,NULL,NULL,4),(19,4,NULL,'',NULL,'1_None_Retention','None',1,1,NULL,NULL,13),(20,4,NULL,'',NULL,'1_None_DLP','None',1,1,NULL,NULL,14),(21,4,NULL,'',NULL,'1_None_Governance','None',1,1,NULL,NULL,15),(22,4,NULL,'',NULL,'1_None_Encryption','None',1,1,NULL,NULL,16),(23,4,NULL,'',NULL,'1_Mitigated_Mitigation','Mitigated',1,1,NULL,NULL,17),(24,4,NULL,'',NULL,'1_Very important_Criticality','Very important',1,1,NULL,NULL,18),(25,4,NULL,'',NULL,'1_Important_Criticality','Important',1,1,NULL,NULL,18),(26,4,NULL,'',NULL,'1_Somewhat important_Criticality','Somewhat important',1,1,NULL,NULL,18),(27,4,NULL,'',NULL,'1_Not important_Criticality','Not important',1,1,NULL,NULL,18);
/*!40000 ALTER TABLE `file_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_tag_type`
--

DROP TABLE IF EXISTS `file_tag_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_tag_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alternative_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `hidden` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sensitive_tag` bit(1) NOT NULL,
  `system` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`),
  KEY `sensitive_tag_idx` (`sensitive_tag`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_tag_type`
--

LOCK TABLES `file_tag_type` WRITE;
/*!40000 ALTER TABLE `file_tag_type` DISABLE KEYS */;
INSERT INTO `file_tag_type` VALUES (1,NULL,'General purpose tag type (default)','\0','User','\0','\0'),(2,NULL,'Marks items with potential asset/sensitivity type','\0','Asset Type','','\0'),(3,NULL,'Marks items with potential sensitivity level','\0','Sensitivity','','\0'),(4,NULL,'Marks items with potential regulation issues','\0','Regulation','','\0'),(5,NULL,'Policy assignment indication','\0','Policy','\0','\0'),(6,NULL,'Indicate pattern search match','\0','Pattern','\0',''),(7,NULL,'Indicate multiple pattern search matches','\0','Pattern-many','\0',''),(8,NULL,'Marks a folder as belonging to a specific department','\0','Department','\0','\0'),(9,NULL,'Indicates that an entity rule matched the file','\0','Entity-rules','\0',''),(10,NULL,'Indicates that a BizList item is associated with the file','','BizList-Item-Association','\0',''),(11,NULL,'Indicates that a single BizListItem matched most of the files','\0','Single-BizList-Match','\0',''),(12,NULL,'Indicates that multiple BizListItems matched most of the files','\0','Multi-BizList-Match','\0',''),(13,NULL,'Retention policy','\0','Retention','\0','\0'),(14,NULL,'DLP policy','\0','DLP','\0','\0'),(15,NULL,'Governance policy','\0','Governance','\0','\0'),(16,NULL,'Encryption policy','\0','Encryption','\0','\0'),(17,NULL,'Mitigation indication','\0','Mitigation','\0','\0'),(18,NULL,'Criticality indication','\0','Criticality','\0','\0');
/*!40000 ALTER TABLE `file_tag_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder_exclude_rule`
--

DROP TABLE IF EXISTS `folder_exclude_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder_exclude_rule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `disabled` bit(1) NOT NULL,
  `match_string` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rootsatch_op` (`root_folder_id`,`match_string`,`operator`),
  CONSTRAINT `FK_291t895y781n6yc5dq6b3n4nc` FOREIGN KEY (`root_folder_id`) REFERENCES `root_folder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder_exclude_rule`
--

LOCK TABLES `folder_exclude_rule` WRITE;
/*!40000 ALTER TABLE `folder_exclude_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `folder_exclude_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folder_tag_association`
--

DROP TABLE IF EXISTS `folder_tag_association`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folder_tag_association` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `association_time_stamp` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `files_should_be_dirty` bit(1) NOT NULL,
  `implicit` bit(1) NOT NULL,
  `solr_dirty` bit(1) NOT NULL,
  `compound_policy_item_id` bigint(20) DEFAULT NULL,
  `doc_type_id` bigint(20) DEFAULT NULL,
  `file_tag_id` bigint(20) DEFAULT NULL,
  `origin_folder_id` bigint(20) NOT NULL,
  `simple_biz_list_item_sid` varchar(255) DEFAULT NULL,
  `doc_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `association_time_stamp_idx` (`association_time_stamp`),
  KEY `solr_dirty_idx` (`solr_dirty`),
  KEY `implicit_idx` (`implicit`),
  KEY `deleted_idx` (`deleted`),
  KEY `FK_17poen2bw2nirliv2efo62832` (`compound_policy_item_id`),
  KEY `FK_7md1p1i71qgv2q9j6tfae01n` (`doc_type_id`),
  KEY `FK_fk5u9lp381gdlt043tueapgo3` (`file_tag_id`),
  KEY `FK_5n8xxeym7yabpv418yccyncrn` (`origin_folder_id`),
  KEY `FK_m4cbjjtiv37mn91phi41pv1ai` (`simple_biz_list_item_sid`),
  KEY `FK_dx68i4asuxhoaukihkmrf90ry` (`doc_folder_id`),
  CONSTRAINT `FK_17poen2bw2nirliv2efo62832` FOREIGN KEY (`compound_policy_item_id`) REFERENCES `pol_compound_policy_item` (`id`),
  CONSTRAINT `FK_5n8xxeym7yabpv418yccyncrn` FOREIGN KEY (`origin_folder_id`) REFERENCES `doc_folder` (`id`),
  CONSTRAINT `FK_7md1p1i71qgv2q9j6tfae01n` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`),
  CONSTRAINT `FK_dx68i4asuxhoaukihkmrf90ry` FOREIGN KEY (`doc_folder_id`) REFERENCES `doc_folder` (`id`),
  CONSTRAINT `FK_fk5u9lp381gdlt043tueapgo3` FOREIGN KEY (`file_tag_id`) REFERENCES `file_tag` (`id`),
  CONSTRAINT `FK_m4cbjjtiv37mn91phi41pv1ai` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folder_tag_association`
--

LOCK TABLES `folder_tag_association` WRITE;
/*!40000 ALTER TABLE `folder_tag_association` DISABLE KEYS */;
/*!40000 ALTER TABLE `folder_tag_association` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group_unification_helper`
--

DROP TABLE IF EXISTS `group_unification_helper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_unification_helper` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_ondb` datetime(3) DEFAULT NULL,
  `new_group_id` varchar(255) NOT NULL,
  `orig_group` varchar(255) NOT NULL,
  `processed` bit(1) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_processed_idx` (`id`,`processed`),
  KEY `orig_group_idx` (`orig_group`,`processed`),
  KEY `FK_9jn3l0dbtonawvwnxhcpch5aq` (`account_id`),
  CONSTRAINT `FK_9jn3l0dbtonawvwnxhcpch5aq` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_unification_helper`
--

LOCK TABLES `group_unification_helper` WRITE;
/*!40000 ALTER TABLE `group_unification_helper` DISABLE KEYS */;
/*!40000 ALTER TABLE `group_unification_helper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingestion_summary`
--

DROP TABLE IF EXISTS `ingestion_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingestion_summary` (
  `content_id` bigint(20) NOT NULL,
  `proposed_titles` longblob,
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingestion_summary`
--

LOCK TABLES `ingestion_summary` WRITE;
/*!40000 ALTER TABLE `ingestion_summary` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingestion_summary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `dtype` varchar(31) NOT NULL,
  `id` char(36) NOT NULL,
  `is_file_fetcher` bit(1) NOT NULL,
  `mcf_job_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `server` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license_resource`
--

DROP TABLE IF EXISTS `license_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license_resource` (
  `id` varchar(255) NOT NULL,
  `issue_time` bigint(20) NOT NULL,
  `signature` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license_resource`
--

LOCK TABLES `license_resource` WRITE;
/*!40000 ALTER TABLE `license_resource` DISABLE KEYS */;
INSERT INTO `license_resource` VALUES ('crawlerExpiry',1476782546000,'q05WtDjrGDJ7j0sE4FoczDz66pk=','1478078546000'),('loginExpiry',1476782546000,'IQymTZuoOQv7S0tS2RfNdm9uFGk=','1479374546000'),('maxFiles',1476782546000,'bCnNKQT/XOjRNGU5bQ6iMz418Ao=','1200000'),('maxRootFolders',1476782546000,'XS6KhgxMv+Zwhzlhj69Rmxv8HXo=','*'),('serverId',1476782546000,'gKmO6SwqtaT8zyQDCRSmzFn6VKs=','7e293d2a-3d12');
/*!40000 ALTER TABLE `license_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `missed_file_match`
--

DROP TABLE IF EXISTS `missed_file_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `missed_file_match` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_ondb` datetime(3) DEFAULT NULL,
  `file1_id` bigint(20) NOT NULL,
  `file2_id` bigint(20) NOT NULL,
  `processed` bit(1) NOT NULL,
  `reason` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_processed_idx` (`id`,`processed`),
  KEY `fileid1_idx` (`file1_id`,`processed`),
  KEY `fileid2_idx` (`file2_id`,`processed`),
  KEY `FK_7gbn3d627l5nie8rf7pxyp4sp` (`account_id`),
  CONSTRAINT `FK_7gbn3d627l5nie8rf7pxyp4sp` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `missed_file_match`
--

LOCK TABLES `missed_file_match` WRITE;
/*!40000 ALTER TABLE `missed_file_match` DISABLE KEYS */;
/*!40000 ALTER TABLE `missed_file_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phase_details`
--

DROP TABLE IF EXISTS `phase_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phase_details` (
  `id` int(11) NOT NULL,
  `count` bigint(20) NOT NULL,
  `count_max_mark` bigint(20) NOT NULL,
  `end` bigint(20) DEFAULT NULL,
  `error` varchar(4096) DEFAULT NULL,
  `errors_count` int(11) NOT NULL,
  `rate` double DEFAULT NULL,
  `start` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phase_details`
--

LOCK TABLES `phase_details` WRITE;
/*!40000 ALTER TABLE `phase_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `phase_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pol_compound_policy_item`
--

DROP TABLE IF EXISTS `pol_compound_policy_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pol_compound_policy_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pol_compound_policy_item`
--

LOCK TABLES `pol_compound_policy_item` WRITE;
/*!40000 ALTER TABLE `pol_compound_policy_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `pol_compound_policy_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pol_compound_policy_item_policy_items`
--

DROP TABLE IF EXISTS `pol_compound_policy_item_policy_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pol_compound_policy_item_policy_items` (
  `pol_compound_policy_item_id` bigint(20) NOT NULL,
  `policy_items_id` bigint(20) NOT NULL,
  PRIMARY KEY (`pol_compound_policy_item_id`,`policy_items_id`),
  KEY `FK_3rth36gni1jx8xou27abvj3uh` (`policy_items_id`),
  CONSTRAINT `FK_3rth36gni1jx8xou27abvj3uh` FOREIGN KEY (`policy_items_id`) REFERENCES `policy_item` (`id`),
  CONSTRAINT `FK_swc4wlxvolxwcw8vds2l496lf` FOREIGN KEY (`pol_compound_policy_item_id`) REFERENCES `pol_compound_policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pol_compound_policy_item_policy_items`
--

LOCK TABLES `pol_compound_policy_item_policy_items` WRITE;
/*!40000 ALTER TABLE `pol_compound_policy_item_policy_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `pol_compound_policy_item_policy_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pol_desired_state_definition`
--

DROP TABLE IF EXISTS `pol_desired_state_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pol_desired_state_definition` (
  `id` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `java_class_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `policy_layer` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pol_desired_state_definition`
--

LOCK TABLES `pol_desired_state_definition` WRITE;
/*!40000 ALTER TABLE `pol_desired_state_definition` DISABLE KEYS */;
INSERT INTO `pol_desired_state_definition` VALUES ('READ_PERMISSIONS_SET','Only the set User/Group has read permissions','com.cla.filecluster.policy.state.readPermissionsSetDesiredState','Only Set User/Group can read',0),('WRITE_PERMISSIONS_SET','Only the set User/Group has write permissions','com.cla.filecluster.policy.state.writePermissionsSetDesiredState','Only Set User/Group can write',0);
/*!40000 ALTER TABLE `pol_desired_state_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_item`
--

DROP TABLE IF EXISTS `policy_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `policy_layer` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_item`
--

LOCK TABLES `policy_item` WRITE;
/*!40000 ALTER TABLE `policy_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_item_action_instances`
--

DROP TABLE IF EXISTS `policy_item_action_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item_action_instances` (
  `policy_item_id` bigint(20) NOT NULL,
  `action_instances_id` bigint(20) NOT NULL,
  PRIMARY KEY (`policy_item_id`,`action_instances_id`),
  KEY `FK_nnh3ap1g8eqb9aheptsbxhpg5` (`action_instances_id`),
  CONSTRAINT `FK_aooglynfuck1et6klwti8jm7l` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`),
  CONSTRAINT `FK_nnh3ap1g8eqb9aheptsbxhpg5` FOREIGN KEY (`action_instances_id`) REFERENCES `action_instance` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_item_action_instances`
--

LOCK TABLES `policy_item_action_instances` WRITE;
/*!40000 ALTER TABLE `policy_item_action_instances` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_item_action_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_item_attachment`
--

DROP TABLE IF EXISTS `policy_item_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `population_filter_json` varchar(255) NOT NULL,
  `policy_item_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_oi5y1swp1sds37lvux6gfx7c` (`policy_item_id`),
  CONSTRAINT `FK_oi5y1swp1sds37lvux6gfx7c` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_item_attachment`
--

LOCK TABLES `policy_item_attachment` WRITE;
/*!40000 ALTER TABLE `policy_item_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_item_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_item_rules`
--

DROP TABLE IF EXISTS `policy_item_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_item_rules` (
  `policy_item_id` bigint(20) NOT NULL,
  `rules_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_1pv38rrequb8it90vbogsn4t` (`rules_id`),
  KEY `FK_avj16pk8vdncyh14x9q4b5msb` (`policy_item_id`),
  CONSTRAINT `FK_1pv38rrequb8it90vbogsn4t` FOREIGN KEY (`rules_id`) REFERENCES `policy_rule_instance` (`id`),
  CONSTRAINT `FK_avj16pk8vdncyh14x9q4b5msb` FOREIGN KEY (`policy_item_id`) REFERENCES `policy_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_item_rules`
--

LOCK TABLES `policy_item_rules` WRITE;
/*!40000 ALTER TABLE `policy_item_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_item_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_rule_instance`
--

DROP TABLE IF EXISTS `policy_rule_instance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `policy_rule_instance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filter` varchar(2048) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_rule_instance`
--

LOCK TABLES `policy_rule_instance` WRITE;
/*!40000 ALTER TABLE `policy_rule_instance` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_rule_instance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pv_analysis_data`
--

DROP TABLE IF EXISTS `pv_analysis_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pv_analysis_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `analysis_meadata` blob,
  `analyze_hint` int(11) NOT NULL,
  `content_id` bigint(20) DEFAULT NULL,
  `pv_collections` longblob,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_id` (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pv_analysis_data`
--

LOCK TABLES `pv_analysis_data` WRITE;
/*!40000 ALTER TABLE `pv_analysis_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `pv_analysis_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_authorities`
--

DROP TABLE IF EXISTS `role_authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_authorities` (
  `role_id` int(11) NOT NULL,
  `authorities` int(11) DEFAULT NULL,
  KEY `FK_6ci0hnfo6cq98pcnei79fkvru` (`role_id`),
  CONSTRAINT `FK_6ci0hnfo6cq98pcnei79fkvru` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_authorities`
--

LOCK TABLES `role_authorities` WRITE;
/*!40000 ALTER TABLE `role_authorities` DISABLE KEYS */;
INSERT INTO `role_authorities` VALUES (1,2),(1,0),(1,1),(2,3),(2,4),(3,6),(3,4),(4,7),(5,4);
/*!40000 ALTER TABLE `role_authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_idx` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Manage accounts','accounts-provisioning','accounts-provisioning'),(2,'Manage users and their roles','users-provisioning','users-provisioning'),(3,'Can operate functional api like scan ingest analyze etc','operator','operator'),(4,'Can activate crawler api','crawler_api','crawler_api'),(5,'Demo showcase','demo-showcase','demo-showcase');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `root_folder`
--

DROP TABLE IF EXISTS `root_folder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `root_folder` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `base_depth` int(11) NOT NULL,
  `description` varchar(4096) DEFAULT NULL,
  `external_crawler_job_id` varchar(255) DEFAULT NULL,
  `extract_biz_lists` bit(1) DEFAULT NULL,
  `file_types` varchar(255) DEFAULT NULL,
  `files_count` int(11) DEFAULT NULL,
  `folders_count` int(11) DEFAULT NULL,
  `last_run_id` bigint(20) DEFAULT NULL,
  `last_run_state` int(11) DEFAULT NULL,
  `media_entity_id` varchar(255) DEFAULT NULL,
  `media_type` int(11) DEFAULT NULL,
  `path` varchar(4096) DEFAULT NULL,
  `path_descriptor_type` varchar(255) DEFAULT NULL,
  `real_path` varchar(4096) DEFAULT NULL,
  `rescan_active` bit(1) NOT NULL,
  `root_folder_state` int(11) DEFAULT NULL,
  `scanned_files_count_cap` int(11) DEFAULT NULL,
  `store_location` int(11) DEFAULT NULL,
  `store_purpose` int(11) DEFAULT NULL,
  `store_rescan_method` int(11) DEFAULT NULL,
  `store_security` int(11) DEFAULT NULL,
  `doc_store_include` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rootFolderState_idx` (`root_folder_state`),
  KEY `mediaType_idx` (`media_type`),
  KEY `FK_4bygybr61432rxkb8ux4w2e7n` (`doc_store_include`),
  KEY `path_idx` (`path`(255)),
  CONSTRAINT `FK_4bygybr61432rxkb8ux4w2e7n` FOREIGN KEY (`doc_store_include`) REFERENCES `doc_store` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `root_folder`
--

LOCK TABLES `root_folder` WRITE;
/*!40000 ALTER TABLE `root_folder` DISABLE KEYS */;
/*!40000 ALTER TABLE `root_folder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `root_folder_schedule`
--

DROP TABLE IF EXISTS `root_folder_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `root_folder_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `number` bigint(20) DEFAULT NULL,
  `root_folder_id` bigint(20) DEFAULT NULL,
  `schedule_group_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `UK_sdoxxobju5onm1nfsvyo4enmx` (`schedule_group_id`,`number`),
  KEY `FK_5npxm2g80cbpr5iupa3odr7ip` (`root_folder_id`),
  CONSTRAINT `FK_5npxm2g80cbpr5iupa3odr7ip` FOREIGN KEY (`root_folder_id`) REFERENCES `root_folder` (`id`),
  CONSTRAINT `FK_qhyjpdoe0r7xns7y1y32uk1ir` FOREIGN KEY (`schedule_group_id`) REFERENCES `schedule_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `root_folder_schedule`
--

LOCK TABLES `root_folder_schedule` WRITE;
/*!40000 ALTER TABLE `root_folder_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `root_folder_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_charts`
--

DROP TABLE IF EXISTS `saved_charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_charts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chart_type` int(11) NOT NULL,
  `chart_display_type` int(11) DEFAULT NULL,
  `hidden` bit(1) NOT NULL,
  `main_entity_type` varchar(255) DEFAULT NULL,
  `max_number_of_entries` int(11) DEFAULT NULL,
  `minimum_count` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `population_json_filter` varchar(2048) DEFAULT NULL,
  `population_json_url` varchar(255) DEFAULT NULL,
  `population_subset_json_filter` varchar(2048) DEFAULT NULL,
  `show_others` bit(1) NOT NULL,
  `show_tablular_view` bit(1) NOT NULL,
  `sort_type` int(11) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jv91ofphv09ci149qg5925i31` (`owner_id`),
  CONSTRAINT `FK_jv91ofphv09ci149qg5925i31` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_charts`
--

LOCK TABLES `saved_charts` WRITE;
/*!40000 ALTER TABLE `saved_charts` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_charts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_filters`
--

DROP TABLE IF EXISTS `saved_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_filters` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `filter_json` varchar(2048) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_idx` (`name`),
  KEY `FK_t4v8kl4r53an4dqivgrv41hre` (`account_id`),
  KEY `FK_lolwslrifr1n5462t8utuguh8` (`owner_id`),
  CONSTRAINT `FK_lolwslrifr1n5462t8utuguh8` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_t4v8kl4r53an4dqivgrv41hre` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_filters`
--

LOCK TABLES `saved_filters` WRITE;
/*!40000 ALTER TABLE `saved_filters` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_filters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_configuration`
--

DROP TABLE IF EXISTS `schedule_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_configuration` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cron_template` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_configuration`
--

LOCK TABLES `schedule_configuration` WRITE;
/*!40000 ALTER TABLE `schedule_configuration` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_group`
--

DROP TABLE IF EXISTS `schedule_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `analyze_phase_behavior` int(11) DEFAULT NULL,
  `cron_trigger_string` varchar(255) DEFAULT NULL,
  `last_run_end_time` bigint(20) NOT NULL,
  `last_run_id` bigint(20) DEFAULT NULL,
  `last_run_start_time` bigint(20) NOT NULL,
  `missed_schedule_time_stamp` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `resume_after_restart` bit(1) NOT NULL,
  `scheduling_description` varchar(255) DEFAULT NULL,
  `scheduling_json` varchar(4096) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_constraint` (`name`),
  KEY `FK_aipjk6b0bsv3dqfiu1ais1wrg` (`account_id`),
  CONSTRAINT `FK_aipjk6b0bsv3dqfiu1ais1wrg` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_group`
--

LOCK TABLES `schedule_group` WRITE;
/*!40000 ALTER TABLE `schedule_group` DISABLE KEYS */;
INSERT INTO `schedule_group` VALUES (1,'\0',1,'0 0 0 * * */1',0,NULL,0,NULL,'default','','Daily at midnight','{\"scheduleType\":\"DAILY\",\"hour\":0,\"minutes\":0,\"every\":1,\"daysOfTheWeek\":null}',1,1);
/*!40000 ALTER TABLE `schedule_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_upgrade_scripts`
--

DROP TABLE IF EXISTS `schema_upgrade_scripts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_upgrade_scripts` (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `execution_time` bigint(20) DEFAULT NULL,
  `installed_on` bigint(20) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `upgrade_state` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_upgrade_scripts`
--

LOCK TABLES `schema_upgrade_scripts` WRITE;
/*!40000 ALTER TABLE `schema_upgrade_scripts` DISABLE KEYS */;
INSERT INTO `schema_upgrade_scripts` VALUES (0,'post_baseline',1471,1476771763678,'SQL','SUCCESS'),(42,'user_group_indexes',0,1476771762112,'SQL','SKIPPED'),(43,'license_repo_and_indexes',0,1476771762123,'SQL','SKIPPED'),(44,'rootFolder_field',0,1476771762128,'SQL','SKIPPED'),(45,'user_fields',0,1476771762136,'SQL','SKIPPED'),(46,'user_access_tokens',0,1476771762142,'SQL','SKIPPED'),(47,'trendcharts',0,1476771762147,'SQL','SKIPPED'),(48,'trendcharts2',0,1476771762153,'SQL','SKIPPED'),(49,'trendcharts3',0,1476771762158,'SQL','SKIPPED'),(50,'trendcharts4',0,1476771762164,'SQL','SKIPPED'),(51,'trendcharts5',0,1476771762169,'SQL','SKIPPED'),(52,'trendcharts6',0,1476771762173,'SQL','SKIPPED'),(53,'bizroles',0,1476771762178,'SQL','SKIPPED'),(55,'bizroles2',0,1476771762195,'SQL','SKIPPED'),(56,'systemroles_display',0,1476771762200,'SQL','SKIPPED');
/*!40000 ALTER TABLE `schema_upgrade_scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simple_date_range_item`
--

DROP TABLE IF EXISTS `simple_date_range_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simple_date_range_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `end_absolute_time` datetime DEFAULT NULL,
  `end_relative_period` int(11) DEFAULT NULL,
  `end_relative_period_amount` int(11) DEFAULT NULL,
  `end_type` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `order_value` bigint(20) DEFAULT NULL,
  `start_absolute_time` datetime DEFAULT NULL,
  `start_relative_period` int(11) DEFAULT NULL,
  `start_relative_period_amount` int(11) DEFAULT NULL,
  `start_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simple_date_range_item`
--

LOCK TABLES `simple_date_range_item` WRITE;
/*!40000 ALTER TABLE `simple_date_range_item` DISABLE KEYS */;
INSERT INTO `simple_date_range_item` VALUES (1,NULL,NULL,NULL,NULL,'Last month',0,NULL,4,1,1),(2,NULL,4,1,1,'1-3 months',1,NULL,4,3,1),(3,NULL,4,3,1,'3-6 months',2,NULL,4,6,1),(4,NULL,4,6,1,'6-12 months',3,NULL,4,12,1),(5,NULL,4,12,1,'1-5 years',4,NULL,5,5,1),(6,NULL,5,5,1,'Older',5,NULL,5,99,1);
/*!40000 ALTER TABLE `simple_date_range_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subtree`
--

DROP TABLE IF EXISTS `subtree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subtree` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `doc_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doc_folder_id_idx` (`doc_folder_id`),
  KEY `type_idx` (`type`),
  KEY `FK_slts4ju3vqmig2q0pthl7r94t` (`account_id`),
  CONSTRAINT `FK_pj3yujop8thbi30c65lo663cq` FOREIGN KEY (`doc_folder_id`) REFERENCES `doc_folder` (`id`),
  CONSTRAINT `FK_slts4ju3vqmig2q0pthl7r94t` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subtree`
--

LOCK TABLES `subtree` WRITE;
/*!40000 ALTER TABLE `subtree` DISABLE KEYS */;
/*!40000 ALTER TABLE `subtree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_search_pattern`
--

DROP TABLE IF EXISTS `text_search_pattern`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_search_pattern` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pattern` varchar(255) DEFAULT NULL,
  `pattern_type` int(11) DEFAULT NULL,
  `validator_class` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_search_pattern`
--

LOCK TABLES `text_search_pattern` WRITE;
/*!40000 ALTER TABLE `text_search_pattern` DISABLE KEYS */;
INSERT INTO `text_search_pattern` VALUES (1,'\0','Search for emails','email','\\b[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})\\b',1,'com.cla.filecluster.domain.dto.filesearchpatterns.EmailTextSearchPattern'),(2,'\0','Search for emails sample pattern','emailPattern','\\\\b[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})\\\\b',0,NULL),(3,'','Social Security Number','ssn','\\b\\d{3}([\\- ])\\d{2}\\1\\d{4}\\b',1,'com.cla.filecluster.domain.dto.filesearchpatterns.SSNTextSearchPattern'),(4,'\0','Israeli ID number','israeli-id','\\b\\d{5,9}\\b',1,'com.cla.filecluster.domain.dto.filesearchpatterns.IsraeliIdTextSearchPattern'),(5,'','Valid credit card numbers','credit-card-valid','(?:[^.\\-\\w])(?:3[47]\\d{2}([\\ \\-]?)\\d{6}\\1\\d|(?:(?:4\\d|5[1-5]|65)\\d{2}|6011)([\\ \\-]?)\\d{4}\\2\\d{4}\\2)\\d{4}(?![\\-\\.\\w])',1,'com.cla.filecluster.domain.dto.filesearchpatterns.GenericCreditCardTextSearchPattern'),(6,'','Invalid credit card numbers','credit-card-invalid','(?:[^.\\-\\w])(?:3[47]\\d{2}([\\ \\-]?)\\d{6}\\1\\d|(?:(?:4\\d|5[1-5]|65)\\d{2}|6011)([\\ \\-]?)\\d{4}\\2\\d{4}\\2)\\d{4}(?![\\-\\.\\w])',1,'com.cla.filecluster.domain.dto.filesearchpatterns.GenericCreditCardBadLuhnTextSearchPattern'),(7,'','IPV4 Addresses','ipv4-addr','(?:[^.\\\\-\\\\w])(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?![\\\\-\\\\.\\\\w])',0,NULL),(8,'','IPV6 Addresses','ipv6-addr','(?<![:\\\\.\\\\w])(?:[A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}(?![:\\\\.\\\\w])',0,NULL);
/*!40000 ALTER TABLE `text_search_pattern` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trend_chart`
--

DROP TABLE IF EXISTS `trend_chart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_chart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chart_display_type` int(11) DEFAULT NULL,
  `chart_value_format_type` int(11) DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `filters` varchar(4096) DEFAULT NULL,
  `hidden` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `resolution_type` int(11) DEFAULT NULL,
  `series_type` varchar(255) DEFAULT NULL,
  `sort_type` int(11) DEFAULT NULL,
  `transformer_script` varchar(4096) DEFAULT NULL,
  `trend_chart_collector_id` bigint(20) DEFAULT NULL,
  `value_type` varchar(255) DEFAULT NULL,
  `view_configuration` varchar(2048) DEFAULT NULL,
  `start_absolute_time` datetime DEFAULT NULL,
  `start_relative_period` int(11) DEFAULT NULL,
  `start_relative_period_amount` int(11) DEFAULT NULL,
  `start_type` int(11) DEFAULT NULL,
  `view_offset` int(11) DEFAULT NULL,
  `view_resolution_count` int(11) DEFAULT NULL,
  `view_resolution_ms` bigint(20) DEFAULT NULL,
  `view_resolution_step` int(11) DEFAULT NULL,
  `view_sample_count` int(11) DEFAULT NULL,
  `end_absolute_time` datetime DEFAULT NULL,
  `end_relative_period` int(11) DEFAULT NULL,
  `end_relative_period_amount` int(11) DEFAULT NULL,
  `end_type` int(11) DEFAULT NULL,
  `owner_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_eq9b62ybcl8y47n2kftwd92xg` (`owner_id`),
  CONSTRAINT `FK_eq9b62ybcl8y47n2kftwd92xg` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trend_chart`
--

LOCK TABLES `trend_chart` WRITE;
/*!40000 ALTER TABLE `trend_chart` DISABLE KEYS */;
INSERT INTO `trend_chart` VALUES (1,5,0,'\0','[{\'name\':\'total\'},{\'name\':\'dtAny\',\'field\':\'docTypeId\',\'value\':\'*\',\'operator\':\'eq\'},{\'name\':\'tt01\',\'field\':\'tagType\',\'value\':1,\'operator\':\'eq\'},{\'name\':\'tt02\',\'field\':\'tagType\',\'value\':2,\'operator\':\'eq\'},{\'name\':\'tt03\',\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'},{\'name\':\'tt04\',\'field\':\'tagType\',\'value\':4,\'operator\':\'eq\'},{\'name\':\'tt05\',\'field\':\'tagType\',\'value\':5,\'operator\':\'eq\'},{\'name\':\'tt06\',\'field\':\'tagType\',\'value\':6,\'operator\':\'eq\'},{\'name\':\'tt07\',\'field\':\'tagType\',\'value\':7,\'operator\':\'eq\'},{\'name\':\'tt08\',\'field\':\'tagType\',\'value\':8,\'operator\':\'eq\'},{\'name\':\'tt09\',\'field\':\'tagType\',\'value\':9,\'operator\':\'eq\'},{\'name\':\'tt10\',\'field\':\'tagType\',\'value\':10,\'operator\':\'eq\'},{\'name\':\'tt11\',\'field\':\'tagType\',\'value\':11,\'operator\':\'eq\'},{\'name\':\'tt12\',\'field\':\'tagType\',\'value\':12,\'operator\':\'eq\'},{\'name\':\'tt13\',\'field\':\'tagType\',\'value\':13,\'operator\':\'eq\'},{\'name\':\'tt14\',\'field\':\'tagType\',\'value\':14,\'operator\':\'eq\'},{\'name\':\'tt15\',\'field\':\'tagType\',\'value\':15,\'operator\':\'eq\'},{\'name\':\'tt16\',\'field\':\'tagType\',\'value\':16,\'operator\':\'eq\'},{\'name\':\'tt17\',\'field\':\'tagType\',\'value\':17,\'operator\':\'eq\'},{\'name\':\'tt18\',\'field\':\'tagType\',\'value\':18,\'operator\':\'eq\'},{\'name\':\'tt19\',\'field\':\'tagType\',\'value\':19,\'operator\':\'eq\'},{\'name\':\'tt20\',\'field\':\'tagType\',\'value\':20,\'operator\':\'eq\'}]','','TagTypes DocType collector',NULL,'file.id',1,NULL,NULL,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,5,0,'\0','[{\'name\':\'total\'},{\'name\':\'ID\', \'field\':\'docTypeId\', \'value\':\'*\', \'operator\':\'eq\'},{\'name\':\'ID_AND_CLASS\',\'logic\':\'and\',\'filters\':[{\'field\':\'docTypeId\', \'value\':\'*\', \'operator\':\'eq\'},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':2},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':3}]},{\'name\':\'ID_AND_CLASS_AND_MIT\',\'logic\':\'and\',\'filters\':[{\'field\':\'docTypeId\',\'value\':\'*\',\'operator\':\'eq\'},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':2},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':3},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':17}]},{\'name\':\'ID_AND_CLASS_AND_MIT_AND_REMED\', \'logic\':\'and\',\'filters\':[{\'field\':\'docTypeId\', \'value\':\'*\', \'operator\':\'eq\'},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':2},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':3},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':17},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':14},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':15},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':16}]},{\'name\':\'CLASS\', \'logic\':\'and\',\'filters\':[{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':2},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':3}]},{\'name\':\'CLASS_and_MIT\', \'logic\':\'and\',\'filters\':[{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':2},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':3},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':17}]},{\'name\':\'CLASS_and_MIT_AND_REMED\', \'logic\':\'and\',\'filters\':[{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':2},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':3},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':17},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':14},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':15},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':16}]},{\'name\':\'MIT\', \'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':17},{\'name\':\'MIT_and_REMED\', \'logic\':\'and\',\'filters\':[{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':17},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':14},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':15},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':16}]},{\'name\':\'REMED\', \'logic\':\'and\',\'filters\':[{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':14},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':15},{\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':16}]},{\'name\':\'DLP\', \'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':14},{\'name\':\'GOVERNANCE\',\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':15},{\'name\':\'ENCRYPTION\',\'field\':\'file.tagType\',\'operator\':\'eq\',\'value\':16}]','','Risk Reduction collector',NULL,'file.id',1,NULL,NULL,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(3,5,0,'\0','[{\'name\':\'total\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'}]},{\'name\':\'dtAny\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'docTypeId\',\'value\':\'*\',\'operator\':\'eq\'}]},{\'name\':\'tt02\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':2,\'operator\':\'eq\'}]},{\'name\':\'tt03\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt04\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':4,\'operator\':\'eq\'}]},{\'name\':\'tt13\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt14\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt15\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt16\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt17\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'8\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'8.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]}]','','Department Residual Risk collector',NULL,'file.tags',1,NULL,NULL,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(4,5,0,'\0','[{\'name\':\'total\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'}]},{\'name\':\'dtAny\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'docTypeId\',\'value\':\'*\',\'operator\':\'eq\'}]},{\'name\':\'tt02\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':2,\'operator\':\'eq\'}]},{\'name\':\'tt03\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt04\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':4,\'operator\':\'eq\'}]},{\'name\':\'tt13\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt14\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt15\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt16\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]},{\'name\':\'tt17\',\'logic\':\'and\',\'filters\':[{\'field\':\'tagType\',\'value\':\'4\',\'operator\':\'eq\'},{\'field\':\'facetPrefix\',\'value\':\'4.\',\'operator\':\'eq\'},{\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'}]}]','','Regulation Residual Risk collector',NULL,'file.tags',1,NULL,NULL,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(5,5,0,'\0','[{\'name\':\'total\'},{\'name\':\'riskType\',\'field\':\'tagType\', \'value\':2, \'operator\':\'eq\'},{\'name\':\'riskLevel\',\'field\':\'tagType\',\'value\':3,\'operator\':\'eq\'} ]','','Risk Trend',NULL,'file.id',1,NULL,NULL,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(6,5,0,'\0','[{\'name\':\'total\'},{\'name\':\'With DocType\',\'field\':\'docTypeId\',\'value\':\'*\',\'operator\':\'eq\'}]','','Doc Types',NULL,'file.id',1,NULL,NULL,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(7,5,0,'\0',NULL,'\0','Risk assignment',NULL,'file.id',1,'output.type = input.tt02; output.level = input.tt03; output.total = input.total;',1,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(8,5,0,'\0',NULL,'\0','DocType Assignment Ratio',NULL,'file.id',1,'output.ratio = Math.round(10000*input.dtAny/(input.total+1))/100; output.total = input.total;',1,'files',NULL,NULL,2,7,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(9,5,0,'\0',NULL,'\0','Residual Risk [%]',NULL,'file.id',1,'output.ratio = 100 - Math.min(100,Math.round((10*input.dtAny + 10*input.tt02 + 10*input.tt03 + 10*input.tt04 + 10*input.tt13 + 10*input.tt14 + 10*input.tt15 + 10*input.tt16 + 20*input.tt17) / (input.total+1))) ;',1,'files',NULL,NULL,2,14,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(10,5,0,'\0',NULL,'\0','Risk Reduction Tasks Status',NULL,'file.id',1,'output.Identification = input.ID - input.ID_AND_CLASS - input.ID_AND_CLASS_AND_MIT - input.ID_AND_CLASS_AND_MIT_AND_REMED; output.Classification = input.CLASS - input.CLASS_and_MIT - input.CLASS_and_MIT_AND_REMED; output.Mitigation = input.MIT - input.MIT_and_REMED; output.Remediation = input.REMED;',2,'files',NULL,NULL,2,14,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(11,5,0,'\0',NULL,'\0','Remediation Status',NULL,'file.id',1,'output.DLP = input.DLP; output.Governance = input.GOVERNANCE; output.Encryption = input.ENCRYPTION; output.total = input.total; ',2,'files',NULL,NULL,2,14,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(12,5,0,'\0',NULL,'\0','Residual Risk [%] by Department',NULL,'file.tags',1,'output.ratios={}; for (dep in input.total) {output.ratios[dep] = 100 - Math.min(100,Math.round((10*(input.dtAny[dep]||0) + 10*(input.tt02[dep]||0) + 10*(input.tt03[dep]||0) + 10*(input.tt04[dep]||0) + 10*(input.tt13[dep]||0) + 10*(input.tt14[dep]||0) + 10*(input.tt15[dep]||0) + 10*(input.tt16[dep]||0) + 20*(input.tt17[dep]||0)) / (input.total[dep]+1)));}',3,'files',NULL,NULL,2,14,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1),(13,5,0,'\0',NULL,'\0','Residual Risk [%] by Regulation',NULL,'file.tags',1,'output.ratios={}; for (reg in input.total) {output.ratios[reg] = 100 - Math.min(100,Math.round((10*(input.dtAny[reg]||0) + 10*(input.tt02[reg]||0) + 10*(input.tt03[reg]||0) + 10*(input.tt04[reg]||0) + 10*(input.tt13[reg]||0) + 10*(input.tt14[reg]||0) + 10*(input.tt15[reg]||0) + 10*(input.tt16[reg]||0) + 20*(input.tt17[reg]||0)) / (input.total[reg]+1)));}',4,'files',NULL,NULL,2,14,1,NULL,NULL,86400000,NULL,NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `trend_chart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trend_chart_data`
--

DROP TABLE IF EXISTS `trend_chart_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_chart_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actual_time_stamp` bigint(20) NOT NULL,
  `requested_time_stamp` bigint(20) NOT NULL,
  `value` varchar(4096) DEFAULT NULL,
  `view_resolution_ms` bigint(20) DEFAULT NULL,
  `trend_chart_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tc_ts_idx` (`trend_chart_id`,`requested_time_stamp`),
  CONSTRAINT `FK_ogspcp18nf7yh7l9dqtns8ro5` FOREIGN KEY (`trend_chart_id`) REFERENCES `trend_chart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trend_chart_data`
--

LOCK TABLES `trend_chart_data` WRITE;
/*!40000 ALTER TABLE `trend_chart_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `trend_chart_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trend_chart_schedule`
--

DROP TABLE IF EXISTS `trend_chart_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trend_chart_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) NOT NULL,
  `cron_trigger_string` varchar(255) DEFAULT NULL,
  `last_requested_time_stamp` bigint(20) DEFAULT NULL,
  `last_run_end_time` bigint(20) NOT NULL,
  `last_run_start_time` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_o0ib9qwcigusyj5il7h78q5iy` (`account_id`),
  CONSTRAINT `FK_o0ib9qwcigusyj5il7h78q5iy` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trend_chart_schedule`
--

LOCK TABLES `trend_chart_schedule` WRITE;
/*!40000 ALTER TABLE `trend_chart_schedule` DISABLE KEYS */;
INSERT INTO `trend_chart_schedule` VALUES (1,'','0 5 * * * *',NULL,0,0,'default',1);
/*!40000 ALTER TABLE `trend_chart_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_access_tokens`
--

DROP TABLE IF EXISTS `user_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_access_tokens` (
  `user_id` bigint(20) NOT NULL,
  `access_tokens` varchar(255) DEFAULT NULL,
  KEY `FK_j9rx80fwoyi35tyl1lsjlu57b` (`user_id`),
  CONSTRAINT `FK_j9rx80fwoyi35tyl1lsjlu57b` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_access_tokens`
--

LOCK TABLES `user_access_tokens` WRITE;
/*!40000 ALTER TABLE `user_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_saved_data`
--

DROP TABLE IF EXISTS `user_saved_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_saved_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `data` longtext,
  `user_key` char(190) DEFAULT NULL,
  `user` char(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_saved_data`
--

LOCK TABLES `user_saved_data` WRITE;
/*!40000 ALTER TABLE `user_saved_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_saved_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_password_change` datetime(3) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `password_must_be_changed` bit(1) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `user_role` int(11) DEFAULT NULL,
  `user_state` int(11) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_idx` (`username`),
  KEY `type_idx` (`user_type`),
  KEY `deleted_idx` (`deleted`),
  KEY `account_type_idx` (`account_id`,`user_type`),
  CONSTRAINT `FK_1yov8c5ew74vlt8qra6cewq99` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'\0',NULL,'2016-10-18 06:22:25.866','Joe Smith','$2a$10$R7dBkAKl78Mp0xx2IaoHueUG1SOq4JiwvnfNT5LF8nYO2Ue0Z794m','\0','admin',1,0,0,'aa',1),(2,'\0',NULL,'2016-10-18 06:22:26.059','scheduleUser','$2a$10$w3YdKOvZxhiBNoJ1xAAMpOUriHdkjromhkTUmyrgpcgajTSxk75dm','\0','operator',1,0,1,'scheduleInternalUser',1),(3,'\0',NULL,'2016-10-18 06:22:26.225','mcfUser','$2a$10$EFIhCsSsMoU.rzrINyNXvug.Z6Zz.vAiHqEYXXGImodHrW.r.IGqu','\0','operator',1,0,1,'mcf',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_biz_roles`
--

DROP TABLE IF EXISTS `users_biz_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_biz_roles` (
  `users_id` bigint(20) NOT NULL,
  `biz_roles_id` bigint(20) NOT NULL,
  PRIMARY KEY (`users_id`,`biz_roles_id`),
  KEY `FK_3klo7bc2fjwve8is5nm0tmxqi` (`biz_roles_id`),
  CONSTRAINT `FK_3klo7bc2fjwve8is5nm0tmxqi` FOREIGN KEY (`biz_roles_id`) REFERENCES `biz_role` (`id`),
  CONSTRAINT `FK_6u7beig0ujcpybqrp8rn5l07u` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_biz_roles`
--

LOCK TABLES `users_biz_roles` WRITE;
/*!40000 ALTER TABLE `users_biz_roles` DISABLE KEYS */;
INSERT INTO `users_biz_roles` VALUES (1,1),(2,2),(3,2);
/*!40000 ALTER TABLE `users_biz_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `users_id` bigint(20) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`users_id`,`roles_id`),
  KEY `FK_60loxav507l5mreo05v0im1lq` (`roles_id`),
  CONSTRAINT `FK_3b2cl2u4ck187o21r4uhp6psv` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`),
  CONSTRAINT `FK_60loxav507l5mreo05v0im1lq` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-18  9:50:05
