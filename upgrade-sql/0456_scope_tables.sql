CREATE TABLE `scope` (
	`type` VARCHAR(31) NOT NULL,
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`description` VARCHAR(255) NULL DEFAULT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	`user_id` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `us_user_id_idx` (`user_id`),
	UNIQUE INDEX `wgs_name_idx` (`name`),
	CONSTRAINT `fk_us_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `work_groups` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`description` VARCHAR(255) NULL DEFAULT NULL,
	`name` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `wg_name_idx` (`name`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `work_groups_scope_work_groups` (
	`work_groups_scope_id` BIGINT(20) NOT NULL,
	`work_group_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`work_groups_scope_id`, `work_group_id`),
	UNIQUE INDEX `wgswg_work_group_id_idx` (`work_group_id`),
	CONSTRAINT `fk_wgswg_work_group_id` FOREIGN KEY (`work_group_id`) REFERENCES `work_groups` (`id`),
	CONSTRAINT `fk_wgswg_work_groups_scope_id` FOREIGN KEY (`work_groups_scope_id`) REFERENCES `scope` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `users_work_groups` (
	`user_id` BIGINT(20) NOT NULL,
	`work_group_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`user_id`, `work_group_id`),
	INDEX `uwg_work_group_id_idx` (`work_group_id`),
	CONSTRAINT `fk_uwg_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
	CONSTRAINT `fk_uwg_work_group_id` FOREIGN KEY (`work_group_id`) REFERENCES `work_groups` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

ALTER TABLE `file_tag_type`
	ADD COLUMN `tag_type_scope_id` BIGINT NULL DEFAULT NULL,
	ADD COLUMN `single_value_tag`  BIT(1) NOT NULL DEFAULT 0;

ALTER TABLE `doc_type`
	ADD COLUMN `doc_type_scope_id` BIGINT NULL DEFAULT NULL,
	ADD COLUMN `single_value`  BIT(1) NOT NULL DEFAULT 0;

ALTER TABLE `cla_file_tag_association`
	ADD COLUMN `tag_association_scope_id` BIGINT(20) NULL DEFAULT NULL;

ALTER TABLE `folder_tag_association`
   	ADD COLUMN `tag_association_scope_id` BIGINT(20) NULL DEFAULT NULL;

ALTER TABLE `business_id_tag_association`
    ADD COLUMN `tag_association_scope_id` BIGINT(20) NULL DEFAULT NULL;

ALTER TABLE `doc_type_association`
    ADD COLUMN `doc_type_association_scope_id` BIGINT(20) NULL DEFAULT NULL;