create TABLE tmp_wg_to_sc select * from work_groups_scope_work_groups;
drop table IF EXISTS work_groups_scope_work_groups;
CREATE TABLE IF NOT EXISTS `work_groups_scope_work_groups` (
	`work_groups_scope_id` BIGINT(20) NOT NULL,
	`work_group_id` BIGINT(20) NOT NULL,
	PRIMARY KEY (`work_groups_scope_id`, `work_group_id`),
	INDEX `wgswg_work_group_id_idx` (`work_group_id`),
	CONSTRAINT `fk_wgswg_work_group_id` FOREIGN KEY (`work_group_id`) REFERENCES `work_groups` (`id`),
	CONSTRAINT `fk_wgswg_work_groups_scope_id` FOREIGN KEY (`work_groups_scope_id`) REFERENCES `scope` (`id`)
);


INSERT INTO work_groups_scope_work_groups (work_groups_scope_id, work_group_id)
select * from tmp_wg_to_sc;
drop table tmp_wg_to_sc;

