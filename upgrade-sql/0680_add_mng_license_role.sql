INSERT INTO `roles` VALUES
     (42,'Validate and update license key','Manage license','MngLicense')
;


INSERT INTO `role_authorities` VALUES
   (42,41)
   ;

insert into template_roles
select template_id, 42 from template_roles where role_id in (
select id from roles where name='GeneralAdmin');
