CREATE TABLE `system_settings` (
  `id` bigint(20) NOT NULL,
  `created_time_stamp` bigint(20) DEFAULT NULL,
  `modified_time_stamp` bigint(20) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_index` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into system_settings (id, name, value, created_time_stamp, modified_time_stamp)
select 100, u.user_key, u.`data`, now(), now() from `user_saved_data` u where u.user_key = 'emailAddresses' and
 NOT EXISTS (select * from system_settings where name = 'emailAddresses')
;

insert into system_settings (id, name, value, created_time_stamp, modified_time_stamp)
select 101, u.user_key, u.`data`, now(), now() from `user_saved_data` u where u.user_key = 'smtpConfiguration' and
 NOT EXISTS (select * from system_settings where name = 'smtpConfiguration')
;

insert into system_settings (id, name, value, created_time_stamp, modified_time_stamp)
select 102, u.user_key, u.`data`, now(), now() from `user_saved_data` u where u.user_key = 'sendEmailReportEnabled' and
 NOT EXISTS (select * from system_settings where name = 'sendEmailReportEnabled')
;
