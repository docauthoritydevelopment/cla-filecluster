TRUNCATE TABLE saved_filters;

ALTER TABLE `saved_filters`
    DROP foreign key `FKm71rfktl6njuyuijxg8fo70vl`,
    DROP INDEX `name_idx`,
    DROP COLUMN `filter_json`;

alter table saved_filters add column `filter_descriptor` JSON NULL DEFAULT NULL;

alter table saved_filters add column `raw_filter` VARCHAR(2048) NULL DEFAULT NULL;

alter table saved_filters add column `scope_id` BIGINT(20) NULL DEFAULT NULL;

alter table saved_filters
    ADD UNIQUE INDEX `name_idx` (`name`),
    ADD INDEX `FK45x8ydttrskd93le4tkw86bg6` (`scope_id`),
    ADD CONSTRAINT `FK45x8ydttrskd93le4tkw86bg6` FOREIGN KEY (`scope_id`) REFERENCES `scope` (`id`);

CREATE TABLE `user_view_data` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`display_data` VARCHAR(2048) NULL DEFAULT NULL,
	`display_type` VARCHAR(255) NULL DEFAULT NULL,
	`name` VARCHAR(255) NOT NULL,
	`filter_id` BIGINT(20) NULL DEFAULT NULL,
	`scope_id` BIGINT(20) NULL DEFAULT NULL,
	`owner_id` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FKi24qc3orov34yugmwyixig6gd` (`filter_id`),
	INDEX `FKmig1qk1wlypbitls8my2v5l9n` (`scope_id`),
	CONSTRAINT `FKi24qc3orov34yugmwyixig6gd` FOREIGN KEY (`filter_id`) REFERENCES `saved_filters` (`id`),
	CONSTRAINT `FKmig1qk1wlypbitls8my2v5l9n` FOREIGN KEY (`scope_id`) REFERENCES `scope` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
