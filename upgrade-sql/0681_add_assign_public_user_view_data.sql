INSERT INTO `roles` VALUES
     (43,'Assign filter/chart/dashboard widget as public','Assign public user view data','AssignPublicUserViewData')
;


INSERT INTO `role_authorities` VALUES
   (43,42)
   ;

insert into template_roles
select template_id, 43 from template_roles where role_id in (
select id from roles where name='AssignDepartment')
and template_id not in (select template_id from template_roles where role_id in (
select id from roles where name='TechSupport')) ;