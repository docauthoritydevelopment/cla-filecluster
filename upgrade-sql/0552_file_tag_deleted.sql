ALTER TABLE `file_tag` ADD COLUMN `deleted` BIT(1) NULL DEFAULT b'0';

ALTER TABLE `file_tag_type` ADD COLUMN `deleted` BIT(1) NULL DEFAULT b'0';

