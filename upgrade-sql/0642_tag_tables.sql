CREATE TABLE `tag_association` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`association_object_id` VARCHAR(255) NULL DEFAULT NULL,
	`association_time_stamp` BIGINT(20) NOT NULL,
	`association_type` INT(11) NULL DEFAULT NULL,
	`deleted` BIT(1) NOT NULL,
	`dirty` BIT(1) NOT NULL,
	`implicit` BIT(1) NOT NULL,
	`origin_depth_from_root` INT(11) NULL DEFAULT NULL,
	`origin_folder_id` BIGINT(20) NULL DEFAULT NULL,
	`doc_type_id` BIGINT(20) NULL DEFAULT NULL,
	`file_tag_id` BIGINT(20) NULL DEFAULT NULL,
	`functional_role_id` BIGINT(20) NULL DEFAULT NULL,
	`tag_association_scope_id` BIGINT(20) NULL DEFAULT NULL,
	`simple_biz_list_item_sid` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK5gxhk29d8kn04wc5qg642mmk5` (`doc_type_id`),
	INDEX `FK11ckvk0sbpx9knx6b5lc3qiv1` (`file_tag_id`),
	INDEX `FK13nh9de7ko6foteg6r0ufxqt5` (`functional_role_id`),
	INDEX `FK34k2gg9i0wbgvtodsj6pa1qls` (`tag_association_scope_id`),
	INDEX `FK996c2t120r4js2ldvgeb0l7q2` (`simple_biz_list_item_sid`),
	INDEX `FKqtvbh0gppxhskmf6uhefciwfw` (`association_type`, `association_object_id`),
	CONSTRAINT `FK11ckvk0sbpx9knx6b5lc3qiv1` FOREIGN KEY (`file_tag_id`) REFERENCES `file_tag` (`id`),
	CONSTRAINT `FK13nh9de7ko6foteg6r0ufxqt5` FOREIGN KEY (`functional_role_id`) REFERENCES `comp_role` (`id`),
	CONSTRAINT `FK34k2gg9i0wbgvtodsj6pa1qls` FOREIGN KEY (`tag_association_scope_id`) REFERENCES `scope` (`id`),
	CONSTRAINT `FK5gxhk29d8kn04wc5qg642mmk5` FOREIGN KEY (`doc_type_id`) REFERENCES `doc_type` (`id`),
	CONSTRAINT `FK996c2t120r4js2ldvgeb0l7q2` FOREIGN KEY (`simple_biz_list_item_sid`) REFERENCES `bl_item_simple` (`sid`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

insert into tag_association (association_time_stamp, association_object_id,association_type,deleted,dirty,implicit,doc_type_id,file_tag_id,functional_role_id,tag_association_scope_id,simple_biz_list_item_sid)
select association_time_stamp,file_group_id,2,deleted,solr_dirty,0,doc_type_id,file_tag_id,functional_role_id,tag_association_scope_id,simple_biz_list_item_sid from business_id_tag_association;









