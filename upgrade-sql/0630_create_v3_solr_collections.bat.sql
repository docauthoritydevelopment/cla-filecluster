REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster

pushd ..
SET LOGF=installation_logs\%0.log
REM TXT files created by Installer before upgrade scripts are activated
IF NOT EXIST "zk-address.txt" (
  echo ERROR: file zk-address.txt does not exist
  goto finally
)
IF NOT EXIST "num-shards.txt" (
  echo ERROR: file num-shards.txt does not exist
  goto finally
)
FOR /f "delims=" %%x IN (zk-address.txt) DO SET ZK_ADDESS=%%x
FOR /f "delims=" %%x IN (num-shards.txt) DO SET SHARDS=%%x
ECHO Creating new collections with %SHARDS% using ZK_ADDESS %ZK_ADDESS%
call solr_cloud_config.bat zk-create-collection-if-needed %ZK_ADDESS% BizList %SHARDS%
call solr_cloud_config.bat zk-create-collection-if-needed %ZK_ADDESS% UnmatchedContent %SHARDS%
call solr_cloud_config.bat zk-create-collection-if-needed %ZK_ADDESS% CatFolders %SHARDS%
popd
goto finally

:usage
echo Usage:
echo        %0
goto finally

:finally
exit /b 0
