alter table root_folder ADD COLUMN `is_accessible` INT(11) NULL DEFAULT NULL;
alter table root_folder ADD COLUMN `accessability_last_cng_date` BIGINT(20) NULL DEFAULT NULL;
alter table root_folder ADD COLUMN `is_first_scan` BIT(1) NULL DEFAULT NULL;

update root_folder set is_first_scan = 1 where last_run_id is null;
update root_folder set is_first_scan = 0 where last_run_id is not null;

update root_folder set is_accessible = 2 where is_accessible is null;