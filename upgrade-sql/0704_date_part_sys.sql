ALTER TABLE `date_range_partition`
	ADD COLUMN `is_system` BIT(1) NOT NULL;

update date_range_partition set is_system = 1 where name = 'default';