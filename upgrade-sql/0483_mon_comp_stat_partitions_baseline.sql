truncate table mon_component_status;

-- add time_stamp to primary key so we can partition by it
ALTER TABLE mon_component_status DROP PRIMARY KEY, add PRIMARY KEY(`id`, `timestamp`);

-- create partition by timestamp and the first partition
set @partition_date = UNIX_TIMESTAMP(DATE_ADD(CURDATE(), INTERVAL 1 DAY))*1000;
set @alter_p := concat('ALTER TABLE mon_component_status PARTITION BY RANGE (`timestamp`) (PARTITION p0 VALUES LESS THAN(',@partition_date,') ENGINE=InnoDB)');
prepare stmnt from @alter_p;
execute stmnt;