ALTER TABLE `comp_role`
	DROP INDEX `name_idx`,
	ADD UNIQUE INDEX `name_idx` (`name`, `type`),
	DROP INDEX `displayname_idx`,
	ADD UNIQUE INDEX `displayname_idx` (`display_name`, `type`);

