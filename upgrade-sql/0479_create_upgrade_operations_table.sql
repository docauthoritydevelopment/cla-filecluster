CREATE TABLE `upgrade_op_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` json DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `state` varchar(255) NOT NULL,
  `last_state_time` bigint(20) DEFAULT NULL,
  `upgrade_step` int(11) DEFAULT NULL,
  `upgrade_version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
