INSERT INTO `file_tag_type` (`description`, `hidden`, `name`, `sensitive_tag`, `single_value_tag`, `system`) VALUES ('Indicates processing error on a file', b'1', 'Processing error', b'0', b'0', b'1');
select @tag_type_id  :=  tty.id  from `file_tag_type` tty where tty.name = 'Processing Error';
select @owner_id  :=  u.id  from `users` u where u.role_name = 'admin';
INSERT INTO `file_tag`
    (`action`,`identifier`,`name`,`type_id`,`owner_id`)
VALUES
    (0,'unknown_Processing Error','unknown',@tag_type_id,@owner_id),
    (0,'logic_Processing Error','logic',@tag_type_id,@owner_id),
    (0,'encrypted_Processing Error','encrypted',@tag_type_id,@owner_id),
    (0,'corrupted_Processing Error','corrupted',@tag_type_id,@owner_id),
    (0,'old-format_Processing Error','old-format',@tag_type_id,@owner_id),
    (0,'wrong-extension_Processing Error','wrong-extension',@tag_type_id,@owner_id),
    (0,'i/o_Processing Error','i/o',@tag_type_id,@owner_id),
    (0,'strict-open-xml_Processing Error','strict-open-xml',@tag_type_id,@owner_id),
    (0,'limit-exceeded_Processing Error','limit-exceeded',@tag_type_id,@owner_id),
    (0,'indexing_Processing Error','indexing',@tag_type_id,@owner_id),
    (0,'update-properties_Processing Error','update-properties',@tag_type_id,@owner_id),
    (0,'update-acl_Processing Error','update-acl',@tag_type_id,@owner_id),
    (0,'file-not-found_Processing Error','file-not-found',@tag_type_id,@owner_id),
    (0,'too-many-requests_Processing Error','too-many-requests',@tag_type_id,@owner_id),
    (0,'suspect-poison_Processing Error','suspect-poison',@tag_type_id,@owner_id),
    (0,'package-crawl-internal-error_Processing Error','package-crawl-internal-error',@tag_type_id,@owner_id),
    (0,'Error package crawl password protected_Processing Error','Error package crawl password protected',@tag_type_id,@owner_id),
    (0,'offline-skipped_Processing Error','offline-skipped',@tag_type_id,@owner_id);