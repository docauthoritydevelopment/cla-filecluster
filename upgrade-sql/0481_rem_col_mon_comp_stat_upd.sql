alter table `mon_component_status` drop column `updated_on_db` ;
alter table `mon_component_status` drop column `timestamp` ;
alter table `mon_component_status` add column `timestamp` BIGINT(20) NOT NULL;