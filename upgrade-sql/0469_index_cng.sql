alter table `action_execution_task_status` drop INDEX `IDXi0lpsaaddfqloeoq4a92xqmeh`;

alter table `act_file_action_desired` drop  INDEX `IDX3ctwjwu7d64dqn75mbfbpwm1d` ;
alter table `act_file_action_desired` drop  INDEX `IDXfakh4awiwi5nqs2nsjgtw7rrg` ;
alter table `act_file_action_desired` drop  INDEX `IDXivwafeh097g1pgx34iokqleve` ;

alter table `audit` drop  INDEX `sub_system_idx`;
alter table `audit` drop  INDEX `username_idx`;

alter table `biz_list` drop  INDEX `name_idx`;

alter table bl_item_simple drop INDEX `name_idx`;
alter table bl_item_simple drop INDEX `entity_id_idx`;

alter table business_id_tag_association drop INDEX `association_time_stamp_idx`;

alter table `biz_list_item_extraction`  drop INDEX  `bl_e_dirty_bli`;
alter table `biz_list_item_extraction` drop INDEX  `bl_e_dirty_f`;

alter table `cla_files_md` drop INDEX `acct_content_metadata_idx`;
alter table `cla_files_md` drop INDEX `acct_dirty_id_idx`;
alter table `cla_files_md` drop INDEX `rf_media_itm_idx`;
alter table `cla_files_md` drop INDEX `state_idx`;
alter table `cla_files_md` drop INDEX `rf_acl_sig_idx`;
alter table `cla_files_md` drop INDEX `rf_fileNameHashed_idx`;
alter table `cla_files_md` drop INDEX `rf_fNHashed_lm_idx`;
alter table `cla_files_md` drop INDEX `rf_fNHashed_lm_ow_acl_idx`;
alter table `cla_files_md` drop INDEX `rf_deleted_id_idx`;
alter table `cla_files_md`  add INDEX `rf_deleted_id_idx` (root_folder_id,deleted);
alter table `cla_files_md` drop INDEX `rf_deleted_pkg_id_idx`;

alter table `cla_file_tag_association` drop INDEX `association_time_stamp_idx`;
alter table `cla_file_tag_association` drop INDEX `dirty_idx`;

alter table `content_metadata` drop INDEX `type_idx`;
alter table `content_metadata` drop INDEX `popDirty_joinSignatureIdz`;
alter table `content_metadata` drop INDEX `contentSignatureIdx`;
alter table `content_metadata` drop INDEX `user_content_signature_idx`;

alter table `crawl_runs` drop INDEX `idx_stoptime`;
alter table `crawl_runs` drop INDEX `idx_runtype`;

alter table deb_file_hist_info drop INDEX `lid_idx`;
alter table deb_file_hist_info drop INDEX `type_idx`;
alter table deb_file_hist_info drop INDEX `uuid_idx`;
alter table deb_file_hist_info drop INDEX `ts_idx`;

alter table deb_similarity_info drop INDEX `lid_idx1`;
alter table deb_similarity_info drop INDEX `lid_idx2`;
alter table deb_similarity_info drop INDEX `type_idx`;
alter table deb_similarity_info drop INDEX `ts_idx`;
alter table deb_similarity_info drop INDEX `uuid_idx1`;
alter table deb_similarity_info drop INDEX `uuid_idx2`;

alter table df_acl_items drop INDEX `type_idx`;

alter table doc_folder drop INDEX `pkg_rootFid_del_idx`;
alter table doc_folder drop INDEX `deleted_rootFid_pkg_idx`;
alter table doc_folder drop INDEX `deleted_id_idx`;
alter table doc_folder drop INDEX `deleted_depth_subFolders_id_idx`;
alter table doc_folder drop INDEX `acl_signature_idx`;
alter table doc_folder drop INDEX `name_idx`;
alter table doc_folder drop INDEX `real_path_idx`;
alter table doc_folder drop INDEX `del_real_path_idx`;
alter table doc_folder drop INDEX `deleted_pFolder_pInfo_idx`;
alter table doc_folder drop INDEX `parentsInfo_idx`;
alter table doc_folder add  INDEX `depth_rootFid_idx` (`root_folder_id`, `depth_from_root`);

alter table doc_type_association drop INDEX `association_time_stamp_idx`;
alter table doc_type_association drop INDEX `deleted_idx`;

alter table doc_type_risk_information drop INDEX `name_idx`;

alter table extracted_entities drop INDEX `clearFields_idx`;
alter table extracted_entities drop INDEX `rule_idx`;
alter table extracted_entities drop INDEX `ruleParam_idx`;
alter table extracted_entities drop INDEX `customField_idx`;
alter table extracted_entities drop INDEX `groupId_idx`;

alter table extracted_text drop INDEX `resultOrder_idx_et`;

alter table file_groups drop INDEX `name_idx`;
alter table file_groups drop INDEX `id_del_idx`;

alter table extraction_configuration drop INDEX `name_idx`;

alter table file_tag drop INDEX `identifier_idx`;

alter table `folder_exclude_rule` add INDEX `rootfolder_idx` (root_folder_id);
alter table folder_exclude_rule drop INDEX `rootsatch_op`;

alter table folder_tag_association drop INDEX `association_time_stamp_idx`;
alter table folder_tag_association drop INDEX `deleted_idx`;

alter table group_unification_helper drop INDEX `id_processed_idx`;


alter table jm_jobs drop INDEX `type_idx`;
alter table jm_jobs drop INDEX `item_id_idx`;

alter table jm_tasks drop INDEX `job_item_idx`;

drop table merging_groups;

alter table missed_file_match drop INDEX `fileid1_idx`;
alter table missed_file_match drop INDEX `fileid2_idx`;
alter table missed_file_match drop INDEX `id_processed_idx`;
alter table `missed_file_match` add INDEX `processed_idx` (processed);

alter table mon_component_status drop INDEX `idx_type`;

alter table root_folder drop INDEX `mediaType_idx`;

alter table `trend_chart_data` add INDEX `trend_chart_idx` (trend_chart_id);
alter table trend_chart_data drop INDEX `tc_ts_idx`;

alter table users drop INDEX `type_idx`;

-- for saga only !
-- ALTER TABLE `jm_tasks` DROP INDEX `job_stritem_idx`;
-- drop table crawl_file_change_log;
-- drop table crawl_folder_change_log;
-- drop table crawl_temp_file;
-- drop table crawl_temp_folder;
-- drop table tmp_file_groups_180205;