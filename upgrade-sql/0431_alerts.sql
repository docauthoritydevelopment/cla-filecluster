CREATE TABLE `alerts` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NOT NULL,
	`acknowledged` BIT(1) NOT NULL,
	`description` VARCHAR(255) NULL DEFAULT NULL,
	`event_type` INT(11) NOT NULL,
	`message` VARCHAR(255) NULL DEFAULT NULL,
	`severity` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`, `date_created`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
