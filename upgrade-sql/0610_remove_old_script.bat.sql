REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster

pushd ..
SET LOGF=installation_logs\%0.log
SET FILE_TO_DEL=kvdb-rebuilder.bat
IF EXIST "%FILE_TO_DEL%" (
    del /q /f %FILE_TO_DEL%
    Echo File %FILE_TO_DEL% deleted >> %LOGF%
) ELSE (
    Echo File %FILE_TO_DEL% not found >> %LOGF%
)
popd
goto finally

:usage
echo Usage:
echo        %0
goto finally

:finally
exit /b 0
