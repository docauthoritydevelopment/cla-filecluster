CREATE TABLE `processes` (
	`type` VARCHAR(31) NOT NULL,
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`process_definition` JSON NULL DEFAULT NULL,
	`process_key` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `process_key_idx` (`process_key`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
