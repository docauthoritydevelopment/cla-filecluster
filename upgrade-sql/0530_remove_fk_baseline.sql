-- Note: if you get Error Code: 1418 try running SET GLOBAL log_bin_trust_function_creators = 1;

DROP TABLE IF EXISTS `_schema_upgrade_fk_log`;
CREATE TABLE `_schema_upgrade_fk_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_stamp` DATETIME(3) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `message` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP PROCEDURE IF EXISTS `RemoveForeignKeys`;
DELIMITER $$

CREATE PROCEDURE `RemoveForeignKeys` (
    exclude_tbl_names  VARCHAR(128)
)
BEGIN
    DECLARE done INT DEFAULT 0;
    DECLARE isExcluded INTEGER;
    DECLARE tbl_name VARCHAR(80);
    DECLARE col_name VARCHAR(80);
    DECLARE constr_name VARCHAR(80);
    DECLARE ref_tbl_name VARCHAR(80);
    DECLARE ref_col_name VARCHAR(80);
    DECLARE curs CURSOR FOR SELECT
        TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
        FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE REFERENCED_TABLE_SCHEMA = database();
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    OPEN curs;
    REPEAT
        FETCH curs INTO tbl_name,col_name,constr_name,ref_tbl_name,ref_col_name;

        SELECT LOCATE(CONCAT(',',tbl_name),CONCAT(',',exclude_tbl_names)) INTO isExcluded;
        IF done = 0 THEN
            IF isExcluded = 0 THEN
                -- ALTER TABLE tbl_magazine_issue DROP FOREIGN KEY FK_tbl_magazine_issue_mst_users
                SET @sqlstmt = CONCAT('ALTER TABLE ',tbl_name ,' DROP FOREIGN KEY ', constr_name);
                PREPARE st FROM @sqlstmt;
                EXECUTE st;
                DEALLOCATE PREPARE st;
                -- Add KEY instead
--                SET @sqlstmt = CONCAT('ALTER TABLE ',tbl_name ,' ADD INDEX ', constr_name, ' (', col_name, ') ');
--                PREPARE st FROM @sqlstmt;
--                EXECUTE st;
--                DEALLOCATE PREPARE st;
                INSERT INTO `_schema_upgrade_fk_log` (`time_stamp`, `status`, `message`)
                    SELECT now(3),2,CONCAT('Dropped FK ', constr_name, ' from table ', tbl_name,' referred to ', ref_tbl_name,'.',ref_col_name) Info_Msg;
            ELSE
                INSERT INTO `_schema_upgrade_fk_log` (`time_stamp`, `status`, `message`)
                    SELECT now(3),1,CONCAT('Skipped FK ', constr_name, ' of table ', tbl_name,' referred to ', ref_tbl_name,'.',ref_col_name) Info_Msg;
            END IF;
        END IF;
    UNTIL done END REPEAT;
    CLOSE curs;
END $$

DELIMITER ;

-- Using the procedure:
--      CALL RemoveForeignKeys('exludedTbl1,exludedTbl2');
--      CALL RemoveForeignKeys('');

-- INSERT INTO `_schema_upgrade_fk_log` (`time_stamp`, `status`, `message`)
--    SELECT now(3),0,'Done removing foreign keys';
--
-- SELECT * FROM `_schema_upgrade_fk_log`;

