CREATE TABLE IF NOT EXISTS `workflow_filter_descriptor` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`date_created` BIGINT(20) NULL DEFAULT NULL,
	`date_modified` BIGINT(20) NULL DEFAULT NULL,
	`filter` JSON NULL DEFAULT NULL,
	`workflow_file_data` JSON NULL DEFAULT NULL,
	`workflow_id` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `workflow_id_idx` (`workflow_id`),
	CONSTRAINT `workflow_id_fk` FOREIGN KEY (`workflow_id`) REFERENCES `workflow` (`id`)
);
