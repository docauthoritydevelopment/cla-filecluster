ALTER TABLE `business_id_tag_association` ADD COLUMN `functional_role_id` bigint(20);
ALTER TABLE `cla_file_tag_association` ADD COLUMN `functional_role_id` bigint(20);
ALTER TABLE `folder_tag_association` ADD COLUMN `functional_role_id` bigint(20);
ALTER TABLE `ldap_group_mapping_biz_roles` CHANGE `biz_roles_id` `da_roles_id` BIGINT(20);
RENAME TABLE `ldap_group_mapping_biz_roles` TO `ldap_group_mapping_da_roles`;
