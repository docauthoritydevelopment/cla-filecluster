CREATE TABLE `diagnostic` (
	`id` BIGINT(20) NOT NULL,
	`create_time` BIGINT(20) NULL DEFAULT NULL,
	`diagnostic_type` VARCHAR(255) NOT NULL,
	`data` JSON NULL DEFAULT NULL,
	`start_time` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;