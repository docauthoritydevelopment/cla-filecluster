ALTER TABLE `root_folder`
	ADD COLUMN `scanned_meaningful_files_count_cap` INT(11) NULL DEFAULT NULL;

ALTER TABLE jm_jobs
	ADD COLUMN `scanned_meaningful_files_count` INT(11) NOT NULL DEFAULT 0;

