INSERT INTO `roles` VALUES
     (34,'Manage Departments','Manage departments','MngDepartmentConfig')
   ,(35,'Assign Departments','Assign Departments','AssignDepartment')
   ,(36,'View Departments hierarchy','View Departments tree','ViewDepartmentTree')
   ,(37,'View Departments associations','View assigned Departments','ViewDepartmentAssociation')
;


INSERT INTO `role_authorities` VALUES
   (34,33),(35,34),(36,35),(37,36)
   ;

insert into template_roles
select template_id, 34 from template_roles where role_id = 15;


insert into template_roles
select template_id, 35 from template_roles where role_id = 16;


insert into template_roles
select template_id, 36 from template_roles where role_id = 17;


insert into template_roles
select template_id, 37 from template_roles where role_id = 18;




