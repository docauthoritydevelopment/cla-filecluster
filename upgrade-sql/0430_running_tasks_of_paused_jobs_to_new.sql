UPDATE jm_tasks st JOIN jm_jobs sj ON st.job_id = sj.id
SET st.state = 0
WHERE (st.state = 1 OR st.state = 2) AND sj.state = 'PAUSED';
