ALTER TABLE `customer_data_center` ADD COLUMN `public_key` VARCHAR(1024) NULL AFTER `name`;

DROP TABLE IF EXISTS entity_credentials ;

CREATE TABLE `entity_credentials` (
	`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
	`entity_id` BIGINT(20) NOT NULL,
	`entity_type` VARCHAR(255) NOT NULL,
	`password` VARCHAR(500) NOT NULL,
    `username` VARCHAR(50) NULL DEFAULT NULL,
	`created_time_stamp` BIGINT(20) NULL DEFAULT NULL,
	`modified_time_stamp` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `entity_identify` (`entity_type`, `entity_id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8
;
