-- Initial creation set default to null to handle exsisting records
alter table crawl_runs add column `version` INT(11) NULL DEFAULT 0;
alter table jm_tasks add column `version` INT(11) NULL DEFAULT 0;
alter table jm_jobs add column `version` INT(11) NULL DEFAULT 0;
-- Update null version values in case of P01 already upgraded
update crawl_runs set version = 0 where version is null and id>0;
update jm_jobs set version = 0 where version is null and id>0;
update jm_tasks set version = 0 where version is null and id>0;
-- Set field default value to null to have table definition identical to fresh schema
alter table crawl_runs modify column `version` INT(11) NULL DEFAULT NULL;
alter table jm_tasks modify column `version` INT(11) NULL DEFAULT NULL;
alter table jm_jobs modify column `version` INT(11) NULL DEFAULT NULL;
