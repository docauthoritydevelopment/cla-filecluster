package com.cla.filecluster.utils;

/**
 * Created by: yael
 * Created on: 6/24/2018
 */
public class EncMain {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("password to encrypt is mandatory");
            return;
        }

        String password = args[0];

        if (args.length == 2) {
            String seed = args[1];
            EncUtils.setSeed(seed);
        }

        System.out.println("The result of the encryption of given password is: " + EncUtils.encrypt(password));
    }
}
