package com.cla.filecluster.utils;

import com.google.common.base.Strings;
import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by: yael
 * Created on: 6/21/2018
 */
public class EncUtils {
    private static PooledPBEStringEncryptor encryptor;

    private static String seed = "h5m340ijt9034kfrt3jy9054ytk304-kg40o3eg";

    static  {
        try {
            Properties properties = new Properties();
            File propertiesFile = new File("./config","application.properties");
            if (propertiesFile.exists()) {
                properties.load(new FileInputStream(propertiesFile));
                if (!Strings.isNullOrEmpty(properties.getProperty("enc.seed"))) {
                    seed = properties.getProperty("enc.seed");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        createInstance();
    }

    private synchronized static PooledPBEStringEncryptor getInstance() {
        return encryptor;
    }

    private synchronized static void createInstance() {
        encryptor = new PooledPBEStringEncryptor();
        SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setPassword(seed);
        config.setAlgorithm("PBEWithMD5AndDES");
        config.setKeyObtentionIterations("1000");
        config.setPoolSize("1");
        config.setProviderName("SunJCE");
        config.setSaltGeneratorClassName("org.jasypt.salt.RandomSaltGenerator");
        config.setStringOutputType("base64");
        encryptor.setConfig(config);
    }

    public static void setSeed(String newSeed) {
        seed = newSeed;
        createInstance();
    }

    public static String encrypt(String data) {
        return getInstance().encrypt(data);
    }

    public static String decrypt(String data) {
        return getInstance().decrypt(data);
    }
}
