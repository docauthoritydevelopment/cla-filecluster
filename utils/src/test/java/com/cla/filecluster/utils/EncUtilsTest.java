package com.cla.filecluster.utils;

import org.junit.Assert;
import org.junit.Test;

public class EncUtilsTest {

    @Test
    public void testEnc() {
        String password = "root";
        String res = EncUtils.encrypt(password);
        String clear = EncUtils.decrypt(res);
        Assert.assertEquals(password, clear);
        Assert.assertNotEquals(password, res);
    }

    @Test
    public void testCngSeed() {
        String password = "root";
        String res = EncUtils.encrypt(password);
        EncUtils.setSeed("6y6y6y6y6y6y6y6y6y6y6y6");
        String res2 = EncUtils.encrypt(password);
        Assert.assertNotEquals(res2, res);
    }
}