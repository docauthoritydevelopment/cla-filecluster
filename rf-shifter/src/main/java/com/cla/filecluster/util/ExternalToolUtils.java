package com.cla.filecluster.util;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.io.FilenameUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public class ExternalToolUtils {

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

    public static SolrClient buildSolrClient(String solrUrl) {
        if (isSolrCloudUrl(solrUrl)) {
            String[] split = solrUrl.split("/", 2);
            String zkUrl = split[0];
            String zkRoot = (split.length == 1) ? null : ("/" + split[1]);
            return new CloudSolrClient.Builder(Lists.newArrayList(zkUrl), Optional.of(zkRoot)).build();
        } else {
            HttpSolrClient solrClient = new HttpSolrClient.Builder().allowCompression(true)
                    .withBaseSolrUrl(solrUrl).build();

            return solrClient;
        }
    }

    public static boolean isSolrCloudUrl(String solrUrl) {
        return !solrUrl.startsWith("http");
    }

    public static String normalizeFileSharePath(String path) {
        Path normalize = Paths.get(path).toAbsolutePath().normalize();
        return convertPathToUniversalString(normalize);
    }

    public static String getHashedFolderString(final String path, final Long rootFolderId) {
        String pathString = convertPathToUniversalString(path);
        return md5DigestAsHex((rootFolderId + ":" + pathString).getBytes(StandardCharsets.UTF_8));
    }

    public static String convertPathToUniversalString(Path path) {
        return convertPathToUniversalString(path.toString());
    }

    public static String convertPathToUniversalString(String path) {
        if (path == null) {
            return null;
        }
        path = FilenameUtils.separatorsToUnix(path);
        path = path.replace("//", "/");
        if (path.startsWith("file:/")) {
            path = path.substring(6);
        } else if (path.startsWith("http")) {
            path = path.replace(":/", "://"); // Regain lost '/' in order to re-standardize url.
        }

        if (!path.endsWith("/")) {
            path = path.concat("/");
        }
        return path;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars).toLowerCase();
    }

    public static String getHashedFileName(final String fileName) {
        try {
            return md5DigestAsHex(fileName.getBytes("UTF-8"));
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String md5DigestAsHex(byte[] bytes) {
        return digestAsHexString(MD5_ALGORITHM_NAME, bytes);
    }

    private static String digestAsHexString(String algorithm, byte[] bytes) {
        char[] hexDigest = digestAsHexChars(algorithm, bytes);
        return new String(hexDigest);
    }

    private static char[] digestAsHexChars(String algorithm, byte[] bytes) {
        byte[] digest = digest(algorithm, bytes);
        return encodeHex(digest);
    }

    private static byte[] digest(String algorithm, byte[] bytes) {
        return getDigest(algorithm).digest(bytes);
    }

    private static MessageDigest getDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException("Could not find MessageDigest with algorithm \"" + algorithm + "\"", ex);
        }
    }

    private static char[] encodeHex(byte[] bytes) {
        char[] chars = new char[32];
        for (int i = 0; i < chars.length; i = i + 2) {
            byte b = bytes[i / 2];
            chars[i] = HEX_CHARS[(b >>> 0x4) & 0xf];
            chars[i + 1] = HEX_CHARS[b & 0xf];
        }
        return chars;
    }

    private static final String MD5_ALGORITHM_NAME = "MD5";

    private static final char[] HEX_CHARS =
            {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String getParentNameOnly(String fileFullName) {
        String path = getParentDir(fileFullName);
        final String[] pathParts = FilenameUtils.getPath(path).split("[\\\\/]");
        return pathParts.length == 0 ? "" : pathParts[pathParts.length-1];
    }

    public static String getParentDir(String path) {
        if (Strings.isNullOrEmpty(path)) return "";
        if (path.endsWith("/") || path.endsWith("\\")) {
            path = path.substring(0, path.length()-1);
        }
        int indexOf = path.lastIndexOf("/");
        if (indexOf < 0) {
            indexOf = path.lastIndexOf("\\");
        }
        if (indexOf <= 0) {
            return "";
        }
        return path.substring(0, indexOf+1);
    }
}