package com.cla.filecluster.rfshifter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by: yael
 * Created on: 3/17/2019
 */
public class RecalcHashFilesFolders {

    private final Connection connection;
    private final SolrClient solrClient;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        boolean configSet = false;
        Level claLogLevel = Level.WARN;
        boolean simulateAction = false;
        int pageSize = 10000;
        String rootFolderIds = null;
        Integer olderThanDays = null;
        Integer newerThanDays = null;

        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if ("-l".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        claLogLevel = Level.valueOf(args[argsInx].toUpperCase());
                        System.out.println(String.format("Setting logging to %s", claLogLevel.toString()));
                    } catch (Exception e) {
                    }
                }
            } else if ("-sim".equals(args[argsInx])) {
                simulateAction = true;
                System.out.println(String.format("Setting simulation only to %s", simulateAction));
            } else if ("-page-size".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        pageSize = Integer.valueOf(args[argsInx]);
                        System.out.println(String.format("Setting page size to %d", pageSize));
                    } catch (Exception e) {
                    }
                }
            } else if ("-r".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        rootFolderIds = args[argsInx];
                        rootFolderIds = rootFolderIds.replace(",", " ");
                        System.out.println("Setting root folder ids to (" + rootFolderIds + ")");
                    } catch (Exception e) {
                    }
                }
            } else if ("-o".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        olderThanDays = Integer.valueOf(args[argsInx]);
                        System.out.println(String.format("Setting olderThanDays to %d", olderThanDays));
                    } catch (Exception e) {
                    }
                }
            } else if ("-n".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        newerThanDays = Integer.valueOf(args[argsInx]);
                        System.out.println(String.format("Setting newerThanDays to %d", newerThanDays));
                    } catch (Exception e) {
                    }
                }
            } else if (!configSet) {
                configPath = args[argsInx];
                configSet = true;
            }
        }
        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        silentLoggers(claLogLevel);

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        new RecalcHashFilesFolders(connection, solrClient).recalcHashFilesFolders(
                pageSize, simulateAction, rootFolderIds, olderThanDays, newerThanDays);
    }

    private static void silentLoggers(Level claLevel) {
        Logger logger;
        logger = (Logger) LoggerFactory.getLogger("jetbrains.exodus");
        logger.setLevel(Level.ERROR);
        logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.WARN);

        logger = (Logger) LoggerFactory.getLogger("com.cla");
        logger.setLevel(claLevel);
    }

    private static void showUsage() {
        System.err.println("Usage: solr_clean_dup_files.bat [config path optional]");
    }

    public RecalcHashFilesFolders(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    public void recalcHashFilesFolders(int pageSize,
                                       boolean simulateAction,
                                       String rootFolderIds,
                                       Integer olderThanDays,
                                       Integer newerThanDays) throws Exception {
        try {
            System.out.println("============= Rehash files ===========");
            System.out.println();
            reHashFiles(pageSize, simulateAction, rootFolderIds, olderThanDays, newerThanDays);
            System.out.println("============= Rehash folders ===========");
            System.out.println();
            reHashFolders(pageSize, simulateAction, rootFolderIds);
            System.out.println();
            System.out.println("===============  Rehash Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    public void reHashFiles(int pageSize, boolean simulateAction,
                            String rootFolderIds,
                            Integer olderThanDays,
                            Integer newerThanDays) throws Exception {
        int cycleNum = 1;
        int numFound;
        String cursorMark = "*";
        Set<Long> rfIds = new HashSet<>();
        int handled = 0;

        do {
            List<SolrInputDocument> filesFix = new ArrayList<>();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFields("id", "fullName", "rootFolderId", "fileNameHashed", "fileId", "contentId");
            solrQuery.setRows(pageSize);
            solrQuery.setSort(SolrQuery.SortClause.asc("id"));
            solrQuery.set("cursorMark", cursorMark);

            if (!Strings.isNullOrEmpty(rootFolderIds)) {
                solrQuery.addFilterQuery("rootFolderId:(" + rootFolderIds + ")");
            }

            if (olderThanDays != null) {
                long millis = TimeUnit.DAYS.toMillis(olderThanDays);
                millis = System.currentTimeMillis() - millis;
                solrQuery.addFilterQuery("lastMetadataChangeDate:[* TO " + millis + "]");
            }

            if (newerThanDays != null) {
                long millis = TimeUnit.DAYS.toMillis(newerThanDays);
                millis = System.currentTimeMillis() - millis;
                solrQuery.addFilterQuery("lastMetadataChangeDate:[" + millis + " TO *]");
            }

            QueryResponse resp = solrClient.query("CatFiles", solrQuery, SolrRequest.METHOD.POST);
            if (resp != null && resp.getResults() != null) {
                cursorMark = resp.getNextCursorMark();

                long total = ((SolrDocumentList) resp.getResponse().get("response")).getNumFound();
                numFound = resp.getResults().size();
                handled += numFound;
                System.out.println(String.format("-- content cycle %d handling %d left %d", cycleNum, numFound, total-handled));

                for (SolrDocument doc : resp.getResults()) {
                    String fullName = (String) doc.getFieldValue("fullName");
                    String hashNew = ExternalToolUtils.getHashedFileName(fullName);
                    String hashOld = (String) doc.getFieldValue("fileNameHashed");
                    if (!hashNew.equals(hashOld)) {
                        SolrInputDocument fileFixRecord = new SolrInputDocument();
                        fileFixRecord.setField("id", doc.get("id"));

                        System.out.println("file " + doc.get("id") + " old hash: " + hashOld + " new hash: " + hashNew);

                        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
                        fieldModifier.put("set", hashNew);
                        fileFixRecord.addField("fileNameHashed", fieldModifier);

                        filesFix.add(fileFixRecord);

                        Long rootFolderId = (Long) doc.getFieldValue("rootFolderId");
                        rfIds.add(rootFolderId);
                    }

                    // search for invalid file ids
                    String id = (String) doc.get("id");
                    String contentId = (String) doc.get("contentId");
                    String fileId = String.valueOf((Long) doc.get("fileId"));
                    if (contentId != null) {
                        String compositeId = contentId + "!" + fileId;
                        if (!compositeId.equals(id)) {
                            System.out.println("file " + fileId + " has id " + id + " but has content " + contentId);
                        }
                    }
                }

                // save fix to files
                if (filesFix.size() > 0) {
                    if (simulateAction) {
                        System.out.println(String.format("-- Fixfile SIMULATION records %d", filesFix.size()));
                    } else {
                        System.out.println(String.format("-- Fixfile records %d", filesFix.size()));
                        solrClient.add("CatFiles", filesFix);
                        solrClient.commit("CatFiles", false, true, true);
                    }
                }
            } else {
                numFound = 0;
            }
            cycleNum++;
        } while (numFound == pageSize);
        System.out.println("root folders where files were fixed " + rfIds);
    }

    public void reHashFolders(int pageSize, boolean simulateAction,
                              String rootFolderIds) throws Exception {
        int cycleNum = 1;
        int numFound;
        String cursorMark = "*";
        Set<Long> rfIds = new HashSet<>();
        int handled = 0;

        do {
            List<SolrInputDocument> folderFix = new ArrayList<>();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFields("id", "path", "rootFolderId", "folderHash");
            solrQuery.setRows(pageSize);
            solrQuery.setSort(SolrQuery.SortClause.asc("id"));
            solrQuery.set("cursorMark", cursorMark);

            if (!Strings.isNullOrEmpty(rootFolderIds)) {
                solrQuery.addFilterQuery("rootFolderId:(" + rootFolderIds + ")");
            }

            QueryResponse resp = solrClient.query("CatFolders", solrQuery, SolrRequest.METHOD.POST);
            if (resp != null && resp.getResults() != null) {
                cursorMark = resp.getNextCursorMark();

                long total = ((SolrDocumentList) resp.getResponse().get("response")).getNumFound();
                numFound = resp.getResults().size();
                handled += numFound;
                System.out.println(String.format("-- content cycle %d handling %d left %d", cycleNum, numFound, total-handled));

                for (SolrDocument doc : resp.getResults()) {
                    String fullName = (String) doc.getFieldValue("path");
                    Long rootFolderId = (Long) doc.getFieldValue("rootFolderId");
                    String hashNew = ExternalToolUtils.getHashedFolderString(fullName, rootFolderId);
                    String hashOld = (String) doc.getFieldValue("folderHash");
                    if (!hashNew.equals(hashOld)) {
                        SolrInputDocument fileFixRecord = new SolrInputDocument();
                        fileFixRecord.setField("id", doc.get("id"));

                        System.out.println("file " + doc.get("id") + " old hash: " + hashOld + " new hash: " + hashNew);

                        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
                        fieldModifier.put("set", hashNew);
                        fileFixRecord.addField("folderHash", fieldModifier);

                        folderFix.add(fileFixRecord);
                        rfIds.add(rootFolderId);
                    }
                }

                // save fix to folders
                if (folderFix.size() > 0) {
                    if (simulateAction) {
                        System.out.println(String.format("-- FixFolder SIMULATION records %d", folderFix.size()));
                    } else {
                        System.out.println(String.format("-- FixFolder records %d", folderFix.size()));
                        solrClient.add("CatFolders", folderFix);
                        solrClient.commit("CatFolders", false, true, true);
                    }
                }
            } else {
                numFound = 0;
            }
            cycleNum++;
        } while (numFound == pageSize);
        System.out.println("root folders where folders were fixed " + rfIds);
    }
}
