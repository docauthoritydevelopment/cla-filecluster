package com.cla.filecluster.rfshifter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This tool was created as a result of a bug in migration script
 * not all duplicate files were deleted from solr
 * it seems we have many records with the same file id but different content
 * which should be removed
 *
 * Created by: yael
 * Created on: 2/17/2019
 */
public class DeleteOldDuplicateFilesSolr {
    private static Logger logger = (Logger) LoggerFactory.getLogger(DeleteOldDuplicateFilesSolr.class);

    private final Connection connection;
    private final SolrClient solrClient;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        boolean configSet = false;
        boolean simulateDups = false;
        int simulateDupsCount = 1000;
        Level claLogLevel = Level.WARN;

        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if ("-l".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        claLogLevel = Level.valueOf(args[argsInx].toUpperCase());
                        System.out.println(String.format("Setting logging to %s", claLogLevel.toString()));
                    } catch (Exception e) {}
                }
            } else if ("-sim".equals(args[argsInx])) {
                simulateDups = true;
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        simulateDupsCount = Integer.valueOf(args[argsInx]);
                    } catch (Exception e) {}
                }
            } else if (!configSet) {
                configPath = args[argsInx];
                configSet = true;
            }
        }
        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        silentLoggers(claLogLevel);

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        if (simulateDups) {
            new DeleteOldDuplicateFilesSolr(connection, solrClient).createDuplicates(simulateDupsCount);
        } else {
            new DeleteOldDuplicateFilesSolr(connection, solrClient).deleteAllDuplicates();
        }
    }

    private static void silentLoggers(Level claLevel) {
        Logger logger;
        logger = (Logger) LoggerFactory.getLogger("jetbrains.exodus");
        logger.setLevel(Level.ERROR);
        logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.WARN);

        logger = (Logger) LoggerFactory.getLogger("com.cla");
        logger.setLevel(claLevel);
    }

    private static void showUsage() {
        System.err.println("Usage: solr_clean_dup_files.bat [config path optional]");
    }

    public DeleteOldDuplicateFilesSolr(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    private void createDuplicates(int simulateDupsCount) throws Exception {
        try {
            System.out.println("============= Create simulated Solr duplications ===========");
            System.out.println();
            doCreateDuplicates(simulateDupsCount);
            System.out.println();
            System.out.println("===============  Simulation Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void deleteAllDuplicates() throws Exception {
        try {
            System.out.println("============= Executing file duplicate solr deletion ===========");
            System.out.println();
            deleteDuplicates();
            System.out.println();
            System.out.println("===============  Delete Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    final static int CID_OFFSET = 10000000;
    private void doCreateDuplicates(int simulateDupsCount) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("*:*");
        solrQuery.addSort("fileNameHashed", SolrQuery.ORDER.asc);
        solrQuery.setFilterQueries("processing_state:ANALYSED");
        solrQuery.setStart(0);
        solrQuery.setRows(simulateDupsCount);

        QueryResponse resp = solrClient.query("CatFiles", solrQuery);
        List<SolrInputDocument> newDocs = new ArrayList<>();
        if (resp != null && resp.getResults() != null) {
            resp.getResults().forEach(r -> {
                Long fileId = Long.valueOf((String) r.getFieldValue("fileId").toString());
                Long contentId = Long.valueOf((String) r.getFieldValue("contentId").toString());
                contentId += CID_OFFSET;
                SolrInputDocument doc = new SolrInputDocument();
                for (String fieldName : r.getFieldNames()) {
                    if (!"_version_".equals(fieldName)) {
                        doc.addField(fieldName, r.getFieldValue(fieldName));
                    }
                }
                doc.setField("id", getCompositeId(contentId, fileId));
                doc.setField("contentId", ""+contentId);
                doc.setField("processing_state", (fileId%2==0)?"SCANNED_AND_STOPPED":"ERROR");
                doc.removeField("mediaType");
                newDocs.add(doc);
            });
        }
        UpdateResponse updateResp = solrClient.add("CatFiles", newDocs, 1000);
        System.out.println(String.format("Created %d new records. Status=%d", newDocs.size(), updateResp.getStatus()));
    }

    private void deleteDuplicates() throws Exception {

        int start = 0;
        int cycleNum = 1;
        int pageSize = 5000;
        long total;
        long numFound;

        do {
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.addField("fileId");
            solrQuery.addField("id");
            solrQuery.setRows(pageSize);
            solrQuery.setFilterQueries("-mediaType:*");
            solrQuery.setStart(0);

            System.out.println();
            System.out.println(String.format("-- cycle %d", cycleNum));
            QueryResponse resp = solrClient.query("CatFiles", solrQuery);
            if (resp != null && resp.getResults() != null) {
                total = ((SolrDocumentList) resp.getResponse().get("response")).getNumFound();
                numFound = resp.getResults().size();
                Map<Long, String> resSolr = new HashMap<>();
                resp.getResults().forEach(r -> {
                    Object id = r.getFieldValue("id");
                    Object fileId = r.getFieldValue("fileId");
                    if (fileId != null) {
                        resSolr.put((Long) fileId, (String) id);
                    } else {
                        if (id != null) {
                            String[] ids = ((String) id).split("!");
                            resSolr.put(Long.parseLong(ids[1]), (String) id);
                        }
                    }
                });
                if (!resSolr.isEmpty()) {
                    List<String> idsToDelete = new ArrayList<>();
                    String idsCommaSeparated = resSolr.keySet().stream().map(Object::toString).collect(Collectors.joining(","));
                    PreparedStatement st = connection.prepareStatement("select f.id, f.content_metadata_id from cla_files_md f " +
                            "where f.id in (" + idsCommaSeparated + ")");
                    System.out.println("-- get file ids from db "+idsCommaSeparated);
                    ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        Long fileId = rs.getLong(1);
                        Long contentId = rs.getObject(2) == null ? null : rs.getLong(2);

                        long cid;
                        if (contentId == null) {
                            cid = fileId;
                        } else {
                            cid = contentId;
                        }

                        String compositeId = getCompositeId(cid, fileId);

                        if (resSolr.containsKey(fileId)) {
                            String id = resSolr.get(fileId);
                            if (!id.equals(compositeId)) {
                                //System.out.println("Calculated id "+compositeId+" different from solr id "+id+" use solr");
                                idsToDelete.add(id);
                            }
                        } else {
                            System.out.println("-- problem: cant find file in list "+fileId);
                        }
                    }
                    if (!idsToDelete.isEmpty()) {
                        solrClient.deleteById("CatFiles", idsToDelete);
                        solrClient.commit("CatFiles", false, true, true);
                        System.out.println(String.format("Deleting records for %d contents... %d of %d total %d", idsToDelete.size(), start, numFound, total));
                    }
                } else if (numFound > 0) {
                    System.out.println("-- problem: got results but couldnt extract ids!!!");
                }
                cycleNum++;
                start += pageSize;
            } else {
                numFound = 0;
            }
        } while (numFound == pageSize);
    }

    private String getCompositeId(Long contentId, Long fileId) {
        Long firstSegment = (contentId == null) ? fileId : contentId;
        return firstSegment + "!" + fileId;
    }
}