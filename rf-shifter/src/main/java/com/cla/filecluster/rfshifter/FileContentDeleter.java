package com.cla.filecluster.rfshifter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 2/10/2019
 */
public class FileContentDeleter {
    private static int COMMIT_WITHIN_MS = 10000;
    private static int MAX_SEGMENTS_DEFAULT = 10;

    private final Connection connection;
    private final SolrClient solrClient;
    private final int maxSegments;
    private final boolean forceOptimization;
    private final boolean forceOptimizationIfDelete;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        boolean configSet = false;
        boolean removeDeletedCatFiles = false;
        boolean forceOptimization = false;
        boolean forceOptimizationIfDelete = false;
        int maxSegments = MAX_SEGMENTS_DEFAULT;
        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if ("-seg".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        maxSegments = Integer.valueOf(args[argsInx]);
                        System.out.println(String.format("Setting max-segments to %d", maxSegments));
                    } catch (Exception e) {
                    }
                }
            } else if ("-fo".equals(args[argsInx])) {
                forceOptimization = true;
            } else if ("-o".equals(args[argsInx])) {
                forceOptimizationIfDelete = true;
            } else if ("-d".equals(args[argsInx])) {
                removeDeletedCatFiles = true;
            } else if (!configSet) {
                configPath = args[argsInx];
                configSet = true;
            }
        }

        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        silentLoggers();

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        new FileContentDeleter(connection, solrClient, maxSegments, forceOptimization, forceOptimizationIfDelete).deleteFilesAndContents(removeDeletedCatFiles);
    }

    private static void silentLoggers() {
        Logger logger;
        logger = (Logger) LoggerFactory.getLogger("jetbrains.exodus");
        logger.setLevel(Level.ERROR);
        logger = (Logger) LoggerFactory.getLogger("com.cla");
        logger.setLevel(Level.WARN);
        logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.WARN);
    }

    private static void showUsage() {
        System.err.println("Usage: solr_content_cleanup.bat [config path optional]");
    }

    private FileContentDeleter(Connection connection, SolrClient solrClient, int maxSegments, boolean forceOptimization, boolean forceOptimizationIfDelete) {
        this.connection = connection;
        this.solrClient = solrClient;
        this.maxSegments = maxSegments;
        this.forceOptimization = forceOptimization;
        this.forceOptimizationIfDelete = forceOptimizationIfDelete;
    }

    private void deleteFilesAndContents(boolean removeDeletedCatFiles) throws Exception {
        try {
            validateNoActiveRuns();
            System.out.println("============= Executing file and content deletion ===========");
            System.out.println();
            long totalDeleted = deleteContentRelatedInBatch();


            if (removeDeletedCatFiles) {
                totalDeleted += deleteFiles();
            } else {
                System.out.println("============= Skipping deleted CatFile cleanup ===========");

            }

            if (forceOptimization || (forceOptimizationIfDelete && totalDeleted > 0)) {
                optimizedCleanedCollections();
            }
            System.out.println(String.format("-- Deleting from all collections END. Total of %d records were deleted from collections: AnalysisData,Extraction,PVS,ContentMetadata.", totalDeleted));

            System.out.println();
            System.out.println("===============  Delete Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private long deleteFiles() throws Exception {
        System.out.println();

        SolrQuery countSolrQuery = new SolrQuery();
        countSolrQuery.setQuery("*:*");
        countSolrQuery.setFields("id");
        countSolrQuery.addFilterQuery("deleted:true");
        countSolrQuery.addSort("fileId", SolrQuery.ORDER.asc);
        countSolrQuery.setRows(0);
        countSolrQuery.setStart(0);
        QueryResponse countResp = solrClient.query("CatFiles", countSolrQuery);
        long total = ((SolrDocumentList) countResp.getResponse().get("response")).getNumFound();

        System.out.println(String.format("-- Deleting %d files from CatFiles", total));
        UpdateResponse deleteResp = solrClient.deleteByQuery("CatFiles", "deleted:true", COMMIT_WITHIN_MS);
        System.out.println(String.format("-- CatFiles records deletion done. Status %d", deleteResp.getStatus()));
        return total;
    }

    private long deleteContentRelatedInBatch() throws Exception {

        int start = 0;
        List<String> contentIdsToDel;
        int cycleNum = 1;
        int pageSize = 5000;

        SolrQuery countSolrQuery = new SolrQuery();
        countSolrQuery.setQuery("*:*");
        countSolrQuery.setFields("id");
        countSolrQuery.addFilterQuery("fileCount:0");
        countSolrQuery.addSort("id", SolrQuery.ORDER.asc);
        countSolrQuery.setRows(0);
        countSolrQuery.setStart(0);
        QueryResponse countResp = solrClient.query("ContentMetadata", countSolrQuery);
        long total = ((SolrDocumentList) countResp.getResponse().get("response")).getNumFound();
        System.out.println();
        System.out.println(String.format("-- Deleting from all collections START. Total of %d records to delete", total));

        do {
            contentIdsToDel = new ArrayList<>();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFields("id");
            solrQuery.addFilterQuery("fileCount:0");
            solrQuery.addSort("id", SolrQuery.ORDER.asc);
            solrQuery.setRows(pageSize);
            solrQuery.setStart(0);
            QueryResponse resp = solrClient.query("ContentMetadata", solrQuery);
            long numFound = ((SolrDocumentList) resp.getResponse().get("response")).getNumFound();

            if (resp.getResults().size() > 0) {
                System.out.println(String.format("-- content cycle %d", cycleNum));
                for (SolrDocument doc : resp.getResults()) {
                    contentIdsToDel.add((String) doc.getFieldValue("id"));
                }

                String values = contentIdsToDel.stream().collect(
                        Collectors.joining(" ", "(", ")"));

                System.out.println();
                System.out.println(String.format("Deleting records for %d contents... %d of %d (%.1f%%). NumFound=%d", contentIdsToDel.size(), start, total, (start * 100f) / total, numFound));
                UpdateResponse deleteResp = solrClient.deleteById("AnalysisData", contentIdsToDel, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- AnalysisData records deleted. status %d", deleteResp.getStatus()));
                deleteResp = solrClient.deleteById("Extraction", contentIdsToDel, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- Extraction records deleted. status %d", deleteResp.getStatus()));
                deleteResp = solrClient.deleteByQuery("PVS", "fileId:" + values, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- PVS records deleted. status %d", deleteResp.getStatus()));
                deleteResp = solrClient.deleteById("ContentMetadata", contentIdsToDel, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- ContentMetadata records deleted. status %d", deleteResp.getStatus()));
                deleteResp = solrClient.commit("ContentMetadata", false, true, true);
                System.out.println(String.format("-- ContentMetadata content committed. Status %d", deleteResp.getStatus()));
                cycleNum++;
                start += pageSize;
            }
        } while (contentIdsToDel.size() == pageSize);

        return total;
    }

    private void optimizedCleanedCollections() throws SolrServerException, IOException {
        UpdateResponse optResp = solrClient.commit("AnalysisData", false, true, true);
        System.out.println(String.format("-- AnalysisData content committed. Status %d", optResp.getStatus()));
        optResp = solrClient.optimize("AnalysisData", false, false, maxSegments);
        System.out.println(String.format("-- AnalysisData optimized, maxSegments=%d. status %d", maxSegments, optResp.getStatus()));

        optResp = solrClient.commit("Extraction", false, true, true);
        System.out.println(String.format("-- Extraction content committed. Status %d", optResp.getStatus()));
        optResp = solrClient.optimize("Extraction", false, false, maxSegments);
        System.out.println(String.format("-- Extraction optimized, maxSegments=%d. status %d", maxSegments, optResp.getStatus()));

        optResp = solrClient.commit("PVS", false, true, true);
        System.out.println(String.format("-- PVS content committed. Status %d", optResp.getStatus()));
        optResp = solrClient.optimize("PVS", false, false, maxSegments);
        System.out.println(String.format("-- PVS optimized, maxSegments=%d. status %d", maxSegments, optResp.getStatus()));

        optResp = solrClient.optimize("ContentMetadata", false, false, maxSegments);
        System.out.println(String.format("-- ContentMetadata optimized, maxSegments=%d. status %d", maxSegments, optResp.getStatus()));

        optResp = solrClient.commit("CatFiles", false, true, true);
        System.out.println(String.format("-- CatFiles content committed. Status %d", optResp.getStatus()));
        optResp = solrClient.optimize("CatFiles", false, false, maxSegments);
        System.out.println(String.format("-- CatFiles optimized, maxSegments=%d. status %d", maxSegments, optResp.getStatus()));

    }

    private void validateNoActiveRuns() throws SQLException {
        boolean isValid = true;
        try (Statement stmt = connection.createStatement()) {
            System.out.println();
            System.out.println("-- validate no active runs");
            ResultSet resultSet = stmt.executeQuery("select * from crawl_runs where run_status IN ( 'RUNNING' )");

            if (resultSet.next()) {
                isValid = false;
            }
            resultSet.close();
        }

        if (!isValid) {
            throw new RuntimeException("Active run exists please stop before continuing!!");
        }
    }
}
