package com.cla.filecluster.rfshifter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;
import static java.util.Map.Entry.*;

/**
 * Tool to search for duplicate files in solr, grade them and delete the irrelevant
 *
 * Created by: yael
 * Created on: 3/3/2019
 */
public class DeleteDuplicateFilesSolr {

    private final Connection connection;
    private final SolrClient solrClient;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        boolean configSet = false;
        boolean simulateDups = false;
        int simulateDupsCount = 1000;
        Level claLogLevel = Level.WARN;
        boolean isTest = false;
        int pageSize = 10000;

        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if ("-l".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        claLogLevel = Level.valueOf(args[argsInx].toUpperCase());
                        System.out.println(String.format("Setting logging to %s", claLogLevel.toString()));
                    } catch (Exception e) {
                    }
                }
            } else if ("-sim".equals(args[argsInx])) {
                simulateDups = true;
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        simulateDupsCount = Integer.valueOf(args[argsInx]);
                    } catch (Exception e) {
                    }
                }
            } else if ("-page-size".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        pageSize = Integer.valueOf(args[argsInx]);
                        System.out.println(String.format("Setting page size to %d", pageSize));
                    } catch (Exception e) {
                    }
                }
            } else if ("-test".equals(args[argsInx])) {
                isTest = true;
            } else if (!configSet) {
                configPath = args[argsInx];
                configSet = true;
            }
        }
        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        silentLoggers(claLogLevel);

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        if (simulateDups) {
            new DeleteDuplicateFilesSolr(connection, solrClient).createDuplicates(simulateDupsCount);
        } else {
            new DeleteDuplicateFilesSolr(connection, solrClient).deleteAllDuplicates(isTest, pageSize);
        }
    }

    private static void silentLoggers(Level claLevel) {
        Logger logger;
        logger = (Logger) LoggerFactory.getLogger("jetbrains.exodus");
        logger.setLevel(Level.ERROR);
        logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.WARN);

        logger = (Logger) LoggerFactory.getLogger("com.cla");
        logger.setLevel(claLevel);
    }

    private static void showUsage() {
        System.err.println("Usage: solr_clean_dup_files.bat [config path optional]");
    }

    public DeleteDuplicateFilesSolr(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    private void createDuplicates(int simulateDupsCount) throws Exception {
        try {
            System.out.println("============= Create simulated Solr duplications ===========");
            System.out.println();
            doCreateDuplicates(simulateDupsCount);
            System.out.println();
            System.out.println("===============  Simulation Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void deleteAllDuplicates(boolean isTest, int pageSize) throws Exception {
        try {
            System.out.println("============= Executing file duplicate solr deletion ===========");
            System.out.println();
            if (isTest) {
                System.out.println("TEST MODE");
                System.out.println();
            }
            deleteDuplicates(isTest, pageSize);
            System.out.println();
            System.out.println("===============  Delete Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    static Random rand = new Random();
    final static int CID_OFFSET = 10000000;
    private void doCreateDuplicates(int simulateDupsCount) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("*:*");
        solrQuery.addSort("fileNameHashed", SolrQuery.ORDER.asc);
        solrQuery.setFilterQueries("processing_state:ANALYSED");
        solrQuery.setStart(0);
        solrQuery.setRows(simulateDupsCount);

        QueryResponse resp = solrClient.query("CatFiles", solrQuery, SolrRequest.METHOD.POST);
        List<SolrInputDocument> newDocs = new ArrayList<>();
        if (resp != null && resp.getResults() != null) {
            resp.getResults().forEach(r -> {
                Long fileId = Long.valueOf((String) r.getFieldValue("fileId").toString());
                Long contentId = Long.valueOf((String) r.getFieldValue("contentId").toString());
                contentId += CID_OFFSET + rand.nextInt(4000);
                SolrInputDocument doc = new SolrInputDocument();
                for (String fieldName : r.getFieldNames()) {
                    if (!"_version_".equals(fieldName)) {
                        doc.addField(fieldName, r.getFieldValue(fieldName));
                    }
                }
                doc.setField("id", getCompositeId(contentId, fileId));
                doc.setField("contentId", ""+contentId);
                doc.setField("processing_state", (fileId%2==0)?"SCANNED_AND_STOPPED":"ERROR");
                newDocs.add(doc);
            });
        }
        if (newDocs.isEmpty()) {
            System.out.println("could not create simulated files as no existing analyzed file currently in solr");
        } else {
            UpdateResponse updateResp = solrClient.add("CatFiles", newDocs, 1000);
            System.out.println(String.format("Created %d new records. Status=%d", newDocs.size(), updateResp.getStatus()));
            solrClient.commit("CatFiles");//), false, true, true);
        }
    }

    private void deleteDuplicates(boolean isTest, int pageSize) throws Exception {

        int start = 0;
        int cycleNum = 1;
        long numFound;
        System.out.println(String.format("Starting de-duplication, page size: %d", pageSize));

        do {
            System.out.println();
            System.out.println(String.format("-- cycle %d page size %d start %d", cycleNum, pageSize, start));
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFacetMinCount(2);
            solrQuery.setFacet(true);
            solrQuery.addFacetField("fileId");
            solrQuery.setFacetLimit(pageSize);
            solrQuery.set("facet.offset", start);
            solrQuery.setRows(0);

            QueryResponse resp = solrClient.query("CatFiles", solrQuery, SolrRequest.METHOD.POST);
            if (resp != null && resp.getFacetFields() != null && !resp.getFacetFields().isEmpty()) {
                FacetField field = resp.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                numFound = values.size();
                System.out.println("numFound size "+numFound);
                int total = 0;
                Map<String, Long> duplicateFiles = new HashMap<>();
                for (FacetField.Count c : values) {
                    String fileId = c.getName();
                    long copies = c.getCount();
                    total+=copies;
                    duplicateFiles.put(fileId, copies);
                }
                System.out.println("duplicateFiles size "+duplicateFiles.size()+ " to get max is "+total);

                if (!duplicateFiles.isEmpty()) {

                    String idsForQuery = duplicateFiles.keySet().stream().collect(
                            Collectors.joining(" ", "(", ")"));

                    solrQuery = new SolrQuery();
                    solrQuery.setQuery("*:*");
                    solrQuery.addFilterQuery("fileId:" +idsForQuery);
                    solrQuery.setFields("id", "fileId", "contentId", "mediaType", "tags2", "processing_state", "userGroupId");
                    solrQuery.setRows(total+1000);
                    resp = solrClient.query("CatFiles", solrQuery, SolrRequest.METHOD.POST);
                    Map<String, List<SolrDocument>> copies = new HashMap<>();
                    Set<String> contentIdsToSearch = new HashSet<>();
                    Set<String> existingContentIds = new HashSet<>();
                    if (resp != null && resp.getResults() != null) {
                        System.out.println("resp size "+resp.getResults().size());
                        for (SolrDocument doc : resp.getResults()) {
                            String fileId = ((Long)doc.getFieldValue("fileId")).toString();
                            List<SolrDocument> list = copies.getOrDefault(fileId, new ArrayList<>());
                            list.add(doc);
                            copies.put(fileId, list);
                            contentIdsToSearch.add((String)doc.getFieldValue("contentId"));
                        }
                        System.out.println("copies size "+copies.size());

                        if (!contentIdsToSearch.isEmpty()) {
                            idsForQuery = contentIdsToSearch.stream().collect(
                                    Collectors.joining(" ", "(", ")"));
                            System.out.println("get contents by ids "+contentIdsToSearch.size());
                            solrQuery = new SolrQuery();
                            solrQuery.setQuery("*:*");
                            solrQuery.addFilterQuery("id:" +idsForQuery);
                            solrQuery.setFields("id");
                            resp = solrClient.query("ContentMetadata", solrQuery, SolrRequest.METHOD.POST);
                            if (resp != null && resp.getResults() != null) {
                                for (SolrDocument doc : resp.getResults()) {
                                    existingContentIds.add((String)doc.getFieldValue("id"));
                                }
                            }
                        }

                        Set<String> idsToDelete = new HashSet<>();
                        for (Map.Entry<String, List<SolrDocument>> copy : copies.entrySet()) {
                            List<String> toDel = chooseHealthyCopy(copy.getValue(), existingContentIds);
                            idsToDelete.addAll(toDel);
                        }

                        if (!idsToDelete.isEmpty()) {
                            if (!isTest) {
                                solrClient.deleteById("CatFiles", new ArrayList<>(idsToDelete));
                                solrClient.commit("CatFiles", false, true, true);
                                System.out.println(String.format("Deleting records for %d files", idsToDelete.size()));
                            } else {
                                System.out.println("Should be deleting records for files (TEST MODE)");
                                System.out.println(idsToDelete);
                            }
                        } else {
                            System.out.println("nothing to delete - weird");
                        }
                    }
                } else {
                    if (numFound > 0) {
                        System.out.println("nothing to see - weird");
                        numFound = 0;
                    }
                }

                if (isTest) {
                    start += pageSize;
                }
            } else {
                numFound = 0;
            }
            cycleNum++;
        } while (numFound > 0); //== pageSize
    }

    private List<String> chooseHealthyCopy(List<SolrDocument> copies, Set<String> existingContentIds) {
        List<String> idsToDelete = new ArrayList<>();

        Map<String, Integer> grades = new HashMap<>();

        for (SolrDocument copy : copies) {
            String id = (String)copy.getFieldValue("id");
            String contentId = (String)copy.getFieldValue("contentId");
            List<String> tags2 = (List<String>)copy.getFieldValue("tags2");
            String userGroupId = (String)copy.getFieldValue("userGroupId");
            Integer mediaType = (Integer)copy.getFieldValue("mediaType");
            String processing_state = (String)copy.getFieldValue("processing_state");

            Integer grade = 0;

            if (mediaType == null) {
                grade -= 100;
            }

            if (contentId == null) {
                grade -= 100;
            } else if (!existingContentIds.contains(contentId)) {
                grade -= 100;
            }

            if (processing_state.equals("ERROR") || processing_state.equals("SCANNED_AND_STOPPED")) {
                grade -= 100;
            } else if (processing_state.equals("SCANNED")) {
                grade += 10;
            } else if (processing_state.equals("INGESTED")) {
                grade += 20;
            } else if (processing_state.equals("ANALYSED")) {
                grade += 30;
            }

            if (userGroupId != null && !userGroupId.isEmpty()) {
                grade += 20;
            }

            if (tags2 != null && !tags2.isEmpty()) {
                grade += 20;
            }

            grades.put(id, grade);
        }

        // sort this map by values first
        Map<String, Integer> sorted = grades
                .entrySet()
                .stream()
                .sorted(comparingByValue())
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));

        int i = 0;
        for (String id : sorted.keySet()) {
            if (i < sorted.size() - 1) {
                idsToDelete.add(id);
            }
            ++i;
        }

        return idsToDelete;
    }

    private String getCompositeId(Long contentId, Long fileId) {
        Long firstSegment = (contentId == null) ? fileId : contentId;
        return firstSegment + "!" + fileId;
    }
}
