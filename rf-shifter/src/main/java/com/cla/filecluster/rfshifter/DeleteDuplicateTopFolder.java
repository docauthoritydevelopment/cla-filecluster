package com.cla.filecluster.rfshifter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.stream.Collectors;

/**
 * we have duplicate folders on the higest level
 * search for them and remove
 * Created by: yael
 * Created on: 4/14/2019
 */
public class DeleteDuplicateTopFolder {

    private static final String UPDATE_VALS_REQUEST_HANDLER = "/update/vals";

    private final Connection connection;
    private final SolrClient solrClient;
    private static int pageSize = 10000;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        Level claLogLevel = Level.WARN;
        boolean simulateDups = false;
        boolean configSet = false;
        boolean pageSizeSet = false;
        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if ("-l".equals(args[argsInx])) {
                if (argsInx <= args.length) {
                    argsInx++;
                    try {
                        claLogLevel = Level.valueOf(args[argsInx].toUpperCase());
                        System.out.println(String.format("Setting logging to %s", claLogLevel.toString()));
                    } catch (Exception e) {
                    }
                }
            }
            else if (pageSizeSet) {
                pageSizeSet = false;
                pageSize = Integer.parseInt(args[argsInx]);
            } else if ("-p".equals(args[argsInx])) {
                pageSizeSet = true;
            } else if ("-sim".equals(args[argsInx])) {
                simulateDups = true;
            } else if (!configSet){
                configPath = args[argsInx];
                configSet = true;
            }
        }

        silentLoggers(claLogLevel);

        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");

        if (simulateDups) {
            new DeleteDuplicateTopFolder(connection, solrClient).createDuplicates();
        } else {
            new DeleteDuplicateTopFolder(connection, solrClient).fixDupDocFolder();
        }
    }

    private static void silentLoggers(Level claLevel) {
        Logger logger;
        logger = (Logger) LoggerFactory.getLogger("jetbrains.exodus");
        logger.setLevel(Level.ERROR);
        logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.WARN);

        logger = (Logger) LoggerFactory.getLogger("com.cla");
        logger.setLevel(claLevel);
    }

    private DeleteDuplicateTopFolder(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    private void createDuplicates() throws Exception {
        try {
            System.out.println("============= Create simulated Solr duplications ===========");
            System.out.println();
            doCreateDuplicates();
            System.out.println();
            System.out.println("===============  Simulation Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void fixDupDocFolder() throws Exception {
        try {
            System.out.println("============= Executing fix top doc folder duplicate ===========");
            System.out.println();
            searchDocFolderAndFix();
            System.out.println();
            System.out.println("===============  duplicate top folder fix ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void searchDocFolderAndFix() throws Exception {
        int start = 0;
        int cycleNum = 1;
        long numFound;
        System.out.println(String.format("Starting folder de-duplication, page size: %d", pageSize));

        do {
            System.out.println();
            System.out.println(String.format("-- cycle %d page size %d start %d", cycleNum, pageSize, start));
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFacetMinCount(2);
            solrQuery.setFacet(true);
            solrQuery.addFilterQuery("depthFromRoot:0");
            solrQuery.addFacetField("rootFolderId");
            solrQuery.setFacetLimit(pageSize);
            solrQuery.set("facet.offset", start);
            solrQuery.setRows(0);

            QueryResponse resp = solrClient.query("CatFolders", solrQuery, SolrRequest.METHOD.POST);
            List<String> duplicateFolderRootFolder = new ArrayList<>();
            if (resp != null && resp.getFacetFields() != null && !resp.getFacetFields().isEmpty()) {
                FacetField field = resp.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                numFound = values.size();
                System.out.println("numFound size " + numFound);
                for (FacetField.Count c : values) {
                    String rootFolderId = c.getName();
                    duplicateFolderRootFolder.add(rootFolderId);
                }
            } else {
                numFound = 0;
            }

            if (duplicateFolderRootFolder.size() > 0) {
                SolrQuery solrQueryDoubleSearch = new SolrQuery();
                solrQueryDoubleSearch.setQuery("*:*");
                solrQueryDoubleSearch.setFields("id", "path", "realPath", "acl_read", "acl_write", "aclSignature", "deleted", "rootFolderId", "parentsInfo");
                solrQuery.addFilterQuery("depthFromRoot:0");

                String idsForQuery = duplicateFolderRootFolder.stream().collect(
                        Collectors.joining(" ", "(", ")"));

                solrQuery.addFilterQuery("rootFolderId:"+idsForQuery);
                solrQuery.setRows(duplicateFolderRootFolder.size()*5);
                resp = solrClient.query("CatFolders", solrQuery, SolrRequest.METHOD.POST);
                Map<Long, List<SolrDocument>> duplicates = new HashMap<>();
                if (resp != null && resp.getResults() != null) {
                    for (SolrDocument doc : resp.getResults()) {
                        Long rootFolderId = (Long)doc.getFieldValue("rootFolderId");
                        List<SolrDocument> dup = duplicates.getOrDefault(rootFolderId, new ArrayList<>());
                        dup.add(doc);
                        duplicates.put(rootFolderId, dup);
                    }
                }

                List<String> idsFolderToDelete = new ArrayList<>();
                if (duplicates.keySet().size() > 0) {
                    duplicates.entrySet().forEach(entry -> {
                        List<SolrDocument> folders = entry.getValue();
                        SolrDocument folderToDelete = chooseFolderToDelete(folders);
                        if (folderToDelete != null) {
                            try {
                                folders.remove(folderToDelete);
                                SolrDocument folderToRemain = folders.get(0);

                                // move sub folders
                                moveSubFolders(folderToDelete.get("id").toString(), folderToRemain.get("id").toString());

                                // move files
                                moveFiles(folderToDelete.get("id").toString(), folderToRemain.get("id").toString());

                                // collect to delete folder
                                idsFolderToDelete.add(folderToDelete.get("id").toString());
                            } catch (Exception e) {
                                System.out.println(String.format("problem deleting folder %d", folderToDelete.get("id").toString()));
                                e.printStackTrace();
                            }
                        }
                    });

                    if (idsFolderToDelete.size() > 0) {
                        solrClient.deleteById("CatFolders", idsFolderToDelete);
                        solrClient.commit("CatFolders", false, true, true);
                        solrClient.commit("CatFiles", false, true, true);
                        System.out.println(String.format("Deleting records for %d folders", idsFolderToDelete.size()));
                    }
                }
            }
        } while (numFound > 0);
    }

    private void moveSubFolders(String toDelFolderId, String folderToRemainId) throws Exception {
        solrUpdateFieldByQuery("parentFolderId", folderToRemainId,
                "CatFolders",
                Lists.newArrayList("parentFolderId:"+toDelFolderId), "set");

        solrUpdateFieldByQuery("parentsInfo", PARENT_INFO_PREFIX+folderToRemainId,
                "CatFolders",
                Lists.newArrayList("parentsInfo:"+PARENT_INFO_PREFIX+toDelFolderId, "-depthFromRoot:0"), "add");

        solrUpdateFieldByQuery("parentsInfo", PARENT_INFO_PREFIX+toDelFolderId,
                "CatFolders",
                Lists.newArrayList("parentsInfo:"+PARENT_INFO_PREFIX+toDelFolderId, "-depthFromRoot:0"), "remove");
    }

    private void moveFiles(String toDelFolderId, String folderToRemainId) throws Exception {
        solrUpdateFieldByQuery("folderId", folderToRemainId,
                "CatFiles",
                Lists.newArrayList("folderId:"+toDelFolderId), "set");

        solrUpdateFieldByQuery("parentFolderIds", PARENT_INFO_PREFIX+folderToRemainId,
                "CatFiles",
                Lists.newArrayList("parentFolderIds:"+PARENT_INFO_PREFIX+toDelFolderId), "add");

        solrUpdateFieldByQuery("parentFolderIds", PARENT_INFO_PREFIX+toDelFolderId,
                "CatFiles",
                Lists.newArrayList("parentFolderIds:"+PARENT_INFO_PREFIX+toDelFolderId), "remove");
    }

    private SolrDocument chooseFolderToDelete(List<SolrDocument> folders) {
        SolrDocument result = null;
        int resultGrade = 0;

        if (folders != null && folders.size() > 1) {
            for (SolrDocument folder : folders) {
                int currentGrade = gradeFolder(folder);

                if (result == null || resultGrade < currentGrade) {
                    result = folder;
                    resultGrade = currentGrade;
                }
            }
        }

        return result;
    }

    private int gradeFolder(SolrDocument folder) {
        int resultGrade = 0;

        if (folder.getFieldValue("acl_read") == null) {
            resultGrade++;
        }

        if (folder.getFieldValue("aclSignature") == null) {
            resultGrade++;
        }

        if (folder.getFieldValue("acl_write") == null) {
            resultGrade++;
        }

        if (folder.getFieldValue("associations") == null || ((List<String>)folder.getFieldValue("associations")).isEmpty()) {
            resultGrade++;
        }

        if (folder.getFieldValue("mediaEntityId") == null) {
            resultGrade++;
        }

        if (folder.getFieldValue("deleted") != null && (Boolean)folder.getFieldValue("deleted")) {
            resultGrade+=100;
        }

        return resultGrade;
    }

    private Integer solrUpdateFieldByQuery(String field, String value, String collection, List<String> fq, String operation) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);
        solrQuery.setQuery("*:*");
        fq.forEach(solrQuery::addFilterQuery);
        solrQuery.set("field", field);
        if (value != null) {
            solrQuery.set("value", value);
        }
        solrQuery.set("update.op", operation);
        QueryResponse resp = this.solrClient.query(collection, solrQuery, SolrRequest.METHOD.POST);
        Integer cnged = resp.getResponse().get("docsChanged") == null ? null :
                (Integer) resp.getResponse().get("docsChanged");
        return cnged;
    }

    static Random rand = new Random();
    final static int CID_OFFSET = 10000000;
    private void doCreateDuplicates() throws Exception {

        int simulateDupsCount = (getRootFolderNum() / 2) + 1;

        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("*:*");
        solrQuery.setFilterQueries("depthFromRoot:0");
        solrQuery.setStart(0);
        solrQuery.setRows(simulateDupsCount);

        QueryResponse resp = solrClient.query("CatFolders", solrQuery, SolrRequest.METHOD.POST);
        List<SolrInputDocument> newDocs = new ArrayList<>();
        if (resp != null && resp.getResults() != null) {
            Map<String, String> oldToNewFolder = new HashMap<>();
            resp.getResults().forEach(r -> {
                SolrInputDocument doc = new SolrInputDocument();
                Long oldId = Long.valueOf((String) r.getFieldValue("id").toString());
                Long id = oldId + CID_OFFSET + rand.nextInt(40000);
                for (String fieldName : r.getFieldNames()) {
                    if (!"_version_".equals(fieldName) && !"acl_read".equals(fieldName)) {
                        doc.addField(fieldName, r.getFieldValue(fieldName));
                    }
                }
                doc.setField("id", id);
                doc.setField("parentsInfo", Lists.newArrayList(PARENT_INFO_PREFIX+id));
                newDocs.add(doc);
                oldToNewFolder.put(String.valueOf(oldId), String.valueOf(id));
            });

            if (newDocs.isEmpty()) {
                System.out.println("could not create simulated folders as no existing in solr");
            } else {
                UpdateResponse updateResp = solrClient.add("CatFolders", newDocs, 1000);
                System.out.println(String.format("Created %d new records. Status=%d", newDocs.size(), updateResp.getStatus()));

                oldToNewFolder.entrySet().forEach(entry -> {
                    String oldId = entry.getKey();
                    String newId = entry.getValue();

                    try {
                        // move sub folders
                        moveSubFolders(oldId, newId);

                        // move files
                        moveFiles(oldId, newId);
                    } catch (Exception e) {
                        System.out.println("could not move folders subs "+e.getMessage());
                        e.printStackTrace();
                    }
                });

                solrClient.commit("CatFolders");
                solrClient.commit("CatFiles");
            }
        }
    }

    private int getRootFolderNum() throws Exception {
        int rootFolderNum = 0;

        try (Statement stmt = connection.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("select count(1) from root_folder");

            if (resultSet.next()) {
                rootFolderNum = resultSet.getInt(1);
            }
            resultSet.close();
        }

        return rootFolderNum;
    }

    private static final String PARENT_INFO_PREFIX = "0.";
}
