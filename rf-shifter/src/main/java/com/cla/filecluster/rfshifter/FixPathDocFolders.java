package com.cla.filecluster.rfshifter;

import com.cla.filecluster.util.ExternalToolUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Tool to fix badly migrated doc folders
 * path should start with /
 * need to also remove the duplicate folders created and attach the files to original folders
 *
 * Created by: yael
 * Created on: 2/24/2019
 */
public class FixPathDocFolders {

    private static final String UPDATE_VALS_REQUEST_HANDLER = "/update/vals";

    private final Connection connection;
    private final SolrClient solrClient;
    private static int pageSize = 10000;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        boolean configSet = false;
        boolean pageSizeSet = false;
        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if (pageSizeSet) {
                pageSizeSet = false;
                pageSize = Integer.parseInt(args[argsInx]);
            } else if ("-p".equals(args[argsInx])) {
                pageSizeSet = true;
            } else if (!configSet){
                configPath = args[argsInx];
                configSet = true;
            }
        }

        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        new FixPathDocFolders(connection, solrClient).fixPathDocFolder();
    }

    private FixPathDocFolders(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    private void fixPathDocFolder() throws Exception {
        try {
            System.out.println("============= Executing fix doc folder oath ===========");
            System.out.println();
            searchDocFolderAndFix();
            fixRealPathTopLevelFolder();
            System.out.println();
            System.out.println("===============  Delete fix ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void searchDocFolderAndFix() throws Exception {

        int cycleNum = 1;
        int numFound;

        do {
            // find doc folders with bad path (in pages)
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFields("id", "path", "realPath","rootFolderId","name","depthFromRoot","parentsInfo");
            solrQuery.addFilterQuery("-path:\\/*");
            solrQuery.setRows(pageSize);
            solrQuery.setStart(0);
            
            QueryResponse resp = solrClient.query("CatFolders", solrQuery);
            if (resp != null && resp.getResults() != null) {
                long total = ((SolrDocumentList) resp.getResponse().get("response")).getNumFound();
                numFound = resp.getResults().size();
                System.out.println(String.format("-- content cycle %d handling %d left %d", cycleNum, numFound, total));

                List<String> folderIdsToDel = new ArrayList<>();
                List<FileFixData> fileFixData = new ArrayList<>();
                List<SolrInputDocument> docFolderFix = new ArrayList<>();
                List<SolrInputDocument> filesFix = new ArrayList<>();

                for (SolrDocument doc : resp.getResults()) {

                    // search for the folder double
                    SolrQuery solrQueryDoubleSearch = new SolrQuery();
                    solrQueryDoubleSearch.setQuery("*:*");
                    solrQueryDoubleSearch.setFields("id", "path", "realPath");
                    solrQueryDoubleSearch.addFilterQuery("-id:" +doc.getFieldValue("id"));
                    solrQueryDoubleSearch.addFilterQuery("rootFolderId:" +doc.getFieldValue("rootFolderId"));
                    solrQueryDoubleSearch.addFilterQuery("depthFromRoot:" +doc.getFieldValue("depthFromRoot"));
                    solrQueryDoubleSearch.addFilterQuery("name:\"" +doc.getFieldValue("name")+"\"");
                    QueryResponse respDoubleSearch = solrClient.query("CatFolders", solrQueryDoubleSearch);
                    if (respDoubleSearch != null && respDoubleSearch.getResults() != null) {
                        for (SolrDocument docDuplicateSearch : respDoubleSearch.getResults()) {
                            if (docDuplicateSearch.getFieldValue("realPath").equals(doc.getFieldValue("realPath"))) {
                                // found duplicate mark to delete and save info to fix files
                                folderIdsToDel.add(String.valueOf(docDuplicateSearch.get("id")));
                                fileFixData.add(new FileFixData((Long)docDuplicateSearch.get("id"),
                                        (Long)doc.get("id"), (List<String>)doc.get("parentsInfo")));
                            }
                        }

                        // create fix doc for folder path
                        SolrInputDocument docFolderFixRecord = new SolrInputDocument();
                        docFolderFixRecord.setField("id", doc.get("id"));

                        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
                        fieldModifier.put("set", "/"+doc.get("path"));
                        docFolderFixRecord.addField("path", fieldModifier);

                        docFolderFix.add(docFolderFixRecord);
                    }
                }

                // fix files folder id and parent info
                if (fileFixData.size() > 0) {
                    for (FileFixData data : fileFixData) {

                        // get all files to fix
                        SolrQuery solrQueryFileFixSearch = new SolrQuery();
                        solrQueryFileFixSearch.setQuery("*:*");
                        solrQueryFileFixSearch.setFields("id");
                        solrQueryFileFixSearch.addFilterQuery("folderId:" +data.currentFolderId);
                        QueryResponse respFileSearch = solrClient.query("CatFiles", solrQueryFileFixSearch);
                        if (respFileSearch != null && respFileSearch.getResults() != null) {
                            for (SolrDocument docFileSearch : respFileSearch.getResults()) {

                                // create fix document
                                SolrInputDocument fileFixRecord = new SolrInputDocument();
                                fileFixRecord.setField("id", docFileSearch.get("id"));

                                Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
                                fieldModifier.put("set", data.fixFolderId);
                                fileFixRecord.addField("folderId", fieldModifier);

                                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                                fieldModifier.put("set", data.fixParentInfo);
                                fileFixRecord.addField("parentFolderIds", fieldModifier);

                                filesFix.add(fileFixRecord);
                            }
                        }
                    }

                    // save fix to files 
                    if (filesFix.size() > 0) {
                        solrClient.add("CatFiles", filesFix);
                        System.out.println(String.format("-- Fixfile records %d", filesFix.size()));
                        solrClient.commit("CatFiles", false, true, true);
                    }
                }

                // delete duplicate folders
                if (folderIdsToDel.size() > 0) {
                    solrClient.deleteById("CatFolders", folderIdsToDel);
                    System.out.println(String.format("-- Delete doc folder records %d", folderIdsToDel.size()));
                }

                // update doc folder path
                if (docFolderFix.size() > 0) {
                    solrClient.add("CatFolders", docFolderFix);
                    System.out.println(String.format("-- Fix doc folder records %d", docFolderFix.size()));
                }

                solrClient.commit("CatFolders", false, true, true);

                cycleNum++;
            } else {
                numFound = 0;
            }

        } while (numFound == pageSize);

    }

    private void fixRealPathTopLevelFolder() throws Exception {
        int total = solrUpdateFieldByQuery("realPath",
                "", "CatFolders",
                Lists.newArrayList("depthFromRoot:0", "pathDescriptorType:WINDOWS"));

        System.out.println(String.format("-- Done fix realPath for top level doc folders: %d updated", total));

        total = solrUpdateFieldByQuery("mediaEntityId",
                "", "CatFolders",
                Lists.newArrayList("depthFromRoot:0", "pathDescriptorType:WINDOWS"));

        System.out.println(String.format("-- Done fix mediaEntityId for top level doc folders: %d updated", total));
    }

    private Integer solrUpdateFieldByQuery(String field, String value, String collection, List<String> fq) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);
        solrQuery.setQuery("*:*");
        fq.forEach(solrQuery::addFilterQuery);
        solrQuery.set("field", field);
        if (value != null) {
            solrQuery.set("value", value);
        }
        solrQuery.set("update.op", "set");
        QueryResponse resp = this.solrClient.query(collection, solrQuery, SolrRequest.METHOD.POST);
        Integer cnged = resp.getResponse().get("docsChanged") == null ? null :
                (Integer) resp.getResponse().get("docsChanged");
        return cnged;
    }

    private class FileFixData {
        Long currentFolderId;
        Long fixFolderId;
        List<String> fixParentInfo;

        public FileFixData(Long currentFolderId, Long fixFolderId, List<String> fixParentInfo) {
            this.currentFolderId = currentFolderId;
            this.fixFolderId = fixFolderId;
            this.fixParentInfo = fixParentInfo;
        }
    }
}
