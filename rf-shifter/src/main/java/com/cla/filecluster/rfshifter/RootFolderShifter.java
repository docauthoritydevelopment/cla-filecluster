package com.cla.filecluster.rfshifter;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.*;

/**
 * Created by: yael
 * Created on: 12/23/2018
 */
public class RootFolderShifter {

    private static final String INSERT_NEW_ROOT_FOLDER_STATEMENT = "INSERT INTO root_folder " +
            " (id, base_depth, deleted, failure_getting_share_permission, crawler_type, description, " +
            "file_types,files_count,folders_count,media_entity_id,media_type," +
            "nick_name,path,path_descriptor_type,real_path,reingest_time_stamp,rescan_active,scan_task_depth," +
            "scanned_files_count_cap,store_location,store_purpose,store_rescan_method,store_security,customer_data_center_id,doc_store_include," +
            "media_connection_details) " +
            "SELECT  %d,%d, 0, 0, rf.crawler_type, rf.description, rf.file_types,rf.files_count, rf.folders_count,rf.media_entity_id, rf.media_type, " +
            "'%s', '%s',rf.path_descriptor_type, '%s', %d, rf.rescan_active, rf.scan_task_depth, rf.scanned_files_count_cap, rf.store_location, " +
            "rf.store_purpose, rf.store_rescan_method,rf.store_security,rf.customer_data_center_id,rf.doc_store_include,rf.media_connection_details " +
            "FROM root_folder rf " +
            "WHERE  rf.id = %d";

    private static final String DELETE_OLD_RF_NONE_FINISHED_CRAWLS_RUNS = "delete rf, cr, jb, t from root_folder rf\n" +
            "join crawl_runs cr on cr.root_folder_id = rf.id\n" +
            "join jm_jobs jb on jb.run_context = cr.id\n" +
            "left join jm_tasks t on t.job_id = jb.id\n" +
            "where cr.root_folder_id =  %d and cr.run_status <> 'FINISHED_SUCCESSFULLY'";

    private static final String DELETE_OLD_RF_STATMENT = "delete rf from root_folder rf where rf.id =  %d";

    private static final String SHIFT_RF_SCHEDULER_STATEMENT = "update root_folder_schedule rfs " +
            "set rfs.root_folder_id = %d\n" +
            "where rfs.root_folder_id = %d";

    private static final String DELETE_FROM_ROOT_FOLDER_SCHEDULE_PREVIOUS_RF = "Delete rfs from root_folder_schedule rfs where rfs.root_folder_id= %d";


    private static final String SELECT_NEW_RF_AND_DOC_STORE_STATEMENT = "select max(rf.id) + 1,rf.doc_store_include from root_folder rf";

    private static final String SELECT_NEW_RF_DETAILS_STATEMENT = "Select rf.id,rf.doc_store_include,rf.path,rf.real_path,rf.media_type from root_folder rf where rf.real_path = '%s'";

    private static final String SELECT_RF_ID_BY_RF_REAL_PATH_STATEMENT = "select rf.id,media_type from root_folder rf where rf.real_path = '%s'";

    private static final String SELECT_ROOT_FOLDER_SCHEDULE_RFS_BY_RF_ID_STATMENT = "Select * from root_folder_schedule rfs where rfs.root_folder_id = %d";

    private static final String SHIFT_CRAWL_RUNS_STATEMENT = "Update crawl_runs cr\n" +
            "set cr.root_folder_id = %d,\n" +
            "cr.run_config='%s'\n" +
            "where cr.id=%d";
    private static final String SELECT_CRAWL_RUNS_BY_RF_ID_STATEMENT = "Select * from crawl_runs cr where cr.root_folder_id=%d";

    private static final String SHIFT_EXCLUDE_RULES_TO_NEW_RF_STATEMENT = "Update folder_exclude_rule fer\n" +
            "set fer.root_folder_id = %d\n" +
            "where fer.root_folder_id=%s";

    private static final String SHIFT_SHARE_PERM_TO_NEW_RF_STATEMENT = "Update root_folder_share_permission fer\n" +
            "set fer.root_folder_id = %d\n" +
            "where fer.root_folder_id=%s";

    private static final String HAS_ACTIVE_RUNS_STATEMENT = "select count(cr.id) from crawl_runs cr\n" +
            "join root_folder rf on cr.root_folder_id = rf.id\n" +
            "where rf.real_path = '%s' and cr.run_status in ('RUNNING','PAUSED')";

    private static final String UPDATE_VALS_REQUEST_HANDLER = "/update/vals";

    private static final String RF_ID = "rf_id";
    private static final String DOC_STORE_ID = "doc_store_id";
    private static final String RF_PATH = "rf_path";
    private static final String RF_REAL_PATH = "rf_real_path";
    private static final String RF_MEDIA_TYPE = "rf_media_type";

    private final Connection connection;
    private final SolrClient solrClient;
    private final String oldRfPath;
    private final String newRfPath;
    private Integer newRFBaseDepth;
    private String newRFNickName;
    private String pathDiff;

    public static void main(String[] args) throws Exception {
        if (args.length < 2 || args.length > 3) {
            showUsage();
            System.exit(-1);
        }
        String configPath = "./config";
        String oldRfPath;
        String newRfPath;

        if (args.length == 3) { // Case config path specified
            configPath = args[0];
            oldRfPath = args[1];
            newRfPath = args[2];
        } else {// Otherwise assume it is under ./config
            oldRfPath = args[0];
            newRfPath = args[1];
        }

        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        silentThirdPartyLoggers();

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.print(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.print(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.print(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.print(" DONE!");

        System.out.println();
        System.out.println();

        // Shift RFs
        new RootFolderShifter(connection, solrClient, oldRfPath, newRfPath).shift();
    }

    private static void silentThirdPartyLoggers() {
        Logger logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.ERROR);
    }

    private static void showUsage() {
        System.err.println("Usage: rf_shifter.bat [old-rf-path] [new-rf-path]");
    }

    public RootFolderShifter(Connection connection, SolrClient solrClient, String oldRfPath, String newRfPath) {
        this.connection = connection;
        this.solrClient = solrClient;
        this.oldRfPath = isHttp(oldRfPath) ? oldRfPath : FilenameUtils.normalizeNoEndSeparator(oldRfPath);
        this.newRfPath = isHttp(newRfPath) ? newRfPath : FilenameUtils.normalizeNoEndSeparator(newRfPath);
        this.pathDiff = this.oldRfPath.substring(this.newRfPath.length());
        this.newRFBaseDepth = depthCount(newRfPath);
        this.newRFNickName = FilenameUtils.getName(this.newRfPath);
    }

    private boolean isHttp(String path) {
        return path != null && (path.startsWith("http://") || path.startsWith("https://"));
    }

    private Integer depthCount(String newRfPath) {
        return StringUtils.countMatches(ExternalToolUtils.convertPathToUniversalString(newRfPath), "/") - 1;
    }

    private void shift() throws Exception {
        try {
            System.out.println(String.format("============= Executing shift root folder command from %s to %s ===========", oldRfPath, newRfPath));
            System.out.println();
            assertOldAndNewRfPaths();
            shiftToNewRf();
            System.out.println();
            System.out.println("===============================  Shift Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void assertOldAndNewRfPaths() throws SQLException {
        if (StringUtils.isBlank(oldRfPath)) {
            throw new RuntimeException("Old RF path is blank!!");
        }
        if (StringUtils.isBlank(newRfPath)) {
            throw new RuntimeException("New RF path is blank!!");
        }

        if (!oldRfPath.startsWith(newRfPath)) {
            throw new RuntimeException(String.format("\nNew RF path (%s)\n is not part of Old RF path (%s)", newRfPath, oldRfPath));
        }

        if (oldRfPath.equals(newRfPath)) {
            throw new RuntimeException(String.format("\nNew RF path (%s)\n is the same as Old RF path (%s)", newRfPath, oldRfPath));
        }

        if (hasActiveRun(oldRfPath)) {
            throw new RuntimeException(String.format("\nCannot run till RF (%s) scan is completed", oldRfPath));
        }

        if (hasActiveRun(newRfPath)) {
            throw new RuntimeException(String.format("\nCannot run till RF (%s) scan is completed", newRfPath));
        }
    }

    private boolean hasActiveRun(String path) throws SQLException {
        boolean activeRun;
        Statement stmt = connection.createStatement();
        try {
            ResultSet resultSet = stmt.executeQuery(String.format(HAS_ACTIVE_RUNS_STATEMENT, escapeBackwardSeparator(path)));
            resultSet.next();
            activeRun = resultSet.getLong(1) > 0;
            resultSet.close();
        } finally {
            stmt.close();
        }
        return activeRun;
    }

    private void shiftToNewRf() throws Exception {
        Statement stmt = connection.createStatement();
        try {
            stmt.execute("START TRANSACTION;");
            Pair<Long, Integer> oldRf = getOldRfId();
            Long oldRfId = oldRf.getKey();
            Map<String, Object> newRootFolder = insertNewRootFolder(oldRfId, oldRf.getValue());
            List<String> newParentFolders = insertNewParentFolders(oldRfId, newRootFolder);
            shiftDocFoldersToNewRF(newParentFolders, oldRfId, (Long) newRootFolder.get(RF_ID), (Integer)newRootFolder.get(RF_MEDIA_TYPE));
            shiftFilesToNewRF(oldRfId, (Long) newRootFolder.get(RF_ID));
            shiftFoldersToNewRF(oldRfId, (Long) newRootFolder.get(RF_ID));
            shiftSchedulerToNewRF(oldRfId, (Long) newRootFolder.get(RF_ID));
            deleteOldRFNoneFinishedScans(oldRfId);
            shiftCrawledRunsToNewRF(oldRfId, (Long) newRootFolder.get(RF_ID));
            shiftExcludeRules(oldRfId, (Long) newRootFolder.get(RF_ID));
            shiftSharePerm(oldRfId, (Long) newRootFolder.get(RF_ID));
            deleteOldRf(oldRfId);
            stmt.execute("COMMIT;");
            new RootFolderShifter.OldAndNewRfId(oldRfId, (Long) newRootFolder.get(RF_ID));
        } catch (Exception e) {
            stmt.execute("ROLLBACK;");
            throw e;
        } finally {
            stmt.close();
        }
    }

    private void shiftFilesToNewRF(long oldRfId, long newRFId) throws Exception {
        Integer total = solrUpdateFieldByQuery("rootFolderId",
                String.valueOf(newRFId), "CatFiles",
                Lists.newArrayList("rootFolderId:"+oldRfId));

        solrClient.commit("CatFiles");

        System.out.println(String.format("-- Done files shifting: %d updated", total));
    }

    private void shiftFoldersToNewRF(long oldRfId, long newRFId) throws Exception {
        Integer total = solrUpdateFieldByQuery("rootFolderId",
                String.valueOf(newRFId), "CatFolders",
                Lists.newArrayList("rootFolderId:"+oldRfId));

        solrClient.commit("CatFolders");

        System.out.println(String.format("-- Done leftover folders shifting: %d updated", total));
    }

    private void shiftFoldersToNewFolder(String oldFolderId, String newFolderId) throws Exception {
        Integer total = solrUpdateFieldByQuery("parentFolderId",
                newFolderId, "CatFolders",
                Lists.newArrayList("parentFolderId:"+oldFolderId));

        System.out.println(String.format("-- Done files shifting: %d updated", total));
    }

    private void shiftFilesToNewFolder(String oldFolderId, String newFolderId) throws Exception {
        Integer total = solrUpdateFieldByQuery("folderId",
                newFolderId, "CatFiles",
                Lists.newArrayList("folderId:"+oldFolderId));

        System.out.println(String.format("-- Done files shifting: %d updated", total));
    }

    private void shiftDocFoldersToNewRF(List<String> docFoldersIds, long oldRfId, Long newRfId, int mediaType) throws Exception {
        System.out.println();
        System.out.println(String.format("-- Start Update %d new parent doc folders parents info", docFoldersIds.size()));

        // Shift depth
        Integer depthAddition = depthCount(oldRfPath) - depthCount(newRfPath);
        Integer rowsAffected;
        int currentDepth = 1;
        do {
            Integer newDepth = currentDepth + depthAddition;
            rowsAffected = solrUpdateFieldByQuery(Lists.newArrayList("depthFromRoot", "rootFolderId"),
                    Lists.newArrayList(newDepth.toString(), newRfId.toString()), "CatFolders",
                    Lists.newArrayList("rootFolderId:" + oldRfId, "depthFromRoot:" + currentDepth));

            if (rowsAffected != null && rowsAffected > 0) {
                System.out.println(String.format("   Updated %d doc folders depth and RF id to new RF (path: %s)", rowsAffected, newRfId));
            }
            currentDepth++;
        } while (rowsAffected != null && rowsAffected > 0);
        solrClient.commit("CatFolders");    // Todo: consider a different commit (waitFlush=false)

        // Shift parents info
        List<SolrInputDocument> docs = new ArrayList<>();
        QueryResponse resp = getFoldersForRootFolder(newRfId);
        List<FolderData> folderData = new ArrayList<>();
        Table<Integer, String, List<String>> parentsInfoTable = HashBasedTable.create();
        resp.getResults().forEach(doc -> {
            String dfId = (String) doc.getFieldValue("id");
            Integer depthFromRoot = (Integer) doc.getFieldValue("depthFromRoot");
            List<String> newParentsInfo = new ArrayList<>();
            if (depthFromRoot > 1 && !docFoldersIds.contains(dfId)) {
                String path = (String) doc.getFieldValue("path");
                Long parentFolderId = (Long) doc.getFieldValue("parentFolderId");

                FolderData data = new FolderData();
                data.id = dfId;
                data.path = path;
                data.realPath = (String) doc.getFieldValue("realPath");
                data.mediaEntityId = (String) doc.getFieldValue("mediaEntityId");

                // calc parent info
                if (parentFolderId == null || parentFolderId == 0) {
                    newParentsInfo.add("0." + dfId);
                    parentsInfoTable.put(depthFromRoot, dfId, newParentsInfo);
                } else {
                    List<String> parentsInfo = parentsInfoTable.get(depthFromRoot - 1, parentFolderId);
                    if (parentsInfo != null) {
                        newParentsInfo.addAll(parentsInfo);
                    }
                    newParentsInfo.add(depthFromRoot + "." + dfId);
                    parentsInfoTable.put(depthFromRoot, dfId, newParentsInfo);
                }
                data.parentInfo = newParentsInfo;

                folderData.add(data);

            } else if (depthFromRoot > 0 && docFoldersIds.contains(dfId)) {
                Long parentFolderId = (Long) doc.getFieldValue("parentFolderId");
                List<String> parentsInfo = parentsInfoTable.get(depthFromRoot - 1, parentFolderId);
                if (parentsInfo != null) {
                    newParentsInfo.addAll(parentsInfo);
                }
                newParentsInfo.add(depthFromRoot + "." + dfId);
                SolrInputDocument docToAdd = new SolrInputDocument();
                docToAdd.setField("id", dfId);
                Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put("set", newParentsInfo);
                docToAdd.addField("parentsInfo", fieldModifier);
                docs.add(docToAdd);
                parentsInfoTable.put(depthFromRoot, dfId, newParentsInfo);
            } else {
                newParentsInfo = (List<String>) doc.getFieldValue("parentsInfo");
                parentsInfoTable.put(depthFromRoot, dfId, newParentsInfo);
            }

            try {
                updateParentsInfoForDocumentsInSolr(dfId, newParentsInfo);
            } catch (Exception e) {
                System.out.println("problem for folder " + dfId + " on updateParentsInfoForDocumentsInSolr with parent info " + newParentsInfo);
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });

        String pathDiffNorm = mediaType == 0 ? pathDiff.replace("\\", "/") : pathDiff;
        pathDiffNorm = mediaType == 0 ? pathDiffNorm.replace("//", "/") : pathDiffNorm;
        pathDiffNorm = ((pathDiffNorm.startsWith("/") ||  mediaType != 0) ? pathDiffNorm : "/" + pathDiffNorm);

        for (FolderData data : folderData) {
            docs.add(createFolderDocument(data, mediaType, pathDiffNorm, newRfId));
        }

        if (!docs.isEmpty()) {
            solrClient.add("CatFolders", docs);
        }

        solrClient.commit("CatFolders");
        solrClient.commit("CatFiles");

        System.out.println(String.format("-- Done doc folders shifting: %d updated", docs.size()));
    }

    private QueryResponse getFoldersForRootFolder(Long newRfId) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("*:*");
        solrQuery.setFields("id", "depthFromRoot", "parentFolderId", "realPath", "path", "mediaEntityId", "parentsInfo");
        solrQuery.addFilterQuery("rootFolderId:" + newRfId);
        solrQuery.addSort("depthFromRoot", SolrQuery.ORDER.asc);
        return solrClient.query("CatFolders", solrQuery);
    }

    private SolrInputDocument createFolderDocument(FolderData data, int mediaType, String pathDiffNorm, Long newRfId) {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", data.id);

        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", data.parentInfo);
        doc.addField("parentsInfo", fieldModifier);

        String realPath = data.realPath;
        realPath = pathDiff + realPath;
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", realPath);
        doc.addField("realPath", fieldModifier);

        if (mediaType == 0) {
            String mediaEntityId = data.mediaEntityId;
            mediaEntityId = pathDiff + mediaEntityId;
            fieldModifier = Maps.newHashMapWithExpectedSize(1);
            fieldModifier.put("set", mediaEntityId);
            doc.addField("mediaEntityId", fieldModifier);
        }

        String path = data.path;
        path = pathDiffNorm + (path.startsWith("/") && pathDiffNorm.endsWith("/") ? path.substring(1) : path);
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", path);
        doc.addField("path", fieldModifier);

        String hash = folderHashValue(newRfId, path);
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", hash);
        doc.addField("folderHash", fieldModifier);

        return doc;
    }

    class FolderData {
        String path;
        String realPath;
        String id;
        List<String> parentInfo;
        String mediaEntityId;
    }

    private void updateParentsInfoForDocumentsInSolr(String folderId, List<String> parentInfo) throws SolrServerException, IOException {
        int page = 1000;
        SolrQuery query = new SolrQuery();      // Todo: create fresh query for each cycle
        query.setQuery("*:*");
        query.setStart(0);
        query.setRows(page);
        query.addFilterQuery("folderId:" + folderId);
        QueryResponse queryResponse;
        do {
            queryResponse = solrClient.query("CatFiles", query, SolrRequest.METHOD.POST);
            SolrDocumentList results = queryResponse.getResults();
            List<SolrInputDocument> parentIndoUpdates = new LinkedList<>();
            for (SolrDocument solrDocument : results) {
                parentIndoUpdates.add(createFileUpdDoc(solrDocument, parentInfo, folderId));
            }
            if (!parentIndoUpdates.isEmpty()) {
                System.out.println(String.format("      Shifting %d documents for %s", parentIndoUpdates.size(), folderId));
                solrClient.add("CatFiles", parentIndoUpdates);
            }
            query.setStart(query.getStart() + page);        // Todo: move to deep paging
        } while (queryResponse.getResults().size() > 0);
    }

    private SolrInputDocument createFileUpdDoc(SolrDocument solrDocument, List<String> parentInfo, String folderId) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        solrInputDocument.setField("id", solrDocument.getFieldValue("id"));

        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", parentInfo);
        solrInputDocument.setField("parentFolderIds", fieldModifier);

        int mediaType = (Integer)solrDocument.getFieldValue("mediaType");

        String path = (String)solrDocument.getFieldValue("fullPath");
        path = (mediaType == 0 ? pathDiff.replace("\\", "/") : pathDiff) + path;
        fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", path);
        solrInputDocument.setField("fullPath", fieldModifier);

        String realPath = (String)solrDocument.getFieldValue("fullName");
        realPath = pathDiff + realPath;
        fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", realPath);
        solrInputDocument.setField("fullName", fieldModifier);

        fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", realPath.toLowerCase());
        solrInputDocument.setField("fullNameSearch", fieldModifier);

        fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", ExternalToolUtils.getParentNameOnly(realPath.toLowerCase()));
        solrInputDocument.setField("folderName", fieldModifier);

        String fileNameHashed = ExternalToolUtils.getHashedFileName(realPath);
        fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", fileNameHashed);
        solrInputDocument.setField("fileNameHashed", fieldModifier);

        if (mediaType == 0) {
            String mediaEntityId = (String)solrDocument.getFieldValue("mediaEntityId");
            mediaEntityId = pathDiff + mediaEntityId;
            fieldModifier = new HashMap<>(1);
            fieldModifier.put("set", realPath);
            solrInputDocument.setField("mediaEntityId", mediaEntityId);
        }
        return solrInputDocument;
    }

    private SolrInputDocument handleDocFolderFirstLevelFix(SolrDocument folderRecord, Long newRfId) {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", folderRecord.getFieldValue("id"));

        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", newRfId);
        doc.addField("rootFolderId", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", folderHashValue(newRfId, (String)folderRecord.getFieldValue("path")));
        doc.addField("folderHash", fieldModifier);

        Integer depthAddition = depthCount(oldRfPath) - depthCount(newRfPath);
        Integer absoluteDepth = (Integer)folderRecord.getFieldValue("absoluteDepth");
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", absoluteDepth-depthAddition);
        doc.addField("absoluteDepth", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", null);
        doc.addField("pathDescriptorType", fieldModifier);
        doc.addField("aclSignature", fieldModifier);
        doc.addField("acl_write", fieldModifier);
        doc.addField("acl_read", fieldModifier);
        doc.addField("acl_denied_read", fieldModifier);
        doc.addField("acl_denied_write", fieldModifier);

        return doc;
    }

    private List<String> insertNewParentFolders(Long oldRfId, Map<String, Object> newRootFolder) throws Exception {
        int mediaType = (Integer)newRootFolder.get(RF_MEDIA_TYPE);
        List<String> result = new LinkedList<>();
        String parentId = null;
        List<String> newPaths = extractNewPaths(mediaType);
        List<SolrInputDocument> docs = new ArrayList<>();

        // get parent folder
        Object pathDescriptorType = null, aclSignature = null, acl_read = null, acl_write = null, acl_denied_read = null, acl_denied_write = null;
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery("*:*");
        solrQuery.addFilterQuery("rootFolderId:"+oldRfId, "-parentFolderId:*");
        QueryResponse resp = solrClient.query("CatFolders", solrQuery);
        String wasFolderId = null; String toFolderId = null;
        if (resp.getResults().size() == 1) {
            SolrDocument d = resp.getResults().get(0);

            // create upd record
            SolrInputDocument doc = handleDocFolderFirstLevelFix(d, (Long)newRootFolder.get(RF_ID));

            // get parent id for new paths and id to shift others to
            wasFolderId = (String)d.getFieldValue("id");
            parentId = d.getFieldValue("id").toString();

            // get data that belongs to the lowest child created now as we switch folders
            pathDescriptorType =  d.getFieldValue("pathDescriptorType");
            aclSignature =  d.getFieldValue("aclSignature");
            acl_read =  d.getFieldValue("acl_read");
            acl_write =  d.getFieldValue("acl_write");
            acl_denied_read =  d.getFieldValue("acl_denied_read");
            acl_denied_write =  d.getFieldValue("acl_denied_write");

            docs.add(doc);
        } else if (resp.getResults().size() == 0) {
            return result;
        }

        System.out.println();
        System.out.println(String.format("-- Start creation of %d doc folders", newPaths.size()));

        int i = 0;
        for (String path : newPaths) {
            i++;
            String newDfId = getNewDfId();
            String suggestedDfId = String.valueOf(newDfId);
            SolrInputDocument doc = insertNewParentFolder(
                    path,
                    newRootFolder,
                    parentId,
                    suggestedDfId,
                    mediaType
            );

            Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
            fieldModifier.put("set", pathDescriptorType);
            doc.addField("pathDescriptorType", fieldModifier);

            if (i == newPaths.size()) {
                toFolderId = newDfId;

                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put("set", aclSignature);
                doc.addField("aclSignature", fieldModifier);

                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put("set", acl_read);
                doc.addField("acl_read", fieldModifier);

                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put("set", acl_write);
                doc.addField("acl_write", fieldModifier);

                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put("set", acl_denied_read);
                doc.addField("acl_denied_read", fieldModifier);

                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put("set", acl_denied_write);
                doc.addField("acl_denied_write", fieldModifier);
            }

            docs.add(doc);
            System.out.println(String.format("    Created (path: %s, id: %s, parentId: %s)", path, newDfId, parentId));
            parentId = String.valueOf(newDfId);
            result.add(newDfId);
        }

        if (!docs.isEmpty()) {
            solrClient.add("CatFolders", docs);
        }

        // move children to new parent
        if (toFolderId != null && wasFolderId != null) {
            shiftFoldersToNewFolder(wasFolderId, toFolderId);
            shiftFilesToNewFolder(wasFolderId, toFolderId);
        }

        solrClient.commit("CatFolders");
        solrClient.commit("CatFiles");

        System.out.println(String.format("-- Done creation of %d doc folders", newPaths.size()));
        return result;
    }

    private SolrInputDocument insertNewParentFolder(String path, Map<String, Object> newRootFolder, String parentId, String dfId, int mediaType) {
        long newRfId = (long) newRootFolder.get(RF_ID);

        String newRfPathDb = (String)newRootFolder.get(RF_PATH);
        String newRfRealPathDb = (String)newRootFolder.get(RF_REAL_PATH);

        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", dfId);

        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", newRfId);
        doc.addField("rootFolderId", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", String.valueOf(depthCount(path)));
        doc.addField("absoluteDepth", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", false);
        doc.addField("deleted", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", String.valueOf(depthCount(path) - newRFBaseDepth));
        doc.addField("depthFromRoot", fieldModifier);

        String mediaEntityId = isHttp(path) ? "" : (path);
        if (mediaEntityId.startsWith(newRfRealPathDb)) {
            mediaEntityId = mediaEntityId.substring(newRfRealPathDb.length());
        }
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", isHttp(path) ? null : mediaEntityId);
        doc.addField("mediaEntityId", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", name(path));
        doc.addField("name", fieldModifier);

        String folderPath = path;
        if (mediaType == 0) {
            folderPath = ExternalToolUtils.normalizeFileSharePath(folderPath);
        }
        folderPath = ExternalToolUtils.convertPathToUniversalString(folderPath);
        if (folderPath.startsWith(newRfPathDb)) {
            folderPath = folderPath.substring(newRfPathDb.length());
        }
        folderPath = (folderPath.startsWith("/") ? folderPath : ("/"+folderPath));
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", folderPath);
        doc.addField("path", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", folderHashValue(newRfId, folderPath));
        doc.addField("folderHash", fieldModifier);

        String folderRealPath = (path);
        if (folderRealPath.startsWith(newRfRealPathDb)) {
            folderRealPath = folderRealPath.substring(newRfRealPathDb.length());
        }
        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", folderRealPath);
        doc.addField("realPath", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", parentId);
        doc.addField("parentFolderId", fieldModifier);

        fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", 0);
        doc.addField("type", fieldModifier);

        return doc;
    }

    private void shiftExcludeRules(long oldRfId, Long newRfId) throws SQLException {
        Statement stmt = connection.createStatement();
        System.out.println();
        System.out.print(String.format("-- Start shifting exclude rules to new RF (id: %d, path: %s): ...", newRfId, newRfPath));
        int rowsAffected;
        try {
            stmt.executeUpdate(String.format(SHIFT_EXCLUDE_RULES_TO_NEW_RF_STATEMENT, newRfId, oldRfId));
            rowsAffected = stmt.getUpdateCount();
        } finally {
            stmt.close();
        }
        System.out.print(String.format(" DONE!! (shifted %d exclude rules)", rowsAffected));
        System.out.println();
    }

    private void shiftSharePerm(long oldRfId, Long newRfId) throws SQLException {
        Statement stmt = connection.createStatement();
        System.out.println();
        System.out.print(String.format("-- Start shifting share perm to new RF (id: %d, path: %s): ...", newRfId, newRfPath));
        int rowsAffected;
        try {
            stmt.executeUpdate(String.format(SHIFT_SHARE_PERM_TO_NEW_RF_STATEMENT, newRfId, oldRfId));
            rowsAffected = stmt.getUpdateCount();
        } finally {
            stmt.close();
        }
        System.out.print(String.format(" DONE!! (shifted %d share perm)", rowsAffected));
        System.out.println();
    }

    private void deleteOldRf(long oldRfId) throws SQLException {
        Statement stmt = connection.createStatement();
        try {
            stmt.executeUpdate(String.format(DELETE_OLD_RF_STATMENT, oldRfId));
            System.out.println();
            System.out.println(String.format("-- Deleted previous root folder (path: %s, id:%s)", oldRfPath, oldRfId));
        } finally {
            stmt.close();
        }
    }

    private void shiftCrawledRunsToNewRF(long oldRfId, Long newRFId) throws SQLException, IOException {
        Statement stmt = connection.createStatement();
        Map<Long, String> updateRunConfigMap = new HashMap<>();
        try {
            System.out.println();
            System.out.println(String.format("-- Start crawl runs shifting of %s to new root folder (path: %s)", oldRfPath, newRfPath));
            ResultSet resultSet = stmt.executeQuery(String.format(SELECT_CRAWL_RUNS_BY_RF_ID_STATEMENT, oldRfId));

            while (resultSet.next()) {
                String runConfig = resultSet.getString("run_config");
                updateRunConfigMap.put(resultSet.getLong("id"), adjustRunConfig(runConfig, newRFId));
            }
            resultSet.close();
            for (Map.Entry<Long, String> entry : updateRunConfigMap.entrySet()) {
                stmt.executeUpdate(String.format(SHIFT_CRAWL_RUNS_STATEMENT,
                        newRFId,
                        entry.getValue(),
                        entry.getKey()));
            }
        } finally {
            stmt.close();
        }
        System.out.println(String.format("-- Done crawl runs shifting: %d updated", updateRunConfigMap.size()));
    }

    private String adjustRunConfig(String runConfig, Long newRFId) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(runConfig, ObjectNode.class);
        ((ObjectNode) jsonNode).put("id", newRFId);
        ((ObjectNode) jsonNode).put("path", escapeBackwardSeparator(ExternalToolUtils.convertPathToUniversalString(newRfPath)));
        ((ObjectNode) jsonNode).put("realPath", escapeBackwardSeparator(newRfPath));
        ((ObjectNode) jsonNode).put("nickName", newRFNickName);
        return objectMapper.writeValueAsString(jsonNode);
    }

    private void shiftSchedulerToNewRF(Long oldRfId, Long newRfId) throws SQLException {
        Statement stmt = connection.createStatement();
        try {
            ResultSet resultSet = stmt.executeQuery(String.format(SELECT_ROOT_FOLDER_SCHEDULE_RFS_BY_RF_ID_STATMENT, newRfId));
            if (!resultSet.next()) {
                stmt.executeUpdate(String.format(SHIFT_RF_SCHEDULER_STATEMENT, newRfId, oldRfId));
            } else {
                stmt.executeUpdate(String.format(DELETE_FROM_ROOT_FOLDER_SCHEDULE_PREVIOUS_RF, oldRfId));
            }
            resultSet.close();
        } finally {
            stmt.close();
        }
    }

    private Pair<Long, Integer> getOldRfId() throws SQLException {
        Long rootFolderId;
        Integer mediaType;
        Statement stmt = connection.createStatement();
        try {
            ResultSet resultSet = stmt.executeQuery(String.format(SELECT_RF_ID_BY_RF_REAL_PATH_STATEMENT,
                    escapeBackwardSeparator(oldRfPath)));
            if (resultSet.next()) {
                rootFolderId = resultSet.getLong(1);
                mediaType = resultSet.getInt(2);
                resultSet.close();
            } else {
                throw new RuntimeException(String.format("No root folder found with path %s in root_folders table!!!", oldRfPath));
            }
        } finally {
            stmt.close();
        }

        return Pair.of(rootFolderId, mediaType);
    }

    private String escapeBackwardSeparator(String path) {
        return path.replaceAll("\\\\", "\\\\\\\\");
    }

    private String getNewDfId() throws SQLException {
        Statement stmt = connection.createStatement();
        try {
            stmt.execute("update hibernate_sequences set next_val = next_val + 1 where sequence_name='DOC_FOLDER'");
            ResultSet resultSet = stmt.executeQuery("select next_val from hibernate_sequences s where sequence_name='DOC_FOLDER'");
            resultSet.next();
            long newDfId = resultSet.getLong(1);
            resultSet.close();
            return String.valueOf(newDfId);
        } finally {
            stmt.close();
        }
    }

    private List<String> extractNewPaths(int mediaType) {
        int index = oldRfPath.indexOf(newRfPath) + newRfPath.length();
        String[] segments = oldRfPath.substring(index).split("\\\\");
        List<String> result = new ArrayList<>();
        String newPath = newRfPath;
        for (String segment : segments) {
            if (!"".equals(segment)) {
                newPath += mediaType == 0 ? "\\" + segment : segment;
                result.add(newPath);
            }
        }
        return result;
    }

    private String name(String path) {
        return FilenameUtils.getName(path);
    }

    private String folderHashValue(long newRfId, String path) {
        try {
            String pathString = ExternalToolUtils.convertPathToUniversalString(path);
            return ExternalToolUtils.bytesToHex(MessageDigest.getInstance("MD5")
                    .digest((newRfId + ":" + pathString).getBytes("UTF-8")));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private void deleteOldRFNoneFinishedScans(long oldRfId) throws SQLException {
        int rowsAffected;
        Statement stmt = connection.createStatement();
        try {
            stmt.executeUpdate(String.format(DELETE_OLD_RF_NONE_FINISHED_CRAWLS_RUNS, oldRfId));
            rowsAffected = stmt.getUpdateCount();
        } finally {
            stmt.close();
        }
        System.out.println();
        System.out.println(String.format("-- Deleted %d none finished scans of previous root folder (path: %s, id:%s)", rowsAffected, oldRfPath, oldRfId));
    }

    private Map<String, Object> insertNewRootFolder(long oldRfId, int oldMediaType) throws SQLException {
        Map<String, Object> rootFolder = new HashMap<>();
        Statement stmt = connection.createStatement();
        ResultSet resultSet;
        try {
            resultSet = stmt.executeQuery(String.format(SELECT_NEW_RF_DETAILS_STATEMENT, escapeBackwardSeparator(newRfPath)));
            boolean rfExists = resultSet.next();
            if (rfExists) {
                rootFolder.put(RF_ID, resultSet.getLong(1));
                rootFolder.put(DOC_STORE_ID, resultSet.getLong(2));
                rootFolder.put(RF_PATH, resultSet.getString(3));
                rootFolder.put(RF_REAL_PATH, resultSet.getString(4));
                rootFolder.put(RF_MEDIA_TYPE, resultSet.getInt(5));
                resultSet.close();
            } else {
                resultSet = stmt.executeQuery(SELECT_NEW_RF_AND_DOC_STORE_STATEMENT);
                resultSet.next();
                rootFolder.put(RF_ID, resultSet.getLong(1));
                rootFolder.put(DOC_STORE_ID, resultSet.getLong(2));
                resultSet.close();
                System.out.println(String.format("-- Create new RF (path: %s, id: %d)", newRfPath, rootFolder.get(RF_ID)));

                String newRfPathDb = ExternalToolUtils.convertPathToUniversalString(newRfPath);
                String newRfRealPathDb = (newRfPath);

                rootFolder.put(RF_PATH, newRfPathDb);
                rootFolder.put(RF_REAL_PATH, newRfRealPathDb);
                rootFolder.put(RF_MEDIA_TYPE, oldMediaType);

                stmt.executeUpdate(String.format(INSERT_NEW_ROOT_FOLDER_STATEMENT,
                        rootFolder.get(RF_ID),
                        newRFBaseDepth,
                        newRFNickName,
                        newRfPathDb,
                        escapeBackwardSeparator(newRfRealPathDb),
                        System.currentTimeMillis(),
                        oldRfId));
            }
        } finally {
            stmt.close();
        }
        return rootFolder;
    }

    private Integer solrUpdateFieldByQuery(String field, String value, String collection, List<String> fq) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);
        solrQuery.setQuery("*:*");
        fq.forEach(solrQuery::addFilterQuery);
        solrQuery.set("field", field);
        if (value != null) {
            solrQuery.set("value", value);
        }
        solrQuery.set("update.op", "set");
        QueryResponse resp = this.solrClient.query(collection, solrQuery, SolrRequest.METHOD.POST);
        Integer cnged = resp.getResponse().get("docsChanged") == null ? null :
                (Integer) resp.getResponse().get("docsChanged");
        return cnged;
    }

    private Integer solrUpdateFieldByQuery(List<String> fields, List<String> values, String collection, List<String> fq) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);
        solrQuery.setQuery("*:*");
        fq.forEach(solrQuery::addFilterQuery);

        for (int i = 1; i <= fields.size(); ++i) {
            solrQuery.set("field" + i, fields.get(i - 1));
            solrQuery.set("value" + i, values.get(i - 1));
        }

        solrQuery.set("update.op", "set");
        solrQuery.set("cnt", String.valueOf(fields.size()));
        QueryResponse resp = this.solrClient.query(collection, solrQuery, SolrRequest.METHOD.POST);
        Integer cnged = resp.getResponse().get("docsChanged") == null ? null :
                (Integer) resp.getResponse().get("docsChanged");
        return cnged;
    }

    private static class OldAndNewRfId {

        final long oldRfId;
        final long newRfId;

        private OldAndNewRfId(long oldRfId, long newRfId) {
            this.oldRfId = oldRfId;
            this.newRfId = newRfId;
        }
    }
}
