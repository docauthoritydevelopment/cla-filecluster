package com.cla.filecluster.rfshifter;

import com.cla.filecluster.util.ExternalToolUtils;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Run file Solr fix for missing parent info for files and folders
 * Created by: yael
 * Created on: 2/17/2019
 */
public class MissingParentInfo {

    private static final String UPDATE_VALS_REQUEST_HANDLER = "/update/vals";

    private final Connection connection;
    private final SolrClient solrClient;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";
        boolean configSet = false;

        for (int argsInx = 0; argsInx < args.length; ++argsInx) {
            if (!configSet) {
                configPath = args[argsInx];
                configSet = true;
            }
        }
        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        new MissingParentInfo(connection, solrClient).fixParentInfo();
    }

    private static void showUsage() {
        System.err.println("Usage: solr_parent_info.bat [config path optional]");
    }

    public MissingParentInfo(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    private void fixParentInfo() throws Exception {
        try {
            System.out.println("============= Executing file folder missing parent info solr ===========");
            System.out.println();
            fixParentInfoFilesAndFolders();
            System.out.println();
            System.out.println("===============  Fix Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void fixParentInfoFilesAndFolders() throws Exception {
        List<Long> rootFolderIds = getRootFolderIds();
        for (Long rootFolderId : rootFolderIds) {
            System.out.println("");
            Long docFolderId = getDocFolderIdForRoorFolder(rootFolderId);
            if (docFolderId != null && docFolderId > 0) {
                String value = "0."+docFolderId;
                Integer cnged = solrUpdateFieldByQuery("parentsInfo", value, "CatFolders", Lists.newArrayList("rootFolderId:" + rootFolderId, "-parentsInfo:" + value));
                System.out.println("changed "+cnged+" folder records for root folder "+rootFolderId);
                cnged = solrUpdateFieldByQuery("parentFolderIds", value, "CatFiles", Lists.newArrayList("rootFolderId:" + rootFolderId, "-parentFolderIds:" + value));
                System.out.println("changed "+cnged+" file records for root folder "+rootFolderId);
            } else {
                System.out.println("NOTICE: cant find doc folder in db for root folder "+rootFolderId+" skipping");
            }
        }
    }

    private Integer solrUpdateFieldByQuery(String field, String value, String collection, List<String> fq) throws Exception {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);
        solrQuery.setQuery("*:*");
        fq.forEach(solrQuery::addFilterQuery);
        solrQuery.set("field", field);
        if (value != null) {
            solrQuery.set("value", value);
        }
        solrQuery.set("update.op", "add");
        QueryResponse resp = this.solrClient.query(collection, solrQuery, SolrRequest.METHOD.POST);
        Integer cnged = resp.getResponse().get("docsChanged") == null ? null :
                (Integer) resp.getResponse().get("docsChanged");
        return cnged;
    }

    private Long getDocFolderIdForRoorFolder(Long rootFolderId) throws Exception {
        Long docFolderId = null;

        try (Statement stmt = connection.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("select id from doc_folder where depth_from_root = 0 and root_folder_id = "+ rootFolderId);

            if (resultSet.next()) {
                docFolderId = resultSet.getLong(1);
            }
            resultSet.close();
        }

        return docFolderId;
    }

    private List<Long> getRootFolderIds() throws SQLException {
        List<Long> rootFolderIds = new ArrayList<>();
        try (Statement stmt = connection.createStatement()) {
            ResultSet resultSet = stmt.executeQuery("select id from root_folder");

            while (resultSet.next()) {
                rootFolderIds.add(resultSet.getLong(1));
            }
            resultSet.close();
        }

        return rootFolderIds;
    }
}
