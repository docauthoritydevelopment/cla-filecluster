package com.cla.filecluster.delete;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import com.cla.filecluster.util.ExternalToolUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class FileContentDeleter {

    private int COMMIT_WITHIN_MS = 60000;

    private final Connection connection;
    private final SolrClient solrClient;

    public static void main(String[] args) throws Exception {
        String configPath = "./config";

        if (args.length == 1) {
            configPath = args[0];
        }

        Properties properties = new Properties();

        File propertiesFile = new File(configPath, "application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file (" + propertiesFile.getAbsolutePath() + ")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load properties file (IO Exception)", e);
        }

        // Extract connection properties
        String dbUrl = properties.getProperty("bonecp.url");
        String userName = properties.getProperty("bonecp.username");
        String password = properties.getProperty("bonecp.password");
        String solrUrl = properties.getProperty("spring.data.solr.host");

        // Load solr client
        System.out.println(String.format("-- Loading solr client (%s) ...", solrUrl));
        SolrClient solrClient = ExternalToolUtils.buildSolrClient(solrUrl);
        System.out.println(" DONE!");
        System.out.println();

        // Load mysql connection
        System.out.println(String.format("-- Loading connection (%s) ...", dbUrl));
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                dbUrl, userName, password);
        System.out.println(" DONE!");
        new FileContentDeleter(connection, solrClient).deleteFilesAndContents();
    }

    private static void silentThirdPartyLoggers() {
        Logger logger = (Logger) LoggerFactory.getLogger("org");
        logger.setLevel(Level.ERROR);
    }

    private static void showUsage() {
        System.err.println("Usage: file-content-deleter.bat [config path optional]");
    }

    private FileContentDeleter(Connection connection, SolrClient solrClient) {
        this.connection = connection;
        this.solrClient = solrClient;
    }

    private void deleteFilesAndContents() throws Exception {
        try {
            System.out.println("============= Executing file and content deletion ===========");
            System.out.println();

            validateNoActiveRuns();
            deleteContentRelatedInBatch();
            deleteFiles();

            System.out.println();
            System.out.println("===============  Delete Done ================================");
        } finally {
            connection.close();
            solrClient.close();
        }
    }

    private void deleteFiles() throws Exception {
        System.out.println();
        System.out.println("-- delete files from CatFiles");
        UpdateResponse deleteResp = solrClient.deleteByQuery("CatFiles", "deleted:true", COMMIT_WITHIN_MS);
        System.out.println(String.format("-- deleted from CatFiles status %d", deleteResp.getStatus()));
    }

    private void deleteContentRelatedInBatch() throws Exception {

        int start = 0;
        List<String> contentIdsToDel;
        int cycleNum = 1;
        int pageSize = 5000;

        System.out.println("-- delete contents from all collections START");

        do {
            contentIdsToDel = new ArrayList<>();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.setQuery("*:*");
            solrQuery.setFields("id");
            solrQuery.addFilterQuery("fileCount:0");
            solrQuery.addSort("id", SolrQuery.ORDER.asc);
            solrQuery.setRows(pageSize);
            solrQuery.setStart(start);
            QueryResponse resp = solrClient.query("ContentMetadata", solrQuery);

            if (resp.getResults().size() > 0) {
                System.out.println(String.format("-- content cycle %d", cycleNum));
                for (SolrDocument doc : resp.getResults()) {
                    contentIdsToDel.add((String) doc.getFieldValue("id"));
                }

                String values = contentIdsToDel.stream().collect(
                        Collectors.joining(" ", "(", ")"));

                System.out.println();
                System.out.println(String.format("-- delete %d from AnalysisData", contentIdsToDel.size()));
                UpdateResponse deleteResp = solrClient.deleteById("AnalysisData", contentIdsToDel, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- deleted from AnalysisData status %d", deleteResp.getStatus()));
                System.out.println(String.format("-- delete %d from Extraction", contentIdsToDel.size()));
                deleteResp = solrClient.deleteById("Extraction", contentIdsToDel, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- deleted from Extraction status %d", deleteResp.getStatus()));
                System.out.println(String.format("-- delete %d from PVS", contentIdsToDel.size()));
                deleteResp = solrClient.deleteByQuery("PVS", "fileId:" + values, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- deleted from PVS status %d", deleteResp.getStatus()));
                System.out.println(String.format("-- delete %d from ContentMetadata", contentIdsToDel.size()));
                deleteResp = solrClient.deleteById("ContentMetadata", contentIdsToDel, COMMIT_WITHIN_MS);
                System.out.println(String.format("-- deleted from ContentMetadata status %d", deleteResp.getStatus()));
                cycleNum++;
                start += pageSize;
            }
        } while (contentIdsToDel.size() == pageSize);

        System.out.println("-- delete contents from all collections END");
    }

    private void validateNoActiveRuns() throws SQLException {
        boolean isValid = true;
        try (Statement stmt = connection.createStatement()) {
            System.out.println();
            System.out.println("-- validate no active runs");
            ResultSet resultSet = stmt.executeQuery("select * from crawl_runs where run_status != 'FINISHED_SUCCESSFULLY'");

            if (resultSet.next()) {
                isValid = false;
            }
            resultSet.close();
        }

        if (!isValid) {
            throw new RuntimeException("Active run exists please stop before continuing!!");
        }
    }
}