set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\solr-7.7.2
set SOLR_HOME=%DIRNAME%\solrCloud
set SOLR_JAVA_MEM=-Xms2g -Xmx2g
set ZK_CLIENT_TIMEOUT=60000

if not "%CUSTOM_SOLR_DATA_DIR%" == "" goto dataDirDef

if not exist "D:\Data\SolrData6" goto noData1
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData6"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData6
goto dataDirDef

:noData1
if not exist "%CLA_HOME%\data" goto noData2
echo "CUSTOM SOLR DATA DIR FOUND NEAR SOLR"
set CUSTOM_SOLR_DATA_DIR=%CLA_HOME%\data
goto dataDirDef

:noData2
if not exist "D:\Data\SolrData2" goto noData3
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData2"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData2
goto dataDirDef

:noData3
set CUSTOM_SOLR_DATA_DIR=%SOLR_HOME%
goto dataDirDef

:dataDirDef
echo CUSTOM_SOLR_DATA_DIR=%CUSTOM_SOLR_DATA_DIR%
echo ZK_CLIENT_TIMEOUT=%ZK_CLIENT_TIMEOUT%
call "%CLA_HOME%\bin\solr.cmd" start -z localhost:2181 -p 8983 -c -Denable.runtime.lib=true -Dsolr.data.dir="%CUSTOM_SOLR_DATA_DIR%" -a "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=18983" %1 %2 %3
