package com.cla.connector.analysis.api;

import com.cla.connector.analysis.services.ConnectorAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.ExitRequest;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.commands.Quit;
import reactor.core.scheduler.Schedulers;

@ShellComponent
public class ShutdownExit implements Quit.Command {

    @Autowired
    private ConnectorAnalysisService connectorAnalysisService;

    @ShellMethod(value = "Exit the shell.", key = {"quit", "exit"})
    public void quit() {
        connectorAnalysisService.stopCrawl();
        Schedulers.elastic().dispose();
        throw new ExitRequest();
    }
}
