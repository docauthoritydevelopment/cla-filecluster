package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by oren on 7/18/2018.
 *
 */
public interface ConnectorAdapter {

    List<ServerResourceDto> testConnection();

    Flux<ClaFilePropertiesDto> crawl(String path, boolean ignoreAccessErrors);

    InputStream getInputStream(String file) throws IOException;

    Mono<ClaFilePropertiesDto> getFileAcl(String path);
}
