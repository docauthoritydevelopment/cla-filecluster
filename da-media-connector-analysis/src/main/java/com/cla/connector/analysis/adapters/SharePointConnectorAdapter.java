package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.mediaconnector.microsoft.MSItemKey;
import com.cla.connector.mediaconnector.microsoft.MicrosoftDocAuthorityClient;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointMediaConnector;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by oren on 7/18/2018.
 *
 */
public class SharePointConnectorAdapter extends SharePointMediaConnector implements MicrosoftAdapterBase {
    private static final Logger logger = LoggerFactory.getLogger(SharePointConnectorAdapter.class);
    private static final List<String> SYSTEM_FOLDERS = Lists.newArrayList("/Forms");
    private static final Predicate<String> IS_SYSTEM_FOLDER_PRED =
            path -> SYSTEM_FOLDERS.stream().noneMatch(path::endsWith);


    private SharePointConnectorAdapter(SharePointConnectionParametersDto sharePointConnectionDetailsDto, int maxRetries,
                                       int pageSize, long maxSupportFileSize, MSConnectionConfig config,
                                       int siteCrawlMaxDepth,
                                       String folderToFail,
                                       List<String> foldersToFilter,
                                       boolean isSpecialCharsSupported,
                                       String... charsToFilter) {
        super(sharePointConnectionDetailsDto, null, maxRetries, pageSize, maxSupportFileSize, config, siteCrawlMaxDepth, folderToFail, foldersToFilter, isSpecialCharsSupported, charsToFilter);
    }

    @Override
    public Flux<ClaFilePropertiesDto> crawl(String path, boolean ignoreAccessErrors) {
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(path);
        return crawl(key.getSite(), key.getPath());
    }

    private Flux<ClaFilePropertiesDto> crawl(String subSite, String path) {
        return Mono.fromSupplier(() -> {
                String convertedPath = convertFileNameIfNeeded(path);
                try {
                    return getFolderProperties(subSite, convertedPath).getListId();
                } catch (FileNotFoundException e) {
                    logger.error("Failed to acquire list-id for {}", path, e);
                    throw new RuntimeException(e);
                }
            })
            .defaultIfEmpty(StringUtils.EMPTY)
            .flatMapMany(listId -> StringUtils.isEmpty(listId) ? crawlSite(subSite, path) : crawlFolders(subSite, path, listId));
    }

    private Flux<ClaFilePropertiesDto> crawlFolders(String subSite, String path, String listId) {
        String convertedPath = convertFileNameIfNeeded(path);

        Flux<ClaFilePropertiesDto> fileFlux = Flux.defer(() -> Flux.create(emitter -> {
            streamFiles(-1L, path, subSite, listId, x -> true, emitter::next);
            emitter.complete();
        }));

        Flux<ClaFilePropertiesDto> subFolderCrawlFlux = Flux.defer(() -> Flux.fromIterable(listFoldersUnderLibrary(path, subSite)))
                        .map(ServerResourceDto::getFullName)
                        .filter(IS_SYSTEM_FOLDER_PRED)
                        .flatMap(folderPath -> crawlFolders(subSite, folderPath, listId));

        Mono<ClaFilePropertiesDto> currentDir =
                Mono.create(emitter -> processFolderData(-1L, emitter::success, path, subSite, convertedPath, listId));

        return Flux.concat(currentDir, fileFlux, subFolderCrawlFlux)
                .doOnCancel(() -> microsoftDocAuthorityClient.terminateHttpClient());
    }

    private Flux<ClaFilePropertiesDto> crawlSite(String subSite, String path) {
        String sitePath = SharePointParseUtils.applySiteMark(path, subSite);
        Mono<ClaFilePropertiesDto> siteMono = Mono.fromSupplier(
                () -> {
                    ClaFilePropertiesDto siteProp = ClaFilePropertiesDto.create().setFolder(true);
                    siteProp.setFileName(sitePath);
                    return siteProp;
                });

        Flux<ClaFilePropertiesDto> siteFlux = Flux.just(listSubSites(sitePath))
                .flatMap(Flux::fromIterable)
                .map(site -> SharePointParseUtils.splitPathAndSubsite(site.getFullName()))
                .flatMap(key -> crawl(key.getSite(), key.getPath()));

        Flux<ClaFilePropertiesDto> folderFlux = Flux.just(listDocumentLibraries(subSite))
                .flatMap(Flux::fromIterable)
                .filter(docLib -> IS_SYSTEM_FOLDER_PRED.test(docLib.getFullName()))
                .flatMap(docLib -> crawlFolders(subSite, docLib.getFullName(), docLib.getId()));

        return Flux.concat(siteMono, siteFlux, folderFlux);
    }

    @Override
    public List<ServerResourceDto> testConnection() {
        try {
            super.testConnection();
            System.out.println("Test passed");
        } catch (Exception e) {
            System.out.println("Test failed: " + e.getMessage());
        }
        return null;
    }

    @Override
    public InputStream getInputStream(String filename) throws FileNotFoundException {
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(filename);
        return super.getInputStreamForFileName(key.getPath(), key.getSite(), true);
    }

    public static SharePointConnectorAdapterBuilder getBuilder() {
        return new SharePointConnectorAdapterBuilder();
    }

    @Override
    public String convertFileNameIfNeeded(String path) {
        return super.convertFileNameIfNeeded(path);
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public MicrosoftDocAuthorityClient getMicrosoftDocAuthorityClient() {
        return microsoftDocAuthorityClient;
    }

    public static class SharePointConnectorAdapterBuilder extends MicrosoftConnectorBaseBuilder<SharePointMediaConnector, SharePointConnectorAdapterBuilder> {

        @Override
        protected SharePointConnectorAdapterBuilder getThis() {
            return this;
        }

        @Override
        public SharePointConnectorAdapter build() {
            return new SharePointConnectorAdapter(
                    sharePointConnectionParametersDto,
                    maxRetries,
                    pageSize,
                    maxFileSize,
                    connectionConfig,
                    -1,
                    folderToFail,
                    foldersToFilter,
                    isSpecialCharsSupported,
                    charsToFilter);
        }
    }
}