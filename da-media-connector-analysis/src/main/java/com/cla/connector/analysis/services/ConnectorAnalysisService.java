package com.cla.connector.analysis.services;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.cla.connector.analysis.adapters.*;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.Exchange365ConnectionParametersDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.mediaconnector.box.BoxMediaConnector;
import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.mediaconnector.microsoft.MicrosoftConnectorBase;
import com.cla.connector.mediaconnector.microsoft.MicrosoftDocAuthorityClient;
import com.cla.connector.mediaconnector.microsoft.MicrosoftDocAuthorityClient2013;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.cla.connector.utils.FileNamingUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.scheduler.Schedulers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ConnectorAnalysisService {
    private static final Logger logger = LoggerFactory.getLogger(ConnectorAnalysisService.class);

    private ConnectorAdapter adapter;

    private Disposable crawlDisposable;

    private List<ClaFilePropertiesDto> filesDiscovered = Lists.newArrayList();
    private List<ClaFilePropertiesDto> foldersDiscovered = Lists.newArrayList();
    private List<Exception> errors = Lists.newArrayList();
    private Map<String, Integer> fileTypes = Maps.newHashMap();
    private long crawlStartTime;
    private long lastUpdate;

    private boolean crawlCanceled;

    private Level connectorLogLevel = Level.WARN;

    public boolean isConnectionConfigured() {
        return adapter != null;
    }

    public boolean isFileShareConnection() {
        return isConnectionConfigured() && adapter instanceof FileShareConnectorAdapter;
    }

    public void createFileShareAdapter() {
        stopCrawl();
        adapter = new FileShareConnectorAdapter();
        logger.info("File-share adapter created");
    }

    public void createSharepointAdapter(String url, String username, String password, String domain) {
        stopCrawl();
        SharePointConnectionParametersDto dto = new SharePointConnectionParametersDto();
        dto.setUrl(url);
        dto.setUsername(username);
        dto.setPassword(password);
        dto.setDomain(domain);
        MSConnectionConfig config = MSConnectionConfig.builder().build();

        adapter = SharePointConnectorAdapter.getBuilder()
                .withSharePointConnectionParametersDto(dto)
                .withConnectionConfig(config)
                .withMaxRetries(3)
                .withCharactersToFilter("&#x14;","&#x2;","&#xB;")
                .build();

        logger.info("Share point adapter created (url: {} user: {})", url, username);
        muteLogger(MicrosoftConnectorBase.class, SharePointParseUtils.class, MicrosoftDocAuthorityClient.class, MicrosoftDocAuthorityClient2013.class);
    }

    public void createOnedriveAdapter(String url, String username, String password, String domain) {
        stopCrawl();
        SharePointConnectionParametersDto dto = new SharePointConnectionParametersDto();
        dto.setUrl(url);
        dto.setUsername(username);
        dto.setPassword(password);
        dto.setDomain(domain);
        MSConnectionConfig config = MSConnectionConfig.builder().build();

        adapter = OneDriveConnectorAdapter.getBuilder()
                .withSharePointConnectionParametersDto(dto)
                .withConnectionConfig(config)
                .withMaxRetries(3)
                .withCharactersToFilter("&#x14;","&#x2;","&#xB;")
                .build();

        logger.info("Onedrive adapter created (url: {} user: {})", url, username);
        muteLogger(MicrosoftConnectorBase.class, SharePointParseUtils.class, MicrosoftDocAuthorityClient.class);
    }

    public void createExchange365Adapter(String password,
                    String tenantId, String applicationId, String adUrlOverride, String graphEndpointOverride) {
        stopCrawl();

        Exchange365ConnectionParametersDto params = new Exchange365ConnectionParametersDto();
        params.setPassword(password);
        params.setTenantId(tenantId);
        params.setApplicationId(applicationId);
        params.setAdUrlOverride(adUrlOverride);
        params.setGraphEndpointOverride(graphEndpointOverride);
        adapter = new Exchange365Adapter(params);
    }

    public void createBoxAdapter(String jwt) {
        stopCrawl();
        adapter = new BoxConnectorAdapter(jwt);
        muteLogger(BoxMediaConnector.class);
    }

    public void crawl(String path, boolean ignoreAccessErrors) {
        stopCrawl();
        logger.info("Start crawling {}", path);

        filesDiscovered.clear();
        errors.clear();
        foldersDiscovered.clear();
        fileTypes.clear();

        try {
            crawlDisposable = adapter.crawl(path, ignoreAccessErrors)
                    .subscribeOn(Schedulers.elastic())
                    .doOnError(t -> logger.error("Failed to produce item", t))
                    .doOnEach(t -> lastUpdate = System.currentTimeMillis())
                    .doOnNext(this::processDiscoveredItem)
                    .doOnError(throwable -> errors.add((Exception) throwable))
                    .doOnComplete(() -> crawlCompleted(path))
                    .doOnSubscribe(s -> crawlStartTime = System.currentTimeMillis())
                    .subscribe();
            crawlCanceled = false;
        } catch (Exception e) {
            logger.error("Failed to crawl", e);
        }
    }

    private void processDiscoveredItem(ClaFilePropertiesDto item) {
        if (item.isFolder()) {
            foldersDiscovered.add(item);
        } else {
            filesDiscovered.add(item);
            String extension = FileNamingUtils.getFilenameExtension(item.getFileName())
                    .toUpperCase();
            if (!StringUtils.isEmpty(extension)) {
                Integer count = fileTypes.getOrDefault(extension, 0);
                fileTypes.put(extension, ++count);
            }
        }
    }

    public void printSampleFiles(int howMany) {
        int fromIndex = filesDiscovered.size() > howMany ? filesDiscovered.size() - howMany : 0;
        filesDiscovered.subList(fromIndex, filesDiscovered.size())
                .stream()
                .map(prop -> "Discovered file: " + prop.getFileName() + "(id: " + prop.getMediaItemId() + ")")
                .forEach(logger::info);
    }

    private void crawlCompleted(String path) {
        crawlDisposable = null;
        logger.info("Crawl {} completed", path);
        stats();
    }

    public void stopCrawl() {
        if (isCrawlActive()) {
            crawlCanceled = true;
        }
        Optional.ofNullable(crawlDisposable)
                .filter(dis -> !dis.isDisposed())
                .ifPresent(dis -> {
                    dis.dispose();
                    logger.info("Crawl halted");
                    stats();
                    crawlDisposable = null;
                });

    }

    public void stats() {
        String crawlingMsg;
        if (isCrawlActive()) {
            crawlingMsg = "*** Ongoing crawl ***";
        } else {
            crawlingMsg = crawlCanceled ? " *** Crawl canceled ***" : " *** Crawl completed ***";
        }

        logger.info(crawlingMsg);
        logger.info("Folders discovered: {}", foldersDiscovered.size());
        logger.info("Files discovered: {}", filesDiscovered.size());
        logger.info("Errors: {}", errors.size());

        long uptoTime = Optional.ofNullable(crawlDisposable)
                .filter(dis -> !dis.isDisposed())
                .map(dis -> System.currentTimeMillis())
                .orElse(lastUpdate);

        int secondsPassed = (int) (uptoTime - crawlStartTime)/1000;
        logger.info("Discover rate: {}/sec", ((float) filesDiscovered.size() + foldersDiscovered.size())/ secondsPassed);
        List<Map.Entry<String, Integer>> fTypes = fileTypes.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey, String::compareTo))
                .collect(Collectors.toList());
        logger.info("File types: {}", fTypes);
    }

    public void dumpAcls(String filename) {
        try {
            adapter.getFileAcl(filename)
                    .subscribeOn(Schedulers.single())
                    .doOnNext(file -> logger.info("File: '{}' owned by '{}'", file.getFileName(), file.getOwnerName()))
                    .doOnNext(file -> dumpCollection("R+", file.getAclReadAllowed()))
                    .doOnNext(file -> dumpCollection("W+", file.getAclWriteAllowed()))
                    .doOnNext(file -> dumpCollection("R-", file.getAclReadDenied()))
                    .doOnNext(file -> dumpCollection("W-", file.getAclWriteDenied()))
                    .subscribe();
        } catch (Exception e) {
            logger.error("Failed to get acl for file {}", filename, e);
        }
    }

    private void dumpCollection(String classification, Collection<String> collection) {
        collection.stream()
                .map(item -> classification + ": " + item)
                .forEach(logger::info);
    }

    public boolean isCrawlActive() {
        return Optional.ofNullable(crawlDisposable)
                .filter(dis -> !dis.isDisposed())
                .isPresent();
    }

    public void dumpFileContent(String filepath, String dest) throws IOException {
        boolean succeed = false;

        try(OutputStream os = new FileOutputStream(dest)) {
            IOUtils.copy(adapter.getInputStream(filepath), os);
            succeed = true;
        } finally {
            if (!succeed) {
                File file = new File(dest);
                if (file.exists()) {
                    if (!file.delete()) {
                        logger.error("Failed to delete {}", filepath);
                    }
                }
            }
        }
    }

    private void muteLogger(Class<?>...cls) {
        for (Class<?> c : cls) {
            ch.qos.logback.classic.Logger logger = ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger(c);
            logger.setLevel(connectorLogLevel);
        }
    }

    public void setConnectorLogLevel(Level connectorLogLevel) {
        this.connectorLogLevel = connectorLogLevel;
    }

    public void testConnection() {
        adapter.testConnection();
    }
}
