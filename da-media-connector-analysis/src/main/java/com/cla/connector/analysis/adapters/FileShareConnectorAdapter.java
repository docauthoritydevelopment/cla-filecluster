package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaConnector;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.connector.mediaconnector.fileshare.preserveaccesstime.WindowsPreserveAccessTime;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

public class FileShareConnectorAdapter extends FileShareMediaConnector implements ConnectorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(FileShareConnectorAdapter.class);

    public FileShareConnectorAdapter() {
        super(true, true, null);
    }

    @Override
    public Flux<ClaFilePropertiesDto> crawl(String folder, boolean ignoreAccessErrors) {
        Path path = Paths.get(folder);
        if (!WindowsPreserveAccessTime.isDirectory(path, true, ignoreAccessErrors)) {
            logger.warn("{} is not a folder", folder);
            return Flux.empty();
        }

        Flux<ClaFilePropertiesDto> files;
        final List<Path> subDirs = Lists.newArrayList();
        try {
            Stream<Path> pathStream = Files.list(path);
            files = Flux.fromStream(pathStream)
                        .doOnNext(dir -> {
                            if(WindowsPreserveAccessTime.isDirectory(dir, true, ignoreAccessErrors)) {
                                subDirs.add(dir);
                            }
                        })
                    .filter(dir -> !WindowsPreserveAccessTime.isDirectory(dir, true, ignoreAccessErrors))
                    .map(file -> FileShareMediaUtils.getMediaProperties(file, true, true));
        } catch (IOException e) {
            logger.error("Failed to crawl folder {}", folder, e);
            files = Flux.empty();
        }

        Flux<ClaFilePropertiesDto> subDirFlux = Flux.fromIterable(subDirs)
                .map(dir -> dir.isAbsolute() ? dir.toString() : dir.toAbsolutePath().normalize().toString())
                .map(dir -> crawl(dir, ignoreAccessErrors))
                .flatMap(Function.identity());

        Mono<ClaFilePropertiesDto> currentDir =
                Mono.just(FileShareMediaUtils.getMediaProperties(path, true, true))
                        .doOnNext(prop -> prop.setFolder(true));

        return Flux.concat(currentDir, files, subDirFlux);
    }

    @Override
    public InputStream getInputStream(String file) throws IOException {
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setFileName(file);
        return getInputStream(prop);
    }

    @Override
    public Mono<ClaFilePropertiesDto> getFileAcl(String path) {
        return Mono.fromSupplier(() -> {
            ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
            props.setFileName(path);
            return props;
        })
                .doOnNext(this::fetchAcls);
    }
}
