package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.microsoft.MSItemKey;
import com.cla.connector.mediaconnector.microsoft.MicrosoftDocAuthorityClient;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointParseUtils;
import com.independentsoft.share.ServiceException;
import org.slf4j.Logger;
import reactor.core.publisher.Mono;

public interface MicrosoftAdapterBase extends ConnectorAdapter {

    default Mono<ClaFilePropertiesDto> getFileAcl(String path) {
        MSItemKey key = SharePointParseUtils.splitPathAndSubsite(path);
        final String convertedPath = convertFileNameIfNeeded(key.getPath());
        Mono<ClaFilePropertiesDto> fileProp = Mono.defer(() -> Mono.fromSupplier(() -> {
            try {
                return getMicrosoftDocAuthorityClient().getFileMediaItemId(key.getSite(), convertedPath);
            } catch (ServiceException e) {
                getLogger().error("Failed", e);
                return null;
            }
        }));

        return fileProp.doOnNext(prop -> prop.setFileName(path))
            .doOnNext(this::fetchAcls);

    }

    String convertFileNameIfNeeded(String path);

    void fetchAcls(ClaFilePropertiesDto prop);

    Logger getLogger();

    MicrosoftDocAuthorityClient getMicrosoftDocAuthorityClient();
}
