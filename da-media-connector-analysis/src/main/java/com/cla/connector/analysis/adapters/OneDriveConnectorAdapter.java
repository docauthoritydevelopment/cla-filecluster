package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.mediaconnector.microsoft.MSConnectionConfig;
import com.cla.connector.mediaconnector.microsoft.MicrosoftDocAuthorityClient;
import com.cla.connector.mediaconnector.microsoft.onedrive.OneDriveMediaConnector;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by oren on 7/18/2018.
 *
 */
public class OneDriveConnectorAdapter extends OneDriveMediaConnector implements MicrosoftAdapterBase {
    private static final Logger logger = LoggerFactory.getLogger(OneDriveConnectorAdapter.class);

    private OneDriveConnectorAdapter(SharePointConnectionParametersDto sharePointConnectionDetailsDto,
                                     int maxRetries,
                                     int pageSize,
                                     long maxSupportFileSize,
                                     MSConnectionConfig config,
                                     List<String> foldersToFilter,
                                     boolean isSpecialCharsSupported,
                                     String... charsToFilter) {

        super(sharePointConnectionDetailsDto, null, maxRetries, pageSize, maxSupportFileSize, config, true, foldersToFilter, isSpecialCharsSupported, charsToFilter);
    }

    @Override
    public List<ServerResourceDto> testConnection() {
        try {
            super.testConnection();
            System.out.println("Test succeeded");
        } catch (Exception e) {
            System.out.println("Test failed: " + e.getMessage());
        }
        return null;
    }

    @Override
    public Flux<ClaFilePropertiesDto> crawl(final String path, boolean ignoreAccessErrors) {
        final String convertedPath = convertFileNameIfNeeded(path);
        Flux<ClaFilePropertiesDto> fileFlux = getListIdDeferredMono(convertedPath)
                .flatMapMany(lid ->
                        Flux.create(emitter -> {
                            streamFiles(-1L, convertedPath, null, lid, x -> true, emitter::next);
                            emitter.complete();
                        }));

        Flux<ClaFilePropertiesDto> subFolderCrawlFlux = Flux.defer(() -> Flux.fromIterable(listFoldersUnderLibrary(convertedPath, null)))
                .map(ServerResourceDto::getFullName)
                .flatMap(p -> crawl(p, ignoreAccessErrors));

        Mono<ClaFilePropertiesDto> currentDir =
                Mono.create(emitter -> processFolderData(-1L, emitter::success, convertedPath, null, convertedPath, null));

        return Flux.concat(currentDir, fileFlux, subFolderCrawlFlux)
                .doOnCancel(() -> microsoftDocAuthorityClient.terminateHttpClient());
    }

    @NotNull
    private Mono<String> getListIdDeferredMono(String convertedPath) {
        return Mono.defer(() -> Mono.fromSupplier(() -> {
            try {
                return getFolderProperties(null, convertedPath).getListId();
            } catch (FileNotFoundException e) {
                logger.error("Error while obtaining Folder's properties");
                return null;
            }
        }));
    }

    public InputStream getInputStream(String path) throws FileNotFoundException {
        path = convertFileNameIfNeeded(path);
        return super.getInputStreamForFileName(path, null, true);
    }

    @Override
    public String convertFileNameIfNeeded(String path) {
        return super.convertFileNameIfNeeded(path);
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public MicrosoftDocAuthorityClient getMicrosoftDocAuthorityClient() {
        return microsoftDocAuthorityClient;
    }

    public static OneDriveConnectorAdapterBuilder getBuilder() {
        return new OneDriveConnectorAdapterBuilder();
    }

    public static class OneDriveConnectorAdapterBuilder extends MicrosoftConnectorBaseBuilder<OneDriveConnectorAdapter, OneDriveConnectorAdapterBuilder> {

        @Override
        protected OneDriveConnectorAdapterBuilder getThis() {
            return this;
        }

        @Override
        public OneDriveConnectorAdapter build() {
            return new OneDriveConnectorAdapter(
                    sharePointConnectionParametersDto,
                    maxRetries,
                    pageSize,
                    maxFileSize,
                    connectionConfig,
                    foldersToFilter,
                    false,
                    charsToFilter);
        }
    }
}