package com.cla.connector.analysis.api;

import com.cla.connector.analysis.services.ConnectorAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.CommandRegistry;
import org.springframework.shell.ParameterResolver;
import org.springframework.shell.standard.CommandValueProvider;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.shell.standard.commands.Help;

import javax.validation.ValidatorFactory;
import java.io.IOException;
import java.util.List;

@ShellComponent
public class CustomHelp implements Help.Command {
    private static final String BACK_SLASH_ESCAPE_MSG = "*** Special characters are escaped with '\\'";
    private static final String WHITESPACE_ARG_MSG = "*** If argument contains a white-space, please wrap it with parenthesis";

    private Help help;
    private ConnectorAnalysisService connectorAnalysisService;

    @Autowired
    public CustomHelp(List<ParameterResolver> parameterResolvers, ConnectorAnalysisService connectorAnalysisService) {
        help = new Help(parameterResolvers);
        this.connectorAnalysisService = connectorAnalysisService;
    }

    @Autowired // ctor injection impossible b/c of circular dependency
    public void setCommandRegistry(CommandRegistry commandRegistry) {
        help.setCommandRegistry(commandRegistry);
    }


    @Autowired(required = false)
    public void setValidatorFactory(ValidatorFactory validatorFactory) {
        help.setValidatorFactory(validatorFactory);
    }

    @ShellMethod(value = "Display help about available commands.", prefix = "-")
    public CharSequence help(
            @ShellOption(defaultValue = ShellOption.NULL, valueProvider = CommandValueProvider.class, value = { "-C",
                    "--command" }, help = "The command to obtain help for.") String command)
            throws IOException {

        return help.help(command)
                + "\n" + BACK_SLASH_ESCAPE_MSG
                + "\n" + WHITESPACE_ARG_MSG
                + (connectorAnalysisService.isFileShareConnection() ? "\n~~~~ Be sure to escape '\\'s ('\\\\')  ~~~~" : "")
                + "\n\n\n";
    }
}