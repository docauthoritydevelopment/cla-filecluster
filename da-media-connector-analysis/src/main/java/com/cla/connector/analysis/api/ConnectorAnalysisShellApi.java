package com.cla.connector.analysis.api;

import ch.qos.logback.classic.Level;
import com.cla.connector.analysis.services.ConnectorAnalysisService;
import com.cla.connector.domain.dto.media.MediaType;
import org.jline.utils.AttributedString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.Availability;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import org.springframework.stereotype.Component;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by oren on 7/18/2018.
 *
 */
@ShellComponent("Connector Analysis")
public class ConnectorAnalysisShellApi {
    private static final Logger logger = LoggerFactory.getLogger(ConnectorAnalysisShellApi.class);

    private static final String ADVANCED_COMMANDS = "Advanced";

    private static final String CONNECTORS_COMMANDS = "Connectors";

    @Autowired
    private ConnectorAnalysisService connectorAnalysisService;

    private MediaType mediaType;

    @ShellMethod(value = "Create new connection", group = CONNECTORS_COMMANDS)
    public void createSharePointConnection(@ShellOption String url, @ShellOption String username, @ShellOption(defaultValue = ShellOption.NULL) String password, @ShellOption(defaultValue = ShellOption.NULL) String domain) {
        String pass = Optional.ofNullable(password).orElseGet(this::readPassword);
        connectorAnalysisService.createSharepointAdapter(url, username, pass, domain);
        mediaType = MediaType.SHARE_POINT;
    }

    @ShellMethod(value = "Create new connection", group = CONNECTORS_COMMANDS)
    public void createOneDriveConnection(@ShellOption String url, @ShellOption String username, @ShellOption(defaultValue = ShellOption.NULL) String password, @ShellOption(defaultValue = ShellOption.NULL) String domain) {
        String pass = Optional.ofNullable(password).orElseGet(this::readPassword);
        connectorAnalysisService.createOnedriveAdapter(url, username, pass, domain);
        mediaType = MediaType.ONE_DRIVE;
    }

    @ShellMethod(value = "Create file-share connection", group = CONNECTORS_COMMANDS)
    public void createFileShareConnection() {
        connectorAnalysisService.createFileShareAdapter();
        mediaType = MediaType.FILE_SHARE;
        System.out.println("\n*** Windows paths must be escaped with '\\'s ***");
    }

    @ShellMethod(value = "Create box connection", group = CONNECTORS_COMMANDS)
    public void createBoxConnection(@ShellOption String jwt) {
        connectorAnalysisService.createBoxAdapter(jwt);
        mediaType = MediaType.BOX;
    }

    @ShellMethod(value = "Create new connection", group = CONNECTORS_COMMANDS)
    public void createExchange365Connection(@ShellOption String tenantId,
                                            @ShellOption String applicationId,
                                            @ShellOption(defaultValue = ShellOption.NULL) String password) { /*,
                                            @ShellOption(defaultValue = ShellOption.NULL) String adUrlOverride,
                                            @ShellOption(defaultValue = ShellOption.NULL) String graphEndpointOverride) {*/
        String pass = Optional.ofNullable(password).orElseGet(this::readPassword);
        connectorAnalysisService.createExchange365Adapter(pass, tenantId, applicationId, null, null);//, adUrlOverride, graphEndpointOverride);
        mediaType = MediaType.EXCHANGE_365;
    }

    @ShellMethod("Discover files and folder under path")
    public void crawl(@ShellOption(help = "Path to crawl") String path) {
        connectorAnalysisService.crawl(path, false);
    }

    @ShellMethod("Stop active crawler")
    public void stopCrawling() {
        connectorAnalysisService.stopCrawl();
    }

    @ShellMethod("Test Connection")
    public void test() {
        connectorAnalysisService.testConnection();
    }

    @ShellMethod(value = "Print few discovered files", group = ADVANCED_COMMANDS)
    public void printSampledDiscoveredFiles(@ShellOption(defaultValue = "10") int howMany) {
        connectorAnalysisService.printSampleFiles(howMany);
    }

    @ShellMethod(value = "Set connector log-level", group = ADVANCED_COMMANDS)
    public void setConnectorsLogLevel(@ShellOption(defaultValue = "WARN") Level level) {
        connectorAnalysisService.setConnectorLogLevel(level);
        System.out.println("Logger set to " + level + "\nPlease recreate connector in order to reflect log-level changes.");
    }

    @ShellMethod(value = "Dump file content", group = ADVANCED_COMMANDS)
    public void dumpFileContent(@ShellOption String filePath) {
        String currentDir = System.getProperty("user.dir");
        if (!currentDir.endsWith(File.separator)) {
            currentDir += File.separator;
        }
        String chr = filePath.startsWith("http") ? "/" : File.separator;
        String destFilename = filePath.substring(filePath.lastIndexOf(chr) + 1);
        try {
            connectorAnalysisService.dumpFileContent(filePath, currentDir + destFilename);
            logger.info("Succeed. Saved to {}", currentDir + destFilename);
        } catch (IOException e) {
            logger.error("Failed to dump {} content", filePath, e);
        }
    }

    @ShellMethod(value = "Get ACLs for file", group = ADVANCED_COMMANDS)
    public void getAcl(@ShellOption String filename) {
        connectorAnalysisService.dumpAcls(filename);
    }

    @ShellMethod(value = "Get ACLs for file (BOX)", group = ADVANCED_COMMANDS)
    public void getAclBox(@ShellOption String id) {
        getAcl(id);
    }

    @ShellMethod("Print stats of last/on-going crawl")
    public void stats() {
        connectorAnalysisService.stats();
    }

    private String readPassword() {
        Console console = System.console();
        if (console == null) {
            System.out.print("Please enter password (password is visible): ");
            return new Scanner(System.in).nextLine();
        } else {
            System.out.print("Please enter password (password is invisible): ");
            return new String(System.console().readPassword());
        }
    }

    @ShellMethod("Pauses script execution")
    public void pause(@ShellOption int secs) {
        try {
            TimeUnit.SECONDS.sleep(secs);
        } catch (InterruptedException e) {
            logger.error("Pause interrupted", e);
        }
    }

    @SuppressWarnings("unused")
    public Availability getAclAvailability() {
        Availability generalAvailability = isAvailable();
        if (!generalAvailability.isAvailable()) {
            return generalAvailability;
        }

        if (MediaType.BOX == mediaType) {
            return Availability.unavailable("Only for none-BOX connectors");
        }

        return generalAvailability;
    }

    @SuppressWarnings("unused")
    public Availability getAclBoxAvailability() {
        Availability generalAvailability = isAvailable();
        if (!generalAvailability.isAvailable()) {
            return generalAvailability;
        }

        if (MediaType.BOX != mediaType) {
            return Availability.unavailable("Only for BOX connectors");
        }

        return generalAvailability;
    }

    @SuppressWarnings("unused")
    public Availability stopCrawlingAvailability() {
        return connectorAnalysisService.isConnectionConfigured() && connectorAnalysisService.isCrawlActive()
                ? Availability.available() : Availability.unavailable("No crawl is active");
    }

    @SuppressWarnings("unused")
    public Availability printSampledDiscoveredFilesAvailability() {
        return isAvailable();
    }

    @SuppressWarnings("unused")
    public Availability dumpFileContentAvailability() {
        return isAvailable();
    }

    @SuppressWarnings("unused")
    public Availability crawlAvailability() {
        return isAvailable();
    }

    @SuppressWarnings("unused")
    public Availability testAvailability() {
        return isAvailable();
    }

    @SuppressWarnings("unused")
    public Availability resetConnectionAvailability() {
        return isAvailable();
    }

    @SuppressWarnings("unused")
    public Availability listConnectionsAvailability() {
        return isAvailable();
    }

    private Availability isAvailable() {
        return connectorAnalysisService.isConnectionConfigured() ? Availability.available() : Availability.unavailable("no connectors were configured");
    }

    @Component
    public class Prompt implements PromptProvider {

        @Override
        public AttributedString getPrompt() {
            String prompt = Optional.ofNullable(mediaType)
                    .map(MediaType::name)
                    .orElse("shell");
            return new AttributedString(prompt + ":>");
        }
    }
}