package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.mediaconnector.box.BoxMediaConnector;
import com.cla.connector.progress.DummyProgressTracker;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;

public class BoxConnectorAdapter extends BoxMediaConnector implements ConnectorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(BoxConnectorAdapter.class);
    private static final int THROTTLING_RETRIES = 3;
    private static final int PARALLEL_SCAN_POOL_SIZE = 3;

    public BoxConnectorAdapter(String jwt) {
        super(jwt, THROTTLING_RETRIES, PARALLEL_SCAN_POOL_SIZE);
    }

    @Override
    public List<ServerResourceDto> testConnection() {
        try {
            if (super.testConnection().size() == 0) {
                throw new RuntimeException("Received 0 Sub-folders");
            }
            System.out.println("Test succeeded");
        } catch (Exception e) {
            System.out.println("Test failed: " + e.getMessage());
        }
        return null;
    }

    @Override
    public Flux<ClaFilePropertiesDto> crawl(String path, boolean ignoreAccessErrors) {
        ScanTaskParameters taskParameters = getScanTaskParams(path);
        return Flux.create(emitter -> {
            streamMediaItems(taskParameters, emitter::next, null, null, null, new DummyProgressTracker());
            emitter.complete();
        });
    }

    private ScanTaskParameters getScanTaskParams(String path) {
        Set<String> extensions = Sets.newHashSet("pdf", "docx", "doc", "xls", "xlsx");
        ScanTypeSpecification scanTypes = new ScanTypeSpecification(null, extensions, null);

        return ScanTaskParameters.create()
                .setPath(path)
                .setRunId(-1L)
                .setScanTypeSpecification(scanTypes);
    }

    @Override
    public InputStream getInputStream(String file) {
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId(file);
        return getInputStream(prop);
    }

    @Override
    public Mono<ClaFilePropertiesDto> getFileAcl(String mediaItemId) {
        return Mono.defer(() -> {
            try {
                return Mono.just(getMediaItemAttributes(mediaItemId, true));
            } catch (FileNotFoundException e) {
                logger.error("Failed to get acls for item id {}", mediaItemId, e);
                throw new RuntimeException(e);
            }
        });
    }
}
