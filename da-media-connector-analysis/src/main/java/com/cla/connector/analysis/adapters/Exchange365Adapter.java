package com.cla.connector.analysis.adapters;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.Exchange365ConnectionParametersDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.mediaconnector.ThrottlingStartledTurtle;
import com.cla.connector.mediaconnector.exchange.Exchange365MediaConnector;
import com.cla.connector.mediaconnector.exchange.Exchange365Params;
import com.cla.connector.mediaconnector.exchange.auth.MSGraphAuthConfig;
import com.cla.connector.mediaconnector.exchange.throttling.MsGraphThrottlingParams;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Created by: yael
 * Created on: 7/28/2019
 */
public class Exchange365Adapter extends Exchange365MediaConnector implements ConnectorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(Exchange365Adapter.class);

    public Exchange365Adapter(Exchange365ConnectionParametersDto params) {
        super(getFromParams(params));
    }

    public static Exchange365Params getFromParams(Exchange365ConnectionParametersDto params) {
        params.setConnectionConnectTimeout(5000);
        params.setConnectionReadTimeout(5000);

        MSGraphAuthConfig authConfig = MSGraphAuthConfig.builder()
                .connectTimeout(5000)
                .readTimeout(5000)
                .retrySleepInterval(1000)
                .maxAttempts(3)
                .prefetchTokenBeforeExpiry(60000)
                .build();

        ThrottlingStartledTurtle THROTTLER = new ThrottlingStartledTurtle();

        MsGraphThrottlingParams throttlingParams =
                new MsGraphThrottlingParams(THROTTLER, 1000);
        Exchange365Params exchange365Params =
                new Exchange365Params(params, authConfig, throttlingParams);
        return exchange365Params;
    }

    @Override
    public List<ServerResourceDto> testConnection() {
        try {
            if (super.testConnection().size() == 0) {
                throw new RuntimeException("Received no data");
            }
            System.out.println("Test succeeded");
        } catch (Exception e) {
            System.out.println("Test failed: " + e.getMessage());
        }
        return null;
    }

    @Override
    public Flux<ClaFilePropertiesDto> crawl(String mailboxUPN, boolean ignoreAccessErrors) {
        ScanTaskParameters taskParameters = getScanTaskParams(mailboxUPN);
        Consumer<String> createScanTaskConsumer = System.out::println;
        return Flux.create(emitter -> {
            streamMediaChangeLog(
                    taskParameters, null, null, 0, emitter::next, createScanTaskConsumer, null);
            emitter.complete();
        });
    }

    private ScanTaskParameters getScanTaskParams(String mailboxUPN) {
        Set<String> extensions = Sets.newHashSet("pdf", "docx", "doc", "xls", "xlsx", "eml", ItemType.MESSAGE.getSpecialExtension());
        List<FileType> typesToScan = Lists.newArrayList(FileType.EXCEL, FileType.MAIL, FileType.PDF, FileType.WORD);
        ScanTypeSpecification scanTypes = new ScanTypeSpecification(typesToScan, extensions, null);

        return ScanTaskParameters.create()
                .setMailboxUPN(mailboxUPN)
                .setRunId(-1L)
                .setScanTypeSpecification(scanTypes);
    }

    @Override
    public InputStream getInputStream(String file) throws IOException {
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId(file);
        return getInputStream(prop);
    }

    @Override
    public Mono<ClaFilePropertiesDto> getFileAcl(String mediaItemId) {
        return Mono.defer(() -> {
            try {
                return Mono.just(getMediaItemAttributes(mediaItemId, true));
            } catch (FileNotFoundException e) {
                logger.error("Failed to get acls for item id {}", mediaItemId, e);
                throw new RuntimeException(e);
            }
        });
    }
}
