package com.cla.connector.analysis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * cli tool to test media connectors
 *
 * Created by oren on 7/18/2018.
 */
@SpringBootApplication
public class ConnectorAnalyzerApplication {

    public static void main(String args[]) {
        SpringApplication.run(ConnectorAnalyzerApplication.class, args);
    }
}