Media-Processor Sharepoint Analyzer

Preface
A tool to test and examine data retrieved via API by sharepoint instance.

Installation
The tool does not require any special configuration/installation, but it does require java runtime environment installed on the machine.
Java runtime version: 1.8 (or later).
* Please make sure JAVA_HOME environment variable is set to java installtion directory (such directory would contain a 'bin' directory with a java.exe file)

Usage
On command prompt, at the location (directory) of the tool, execute 'mp_sp_analyzer.bat' and follow instructions.
- URL: Address of the tested sharepoint instance. (i.e. https://domain.sharepoint.com/)
- Username: Username to use.
- Password: Matching password.
- Domain: Domain associated with the sharepoint instance (if none enter '!@#')

After the above was entered, the connection will be automatically tested.
Then, a list of commands will be printed.
Enter a command; If prompt, enter necessary data.
Most of the times you'll be asked for a path of a folder. Typically path will be relative to the connection url.
e.g.:
If the connection URL is 'https://domain.sharepoint.com/' and the full path is 'https://domain.sharepoint.com/Shared Documents/folder'
the relative path to use will be '/Shared Documents/folder'.

Logs
Log files can be found under 'logs\' directory within the directory used to run the tool.

IMPORTANT
Logs may contain connection password used with the tool.
Please make sure to review the log files and remove all password occurrences.