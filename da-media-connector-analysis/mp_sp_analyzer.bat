@ECHO OFF

REM init
if exist "%JAVA_HOME%" goto run

if exist %~dp0\..\JRE set JAVA_HOME=%~dp0\..\JRE
if exist E:\DocAuthority\JRE set JAVA_HOME=E:\DocAuthority\JRE
if exist D:\DocAuthority\JRE set JAVA_HOME=D:\DocAuthority\JRE
if exist C:\DocAuthority\JRE set JAVA_HOME=C:\DocAuthority\JRE
if not exist "%JAVA_HOME%" goto error
echo JAVA_HOME=%JAVA_HOME%

:run
"%JAVA_HOME%\bin\java" -jar da-media-proc-2.0.0-fat.jar analyze
goto exit


:error
ECHO 'JAVA_HOME' environment variable must to be set!

:exit
ECHO Bye bye
@ECHO ON