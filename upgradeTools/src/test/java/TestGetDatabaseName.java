import org.junit.Ignore;
import org.junit.Test;

import java.sql.*;

/**
 * Created by uri on 13/03/2016.
 */
public class TestGetDatabaseName {

    @Test
    @Ignore
    public void testGetDatabaseName() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/cla?autoReconnect=true&useUnicode=true&createDatabaseIfNotExist=true&characterEncoding=utf-8";
        Connection connection = DriverManager.getConnection(
                url, "root", "root");
        try {
            logInfo("Connection Schema: "+connection.getSchema());
            Statement stmt = connection.createStatement();
            String sql;
            sql = "select database()";
            ResultSet rs = stmt.executeQuery(sql);
            String schemaName = rs.getMetaData().getSchemaName(1);
            if (schemaName == null || schemaName.length() == 0) {
                rs.next();
                schemaName = rs.getString(1);
            }
            logInfo("Selected database: "+schemaName);
        } catch (SQLException e) {
            logError("Failed to show connection details",e);
            throw new RuntimeException("Failed to show connection details:"+e.getMessage(),e);
        }

    }

    private static void logError(String s) {
        System.err.println("ERROR: "+s);
    }

    private static void logError(String s, Exception e) {
        System.err.println("ERROR: "+s);
        e.printStackTrace();
    }

    private static void logInfo(String s) {
        System.out.println(s);
    }

}
