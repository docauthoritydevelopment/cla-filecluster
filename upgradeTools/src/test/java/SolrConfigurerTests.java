import com.cla.filecluster.scripts.SolrCloudConfigurer;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

/**
 * Samples on how to delete and configure Solr Cloud collections
 * Created by uri on 05-Jun-17.
 */
@Ignore
public class SolrConfigurerTests {

    @Test
    public void testDeleteAllCollections() {
        String zkHost = "localhost:9983";
        CloudSolrClient cloudSolrClient = new CloudSolrClient
                .Builder().withZkHost(zkHost).build();
        SolrCloudConfigurer solrCloudConfigurer = new SolrCloudConfigurer(cloudSolrClient);
        solrCloudConfigurer.deleteAllCollections();
    }

    @Test
    public void testCreateCollections() {
        String zkHost = "localhost:9983";
        CloudSolrClient cloudSolrClient = new CloudSolrClient
                .Builder().withZkHost(zkHost).build();
        SolrCloudConfigurer solrCloudConfigurer = new SolrCloudConfigurer(cloudSolrClient);
        List<String> collections = Lists.newArrayList(
                "FileState",
                "CatFiles",
                "AnalysisData",
                "ContentMetadata",
                "CatFolders",
                "BizList",
                "FileGroups",
                "GrpHelper",
                "Titles",
                "FileContent",
                "Groups",
                "PVS",
                "Extraction");
        for (String collection : collections) {
            solrCloudConfigurer.createCollection(collection,2,collection, null);
        }

    }

    @Test
    public void testCreateTestCollections() {
        String zkHost = "localhost:9983";
        CloudSolrClient cloudSolrClient = new CloudSolrClient
                .Builder().withZkHost(zkHost).build();
        SolrCloudConfigurer solrCloudConfigurer = new SolrCloudConfigurer(cloudSolrClient);
        List<String> collections = Lists.newArrayList(
                "FileState",
                "CatFiles",
                "AnalysisData",
                "ContentMetadata",
                "CatFolders",
                "BizList",
                "FileGroups",
                "GrpHelper",
                "Titles",
                "FileContent",
                "Groups",
                "PVS",
                "Extraction");
        for (String collection : collections) {
            solrCloudConfigurer.createCollection("Test"+collection,2,collection, null);
        }

    }

}
