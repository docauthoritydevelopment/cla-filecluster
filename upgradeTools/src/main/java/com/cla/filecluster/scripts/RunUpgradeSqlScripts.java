package com.cla.filecluster.scripts;

import com.cla.filecluster.utils.EncUtils;
import com.google.common.base.Strings;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by uri on 03/03/2016.
 */
public class RunUpgradeSqlScripts {


    public static final String INFO = "info";
    public static final String EXECUTE = "execute";
    public static final String BASELINE= "baseline";
    public static final String CLA_FILE_CLUSTER_UPGRADE_FOLDER_PROPERTY = "com.cla.filecluster.upgradeFolder";
    private static String tempFolder = ".\\temp";
    private static String logsFolder = "..\\installation_logs";
    private static long timeoutSec = 45;

    private static final String DROP_SCHEMA = "drop-schema";

    public static void main(String[] args) throws ClassNotFoundException,
            SQLException {
        String url; // "jdbc:mysql://localhost:3306/database"
        String userName;
        String password;
        if (args.length == 0) {
            logError("Illegal command arguments.\nArguments must be: [action] [database-url] [userName] [password]");
            throw new RuntimeException("Illegal command arguments. Arguments must be database-url, username, password");
        }
        String action = args[0];
        if (action.equalsIgnoreCase("solr")) {
            SolrCloudConfigurer.execute(args);
//            logInfo("Finished executing Solr action");
            System.exit(0);
            return; // We never reach here
        }
        if (args.length == 1) {
            logInfo("Reading Connection details from application.properties file");
            Properties properties = new Properties();
            File propertiesFile = new File("./config","application.properties");
            if (!propertiesFile.exists()) {
                throw new RuntimeException("Failed to find application.properties file ("+propertiesFile.getAbsolutePath()+")");
            }
            try {
                properties.load(new FileInputStream(propertiesFile));
            } catch (IOException e) {
                throw new RuntimeException("Failed to load properties file (IO Exception)",e);
            }
            String tempFolderProp = properties.getProperty("upgrade.script-temp-folder");
            if (tempFolderProp != null) {
                tempFolder = tempFolderProp;
            }
            String timeoutSecProp = properties.getProperty("upgrade.batch-timeout-sec");
            if (timeoutSecProp != null) {
                timeoutSec = Integer.valueOf(timeoutSecProp);
            }
            url = properties.getProperty("bonecp.url");
            userName = properties.getProperty("bonecp.username");
            password = properties.getProperty("bonecp.password");
            String useEncPasswordStr = properties.getProperty("use-enc-pass");
            boolean useEncPass = false;
            if (!Strings.isNullOrEmpty(useEncPasswordStr)) {
                useEncPass = Boolean.parseBoolean(useEncPasswordStr);
            }
            password = useEncPass ? EncUtils.decrypt(password) : password;
        }
        else if (args.length == 4) {
            url = args[1]; // "jdbc:mysql://localhost:3306/database"
            userName = args[2];
            password = args[3];
        }
        else {
            logError("Illegal command arguments.\nArguments must be: [action] [database-url] [userName] [password]");
            throw new RuntimeException("Illegal command arguments. Arguments must be database-url, username, password");
        }
        String sqlUpgradeScriptsFolder = System.getProperty(CLA_FILE_CLUSTER_UPGRADE_FOLDER_PROPERTY,"./upgrade-sql");

        // Create MySql Connection
        logInfo("Checking driver and creating connection");
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = DriverManager.getConnection(
                url, userName, password);
        String schemaName = showConnectionDetails(connection);

        boolean tableFound = createTableIfNecessary(connection);
        if (!tableFound) {
            markAllScriptsAsSkipped(sqlUpgradeScriptsFolder,connection);
        }
        if (action.equalsIgnoreCase(INFO)) {
            logInfo("Get upgrade scripts information on "+url+" with user "+userName);
            getSqlScriptsInformation(sqlUpgradeScriptsFolder,connection);
        }
        else if (action.equalsIgnoreCase(EXECUTE)){
            logInfo("Execute upgrade scripts on "+url+" with user "+userName);
            runSqlScripts(sqlUpgradeScriptsFolder,connection);
        }
        else if (action.equalsIgnoreCase(BASELINE)) {
            logInfo("Run baseline script on "+url+" with user "+userName);
            runBaselineScript(sqlUpgradeScriptsFolder,connection);
        }
        else if (action.equalsIgnoreCase(DROP_SCHEMA)) {
            logInfo("Drop schema on "+url+" with user "+userName);
            dropSchema(schemaName,connection);
        }
        else {
            logError("Unknown command: "+action+". Supported actions are: [execute,info,baseline]");
            throw new RuntimeException("Unknown command: "+action+". Supported actions are: [execute,info,baseline]");
        }
    }

    private static String showConnectionDetails(Connection connection) {
        try {
            logInfo("Connection Schema: "+connection.getSchema());
            Statement stmt = connection.createStatement();
            String sql;
            sql = "select database()";
            ResultSet rs = stmt.executeQuery(sql);
            String schemaName = rs.getMetaData().getSchemaName(1);
            if (schemaName == null || schemaName.length() == 0) {
                rs.next();
                schemaName = rs.getString(1);
            }
            logInfo("Selected database: "+schemaName);
            return schemaName;
        } catch (SQLException e) {
            logError("Failed to show connection details",e);
            throw new RuntimeException("Failed to show connection details:"+e.getMessage(),e);
        }

    }

    /**
     *
     * @param connection the db connection
     * @return true if the table was created now
     */
    private static boolean createTableIfNecessary(Connection connection) {
        //schema_upgrade_scripts
        try {
            Statement stmt = connection.createStatement();
            String sql;
            sql = "show tables like 'schema_upgrade_scripts'";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                logInfo("Found table 'schema_upgrade_scripts'");
                return true;
            }
            else {
                logInfo("Creating table 'schema_upgrade_scripts'");
                stmt = connection.createStatement();
                sql = "CREATE TABLE `schema_upgrade_scripts` (" +
                        "`id` bigint(20) NOT NULL," +
                        "`description` varchar(255) DEFAULT NULL," +
                        "`execution_time` bigint(20) DEFAULT NULL," +
                        "`installed_on` bigint(20) DEFAULT NULL," +
                        "`type` varchar(255) NOT NULL," +
                        "`upgrade_state` varchar(255) DEFAULT NULL," +
                        "PRIMARY KEY (`id`)" +
                        ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
                boolean execute = stmt.execute(sql);
                if (!execute) {
                    logError("Failed to create schema_upgrade_scripts table");
                }
                return false;
            }
        } catch (SQLException e) {
            logError("Cannot check if the schema upgrade_scripts_table already exists in schema ("+e.getMessage()+") "+e.getClass().getName(),e);
            throw new RuntimeException(e);
        }
    }

    private static void getSqlScriptsInformation(String sqlUpgradeScriptsFolder, Connection con) {
        //Go over the scripts folder
        Path sqlFolder = Paths.get(sqlUpgradeScriptsFolder);
        if (Files.exists(sqlFolder)) {
            try {
                List<UpgradeSchemaDto> sqlScripts = UpgradeSchemaDto.findSqlScriptsInFolder(sqlFolder);
                logInfo("Checking information on "+sqlScripts.size()+" Scripts");
                Collections.sort(sqlScripts);
                for (UpgradeSchemaDto sqlScript : sqlScripts) {
                    boolean scriptInSchema = isScriptInSchema(sqlScript.getVersion(), con);
                    if (!scriptInSchema) {
                        logInfo("* SQL Script: "+sqlScript.prettyString()+" pending");
                    }
                }

            } catch (IOException e) {
                logError("Failed to collect SQL scripts from folder "+sqlFolder,e);
                throw new RuntimeException("Failed to collect SQL scripts",e);
            }
        }
        else {
            logError("Unable to find the upgradeSQL folder "+sqlFolder);
        }
    }

    private static void dropSchema(String schemaName, Connection connection) {
        try {
            Statement stmt = connection.createStatement();
            String sql;
            // DROP SCHEMA docAuthority
            sql = "drop schema "+schemaName;
            boolean execute = stmt.execute(sql);
//            if (!execute) {
//                logError("Failed to drop schema "+schemaName);
//            }
        } catch (SQLException e) {
            logError("Failed to drop schema "+schemaName+ "("+e.getMessage()+") "+e.getClass().getName(),e);
            throw new RuntimeException(e);
        }

    }

    private static boolean isBaselineScript(UpgradeSchemaDto sqlScript) {
        return (sqlScript.getDescription() != null && sqlScript.getDescription().contains("baseline"));
    }

    private static void markAllScriptsAsSkipped(String sqlUpgradeScriptsFolder, Connection connection) {
        logInfo("Mark all scripts in folder as skipped");
        //Go over the scripts folder
        Path sqlFolder = Paths.get(sqlUpgradeScriptsFolder);
        if (Files.exists(sqlFolder)) {
            try {
                List<UpgradeSchemaDto> sqlScripts = UpgradeSchemaDto.findSqlScriptsInFolder(sqlFolder);
                Collections.sort(sqlScripts);
                for (UpgradeSchemaDto sqlScript : sqlScripts) {
                    if (isBaselineScript(sqlScript)) {
                        logInfo("Found baseline SQL script. "+sqlScript+" is not marked as skipped");
                    }
                    else {
                        logInfo("Mark SQL script "+sqlScript+" as skipped");
                        markScriptInSchema(sqlScript,"SKIPPED",connection);
                    }
                }
            } catch (IOException e) {
                logError("Failed to collect SQL scripts from folder "+sqlFolder,e);
                throw new RuntimeException("Failed to collect SQL scripts",e);
            }
        }
        else {
            logError("Unable to find the upgradeSQL folder "+sqlFolder);
        }
    }

    private static void runBaselineScript(String sqlUpgradeScriptsFolder, Connection connection) {
        Path sqlFolder = getSqlFolder(sqlUpgradeScriptsFolder);
        try {
            List<UpgradeSchemaDto> sqlScripts = UpgradeSchemaDto.findSqlScriptsInFolder(sqlFolder);
            Collections.sort(sqlScripts);
            if (isScriptInSchema(0L,connection)) {
                logInfo("BaseLine SQL Script is already in schema. Nothing to do");
            }
            else {
                for (UpgradeSchemaDto sqlScript : sqlScripts) {
                    logInfo("Checking sql Script "+sqlScript);
                    if (isBaselineScript(sqlScript)) {
                        logInfo("Running sql Script "+sqlScript);
                        long start = System.currentTimeMillis();
                        boolean result = runSqlScript(connection, sqlScript.getFile().toString());
                        sqlScript.setExecutionTime(System.currentTimeMillis() -start);
                        String upgradeState = result ? "SUCCESS" : "FAILURE";
                        markScriptInSchema(sqlScript,upgradeState,connection);
                    }
                }
            }
        } catch (IOException e) {
            logError("Failed to collect SQL scripts from folder "+sqlFolder,e);
            throw new RuntimeException("Failed to collect SQL scripts",e);
        }
    }

    private static Path getSqlFolder(String sqlUpgradeScriptsFolder) {
        Path sqlFolder = Paths.get(sqlUpgradeScriptsFolder);
        if (!Files.exists(sqlFolder)) {
            logError("Unable to find the upgradeSQL folder "+sqlFolder);
            throw new RuntimeException("Unable to find the upgradeSQL folder "+sqlFolder);
        }
        return sqlFolder;
    }

    private static void runSqlScripts(String sqlUpgradeScriptsFolder, Connection con) {
        //Go over the scripts folder
        Path sqlFolder = getSqlFolder(sqlUpgradeScriptsFolder);
        try {
            List<UpgradeSchemaDto> sqlScripts = UpgradeSchemaDto.findSqlScriptsInFolder(sqlFolder);
            Collections.sort(sqlScripts);
            for (UpgradeSchemaDto sqlScript : sqlScripts) {
                logInfo("Checking sql Script "+sqlScript);
                if (sqlScript.isJavaUpgradeScript()) {
                    continue;
                }
                if (!isScriptInSchema(sqlScript.getVersion(),con)) {
                    if (sqlScript.getVersion() != null && sqlScript.getVersion() == 0L) {
                        logInfo("skipping baseline script "+sqlScript);
                        continue;
                    }
                    logInfo("Running Script "+sqlScript);
                    long start = System.currentTimeMillis();
                    boolean result = false;
                    if (sqlScript.isSqlUpgradeScript()) {
                        result = runSqlScript(con, sqlScript.getFile().toString());
                    }
                    else if (sqlScript.isBatUpgradeScript()) {
                        result = runBatScript(sqlScript.getFile().toString());
                    }
                    else {
                        logInfo("Unexpected script type " + sqlScript.getScriptType().toString());
                    }
                    sqlScript.setExecutionTime(System.currentTimeMillis() -start);
                    String upgradeState = result ? "SUCCESS" : "FAILURE";
                    markScriptInSchema(sqlScript,upgradeState,con);
                }
            }

        } catch (IOException e) {
            logError("Failed to collect SQL scripts from folder "+sqlFolder,e);
            throw new RuntimeException("Failed to collect SQL scripts",e);
        }
    }

    private static void markScriptInSchema(UpgradeSchemaDto sqlScript, String upgradeState, Connection connection) {
        try {
            Statement stmt = connection.createStatement();
            String sql;
//            INSERT INTO `cla`.`root_folder` (`id`, `base_depth`, `folders_count`, `path`, `doc_store_ignore`, `doc_store_include`)
//              VALUES ('2', '7', '1', '1', '1', '1');

            long id = sqlScript.getVersion();
            String type = sqlScript.getScriptTypeName();
            String description = (sqlScript.getDescription()==null)?"":sqlScript.getDescription();
            String installedOn = String.valueOf(System.currentTimeMillis());
            long executionTime = sqlScript.getExecutionTime();
            sql = "INSERT INTO `schema_upgrade_scripts` " +
                    "(`id`, `description`, `execution_time`, `installed_on`, `type`, `upgrade_state`) " +
                    "VALUES ('"+id+"', '"+description+"', '"+executionTime+"', '"+installedOn+"', '"+type+"', '"+upgradeState+"')";
            boolean execute = stmt.execute(sql);
//            if (!execute) {
//                logError("Failed to update schema_upgrade_script table with new script");
//            }
        } catch (SQLException e) {
            logError("Cannot mark upgrade script in schema ("+e.getMessage()+") "+e.getClass().getName(),e);
            if (e instanceof MySQLIntegrityConstraintViolationException) {
                logError("SQL Integrity constraint - probably duplicate upgrade scripts with the same id");
                throw new RuntimeException("SQL Integrity constraint - probably duplicate upgrade scripts with the same id:" + e.getMessage(),e);
            }
            else {
                throw new RuntimeException(e);
            }
        }
    }

    private static boolean isScriptInSchema(long version, Connection connection) {
        try {
            Statement stmt = connection.createStatement();
            String sql;
            sql = "select * from schema_upgrade_scripts where id="+version;
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                String upgradeState = rs.getString("upgrade_state");
                logInfo("Sql Script "+version+" already in schema");
                return true;
            }
            else {
                return false;
            }
        } catch (SQLException e) {
            logError("Cannot check if the schema upgrade script already exists in schema ("+e.getMessage()+") "+e.getClass().getName(),e);
            throw new RuntimeException(e);
        }
    }


    private static boolean runSqlScript(Connection con,String sqlScriptPath) {
        logInfo("***********************************");
        logInfo("Run script "+sqlScriptPath);
        logInfo("***********************************");
        try {
            // Initialize object for ScripRunner
            ScriptRunner sr = new ScriptRunner(con, false, false);

            // Give the input file to Reader
            Reader reader = new BufferedReader(
                    new FileReader(sqlScriptPath));

            // Exctute script
            sr.runScript(reader);

        } catch (Exception e) {
            logError("Failed to Execute" + sqlScriptPath
                    + " The error is " + e.getMessage(),e);
            return false;
        }
        return true;
    }

    private static boolean runBatScript(String batScriptPath) {
        String tmpBatchName = tempFolder + "\\" + com.google.common.io.Files.getNameWithoutExtension(batScriptPath);
        boolean ret = false;
        if (!Files.isDirectory(Paths.get(tempFolder))) {
            try {
                logInfo("Creating missing tempFolder: " + tempFolder);
                Files.createDirectories(Paths.get(tempFolder));
            } catch (IOException e) {
                logError("Could not create missing tempFolder: " + tempFolder + ". Error: " + e.getMessage(), e);
                return false;
            }
        }
        try {
            String basename = (new File(batScriptPath)).getName();
            (new File(logsFolder)).mkdirs();
            Files.copy(Paths.get(batScriptPath), Paths.get(tmpBatchName), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
            String cmd[] = {"cmd", "/c ",tmpBatchName};
//            logInfo("Executing: " + Arrays.toString(cmd));
            Process p = new ProcessBuilder(cmd)
                    .redirectOutput(new File(logsFolder,basename + ".out"))
                    .redirectError(new File(logsFolder,basename + ".err"))
                    .start();
            p.waitFor(timeoutSec, TimeUnit.SECONDS);
            ret = true;
            if (p.exitValue()!=0) {
                logError("Script exited with code " + p.exitValue());
                ret = false;
            }
        } catch (Exception e) {
            logError("Failed to Execute" + batScriptPath
                    + " The error is " + e.getMessage(), e);
        }
        return ret;
    }

    private static void logError(String s) {
        System.err.println("ERROR: "+s);
    }

    private static void logError(String s, Exception e) {
        System.err.println("ERROR: "+s);
        e.printStackTrace();
    }

    private static void logInfo(String s) {
        System.out.println(s);
    }

}
