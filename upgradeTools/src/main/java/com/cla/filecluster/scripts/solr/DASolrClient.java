package com.cla.filecluster.scripts.solr;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by oren on 5/2/2018.
 */
public class DASolrClient {

    public static final String DEFAULT_PLUGIN_BLOB_NAME = "daplugins";
    private final ObjectMapper mapper = new ObjectMapper();
    
    private String liveNodeUrl;
    
    public DASolrClient(String liveNodeUrl) {
        this.liveNodeUrl = liveNodeUrl;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private HttpResponse sendHttpRequestRequest(HttpClient httpClient, String url, boolean isPostRequest, Optional<HttpEntity> entity) throws IOException {
        HttpRequestBase request;
        if (isPostRequest) {
            HttpPost post = new HttpPost(url);
            post.setEntity(entity.orElse(null));
            request = post;
        } else {
            request = new HttpGet(url);
        }
        return httpClient.execute(request);
    }

    private int getCurrentBlobVersion(String blobName, HttpClient httpClient, String nodeUrl) {
        logInfo("Obtaining blob's version");
        SolrResponseContainer respWrapper;
        try {
            HttpResponse res = sendHttpRequestRequest(httpClient, nodeUrl+ ".system/blob/?omitHeader=true", false, Optional.empty());
            if (res.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                logError("Failed to retrieve solr-blob list from  blob store (http status: " + res.getStatusLine().getStatusCode() + ")");
                throw new RuntimeException("Failed to retrieve solr-blob list from  blob store (http status: " + res.getStatusLine().getStatusCode() + ")");
            }
            respWrapper = mapper.readValue(res.getEntity().getContent(), SolrResponseContainer.class);
        } catch (IOException e) {
            logError("Failed to obtain blob's version (blob name: " + blobName + ")", e);
            throw new RuntimeException("Failed to retrieve solr blob list", e);
        }

        return respWrapper.response.docs.stream()
                .filter(blob -> blobName.equals(blob.blobName))
                .mapToInt(SolrBlobsResponseWrapper.BlobMetadata::getVersion)
                .max()
                .orElse(0);
    }

    public void uploadJar(File jarFile, String blobName, HttpClient httpClient, String aLiveNode) {
        if (!jarFile.exists()) {
            throw new RuntimeException(new FileNotFoundException(jarFile.getAbsolutePath() + " doesn't seem to exist"));
        }

        logInfo("Uploading jar file: " + jarFile.getName() + " under name " + blobName + " to " + aLiveNode);
        HttpEntity jarFileEntity = MultipartEntityBuilder.create().addBinaryBody("binary", jarFile).build();
        SolrRuntimeLibActionResponse solrResp;
        try {
            HttpResponse resp = sendHttpRequestRequest(httpClient, aLiveNode + ".system/blob/" + blobName + "?omitHeader=true", true, Optional.of(jarFileEntity));
            solrResp = mapper.readValue(resp.getEntity().getContent(), SolrRuntimeLibActionResponse.class);
        } catch (IOException e) {
            logError("Failed to upload jar to blob store", e);
            throw new RuntimeException("Failed to upload jar to blob store: " + e.getMessage());
        }

        if (solrResp.error != null) {
            logError("Failed to upload jar: " + solrResp.error);
            throw new RuntimeException("Failed to upload jar: " + solrResp.error);
        }
    }

    public void addRuntimeLib(String collection, String blobName, int version, HttpClient httpClient, String nodeUrl) {
        processRuntimeLibOperation("create", collection, blobName, version, httpClient, nodeUrl);
    }

    public void updateRuntimeLib(String collection, String blobName, int version, HttpClient httpClient, String nodeUrl) {
        processRuntimeLibOperation("update", collection, blobName, version, httpClient, nodeUrl);
    }

    private void processRuntimeLibOperation(String operation, String collection, String blobName, int version, HttpClient httpClient, String nodeUrl) {
        logInfo("Processing runtime-lib config: " + operation + " " + blobName + " version: " + version);
        final String updateLibJsonJson = "{ \"" + operation + "-runtimelib\": {\"name\": \"" + blobName + "\", \"version\": " + version + "}}";
        HttpEntity entity = new StringEntity(updateLibJsonJson, ContentType.APPLICATION_JSON);
        try {
            HttpResponse resp = sendHttpRequestRequest(httpClient, getConfigUrl(collection, nodeUrl), true, Optional.of(entity));
            SolrRuntimeLibActionResponse solrResp = mapper.readValue(resp.getEntity().getContent(), SolrRuntimeLibActionResponse.class);

            if(solrResp.error != null || (solrResp.errorMessages != null && solrResp.errorMessages.length > 0)) {
                boolean isLibNew= Arrays.stream(solrResp.errorMessages)
                        .map(msg -> msg.errorMessages)
                        .flatMap(Arrays::stream)
                        .anyMatch(msg -> msg.endsWith("does not exist . Do an 'create-runtimelib' , if you want to create it "));

                if ("update".equalsIgnoreCase(operation)) {
                    if (isLibNew) {
                        logInfo("Seems like a new blob - attempting to add");
                        processRuntimeLibOperation("create", collection, blobName, version, httpClient, nodeUrl);
                    }
                } else if (!isLibNew) {
                    logInfo("Seems like an existing blob - attempting to update");
                    processRuntimeLibOperation("update", collection, blobName, version, httpClient, nodeUrl);
                } else {
                    String errs = Arrays.stream(solrResp.errorMessages)
                            .map(msg -> msg.errorMessages)
                            .flatMap(Arrays::stream)
                            .collect(Collectors.joining("|"));

                    logError("Failed to process runtime lib: " + errs);
                    throw new RuntimeException("Failed to " + operation + " runtime lib: " + errs);
                }
            }
        } catch (IOException e) {
            logError("Failed to update runtime-lib: " + blobName + " version: " + version);
            throw new RuntimeException("Failed to update runtime-lib: " + blobName + " version: " + version);
        }
    }

    public void deletePluginRuntime(String collection, String pluginName) {
        final String deleteJson = "{ \"delete-runtimelib\": \"" + pluginName + "\" }";

        HttpEntity entity  = new StringEntity(deleteJson, ContentType.APPLICATION_JSON);
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpResponse resp = sendHttpRequestRequest(httpClient, getConfigUrl(collection, liveNodeUrl), true, Optional.of(entity));
            SolrComponentActionResponse solrResp = mapper.readValue(resp.getEntity().getContent(), SolrComponentActionResponse.class);
            validateSolrConfigComponentResponse(solrResp);
        } catch (IOException e) {
            logError("Failed to remove plugin: " + pluginName + " from collection " + collection);
        }
    }

    /**
     * Upgrade existing plugin.
     * Uploads binaries, and switch runtime-lib.
     */
    public void upgradePlugin(String collection, String pathToJar, String blobName) {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            uploadJar(new File(pathToJar), blobName, httpClient, liveNodeUrl);
            int versionForUploadedJar = getCurrentBlobVersion(blobName, httpClient, liveNodeUrl);
            if (versionForUploadedJar == 1) { // probably new runtime lib
                addRuntimeLib(collection, blobName, versionForUploadedJar, httpClient, liveNodeUrl);
            } else {
                updateRuntimeLib(collection, blobName, versionForUploadedJar, httpClient, liveNodeUrl);
            }
        } catch (IOException e) {
            logError("An error occurred while attempting to update plugin", e);
            throw new RuntimeException("An error occurred while attempting to update plugin: " + e.getMessage());
        }
    }

    private void validateSolrConfigComponentResponse(SolrComponentActionResponse solrResp) {
        String err = "";
        if (solrResp.error != null) {
            err += solrResp.error;
        }
        if (solrResp.errorMessages != null && solrResp.errorMessages.length > 0) {
            err += (err.length() == 0 ? "" : ", ") +
                    Arrays.stream(solrResp.errorMessages)
                            .map(errMsg -> errMsg.errorMessages)
                            .flatMap(Arrays::stream)
                            .collect(Collectors.joining(","));
        }
        if (err.length() > 0) {
            logError(err);
            throw new RuntimeException("Failed to config component config: " + err  );
        }
    }

    /**
     * Adds component the collection config overlay
     */
    public void addComponent(String collection, ComponentType componentType, String pluginName, int version, String clsFullyQualifiedName) {
        final String addJson =
                "{\"create-" + componentType .str+ "\": { \"name\": \"" + pluginName + "\", \"class\": \"" + clsFullyQualifiedName + "\", \"version\":\"" + version + "\", \"runtimeLib\": \"true\"} }";

        HttpEntity entity = new StringEntity(addJson, ContentType.APPLICATION_JSON);
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpResponse resp = sendHttpRequestRequest(httpClient, getConfigUrl(collection, liveNodeUrl), true, Optional.of(entity));
            SolrComponentActionResponse solrResp = mapper.readValue(resp.getEntity().getContent(), SolrComponentActionResponse.class);
            validateSolrConfigComponentResponse(solrResp);
        } catch (IOException e) {
            logError("Failed to add plugin: " + pluginName + " from collection " + collection, e);
        }
    }

    /**
     * Modifies component config for the collection config overlay
     */
    public void modifyComponent(String collection, ComponentType componentType, String pluginName, int version, String clsFullyQualifiedName) {
        final String addJson =
                "{\"update-" + componentType .str+ "\": { \"name\": \"" + pluginName + "\", \"class\": \"" + clsFullyQualifiedName + "\", \"version\":\"" + version + "\", \"runtimeLib\": \"true\"} }";

        HttpEntity entity = new StringEntity(addJson, ContentType.APPLICATION_JSON);
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpResponse resp = sendHttpRequestRequest(httpClient, getConfigUrl(collection, liveNodeUrl), true, Optional.of(entity));
            SolrComponentActionResponse solrResp = mapper.readValue(resp.getEntity().getContent(), SolrComponentActionResponse.class);
            validateSolrConfigComponentResponse(solrResp);
        } catch (IOException e) {
            logError("Failed to modify plugin: " + pluginName + " from collection " + collection, e);
        }
    }

    public void deleteComponent(String collection, ComponentType componentType, String pluginName) {
        final String deleteJson = "{ \"delete-" + componentType.str + "\": \"" + pluginName + "\" }";

        HttpEntity entity = new StringEntity(deleteJson, ContentType.APPLICATION_JSON);
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpResponse resp = sendHttpRequestRequest(httpClient, getConfigUrl(collection, liveNodeUrl), true, Optional.of(entity));
            SolrComponentActionResponse solrResp = mapper.readValue(resp.getEntity().getContent(), SolrComponentActionResponse.class);
            validateSolrConfigComponentResponse(solrResp);
        } catch (IOException e) {
            logError("Failed to remove plugin: " + pluginName + " from collection " + collection, e);
        }
    }

    private String getConfigUrl(String collection, String nodeUrl) {
        return nodeUrl + collection + "/config?omitHeader=true";
    }

    private static void logError(String s) {
        logError(s, null);
    }

    private static void logError(String s, Exception e) {
        System.err.println("ERROR: " + s);
        Optional.ofNullable(e).ifPresent(Exception::printStackTrace);
    }

    private static void logInfo(String s) {
        System.out.println(s);
    }

    public enum ComponentType {
        REQUEST_HANDLER("requesthandler"),
        SEARCH_COMPONENT("searchcomponent"),
        QUERY_PARSER("queryparser");

        private String str;

        ComponentType(String str) {
            this.str = str;
        }

    }
}
