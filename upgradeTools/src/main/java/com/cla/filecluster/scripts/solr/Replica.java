package com.cla.filecluster.scripts.solr;

/**
 * Created by oren on 01/10/2017.
 */
public class Replica {

    String name;
    Boolean active;
    Boolean leader;
    String coreName;
    String type;

    public Replica(String name, Boolean active, Boolean leader, String coreName, String type) {
        this.name = name;
        this.active = active;
        this.leader = leader;
        this.coreName = coreName;
        this.type = type;
    }

    public Replica(String name, Boolean active, Boolean leader, String coreName) {
        this(name, active, leader, coreName, null);
    }

    public Replica(String name, Boolean active, Boolean leader) {
        this(name, active, leader, null, null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getLeader() {
        return leader;
    }

    public void setLeader(Boolean leader) {
        this.leader = leader;
    }

    public String getCoreName() {
        return coreName;
    }

    public void setCoreName(String coreName) {
        this.coreName = coreName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Replica replica = (Replica) o;
        if (name != null ? !name.equals(replica.name) : replica.name != null) return false;
        if (coreName != null ? !coreName.equals(replica.coreName) : replica.coreName != null) return false;
        if (active != null ? !active.equals(replica.active) : replica.active != null) return false;
        return leader != null ? leader.equals(replica.leader) : replica.leader == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (coreName != null ? coreName.hashCode() : 0);
        result = 31 * result + (active != null ? active.hashCode() : 0);
        result = 31 * result + (leader != null ? leader.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return name + " " + (active ? "a " : "na") + " " + (leader ? "l " : "nl") + (type == null ? "" : (" " + type)) + (coreName == null ? "" : (" " + coreName));
    }
}
