package com.cla.filecluster.scripts.solr;

import com.fasterxml.jackson.annotation.JsonProperty;
/**
 * Created by oren on 4/29/2018.
 */
public class SolrBaseResponse<T> {

    public String error;

    public SolrBlobsResponseWrapper response;

    @JsonProperty("WARNING")
    public String warning;

    public T[] errorMessages;
}
