package com.cla.filecluster.scripts.solr;

/**
 * Created by oren on 01/10/2017.
 */
public enum ExitCode {

    NOT_FOUND (-1),
    IN_PROGRESS (1),
    FAILED(2),
    COMPLETED(0);

    int code;

    private ExitCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}
