package com.cla.filecluster.scripts.solr;

/**
 * Created by oren on 4/29/2018.
 */
public class SolrResponseContainer {
    public SolrBlobsResponseWrapper response;

    @Override
    public String toString() {
        return "SolrResponseContainer{" +
                "response=" + response.toString() +
                '}';
    }
}
