package com.cla.filecluster.scripts;

import com.cla.filecluster.scripts.solr.DASolrClient;
import com.cla.filecluster.scripts.solr.ExitCode;
import com.cla.filecluster.scripts.solr.Replica;
import com.cla.filecluster.scripts.solr.ShardAlignment;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrResponse;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.RequestStatusState;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.cloud.SolrZkClient;
import org.apache.solr.common.cloud.ZkConfigManager;
import org.apache.solr.common.params.CursorMarkParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by uri on 05-Jun-17.
 */
public class SolrCloudConfigurer {

    public static final Logger logger = LoggerFactory.getLogger(SolrCloudConfigurer.class);

    public static final String CREATE_COLLECTION_IF_NEEDED_COMMAND = "create-collection-if-needed";
    public static final String CREATE_COLLECTION_COMMAND = "create-collection";
    public static final String DELETE_COLLECTIONS_COMMAND = "delete-collections";
    public static final String RELOAD_COLLECTIONS_COMMAND = "reload-collections";
    public static final String RELOAD_COLLECTION_COMMAND = "reload-collection";
    public static final String UPLOAD_CONFIG_COMMAND = "zk-upconfig";
    public static final String ZK_MAKE_ROOT = "zk-mkdirs";
    public static final String DUMP_COLLECTION_STATUS_COMMAND = "dump-collection-status";
    public static final String ASYNC_OPERATION_STATUS_COMMAND = "async-operation-status";
    public static final String SPLIT_SHARD_COMMAND = "split-shard";
    public static final String REPLICATE_COLLECTION_COMMAND = "replicate-collection";
    public static final String SPLIT_AND_MOVE_SHARD_COMMAND = "split-and-move-shard";
    public static final String FORCE_LEADER_COMMAND = "force-leader";
    public static final String MOVE_REPLICA_COMMAND = "move-replica";
    public static final String ALIGN_SHARDS_COMMAND = "align-shards";
    public static final String ALIGN_DA_SHARDS_COMMAND = "align-da-shards";
    public static final String LIST_ALIASES_COMMAND = "list-aliases";
    public static final String LIST_NODES_COMMAND = "list-nodes";
    public static final String CREATE_ALIAS_COMMAND = "create-alias";
    public static final String DELETE_ALIAS_COMMAND = "delete-alias";
    public static final String MIGRATE_COLLECTION_COMMAND = "migrate-collection";
    public static final String MIGRATE_COLLECTION_2017_10_COMMAND = "migrate-2017-10";
    public static final String BACKUP_COMMAND = "backup";
    public static final String BACKUP_ALL_COMMAND = "backup-all";
    public static final String RESTORE_COMMAND = "restore";
    public static final String RESTORE_ALL_COMMAND = "restore-all";
    public static final String REMOVE_TASK_ID_COMMAND = "remove-task-id";
    public static final String REMOVE_ALL_TASK_IDS = "remove-all-task-ids";
    public static final String DELETE_DOWN_REPLICAS_COMMAND = "delete-down-replicas";
    public static final String DELETE_LEGACY_REPLICAS_COMMAND = "delete-legacy-down-replicas";
    public static final String DELETE_MIGRATED_REPLICAS_COMMAND = "delete-fornax-migrated-replicas";
    public static final String DELETE_MIGRATED_COLLECTIONS_COMMAND = "delete-fornax-migrated-collections";

    public static final String PLUGIN_UPGRADE = "plugin-upgrade";
    public static final String PLUGIN_UPLOAD = "plugin-upload";
    public static final String PLUGIN_ADD_RUNTIME_LIB = "plugin-add-runtime-lib";
    public static final String PLUGIN_MODIFY_RUNTIME_LIB = "plugin-modify-runtime-lib";
    public static final String PLUGIN_DELETE_RUNTIME_LIB = "plugin-delete-runtime-lib";

    public static final String PLUGIN_ADD_COMPONENT = "plugin-add-component";
    public static final String PLUGIN_MODIFY_COMPONENT = "plugin-modify-component";
    public static final String PLUGIN_DELETE_COMPONENT = "plugin-delete-component";

    private static final String PREFERRED_LEADER_PROPERTY = "preferredLeader";
    private static final String CURSOR_MARK_COMMAND_PROPERTY = "cursor=";
    private static final String ACTIVE = "active";
    private static final int REPLICA_BECOMING_ACTIVE_TIMEOUT = 600; // in seconds
    private static final int POLING_INTERVAL = 2; // in seconds
    private static final int COLLECTION_TRANSFORM_DEFAULT_BATCH_SIZE = 1000;
    private static final int COMMIT_WITHIN_MS = 1000;
    private static final String BACKUP_TIMESTAMP_FORMAT = "yyyyMMddHHmm";
    private static final int UPCONFIG_CLIENT_TIMEOUT_MS = 60000;
    private static final int ADMIN_ACTION_TIMEOUT_SECONDS = 15;

    static final private String FORNAX_MIGRATED_COLLECTION_NAME_SUFFIX = "_2";

    private static final String DEFAULT_PLUGIN_BLOB_NAME = "daplugs";

    private SolrClient solrClient;
    private ObjectMapper objectMapper = new ObjectMapper();

    public SolrCloudConfigurer(SolrClient solrClient) {
        this.solrClient = solrClient;
    }

    public List<String> listCollections() {
        logInfo("List Collections");
        CollectionAdminRequest listRequest = new CollectionAdminRequest.List();
        List<String> result = new ArrayList<>();
        try {
            SolrResponse process = listRequest.process(solrClient);
            List collections = (List) process.getResponse().get("collections");
            for (Object collection : collections) {
                result.add(collection.toString());
            }
            return result;
        } catch (SolrServerException e) {
            throw new RuntimeException("Failed to list Solr collections", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to list Solr collections", e);
        }
    }

    public void deleteAllCollections() {
        List<String> collections = listCollections();
        for (String collection : collections) {
            deleteCollection(collection);
        }

    }

    private void reloadAllCollections() {
        List<String> collections = listCollections();
        for (String collection : collections) {
            reloadCollection(collection);
        }
    }

    private void reloadCollection(String collection) {
        logInfo("Reload collection " + collection);
        CollectionAdminRequest.Reload reload = CollectionAdminRequest.reloadCollection(collection);
        try {
            reload.process(solrClient);
        } catch (SolrServerException e) {
            throw new RuntimeException("Failed to reload Solr collection", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to reload Solr collection", e);
        }
    }

    public void deleteCollection(String collection) {
        logInfo("Delete Collection " + collection);
        CollectionAdminRequest.Delete delete = CollectionAdminRequest.deleteCollection(collection);
        try {
            delete.process(solrClient);
        } catch (SolrServerException e) {
            throw new RuntimeException("Failed to delete Solr collection", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to delete Solr collection", e);
        }
    }

    public void deleteCollectionAsync(String collection) {
        String taskId = "delete_coll_" + collection;
        verifyTaskDoNotExist(taskId);
        CollectionAdminRequest.Delete delete = CollectionAdminRequest.deleteCollection(collection);
        try {
            delete.processAsync(taskId, solrClient);
        } catch (IOException | SolrServerException | SolrException e) {
            logError("Failed to delete collection " + collection, e);
        }
    }


    private void createCollectionIfNeeded(String collectionName, int numberOfShards, String configName, String rules) {
        boolean collectionExists = isCollectionExists(collectionName);
        if (collectionExists) {
            logInfo("Collection exists - reload");
            reloadCollection(collectionName);
        } else {
            logInfo("Collection doesn't exist - creating");
            createCollection(collectionName, numberOfShards, configName, rules);
        }
    }

    private void uploadConfig(String zkhost, String configFilePath, String configName) {
        Path path = null;
        try {
            path = Paths.get(configFilePath);
        } catch (Exception e) {
            throw new RuntimeException("Illegal path " + configFilePath, e);
        }
        if (!Files.isDirectory(path)) {
            throw new RuntimeException("Could not open directory " + configFilePath);
        }
        SolrZkClient zkClient = null;
        try {
            zkClient = new SolrZkClient(zkhost, UPCONFIG_CLIENT_TIMEOUT_MS, UPCONFIG_CLIENT_TIMEOUT_MS, () -> {});
            ZkConfigManager configManager = new ZkConfigManager(zkClient);
            configManager.uploadConfigDir(path, configName, null);
        } catch (Exception e) {
            throw new RuntimeException("Failed upload configuration " + configFilePath, e);
        }
        logInfo("Uploaded configuration " + configFilePath + " for config " + configName);
    }

    private void mkdir(String zkhost, String zkdir) {
        Path path = null;
        SolrZkClient zkClient = null;
        try {
            zkClient = new SolrZkClient(zkhost, UPCONFIG_CLIENT_TIMEOUT_MS, UPCONFIG_CLIENT_TIMEOUT_MS, () -> {});
            zkClient.makePath(zkdir, false, true);
        } catch (Exception e) {
            throw new RuntimeException("Failed makepath " + zkdir, e);
        }
        logInfo("Done creating zookeeper path " + zkdir);
    }

//    private void uploadConfig(String configFilePath, String configName) {
//        Path path = null;
//        try {
//            path = Paths.get(configFilePath);
//        } catch (Exception e) {
//            throw new RuntimeException("Illegal path "+ configFilePath, e);
//        }
//        if (!Files.isRegularFile(path)) {
//            throw new RuntimeException("Could not open file "+ configFilePath);
//        }
//        try {
//            cloudSolrClient.uploadConfig(path, configName);
//        } catch (Exception e) {
//            throw new RuntimeException("Failed upload configuration "+ configFilePath, e);
//        }
//        logInfo("Uploaded configuration " + configFilePath + " for config " + configName);
//    }

    private boolean isCollectionExists(String collectionName) {
        List<String> collections = listCollections();
        for (String collection : collections) {
            if (collection.equalsIgnoreCase(collectionName)) {
                return true;
            }
        }
        return false;
    }

    public CollectionAdminResponse createCollection(String collectionName, int numOfShards, String configName, String rules) {
        CollectionAdminRequest.Create create = CollectionAdminRequest.createCollection(collectionName, configName, numOfShards, 1);
        if (rules != null) {
            String [] rulesSplit = rules.split(";");    // Allow single rule with multiple comma-separated items
            create.setRule(rulesSplit);
        }
//        create.setCreateNodeSet("localhost:8983_solr, localhost:8984_solr");
//        CollectionAdminRequest.Create create = new CollectionAdminRequest.Create();
//        create.setCollectionName(collectionName);
//        create.setNumShards(numOfShards);
        create.setMaxShardsPerNode(numOfShards);
//        create.setConfigName(configName);
        logInfo("Create Collection " + collectionName + " with " + numOfShards + " shards on config " + configName);
        try {
            CollectionAdminResponse response = create.process(solrClient);
            logInfo("Finished Creating Collection");
            return response;
        } catch (SolrServerException e) {
            throw new RuntimeException("Failed to create Solr collection", e);
        } catch (IOException e) {
            throw new RuntimeException("Failed to create Solr collection", e);
        }
    }

    //    ------

    public String replicateCollection(String collectionName, String shardName, String targetNode) {
        String requestId = "replicate_" + collectionName + "_" + shardName + "_" + targetNode;
        verifyTaskDoNotExist(requestId);
        CollectionAdminRequest.AddReplica addReplica = CollectionAdminRequest.addReplicaToShard(collectionName, shardName);
        Optional.ofNullable(targetNode).ifPresent(node -> addReplica.setNode(node));

        try {
            addReplica.processAsync(requestId, solrClient);
        } catch (HttpSolrClient.RemoteSolrException | SolrServerException | IOException e) {
            throw new RuntimeException("Failed to replicate collection", e);
        }

        return requestId;
    }

    private void verifyTaskDoNotExist(String requestId) {
        if (getAsyncRequestStatus(requestId, false) != ExitCode.NOT_FOUND) {
            // TODO re think this logic!
            logInfo(String.format("Warning: Task with id %s already exists. Removing it if it's finished and proceeding", requestId));
            removeFinishedAsyncResponseStatus(requestId);
        }
    }

    public String splitShard(String collectionName, String shardName) {
        String requestId = "Split_" + collectionName + "_" + shardName;
        verifyTaskDoNotExist(requestId);
        try {
            requestId = CollectionAdminRequest.splitShard(collectionName)
                    .setShardName(shardName)
                    .processAsync(requestId, solrClient);
        } catch (IOException | SolrServerException | SolrException e) {
            throw new RuntimeException("Failed to splitShard for collection: " + collectionName, e);
        }
        logInfo("Task id: " + requestId);
        return requestId;
    }

    public void generateAlignShardsScript(List<String> collectionGroup, Writer outputWriter, final boolean isNew) throws IOException {
        if (!(solrClient instanceof CloudSolrClient)) {
            throw new RuntimeException("Solr configured is not via ZooKeeper");
        }
        CloudSolrClient cloudSolrClient = (CloudSolrClient) solrClient;
        outputWriter.append(isNew?
                "@ECHO OFF\n" +
                        "IF [%1] == [do-align] GOTO doAlign\n" +
                        "ECHO Usage:\n" +
                        "ECHO        %0 do-align\n" +
                        "exit /b 0\n\n" +
                        ":doAlign\n" :
                "\n\n");

        outputWriter.append(String.format("ECHO Aligning collection shards of %s with %s\n",
                collectionGroup.stream().skip(1).collect(Collectors.joining(",")),
                collectionGroup.get(0)));
        List<ShardAlignment> shardToBeAligned = validateShardAlignments(collectionGroup);
        if (shardToBeAligned.size() > 0) {
            for (ShardAlignment anAlignment : shardToBeAligned) {
                outputWriter.append("ECHO ").append(anAlignment.getAlignDescription()).append("\n");
                outputWriter.append(anAlignment.getAlignCommand(cloudSolrClient.getZkHost())).append("\n");
            }
        } else {
            outputWriter.append("ECHO ... nothing to align\n");
        }
    }

    /**
     * @param collectionGroup Group of collections. First element is the alignment leader.
     * @return shards needed to be aligned.
     */
    private List<ShardAlignment> validateShardAlignments(List<String> collectionGroup) {
        SimpleOrderedMap clusterStatusMap = getClusterStatusMap();
        SimpleOrderedMap collectionStatus = ((SimpleOrderedMap) clusterStatusMap.get("collections"));
//         <NodeName, <CollectionName, <ShardName, Replica>>
        Map<String, Map<String, Map<String, List<Replica>>>> nodesMap = getNodeCollectionStatuses(collectionStatus);
//         <Collection, <Shard, Node>
        Map<String, Map<String, String>> collectionShardMap = transformNodeMapToCollectionMap(nodesMap);

        String followedBy = collectionGroup.get(0);
        Map<String, String> followedByShards = collectionShardMap.get(followedBy);
        List<ShardAlignment> invalidAlignmentShards = new ArrayList<>();
        followedByShards.forEach((fbShard, fbNode) -> {
            for (int idx = 1; idx < collectionGroup.size(); idx++) {
                String follower = collectionGroup.get(idx);
                Map<String, String> followerShardMap = collectionShardMap.get(follower);
                String followerNode = followerShardMap.get(fbShard);
                if (!fbNode.equalsIgnoreCase(followerNode)) {
                    invalidAlignmentShards.add(new ShardAlignment(follower, fbShard, followerNode, fbNode));
                }
            }
        });

        return invalidAlignmentShards;
    }

    /**
     * @param nodesMap
     * @return Map<Collection, Map<Shard, Node>>
     */
    private Map<String, Map<String, String>> transformNodeMapToCollectionMap(Map<String, Map<String, Map<String, List<Replica>>>> nodesMap) {
        Map<String, Map<String, String>> output = new HashMap<>();
        nodesMap.forEach((node, collectionMap) -> {
            collectionMap.forEach((collection, shards) -> {
                Map<String, String> outputShardMap = getFromMapOrPutIfNoneExist(output, collection, () -> new HashMap<String, String>());
                shards.forEach((shardKey, replicas) -> {
                    Optional<Replica> leader =
                            replicas.stream()
                                    .filter(aReplica -> Boolean.TRUE.equals(aReplica.getLeader()))
                                    .findAny();
                    if (leader.isPresent()) {
                        outputShardMap.put(shardKey, node);
                    }
                });
            });
        });

        return output;
    }

    private <T, K> K getFromMapOrPutIfNoneExist(Map<T, K> map, T key, Supplier<K> valSupplier) {
        return Optional.ofNullable(map.get(key)).orElseGet(() -> {
            K val = valSupplier.get();
            map.put(key, val);
            return val;
        });
    }

    public void printClusterStatuses() {
        SimpleOrderedMap responseMap = getClusterStatusMap();
        SimpleOrderedMap collectionStatus = ((SimpleOrderedMap) responseMap.get("collections"));

        List<String> liveNodes = (List) responseMap.get("live_nodes");
        liveNodes.forEach(node -> logInfo(node));
        logInfo("");

        Map<String, Map<String, Map<String, List<Replica>>>> nodesMap = getNodeCollectionStatuses(collectionStatus);
        dumpNodeMap(nodesMap);
    }

    public boolean isCollectionAlive(String collectionName) {
        SimpleOrderedMap responseMap = getClusterStatusMap();
        SimpleOrderedMap allCollectionStatus = (SimpleOrderedMap) responseMap.get("collections");

        SimpleOrderedMap collectionStatus = (SimpleOrderedMap) allCollectionStatus.get(collectionName);
        if (collectionStatus == null) {
            throw new RuntimeException("Could not find collection: " + collectionName);
        }
        SimpleOrderedMap shards = (SimpleOrderedMap) allCollectionStatus.get("shards");
        boolean[] shardDown = {false};
        ((Map) shards).keySet().forEach(shardName -> {
            if (!shardDown[0]) {
                SimpleOrderedMap shardEntryMap = (SimpleOrderedMap) shards.get((String) shardName);
                Map<String, Map<String, String>> replicas = (Map) shardEntryMap.get("replicas");
                boolean[] foundOne = {false};
                replicas.values()
                        .forEach(replica -> {
                            if (!foundOne[0]) {
                                String state = replica.get("state");
                                if (ACTIVE.equals(state)) {
                                    foundOne[0] = true;
                                }
                            }
                        });
                if (!foundOne[0]) {
                    logInfo("No active replica for shard " + shardName + " of collection " + collectionName);
                    shardDown[0] = true;
                }
            }
        });
        return !shardDown[0];
    }

    public Map<String, String> listAliases() {
        try {
            Map<String, String> aliases = new CollectionAdminRequest.ListAliases().process(solrClient).getAliases();
            if (aliases != null) {
                dumpAliasesMap(aliases);
            }
            return aliases;
//            return Collections.unmodifiableMap(aliases);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to ListAliases", e);
        }
    }

    public List<String> listNodes() {
        try {
            List<String> nodes = getClusterLiveNodes();
            if (nodes != null) {
                logInfo(nodes.stream().collect(Collectors.joining(",")));
            } else {
                logInfo("");
            }
            return nodes;
//            return Collections.unmodifiableList(nodes);
        } catch (Exception e) {
            logError("Failed to getClusterLiveNodes", e);
        }
        return null;
    }

    public CollectionAdminResponse createAlias(String aliasName, String... collections) {
        return createAlias(aliasName, Arrays.asList(collections).stream().collect(Collectors.joining(",")));
    }

    public CollectionAdminResponse createAlias(String aliasName, String collections) {
        CollectionAdminResponse response;
        try {
            response = CollectionAdminRequest.createAlias(aliasName, collections)
                    .process(solrClient);
            if (!response.isSuccess() && response.getErrorMessages() != null) {
                logError(String.format("Failed to createAlias %s for collections: %s. Error: %s", aliasName, collections, response.getErrorMessages()));
            }
            return response;
        } catch (IOException | SolrServerException | SolrException e) {
            throw new RuntimeException("Failed to createAlias " + aliasName + " for collections: " + collections, e);
        }
    }

    public void deletedAlias(String aliasName) {
        CollectionAdminResponse response;
        try {
            response = CollectionAdminRequest.deleteAlias(aliasName)
                    .process(solrClient);
            if (!response.isSuccess() && response.getErrorMessages() != null) {
                logError(String.format("Failed to deletedAlias %s. Error: %s", aliasName, response.getErrorMessages()));
            }
        } catch (IOException | SolrServerException | SolrException e) {
            throw new RuntimeException("Failed to deletedAlias " + aliasName, e);
        }
    }

    public SimpleOrderedMap getClusterStatusMap() {
        CollectionAdminResponse response;
        try {
            response = CollectionAdminRequest.getClusterStatus().process(solrClient);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to retrieve cluster status", e);
        }
        return (SimpleOrderedMap) response.getResponse().get("cluster");
    }

    public JsonNode getCollectionState(String collection) throws IOException {
        NamedList<Object> response;
        try {
            response = solrClient.request(CollectionAdminRequest.getClusterStatus(), collection);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to retrieve cluster status", e);
        }
        JsonNode cluster = objectMapper.readTree(((SimpleOrderedMap) response.get("cluster")).jsonStr());
        return cluster.get("collections").get(collection);
    }

    private static List<String> groupingCollecions = Arrays.asList("PVS", "AnalysisData", "ContentMetadata", "GrpHelper");
    private static List<String> fileCollecions = Arrays.asList("Extraction", "CatFiles", "BizList", "FileGroups");

    private void dumpNodeMap(Map<String, Map<String, Map<String, List<Replica>>>> nodesMap) {
        StringBuilder strBuilder = new StringBuilder();
        for (String node : nodesMap.keySet()) {
            strBuilder.append("Node: ").append(node).append("\n");

            Map<String, Map<String, List<Replica>>> collections = nodesMap.get(node);
            List<String> sortedCollections = collections.keySet().stream()
                    .sorted((a, b) -> ratedCollectionName(a).compareTo(ratedCollectionName(b))).collect(Collectors.toList());
            String prevIndex = null;
            for (String col : sortedCollections) {
                Map<String, List<Replica>> shards = collections.get(col);
                // Add blank line between DA collection groups
                String index = getCollectionGroupIndex(col);
                if (prevIndex != null && !prevIndex.equals(index)) {
                    strBuilder.append("\n");
                }
                prevIndex = index;
                generateCollectionDump(strBuilder, col, shards);
            }
            strBuilder.append("\n");
        }
        logInfo(strBuilder.toString());
    }

    public int deleteDeadReplicas(boolean deletedNullType) {
        SimpleOrderedMap responseMap = getClusterStatusMap();
        SimpleOrderedMap collectionStatus = ((SimpleOrderedMap) responseMap.get("collections"));
        Map<String, Map<String, Map<String, List<Replica>>>> nodesMap = getNodeCollectionStatuses(collectionStatus);
        int count = doDeleteDeadReplicas(nodesMap, deletedNullType);
        return count;
    }

    private int doDeleteDeadReplicas(Map<String, Map<String, Map<String, List<Replica>>>> nodesMap, boolean deletedNullType) {
        int count = 0;
        for (String node : nodesMap.keySet()) {
            Map<String, Map<String, List<Replica>>> collections = nodesMap.get(node);
            List<String> sortedCollections = collections.keySet().stream()
                    .sorted((a, b) -> ratedCollectionName(a).compareTo(ratedCollectionName(b))).collect(Collectors.toList());
            String prevIndex = null;
            for (String col : sortedCollections) {
                Map<String, List<Replica>> shards = collections.get(col);
                // Add blank line between DA collection groups
                String index = getCollectionGroupIndex(col);
                prevIndex = index;
                count += doDeleteDeadReplicasForShards(col, shards, deletedNullType);
            }
        }
        return count;
    }

    private int doDeleteDeadReplicasForShards(String collection, Map<String, List<Replica>> shards, boolean deletedNullType) {
        int count = 0;
        for (String shardName : shards.keySet()) {
            for (Replica rep : shards.get(shardName)) {
                if (!rep.getActive() && (!deletedNullType || rep.getType() == null)) {
                    doDeleteSingleDeadReplica(collection, shardName, rep);
                    count++;
                }
            }
        }
        return count;
    }

    private void doDeleteSingleDeadReplica(String collection, String shardName, Replica rep) {
        CollectionAdminResponse response;
        try {
            // This is different than 'deleteReplica(collectionName, shardName, replica)' as it should not delete the actual data.
            CollectionAdminRequest.DeleteReplica request = CollectionAdminRequest.deleteReplica(collection, shardName, rep.getName());
            request.setDeleteDataDir(false).setDeleteIndexDir(false).setDeleteInstanceDir(false).setOnlyIfDown(true);
            String asyncId = request.processAsync(solrClient);
            logInfo(String.format("Deleting replica %s|%s|%s (%s, %s). AsyncID=%s",
                    collection, shardName, rep.getName(), rep.getActive()?"active":"down", rep.getLeader()?"leader":"not-leader", asyncId));
        } catch (IOException | SolrServerException | SolrException e) {
            throw new RuntimeException(String.format("Failed to DeleteReplica %s|%s|%s",collection, shardName, rep.getName()), e);
        }
    }

    public int deleteMigratedReplicas(boolean deleteCollection) {
        SimpleOrderedMap responseMap = getClusterStatusMap();
        SimpleOrderedMap collectionStatus = ((SimpleOrderedMap) responseMap.get("collections"));
        Map<String, Map<String, Map<String, List<Replica>>>> nodesMap = getNodeCollectionStatuses(collectionStatus);
        int count = doDeleteMigratedReplicas(nodesMap, deleteCollection);
        return count;
    }

    private int doDeleteMigratedReplicas(Map<String, Map<String, Map<String, List<Replica>>>> nodesMap, boolean deleteCollection) {
        int count = 0;
        for (String node : nodesMap.keySet()) {
            Map<String, Map<String, List<Replica>>> collections = nodesMap.get(node);
            for (String col : collections.keySet()) {
                String migratedOrigColName = getOriginalCollectionName(col);
                if (migratedOrigColName != null) {
                    // Should delete
                    if (deleteCollection) {
                        deleteCollectionAsync(migratedOrigColName);
                        logInfo(String.format("Deleting migrated collection %s",migratedOrigColName));
                        count++;
                    } else {
                        Map<String, List<Replica>> shardsToDelete = collections.get(migratedOrigColName);
                        // Add blank line between DA collection groups
                        count += doDeleteMigratedReplicasForShards(migratedOrigColName, shardsToDelete);
                    }
                }
            }
        }
        return count;
    }

    private String getOriginalCollectionName(String col) {
        return col.endsWith(FORNAX_MIGRATED_COLLECTION_NAME_SUFFIX) ?
                col.substring(0, col.length() - FORNAX_MIGRATED_COLLECTION_NAME_SUFFIX.length()) : null;
    }

    private int doDeleteMigratedReplicasForShards(String collection, Map<String, List<Replica>> shards) {
        int count = 0;
        for (String shardName : shards.keySet()) {
            for (Replica rep : shards.get(shardName)) {
                logInfo(String.format("Deleting migrated replica %s|%s|%s (%s, %s)",
                        collection, shardName, rep.getName(), rep.getActive()?"active":"down", rep.getLeader()? "leader" : "not-leader"));

                deleteReplica(collection, shardName, rep.getName());
                count++;
            }
        }
        return count;
    }

    private static String ratedCollectionName(final String name) {
        return getCollectionGroupIndex(name) + name;
    }

    private static String getCollectionGroupIndex(String name) {
        return groupingCollecions.contains(name) ? "1" : (fileCollecions.contains(name) ? "2" : "3");
    }

    private void generateCollectionDump(StringBuilder strBuilder, String collection, Map<String, List<Replica>> shards) {
        int collectionTextLength = 20;
        int shardTextLength = 12;

        for (String shardName : shards.keySet()) {
            for (Replica rep : shards.get(shardName)) {
                strBuilder.append("\t").append(String.format("%-" + collectionTextLength + "s", collection)).append(": ");
                strBuilder.append(String.format("%-" + shardTextLength + "s", shardName));
                strBuilder.append(rep.toString() /*.replace(collection + "_", "") */);
                strBuilder.append("\n");
            }
        }
    }

    private Map<String, Map<String, Map<String, List<Replica>>>> getNodeCollectionStatuses(SimpleOrderedMap collectionStatus) {
//         <NodeName, <CollectionName, <ShardName, Replica>>
        Map<String, Map<String, Map<String, List<Replica>>>> nodesMap = new HashMap<>();
        for (int idx = 0; idx < collectionStatus.size(); idx++) {
            String currentCollectionName = collectionStatus.getName(idx);
            Map aCol = (Map) collectionStatus.getVal(idx);
            Map<String, Map<String, Object>> shardMap = (Map<String, Map<String, Object>>) aCol.get("shards");
            populateShardStatuses(nodesMap, currentCollectionName, shardMap);
        }

        return nodesMap;
    }

    private void populateShardStatuses(Map<String, Map<String, Map<String, List<Replica>>>> nodesMap, String currentCollectionName, Map<String, Map<String, Object>> shardMap) {
        Set<String> shardKeys = shardMap.keySet();
        for (String aShardKey : shardKeys) {
            Map<String, Object> shardEntryMap = shardMap.get(aShardKey);
            Map<String, Map<String, String>> replicas = (Map) shardEntryMap.get("replicas");
            replicas.keySet()
                    .forEach(repKey -> {
                        Map<String, String> aRep = replicas.get(repKey);
                        addReplicaToNodeMap(
                            nodesMap,
                            currentCollectionName,
                            aRep.get("node_name"),
                            aShardKey,
                            repKey,
                            aRep.get("state"),
                            Boolean.valueOf(aRep.get("leader")),
                            aRep.get("core"),
                            aRep.get("type"));
                    });

            if (replicas.size() == 0) {
                addReplicaToNodeMap(nodesMap, currentCollectionName, null, aShardKey, null, null, false, null, null);
            }
        }
    }

    private void addReplicaToNodeMap(Map<String, Map<String, Map<String, List<Replica>>>> nodesMap,
                                     String collection, String node, String shardName, String repName, String repState,
                                     boolean isLeader, String coreName, String repType) {
        Map<String, Map<String, List<Replica>>> collectionMap = getFromMapOrPutIfNoneExist(nodesMap, node, () -> new HashMap<>());
        Map<String, List<Replica>> colStatus = getFromMapOrPutIfNoneExist(collectionMap, collection, () -> new LinkedHashMap<>());
        List<Replica> replicas = getFromMapOrPutIfNoneExist(colStatus, shardName, () -> new ArrayList<>());
        if (repName != null || repState != null) {
            replicas.add(new Replica(repName, ACTIVE.equals(repState), isLeader, coreName, repType));
        }
    }

    private static void dumpAliasesMap(Map<String, String> aliases) {
        StringBuilder strBuilder = new StringBuilder();
        aliases.entrySet().forEach(e -> {
            strBuilder.append("Alias: ").append(e.getKey()).append(" -> ").append(e.getValue()).append("\n");
        });
        strBuilder.append("\n");
        logInfo(strBuilder.toString());
    }

    public ExitCode getAsyncRequestStatus(String requestId, boolean verbose) {
        CollectionAdminRequest.RequestStatusResponse response;

        try {
            CollectionAdminRequest.RequestStatus statusRequest = CollectionAdminRequest.requestStatus(requestId);
            response = statusRequest.process(solrClient);
        } catch (IOException | SolrServerException | SolrException e) {
            throw new RuntimeException("Failed to retrieve status for async operation with id " + requestId, e);
        }

        RequestStatusState state = response.getRequestStatus();
        ExitCode resultCode;
        String verboseMsg;
        switch (state) {
            case RUNNING:
            case SUBMITTED:
                resultCode = ExitCode.IN_PROGRESS;
                verboseMsg = "Task is still running";
                break;

            case FAILED:
                resultCode = ExitCode.FAILED;
                verboseMsg = "Request failed, reason: " + ((SimpleOrderedMap) response.getResponse().get("exception")).get("msg").toString();
                break;

            case NOT_FOUND:
                resultCode = ExitCode.NOT_FOUND;
                verboseMsg = "Task with request id wasn't found on server";
                break;

            case COMPLETED:
                resultCode = ExitCode.COMPLETED;
                verboseMsg = "Task completed successfully";
                break;

            default:
                throw new RuntimeException("Undefined state " + state);
        }

        if (verbose) {
            logInfo(String.format("Operation with id %s exited with code %d, message: %s", requestId, resultCode.getCode(), verboseMsg));
        }

        return resultCode;
    }

    public void dumpOverseerStatus() {
        CollectionAdminRequest.OverseerStatus clus = CollectionAdminRequest.getOverseerStatus();
        CollectionAdminResponse stat;
        try {
            stat = clus.process(solrClient);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to get overseer status", e);
        }

        List<String> operations = Arrays.asList("am_i_leader", "addreplica", "overseerstatus");
        NamedList collectionOperations = (NamedList) stat.getResponse().get("collection_operations");

        StringBuilder errorString = new StringBuilder();
        for (int idx = 0; idx < operations.size(); idx++) {
            errorString.append(String.format("%-14s: ", operations.get(idx)));
            NamedList anOperation = (NamedList) collectionOperations.get(operations.get(idx));
            int errors = (int) anOperation.get("errors");
            if (errors > 0) {
                List<SimpleOrderedMap> failure = (ArrayList) anOperation.get("recent_failures");
                errorString.append(String.format("Errors detected!\tDisplaying last %d failures:", failure.size()));
                failure.forEach(err -> {
                    Map<String, String> reqEntry = (Map<String, String>) err.get("request");
                    NamedList<String> resEntry = (SimpleOrderedMap) ((NamedList) err.get("response")).get("exception");
                    errorString.append(String.format("\n%20s%s: %s", "", reqEntry.get("async"), resEntry.get("msg")));
                });
            } else {
                errorString.append("ok");
            }
            errorString.append("\n");
        }
        logInfo(errorString.toString());
    }


    private Map<String, Map<String, String>> getReplicaStatus(String collectionName, String shardName) {
        SimpleOrderedMap responseMap = getClusterStatusMap();
        SimpleOrderedMap collectionStatus = ((SimpleOrderedMap) responseMap.get("collections"));
        Map collection = (Map) collectionStatus.get(collectionName);
        Map<String, Map<String, Object>> shardMap = (Map<String, Map<String, Object>>) collection.get("shards");
        return (Map<String, Map<String, String>>) shardMap.get(shardName).get("replicas");
    }

    private String extractReplicasNodeName(String collectionName, String shardName, String targetNode) {
        Map<String, Map<String, String>> replicas = getReplicaStatus(collectionName, shardName);
        Optional<String> replicaNameOpt = replicas.keySet().stream().filter(key -> replicas.get(key).get("node_name").equals(targetNode)).findAny();
        return replicaNameOpt.orElseThrow(
                () -> new RuntimeException(String.format("Couldn't find any replica for node %s. Try adding '_solr' to target node", targetNode))
        );
    }

    private void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private void waitForReplicaToBecomeActive(String collectionName, String shardName, String node, int timeoutSeconds) {
        Map<String, Map<String, String>> replicas;
        int timeoutCounter = 0;
        boolean isActive = true;
        do {
            if (!isActive) {
                logInfo(String.format("Waiting for replica to become active (%d)", timeoutCounter));
                sleep(TimeUnit.SECONDS.toMillis(POLING_INTERVAL));
            }
            replicas = getReplicaStatus(collectionName, shardName);
            isActive = ACTIVE.equals(replicas.get(node).get("state"));
            timeoutCounter += POLING_INTERVAL;
        } while (!isActive && (timeoutSeconds <= 0 || timeoutCounter < timeoutSeconds));

        if (!isActive) {
            throw new RuntimeException(String.format("Waiting for %s/%s on node %s to become active timed out!", collectionName, shardName, node));
        } else {
            sleep(TimeUnit.SECONDS.toMillis(5));  // wait a bit more, so the state will be updated everywhere
        }
    }

    public void forceLeader(String collectionName, String shardName, String targetNode, int timeoutSec) {
        logInfo(String.format("Attempting to force %s lead on node %s", shardName, targetNode));
        String replicasNodeName = extractReplicasNodeName(collectionName, shardName, targetNode);
        waitForReplicaToBecomeActive(collectionName, shardName, replicasNodeName, timeoutSec);

        try {
            CollectionAdminRequest.addReplicaProperty(collectionName, shardName, replicasNodeName, PREFERRED_LEADER_PROPERTY, Boolean.TRUE.toString())
                    .process(solrClient);

            CollectionAdminRequest.rebalanceLeaders(collectionName).process(solrClient);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to force replica lead", e);
        }
    }

    private void removeFinishedAsyncResponseStatus(String requestId) {
        ExitCode code = getAsyncRequestStatus(requestId, false);
        if (code == ExitCode.COMPLETED || code == ExitCode.FAILED) {
            removeTaskId(requestId);
        }
    }

    private ExitCode waitForTaskToComplete(String taskId, boolean verbose) {
        ExitCode taskExitCode;
        do {
            logInfo(String.format("Waiting for task %s to complete...", taskId));
            sleep(TimeUnit.SECONDS.toMillis(1));
            taskExitCode = getAsyncRequestStatus(taskId, verbose);
        } while (taskExitCode == ExitCode.IN_PROGRESS);
        if (taskExitCode.getCode() == 0) {
            logInfo("Completed successfully\n");
        } else {
            logInfo(String.format("Finished with exit code %d", taskExitCode.getCode()));
        }
        return taskExitCode;
    }

    public ExitCode splitAndMoveShard(String collectionName, String shardName, String targetNode, int timeoutSec, boolean verbose) {
        String taskId = splitShard(collectionName, shardName);
        ExitCode taskExitCode = waitForTaskToComplete(taskId, verbose);

        if (taskExitCode != ExitCode.COMPLETED) {
            logInfo("Failed to split shard, aborting");
            return taskExitCode;
        }

        shardName += "_1";
        logInfo(String.format("Replicating %s shard %s to node %s...", collectionName, shardName, targetNode));
        taskId = replicateCollection(collectionName, shardName, targetNode);
        taskExitCode = waitForTaskToComplete(taskId, verbose);
        if (taskExitCode != ExitCode.COMPLETED) {
            logInfo("Failed to replicate, aborting");
            return taskExitCode;
        }

        forceLeader(collectionName, shardName, targetNode, timeoutSec);

        return taskExitCode;
    }

    public ExitCode moveReplica(String collectionName, String shardName, String sourceNode, String targetNode, int timeoutSec, boolean moveExisting, boolean verbose) {
        String taskId;
        ExitCode taskExitCode;
        if (isReplicaExistingInTargetNode(targetNode, collectionName, shardName)) {
            if (moveExisting) {
                logInfo(String.format("Replica %s/%s already exists on target node %s. continuing ...", collectionName, shardName, targetNode));
            } else {
                logError(String.format("Replica %s/%s already exists on target node %s.", collectionName, shardName, targetNode));
                return ExitCode.FAILED;
            }
        } else {
            // Replicate if not already exists
            taskId = replicateCollection(collectionName, shardName, targetNode);
            taskExitCode = waitForTaskToComplete(taskId, verbose);
            if (taskExitCode != ExitCode.COMPLETED) {
                logInfo("Failed to move replica, aborting");
                return taskExitCode;
            }
        }
        forceLeader(collectionName, shardName, targetNode, timeoutSec);
        String replica = extractReplicasNodeName(collectionName, shardName, sourceNode);
        taskId = deleteReplica(collectionName, shardName, replica);
        taskExitCode = waitForTaskToComplete(taskId, verbose);

        return taskExitCode;
    }

    private boolean isReplicaExistingInTargetNode(String nodeName, String collectionName, String shardName) {
        SimpleOrderedMap clusterStatusMap = getClusterStatusMap();
        SimpleOrderedMap collectionStatus = ((SimpleOrderedMap) clusterStatusMap.get("collections"));
//         <NodeName, <CollectionName, <ShardName, Replica>>
        Map<String, Map<String, Map<String, List<Replica>>>> nodesMap = getNodeCollectionStatuses(collectionStatus);
        Map<String, List<Replica>> collectionShards = nodesMap.get(nodeName).get(collectionName);
        if (collectionShards != null) {
            List<Replica> replicas = collectionShards.get(shardName);
            if (replicas != null && !replicas.isEmpty()) {
                replicas.forEach(rep -> logInfo(String.format("%s/%s already found on %s active=%b, leader=%b", collectionName, shardName, nodeName, rep.getActive(), rep.getLeader())));
                return true;
            }
        }
        return false;
    }

    private String deleteReplica(String collectionName, String shardName, String replica) {
        String taskId = "delete_replica_" + collectionName + "_" + shardName + "_" + replica;
        verifyTaskDoNotExist(taskId);
        CollectionAdminRequest.DeleteReplica deleteReplica = CollectionAdminRequest.deleteReplica(collectionName, shardName, replica);
        try {
            deleteReplica.processAsync(taskId, solrClient);
        } catch (IOException | SolrServerException | SolrException e) {
            logError("Failed to delete replica", e);
        }

        return taskId;
    }

    private String deleteShard(String collectionName, String shardName) {
        String taskId = "delete_shard_" + collectionName + "_" + shardName;
        verifyTaskDoNotExist(taskId);
        CollectionAdminRequest.DeleteShard deleteShard = CollectionAdminRequest.deleteShard(collectionName, shardName);
        try {
            deleteShard.processAsync(taskId, solrClient);
        } catch (IOException | SolrServerException | SolrException e) {
            logError("Failed to delete replica", e);
        }

        return taskId;
    }

    public long migrateCollection(String srcCollection, String destCollection, String fromCursorMark, int batchSize,
                                  Map<String, Map<String, Function<Object, Object>>> conversionMaps) {
        final SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.setStart(0); // must be 0 since cursor mark is used.
        query.setRows(batchSize);
        query.addSort("id", SolrQuery.ORDER.asc);
        String cursorMark = Optional.ofNullable(fromCursorMark).orElse(CursorMarkParams.CURSOR_MARK_START);
        String lastSuccessfulBatchCursor = null;

        List<SolrInputDocument> transformedDocs = new ArrayList<>(batchSize);
        boolean done = false;
        long totalTransformed = 0;
        Map<String, Function<Object, Object>> conversionMap = conversionMaps.get(srcCollection);
        if (conversionMap == null) {
            logInfo("No fields conversion map for source collection " + srcCollection);
        } else {
            logInfo("Fields conversion map for source collection " + srcCollection + ": " +
                    conversionMap.keySet().stream().collect(Collectors.joining(",")));
        }

        while (!done) {
            query.set(CursorMarkParams.CURSOR_MARK_PARAM, cursorMark);
            QueryResponse queryResponse;
            long ts;
            try {
                ts = System.currentTimeMillis();
                queryResponse = solrClient.query(srcCollection, query);
                if (queryResponse.getResults().getNumFound() == 0) {
                    break;
                }
                List<SolrDocument> results = queryResponse.getResults();
                transformedDocs.clear();

                if (results.size() > 0) {
                    results.forEach(doc -> {
                        SolrInputDocument transformed = transformDocument(doc, conversionMap);
                        transformedDocs.add(transformed);
                    });
                    solrClient.add(destCollection, transformedDocs, COMMIT_WITHIN_MS);
                    totalTransformed += results.size();
                }
                done = results.size() != batchSize;
                ts = System.currentTimeMillis() - ts;
                long total = queryResponse.getResults().getNumFound();
                logInfo(String.format("Converted %d / %d (%d%%) over %dms. Marker=%s",
                        totalTransformed, total, ((int) (100 * totalTransformed / total)), ts, queryResponse.getNextCursorMark()));
            } catch (Exception e) {
                String lastCursorMark = Optional.ofNullable(lastSuccessfulBatchCursor).orElse("Start from beginning");
                logInfo(String.format("Data migration for collection %s failed. Resume with cursor-mark: %s", srcCollection, lastCursorMark));
                throw new RuntimeException("Failed to migrate collection", e);
            }
            cursorMark = lastSuccessfulBatchCursor = queryResponse.getNextCursorMark();
        }
        return totalTransformed;
    }

    private long getCollectionSize(String collection) throws IOException, SolrServerException {
        final SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.setRows(0);
        QueryResponse response = solrClient.query(collection, query);

        return response.getResults().getNumFound();
    }

    private SolrInputDocument transformDocument(SolrDocument doc, final Map<String, Function<Object, Object>> conversionMap) {
        SolrInputDocument transformedDocument = new SolrInputDocument();
        //transformedDocument.setField(A_FIELD.getSolrName(), doc.get("a_field"));
        // ...

        doc.getFieldNames().forEach(key -> {
            Object newVal = transformField(key, doc.getFieldValue(key), conversionMap);
            if (newVal != null) {   // Allow skip of copy fields and others
                transformedDocument.setField(key, newVal);
            }
        });
        transformedDocument.removeField("_version_"); // Otherwise will fail on version conflict

        return transformedDocument;
    }

    private static Map<String, Map<String, Function<Object, Object>>> conversionMaps_test = new HashMap<>();

    static {
        Map<String, Function<Object, Object>> conversionMap = new HashMap<>();
        conversionMap.put("id", a -> {
            logInfo("converting " + a.toString());
            return a.toString();
        });
        conversionMaps_test.put("Extraction", conversionMap);
        conversionMaps_test.put("AnalysisData", conversionMap);

        conversionMap = new HashMap<>();
        conversionMap.put("id", a -> {
            String sp[] = ((String) a).split("\\!", 2);
//            logInfo("converting "+a.toString());
            return sp[1] + "!" + sp[0];
        });
        conversionMaps_test.put("PVS_TEST", conversionMap);
    }

    static private final Pattern oldPvsIdSplit = Pattern.compile("/");
    private static Function<Object, Object> cvtFieldLongToString = (Object a) -> String.valueOf(a);
    private static Function<Object, Object> cvtFieldNullify = (Object a) -> null;
    //    private static Function<Object, Object> cvtFieldLongToString = (Object a)->{
//        if (a instanceof Long) {
//            return String.valueOf((Long)a);
//        } else if (a instanceof String) {
//            return a;
//        } else {
//            logError("Unexpected field type " + a.getClass());
//        }
//        return String.valueOf((Long)a);
//    };
    private static Function<Object, Object> cvtFieldPvsIdOldToCompound = (Object a) -> {
        String sp[] = oldPvsIdSplit.split((String) a, 2);
        return sp[1] + "!" + sp[0];
    };

    public static Map<String, Map<String, Function<Object, Object>>> conversionMaps_2017_10 = new HashMap<>();

    static {
        Map<String, Function<Object, Object>> cmIdLongToString = new HashMap<>();
        cmIdLongToString.put("id", cvtFieldLongToString);
        conversionMaps_2017_10.put("AnalysisData", cmIdLongToString);
        conversionMaps_2017_10.put("ContentMetadata", cmIdLongToString);
        conversionMaps_2017_10.put("Extraction", cmIdLongToString);
        conversionMaps_2017_10.put("GrpHelper", cmIdLongToString);

        Map<String, Function<Object, Object>> cmCatFiles = new HashMap<>();
        cmCatFiles.put("contentId", cvtFieldLongToString);
        cmCatFiles.put("sortName", cvtFieldNullify);
        conversionMaps_2017_10.put("CatFiles", cmCatFiles);

        Map<String, Function<Object, Object>> cmPvsId = new HashMap<>();
        cmPvsId.put("id", cvtFieldPvsIdOldToCompound);
        conversionMaps_2017_10.put("PVS", cmPvsId);
    }

    private Object transformField(String key, Object o, Map<String, Function<Object, Object>> conversionMap) {
        Function<Object, Object> f = conversionMap == null ? null : conversionMap.get(key);
        Object res = (f == null) ? o : f.apply(o);
        return res;
    }

    public String backup(String collection, String backupToPath) {
        logInfo("Backing up collection " + collection + " to " + backupToPath);
        final File target = new File(backupToPath);
        if (target.exists()) {
            if (!target.isDirectory()) {
                logError("There is an existing file with same name for target " + backupToPath);
                throw new RuntimeException("Target location is an existing file");
            }
        } else {
            boolean createdSuccessfully = target.mkdirs();
            if (!createdSuccessfully) {
                logError("Failed to create " + backupToPath);
                throw new RuntimeException("Failed to create " + backupToPath);
            }
        }

        String taskId = "backup_collection_" + collection;
        verifyTaskDoNotExist(taskId);

        CollectionAdminRequest.Backup backupReq = CollectionAdminRequest.backupCollection(collection, collection);
        backupReq.setLocation(backupToPath);

        try {
            backupReq.processAsync(taskId, solrClient);
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to backup collection " + collection, e);
        }

        return taskId;
    }

    public String restore(String collection, String location, Optional<String> targetCollectionName) {
        logInfo("Restoring collection " + collection + " from " + location);
        String taskId = "restore_collection_" + collection;
        verifyTaskDoNotExist(taskId);

        CollectionAdminRequest.Restore restore = CollectionAdminRequest.restoreCollection(collection, collection);
        restore.setConfigName(collection);
        restore.setLocation(location);

        try {
            restore.processAsync(taskId, solrClient);
        } catch (IOException | SolrServerException | SolrException e) {
            throw new RuntimeException("Failed to restore collection " + collection, e);
        }
        return taskId;
    }

    public void removeTaskId(String taskId) {
        try {
            CollectionAdminResponse response = CollectionAdminRequest.deleteAsyncId(taskId).process(solrClient);
            if (response.isSuccess()) {
                logInfo(String.format("Task %s removed successfully", taskId));
            } else {
                logInfo(String.format("Failed to remove task %s with error: %s", taskId, response.getResponse().get("status")));
            }
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException(String.format("Failed to remove async response with id %s", taskId));
        }
    }

    public void removeAllTaskIds() {
        try {
            CollectionAdminResponse response = CollectionAdminRequest.deleteAllAsyncIds().process(solrClient);
            logInfo(response.getResponse().getVal(1).toString());
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException("Failed to clear async task ids");
        }
    }

    private String getLiveNodeUrl() {
        SimpleOrderedMap responseMap = getClusterStatusMap();
        List<String> liveNodes = (List) responseMap.get("live_nodes");
        String aLiveNode = liveNodes.stream()
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("No solr live nodes found?!"))
                .replace("_solr", "/solr");

        aLiveNode = "http://" + aLiveNode;

        if (!aLiveNode.endsWith("/")) {
            aLiveNode += "/";
        }
        return aLiveNode;
    }

    private static void logError(String s) {
        logError(s, null);
    }

    private static void logError(String s, Exception e) {
        System.err.println("ERROR: " + s);
        Optional.ofNullable(e).ifPresent(ex -> ex.printStackTrace());
        logger.error(s, e);
    }

    private static void logInfo(String s) {
        System.out.println(s);
        logger.info(s);
    }

    /**
     * TODO - many many fixes. Mainly around documentation and usability
     *
     * @param args
     */
    public static void execute(String[] args) {
        if (args.length < 3) {
            throw new RuntimeException("Missing action argument.");
        }
        String zkHost = getArg(args, 1, null);
        SolrCloudConfigurer solrCloudConfigurer = new SolrCloudConfigurer(
                new CloudSolrClient.Builder(Lists.newArrayList(zkHost), Optional.empty()).build());

        String solrAction = getArg(args, 2, null);
        String collectionName = getArg(args, 3, null);
        String configName = getArg(args, 4, null);
        String targetNode = getArg(args, 5, null);
        String aliasName = getArg(args, 3, null);
        String configFilePath = getArg(args, 3, null);
        String zkdir = getArg(args, 3, null);
        String aliasedCollections = getArg(args, 4, null);
        int timeoutSec;
        Integer numberOfShards = null;
        String destinationFile = null;
        String creationRule = null;

        final List<String> withNumberOfShard = Arrays.asList(CREATE_COLLECTION_IF_NEEDED_COMMAND, CREATE_COLLECTION_COMMAND);
        if (withNumberOfShard.contains(solrAction)) {
            numberOfShards = getIntArg(args, 5, 0);
            creationRule = getArg(args, 6, null);
            if (creationRule != null) {
                // Replace ~ with ! to allow CMD batch to avoid complex escaping in not-equal rules.
                creationRule = creationRule.replaceAll("~", "!");
            }
            if (numberOfShards == 0) {
                logError("Number of shards: bad number " + (args.length > 5 ? args[5] : "''"));
                throw new RuntimeException("Number of shards: bad number " + (args.length > 5 ? args[5] : "''"));
            }
        }

        boolean verbose;
        boolean moveExisting;
        ExitCode taskExitCode;
        int count;
        switch (solrAction) {
            case CREATE_COLLECTION_IF_NEEDED_COMMAND:
                logInfo("Create Collection (if needed) "+collectionName+ " On Config "+configName+" with "+numberOfShards+" shards" + (creationRule==null?"":(" rule="+creationRule)));
                solrCloudConfigurer.createCollectionIfNeeded(collectionName, numberOfShards, configName, creationRule);
                break;
            case CREATE_COLLECTION_COMMAND:
                logInfo("Create Collection "+collectionName+ " On Config "+configName+" with "+numberOfShards+" shards" + (creationRule==null?"":(" rule="+creationRule)));
                solrCloudConfigurer.createCollection(collectionName, numberOfShards, configName, creationRule);
                break;
            case UPLOAD_CONFIG_COMMAND:
                logInfo("Upload configuration " + configFilePath + " for Config " + configName);
                solrCloudConfigurer.uploadConfig(zkHost, configFilePath, configName);
                break;
            case ZK_MAKE_ROOT:
                logInfo("Create zookeeper dir " + zkdir);
                solrCloudConfigurer.mkdir(zkHost, zkdir);
                break;
            case DELETE_COLLECTIONS_COMMAND:
                logInfo("Delete Collections");
                solrCloudConfigurer.deleteAllCollections();
                break;
            case RELOAD_COLLECTIONS_COMMAND:
                logInfo("Reload Collections from zookeeper");
                solrCloudConfigurer.reloadAllCollections();
                break;
            case RELOAD_COLLECTION_COMMAND:
                logInfo("Reload Collection " + collectionName);
                solrCloudConfigurer.reloadCollection(collectionName);
                break;

            case DUMP_COLLECTION_STATUS_COMMAND:
                logInfo("Printing node/collection status");
                solrCloudConfigurer.printClusterStatuses();
                break;

            case SPLIT_SHARD_COMMAND:
                logInfo(String.format("Splitting collection %s shard %s", collectionName, configName));
                solrCloudConfigurer.splitShard(collectionName, configName);
                break;

            case REPLICATE_COLLECTION_COMMAND:
                String targetNodeStr = Optional.ofNullable(targetNode).orElse("same node");
                logInfo(String.format("Replicating %s %s to %s", collectionName, configName, targetNodeStr));
                solrCloudConfigurer.replicateCollection(collectionName, configName, targetNode);
                break;

            case SPLIT_AND_MOVE_SHARD_COMMAND:
                verbose = getBooleanArg(args, 7, false);
                logInfo(String.format("Splitting collection %s, shard %s", collectionName, configName));
                timeoutSec = getIntArg(args, 6, REPLICA_BECOMING_ACTIVE_TIMEOUT);
                taskExitCode = solrCloudConfigurer.splitAndMoveShard(collectionName, configName, targetNode, timeoutSec, verbose);
                System.exit(taskExitCode.getCode());
                break;

            case ASYNC_OPERATION_STATUS_COMMAND:
                if (collectionName == null) {
                    solrCloudConfigurer.dumpOverseerStatus();
                } else {
                    taskExitCode = solrCloudConfigurer.getAsyncRequestStatus(collectionName, Boolean.valueOf(configName));
                    System.exit(taskExitCode.getCode());
                }
                break;

            case FORCE_LEADER_COMMAND:
                logInfo(String.format("Forcing %s to lead for collection %s, shard %s", targetNode, collectionName, configName));
                timeoutSec = getIntArg(args, 6, REPLICA_BECOMING_ACTIVE_TIMEOUT);
                solrCloudConfigurer.forceLeader(collectionName, configName, targetNode, timeoutSec);
                break;

            case LIST_ALIASES_COMMAND:
                logInfo("Listing collection aliases:");
                solrCloudConfigurer.listAliases();
                break;

            case LIST_NODES_COMMAND:
                logInfo("Listing nodes:");
                solrCloudConfigurer.listNodes();
                break;

            case CREATE_ALIAS_COMMAND:
                logInfo(String.format("Create alias %s for collections %s", aliasName, aliasedCollections));
                solrCloudConfigurer.createAlias(aliasName, aliasedCollections);
                break;

            case DELETE_ALIAS_COMMAND:
                logInfo(String.format("Delete alias %s", aliasName));
                solrCloudConfigurer.deletedAlias(aliasName);
                break;

            case DELETE_DOWN_REPLICAS_COMMAND:
                logInfo("Deleting 'down state' replicas from all collections:");
                count = solrCloudConfigurer.deleteDeadReplicas(false);
                logInfo(count == 0 ? "None found." : String.format("%d replicas set for async deletion.", count));
                break;

            case DELETE_LEGACY_REPLICAS_COMMAND:
                logInfo("Deleting 'Legacy down state' replicas from all collections:");
                count = solrCloudConfigurer.deleteDeadReplicas(true);
                logInfo(count == 0 ? "None found." : String.format("%d replicas set for async deletion.", count));
                break;

            case DELETE_MIGRATED_REPLICAS_COMMAND:
                logInfo("Deleting Fornax migrated replicas:");
                count = solrCloudConfigurer.deleteMigratedReplicas(false);
                logInfo(count == 0 ? "None found." : String.format("%d migrated replicas set for async deletion.", count));
                break;

            case DELETE_MIGRATED_COLLECTIONS_COMMAND:
                logInfo("Deleting Fornax migrated collections:");
                count = solrCloudConfigurer.deleteMigratedReplicas(true);
                logInfo(count == 0 ? "None found." : String.format("%d migrated collections set for async deletion.", count));
                break;

            case MOVE_REPLICA_COMMAND:
                String shardName = getArg(args, 4, null);
                String sourceNode = getArg(args, 5, null);
                targetNode = getArg(args, 6, null);
                timeoutSec = getIntArg(args, 7, REPLICA_BECOMING_ACTIVE_TIMEOUT);
                moveExisting = getArg(args, 8, "").equals("ok-if-exists");
                verbose = getBooleanArg(args, moveExisting ? 9 : 8, false);
                logInfo(String.format("Moving %s %s from %s to %s (continue if exists=%b)", collectionName, shardName, sourceNode, targetNode, moveExisting));
                taskExitCode = solrCloudConfigurer.moveReplica(collectionName, shardName, sourceNode, targetNode, timeoutSec, moveExisting, verbose);
                System.exit(taskExitCode.getCode());
                break;

            case ALIGN_SHARDS_COMMAND:
                String collectionsToAlign = getArg(args, 3, null);
                if (collectionsToAlign == null || collectionsToAlign.isEmpty()) {
                    logError("Missing parameter: collections-to-align");
                    throw new RuntimeException("Missing parameter: collections-to-align");
                }
                List<String> collectionsList = Arrays.asList(collectionsToAlign.split(",", 10));
                if (collectionsList.size() < 2) {
                    logError("Bad parameter: collections-to-align: " + collectionsToAlign);
                    throw new RuntimeException("Bad parameter: collections-to-align: " + collectionsToAlign);
                }
                destinationFile = getArg(args, 4, "da_align_shards.bat");
                boolean append = (args.length > 5) && args[5].equals("append");
                logInfo("Creating shards alignment script " + destinationFile + " for " + collectionsToAlign);
                try (FileWriter destination = new FileWriter(destinationFile, append)) {
                    solrCloudConfigurer.generateAlignShardsScript(collectionsList, destination, !append);
                } catch (IOException e) {
                    logError("Couldn't write to file: " + destinationFile, e);
                }
                break;

            case ALIGN_DA_SHARDS_COMMAND:
                destinationFile = getArg(args, 3, "da_align_shards.bat");
                append = (args.length > 4) && args[4].equals("append");
                try (FileWriter destination = new FileWriter(destinationFile, append)) {
                    solrCloudConfigurer.generateAlignShardsScript(groupingCollecions, destination, true);
                    solrCloudConfigurer.generateAlignShardsScript(fileCollecions, destination, false);
                } catch (IOException e) {
                    logError("Couldn't write to file", e);
                }
                break;

            case MIGRATE_COLLECTION_COMMAND:
            case MIGRATE_COLLECTION_2017_10_COMMAND:
                String destCollection = Optional.ofNullable(getArg(args, 4, null)).orElseThrow(() -> new RuntimeException("Missing parameter destination-collection"));
                String fromCursorMark = getArg(args, 5, null);
                if (fromCursorMark != null) {
                    fromCursorMark = fromCursorMark.startsWith(CURSOR_MARK_COMMAND_PROPERTY) ?
                            fromCursorMark.substring(CURSOR_MARK_COMMAND_PROPERTY.length(), fromCursorMark.length()) : fromCursorMark;
                }
                int migrationBatchSize = getIntArg(args, 6, COLLECTION_TRANSFORM_DEFAULT_BATCH_SIZE);
                long result = solrCloudConfigurer.migrateCollection(collectionName, destCollection, fromCursorMark, migrationBatchSize,
                        solrAction.equals(MIGRATE_COLLECTION_2017_10_COMMAND) ? conversionMaps_2017_10 : conversionMaps_test);
                try {
                    solrCloudConfigurer.sleep(COMMIT_WITHIN_MS); // wait for data to be committed.
                    long srcColSize = solrCloudConfigurer.getCollectionSize(collectionName);
                    long destColSize = solrCloudConfigurer.getCollectionSize(destCollection);
                    if (srcColSize != destColSize) {
                        logError(String.format("Source and destination collection sizes do not match! (%d/%d)", srcColSize, destColSize));
                    } else {
                        logInfo(String.format("transformed %d documents from %s to %s", result, collectionName, destCollection));
                    }
                } catch (IOException | SolrServerException | SolrException e) {
                    logError("Couldn't obtain collections size", e);
                }

                break;

            case BACKUP_COMMAND:
                String target = getBackupTargetPath(args, 4);
                solrCloudConfigurer.backup(collectionName, target);
                break;

            case BACKUP_ALL_COMMAND:
                logInfo("Backing up all collections");
                target = getBackupTargetPath(args, 3);
                List<String> collections = solrCloudConfigurer.listCollections();
                collections.parallelStream()
                        .forEach(collection -> {
                            String taskId = solrCloudConfigurer.backup(collection, target);
                            solrCloudConfigurer.waitForTaskToComplete(taskId, true);
                        });
                break;

            case RESTORE_COMMAND:
                String backupPath = getArg(args, 4, null);
                Optional<String> optCollectionName = Optional.ofNullable(getArg(args, 5, null));
                String taskId = solrCloudConfigurer.restore(collectionName, backupPath, optCollectionName);
                solrCloudConfigurer.waitForTaskToComplete(taskId, true);
                break;

            case RESTORE_ALL_COMMAND:
                backupPath = getArg(args, 3, null);
                String collectionSuffix = Optional.ofNullable(getArg(args, 4, null)).orElse("");

                File[] files = new File(backupPath).listFiles();
                Arrays.asList(files).parallelStream().forEach(file -> {
                    String fileName = file.getName();
                    String restoreTaskId = solrCloudConfigurer.restore(fileName, backupPath, Optional.of(fileName + collectionSuffix));
                    solrCloudConfigurer.waitForTaskToComplete(restoreTaskId, true);
                });
                break;

            case REMOVE_TASK_ID_COMMAND:
                solrCloudConfigurer.removeTaskId(collectionName);
                break;

            case REMOVE_ALL_TASK_IDS:
                solrCloudConfigurer.removeAllTaskIds();
                break;

            case PLUGIN_UPGRADE:
                String jarPath = Optional.ofNullable(getArg(args, 4, null))
                        .orElseThrow(() -> new IllegalArgumentException("Missing path to jar"));

                DASolrClient daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                String blobName = getArg(args, 5, DASolrClient.DEFAULT_PLUGIN_BLOB_NAME);
                daSolrClient.upgradePlugin(collectionName, jarPath, blobName);
                break;

            case PLUGIN_UPLOAD:
                jarPath = Optional.ofNullable(getArg(args, 4, null))
                        .orElseThrow(() -> new IllegalArgumentException("Missing path to jar"));

                File jar = new File(jarPath);
                blobName = getArg(args, 5, DASolrClient.DEFAULT_PLUGIN_BLOB_NAME);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                    daSolrClient.uploadJar(jar, blobName, httpClient, solrCloudConfigurer.getLiveNodeUrl());
                } catch (IOException e) {
                    logError("Failed to upload plugin", e);
                }
                break;

            case PLUGIN_ADD_RUNTIME_LIB:
                int version = getIntArg(args, 4, -1);
                blobName = getArg(args, 5, DASolrClient.DEFAULT_PLUGIN_BLOB_NAME);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                    daSolrClient.addRuntimeLib(collectionName, blobName, version, httpClient, solrCloudConfigurer.getLiveNodeUrl());
                } catch (IOException e) {
                    logError("Failed add lib to runtime", e);
                }
                break;

            case PLUGIN_MODIFY_RUNTIME_LIB:
                version = getIntArg(args, 4, -1);
                blobName = getArg(args, 5, DASolrClient.DEFAULT_PLUGIN_BLOB_NAME);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                    daSolrClient.updateRuntimeLib(collectionName, blobName, version, httpClient, solrCloudConfigurer.getLiveNodeUrl());
                } catch (IOException e) {
                    logError("Failed add lib to runtime", e);
                }
                break;

            case PLUGIN_DELETE_RUNTIME_LIB:
                blobName = getArg(args, 4, DASolrClient.DEFAULT_PLUGIN_BLOB_NAME);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                daSolrClient.deletePluginRuntime(collectionName, blobName);
                break;

            case PLUGIN_ADD_COMPONENT:
                DASolrClient.ComponentType componentType = DASolrClient.ComponentType.valueOf(getArg(args, 4, null));
                String pluginName = getArg(args, 5, null);
                String pluginCls = getArg(args, 6, null);
                version = getIntArg(args, 7, 2);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                daSolrClient.addComponent(collectionName, componentType, pluginName, version, pluginCls);
                break;

            case PLUGIN_MODIFY_COMPONENT:
                componentType = DASolrClient.ComponentType.valueOf(getArg(args, 4, null));
                pluginName = getArg(args, 5, null);
                pluginCls = getArg(args, 6, null);
                version = getIntArg(args, 7, 2);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                daSolrClient.modifyComponent(collectionName, componentType, pluginName, version, pluginCls);
                break;

            case PLUGIN_DELETE_COMPONENT:
                componentType = DASolrClient.ComponentType.valueOf(getArg(args, 4, null));
                pluginName = getArg(args, 5, null);
                daSolrClient = new DASolrClient(solrCloudConfigurer.getLiveNodeUrl());
                daSolrClient.deleteComponent(collectionName, componentType, pluginName);
                break;

            default:
                throw new RuntimeException("Unsupported solr action " + solrAction);
        }
    }

    private static String getBackupTargetPath(String args[], int pos) {
        Optional<String> targetPath = Optional.ofNullable(getArg(args, pos, null));
        SimpleDateFormat sdf = new SimpleDateFormat(BACKUP_TIMESTAMP_FORMAT);
        String timestamp = sdf.format(Calendar.getInstance().getTime());
        return targetPath.orElseThrow(() -> new RuntimeException("Path must be supplied")) + File.separator + timestamp;
    }

    public static int getIntArg(String[] args, int ordinal, int defaultValue) {
        int res = defaultValue;
        try {
            res = args.length > ordinal ? Integer.valueOf(args[ordinal]) : defaultValue;
        } catch (NumberFormatException e) {}
        return res;
    }

    public static boolean getBooleanArg(String[] args, int ordinal, boolean defaultValue) {
        boolean res = defaultValue;
        try {
            res = args.length > ordinal ? Boolean.valueOf(args[ordinal]) : defaultValue;
        } catch (NumberFormatException e) {}
        return res;
    }

    public static String getArg(String[] args, int ordinal, String defaultValue) {
        return args.length > ordinal ? args[ordinal] : defaultValue;
    }

    public boolean isCloud() {
        return solrClient instanceof CloudSolrClient;
    }

    public void close() throws IOException {
        solrClient.close();
        logger.info("Done: Closed configurer inner cloud client");
    }

    public SolrClient getSolrClient() {
        return solrClient;
    }

    public UpdateResponse optimizeNoWaitNoFlush(String pvsCollection) {
        try {
            return solrClient.optimize(pvsCollection, false, false);
        } catch (Throwable t) {
            throw new RuntimeException("Failed to run optimize", t);
        }
    }

    public static void throwIfFail(Supplier<CollectionAdminResponse> adminCommand, String failMessage) {
        try {
            CollectionAdminResponse response = adminCommand.get();
           /* if (!response.isSuccess()) {
                throw new RuntimeException(response.getErrorMessages().toString());
            }*/
        } catch (Throwable t) {
            throw new RuntimeException(failMessage, t);
        }

    }

    private List<String> getClusterLiveNodes() {
        CollectionAdminRequest.ClusterStatus clusterStatusReq = new CollectionAdminRequest.ClusterStatus();
//        clusterStatus.setCollectionName(collection);
        try {
            CollectionAdminResponse response = clusterStatusReq.process(solrClient);
            NamedList<Object> responseList = response.getResponse();
            NamedList<Object> clusterStatus = (NamedList<Object>) responseList.get("cluster");
            if (clusterStatus == null) {
                logError("Failed to get Solr cluster status");
                return null;
            }
            List<String> liveNodes = (List) clusterStatus.get("live_nodes");
            if (liveNodes == null) {
                logError("Failed to get Solr cluster status");
                return null;
            }
            return liveNodes;
        } catch (SolrServerException | IOException e) {
            logError("Failed to get Solr cluster status", e);
            return null;
        }
    }

}