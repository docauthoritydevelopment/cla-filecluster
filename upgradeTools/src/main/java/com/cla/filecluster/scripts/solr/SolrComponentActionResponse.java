package com.cla.filecluster.scripts.solr;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by oren on 5/3/2018.
 */
public class SolrComponentActionResponse extends SolrBaseResponse<SolrComponentActionResponse.ErrorMessages> {

    public static class ErrorMessages {

        @JsonProperty("create-requesthandler")
        public ComponentConfig createConfig;

        @JsonProperty("update-requesthandler")
        public ComponentConfig updateConfig;

        @JsonProperty("delete-requesthandler")
        public ComponentConfig deleteConfig;

        public String[] errorMessages;
    }

    public static class ComponentConfig {
        @JsonProperty("class")
        public String cls;
        public String name;
        public String runtimeLib;
        public String version;
    }
}
