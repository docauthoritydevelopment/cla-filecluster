package com.cla.filecluster.scripts.solr;

/**
 * Created by oren on 10/15/2017.
 */
public class ShardAlignment {

    private String collectionName;

    private String shardName;

    private String sourceNode;

    private String targetNode;

    public ShardAlignment(String collectionName, String shardName, String sourceNode, String targetNode) {
        this.collectionName = collectionName;
        this.shardName = shardName;
        this.sourceNode = sourceNode;
        this.targetNode = targetNode;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public String getShardName() {
        return shardName;
    }

    public String getSourceNode() {
        return sourceNode;
    }

    public String getTargetNode() {
        return targetNode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShardAlignment that = (ShardAlignment) o;

        if (collectionName != null ? !collectionName.equals(that.collectionName) : that.collectionName != null)
            return false;
        if (shardName != null ? !shardName.equals(that.shardName) : that.shardName != null) return false;
        if (sourceNode != null ? !sourceNode.equals(that.sourceNode) : that.sourceNode != null) return false;
        return targetNode != null ? targetNode.equals(that.targetNode) : that.targetNode == null;
    }

    @Override
    public int hashCode() {
        int result = collectionName != null ? collectionName.hashCode() : 0;
        result = 31 * result + (shardName != null ? shardName.hashCode() : 0);
        result = 31 * result + (sourceNode != null ? sourceNode.hashCode() : 0);
        result = 31 * result + (targetNode != null ? targetNode.hashCode() : 0);
        return result;
    }

    public String getAlignCommand(String zkHost) {
        return String.format("CALL solr_cloud_config.bat move-replica %s %s %s %s %s", zkHost, collectionName, shardName, sourceNode, targetNode);
    }

    public String getAlignDescription() {
        return String.format("Moving collection %s  shard %s from %s to %s", collectionName, shardName, sourceNode, targetNode);
    }

    @Override
    public String toString() {
        return "ShardAlignment{" +
                "collectionName='" + collectionName + '\'' +
                ", shardName='" + shardName + '\'' +
                ", sourceNode='" + sourceNode + '\'' +
                ", targetNode='" + targetNode + '\'' +
                '}';
    }
}
