package com.cla.filecluster.scripts.solr;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by oren on 5/3/2018.
 */
public class SolrRuntimeLibActionResponse extends SolrBaseResponse<SolrRuntimeLibActionResponse.ErrorMessages> {

    public static class ErrorMessages {
        public String [] errorMessages;

        @JsonProperty("update-runtimelib")
        public Object updateRuntimelib;
    }
}
