package com.cla.filecluster.scripts.solr;

import java.util.List;

/**
 * Created by oren on 4/29/2018.
 */
public class SolrBlobsResponseWrapper {

    public int numFound;
    public int start;
    public List<BlobMetadata> docs;

    @Override
    public String toString() {
        return "SolrBlobsResponseWrapper{" +
                "numFound=" + numFound +
                ", start=" + start +
                ", docs=" + docs +
                '}';
    }

    public static class BlobMetadata {
        public String blobName;
        public String id;
        public String md5;
        public long size;
        public String timestamp;
        public int version;

        public int getVersion() {
            return version;
        }

        @Override
        public String toString() {
            return "BlobMetadata{" +
                    "blobName='" + blobName + '\'' +
                    ", id='" + id + '\'' +
                    ", md5='" + md5 + '\'' +
                    ", size=" + size +
                    ", timestamp=" + timestamp +
                    ", version=" + version +
                    '}';
        }
    }

}
