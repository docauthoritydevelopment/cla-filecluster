package com.cla.mcf.output.filecluster;

import com.cla.mcf.output.filecluster.entities.ClaFilePropertiesDto;
import com.cla.mcf.output.filecluster.entities.RawDocumentMessage;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.config.SocketConfig;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.manifoldcf.agents.interfaces.IOutputAddActivity;
import org.apache.manifoldcf.agents.interfaces.IOutputRemoveActivity;
import org.apache.manifoldcf.agents.interfaces.RepositoryDocument;
import org.apache.manifoldcf.agents.interfaces.ServiceInterruption;
import org.apache.manifoldcf.agents.system.Logging;
import org.apache.manifoldcf.core.interfaces.ManifoldCFException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.StringWriter;
import java.util.*;

public class ClaHttpPoster {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaHttpPoster.class);

    private final String httpDocAuthorityServerUrl;

    private final String type;

    private final int ingesterSignatureMaxSize;

    private CloseableHttpClient localClient;

    protected PoolingHttpClientConnectionManager connectionManager = null;

    // Action URI pieces
    private static final String RAW_DOCUMENT_URL = "/api/mcf/ingest/raw";

    private static final String DELETE_DOCUMENT_URL = "/api/mcf/ingest/delete";

    // Document max length
    private final Long maxDocumentLength;

    /**
     * How long to wait before retrying a failed ingestion
     */
    private static final long interruptionRetryTime = 60000L;

    final ObjectMapper objectMapper = new ObjectMapper();

    public ClaHttpPoster(Properties properties) throws SecurityException {

        String serverAddress = properties.getProperty(FileClusterConfig.SERVER_ADDRESS);
        String serverPort = properties.getProperty(FileClusterConfig.SERVER_PORT);
        type = properties.getProperty(FileClusterConfig.CONNECTOR_TYPE);
        String user = properties.getProperty(FileClusterConfig.USER);
        String password = properties.getProperty(FileClusterConfig.PASSWORD);

        int socketTimeout = Integer.valueOf(properties.getProperty(FileClusterConfig.SOCKET_TIMEOUT));
        int connectionTimeout = Integer.valueOf(properties.getProperty(FileClusterConfig.CONNECTION_TIMEOUT));
        this.maxDocumentLength = Long.valueOf(properties.getProperty(FileClusterConfig.MAX_DOCUMENT_LENGTH));
        this.ingesterSignatureMaxSize = Integer.valueOf(properties.getProperty(FileClusterConfig.INGEST_SIGNATURE_MAX_SIZE));

        // First, we need an HttpClient where basic auth is properly set up.
        connectionManager = new PoolingHttpClientConnectionManager(RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .build());
        connectionManager.setDefaultMaxPerRoute(1);
        connectionManager.setValidateAfterInactivity(2000);
        connectionManager.setDefaultSocketConfig(SocketConfig.custom()
                .setTcpNoDelay(true)
                .setSoTimeout(socketTimeout)
                .build());
        RequestConfig.Builder requestBuilder = RequestConfig.custom()
                .setCircularRedirectsAllowed(true)
                .setSocketTimeout(socketTimeout)
                .setExpectContinueEnabled(true)
                .setConnectTimeout(connectionTimeout)
                .setConnectionRequestTimeout(socketTimeout);

        HttpClientBuilder clientBuilder = HttpClients.custom()
                .setConnectionManager(connectionManager)
                .disableAutomaticRetries()
                .setDefaultRequestConfig(requestBuilder.build())
                .setRedirectStrategy(new DefaultRedirectStrategy())
                .setRequestExecutor(new HttpRequestExecutor(socketTimeout));

//        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
//        Credentials credentials = new UsernamePasswordCredentials(user, password);
//        String realm = "http" + "://" + serverAddress + ":" + serverPort;
//        credentialsProvider.setCredentials(AuthScope.ANY, credentials);
//        credentialsProvider.setCredentials(new AuthScope(AuthScope.ANY_HOST, AuthScope.ANY_PORT, realm), credentials);

//        clientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        localClient = clientBuilder.build();
        httpDocAuthorityServerUrl = "http" + "://" + serverAddress + ":" + serverPort;
        try {
            authenticate(localClient,user,password,httpDocAuthorityServerUrl);
        } catch (IOException e) {
            logger.error("Failed to authenticate with DocAuthority Server",e);
            throw new SecurityException("Failed to authenticate with DocAuthority Server",e);
        }

    }

    private String authenticate(final HttpClient httpclient, String user, String password, String serverUrl) throws IOException {
        String authCookie;
        try {
            logger.info("Authenticating with user {}",user);
            final HttpPost authpost = new HttpPost(serverUrl+"/login");
            final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("username", user));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            authpost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            final HttpResponse response = httpclient.execute(authpost);
            if (response.getStatusLine().getStatusCode() != 200)
                throw new SecurityException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
            authCookie = response.getHeaders("Set-Cookie")[0].getValue();
            logger.info("Authenticated Successfully");
        } catch (IOException e) {
            logger.error("Failed to authenticate with user {} {} to url {}/login",user,password,serverUrl);
            authCookie = null;
            throw e;
        } catch (SecurityException e) {
            logger.error("Failed to authenticate with user {} to url /login",user);
            authCookie = null;
            throw e;
        }
        return authCookie;
    }

    public void shutdown() {
        if (localClient != null) {
            localClient = null;
        }
        if (connectionManager != null)
            connectionManager.shutdown();
        connectionManager = null;
    }

    /**
     * Handle an IOException.
     * I'm not actually sure where these exceptions come from in SolrJ, but we handle them
     * as real I/O errors, meaning they should be retried.
     */
    protected static void handleIOException(IOException e, String context)
            throws ManifoldCFException, ServiceInterruption {
        if ((e instanceof InterruptedIOException) && (!(e instanceof java.net.SocketTimeoutException)))
            throw new ManifoldCFException(e.getMessage(), ManifoldCFException.INTERRUPTED);

        long currentTime = System.currentTimeMillis();

        if (e instanceof java.net.ConnectException) {
            // Server isn't up at all.  Try for a brief time then give up.
            String message = "Server could not be contacted during " + context + ": " + e.getMessage();
            Logging.ingest.warn(message, e);
            throw new ServiceInterruption(message,
                    e,
                    currentTime + interruptionRetryTime,
                    -1L,
                    3,
                    true);
        }

        if (e instanceof java.net.SocketTimeoutException) {
            String message2 = "Socket timeout exception during " + context + ": " + e.getMessage();
            Logging.ingest.warn(message2, e);
            throw new ServiceInterruption(message2,
                    e,
                    currentTime + interruptionRetryTime,
                    currentTime + 20L * 60000L,
                    -1,
                    false);
        }

        if (e.getClass().getName().equals("java.net.SocketException")) {
            // In the past we would have treated this as a straight document rejection, and
            // treated it in the same manner as a 400.  The reasoning is that the server can
            // perfectly legally send out a 400 and drop the connection immediately thereafter,
            // this a race condition.
            // However, Solr 4.0 (or the Jetty version that the example runs on) seems
            // to have a bug where it drops the connection when two simultaneous documents come in
            // at the same time.  This is the final version of Solr 4.0 so we need to deal with
            // this.
            if (e.getMessage().toLowerCase(Locale.ROOT).indexOf("broken pipe") != -1 ||
                    e.getMessage().toLowerCase(Locale.ROOT).indexOf("connection reset") != -1 ||
                    e.getMessage().toLowerCase(Locale.ROOT).indexOf("target server failed to respond") != -1) {
                // Treat it as a service interruption, but with a limited number of retries.
                // In that way we won't burden the user with a huge retry interval; it should
                // give up fairly quickly, and yet NOT give up if the error was merely transient
                String message = "Server dropped connection during " + context + ": " + e.getMessage();
                Logging.ingest.warn(message, e);
                throw new ServiceInterruption(message,
                        e,
                        currentTime + interruptionRetryTime,
                        -1L,
                        3,
                        false);
            }

            // Other socket exceptions are service interruptions - but if we keep getting them, it means
            // that a socket timeout is probably set too low to accept this particular document.  So
            // we retry for a while, then skip the document.
            String message2 = "Socket exception during " + context + ": " + e.getMessage();
            Logging.ingest.warn(message2, e);
            throw new ServiceInterruption(message2,
                    e,
                    currentTime + interruptionRetryTime,
                    currentTime + 20L * 60000L,
                    -1,
                    false);
        }

        // Otherwise, no idea what the trouble is, so presume that retries might fix it.
        String message3 = "IO exception during " + context + ": " + e.getMessage();
        Logging.ingest.warn(message3, e);
        throw new ServiceInterruption(message3,
                e,
                currentTime + interruptionRetryTime,
                currentTime + 2L * 60L * 60000L,
                -1,
                true);
    }

    public boolean indexPost(String documentURI, RepositoryDocument document, String authorityNameString, IOutputAddActivity activities) throws ManifoldCFException, ServiceInterruption {
        if (Logging.ingest.isDebugEnabled())
            Logging.ingest.debug("indexPost(): '" + documentURI + "'");

        // If the document is too long, reject it.
        if (maxDocumentLength != null && document.getBinaryLength() > maxDocumentLength.longValue()) {
            activities.recordActivity(null, FileClusterOutputConnector.INGEST_ACTIVITY, null, documentURI, activities.EXCLUDED_LENGTH, "Solr connector rejected document due to its big size: ('" + document.getBinaryLength() + "')");
            return false;
        }


        try {
            IngestThread t = new IngestThread(documentURI,document, authorityNameString, activities);
            try {
                t.start();
                t.finishUp();

                if (t.getActivityCode() != null)
                    activities.recordActivity(t.getActivityStart(), FileClusterOutputConnector.INGEST_ACTIVITY, t.getActivityBytes(), documentURI, t.getActivityCode(), t.getActivityDetails());

                return t.getRval();
            } catch (InterruptedException e) {
                t.interrupt();
                throw new ManifoldCFException("Interrupted: " + e.getMessage(), ManifoldCFException.INTERRUPTED);
            } catch (RuntimeException e) {
                if (t.getActivityCode() != null)
                    activities.recordActivity(t.getActivityStart(), FileClusterOutputConnector.INGEST_ACTIVITY, t.getActivityBytes(), documentURI, t.getActivityCode(), t.getActivityDetails());
                throw e;
            } catch (IOException e) {
                if (t.getActivityCode() != null)
                    activities.recordActivity(t.getActivityStart(), FileClusterOutputConnector.INGEST_ACTIVITY, t.getActivityBytes(), documentURI, t.getActivityCode(), t.getActivityDetails());
                throw e;
            }
        } catch (RuntimeException e) {
            handleRuntimeException(e, "indexing " + documentURI);
            return false;
        } catch (IOException ioe) {
            handleIOException(ioe, "indexing " + documentURI);
            return false;
        }
    }

    /**
     * Convert an unqualified ACL to qualified form.
     *
     * @param acl                 is the initial, unqualified ACL.
     * @param authorityNameString is the name of the governing authority for this document's acls, or null if none.
     * @param activities          is the activities object, so we can report what's happening.
     * @return the modified ACL.
     */
    protected static List<String> convertACL(String[] acl, String authorityNameString, IOutputAddActivity activities)
            throws ManifoldCFException {
        if (acl != null) {
            List<String> rval = new ArrayList(acl.length);
            int i = 0;
            while (i < acl.length) {
                rval.add(activities.qualifyAccessToken(authorityNameString, acl[i]));
                i++;
            }
            return rval;
        }
        return Collections.EMPTY_LIST;
    }

    /**
     * Handle a RuntimeException.
     * Unfortunately, SolrCloud 4.6.x throws RuntimeExceptions whenever ZooKeeper is not happy.
     * We have to catch these too.  I've logged a ticket: SOLR-5678.
     */
    protected static void handleRuntimeException(RuntimeException e, String context)
            throws ManifoldCFException, ServiceInterruption {
        Throwable childException = e.getCause();
        if (childException != null && childException instanceof java.util.concurrent.TimeoutException) {
            Logging.ingest.warn("runtime exception during " + context + ": " + childException.getMessage(), childException);
            long currentTime = System.currentTimeMillis();
            throw new ServiceInterruption(childException.getMessage(), childException,
                    currentTime + interruptionRetryTime,
                    currentTime + 2L * 60L * 60000L,
                    -1,
                    true);
        }
        throw e;
    }


    private void postRawDocumentMessage(RawDocumentMessage rawDocumentMessage) throws IOException {
        String uri = httpDocAuthorityServerUrl + RAW_DOCUMENT_URL + "?type=" + type;
        logger.info("Post document to: " + uri);
        final HttpPut putRequest = new HttpPut(uri);
        final String jsonString = convertToJson(rawDocumentMessage);
        final StringEntity se = new StringEntity(jsonString);
        se.setContentType("application/json");
        putRequest.setEntity(se);
        final HttpResponse response = localClient.execute(putRequest);

        if (response.getStatusLine().getStatusCode() != 200)
            throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode()
                    + " , while sending doc " + rawDocumentMessage.getUrl());
        logger.info("Sent doc " + rawDocumentMessage.getUrl() + " successfully");
    }

    private String convertToJson(final RawDocumentMessage rawDocumentMessage) throws IOException {
        final StringWriter jsonString = new StringWriter();
        objectMapper.writeValue(jsonString, rawDocumentMessage);
        return jsonString.toString();
    }

    public void deletePostDocument(String documentURI, IOutputRemoveActivity activities) throws ManifoldCFException, ServiceInterruption {
        try {
            deletePost(documentURI,activities);
        } catch (RuntimeException e) {
            handleRuntimeException(e, "removing " + documentURI);
        } catch (IOException ioe) {
            handleIOException(ioe, "removing " + documentURI);
        }
    }

    private void deletePost(String documentURI, IOutputRemoveActivity activities) throws IOException {
        String uri = httpDocAuthorityServerUrl + DELETE_DOCUMENT_URL + "?url="+documentURI;
        logger.info("Delete document. URI:" + uri);
        final HttpDelete deleteRequest = new HttpDelete(uri);
        final HttpResponse response = localClient.execute(deleteRequest);

        if (response.getStatusLine().getStatusCode() != 200)
            throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode()
                    + " , while deleting document " + documentURI);
        logger.info("Deleted doc " + documentURI + " successfully");
    }

    public void checkPost() throws ManifoldCFException, ServiceInterruption {
        if (Logging.ingest.isDebugEnabled())
            Logging.ingest.debug("checkPost()");

        // Open a socket to ingest, and to the response stream to get the post result
        try {
            StatusThread t = new StatusThread();
            try {
                t.start();
                t.finishUp();
                return;
            } catch (InterruptedException e) {
                t.interrupt();
                throw new ManifoldCFException("Interrupted: " + e.getMessage(), ManifoldCFException.INTERRUPTED);
            }
        } catch (RuntimeException e) {
            handleRuntimeException(e, "check");
            return;
        } catch (IOException ioe) {
            handleIOException(ioe, "check");
            return;
        }
    }


    /**
     * Killable thread that does ingestions.
     * Java 1.5 stopped permitting thread interruptions to abort socket waits.  As a result, it is impossible to get threads to shutdown cleanly that are doing
     * such waits.  So, the places where this happens are segregated in their own threads so that they can be just abandoned.
     * <p>
     * This thread does a single document ingestion.
     */
    protected class IngestThread extends java.lang.Thread {
        protected final String documentURI;
        protected final RepositoryDocument document;

        protected Long activityStart = null;
        protected Long activityBytes = null;
        protected String activityCode = null;
        protected String activityDetails = null;
        protected Throwable exception = null;
        protected boolean readFromDocumentStreamYet = false;
        protected boolean rval = false;
        private String authorityNameString;
        private IOutputAddActivity activities;

        public IngestThread(String documentURI, RepositoryDocument document, String authorityNameString, IOutputAddActivity activities) {
            super();
            this.authorityNameString = authorityNameString;
            this.activities = activities;
            setDaemon(true);
            this.documentURI = documentURI;
            this.document = document;
        }

        public void run() {
            long length = document.getBinaryLength();
            InputStream is = document.getBinaryStream();
            String contentType = document.getMimeType();
            String contentName = document.getFileName();

            try {
                // Do the operation!
                long fullStartTime = System.currentTimeMillis();

                // Open a socket to ingest, and to the response stream to get the post result
                try {

                    byte[] content = ClaFileUtils.extractContent(document);
                    Map<String, String[]> metadata = ClaFileUtils.extractMetadata(document);

                    // Convert the incoming acls that we know about to qualified forms, and reject the document if
                    // we don't know how to deal with its acls
                    Map<String, List<String>> aclsMap = new HashMap<String, List<String>>();
                    Map<String, List<String>> denyAclsMap = new HashMap<String, List<String>>();

                    ClaFileUtils.populateAclMaps(documentURI, document, authorityNameString, activities, aclsMap, denyAclsMap);

                    RawDocumentMessage rawDocumentMessage = new RawDocumentMessage(documentURI, content, metadata);
                    ClaFilePropertiesDto fileProperties = ClaFileUtils.createFileProperties(document, content, aclsMap, denyAclsMap, ingesterSignatureMaxSize);
                    rawDocumentMessage.setClaFilePropertiesDto(fileProperties);
                    // Fire off the request.
                    readFromDocumentStreamYet = true;
                    postRawDocumentMessage(rawDocumentMessage);

                    // Successful completion
                    activityStart = new Long(fullStartTime);
                    activityBytes = new Long(length);
                    activityCode = "OK";
                    activityDetails = null;

                    rval = true;
                    return;
                } catch (IOException ioe) {
                    if ((ioe instanceof InterruptedIOException) && (!(ioe instanceof java.net.SocketTimeoutException)))
                        return;

                    activityStart = new Long(fullStartTime);
                    activityCode = ioe.getClass().getSimpleName().toUpperCase(Locale.ROOT);
                    activityDetails = ioe.getMessage();

                    // Log the error
                    Logging.ingest.warn("Error indexing into Solr: " + ioe.getMessage(), ioe);

                    throw ioe;
                }
            } catch (Throwable e) {
                this.exception = e;
            }
        }


        public void finishUp()
                throws InterruptedException, IOException {
            join();

            Throwable thr = exception;
            if (thr != null) {
                if (thr instanceof IOException)
                    throw (IOException) thr;
                if (thr instanceof RuntimeException)
                    throw (RuntimeException) thr;
                if (thr instanceof Error)
                    throw (Error) thr;
                else
                    throw new RuntimeException("Unexpected exception type: " + thr.getClass().getName() + ": " + thr.getMessage(), thr);
            }
        }

        public Long getActivityStart() {
            return activityStart;
        }

        public Long getActivityBytes() {
            return activityBytes;
        }

        public String getActivityCode() {
            return activityCode;
        }

        public String getActivityDetails() {
            return activityDetails;
        }

        public boolean getReadFromDocumentStreamYet() {
            return readFromDocumentStreamYet;
        }

        public boolean getRval() {
            return rval;
        }
    }


    /**
     * Killable thread that does a status check.
     * Java 1.5 stopped permitting thread interruptions to abort socket waits.  As a result, it is impossible to get threads to shutdown cleanly that are doing
     * such waits.  So, the places where this happens are segregated in their own threads so that they can be just abandoned.
     * <p>
     * This thread does a status check.
     */
    protected class StatusThread extends java.lang.Thread {
        protected Throwable exception = null;

        public StatusThread() {
            super();
            setDaemon(true);
        }

        public void run() {
            try {
                // Do the operation!
                //TODO
            } catch (Throwable e) {
                this.exception = e;
            }
        }

        public void finishUp()
                throws InterruptedException, IOException {
            join();

            Throwable thr = exception;
            if (thr != null) {
                if (thr instanceof IOException)
                    throw (IOException) thr;
                if (thr instanceof RuntimeException)
                    throw (RuntimeException) thr;
                if (thr instanceof Error)
                    throw (Error) thr;
                else
                    throw new RuntimeException("Unexpected exception type: " + thr.getClass().getName() + ": " + thr.getMessage(), thr);
            }
        }

    }

}
