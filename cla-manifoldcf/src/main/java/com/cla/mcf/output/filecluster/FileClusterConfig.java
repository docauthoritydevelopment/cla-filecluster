/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cla.mcf.output.filecluster;

public class FileClusterConfig {

    // Configuration parameters
    public static final String USER_HTML_KEY = "connectionUser";
    public static final String PASSWORD_HTML_KEY = "connectionPassword";
    public static final String CONNECTOR_TYPE_HTML_KEY = "connectionType";

    public static final String USER = "USER";
    public static final String PASSWORD = "PASSWORD";
    public static final String CONNECTOR_TYPE = "CONNECTOR_TYPE";

    public static final String USER_DEFAULT = "mcf";
    public static final String PASSWORD_DEFAULT = "QAZmcf2daEDC";
    public static final String TYPE_DEFAULT = "RAW";

    public static final java.lang.String SERVER_ADDRESS = "SERVER_ADDRESS";
    public static final java.lang.String ADDRESS_DEFAULT= "localhost";

    public static final java.lang.String SERVER_PORT = "SERVER_PORT";
    public static final String DEFAULT_PORT = "8080";
    public static final java.lang.String SOCKET_TIMEOUT = "SOCKET_TIMEOUT";
    public static final String CONNECTION_TIMEOUT = "CONNECTION_TIMEOUT";
    public static final java.lang.String MAX_DOCUMENT_LENGTH = "MAX_DOCUMENT_LENGTH";
    public static final java.lang.String INGEST_SIGNATURE_MAX_SIZE = "INGEST_SIGNATURE_MAX_SIZE";
}
