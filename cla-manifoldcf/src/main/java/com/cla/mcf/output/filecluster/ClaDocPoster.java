package com.cla.mcf.output.filecluster;

import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.cla.mcf.output.filecluster.entities.RawDocumentMessage;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.manifoldcf.agents.interfaces.IOutputRemoveActivity;
import org.apache.manifoldcf.agents.interfaces.ServiceInterruption;
import org.apache.manifoldcf.core.interfaces.ManifoldCFException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ClaDocPoster {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaDocPoster.class);

    private static final String RAW_DOCUMENT_URL = "/api/mcf/ingest/raw";
    private static final String TEST_CONNECTION_URL = "/api/mcf/ingest/test";
    public static final int CONNECT_TIMEOUT = 15000;
    final ObjectMapper objectMapper = new ObjectMapper();
	private final HttpHost httpHost;

    private final String type;
    private final String user;
    private final String password;

    private String authCookie;

    String serverAddress;
    String serverPort;

    public ClaDocPoster(Properties properties) {

        this.serverAddress = properties.getProperty(FileClusterConfig.SERVER_ADDRESS);
        this.serverPort = properties.getProperty(FileClusterConfig.SERVER_PORT);
        this.type = properties.getProperty(FileClusterConfig.CONNECTOR_TYPE);
        this.user = properties.getProperty(FileClusterConfig.USER);
        this.password = properties.getProperty(FileClusterConfig.PASSWORD);
        this.httpHost = new HttpHost(serverAddress, Integer.valueOf(serverPort), "http");
    }

    public ClaDocPoster(final String rootUrl) throws URISyntaxException {
		final URIBuilder uriBuilder = new URIBuilder(rootUrl);
		httpHost = new HttpHost(uriBuilder.getHost(), uriBuilder.getPort(), uriBuilder.getScheme());
        type = "legacy";
        this.user = System.getProperty("filecluster.username");
        this.password = System.getProperty("filecluster.password");
	}
	
	public void removeDoc(final String url) throws IOException {
		logger.info("Removing doc " + url + "...");
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			if(authCookie==null)
				authenticate(httpclient);
			final HttpDelete deleteRequest = new HttpDelete("/docs?url="+url);
			deleteRequest.setHeader("Cookie", authCookie);
			final HttpResponse response = httpclient.execute(httpHost, deleteRequest);

			if (response.getStatusLine().getStatusCode() != 200)
				throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode()
						+ " , while delete doc " + url);
			logger.info("Removed doc " + url + " succfully");
		}
	}

    public void postDoc(final RawDocumentMessage rawDocumentMessage) throws IOException {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			if(authCookie==null) {
                authenticate(httpclient);
            }
//			String postUrl = "/docs";
//			if(rawDocumentMessage.isFileFetcher()){
//				postUrl= postUrl+"/fetcher";
//				rawDocumentMessage.setUrl(rawDocumentMessage.getUrl().substring(0, rawDocumentMessage.getUrl().length()-1));
//			}
			logger.info("Post document to:" + RAW_DOCUMENT_URL + "...");
			final HttpPut putRequest = new HttpPut(RAW_DOCUMENT_URL + "?type="+type);
            putRequest.setHeader("Cookie", authCookie);
			final String jsonString = convertToJson(rawDocumentMessage);
			final StringEntity se = new StringEntity(jsonString);
			se.setContentType("application/json");
			putRequest.setEntity(se);
			final HttpResponse response = httpclient.execute(httpHost, putRequest);

			if (response.getStatusLine().getStatusCode() != 200)
				throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode()
						+ " , while sending doc " + rawDocumentMessage.getUrl());
			logger.info("Sent doc " + rawDocumentMessage.getUrl() + " successfully");
		}
        catch (IOException e) {
            logger.error("Failed to post document to DocAuthority",e);
            //Reconnect next time
            authCookie = null;
            throw e;
        }
	}

	private void authenticate(final HttpClient httpclient) throws IOException {
        try {
            logger.info("Authenticating with user {}",user);
            final HttpPost authpost = new HttpPost("/login");
            final List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("username", user));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            authpost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            final HttpResponse response = httpclient.execute(httpHost, authpost);
            if (response.getStatusLine().getStatusCode() != 200)
                throw new SecurityException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
            authCookie = response.getHeaders("Set-Cookie")[0].getValue();
            logger.info("Authenticated Successfully");
        } catch (IOException e) {
            logger.error("Failed to authenticate with user {} {} to url {}:{}/login",user,password,serverAddress,serverPort);
            authCookie = null;
            throw e;
        } catch (SecurityException e) {
            logger.error("Failed to authenticate with user {} to url /login",user);
            authCookie = null;
            throw e;
        }
    }

	private String convertToJson(final RawDocumentMessage rawDocumentMessage) throws IOException {
		final StringWriter jsonString = new StringWriter();
		objectMapper.writeValue(jsonString, rawDocumentMessage);
		return jsonString.toString();
	}

	public void analyze() throws ManifoldCFException  {
		logger.info("Start analyze... ");
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			if(authCookie==null)
				authenticate(httpclient);
			final HttpPost postRequest = new HttpPost("/analyze");
			postRequest.setHeader("Cookie", authCookie);
			final HttpResponse response = httpclient.execute(httpHost, postRequest);

			if (response.getStatusLine().getStatusCode() != 200)
				throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode()+ " , while analyze ");
			logger.info("Finished analyze... ");
		} catch (final IOException e) {
			throw new ManifoldCFException(e);
		}
		
		
		
	}

    public void testConnection() throws ServiceInterruption {
        //TODO
        logger.info("Test Connection... Disabled for now ");
//        RequestConfig requestConfig = RequestConfig.custom()
//                .setConnectTimeout(CONNECT_TIMEOUT).setConnectionRequestTimeout(CONNECT_TIMEOUT).build();
//        try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build()) {
//            if(authCookie==null) {
//                authenticate(httpclient);
//            }
//            final HttpGet getRequest = new HttpGet(TEST_CONNECTION_URL);
//            getRequest.setHeader("Cookie", authCookie);
//            final HttpResponse response = httpclient.execute(httpHost, getRequest);
//            if (response.getStatusLine().getStatusCode() != 200)
//                throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode()+ " , while testing connection");
//            logger.info("Finished testing connection... ");
//        } catch (final Exception e) {
//            logger.error("Failed to test connection to URI {}",TEST_CONNECTION_URL,e);
//            throw new ManifoldCFException("Failed to test connection to DocAuthority",e);
//        }
    }

    public void removeDocument(String documentURI, IOutputRemoveActivity activities) {
        //TODO
    }

    public void shutdown() {
        //TODO
    }
}
