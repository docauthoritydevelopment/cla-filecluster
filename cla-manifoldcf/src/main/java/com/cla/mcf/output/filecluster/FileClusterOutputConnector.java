/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cla.mcf.output.filecluster;

import org.apache.manifoldcf.agents.interfaces.*;
import org.apache.manifoldcf.core.interfaces.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * This is a kafka output connector.
 */
public class FileClusterOutputConnector extends org.apache.manifoldcf.agents.output.BaseOutputConnector {

    public static final String _rcsid = "@(#)$Id: FileClusterOutputConnector.java 988245 2010-08-23 18:39:35Z kwright $";

    // Activities we log

    /**
     * Ingestion activity
     */
    public final static String INGEST_ACTIVITY = "document ingest";
    /**
     * Document removal activity
     */
    public final static String REMOVE_ACTIVITY = "document deletion";

    /**
     * Local connection
     */
    ClaHttpPoster httpPoster = null;

    /**
     * Expiration
     */
    protected long expirationTime = -1L;

    /**
     * The maximum document length
     */
    protected Long maxDocumentLength = null;
    /**
     * Included mime types string
     */
    protected String includedMimeTypesString = null;
    /**
     * Included mime types
     */
    protected Map<String, String> includedMimeTypes = null;
    /**
     * Excluded mime types string
     */
    protected String excludedMimeTypesString = null;
    /**
     * Excluded mime types
     */
    protected Map<String, String> excludedMimeTypes = null;

    /**
     * Idle connection expiration interval
     */
    protected final static long EXPIRATION_INTERVAL = 300000L;

    /**
     * Job notify activity
     */
    public final static String JOB_COMPLETE_ACTIVITY = "output notification";

    private final static String KAFKA_TAB_PARAMETERS = "KafkaConnector.Parameters";

    /**
     * Forward to the javascript to check the configuration parameters
     */
    private static final String EDIT_CONFIG_HEADER_FORWARD = "editConfiguration.js";

    /**
     * Forward to the HTML template to edit the configuration parameters
     */
    private static final String EDIT_CONFIG_FORWARD_PARAMETERS = "editConfiguration_Parameters.html";

    /**
     * Forward to the HTML template to view the configuration parameters
     */
    private static final String VIEW_CONFIG_FORWARD = "viewConfiguration.html";

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(FileClusterOutputConnector.class);


    public static final String DOCAUTHORITY_PROPERTIES = "docauthority.properties";

    /**
     * Constructor.
     */
    public FileClusterOutputConnector() {
    }

    /**
     * Return the list of activities that this connector supports (i.e. writes into the log).
     *
     * @return the list.
     */
    @Override
    public String[] getActivitiesList() {
        return new String[]{INGEST_ACTIVITY, REMOVE_ACTIVITY};
    }
//    public String[] getActivitiesList() {
//        return new String[]{INGEST_ACTIVITY, JOB_COMPLETE_ACTIVITY};
//    }

    /**
     * Connect.
     *
     * @param configParameters is the set of configuration parameters, which in
     *                         this case describe the target appliance, basic auth configuration, etc.
     *                         (This formerly came out of the ini file.)
     */
    @Override
    public void connect(ConfigParams configParameters) {
        logger.info("FileClusterOutputConnector connect");
        super.connect(configParameters);

//        Properties properties = loadProperties(configParameters);
//        httpPoster = new ClaDocPoster(properties);
    }

    /**
     * This method is periodically called for all connectors that are connected but not
     * in active use.
     */
    @Override
    public void poll()
            throws ManifoldCFException {
        if (httpPoster != null) {
            if (expirationTime <= System.currentTimeMillis()) {
                // Expire connection
                httpPoster.shutdown();
                httpPoster = null;
                expirationTime = -1L;
            }
        }
    }

    /**
     * This method is called to assess whether to count this connector instance should
     * actually be counted as being connected.
     *
     * @return true if the connector instance is actually connected.
     */
    @Override
    public boolean isConnected() {
        return httpPoster != null;
    }

    /**
     * Close the connection.  Call this before discarding the connection.
     */
    @Override
    public void disconnect()
            throws ManifoldCFException {
        if (httpPoster != null) {
            httpPoster.shutdown();
            httpPoster = null;
            expirationTime = -1L;
        }
        maxDocumentLength = null;
        includedMimeTypesString = null;
        includedMimeTypes = null;
        excludedMimeTypesString = null;
        excludedMimeTypes = null;
        super.disconnect();
    }

    protected void getSession()
            throws ManifoldCFException, SecurityException {
        if (httpPoster == null) {
            // Pick up timeouts

            try {
                Properties properties = loadProperties(params);
                httpPoster = new ClaHttpPoster(properties);
            } catch (NumberFormatException e) {
                throw new ManifoldCFException(e.getMessage());
            }


        }
        expirationTime = System.currentTimeMillis() + EXPIRATION_INTERVAL;
    }

    /**
     * Test the connection.  Returns a string describing the connection integrity.
     *
     * @return the connection's status as a displayable string.
     */
    @Override
    public String check()
            throws ManifoldCFException {
        try {
            getSession();
//            httpPoster.checkPost();
            return super.check();
        } catch (SecurityException e) {
            return "Security error: " + e.getMessage();
//            return "Transient error: "+e.getMessage();
        }
    }


    public Properties loadProperties(ConfigParams configParameters) {
        Properties properties = new Properties();
        File propFile = new File(DOCAUTHORITY_PROPERTIES);
        if (propFile.exists()) {
            logger.debug("Loading properties file {}");
            try {
                properties.load(new FileInputStream(propFile));
            } catch (IOException e) {
                logger.error("Failed to load properties file {}", propFile, e);
            }
        } else {
            logger.warn("Properties file {} not found", propFile);
            properties.put(FileClusterConfig.SERVER_ADDRESS, FileClusterConfig.ADDRESS_DEFAULT);
            properties.put(FileClusterConfig.SERVER_PORT, FileClusterConfig.DEFAULT_PORT);
            properties.put(FileClusterConfig.CONNECTOR_TYPE, FileClusterConfig.TYPE_DEFAULT);
        }

        String user = configParameters.getParameter(FileClusterConfig.USER);
        String password = configParameters.getParameter(FileClusterConfig.PASSWORD);
//            String password = params.getObfuscatedParameter(SolrConfig.PARAM_PASSWORD);
        String type = configParameters.getParameter(FileClusterConfig.CONNECTOR_TYPE);
        String port = configParameters.getParameter(FileClusterConfig.SERVER_PORT);
        logger.info("Using user: {}", user);
        properties.put(FileClusterConfig.USER, user);
        properties.put(FileClusterConfig.PASSWORD, password);
        properties.put(FileClusterConfig.CONNECTOR_TYPE, type);
        if (port != null && !port.isEmpty()) {
            properties.put(FileClusterConfig.SERVER_PORT, port);
        }

        String socketTimeoutString = params.getParameter(FileClusterConfig.SOCKET_TIMEOUT);
        if (socketTimeoutString == null) {
            socketTimeoutString = "900000";
        }
        properties.put(FileClusterConfig.SOCKET_TIMEOUT, socketTimeoutString);
        String connectTimeoutString = params.getParameter(FileClusterConfig.CONNECTION_TIMEOUT);
        if (connectTimeoutString == null) {
            connectTimeoutString = "60000";
        }
        properties.put(FileClusterConfig.CONNECTION_TIMEOUT, connectTimeoutString);
        properties.put(FileClusterConfig.MAX_DOCUMENT_LENGTH, "1000000000");
        properties.put(FileClusterConfig.INGEST_SIGNATURE_MAX_SIZE, "25000000");
        return properties;
    }


    /**
     * Fill in a Server tab configuration parameter map for calling a Velocity
     * template.
     *
     * @param newMap     is the map to fill in
     * @param parameters is the current set of configuration parameters
     */
    private static void fillInServerConfigurationMap(Map<String, Object> newMap, IPasswordMapperActivity mapper, ConfigParams parameters) {
        logger.info("Fill server configuration map");
        String user = parameters.getParameter(FileClusterConfig.USER);
        String password = parameters.getParameter(FileClusterConfig.PASSWORD);
        String type = parameters.getParameter(FileClusterConfig.CONNECTOR_TYPE);
        String serverPort = parameters.getParameter(FileClusterConfig.SERVER_PORT);

        if (user == null || user.isEmpty()) {
            user = FileClusterConfig.USER_DEFAULT;
        }
        if (password == null || password.isEmpty()) {
            password = FileClusterConfig.PASSWORD_DEFAULT;
        }
        if (type == null || type.isEmpty()) {
            type = FileClusterConfig.TYPE_DEFAULT;
        }
        if (serverPort == null || serverPort.isEmpty()) {
            serverPort = FileClusterConfig.DEFAULT_PORT;
        }

        newMap.put(FileClusterConfig.USER, user);
        newMap.put(FileClusterConfig.PASSWORD, password);
        newMap.put(FileClusterConfig.CONNECTOR_TYPE, type);
        newMap.put(FileClusterConfig.SERVER_PORT, serverPort);
        logger.info("Server configuration map filled");
    }

    @Override
    public void outputConfigurationHeader(IThreadContext threadContext,
                                          IHTTPOutput out, Locale locale, ConfigParams parameters,
                                          List<String> tabsArray) throws ManifoldCFException, IOException {
        // Add the Server tab
        tabsArray.add(Messages.getString(locale, KAFKA_TAB_PARAMETERS));
        // Map the parameters
        Map<String, Object> paramMap = new HashMap<String, Object>();

        // Fill in the parameters from each tab
        fillInServerConfigurationMap(paramMap, out, parameters);

        // Output the Javascript - only one Velocity template for all tabs
        Messages.outputResourceWithVelocity(out, locale, EDIT_CONFIG_HEADER_FORWARD, paramMap);

    }

    @Override
    public void outputConfigurationBody(IThreadContext threadContext,
                                        IHTTPOutput out, Locale locale, ConfigParams parameters, String tabName)
            throws ManifoldCFException, IOException {

        // Call the Velocity templates for each tab
        Map<String, Object> paramMap = new HashMap<String, Object>();

        // Set the tab name
        paramMap.put("TABNAME", tabName);

        // Fill in the parameters
        fillInServerConfigurationMap(paramMap, out, parameters);

        // Server tab
        Messages.outputResourceWithVelocity(out, locale, EDIT_CONFIG_FORWARD_PARAMETERS, paramMap);
    }

    @Override
    public void viewConfiguration(IThreadContext threadContext, IHTTPOutput out,
                                  Locale locale, ConfigParams parameters) throws ManifoldCFException,
            IOException {
        logger.info("View configuration");
        Map<String, Object> paramMap = new HashMap<String, Object>();

        // Fill in map from each tab
        fillInServerConfigurationMap(paramMap, out, parameters);

        Messages.outputResourceWithVelocity(out, locale, VIEW_CONFIG_FORWARD, paramMap);
    }

    @Override
    public String processConfigurationPost(IThreadContext threadContext,
                                           IPostParameters variableContext, ConfigParams parameters)
            throws ManifoldCFException {
        logger.info("Process configuration post");
        // Server tab parameters
        String user = variableContext.getParameter(FileClusterConfig.USER_HTML_KEY);
        if (user != null) {
            parameters.setParameter(FileClusterConfig.USER, user);
        }
        String password = variableContext.getParameter(FileClusterConfig.PASSWORD_HTML_KEY);
        if (password != null) {
            parameters.setParameter(FileClusterConfig.PASSWORD, password);
        }

        String type = variableContext.getParameter(FileClusterConfig.CONNECTOR_TYPE_HTML_KEY);
        if (type != null) {
            parameters.setParameter(FileClusterConfig.CONNECTOR_TYPE, type);
        }

        String serverPort = variableContext.getParameter(FileClusterConfig.SERVER_PORT);
        if (serverPort != null) {
            parameters.setParameter(FileClusterConfig.SERVER_PORT, serverPort);
        }

        return null;
    }

    /**
     * Get an output version string, given an output specification. The output
     * version string is used to uniquely describe the pertinent details of the
     * output specification and the configuration, to allow the Connector
     * Framework to determine whether a document will need to be output again.
     * Note that the contents of the document cannot be considered by this method,
     * and that a different version string (defined in IRepositoryConnector) is
     * used to describe the version of the actual document.
     * <p>
     * This method presumes that the connector object has been configured, and it
     * is thus able to communicate with the output data store should that be
     * necessary.
     *
     * @param spec is the current output specification for the job that is doing
     *             the crawling.
     * @return a string, of unlimited length, which uniquely describes output
     * configuration and specification in such a way that if two such strings are
     * equal, the document will not need to be sent again to the output data
     * sstore.
     */
    @Override
    public VersionContext getPipelineDescription(Specification spec)
            throws ManifoldCFException, ServiceInterruption {
//        getSession();
        return new VersionContext("1.5.0", params, spec);
    }

    /**
     * Pre-determine whether a document's length is indexable by this connector.  This method is used by participating repository connectors
     * to help filter out documents that are too long to be indexable.
     *
     * @param outputDescription is the document's output version.
     * @param length            is the length of the document.
     * @return true if the file is indexable.
     */
    @Override
    public boolean checkLengthIndexable(VersionContext outputDescription, long length, IOutputCheckActivity activities)
            throws ManifoldCFException, ServiceInterruption {
//        getSession();
        if (maxDocumentLength != null && length > maxDocumentLength.longValue())
            return false;
        return true;
    }

    /**
     * Add (or replace) a document in the output data store using the connector.
     * This method presumes that the connector object has been configured, and it
     * is thus able to communicate with the output data store should that be
     * necessary.
     *
     * @param documentURI         is the URI of the document. The URI is presumed to be
     *                            the unique identifier which the output data store will use to process and
     *                            serve the document. This URI is constructed by the repository connector
     *                            which fetches the document, and is thus universal across all output
     *                            connectors.
     * @param document            is the document data to be processed (handed to the output
     *                            data store).
     * @param authorityNameString is the name of the authority responsible for
     *                            authorizing any access tokens passed in with the repository document. May
     *                            be null.
     * @param activities          is the handle to an object that the implementer of a
     *                            pipeline connector may use to perform operations, such as logging
     *                            processing activity, or sending a modified document to the next stage in
     *                            the pipeline.
     * @return the document status (accepted or permanently rejected).
     * @throws IOException only if there's a stream error reading the document
     *                     data.
     */
//    @Override
//    public int addOrReplaceDocumentWithException(String documentURI, VersionContext outputDescription, RepositoryDocument document, String authorityNameString, IOutputAddActivity activities)
//            throws ManifoldCFException, ServiceInterruption, IOException {
//
//        // Establish a session
//        getSession();
//
//        String errorCode = "OK";
//        String errorDesc = null;
//        try {
//
//            byte[] content = extractContent(document);
//            Map<String, String[]> metadata = extractMetadata(document);
//            final Map<String, List<String>> aclsMap = new HashMap<>();
//            final Map<String, List<String>> denyAclsMap = new HashMap<>();
//            populateAclMaps(document, aclsMap, denyAclsMap);
//            RawDocumentMessage rawDocumentMessage = new RawDocumentMessage(documentURI, content, metadata, aclsMap, denyAclsMap);
//
//            httpPoster.postDocument(rawDocumentMessage);
//        } catch (final SecurityException e) {
//            errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
//            errorDesc = "Failed to write document due to: " + e.getMessage();
//            handleSecurityException(e);
//            return DOCUMENTSTATUS_REJECTED;
//        } catch (final IOException e) {
//            errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
//            errorDesc = "Failed to write document due to: " + e.getMessage();
//            handleIOException(e);
//            return DOCUMENTSTATUS_REJECTED;
//        } finally {
//            activities.recordActivity(null, INGEST_ACTIVITY, new Long(document.getBinaryLength()), documentURI,
//                    errorCode, errorDesc);
//        }
//        return DOCUMENTSTATUS_ACCEPTED;
//    }

    /**
     * Add (or replace) a document in the output data store using the connector.
     * This method presumes that the connector object has been configured, and it is thus able to communicate with the output data store should that be
     * necessary.
     *
     * @param documentURI         is the URI of the document.  The URI is presumed to be the unique identifier which the output data store will use to process
     *                            and serve the document.  This URI is constructed by the repository connector which fetches the document, and is thus universal across all output connectors.
     * @param pipelineDescription includes the description string that was constructed for this document by the getOutputDescription() method.
     * @param document            is the document data to be processed (handed to the output data store).
     * @param authorityNameString is the name of the authority responsible for authorizing any access tokens passed in with the repository document.  May be null.
     * @param activities          is the handle to an object that the implementer of a pipeline connector may use to perform operations, such as logging processing activity,
     *                            or sending a modified document to the next stage in the pipeline.
     * @return the document status (accepted or permanently rejected).
     * @throws IOException only if there's a stream error reading the document data.
     */
    @Override
    public int addOrReplaceDocumentWithException(String documentURI, VersionContext pipelineDescription, RepositoryDocument document, String authorityNameString, IOutputAddActivity activities)
            throws ManifoldCFException, ServiceInterruption, IOException {
        // Establish a session
        getSession();

        // Now, go off and call the ingest API.
        if (httpPoster.indexPost(documentURI, document, authorityNameString, activities))
            return DOCUMENTSTATUS_ACCEPTED;
        return DOCUMENTSTATUS_REJECTED;
    }

    /**
     * Remove a document using the connector.
     * Note that the last outputDescription is included, since it may be necessary for the connector to use such information to know how to properly remove the document.
     *
     * @param documentURI       is the URI of the document.  The URI is presumed to be the unique identifier which the output data store will use to process
     *                          and serve the document.  This URI is constructed by the repository connector which fetches the document, and is thus universal across all output connectors.
     * @param outputDescription is the last description string that was constructed for this document by the getOutputDescription() method above.
     * @param activities        is the handle to an object that the implementer of an output connector may use to perform operations, such as logging processing activity.
     */
    @Override
    public void removeDocument(String documentURI, String outputDescription, IOutputRemoveActivity activities)
            throws ManifoldCFException, ServiceInterruption {
        // Establish a session
        try {
            getSession();
            httpPoster.deletePostDocument(documentURI, activities);
        } catch (SecurityException e) {
            logger.error("Failed to delete document. Security exception");
            //TODO - log in activities
        }
    }

    /**
     * Notify the connector of a completed job.
     * This is meant to allow the connector to flush any internal data structures it has been keeping around, or to tell the output repository that this
     * is a good time to synchronize things.  It is called whenever a job is either completed or aborted.
     *
     * @param activities is the handle to an object that the implementer of an output connector may use to perform operations, such as logging processing activity.
     */
    @Override
    public void noteJobComplete(IOutputNotifyActivity activities)
            throws ManifoldCFException, ServiceInterruption {
        // Establish a session
        logger.info("Job complete");
        try {
            getSession();
        } catch (SecurityException e) {
            logger.info("Failed to notify that job is complete. Authorization error: " + e.getMessage());
        }

        // TODO - notify DocAuthority that scan is complete
    }


//    private static String getConfig(ConfigParams config,
//                                    String parameter,
//                                    String defaultValue) {
//        if (config == null) {
//            return defaultValue;
//        }
//        final String protocol = config.getParameter(parameter);
//        if (protocol == null) {
//            return defaultValue;
//        }
//        return protocol;
//    }

//    protected static void handleJSONException(final JSONException e) throws ManifoldCFException {
//        Logging.agents.error("FileSystem: JSONException: " + e.getMessage(), e);
//        throw new ManifoldCFException(e.getMessage(), e);
//    }
//
//    protected static void handleURISyntaxException(final URISyntaxException e) throws ManifoldCFException {
//        Logging.agents.error("FileSystem: URISyntaxException: " + e.getMessage(), e);
//        throw new ManifoldCFException(e.getMessage(), e);
//    }
//
//    protected static void handleSecurityException(final SecurityException e) throws ManifoldCFException {
//        Logging.agents.error("FileSystem: SecurityException: " + e.getMessage(), e);
//        throw new ManifoldCFException(e.getMessage(), e);
//    }
//
//    protected static void handleFileNotFoundException(final FileNotFoundException e) throws ManifoldCFException {
//        Logging.agents.error("FileSystem: Path is illegal: " + e.getMessage(), e);
//        throw new ManifoldCFException(e.getMessage(), e);
//    }

//    /**
//     * Handle IOException
//     */
//    protected static void handleIOException(final IOException e) throws ManifoldCFException, ServiceInterruption {
//        if (!(e instanceof java.net.SocketTimeoutException) && (e instanceof InterruptedIOException)) {
//            throw new ManifoldCFException("Interrupted: " + e.getMessage(), e, ManifoldCFException.INTERRUPTED);
//        }
//        final long currentTime = System.currentTimeMillis();
//        Logging.agents.warn("FileSystem: IO exception: " + e.getMessage(), e);
//        throw new ServiceInterruption("IO exception: " + e.getMessage(), e, currentTime + 300000L,
//                currentTime + 3 * 60 * 60000L, -1, false);
//    }

}
