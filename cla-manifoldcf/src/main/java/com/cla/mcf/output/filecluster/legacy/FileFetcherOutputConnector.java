/* $Id: FileOutputConnector.java 991374 2013-05-31 23:04:08Z minoru $ */

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cla.mcf.output.filecluster.legacy;

import com.cla.mcf.output.filecluster.ClaDocPoster;
import com.cla.mcf.output.filecluster.entities.RawDocumentMessage;
import org.apache.commons.io.IOUtils;
import org.apache.manifoldcf.agents.interfaces.*;
import org.apache.manifoldcf.agents.output.BaseOutputConnector;
import org.apache.manifoldcf.agents.system.Logging;
import org.apache.manifoldcf.core.interfaces.*;
import org.json.JSONException;

import java.io.*;
import java.net.URISyntaxException;
import java.util.*;

public class FileFetcherOutputConnector extends BaseOutputConnector {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(FileFetcherOutputConnector.class);

	public static final String _rcsid = "@(#)$Id: FileOutputConnector.java 988245 2010-08-23 18:39:35Z minoru $";

	// Activities we log

	/** Ingestion activity */
	public final static String INGEST_ACTIVITY = "document ingest";
	/** Document removal activity */
	public final static String REMOVE_ACTIVITY = "document deletion";

	// Activities list
	protected static final String[] activitiesList = new String[] { INGEST_ACTIVITY, REMOVE_ACTIVITY };

//	/** Forward to the javascript to check the configuration parameters */
//	private static final String EDIT_CONFIGURATION_JS = "editConfiguration.js";
//
//	/** Forward to the HTML template to edit the configuration parameters */
//	private static final String EDIT_CONFIGURATION_HTML = "editConfiguration.html";
//
//	/** Forward to the HTML template to view the configuration parameters */
//	private static final String VIEW_CONFIGURATION_HTML = "viewConfiguration.html";

	/**
	 * Forward to the javascript to check the specification parameters for the
	 * job
	 */
	private static final String EDIT_SPECIFICATION_JS = "editSpecification.js";

	/** Forward to the template to edit the configuration parameters for the job */
	private static final String EDIT_SPECIFICATION_HTML = "editSpecification.html";

	/** Forward to the template to view the specification parameters for the job */
	private static final String VIEW_SPECIFICATION_HTML = "viewSpecification.html";

    private ClaDocPoster claDocPoster;

//	private String rootUrl;

	private FileOutputSpecs specs;

    public ClaDocPoster getClaDocPoster() {
		return claDocPoster;
	}

	/**
	 * Constructor.
	 */
	public FileFetcherOutputConnector() {
	}

	/**
	 * Return the list of activities that this connector supports (i.e. writes
	 * into the log).
	 *
	 * @return the list.
	 */
	@Override
	public String[] getActivitiesList() {
		return activitiesList;
	}

	/**
	 * Connect.
	 *
	 * @param configParameters
	 *            is the set of configuration parameters, which in this case
	 *            describe the target appliance, basic auth configuration, etc.
	 *            (This formerly came out of the ini file.)
	 */
	@Override
	public void connect(final ConfigParams configParameters) {
		super.connect(configParameters);
		logger.info("connect {}",configParameters);
	}
	
	

	@Override
	public void noteJobComplete(final IOutputNotifyActivity activities) throws ManifoldCFException, ServiceInterruption {
		logger.info("Finished fetching file");
	}

	/**
	 * Close the connection. Call this before discarding the connection.
	 */
	@Override
	public void disconnect() throws ManifoldCFException {
		super.disconnect();
		logger.info("disconnect");
	}

	/** Set up a session 
	 * @throws URISyntaxException */
	protected void getSession() throws URISyntaxException  {
		if (claDocPoster == null) {
            claDocPoster = new ClaDocPoster(specs.getRootPath());
        }
	}

	/**
	 * Test the connection. Returns a string describing the connection
	 * integrity.
	 *
	 * @return the connection's status as a displayable string.
	 */
	@Override
	public String check() throws ManifoldCFException {
//		try {
			logger.info("check()");
//			getSession();
			return super.check();
//		} catch (final URISyntaxException e) {
//			return "Transient error: " + e.getMessage();
//		}
	}

	/**
	 * Get an output version string, given an output specification. The output
	 * version string is used to uniquely describe the pertinent details of the
	 * output specification and the configuration, to allow the Connector
	 * Framework to determine whether a document will need to be output again.
	 * Note that the contents of the document cannot be considered by this
	 * method, and that a different version string (defined in
	 * IRepositoryConnector) is used to describe the version of the actual
	 * document.
	 *
	 * This method presumes that the connector object has been configured, and
	 * it is thus able to communicate with the output data store should that be
	 * necessary.
	 *
	 * @param spec
	 *            is the current output specification for the job that is doing
	 *            the crawling.
	 * @return a string, of unlimited length, which uniquely describes output
	 *         configuration and specification in such a way that if two such
	 *         strings are equal, the document will not need to be sent again to
	 *         the output data store.
	 */
	@Override
	public VersionContext getPipelineDescription(final Specification spec) throws ManifoldCFException,
			ServiceInterruption {
//		logger.info("getPipelineDescription");
		specs = new FileOutputSpecs(getSpecNode(spec));
		return new VersionContext(specs.toVersionString(), params, spec);
		
	}

	/**
	 * Add (or replace) a document in the output data store using the connector.
	 * This method presumes that the connector object has been configured, and
	 * it is thus able to communicate with the output data store should that be
	 * necessary. The OutputSpecification is *not* provided to this method,
	 * because the goal is consistency, and if output is done it must be
	 * consistent with the output description, since that was what was partly
	 * used to determine if output should be taking place. So it may be
	 * necessary for this method to decode an output description string in order
	 * to determine what should be done.
	 *
	 * @param documentURI
	 *            is the URI of the document. The URI is presumed to be the
	 *            unique identifier which the output data store will use to
	 *            process and serve the document. This URI is constructed by the
	 *            repository connector which fetches the document, and is thus
	 *            universal across all output connectors.
	 * @param outputDescription
	 *            is the description string that was constructed for this
	 *            document by the getOutputDescription() method.
	 * @param document
	 *            is the document data to be processed (handed to the output
	 *            data store).
	 * @param authorityNameString
	 *            is the name of the authority responsible for authorizing any
	 *            access tokens passed in with the repository document. May be
	 *            null.
	 * @param activities
	 *            is the handle to an object that the implementer of an output
	 *            connector may use to perform operations, such as logging
	 *            processing activity.
	 * @return the document status (accepted or permanently rejected).
	 */
	@Override
	public int addOrReplaceDocumentWithException(final String documentURI, final VersionContext outputDescription,
			final RepositoryDocument document, final String authorityNameString, final IOutputAddActivity activities)
			throws ManifoldCFException, ServiceInterruption, IOException {
		// Establish a session
//		getSession();

//		final FileOutputConfig config = getConfigParameters(null);

//		final FileOutputSpecs specs = new FileOutputSpecs(getSpecNode(outputDescription.getSpecification()));

		logger.info("addOrReplaceDocumentWithException {} {}",params,documentURI);
		String errorCode = "OK";
		String errorDesc = null;
		

		
		try {		
			getSession();
			
			final String claJobId = getValue(document,"claJobId");
			final String isFileFetcher = getValue(document,"isFileFetcher");
				
			
			final Map<String,List<String>> aclsMap = new HashMap<>();
		    final Map<String,List<String>> denyAclsMap = new HashMap<>();
			populateAclMaps(document, aclsMap, denyAclsMap);
			logger.info("ACLS for {}: acls:{} denyAcls:{}",documentURI,aclsMap,denyAclsMap);
			
//			claDocPoster.postDoc(new RawDocumentMessage(documentURI,claJobId,IOUtils.toByteArray(document.getBinaryStream()) , null, aclsMap,denyAclsMap,Boolean.valueOf(isFileFetcher)));
		} catch (final URISyntaxException e) {
		      errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
		      errorDesc = "Failed to write document due to: " + e.getMessage();
		      handleURISyntaxException(e);
		} catch (final SecurityException e) {
			errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
			errorDesc = "Failed to write document due to: " + e.getMessage();
			handleSecurityException(e);
			return DOCUMENTSTATUS_REJECTED;
//		} catch (final IOException e) {
//			errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
//			errorDesc = "Failed to write document due to: " + e.getMessage();
//			handleIOException(e);
//			return DOCUMENTSTATUS_REJECTED;
		} finally {
			activities.recordActivity(null, INGEST_ACTIVITY, new Long(document.getBinaryLength()), documentURI,
					errorCode, errorDesc);
		}

		return DOCUMENTSTATUS_ACCEPTED;
	}

	

	private String getValue(final RepositoryDocument document, final String fieldName) {
		final Object[] field = document.getField(fieldName);
		if(field==null){
			logger.warn(fieldName+" metadata is missing. The RawDocumentMessage will not have this information. To correct this you need to add it in the job",field);
			return null;
		}
		return field[0].toString();
	}

	private void populateAclMaps(final RepositoryDocument document, final Map<String, List<String>> aclsMap,
			final Map<String, List<String>> denyAclsMap) {
		for (final Iterator<String> iterator = document.securityTypesIterator(); iterator.hasNext();) {
			final String type = iterator.next();
			final String[] securityACL = document.getSecurityACL(type);
			if(securityACL!=null)
				aclsMap.put(type, Arrays.asList(securityACL));	
			final String[] securityDenyACL = document.getSecurityDenyACL(type);
			if(securityDenyACL!=null)
				denyAclsMap.put(type, Arrays.asList(securityDenyACL));	
		}
	}

	protected static void handleJSONException(final JSONException e) throws ManifoldCFException {
		Logging.agents.error("FileSystem: JSONException: " + e.getMessage(), e);
		throw new ManifoldCFException(e.getMessage(), e);
	}

	protected static void handleURISyntaxException(final URISyntaxException e) throws ManifoldCFException {
		Logging.agents.error("FileSystem: URISyntaxException: " + e.getMessage(), e);
		throw new ManifoldCFException(e.getMessage(), e);
	}

	protected static void handleSecurityException(final SecurityException e) throws ManifoldCFException {
		Logging.agents.error("FileSystem: SecurityException: " + e.getMessage(), e);
		throw new ManifoldCFException(e.getMessage(), e);
	}

	protected static void handleFileNotFoundException(final FileNotFoundException e) throws ManifoldCFException {
		Logging.agents.error("FileSystem: Path is illegal: " + e.getMessage(), e);
		throw new ManifoldCFException(e.getMessage(), e);
	}

	/** Handle IOException */
	protected static void handleIOException(final IOException e) throws ManifoldCFException, ServiceInterruption {
		if (!(e instanceof java.net.SocketTimeoutException) && (e instanceof InterruptedIOException)) {
			throw new ManifoldCFException("Interrupted: " + e.getMessage(), e, ManifoldCFException.INTERRUPTED);
		}
		final long currentTime = System.currentTimeMillis();
		Logging.agents.warn("FileSystem: IO exception: " + e.getMessage(), e);
		throw new ServiceInterruption("IO exception: " + e.getMessage(), e, currentTime + 300000L,
				currentTime + 3 * 60 * 60000L, -1, false);
	}

	/**
	 * Remove a document using the connector. Note that the last
	 * outputDescription is included, since it may be necessary for the
	 * connector to use such information to know how to properly remove the
	 * document.
	 *
	 * @param documentURI
	 *            is the URI of the document. The URI is presumed to be the
	 *            unique identifier which the output data store will use to
	 *            process and serve the document. This URI is constructed by the
	 *            repository connector which fetches the document, and is thus
	 *            universal across all output connectors.
	 * @param outputDescription
	 *            is the last description string that was constructed for this
	 *            document by the getOutputDescription() method above.
	 * @param activities
	 *            is the handle to an object that the implementer of an output
	 *            connector may use to perform operations, such as logging
	 *            processing activity.
	 */
	@Override
	public void removeDocument(final String documentURI, final String outputDescription,
			final IOutputRemoveActivity activities) throws ManifoldCFException, ServiceInterruption {
		// Establish a session
		logger.info("removeDocument {} {}",params,documentURI);

		String errorCode = "OK";
		String errorDesc = null;

//		final FileOutputConfig config = getConfigParameters(null);

//		final StringBuffer path = new StringBuffer();
		try {	
			getSession();
			claDocPoster.removeDoc(documentURI);
		} catch (final URISyntaxException e) {
		      errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
		      errorDesc = "Failed to delete document due to: " + e.getMessage();
		      handleURISyntaxException(e);
		} catch (final FileNotFoundException e) {
			errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
			errorDesc = "Failed to delete document due to: " + e.getMessage();
			handleFileNotFoundException(e);
		} catch (final SecurityException e) {
			errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
			errorDesc = "Failed to delete document due to: " + e.getMessage();
			handleSecurityException(e);
		} catch (final IOException e) {
			errorCode = e.getClass().getSimpleName().toUpperCase(Locale.ROOT);
			errorDesc = "Failed to delete document due to: " + e.getMessage();
			handleIOException(e);
		} finally {
			activities.recordActivity(null, REMOVE_ACTIVITY, null, documentURI, errorCode, errorDesc);
		}
	}

	/**
	 * Output the specification header section. This method is called in the
	 * head section of a job page which has selected a pipeline connection of
	 * the current type. Its purpose is to add the required tabs to the list, and
	 * to output any javascript methods that might be needed by the job editing
	 * HTML.
	 *
	 * @param out
	 *            is the output to which any HTML should be sent.
	 * @param locale
	 *            is the preferred local of the output.
	 * @param os
	 *            is the current pipeline specification for this connection.
	 * @param connectionSequenceNumber
	 *            is the unique number of this connection within the job.
	 * @param tabsArray
	 *            is an array of tab names. Add to this array any tab names that
	 *            are specific to the connector.
	 */
	@Override
	public void outputSpecificationHeader(final IHTTPOutput out, final Locale locale, final Specification os,
			final int connectionSequenceNumber, final List<String> tabsArray) throws ManifoldCFException, IOException {
		super.outputSpecificationHeader(out, locale, os, connectionSequenceNumber, tabsArray);
		tabsArray.add(Messages.getString(locale, "FileConnector.PathTabName"));
		outputResource(EDIT_SPECIFICATION_JS, out, locale, null, null, new Integer(connectionSequenceNumber), null);
	}

	/**
	 * Output the specification body section. This method is called in the body
	 * section of a job page which has selected a pipeline connection of the
	 * current type. Its purpose is to present the required form elements for
	 * editing. The coder can presume that the HTML that is output from this
	 * configuration will be within appropriate <html>, <body>, and <form> tags.
	 * The name of the form is "editjob".
	 *
	 * @param out
	 *            is the output to which any HTML should be sent.
	 * @param locale
	 *            is the preferred local of the output.
	 * @param os
	 *            is the current pipeline specification for this job.
	 * @param connectionSequenceNumber
	 *            is the unique number of this connection within the job.
	 * @param actualSequenceNumber
	 *            is the connection within the job that has currently been
	 *            selected.
	 * @param tabName
	 *            is the current tab name.
	 */
	@Override
	public void outputSpecificationBody(final IHTTPOutput out, final Locale locale, final Specification os,
			final int connectionSequenceNumber, final int actualSequenceNumber, final String tabName)
			throws ManifoldCFException, IOException {
		logger.info("outputSpecificationBody");
		super.outputSpecificationBody(out, locale, os, connectionSequenceNumber, actualSequenceNumber, tabName);
		final FileOutputSpecs specs = getSpecParameters(os);
		outputResource(EDIT_SPECIFICATION_HTML, out, locale, specs, tabName, new Integer(connectionSequenceNumber),
				new Integer(actualSequenceNumber));
	}

	/**
	 * Process a specification post. This method is called at the start of job's
	 * edit or view page, whenever there is a possibility that form data for a
	 * connection has been posted. Its purpose is to gather form information and
	 * modify the transformation specification accordingly. The name of the
	 * posted form is "editjob".
	 *
	 * @param variableContext
	 *            contains the post data, including binary file-upload
	 *            information.
	 * @param locale
	 *            is the preferred local of the output.
	 * @param os
	 *            is the current pipeline specification for this job.
	 * @param connectionSequenceNumber
	 *            is the unique number of this connection within the job.
	 * @return null if all is well, or a string error message if there is an
	 *         error that should prevent saving of the job (and cause a
	 *         redirection to an error page).
	 */
	@Override
	public String processSpecificationPost(final IPostParameters variableContext, final Locale locale,
			final Specification os, final int connectionSequenceNumber) throws ManifoldCFException {
		logger.info("processSpecificationPost {} ",params);
		ConfigurationNode specNode = getSpecNode(os);
		final boolean bAdd = (specNode == null);
		if (bAdd) {
			specNode = new SpecificationNode(FileOutputConstant.PARAM_ROOTPATH);
		}
		FileOutputSpecs.contextToSpecNode(variableContext, specNode, connectionSequenceNumber);
		if (bAdd) {
			os.addChild(os.getChildCount(), specNode);
		}

		return null;
	}

	/**
	 * View specification. This method is called in the body section of a job's
	 * view page. Its purpose is to present the pipeline specification
	 * information to the user. The coder can presume that the HTML that is
	 * output from this configuration will be within appropriate <html> and
	 * <body> tags.
	 *
	 * @param out
	 *            is the output to which any HTML should be sent.
	 * @param locale
	 *            is the preferred local of the output.
	 * @param connectionSequenceNumber
	 *            is the unique number of this connection within the job.
	 * @param os
	 *            is the current pipeline specification for this job.
	 */
	@Override
	public void viewSpecification(final IHTTPOutput out, final Locale locale, final Specification os,
			final int connectionSequenceNumber) throws ManifoldCFException, IOException {
		logger.info("viewSpecification");
		outputResource(VIEW_SPECIFICATION_HTML, out, locale, getSpecParameters(os), null, new Integer(
				connectionSequenceNumber), null);
	}

	/**
	 * @param os
	 * @return
	 */
	final private SpecificationNode getSpecNode(final Specification os) {
		final int l = os.getChildCount();
		for (int i = 0; i < l; i++) {
			final SpecificationNode node = os.getChild(i);
			if (node.getType().equals(FileOutputConstant.PARAM_ROOTPATH)) {
				return node;
			}
		}
		return null;
	}

	/**
	 * @param os
	 * @return
	 * @throws ManifoldCFException
	 */
	final private FileOutputSpecs getSpecParameters(final Specification os) throws ManifoldCFException {
		return new FileOutputSpecs(getSpecNode(os));
	}

//	/**
//	 * @param configParams
//	 * @return
//	 */
//	final private FileOutputConfig getConfigParameters(ConfigParams configParams) {
//		if (configParams == null)
//			configParams = getConfiguration();
//		return new FileOutputConfig(configParams);
//	}

	/**
	 * Read the content of a resource, replace the variable ${PARAMNAME} with
	 * the value and copy it to the out.
	 * 
	 * @param resName
	 * @param out
	 * @throws ManifoldCFException
	 */
	private static void outputResource(final String resName, final IHTTPOutput out, final Locale locale,
			final FileOutputParam params, final String tabName, final Integer sequenceNumber,
			final Integer currentSequenceNumber) throws ManifoldCFException {
		Map<String, String> paramMap = null;
		if (params != null) {
			paramMap = params.buildMap();
			if (tabName != null) {
				paramMap.put("TabName", tabName);
			}
			if (currentSequenceNumber != null)
				paramMap.put("SelectedNum", currentSequenceNumber.toString());
		} else {
			paramMap = new HashMap<String, String>();
		}
		if (sequenceNumber != null)
			paramMap.put("SeqNum", sequenceNumber.toString());
		Messages.outputResourceWithVelocity(out, locale, resName, paramMap, true);
	}

//	/**
//	 * @param documentURI
//	 * @return
//	 * @throws URISyntaxException
//	 * @throws NullPointerException
//	 */
//	final private String documentURItoFilePath(final String documentURI) throws URISyntaxException,
//			NullPointerException {
//		final StringBuffer path = new StringBuffer();
//		URI uri = null;
//
//		uri = new URI(documentURI);
//
//		if (uri.getScheme() != null) {
//			path.append(uri.getScheme());
//			path.append("/");
//		}
//
//		if (uri.getHost() != null) {
//			path.append(uri.getHost());
//			if (uri.getPort() != -1) {
//				path.append(":");
//				path.append(uri.getPort());
//			}
//			if (uri.getRawPath() != null) {
//				if (uri.getRawPath().length() == 0) {
//					path.append("/");
//				} else if (uri.getRawPath().equals("/")) {
//					path.append(uri.getRawPath());
//				} else {
//					for (final String name : uri.getRawPath().split("/")) {
//						if (name.length() > 0) {
//							path.append("/");
//							path.append(convertString(name));
//						}
//					}
//				}
//			}
//			if (uri.getRawQuery() != null) {
//				path.append("?");
//				path.append(convertString(uri.getRawQuery()));
//			}
//		} else {
//			if (uri.getRawSchemeSpecificPart() != null) {
//				for (final String name : uri.getRawSchemeSpecificPart().split("/")) {
//					if (name.length() > 0) {
//						path.append("/");
//						path.append(convertString(name));
//					}
//				}
//			}
//		}
//
//		if (path.toString().endsWith("/")) {
//			path.append(".content");
//		}
//		return path.toString();
//	}

//	final private String convertString(final String input) {
//		final StringBuilder sb = new StringBuilder();
//		for (int i = 0; i < input.length(); i++) {
//			final char c = input.charAt(i);
//			// Handle filename disallowed special characters!
//			if (c == ':') {
//				// MHL for what really happens to colons
//			} else
//				sb.append(c);
//		}
//		return sb.toString();
//	}

	protected static class FileOutputSpecs extends FileOutputParam {
		/**
     * 
     */
		private static final long serialVersionUID = 1859652730572662025L;

		final public static ParameterEnum[] SPECIFICATIONLIST = { ParameterEnum.ROOTPATH };

		private final String rootPath;

		/**
		 * Build a set of ElasticSearch parameters by reading an instance of
		 * SpecificationNode.
		 * 
		 * @param node
		 * @throws ManifoldCFException
		 */
		public FileOutputSpecs(final ConfigurationNode node) throws ManifoldCFException {
			super(SPECIFICATIONLIST);
			String rootPath = null;
			for (final ParameterEnum param : SPECIFICATIONLIST) {
				String value = null;
				if (node != null) {
					value = node.getAttributeValue(param.name());
				}
				if (value == null) {
					value = param.defaultValue;
				}
				put(param, value);
			}
			rootPath = getRootPath();
			this.rootPath = rootPath;
		}

		/**
		 * @param variableContext
		 * @param specNode
		 */
		public static void contextToSpecNode(final IPostParameters variableContext, final ConfigurationNode specNode,
				final int sequenceNumber) {
			for (final ParameterEnum param : SPECIFICATIONLIST) {
				final String p = variableContext.getParameter("s" + sequenceNumber + "_" + param.name().toLowerCase());
				if (p != null) {
					specNode.setAttribute(param.name(), p);
				}
			}
		}

		/** @return a version string representation of the parameter list */
		public String toVersionString() {
			final StringBuilder sb = new StringBuilder();
			pack(sb, rootPath, '+');
			return sb.toString();
		}

		/**
		 * @return
		 */
		public String getRootPath() {
			return get(ParameterEnum.ROOTPATH);
		}

//		/**
//		 * @param content
//		 * @return
//		 * @throws anifoldCFException
//		 */
//		private final static TreeSet<String> createStringSet(final String content) throws ManifoldCFException {
//			final TreeSet<String> set = new TreeSet<String>();
//			BufferedReader br = null;
//			StringReader sr = null;
//			try {
//				sr = new StringReader(content);
//				br = new BufferedReader(sr);
//				String line = null;
//				while ((line = br.readLine()) != null) {
//					line = line.trim();
//					if (line.length() > 0) {
//						set.add(line);
//					}
//				}
//				return set;
//			} catch (final IOException e) {
//				throw new ManifoldCFException(e);
//			} finally {
//				if (br != null) {
//					IOUtils.closeQuietly(br);
//				}
//			}
//		}

	}

}
