package com.cla.mcf.output.filecluster;

import com.cla.mcf.output.filecluster.entities.ClaFilePropertiesDto;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.manifoldcf.agents.interfaces.IOutputAddActivity;
import org.apache.manifoldcf.agents.interfaces.RepositoryDocument;
import org.apache.manifoldcf.core.interfaces.ManifoldCFException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 24/07/2016.
 */
public class ClaFileUtils {

    static final Logger logger = LoggerFactory.getLogger(ClaFileUtils.class);

    public static ClaFilePropertiesDto createFileProperties(RepositoryDocument document, byte[] content, Map<String, List<String>> aclsMap, Map<String, List<String>> denyAclsMap, int ingesterSignatureMaxSize) {
        ClaFilePropertiesDto claFilePropertiesDto = new ClaFilePropertiesDto();
        claFilePropertiesDto.setFileSize(content.length);
        List<String> allowedReadPrincipalNames = aclsMap.get(RepositoryDocument.SECURITY_TYPE_DOCUMENT);
        claFilePropertiesDto.setAllowedReadPrincipalsNames(allowedReadPrincipalNames);
        List<String> deniedReadPrincipalNames = denyAclsMap.get(RepositoryDocument.SECURITY_TYPE_DOCUMENT);
        claFilePropertiesDto.setDeniedReadPrincipalsNames(deniedReadPrincipalNames);
        String contentSignature= createContentSignatureIfNeeded(content, ingesterSignatureMaxSize);
        claFilePropertiesDto.setContentSignature(contentSignature);
        if (document.getCreatedDate() != null) {
            claFilePropertiesDto.setCreationTime(document.getCreatedDate().getTime());
        }
        if (document.getModifiedDate() != null) {
            claFilePropertiesDto.setModTimeMilli(document.getModifiedDate().getTime());
        }
        if (document.getAccessDate() != null) {
            claFilePropertiesDto.setAccessTimeMilli(document.getAccessDate().getTime());
        }
        return claFilePropertiesDto;
    }

    static Map<String, String[]> extractMetadata(RepositoryDocument document) {
        Map<String, String[]> result = new HashMap<>();
        Iterator<String> fieldsIterator = document.getFields();
        try {
            while (fieldsIterator.hasNext()) {
                String fieldName = fieldsIterator.next();
                String[] fieldValues = document.getFieldAsStrings(fieldName);
                result.put(fieldName, fieldValues);
            }
        } catch (IOException e) {
            logger.error("Failed to extract metadata while creating fileClusterRaw message", e);
        } catch (RuntimeException e) {
            logger.error("Failed to extract metadata while creating fileClusterRaw message", e);
        }
        return result;
    }

    protected static byte[] extractContent(RepositoryDocument document) throws IOException {
        return IOUtils.toByteArray(document.getBinaryStream());
    }

    public static void populateAclMaps(String documentURI, RepositoryDocument document, String authorityNameString, IOutputAddActivity activities, Map<String, List<String>> aclsMap, Map<String, List<String>> denyAclsMap) throws ManifoldCFException {
        Iterator<String> aclTypes = document.securityTypesIterator();
        while (aclTypes.hasNext()) {
            String aclType = aclTypes.next();
            // Reject documents that have security we don't know how to deal with in the Solr plugin!!  Only safe thing to do.
            if (!aclType.equals(RepositoryDocument.SECURITY_TYPE_DOCUMENT) &&
                    !aclType.equals(RepositoryDocument.SECURITY_TYPE_SHARE) &&
                    !aclType.startsWith(RepositoryDocument.SECURITY_TYPE_PARENT)) {
                activities.recordActivity(null, FileClusterOutputConnector.INGEST_ACTIVITY, null, documentURI, activities.UNKNOWN_SECURITY, "Solr connector rejected document that has security info which Solr does not recognize: '" + aclType + "'");
                continue;
            }

            aclsMap.put(aclType, ClaHttpPoster.convertACL(document.getSecurityACL(aclType), authorityNameString, activities));
            denyAclsMap.put(aclType, ClaHttpPoster.convertACL(document.getSecurityDenyACL(aclType), authorityNameString, activities));
        }
    }

    public static String createContentSignatureIfNeeded(byte[] content, int ingesterSignatureMaxSize) {
        if (content.length > ingesterSignatureMaxSize) {
            //Only sign up to max size
            byte[] subarray = ArrayUtils.subarray(content, 0, ingesterSignatureMaxSize);
            return sign(subarray);
        } else {
            return sign(content);
        }
    }

    public static String sign(byte[] subarray) {
        byte[] bytes = DigestUtils.sha1(subarray);
        return Base64.encodeBase64String(bytes);
        //return DigestUtils.sha256Hex(subarray); this requires 64 chars
    }

}
