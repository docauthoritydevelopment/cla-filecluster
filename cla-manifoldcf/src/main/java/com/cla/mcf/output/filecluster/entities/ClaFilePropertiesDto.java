package com.cla.mcf.output.filecluster.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

public class ClaFilePropertiesDto implements Serializable {

    private long fileSize = -1;


    Collection<String> allowedReadPrincipalsNames = new HashSet<>();
    Collection<String> deniedReadPrincipalsNames = new HashSet<>();
    Collection<String> allowedWritePrincipalsNames = new HashSet<>();
    Collection<String> deniedWritePrincipalsNames = new HashSet<>();
    private String contentSignature;

    private Long creationTime;
    private long modTimeMilli;
    private long creationTimeMilli;
    private String ownerName;

    public long getFileSize() {
        return fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getContentSignature() {
        return contentSignature;
    }

    public void setContentSignature(String contentSignature) {
        this.contentSignature = contentSignature;
    }

    public Collection<String> getAllowedReadPrincipalsNames() {
        return allowedReadPrincipalsNames;
    }

    public void setAllowedReadPrincipalsNames(Collection<String> allowedReadPrincipalsNames) {
        this.allowedReadPrincipalsNames = allowedReadPrincipalsNames;
    }

    public Collection<String> getDeniedReadPrincipalsNames() {
        return deniedReadPrincipalsNames;
    }

    public void setDeniedReadPrincipalsNames(Collection<String> deniedReadPrincipalsNames) {
        this.deniedReadPrincipalsNames = deniedReadPrincipalsNames;
    }

    public Collection<String> getAllowedWritePrincipalsNames() {
        return allowedWritePrincipalsNames;
    }

    public void setAllowedWritePrincipalsNames(Collection<String> allowedWritePrincipalsNames) {
        this.allowedWritePrincipalsNames = allowedWritePrincipalsNames;
    }

    public Collection<String> getDeniedWritePrincipalsNames() {
        return deniedWritePrincipalsNames;
    }

    public void setDeniedWritePrincipalsNames(Collection<String> deniedWritePrincipalsNames) {
        this.deniedWritePrincipalsNames = deniedWritePrincipalsNames;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public void setModTimeMilli(long modTimeMilli) {
        this.modTimeMilli = modTimeMilli;
    }

    public long getModTimeMilli() {
        return modTimeMilli;
    }

    public void setCreationTimeMilli(long creationTimeMilli) {
        this.creationTimeMilli = creationTimeMilli;
    }

    public long getCreationTimeMilli() {
        return creationTimeMilli;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerName() {
        return ownerName;
    }
}
