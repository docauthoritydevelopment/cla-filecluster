package com.cla.mcf.output.filecluster.entities;

import com.cla.mcf.output.filecluster.legacy.JsonSerislizableVisible;

import java.util.List;
import java.util.Map;

public class RawDocumentMessage implements JsonSerislizableVisible {

	private String url;
	private byte[] content;
	private Map<String, String[]> metadata;

    private ClaFilePropertiesDto claFilePropertiesDto;

	private String claJobId;
	private boolean isFileFetcher;

	public RawDocumentMessage() {
	}

	public RawDocumentMessage(final String url, final byte[] content, final Map<String, String[]> metadata) {
		this.url = url;
		this.content = content;
		this.metadata = metadata;
	}

    public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(final byte[] content) {
		this.content = content;
	}

	public Map<String, String[]> getMetadata() {
		return metadata;
	}

	public void setMetadata(final Map<String, String[]> metadata) {
		this.metadata = metadata;
	}

	public String getClaJobId() {
		return claJobId;
	}

	public void setClaJobId(final String claJobId) {
		this.claJobId = claJobId;
	}

	public boolean isFileFetcher() {
		return isFileFetcher;
	}

	public void setFileFetcher(final boolean isFileFetcher) {
		this.isFileFetcher = isFileFetcher;
	}


    @Override
    public String toString() {
        return "RawDocumentMessage{" +
                "metadata=" + metadata +
                ", url='" + url + '\'' +
                '}';
    }

    public ClaFilePropertiesDto getClaFilePropertiesDto() {
        return claFilePropertiesDto;
    }

    public void setClaFilePropertiesDto(ClaFilePropertiesDto claFilePropertiesDto) {
        this.claFilePropertiesDto = claFilePropertiesDto;
    }
}
