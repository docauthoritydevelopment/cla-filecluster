set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\solr-7.7.2
set SOLR_HOME=%DIRNAME%\solr
set SOLR_JAVA_MEM=-Xms4g -Xmx4g
REM set SOLR_PORT=8983

if not "%CUSTOM_SOLR_DATA_DIR%" == "" goto dataDirDef

if not exist "D:\Data\SolrData6" goto noData1
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData6"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData6
goto dataDirDef

:noData1
if not exist "%CLA_HOME%\data" goto noData2
echo "CUSTOM SOLR DATA DIR FOUND NEAR SOLR"
set CUSTOM_SOLR_DATA_DIR=%CLA_HOME%\data
goto dataDirDef

:noData2
if not exist "D:\Data\SolrData2" goto noData3
echo "CUSTOM SOLR DATA DIR FOUND at D:\Data\SolrData2"
set CUSTOM_SOLR_DATA_DIR=D:\Data\SolrData2
goto dataDirDef

:noData3
set CUSTOM_SOLR_DATA_DIR=%SOLR_HOME%\data
goto dataDirDef

:dataDirDef
echo %CUSTOM_SOLR_DATA_DIR%
call "%CLA_HOME%\bin\solr.cmd" start -f -p 8983 -Dsolr.data.dir="%CUSTOM_SOLR_DATA_DIR%" %1 %2 %3
