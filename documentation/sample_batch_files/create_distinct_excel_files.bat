echo off
setlocal

mkdir result

FOR /L %%A IN (1,1,999) DO (
  call :create %%A
)
goto end

:create
  echo Index %1
  set TARGETFILE="D:\Almost-Known\result\AFFINITY_%1.xlsx"
  echo copy file to %TARGETFILE%
  copy /Y D:\Almost-Known\AFFINITY.xlsx %TARGETFILE%
  echo set tag %1
  call da_office_tagging_tool.bat -op set -s %TARGETFILE% -v %1
  EXIT /b


:end