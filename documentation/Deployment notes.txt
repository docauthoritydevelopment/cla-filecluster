							DocAuthrority Deployment Notes
							==============================
															First version: V2.0, May 2017
Following are technical notes related to DA solution deployment.

1. Communication ports defaults:
	(all ports are TCP unless otherwise specified)
	Component		main ports		JMX			Other
	---------------	----------		-----		-----
	UI				9000
	AppServer		8080			7004
	MediaProcessor	8089			7044
	MySql			3306
	Solr			8983			?
	ZooKeeper		2181
	Kafka			9092			7099
	

	
2. Special environment configuration
	a. Kafka published address
		server.properties:
			advertised.host.name=<externaly accessible address>
			

	