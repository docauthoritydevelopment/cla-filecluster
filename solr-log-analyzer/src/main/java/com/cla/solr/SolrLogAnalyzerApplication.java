package com.cla.solr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SolrLogAnalyzerApplication {

    public static void main(String[] args) {
        if (args == null || args.length != 2) {
            SpringApplication.run(SolrLogAnalyzerApplication.class, args);
        } else {
            new SolrLogAnalyzer().analyze(args[0], args[1]);
        }
    }
}
