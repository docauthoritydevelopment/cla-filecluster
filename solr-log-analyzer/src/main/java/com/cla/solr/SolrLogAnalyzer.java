package com.cla.solr;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.supercsv.io.CsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

@ShellComponent()
public class SolrLogAnalyzer {

    public static final String ANALYSIS_START_TEXT = "Original document contentId=";
    public static SimpleDateFormat DATE_PATTERN = new SimpleDateFormat("dd HH:mm:ss.SSS");
    public static SimpleDateFormat FILE_TIMESTAMP_DATE_PATTERN = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");

    @ShellMethod("Analyze Solr Logs")
    public void analyze(String source, String output) {
        try {
            LogAnalysis logAnalysis = new LogAnalysis();
            Stream<Path> logFiles = Files.list(Paths.get(source));
            logFiles
                    .filter(this::isLogFile)
                    .sorted(Comparator.comparingLong(o -> o.toFile().lastModified()))
                    .forEach(lgFile -> colletLogFileDataIntoLogAnalysis(lgFile, logAnalysis));

            logAnalysis.flushResults(output);

        } catch (Throwable t) {
            throw new RuntimeException(String.format("Failed to analyze log at path %s", source), t);
        }
    }

    private boolean isLogFile(Path file) {
        return file.getFileName().toString().startsWith("solr.log");
    }

    private void colletLogFileDataIntoLogAnalysis(Path lgFile, LogAnalysis logAnalysis) {
        System.out.print(String.format("Collecting analysis data from %s ... ", lgFile));

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                new FileInputStream(lgFile.toString()), "utf-8"))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                try {
                    if (line.contains(ANALYSIS_START_TEXT)) {
                        logAnalysis.newAnalysisData(line);
                    } else if (isByThread(line)) {
                        logAnalysis.collectAnalysisData(line);
                    }
                } catch (Throwable t) {
                    System.err.println(String.format("Failed to parse line from log %s,\nline:%s\n error: %s", lgFile, line, t.getMessage()));
                }
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            System.out.println(String.format("Failed to collect analysis data from log %s,\n error: %s", lgFile, e.getMessage()));
            e.printStackTrace();
        }
        System.out.print(" DONE!");
        System.out.println();
    }

    private boolean isByThread(String line) {
        int indexOfStartBracket = line.indexOf("[");
        int indexOfEndBracket = line.indexOf("]");
        return indexOfStartBracket > -1 && indexOfEndBracket > indexOfStartBracket;
    }

    private static class LogExtraction {

        private static SimpleDateFormat LOG_DATE_PATTERN = new SimpleDateFormat("dd HH:mm:ss.SSS");

        private static String extractThread(String line) {
            return extractText(line, "[", "]");
        }

        private static long extractTime(String line) {
            try {
                int index = line.indexOf(" TRACE");

                if (index == -1) {
                    index = line.indexOf(" DEBUG");
                }

                if (index == -1) {
                    index = line.indexOf(" INFO");
                }

                String dateStr = line.substring(0, index).trim();
                return LOG_DATE_PATTERN.parse(dateStr).getTime();
            } catch (Exception e) {
                throw new RuntimeException(String.format("Failed to parse line %s", line), e);
            }
        }

        public static String extractQueryResultPvType(String line) {
            return extractText(line, "PV Type ", " origin");
        }

        public static long extractQueryResultItemCount(String line) {
            return extractLong(line, "counts ", "/");

        }

        public static long extractQueryResultItemGlobalCount(String line) {
            return extractLong(line, "/", " origin doc");
        }

        public static long extractQueryResultSize(String line) {
            return extractLong(line, "size=");
        }

        public static String extractResultFilteringPvType(String line) {
            return extractText(line, "PVQueryResult(", ") droppted");
        }

        public static long extractResultFilteringDroppedResults(String line) {
            return extractLong(line, "droppted ", " results (out");
        }

        public static long extractResultFilteringTotalResults(String line) {
            return extractLong(line, "out of ", ") of score");
        }

        public static long extractTotalCandidates(String line) {
            return extractLong(line, " - Got ", " candidate docs");
        }

        public static long extractPassedCandidates(String line) {
            return extractLong(line, "docs. ", " passed.");
        }

        public static long extractPotentialCandidates(String line) {
            return extractLong(line, "ms. Got ", " potential");
        }

        public static long extractMatchedCandidates(String line) {
            return extractLong(line, "total of ");
        }

        public static long extractNumOfRequestedDocuments(String line) {
            return extractLong(line, "asked for ", " items,");
        }

        public static long extractNumOfCacheHits(String line) {
            return extractLong(line, "items, ", " returned in");
        }

        private static Long extractContentId(String line) {
            return extractLong(line, "contentId=");
        }

        public static long extractFindDocumentsDuration(String line) {
            return extractLong(line, "returned in ", "ms");
        }

        public static long extractMatchedDuration(String line) {
            return extractLong(line, " in ", " ms");
        }

        private static String extractText(String line, String start, String end) {
            int beginIndex = line.indexOf(start) + start.length();
            String currentString = line.substring(beginIndex);
            return currentString.substring(0, currentString.indexOf(end));
        }

        private static long extractLong(String line, String start, String end) {
            int beginIndex = line.indexOf(start) + start.length();
            String currentString = line.substring(beginIndex);
            return Long.valueOf(currentString.substring(0, currentString.indexOf(end)));
        }

        private static long extractLong(String line, String start) {
            return Long.valueOf(line.substring(line.indexOf(start) + start.length()));
        }

        public static String extractSheetName(String line) {
            String restOfLine = line.substring(line.indexOf("contentId=") + "contentId=".length());
            int startOfSheetNameIndex = restOfLine.indexOf(" ");
            return restOfLine.substring(startOfSheetNameIndex + 1);
        }
    }

    private static class LogAnalysis {

        public static final String QUERY_RESULTS_TEXT = "- Query results";
        public static final String PV_QUERY_RESULTS_FILTERING_TEXT = "- PVQueryResult";
        public static final String SYMILARITY_MAPS_TEXT = "analyzeSimilarityMaps for doc";
        public static final String FIND_DOCUMENTS_TEXT = "findDocuments: asked for";
        public static final String FINISHED_HANDLE_CONTENT_ID_TEXT = "Finished handling contentId";
        public static final String CONTENT_REQUEST_END_TIME = "matched workbooks for contentId";
        private static final String START_EXCEL_ANALYZE = "Analysing ExcelFile";
        public static final String SKIP_FINALIZE_WORKBOOK_SIMILARITY_TEXT = "Skip finalize workbook similarity";
        public static final String RESOURCE_MANAGER_DONE_TIME = "Done for content id";
        public static final String RESOURCE_MANAGER_START_TIME = "Start for content id";

        Map<String, AnalysisDataList> logRawData = new HashMap<>();

        public void newAnalysisData(String line) {
            String thread = LogExtraction.extractThread(line);
            AnalysisDataList analysisDataList = logRawData.get(thread);
            if (analysisDataList == null) {
                analysisDataList = new AnalysisDataList();
                logRawData.put(thread, analysisDataList);
            }

            AnalysisData analysisData = new AnalysisData();
            analysisData.setContentId(LogExtraction.extractContentId(line));
            analysisData.setContentStartTime(LogExtraction.extractTime(line));
            analysisDataList.add(analysisData);

        }

        public void collectAnalysisData(String line) {
            String thread = LogExtraction.extractThread(line);
            AnalysisDataList analysisDataList = logRawData.get(thread);
            if (analysisDataList == null) {
                return;
            }

            AnalysisData analysisData = analysisDataList.inProcessingData();
            if (line.contains(START_EXCEL_ANALYZE)) {
                analysisData.addExcelSheetName(LogExtraction.extractSheetName(line));
                analysisData.addExcelStartTime(LogExtraction.extractTime(line));
            } else if (line.contains(QUERY_RESULTS_TEXT)) {
                analysisData.addPvQueryResults(line);
            } else if (line.contains(PV_QUERY_RESULTS_FILTERING_TEXT)) {
                analysisData.addPVQueryResultsFiltering(line);
            } else if (line.contains(SYMILARITY_MAPS_TEXT)) {
                analysisData.addConvergedDocumentsData(line);
            } else if (line.contains(FIND_DOCUMENTS_TEXT)) {
                analysisData.addFindDocuments(line);
            } else if (line.contains(FINISHED_HANDLE_CONTENT_ID_TEXT)) {
                analysisData.addFinishedHandlingAnalysis(line);
            } else if (line.contains(CONTENT_REQUEST_END_TIME) || line.contains(SKIP_FINALIZE_WORKBOOK_SIMILARITY_TEXT)) {
                analysisData.setContentEndTime(LogExtraction.extractTime(line));
            } else if (line.contains(RESOURCE_MANAGER_DONE_TIME)) {
                analysisData.addResourceManagerFetchTime(line);
            } else if (line.contains(RESOURCE_MANAGER_START_TIME)) {
                analysisData.setFinalConfirmedCandidatesAfterGroupFiltering(line);
            }
        }

        public void flushResults(String outputDirectoryPath) throws IOException {
            File outputDirectory = new File(outputDirectoryPath + "-" + FILE_TIMESTAMP_DATE_PATTERN.format(new Date()));
            outputDirectory.mkdirs();

            TreeSet<AnalysisData> sortedAnalysisData = new TreeSet<>(Comparator.comparing(AnalysisData::getContentStartTime));
            logRawData.entrySet().stream()
                    .forEach(e -> e.getValue().internalList.forEach(a -> sortedAnalysisData.add(a)));

            System.out.println(String.format("Flushing analysis results to %s", outputDirectory));
            flushSolrPotentialVsConverged(outputDirectory, sortedAnalysisData);
            flushConvergedToMatched(outputDirectory, sortedAnalysisData);
            flushPvQueryResultTime(outputDirectory, sortedAnalysisData);
            flushPvQueryResultsFiltering(outputDirectory, sortedAnalysisData);
            flushFindDocumentDataTime(outputDirectory, sortedAnalysisData);
            flushFetchAnalysisDataTime(outputDirectory, sortedAnalysisData);
            flushTotalExcelResultTime(outputDirectory, sortedAnalysisData);
            flushTotalWordResultTime(outputDirectory, sortedAnalysisData);
            flushSummary(outputDirectory, sortedAnalysisData);

        }

        private void flushSummary(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String fileAnalysisName = "Summary.csv";
            System.out.print(String.format("Preparing %s ...", fileAnalysisName));
            File file = new File(outputDirectory, fileAnalysisName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write(
                        "Content Id",
                        "Content Type",
                        "Number of Sheets",
                        "Solr Potential Candidates",
                        "Ph1 disqualified",
                        "Ph1 disqualified %",
                        "Confirmed candidates",
                        "Ph1' Disqualified",
                        "Ph1' Disqualified %",
                        "Candidates After Group Filtering",
                        "Ph2 Disqualified",
                        "Ph2 Disqualified %",
                        "Final Matched Candidates",
                        "Total Duration (MS)",
                        "Solr Total Query Time (MS)",
                        "Solr Total Item Count (MS)",
                        "Solr Max Query Time (MS)",
                        "Solr Max Query Time PV Type",
                        "Solr Max Query Time Item Count (MS)",
                        "Confirmed Candidates Fetch & Analysis Time (MS)",
                        "Matched Candidate Vs Solr Potential Candidate",
                        "Matched Candidate vs Ph1",
                        "Matched Candidate vs Ph1'");
                sortedAnalysisData.stream().forEach(ad -> {
                    try {
                        flushRow(csvListWriter, ad);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(" DONE!");
            System.out.println();
        }

        private void flushFetchAnalysisDataTime(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String fileAnalysisName = "resourceManagerTime.csv";
            System.out.print(String.format("Preparing %s ...", fileAnalysisName));
            File file = new File(outputDirectory, fileAnalysisName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "# of fetched Analysis Data", "Fetch Duration", "Ratio");
                final long[] totalDuration = {0};
                final long[] count = {0};
                sortedAnalysisData.stream().filter(ad -> ad.fetchedAnalysisDataDuration >= 0).forEach(ad -> {
                    totalDuration[0] += ad.fetchedAnalysisDataDuration;
                    count[0] += ad.numOfFetchAnalysisData;
                    try {
                        csvListWriter.write(
                                DATE_PATTERN.format(new Date(ad.fetchAnalysisDataLogTime)),
                                ad.contentId,
                                ad.numOfFetchAnalysisData,
                                ad.fetchedAnalysisDataDuration,
                                ad.fetchedAnalysisDataDuration == 0 ? 0 : String.format("%.2f", (double) ad.fetchedAnalysisDataDuration / ad.numOfFetchAnalysisData));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                csvListWriter.write("Avg duration", (double) totalDuration[0] / count[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(" DONE!");
            System.out.println();
        }

        private void flushFindDocumentDataTime(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String fileAnalysisName = "FindPvs.csv";
            System.out.print(String.format("Preparing %s ...", fileAnalysisName));
            File file = new File(outputDirectory, fileAnalysisName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "Requested", "Cache Hits", "Misses", "Success Ratio", "Actual Results", "Query Time");
                final long[] totalDuration = {0};
                final long[] count = {0};
                sortedAnalysisData.stream().forEach(ad -> {
                    final int[] index = {0};
                    ad.getFindDocumentDataList()
                            .forEach(fd -> {
                                try {
                                    final long[] startTime = {0};
                                    final ConvergedDocumentsData[] convergedDocumentsData = {null};
                                    ad.getConvergedDocumentsDataList().subList(index[0], ad.getConvergedDocumentsDataList().size())
                                            .forEach(cd -> {
                                                if (cd.time >= startTime[0] && cd.time <= fd.time) {
                                                    startTime[0] = cd.time;
                                                    convergedDocumentsData[0] = cd;
                                                    index[0]++;
                                                }
                                            });
                                    totalDuration[0] += fd.duration;
                                    count[0] += fd.numOfRequestedDocuments;

                                    if (convergedDocumentsData[0] == null ||
                                            convergedDocumentsData[0].passedCandidates - fd.numOfRequestedDocuments < 0) {
                                        return;
                                    }
                                    long numOfHits = convergedDocumentsData[0].passedCandidates - fd.numOfRequestedDocuments;
                                    long misses = convergedDocumentsData[0].passedCandidates - numOfHits;
                                    double successRatio = (double) numOfHits / convergedDocumentsData[0].passedCandidates;
                                    csvListWriter.write(
                                            DATE_PATTERN.format(new Date(fd.time)),
                                            ad.contentId,
                                            convergedDocumentsData[0].passedCandidates,
                                            numOfHits,
                                            misses,
                                            successRatio,
                                            fd.numOfActualResults,
                                            fd.duration
                                    );
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                });
                csvListWriter.write("Avg duration", (double) totalDuration[0] / count[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(" DONE!");
            System.out.println();
        }

        private void flushPvQueryResultsFiltering(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String analysisFileName = "PvQueryResultsFilteringTimes.csv";
            System.out.print(String.format("Preparing %s ...", analysisFileName));
            File file = new File(outputDirectory, analysisFileName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "Time", "Results filtered", "Ratio");
                final int[] count = {0};
                final long[] totalDuration = {0};
                sortedAnalysisData.stream()
                        .filter(ad -> !ad.getPvQueryResultList().isEmpty() && !ad.getPvQueryResultsFilteringList().isEmpty())
                        .forEach(ad -> {
                            PvQueryResult lastPvQueryResult = ad.getPvQueryResultList().get(ad.getPvQueryResultList().size() - 1);
                            long start = lastPvQueryResult.time;
                            PvQueryResultFiltering lastFilteringResult = ad.getPvQueryResultsFilteringList().get(ad.getPvQueryResultsFilteringList().size() - 1);
                            long end = lastFilteringResult.time;
                            if (end - start < 0) {
                                return;
                            }
                            ad.getPvQueryResultList()
                                    .forEach(pvr -> {
                                        long duration = end - start;
                                        totalDuration[0] += duration;
                                        count[0] += ad.getPvQueryResultsFilteringList().size();
                                        try {
                                            csvListWriter.write(
                                                    DATE_PATTERN.format(new Date(pvr.time)),
                                                    ad.contentId,
                                                    duration,
                                                    ad.getPvQueryResultsFilteringList().size(),
                                                    (double) duration / ad.getPvQueryResultsFilteringList().size());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    });
                        });
                csvListWriter.write("Avg duration:", count[0] == 0 ? 0 : (double) (totalDuration[0] / count[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print("DONE!");
            System.out.println();
        }

        private void flushTotalWordResultTime(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String fileAnalysisName = "WordTotalAnalysisTime.csv";
            System.out.print(String.format("Preparing %s ...", fileAnalysisName));
            File file = new File(outputDirectory, fileAnalysisName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "Time");
                final long[] totalDuration = {0};
                final long[] count = {0};
                sortedAnalysisData.stream().filter(ad -> ad.getExcelStartTimeList().isEmpty())
                        .forEach(ad -> {
                            try {
                                long duration = ad.getFinishedAnalysisDataList().isEmpty() ? 0 // Something went wrong, should not get here
                                        : ad.getFinishedAnalysisDataList().iterator().next().time - ad.contentStartTime;
                                ad.wordDuration = duration;
                                if (duration < 0) { // Something went wrong, should not get here
                                    return;
                                }
                                totalDuration[0] += duration;
                                count[0]++;
                                csvListWriter.write(DATE_PATTERN.format(new Date(ad.contentStartTime)), ad.contentId, duration);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                csvListWriter.write("Avg duration", (double) totalDuration[0] / count[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(" DONE!");
            System.out.println();
        }

        private void flushTotalExcelResultTime(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String fileAnalysisName = "ExcelTotalAnalysisTime.csv";
            System.out.print(String.format("Preparing %s ...", fileAnalysisName));
            File file = new File(outputDirectory, fileAnalysisName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "Time", "Number of sheets", "Avg time per sheet");
                final long[] totalDuration = {0};
                final long[] count = {0};
                sortedAnalysisData.stream().filter(ad -> !ad.getExcelStartTimeList().isEmpty())
                        .forEach(ad -> {
                            try {
                                long duration = ad.contentEndTime - ad.contentStartTime;
                                if (duration < 0) {
                                    return;
                                }
                                totalDuration[0] += duration;
                                count[0] += ad.getExcelStartTimeList().size();
                                csvListWriter.write(
                                        DATE_PATTERN.format(new Date(ad.contentStartTime)),
                                        ad.contentId,
                                        duration,
                                        ad.getExcelStartTimeList().size(),
                                        (double) duration / ad.getExcelStartTimeList().size());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                csvListWriter.write("Avg duration", (double) totalDuration[0] / count[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(" DONE!");
            System.out.println();
        }

        private void flushConvergedToMatched(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String fileAnalysisName = "ConvergedVsMatched.csv";
            System.out.print(String.format("Preparing %s ...", fileAnalysisName));
            File file = new File(outputDirectory, fileAnalysisName);
            final double[] totalRatio = {0};
            final int[] totalCount = {0};
            final long[] totalDuration = {0};
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "Converged", "Matched", "Ratio", "Matched duration");
                sortedAnalysisData.forEach(ad -> ad.getFinishedAnalysisDataList()
                        .forEach(fad -> {
                            try {
                                if (fad.potentialCandidates == 0) {
                                    return;
                                }
                                double ratio = (double) fad.matchedCandidates / (double) fad.potentialCandidates;
                                totalRatio[0] += ratio;
                                totalCount[0]++;
                                totalDuration[0] += fad.duration;
                                csvListWriter.write(
                                        DATE_PATTERN.format(new Date(fad.time)),
                                        ad.contentId,
                                        fad.potentialCandidates,
                                        fad.matchedCandidates,
                                        ratio,
                                        fad.duration
                                );
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }));
                csvListWriter.write("Avg ratio", totalRatio[0] / totalCount[0]);
                csvListWriter.write("Avg duration:", (double) totalDuration[0] / totalCount[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(" DONE!");
            System.out.println();
        }

        private void flushSolrPotentialVsConverged(File outputDirectory, Collection<AnalysisData> sortedAnalysisDataList) throws IOException {
            String analysisFileName = "SolrVsActual.csv";
            System.out.print(String.format("Preparing %s ...", analysisFileName));
            File file = new File(outputDirectory, analysisFileName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "contentId", "Solr Potential", "Converged", "Ratio", "Convergence Duration");
                final double[] totalRatio = {0};
                final int[] totalCount = {0};
                final long[] totalDuration = {0};

                sortedAnalysisDataList.forEach(ad -> {
                    final int[] index = {0};
                    ad.getConvergedDocumentsDataList()
                            .forEach(cd -> {
                                try {
                                    if (cd.totalCandidates == 0) {
                                        return;
                                    }
                                    double ratio = (double) cd.passedCandidates / (double) cd.totalCandidates;
                                    totalRatio[0] += ratio;
                                    totalCount[0]++;
                                    long duration = 0;

                                    if (!ad.getPvQueryResultsFilteringList().isEmpty()) {
                                        final long[] startTime = {0};
                                        ad.getPvQueryResultsFilteringList().subList(index[0], ad.getPvQueryResultsFilteringList().size())
                                                .forEach(pvrf -> {
                                                    if (pvrf.time >= startTime[0] && pvrf.time <= cd.time) {
                                                        startTime[0] = pvrf.time;
                                                        index[0]++;
                                                    }
                                                });
                                        if (startTime[0] == 0) {
                                            ad.getPvQueryResultList().forEach(pvr -> {
                                                if (pvr.time >= startTime[0] && pvr.time <= cd.time) {
                                                    startTime[0] = pvr.time;
                                                }
                                            });
                                        }
                                        if (startTime[0] == 0) {
                                            startTime[0] = ad.contentStartTime;
                                        }
                                        duration = cd.time - startTime[0];
                                        totalDuration[0] += duration;
                                    }
                                    csvListWriter.write(
                                            DATE_PATTERN.format(new Date(cd.time)),
                                            ad.contentId,
                                            cd.totalCandidates,
                                            cd.passedCandidates,
                                            ratio,
                                            duration
                                    );
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                });
                csvListWriter.write("Avg ratio:", totalRatio[0] / totalCount[0]);
                csvListWriter.write("Avg Convergence duration:", totalCount[0] == 0 ? 0 : totalDuration[0] / totalCount[0]);
            }
            System.out.print("DONE!");
            System.out.println();
        }

        private void flushPvQueryResultTime(File outputDirectory, TreeSet<AnalysisData> sortedAnalysisData) {
            String analysisFileName = "PvQueryTimes.csv";
            System.out.print(String.format("Preparing %s ...", analysisFileName));
            File file = new File(outputDirectory, analysisFileName);
            try (CsvListWriter csvListWriter = new CsvListWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE)) {
                csvListWriter.write("Log Time", "Content Id", "Sheet", "Time", "Item Count", "Global Count", "Num of Results", "Pv Type", "Ratio");
                final int[] count = {0};
                final long[] totalDuration = {0};
                sortedAnalysisData.forEach(ad -> {
                    final long[] initialTime = {ad.contentStartTime};
                    Set<String> pvTypes = new HashSet<>();
                    int[] index = {0};
                    ad.getPvQueryResultList()
                            .forEach(pvr -> {
                                if (pvTypes.contains(pvr.pvType)) {
                                    pvTypes.clear();
                                    index[0]++;
                                }
                                pvTypes.add(pvr.pvType);
                                String sheetName = "!!WORD DOCUMENT!!";
                                if (index[0] < ad.getExcelSheets().size()) {
                                    sheetName = ad.getExcelSheets().get(index[0]);
                                }
                                long duration = pvr.time - initialTime[0];
                                pvr.duration = duration;
                                totalDuration[0] += duration;
                                count[0]++;
                                try {
                                    csvListWriter.write(
                                            DATE_PATTERN.format(new Date(pvr.time)),
                                            ad.contentId,
                                            sheetName,
                                            duration,
                                            pvr.count,
                                            pvr.globalCount,
                                            pvr.resultSize,
                                            pvr.pvType,
                                            (double) duration / pvr.resultSize);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                initialTime[0] = pvr.time;
                            });

                });
                csvListWriter.write("Avg time:", count[0] == 0 ? 0 : (double) (totalDuration[0] / count[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print("DONE!");
            System.out.println();
        }
    }

    private static void flushRow(CsvListWriter csvListWriter, AnalysisData ad) throws IOException {
        Long contentId = ad.contentId;
        String contentType = ad.getExcelSheets().isEmpty() ? "WORD" : "EXCEL";
        Integer numOfSheets = ad.getExcelSheets().size();
        Long totalDuration = contentType.equals("WORD") ? ad.wordDuration : ad.contentEndTime - ad.contentStartTime;
        Long solrPotentialCandidates = ad.getConvergedDocumentsDataList().stream().mapToLong(s -> s.totalCandidates).sum();
        PvQueryResult maxPvr = ad.getPvQueryResultList().isEmpty() ? null : Collections.max(ad.getPvQueryResultList(), Comparator.comparingLong(o -> o.duration));
        if (maxPvr == null) {
            return;
        }
        Long solrMaxPvrTime = maxPvr.duration;
        Long solrMaxItemCount = maxPvr.count;
        String solrMaxPvrType = maxPvr.pvType;
        if (ad.getPvQueryResultList() == null) {
            return;
        }
        Long solrPvQueryTime = ad.getPvQueryResultList().stream().mapToLong(pvr -> pvr.duration).sum();
        Long pvTotalItemCount = ad.getPvQueryResultList().stream().mapToLong(pvr -> pvr.count).sum();
        Long confirmedCandidates = ad.getConvergedDocumentsDataList().stream().mapToLong(s -> s.passedCandidates).sum();
        if (confirmedCandidates == null) {
            return;
        }
        Long afterGroupFiltering = ad.finalConfirmedCandidatesAfterGroupFiltering;
        if (afterGroupFiltering == null) {
            return;
        }

        Long ph1Disqualified = solrPotentialCandidates - confirmedCandidates;
        String ph1DisqualifiedPercentage = String.format("%.2f", ((double) ph1Disqualified / (double) solrPotentialCandidates) * 100);
        Long ph1TagDisqualified = confirmedCandidates - afterGroupFiltering;
        String ph1TagDisqualifiedPercentage = String.format("%.2f", ((double) ph1TagDisqualified / (double) confirmedCandidates) * 100);
        Long fetchAnalysisDataTime = ad.fetchedAnalysisDataDuration;
        Long totalMatchedDocuments = ad.numOfFetchAnalysisData;
        Long ph2Disqualified = afterGroupFiltering - totalMatchedDocuments;
        String ph2DisqualifiedPercentage = String.format("%.2f", ((double) ph2Disqualified / (double) confirmedCandidates) * 100);

        String pvqVsFinalMatchRatio = String.format("%.2f", ((double) totalMatchedDocuments / (double) solrPotentialCandidates) * 100);
        String pvqPh1VsFinalMatchRatio = String.format("%.2f", ((double) totalMatchedDocuments / (double) confirmedCandidates) * 100);
        String pvqPh2VsFinalMatchRatio = String.format("%.2f", ((double) totalMatchedDocuments / (double) afterGroupFiltering) * 100);
        csvListWriter.write(contentId,
                contentType,
                numOfSheets,
                solrPotentialCandidates,
                ph1Disqualified,
                ph1DisqualifiedPercentage,
                confirmedCandidates,
                ph1TagDisqualified,
                ph1TagDisqualifiedPercentage,
                afterGroupFiltering,
                ph2Disqualified,
                ph2DisqualifiedPercentage,
                totalMatchedDocuments,
                totalDuration,
                solrPvQueryTime,
                pvTotalItemCount,
                solrMaxPvrTime,
                solrMaxPvrType,
                solrMaxItemCount,
                fetchAnalysisDataTime,
                pvqVsFinalMatchRatio,
                pvqPh1VsFinalMatchRatio,
                pvqPh2VsFinalMatchRatio);
    }


    private static class AnalysisData {

        public long wordDuration;
        private List<Long> excelStartTimeList = new LinkedList<>();
        private List<PvQueryResult> pvQueryResultList = new LinkedList<>();
        private List<PvQueryResultFiltering> pvQueryResultsFilteringList = new LinkedList<>();
        private List<ConvergedDocumentsData> convergedDocumentsDataList = new LinkedList<>();
        private List<FinishedAnalysisData> finishedAnalysisDataList = new LinkedList<>();
        private List<FindDocumentData> findDocumentDataList = new LinkedList<>();
        private List<String> excelSheets = new ArrayList<>();
        private Long contentStartTime;
        private Long contentId;
        private long contentEndTime;
        private Long numOfFetchAnalysisData = -1L;
        private Long fetchedAnalysisDataDuration = -1L;
        private long fetchAnalysisDataLogTime;
        private Long finalConfirmedCandidatesAfterGroupFiltering;


        public void addExcelStartTime(long startTime) {
            excelStartTimeList.add(startTime);
        }

        public void addPvQueryResults(String line) {
            long time = LogExtraction.extractTime(line);
            String pvType = LogExtraction.extractQueryResultPvType(line);
            long count = LogExtraction.extractQueryResultItemCount(line);
            long globalCount = LogExtraction.extractQueryResultItemGlobalCount(line);
            long resultSize = LogExtraction.extractQueryResultSize(line);
            pvQueryResultList.add(new PvQueryResult(time, pvType, count, globalCount, resultSize));


        }

        public void addPVQueryResultsFiltering(String line) {
            long time = LogExtraction.extractTime(line);
            String filteringPvType = LogExtraction.extractResultFilteringPvType(line);
            long droppedResults = LogExtraction.extractResultFilteringDroppedResults(line);
            long totalResults = LogExtraction.extractResultFilteringTotalResults(line);
            pvQueryResultsFilteringList.add(new PvQueryResultFiltering(time, filteringPvType, droppedResults, totalResults));
        }

        public void addConvergedDocumentsData(String line) {
            long time = LogExtraction.extractTime(line);
            long totalCandidates = LogExtraction.extractTotalCandidates(line);
            long passedCandidates = LogExtraction.extractPassedCandidates(line);
            this.convergedDocumentsDataList.add(new ConvergedDocumentsData(time, totalCandidates, passedCandidates));
        }

        public void addFindDocuments(String line) {
            long time = LogExtraction.extractTime(line);
            long numOfRequestedDocuments = LogExtraction.extractNumOfRequestedDocuments(line);
            long numOfCacheHits = LogExtraction.extractNumOfCacheHits(line);
            long duration = LogExtraction.extractFindDocumentsDuration(line);
            this.findDocumentDataList.add(new FindDocumentData(time, numOfRequestedDocuments, numOfCacheHits, duration));
        }

        public void addFinishedHandlingAnalysis(String line) {
            long time = LogExtraction.extractTime(line);
            long potentialCandidates = LogExtraction.extractPotentialCandidates(line);
            long matchedCandidates = LogExtraction.extractMatchedCandidates(line);
            long duration = LogExtraction.extractMatchedDuration(line);
            this.finishedAnalysisDataList.add(new FinishedAnalysisData(time, potentialCandidates, matchedCandidates, duration));
        }

        public List<ConvergedDocumentsData> getConvergedDocumentsDataList() {
            return convergedDocumentsDataList;
        }

        public List<FinishedAnalysisData> getFinishedAnalysisDataList() {
            return finishedAnalysisDataList;
        }

        public List<PvQueryResult> getPvQueryResultList() {
            return pvQueryResultList;
        }

        public List<PvQueryResultFiltering> getPvQueryResultsFilteringList() {
            return pvQueryResultsFilteringList;
        }

        public List<FindDocumentData> getFindDocumentDataList() {
            return findDocumentDataList;
        }

        public void setContentStartTime(Long contentStartTime) {
            this.contentStartTime = contentStartTime;
        }

        public Long getContentStartTime() {
            return contentStartTime;
        }

        public void setContentId(Long contentId) {
            this.contentId = contentId;
        }

        public Long getContentId() {
            return contentId;
        }

        public void setContentEndTime(long contentEndTime) {
            this.contentEndTime = contentEndTime;
        }

        public long getContentEndTime() {
            return contentEndTime;
        }

        public List<Long> getExcelStartTimeList() {
            return excelStartTimeList;
        }

        public void addExcelSheetName(String sheetName) {
            excelSheets.add(sheetName);
        }

        public List<String> getExcelSheets() {
            return excelSheets;
        }

        public void addResourceManagerFetchTime(String line) {
            Matcher matcher = Pattern.compile("fetchAndExtract process of ([0-9]+).*in ([0-9]+)").matcher(line);
            boolean found = matcher.find();
            if (!found) {
                System.err.println(String.format("Did not find fetched analysis data on line %s", line));
                return;
            }
            this.numOfFetchAnalysisData = Long.valueOf(matcher.group(1));
            this.fetchedAnalysisDataDuration = Long.valueOf(matcher.group(2));
            this.fetchAnalysisDataLogTime = LogExtraction.extractTime(line);
        }

        public void setFinalConfirmedCandidatesAfterGroupFiltering(String line) {
            Matcher matcher = Pattern.compile("Start for content id ([0-9]+).*fetchAndExtract process of ([0-9]+)").matcher(line);
            boolean found = matcher.find();
            if (!found) {
                System.err.println(String.format("Did not find fetched analysis data on line %s", line));
                return;
            }
            this.finalConfirmedCandidatesAfterGroupFiltering = Long.valueOf(matcher.group(2));
        }
    }

    private static class FindDocumentData {

        private final long time;
        private final long numOfRequestedDocuments;
        private final long numOfActualResults;
        private final long duration;

        public FindDocumentData(long time, long numOfRequestedDocuments, long numOfActualResults, long duration) {
            this.time = time;
            this.numOfRequestedDocuments = numOfRequestedDocuments;
            this.numOfActualResults = numOfActualResults;
            this.duration = duration;
        }
    }

    private static class FinishedAnalysisData {

        private final long time;
        private final long potentialCandidates;
        private final long matchedCandidates;
        public final long duration;

        public FinishedAnalysisData(long time, long potentialCandidates, long matchedCandidates, long duration) {
            this.time = time;
            this.potentialCandidates = potentialCandidates;
            this.matchedCandidates = matchedCandidates;
            this.duration = duration;
        }
    }

    private static class ConvergedDocumentsData {

        private final long time;
        private final long totalCandidates;
        private final long passedCandidates;

        public ConvergedDocumentsData(long time, long totalCandidates, long passedCandidates) {
            this.time = time;
            this.totalCandidates = totalCandidates;
            this.passedCandidates = passedCandidates;
        }
    }

    private static class PvQueryResultFiltering {

        private final long time;
        private final String filteringPvType;
        private final long droppedResults;
        private final long totalResults;

        public PvQueryResultFiltering(long time, String filteringPvType, long droppedResults, long totalResults) {
            this.time = time;
            this.filteringPvType = filteringPvType;
            this.droppedResults = droppedResults;
            this.totalResults = totalResults;
        }
    }

    private static class PvQueryResult {

        private final long time;
        private final String pvType;
        private final long count;
        private final long globalCount;
        private final long resultSize;
        public long duration;


        public PvQueryResult(long time, String pvType, long count, long globalCount, long resultSize) {
            this.time = time;
            this.pvType = pvType;
            this.count = count;
            this.globalCount = globalCount;
            this.resultSize = resultSize;
        }

        public long getTime() {
            return time;
        }

        public String getPvType() {
            return pvType;
        }

        public long getCount() {
            return count;
        }

        public long getGlobalCount() {
            return globalCount;
        }

        public long getResultSize() {
            return resultSize;
        }
    }


    private static class AnalysisDataList {

        private List<AnalysisData> internalList = new LinkedList<>();
        private AnalysisData inProcessingData;

        public void add(AnalysisData analysisData) {
            internalList.add(analysisData);
            this.inProcessingData = analysisData;
        }

        public AnalysisData inProcessingData() {
            return inProcessingData;
        }
    }
}
