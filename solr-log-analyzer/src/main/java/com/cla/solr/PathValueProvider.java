package com.cla.solr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.shell.CompletionContext;
import org.springframework.shell.CompletionProposal;
import org.springframework.shell.standard.ValueProvider;
import org.springframework.shell.standard.ValueProviderSupport;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
class PathValueProvider extends ValueProviderSupport{

    private static final Logger logger = LoggerFactory.getLogger(PathValueProvider.class);

    @Override
    public List<CompletionProposal> complete(MethodParameter parameter,
                                             CompletionContext completionContext,
                                             String[] hints) {
        String currentInput = completionContext.currentWordUpToCursor();
        try {
            Path path = Paths.get(currentInput);
            if (Files.exists(path)) {
                return Files.list(path)
                        .map(p -> p.getFileName().toString())
                        .map(CompletionProposal::new)
                        .collect(Collectors.toList());
            }
        } catch (Throwable t) {
            logger.debug(t.getMessage());
        }
        return new LinkedList<>();
    }
}