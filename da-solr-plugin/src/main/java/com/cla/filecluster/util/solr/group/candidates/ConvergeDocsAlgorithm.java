package com.cla.filecluster.util.solr.group.candidates;

import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.pv.PropertyVectorKey;
import com.cla.filecluster.repository.solr.ClaGroupingParams;
import com.cla.filecluster.service.files.pv.MaturityHelper;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.cla.filecluster.util.solr.group.GroupingType;
import com.cla.filecluster.util.solr.group.pv.PVQueryResult;
import com.cla.filecluster.util.solr.group.pv.PvQueryRequestParams;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.cla.filecluster.util.solr.similarity.ClaSimilaritySearcher;
import com.google.common.base.Strings;
import org.apache.lucene.search.Query;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.ObjectReleaseTracker;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Algorithm implementation: Find group candidates by looking at all the property vectors and converging the results
 * Created by uri on 05/02/2017.
 */
public class ConvergeDocsAlgorithm {

    private Map<Integer, List<Integer>> debFiles = null;

    private static Logger logger = LoggerFactory.getLogger(ConvergeDocsAlgorithm.class);

    private static final int MAX_QUERIES = 10;

    private GroupingType groupingType;

    private Properties[] thresholdsByMl;

    private int maxPageSize;
    private Float cutOffValue;

    private volatile int[] lastQueryGroupCount = new int[MAX_QUERIES];

    private Set<Integer> skippedAnalysisPvsNames = null;

    public ConvergeDocsAlgorithm(NamedList debugInfoParams,
                                 GroupingType groupingType,
                                 Properties[] thresholdsByMl,
                                 int maxPageSize, String cutOffValueStr,
                                 String skipAnalyzedPvsByName) {
        this.groupingType = groupingType;
        this.thresholdsByMl = thresholdsByMl;
        this.maxPageSize = maxPageSize;
        try {
            this.cutOffValue = Float.valueOf(Optional.ofNullable(cutOffValueStr).orElse("-1"));
        } catch (NumberFormatException e) {
            logger.warn("cutOffValue not a valid number: {}", cutOffValueStr);
            this.cutOffValue = -1f;
        }
        fillSkippedPvNames(skipAnalyzedPvsByName);
        if (debugInfoParams != null) {
            for (int i = 0; i < debugInfoParams.size(); i++) {
                initDebugInfo(debugInfoParams.getName(i), debugInfoParams.getVal(i));
            }
        }
    }

    public int[] getConvergedDocIds(SolrQueryRequest req, SolrParams params, SolrPvDocument originalDocument, MaturityHelper maturityHelper, boolean testDropThreshold) throws IOException {
        if (originalDocument == null) {
            throw new RuntimeException("OriginalDocument object must not be null");
        }
        Map<String, ClaHistogramCollection<Long>> pvCollections = null;
        if (originalDocument.getPvAnalysisData() != null) {
            ClaSuperCollection<Long> defaultPvCollection = originalDocument.getPvAnalysisData().getDefaultPvCollection();
            if (defaultPvCollection == null) {
                logger.warn("Missing defaultPvCollection on contentId: {} pvContentId={} parts={}",
                        originalDocument.getId(), originalDocument.getPvAnalysisData().getContentId(),
                        originalDocument.getPvAnalysisData().getPvCollections() == null ? "null-collections" :
                                originalDocument.getPvAnalysisData().getPvCollections().getParts()
                                        .stream().collect(Collectors.joining(",")));
                throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Missing defaultPvCollection on contentId: " + originalDocument.getId());
            }
            pvCollections = defaultPvCollection.getCollections();
        }
        int partMaturityLevel = maturityHelper.getPartMaturityLevel(pvCollections);

        return getConvergedDocIds(req, params, originalDocument,"", pvCollections, partMaturityLevel, testDropThreshold);
    }

    public int[] getConvergedDocIds(SolrQueryRequest req, SolrParams params, SolrPvDocument originalDocument,
                                    String sheet, Map<String, ClaHistogramCollection<Long>> pvCollections, int partMaturityLevel, boolean testDropThreshold) throws IOException {
        ClaSimilaritySearcher searcher = new ClaSimilaritySearcher(req.getSearcher());
        try {
            String defType = QParserPlugin.DEFAULT_QTYPE;
            //Handle FilterQuery
            List<Query> requestFilters = copyRequestFilters(req);

            translatedDebFiles(req, searcher, defType);

            ArrayList<PVQueryResult> pvQueryResults = new ArrayList<>();
            if (pvCollections != null) {
                createAndExecuteSubQueries(req, searcher, defType, requestFilters, pvQueryResults, originalDocument, sheet, pvCollections, partMaturityLevel);
            } else if (requestFilters != null) {
                //We support up to MAX_QUERIES queries in our convergence mechanism
                logger.trace("Running #{} extract and execute sub queries ({} requestFilters) ML={}", MAX_QUERIES, requestFilters.size(), partMaturityLevel);
                for (int i = 0; i < MAX_QUERIES; i++) {
                    extractAndExecuteSubQuery(req, params, searcher, defType, requestFilters, pvQueryResults, originalDocument, sheet, partMaturityLevel, i);
                }
            } else {
                logger.debug("Running #{} extract and execute sub queries (requestFilters=null) ML={}", MAX_QUERIES, partMaturityLevel);
            }
            // This should be part (sheet) maturity based value (not the file-level maturity)
//        Properties customThresholdsContent = getCustomThresholds(params, originalDocument.getOriginMaturityLevel(), true);
//        Properties customThresholdsStyle = getCustomThresholds(params, originalDocument.getOriginMaturityLevel(), false);
            Properties customThresholdsContent = getCustomThresholds(params, partMaturityLevel, true);
            Properties customThresholdsStyle = getCustomThresholds(params, partMaturityLevel, false);

            //Converge (group)
            BaseGroupingAlgorithm groupingAlgorithm = createGroupingAlgorithm(pvQueryResults, originalDocument,
                    customThresholdsContent, customThresholdsStyle,
                    debFiles, testDropThreshold);
            return groupingAlgorithm.executeAlgorithm(searcher);
        } finally {
            ObjectReleaseTracker.release(searcher);
            ClaSimilaritySearcher.numOpens.decrementAndGet();
        }
    }


    private List<Query> copyRequestFilters(SolrQueryRequest req) {
        List<Query> filters = null;

        try {
            String contentId = req.getParams().get(ClaGroupingParams.OD);
            String[] fqs = req.getParams().getParams(CommonParams.FQ);
            if (fqs != null && fqs.length != 0) {
                filters = new ArrayList<>();
                for (String fq : fqs) {
                    if (fq != null && fq.trim().length() != 0) {
                        QParser fqp = QParser.getParser(fq, null, req);
                        filters.add(fqp.getQuery());
                        logger.trace("For contentId: {}, added filter query string {}", contentId, fq);
                    }
                }
            }
        } catch (SyntaxError syntaxError) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, syntaxError);
        }
        return filters;
    }

    private void translatedDebFiles(SolrQueryRequest req, SolrIndexSearcher searcher, String defType) {
        if (debFiles != null) {
            Set<Integer> ks = new HashSet<>(debFiles.keySet().size());
            ks.addAll(debFiles.keySet());
            for (Integer k : ks) {
                List<Integer> internalIds = fileId2Internal(k, req, searcher, defType);
                if (internalIds != null) {
                    debFiles.put(k, internalIds);
                }
            }
        }
    }

    private void initDebugInfo(String name, Object item) {
        if (name.startsWith("fileId")) {
            if (debFiles == null) {
                debFiles = new HashMap<>();
            }
            debFiles.put(Integer.valueOf(item.toString()), null);
            logger.debug("debugInfo {}:{} ({})", name, item.toString(), debFiles.size());
        }
    }

    private List<Integer> fileId2Internal(Integer fileId, SolrQueryRequest req, SolrIndexSearcher searcher, String defType) {
        if (fileId == null) {
            return null;
        }
        try {
            QParser parser = QParser.getParser("fileId:" + fileId.toString(), defType, req);
            Query query = parser.getQuery();
            DocList match = searcher.getDocList(query, null, null, 0, 99, 0);    // support up to 99 parts
            if (match.size() != 1) {
                logger.debug("Document ID converter: cound not convert {}. Got {} results", fileId, match.size());
                return null;
            }
            List<Integer> res = new ArrayList<>();
            DocIterator iter = match.iterator();
            while (iter.hasNext()) {
                res.add(iter.next());
            }
            logger.debug("Document ID converter: {}->{}", fileId, res.toString());
            return res;
        } catch (Exception e) {
            logger.warn("Exception during fileId conversion {}. e={}", fileId, e, e);
            return null;
        }
    }

    private void createAndExecuteSubQueries(SolrQueryRequest req,
                                            ClaSimilaritySearcher searcher,
                                            String defType,
                                            List<Query> requestFilters,
                                            ArrayList<PVQueryResult> pvQueryResults,
                                            SolrPvDocument originalDocument,
                                            String sheet, Map<String, ClaHistogramCollection<Long>> collections,
                                            int partMaturityLevel) throws IOException {

        boolean forceMaturityLevelSimilarity = originalDocument.isForceMaturityLevelSimilarity();
        logger.trace("create and execute sub queries ({} requestFilters) ML={} forceML={}",
                requestFilters.size(), partMaturityLevel, forceMaturityLevelSimilarity ? "true" : "false");
//        Integer originMaturityLevel = originalDocument.getOriginMaturityLevel();
        int flags = SolrIndexSearcher.GET_SCORES;

        executeSubQueries(req, searcher, defType, requestFilters, pvQueryResults,
                originalDocument, sheet,collections, partMaturityLevel, forceMaturityLevelSimilarity, flags);

    }

    private void executeSubQueries(SolrQueryRequest req,
                                   ClaSimilaritySearcher searcher,
                                   String defType,
                                   List<Query> requestFilters,
                                   ArrayList<PVQueryResult> pvQueryResults,
                                   SolrPvDocument originalDocument,
                                   String sheet, Map<String, ClaHistogramCollection<Long>> collections,
                                   int partMaturityLevel,
                                   boolean forceMaturityLevelSimilarity, int flags) throws IOException {
        int queryCounter = 0;
        for (final Map.Entry<String, ClaHistogramCollection<Long>> e : collections.entrySet()) {
            PvQueryRequestParams pvQueryRequestParams = createQueryForPv(originalDocument, e);
            if (pvQueryRequestParams == null) {
                continue;
            }
            pvQueryRequestParams.setOriginMaturityLevel(partMaturityLevel);
            pvQueryRequestParams.setForceMaturityLevelSimilarity(forceMaturityLevelSimilarity);
            List<Query> fullFilters = createFullFilters(req, requestFilters,
                    (forceMaturityLevelSimilarity ? partMaturityLevel : null),
                    (originalDocument.isForceWorkbookMaturityLevel() ? originalDocument.getOriginMaturityLevel() : null),
                    "score");
            pvQueryRequestParams.setFilters(fullFilters);
            PVQueryResult pvQueryResult = executePvQuery(req,
                    searcher,
                    flags,
                    defType,
                    pvQueryRequestParams,
                    cutOffValue);
            pvQueryResult.setOriginalDocument(originalDocument);
            pvQueryResult.setSheet(sheet);
            pvQueryResults.add(pvQueryResult);

            lastQueryGroupCount[queryCounter] = pvQueryResult.getResultSize();
            queryCounter++;

        }
    }

    public PvQueryRequestParams createQueryForPv(SolrPvDocument originalDocument, Map.Entry<String, ClaHistogramCollection<Long>> e) {
        long contentId = originalDocument.getId();
        final PropertyVectorKey propertyVectorKey = new PropertyVectorKey(contentId, null, PVType.valueOf(e.getKey()).toInt());
        final PVType pvt = PVType.valueOf(propertyVectorKey.getPvType());
        final ClaHistogramCollection<Long> claHistogramCollection = e.getValue();
        if (skipPv(propertyVectorKey.getPvType())) {
            return null;
        }
        if (claHistogramCollection.getItemsCount() == 0) {
            return null;
        }
        final String queryString = PvAnalysisDataUtils.buildQueryClauseForPv(propertyVectorKey, claHistogramCollection);
        PvQueryRequestParams pvQueryRequestParams = new PvQueryRequestParams(originalDocument, queryString, pvt.name());
        pvQueryRequestParams.setOriginItemsCount(claHistogramCollection.getItemsCount());
        pvQueryRequestParams.setOriginGlobalCount(claHistogramCollection.getGlobalCount());
        return pvQueryRequestParams;
    }

    private void extractAndExecuteSubQuery(SolrQueryRequest req, SolrParams params, ClaSimilaritySearcher searcher,
                                           String defType, List<Query> filters, ArrayList<PVQueryResult> pvQueryResults,
                                           SolrPvDocument originalDocument, String sheet, int partMaturityLevel, int i) throws IOException {
        int flags = SolrIndexSearcher.GET_SCORES;
        //Extract the queries from the params
        String queryString = params.get(ClaGroupingParams.GF + i);
        if (queryString == null) {
            return;
        }
        String pvType = extractPvType(params, i);
        Integer originItemsCount = extractOriginItemsCount(params, i);
        Integer originGlobalCount = extractOriginGlobalCount(params, i);
        //Create full filter list
//        String cutOffField = ClaGroupingParams.GF + i;
//        Integer maturityLevel = originalDocument.getOriginMaturityLevel();
        boolean forceMaturityLevelSimilarity = originalDocument.isForceMaturityLevelSimilarity();
        List<Query> fullFilters = createFullFilters(req, filters,
                (forceMaturityLevelSimilarity ? partMaturityLevel : null),
                (originalDocument.isForceWorkbookMaturityLevel() ? originalDocument.getOriginMaturityLevel() : null),
                null);      // TODO: verify removing - cutOffField);
        PvQueryRequestParams pvQueryRequestParams = new PvQueryRequestParams(originalDocument, queryString, fullFilters, pvType, forceMaturityLevelSimilarity);
        pvQueryRequestParams.setOriginItemsCount(originItemsCount);
        pvQueryRequestParams.setOriginMaturityLevel(partMaturityLevel);
        pvQueryRequestParams.setOriginGlobalCount(originGlobalCount);

        PVQueryResult pvQueryResult = executePvQuery(req,
                searcher,
                flags,
                defType,
                pvQueryRequestParams,
                null);
        pvQueryResult.setOriginalDocument(originalDocument);
        pvQueryResult.setSheet(sheet);
        pvQueryResults.add(pvQueryResult);
        lastQueryGroupCount[i] = pvQueryResult.getResultSize();
    }

    private PVQueryResult executePvQuery(SolrQueryRequest req, ClaSimilaritySearcher searcher, int flags,
                                         String defType,
                                         PvQueryRequestParams pvQueryRequestParams,
                                         Float cutOffValue) throws IOException {

        //Execute query
        DocList queryResult = executeSubQuery(req, searcher, flags, defType,
                pvQueryRequestParams);
        //Create result objects
        PVQueryResult resultObject = createResultObject(pvQueryRequestParams, queryResult, cutOffValue);
        return resultObject;
    }

    private List<Query> createFullFilters(SolrQueryRequest req,
                                          List<Query> filters,
                                          Integer maturityLevel,
                                          Integer workbookMaturityLevel,
                                          String cutOffField) {
        List<Query> fullFilters = new ArrayList<>(filters);
        if (maturityLevel != null) {    // Filter only docs with same ML
            fullFilters.add(createMaturityFilter(req, ClaGroupingParams.ML_field, maturityLevel));
        }
        if (workbookMaturityLevel != null) {    // Filter only docs with same ML
            fullFilters.add(createMaturityFilter(req, ClaGroupingParams.ML_FILE_field, workbookMaturityLevel));
        }
//        if (cutOffValue != null && cutOffField != null) {
//            Query cutOffFilter = createCutOffFilter(req, cutOffField);
//            fullFilters.add(cutOffFilter);
//        }
        return fullFilters;
    }

    private String extractPvType(SolrParams params, int i) {
        String pvType = params.get(ClaGroupingParams.PT + i);
        if (pvType == null) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Grouping Search Component requires " + ClaGroupingParams.PT + " for each " + ClaGroupingParams.GF);
        }
        return pvType;
    }

    private Integer extractOriginItemsCount(SolrParams params, int i) {
        Integer originItemsCount = params.getInt(ClaGroupingParams.OIC + i);
        if (originItemsCount == null) {
            // For backward compatibility - allow missing parameter. TODO: remove when redundant
//            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Grouping Search Component requires " + ClaGroupingParams.OIC + " for each " + ClaGroupingParams.GF);
        }
        return originItemsCount;
    }

    private Properties getCustomThresholds(SolrParams params, Integer originMaturityLevel, boolean isContent) {
        // Get default thresholds set based on the ML value
        int level = (originMaturityLevel == null) ? 0 : originMaturityLevel;
        if (level > 9) {
            // Combined ML - contentMl + styleMl*10;
            level = isContent ? (level % 10) : (level / 10);
        }
        if (level < 0 || level > 3) {
            level = 0;
        }
        Properties props = thresholdsByMl[level];
        String thresholdsDef = params.get(ClaGroupingParams.TR);
        if (!Strings.isNullOrEmpty(thresholdsDef)) {
            try {
                Properties customThresholds = new Properties(props);
                logger.trace("Custom thresholds parameter [{}]", thresholdsDef);
                String[] thresholdList = thresholdsDef.split("[\\s,]");
                for (String thresholdDef : thresholdList) {
                    int sep = thresholdDef.indexOf(":");
                    String threshold = thresholdDef.substring(0, sep);
                    String value = thresholdDef.substring(sep + 1);
                    customThresholds.setProperty(threshold, value);
                }
                logger.debug("Thresholds for ML={}: {}", level, customThresholds.toString());
                return customThresholds;
            } catch (Exception e) {
                logger.error("Error parsing the given custom threshold - illegal value " + thresholdsDef, e);
                throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Error parsing the given custom threshold - illegal value " + thresholdsDef, e);
            }
        } else {
            logger.debug("Orig Thresholds for ML={}: {}", level, props.toString());
            return props;
        }
    }

    private BaseGroupingAlgorithm createGroupingAlgorithm(ArrayList<PVQueryResult> pvQueryResults, SolrPvDocument originalDocument,
                                                          Properties customThresholdsContent, Properties customThresholdsStyle,
                                                          Map<Integer, List<Integer>> debFiles, boolean testDropThreshold) {
        switch (groupingType) {
            case WORD:
                return new WordGroupingAlgorithm(pvQueryResults, originalDocument, customThresholdsContent, customThresholdsStyle, debFiles, testDropThreshold);
            case SHEET:
            case WORKBOOK:
                return new ExcelGroupingAlgorithm(pvQueryResults, originalDocument, customThresholdsContent, customThresholdsStyle, debFiles, testDropThreshold);
        }
        throw new RuntimeException("Unsupported grouping type " + groupingType);
    }

    private Integer extractOriginGlobalCount(SolrParams params, int i) {
        Integer originGlobalCount = params.getInt(ClaGroupingParams.OGC + i);
        if (originGlobalCount == null) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Grouping Search Component requires " + ClaGroupingParams.OGC + " for each " + ClaGroupingParams.GF);
        }
        return originGlobalCount;
    }

    private DocList executeSubQuery(SolrQueryRequest req, ClaSimilaritySearcher searcher, int flags, String defType
            , PvQueryRequestParams pvQueryRequestParams) throws IOException {

//        Map<Long, List<Integer>> originalDocument = pvQueryRequestParams.getOriginalDocument().getPvMap();
        Long debOriginalDocument = pvQueryRequestParams.getOriginalDocument().getId();
        String pvType = pvQueryRequestParams.getPvType();
        Integer originGlobalCount = pvQueryRequestParams.getOriginGlobalCount();
        if (logger.isDebugEnabled()) {
            logger.debug("Execute Query on PV Type {} origin count {} origin doc id {}: \nreq={}\nparams={}", pvType, originGlobalCount, debOriginalDocument, req, pvQueryRequestParams);
        }
        if (logger.isTraceEnabled()) {
            logger.debug("\n+++++++++++\npv.type {}\n,\npv.contentId:{}\npv.query:{}\npv.filters:{}\n+++++++++++\n", pvType, debOriginalDocument, pvQueryRequestParams.getQueryString(), pvQueryRequestParams.getFilters());
        }
        String queryString = pvQueryRequestParams.getQueryString();
        List<Query> filters = pvQueryRequestParams.getFilters();
        DocList result = executeQuery(req, searcher, flags, defType, filters, queryString);
        if (logger.isDebugEnabled()) {
            boolean forceMaturityLevelSimilarity = pvQueryRequestParams.isForceMaturityLevelSimilarity();
            Integer originMaturityLevel = pvQueryRequestParams.getOriginMaturityLevel();
            logger.debug("Query results (PV Type {} origin counts {}/{} origin doc id {}{}{}): size={}",
                    pvType, pvQueryRequestParams.getOriginItemsCount(), originGlobalCount, debOriginalDocument, (forceMaturityLevelSimilarity ? "" : " force"), (originMaturityLevel == null ? "" : (" ml=" + originMaturityLevel)), result.size());
        }
        return result;
    }

    private DocList executeQuery(SolrQueryRequest req, ClaSimilaritySearcher searcher, int flags, String defType, List<Query> filters, String queryString) throws IOException {
        DocList queryResult;
        try {
            QParser parser = QParser.getParser(queryString, defType, req);
            Query query = parser.getQuery();
            //TODO - support multiple result pages with deep paging.
            queryResult = searcher.getDocList(query, filters, null, 0, maxPageSize, flags);

        } catch (SyntaxError syntaxError) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, syntaxError);
        } catch (RuntimeException runtimeException) {
            logger.error("ClaGroupingSearchComponent - Runtime exception while trying to execute query.\n" +
                    "queryString={}\n" +
                    "def={} req={} filters={} flags={} ", queryString, defType, req, filters, flags);
            throw runtimeException;
        }
        return queryResult;
    }

    private Query createCutOffFilter(SolrQueryRequest req, String cutOffField) {
        try {
//            QParser fqp = QParser.getParser("{!frange l=" + cutOffValue + "}query($" + cutOffField + ")", null, req);
            QParser fqp = QParser.getParser("{!frange l=" + cutOffValue + "}" + cutOffField, null, req);
            return fqp.getQuery();
        } catch (SyntaxError syntaxError) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, syntaxError);
        }
    }

    private Query createMaturityFilter(SolrQueryRequest req, String maturityLevelField, Integer maturityLevelValue) {
        try {
            if (logger.isTraceEnabled()) {
                String contentId = req.getParams().get(ClaGroupingParams.OD);
                logger.trace("Adding filter for query for contentId:{}, {}:{} ", contentId, maturityLevelField + ":" + maturityLevelValue.toString());
            }
            QParser fqp = QParser.getParser(maturityLevelField + ":" + maturityLevelValue.toString(), null, req);
            return fqp.getQuery();
        } catch (SyntaxError syntaxError) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, syntaxError);
        }
    }

    private PVQueryResult createResultObject(PvQueryRequestParams pvQueryRequestParams,
                                             DocList queryResult, Float cutOffValue) {
        PVQueryResult pvQueryResult = new PVQueryResult(queryResult, pvQueryRequestParams.getPvType(),
                pvQueryRequestParams.getOriginItemsCount(), pvQueryRequestParams.getOriginGlobalCount(), cutOffValue);
        return pvQueryResult;
    }

    public String getLastQueryGroupCount() {
        return Arrays.toString(lastQueryGroupCount);
    }

    private void fillSkippedPvNames(String skipAnalyzedPvsByName) {
        String[] skipList = skipAnalyzedPvsByName.split(",");
        skippedAnalysisPvsNames = Stream.of(skipList).map(n -> PVType.valueOf(n).toInt()).collect(toSet());

        String collect = skippedAnalysisPvsNames.stream().map(i -> PVType.valueOf(i).name()).collect(Collectors.joining(","));
        logger.info("Set skippedAnalysisPvsNames to {}={}", skippedAnalysisPvsNames,
                collect);
    }

    private boolean skipPv(final int pvType) {
        if (skippedAnalysisPvsNames != null && skippedAnalysisPvsNames.contains(Integer.valueOf(pvType))) {
//			logger.debug("Skipping PVType. {} in skippedAnalysisPvsNames", PVType.valueOf(pvType).name());
            return true;
        }
        return false;
    }

}
