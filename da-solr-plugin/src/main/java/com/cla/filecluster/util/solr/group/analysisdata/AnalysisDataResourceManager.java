package com.cla.filecluster.util.solr.group.analysisdata;

import com.cla.common.utils.FileSizeUnits;
import com.cla.common.utils.TimeRecorderUtils.RecordTimeResult;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.google.common.base.Strings;
import com.google.common.collect.*;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Sort;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.DocList;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.SolrIndexSearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.utils.TimeRecorderUtils.recordTime;

/**
 * Responsible for allocating anlaysis data with respect to preconfigured limits.
 * Running concurrent computations on analysis data,
 */
public class AnalysisDataResourceManager {

    private static final Logger logger = LoggerFactory.getLogger(AnalysisDataResourceManager.class);
    private static final int DEFAULT_SIZE_ESTIMATION = 1000;


    private ScheduledExecutorService workCollector;
    private ScheduledExecutorService highMemorySampler;
    private ExecutorService computationUnitDispatcher;

    private Semaphore resourceSemaphore;

    private List<ComputationUnit> computationUnits = new CopyOnWriteArrayList<>();
    private List<Integer> sizeComputations = Collections.synchronizedList(new LinkedList<>());
    private final Object lock = new Object();
    private Set<Long> largeDocumentsIds = Collections.synchronizedSet(new HashSet<>());

    // Config params
    private Map<String, String> compatibilityMap;
    private long maxMemoryLimit;
    private double maxMemoryThresholdPercentage;
    private int batchSize;
    private int waitTicksBeforeGC;
    private float sizeFactor;
    private int semaphoreLimit;
    private boolean useAdaptiveSize;
    private int maxDocumentSizeInKB;
    private boolean dataCompressed;


    // Memory sampling
    private Stats stats;
    private int noWorkCounter = 0;

    // Stats


    AnalysisDataResourceManager(AnalysisDataResourceManagementConfig analysisResourceManagementConfig) {
        initResourceManager(analysisResourceManagementConfig);
        workCollector = prepareWorkCollector(analysisResourceManagementConfig);
        highMemorySampler = prepareHighMemorySampler();
        computationUnitDispatcher = prepareComputationUnitsDispatcher(analysisResourceManagementConfig.getLoadConcurrency());
    }

    private void initResourceManager(AnalysisDataResourceManagementConfig analysisResourceManagementConfig) {
        this.sizeFactor = analysisResourceManagementConfig.getAnalysisDataSizeFactor();
        this.batchSize = analysisResourceManagementConfig.getBatchSize();
        this.useAdaptiveSize = analysisResourceManagementConfig.isUseAdaptiveSize();
        this.maxMemoryThresholdPercentage = analysisResourceManagementConfig.getMaxHeapMemoryThresholdPercentage();
        this.maxMemoryLimit = (long) (FileSizeUnits.BYTE.toKilobytes(Runtime.getRuntime().totalMemory()) * maxMemoryThresholdPercentage);
        this.waitTicksBeforeGC = analysisResourceManagementConfig.getWaitTicksBeforeGC();
        this.semaphoreLimit = analysisResourceManagementConfig.getMemoryLimitInKB();
        this.maxDocumentSizeInKB = analysisResourceManagementConfig.getMaxDocumentSizeInKB();
        resourceSemaphore = new Semaphore(this.semaphoreLimit);
        dataCompressed = analysisResourceManagementConfig.isDataCompressed();
        compatibilityMap = analysisResourceManagementConfig.getCompatibilityMap();
        this.stats = new Stats(computationUnits, this.resourceSemaphore, this.semaphoreLimit);
    }

    /**
     * Initializes work collector that collect analysis data ids for work
     * by the @{@link AnalysisDataResourceManagementConfig#getFetchIntervalExp()}
     */
    private ScheduledExecutorService prepareWorkCollector(AnalysisDataResourceManagementConfig analysisResourceManagementConfig) {
        String[] intervalAndTimeUnit = analysisResourceManagementConfig.getFetchIntervalExp().split(";");
        Long interval = Long.valueOf(intervalAndTimeUnit[0]);
        TimeUnit timeUnit = TimeUnit.valueOf(intervalAndTimeUnit[1]);
        ScheduledExecutorService scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor(
                        new ThreadFactoryBuilder()
                                .setNameFormat("rm-work-collector-%d")
                                .build());
        scheduledExecutorService.scheduleWithFixedDelay(this::collectWork, 0, interval, timeUnit);
        return scheduledExecutorService;
    }

    /**
     * Responsible for scheduling the memory sampling to indicate on high memory usage
     *
     * @return
     */
    private ScheduledExecutorService prepareHighMemorySampler() {
        ScheduledExecutorService scheduledExecutorService = Executors
                .newSingleThreadScheduledExecutor(
                        new ThreadFactoryBuilder()
                                .setNameFormat("memory-sampler-%d")
                                .build());
        scheduledExecutorService.scheduleWithFixedDelay(this::sampleMemoryConsumption,
                0, 1, TimeUnit.SECONDS);
        return scheduledExecutorService;
    }

    /**
     * Initializes the thread pool responsible for loading analysis data and dispatching it
     * the analysis threads
     *
     * @param loadConcurrency - how concurrent we would like load and compute to be
     * @return the Thread pool which is responsible to submit the load an compute tasks
     */
    private ExecutorService prepareComputationUnitsDispatcher(int loadConcurrency) {
        return Executors.newFixedThreadPool(loadConcurrency,
                new ThreadFactoryBuilder()
                        .setNameFormat("rm-similarity-compute-%d")
                        .build());
    }

    /**
     * Collect ids from all registered analysis threads up to a limit defined by the
     * configed resource semaphore.
     * In case of high memory consumption defined by
     *
     * @{@link AnalysisDataResourceManagementConfig#getMaxHeapMemoryThresholdPercentage()}
     * It halts till memory cool down
     */
    private void collectWork() {
        try {
            if (computationUnits.isEmpty()) {
                logNoWork();
                return;
            }

            if (stats.isHighMemoryConsumption()) {
                return;
            }

            // Remove done computation units
            List<ComputationUnit> workingCopy = computationUnits.stream()
                    .filter(ComputationUnit::hasMoreIds)
                    .collect(Collectors.toList());

            logComputationUnitsStatus();

            int dataSizeInKb = estimateNewDataSizeInKB();

            allocateIdsFromComputationUnits(workingCopy, dataSizeInKb, batchSize)
                    .asMap()
                    .forEach((computationUnit, allocatedIds) ->
                            computationUnitDispatcher.submit(
                                    new ComputationUnitTask(computationUnit, allocatedIds, dataSizeInKb)));
        } catch (Throwable t) {
            logger.error("Failed to collect work", t);
        }
    }


    private void logNoWork() {
        noWorkCounter++;
        if (logger.isTraceEnabled()) {
            if (noWorkCounter == 10000) {
                logger.trace("No work collected for similarity");
                noWorkCounter = 0;
            }
        }
    }

    private int estimateNewDataSizeInKB() {
        int dataSizeInKb;
        synchronized (lock) {
            dataSizeInKb = (int) sizeComputations.stream()
                    .mapToInt(Integer::intValue)
                    .average()
                    .orElse(DEFAULT_SIZE_ESTIMATION);
            sizeComputations.clear();
        }
        return dataSizeInKb;
    }

    private Multimap<ComputationUnit, Long> allocateIdsFromComputationUnits(List<ComputationUnit> workingCopy, int dataSizeInKb, int batchSize) {
        ListMultimap<ComputationUnit, Long> allocatedIds = LinkedListMultimap.create();
        boolean semaphoreFull = false;
        int allocatedAmount = 0;
        while (!semaphoreFull && !workingCopy.isEmpty()) {
            logSemaphoreAvailableKBStatus();
            Iterator<ComputationUnit> iterator = workingCopy.iterator();
            Set<ComputationUnit> completedBatches = new HashSet<>();
            while (iterator.hasNext()) {
                ComputationUnit computationUnit = iterator.next();
                if (!computationUnit.hasMoreIds() || allocatedIds.get(computationUnit).size() == batchSize) {
                    completedBatches.add(computationUnit);
                    continue;
                }
                if (resourceSemaphore.tryAcquire(dataSizeInKb)) {
                    Long contentId = computationUnit.nextContentId();
                    allocatedIds.put(computationUnit, contentId);
                    allocatedAmount++;
                } else {
                    semaphoreFull = true;
                    break;
                }
            }
            workingCopy.removeAll(completedBatches);
        }


        if (!allocatedIds.isEmpty()) {
            logger.debug("Reserve {} KB for allocating {} analysis data for {} workers",
                    allocatedAmount * dataSizeInKb, allocatedAmount, computationUnits.size());
        }
        return allocatedIds;
    }

    private void logSemaphoreAvailableKBStatus() {
        if (logger.isTraceEnabled()) {
            logger.trace("Semaphore available: {} KB", resourceSemaphore.availablePermits());
        }
    }

    private void sampleMemoryConsumption() {
        long heapMemory = getUsedMemory();
        if (heapMemory >= maxMemoryLimit) {
            stats.indicateHighMemoryConsumption();

            if (stats.highMemoryCount() % 10 == 0) {
                logger.warn("High memory utilization {}({}) , Resource manager waiting...", maxMemoryThresholdPercentage, heapMemory);
            }
            stats.incHighMemory();
            if (stats.highMemoryCount() % (waitTicksBeforeGC * 10) == 0) {
                stats.incForcedGC();
                logger.warn("Resource manager, waited for {} seconds, trying to force GC", waitTicksBeforeGC * 10);
                System.gc();
            }
        } else {
            if (stats.isHighMemoryConsumption()) {
                logger.info("Memory below threshold {}({}), Resource manager may resume work", maxMemoryThresholdPercentage, heapMemory);
            }
            stats.indicateNormalMemoryConsumption();
        }
    }

    private long getUsedMemory() {
        Runtime runtime = Runtime.getRuntime();
        return FileSizeUnits.BYTE.toKilobytes(runtime.totalMemory() - runtime.freeMemory());
    }

    private void logComputationUnitsStatus() {
        if (logger.isDebugEnabled()) {
            String distributionMap = computationUnits.stream()
                    .map(worker -> worker.getThreadName() + "=" + worker.getWorkLeft()).collect(Collectors.joining("\n"));
            if (!Strings.isNullOrEmpty(distributionMap)) {
                logger.debug("\nCurrent distribution map:\n[\n{}\n]", distributionMap);
            }
        }
    }


    /**
     * Given list of content ids fetching in a manged way corresponding analysis data
     * with respect to the memory limits.
     * the fetching is done in predefined batches See {@link AnalysisDataResourceManager#batchSize}
     * and the computation for each batch is done asynchronously according to defined concurrency
     *
     * @param req        hold the current search to use for fetching the analysis data
     * @param contentIds list of ids to fetch their respective analysis data
     * @param algorithm  map function you wish to apply on the set of analysis data
     * @param <T>        the result you wish to return from you map algorithm
     * @return
     */
    public <T> Collection<T> mapReduce(SolrQueryRequest req, Collection<Long> contentIds,
                                       Function<List<PvAnalysisData>, Collection<T>> algorithm) {
        if (!contentIds.isEmpty()) {
            ComputationUnit computationUnit = new ComputationUnit(req, Thread.currentThread().getName(),
                    algorithm, contentIds);
            computationUnits.add(computationUnit);
            waitForCompletion(computationUnit);
            return computationUnit.getResult();
        }
        return Lists.newArrayList();
    }

    private void waitForCompletion(ComputationUnit computationUnit) {
        //noinspection SynchronizationOnLocalVariableOrMethodParameter
        synchronized (computationUnit) {
            try {
                logger.debug("Thread {} waiting for completion", computationUnit.getThreadName());
                computationUnit.wait();
                logger.debug("Thread {} completed", computationUnit.getThreadName());
            } catch (InterruptedException e) {
                logger.error("{} interrupted, exiting work");
            }
        }
    }

    public void shutDown() {
        workCollector.shutdownNow();
        computationUnitDispatcher.shutdownNow();
        highMemorySampler.shutdownNow();
    }

    /**
     * Use with care, this method is for fetching single analysis data, if you wish to fetch many analysis data
     * <p>
     * then use {@link AnalysisDataResourceManager#mapReduce}
     *
     * @param req
     * @param contentId
     * @return
     */
    public Optional<PvAnalysisData> findPvAnalysisData(SolrQueryRequest req, Long contentId) {
        try {
            QParser parser = QParser.getParser("id:" + contentId, QParserPlugin.DEFAULT_QTYPE, req);
            SolrIndexSearcher searcher = req.getSearcher();
            DocList docList = searcher.getDocList(parser.getQuery(), Lists.newArrayList(), Sort.INDEXORDER,
                    0, 1, SolrIndexSearcher.GET_DOCLIST);
            if (docList.matches() == 0) {
                return Optional.empty();
            }
            Document doc = searcher.doc(docList.iterator().nextDoc());
            return Optional.of(PvAnalysisDataUtils.convertDocumentToPvAnalysisData(extractAnalysisDataParams(doc),
                    dataCompressed, compatibilityMap));
        } catch (Throwable t) {
            throw new RuntimeException(String.format("Failed to fetch analysis data for contentId: %d", contentId), t);
        }

    }

    private Map<String, Object> extractAnalysisDataParams(Document doc) {
        return ImmutableMap.of(
                "id", doc.get("id"),
                "pvCollections", doc.getBinaryValue("pvCollections").bytes,
                "metadata", doc.getBinaryValue("metadata").bytes,
                "maturity", doc.getField("maturity").numericValue(),
                "format", doc.getField("format").numericValue());
    }


    class ComputationUnitTask implements Runnable {
        private final ComputationUnit computationUnit;
        private final int sizeEstimation;
        private final Collection<Long> allocatedIds;

        ComputationUnitTask(ComputationUnit computationUnit,
                            Collection<Long> allocatedIds, int sizeEstimation) {
            this.computationUnit = computationUnit;
            this.allocatedIds = allocatedIds;
            this.sizeEstimation = sizeEstimation;
        }

        @Override
        public void run() {
            try {
                // Load analysis datas
                RecordTimeResult<List<PvAnalysisData>> recordResult = recordTime(this::loadAnalysisDataList);
                List<PvAnalysisData> analysisDataList = recordResult.getResult();
                stats.setAnalysisDataLoadRateInMillis(calcRateInMs(recordResult.getDuration(), analysisDataList.size()));
                logger.debug("Thread {}, Loaded (analysisData: {}/{} items, time: {} ms, semaphore: {} KB," +
                                " sizeEstimation: {} KB)", computationUnit.getThreadName(),
                        analysisDataList.size(), allocatedIds.size(), recordResult.getDuration(),
                        semaphoreLimit - resourceSemaphore.availablePermits(),
                        sizeEstimation);

                // Run computation on analysis datas
                RecordTimeResult<Void> recordTimeResult = recordTime(analysisDataList, computationUnit::compute);
                stats.setAnalysisDataComputeRateInMillis(calcRateInMs(recordResult.getDuration(), analysisDataList.size()));

                logger.debug("Thread {}: (workedOn: {}, left: {}/{}, time:{} ms",
                        computationUnit.getThreadName(), allocatedIds.size(), computationUnit.getWorkLeft() - allocatedIds.size(),
                        computationUnit.getSize(), recordTimeResult.getDuration(), allocatedIds.size() * sizeEstimation,
                        semaphoreLimit - resourceSemaphore.availablePermits());
            } catch (Throwable t) {
                logger.error(String.format("Thread %s: Failed to compute for %s on %d ids",
                        computationUnit.getThreadName(), allocatedIds.size()), t);
            } finally {
                resourceSemaphore.release(allocatedIds.size() * sizeEstimation);
                logger.debug("Thread {}: released: {}, semaphore: {}", computationUnit.getThreadName(),
                        allocatedIds.size() * sizeEstimation, semaphoreLimit - resourceSemaphore.availablePermits());
                completeBatchOfWorkAndNotifyIfDone();
            }
        }

        private double calcRateInMs(long duration, int size) {
            return (double) duration / (double) size;
        }

        private void completeBatchOfWorkAndNotifyIfDone() {
            synchronized (computationUnit) {
                computationUnit.batchDone(allocatedIds.size());
                if (computationUnit.getWorkLeft() == 0) {
                    logger.debug("Similarity computation is completed, disposing and notifying {}", computationUnit.getThreadName());
                    computationUnits.remove(computationUnit);
                    computationUnit.notifyAll();
                }
            }
        }

        private List<PvAnalysisData> loadAnalysisDataList() {
            long usedMemoryBefore = getUsedMemory();
            List<Pair<PvAnalysisData, Integer>> data = getPvAnalysisDataByContentIdsAndSize(
                    computationUnit.getReq(), allocatedIds);
            long usedMemoryAfter = getUsedMemory();
            // Compute new size estimation
            long memoryDiff = usedMemoryAfter - usedMemoryBefore;
            synchronized (lock) {
                sizeComputations.add(computeNewSizeEstimation(data, memoryDiff));
            }
            return data.stream()
                    .map(Pair::getLeft)
                    .collect(Collectors.toList());
        }


        private int computeNewSizeEstimation(List<Pair<PvAnalysisData, Integer>> data, long memoryDiff) {
            int newSizeEstimation;
            if (useAdaptiveSize) {
                newSizeEstimation = computeAdaptive(data, memoryDiff);
            } else {
                newSizeEstimation = computeBySizeFactor(data, memoryDiff);
            }
            if (newSizeEstimation < 0) {
                logger.warn("Size estimation turned negative!!! using default {}", DEFAULT_SIZE_ESTIMATION);
                newSizeEstimation = DEFAULT_SIZE_ESTIMATION;
            }
            return newSizeEstimation;
        }

        private int computeAdaptive(List<Pair<PvAnalysisData, Integer>> data, long memoryDiff) {
            int sizePerData = (int) memoryDiff / allocatedIds.size();
            if (sizePerData > 0 && sizePerData < semaphoreLimit) {
                logger.trace("Memory consumption of fetching {} analysis data is {} KB, new size estimation for next bulk: {} KB",
                        allocatedIds.size(), memoryDiff, sizePerData);
                return sizePerData;
            } else {
                return computeBySizeFactor(data, memoryDiff);
            }
        }
    }


    private List<Pair<PvAnalysisData, Integer>> getPvAnalysisDataByContentIdsAndSize(SolrQueryRequest req,
                                                                                     Collection<Long> allocatedIds) {
        // Take only ids which are not in the large documents ids list
        List<Long> idsToQuery = allocatedIds.stream()
                .filter(this::notInLargeDocumentIdList)
                .collect(Collectors.toList());
        if (idsToQuery.isEmpty()) {
            logger.warn("All {} candidates surpass allowed size {}", allocatedIds.size(), maxDocumentSizeInKB);
            return Lists.newArrayList();
        }
        List<Pair<PvAnalysisData, Integer>> result = new LinkedList<>();
        String fq = idsToQuery.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(" ", "id:(", ")"));
        try {
            QParser parser = QParser.getParser(fq, QParserPlugin.DEFAULT_QTYPE, req);
            SolrIndexSearcher searcher = req.getSearcher();
            searcher.getDocList(parser.getQuery(), Lists.newArrayList(), Sort.INDEXORDER, 0, idsToQuery.size(),
                    SolrIndexSearcher.GET_DOCLIST)
                    .iterator()
                    .forEachRemaining(docId -> accumulateAnalysisData(searcher, docId, result));
        } catch (Throwable t) {
            throw new RuntimeException(String.format("Failed to find analysis data for %d ids", idsToQuery.size()), t);
        }
        return result;
    }

    private void accumulateAnalysisData(SolrIndexSearcher searcher, Integer docId,
                                        List<Pair<PvAnalysisData, Integer>> resultsToAccumulate) {
        Document doc = null;
        try {
            doc = searcher.doc(docId);
            synchronized (doc) {
                int dataSize = doc.getBinaryValue("pvCollections").length;
                if (FileSizeUnits.BYTE.toKilobytes(dataSize) > this.maxDocumentSizeInKB) {
                    logger.warn("Skipping analysisData  (id={},size={}) as it is greater " +
                            "than maxAllowedSize={}", doc.get("id"), dataSize, maxDocumentSizeInKB);
                    largeDocumentsIds.add(Long.valueOf(doc.get("id")));
                } else {
                    PvAnalysisData pvAnalysisData = PvAnalysisDataUtils
                            .convertDocumentToPvAnalysisData(extractAnalysisDataParams(doc), dataCompressed,
                                    compatibilityMap);
                    resultsToAccumulate.add(Pair.of(pvAnalysisData, dataSize));
                }
            }
        } catch (IOException e) {
            logger.warn(String.format("Failed to get document {} from index", docId), e);

        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed to get document %s from index",
                    doc != null ? ("(id = " + doc.get("id") + ")") : "(internalId=  " + docId + ")"), e);
        }
    }

    private boolean notInLargeDocumentIdList(Long id) {
        return !largeDocumentsIds.contains(id);

    }

    private int computeBySizeFactor(List<Pair<PvAnalysisData, Integer>> data, long memoryDiff) {
        int newSizeEstimation;
        int totalBinarySize = data.stream().mapToInt(Pair::getRight).sum();
        long count = data.size();
        if (totalBinarySize == 0 || count == 0) {
            newSizeEstimation = DEFAULT_SIZE_ESTIMATION;
        } else {
            newSizeEstimation = (int) (FileSizeUnits.BYTE.toKilobytes(totalBinarySize / count) * sizeFactor);
            logger.trace("Memory consumption of fetching {} analysis data is {} KB, " +
                            "new size estimation for next bulk: {} KB, gap between actual used memory {}%",
                    data.size(), memoryDiff, newSizeEstimation,
                    (int) (((double) (memoryDiff - (totalBinarySize * sizeFactor)) / (double) memoryDiff) * 100));
        }
        return newSizeEstimation;
    }


    public Stats getStats() {
        return stats;
    }

    /**
     * Stats of memory and work of analysis data resource manager
     */
    public static class Stats {

        private static final String WORK_ITEM_DESCRIPTION_TEMPLATE = "%s(%d/%d)";

        private final Semaphore resourceSemaphore;
        private final int semaphoreLimit;
        private final Collection<? extends ComputationUnitInfo> computationUnitInfos;
        private AtomicDouble analysisDataLoadRateInMillis = new AtomicDouble(0);
        private AtomicDouble analysisDataComputeRateInMillis = new AtomicDouble(0);

        private boolean highMemoryConsumption;
        private int forcedGCCounter;
        private int highMemoryCounter;

        private Stats(Collection<? extends ComputationUnitInfo> computationUnitInfos, Semaphore resourceSemaphore, int semaphoreLimit) {
            this.computationUnitInfos = computationUnitInfos;
            this.resourceSemaphore = resourceSemaphore;
            this.semaphoreLimit = semaphoreLimit;
        }

        void incForcedGC() {
            forcedGCCounter++;
        }

        void indicateHighMemoryConsumption() {
            this.highMemoryConsumption = true;
        }

        void indicateNormalMemoryConsumption() {
            this.highMemoryConsumption = false;
        }

        public int highMemoryCount() {
            return highMemoryCounter;
        }

        public void incHighMemory() {
            highMemoryCounter++;
        }

        public boolean isHighMemoryConsumption() {
            return highMemoryConsumption;
        }

        public int forcedGCCount() {
            return forcedGCCounter;
        }

        void setAnalysisDataLoadRateInMillis(double avgLoadAnalysisDataPerSec) {
            this.analysisDataLoadRateInMillis.set(avgLoadAnalysisDataPerSec);
        }

        public double analysisDataLoadRateInMillis() {
            return analysisDataLoadRateInMillis.get();
        }

        public void setAnalysisDataComputeRateInMillis(double avgCalcAnalysisDataPerSec) {
            this.analysisDataComputeRateInMillis.set(avgCalcAnalysisDataPerSec);
        }

        public double analysisDataComputeRateInMillis() {
            return analysisDataComputeRateInMillis.get();
        }

        public double usedMemoryPercentage() {
            return (double) (semaphoreLimit - resourceSemaphore.availablePermits()) / (double) semaphoreLimit;
        }

        public String sampleWorkDistribution(int sampleWorkLimit) {
            int limit = computationUnitInfos.size() > sampleWorkLimit ? sampleWorkLimit : computationUnitInfos.size();
            return computationUnitInfos
                    .stream()
                    .map(this::workItemDescription)
                    .limit(limit)
                    .collect(Collectors.joining("\n"));

        }

        private String workItemDescription(ComputationUnitInfo computationUnitInfo) {
            return String.format(WORK_ITEM_DESCRIPTION_TEMPLATE,
                    computationUnitInfo.getThreadName(),
                    computationUnitInfo.getTotalWork() - computationUnitInfo.getWorkLeft(),
                    computationUnitInfo.getTotalWork());
        }
    }
}


