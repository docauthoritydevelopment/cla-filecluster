package com.cla.filecluster.util.solr.group.pv;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;

import java.util.List;
import java.util.Map;

/**
 * Holds a reference to a solr document, along with optional PV data
 * Created by uri on 08/02/2017.
 */
public class SolrPvDocument {
    private final PvAnalysisData pvAnalysisData;
    private Map<Long, List<Integer>> pvMap;
    private Long id;
    private Integer originMaturityLevel;
    private boolean forceMaturityLevelSimilarity;

    private boolean forceWorkbookMaturityLevel;
    private FileType fileType;

    public SolrPvDocument(Long contentId, Map<Long, List<Integer>> pvMap, PvAnalysisData pvAnalysisData, boolean forceMaturityLevel, boolean forceWorkbookMaturityLevel) {
        this.pvAnalysisData = pvAnalysisData;
        this.id = contentId;
        this.pvMap = pvMap;
        this.originMaturityLevel = pvAnalysisData.getMaturityFile();
        this.forceMaturityLevelSimilarity = forceMaturityLevel;
        this.forceWorkbookMaturityLevel = forceWorkbookMaturityLevel;
    }

    public SolrPvDocument() {
        pvAnalysisData= null;
        id = null;
    }

    public long getId() {
        return id;
    }

    public Map<Long, List<Integer>> getPvMap() {
        return pvMap;
    }

    public PvAnalysisData getPvAnalysisData() {
        return pvAnalysisData;
    }

    public void setOriginMaturityLevel(Integer originMaturityLevel) {
        this.originMaturityLevel = originMaturityLevel;
    }

    public Integer getOriginMaturityLevel() {
        return originMaturityLevel;
    }

    public void setForceMaturityLevelSimilarity(boolean forceMaturityLevelSimilarity) {
        this.forceMaturityLevelSimilarity = forceMaturityLevelSimilarity;
    }

    public boolean isForceMaturityLevelSimilarity() {
        return forceMaturityLevelSimilarity;
    }

    public boolean isForceWorkbookMaturityLevel() {
        return forceWorkbookMaturityLevel;
    }

    public void setForceWorkbookMaturityLevel(boolean forceWorkbookMaturityLevel) {
        this.forceWorkbookMaturityLevel = forceWorkbookMaturityLevel;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public FileType getFileType() {
        return fileType;
    }
}
