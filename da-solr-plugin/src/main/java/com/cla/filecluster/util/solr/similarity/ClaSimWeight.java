package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.search.similarities.Similarity;

/** Stores the weight for a query across the indexed collection. This abstract
   * implementation is empty; descendants of {@code Similarity} should
   * subclass {@code ClaSimWeight} and define the statistics they require in the
   * subclass. Examples include idf, average field length, etc.
   */
  public abstract class ClaSimWeight extends Similarity.SimWeight {
    
    /**
     * Sole constructor. (For invocation by subclass 
     * constructors, typically implicit.)
     */
    public ClaSimWeight() {}
    
    /** The value for normalization of contained query clauses (e.g. sum of squared weights).
     * <p>
     * NOTE: a Similarity implementation might not use any query normalization at all,
     * it's not required. However, if it wants to participate in query normalization,
     * it can return a value here.
     */
    public abstract float getValueForNormalization();
    
    /** Assigns the query normalization factor and boost from parent queries to this.
     * <p>
     * NOTE: a Similarity implementation might not use this normalized value at all,
     * it's not required. However, it's usually a good idea to at least incorporate 
     * the boost into its score.
     * <p>
     * NOTE: If this method is called several times, it behaves as if only the
     * last call was performed.
     */
    public abstract void normalize(float queryNorm, float boost);
  }