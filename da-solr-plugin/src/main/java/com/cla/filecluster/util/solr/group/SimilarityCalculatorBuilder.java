package com.cla.filecluster.util.solr.group;

import com.cla.filecluster.service.files.pv.SimilarityCalculatorAbstractBuilder;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static com.cla.filecluster.util.solr.PluginArgsExtractor.*;

public class SimilarityCalculatorBuilder extends SimilarityCalculatorAbstractBuilder {

    private static final Logger logger = LoggerFactory.getLogger(SimilarityCalculatorBuilder.class);


    public SimilarityCalculatorBuilder(NamedList args) {
        groupingForceMaturityLevelSimilarity = getBooleanArgument(args, "fileGrouping.forceMaturityLevelSimilarity",
                true);
        maturityContentPvNames = getArgument(args, "maturity.model.contentPvNames",
                "PV_BodyNgrams,PV_SubBodyNgrams,PV_DocstartNgrams,PV_CellValues,PV_Formulas," +
                        "PV_ConcreteCellValues,PV_TextNgrams");
        String[] cfg = getStringListArgument(args, "wordSimilarityContentHighThreshold", "-");
        if (cfg.length == 1 && cfg[0].equals("-")) {
            logger.info("Similarity Threshold taken from hard-coded degault values");
        } else {
            logger.debug("Similarity Threshold read from configuration: wordSimilarityContentHighThreshold={}", Arrays.toString(cfg));
        }
        this.wordSimilarityContentHighThreshold = getStringListArgument(args, "wordSimilarityContentHighThreshold",
                "0.6,0.8,0.7,0.6");
        this.wordSimilarityStyleLowThreshold = getStringListArgument(args, "wordSimilarityStyleLowThreshold",
                "0.2,0.4,0.3,0.2");
        this.wordSimilarityDocstartLowThreshold = getStringListArgument(args, "wordSimilarityDocstartLowThreshold",
                "0.05,0.08,0.06,0.05");
        this.wordMinStyleItems = getIntListArgument(args, "wordMinStyleItems", "3");
        this.wordMinLetterHeadItems = getIntListArgument(args, "wordMinLetterHeadItems", "3");
        this.wordMinStyleSequenceItems = getIntListArgument(args, "wordMinStyleSequenceItems", "1");
        this.wordSimilarityContentLowThreshold = getStringListArgument(args, "wordSimilarityContentLowThreshold",
                "0.05,0.2,0.12,0.05");
        this.wordSimilarityHeadingsLowThreshold = getStringListArgument(args, "wordSimilarityHeadingsLowThreshold",
                "0.2,0.4,0.3,0.2");
        this.wordSimilarityStyleHighThreshold = getStringListArgument(args, "wordSimilarityStyleHighThreshold",
                "0.6,0.75,0.65,0.6");
        this.wordSimilarityHeadingsHighThreshold = getStringListArgument(args, "wordSimilarityHeadingsHighThreshold",
                "0.5,0.7,0.6,0.5");
        this.wordMinHeadingItems = getIntListArgument(args, "wordMinHeadingItems", "10");
        this.wordMinPLabelItems = getIntListArgument(args, "wordMinPLabelItems", "10");
        this.excelMinHeadingCountThreshold = getIntListArgument(args, "excelMinHeadingCountThreshold", "4");
        this.excelMinLayoutCountThreshold = getIntListArgument(args, "excelMinLayoutCountThreshold", "3");
        this.excelMinFormulaThreshold = getIntListArgument(args, "excelMinFormulaThreshold", "9");
        this.excelMinStyleCountThreshold = getIntListArgument(args, "excelMinStyleCountThreshold", "3");
        this.excelMinConcreteValueThreshold = getIntListArgument(args, "excelMinConcreteValueThreshold", "9");
        this.excelSimilarityHighContentThreshold = getStringListArgument(args, "excelSimilarityHighContentThreshold",
                "0.5,0.7,0.6,0.5");
        this.excelMinValueThreshold = getIntListArgument(args, "excelMinValueThreshold", "9");
        this.excelSimilarityHighStyleThreshold = getStringListArgument(args, "excelSimilarityHighStyleThreshold",
                "0.8,0.9,0.85,0.8");
        this.excelSimilarityHeadingHighSimThreshold = getStringListArgument(args, "excelSimilarityHeadingHighSimThreshold",
                "0.8,0.9,0.85,0.8");
        this.excelSimilarityHighLayoutThreshold = getStringListArgument(args, "excelSimilarityHighLayoutThreshold",
                "0.9,0.95,0.92,0.9");
        this.excelWorkbookAggregatedSimilarityThreshold = getStringListArgument(args, "excelWorkbookAggregatedSimilarityThreshold",
                "0.6,0.8,0.7,0.6");

        NamedList thresholdByPVParams = (NamedList) args.get("maturityParams");
        maturityLowThreshodsByPV = getArgument(thresholdByPVParams, "threshodsByPV.low", "");
        maturityMediumThreshodsByPV = getArgument(thresholdByPVParams, "threshodsByPV.medium", "");
        maturityPvGroups = getArgument(thresholdByPVParams, "maturity.pvGroups", "");
        maturityModelSingleMaturityLevel = false;

    }
}