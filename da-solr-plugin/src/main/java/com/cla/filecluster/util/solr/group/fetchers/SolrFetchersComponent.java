package com.cla.filecluster.util.solr.group.fetchers;

import com.cla.filecluster.util.solr.SolrConstants;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolrFetchersComponent extends SearchComponent implements SolrCoreAware {

    private static final Logger logger = LoggerFactory.getLogger(SolrFetchersComponent.class);

    private static SolrFetchers instance;

    private NamedList args;


    @Override
    public void init(NamedList args) {
        this.args = args;
    }

    public static SolrFetchers getFetchers() {
        return instance;
    }


    @Override
    public void prepare(ResponseBuilder rb) {
    }

    @Override
    public void process(ResponseBuilder rb) {
        logger.info("Requests are not supported, this is only for internal usage of the AnalysisDataResourceManager");
    }

    @Override
    public String getDescription() {
        return "Responsible for fetching analysis data constraint by the jvm memory";
    }

    @Override
    public void inform(SolrCore core) {
        synchronized (SolrFetchersComponent.class) {
            if (instance == null) {
                instance = SolrFetchers.build(args);
            }
        }
    }
}
