package com.cla.filecluster.util.solr.group.finalmatching.excel;

import com.cla.filecluster.util.solr.group.finalmatching.FinalMatchingComponent;
import com.cla.filecluster.util.solr.group.finalmatching.FinalMatchingTaskExecutor;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import org.apache.solr.common.util.NamedList;

import java.util.Collection;

import static com.cla.filecluster.util.solr.PluginArgsExtractor.getIntArgument;

/**
 * // TODO: add description
 */
public class ExcelFinalMatchingComponent extends FinalMatchingComponent<ExcelFinalMatchingRequestData> {


    private static final String ARG_GROUPED_XLS_SHEETS_FIRST_THLD = "groupedExcelSheetsProcessingFirstThreshold";
    private static final String ARG_GROUPED_XLS_SHEETS_LAST_THLD = "groupedExcelSheetsProcessingLastThreshold";

    private int groupedExcelSheetsProcessingFirstThreshold;
    private int groupedExcelSheetsProcessingLastThreshold;

    @Override
    public void init(NamedList args) {
        super.init(args);
        groupedExcelSheetsProcessingFirstThreshold = getIntArgument(args, ARG_GROUPED_XLS_SHEETS_FIRST_THLD, 8);
        groupedExcelSheetsProcessingLastThreshold = getIntArgument(args, ARG_GROUPED_XLS_SHEETS_LAST_THLD, 4);
    }

    @Override
    protected Class<ExcelFinalMatchingRequestData> requestDataClass() {
        return ExcelFinalMatchingRequestData.class;
    }

    @Override
    protected FinalMatchingTaskExecutor newFinalMatchingTaskExecutor(SolrPvDocument sourceDocument,
                                                                     ExcelFinalMatchingRequestData finalMatchingRequestData) {
        return new ExcelFinalMatchingTaskExecutor(
                sourceDocument,
                finalMatchingRequestData,
                similarityCalculator,
                pluginIsForceWorkbookMaturityLevel,
                groupedExcelSheetsProcessingFirstThreshold,
                groupedExcelSheetsProcessingLastThreshold);
    }

    @Override
    protected Collection<Long> extractContentIds(ExcelFinalMatchingRequestData finalMatchingRequestData) {
        return finalMatchingRequestData.getCandidates();
    }

    @Override
    public String getDescription() {
        return "DocAuthority Excel Final Component. Responsible for verifying matches on given set of word candidates";
    }

    @Override
    public String getName() {
        return "excelFinalMatchingComponent";
    }
}
