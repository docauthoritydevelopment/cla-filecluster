package com.cla.filecluster.util.solr.security;

import com.cla.common.constants.CatFileFieldType;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.solr.search.DelegatingCollector;
import org.apache.solr.search.ExtendedQueryBase;
import org.apache.solr.search.PostFilter;

import java.io.IOException;
import java.util.Arrays;

public class AccessControlQuery extends ExtendedQueryBase implements PostFilter {

    private String user;
    private String[] groups;

    public AccessControlQuery(String user, String groups) {
        this.user = user;
        this.groups = groups.split(",");
    }


    @Override
    public DelegatingCollector getFilterCollector(IndexSearcher searcher) {
        return new DelegatingCollector() {
            SortedSetDocValues allowRead;
            SortedSetDocValues allowWrite;
            SortedSetDocValues denyRead;
            SortedSetDocValues denyWrite;

            @Override
            protected void doSetNextReader(LeafReaderContext context) throws IOException {
                LeafReader reader = context.reader();
                allowRead = reader.getSortedSetDocValues(CatFileFieldType.ACL_READ.getSolrName());
                allowWrite = reader.getSortedSetDocValues(CatFileFieldType.ACL_WRITE.getSolrName());
                denyRead = reader.getSortedSetDocValues(CatFileFieldType.ACL_DENIED_READ.getSolrName());
                denyWrite = reader.getSortedSetDocValues(CatFileFieldType.ACL_DENIED_WRITE.getSolrName());
                super.doSetNextReader(context);
            }

            @Override
            public void collect(int doc) throws IOException {
                //if (isAllowed(allowRead.get(doc).utf8ToString(), user, groups)) {
                    super.collect(doc);
                //}
            }
        };
    }

    @Override
    public boolean getCache() {
        return false;  // never cache
    }

    @Override
    public int getCost() {
        return Math.max(super.getCost(), 100);  // never return less than 100 since we only support post filtering
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessControlQuery that = (AccessControlQuery) o;

        if (!Arrays.equals(groups, that.groups)) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = 19;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (groups != null ? Arrays.hashCode(groups) : 0);
        return result;
    }

}
