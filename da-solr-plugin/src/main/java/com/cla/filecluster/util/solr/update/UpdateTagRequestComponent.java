package com.cla.filecluster.util.solr.update;

import com.cla.common.constants.*;
import com.cla.common.tag.scope.PriorityScopedValueResolver;
import com.cla.common.workflow.WorkflowDataResolver;
import com.cla.filecluster.util.solr.search.DaJoinQParser;
import com.cla.filecluster.util.solr.util.AssociationUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.*;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.handler.component.ShardRequest;
import org.apache.solr.handler.component.ShardResponse;
import org.apache.solr.metrics.SolrMetricManager;
import org.apache.solr.metrics.SolrMetricProducer;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.request.SolrRequestInfo;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.search.*;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.CommitUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorChain;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by uri on 06-Jun-17.
 */
public class UpdateTagRequestComponent extends SearchComponent implements SolrCoreAware, SolrMetricProducer {


    private static Logger logger = LoggerFactory.getLogger(UpdateTagRequestComponent.class);

    private final static String DEFAULT_FIELD_TO_UPDATE = "tags";
    private final static int DEFAULT_PAGE_SIZE = 10000;

    private static final String ARG_TAGS_UPDATE_PAGE_SIZE = "solr.updateTags.pageSize";

    private SolrCore solrCore;

    private volatile long lastQueryTime = 0;

    private String lastQuery = "";

    private int pageSize = DEFAULT_PAGE_SIZE;
    private boolean autoCommit = true;
    private boolean softCommit = false;

    private NamedList initArgs;

    @Override
    public void init(NamedList args) {
        super.init(args);
        initArgs = new NamedList();
        initArgs.addAll(args);

        Object updateParams = args.get("updateParams");
        if (updateParams != null) {
            Object oVal = ((NamedList) updateParams).get("pageSize");
            if (oVal != null) {
                pageSize = (int) oVal;
            }
        }
        pageSize = getIntArgument(ARG_TAGS_UPDATE_PAGE_SIZE, pageSize);
        logger.info("Init: pageSize={}", pageSize, autoCommit, softCommit);
    }

    private boolean getBooleanArgument(String name, boolean defaultValue) {
        Boolean value = initArgs.getBooleanArg(name);
        return (value != null) ? value : defaultValue;
    }

    private int getIntArgument(String name, int defaultValue) {
        String value = (String) initArgs.get(name);
        return (value != null) ? Integer.valueOf(value) : defaultValue;
    }

    /**
     * Prepare is PROBABLY called once before sending the message to all the shards
     *
     * @param rb - a single object that follows the entire lifecycle of the user query and holds all relevant data on it
     * @throws IOException If there is a low-level I/O error.
     */
    @Override
    public void prepare(ResponseBuilder rb) throws IOException {

    }

    /**
     * Called once on each shard. The actual processing of the request
     *
     * @param rb - a single object that follows the entire lifecycle of the user query and holds all relevant data on it
     * @throws IOException If there is a low-level I/O error.
     */
    @Override
    public void process(ResponseBuilder rb) throws IOException {
        long startTime = System.currentTimeMillis();
        SolrQueryRequest req = rb.req;
        SolrParams params = req.getParams();
        handleLocalUpdateTagRequest(req, rb.rsp, startTime, params, req.getSearcher());
    }

    /**
     * Updates the local records in this shard. Set the value of the "Tag" parameter to the field "field" parameter.
     * Or remove the "Tag" value from the "field" parameter. Field must be a multivalued field.
     *
     * @param req       SOLR query request
     * @param rsp       SOLR query response
     * @param startTime start time of the operation
     * @param params    parameters
     * @param searcher  SOLR index sercher
     * @throws IOException If there is a low-level I/O error.
     */
    private void handleLocalUpdateTagRequest(SolrQueryRequest req, SolrQueryResponse rsp, long startTime, SolrParams params, SolrIndexSearcher searcher) throws IOException {
        UpdateRequestProcessor updateRequestProcessor = getUpdateRequestProcessor(req, rsp, params);
        String countString = params.get(SolrRequestParams.COUNT);
        int count = 1;
        if (countString != null) {
            try {
                count = Integer.parseUnsignedInt(countString);
            } catch (NumberFormatException e) {
                throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Update Tag Request: illegal value for CNT: " + countString);
            }
        }
        if (count > 10 || count < 1) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Update Tag Request count should be between 1 and 10");
        }
        if (count == 1) {
            // Original implementation -     single field update
            Object tag = params.get(SolrRequestParams.TAG);

            String tagOperation = params.get(SolrRequestParams.TAG_OP, SolrFieldModifiers.ADD);

            String field = params.get(SolrRequestParams.FIELD, DEFAULT_FIELD_TO_UPDATE);

            logger.trace("Start Update Field Request. IsShard={} tag={} tag_op={} field={}", params.getBool(ShardParams.IS_SHARD), tag, tagOperation, field);
            lastQuery = req.getParamString();

            Map<String, Object> fieldModifier = new HashMap<>(1);
            fieldModifier.put(tagOperation, tag);
            boolean singleValue = params.getBool(SolrRequestParams.TAG_SINGLE_VALUE, false);
            boolean multiValue = params.getBool(SolrRequestParams.TAG_MULTI_VALUE, false);
            int totalSize = pagedUpdate(req, rsp, params, searcher,
                    (queryResult) -> updateDocuments(field, req, searcher, updateRequestProcessor, queryResult, fieldModifier, singleValue, multiValue));
            logger.info("Finished Update Field Request (IsShard={} tag={} tag_op={} field={}) on {} documents in {} ms.", params.getBool(ShardParams.IS_SHARD), tag, tagOperation, field, totalSize, lastQueryTime);
            lastQueryTime = System.currentTimeMillis() - startTime;
        } else {
            // Handle mutiple fields operation
            /*
             ** Assumes a 'tags update' Query with the parameters: tag.op cnt field1 tag1 field2 tag2 ...
             *  Sample test URL:
             *       http://localhost:8983/solr/TestCatFiles/update/tags?indent=on&q=folderId:4&wt=json&cnt=2&field1=groupId&field2=userGroupId&tag1=aaaa&tag2=bbbb&tag.op=set
             */
            String tagOperation = params.get(SolrRequestParams.TAG_OP, "add");
            String[] fields = new String[count];
            List<Map<String, Object>> fieldModifiers = Lists.newArrayListWithCapacity(count);
            Map<String, Object> fieldModifier;
            int i;
            for (i = 1; i <= count; ++i) {
                String field = params.get(SolrRequestParams.FIELD + i);
                fields[i - 1] = field;
                String tag = params.get(SolrRequestParams.TAG + i);
                fieldModifier = Maps.newHashMapWithExpectedSize(1);
                fieldModifier.put(tagOperation, tag);
                fieldModifiers.add(fieldModifier);
            }
            if (logger.isTraceEnabled()) {
                logger.trace("Handle Update multi-fields request. IsShard={} values={} tag_op={} fields={}", params.getBool(ShardParams.IS_SHARD),
                        fieldModifiers.stream().map(fm -> (String) fm.get(tagOperation)).collect(Collectors.joining(",", "[", "]")),
                        tagOperation,
                        Arrays.asList(fields).toString());
            }
            lastQuery = req.getParamString();
            int size = pagedUpdate(req, rsp, params, searcher,
                    (queryResult) -> updateDocumentsMultiField(fields, req, searcher, updateRequestProcessor, queryResult, fieldModifiers));
            lastQueryTime = System.currentTimeMillis() - startTime;
            logger.debug("Finished Update multi-fields ({}) request on {} documents in {} ms.", count, size, lastQueryTime);
        }
    }

    private int pagedUpdate(SolrQueryRequest req, SolrQueryResponse rsp, SolrParams params, SolrIndexSearcher searcher, UpdateOperation updateOperation) throws IOException {
        SolrQueryInput solrQueryInput = prepareQuery(req, params);
        DocList queryResult = executeQuery(searcher, solrQueryInput);
        if (solrQueryInput.daJoinQuery) {
            resetRequestInfo(req, rsp);
        }
        while (queryResult.size() > 0) {
            logger.debug("**** Start Update field {} on {} documents, offset: {}", params.get("field"), queryResult.size(), solrQueryInput.offset);
            updateOperation.performUpdate(queryResult);
            logger.debug("**** End Update field {} on {} documents, offset: {}", params.get("field"), queryResult.size(), solrQueryInput.offset);
            solrQueryInput.offset += queryResult.size();
            if (queryResult.size() < solrQueryInput.pageSize) {
                break;
            }
            queryResult = executeQuery(searcher, solrQueryInput);
        }

        rsp.add("docsChanged", solrQueryInput.offset);

        return solrQueryInput.offset;
    }

    private void resetRequestInfo(SolrQueryRequest req, SolrQueryResponse rsp) {
        SolrRequestInfo.clearRequestInfo();
        SolrRequestInfo.setRequestInfo(new SolrRequestInfo(req, rsp));
    }

    private SolrQueryInput prepareQuery(SolrQueryRequest req, SolrParams params) {
        String defType = params.get(QueryParsing.DEFTYPE, QParserPlugin.DEFAULT_QTYPE);
        String q = params.get(CommonParams.Q);
        Query query = null;
        List<Query> filters = null;

        int flags = SolrIndexSearcher.GET_DOCLIST;
        boolean daJoinQuery = false;
        try {
            if (q != null) {
                QParser parser = QParser.getParser(q, defType, req);
                query = parser.getQuery();
            }

            String[] fqs = req.getParams().getParams(CommonParams.FQ);
            if (fqs != null && fqs.length != 0) {
                filters = new ArrayList<>();
                for (String fq : fqs) {
                    if (fq != null && fq.trim().length() != 0) {
                        daJoinQuery |= fq.contains(DaJoinQParser.NAME);
                        QParser fqp = QParser.getParser(fq, null, req);
                        filters.add(fqp.getQuery());
                    }
                }
            }
        } catch (SyntaxError e) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, e);
        }
        return new SolrQueryInput(query, filters, pageSize, daJoinQuery, flags);
    }

    @Override
    public String getDescription() {
        return "Update the tag for a set of documents based on the given query";
    }

    @Override
    public void inform(SolrCore core) {
        this.solrCore = core;

    }

    private UpdateRequestProcessor getUpdateRequestProcessor(SolrQueryRequest req, SolrQueryResponse rsp, SolrParams params) {
        UpdateRequestProcessorChain processorChain =
                req.getCore().getUpdateProcessingChain(params.get(UpdateParams.UPDATE_CHAIN));

        return processorChain.createProcessor(req, rsp);
    }

    /**
     * Update the documents on the local shard
     * This function can probably be rewritten to use pages of Add requests that are processed together in the lucene/solr index
     *
     * @param field                  field to update
     * @param req                    SOLR query request
     * @param searcher               SOLR index searcher
     * @param updateRequestProcessor update request processor
     * @param queryResult            list of SOLR documents
     * @param fieldModifier          map of fields/values to be updated
     * @param singleValue
     * @throws IOException If there is a low-level I/O error.
     */
    private void updateDocuments(String field, SolrQueryRequest req, SolrIndexSearcher searcher,
                                 UpdateRequestProcessor updateRequestProcessor, DocList queryResult,
                                 Map<String, Object> fieldModifier, boolean singleValue, boolean multiValue) throws IOException {
        List<String> ids = new ArrayList<>();
        if (queryResult.size() == 0) {
            return;
        }
        try {
            AddUpdateCommand addUpdateCommand = new AddUpdateCommand(req);
            DocIterator iterator = queryResult.iterator();
            while (iterator.hasNext()) {
                try {
                    updateSingleDocument(field, req, searcher, updateRequestProcessor, fieldModifier, ids, addUpdateCommand, iterator, singleValue, multiValue);
                }
                catch (Exception e){
                    logger.error("Failed to update single document, continue.", e);
                }
            }
        } finally {
            finalizeUpdateProcessor(req, updateRequestProcessor, ids);
        }

    }

    private void updateDocumentsMultiField(String[] fields, SolrQueryRequest req, SolrIndexSearcher searcher, UpdateRequestProcessor updateRequestProcessor, DocList queryResult, List<Map<String, Object>> fieldModifiers) throws IOException {
        logger.debug("Update tag on {} documents", queryResult.size());
        List<String> ids = new ArrayList<>();
        if (queryResult.size() == 0) {
            return;
        }
        try {
            AddUpdateCommand addUpdateCommand = new AddUpdateCommand(req);
            DocIterator iterator = queryResult.iterator();
            while (iterator.hasNext()) {
                try {
                    updateSingleDocumentMultiField(fields, req, searcher, updateRequestProcessor, fieldModifiers, ids, addUpdateCommand, iterator);
                }
                catch (Exception e){
                    logger.error("Failed to update single document, continue.", e);
                }
            }
        } finally {
            finalizeUpdateProcessor(req, updateRequestProcessor, ids);
        }
    }

    private void finalizeUpdateProcessor(SolrQueryRequest req, UpdateRequestProcessor updateRequestProcessor, List<String> ids) throws IOException {
        SolrCommitType commitType;
        try {
            commitType = SolrCommitType.valueOf(req.getParams().get(SolrRequestParams.COMMIT_TYPE, SolrCommitType.NONE.name()));
        } catch (IllegalArgumentException e) {
            commitType = SolrCommitType.NONE;
            logger.warn("Bad value {} for parameter {}. Fallback to default {}", req.getParams().get(SolrRequestParams.COMMIT_TYPE), SolrRequestParams.COMMIT_TYPE, commitType);
        }
        CommitUpdateCommand commitUpdateCommand = null;
        switch (commitType) {
            case HARD:
                commitUpdateCommand = new CommitUpdateCommand(req, false);
                break;
            case SOFT:
                commitUpdateCommand = new CommitUpdateCommand(req, false);
                commitUpdateCommand.softCommit = true;
                break;
            case SOFT_NOWAIT:
                commitUpdateCommand = new CommitUpdateCommand(req, false);
                commitUpdateCommand.softCommit = true;
                commitUpdateCommand.waitSearcher = false;
                break;
        }
        long start = System.currentTimeMillis();
        if (commitUpdateCommand != null) {
            updateRequestProcessor.processCommit(commitUpdateCommand);
        }
        updateRequestProcessor.finish();
        if (logger.isTraceEnabled()) {
            logger.debug("Commit ({}) and finish update request in {} ms for ids: {}", commitType, System.currentTimeMillis() - start, ids.toString());
        } else {
            logger.debug("Commit ({}) and finish update request for {} documents in {} ms", commitType, ids.size(), System.currentTimeMillis() - start);
        }
    }

    private void updateSingleDocumentMultiField(String[] fields, SolrQueryRequest req, SolrIndexSearcher searcher,
                                                UpdateRequestProcessor updateRequestProcessor, List<Map<String, Object>> fieldModifiers,
                                                List<String> ids, AddUpdateCommand addUpdateCommand, DocIterator iterator) throws IOException {
        String id = null;
        try {
            Document document = searcher.doc(iterator.nextDoc());
            SolrInputDocument solrInputDocument = new SolrInputDocument();
            addUpdateCommand.clear();
            addUpdateCommand.solrDoc = solrInputDocument;
            id = document.get("id");
            ids.add(id);
            addUpdateCommand.solrDoc.setField("id", id);
            for (int i = 0; i < fields.length; ++i) {
                if (fields[i] != null) {
                    addUpdateCommand.solrDoc.setField(fields[i], fieldModifiers.get(i));
                }
            }
            updateRequestProcessor.processAdd(addUpdateCommand);
            if (logger.isTraceEnabled()) {
                logger.trace("Update doc={} fields={} req={} modifiers={}", id, Arrays.asList(fields).toString(), req, fieldModifiers.toString());
            }
        }
        catch(Exception e){
            logger.error("Failed to update doc={} req={}", id, req, e);
            throw e;
        }
    }

    private void updateSingleDocument(String field, SolrQueryRequest req, SolrIndexSearcher searcher,
                                      UpdateRequestProcessor updateRequestProcessor, Map<String, Object> fieldModifier,
                                      List<String> ids, AddUpdateCommand addUpdateCommand, DocIterator iterator,
                                      boolean singleValue, boolean multiValue) throws IOException {
        Document document = searcher.doc(iterator.nextDoc());
        String id = document.get("id");
        try {
            SolrInputDocument solrInputDocument = new SolrInputDocument();
            addUpdateCommand.clear();
            addUpdateCommand.solrDoc = solrInputDocument;
            ids.add(id);
            addUpdateCommand.solrDoc.setField("id", id);


        // Case priority scoped tag, and action is not set then update behavior is different
        if (singleValue && !fieldModifier.containsKey("set")) {
            String tagValue = (String) fieldModifier.values().iterator().next();
            String[] existingScopedValues = document.getValues(field);
            String[] preChange = document.getValues(field);
            PriorityScopedValueResolver priorityScopedValueResolver = new PriorityScopedValueResolver(tagValue);
            existingScopedValues = priorityScopedValueResolver.resolveNewScopedValues(existingScopedValues, fieldModifier.containsKey("add"));
            fieldModifier = new HashMap<>();
            fieldModifier.put("set", existingScopedValues);
            AssociationUtils.setActiveAssociationValues(addUpdateCommand, document, preChange, existingScopedValues, field);
        }
        else if (multiValue && !fieldModifier.containsKey("set")) {
            String tagValue = (String) fieldModifier.values().iterator().next();
            String[] existingScopedValues = document.getValues(field);
            Set<String> existingValues = new HashSet<>();
            Collections.addAll(existingValues, existingScopedValues);
            boolean isAdd = fieldModifier.containsKey("add");
            if (isAdd) {
                existingValues.add(tagValue);
            } else {
                existingValues.remove(tagValue);
            }
            fieldModifier = new HashMap<>();
            fieldModifier.put("set", existingValues);
            AssociationUtils.addRemoveFromActiveAssociationValues(addUpdateCommand, document, field, tagValue, isAdd);
        }
        // Case wf data then update correct work flow
        else if (field.equals(CatFileFieldType.WORKFLOW_DATA.getSolrName())) {
            String[] existingWorkflowValues = document.getValues(field);
            String workflowDataValue = (String) fieldModifier.values().iterator().next();
            existingWorkflowValues = WorkflowDataResolver.resolveNewWorkflowDataValues(
                    existingWorkflowValues,
                    resolveOperation(fieldModifier),
                    workflowDataValue);
            fieldModifier = new HashMap<>();
            fieldModifier.put("set", existingWorkflowValues);
        }

        addUpdateCommand.solrDoc.setField(field, fieldModifier);

            if (searcher.getCore().getName().contains("CatFiles")) {
                fieldModifier = new HashMap<>();
                fieldModifier.put("set", System.currentTimeMillis());
                addUpdateCommand.solrDoc.setField(CatFileFieldType.UPDATE_DATE.getSolrName(), fieldModifier);
            }

            updateRequestProcessor.processAdd(addUpdateCommand);
            if (logger.isTraceEnabled()) {
                logger.trace("Update doc={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
            }
        }
        catch(Exception e){
            logger.error("Failed to update doc={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
            throw e;
        }
    }

    private SolrFieldOp resolveOperation(Map<String, Object> fieldModifier) {
        return SolrFieldOp.byName(fieldModifier.keySet().iterator().next());
    }

    /**
     * Run a query on the local solr shard index.
     * Can probably be replaced with SolrPluginUtils.runSimpleQuery(
     *
     * @param searcher SOLR index searcher
     * @return list of SOLR documents
     * @throws IOException If there is a low-level I/O error.
     */
    private DocList executeQuery(SolrIndexSearcher searcher, SolrQueryInput solrQueryInput) throws IOException {
        long start = System.currentTimeMillis();
        logger.debug("**** Fetching documents by {}", solrQueryInput);
        DocList docList = searcher.getDocList(solrQueryInput.query, solrQueryInput.filters, null, solrQueryInput.offset, pageSize, solrQueryInput.flags);
        logger.debug("**** took {} ms for {} with query input {}", System.currentTimeMillis() - start, docList.size(), solrQueryInput);
        return docList;
    }


    /**
     * Dispatch shard request in <code>STAGE_EXECUTE_QUERY</code> stage
     */
    @Override
    public int distributedProcess(ResponseBuilder rb) {
        SolrParams params = rb.req.getParams();
        logger.info("UpdateTagRequestComponent distributedProcess. rb.stage={} isShard={}", rb.stage, params.getBool(ShardParams.IS_SHARD));
        if (rb.stage < ResponseBuilder.STAGE_EXECUTE_QUERY)
            return ResponseBuilder.STAGE_EXECUTE_QUERY;
        if (rb.stage == ResponseBuilder.STAGE_EXECUTE_QUERY) {
            logger.trace("UpdateTagRequestComponent - create shard request");
            ShardRequest sreq = new ShardRequest();
            sreq.purpose = ShardRequest.PURPOSE_GET_TOP_IDS;
            sreq.params = new ModifiableSolrParams(rb.req.getParams());
            sreq.params.remove(ShardParams.SHARDS);
            rb.addRequest(this, sreq);
            return ResponseBuilder.STAGE_GET_FIELDS;
        }
        return ResponseBuilder.STAGE_DONE;
    }

    @Override
    public void handleResponses(ResponseBuilder rb, ShardRequest sreq) {
        logger.debug("UpdateTagRequestComponent - handle responses. purpose={}", sreq.purpose);
        if (sreq.purpose == ShardRequest.PURPOSE_GET_TOP_IDS) {
            logger.trace("ShardRequest.response.size: " + sreq.responses.size());
            SolrDocumentList responseDocs = new SolrDocumentList();
            long numFound = 0;
            for (ShardResponse srsp : sreq.responses) {
                logger.trace("ShardRequest.response.shard: " + srsp.getShard());
                SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse().getResponse().get("response");
                if (docs != null) {
                    logger.trace("ShardRequest.response.documents: " + docs.size());
                    numFound += docs.size();
                    responseDocs.addAll(docs);
                }
//                NamedList contentIds = (NamedList) srsp.getSolrResponse().getResponse().get(SimilarityCalculator.CONTENT_IDS);
//                if (contentIds != null) {
//                    logger.trace("ShardRequest.response.contentIds: " + contentIds.size());
//                    rb.rsp.add(SimilarityCalculator.CONTENT_IDS, contentIds);
//                }

            }
//            Map<Object,ShardDoc> resultIds = new HashMap<>();
            responseDocs.setNumFound(numFound);
            logger.trace("Write final response");
//            rb.rsp.add(RESPONSE_BUCKET, responseDocs);
        } else {
            super.handleResponses(rb, sreq);
        }
    }


    @Override
    public void initializeMetrics(SolrMetricManager manager, String registryName, String tag, String scope) {
        manager.registerGauge(this, registryName, () -> lastQueryTime, tag, true, "lastQueryTime", getCategory().toString(), scope);
        manager.registerGauge(this, registryName, () -> lastQuery, tag, true, "lastQuery", getCategory().toString(), scope);
    }

    private class SolrQueryInput {

        private final Query query;

        private final List<Query> filters;

        private final int flags;

        private final int pageSize;

        private final boolean daJoinQuery;

        public int offset = 0;

        public SolrQueryInput(Query query, List<Query> filters, int pageSize, boolean daJoinQuery, int flags) {
            this.query = query;
            this.filters = filters;
            this.pageSize = pageSize;
            this.daJoinQuery = daJoinQuery;
            this.flags = flags;
        }

        @Override
        public String toString() {
            return "SolrQueryInput{" +
                    "query=" + query +
                    ", filters=" + filters +
                    ", flags=" + flags +
                    ", offset=" + offset +
                    ", pageSize=" + pageSize +
                    '}';
        }
    }

    private interface UpdateOperation {
        void performUpdate(DocList queryResult) throws IOException;
    }
}