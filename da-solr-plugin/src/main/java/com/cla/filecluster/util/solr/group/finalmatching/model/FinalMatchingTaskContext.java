package com.cla.filecluster.util.solr.group.finalmatching.model;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.GrpHelperFetcher;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.cla.filecluster.util.solr.group.verify.UnmatchedContentCache;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinalMatchingTaskContext<T> {

    private SimilarityCalculator similarityCalculator;
    private Collection<T> subList;
    private List<PvAnalysisData> pvAnalysisDataList;
    private SolrPvDocument sourceDocument;

}
