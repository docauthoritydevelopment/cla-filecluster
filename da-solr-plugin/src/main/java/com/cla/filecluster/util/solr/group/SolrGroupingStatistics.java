package com.cla.filecluster.util.solr.group;

/**
 * Created by uri on 27-Feb-17.
 */
public class SolrGroupingStatistics {
    private long lastQueryTime;

    private long fetchSourcePvAnalysisDataTime;
    private long totalDuration = 0;
    private long analyzeSheetSimilarityTime;
    private long finalizeWorkbookSimilarityTime;
    private long convergeDocIdsTime;
    private long verifyCandidatesTime;

    public void setLastQueryTime(long lastQueryTime) {
        this.lastQueryTime = lastQueryTime;
    }

    public long getLastQueryTime() {
        return lastQueryTime;
    }

    public synchronized void addTotalDuration(long l) {
        totalDuration+=l;
    }

    public long getTotalDuration() {
        return totalDuration;
    }

    public synchronized void addFetchSourcePvAnalysisDataTime(long l) {
        fetchSourcePvAnalysisDataTime+=l;
    }

    public synchronized void addAnalyzeSheetSimilarityTime(long l) {
        analyzeSheetSimilarityTime+=l;
    }

    public synchronized void addFinalizeWorkbookSimilarityTime(long l) {
        finalizeWorkbookSimilarityTime+=l;
    }

    public synchronized void addConvergeDocIdsTime(long l) {
        convergeDocIdsTime+=l;
    }

    public synchronized void addVerifyCandidatesTime(long l) {
        verifyCandidatesTime+=l;
    }

    public long getFetchSourcePvAnalysisDataTime() {
        return fetchSourcePvAnalysisDataTime;
    }

    public long getAnalyzeSheetSimilarityTime() {
        return analyzeSheetSimilarityTime;
    }

    public long getFinalizeWorkbookSimilarityTime() {
        return finalizeWorkbookSimilarityTime;
    }

    public long getConvergeDocIdsTime() {
        return convergeDocIdsTime;
    }

    public long getVerifyCandidatesTime() {
        return verifyCandidatesTime;
    }
}
