package com.cla.filecluster.util.solr.group.finalmatching.word;

import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.finalmatching.AbstractFinalMatchingTaskExecutor;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTask;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WordFinalMatchingTaskExecutor extends AbstractFinalMatchingTaskExecutor {

    private final Map<Long, SimilarityMapKey> contentIdToSimilarityMapKey;

    public WordFinalMatchingTaskExecutor(SolrPvDocument sourceDocument,
                                         WordFinalMatchingRequestData finalMatchingRequestData,
                                         SimilarityCalculator similarityCalculator) {
        super(sourceDocument, similarityCalculator);
        contentIdToSimilarityMapKey = finalMatchingRequestData.getCandidates().stream()
                .collect(Collectors.toMap(SimilarityMapKey::getDocIdFromKey, Function.identity()));

    }

    @Override
    protected FinalMatchingTask newFinalMatchingTask(List<PvAnalysisData> pvAnalysisDataList) {
        List<SimilarityMapKey> subList = pvAnalysisDataList.stream().
                map(pvAnalysisData -> contentIdToSimilarityMapKey.get(pvAnalysisData.getContentId()))
                .collect(Collectors.toList());
        WordFinalMatchingTaskContext context = buildContext(pvAnalysisDataList, subList);
        return new WordFinalMatchingTask(context);
    }

    private WordFinalMatchingTaskContext buildContext(List<PvAnalysisData> pvAnalysisDataList, List<SimilarityMapKey> subList) {
        return new WordFinalMatchingTaskContext(similarityCalculator,subList,pvAnalysisDataList,sourceDocument);
    }
}
