package com.cla.filecluster.util.solr.group;

import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.MaturityLevel;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.repository.solr.ClaGroupingParams;
import com.cla.filecluster.service.files.pv.MaturityHelper;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.cla.filecluster.util.solr.PluginArgsExtractor;
import com.cla.filecluster.util.solr.SearchComponentMetricFilter;
import com.cla.filecluster.util.solr.group.candidates.ConvergeDocsAlgorithm;
import com.cla.filecluster.util.solr.group.fetchers.SolrAnalysisDataFetcher;
import com.cla.filecluster.util.solr.group.finalmatching.FinalMatchingQueryParams;
import com.cla.filecluster.util.solr.group.finalmatching.excel.ExcelFinalMatchingRequestData;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingRequestData;
import com.cla.filecluster.util.solr.group.finalmatching.word.WordFinalMatchingRequestData;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.cla.filecluster.util.solr.group.verify.VerifyCandidatesAlgorithm;
import com.cla.filecluster.util.solr.group.verify.VerifyCandidatesAlgorithmConfiguration;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Metric;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.cache.CacheStats;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.UnmodifiableIterator;
import org.apache.lucene.util.SmallFloat;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.ShardParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.handler.component.ShardRequest;
import org.apache.solr.handler.component.ShardResponse;
import org.apache.solr.metrics.SolrMetricManager;
import org.apache.solr.metrics.SolrMetricProducer;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.SolrIndexSearcher;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchersComponent.getFetchers;
import static java.util.stream.Collectors.toSet;

/**
 * Created by uri on 23/06/2015.
 */
public class ClaGroupingSearchComponent extends SearchComponent implements SolrCoreAware, SolrMetricProducer {


    private static Logger logger = LoggerFactory.getLogger(ClaGroupingSearchComponent.class);

    public static final String WORD_FINAL_MATCHING_HANDLER = "/wordFinalMatching";
    public static final String EXCEL_FINAL_MATCHING_HANDLER = "/excelFinalMatching";

    /*-- DUMMY TEST PARAMS TO REMOVE LATER --*/
    private static final String ARG_TEST_DROP_THRESHOLD = "testDropThreshold";
    private static final String ARG_TEST_MIN_FILTER_THRESHOLD = "testMinFilterThreshold";

    private static final String ARG_FINAL_MATCHING_BATCH_SIZE = "finalMatchingBatchSize";
    private static final String ARG_GROUPING_TYPE = "groupingType";
    private static final String ARG_CUTOFF_VAL = "cutOffValue";
    private static final String ARG_MAX_PAGE_SIZE = "maxPageSize";
    private static final String ARG_SKIP_PVS_BY_NAME = "skipAnalyzedPvsByPVName";
    private static final String ARG_DEBUG_INFO = "debugInfo";
    private static final String ARG_GROUPED_XLS_SHEETS_FIRST_THLD = "groupedExcelSheetsProcessingFirstThreshold";
    private static final String ARG_GROUPED_XLS_SHEETS_LAST_THLD = "groupedExcelSheetsProcessingLastThreshold";

    private static final String RESPONSE_BUCKET = "response";

    private SolrCore solrCore;
    private NamedList initArgs;
    private PluginArgsExtractor pluginArgsExtractor;
    private volatile SolrGroupingStatistics solrGroupingStatistics = new SolrGroupingStatistics();

    private ObjectMapper objectMapper = new ObjectMapper();
    private ConvergeDocsAlgorithm convergeDocsAlgorithm;
    private VerifyCandidatesAlgorithm verifyCandidatesAlgorithm;
    private VerifyCandidatesAlgorithmConfiguration algorithmConfig;
    private MaturityHelper maturityHelper;


    private Properties thresholds = new Properties();
    private Properties thresholds_ML_1 = new Properties();
    private Properties thresholds_ML_2 = new Properties();
    private Properties thresholds_ML_3 = new Properties();
    private Properties[] thresholdsByMl;
    private Map<String, Integer[]> maturityThresholdsMap = new HashMap<>();
    private Map<String, Integer> maturityPvGroupMap = new HashMap<>();

    private String lastQuery = "";
    private Counter numberOfRequests;
    private GroupingType groupingType;
    private float similarityExcelWorkbookAggregatedSimilarityThreshold;
    private String pluginThresholdByPVLowStr;
    private String pluginThresholdByPVMediumStr;
    private String maturityPvGroupsStr;
    private boolean pluginIsForceMaturityLevel = false;
    private boolean pluginIsForceWorkbookMaturityLevel = false;
    private int groupedExcelSheetsProcessingFirstThreshold = 8;
    private int groupedExcelSheetsProcessingLastThreshold = 4;
    private int finalMatchingBatchSize;


    public ClaGroupingSearchComponent() {
    }

    @SuppressWarnings("rawtypes")
    @Override
    /*
     * Check the solrConfig for groupingType and thresholds when solr loads up.
     */
    public void init(NamedList args) {
        logger.info("Init ClaGroupingSearchComponent ({})", getName());
        super.init(args);
        initArgs = new NamedList();
        initArgs.addAll(args);

        this.pluginArgsExtractor = PluginArgsExtractor.newExtractor(initArgs);
        String groupingTypeString = (String) args.get(ARG_GROUPING_TYPE);
        if (groupingTypeString == null) {
            throw new SolrException(SolrException.ErrorCode.SERVER_ERROR,
                    "Need to specify the grouping type for the CLA grouping search component");
        }

        groupingType = GroupingType.valueOf(groupingTypeString.toUpperCase());
        this.finalMatchingBatchSize = pluginArgsExtractor.getIntArgument(ARG_FINAL_MATCHING_BATCH_SIZE, 1000);
        algorithmConfig = new VerifyCandidatesAlgorithmConfiguration();
        algorithmConfig.setTestDropThreshold(pluginArgsExtractor.getBooleanArgument(ARG_TEST_DROP_THRESHOLD,
                false));
        algorithmConfig.setTestMinFilteredThreshold(pluginArgsExtractor.getIntArgument(ARG_TEST_MIN_FILTER_THRESHOLD,
                10000));

        groupedExcelSheetsProcessingFirstThreshold = pluginArgsExtractor.getIntArgument(ARG_GROUPED_XLS_SHEETS_FIRST_THLD,
                8);
        groupedExcelSheetsProcessingLastThreshold = pluginArgsExtractor.getIntArgument(ARG_GROUPED_XLS_SHEETS_LAST_THLD,
                4);
        similarityExcelWorkbookAggregatedSimilarityThreshold = Float.valueOf(
                pluginArgsExtractor.getArgument("similarityExcelWorkbookAggregatedSimilarityThreshold", "0.6"));

        String cutOffValue = (String) args.get(ARG_CUTOFF_VAL);
        if (cutOffValue != null && cutOffValue.startsWith("-")) {
            cutOffValue = null;
        }

        int maxPageSize = pluginArgsExtractor.getIntArgument(ARG_MAX_PAGE_SIZE, 50000);

        String skipAnalyzedPvsByName = (String) args.get(ARG_SKIP_PVS_BY_NAME);
        if (skipAnalyzedPvsByName == null) {
            throw new RuntimeException("Missing parameter " + ARG_SKIP_PVS_BY_NAME);
        }

        NamedList debugInfoParams = (NamedList) args.get(ARG_DEBUG_INFO);

        initThresholdProps(args);
        initThresholdProps(thresholds_ML_1, args, 1);
        initThresholdProps(thresholds_ML_2, args, 2);
        initThresholdProps(thresholds_ML_3, args, 3);
        thresholdsByMl = new Properties[]{thresholds, thresholds_ML_1, thresholds_ML_2, thresholds_ML_3};

        // Extract the per PV-name thresholds from solrconfig arguments
        initThresholdByPvProps(args);

        convergeDocsAlgorithm = new ConvergeDocsAlgorithm(debugInfoParams, groupingType, thresholdsByMl, maxPageSize,
                cutOffValue, skipAnalyzedPvsByName);

        logger.info("Finished Init of ClaGroupingSearchComponent. groupingType={}, cutOffValue={}",
                groupingType, Strings.nullToEmpty(cutOffValue));
//        dumpNormDebug();
    }


    /**
     * Inform happens after init. And we need the solr Core to init the fetchers
     *
     * @param core
     */
    @Override
    public void inform(SolrCore core) {
        this.solrCore = core;
        String solrCoreName = solrCore.getName();
        logger.info("Init (inform) FileCluster Grouping Component at solrCore: {}", solrCoreName);
        initVerifyCandidatesComponents();
    }


    private void initVerifyCandidatesComponents() {
        // Init maturity helper
        String maturityContentPvNames = pluginArgsExtractor.getArgument("maturity.model.contentPvNames",
                "PV_BodyNgrams,PV_SubBodyNgrams,PV_DocstartNgrams," +
                        "PV_CellValues,PV_Formulas,PV_ConcreteCellValues,PV_TextNgrams");
        fillMaturityLevelThresholds(pluginThresholdByPVLowStr, MaturityLevel.ML_Low);
        fillMaturityLevelThresholds(pluginThresholdByPVMediumStr, MaturityLevel.ML_Med);
        MaturityHelper.maturityThreshodsToPvGroups(maturityThresholdsMap, maturityPvGroupMap, maturityPvGroupsStr);
        maturityHelper = new MaturityHelper(maturityContentPvNames, false, true);
        maturityHelper.setMaturityPvGroupMap(maturityPvGroupMap);
        maturityHelper.setMaturityThresholdsMap(maturityThresholdsMap);


        verifyCandidatesAlgorithm = new VerifyCandidatesAlgorithm(algorithmConfig);
    }

    private void fillMaturityLevelThresholds(String maturityThreshodsByPV, MaturityLevel level) {
        if (maturityThreshodsByPV == null || maturityThreshodsByPV.isEmpty()) {
            return;
        }
        Set<String> pvtSet = Stream.of(maturityThreshodsByPV.split(",")).collect(toSet());
        for (String pvtDef : pvtSet) {
            String[] items = pvtDef.split(":", 2);
            String pvName = items[0];
            int val = Integer.valueOf(items[1]).intValue();
            Integer[] valArray = maturityThresholdsMap.get(pvName);
            if (valArray == null) {
                valArray = new Integer[MaturityLevel.MAX_VALUE];
                maturityThresholdsMap.put(pvName, valArray);
                for (int i = 0; i < valArray.length; ++i) valArray[i] = 0;
            }
            valArray[level.toInt()] = val;
        }
//		logger.debug("Set fillMaturityLevelThresholds {} to {}", level, maturityThreshodsByPV);
    }


    @SuppressWarnings("unused")
    private void dumpNormDebug() {
        logger.info("NORM_TABLE: i,f[i],f(i),1/(f(1/i),f(sqrt(i))^2,sqrt(f(i^2))");
        for (int i = 0; i < 256; i++) {
            logger.info("NORM_TABLE: {},{},{},{},{}", i,
                    SmallFloat.byte315ToFloat((byte) i),
                    SmallFloat.byte315ToFloat(SmallFloat.floatToByte315(1f * i)),
                    1f / SmallFloat.byte315ToFloat(SmallFloat.floatToByte315(1f / i)),
                    Math.pow(SmallFloat.byte315ToFloat(SmallFloat.floatToByte315(Double.valueOf(Math.sqrt(1d * i)).floatValue())), 2d),
                    Math.sqrt(SmallFloat.byte315ToFloat(SmallFloat.floatToByte315(1f * i * i)))
            );
        }
    }

    /**
     * Read an indvidual threshold value from the configuration
     */
    @SuppressWarnings("rawtypes")
    public void initThresholdProps(NamedList args) {
        initThresholdProps(thresholds, args, -1);
    }

    @SuppressWarnings("rawtypes")
    public void initThresholdProps(Properties props, NamedList args, int maturityLevel) {
        final String paramListName = (maturityLevel >= 0) ?
                ("thresholds_" + maturityLevel) :
                "thresholds";
        NamedList thresholdParams = (NamedList) args.get(paramListName);
        if (thresholdParams != null) {
            logger.info("initThresholdProps: read {}={}", paramListName, props.toString());
            for (int i = 0; i < thresholdParams.size(); i++) {
                SolrFileClusterUtils.initThreshold(props, thresholdParams.getName(i), thresholdParams.getVal(i));
            }
        }
    }

    @SuppressWarnings("rawtypes")
    public void initThresholdByPvProps(NamedList args) {
        NamedList thresholdByPVParams = (NamedList) args.get("maturityParams");
        if (thresholdByPVParams != null) {
            pluginThresholdByPVLowStr = (String) thresholdByPVParams.get("threshodsByPV.low");
            pluginThresholdByPVMediumStr = (String) thresholdByPVParams.get("threshodsByPV.medium");
            Boolean b = thresholdByPVParams.getBooleanArg("forceMaturityLevel");
            pluginIsForceMaturityLevel = (b != null && b);
            b = thresholdByPVParams.getBooleanArg("forceWorkbookMaturityLevel");
            pluginIsForceWorkbookMaturityLevel = (b != null && b);
            maturityPvGroupsStr = (String) thresholdByPVParams.get("maturity.pvGroups");

            logger.info("initThresholdByPvProps: read {}={}", "thresholdsByPVName:low", pluginThresholdByPVLowStr);
            logger.info("initThresholdByPvProps: read {}={}", "thresholdsByPVName:medium", pluginThresholdByPVMediumStr);
            logger.info("initThresholdByPvProps: read {}={}", "thresholdsByPVName:forceMaturityLevel", pluginIsForceMaturityLevel);
            logger.info("initThresholdByPvProps: read {}={}", "thresholdsByPVName:forceWorkbookMaturityLevel", pluginIsForceWorkbookMaturityLevel);
        }
    }

    @Override
    public void prepare(ResponseBuilder rb) {
    }

    @Override
    public void process(ResponseBuilder rb) {
        long startTime = System.currentTimeMillis();
        collectStartStatistics(rb);

        SolrQueryRequest req = rb.req;
        SolrParams params = req.getParams();

        logger.debug("Process a ClaGroupingSearchComponent request. type={}, IsShard={}", groupingType, params.getBool(ShardParams.IS_SHARD));
        SolrPvDocument originalDocument = getOriginalDocument(req, params, req.getSearcher());
        if (originalDocument == null) {
            logger.info("Aborting grouping process. req={}", req);
            return;
        }

        try {
            SolrFileClusterUtils.fillFileType(params, originalDocument);
            Collection<Long> matchingContentIds;
            if (GroupingType.WORKBOOK.equals(groupingType)) {
                matchingContentIds = findWorkBookGroups(req, params, originalDocument);
            } else {
                matchingContentIds = findWordOrSheetGroups(req, params, originalDocument, maturityHelper);
            }

            fillResponse(rb, matchingContentIds);
            collectEndStatistics(startTime);
        } catch (Exception e) {
            logger.error("error on process", e);
            throw new RuntimeException(e);
        }
    }

    private void fillResponse(ResponseBuilder rb, Collection<Long> matchingDocumentIds) {
        NamedList<Object> contentIds = new NamedList<>();
        for (Long documentId : matchingDocumentIds) {
            contentIds.add("contentId", documentId);
        }
        rb.rsp.add(SimilarityCalculator.CONTENT_IDS, contentIds);
    }

    private Collection<Long> findWordOrSheetGroups(SolrQueryRequest req, SolrParams params,
                                                   SolrPvDocument originalDocument, MaturityHelper maturityHelper)
            throws IOException {
        // First phase verify possible solr candidates
        long start = System.currentTimeMillis();
        int[] convergedDocIds = convergeDocsAlgorithm.getConvergedDocIds(req, params, originalDocument, maturityHelper,
                algorithmConfig.isTestDropThreshold());
        long step2Start = System.currentTimeMillis();
        solrGroupingStatistics.addConvergeDocIdsTime(step2Start - start);
        Collection<SimilarityMapKey> similarityMapKeys = verifyCandidatesAlgorithm.verifyCandidates(
                req, convergedDocIds, originalDocument);
        logger.info("Found {} matched word/sheet for contentId={} took={}", convergedDocIds.length,
                originalDocument.getId(), (System.currentTimeMillis() - start));

        // Second phase find final matching candidates
        if (!similarityMapKeys.isEmpty()) {
            WordFinalMatchingRequestData wordFinalMatchingRequestData = new WordFinalMatchingRequestData(
                    originalDocument.getId(), similarityMapKeys);
            return executeFinalMatchingQuery(wordFinalMatchingRequestData, WORD_FINAL_MATCHING_HANDLER);
        } else {
            return Lists.newArrayList();
        }

    }

    /**
     * Build Solr query which calls to the correct matching handler(WORD or EXCEL) and return list of ids
     *
     * @param finalMatchingRequestData
     * @param finalMatchingHandler
     * @return ids of final matching candidates
     * @throws IOException
     * @throws SolrServerException
     */
    private Collection<Long> executeFinalMatchingQuery(FinalMatchingRequestData finalMatchingRequestData,
                                                       String finalMatchingHandler) {
        List<Long> result = Lists.newLinkedList();
        UnmodifiableIterator<List> partitionIterator =
                Iterators.partition(finalMatchingRequestData.getCandidates().iterator(), finalMatchingBatchSize);
        partitionIterator.forEachRemaining(page -> {
            FinalMatchingRequestData requestDataPage = finalMatchingRequestData.extractPage(page);
            final NamedList<Long> contentIds = runPagedFinalMatchingQuery(requestDataPage, finalMatchingHandler);
            contentIds.forEach((key, contentId) -> result.add(contentId));
        });
        return result;
    }

    private NamedList<Long> runPagedFinalMatchingQuery(FinalMatchingRequestData pagedRequestData, String finalMatchingHandler) {
        try {
            SolrClient solrClient = getFetchers().getSolrAnalysisDataFetcher().getSolrClient();
            String requestDataParamValue = objectMapper.writeValueAsString(pagedRequestData);
            final SolrQuery query = new SolrQuery();
            query.setRequestHandler(finalMatchingHandler);
            query.add(FinalMatchingQueryParams.FINAL_MATCHING_REQUEST_DATA, requestDataParamValue);
            // TODO: add comment why do we need this shads qt here
            if (solrClient instanceof CloudSolrClient) {
                query.add("shards.qt", WORD_FINAL_MATCHING_HANDLER);
            }

            NamedList<Object> response = solrClient.query(query, SolrRequest.METHOD.POST).getResponse();
            return (NamedList<Long>) response.get(SimilarityCalculator.CONTENT_IDS);

        } catch (Exception e) {
            logger.error("Failed to execute final m" +
                            "atching query for sourceDocumentId {} on {} candidates",
                    pagedRequestData.getSourceDocumentId(), pagedRequestData.getCandidates().size());
        }
        return new NamedList<>();
    }

    private Collection<Long> findWorkBookGroups(SolrQueryRequest req, SolrParams params, SolrPvDocument originalDocument)
            throws IOException {
        long startFunc = System.currentTimeMillis();
        long contentId = originalDocument.getId();
        logger.debug("findWorkBookGroups on {}", contentId);
        PvAnalysisData pvAnalysisData = originalDocument.getPvAnalysisData();
        if (pvAnalysisData == null) {
            logger.warn("Missing PvAnalysisData on contentId: {}", originalDocument.getId());
            throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Missing PvAnalysisData on contentId:" + originalDocument.getId());
        }
        ExcelAnalysisMetadata analysisMeadata = (ExcelAnalysisMetadata) pvAnalysisData.getAnalysisMeadata();
        if (analysisMeadata == null) {
            logger.warn("Missing analysisMeadata on contentId: {} pvContentId={}", originalDocument.getId(), pvAnalysisData.getContentId());
            throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Missing PvAnalysisData on contentId:" + originalDocument.getId());
        }

        final Map<String, Float> sheetsDominanceRatio = PvAnalysisDataUtils.getExcelSheetsDominance(analysisMeadata);
        final List<String> sheetsSortedBySizeDesc = PvAnalysisDataUtils.sortSheetsBySize(sheetsDominanceRatio);

        float skippedSheetsDominance = PvAnalysisDataUtils.limitProcessedSheets(analysisMeadata, sheetsDominanceRatio, sheetsSortedBySizeDesc,
                groupedExcelSheetsProcessingFirstThreshold, groupedExcelSheetsProcessingLastThreshold);


        final Map<Long, Map<String, List<String>>> matchedSheetsByDoc = new HashMap<>();    // order all matched sheets by their doc with the matched part
        float accumulatedDominance = 0f;
        long start = System.currentTimeMillis();
        for (final String sheet : sheetsSortedBySizeDesc) {
            if (matchedSheetsByDoc.isEmpty() && accumulatedDominance > (1f - similarityExcelWorkbookAggregatedSimilarityThreshold)) {
                // No matched sheets and remaining sheets to process can not generate sufficient dominance -> stop file PV processing
                logger.debug("Stop analysis - no matches, accDom={} before part={} contentId={}", accumulatedDominance, sheet, contentId);
                break;
            }
            logger.debug("Analysing ExcelFile contentId={} {}", contentId, sheet);
            analyzeSheetSimilarity(req, params, originalDocument, matchedSheetsByDoc, sheet);
            accumulatedDominance += sheetsDominanceRatio.get(sheet);
        }
        long step2Start = System.currentTimeMillis();
        if (matchedSheetsByDoc.isEmpty()) {
            logger.debug("Skip finalize workbook similarity. No matches");
            return new ArrayList<>();
        }

        Collection<Long> matchingContentIds = findWorkbookFinalMatches(originalDocument, matchedSheetsByDoc,
                sheetsDominanceRatio, skippedSheetsDominance);
        long end = System.currentTimeMillis();
        solrGroupingStatistics.addAnalyzeSheetSimilarityTime(step2Start - start);
        solrGroupingStatistics.addFinalizeWorkbookSimilarityTime(end - step2Start);
        logger.info("Found {} matched workbooks for contentId={} took={}", matchingContentIds.size(), contentId, (System.currentTimeMillis() - startFunc));
        return matchingContentIds;
    }

    private Collection<Long> findWorkbookFinalMatches(SolrPvDocument originalDocument,
                                                      Map<Long, Map<String, List<String>>> matchedSheetsByDoc,
                                                      Map<String, Float> sheetsDominanceRatio,
                                                      float skippedSheetsDominance) {
        List<Long> contentIds = matchedSheetsByDoc.entrySet().stream()
                .map(e -> e.getKey())
                .collect(Collectors.toList());
        ExcelFinalMatchingRequestData excelFinalMatchingRequestData = new ExcelFinalMatchingRequestData(
                originalDocument.getId(),
                contentIds,
                matchedSheetsByDoc,
                sheetsDominanceRatio,
                skippedSheetsDominance);
        return executeFinalMatchingQuery(excelFinalMatchingRequestData, EXCEL_FINAL_MATCHING_HANDLER);
    }


    private void analyzeSheetSimilarity(SolrQueryRequest req,
                                        SolrParams params,
                                        SolrPvDocument originalDocument,
                                        Map<Long, Map<String, List<String>>> matchedSheetsByDoc,
                                        String sheet) throws IOException {
        PvAnalysisData pvAnalysisData = originalDocument.getPvAnalysisData();
        final Map<String, ClaSuperCollection<Long>> sheetsPvCollections = pvAnalysisData.getPvCollections().getCollections();
        final long start = System.currentTimeMillis();
        final ClaSuperCollection<Long> sheetSuperCollection = sheetsPvCollections.get(sheet);
        Map<String, ClaHistogramCollection<Long>> pvCollections = sheetSuperCollection.getCollections();
        long startConverge = System.currentTimeMillis();

        int partMaturityLevel = maturityHelper.getPartMaturityLevel(pvCollections);
        if (logger.isTraceEnabled()) {
            logger.trace("Analyze sheet similarity - start single-step for {} {} ml={} fileML={}...", originalDocument.getId(), sheet, partMaturityLevel, originalDocument.getOriginMaturityLevel());
        }

        int[] convergedDocIds = convergeDocsAlgorithm.getConvergedDocIds(req, params, originalDocument, sheet, pvCollections, partMaturityLevel, algorithmConfig.isTestDropThreshold());

        long duration = System.currentTimeMillis() - startConverge;
        solrGroupingStatistics.addConvergeDocIdsTime(duration);
        Collection<SimilarityMapKey> matchedSheets = verifyCandidatesAlgorithm.
                verifyCandidates(req, convergedDocIds, originalDocument);
        HashSet<SimilarityMapKey> matchedSheetsSet = Sets.newHashSet(matchedSheets);
        addToMatchedSheets(sheet, matchedSheetsByDoc, matchedSheetsSet);
        if (logger.isTraceEnabled()) {
            final long finish = System.currentTimeMillis();
            logger.trace("Finished handle sheet {} {} in {}ms. Got {} potential group candidates. Matched a total of {}",
                    originalDocument.getId(), sheet, finish - start, convergedDocIds.length, matchedSheets.size());
        }
    }

    private void addToMatchedSheets(String part, Map<Long, Map<String, List<String>>> matchedSheetsByDoc, Set<SimilarityMapKey> matchedSheets) {
        if (matchedSheets != null) {
            matchedSheets.forEach(matchedPartKey -> PvAnalysisDataUtils.mapPartToDoc(matchedSheetsByDoc, matchedPartKey, part));
        }
    }

    private SolrPvDocument getOriginalDocument(SolrQueryRequest req, SolrParams params, SolrIndexSearcher searcher) {
        String defType = QParserPlugin.DEFAULT_QTYPE;
        String originalDocumentQuery = params.get(ClaGroupingParams.OD);
        if (originalDocumentQuery == null) {
            logger.warn("Missing OD field on Grouping request. Can't get original document");
            return new SolrPvDocument();
        }
        try {
            Long contentId = Long.valueOf(originalDocumentQuery);
            logger.debug("Original document contentId={}", contentId);
            Map<Long, List<Integer>> res = new HashMap<>(1);
            if (!GroupingType.WORKBOOK.equals(groupingType)) {
                List<Integer> list = SolrFileClusterUtils.fileId2Internal(contentId, req, searcher, defType);
                res.put(contentId, list);
            }
            if (GroupingType.WORD.equals(groupingType) || GroupingType.WORKBOOK.equals(groupingType)) {
                long startTime = System.currentTimeMillis();
                SolrAnalysisDataFetcher solrAnalysisDataFetcher = getFetchers().getSolrAnalysisDataFetcher();
                PvAnalysisData pvAnalysisData = solrAnalysisDataFetcher.getPvAnalysisDataByContentId(contentId);
                if (pvAnalysisData == null) {
                    logger.warn("Could not get analysisData for contentId {}. Aborting similarity.", contentId);
                    return null;
                }
                solrGroupingStatistics.addFetchSourcePvAnalysisDataTime(System.currentTimeMillis() - startTime);
                return new SolrPvDocument(contentId, res, pvAnalysisData, pluginIsForceMaturityLevel, pluginIsForceWorkbookMaturityLevel);
            } else {
                return new SolrPvDocument(contentId, res, null, pluginIsForceMaturityLevel, pluginIsForceWorkbookMaturityLevel);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void handleResponses(ResponseBuilder rb, ShardRequest sreq) {
        logger.debug("ClaGroupingSearchComponent - handle responses. purpose={}", sreq.purpose);
        if (sreq.purpose == ShardRequest.PURPOSE_GET_TOP_IDS) {
            logger.trace("ShardRequest.response.size: " + sreq.responses.size());
            SolrDocumentList responseDocs = new SolrDocumentList();
            NamedList responseContentIds = new NamedList();
            long numFound = 0;
            for (ShardResponse srsp : sreq.responses) {
                logger.trace("ShardRequest.response.shard: " + srsp.getShard());
                SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse().getResponse().get("response");
                if (docs != null) {
                    logger.trace("ShardRequest.response.documents: " + docs.size());
                    numFound += docs.size();
                    responseDocs.addAll(docs);
                }
                NamedList contentIds = (NamedList) srsp.getSolrResponse().getResponse().get(SimilarityCalculator.CONTENT_IDS);
                if (contentIds != null) {
                    logger.trace("ShardRequest.response.contentIds: " + contentIds.size());
                    responseContentIds.addAll(contentIds);
                }

            }
            responseDocs.setNumFound(numFound);
            logger.trace("Write final response with {} contentIds", responseContentIds.size());
            rb.rsp.add(RESPONSE_BUCKET, responseDocs);
            rb.rsp.add(SimilarityCalculator.CONTENT_IDS, responseContentIds);
        } else {
            super.handleResponses(rb, sreq);
        }
    }

    private void collectEndStatistics(long startTime) {
        long took = System.currentTimeMillis() - startTime;
        solrGroupingStatistics.setLastQueryTime(took);
        solrGroupingStatistics.addTotalDuration(took);
    }

    private void collectStartStatistics(ResponseBuilder rb) {
        numberOfRequests.inc();
        lastQuery = rb.req.getParamString();
    }


    /**
     * Dispatch shard request in <code>STAGE_EXECUTE_QUERY</code> stage
     */
    @Override
    public int distributedProcess(ResponseBuilder rb) {
        SolrParams params = rb.req.getParams();
        logger.info("ClaGropuingSearchComponent distributedProcess. rb.stage={} isShard={}", rb.stage, params.getBool(ShardParams.IS_SHARD));
        if (rb.stage < ResponseBuilder.STAGE_EXECUTE_QUERY)
            return ResponseBuilder.STAGE_EXECUTE_QUERY;
        if (rb.stage == ResponseBuilder.STAGE_EXECUTE_QUERY) {
            logger.trace("ClaGropuingSearchComponent - create shard request");
            ShardRequest sreq = new ShardRequest();
            sreq.purpose = ShardRequest.PURPOSE_GET_TOP_IDS;
            sreq.params = new ModifiableSolrParams(rb.req.getParams());
            sreq.params.remove(ShardParams.SHARDS);
            rb.addRequest(this, sreq);
            return ResponseBuilder.STAGE_GET_FIELDS;
        }
        return ResponseBuilder.STAGE_DONE;
    }


    @Override
    public String getDescription() {
        return "DocAuthority Grouping component. Responsible for returning a document group. Initialized for " + groupingType;
    }


    @Override
    public void initializeMetrics(SolrMetricManager manager, String registry, String tag, String scope) {
        numberOfRequests = manager.counter(this, registry, "requests");
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getLastQueryTime(), tag, true, "lastQueryTime", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> lastQuery, tag, true, "lastQuery", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getTotalDuration(), tag, true, "totalDuration", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> convergeDocsAlgorithm.getLastQueryGroupCount(), tag, true, "lastQueryGroupCount", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getAnalyzeSheetSimilarityTime(), tag, true, "analyzeSheetSimilarityDuration", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getConvergeDocIdsTime(), tag, true, "convergeDocIdsDuration", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getFetchSourcePvAnalysisDataTime(), tag, true, "fetchSourcePvAnalysisDataDuration", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getFinalizeWorkbookSimilarityTime(), tag, true, "FinalizeWorkbookSimilarityDuration", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> solrGroupingStatistics.getVerifyCandidatesTime(), tag, true, "VerifyCandidatesTime", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats(), "UnMatchCandidatesCache", true, "stats", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats().hitCount(), "UnMatchCandidatesCache", true, "hitCount", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats().hitRate(), "UnMatchCandidatesCache", true, "hitRate", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats().evictionCount(), "UnMatchCandidatesCache", true, "evictionCount", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats().missRate(), "UnMatchCandidatesCache", true, "missRate", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats().missCount(), "UnMatchCandidatesCache", true, "missCount", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnmatchedContentCacheStats().loadExceptionRate(), "UnMatchCandidatesCache", true, "loadExceptionRate", getCategory().toString(), scope);

        this.registry = manager.registry(registry);
        Map<String, Metric> metrics = manager.getMetrics(registry, SearchComponentMetricFilter.of(this));
        if (metrics != null) {
            this.metricNames.addAll(metrics.keySet());
        }
    }

    public CacheStats getUnmatchedContentCacheStats() {
        return getFetchers().getUnmatchedContentCache().stats();
    }
}
