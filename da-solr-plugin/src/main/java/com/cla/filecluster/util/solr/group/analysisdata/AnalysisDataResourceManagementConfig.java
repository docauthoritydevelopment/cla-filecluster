package com.cla.filecluster.util.solr.group.analysisdata;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnalysisDataResourceManagementConfig {

    private int memoryLimitInKB;
    private float analysisDataSizeFactor;
    private boolean useAdaptiveSize;
    private boolean parallelLoad;
    private long timeoutInMinutes;
    private String fetchIntervalExp;
    private int maxDocumentSizeInKB;
    private double maxHeapMemoryThresholdPercentage;
    private int batchSize;
    private int waitTicksBeforeGC;
    private int loadConcurrency;
    private boolean dataCompressed;
    private Map<String, String> compatibilityMap;


}
