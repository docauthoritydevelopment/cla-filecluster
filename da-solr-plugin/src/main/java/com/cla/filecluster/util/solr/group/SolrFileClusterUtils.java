package com.cla.filecluster.util.solr.group;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.repository.solr.ClaGroupingParams;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import org.apache.lucene.search.Query;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.DocIterator;
import org.apache.solr.search.DocList;
import org.apache.solr.search.QParser;
import org.apache.solr.search.SolrIndexSearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by uri on 2/16/2017.
 */
public class SolrFileClusterUtils {

    private static final Logger logger = LoggerFactory.getLogger(SolrFileClusterUtils.class);

    static void initThreshold(Properties props, String name, Object threshold) {
        logger.info("thresholds {}:{}", name, threshold.toString());
        props.put(name, threshold);
    }

    static void fillFileType(SolrParams params, SolrPvDocument originalDocument) {
        String fileType = params.get(ClaGroupingParams.FT);
        if (fileType != null) {
            originalDocument.setFileType(FileType.valueOf(fileType));
        }
    }

    static Integer extractMaturityLevel(SolrParams params, String paramField) {
        Integer mlVal = params.getInt(paramField);
        if (mlVal == null) {
//            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Grouping Search Component requires " + ClaGroupingParams.OGC + " for each " + ClaGroupingParams.GF);
        }
        return mlVal;
    }

    static List<Integer> fileId2Internal(Long fileId, SolrQueryRequest req, SolrIndexSearcher searcher, String defType)  {
        if (fileId == null) {
            return null;
        }
        try {
            QParser parser = QParser.getParser("fileId:"+fileId.toString(), defType, req);
            Query query = parser.getQuery();
            DocList match = searcher.getDocList(query, null, null, 0, 99, 0);	// support up to 99 parts
            if (match.size() != 1) {
                logger.debug("Document ID converter: could not convert {}. Got {} results", fileId, match.size());
                return null;
            }
            List<Integer> res = new ArrayList<>();
            DocIterator iter = match.iterator();
            while (iter.hasNext()) {
                res.add(iter.next());
            }
            logger.debug("Document ID converter: {}->{}", fileId, res.toString());
            return res;
        } catch (Exception e) {
            logger.warn("Exception during fileId conversion {}. e={}", fileId, e, e);
            return null;
        }
    }
}
