package com.cla.filecluster.util.solr.group.finalmatching.excel;

import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingRequestData;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class ExcelFinalMatchingRequestData extends FinalMatchingRequestData<Collection<Long>> {

    private Map<Long, Map<String, List<String>>> matchedSheetsByDoc;
    private Map<String, Float> sheetsDominanceRatio;
    private float skippedSheetsDominance;

    public ExcelFinalMatchingRequestData() {
    }

    @Override
    public FinalMatchingRequestData<Collection<Long>> extractPage(Collection<Long> ids) {
        Map<Long, Map<String, List<String>>> partitionedMatchedSheetsByDoc = ids.stream()
                .filter(id -> matchedSheetsByDoc.containsKey(id))
                .collect(Collectors.toMap(id -> id, id -> matchedSheetsByDoc.get(id)));
        return new ExcelFinalMatchingRequestData(sourceDocumentId, ids, partitionedMatchedSheetsByDoc,
                sheetsDominanceRatio, skippedSheetsDominance);
    }

    public ExcelFinalMatchingRequestData(Long sourceDocumentId,
                                         Collection<Long> candidates,
                                         Map<Long, Map<String, List<String>>> matchedSheetsByDoc,
                                         Map<String, Float> sheetsDominanceRatio,
                                         float skippedSheetsDominance) {
        super(sourceDocumentId, candidates);
        this.matchedSheetsByDoc = matchedSheetsByDoc;
        this.sheetsDominanceRatio = sheetsDominanceRatio;
        this.skippedSheetsDominance = skippedSheetsDominance;
    }

    public Map<Long, Map<String, List<String>>> getMatchedSheetsByDoc() {
        return matchedSheetsByDoc;
    }

    public void setMatchedSheetsByDoc(Map<Long, Map<String, List<String>>> matchedSheetsByDoc) {
        this.matchedSheetsByDoc = matchedSheetsByDoc;
    }

    public Map<String, Float> getSheetsDominanceRatio() {
        return sheetsDominanceRatio;
    }

    public void setSheetsDominanceRatio(Map<String, Float> sheetsDominanceRatio) {
        this.sheetsDominanceRatio = sheetsDominanceRatio;
    }

    public float getSkippedSheetsDominance() {
        return skippedSheetsDominance;
    }

    public void setSkippedSheetsDominance(float skippedSheetsDominance) {
        this.skippedSheetsDominance = skippedSheetsDominance;
    }


}
