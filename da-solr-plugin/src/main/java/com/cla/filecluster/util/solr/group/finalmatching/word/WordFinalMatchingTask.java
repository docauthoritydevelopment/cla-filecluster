package com.cla.filecluster.util.solr.group.finalmatching.word;

import com.cla.common.domain.dto.pv.GroupingResult;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.ContentIdGroupsPool;
import com.cla.filecluster.service.files.pv.PvAnalysisDataPool;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTask;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class WordFinalMatchingTask extends FinalMatchingTask<SimilarityMapKey, WordFinalMatchingTaskContext> {

    private static final Logger logger = LoggerFactory.getLogger(WordFinalMatchingTask.class);

    public WordFinalMatchingTask(WordFinalMatchingTaskContext finalMatchingContext) {
        super(finalMatchingContext);
    }

    @Override
    public Pair<GroupingResult, Optional<SimilarityMapKey>> analyseMatch(SimilarityMapKey groupCandidate,
                                                                         PvAnalysisDataPool pvAnalysisDataPool,
                                                                         ContentIdGroupsPool contentPool) {
        SolrPvDocument sourceDocument = finalMatchingTaskContext.getSourceDocument();
        GroupingResult groupingResult = analyzeWordFileSimilarityMapPotentialCandidate(sourceDocument.getId(),
                sourceDocument.getPvAnalysisData(), getCandidateId(groupCandidate), pvAnalysisDataPool);

        if (groupingResult != null && groupingResult.isMatch()) {
            return Pair.of(groupingResult, Optional.of(groupCandidate));
        }

        return Pair.of(groupingResult, Optional.empty());
    }

    @Override
    public long getCandidateId(SimilarityMapKey entry) {
        return entry.getDocIdFromKey();
    }

    private GroupingResult analyzeWordFileSimilarityMapPotentialCandidate(long sourceDocumentId,
                                                                          PvAnalysisData doc1PvAnalysisData,
                                                                          Long groupCandidate,
                                                                          PvAnalysisDataPool pvAnalysisDataPool) {
        final int simCalc2;
        final StringBuilder sim2DebInfo = new StringBuilder();

        try {
            SimilarityCalculator similarityCalculator = finalMatchingTaskContext.getSimilarityCalculator();
            simCalc2 = similarityCalculator.getWordSimCalc2(doc1PvAnalysisData, sim2DebInfo, groupCandidate,
                    pvAnalysisDataPool);
        } catch (RuntimeException e) {
            logger.error("Failed to analyze similarity between contents {} and {}. Got Exception.", sourceDocumentId, groupCandidate, e);
            return null;
        }
        final String simCalc = "?";
        GroupingResult groupingResult;
        if (simCalc2 != 0) {
            if (logger.isTraceEnabled()) {
                logger.trace("Word similarity qualified code=()/() sourceDocumentId: {}, groupCandiate: {} " +
                        simCalc, simCalc2, sourceDocumentId, groupCandidate);
            }
            groupingResult = new GroupingResult(simCalc, simCalc2, '?', true);
        } else {
            if (logger.isTraceEnabled()) {
                logger.trace("Word similarity disqualified code={}/{} for contents {} and {}",
                        simCalc, simCalc2, sourceDocumentId, groupCandidate);
            }
            groupingResult = GroupingResult.falseGroupingResult(simCalc, simCalc2);
        }
        return groupingResult;
    }

}