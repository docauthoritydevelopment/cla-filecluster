package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;

import java.io.IOException;

public abstract class ClaScorer extends Scorer {
    /**
     * Constructs a Scorer
     *
     * @param weight The scorers <code>Weight</code>.
     */
    protected ClaScorer(Weight weight) {
        super(weight);
    }

    protected abstract int freq() throws IOException;
}
