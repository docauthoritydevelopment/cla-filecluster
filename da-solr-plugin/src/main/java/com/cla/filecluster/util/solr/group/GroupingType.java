package com.cla.filecluster.util.solr.group;

/**
 * Created by uri on 30/06/2015.
 */
public enum GroupingType {
    WORD,
    SHEET,
    WORKBOOK
}
