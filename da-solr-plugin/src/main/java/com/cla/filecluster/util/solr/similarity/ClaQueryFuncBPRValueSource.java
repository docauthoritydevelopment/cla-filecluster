package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexReaderContext;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.function.FunctionValues;
import org.apache.lucene.queries.function.ValueSource;
import org.apache.lucene.queries.function.docvalues.BoolDocValues;
import org.apache.lucene.queries.function.valuesource.QueryValueSource;
import org.apache.lucene.queryparser.flexible.core.QueryNodeException;
import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
 * Usage:
 * http://localhost:9080/PVS/query?q=PV_field:(token token^boost ...)&fl=score,id&rows=10000&fq=query({!func v=clabpr($q)})&fq={!frange l=0}query($q) 
 * 
 * http://localhost:9080/PVS/query?q=PV_SubBodyNgrams:(11EBB9A6444309B5D^0.987654 45903CF1A181096C B9DBA2FFB813CD3C 43EBF172B568ABF6)&fl=score,id&rows=10000&debug=true&fq=query({!func v=clabpr($q)})&fq={!frange l=0}query($q)
 */
public class ClaQueryFuncBPRValueSource extends ValueSource {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaQueryFuncBPRValueSource.class);

//	protected ValueSource source;
	private final  float minScore;
	private final  float bprFactor;
	private final  String field;
	private final List<String> tokens;
	private List<TokenItem> tiList = null;
	private QueryValueSource qvs = null;
	private boolean processingDone = false;
	private final String debDocId;
	private final BprStats stats = new BprStats();
	private final long startTs;
	
	public ClaQueryFuncBPRValueSource(final String field, final List<String> tokens, final float minScore, final float bprFactor, final String debDocId, final long startTs) {
		this.field = field;
		this.tokens = tokens;
		this.minScore = minScore;
		this.bprFactor = bprFactor;
		this.debDocId = debDocId;
		this.startTs = startTs;
		if (logger.isTraceEnabled())
		 {
			logger.trace("new ClaQueryFuncBPRValueSource={}",toTraceString());
//		else if (logger.isDebugEnabled())
//			logger.debug("new ClaQueryFuncBPRValueSource={}",toDebugString());
		}
	}

    @Override
    public FunctionValues getValues(Map fcontext, LeafReaderContext readerContext) throws IOException {
        if (!processingDone) {
            final long tsSnapshot1 = System.currentTimeMillis();
            processingDone = true;	// Allow one time processing (in case of failure)
            tiList = convertTokens(tokens, fcontext, readerContext.parent);
            if (logger.isTraceEnabled()) {
                logger.trace("tiList created: {}",tiList.toString());
            }
            // Process df stats and set 'dropped' tokens
            processBpr();
//			logger.debug("BPR-Stats for {} {}: {}",debDocId,field, stats);
            final long tsSnapshot2 = System.currentTimeMillis();
            if (logger.isDebugEnabled()) {
                if ((hashCode()%200)==0) {
                    logger.debug("BPR-Stats titles,part@doc,field,{},tokenList,dTime1ms,dTime1ms", stats.getToStringTitle());
                }
                logger.debug("BPR-Stats,{},{},{},{},{},{}",debDocId,field,stats, toDfString(tiList), tsSnapshot1-startTs,tsSnapshot2-tsSnapshot1);
            }
            final String queryStr = getUpdatedQuery(tiList, 1f,minScore);
            logger.trace("updated query string: {}",queryStr);
            qvs = buildQuery(queryStr);
            tiList.clear();	// free resources ASAP
            tiList = null;
        }
        if (qvs!=null) {
            return qvs.getValues(fcontext, readerContext);
        }

        return (new BoolDocValues(this) {
            @Override
            public boolean boolVal(final int doc) {
                return false;
            }
        });
    }

	private QueryValueSource buildQuery(final String queryStr) {
		QueryValueSource newQvs = null;
		try {
			final Query q = QueryParserUtil.parse(queryStr, new String[] {field}, new BooleanClause.Occur[]{BooleanClause.Occur.SHOULD}, new StandardAnalyzer());
			logger.trace("created query: {}",q.toString());
			newQvs = new QueryValueSource(q, 0f);
		} catch (final QueryNodeException e) {
			logger.error("Error parsing query '{}'", queryStr, e);
		}
		return newQvs;
	}

	private void processBpr() {
		// Input: tiList, bprFactor
		// Output tiList.dropped; stats
		calcStats();
		// TODO: process df stats and set 'dropped' tokens
		markDropped();		
	}

	private void markDropped() {
		tiList.stream().filter(ti->shouldDrop(ti.getDf())).forEach(ti->{
			ti.setDropped(true);
			++stats.countDropped;
			stats.sumDropped += ti.getDf();
		});
	}

	private boolean shouldDrop(final int df) {
		return (df > stats.average*bprFactor);
	}

	private void calcStats() {
		tiList.stream().mapToInt(ti->ti.getDf())
		.forEach(df->{
			++stats.count;
			stats.sum += df;
			stats.sumSquare += df*df;
		});
		stats.average = (stats.count==0)?0f:((float)stats.sum)/stats.count;
		stats.stdDev = (stats.count==0)?0f:((float)Math.sqrt((((double)stats.sumSquare)/stats.count) - stats.average*stats.average));
	}

	private String getUpdatedQuery(final List<TokenItem> tiList, final float f, final float minScoreThreshold) {
		return tiList.stream().filter(ti->!ti.isDropped())
				.map(ti->ti.getToken()+((ti.getBoost()==1f)?"":String.format("^%.3f", ti.getBoost())))
				.collect(Collectors.joining(" ", field+":(", ")"));
	}

	private List<TokenItem> convertTokens(final List<String> tokens, 
			@SuppressWarnings("rawtypes") final Map fcontext, final IndexReaderContext readerContext) {
	    final IndexSearcher searcher = (IndexSearcher)fcontext.get("searcher");
	    final IndexReader ir = searcher.getIndexReader();
		return tokens.stream().map(t->(new TokenItem(t, ir))).collect(Collectors.toList());
	}

	private class TokenItem {
		private String token = null;
		private float boost = 1f;
		private boolean dropped = false;
		int df;
		long ttf;
		
		public TokenItem(final String tokenStr, final IndexReader reader) {
			final String[] split = tokenStr.split("\\^",2);
			this.token = new String(split[0]);
			if (split.length==2) {
				boost = Float.parseFloat(split[1]);
			}
			try {
				final Term t = new Term(field, token);
				df = reader.docFreq(t);
				ttf = reader.totalTermFreq(t);
			} catch (final Exception e) {
				logger.warn("Error creating termVector for field {} t={}. {}",field,tokenStr,e.getMessage());
			}
		}

		public float getBoost() {
			return boost;
		}

		@SuppressWarnings("unused")
		public void setBoost(final float boost) {
			this.boost = boost;
		}

		public boolean isDropped() {
			return dropped;
		}

		public void setDropped(final boolean dropped) {
			this.dropped = dropped;
		}

		public String getToken() {
			return token;
		}

		public int getDf() {
			return df;
		}

		@SuppressWarnings("unused")
		public long getTtf() {
			return ttf;
		}

		@Override
		public String toString() {
			return "ti:{" + token +  ", df=" + df + ", ttf=" + ttf + ", b=" + boost + ", dpped=" + (dropped?1:0) + "}";
		}
	}	// class TokenItem

	public static String toDfString(final List<TokenItem> list) {
		final int[] hist=new int[50];
		list.stream().filter(ti->ti.getDf()<hist.length).forEach(ti->hist[ti.getDf()]++);
		return
				((hist[0]==0?"":""+hist[0]+" zero df tokens! " + // dump tokens with zero doc-frequency
						list.stream().filter(ti->ti.getDf()==0)
						.map(ti->String.valueOf(ti.getToken())).collect(Collectors.joining(" ","{","} "))))
				+
				IntStream.iterate(1, inx->inx+1).limit(hist.length-1).filter(inx->hist[inx]>0)
				.mapToObj(inx->""+inx+"x"+hist[inx]).collect(Collectors.joining(" ", "{", ";"))
				+
				list.stream().filter(ti->ti.getDf()>=hist.length)
				.map(ti->String.valueOf(ti.getDf())).collect(Collectors.joining(" ", "", "} "))
//				+
//				list.toString()
				;
	}

	private class BprStats {
		public int count = 0;
		public int sum = 0;
		public long sumSquare = 0;
		public int countDropped = 0;
		public int sumDropped = 0;
		public float stdDev;
		public float average;
		
		
		public String getToStringTitle() {
			return "count,average,countDropped,cntDrp%,sumDropped,sumDrp%,stdDev";
		}
		@Override
		public String toString() {
			return "" + count + "," + average + "," + countDropped  + "," + ((1000*countDropped/count)/10f) + "%," + 
					+ sumDropped + "," + ((1000*sumDropped/sum)/10f) + "%," + stdDev;
		}
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof ClaQueryFuncBPRValueSource)) {
			return false;
		}
		final ClaQueryFuncBPRValueSource other = (ClaQueryFuncBPRValueSource)o;
		if (field == null || (other.field!=null && !field.equals(other.field))) {
			return false;
		}
		if (tokens == null || (other.tokens!=null && !tokens.equals(other.tokens))) {
			return false;
		}
		return true;
	}
	@Override
	public int hashCode() {
		return field.hashCode()+tokens.hashCode();
	}
	@Override
	public String description() {
		return toString();
	}

	@Override
	public String toString() {
		return "ClaQueryFuncBPRValueSource [field=" + field + ", tokens="
				+ (tokens==null?"null":tokens) + ", tiList=" + (tiList==null?"null":tiList) + "]";
	}
	public String toTraceString() {
		return "{debDocId=" + debDocId + "field=" + field + ", tokens="
				+ (tokens==null?"null":tokens) + ", tiList=" + (tiList==null?"null":tiList) + "}";
	}
	public String toDebugString() {
		return "{debDocId=" + debDocId + "field=" + field + ", #tokens="
				+ (tokens==null?-1:tokens.size()) + ", #tiList=" + (tiList==null?-1:tiList.size()) + "}";
	}
}
