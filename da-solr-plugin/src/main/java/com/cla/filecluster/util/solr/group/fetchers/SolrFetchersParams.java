package com.cla.filecluster.util.solr.group.fetchers;

/**
 * Created By Itai Marko
 */
public interface SolrFetchersParams {


    String ARG_USE_UNMATCHED_CONTENT_CACHE = "unmatchedContentCache.active";
    String ARG_CACHE_UNMATCHED_CONTENT_MAX_SIZE = "unmatchedContentCacheMaxSize";
    String ARG_CACHE_UNMATCHED_CONTENT_CACHE_CONCURRENCY = "unmatchedContentCache.concurrency";
    String ARG_UNMATCHED_CONTENT_CACHE_BASE_URL = "unmatchedContentCache.baseUrl";
    String ARG_UNMATCHED_CONTENT_CACHE_MAX_CONNECTIONS = "unmatchedContentCache.maxConnections";
    String ARG_UNMATCHED_CONTENT_CACHE_MAX_CONNECTIONS_PER_HOST = "unmatchedContentCache.maxConnectionsPerHost";
    String ARG_UNMATCHED_CONTENT_CACHE_MAX_CONNECTION_TIMEOUT = "unmatchedContentCache.timeout";



    String ARG_GRP_HELPER_BASE_URL = "grpHelper.baseUrl";
    String ARG_GRP_HELPER_MAX_CONNECTIONS = "grpHelper.maxConnections";
    String ARG_GRP_HELPER_MAX_CONNECTIONS_PER_HOST = "grpHelper.maxConnectionsPerHost";
    String ARG_GRP_HELPER_CONNECTION_TIMEOUT = "grpHelper.timeout";

    String ARG_SOLR_FETCHERS_ANALYSIS_DATA_BASE_URL = "analysisData.baseUrl";
    String ARG_SOLR_FETCHERS_ANALYSIS_DATA_MAX_CONNECTIONS = "analysisData.maxConnections";
    String ARG_SOLR_FETCHERS_ANALYSIS_DATA_MAX_CONNECTIONS_PER_HOST = "analysisData.maxConnectionsPerHost";
    String ARG_SOLR_FETCHERS_ANALYSIS_DATA_CONNECTION_TIMEOUT = "analysisData.socketTimeout";
    String ARG_SOLR_FETCHERS_ANALYSIS_DATA_COMPRESSED = "analysisData.compressed";
    String ARC_SOLR_FETCHERS_ANALYSIS_DATA_COMPATIBILITY_MAP = "analysisData.compatibilityMap";
}
