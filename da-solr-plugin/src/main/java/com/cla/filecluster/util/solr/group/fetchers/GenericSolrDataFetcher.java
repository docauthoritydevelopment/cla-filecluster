package com.cla.filecluster.util.solr.group.fetchers;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public abstract class GenericSolrDataFetcher {
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(GenericSolrDataFetcher.class);

    public static String SolrURLPrefix = "http://localhost:8983/solr/";

    private static int operationsRetryCount = 3;
    private static long operationsSleepSec = 10;
    private static boolean applyRetryStrategy = true;
    protected final SolrClient solrClient;

    public GenericSolrDataFetcher(SolrClient solrClient) {
        this.solrClient = solrClient;
    }

    protected static Map<String, Object> atomicSetValue(Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put("set", value);
        return fieldModifier;
    }

    public SolrClient getSolrClient() {
        return solrClient;
    }

    public List<SolrDocument> findDocuments(Collection<Long> contentIds) {
        if (contentIds.size() == 0) {
            return new ArrayList<>();
        }
        //id:1 OR id:2 OR id:3
        final SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.setRows(contentIds.size());   // Overrides Solr default page size - paging logic should be implemented at higher levels to maintain scalability

        //id:(1 2 3)
        String fq = contentIds.stream().map(l -> String.valueOf(l)).collect(Collectors.joining(" ", "id:(", ")"));
        query.addFilterQuery(fq);
        try {
            long ts = 0;
            if (logger.isDebugEnabled()) {
                ts = System.currentTimeMillis();
            }
//            QueryResponse queryResponse = getSolrClient().query(query, SolrRequest.METHOD.POST);
            QueryResponse queryResponse = runWithRetry(() -> getSolrClient().query(query, SolrRequest.METHOD.POST),
                    "query(" + query.toQueryString() + ")");
            SolrDocumentList results = queryResponse.getResults();
            if (logger.isDebugEnabled()) {
                logger.debug("findDocuments: asked for {} items, {} returned in {}ms", contentIds.size(), queryResponse.getResults().size(), System.currentTimeMillis() - ts);
            }
            return queryResponse.getResults();
        } catch (SolrServerException e) {
            logger.error("Failed to find Solr documents by ids - solr exception", e);
            throw new RuntimeException("Failed to find Solr documents by ids- solr exception", e);
        } catch (IOException e) {
            logger.error("Failed to find Solr documents by ids - IOException", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        } catch (Exception e) {
            logger.error("Failed to find Solr documents by ids - unknown exception", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        }
    }

    public QueryResponse runQuery(SolrQuery query) {
        try {
            return getSolrClient().query(query);
        } catch (SolrServerException e) {
            logger.error("Failed to run query - solr exception", e);
            throw new RuntimeException("Failed to run query - solr exception", e);
        } catch (IOException e) {
            logger.error("Failed to run query - IOException", e);
            throw new RuntimeException("Failed to run query - solr exception", e);
        } catch (Exception e) {
            logger.error("Failed to run query - unknown exception", e);
            throw new RuntimeException("Failed to run query - solr exception", e);
        }
    }

    public SolrDocument getDocument(long contentId) {
        final SolrQuery query = new SolrQuery();
        query.setQuery("id:" + contentId);
//        query.addFilterQuery("id:" + );
        try {
//            QueryResponse queryResponse = getSolrClient().query(query);
            QueryResponse queryResponse = runWithRetry(() -> getSolrClient().query(query),
                    "query(" + query.toQueryString() + ")");
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                logger.debug("Unable to find document by id {}. Query: {}", contentId, query.toString());
//                logger.debug("Got 0 results for find category file. Solr Query: {}",query.getQuery());
                return null;
            }
            return queryResponse.getResults().get(0);
        } catch (SolrServerException e) {
            logger.error("Failed to find document by id - solr exception", e);
            throw new RuntimeException("Failed to find document by id - solr exception", e);
        } catch (IOException e) {
            logger.error("Failed to find Solr documents by ids - IOException", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        } catch (Exception e) {
            logger.error("Failed to find Solr documents by ids - unknown exception", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        }

    }

    protected void setFieldIfNotNull(SolrInputDocument doc, String fieldName, Object value) {
        if (value != null) {
            doc.setField(fieldName, value);
        }
    }

    private <T> T runWithRetry(RetryCallback<T> callback, String callName) throws Exception {
        T response = null;
        int retryCount = 0;
        if (applyRetryStrategy) {
            do {
                retryCount++;
                try {
                    response = callback.doSolrCall();
                    return response;
                } catch (Exception e) {
                    if (isDoNotRetryException(e)) {
                        logger.trace("Do not retry: #{} for {} err={}", retryCount, callName, e.getClass().toString());
                        throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Retry exhosted", e);
                    } else if (retryCount >= operationsRetryCount) {
                        logger.error("Failed all retries for {}. Exception: {}", callName, e.getMessage());
                        throw new SolrServerException("Retry exhosted for: " + callName, e);
                    }
                    long sleepSeconds = operationsSleepSec * retryCount;  // wait more as we go ...
                    logger.warn("Retry #{} for {} err={}. Rerun in {} seconds", retryCount, callName, e, sleepSeconds);
                    try {
                        Thread.sleep(sleepSeconds * 1000);
                    } catch (InterruptedException e1) {
                    }
                }
            } while (response == null);
        } else {
            response = callback.doSolrCall();
        }
        return response;
    }

    public interface RetryCallback<T> {
        T doSolrCall() throws Exception;
    }

    private boolean isDoNotRetryException(final Throwable lastThrowable) {
        if (lastThrowable == null) {
            return false;
//        } else if (lastThrowable instanceof HttpSolrClient.RemoteSolrException) {
//            // 	SolrException - is it too general?
//            return true;
        } else if (lastThrowable instanceof NullPointerException) {
            return true;
        } else if (lastThrowable.getMessage() != null && lastThrowable.getMessage().contains("org.apache.solr.search.SyntaxError")) {
            return true;
        }
        return false;
    }

    public static int getOperationsRetryCount() {
        return operationsRetryCount;
    }

    public static void setOperationsRetryCount(int value) {
        operationsRetryCount = value;
    }

    public static long getOperationsSleepSec() {
        return operationsSleepSec;
    }

    public static void setOperationsSleepSec(long value) {
        operationsSleepSec = value;
    }

    public static boolean isApplyRetryStrategy() {
        return applyRetryStrategy;
    }

    public static void setApplyRetryStrategy(boolean value) {
        applyRetryStrategy = value;
    }
}
