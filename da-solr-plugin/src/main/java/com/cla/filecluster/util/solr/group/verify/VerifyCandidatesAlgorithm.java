package com.cla.filecluster.util.solr.group.verify;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.service.files.pv.ContentGroupDetails;
import com.cla.filecluster.service.files.pv.GrpHelperFetcher;
import com.cla.filecluster.service.files.pv.PvAnalysisDataFetcher;
import com.cla.filecluster.util.solr.group.fetchers.SolrFetchers;
import com.cla.filecluster.util.solr.group.fetchers.SolrGrpHelperFetcher;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.lucene.document.Document;
import org.apache.solr.common.SolrException;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.SolrIndexSearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchersComponent.getFetchers;

/**
 * Created by uri on 05/02/2017.
 */
public class VerifyCandidatesAlgorithm {

    private final static Logger logger = LoggerFactory.getLogger(VerifyCandidatesAlgorithm.class);
    private final static String PVS_SOLR_ID_SEPARATOR = "!";

    private final int testMinFilteredThreshold;
    private boolean testDropThreshold;

    public VerifyCandidatesAlgorithm(VerifyCandidatesAlgorithmConfiguration config) {
        this.testMinFilteredThreshold = config.getTestMinFilteredThreshold();
        this.testDropThreshold = config.isTestDropThreshold();
    }

    public Collection<SimilarityMapKey> verifyCandidates(SolrQueryRequest req, int[] candidateDocumentIds,
                                                         SolrPvDocument sourceDocument) {
        long contentId = sourceDocument.getId();
        logger.debug("Verify {} candidates for contentId {}", candidateDocumentIds.length, contentId);
        SolrIndexSearcher searcher = req.getSearcher();
        Set<String> fields = Sets.newHashSet("id");
        Collection<SimilarityMapKey> groupCandidates = convertDocIdsToSimilarityMapKeys(candidateDocumentIds, searcher, fields);
        logger.trace("Converted candidates into {} results", groupCandidates.size());

        Collection<SimilarityMapKey> filteredCandidates;
        // filter out candidates that already belong to the same group
        try {
            SolrGrpHelperFetcher solrGrpHelperFetcher = getFetchers().getSolrGrpHelperFetcher();
            String inputDocumentGroup = solrGrpHelperFetcher.getContentGroupId(contentId);
            logger.trace("Original document is in group {}", inputDocumentGroup);
            //TODO - replace with a join query
            filteredCandidates = filterGroupAndDeletedCandidates(inputDocumentGroup, groupCandidates);
        } catch (Exception e) {
            logger.error("Failed to filter candidates of {} by input group", contentId, e);
            throw new SolrException(SolrException.ErrorCode.SERVER_ERROR,
                    "Failed to filter candidates of " + contentId + " by input group (" + e.getMessage() + ")", e);
        }

        // filter out candidates that are marked as unmatched to the current document
        if (getFetchers().isUseUnmatchedContentCache()) {
            UnmatchedContentCache unmatchedContentCache = getFetchers().getUnmatchedContentCache();
            Set<Long> unmatchedContentsSet = unmatchedContentCache.safeGet(contentId);
            int sizeBefore = filteredCandidates.size();
            filteredCandidates = filteredCandidates.stream()
                    .filter(mapKey -> !unmatchedContentsSet.contains(mapKey.getDocIdFromKey()))
                    .collect(Collectors.toSet());
            int sizeAfter = filteredCandidates.size();
            if (sizeAfter < sizeBefore) {
                logger.debug("For content {} Filtered {} unmatched candidates out of {}. {} Left.", contentId, sizeBefore - sizeAfter, sizeBefore, sizeAfter);
            }
        }

        return filteredCandidates;
    }

    private Collection<SimilarityMapKey> filterGroupAndDeletedCandidates(String inputGroup,
                                                                         Collection<SimilarityMapKey> groupCandidates) {
        if (inputGroup == null || groupCandidates == null || groupCandidates.size() == 0) {
            return groupCandidates;
        }
        if (testDropThreshold) {
            int min = Math.min(testMinFilteredThreshold, groupCandidates.size());
            logger.debug("Using dummy filtering by configured threshold {}, matched documents: {}", testMinFilteredThreshold, min);
            return groupCandidates.stream().limit(min).collect(Collectors.toList());
        }
        SolrGrpHelperFetcher grpHelperFetcher = getFetchers().getSolrGrpHelperFetcher();
        List<Long> contentIds = groupCandidates.stream().map(SimilarityMapKey::getDocIdFromKey).collect(Collectors.toList());
        Map<Long, ContentGroupDetails> contentGroupByIds = grpHelperFetcher.getContentGroupByIds(contentIds);
        //TODO - implement cache
        return groupCandidates.stream().filter(f -> filterByGroup(f, inputGroup, contentGroupByIds)).collect(Collectors.toList());
    }

    private boolean filterByGroup(SimilarityMapKey f, String inputDocumentGroup, Map<Long, ContentGroupDetails> contentGroupByIds) {
        Long contentId = f.getDocIdFromKey();
        ContentGroupDetails contentGroupDetails = contentGroupByIds.get(contentId);
        if (contentGroupDetails == null) {
            return true;
        }
        if (AnalyzeHint.EXCLUDED.equals(contentGroupDetails.getAnalyzeHint())) {
            logger.debug("Candidate {} is filtered out due to analysis hint", f);
            return false;
        }
        if (contentGroupDetails.getGroupId() == null || !contentGroupDetails.getGroupId().equals(inputDocumentGroup)) {
            return true;
        }
        logger.trace("Candidate {} is filtered out due to matched group", f);
        return false;
    }


    private Collection<SimilarityMapKey> convertDocIdsToSimilarityMapKeys(int[] candidateDocumentIds,
                                                                          SolrIndexSearcher searcher, Set<String> fields) {
        Collection<SimilarityMapKey> result = new ArrayList<>();
        for (int docId : candidateDocumentIds) {
            try {
                Document doc = searcher.doc(docId, fields);
                SimilarityMapKey mapKey = createSimilarityMapKey(doc.get("id"), docId);

                result.add(mapKey);
            } catch (IOException e) {
                logger.error("Failed to fetch document id {}", docId, e);
                throw new RuntimeException("Failed to fetch document id " + docId, e);
            }
        }
        return result;
    }

    private Pair<String, Long> parseDocId(String id) {
        long fileId;
        String part;
        final String[] sp = id.split(PVS_SOLR_ID_SEPARATOR, 2);
        part = sp[1];
        fileId = Long.parseLong(sp[0]);
        return Pair.of(part, fileId);
    }

    private SimilarityMapKey createSimilarityMapKey(String id, int docId) {
        final Pair<String, Long> stringLongPair = parseDocId(id);
        return new SimilarityMapKey(stringLongPair.getRight(), stringLongPair.getLeft(), docId);
    }


}
