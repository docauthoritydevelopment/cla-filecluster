package com.cla.filecluster.util.solr.group.finalmatching.excel;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.finalmatching.AbstractFinalMatchingTaskExecutor;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTask;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelFinalMatchingTaskExecutor extends AbstractFinalMatchingTaskExecutor {

    private final ExcelFinalMatchingRequestData finalMatchingRequestData;
    private final boolean pluginIsForceWorkbookMaturityLevel;
    private final int groupedExcelSheetsProcessingFirstThreshold;
    private final int groupedExcelSheetsProcessingLastThreshold;

    public ExcelFinalMatchingTaskExecutor(SolrPvDocument sourceDocument,
                                          ExcelFinalMatchingRequestData finalMatchingRequestData,
                                          SimilarityCalculator similarityCalculator,
                                          boolean pluginIsForceWorkbookMaturityLevel,
                                          int groupedExcelSheetsProcessingFirstThreshold,
                                          int groupedExcelSheetsProcessingLastThreshold) {
        super(sourceDocument, similarityCalculator);
        this.similarityCalculator = similarityCalculator;
        this.finalMatchingRequestData = finalMatchingRequestData;
        this.pluginIsForceWorkbookMaturityLevel = pluginIsForceWorkbookMaturityLevel;
        this.groupedExcelSheetsProcessingFirstThreshold = groupedExcelSheetsProcessingFirstThreshold;
        this.groupedExcelSheetsProcessingLastThreshold = groupedExcelSheetsProcessingLastThreshold;
    }


    @Override
    protected FinalMatchingTask newFinalMatchingTask(List<PvAnalysisData> pvAnalysisDataList) {
        Map<Long, Map<String, List<String>>> matchedSheetsByDoc = finalMatchingRequestData.getMatchedSheetsByDoc();
        Map<Long, Map<String, List<String>>> subMap = new HashMap<>();
        pvAnalysisDataList.forEach(pvAd -> {
            Map<String, List<String>> matchedSheet = matchedSheetsByDoc.get(pvAd.getContentId());
            if (matchedSheet != null) {
                subMap.put(pvAd.getContentId(), matchedSheet);
            }
        });
        ExcelFinalMatchingTaskContext context = buildTaskContext(pvAnalysisDataList, subMap);
        return new ExcelFinalMatchingTask(context);
    }

    private ExcelFinalMatchingTaskContext buildTaskContext(List<PvAnalysisData> pvAnalysisDataList, Map<Long, Map<String, List<String>>> subMap) {
        return new ExcelFinalMatchingTaskContext(
                similarityCalculator,
                subMap.entrySet(),
                pvAnalysisDataList,
                sourceDocument,
                finalMatchingRequestData.getSheetsDominanceRatio(),
                finalMatchingRequestData.getSkippedSheetsDominance(),
                pluginIsForceWorkbookMaturityLevel,
                groupedExcelSheetsProcessingFirstThreshold,
                groupedExcelSheetsProcessingLastThreshold);
    }
}
