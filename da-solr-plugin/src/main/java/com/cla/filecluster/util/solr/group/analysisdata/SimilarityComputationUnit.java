package com.cla.filecluster.util.solr.group.analysisdata;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;

import java.util.*;
import java.util.function.Consumer;

/**
 * Represent a computation work done by the resource manager
 */
public class SimilarityComputationUnit {

    private final Consumer<List<PvAnalysisData>> similarityExtractionOperation;
    private Iterator<Long> iterator;

    private final String threadName;
    private int workLeft;
    private int totalSize;


    public SimilarityComputationUnit(String threadName,
                                     Consumer<List<PvAnalysisData>> similarityExtractionOperation,
                                     Collection<Long> ids) {
        this.threadName = threadName;
        this.similarityExtractionOperation = similarityExtractionOperation;
        iterator = new ArrayList<>(ids).iterator();
        totalSize = ids.size();
        workLeft = totalSize;
    }

    public boolean hasMoreIds() {
        return iterator.hasNext();
    }

    public String getThreadName() {
        return threadName;
    }

    public Long nextContentId() {
        return iterator.next();
    }

    public long getWorkLeft() {
        return workLeft;
    }



    public void compute(List<PvAnalysisData> pvAnalysisDataList) {
        similarityExtractionOperation.accept(pvAnalysisDataList);
    }

    public int getSize() {
        return totalSize;
    }

    public void batchDone(int batchSize) {
        workLeft -= batchSize;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimilarityComputationUnit dataStore = (SimilarityComputationUnit) o;
        return Objects.equals(threadName, dataStore.threadName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(threadName);
    }

}
