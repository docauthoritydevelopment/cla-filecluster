package com.cla.filecluster.util.solr.group.analysisdata;

public interface ComputationUnitInfo {

    String getThreadName();

    int getWorkLeft();

    int getTotalWork();
}
