package com.cla.filecluster.util.solr.update;

import org.apache.solr.search.DocList;

import java.io.IOException;

/**
 * Created by: yael
 * Created on: 3/27/2018
 */
public interface UpdateOperation {
    void performUpdate(DocList queryResult) throws IOException;
}
