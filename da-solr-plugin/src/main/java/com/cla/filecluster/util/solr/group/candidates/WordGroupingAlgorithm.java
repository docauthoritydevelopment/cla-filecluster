package com.cla.filecluster.util.solr.group.candidates;

import com.cla.common.domain.dto.pv.PVType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.util.solr.group.pv.PVQueryResult;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by uri on 24/06/2015.
 */
public class WordGroupingAlgorithm extends BaseGroupingAlgorithm {

    private final int wordMinHeadingItems;
    private final int wordMinPLabelItems;
    
    private final int wordMinStyleItems;
    private final int wordMinStyleSequenceItems;
    
    private float wordSimilarityContentHighThreshold;
    private float wordSimilarityContentLowThreshold;
    private float wordSimilarityStyleHighThreshold;
    private float wordSimilarityStyleLowThreshold;
    private float wordSimilarityHeadingsLowThreshold;
    private float wordSimilarityHeadingsHighThreshold;
    private float wordSimilarityDocstartLowThreshold;

    public WordGroupingAlgorithm(ArrayList<PVQueryResult> pvQueryResults, SolrPvDocument originalDocument,
    		Properties thresholdsContent, Properties thresholdsStyle, Map<Integer, List<Integer>> debFiles, boolean testDropThreshold) {
        super(pvQueryResults, originalDocument, thresholdsContent, thresholdsStyle, debFiles,testDropThreshold);

        // The usage of thresholdsContent vs. thresholdsStyle is hard-coded based on the specific threshold. For better configuration, the logic should use the 'contentPvs' property.

        //Define thresholds for match
        wordMinPLabelItems = Integer.valueOf(thresholdsStyle.getProperty("minPLabelItems","10"));
        wordMinHeadingItems = Integer.valueOf(thresholdsStyle.getProperty("minHeadingItems","10"));

        wordMinStyleItems = Integer.valueOf(thresholdsStyle.getProperty("wordMinStyleItems","3"));
        wordMinStyleSequenceItems = Integer.valueOf(thresholdsStyle.getProperty("wordMinStyleSequenceItems","1"));
        
        wordSimilarityContentHighThreshold = Float.valueOf(thresholdsContent.getProperty("similarityContentHighThreshold","0.6"));
        wordSimilarityContentLowThreshold = Float.valueOf(thresholdsContent.getProperty("similarityContentLowThreshold","0.05"));
        wordSimilarityStyleHighThreshold = Float.valueOf(thresholdsStyle.getProperty("similarityStyleHighThreshold","0.6"));
        wordSimilarityStyleLowThreshold = Float.valueOf(thresholdsStyle.getProperty("similarityStyleLowThreshold","0.2"));
        wordSimilarityHeadingsLowThreshold = Float.valueOf(thresholdsStyle.getProperty("similarityLowHeadingsThreshold","0.2"));
        wordSimilarityHeadingsHighThreshold = Float.valueOf(thresholdsStyle.getProperty("similarityHighHeadingsThreshold","0.5"));
        wordSimilarityDocstartLowThreshold = Float.valueOf(thresholdsContent.getProperty("similarityDocstartLowThreshold","0.05"));
        if (FileType.PDF.equals(originalDocument.getFileType())) {
            //Custom style threshold for PDF
            wordSimilarityStyleLowThreshold = wordSimilarityStyleLowThreshold / 2.0f;
        }
    }

    @Override
    protected int simThresholdPassed(Map<PVType, Float> simRates,
    		Map<PVType, Integer> docPvItemsCounts, Map<PVType, Integer> docPvGlobalCounts) {
        return wordSimThresholdPassed(simRates, docPvItemsCounts, docPvGlobalCounts,
                wordSimilarityContentHighThreshold,
                wordSimilarityContentLowThreshold,
                wordSimilarityStyleHighThreshold,
                wordSimilarityStyleLowThreshold,	// TODO - Give PDF some grace in weak style similarity
                wordSimilarityHeadingsLowThreshold,
                wordSimilarityHeadingsHighThreshold,
                wordSimilarityDocstartLowThreshold);
    }

    private int wordSimThresholdPassed(final Map<PVType, Float> simRates, 
    		final Map<PVType, Integer> docPvItemsCounts, final Map<PVType, Integer> docPvGlobalCounts,
                                       final float contentHighThreshold,
                                       final float contentLowThreshold,
                                       final float styleHighThreshold,
                                       final float styleLowThreshold,
                                       final float headingsLowThreshold,
                                       final float headingsHighThreshold,
                                       final float docstartLowThreshold) {

        final float docstartNgramsSimRate = floatVal(simRates.get(PVType.PV_DocstartNgrams));
        final float bodyNgramsSimRate = floatVal(simRates.get(PVType.PV_BodyNgrams));
        final float subBodyNgramsSimRate = floatVal(simRates.get(PVType.PV_SubBodyNgrams));
        final float styleSignaturesSimRate = floatVal(simRates.get(PVType.PV_StyleSignatures));
        final float styleSequenceSimRate = floatVal(simRates.get(PVType.PV_StyleSequence));
        final float absoluteStyleSignaturesSimRate = floatVal(simRates.get(PVType.PV_AbsoluteStyleSignatures));
        final float absoluteStyleSequenceSimRate = floatVal(simRates.get(PVType.PV_AbsoluteStyleSequence));
        final float headingsSimRate = floatVal(simRates.get(PVType.PV_Headings));
        final float pLabelSimRate = floatVal(simRates.get(PVType.PV_ParagraphLabels));
        final float letterHeadLabelsSimRate = floatVal(simRates.get(PVType.PV_LetterHeadLabels));

		/*
		 * Strong content sim && weak style sim => sure match
		 */
        if ((bodyNgramsSimRate > contentHighThreshold ||
                subBodyNgramsSimRate > contentHighThreshold) &&
                (styleSignaturesSimRate > styleLowThreshold ||
                        styleSequenceSimRate > styleLowThreshold ||
                        absoluteStyleSignaturesSimRate > styleLowThreshold ||
                        absoluteStyleSequenceSimRate > styleLowThreshold)) {

            if (docstartNgramsSimRate < docstartLowThreshold) {
                if (NotNull(docPvGlobalCounts.get(PVType.PV_Headings)) > wordMinHeadingItems &&
                        headingsSimRate < headingsHighThreshold) {
                    return 0;
                }
                else {
                    return 1;
                }
            }
            return 2;
        }

		/*
		 * Strong style sim
		 */
        if ((styleSignaturesSimRate > styleHighThreshold &&  NotNull(docPvItemsCounts.get(PVType.PV_StyleSignatures)) > wordMinStyleItems) ||
                (styleSequenceSimRate > styleHighThreshold &&  NotNull(docPvItemsCounts.get(PVType.PV_StyleSequence)) > wordMinStyleSequenceItems) ||
                (absoluteStyleSignaturesSimRate > styleHighThreshold && NotNull(docPvItemsCounts.get(PVType.PV_AbsoluteStyleSignatures)) > wordMinStyleItems) ||
                (absoluteStyleSequenceSimRate > styleHighThreshold &&  NotNull(docPvItemsCounts.get(PVType.PV_AbsoluteStyleSequence)) > wordMinStyleSequenceItems)) {
            if (docstartNgramsSimRate > contentLowThreshold ||
                    bodyNgramsSimRate > contentLowThreshold ||
                    subBodyNgramsSimRate > contentLowThreshold) {

                if (docstartNgramsSimRate < docstartLowThreshold) {
                    if (NotNull(docPvGlobalCounts.get(PVType.PV_Headings)) > wordMinHeadingItems &&
                            headingsSimRate < headingsHighThreshold) {
                        return 0;
                    }
                }

                if (NotNull(docPvGlobalCounts.get(PVType.PV_Headings)) > wordMinHeadingItems) {
                    if (headingsSimRate > headingsLowThreshold) {
                        // headings presence with weak sim, falses match.
                        return 3;
                    }
                    else {
                        return 0;
                    }
                }
                else if (NotNull(docPvGlobalCounts.get(PVType.PV_ParagraphLabels)) > wordMinPLabelItems) {
                    if (pLabelSimRate > headingsLowThreshold) {
                        // No headings + paragraph-labels presence + weak sim, falses match
                        return 4;
                        // TODO: to be straighten with orderedMatchCount for letterHeadLabels/pLabels < minLetterHeadItems) ...
                    }
                    else {
                        return 0;
                    }
                }
                else if (NotNull(docPvGlobalCounts.get(PVType.PV_LetterHeadLabels)) > wordMinPLabelItems) {
                    if (letterHeadLabelsSimRate> headingsLowThreshold) {
                        // No headings + letter-head presence + weak sim, falses match
                        return 5;
                        // TODO: to be straighten with orderedMatchCount for letterHeadLabels/pLabels < minLetterHeadItems) ...
                    }
                    else {
                        return 0;
                    }
                }
                return 6;
            }
        }
        return 0;
    }

}
