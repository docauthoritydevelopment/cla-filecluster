package com.cla.filecluster.util.solr.group.finalmatching;

public interface FinalMatchingQueryParams {

    String FINAL_MATCHING_REQUEST_DATA = "requestJson";
}
