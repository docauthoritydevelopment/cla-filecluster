package com.cla.filecluster.util.solr.group.fetchers;

import com.cla.common.utils.FileSizeUnits;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.PvAnalysisDataFetcher;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrException;
import org.apache.solr.search.SolrCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

/**
 * Created by uri on 08/02/2017.
 */
public class SolrAnalysisDataFetcher extends GenericSolrDataFetcher implements PvAnalysisDataFetcher {

    private static final Logger logger = LoggerFactory.getLogger(SolrAnalysisDataFetcher.class);
    private final Map<String, String> compatibilityMap;

    private boolean compressAnalysisData;

    private long callCount = 0;
    private long fetchCount = 0;


    ForkJoinPool customThreadPool = new ForkJoinPool(Runtime.getRuntime().availableProcessors());

    public SolrAnalysisDataFetcher(SolrClient solrClient, boolean compressAnalysisData,
                                   Map<String, String> compatibilityMap) {
        super(solrClient);
        this.compressAnalysisData = compressAnalysisData;
        this.compatibilityMap = compatibilityMap;
    }


    @Override
    public PvAnalysisData getPvAnalysisDataByContentId(Long contentId) {
        Pair<PvAnalysisData, Integer> pair = getPvAnalysisDataAndSizeByContentId(contentId);
        return pair != null ? pair.getLeft() : null;
    }

    @Override
    public Pair<PvAnalysisData, Integer> getPvAnalysisDataAndSizeByContentId(Long contentId) {
        if (logger.isDebugEnabled() && (callCount % 100) == 0) {
            logMemoryConsumption();
        }
        callCount++;
        fetchCount++;
        SolrDocument document = getDocument(contentId);
        if (document == null) {
            logger.warn("Unable to find Pv Analysis data for content ID: {}", contentId);
            return null;
        }
        Optional<Pair<PvAnalysisData, Integer>> optionalPair = convertDocumentToPvAnalysisData(document, compressAnalysisData);
        return optionalPair.isPresent() ? optionalPair.get() : null;
    }

    @Override
    public List<PvAnalysisData> getPvAnalysisDataByContentIds(Collection<Long> contentIdList) {
        return getPvAnalysisDataByContentIdsAndSize(contentIdList).stream().map(p -> p.getLeft()).collect(Collectors.toList());
    }

    @Override
    public List<Pair<PvAnalysisData, Integer>> getPvAnalysisDataByContentIdsAndSize(Collection<Long> contentIdList) {
        if (logger.isDebugEnabled() && (callCount % 100) == 0) {
            logMemoryConsumption();
        }
        callCount++;
        fetchCount += contentIdList.size();
        if (logger.isTraceEnabled()) {
            String printIds = contentIdList.stream().map(f -> f.toString()).collect(Collectors.joining(","));
            logger.trace("Get PV analysis data for content Ids: {}", printIds);
        }
        List<SolrDocument> documents = findDocuments(contentIdList);
        List<Pair<PvAnalysisData, Integer>> collect;
        long start = System.currentTimeMillis();
        logger.trace("Thread {}: Start extract Analysis data for {} docs",
                Thread.currentThread().getName(), contentIdList.size());
        try {
            collect = customThreadPool.submit(
                    () -> documents
                            .parallelStream()
                            .map(f -> convertDocumentToPvAnalysisData(f, compressAnalysisData))
                            .filter(Optional::isPresent)
                            .map(Optional::get))
                    .get().collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Failed to execute extract documents operation", e);
            throw new RuntimeException(e);
        }
        logger.trace("Thread {}: Done extract Analysis data for {} docs in {} ms",
                Thread.currentThread().getName(), contentIdList.size(), System.currentTimeMillis() - start);
        return collect;
    }

    private boolean safeExistsInCache(SolrCache excludeLargeAnalysisDataCache, Long id) {
        return excludeLargeAnalysisDataCache == null ? false : excludeLargeAnalysisDataCache.get(id) != null;
    }

    @Override
    public void updateStatistics(long time, int size) {
        //TODO
    }

    private void logMemoryConsumption() {
        if (logger.isDebugEnabled()) {
            Runtime runtime = Runtime.getRuntime();
            long tm = FileSizeUnits.BYTE.toMegabytes(runtime.totalMemory());
            long fm = FileSizeUnits.BYTE.toMegabytes(runtime.freeMemory());
            long mm = FileSizeUnits.BYTE.toMegabytes(runtime.maxMemory());
            logger.debug("Heap status: counters=({}/{}), mem(T;U;F;M),{},{},{},{},MB", callCount, fetchCount, tm, tm - fm, fm, mm);
        }
    }

    public Optional<Pair<PvAnalysisData, Integer>> convertDocumentToPvAnalysisData(SolrDocument document, boolean compressAnalysisData) {
        if (document == null) {
            throw new RuntimeException("Unable to convert null document to AnalysisData");
        }
        Long contentId = Long.parseLong((String) document.get("id"));
        byte[] pvCollectionsSrc = (byte[]) document.get("pvCollections");

        try {
            return Optional.of(Pair.of(
                    PvAnalysisDataUtils.convertDocumentToPvAnalysisData(document, compressAnalysisData, compatibilityMap),
                    pvCollectionsSrc.length));
        } catch (Exception e) {
            logger.error("Failed to convert document (content id: {}) to PV Analysis data", contentId, e);
            throw new SolrException(SolrException.ErrorCode.SERVER_ERROR, "Failed to convert document (content id: " + contentId + ") to PV Analysis data", e);
        }
    }


}
