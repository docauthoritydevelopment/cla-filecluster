package com.cla.filecluster.util.solr.security;

import org.apache.lucene.search.Query;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.SyntaxError;

/**
 * Currently not in use. <br/>
 * May become relevant if we decide to enforce ACLs by means of PostFilter  <br/>
 * in order to activate - add the following line in solrconfig.xml <br/>
 * &lt queryParser name="acl" class="com.cla.filecluster.util.solr.security.AccessControlQParserPlugin"/&gt
 *
 * @see AccessControlQuery
 */
public class AccessControlQParserPlugin extends QParserPlugin {

    public static String NAME = "acl";

    public void init(NamedList args) {
    }

    @Override
    public QParser createParser(String qstr, SolrParams localParams,
                                SolrParams params, SolrQueryRequest req) {
        return new QParser(qstr, localParams, params, req) {
            @Override
            public Query parse() throws SyntaxError {
                return new AccessControlQuery(localParams.get("user"), localParams.get("groups"));
            }
        };
    }

}
