/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cla.filecluster.util.solr.similarity;


import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

/**
 * A Query that matches documents matching boolean combinations of other
 * queries, e.g. {@link ClaTermQuery}s, {@link PhraseQuery}s or other
 * BooleanQuerys.
 */
public class ClaBooleanQuery extends ClaQuery implements Iterable<ClaBooleanClause> {

    private static int maxClauseCount = 1024;

    /**
     * Thrown when an attempt is made to add more than {@link
     * #getMaxClauseCount()} clauses. This typically happens if
     * a PrefixQuery, FuzzyQuery, WildcardQuery, or TermRangeQuery
     * is expanded to many terms during search.
     */
    public static class TooManyClauses extends RuntimeException {
        public TooManyClauses() {
            super("maxClauseCount is set to " + maxClauseCount);
        }
    }

    /**
     * Return the maximum number of clauses permitted, 1024 by default.
     * Attempts to add more than the permitted number of clauses cause {@link
     * TooManyClauses} to be thrown.
     *
     * @see #setMaxClauseCount(int)
     */
    public static int getMaxClauseCount() {
        return maxClauseCount;
    }

    /**
     * Set the maximum number of clauses permitted per ClaBooleanQuery.
     * Default value is 1024.
     */
    public static void setMaxClauseCount(int maxClauseCount) {
        if (maxClauseCount < 1) {
            throw new IllegalArgumentException("maxClauseCount must be >= 1");
        }
        ClaBooleanQuery.maxClauseCount = maxClauseCount;
    }

    /**
     * A builder for boolean queries.
     */
    public static class Builder {

        private int minimumNumberShouldMatch;
        private final List<ClaBooleanClause> clauses = new ArrayList<>();

        /**
         * Sole constructor.
         */
        public Builder() {
        }

        /**
         * Specifies a minimum number of the optional BooleanClauses
         * which must be satisfied.
         *
         * <p>
         * By default no optional clauses are necessary for a match
         * (unless there are no required clauses).  If this method is used,
         * then the specified number of clauses is required.
         * </p>
         * <p>
         * Use of this method is totally independent of specifying that
         * any specific clauses are required (or prohibited).  This number will
         * only be compared against the number of matching optional clauses.
         * </p>
         *
         * @param min the number of optional clauses that must match
         */
        public Builder setMinimumNumberShouldMatch(int min) {
            this.minimumNumberShouldMatch = min;
            return this;
        }

        /**
         * Add a new clause to this {@link Builder}. Note that the order in which
         * clauses are added does not have any impact on matching documents or query
         * performance.
         *
         * @throws TooManyClauses if the new number of clauses exceeds the maximum clause number
         */
        public Builder add(ClaBooleanClause clause) {
            if (clauses.size() >= maxClauseCount) {
                throw new TooManyClauses();
            }
            clauses.add(clause);
            return this;
        }

        /**
         * Add a new clause to this {@link Builder}. Note that the order in which
         * clauses are added does not have any impact on matching documents or query
         * performance.
         *
         * @throws TooManyClauses if the new number of clauses exceeds the maximum clause number
         */
        public Builder add(ClaQuery query, ClaBooleanClause.Occur occur) {
            return add(new ClaBooleanClause(query, occur));
        }

        /**
         * Create a new {@link ClaBooleanQuery} based on the parameters that have
         * been set on this builder.
         */
        public ClaBooleanQuery build() {
            return new ClaBooleanQuery(minimumNumberShouldMatch, clauses.toArray(new ClaBooleanClause[0]));
        }

    }

    private final int minimumNumberShouldMatch;
    private final List<ClaBooleanClause> clauses;              // used for toString() and getClauses()
    private final Map<ClaBooleanClause.Occur, Collection<ClaQuery>> clauseSets; // used for equals/hashcode

    ClaBooleanQuery(int minimumNumberShouldMatch,
                    ClaBooleanClause[] clauses) {
        this.minimumNumberShouldMatch = minimumNumberShouldMatch;
        this.clauses = Collections.unmodifiableList(Arrays.asList(clauses));
        clauseSets = new EnumMap<>(ClaBooleanClause.Occur.class);
        // duplicates matter for SHOULD and MUST
        clauseSets.put(ClaBooleanClause.Occur.SHOULD, new ClaMultiset<>());
        clauseSets.put(ClaBooleanClause.Occur.MUST, new ClaMultiset<>());
        // but not for FILTER and MUST_NOT
        clauseSets.put(ClaBooleanClause.Occur.FILTER, new HashSet<>());
        clauseSets.put(ClaBooleanClause.Occur.MUST_NOT, new HashSet<>());
        for (ClaBooleanClause clause : clauses) {
            clauseSets.get(clause.getOccur()).add(clause.getQuery());
        }
    }

    /**
     * Gets the minimum number of the optional BooleanClauses
     * which must be satisfied.
     */
    public int getMinimumNumberShouldMatch() {
        return minimumNumberShouldMatch;
    }

    /**
     * Return a list of the clauses of this {@link ClaBooleanQuery}.
     */
    public List<ClaBooleanClause> clauses() {
        return clauses;
    }

    /**
     * Return the collection of queries for the given {@link ClaBooleanClause.Occur}.
     */
    Collection<ClaQuery> getClauses(ClaBooleanClause.Occur occur) {
        return clauseSets.get(occur);
    }

    /**
     * Returns an iterator on the clauses in this query. It implements the {@link Iterable} interface to
     * make it possible to do:
     * <pre class="prettyprint">for (ClaBooleanClause clause : booleanQuery) {}</pre>
     */
    @Override
    public final Iterator<ClaBooleanClause> iterator() {
        return clauses.iterator();
    }

    private ClaBooleanQuery rewriteNoScoring() {
        if (clauseSets.get(ClaBooleanClause.Occur.MUST).size() == 0) {
            return this;
        }
        ClaBooleanQuery.Builder newQuery = new ClaBooleanQuery.Builder();
        newQuery.setMinimumNumberShouldMatch(getMinimumNumberShouldMatch());
        for (ClaBooleanClause clause : clauses) {
            if (clause.getOccur() == ClaBooleanClause.Occur.MUST) {
                newQuery.add(clause.getQuery(), ClaBooleanClause.Occur.FILTER);
            } else {
                newQuery.add(clause);
            }
        }
        return newQuery.build();
    }

    /**
     * Disregard the score and call previous
     * @param searcher
     * @param needsScores
     * @param boost
     * @return
     * @throws IOException
     */
    @Override
    public Weight createWeight(IndexSearcher searcher, boolean needsScores, float boost) throws IOException {
        return createClaWeight((ClaSimilaritySearcher) searcher, needsScores);
    }

    @Override
    public ClaWeight createClaWeight(ClaSimilaritySearcher searcher, boolean needsScores) throws IOException {
        ClaBooleanQuery query = this;
        if (needsScores == false) {
            query = rewriteNoScoring();
        }
        return new ClaBooleanWeight(query, searcher, needsScores, false);
    }

    @Override
    public Query rewrite(IndexReader reader) throws IOException {
        if (clauses.size() == 0) {
            return new MatchNoDocsQuery("empty ClaBooleanQuery");
        }

        // optimize 1-clause queries
        if (clauses.size() == 1) {
            ClaBooleanClause c = clauses.get(0);
            Query query = c.getQuery();
            if (minimumNumberShouldMatch == 1 && c.getOccur() == ClaBooleanClause.Occur.SHOULD) {
                return query;
            } else if (minimumNumberShouldMatch == 0) {
                switch (c.getOccur()) {
                    case SHOULD:
                    case MUST:
                        return query;
                    case FILTER:
                        // no scoring clauses, so return a score of 0
                        return new ClaBoostQuery(new ClaConstantScoreQuery(query), 0);
                    case MUST_NOT:
                        // no positive clauses
                        return new MatchNoDocsQuery("pure negative ClaBooleanQuery");
                    default:
                        throw new AssertionError();
                }
            }
        }

        // recursively rewrite
        {
            ClaBooleanQuery.Builder builder = new ClaBooleanQuery.Builder();
            builder.setMinimumNumberShouldMatch(getMinimumNumberShouldMatch());
            boolean actuallyRewritten = false;
            for (ClaBooleanClause clause : this) {
                ClaQuery query = clause.getQuery();
                ClaQuery rewritten = query.rewriteToCla(reader);
                if (rewritten != query) {
                    actuallyRewritten = true;
                }
                builder.add(rewritten, clause.getOccur());
            }
            if (actuallyRewritten) {
                return builder.build();
            }
        }

        // remove duplicate FILTER and MUST_NOT clauses
        {
            int clauseCount = 0;
            for (Collection<ClaQuery> queries : clauseSets.values()) {
                clauseCount += queries.size();
            }
            if (clauseCount != clauses.size()) {
                // since clauseSets implicitly deduplicates FILTER and MUST_NOT
                // clauses, this means there were duplicates
                ClaBooleanQuery.Builder rewritten = new ClaBooleanQuery.Builder();
                rewritten.setMinimumNumberShouldMatch(minimumNumberShouldMatch);
                for (Map.Entry<ClaBooleanClause.Occur, Collection<ClaQuery>> entry : clauseSets.entrySet()) {
                    final ClaBooleanClause.Occur occur = entry.getKey();
                    for (ClaQuery query : entry.getValue()) {
                        rewritten.add(query, occur);
                    }
                }
                return rewritten.build();
            }
        }

        // Check whether some clauses are both required and excluded
        final Collection<ClaQuery> mustNotClauses = clauseSets.get(ClaBooleanClause.Occur.MUST_NOT);
        if (!mustNotClauses.isEmpty()) {
            final Predicate<Query> p = clauseSets.get(ClaBooleanClause.Occur.MUST)::contains;
            if (mustNotClauses.stream().anyMatch(p.or(clauseSets.get(ClaBooleanClause.Occur.FILTER)::contains))) {
                return new MatchNoDocsQuery("FILTER or MUST clause also in MUST_NOT");
            }
            if (mustNotClauses.contains(new MatchAllDocsQuery())) {
                return new MatchNoDocsQuery("MUST_NOT clause is MatchAllDocsQuery");
            }
        }

        // remove FILTER clauses that are also MUST clauses
        // or that match all documents
        if (clauseSets.get(ClaBooleanClause.Occur.MUST).size() > 0 && clauseSets.get(ClaBooleanClause.Occur.FILTER).size() > 0) {
            final Set<ClaQuery> filters = new HashSet<>(clauseSets.get(ClaBooleanClause.Occur.FILTER));
            boolean modified = filters.remove(new MatchAllDocsQuery());
            modified |= filters.removeAll(clauseSets.get(ClaBooleanClause.Occur.MUST));
            if (modified) {
                ClaBooleanQuery.Builder builder = new ClaBooleanQuery.Builder();
                builder.setMinimumNumberShouldMatch(getMinimumNumberShouldMatch());
                for (ClaBooleanClause clause : clauses) {
                    if (clause.getOccur() != ClaBooleanClause.Occur.FILTER) {
                        builder.add(clause);
                    }
                }
                for (ClaQuery filter : filters) {
                    builder.add(filter, ClaBooleanClause.Occur.FILTER);
                }
                return builder.build();
            }
        }

        // convert FILTER clauses that are also SHOULD clauses to MUST clauses
        if (clauseSets.get(ClaBooleanClause.Occur.SHOULD).size() > 0 && clauseSets.get(ClaBooleanClause.Occur.FILTER).size() > 0) {
            final Collection<ClaQuery> filters = clauseSets.get(ClaBooleanClause.Occur.FILTER);
            final Collection<ClaQuery> shoulds = clauseSets.get(ClaBooleanClause.Occur.SHOULD);

            Set<Query> intersection = new HashSet<>(filters);
            intersection.retainAll(shoulds);

            if (intersection.isEmpty() == false) {
                ClaBooleanQuery.Builder builder = new ClaBooleanQuery.Builder();
                int minShouldMatch = getMinimumNumberShouldMatch();

                for (ClaBooleanClause clause : clauses) {
                    if (intersection.contains(clause.getQuery())) {
                        if (clause.getOccur() == ClaBooleanClause.Occur.SHOULD) {
                            builder.add(new ClaBooleanClause(clause.getQuery(), ClaBooleanClause.Occur.MUST));
                            minShouldMatch--;
                        }
                    } else {
                        builder.add(clause);
                    }
                }

                builder.setMinimumNumberShouldMatch(Math.max(0, minShouldMatch));
                return builder.build();
            }
        }

        // Deduplicate SHOULD clauses by summing up their boosts
        if (clauseSets.get(ClaBooleanClause.Occur.SHOULD).size() > 0 && minimumNumberShouldMatch <= 1) {
            Map<ClaQuery, Double> shouldClauses = new HashMap<>();
            for (ClaQuery query : clauseSets.get(ClaBooleanClause.Occur.SHOULD)) {
                double boost = 1;
                while (query instanceof ClaBoostQuery) {
                    ClaBoostQuery bq = (ClaBoostQuery) query;
                    boost *= bq.getBoost();
                    query = bq.getQuery();
                }
                shouldClauses.put(query, shouldClauses.getOrDefault(query, 0d) + boost);
            }
            if (shouldClauses.size() != clauseSets.get(ClaBooleanClause.Occur.SHOULD).size()) {
                ClaBooleanQuery.Builder builder = new ClaBooleanQuery.Builder()
                        .setMinimumNumberShouldMatch(minimumNumberShouldMatch);
                for (Map.Entry<ClaQuery, Double> entry : shouldClauses.entrySet()) {
                    ClaQuery query = entry.getKey();
                    float boost = entry.getValue().floatValue();
                    if (boost != 1f) {
                        query = new ClaBoostQuery(query, boost);
                    }
                    builder.add(query, ClaBooleanClause.Occur.SHOULD);
                }
                for (ClaBooleanClause clause : clauses) {
                    if (clause.getOccur() != ClaBooleanClause.Occur.SHOULD) {
                        builder.add(clause);
                    }
                }
                return builder.build();
            }
        }

        // Deduplicate MUST clauses by summing up their boosts
        if (clauseSets.get(ClaBooleanClause.Occur.MUST).size() > 0) {
            Map<ClaQuery, Double> mustClauses = new HashMap<>();
            for (ClaQuery query : clauseSets.get(ClaBooleanClause.Occur.MUST)) {
                double boost = 1;
                while (query instanceof ClaBoostQuery) {
                    ClaBoostQuery bq = (ClaBoostQuery) query;
                    boost *= bq.getBoost();
                    query = bq.getQuery();
                }
                mustClauses.put(query, mustClauses.getOrDefault(query, 0d) + boost);
            }
            if (mustClauses.size() != clauseSets.get(ClaBooleanClause.Occur.MUST).size()) {
                ClaBooleanQuery.Builder builder = new ClaBooleanQuery.Builder()
                        .setMinimumNumberShouldMatch(minimumNumberShouldMatch);
                for (Map.Entry<ClaQuery, Double> entry : mustClauses.entrySet()) {
                    ClaQuery query = entry.getKey();
                    float boost = entry.getValue().floatValue();
                    if (boost != 1f) {
                        query = new ClaBoostQuery(query, boost);
                    }
                    builder.add(query, ClaBooleanClause.Occur.MUST);
                }
                for (ClaBooleanClause clause : clauses) {
                    if (clause.getOccur() != ClaBooleanClause.Occur.MUST) {
                        builder.add(clause);
                    }
                }
                return builder.build();
            }
        }

        // Rewrite queries whose single scoring clause is a MUST clause on a
        // MatchAllDocsQuery to a ClaConstantScoreQuery
        {
            final Collection<ClaQuery> musts = clauseSets.get(ClaBooleanClause.Occur.MUST);
            final Collection<ClaQuery> filters = clauseSets.get(ClaBooleanClause.Occur.FILTER);
            if (musts.size() == 1
                    && filters.size() > 0) {
                Query must = musts.iterator().next();
                float boost = 1f;
                if (must instanceof ClaBoostQuery) {
                    ClaBoostQuery claBoostQuery = (ClaBoostQuery) must;
                    must = claBoostQuery.getQuery();
                    boost = claBoostQuery.getBoost();
                }
                if (must.getClass() == MatchAllDocsQuery.class) {
                    // our single scoring clause matches everything: rewrite to a CSQ on the filter
                    // ignore SHOULD clause for now
                    ClaBooleanQuery.Builder builder = new ClaBooleanQuery.Builder();
                    for (ClaBooleanClause clause : clauses) {
                        switch (clause.getOccur()) {
                            case FILTER:
                            case MUST_NOT:
                                builder.add(clause);
                                break;
                            default:
                                // ignore
                                break;
                        }
                    }
                    ClaQuery rewritten = builder.build();
                    rewritten = new ClaConstantScoreQuery(rewritten);
                    if (boost != 1f) {
                        rewritten = new ClaBoostQuery(rewritten, boost);
                    }

                    // now add back the SHOULD clauses
                    builder = new ClaBooleanQuery.Builder()
                            .setMinimumNumberShouldMatch(getMinimumNumberShouldMatch())
                            .add(rewritten, ClaBooleanClause.Occur.MUST);
                    for (ClaQuery query : clauseSets.get(ClaBooleanClause.Occur.SHOULD)) {
                        builder.add(query, ClaBooleanClause.Occur.SHOULD);
                    }
                    rewritten = builder.build();
                    return rewritten;
                }
            }
        }

        return super.rewrite(reader);
    }

    /**
     * Prints a user-readable version of this query.
     */
    @Override
    public String toString(String field) {
        StringBuilder buffer = new StringBuilder();
        boolean needParens = getMinimumNumberShouldMatch() > 0;
        if (needParens) {
            buffer.append("(");
        }

        int i = 0;
        for (ClaBooleanClause c : this) {
            buffer.append(c.getOccur().toString());

            Query subQuery = c.getQuery();
            if (subQuery instanceof ClaBooleanQuery) {  // wrap sub-bools in parens
                buffer.append("(");
                buffer.append(subQuery.toString(field));
                buffer.append(")");
            } else {
                buffer.append(subQuery.toString(field));
            }

            if (i != clauses.size() - 1) {
                buffer.append(" ");
            }
            i += 1;
        }

        if (needParens) {
            buffer.append(")");
        }

        if (getMinimumNumberShouldMatch() > 0) {
            buffer.append('~');
            buffer.append(getMinimumNumberShouldMatch());
        }

        return buffer.toString();
    }

    /**
     * Compares the specified object with this boolean query for equality.
     * Returns true if and only if the provided object<ul>
     * <li>is also a {@link ClaBooleanQuery},</li>
     * <li>has the same value of {@link #getMinimumNumberShouldMatch()}</li>
     * <li>has the same {@link ClaBooleanClause.Occur#SHOULD} clauses, regardless of the order</li>
     * <li>has the same {@link ClaBooleanClause.Occur#MUST} clauses, regardless of the order</li>
     * <li>has the same set of {@link ClaBooleanClause.Occur#FILTER} clauses, regardless of the
     * order and regardless of duplicates</li>
     * <li>has the same set of {@link ClaBooleanClause.Occur#MUST_NOT} clauses, regardless of
     * the order and regardless of duplicates</li></ul>
     */
    @Override
    public boolean equals(Object o) {
        return sameClassAs(o) &&
                equalsTo(getClass().cast(o));
    }

    private boolean equalsTo(ClaBooleanQuery other) {
        return getMinimumNumberShouldMatch() == other.getMinimumNumberShouldMatch() &&
                clauseSets.equals(other.clauseSets);
    }

    private int computeHashCode() {
        int hashCode = Objects.hash(minimumNumberShouldMatch, clauseSets);
        if (hashCode == 0) {
            hashCode = 1;
        }
        return hashCode;
    }

    // cached hash code is ok since boolean queries are immutable
    private int hashCode;

    @Override
    public int hashCode() {
        // no need for synchronization, in the worst case we would just compute the hash several times.
        if (hashCode == 0) {
            hashCode = computeHashCode();
            assert hashCode != 0;
        }
        assert hashCode == computeHashCode();
        return hashCode;
    }

}
