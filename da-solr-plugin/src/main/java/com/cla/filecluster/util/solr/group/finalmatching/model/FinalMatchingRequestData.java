package com.cla.filecluster.util.solr.group.finalmatching.model;

import java.util.Collection;


public abstract class FinalMatchingRequestData<C extends Collection> {

    protected Long sourceDocumentId;
    protected C candidates;

    public FinalMatchingRequestData() {
    }

    protected FinalMatchingRequestData(Long sourceDocumentId, C candidates) {
        this.sourceDocumentId = sourceDocumentId;
        this.candidates = candidates;
    }

    public Long getSourceDocumentId() {
        return sourceDocumentId;
    }

    public void setSourceDocumentId(Long sourceDocumentId) {
        this.sourceDocumentId = sourceDocumentId;
    }

    public C getCandidates() {
        return candidates;
    }

    public void setCandidates(C candidates) {
        this.candidates = candidates;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "sourceDocumentId=" + sourceDocumentId +
                ", numOfCandidates=" + (candidates == null ? 0 : candidates.size()) +
                '}';
    }

    public abstract FinalMatchingRequestData<C> extractPage(C c);
}
