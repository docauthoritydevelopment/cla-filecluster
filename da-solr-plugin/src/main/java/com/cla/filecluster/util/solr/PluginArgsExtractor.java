package com.cla.filecluster.util.solr;

import org.apache.solr.common.util.NamedList;

public class PluginArgsExtractor {


    private NamedList initArgs;

    private PluginArgsExtractor(NamedList initArgs) {
        this.initArgs = initArgs;
    }

    public static PluginArgsExtractor newExtractor(NamedList initArgs) {
        return new PluginArgsExtractor(initArgs);
    }

    public boolean getBooleanArgument(String name, boolean defaultValue) {
        return getBooleanArgument(initArgs, name, defaultValue);
    }

    public static boolean getBooleanArgument(NamedList namedList, String name, boolean defaultValue) {
        Boolean value = namedList.getBooleanArg(name);
        return (value != null) ? value : defaultValue;
    }

    public static long getLongArgument(NamedList namedList, String name, long defaultValue) {
        String value = (String) namedList.get(name);
        return (value != null) ? Long.valueOf(value) : defaultValue;
    }

    public long getLongArgument(String name, long defaultValue) {
        return getLongArgument(initArgs, name, defaultValue);
    }

    public int getIntArgument(String name, int defaultValue) {
        return getIntArgument(initArgs, name, defaultValue);
    }

    public static int getIntArgument(NamedList namedList, String name, int defaultValue) {
        String value = (String) namedList.get(name);
        return (value != null) ? Integer.valueOf(value) : defaultValue;
    }

    public String getArgument(String name, String defaultValue) {
        return getArgument(initArgs, name, defaultValue);
    }

    public static String getArgument(NamedList args, String name, String defaultValue) {
        String value = (String) args.get(name);
        return (value != null) ? value : defaultValue;
    }

    public int[] getIntListArgument(String name, String defaultValue) {
        return getIntListArgument(initArgs,name,defaultValue);
    }


    public static int[] getIntListArgument(NamedList args, String name, String defaultValue) {
        String[] stringListArgument = getStringListArgument(args,name, defaultValue);
        int[] result = new int[stringListArgument.length];
        for (int i = 0; i < stringListArgument.length; i++) {
            result[i] = Integer.valueOf(stringListArgument[i]);
        }
        return result;
    }

    public String[] getStringListArgument(String name, String defaultValue) {
        return getStringListArgument(initArgs,name,defaultValue);
    }

    public static String[] getStringListArgument(NamedList args, String name,
                                                 String defaultValue) {
        String valueString = (String) args.get(name);
        if (valueString != null) {
            return valueString.split(",");
        } else {
            return defaultValue.split(",");
        }
    }

    public float getFloatArgument(String name, float defaultValue) {
        return getFloatArgument(initArgs, name, defaultValue);
    }

    public static float getFloatArgument(NamedList namedList, String name, float defaultValue) {
        String value = (String) namedList.get(name);
        return (value != null) ? Float.valueOf(value) : defaultValue;
    }
}
