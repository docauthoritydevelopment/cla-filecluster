package com.cla.filecluster.util.solr.group.fetchers;

import com.cla.filecluster.util.solr.PluginArgsExtractor;
import com.cla.filecluster.util.solr.SolrConstants;
import com.cla.filecluster.util.solr.group.verify.SolrUnmatchedContentRepository;
import com.cla.filecluster.util.solr.group.verify.UnmatchedContentCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.http.client.HttpClient;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static com.cla.filecluster.util.solr.PluginArgsExtractor.getBooleanArgument;
import static com.cla.filecluster.util.solr.PluginArgsExtractor.getIntArgument;
import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchersParams.*;
import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchersParams.ARG_CACHE_UNMATCHED_CONTENT_CACHE_CONCURRENCY;


/**
 * Holds all fetches for getting related analysis data, GrpHelper, UnMatchedContent, AnalysisData...
 */
public class SolrFetchers {

    private static final Logger logger = LoggerFactory.getLogger(SolrFetchers.class);

    // Defaults
    public static final int SOLR_DEFAULT_MAX_CONNECTIONS = 128;
    public static final int SOLR_DEFAULT_MAX_CONNECTIONS_PER_HOST = 32;
    public static final int SOLR_DEFAULT_TIMEOUT = 600000;
    private static final int DEFAULT_CACHE_MAX_SIZE = 50000;
    private static final int DEFAULT_CACHE_CONCURRENCY = 10;
    public static final boolean DEFAULT_ANALYSIS_DATA_COMPRESSED = true;
    public static String ANALYSIS_DATA_DEFAULT_DESERIALIZE_COMPATIBILITY_MAP = "{" +
            "com.cla.common.domain.dto.FileType:com.cla.connector.domain.dto.file.FileType" +
            "}";
    public static String ARG_SOLR_DEFAULT_BASE_URL = "http://localhost:8983/solr/";


    private SolrGrpHelperFetcher solrGrpHelperFetcher;
    private UnmatchedContentCache unmatchedContentCache;
    private SolrAnalysisDataFetcher solrAnalysisDataFetcher;
    private boolean useUnmatchedContentCache;

    public static SolrFetchers build(NamedList initArgs) {
        SolrFetchers solrFetchers = new SolrFetchers();
        solrFetchers.setSolrAnalysisDataFetcher(buildSolrAnalysisDataFetcher(initArgs));
        solrFetchers.setSolrGrpHelperFetcher(buildSolrGrpHelperFetcher(initArgs));
        solrFetchers.setUseUnmatchedContentCache(
                getBooleanArgument(initArgs, ARG_USE_UNMATCHED_CONTENT_CACHE, true));
        solrFetchers.setUnmatchedContentCache(buildUnmatchedContentCache(initArgs));
        return solrFetchers;
    }


    public static SolrAnalysisDataFetcher buildSolrAnalysisDataFetcher(NamedList args) {
        // Build analysis data solr client
        String analysisDataSolrUrl = PluginArgsExtractor.getArgument(args, ARG_SOLR_FETCHERS_ANALYSIS_DATA_BASE_URL,
                ARG_SOLR_DEFAULT_BASE_URL);
        int analysisDataMaxConnections = PluginArgsExtractor.getIntArgument(args,
                ARG_SOLR_FETCHERS_ANALYSIS_DATA_MAX_CONNECTIONS, SOLR_DEFAULT_MAX_CONNECTIONS);
        int analysisDataMaxConnectionsPerHost = PluginArgsExtractor.getIntArgument(args,
                ARG_SOLR_FETCHERS_ANALYSIS_DATA_MAX_CONNECTIONS_PER_HOST, SOLR_DEFAULT_MAX_CONNECTIONS_PER_HOST);
        int socketTimeout = PluginArgsExtractor.getIntArgument(args,
                ARG_SOLR_FETCHERS_ANALYSIS_DATA_CONNECTION_TIMEOUT, SOLR_DEFAULT_TIMEOUT);
        String solrUrl = analysisDataSolrUrl + SolrConstants.ANALYSIS_DATA_CORE_NAME;
        SolrClient solrClient = buildSolrClient(solrUrl, analysisDataMaxConnections, analysisDataMaxConnectionsPerHost,
                socketTimeout);


        boolean compressAnalysisData = PluginArgsExtractor.getBooleanArgument(args,
                ARG_SOLR_FETCHERS_ANALYSIS_DATA_COMPRESSED, DEFAULT_ANALYSIS_DATA_COMPRESSED);

        // Build compatibility map for previous analysis data documents
        String compatibilityMapExp = PluginArgsExtractor.getArgument(args,
                ARC_SOLR_FETCHERS_ANALYSIS_DATA_COMPATIBILITY_MAP, ANALYSIS_DATA_DEFAULT_DESERIALIZE_COMPATIBILITY_MAP);
        Map<String, String> compatibilityMap = parseMap(compatibilityMapExp);

        logger.info("Initialized SolrAnalysisDataFetcher: " +
                        "(url: {}, maxConnections: {}, maxConnectionsPerHost, timeout: {}, compressed: {})",
                solrUrl, solrUrl, socketTimeout, compressAnalysisData);
        return new SolrAnalysisDataFetcher(solrClient, compressAnalysisData, compatibilityMap);
    }

    private static Map<String, String> parseMap(String compatibilityMapExp) {
        Map<String, String> result = Maps.newHashMap();
        String actualExp = compatibilityMapExp.substring(1, compatibilityMapExp.length() - 1);
        String[] mapEntries = actualExp.split(",");
        Stream.of(mapEntries).forEach(entry -> {
            String[] keyAndValue = entry.split(":");
            result.put(keyAndValue[0], keyAndValue[1]);
        });
        return result;
    }

    public static SolrGrpHelperFetcher buildSolrGrpHelperFetcher(NamedList args) {
        // Build Solr client
        String baseUrl = PluginArgsExtractor.getArgument(args, ARG_GRP_HELPER_BASE_URL, ARG_SOLR_DEFAULT_BASE_URL);
        int fetchersMaxConnections = PluginArgsExtractor.getIntArgument(args, ARG_GRP_HELPER_MAX_CONNECTIONS,
                SOLR_DEFAULT_MAX_CONNECTIONS);
        int fetchersMaxConnectionsPerHost = PluginArgsExtractor.getIntArgument(args,
                ARG_GRP_HELPER_MAX_CONNECTIONS_PER_HOST, SOLR_DEFAULT_MAX_CONNECTIONS_PER_HOST);
        int fetchersTimeout = PluginArgsExtractor.getIntArgument(args, ARG_GRP_HELPER_CONNECTION_TIMEOUT,
                SOLR_DEFAULT_TIMEOUT);
        String solrUrl = baseUrl + SolrConstants.GRP_HELPER_CORE_NAME;
        SolrClient solrClient = buildSolrClient(solrUrl, fetchersMaxConnections, fetchersMaxConnectionsPerHost, fetchersTimeout);

        logger.info("Initialized GrpHelper: " +
                        "(url: {}, maxConnections: {}, maxConnectionsPerHost, timeout: {})",
                solrUrl, fetchersMaxConnections, fetchersMaxConnectionsPerHost, fetchersTimeout);
        return new SolrGrpHelperFetcher(solrClient);
    }

    public static UnmatchedContentCache buildUnmatchedContentCache(NamedList args) {
        //Build solr client
        String url = PluginArgsExtractor.getArgument(args, ARG_UNMATCHED_CONTENT_CACHE_BASE_URL,
                ARG_SOLR_DEFAULT_BASE_URL);
        int fetchersMaxConnections = PluginArgsExtractor.getIntArgument(args,
                ARG_UNMATCHED_CONTENT_CACHE_MAX_CONNECTIONS, SOLR_DEFAULT_MAX_CONNECTIONS);
        int fetchersMaxConnectionsPerHost = PluginArgsExtractor.getIntArgument(args,
                ARG_UNMATCHED_CONTENT_CACHE_MAX_CONNECTIONS_PER_HOST, SOLR_DEFAULT_MAX_CONNECTIONS_PER_HOST);
        int fetchersTimeout = PluginArgsExtractor.getIntArgument(args,
                ARG_UNMATCHED_CONTENT_CACHE_MAX_CONNECTION_TIMEOUT, SOLR_DEFAULT_TIMEOUT);
        SolrClient solrClient = buildSolrClient(url, fetchersMaxConnections, fetchersMaxConnectionsPerHost, fetchersTimeout);

        // Build unmatched content repository
        SolrUnmatchedContentRepository solrUnmatchedContentRepository = new SolrUnmatchedContentRepository(
                solrClient, SolrConstants.UNMATCHED_CONTENT_CORE_NAME);

        int cacheSize = getIntArgument(args, ARG_CACHE_UNMATCHED_CONTENT_MAX_SIZE, DEFAULT_CACHE_MAX_SIZE);
        int cacheConcurrency = getIntArgument(args, ARG_CACHE_UNMATCHED_CONTENT_CACHE_CONCURRENCY,
                DEFAULT_CACHE_CONCURRENCY);

        logger.info("Initialized UnmatchedContentCache: " +
                        "(url: {}, maxConnections: {}, maxConnectionsPerHost, timeout: {}, size:{}, concurrency: {})",
                url + SolrConstants.UNMATCHED_CONTENT_CORE_NAME, fetchersMaxConnections, fetchersMaxConnectionsPerHost,
                fetchersTimeout, cacheSize, cacheConcurrency);
        return new UnmatchedContentCache(solrUnmatchedContentRepository, cacheSize, cacheConcurrency);
    }


    private static SolrClient buildSolrClient(String solrUrl, int maxConnections, int maxConnectionsPerHost,
                                              int socketTimeout) {

        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set(HttpClientUtil.PROP_MAX_CONNECTIONS, maxConnections);
        params.set(HttpClientUtil.PROP_MAX_CONNECTIONS_PER_HOST, maxConnectionsPerHost);
        HttpClient httpClient = HttpClientUtil.createClient(params);
        if (isSolrCloudUrl(solrUrl)) {
            String[] split = solrUrl.split("/", 2);
            String zkUrl = split[0];
            String zkRoot = (split.length == 1) ? null : ("/" + split[1]);


            return new CloudSolrClient
                    .Builder(Lists.newArrayList(zkUrl), Optional.of(zkRoot))
                    .withSocketTimeout(socketTimeout)
                    .withHttpClient(httpClient)
                    .build();
        } else {
            return new HttpSolrClient.Builder()
                    .withBaseSolrUrl(solrUrl)
                    .withHttpClient(httpClient)
                    .withSocketTimeout(socketTimeout)
                    .allowCompression(true)
                    .build();

        }
    }

    private static boolean isSolrCloudUrl(String solrUrl) {
        return !solrUrl.startsWith("http");
    }


    public SolrGrpHelperFetcher getSolrGrpHelperFetcher() {
        return solrGrpHelperFetcher;
    }

    public void setSolrGrpHelperFetcher(SolrGrpHelperFetcher solrGrpHelperFetcher) {
        this.solrGrpHelperFetcher = solrGrpHelperFetcher;
    }

    public UnmatchedContentCache getUnmatchedContentCache() {
        return unmatchedContentCache;
    }

    public void setUnmatchedContentCache(UnmatchedContentCache unmatchedContentCache) {
        this.unmatchedContentCache = unmatchedContentCache;
    }

    public void setUseUnmatchedContentCache(boolean useUnmatchedContentCache) {
        this.useUnmatchedContentCache = useUnmatchedContentCache;
    }

    public boolean isUseUnmatchedContentCache() {
        return useUnmatchedContentCache;
    }

    public void setSolrAnalysisDataFetcher(SolrAnalysisDataFetcher solrAnalysisDataFetcher) {
        this.solrAnalysisDataFetcher = solrAnalysisDataFetcher;
    }

    public SolrAnalysisDataFetcher getSolrAnalysisDataFetcher() {
        return solrAnalysisDataFetcher;
    }

    @Override
    public String toString() {
        return "SolrFetchers{" +
                "solrGrpHelperFetcher=" + solrGrpHelperFetcher +
                ", unmatchedContentCache=" + unmatchedContentCache +
                ", solrAnalysisDataFetcher=" + solrAnalysisDataFetcher +
                ", useUnmatchedContentCache=" + useUnmatchedContentCache +
                '}';
    }
}
