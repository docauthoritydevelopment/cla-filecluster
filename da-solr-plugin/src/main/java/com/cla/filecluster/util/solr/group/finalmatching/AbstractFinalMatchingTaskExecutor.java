package com.cla.filecluster.util.solr.group.finalmatching;

import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTask;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;

import java.util.Collection;
import java.util.List;

public abstract class AbstractFinalMatchingTaskExecutor implements FinalMatchingTaskExecutor {

    protected final SolrPvDocument sourceDocument;
    protected SimilarityCalculator similarityCalculator;

    public AbstractFinalMatchingTaskExecutor(SolrPvDocument sourceDocument, SimilarityCalculator similarityCalculator) {
        this.sourceDocument = sourceDocument;
        this.similarityCalculator = similarityCalculator;
    }

    @Override
    public Collection<SimilarityMapKey> execute(List<PvAnalysisData> pvAnalysisDataList) {
        return newFinalMatchingTask(pvAnalysisDataList).extractSimilarityMapKeys();
    }

    protected abstract FinalMatchingTask newFinalMatchingTask(List<PvAnalysisData> pvAnalysisDataList);


}
