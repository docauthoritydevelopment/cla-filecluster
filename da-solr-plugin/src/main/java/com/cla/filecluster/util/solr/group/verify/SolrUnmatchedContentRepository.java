package com.cla.filecluster.util.solr.group.verify;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by: yael
 * Created on: 10/14/2018
 */
public class SolrUnmatchedContentRepository {

    private static Logger logger = LoggerFactory.getLogger(SolrUnmatchedContentRepository.class);
    private final String collection;

    private SolrClient solrClient;

    public SolrUnmatchedContentRepository(SolrClient solrClient, String collection) {
        this.solrClient = solrClient;
        this.collection = collection;
    }

    public Set<Long> getByContentId(Long contentId) {
        Set<Long> unmatchedContents = new HashSet<>();
        final SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        query.addFilterQuery("contentId1:" + contentId + " OR contentId2:" + contentId);
        try {
            QueryResponse queryResponse = solrClient.query(collection, query);
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                logger.debug("Unable to find document by id {}. Query: {}", contentId, query.toString());
                return unmatchedContents;
            }
            results.forEach(
                    r ->
                    {
                        Long contentId1 = (Long) r.getFieldValue("contentId1");
                        Long contentId2 = (Long) r.getFieldValue("contentId2");
                        if (contentId1.equals(contentId)) {
                            unmatchedContents.add(contentId2);
                        } else {
                            unmatchedContents.add(contentId1);
                        }
                    }
            );
            return unmatchedContents;
        } catch (SolrServerException e) {
            logger.error("Failed to find document by id - solr exception", e);
            throw new RuntimeException("Failed to find document by id - solr exception", e);
        } catch (IOException e) {
            logger.error("Failed to find Solr documents by ids - IOException", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        } catch (Exception e) {
            logger.error("Failed to find Solr documents by ids - unknown exception", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        }
    }

    public void addNewPair(Long contentId1, Long contentId2) {
        SolrInputDocument doc = new SolrInputDocument();
        try {
            String id = String.valueOf(contentId2) + "!" + String.valueOf(contentId1);
            doc.setField("id", id);
            doc.setField("contentId1", contentId2);
            doc.setField("contentId2", contentId1);
            solrClient.add(collection, doc);
        } catch (Exception e) {
            logger.error("Failed to add document - unknown exception", e);
            throw new RuntimeException("Failed to add document - solr exception", e);
        }
    }
}
