package com.cla.filecluster.util.solr.group.analysisdata;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.google.common.collect.Lists;
import org.apache.solr.request.SolrQueryRequest;

import java.util.*;
import java.util.function.Function;

/**
 * Represent a computation work done by the resource manager
 */
public class ComputationUnit<T> implements ComputationUnitInfo {

    private final Function<List<PvAnalysisData>, Collection<T>> algorithm;
    private final SolrQueryRequest req;
    private Iterator<Long> iterator;

    private final String threadName;
    private int workLeft;
    private int totalSize;
    private Collection<T> result = Collections.synchronizedList(Lists.newLinkedList());


    public ComputationUnit(SolrQueryRequest req, String threadName,
                           Function<List<PvAnalysisData>, Collection<T>> algorithm,
                           Collection<Long> ids) {
        this.req = req;
        this.threadName = threadName;
        this.algorithm = algorithm;
        iterator = new ArrayList<>(ids).iterator();
        totalSize = ids.size();
        workLeft = totalSize;
    }

    public boolean hasMoreIds() {
        return iterator.hasNext();
    }

    public String getThreadName() {
        return threadName;
    }

    public Long nextContentId() {
        return iterator.next();
    }

    public int getWorkLeft() {
        return workLeft;
    }

    @Override
    public int getTotalWork() {
        return totalSize;
    }


    public void compute(List<PvAnalysisData> pvAnalysisDataList) {
        result.addAll(algorithm.apply(pvAnalysisDataList));
    }

    public int getSize() {
        return totalSize;
    }

    public void batchDone(int batchSize) {
        workLeft -= batchSize;
    }

    public SolrQueryRequest getReq() {
        return req;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ComputationUnit dataStore = (ComputationUnit) o;
        return Objects.equals(threadName, dataStore.threadName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(threadName);
    }

    public Collection<T> getResult() {
        return result;
    }

    public ComputationUnitInfo toInfo() {
        return this;
    }
}
