package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Weight;
import org.apache.solr.search.SolrIndexSearcher;

import java.io.IOException;

public class ClaSimilaritySearcher extends SolrIndexSearcher {


    public ClaSimilaritySearcher(SolrIndexSearcher solrIndexSearcher) throws IOException {
        super(solrIndexSearcher.getCore(), solrIndexSearcher.getPath(),
                solrIndexSearcher.getSchema(), solrIndexSearcher.getName(),
                solrIndexSearcher.getIndexReader(), true, solrIndexSearcher.isCachingEnabled(), false, solrIndexSearcher.getCore().getDirectoryFactory());
        assert solrIndexSearcher.getSchema().getSimilarity() instanceof ClaLuceneSimilarity;
    }

    @Override
    public Weight createWeight(Query query, boolean needsScores, float boost) throws IOException {
        if (shouldUseCompatibleQuery(query)) {
            return createCompatibleWeight(query, needsScores);
        }
        return super.createWeight(query,needsScores,boost);
    }

    private boolean shouldUseCompatibleQuery(Query query) {
        return query instanceof BooleanQuery;
    }

    public ClaWeight createCompatibleWeight(Query query, boolean needsScores) throws IOException {
        query = rewrite(query);
        ClaQuery claQuery = ClaSimilarityUtils.toClaQuery(query);
        ClaWeight weight = claQuery.createClaWeight(this, needsScores);
        float v = weight.getValueForNormalization();
        float norm = ClaSimilarityUtils.queryNorm(getSimilarity(needsScores), v);
        if (Float.isInfinite(norm) || Float.isNaN(norm)) {
            norm = 1.0f;
        }
        weight.normalize(norm, 1.0f);
        return weight;
    }



}
