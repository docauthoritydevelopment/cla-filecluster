package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.*;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.Bits;

import java.io.IOException;
import java.util.*;

public class ClaBooleanWeight extends ClaWeight {

    /**
     * The Similarity implementation.
     */
    final Similarity similarity;
    final ClaBooleanQuery query;

    final ArrayList<ClaWeight> weights;
    final int maxCoord;  // num optional + num required
    final boolean disableCoord;
    final boolean needsScores;
    final float coords[];

    ClaBooleanWeight(ClaBooleanQuery query, ClaSimilaritySearcher searcher, boolean needsScores, boolean disableCoord) throws IOException {
        super(query);
        this.query = query;
        this.needsScores = needsScores;
        this.similarity = searcher.getSimilarity(needsScores);
        weights = new ArrayList<>();
        int i = 0;
        int maxCoord = 0;
        for (ClaBooleanClause c : query) {
            ClaWeight w = c.getQuery().createClaWeight(searcher, needsScores && c.isScoring());
            weights.add(w);
            if (c.isScoring()) {
                maxCoord++;
            }
            i += 1;
        }
        this.maxCoord = maxCoord;

        // precompute coords (0..N, N).
        // set disableCoord when its explicit, scores are not needed, no scoring clauses, or the sim doesn't use it.
        coords = new float[maxCoord + 1];
        Arrays.fill(coords, 1F);
        coords[0] = 0f;
        if (maxCoord > 0 && needsScores && disableCoord == false) {
            // compute coords from the similarity, look for any actual ones.
            boolean seenActualCoord = false;
            for (i = 1; i < coords.length; i++) {
                coords[i] = coord(i, maxCoord);
                seenActualCoord |= (coords[i] != 1F);
            }
            this.disableCoord = seenActualCoord == false;
        } else {
            this.disableCoord = true;
        }
    }

    @Override
    public void extractTerms(Set<Term> terms) {
        int i = 0;
        for (ClaBooleanClause clause : query) {
            if (clause.isScoring() || (needsScores == false && clause.isProhibited() == false)) {
                weights.get(i).extractTerms(terms);
            }
            i++;
        }
    }


    public float getValueForNormalization() {
        float sum = 0.0f;
        int i = 0;
        for (ClaBooleanClause clause : query) {
            // call sumOfSquaredWeights for all clauses in case of side effects
            float s = weights.get(i).getValueForNormalization();         // sum sub weights
            if (clause.isScoring()) {
                // only add to sum for scoring clauses
                sum += s;
            }
            i += 1;
        }

        return sum;
    }

    public float coord(int overlap, int maxOverlap) {
        if (overlap == 0) {
            // special case that there are only non-scoring clauses
            return 0F;
        } else if (maxOverlap == 1) {
            // LUCENE-4300: in most cases of maxOverlap=1, BQ rewrites itself away,
            // so coord() is not applied. But when BQ cannot optimize itself away
            // for a single clause (minNrShouldMatch, prohibited clauses, etc), it's
            // important not to apply coord(1,1) for consistency, it might not be 1.0F
            return 1F;
        } else {
            // common case: use the similarity to compute the coord
            return ClaSimilarityUtils.coord(similarity, overlap, maxOverlap);
        }
    }

    public void normalize(float norm, float boost) {
        for (ClaWeight w : weights) {
            // normalize all clauses, (even if non-scoring in case of side affects)
            w.normalize(norm, boost);
        }
    }

    @Override
    public Explanation explain(LeafReaderContext context, int doc) throws IOException {
        final int minShouldMatch = query.getMinimumNumberShouldMatch();
        List<Explanation> subs = new ArrayList<>();
        int coord = 0;
        float sum = 0.0f;
        boolean fail = false;
        int matchCount = 0;
        int shouldMatchCount = 0;
        Iterator<ClaBooleanClause> cIter = query.iterator();
        for (Iterator<ClaWeight> wIter = weights.iterator(); wIter.hasNext(); ) {
            Weight w = wIter.next();
            ClaBooleanClause c = cIter.next();
            Explanation e = w.explain(context, doc);
            if (e.isMatch()) {
                if (c.isScoring()) {
                    subs.add(e);
                    sum += e.getValue();
                    coord++;
                } else if (c.isRequired()) {
                    subs.add(Explanation.match(0f, "match on required clause, product of:",
                            Explanation.match(0f, ClaBooleanClause.Occur.FILTER + " clause"), e));
                } else if (c.isProhibited()) {
                    subs.add(Explanation.noMatch("match on prohibited clause (" + c.getQuery().toString() + ")", e));
                    fail = true;
                }
                if (!c.isProhibited()) {
                    matchCount++;
                }
                if (c.getOccur() == ClaBooleanClause.Occur.SHOULD) {
                    shouldMatchCount++;
                }
            } else if (c.isRequired()) {
                subs.add(Explanation.noMatch("no match on required clause (" + c.getQuery().toString() + ")", e));
                fail = true;
            }
        }
        if (fail) {
            return Explanation.noMatch("Failure to meet condition(s) of required/prohibited clause(s)", subs);
        } else if (matchCount == 0) {
            return Explanation.noMatch("No matching clauses", subs);
        } else if (shouldMatchCount < minShouldMatch) {
            return Explanation.noMatch("Failure to match minimum number of optional clauses: " + minShouldMatch, subs);
        } else {
            // we have a match
            Explanation result = Explanation.match(sum, "sum of:", subs);
            final float coordFactor = disableCoord ? 1.0f : coord(coord, maxCoord);
            if (coordFactor != 1f) {
                result = Explanation.match(sum * coordFactor, "product of:",
                        result, Explanation.match(coordFactor, "coord(" + coord + "/" + maxCoord + ")"));
            }
            return result;
        }
    }

    static BulkScorer disableScoring(final BulkScorer scorer) {
        return new BulkScorer() {

            @Override
            public int score(final LeafCollector collector, Bits acceptDocs, int min, int max) throws IOException {
                final ClaLeafCollector noScoreCollector = new ClaLeafCollector() {
                    ClaFakeScorer fake = new ClaFakeScorer();

                    @Override
                    public void setScorer(Scorer scorer) throws IOException {
                        collector.setScorer(fake);
                    }

                    @Override
                    public void setScorer(ClaScorer scorer) throws IOException {
                        collector.setScorer(scorer);
                    }

                    @Override
                    public void collect(int doc) throws IOException {
                        fake.doc = doc;
                        collector.collect(doc);
                    }
                };
                return scorer.score(noScoreCollector, acceptDocs, min, max);
            }

            @Override
            public long cost() {
                return scorer.cost();
            }
        };
    }

    // Return a BulkScorer for the optional clauses only,
    // or null if it is not applicable
    // pkg-private for forcing use of ClaBooleanScorer in tests
    BulkScorer optionalBulkScorer(LeafReaderContext context) throws IOException {
        List<BulkScorer> optional = new ArrayList<BulkScorer>();
        Iterator<ClaBooleanClause> cIter = query.iterator();
        for (Weight w : weights) {
            ClaBooleanClause c = cIter.next();
            if (c.getOccur() != ClaBooleanClause.Occur.SHOULD) {
                continue;
            }
            BulkScorer subScorer = w.bulkScorer(context);

            if (subScorer != null) {
                optional.add(subScorer);
            }
        }

        if (optional.size() == 0) {
            return null;
        }

        if (query.getMinimumNumberShouldMatch() > optional.size()) {
            return null;
        }

        if (optional.size() == 1) {
            BulkScorer opt = optional.get(0);
            if (!disableCoord && maxCoord > 1) {
                return new ClaBooleanTopLevelScorers.BoostedBulkScorer(opt, coord(1, maxCoord));
            } else {
                return opt;
            }
        }

        return new ClaBooleanScorer(this, disableCoord, maxCoord, optional, Math.max(1, query.getMinimumNumberShouldMatch()), needsScores);
    }

    // Return a BulkScorer for the required clauses only,
    // or null if it is not applicable
    private BulkScorer requiredBulkScorer(LeafReaderContext context) throws IOException {
        BulkScorer scorer = null;

        Iterator<ClaBooleanClause> cIter = query.iterator();
        for (Weight w : weights) {
            ClaBooleanClause c = cIter.next();
            if (c.isRequired() == false) {
                continue;
            }
            if (scorer != null) {
                // we don't have a BulkScorer for conjunctions
                return null;
            }
            scorer = w.bulkScorer(context);
            if (scorer == null) {
                // no matches
                return null;
            }
            if (c.isScoring() == false) {
                if (needsScores) {
                    scorer = disableScoring(scorer);
                }
            } else {
                assert maxCoord == 1;
            }
        }
        return scorer;
    }

    /**
     * Try to build a boolean scorer for this weight. Returns null if {@link ClaBooleanScorer}
     * cannot be used.
     */
    BulkScorer booleanScorer(LeafReaderContext context) throws IOException {
        final int numOptionalClauses = query.getClauses(ClaBooleanClause.Occur.SHOULD).size();
        final int numRequiredClauses = query.getClauses(ClaBooleanClause.Occur.MUST).size() + query.getClauses(ClaBooleanClause.Occur.FILTER).size();

        BulkScorer positiveScorer;
        if (numRequiredClauses == 0) {
            positiveScorer = optionalBulkScorer(context);
            if (positiveScorer == null) {
                return null;
            }

            // TODO: what is the right heuristic here?
            final long costThreshold;
            if (query.getMinimumNumberShouldMatch() <= 1) {
                // when all clauses are optional, use ClaBooleanScorer aggressively
                // TODO: is there actually a threshold under which we should rather
                // use the regular scorer?
                costThreshold = -1;
            } else {
                // when a minimum number of clauses should match, ClaBooleanScorer is
                // going to score all windows that have at least minNrShouldMatch
                // matches in the window. But there is no way to know if there is
                // an intersection (all clauses might match a different doc ID and
                // there will be no matches in the end) so we should only use
                // ClaBooleanScorer if matches are very dense
                costThreshold = context.reader().maxDoc() / 3;
            }

            if (positiveScorer.cost() < costThreshold) {
                return null;
            }

        } else if (numRequiredClauses == 1
                && numOptionalClauses == 0
                && query.getMinimumNumberShouldMatch() == 0) {
            positiveScorer = requiredBulkScorer(context);
        } else {
            // TODO: there are some cases where ClaBooleanScorer
            // would handle conjunctions faster than
            // BooleanScorer2...
            return null;
        }

        if (positiveScorer == null) {
            return null;
        }

        List<ClaScorer> prohibited = new ArrayList<>();
        Iterator<ClaBooleanClause> cIter = query.iterator();
        for (ClaWeight w : weights) {
            ClaBooleanClause c = cIter.next();
            if (c.isProhibited()) {
                ClaScorer scorer = w.compatibleScorer(context);
                if (scorer != null) {
                    prohibited.add(scorer);
                }
            }
        }

        if (prohibited.isEmpty()) {
            return positiveScorer;
        } else {
            Scorer prohibitedScorer = prohibited.size() == 1
                    ? prohibited.get(0)
                    : new ClaDisjunctionSumScorer(this, prohibited, coords, false);
            if (prohibitedScorer.twoPhaseIterator() != null) {
                // ClaReqExclBulkScorer can't deal efficiently with two-phased prohibited clauses
                return null;
            }
            return new ClaReqExclBulkScorer(positiveScorer, prohibitedScorer.iterator());
        }
    }

    @Override
    public BulkScorer bulkScorer(LeafReaderContext context) throws IOException {
        final BulkScorer bulkScorer = booleanScorer(context);
        if (bulkScorer != null) {
            // bulk scoring is applicable, use it
            return bulkScorer;
        } else {
            // use a Scorer-based impl (BS2)
            return super.bulkScorer(context);
        }
    }

    @Override
    public Scorer scorer(LeafReaderContext context) throws IOException {
        return compatibleScorer(context);
    }

    public ClaScorer compatibleScorer(LeafReaderContext context) throws IOException {
        ClaScorerSupplier scorerSupplier = compatibleScorerSupplier(context);
        if (scorerSupplier == null) {
            return null;
        }
        return scorerSupplier.get(false);
    }

    @Override
    public ScorerSupplier scorerSupplier(LeafReaderContext context) throws IOException {
        return compatibleScorerSupplier(context);
    }

    public ClaScorerSupplier compatibleScorerSupplier(LeafReaderContext context) throws IOException {
        int minShouldMatch = query.getMinimumNumberShouldMatch();

        final Map<ClaBooleanClause.Occur, Collection<ClaScorerSupplier>> scorers = new EnumMap<>(ClaBooleanClause.Occur.class);
        for (ClaBooleanClause.Occur occur : ClaBooleanClause.Occur.values()) {
            scorers.put(occur, new ArrayList<>());
        }

        Iterator<ClaBooleanClause> cIter = query.iterator();
        for (ClaWeight w : weights) {
            ClaBooleanClause c = cIter.next();
            ClaScorerSupplier subScorer = w.compatibleScorerSupplier(context);
            if (subScorer == null) {
                if (c.isRequired()) {
                    return null;
                }
            } else {
                scorers.get(c.getOccur()).add(subScorer);
            }
        }

        // scorer simplifications:

        if (scorers.get(ClaBooleanClause.Occur.SHOULD).size() == minShouldMatch) {
            // any optional clauses are in fact required
            scorers.get(ClaBooleanClause.Occur.MUST).addAll(scorers.get(ClaBooleanClause.Occur.SHOULD));
            scorers.get(ClaBooleanClause.Occur.SHOULD).clear();
            minShouldMatch = 0;
        }

        if (scorers.get(ClaBooleanClause.Occur.FILTER).isEmpty() && scorers.get(ClaBooleanClause.Occur.MUST).isEmpty() && scorers.get(ClaBooleanClause.Occur.SHOULD).isEmpty()) {
            // no required and optional clauses.
            return null;
        } else if (scorers.get(ClaBooleanClause.Occur.SHOULD).size() < minShouldMatch) {
            // either >1 req scorer, or there are 0 req scorers and at least 1
            // optional scorer. Therefore if there are not enough optional scorers
            // no documents will be matched by the query
            return null;
        }

        // we don't need scores, so if we have required clauses, drop optional clauses completely
        if (!needsScores && minShouldMatch == 0 && scorers.get(ClaBooleanClause.Occur.MUST).size() + scorers.get(ClaBooleanClause.Occur.FILTER).size() > 0) {
            scorers.get(ClaBooleanClause.Occur.SHOULD).clear();
        }

        return new ClaBoolean2ScorerSupplier(this, scorers, disableCoord, coords, maxCoord, needsScores, minShouldMatch);
    }

}
