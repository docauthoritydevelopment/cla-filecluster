package com.cla.filecluster.util.solr.group.finalmatching;

import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;

import java.util.Collection;
import java.util.List;

public interface FinalMatchingTaskExecutor {

    Collection<SimilarityMapKey> execute(List<PvAnalysisData> pvAnalysisDataList);
}
