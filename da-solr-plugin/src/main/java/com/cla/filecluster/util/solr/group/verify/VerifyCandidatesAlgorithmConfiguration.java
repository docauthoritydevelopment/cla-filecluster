package com.cla.filecluster.util.solr.group.verify;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class VerifyCandidatesAlgorithmConfiguration {

    private boolean testDropThreshold;
    private int testMinFilteredThreshold;


}
