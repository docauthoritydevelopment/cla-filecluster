package com.cla.filecluster.util.solr.group.finalmatching.excel;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.pv.ExcelSimilarityParameters;
import com.cla.common.domain.dto.pv.GroupingResult;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.ContentGroupDetails;
import com.cla.filecluster.service.files.pv.ContentIdGroupsPool;
import com.cla.filecluster.service.files.pv.PvAnalysisDataPool;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTask;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ExcelFinalMatchingTask extends
        FinalMatchingTask<Map.Entry<Long, Map<String, List<String>>>, ExcelFinalMatchingTaskContext> {

    private static final Logger logger = LoggerFactory.getLogger(ExcelFinalMatchingTask.class);

    private Map<String, Float> sheetsDominanceRatio;
    private float skippedSheetsDominance;
    private Map<Long, Map.Entry<Long, Map<String, List<String>>>> entriesMap;

    public ExcelFinalMatchingTask(ExcelFinalMatchingTaskContext finalMatchingTaskContext) {
        super(finalMatchingTaskContext);
        this.sheetsDominanceRatio = finalMatchingTaskContext.getSheetsDominanceRatio();
        this.skippedSheetsDominance = finalMatchingTaskContext.getSkippedSheetsDominance();
        entriesMap = Maps.newHashMap();
        for (Map.Entry<Long, Map<String, List<String>>> entry : finalMatchingTaskContext.getSubList()) {
            entriesMap.put(getCandidateId(entry), entry);
        }
    }


    @Override
    public Pair<GroupingResult, Optional<SimilarityMapKey>> analyseMatch(Map.Entry<Long, Map<String, List<String>>> docEntry,
                                                                         PvAnalysisDataPool pvAnalysisDataPool, ContentIdGroupsPool contentPool) {
        Long targetId = docEntry.getKey();
        SolrPvDocument sourceDocument = finalMatchingTaskContext.getSourceDocument();
        GroupingResult groupingResult = finalizeExcelWorkbookSimilaritySingleFile(sourceDocument.getId(),
                sheetsDominanceRatio, sourceDocument.getPvAnalysisData(), docEntry,
                pvAnalysisDataPool, contentPool, skippedSheetsDominance);
        if (groupingResult.isMatch()) {
            return Pair.of(groupingResult, Optional.of(new SimilarityMapKey(targetId, null)));
        }
        return Pair.of(groupingResult, Optional.empty());
    }

    private GroupingResult finalizeExcelWorkbookSimilaritySingleFile(long sourceDocumentId,
                                                                     Map<String, Float> sheetsDominanceRatio,
                                                                     PvAnalysisData doc1PvAnalysisData,
                                                                     Map.Entry<Long, Map<String, List<String>>> docEntry,
                                                                     PvAnalysisDataPool pvAnalysisDataPool,
                                                                     ContentIdGroupsPool contentIdGroupsPool, float skippedSheetsDominance) {
        SimilarityCalculator similarityCalculator = finalMatchingTaskContext.getSimilarityCalculator();
        int workbook1Maturity = similarityCalculator.workbookMaturityLevel(doc1PvAnalysisData);

        if (workbook1Maturity == 0) {
            logger.info("Zero ML for workbook. Content md-id={}", sourceDocumentId);
        }
        ExcelSimilarityParameters esp = similarityCalculator.getExcelSimParamsByMaturity()[SimilarityCalculator
                .maturityCombinedValueMaxMaturityValue(workbook1Maturity)];

        final Map<String, String> sheetMatches = new HashMap<>();
        final float aggregatedSimilarity = PvAnalysisDataUtils.workBookAggregatedSimilarity(sourceDocumentId,
                docEntry.getKey(), docEntry.getValue(), sheetsDominanceRatio, sheetMatches);

        if (aggregatedSimilarity > esp.excelWorkbookAggregatedSimilarityThreshold) {
            logger.trace("aggregatedSimilarity ({}) > esp.excelWorkbookAggregatedSimilarityThreshold ({})",
                    aggregatedSimilarity, esp.excelWorkbookAggregatedSimilarityThreshold);
            // Candidate for match - get metadata from matchedDoc and re-calc matched sheet aggregated dominance
            final Long doc2Lid = docEntry.getKey();

            ContentGroupDetails contentGroupDetails = contentIdGroupsPool.getContentGroupId(doc2Lid);
            if (contentGroupDetails != null && AnalyzeHint.EXCLUDED.equals(contentGroupDetails.getAnalyzeHint())) {
                //File is excluded from grouping
                if (logger.isTraceEnabled()) {
                    logger.trace("Grouping result false cause candidateId:{} is EXCLUDED");
                }
                return GroupingResult.falseGroupingResult("?", 0);
            }
            // Get matched file detailed info and rule-out some potential false positive criteria (similar to Excel)
            final PvAnalysisData doc2PvAnalysisData = pvAnalysisDataPool.getRecord(doc2Lid);

            if (finalMatchingTaskContext.isGroupingForceWorkbookMaturityLevelSimilarity()) {
                int workbook2Maturity = similarityCalculator.workbookMaturityLevel(doc2PvAnalysisData);
                if (workbook1Maturity != workbook2Maturity) {
                    if (logger.isTraceEnabled()) {
                        logger.trace("finalyzeExcelWorkbookSimilarity ({},{}): failure due to maturity level mismatch {}<>{}", "?", doc2Lid, workbook1Maturity, workbook2Maturity);
                    }
                    return GroupingResult.falseGroupingResult();
                }
            }

            final Map<String, Float> matchedDocSheetsDominanceRatio = PvAnalysisDataUtils.getExcelSheetsDominance((ExcelAnalysisMetadata) doc2PvAnalysisData.getAnalysisMeadata());

            final float aggregatedSimilarity2 = similarityCalculator.workBookAggregatedSimilarityFullMatch(sourceDocumentId, docEntry.getKey(),
                    sheetsDominanceRatio, matchedDocSheetsDominanceRatio, sheetMatches,
                    doc1PvAnalysisData, doc2PvAnalysisData, skippedSheetsDominance);
            if (aggregatedSimilarity2 > esp.excelWorkbookAggregatedSimilarityThreshold) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Match: true, workBookAggregatedSimilarityFullMatch ({}) >" +
                                    " excelWorkbookAggregatedSimilarityThreshold ({})", aggregatedSimilarity2,
                            esp.excelWorkbookAggregatedSimilarityThreshold);
                }
                return new GroupingResult("+", 1, ' ', true);
            }
        }
        return GroupingResult.falseGroupingResult();
    }

    @Override
    public long getCandidateId(Map.Entry<Long, Map<String, List<String>>> entry) {
        return entry.getKey();
    }
}