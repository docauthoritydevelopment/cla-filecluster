package com.cla.filecluster.util.solr.group.fetchers;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.filecluster.service.files.pv.ContentGroupDetails;
import com.cla.filecluster.service.files.pv.GrpHelperFetcher;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class SolrGrpHelperFetcher extends GenericSolrDataFetcher implements GrpHelperFetcher {


    private static final Logger logger = LoggerFactory.getLogger(SolrGrpHelperFetcher.class);

    public SolrGrpHelperFetcher(SolrClient solrClient) {
        super(solrClient);
    }

    @Override
    public String getContentGroupId(long contentId) {
        SolrDocument document = getDocument(contentId);
        if (document != null) {
            return (String) document.getFieldValue("groupId");
        }
        return null;
    }

    @Override
    public UUID getContentGroupIdUUID(long contentId) {
        String contentGroupId = getContentGroupId(contentId);
        return getGroupUUID(contentGroupId);
    }

    private UUID getGroupUUID(String fileGroup) {
        return (fileGroup == null) ? null : UUID.fromString(fileGroup);
    }

    @Override
    public ContentGroupDetails getContentGroupDetails(long contentId) {
        SolrDocument document = getDocument(contentId);
        String groupId = (String) document.getFieldValue("groupId");
        AnalyzeHint analyzeHint = AnalyzeHint.convert((String) document.getFieldValue("analysisHint"));
        return new ContentGroupDetails(groupId, analyzeHint);
    }

    @Override
    public Map<Long, ContentGroupDetails> getContentGroupByIds(Collection<Long> contentIds) {
        List<SolrDocument> documents = findDocuments(contentIds);
        Map<Long, ContentGroupDetails> result = new HashMap<>();
        for (SolrDocument document : documents) {
            try {
//                long id = (Long) document.getFieldValue("id");
                long id = Long.parseLong((String) document.getFieldValue("id"));
                String groupId = (String) document.getFieldValue("groupId");
                AnalyzeHint analyzeHint = null;
                String analysisHint = (String) document.getFieldValue("analysisHint");
                try {
                    analyzeHint = AnalyzeHint.convert(analysisHint);
                } catch (Exception e) {
                    logger.error("Failed to convert analyze hint to enum (" + analysisHint + ")");
                    analyzeHint = AnalyzeHint.NORMAL;
                }
                result.put(id, new ContentGroupDetails(groupId, analyzeHint));
            } catch (Exception e) {
                logger.error("Failed to convert solr document {} into ContentGroupDetails", document, e);
            }
        }
        return result;
    }

    @Override
    public void updateStatistics(long time, int size) {

    }

}
