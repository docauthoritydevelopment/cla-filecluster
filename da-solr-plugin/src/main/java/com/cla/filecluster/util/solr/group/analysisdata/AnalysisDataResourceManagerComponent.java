package com.cla.filecluster.util.solr.group.analysisdata;

import com.cla.common.utils.FileSizeUnits;
import com.cla.filecluster.util.solr.SearchComponentMetricFilter;
import com.codahale.metrics.Metric;
import com.google.common.collect.Maps;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.metrics.SolrMetricManager;
import org.apache.solr.metrics.SolrMetricProducer;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.stream.Stream;

import static com.cla.filecluster.util.solr.PluginArgsExtractor.*;
import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchers.ANALYSIS_DATA_DEFAULT_DESERIALIZE_COMPATIBILITY_MAP;

public class AnalysisDataResourceManagerComponent extends SearchComponent implements SolrCoreAware, SolrMetricProducer {

    private static final Logger logger = LoggerFactory.getLogger(AnalysisDataResourceManagerComponent.class);

    /*--- ANALYSIS DATA MEMORY MANAGEMENT CONFIG PARAMS ----*/
    private static final String ANALYSIS_DATA_MEMORY_PERCENTAGE = "memoryPercent";
    private static final String ANALYSIS_DATA_SIZE_FACTOR = "sizeFactor";
    private static final String ANALYSIS_DATA_BATCH_SIZE = "batchSize";

    private static final String ANALYSIS_DATA_LOAD_TIMEOUT = "timeoutInMinutes";
    private static final String ANALYSIS_DATA_FETCH_INTERVAL_EXP = "fetchIntervalExp";
    private static final String ANALYSIS_DATA_FETCH_CONCURRENCY = "fetchConcurrency";
    private static final String ANALYSIS_DATA_MAX_HEAP_MEMORY_PERCENTAGE = "maxHeapMemoryPercentage";
    private static final String ANALYSIS_DATA_WAIT_TICKS_BEFORE_GC = "waitTicksBeforeGC";
    private static final String ANALYSIS_DATA_MAX_DOCUMENT_SIZE_IN_KB = "maxDocumentSizeInKB";


    private static final int SAMPLE_WORK_LIMIT = 10;

    private static AnalysisDataResourceManager instance;


    @Override
    public void init(NamedList args) {
        initializeAnalysisDataResourceManager(args);
    }

    private void initializeAnalysisDataResourceManager(NamedList args) {
        if (instance != null) {
            instance.shutDown();
        }
        AnalysisDataResourceManagementConfig analysisResourceManagementConfig =
                buildAnalysisDataResourceManagementConfig(args);
        instance = new AnalysisDataResourceManager(analysisResourceManagementConfig);

        logger.info("Initialized AnalysisDataResourceManager: \n(memoryLimit: {},\n sizeFactor:{},\n batchSize:{}," +
                        ",\ntimeout: {} minutes,\nfetchInterval:{},\nmaxHeapMemoryThresholdPercentage: {}," +
                        "\nwaitTicksBeforeGC: {},\nconcurrency: {})",
                analysisResourceManagementConfig.getMemoryLimitInKB(),
                analysisResourceManagementConfig.getAnalysisDataSizeFactor(),
                analysisResourceManagementConfig.getBatchSize(),
                analysisResourceManagementConfig.getTimeoutInMinutes(),
                analysisResourceManagementConfig.getFetchIntervalExp(),
                analysisResourceManagementConfig.getMaxHeapMemoryThresholdPercentage(),
                analysisResourceManagementConfig.getWaitTicksBeforeGC(),
                analysisResourceManagementConfig.getLoadConcurrency());
    }

    private AnalysisDataResourceManagementConfig buildAnalysisDataResourceManagementConfig(NamedList args) {

        return AnalysisDataResourceManagementConfig.builder()
                .analysisDataSizeFactor(getFloatArgument(args, ANALYSIS_DATA_SIZE_FACTOR, 2.0f))
                .memoryLimitInKB(calcMemoryLimit(getIntArgument(args, ANALYSIS_DATA_MEMORY_PERCENTAGE, 40)))
                .maxDocumentSizeInKB(getIntArgument(args, ANALYSIS_DATA_MAX_DOCUMENT_SIZE_IN_KB, 1000))
                .maxHeapMemoryThresholdPercentage((double) getIntArgument(args,
                        ANALYSIS_DATA_MAX_HEAP_MEMORY_PERCENTAGE, 70) / (double) 100)
                .waitTicksBeforeGC(getIntArgument(args, ANALYSIS_DATA_WAIT_TICKS_BEFORE_GC, 3))
                .batchSize(getIntArgument(args, ANALYSIS_DATA_BATCH_SIZE, 10000))
                .timeoutInMinutes(getLongArgument(args, ANALYSIS_DATA_LOAD_TIMEOUT, 10))
                .fetchIntervalExp(getArgument(args, ANALYSIS_DATA_FETCH_INTERVAL_EXP, "1;SECONDS"))
                .loadConcurrency(getIntArgument(args, ANALYSIS_DATA_FETCH_CONCURRENCY, 5))
                .compatibilityMap(parseMap(args))
                .build();
    }

    public static AnalysisDataResourceManager getManager() {
        return instance;
    }


    private Map<String, String> parseMap(NamedList args) {
        String compatibilityMapExp = getArgument(args, "solr.analysisData.deserialize.compatibilityMap",
                ANALYSIS_DATA_DEFAULT_DESERIALIZE_COMPATIBILITY_MAP);
        Map<String, String> result = Maps.newHashMap();
        String actualExp = compatibilityMapExp.substring(1, compatibilityMapExp.length() - 1);
        String[] mapEntries = actualExp.split(",");
        Stream.of(mapEntries).forEach(entry -> {
            String[] keyAndValue = entry.split(":");
            result.put(keyAndValue[0], keyAndValue[1]);
        });
        return result;
    }


    @Override
    public void prepare(ResponseBuilder rb) {
    }

    @Override
    public void process(ResponseBuilder rb) {
        logger.info("Requests are not supported, this is only for internal usage of the AnalysisDataResourceManager");
    }

    @Override
    public String getDescription() {
        return "Responsible for fetching analysis data constraint by the jvm memory";
    }

    @Override
    public void inform(SolrCore core) {

    }

    private int calcMemoryLimit(int percentage) {
        return (int) FileSizeUnits.BYTE.toKilobytes(Runtime.getRuntime().maxMemory() * (percentage / 100f));
    }


    @Override
    public void initializeMetrics(SolrMetricManager manager, String registry, String tag, String scope) {
        AnalysisDataResourceManager.Stats stats = getManager().getStats();
        manager.registerGauge(this, registry, () -> stats.analysisDataLoadRateInMillis(), tag, true,
                "analysisDataLoadRateInMillis", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> stats.analysisDataComputeRateInMillis(), tag, true,
                "analysisDataComputeRateInMillis", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> stats.sampleWorkDistribution(SAMPLE_WORK_LIMIT), tag,
                true, "workDistribution (" + SAMPLE_WORK_LIMIT + ")", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> stats.usedMemoryPercentage(), tag, true,
                "memoryConsumption", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> stats.highMemoryCount(), tag, true,
                "highMemoryConsumptionCount", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> stats.isHighMemoryConsumption(), tag, true,
                "highMemoryConsumptionWarning", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> stats.forcedGCCount(), tag, true,
                "forcedGCCounter", getCategory().toString(), scope);


        this.registry = manager.registry(registry);
        Map<String, Metric> metrics = manager.getMetrics(registry, SearchComponentMetricFilter.of(this));
        if (metrics != null) {
            this.metricNames.addAll(metrics.keySet());
        }

    }
}
