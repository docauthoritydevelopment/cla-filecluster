package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.Query;

import java.io.IOException;

public abstract class ClaQuery extends Query {

    public abstract ClaWeight createClaWeight(ClaSimilaritySearcher searcher, boolean needsScores) throws IOException;

    public ClaQuery rewriteToCla(IndexReader reader) {
        return this;
    }
}
