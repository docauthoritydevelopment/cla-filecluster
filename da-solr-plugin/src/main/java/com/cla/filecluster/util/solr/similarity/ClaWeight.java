package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.Weight;

import java.io.IOException;

public abstract class ClaWeight extends Weight {
    /**
     * Sole constructor, typically invoked by sub-classes.
     *
     * @param query the parent query
     */
    protected ClaWeight(Query query) {
        super(query);
    }

    @Override
    public boolean isCacheable(LeafReaderContext ctx) {
        return false;
    }

    protected abstract float getValueForNormalization();

    protected abstract void normalize(float norm, float boost);

    protected ClaScorerSupplier compatibleScorerSupplier(LeafReaderContext context) throws IOException {
        final ClaScorer scorer = compatibleScorer(context);
        if (scorer == null) {
            return null;
        }
        return new ClaScorerSupplier() {
            @Override
            public ClaScorer get(boolean randomAccess) {
                return scorer;
            }

            @Override
            public Scorer get(long leadCost) {
                return scorer;
            }

            @Override
            public long cost() {
                return scorer.iterator().cost();
            }
        };
    }

    protected abstract ClaScorer compatibleScorer(LeafReaderContext context) throws IOException;
}
