package com.cla.filecluster.util.solr.update;

import com.cla.common.constants.CatFolderSolrActions;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrRequestParams;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.*;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.handler.component.ShardRequest;
import org.apache.solr.handler.component.ShardResponse;
import org.apache.solr.metrics.SolrMetricManager;
import org.apache.solr.metrics.SolrMetricProducer;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.search.*;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.CommitUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorChain;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by: yael
 * Created on: 3/27/2018
 */
public class UpdateFolderBulkComponent extends SearchComponent implements SolrCoreAware,SolrMetricProducer {

    private static Logger logger = LoggerFactory.getLogger(UpdateFolderBulkComponent.class);

    private static final int DEFAULT_PAGE_SIZE = 10000;

    private int pageSize = DEFAULT_PAGE_SIZE;

    private SolrCore solrCore;

    private volatile long lastQueryTime = 0;

    private String lastQuery = "";

    @Override
    public void init(NamedList args) {
        super.init(args);
        Object updateParams = args.get("updateParams");
        if (updateParams != null) {
            pageSize = (int) ((NamedList) updateParams).get("pageSize");
        }
        logger.info("pageSize {}", pageSize);
    }

    /**
     * Prepare is PROBABLY called once before sending the message to all the shards
     *
     * @param rb - a single object that follows the entire lifecycle of the user query and holds all relevant data on it
     * @throws IOException If there is a low-level I/O error.
     */
    @Override
    public void prepare(ResponseBuilder rb) throws IOException {
    }


    /**
     * Called once on each shard. The actual processing of the request
     *
     * @param rb - a single object that follows the entire lifecycle of the user query and holds all relevant data on it
     * @throws IOException If there is a low-level I/O error.
     */
    @Override
    public void process(ResponseBuilder rb) throws IOException {
        long startTime = System.currentTimeMillis();
        SolrQueryRequest req = rb.req;
        SolrParams params = req.getParams();
        SolrIndexSearcher searcher = req.getSearcher();
        handleUpdateRequest(req, rb, startTime, params, searcher);
    }

    @Override
    public void initializeMetrics(SolrMetricManager manager, String registry, String tag, String scope) {
        manager.registerGauge(this, registry, () -> lastQueryTime, tag, true, "lastQueryTime", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> lastQuery, tag, true, "lastQuery", getCategory().toString(), scope);
    }


    private SolrQueryInput prepareQuery(SolrQueryRequest req, SolrParams params) {
        String defType = params.get(QueryParsing.DEFTYPE, QParserPlugin.DEFAULT_QTYPE);
        String q = params.get(CommonParams.Q);
        Query query = null;
        List<Query> filters = null;

        int flags = SolrIndexSearcher.GET_DOCLIST;

        try {
            if (q != null) {
                QParser parser = QParser.getParser(q, defType, req);
                query = parser.getQuery();
            }

            String[] fqs = req.getParams().getParams(CommonParams.FQ);
            if (fqs != null && fqs.length != 0) {
                filters = new ArrayList<>();
                for (String fq : fqs) {
                    if (fq != null && fq.trim().length() != 0) {
                        QParser fqp = QParser.getParser(fq, null, req);
                        filters.add(fqp.getQuery());
                    }
                }
            }
        } catch (SyntaxError e) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, e);
        }
        return new SolrQueryInput(query, filters, pageSize, flags);
    }

    /**
     * Run a query on the local solr shard index.
     * Can probably be replaced with SolrPluginUtils.runSimpleQuery(
     *
     * @param searcher SOLR index searcher
     * @return list of SOLR documents
     * @throws IOException If there is a low-level I/O error.
     */
    private DocList executeCoreQuery(SolrIndexSearcher searcher, SolrQueryInput solrQueryInput) throws IOException {
        long start = System.currentTimeMillis();
        logger.debug("**** Fetching documents by {}", solrQueryInput);
        DocList docList = searcher.getDocList(solrQueryInput.getQuery(), solrQueryInput.getFilters(), null, solrQueryInput.getOffset(), pageSize, solrQueryInput.getFlags());
        logger.debug("**** took {} ms for {}", System.currentTimeMillis() - start, solrQueryInput);
        return docList;
    }

    private int pagedUpdate(SolrQueryRequest req, SolrParams params, SolrIndexSearcher searcher, UpdateOperation updateOperation) throws IOException {
        SolrQueryInput solrQueryInput = prepareQuery(req, params);
        DocList queryResult = executeCoreQuery(searcher, solrQueryInput);
        while (queryResult.size() > 0) {
            logger.debug("**** Start Update field {} on {} documents, offset: {}", params.get("field"), queryResult.size(), solrQueryInput.offset);
            updateOperation.performUpdate(queryResult);
            logger.debug("**** End Update field {} on {} documents, offset: {}", params.get("field"), queryResult.size(), solrQueryInput.offset);
            solrQueryInput.offset += queryResult.size();
            if (queryResult.size() < solrQueryInput.getPageSize()) {
                break;
            }
            queryResult = executeCoreQuery(searcher, solrQueryInput);
        }
        return solrQueryInput.offset;
    }

    /**
     * Update the documents on the local shard
     * This function can probably be rewritten to use pages of Add requests that are processed together in the lucene/solr index
     *
     * @param field                  field to update
     * @param req                    SOLR query request
     * @param searcher               SOLR index searcher
     * @param updateRequestProcessor update request processor
     * @param queryResult            list of SOLR documents
     * @param fieldModifier          map of fields/values to be updated
     * @throws IOException If there is a low-level I/O error.
     */
    private void updateDocuments(String field, SolrQueryRequest req, SolrIndexSearcher searcher, ResponseBuilder rb,
                                   UpdateRequestProcessor updateRequestProcessor, DocList queryResult,
                                   Map<String, Object> fieldModifier) throws IOException {
        StringJoiner ids = new StringJoiner(",");
        if (queryResult.size() == 0) {
            return;
        }
        try {
            AddUpdateCommand addUpdateCommand = new AddUpdateCommand(req);
            Iterator iterator = queryResult.iterator();
            while (iterator != null && iterator.hasNext()) {
                updateSingleDocument(field, req, searcher, updateRequestProcessor, fieldModifier, ids, addUpdateCommand, iterator);
            }
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        } finally {
            updateRequestProcessor.processCommit(new CommitUpdateCommand(req, false));
            updateRequestProcessor.finish();
        }
    }

    @Override
    public void inform(SolrCore core) {
        this.solrCore = core;

    }

    private UpdateRequestProcessor getUpdateRequestProcessor(SolrQueryRequest req, SolrQueryResponse rsp, SolrParams params) {
        UpdateRequestProcessorChain processorChain =
                req.getCore().getUpdateProcessingChain(params.get(UpdateParams.UPDATE_CHAIN));

        return processorChain.createProcessor(req, rsp);
    }

    private void handleUpdateRequest(SolrQueryRequest req, ResponseBuilder rb, long startTime, SolrParams params, SolrIndexSearcher searcher) throws IOException {
        UpdateRequestProcessor updateRequestProcessor = getUpdateRequestProcessor(req, rb.rsp, params);
        String operation = params.get(SolrRequestParams.OPERATION);
        logger.trace("Handle Update Folder Fields Request. IsShard={} operation={}", params.getBool(ShardParams.IS_SHARD), operation);
        lastQuery = req.getParamString();

        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrRequestParams.OPERATION, operation);

        int totalSize = pagedUpdate(req, params, searcher,
                (queryResult) -> updateDocuments(operation, req, searcher, rb, updateRequestProcessor, queryResult, fieldModifier));
        logger.info("Finished Update Folder Fields Request (IsShard={} operation={}) on {} documents in {} ms.", params.getBool(ShardParams.IS_SHARD), operation, totalSize, lastQueryTime);
        lastQueryTime = System.currentTimeMillis() - startTime;
    }

    private void updateSingleDocument(String field, SolrQueryRequest req, SolrIndexSearcher searcher,
                                        UpdateRequestProcessor updateRequestProcessor, Map<String, Object> fieldModifier,
                                        StringJoiner ids, AddUpdateCommand addUpdateCommand, Iterator iterator) throws IOException {
        String operation = (String)fieldModifier.get(SolrRequestParams.OPERATION);

        DocIterator docIterator = (DocIterator)iterator;
        Document document = searcher.doc(docIterator.nextDoc());
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        addUpdateCommand.clear();
        addUpdateCommand.solrDoc = solrInputDocument;

        // the folder id to be updated
        String id = document.get("id");
        ids.add(id);
        addUpdateCommand.solrDoc.setField("id", id);

        /*for (IndexableField f : document.getFields()) {
            addUpdateCommand.solrDoc.setField(f.name(), f.stringValue());
        }*/

        if (operation.equalsIgnoreCase(CatFolderSolrActions.UPDATE_PARENT_INFO_NO_PARENT.getName())) {
            Map<String, Object> scopedFieldModifier = new HashMap<>();
            String[] values = document.getValues(CatFoldersFieldType.DEPTH_FROM_ROOT.getSolrName());
            String parentsInfo = values[0] + "." + id;
            scopedFieldModifier.put("set", parentsInfo);
            addUpdateCommand.solrDoc.setField(CatFoldersFieldType.PARENTS_INFO.getSolrName(), scopedFieldModifier);
        }

        updateRequestProcessor.processAdd(addUpdateCommand);
        logger.trace("Update docFolder={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
        if (logger.isTraceEnabled()) {
            logger.trace("Update docFolder={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
        }
    }

    @Override
    public String getDescription() {
        return "update folder data";
    }

    /**
     * Dispatch shard request in <code>STAGE_EXECUTE_QUERY</code> stage
     */
    @Override
    public int distributedProcess(ResponseBuilder rb) {
        SolrParams params = rb.req.getParams();
        logger.info("BulkUpdateRequest distributedProcess. rb.stage={} isShard={}", rb.stage, params.getBool(ShardParams.IS_SHARD));
        if (rb.stage < ResponseBuilder.STAGE_EXECUTE_QUERY)
            return ResponseBuilder.STAGE_EXECUTE_QUERY;
        if (rb.stage == ResponseBuilder.STAGE_EXECUTE_QUERY) {
            logger.trace("BulkUpdateRequest - create shard request");
            ShardRequest sreq = new ShardRequest();
            sreq.purpose = ShardRequest.PURPOSE_GET_TOP_IDS;
            sreq.params = new ModifiableSolrParams(rb.req.getParams());
            sreq.params.remove(ShardParams.SHARDS);
            rb.addRequest(this, sreq);
            return ResponseBuilder.STAGE_GET_FIELDS;
        }
        return ResponseBuilder.STAGE_DONE;
    }

    @Override
    public void handleResponses(ResponseBuilder rb, ShardRequest sreq) {
        logger.debug("BulkUpdateRequest - handle responses. purpose={}", sreq.purpose);
        if (sreq.purpose == ShardRequest.PURPOSE_GET_TOP_IDS) {
            logger.trace("ShardRequest.response.size: " + sreq.responses.size());
            SolrDocumentList responseDocs = new SolrDocumentList();
            long numFound = 0;
            for (ShardResponse srsp : sreq.responses) {
                logger.trace("ShardRequest.response.shard: " + srsp.getShard());
                SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse().getResponse().get("response");
                if (docs != null) {
                    logger.trace("ShardRequest.response.documents: " + docs.size());
                    numFound += docs.size();
                    responseDocs.addAll(docs);
                }
            }
            responseDocs.setNumFound(numFound);
            logger.trace("Write final response");
        } else {
            super.handleResponses(rb, sreq);
        }
    }
}
