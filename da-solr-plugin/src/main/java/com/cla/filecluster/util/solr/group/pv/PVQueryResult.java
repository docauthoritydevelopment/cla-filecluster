package com.cla.filecluster.util.solr.group.pv;

import com.cla.common.domain.dto.pv.PVType;
import org.apache.solr.search.DocList;

/**
 * Created by uri on 24/06/2015.
 */
public class PVQueryResult {

    DocList queryResult;

    int resultSize;
    final PVType pvType;

    final int originItemsCount;
    final int originGlobalCount;
    private final Float cutOffValue;
    private SolrPvDocument originalDocument;
    private String sheet;

    public PVQueryResult(DocList queryResult, String pvType, Integer originItemsCount, Integer originGlobalCount, Float cutOffValue) {
        this.queryResult = queryResult;
        this.originGlobalCount = originGlobalCount.intValue();
        // For backward compatibility - if itemsCount was not passed in the query, use the globalCount value.  TODO: remove this when redundant
        this.originItemsCount = (originItemsCount==null)?this.originGlobalCount:originItemsCount.intValue();
        this.pvType = PVType.valueOf(pvType);
        resultSize = queryResult.size();
        this.cutOffValue = cutOffValue;
    }

    public DocList getQueryResult() {
        return queryResult;
    }


    public PVType getPvType() {
        return pvType;
    }

    public int getOriginItemsCount() {
        return originItemsCount;
    }

    public int getOriginGlobalCount() {
        return originGlobalCount;
    }

    public int getResultSize() {
        return resultSize;
    }

    public Float getCutOffValue() {
        return cutOffValue;
    }

    public void setOriginalDocument(SolrPvDocument originalDocument) {
        this.originalDocument = originalDocument;
    }

    public SolrPvDocument getOriginalDocument() {
        return originalDocument;
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public String getSheet() {
        return sheet;
    }
}
