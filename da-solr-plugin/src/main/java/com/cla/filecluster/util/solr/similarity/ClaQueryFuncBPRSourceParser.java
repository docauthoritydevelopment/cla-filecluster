package com.cla.filecluster.util.solr.similarity;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.lucene.queries.function.ValueSource;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.search.FunctionQParser;
import org.apache.solr.search.SyntaxError;
import org.apache.solr.search.ValueSourceParser;

/*
 * Usage:
 * http://localhost:9080/PVS/query?q=PV_field:(token token^boost ...)&fl=score,id&rows=10000&fq=query({!func v=clabpr($q)})&fq={!frange l=0}query($q) 
 * 
 * http://localhost:9080/PVS/query?q=PV_SubBodyNgrams:(11EBB9A6444309B5D^0.987654 45903CF1A181096C B9DBA2FFB813CD3C 43EBF172B568ABF6)&fl=score,id&rows=10000&debug=true&fq=query({!func v=clabpr($q)})&fq={!frange l=0}query($q)
 */

public class ClaQueryFuncBPRSourceParser extends ValueSourceParser {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaQueryFuncBPRSourceParser.class);

	private static final String MIN_SCORE_PARAM = "minScore";
	private static final float MIN_SCORE_DEFAULT = 0.001f;

	private static final String BPR_FACTOR_PARAM = "bprFactor";
	private static final float BPR_FACTOR_DEFAULT = 50f;

	protected float minScore;
	protected float bprFactor;

	public ClaQueryFuncBPRSourceParser() {
		super();
		logger.trace("new ClaQueryFuncBPRSourceParser()");
	}

	@Override
	public void init(@SuppressWarnings("rawtypes") final NamedList namedList) {
		final SolrParams p = SolrParams.toSolrParams(namedList);
		minScore = p.getFloat(MIN_SCORE_PARAM,MIN_SCORE_DEFAULT);
		bprFactor = p.getFloat(BPR_FACTOR_PARAM,BPR_FACTOR_DEFAULT);
		logger.info("init: minScore={} bprFactor={} (nameList={})",minScore,bprFactor,namedList.toString());
	}

	@Override
	public ValueSource parse(final FunctionQParser fp) throws SyntaxError {
		// parameter should be in the form of 'field:(term term ... )'
		final long startTs = System.currentTimeMillis();
		final String param = fp.parseArg();
		String[] split = param.split(":",2);
		final String field = split[0];
		if (split.length!=2 || split[1].charAt(0)!='(') {
			throw new SyntaxError("bad parameter '"+param+"'. Expecting 'field:(term term ... )'.");
		}
		String debDocId = "";
		if (fp.hasMoreArguments()) {
			// Optional docId for debug purposes
			debDocId = fp.parseArg();
		}
		split = split[1].substring(1).split("[ \\+\\)]");
		final List<String> tokens = Stream.of(split).map(t->t.trim()).collect(Collectors.toList());
		if (fp.hasMoreArguments()) {
			// Optional custom bprFactor
			final Float f = fp.parseFloat();
			if (f==null) {
				throw new SyntaxError("bad parameter. Float value expected");
			}
			logger.trace("parse: forcing bprFactor from {} to {}", bprFactor, f);
			bprFactor = f;
		}

		logger.trace("parse:{field={} tokens={}", field, tokens);
		return new ClaQueryFuncBPRValueSource(field, tokens, minScore, bprFactor, debDocId, startTs);
	}

}