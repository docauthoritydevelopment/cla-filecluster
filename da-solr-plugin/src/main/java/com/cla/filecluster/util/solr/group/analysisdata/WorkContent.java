package com.cla.filecluster.util.solr.group.analysisdata;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;

import java.util.List;

public class WorkContent {

    public final List<PvAnalysisData> content;
    public final int dataSizeInKb;

    public WorkContent(List<PvAnalysisData> content, int dataSizeInKb) {
        this.content = content;
        this.dataSizeInKb = dataSizeInKb;
    }
}
