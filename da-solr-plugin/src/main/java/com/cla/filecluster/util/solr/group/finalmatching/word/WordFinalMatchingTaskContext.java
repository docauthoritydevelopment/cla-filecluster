package com.cla.filecluster.util.solr.group.finalmatching.word;

import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.GrpHelperFetcher;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTaskContext;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.cla.filecluster.util.solr.group.verify.UnmatchedContentCache;

import java.util.Collection;
import java.util.List;


public class WordFinalMatchingTaskContext extends FinalMatchingTaskContext<SimilarityMapKey> {

    public WordFinalMatchingTaskContext(SimilarityCalculator similarityCalculator, Collection<SimilarityMapKey> subList,
                                        List<PvAnalysisData> pvAnalysisDataList, SolrPvDocument sourceDocument) {
        super(similarityCalculator, subList, pvAnalysisDataList, sourceDocument);
    }
}
