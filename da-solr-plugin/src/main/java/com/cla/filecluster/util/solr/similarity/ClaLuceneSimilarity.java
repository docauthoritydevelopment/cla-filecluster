package com.cla.filecluster.util.solr.similarity;


/**
 * <p>
 * A similarity which ignores idf()
 * To be used as the similarity class for the Solr PVS core.
 * </p>
 */
public class ClaLuceneSimilarity extends ClaClassicSimilarity {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaLuceneSimilarity.class);

	public ClaLuceneSimilarity() {
		super();
		//    setDiscountOverlaps(false);
	}

	@Override
	public float idf(final long docFreq, final long numDocs) {
		//    return (float)(Math.log(numDocs/(double)(docFreq+1)) + 1.0);
		if (logger.isTraceEnabled())
			logger.trace("force idf({},{}) to 1",docFreq,numDocs);
		return 1f;
	}

	@Override
	public float queryNorm(final float sumOfSquaredWeights) {
		final float res = super.queryNorm(sumOfSquaredWeights);
		final long encNorm = encodeNormValue(res);
		final float decRes = decodeNormValue(encNorm);
		if (logger.isTraceEnabled())
			logger.trace("queryNorm({})={}={}={} err-factor-square={}",sumOfSquaredWeights,res, encNorm, decRes, decRes*decRes/res/res);
		return decRes;
	}


	@Override
	public float coord(final int overlap, final int maxOverlap) {
		final float res = super.coord(overlap, maxOverlap);
		if (logger.isTraceEnabled())
			logger.trace("coord({},{})={}",overlap,maxOverlap,res);
		return res;
	}

	@Override
	public float lengthNorm(int length) {
		final float res = super.lengthNorm(length);
		if (logger.isTraceEnabled())
			logger.trace("lengthNorm={}",res);
		return res;
	}



	@Override
	public String toString() {
		return "ClaLuceneSimilarity:"+ super.toString();
	}

}
