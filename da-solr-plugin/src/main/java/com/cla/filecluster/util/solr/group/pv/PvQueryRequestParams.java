package com.cla.filecluster.util.solr.group.pv;

import org.apache.lucene.search.Query;

import java.util.List;
import java.util.Map;

/**
 * Created by uri on 2/14/2017.
 */
public class PvQueryRequestParams {

    private SolrPvDocument originalDocument;
    private String queryString;
    private List<Query> filters;
    private boolean forceMaturityLevelSimilarity;
    private Integer originItemsCount;
    private Integer originMaturityLevel;
    private String pvType;
    private Integer originGlobalCount;

    public PvQueryRequestParams(SolrPvDocument originalDocument, String queryString, List<Query> fullFilters
            , String pvType, boolean forceMaturityLevelSimilarity) {
        this.originalDocument = originalDocument;
        this.queryString = queryString;
        filters = fullFilters;
        this.pvType = pvType;
        this.forceMaturityLevelSimilarity = forceMaturityLevelSimilarity;
    }

    public PvQueryRequestParams(SolrPvDocument originalDocument, String queryString
            , String pvType) {
        this.originalDocument = originalDocument;
        this.queryString = queryString;
        this.pvType = pvType;
    }

    public SolrPvDocument getOriginalDocument() {
        return originalDocument;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public List<Query> getFilters() {
        return filters;
    }

    public void setOriginItemsCount(Integer originItemsCount) {
        this.originItemsCount = originItemsCount;
    }

    public Integer getOriginItemsCount() {
        return originItemsCount;
    }

    public boolean isForceMaturityLevelSimilarity() {
        return forceMaturityLevelSimilarity;
    }

    public void setOriginMaturityLevel(Integer originMaturityLevel) {
        this.originMaturityLevel = originMaturityLevel;
    }

    public Integer getOriginMaturityLevel() {
        return originMaturityLevel;
    }

    public String getPvType() {
        return pvType;
    }

    public void setPvType(String pvType) {
        this.pvType = pvType;
    }

    public void setOriginGlobalCount(Integer originGlobalCount) {
        this.originGlobalCount = originGlobalCount;
    }

    public Integer getOriginGlobalCount() {
        return originGlobalCount;
    }

    public void setFilters(List<Query> filters) {
        this.filters = filters;
    }

    public void setForceMaturityLevelSimilarity(boolean forceMaturityLevelSimilarity) {
        this.forceMaturityLevelSimilarity = forceMaturityLevelSimilarity;
    }
}
