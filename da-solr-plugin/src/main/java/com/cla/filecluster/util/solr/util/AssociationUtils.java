package com.cla.filecluster.util.solr.util;

import com.cla.filecluster.util.ActiveAssociationUtils;
import org.apache.lucene.document.Document;
import org.apache.solr.update.AddUpdateCommand;

import java.util.*;

public class AssociationUtils {

    public static void setActiveAssociationValues(AddUpdateCommand addUpdateCommand, Document document,
                                                  String[] preChange, String[] postChange, String field) {
        if (field.equals("department") || field.equals("scopedTags")) {
            Set<String> activeAssociations = new HashSet<>(Arrays.asList(
                    document.getValues("activeAssociations")));

            Set<String> pre = new HashSet<>(Arrays.asList(preChange));
            Set<String> post = new HashSet<>(Arrays.asList(postChange));

            Set<String> commonOnes = new HashSet<>(pre);
            commonOnes.retainAll(post);

            pre.removeAll(commonOnes);
            post.removeAll(commonOnes);

            setActiveAssociationValues(pre, activeAssociations, false);
            setActiveAssociationValues(post, activeAssociations, true);

            Map<String, Object> fieldModifier = new HashMap<>();
            fieldModifier.put("set", activeAssociations);
            addUpdateCommand.solrDoc.setField("activeAssociations", fieldModifier);
        }
    }

    public static void addRemoveFromActiveAssociationValues(AddUpdateCommand addUpdateCommand, Document document,
                                                            String field, String value, boolean isAdd) {
        if (field.equals("department") || field.equals("scopedTags")) {
            Set<String> activeAssociations = new HashSet<>(Arrays.asList(
                    document.getValues("activeAssociations")));
            String val = ActiveAssociationUtils.getActiveAssociationIdentifier(value);
            if (!isAdd || !activeAssociations.contains(val)) {
                Map<String, Object> fieldModifier = new HashMap<>();
                fieldModifier.put(isAdd ? "add" : "remove", val);
                addUpdateCommand.solrDoc.setField("activeAssociations", fieldModifier);
            }
        }
    }

    static void setActiveAssociationValues(Collection<String> ids, Set<String> activeAssociations, boolean isAdd) {
        if (ids != null && !ids.isEmpty()) {
            ids.forEach(id -> {
                String val = ActiveAssociationUtils.getActiveAssociationIdentifier(id);
                if (isAdd) {
                    activeAssociations.add(val);
                } else {
                    activeAssociations.remove(val);
                }
            });
        }
    }
}
