package com.cla.filecluster.util.solr.group.finalmatching;

import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.PluginArgsExtractor;
import com.cla.filecluster.util.solr.SearchComponentMetricFilter;
import com.cla.filecluster.util.solr.group.SimilarityCalculatorBuilder;
import com.cla.filecluster.util.solr.group.fetchers.SolrAnalysisDataFetcher;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingRequestData;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.codahale.metrics.Metric;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.CacheStats;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.ShardParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.handler.component.ShardRequest;
import org.apache.solr.handler.component.ShardResponse;
import org.apache.solr.metrics.SolrMetricManager;
import org.apache.solr.metrics.SolrMetricProducer;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


import static com.cla.filecluster.util.solr.group.analysisdata.AnalysisDataResourceManagerComponent.getManager;
import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchersComponent.getFetchers;

/**
 * Class we run final matching on source document against a list of candidate ids
 */
public abstract class FinalMatchingComponent<R extends FinalMatchingRequestData>
        extends SearchComponent implements SolrCoreAware, SolrMetricProducer {


    private static final Logger logger = LoggerFactory.getLogger(FinalMatchingComponent.class);


    private static final String RESPONSE_BUCKET = "response";
    private static final String MATCH_TIME_TEMPLATE = "%d=%d";
    private static final int MATCH_TIME_SAMPLE_LIMIT = 5;

    protected SimilarityCalculator similarityCalculator;

    private NamedList initArgs = new NamedList();

    private boolean pluginIsForceMaturityLevel = false;
    protected boolean pluginIsForceWorkbookMaturityLevel = false;

    private ObjectMapper finalMatchingRequestDataMapper = new ObjectMapper();

    private List<String> sampleMatchTimeStats = IntStream.range(0, MATCH_TIME_SAMPLE_LIMIT)
            .mapToObj(i -> "")
            .collect(Collectors.toList());

    private int avgMatchStatCurrentIndex = 0;


    public FinalMatchingComponent() {
    }

    @SuppressWarnings("rawtypes")
    @Override
    /*
     * Check the solrConfig for groupingType and thresholds when solr loads up.
     */
    public void init(NamedList args) {
        logger.info("Init params for {}", getClass().getSimpleName());
        super.init(args);
        initArgs.addAll(args);

        NamedList thresholdByPVParams = (NamedList) initArgs.get("maturityParams");
        pluginIsForceMaturityLevel = PluginArgsExtractor.getBooleanArgument(thresholdByPVParams,
                "forceMaturityLevel", false);
        pluginIsForceWorkbookMaturityLevel = PluginArgsExtractor.getBooleanArgument(thresholdByPVParams,
                "forceWorkbookMaturityLevel", false);
        this.similarityCalculator = new SimilarityCalculatorBuilder(initArgs).init(true);
    }


    /**
     * Inform happens after init. And we need the solr Core to init the fetchers
     *
     * @param core
     */
    @Override
    public void inform(SolrCore core) {
    }


    @Override
    public void prepare(ResponseBuilder rb) {
        //Do nothing
    }

    @Override
    public void process(ResponseBuilder rb) throws IOException {
        SolrQueryRequest req = rb.req;
        SolrParams params = req.getParams();
        try {
            long start = System.currentTimeMillis();
            R finalMatchingRequestData = extractFinalMatchingRequestData(params);
            logger.debug("Start: Process {}", finalMatchingRequestData);

            Collection<SimilarityMapKey> matchedCandidates = executeFinalMatching(req, finalMatchingRequestData);
            logger.debug("For {}, {} actual matching candidates", finalMatchingRequestData,
                    matchedCandidates.size());
            populateResponse(rb, matchedCandidates);

            long duration = System.currentTimeMillis() - start;
            logger.debug("Done: Process in {} ms", finalMatchingRequestData, duration);
            writeMatchTimeStat(duration, finalMatchingRequestData.getCandidates().size());
        } catch (Throwable t) {
            logger.error(String.format("Error processing req=%s", req), t);
            throw t;
        }
    }

    private void writeMatchTimeStat(long duration, int size) {
        sampleMatchTimeStats.set(avgMatchStatCurrentIndex, String.format(MATCH_TIME_TEMPLATE, duration, size));
        avgMatchStatCurrentIndex = (avgMatchStatCurrentIndex + 1) % MATCH_TIME_SAMPLE_LIMIT;
    }

    private R extractFinalMatchingRequestData(SolrParams params) throws IOException {
        String finalMatchingRequestDataValue = params.get(FinalMatchingQueryParams.FINAL_MATCHING_REQUEST_DATA);
        return finalMatchingRequestDataMapper
                .readValue(finalMatchingRequestDataValue, requestDataClass());
    }

    protected abstract Class<R> requestDataClass();


    private Collection<SimilarityMapKey> executeFinalMatching(SolrQueryRequest req,
                                                              R finalMatchingRequestData) {
        return findSourceDocument(finalMatchingRequestData.getSourceDocumentId())
                .map(sourceDocument -> findFinalMatchingInCandidates(req, finalMatchingRequestData, sourceDocument))
                .orElseGet(() -> {
                    logger.warn("Unable to find analysis data for id {}. Skipping analysis",
                            finalMatchingRequestData.getSourceDocumentId());
                    return new ArrayList<>();
                });
    }

    private Collection<SimilarityMapKey> findFinalMatchingInCandidates(SolrQueryRequest req,
                                                                       R finalMatchingRequestData,
                                                                       SolrPvDocument sourceDocument) {
        FinalMatchingTaskExecutor finalMatchingTaskExecutor = newFinalMatchingTaskExecutor(
                sourceDocument, finalMatchingRequestData);

        return getManager().mapReduce(
                req,
                extractContentIds(finalMatchingRequestData),
                finalMatchingTaskExecutor::execute);

    }

    protected abstract FinalMatchingTaskExecutor newFinalMatchingTaskExecutor(SolrPvDocument sourceDocument, R finalMatchingRequestData);

    protected abstract Collection<Long> extractContentIds(R finalMatchingRequestData);

    private void populateResponse(ResponseBuilder rb, Collection<SimilarityMapKey> matchedWorkBooks) {
        NamedList<Object> contentIds = new NamedList<>();
        for (SimilarityMapKey matchedWorkBook : matchedWorkBooks) {
            contentIds.add("contentId", matchedWorkBook.getDocIdFromKey());
        }
        rb.rsp.add(SimilarityCalculator.CONTENT_IDS, contentIds);
    }

    private Optional<SolrPvDocument> findSourceDocument(Long contentId) {
        try {
            SolrAnalysisDataFetcher solrAnalysisDataFetcher = getFetchers().getSolrAnalysisDataFetcher();
            logger.debug("Original document contentId={}", contentId);
            Map<Long, List<Integer>> res = new HashMap<>(1);
            PvAnalysisData pvAnalysisData = solrAnalysisDataFetcher.getPvAnalysisDataByContentId(contentId);
            if (pvAnalysisData == null) {
                logger.warn("Could not find analysisData for contentId {}", contentId);
                return Optional.empty();
            }
            return Optional.of(new SolrPvDocument(contentId, res, pvAnalysisData,
                    pluginIsForceMaturityLevel, pluginIsForceWorkbookMaturityLevel));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void handleResponses(ResponseBuilder rb, ShardRequest sreq) {
        logger.debug("ClaGroupingSearchComponent - handle responses. purpose={}", sreq.purpose);
        if (sreq.purpose == ShardRequest.PURPOSE_GET_TOP_IDS) {
            logger.trace("ShardRequest.response.size: " + sreq.responses.size());
            SolrDocumentList responseDocs = new SolrDocumentList();
            NamedList responseContentIds = new NamedList();
            long numFound = 0;
            for (ShardResponse srsp : sreq.responses) {
                logger.trace("ShardRequest.response.shard: " + srsp.getShard());
                SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse().getResponse().get("response");
                if (docs != null) {
                    logger.trace("ShardRequest.response.documents: " + docs.size());
                    numFound += docs.size();
                    responseDocs.addAll(docs);
                }
                NamedList contentIds = (NamedList) srsp.getSolrResponse().getResponse().get(SimilarityCalculator.CONTENT_IDS);
                if (contentIds != null) {
                    logger.trace("ShardRequest.response.contentIds: " + contentIds.size());
                    responseContentIds.addAll(contentIds);
                }

            }
//            Map<Object,ShardDoc> resultIds = new HashMap<>();
            responseDocs.setNumFound(numFound);
            logger.trace("Write final response with {} contentIds", responseContentIds.size());
            rb.rsp.add(RESPONSE_BUCKET, responseDocs);
            //depends on groupingType?
            rb.rsp.add(SimilarityCalculator.CONTENT_IDS, responseContentIds);
        } else {
            super.handleResponses(rb, sreq);
        }
    }

    /**
     * Dispatch shard request in <code>STAGE_EXECUTE_QUERY</code> stage
     */
    @Override
    public int distributedProcess(ResponseBuilder rb) {
        SolrParams params = rb.req.getParams();
        logger.info("ClaGropuingSearchComponent distributedProcess. rb.stage={} isShard={}", rb.stage, params.getBool(ShardParams.IS_SHARD));
        if (rb.stage < ResponseBuilder.STAGE_EXECUTE_QUERY)
            return ResponseBuilder.STAGE_EXECUTE_QUERY;
        if (rb.stage == ResponseBuilder.STAGE_EXECUTE_QUERY) {
            logger.trace("FinalMatchingComponent - create shard request");
            ShardRequest sreq = new ShardRequest();
            sreq.purpose = ShardRequest.PURPOSE_GET_TOP_IDS;
            sreq.params = new ModifiableSolrParams(rb.req.getParams());
            sreq.params.remove(ShardParams.SHARDS);
            rb.addRequest(this, sreq);
            return ResponseBuilder.STAGE_GET_FIELDS;
        }
        return ResponseBuilder.STAGE_DONE;
    }


    @Override
    public void initializeMetrics(SolrMetricManager manager, String registry, String tag, String scope) {
        manager.registerGauge(this, registry, () -> sampleMatchTimeStats, tag, true,
                "sampleMatchesTime (" + MATCH_TIME_SAMPLE_LIMIT + ")", getCategory().toString(), scope);

        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats(), "UnMatchCandidatesCache", true, "stats",
                getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats().hitCount(), "UnMatchCandidatesCache", true, "hitCount",
                getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats().hitRate(), "UnMatchCandidatesCache", true, "hitRate",
                getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats().evictionCount(), "UnMatchCandidatesCache", true,
                "evictionCount", getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats().missRate(), "UnMatchCandidatesCache", true, "missRate",
                getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats().missCount(), "UnMatchCandidatesCache", true, "missCount",
                getCategory().toString(), scope);
        manager.registerGauge(this, registry, () -> getUnMatchedContentCacheStats().loadExceptionRate(), "UnMatchCandidatesCache", true,
                "loadExceptionRate", getCategory().toString(), scope);


        this.registry = manager.registry(registry);
        Map<String, Metric> metrics = manager.getMetrics(registry, SearchComponentMetricFilter.of(this));
        if (metrics != null) {
            this.metricNames.addAll(metrics.keySet());
        }
    }

    private CacheStats getUnMatchedContentCacheStats() {
        return getFetchers().getUnmatchedContentCache().stats();
    }

}
