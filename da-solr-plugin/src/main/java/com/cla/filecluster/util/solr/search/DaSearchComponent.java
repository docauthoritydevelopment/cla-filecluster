package com.cla.filecluster.util.solr.search;

import com.cla.filecluster.repository.solr.ClaSearchParams;
import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.google.common.collect.Sets;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.search.Query;
import org.apache.solr.cloud.ZkController;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.cloud.Aliases;
import org.apache.solr.common.cloud.Replica;
import org.apache.solr.common.cloud.Slice;
import org.apache.solr.common.cloud.ZkStateReader;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.MultiMapSolrParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.core.CoreContainer;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.component.QueryComponent;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.request.LocalSolrQueryRequest;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.BasicResultContext;
import org.apache.solr.response.ResultContext;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.schema.IndexSchema;
import org.apache.solr.schema.SchemaField;
import org.apache.solr.search.*;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by uri on 07-Jun-17.
 * <p>
 * NOT IN USAGE - using DaJoinQuery parser instead (need to uncommend CatFiles/solrconfig.xml config section.
 */
public class DaSearchComponent extends QueryComponent implements SolrCoreAware {

    private SolrCore core;

    private Counter totalTime;
    private Counter allRrequests;
    private Counter joinRequests;

    private static Logger logger = LoggerFactory.getLogger(DaSearchComponent.class);
    /**
     * Holds all the cores in this Solr instance
     */
    private CoreContainer coreContainer;
    private int maxPageSize = 50000;

    private final String RESPONSE_BUCKET = "response";

    /**
     * Called once when the system goes up
     *
     * @param core
     */
    @Override
    public void inform(SolrCore core) {
        this.core = core;
        coreContainer = this.core.getCoreContainer();
    }

    @Override
    public void prepare(ResponseBuilder rb) throws IOException {
        super.prepare(rb);
    }

    @Override
    public void process(ResponseBuilder rb) throws IOException {
        long startTime = System.currentTimeMillis();
        SolrQueryRequest req = rb.req;
        SolrParams params = req.getParams();
        String joinCollection = params.get(ClaSearchParams.JOIN_COLLECTION);
        String joinFromField = params.get(ClaSearchParams.JOIN_FROM_FIELD, "id");
        String joinToField = params.get(ClaSearchParams.JOIN_TO_FIELD, "contentId");

        allRrequests.inc();
        if (joinCollection != null) {
            joinRequests.inc();
            logger.debug("Processing joinQuery: params={}, joinCollection={}, joinFromField={}", params, joinCollection, joinFromField);
            SolrIndexSearcher searcher = req.getSearcher();
            List<Long> matchedIds = executeJoinQuery(startTime, params, joinCollection, joinFromField);
            if (matchedIds.size() == 0) {
                createDocResponse(rb, new int[]{});
                doFieldSortValues(rb, searcher); //Required when returning zero results on SolrCloud
                return;
            }

            String joinFilter = createJoinFilter(joinToField, matchedIds);
            addNewFilter(joinFilter, rb);
        } else {
            logger.debug("No joinQuery: params={}, joinCollection={}, joinFromField={}", params, joinCollection, joinFromField);
        }
//        logger.debug("Running super query process({})", rb.req);
//        super.process(rb);
    }

    /**
     * @param startTime
     * @param params
     * @param joinCollection
     * @param joinFromField
     * @return external document ids (not Lucene ids)
     * @throws IOException
     */
    private List<Long> executeJoinQuery(long startTime, SolrParams params, String joinCollection, String joinFromField) throws IOException {
        List<Long> matchedIds = new ArrayList<>();
        logger.info("Join with Component {}", joinCollection);

        List<SolrCore> joinCores = findLocalComponentCores(joinCollection);
        if (joinCores.size() == 0) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Couldn't find any Solr cores with name " + joinCollection);
        }

        String joinQuery = extractJoinQuery(params);
        int flags = SolrIndexSearcher.GET_SCORES;
        String defType = QParserPlugin.DEFAULT_QTYPE;

        for (SolrCore joinCore : joinCores) {
            NamedList args = new NamedList();
            SolrQueryRequest localRequest = new LocalSolrQueryRequest(joinCore, args);
//                SolrQueryRequest localRequest = req;

            SolrDocumentList solrDocuments = executeQueryOnCore(joinFromField, joinQuery, flags, defType, joinCore, localRequest);
            logger.info("Got {} documents from query on core {}", solrDocuments.size(), joinCore.getName());
            addIdsToFilter(matchedIds, solrDocuments, joinFromField);
        }
        long duration = System.currentTimeMillis() - startTime;
        totalTime.inc(duration);
        logger.info("Join collection finished on {} cores and matched {} items in {}ms", joinCores.size(), matchedIds.size(), duration);
        return matchedIds;
    }

    private void createDocResponse(ResponseBuilder rb, int[] convergedDocIds) {
        SolrQueryResponse rsp = rb.rsp;
        float[] docScores = new float[convergedDocIds.length];
        DocListAndSet res = new DocListAndSet();
        res.docList = new DocSlice(0, convergedDocIds.length, convergedDocIds, docScores, convergedDocIds.length, 0);
        rb.setResults(res);
        ResultContext ctx = new BasicResultContext(rb);
        rsp.addResponse(ctx);
//        rsp.add(RESPONSE_BUCKET, ctx);
    }

    private void addNewFilter(String joinFilter, ResponseBuilder rb) {
        SolrQueryRequest req = rb.req;
        MultiMapSolrParams reqParams = (MultiMapSolrParams) req.getParams();
        MultiMapSolrParams.addParam(CommonParams.FQ, joinFilter, reqParams.getMap());
        String[] params = req.getParams().getParams(CommonParams.FQ);
        logger.debug("There are now {} filter params in the request!", params.length);
        try {
            QParser fqp = QParser.getParser(joinFilter, req);
            fqp.setIsFilter(true);
            Query query = fqp.getQuery();
            if (rb.getFilters() == null) {
                rb.setFilters(new ArrayList<>());
            }
            rb.getFilters().add(query);
        } catch (SyntaxError syntaxError) {
            logger.error("Failed to add create Join Filter from {}", joinFilter);
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, syntaxError);
        }
    }

    private SolrDocumentList executeQueryOnCore(String joinFromField, String joinQuery, int flags, String defType, SolrCore joinCore, SolrQueryRequest localRequest) throws IOException {
        SolrIndexSearcher solrIndexSearcher = joinCore.getSearcher().get();
        List<Query> filters = new ArrayList<>();
        DocList docList = executeSubQuery(localRequest, solrIndexSearcher, flags, defType, filters, joinQuery);
        Set<String> fields = Sets.newHashSet(joinFromField);
        Map<SolrDocument, Integer> ids = new HashMap<>();
        return docListToSolrDocumentList(docList, solrIndexSearcher, fields, ids);
    }

    private void addIdsToFilter(List<Long> matchedIds, SolrDocumentList docList, String field) {
        for (SolrDocument entry : docList) {
            Long value = (Long) entry.get(field);
            matchedIds.add(value);
        }
    }

    private String createJoinFilter(String filterField, List<Long> matchedIds) {
        logger.info("Create a filter on {} matched ids ", matchedIds.size());
        StringJoiner stringJoiner = new StringJoiner(" ", filterField + ": (", ")");
        matchedIds.forEach(f -> stringJoiner.add(String.valueOf(f)));
        String filterString = stringJoiner.toString();
        logger.debug("Created filter string: {}", filterString); //TODO 2.0 - change to trace
        return filterString;
    }

    private String extractJoinQuery(SolrParams params) {
        String joinQueryField = params.get(ClaSearchParams.JOIN_QUERY_FIELD);
        if (joinQueryField == null) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Missing join query field");
        }
        String joinQueryValue = params.get(ClaSearchParams.JOIN_QUERY_VALUE);
        if (joinQueryValue == null) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Missing join query value");
        }
        String fieldNamePrefix = joinQueryField + ":";
        String result = joinQueryValue;
        if (joinQueryValue.startsWith(fieldNamePrefix)) {
            logger.debug("Not adding fieldName to join query: {}", joinQueryValue);
        } else {
            result = fieldNamePrefix + joinQueryValue;
        }
        logger.info("Join Query string {}", result);
        return result;
    }

    private DocList executeSubQuery(SolrQueryRequest req, SolrIndexSearcher searcher, int flags, String defType, List<Query> filters, String queryString) throws IOException {
        try {
            QParser parser = QParser.getParser(queryString, defType, req);
            Query query = parser.getQuery();
            //TODO - support multiple result pages with deep paging.
            DocList queryResult = searcher.getDocList(query, filters, null, 0, maxPageSize, flags);
            return queryResult;
        } catch (SyntaxError syntaxError) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, syntaxError);
        } catch (RuntimeException runtimeException) {
            logger.error("DaSearchComponent - Runtime exception while trying to execute query.\n" +
                    "queryString={}\n" +
                    "def={} req={} filters={} flags={} ", queryString, defType, req, filters, flags);
            throw runtimeException;
        }
    }

    private List<SolrCore> findLocalComponentCores(String collectionName) {
        List<SolrCore> result = new ArrayList<>();
        for (SolrCore solrCore : coreContainer.getCores()) {
            if (collectionName.equalsIgnoreCase(solrCore.getCoreDescriptor().getCollectionName())) {
                logger.info("Found local solr core: {}", solrCore.getName());
                result.add(solrCore);
            } else if (collectionName.equalsIgnoreCase(solrCore.getName())) {
                logger.info("Found local solr core: {}", solrCore.getName());
                result.add(solrCore);
            }
        }
        return result;
//        ShardHandler shardHandler = coreContainer.getShardHandlerFactory().getShardHandler();
//        docRouter.getTargetSlice()
//        CoreContainer container = rb.req.getCore().getCoreDescriptor().getCoreContainer();
//        CloudDescriptor cloudDescriptor = coreDescriptor.getCloudDescriptor();
//
//        container.getUpdateShardHandler()

    }


    /**
     * Returns an String with the name of a core.
     * <p>
     * This method searches the core with fromIndex name in the core's container.
     * If fromIndex isn't name of collection or alias it's returns fromIndex without changes.
     * If fromIndex is name of alias but if the alias points to multiple collections it's throw
     * SolrException.ErrorCode.BAD_REQUEST because multiple shards not yet supported.
     *
     * @param collectionName name of the collection
     * @param container      the core container for searching the core with fromIndex name or alias
     * @return the string with name of core
     */
    public static String getCoreName(final String collectionName, CoreContainer container) {
        if (container.isZooKeeperAware()) {
            ZkController zkController = container.getZkController();
            final String resolved =
                    zkController.getClusterState().hasCollection(collectionName)
                            ? collectionName : resolveAlias(collectionName, zkController);
            if (resolved == null) {
                throw new SolrException(SolrException.ErrorCode.BAD_REQUEST,
                        "SolrCloud join: Collection '" + collectionName + "' not found!");
            }
            return findLocalReplicaForFromIndex(zkController, resolved);
        }
        return collectionName;
    }

    private static String resolveAlias(String fromIndex, ZkController zkController) {
        final Aliases aliases = zkController.getZkStateReader().getAliases();
        if (aliases != null) {
            final String resolved;
            Map<String, String> collectionAliases = aliases.getCollectionAliasMap();
            resolved = (collectionAliases != null) ? collectionAliases.get(fromIndex) : null;
            if (resolved != null) {
                if (resolved.split(",").length > 1) {
                    throw new SolrException(SolrException.ErrorCode.BAD_REQUEST,
                            "SolrCloud join: Collection alias '" + fromIndex +
                                    "' maps to multiple collections (" + resolved +
                                    "), which is not currently supported for joins.");
                }
                return resolved;
            }
        }
        return null;
    }

    private static String findLocalReplicaForFromIndex(ZkController zkController, String fromIndex) {
        String fromReplica = null;

        String nodeName = zkController.getNodeName();
        for (Slice slice : zkController.getClusterState().getCollection(fromIndex).getActiveSlices()) {
            if (fromReplica != null)
                throw new SolrException(SolrException.ErrorCode.BAD_REQUEST,
                        "SolrCloud join: multiple shards not yet supported " + fromIndex);

            for (Replica replica : slice.getReplicas()) {
                if (replica.getNodeName().equals(nodeName)) {
                    fromReplica = replica.getStr(ZkStateReader.CORE_NAME_PROP);
                    // found local replica, but is it Active?
                    if (replica.getState() != Replica.State.ACTIVE)
                        throw new SolrException(SolrException.ErrorCode.BAD_REQUEST,
                                "SolrCloud join: " + fromIndex + " has a local replica (" + fromReplica +
                                        ") on " + nodeName + ", but it is " + replica.getState());

                    break;
                }
            }
        }

        if (fromReplica == null)
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST,
                    "SolrCloud join: No active replicas for " + fromIndex +
                            " found in node " + nodeName);

        return fromReplica;
    }


    @Override
    public String getDescription() {
        return "da-query";
    }


    @Override
    public MetricRegistry getMetricRegistry() {
        MetricRegistry metricRegistry = new MetricRegistry();
        allRrequests = metricRegistry.counter("da-all-requests");
        joinRequests = metricRegistry.counter("da-join-requests");
        totalTime = metricRegistry.counter("da-totalTime");
        return metricRegistry;
    }


    /**
     * Convert a DocList to a SolrDocumentList
     * <p>
     * The optional param "ids" is populated with the lucene document id
     * for each SolrDocument.
     *
     * @param docs     The {@link org.apache.solr.search.DocList} to convert
     * @param searcher The {@link org.apache.solr.search.SolrIndexSearcher} to use to load the docs from the Lucene index
     * @param fields   The names of the Fields to load
     * @param ids      A map to store the ids of the docs
     * @return The new {@link org.apache.solr.common.SolrDocumentList} containing all the loaded docs
     * @throws java.io.IOException if there was a problem loading the docs
     * @since solr 1.4
     */
    public static SolrDocumentList docListToSolrDocumentList(
            DocList docs,
            SolrIndexSearcher searcher,
            Set<String> fields,
            Map<SolrDocument, Integer> ids) throws IOException {
    /*  DWS deprecation note:
     It's only called by ClusteringComponent, and I think the "ids" param aspect is a bit messy and not worth supporting.
     If someone wants a similar method they can speak up and we can add a method to SolrDocumentFetcher.
     */
        IndexSchema schema = searcher.getSchema();

        SolrDocumentList list = new SolrDocumentList();
        list.setNumFound(docs.matches());
        list.setMaxScore(docs.maxScore());
        list.setStart(docs.offset());

        DocIterator dit = docs.iterator();

        while (dit.hasNext()) {
            int docid = dit.nextDoc();

            Document luceneDoc = searcher.doc(docid, fields);
            SolrDocument doc = new SolrDocument();

            for (IndexableField field : luceneDoc) {
                if (null == fields || fields.contains(field.name())) {
                    SchemaField sf = schema.getField(field.name());
                    doc.addField(field.name(), sf.getType().toObject(field));
                }
            }
            if (docs.hasScores() && (null == fields || fields.contains("score"))) {
                doc.addField("score", dit.score());
            }

            list.add(doc);

            if (ids != null) {
                ids.put(doc, new Integer(docid));
            }
        }
        return list;
    }

}
