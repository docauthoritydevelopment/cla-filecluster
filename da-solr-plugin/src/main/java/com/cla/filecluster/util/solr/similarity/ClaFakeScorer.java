package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Scorer;

import java.io.IOException;

class ClaFakeScorer extends Scorer {

  float score;
  int doc = -1;
  int freq = 1;

  ClaFakeScorer() {
    super(null);
  }

  @Override
  public int docID() {
    return doc;
  }

  @Override
  public float score() throws IOException {
    return score;
  }

  @Override
  public DocIdSetIterator iterator() {
    throw new UnsupportedOperationException();
  }

  public int freq() throws IOException {
    return freq;
  }
}
