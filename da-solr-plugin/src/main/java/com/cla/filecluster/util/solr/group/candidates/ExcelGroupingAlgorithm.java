package com.cla.filecluster.util.solr.group.candidates;

import com.cla.common.domain.dto.pv.PVType;
import com.cla.filecluster.util.solr.group.pv.PVQueryResult;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by uri on 24/06/2015.
 */
public class ExcelGroupingAlgorithm extends BaseGroupingAlgorithm {


    private float excelSimilarityHighContentThreshold;
    private float excelSimilarityHighStyleThreshold;
    private float excelSimilarityHighLayoutThreshold;
    private float excelSimilarityHeadingHighSimThreshold;
    private int excelMinHeadingCountThreshold;
    private int excelMinStyleCountThreshold;
    private int excelMinLayoutCountThreshold;
    private int excelMinValueThreshold;
    private int excelMinConcreteValueThreshold;
    private int excelMinFormulaThreshold;

    public ExcelGroupingAlgorithm(ArrayList<PVQueryResult> pvQueryResults, SolrPvDocument originalDocument,
    		Properties thresholdsContent, Properties thresholdsStyle, Map<Integer, List<Integer>> debFiles, boolean testDropThreshold) {
        super(pvQueryResults, originalDocument, thresholdsContent, thresholdsStyle, debFiles,testDropThreshold);

        //Define thresholds for match
        
        // The usage of thresholdsContent vs. thresholdsStyle is hard-coded based on the specific threshold. For better configuration, the logic should use the 'contentPvs' property.

        excelMinHeadingCountThreshold = Integer.valueOf(thresholdsStyle.getProperty("excelMinHeadingCountThreshold","4"));
        excelMinStyleCountThreshold = Integer.valueOf(thresholdsStyle.getProperty("excelMinStyleCountThreshold","3"));
        excelMinLayoutCountThreshold = Integer.valueOf(thresholdsStyle.getProperty("excelMinLayoutCountThreshold","3"));

        excelMinValueThreshold = Integer.valueOf(thresholdsContent.getProperty("excelMinValueThreshold","9"));
        excelMinConcreteValueThreshold = Integer.valueOf(thresholdsContent.getProperty("excelMinConcreteValueThreshold","9"));
        excelMinFormulaThreshold = Integer.valueOf(thresholdsContent.getProperty("excelMinFormulaThreshold","9"));
        
        excelSimilarityHighContentThreshold = Float.valueOf(thresholdsContent.getProperty("excelSimilarityHighContentThreshold ","0.5"));
        excelSimilarityHighStyleThreshold = Float.valueOf(thresholdsStyle.getProperty("excelSimilarityHighStyleThreshold","0.8"));
        excelSimilarityHighLayoutThreshold = Float.valueOf(thresholdsStyle.getProperty("excelSimilarityHighLayoutThreshold","0.9"));
        excelSimilarityHeadingHighSimThreshold = Float.valueOf(thresholdsStyle.getProperty("excelSimilarityHeadingHighSimThreshold","0.8"));
    }

    @Override
    protected int simThresholdPassed(Map<PVType, Float> simRates,
    		Map<PVType, Integer> docPvItemsCounts, Map<PVType, Integer> docPvGlobalCounts) {

        return excelSimThresholdPassed(simRates, docPvItemsCounts, docPvGlobalCounts,
                excelSimilarityHighContentThreshold,
                excelSimilarityHighStyleThreshold,
                excelSimilarityHighLayoutThreshold,
                excelSimilarityHeadingHighSimThreshold);
    }


    private int excelSimThresholdPassed(final Map<PVType, Float> simRates, 
    		final Map<PVType, Integer> pvItemsCounts, final Map<PVType, Integer> pvGlobalCounts,
                                        final float contentThreshold,
                                        final float styleThreshold,
                                        final float layoutThreshold,
                                        final float headingHighSimThreshold) {

        final float rowHeadingsSimRate = floatVal(simRates.get(PVType.PV_RowHeadings));
        final float styleSimRate = floatVal(simRates.get(PVType.PV_ExcelStyles));
        final float valuesSimRate = floatVal(simRates.get(PVType.PV_CellValues));
        final float formulasSimRate = floatVal(simRates.get(PVType.PV_Formulas));
        final float concreteValuesSimRate = floatVal(simRates.get(PVType.PV_ConcreteCellValues));
//		final float textNgramsSimRate = floatVal(simRates.get(PVType.PV_TextNgrams));
//		final float titlesNgramsSimRate = floatVal(simRates.get(PVType.PV_TitlesNgrams));
        final float sheetLayoutSimRate = floatVal(simRates.get(PVType.PV_SheetLayoutSignatures));

        final int rowHeadingsSimCount = intVal(pvGlobalCounts.get(PVType.PV_RowHeadings));
        final int styleSimCount = intVal(pvGlobalCounts.get(PVType.PV_ExcelStyles));
//		final int valuesSimCount = intVal(pvCounts.get(PVType.PV_CellValues));
//		final int formulasSimCount = intVal(pvCounts.get(PVType.PV_Formulas));
        final int sheetLayoutSimCount = intVal(pvGlobalCounts.get(PVType.PV_SheetLayoutSignatures));
        final int valuesMinSimCount = intVal(pvGlobalCounts.get(PVType.PV_CellValues));
        final int concreteValuesMinSimCount = intVal(pvGlobalCounts.get(PVType.PV_ConcreteCellValues));
        final int formulasMinSimCount = intVal(pvGlobalCounts.get(PVType.PV_Formulas));

//		final float formulaDominance = (valuesSimCount==0)?0f:((float)formulasSimCount)/((float)valuesSimCount);

        int result = 0;
        int contentSimCode = 0;
        if (valuesSimRate >= contentThreshold && valuesMinSimCount > excelMinValueThreshold) {
            contentSimCode = 1;
        } else if (concreteValuesSimRate >= contentThreshold && concreteValuesMinSimCount > excelMinConcreteValueThreshold) {
            contentSimCode = 2;
        } else if (formulasSimRate >= contentThreshold && formulasMinSimCount > excelMinFormulaThreshold) {
            contentSimCode = 3;
        }

        int visualSimCode = 0;
        if (rowHeadingsSimCount > excelMinHeadingCountThreshold && rowHeadingsSimRate >= contentThreshold) {
            visualSimCode = 1;
        } else if (styleSimCount > excelMinStyleCountThreshold && styleSimRate >= styleThreshold) {
            visualSimCode = 2;
        } else if (sheetLayoutSimCount > excelMinLayoutCountThreshold && sheetLayoutSimRate >= layoutThreshold) {
            visualSimCode = 3;
        }
        // Special handling of strong row-headings + style - treat as content similarity was found
        if (rowHeadingsSimCount > excelMinHeadingCountThreshold && rowHeadingsSimRate >= headingHighSimThreshold) {
            if (styleSimCount == 0 || styleSimRate >= headingHighSimThreshold) {
                visualSimCode = (styleSimCount == 0)?4:5;
                if (contentSimCode==0) {
                    contentSimCode = 4;
                }
            }
        }

        if (contentSimCode != 0 && visualSimCode != 0) {
            result = 10 * contentSimCode + visualSimCode;	// two digits code indicating match origin
        }

        return result;
    }

}
