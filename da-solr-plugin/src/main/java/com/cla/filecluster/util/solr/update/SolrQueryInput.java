package com.cla.filecluster.util.solr.update;

import org.apache.lucene.search.Query;

import java.util.List;

/**
 * Created by: yael
 * Created on: 3/27/2018
 */
public class SolrQueryInput {
    private final Query query;

    private final List<Query> filters;

    private final int flags;

    private final int pageSize;

    public int offset = 0;

    public SolrQueryInput(Query query, List<Query> filters, int pageSize, int flags) {
        this.query = query;
        this.filters = filters;
        this.pageSize = pageSize;
        this.flags = flags;
    }

    public Query getQuery() {
        return query;
    }

    public List<Query> getFilters() {
        return filters;
    }

    public int getFlags() {
        return flags;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public String toString() {
        return "SolrQueryInput{" +
                "query=" + query +
                ", filters=" + filters +
                ", flags=" + flags +
                ", offset=" + offset +
                ", pageSize=" + pageSize +
                '}';
    }
}
