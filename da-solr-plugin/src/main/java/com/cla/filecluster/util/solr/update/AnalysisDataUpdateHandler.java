package com.cla.filecluster.util.solr.update;

import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.MaturityLevel;
import com.cla.filecluster.domain.entity.pv.PvAnalysisDataFormat;
import com.cla.filecluster.repository.solr.ClaGroupingParams;
import com.cla.filecluster.service.files.pv.MaturityHelper;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.cla.filecluster.util.solr.group.fetchers.GenericSolrDataFetcher;
import com.google.common.collect.ImmutableMap;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.core.InitParams;
import org.apache.solr.core.SolrCore;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.DirectUpdateHandler2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/**
 * Created by: yael
 * Created on: 8/21/2018
 */
public class AnalysisDataUpdateHandler extends DirectUpdateHandler2 {

    private static final String PVSolrGlobalCountFieldsSuffix = "_cnt";
    private static final String PVSolrHistogramSizeFieldsSuffix = "_size";

    private SolrCore solrCore;
    private SolrClient pvsSolrClient;
    private MaturityHelper maturityHelper;
    private static Set<String> skippedSavedPvsNames;

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    // config
    private boolean compressAnalysisData = true;
    private static String pvsSolrIdSeparator = "!";
    private static String skipIngestSavePvsByName = "";
    private static String maturityLowThreshodsByPV = "";
    private static String maturityMediumThreshodsByPV = "";
    private static String maturityPvGroups = "";
    private boolean maturityModelSingleMaturityLevel = false;
    private String contentPvNames = "PV_BodyNgrams,PV_SubBodyNgrams,PV_DocstartNgrams,PV_CellValues,PV_Formulas,PV_ConcreteCellValues,PV_TextNgrams";

    public AnalysisDataUpdateHandler(SolrCore core) {
        super(core);
        solrCore = core;
        initExternalFetchers();
        initMaturityHelper();

        InitParams params = core.getSolrConfig().getInitParams().get("config");

        if (params != null) {
            if (params.defaults.get("analysisData.compress") != null) {
                compressAnalysisData = params.defaults.getBooleanArg("analysisData.compress");
            }

            if (params.defaults.get("pvsSolr.idSeparator") != null) {
                pvsSolrIdSeparator = (String) params.defaults.get("pvsSolr.idSeparator");
            }

            if (params.defaults.get("skipIngestSavePvsByPVName") != null) {
                skipIngestSavePvsByName = (String) params.defaults.get("skipIngestSavePvsByPVName");
            }

            if (params.defaults.get("maturity.low.threshodsByPV") != null) {
                maturityLowThreshodsByPV = (String) params.defaults.get("maturity.low.threshodsByPV");
            }

            if (params.defaults.get("maturity.medium.threshodsByPV") != null) {
                maturityMediumThreshodsByPV = (String) params.defaults.get("maturity.medium.threshodsByPV");
            }

            if (params.defaults.get("maturity.pvGroups") != null) {
                maturityPvGroups = (String) params.defaults.get("maturity.pvGroups");
            }

            if (params.defaults.get("maturity.model.contentPvNames") != null) {
                contentPvNames = (String) params.defaults.get("maturity.model.contentPvNames");
            }

            if (params.defaults.get("maturity.model.singleMaturityLevel") != null) {
                maturityModelSingleMaturityLevel = params.defaults.getBooleanArg("maturity.model.singleMaturityLevel");
            }
        }
    }

    private void initExternalFetchers() {
        String solrCoreName = solrCore == null ? "" : solrCore.getName();
        String pvsCoreName = solrCoreName.startsWith("Test") ? "TestPVS" : "PVS";

        //TODO 2.0 - read host+port from the configuration and fallback to "real port"
        String pvsSolrUrl = GenericSolrDataFetcher.SolrURLPrefix + pvsCoreName;
        HttpSolrClient.Builder builder = new HttpSolrClient.Builder();
        pvsSolrClient = builder.withBaseSolrUrl(pvsSolrUrl).build();
    }

    private void initMaturityHelper() {
        Map<String, Integer[]> maturityThresholdsMap = new HashMap<>();
        Map<String, Integer> maturityPvGroupMap = new HashMap<>();

        skippedSavedPvsNames = Stream.of(skipIngestSavePvsByName.split(",")).collect(toSet());

        logger.debug("Init maturity levels service");
        MaturityHelper.fillMaturityLevelThresholds(maturityThresholdsMap, maturityLowThreshodsByPV, MaturityLevel.ML_Low);
        MaturityHelper.fillMaturityLevelThresholds(maturityThresholdsMap, maturityMediumThreshodsByPV, MaturityLevel.ML_Med);
        logger.info("MaturityLevelThresholds: {}",
                ((Stream<String>) maturityThresholdsMap.entrySet().stream().map(e ->
                        (e.getKey().concat(": ").concat(Arrays.toString(e.getValue()))))).collect(Collectors.joining(" ")));

        MaturityHelper.maturityThreshodsToPvGroups(maturityThresholdsMap, maturityPvGroupMap, maturityPvGroups);

        logger.debug("Maturity levels service initialized");

        maturityHelper = new MaturityHelper(contentPvNames, maturityModelSingleMaturityLevel);
        maturityHelper.setMaturityPvGroupMap(maturityPvGroupMap);
        maturityHelper.setMaturityThresholdsMap(maturityThresholdsMap);
    }

    @Override
    public int addDoc(AddUpdateCommand cmd) throws IOException {
        int res = super.addDoc(cmd);

        SolrInputDocument doc = cmd.solrDoc;
        Long contentId = (Long) doc.getFieldValue("id");
        byte[] bytes = (byte[]) doc.getFieldValue("pvCollections");
        int maturity = (int) doc.getFieldValue("maturity");
        FileCollections pvCollections = null;
        if (!doc.containsKey("format")) {
            pvCollections = (FileCollections) PvAnalysisDataUtils.legacyDeserializeFromByteArray(
                    bytes, compressAnalysisData,
                    ImmutableMap.of("com.cla.connector.domain.dto.FileType",
                            "com.cla.connector.domain.dto.file.FileType"));
        } else {
            int format = (int) doc.getFieldValue("format");
            PvAnalysisDataFormat pvAnalysisDataFormat = PvAnalysisDataFormat.values()[format];
            switch (pvAnalysisDataFormat) {
                case PV_COLLECTIONS:
                case PV_ARRAYS:
                    pvCollections = PvAnalysisDataUtils.deserializePvCollections(bytes);
                    break;
            }
            if (pvCollections == null) {
                throw new RuntimeException(String.format("PvAnalysisData format %s is not supported yet", pvAnalysisDataFormat));
            }
        }

        List<SolrInputDocument> docs = new ArrayList<>();
        pvCollections.getCollections()
                .forEach((key, value) ->
                        docs.add(createHistograms(contentId, key, value, maturity)));

        if (docs.size() > 0) {
            try {
                pvsSolrClient.add(docs);
            } catch (Exception e) {
                logger.error("failed saving pvs to solr", e);
                throw new RuntimeException("failed saving pvs to solr", e);
            }
        }

        return res;
    }

    public SolrInputDocument createHistograms(final Long docLid, final String part, final ClaSuperCollection<Long> superCollection, Integer maturityFileLevel) {
        final SolrInputDocument doc = createSolrInputDoc(docLid, part);
        doc.setField(ClaGroupingParams.ML_field, maturityHelper.getPartMaturityLevel(superCollection));
        doc.setField(ClaGroupingParams.ML_FILE_field, maturityFileLevel == null ? 0 : maturityFileLevel);
        superCollection.getCollections().entrySet().stream()
                .filter(e -> !skipPv(e.getKey()))
                .forEach(e -> {
                    doc.setField(e.getKey(), e.getValue().getJoinedKeys());
                    doc.setField(e.getKey() + PVSolrGlobalCountFieldsSuffix, e.getValue().getGlobalCount());
                    doc.setField(e.getKey() + PVSolrHistogramSizeFieldsSuffix, e.getValue().getItemsCount());
                });
        return doc;
    }

    private boolean skipPv(final String pvName) {
        return skippedSavedPvsNames != null && skippedSavedPvsNames.contains(pvName);
    }

    private SolrInputDocument createSolrInputDoc(final Long docLid, final String part) {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", docLid + pvsSolrIdSeparator + part);
        doc.setField("fileId", docLid);
        return doc;
    }
}
