package com.cla.filecluster.util.solr.group.finalmatching.excel;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.GrpHelperFetcher;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingTaskContext;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.cla.filecluster.util.solr.group.verify.UnmatchedContentCache;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class ExcelFinalMatchingTaskContext extends FinalMatchingTaskContext<Map.Entry<Long, Map<String, List<String>>>> {

    private final Map<String, Float> sheetsDominanceRatio;
    private final float skippedSheetsDominance;
    private final boolean groupingForceWorkbookMaturityLevelSimilarity;
    private final int groupedExcelSheetsProcessingFirstThreshold;
    private final int groupedExcelSheetsProcessingLastThreshold;


    public ExcelFinalMatchingTaskContext(SimilarityCalculator similarityCalculator,
                                         Collection<Map.Entry<Long, Map<String, List<String>>>> subList,
                                         List<PvAnalysisData> pvAnalysisDataList,
                                         SolrPvDocument sourceDocument,
                                         Map<String, Float> sheetsDominanceRatio,
                                         float skippedSheetsDominance,
                                         boolean groupingForceWorkbookMaturityLevelSimilarity,
                                         int groupedExcelSheetsProcessingFirstThreshold,
                                         int groupedExcelSheetsProcessingLastThreshold) {
        super(similarityCalculator, subList, pvAnalysisDataList, sourceDocument);
        this.sheetsDominanceRatio = sheetsDominanceRatio;
        this.skippedSheetsDominance = skippedSheetsDominance;
        this.groupingForceWorkbookMaturityLevelSimilarity = groupingForceWorkbookMaturityLevelSimilarity;
        this.groupedExcelSheetsProcessingFirstThreshold = groupedExcelSheetsProcessingFirstThreshold;
        this.groupedExcelSheetsProcessingLastThreshold = groupedExcelSheetsProcessingLastThreshold;
    }
}
