package com.cla.filecluster.util.solr.group.finalmatching.word;

import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.util.solr.group.finalmatching.model.FinalMatchingRequestData;

import java.util.Collection;


public class WordFinalMatchingRequestData extends FinalMatchingRequestData<Collection<SimilarityMapKey>> {

    public WordFinalMatchingRequestData() {
    }

    public WordFinalMatchingRequestData(Long sourceDocumentId, Collection<SimilarityMapKey> candidates) {
        super(sourceDocumentId, candidates);
    }

    @Override
    public FinalMatchingRequestData<Collection<SimilarityMapKey>> extractPage(Collection<SimilarityMapKey> similarityMapKeys) {
        return new WordFinalMatchingRequestData(sourceDocumentId, similarityMapKeys);
    }
}
