package com.cla.filecluster.util.solr;

public interface SolrConstants {


    String ANALYSIS_DATA_CORE_NAME = "AnalysisData";
    String GRP_HELPER_CORE_NAME = "GrpHelper";
    String UNMATCHED_CONTENT_CORE_NAME = "UnmatchedContent";
}
