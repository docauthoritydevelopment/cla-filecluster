package com.cla.filecluster.util.solr.similarity;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.BoostQuery;
import org.apache.lucene.search.DocIdSet;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.util.Bits;
import org.apache.solr.search.Filter;

import java.io.IOException;

public class ClaSimilarityUtils {

    public static float queryNorm(Similarity similarity, float v) {
        if (!(similarity instanceof ClaLuceneSimilarity)) {
            throw new UnsupportedOperationException(String.format("Similarity %s queryNorm is not supported, " +
                    "this must be used only with %s", similarity.getClass().getName(), ClaLuceneSimilarity.class.getName()));
        }
        return ((ClaLuceneSimilarity) similarity).queryNorm(v);
    }

    public static float coord(Similarity similarity, int overlap, int maxOverlap) {
        if (!(similarity instanceof ClaLuceneSimilarity)) {
            throw new UnsupportedOperationException(String.format("Similarity %s queryNorm is not supported, " +
                    "this must be used only with %s", similarity.getClass().getName(), ClaLuceneSimilarity.class.getName()));
        }
        return ((ClaLuceneSimilarity) similarity).coord(overlap, maxOverlap);
    }

    public static ClaQuery toClaQuery(Query query) {
        if (query instanceof org.apache.lucene.search.BooleanQuery) {
            return new ClaBooleanQuery(
                    ((org.apache.lucene.search.BooleanQuery) query).getMinimumNumberShouldMatch(),
                    ((org.apache.lucene.search.BooleanQuery) query).clauses().stream()
                            .map(ClaSimilarityUtils::toClaBooleanClause)
                            .toArray((ClaBooleanClause[]::new)));
        } else if (query instanceof TermQuery) {
            return new ClaTermQuery(((TermQuery) query).getTerm());
        }
        if (query instanceof BoostQuery) {
            return new ClaBoostQuery(toClaQuery(((BoostQuery) query).getQuery()), ((BoostQuery) query).getBoost());
        }
        if (query instanceof Filter) {
            return new ClaFilter(false) {

                @Override
                public String toString(String field) {
                    return query.toString();
                }

                @Override
                public boolean equals(Object obj) {
                    return query.equals(obj);
                }

                @Override
                public int hashCode() {
                    return query.hashCode();
                }

                @Override
                public DocIdSet getDocIdSet(LeafReaderContext context, Bits acceptDocs) throws IOException {
                    return ((Filter) query).getDocIdSet(context, acceptDocs);
                }
            };

        }
        throw new UnsupportedOperationException(String.format("toClaQuery is not allowed for %s", query.getClass().getName()));
    }

    private static ClaBooleanClause toClaBooleanClause(org.apache.lucene.search.BooleanClause booleanClause) {
        return new ClaBooleanClause(toClaQuery(booleanClause.getQuery()), toClaOccur(booleanClause));
    }

    private static ClaBooleanClause.Occur toClaOccur(org.apache.lucene.search.BooleanClause booleanClause) {
        return ClaBooleanClause.Occur.valueOf(booleanClause.getOccur().name());
    }
}
