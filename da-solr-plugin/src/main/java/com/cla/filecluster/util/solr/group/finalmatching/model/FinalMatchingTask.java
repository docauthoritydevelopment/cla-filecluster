package com.cla.filecluster.util.solr.group.finalmatching.model;

import com.cla.common.domain.dto.pv.GroupingResult;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.ContentIdGroupsPool;
import com.cla.filecluster.service.files.pv.PvAnalysisDataPool;
import com.cla.filecluster.util.solr.group.fetchers.SolrFetchers;
import com.cla.filecluster.util.solr.group.fetchers.SolrGrpHelperFetcher;
import com.cla.filecluster.util.solr.group.verify.UnmatchedContentCache;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.cla.filecluster.util.solr.group.fetchers.SolrFetchersComponent.getFetchers;

public abstract class FinalMatchingTask<T, C extends FinalMatchingTaskContext<T>> {

    private static final Logger logger = LoggerFactory.getLogger(FinalMatchingTask.class);
    protected final C finalMatchingTaskContext;


    public FinalMatchingTask(C finalMatchingTaskContext) {
        this.finalMatchingTaskContext = finalMatchingTaskContext;
    }


    public abstract long getCandidateId(T entry);

    public abstract Pair<GroupingResult, Optional<SimilarityMapKey>> analyseMatch(T groupCandidate, PvAnalysisDataPool pvAnalysisDataPool, ContentIdGroupsPool contentPool);

    public Collection<SimilarityMapKey> extractSimilarityMapKeys() {
        SolrFetchers fetchers = getFetchers();
        Collection<T> subList = finalMatchingTaskContext.getSubList();
        List<SimilarityMapKey> result = new ArrayList<>(subList.size());
        List<Long> contentIds = subList.stream()
                .map(this::getCandidateId)
                .collect(Collectors.toList());

        List<PvAnalysisData> pvAnalysisDataList = finalMatchingTaskContext.getPvAnalysisDataList();
        PvAnalysisDataPool pvAnalysisDataPool = new PvAnalysisDataPool(pvAnalysisDataList);
        ContentIdGroupsPool contentPool = new ContentIdGroupsPool(fetchers.getSolrGrpHelperFetcher(), contentIds);

        List<Long> sourceUnmatchedContentIds = Lists.newLinkedList();
        for (T groupCandidate : subList) {
            //Analyze element in sublist
            try {
                long candidateId = getCandidateId(groupCandidate);
                long sourceContentId = finalMatchingTaskContext.getSourceDocument().getId();
                Pair<GroupingResult, Optional<SimilarityMapKey>> pair = analyseMatch(groupCandidate,
                        pvAnalysisDataPool, contentPool);
                GroupingResult groupingResult = pair.getKey();
                pair.getValue().ifPresent(result::add);
                logger.trace("sourceDocumentId:{} matchResult for groupCandidateId:{} is: {}",
                        sourceContentId, candidateId,groupingResult.isMatch());
                if (fetchers.isUseUnmatchedContentCache() && !groupingResult.isMatch()) {
                    // remember if there was no match
                    UnmatchedContentCache unmatchedContentCache = fetchers.getUnmatchedContentCache();
                    unmatchedContentCache.putBiDir(candidateId, sourceContentId);
                    sourceUnmatchedContentIds.add(candidateId);
                }
            } catch (Exception e) {
                logger.error("Failed to verify word candidate {}. Skipping.", groupCandidate, e);
            }
        }

        return result;
    }
}
