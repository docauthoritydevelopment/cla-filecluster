package com.cla.filecluster.util.solr.group.candidates;

import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.pv.SimilarityRates;
import com.cla.filecluster.service.files.pv.SimplifiedPartSimilarityMaps;
import com.cla.filecluster.service.files.pv.SimplifiedSimilarityMap;
import com.cla.filecluster.util.solr.group.pv.PVQueryResult;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;
import com.google.common.collect.Sets;
import org.apache.solr.search.DocIterator;
import org.apache.solr.search.SolrIndexSearcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

/**
 * Created by uri on 24/06/2015.
 */
public abstract class BaseGroupingAlgorithm {
    protected ArrayList<PVQueryResult> pvQueryResults;
    protected final SolrPvDocument originalDocument;
    protected final Properties thresholdsContent;
    protected final Properties thresholdsStyle;
    protected final Map<Integer, List<Integer>> debFiles;

    protected final Map<Integer, Float> avarageDocumentSimilarity = new HashMap<>();
    private final Map<Integer, Integer> documentSimilarityRate = new HashMap<>();
    private boolean testDropThreshold;

    public BaseGroupingAlgorithm(ArrayList<PVQueryResult> pvQueryResults, SolrPvDocument originalDocument,
                                 Properties thresholdsContent, Properties thresholdsStyle, Map<Integer, List<Integer>> debFiles2, boolean testDropThreshold) {
        this.pvQueryResults = pvQueryResults;
        this.originalDocument = originalDocument;
        this.thresholdsContent = thresholdsContent;
        this.thresholdsStyle = thresholdsStyle;
        this.debFiles = debFiles2;
        this.testDropThreshold = testDropThreshold;
    }

    private static Logger logger = LoggerFactory.getLogger(BaseGroupingAlgorithm.class);

    public int[] executeAlgorithm(SolrIndexSearcher searcher) {
        final SimplifiedPartSimilarityMaps similarityMaps = createSimilarityMaps();
        if (logger.isTraceEnabled()) {
            logSimilarityScores(searcher, similarityMaps);
        }
        int[] convergedGroup = analyzeSimilarityMaps(similarityMaps);
        return convergedGroup;
    }

    private void logSimilarityScores(SolrIndexSearcher searcher, SimplifiedPartSimilarityMaps similarityMaps) {
        similarityMaps.entrySet().forEach(e -> {
            SimplifiedSimilarityMap simplifiedSimilarityMap = e.getValue();
            String docsScores = simplifiedSimilarityMap.getSimilarityMap().entrySet().stream()
                    .map(en -> contentSimilarityScore(searcher, en))
                    .collect(Collectors.joining(","));

            logger.trace("DocumentId={}, fileType={}, pvType={},sheet={}, docScores=({})",
                    simplifiedSimilarityMap.getDocumentId(),
                    simplifiedSimilarityMap.getFileType(),
                    simplifiedSimilarityMap.getPvType(),
                    simplifiedSimilarityMap.getSheet(),
                    docsScores);
        });
    }

    private String contentSimilarityScore(SolrIndexSearcher searcher, Map.Entry<Integer, SimilarityRates> entry) {
        try {
            Integer internalId = entry.getKey();
            return searcher.doc(internalId, Sets.newHashSet("fileId")).get("fileId") + "=" + entry.getValue().getSimilarity();
        } catch (IOException e) {
            throw new RuntimeException("Failed to get doc for " + entry);
        }
    }

    private SimplifiedPartSimilarityMaps createSimilarityMaps() {
        final SimplifiedPartSimilarityMaps similarityMaps = new SimplifiedPartSimilarityMaps();
        for (PVQueryResult pvQueryResult : pvQueryResults) {
            SimplifiedSimilarityMap simplifiedSimilarityMap = createSimilarityMap(pvQueryResult);
            similarityMaps.put(pvQueryResult.getPvType(), simplifiedSimilarityMap, pvQueryResult.getOriginItemsCount(), pvQueryResult.getOriginGlobalCount());
        }
        return similarityMaps;
    }

    public int[] analyzeSimilarityMaps(final SimplifiedPartSimilarityMaps similarityMaps) {
        final Set<Integer> allKeys = similarityMaps.values().stream().flatMap(m -> m.getSimilarityMap().keySet().stream()).collect(toSet());
        int[] convergedDocuments = allKeys.stream().filter(f -> checkKeySimilarity(f, similarityMaps)).mapToInt(i -> i).toArray();

        if (logger.isDebugEnabled()) {
            Long originalDocId = originalDocument.getId();
            logger.debug("analyzeSimilarityMaps for doc {} - Got {} candidate docs. {} passed.", originalDocId, allKeys.size(), convergedDocuments.length);
        }

        if (debFiles != null && logger.isDebugEnabled()) {
            for (Integer f : debFiles.keySet()) {
                List<Integer> internalIds = debFiles.get(f);
                if (internalIds != null) {
                    for (Integer internalId : internalIds) {
                        if (internalId != null && allKeys.contains(internalId)) {
                            logger.debug("SimDeb for file {} ({}): {}", f, internalId,
                                    similarityMaps.entrySet().stream().map(e -> (
                                            e.getKey().toString().substring(0, 4) + ":" +
                                                    (((int) (100f * e.getValue().getSimilarityRate(internalId))) / 100f)))
                                            .collect(Collectors.joining(",")));
                        }
                    }
                }
            }
            //			logger.debug("allKeys={}", allKeys.toString());
            //			logger.debug("converged={}", allKeys.stream().filter(f -> checkKeySimilarity(f, similarityMaps)).map(k->k.toString()).collect(Collectors.joining(",")));
        }
        return convergedDocuments;
    }

    /**
     * This is the equivalent of checkWordSimilarity
     *
     * @param documentId
     * @param similarityMaps
     */
    private boolean checkKeySimilarity(Integer documentId, SimplifiedPartSimilarityMaps similarityMaps) {
        final Map<PVType, Float> simRates = new HashMap<>();
        final Map<PVType, Float> inclusionRates = new HashMap<>();
        similarityMaps.entrySet().stream()
                .forEach(e -> {
                    simRates.put(e.getKey(), e.getValue().getSimilarityRate(documentId));
                    inclusionRates.put(e.getKey(), e.getValue().getInclusionRate(documentId));
                });
//        if (originalDocument != null && originalDocument.getPvMap() != null
//                && originalDocument.getPvMap().values().stream().findFirst().orElse(Collections.emptyList()).contains(documentId)) {
//            logger.debug("Check identity similarity {}", simRates.toString());
//        }

        // Seems to be redundant legacy code - disable for now 
//        calculateAvarageDocumentSimilarity(simRates,documentId);
        final Map<PVType, Integer> docPvItemsCounts = similarityMaps.getPvItemsCounts();
        final Map<PVType, Integer> docPvGlobalCounts = similarityMaps.getPvGlobalCounts();

        final int simCalc = simThresholdPassed(simRates, docPvItemsCounts, docPvGlobalCounts);
        if (simCalc != 0 || testDropThreshold) {
            //Potential similarity
            documentSimilarityRate.put(documentId, simCalc);
            return true;
        }
        return false;
    }

    private void calculateAvarageDocumentSimilarity(Map<PVType, Float> simRates, Integer documentId) {
        Double avarage = simRates.values().stream().mapToDouble(f -> f).average().getAsDouble();
        avarageDocumentSimilarity.put(documentId, avarage.floatValue());
    }

    protected abstract int simThresholdPassed(Map<PVType, Float> simRates,
                                              Map<PVType, Integer> docPvItemsCounts, Map<PVType, Integer> docPvGlobalCounts);

    //    protected abstract simThresholdPassed
    private SimplifiedSimilarityMap createSimilarityMap(PVQueryResult pvQueryResult) {
        SolrPvDocument originalDocument = pvQueryResult.getOriginalDocument();
        final SimplifiedSimilarityMap analyzedPvSimilarities = new SimplifiedSimilarityMap(
                originalDocument.getId(),
                originalDocument.getFileType(),
                pvQueryResult.getPvType(),
                pvQueryResult.getSheet());
        DocIterator iterator = pvQueryResult.getQueryResult().iterator();
        int lowScoreCount = 0;
        float cutOffValue = pvQueryResult.getCutOffValue();
        while (iterator.hasNext()) {
            int docId = iterator.nextDoc();
            float simRate = iterator.score();
            if (cutOffValue < 0f || simRate > cutOffValue) {

//            if (originDocId == null || !(originDocId.values().stream().findFirst().orElse(Collections.emptyList()).contains(docId))) {
                analyzedPvSimilarities.setRates(docId, new SimilarityRates(simRate, 0f));
//            }
            } else {
                lowScoreCount++;
            }
        }
        if (lowScoreCount > 0) {
            logger.debug("PVQueryResult({}) droppted {} results (out of {}) of score lower than {}",
                    pvQueryResult.getPvType(), lowScoreCount, pvQueryResult.getResultSize(), cutOffValue);
        }
        return analyzedPvSimilarities;
    }

    protected float floatVal(final Float f) {
        return (f == null) ? 0 : f.floatValue();
    }

    protected int intVal(final Integer i) {
        return (i == null) ? 0 : i.intValue();
    }

    protected Integer NotNull(final Integer val) {
        return (val == null) ? 0 : val;
    }

    public Map<Integer, Integer> getDocumentSimilarityRate() {
        return documentSimilarityRate;
    }

    public Map<Integer, Float> getAvarageDocumentSimilarity() {
        return avarageDocumentSimilarity;
    }
}
