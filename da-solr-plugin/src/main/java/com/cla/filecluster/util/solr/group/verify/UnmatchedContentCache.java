package com.cla.filecluster.util.solr.group.verify;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UnmatchedContentCache {

    private static final Logger logger = LoggerFactory.getLogger(UnmatchedContentCache.class);

    private LoadingCache<Long, Set<Long>> internalCache;
    private SolrUnmatchedContentRepository solrUnmatchedContentRepository;
    private ExecutorService executorServiceUnmatchedCache;
    private boolean active;


    public UnmatchedContentCache(SolrUnmatchedContentRepository solrUnmatchedContentRepository, int cacheSize,
                                 int cacheConcurrency) {
        this.internalCache = CacheBuilder.newBuilder()
                .maximumSize(cacheSize)
                .build(new CacheLoader<Long, Set<Long>>() {
                    @Override
                    public Set<Long> load(Long key) {
                        return solrUnmatchedContentRepository.getByContentId(key);
                    }
                });

        this.solrUnmatchedContentRepository = solrUnmatchedContentRepository;
        this.executorServiceUnmatchedCache = Executors.newFixedThreadPool(cacheConcurrency);
    }

    private synchronized void put(Long key, Long value) {
        logger.trace("Adding unmatched entry (first: {}, second: {})", key, value);
        Set<Long> set = internalCache.getUnchecked(key);
        set.add(value);
        logger.trace("Unmatched entry (first: {}, second: {}) added, total unmatched for {}: {} candidates",
                key, value, key, set.size());
    }

    public synchronized void putBiDir(Long id1, Long id2) {
        put(id1, id2);
        put(id2, id1);
        executorServiceUnmatchedCache.submit(() -> solrUnmatchedContentRepository.addNewPair(id2, id1));
    }

    private Set<Long> get(Long key) {
        return internalCache.getUnchecked(key);
    }

    public synchronized Set<Long> safeGet(Long key) {
        Set<Long> set = get(key);
        logger.trace("Got for candidate {}, {} unmatched", key, set == null ? 0 : set.size());
        return set == null ? Sets.newHashSet() : set;
    }

    public synchronized boolean arePaired(Long id1, Long id2) {
        Set<Long> set = get(id1);
        return (set != null) && set.contains(id2);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public CacheStats stats() {
        return internalCache.stats();
    }
}
