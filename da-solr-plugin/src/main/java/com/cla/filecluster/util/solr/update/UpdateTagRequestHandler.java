package com.cla.filecluster.util.solr.update;

import com.cla.common.constants.SolrRequestParams;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.ShardParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.params.UpdateParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.apache.solr.core.SolrCore;
import org.apache.solr.handler.RequestHandlerBase;
import org.apache.solr.handler.component.DebugComponent;
import org.apache.solr.metrics.SolrMetricManager;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.search.*;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.CommitUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorChain;
import org.apache.solr.util.plugin.SolrCoreAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by uri on 01/09/2015.
 * Handler not in use anymore
 */
@Deprecated
public class UpdateTagRequestHandler extends RequestHandlerBase implements SolrCoreAware {

    public int maxPageSize = 50000;

    String fieldToUpdate = "tags";

    private static Logger logger = LoggerFactory.getLogger(UpdateTagRequestHandler.class);
    private String lastQuery = "";
    private volatile long lastQueryTime = 0;

    private SolrCore core;

    @Override
    public void init(NamedList args) {
        logger.info("Init UpdateTagRequestHandler");

        super.init(args);
    }


    /**
     * Initialize the components based on name.  Note, if using <code>INIT_FIRST_COMPONENTS</code> or <code>INIT_LAST_COMPONENTS</code>,
     * then the {@link DebugComponent} will always occur last.  If this is not desired, then one must explicitly declare all components using
     * the <code>INIT_COMPONENTS</code> syntax.
     */
    @Override
    @SuppressWarnings("unchecked")
    public void inform(SolrCore core) {
        this.core = core;
//        shardHandlerFactory = core.getCoreDescriptor().getCoreContainer().getShardHandlerFactory();

    }


    @Override
    public void handleRequestBody(SolrQueryRequest req, SolrQueryResponse rsp) throws Exception {
        long startTime = System.currentTimeMillis();

        SolrParams params = req.getParams();
        SolrIndexSearcher searcher = req.getSearcher();
        handleLocalUpdateTagRequest(req, rsp, startTime, params, searcher);
    }

    private void handleLocalUpdateTagRequest(SolrQueryRequest req, SolrQueryResponse rsp, long startTime, SolrParams params, SolrIndexSearcher searcher) throws IOException {
        UpdateRequestProcessor updateRequestProcessor = getUpdateRequestProcessor(req, rsp, params);

        String countString = params.get(SolrRequestParams.COUNT);
        int count = 1;
        if (countString != null) {
            try {
                int c = Integer.parseUnsignedInt(countString);
                count = c;
            } catch (NumberFormatException e) {
                throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Update Tag Request: illegal value for CNT: " + countString);
            }
        }
        if (count > 10 || count < 1) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Update Tag Request count should be between 1 and 10");
        }
        if (count == 1) {
            // Original implementation - single field update
            String tag = params.get(SolrRequestParams.TAG);

            String tagOperation = params.get(SolrRequestParams.TAG_OP, "add");

            String field = params.get(SolrRequestParams.FIELD, fieldToUpdate);

            logger.debug("Handle Update Tag Request. IsShard={} tag={} tag_op={} field={}", params.getBool(ShardParams.IS_SHARD), tag, tagOperation, field);
            lastQuery = req.getParamString();

            if (tag == null) {
                throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, "Update Tag Request requires a tag. No tag given");
            }
//        if (StringUtils.isEmpty(field)) {
//            field = defaultFieldToUpdate;
//        }
            DocList queryResult = executeQuery(req, params, searcher);
            Map<String, Object> fieldModifier = new HashMap<>(1);
            fieldModifier.put(tagOperation, tag);

            updateDocuments(field, req, searcher, updateRequestProcessor, queryResult, fieldModifier);
            lastQueryTime = System.currentTimeMillis() - startTime;
            logger.debug("Finished Update Tag Request on {} documents in {} ms.", queryResult.size(), lastQueryTime);
        } else {
            // Handle mutiple fields operation
            /*
             ** Assumes a 'tags update' Query with the parameters: tag.op cnt field1 tag1 field2 tag2 ...
             */
            String tagOperation = params.get(SolrRequestParams.TAG_OP, "add");
            String[] fields = new String[count];
            List<Map<String, Object>> fieldModifiers = new ArrayList<Map<String, Object>>(count);
            new HashMap<>(1);
            int i;
            for (i = 1; i <= count; ++i) {
                String field = params.get(SolrRequestParams.FIELD + i);
                fields[i - 1] = field;
                String tag = params.get(SolrRequestParams.TAG + i);
                if (tag == null) {
                    throw new SolrException(SolrException.ErrorCode.BAD_REQUEST,
                            "Update Tag Request requires a tag. No value given for " + (SolrRequestParams.TAG + i));
                }
                Map<String, Object> fieldModifier = new HashMap<>(1);
                fieldModifier.put(tagOperation, tag);
                fieldModifiers.add(fieldModifier);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Handle Update multi-fields request. IsShard={} values={} tag_op={} field={}", params.getBool(ShardParams.IS_SHARD),
                        fieldModifiers.stream().map(fm -> (String) fm.get(tagOperation)).collect(Collectors.joining(",", "[", "]")),
                        tagOperation,
                        Arrays.asList(fields).toString());
            }
            lastQuery = req.getParamString();
            DocList queryResult = executeQuery(req, params, searcher);

            updateDocumentsMultiField(fields, req, searcher, updateRequestProcessor, queryResult, fieldModifiers);
            lastQueryTime = System.currentTimeMillis() - startTime;
            logger.debug("Finished Update multi-fields ({}) request on {} documents in {} ms.", count, queryResult.size(), lastQueryTime);
        }
    }


    private void updateDocuments(String field, SolrQueryRequest req, SolrIndexSearcher searcher, UpdateRequestProcessor updateRequestProcessor, DocList queryResult, Map<String, Object> fieldModifier) throws IOException {
        logger.trace("Update tag on {} documents", queryResult.size());
        StringJoiner ids = new StringJoiner(",");
        if (queryResult.size() == 0) {
            return;
        }
        try {
            AddUpdateCommand addUpdateCommand = new AddUpdateCommand(req);
            DocIterator iterator = queryResult.iterator();
            while (iterator.hasNext()) {
                Document document = searcher.doc(iterator.nextDoc());
                SolrInputDocument solrInputDocument = new SolrInputDocument();
                addUpdateCommand.clear();
                addUpdateCommand.solrDoc = solrInputDocument;
                String id = document.get("id");
                ids.add(id);
                addUpdateCommand.solrDoc.setField("id", id);
                addUpdateCommand.solrDoc.setField(field, fieldModifier);
                updateRequestProcessor.processAdd(addUpdateCommand);
//                logger.trace("Update doc={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
                if (logger.isTraceEnabled()) {
                    logger.trace("Update doc={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
                }
            }
        } finally {
            logger.debug("Commit and finish update request on ids: {}", ids);
            updateRequestProcessor.processCommit(new CommitUpdateCommand(req, false));
            updateRequestProcessor.finish();
        }

    }

    private void updateDocumentsMultiField(String[] fields, SolrQueryRequest req, SolrIndexSearcher searcher, UpdateRequestProcessor updateRequestProcessor, DocList queryResult, List<Map<String, Object>> fieldModifiers) throws IOException {
        logger.trace("Update tag on {} documents", queryResult.size());
        StringJoiner ids = new StringJoiner(",");
        if (queryResult.size() == 0) {
            return;
        }
        try {
            AddUpdateCommand addUpdateCommand = new AddUpdateCommand(req);
            DocIterator iterator = queryResult.iterator();
            while (iterator.hasNext()) {
                Document document = searcher.doc(iterator.nextDoc());
                SolrInputDocument solrInputDocument = new SolrInputDocument();
                addUpdateCommand.clear();
                addUpdateCommand.solrDoc = solrInputDocument;
                String id = document.get("id");
                ids.add(id);
                addUpdateCommand.solrDoc.setField("id", id);
                for (int i = 0; i < fields.length; ++i) {
                    if (fields[i] != null) {
                        addUpdateCommand.solrDoc.setField(fields[i], fieldModifiers.get(i));
                    }
                }
                updateRequestProcessor.processAdd(addUpdateCommand);
//                logger.trace("Update doc={} field={} req={} mod={}", id, field, req, fieldModifier.toString());
                if (logger.isTraceEnabled()) {
                    logger.trace("Update doc={} fields={} req={} modifiers={}", id, Arrays.asList(fields).toString(), req, fieldModifiers.toString());
                }
            }
        } finally {
            logger.debug("Commit and finish update request on ids: {}", ids);
            updateRequestProcessor.processCommit(new CommitUpdateCommand(req, false));
            updateRequestProcessor.finish();
        }

    }

    private DocList executeQuery(SolrQueryRequest req, SolrParams params, SolrIndexSearcher searcher) throws IOException {
        String defType = params.get(QueryParsing.DEFTYPE, QParserPlugin.DEFAULT_QTYPE);
        String q = params.get(CommonParams.Q);
        Query query = null;
        List<Query> filters = null;

        int flags = SolrIndexSearcher.GET_DOCLIST;

        try {
            if (q != null) {
                QParser parser = QParser.getParser(q, defType, req);
                query = parser.getQuery();
            }

            String[] fqs = req.getParams().getParams(CommonParams.FQ);
            if (fqs != null && fqs.length != 0) {
                filters = new ArrayList<>();
                for (String fq : fqs) {
                    if (fq != null && fq.trim().length() != 0) {
                        QParser fqp = QParser.getParser(fq, null, req);
                        filters.add(fqp.getQuery());
                    }
                }
            }
        } catch (SyntaxError e) {
            throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, e);
        }


        return searcher.getDocList(query, filters, null, 0, maxPageSize, flags);
    }

    private UpdateRequestProcessor getUpdateRequestProcessor(SolrQueryRequest req, SolrQueryResponse rsp, SolrParams params) {
        UpdateRequestProcessorChain processorChain =
                req.getCore().getUpdateProcessingChain(params.get(UpdateParams.UPDATE_CHAIN));

        return processorChain.createProcessor(req, rsp);
    }


    @Override
    public String getDescription() {
        return "Update the tag for a set of documents based on the given query";
    }


    @Override
    public void initializeMetrics(SolrMetricManager manager, String registryName, String tag, String scope) {
        super.initializeMetrics(manager, registryName, tag, scope);
        manager.registerGauge(this, registryName, () -> lastQueryTime, tag, true, "lastQueryTime", getCategory().toString(), scope);
        manager.registerGauge(this, registryName, () -> lastQuery, tag, true, "lastQuery", getCategory().toString(), scope);
    }
}
