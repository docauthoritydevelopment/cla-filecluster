package com.cla.filecluster.util.solr.group.finalmatching.word;

import com.cla.filecluster.util.solr.group.finalmatching.FinalMatchingComponent;
import com.cla.filecluster.util.solr.group.finalmatching.FinalMatchingTaskExecutor;
import com.cla.filecluster.util.solr.group.pv.SolrPvDocument;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 *
 */
public class WordFinalMatchingComponent extends FinalMatchingComponent<WordFinalMatchingRequestData> {


    @Override
    protected Class<WordFinalMatchingRequestData> requestDataClass() {
        return WordFinalMatchingRequestData.class;
    }

    @Override
    protected FinalMatchingTaskExecutor newFinalMatchingTaskExecutor(SolrPvDocument sourceDocument,
                                                                     WordFinalMatchingRequestData finalMatchingRequestData) {
        return new WordFinalMatchingTaskExecutor(sourceDocument, finalMatchingRequestData,similarityCalculator);
    }

    @Override
    protected Collection<Long> extractContentIds(WordFinalMatchingRequestData finalMatchingRequestData) {
        return finalMatchingRequestData.getCandidates().stream()
                .map(similarityMapKey -> similarityMapKey.getDocIdFromKey())
                .collect(Collectors.toList());
    }

    @Override
    public String getDescription() {
        return "DocAuthority Word Final Component. Responsible for verifying matches on given set of word candidates";
    }

    @Override
    public String getName() {
        return "wordFinalMatchingComponent";
    }
}
