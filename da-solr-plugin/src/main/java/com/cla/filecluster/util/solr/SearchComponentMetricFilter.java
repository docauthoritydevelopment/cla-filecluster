package com.cla.filecluster.util.solr;

import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricFilter;
import org.apache.solr.handler.component.SearchComponent;

/**
 * Created By Itai Marko
 */
public class SearchComponentMetricFilter implements MetricFilter {


    private final String componentName;

    public static SearchComponentMetricFilter of(SearchComponent searchComponent) {
        return new SearchComponentMetricFilter(searchComponent.getName());
    }

    private SearchComponentMetricFilter(String searchComponentName) {
        this.componentName = searchComponentName;
    }

    @Override
    public boolean matches(String name, Metric metric) {
        return name.contains(componentName);
    }
}
