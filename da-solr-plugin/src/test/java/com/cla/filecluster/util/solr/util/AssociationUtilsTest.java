package com.cla.filecluster.util.solr.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.ContentStream;
import org.apache.solr.core.SolrCore;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.schema.IndexSchema;
import org.apache.solr.search.SolrIndexSearcher;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.util.RTimerTree;
import org.junit.Test;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class AssociationUtilsTest {

    @Test
    public void setActiveAssociationValuesAddValue() {
        AddUpdateCommand command = createCommand();
        Document document = new Document();
        document.add(new StringField("activeAssociations", "dt.g.120.337", Field.Store.YES));
        String[] preChange = new String[] {"dt.g.120.337..fileGroup"};
        String[] postChange = new String[] {"dt.g.120.337..fileGroup", "g.22.105.MANUAL.f0"};
        AssociationUtils.setActiveAssociationValues(command, document, preChange, postChange, "scopedTags");
        Map res = (Map)command.solrDoc.getFieldValue("activeAssociations");
        assertEquals(1, res.size());
        assertTrue(res.containsKey("set"));
        Set<String> vals = (Set)res.get("set");
        assertEquals(2, vals.size());
        assertTrue(vals.contains("dt.g.120.337"));
        assertTrue(vals.contains("g.22.105"));
    }

    @Test
    public void setActiveAssociationValuesRemoveValue() {
        AddUpdateCommand command = createCommand();
        Document document = new Document();
        document.add(new StringField("activeAssociations", "dt.g.120.337", Field.Store.YES));
        String[] preChange = new String[] {"dt.g.120.337..fileGroup", "g.22.105.MANUAL.f0"};
        String[] postChange = new String[] {"dt.g.120.337..fileGroup"};
        AssociationUtils.setActiveAssociationValues(command, document, preChange, postChange, "scopedTags");
        Map res = (Map)command.solrDoc.getFieldValue("activeAssociations");
        assertEquals(1, res.size());
        assertTrue(res.containsKey("set"));
        Set<String> vals = (Set)res.get("set");
        assertEquals(1, vals.size());
        assertTrue(vals.contains("dt.g.120.337"));
    }

    @Test
    public void addFromActiveAssociationValuesExists() {
        AddUpdateCommand command = createCommand();
        Document document = new Document();
        document.add(new StringField("activeAssociations", "dt.g.120.337", Field.Store.YES));
        AssociationUtils.addRemoveFromActiveAssociationValues(command, document,"scopedTags", "dt.g.120.337..fileGroup", true);
        assertTrue(command.solrDoc.getFieldValue("activeAssociations") == null);
    }

    @Test
    public void addFromActiveAssociationValuesNotExist() {
        AddUpdateCommand command = createCommand();
        Document document = new Document();
        document.add(new StringField("activeAssociations", "g.22.105", Field.Store.YES));
        AssociationUtils.addRemoveFromActiveAssociationValues(command, document,"scopedTags", "dt.g.120.337..fileGroup", true);
        Map res = (Map)command.solrDoc.getFieldValue("activeAssociations");
        assertEquals(1, res.size());
        assertTrue(res.containsKey("add"));
        assertTrue(res.containsValue("dt.g.120.337"));
    }

    @Test
    public void removeFromActiveAssociationValues() {
        AddUpdateCommand command = createCommand();
        Document document = new Document();
        document.add(new StringField("activeAssociations", "dt.g.120.337", Field.Store.YES));
        AssociationUtils.addRemoveFromActiveAssociationValues(command, document,"scopedTags", "dt.g.120.337..fileGroup", false);
        Map res = (Map)command.solrDoc.getFieldValue("activeAssociations");
        assertEquals(1, res.size());
        assertTrue(res.containsKey("remove"));
        assertTrue(res.containsValue("dt.g.120.337"));
    }

    private AddUpdateCommand createCommand() {
        AddUpdateCommand command = new AddUpdateCommand(new ReqTest());
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        command.solrDoc = solrInputDocument;
        return command;
    }

    @Test
    public void setActiveAssociationValuesInnerAdd() {
        List<String> ids = Lists.newArrayList("g.22.105.MANUAL.f0",
                "dt.g.120.337..fileGroup",
                "g.30.173.MANUAL.vdt.fileGroup");
        Set<String> activeAssociations = Sets.newHashSet();
        AssociationUtils.setActiveAssociationValues(ids, activeAssociations, true);
        assertEquals(3, activeAssociations.size());
        assertTrue(activeAssociations.contains("dt.g.120.337"));
        assertTrue(activeAssociations.contains("g.30.173"));
        assertTrue(activeAssociations.contains("g.22.105"));
    }

    @Test
    public void setActiveAssociationValuesInnerAddWithOptions() {
        List<String> ids = Lists.newArrayList("g.2.17.MANUAL.f1;g.2.15.MANUAL.f0",
                "dt.g.120.337..fileGroup;dt.g.1.5..f1;dt.g.1.2..f0",
                "g.30.173.MANUAL.vdt.fileGroup");
        Set<String> activeAssociations = Sets.newHashSet();
        AssociationUtils.setActiveAssociationValues(ids, activeAssociations, true);
        assertEquals(3, activeAssociations.size());
        assertTrue(activeAssociations.contains("dt.g.120.337"));
        assertTrue(activeAssociations.contains("g.30.173"));
        assertTrue(activeAssociations.contains("g.2.17"));
    }

    @Test
    public void setActiveAssociationValuesInnerRemoveVdt() {
        List<String> ids = Lists.newArrayList(
                "g.30.173.MANUAL.vdt.fileGroup");
        Set<String> activeAssociations = Sets.newHashSet("dt.g.120.337", "g.30.173", "g.22.105");
        AssociationUtils.setActiveAssociationValues(ids, activeAssociations, false);
        assertEquals(2, activeAssociations.size());
        assertTrue(activeAssociations.contains("dt.g.120.337"));
        assertTrue(activeAssociations.contains("g.22.105"));
    }

    @Test
    public void setActiveAssociationValuesInnerRemoveDt() {
        List<String> ids = Lists.newArrayList(
                "dt.g.120.337..fileGroup");
        Set<String> activeAssociations = Sets.newHashSet("dt.g.120.337", "g.30.173", "g.22.105");
        AssociationUtils.setActiveAssociationValues(ids, activeAssociations, false);
        assertEquals(2, activeAssociations.size());
        assertTrue(activeAssociations.contains("g.30.173"));
        assertTrue(activeAssociations.contains("g.22.105"));
    }

    class ReqTest implements SolrQueryRequest {
        @Override
        public SolrParams getParams() {
            return null;
        }

        @Override
        public void setParams(SolrParams params) {

        }

        @Override
        public Iterable<ContentStream> getContentStreams() {
            return null;
        }

        @Override
        public SolrParams getOriginalParams() {
            return null;
        }

        @Override
        public Map<Object, Object> getContext() {
            return null;
        }

        @Override
        public void close() {

        }

        @Override
        public long getStartTime() {
            return 0;
        }

        @Override
        public RTimerTree getRequestTimer() {
            return null;
        }

        @Override
        public SolrIndexSearcher getSearcher() {
            return null;
        }

        @Override
        public SolrCore getCore() {
            return null;
        }

        @Override
        public IndexSchema getSchema() {
            return null;
        }

        @Override
        public void updateSchemaToLatest() {

        }

        @Override
        public String getParamString() {
            return null;
        }

        @Override
        public Map<String, Object> getJSON() {
            return null;
        }

        @Override
        public void setJSON(Map<String, Object> json) {

        }

        @Override
        public Principal getUserPrincipal() {
            return null;
        }
    }
}