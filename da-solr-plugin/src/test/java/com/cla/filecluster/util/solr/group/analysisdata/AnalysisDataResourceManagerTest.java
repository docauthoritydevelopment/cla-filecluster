package com.cla.filecluster.util.solr.group.analysisdata;


import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.AnalysisMetadata;
import com.cla.common.utils.FileSizeUnits;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.util.solr.group.fetchers.SolrAnalysisDataFetcher;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class AnalysisDataResourceManagerTest {

    /*@Test
    public void testResourceManagerSimulation() throws InterruptedException {
        AnalysisDataResourceManagementConfig config = AnalysisDataResourceManagementConfig.builder()
                .analysisDataSizeFactor(10)
                .batchSize(1000)
                .fetchIntervalExp("1;SECONDS")
                .loadConcurrency(5)
                .useAdaptiveSize(false)
                .maxDocumentSizeInKB(1000)
                .timeoutInMinutes(5)
                .memoryLimitInKB(4_000_000)
                .maxHeapMemoryThresholdPercentage(70)
                .build();

        DummyAnalysisDataFetcher analysisDataFetcher = new DummyAnalysisDataFetcher(100);
        AnalysisDataResourceManager analysisDataResourceManager = new AnalysisDataResourceManager(analysisDataFetcher, config);

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        IntStream.range(0, 10).forEach(index -> {
            List<Long> contentIds = LongStream.range(0, 1000)
                    .boxed().collect(Collectors.toList());
            try {
                Thread.sleep(200);
            } catch (InterruptedException ignored) {
            }
            executorService.submit(() -> analysisDataResourceManager.fetchDataAndExecute(contentIds,
                    pvAnalysisDataList -> {
                        System.out.println(String.format("Thread %s: Started Working", Thread.currentThread().getName()));
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ignored) {

                        }
                        System.out.println(String.format("Thread %s: Done Working", Thread.currentThread().getName()));
                    }));
        });
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> DONE!!!!");
    }


    private static class DummyAnalysisDataFetcher extends SolrAnalysisDataFetcher {


        private int dataSizeMax;

        DummyAnalysisDataFetcher(int dataSizeMax) {
            super("", false, 1000, Maps.newHashMap());
            this.dataSizeMax = dataSizeMax;
        }

        @Override
        public PvAnalysisData getPvAnalysisDataByContentId(Long contentId) {
            return new PvAnalysisData(contentId, new ClaSuperCollection<>(), new AnalysisMetadata());
        }

        @Override
        public Pair<PvAnalysisData, Integer> getPvAnalysisDataAndSizeByContentId(Long contentId) {
            return Pair.of(
                    new PvAnalysisData(contentId, new ClaSuperCollection<>(), new AnalysisMetadata()),
                    Long.valueOf(FileSizeUnits.KB.toBytes((int) (Math.random() * (dataSizeMax + 1))))
                            .intValue());
        }

        @Override
        public List<PvAnalysisData> getPvAnalysisDataByContentIds(Collection<Long> contentIdList) {
            return contentIdList.stream().map(contentId -> new PvAnalysisData(contentId, new ClaSuperCollection<>(), new AnalysisMetadata()))
                    .collect(Collectors.toList());
        }

        @Override
        public List<Pair<PvAnalysisData, Integer>> getPvAnalysisDataByContentIdsAndSize(Collection<Long> contentIdList) {
            try {
                Thread.sleep((long) Math.floor((Math.random() * 3000) + 100));
            } catch (InterruptedException ignored) {
            }

            Long contentIdToFilter = Lists.newArrayList(contentIdList).get((int) (Math.floor(Math.random() * contentIdList.size())));
            return contentIdList.stream()
                    .filter(contentId -> !contentId.equals(contentIdToFilter))
                    .map(contentId ->
                            Pair.of(new PvAnalysisData(contentId, new ClaSuperCollection<>(), new AnalysisMetadata()),
                                    Long.valueOf(FileSizeUnits.KB.toBytes(Math.floor(Math.random() * (dataSizeMax + 1))))
                                            .intValue()))
                    .collect(Collectors.toList());
        }

        @Override
        public void updateStatistics(long time, int size) {

        }
    }*/

}
