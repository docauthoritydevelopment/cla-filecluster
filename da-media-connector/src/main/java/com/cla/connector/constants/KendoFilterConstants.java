package com.cla.connector.constants;

/**
 * Kendo filter constants
 * Created by vladi on 3/1/2017.
 */
public interface KendoFilterConstants {
    String EQ = "eq";
    String NEQ = "neq";
    String STARTSWITH = "startswith";
    String ENDSWITH = "endswith";
    String CONTAINS = "contains";
    String DOESNOTCONTAIN = "doesnotcontain";
    String GT = "gt";
    String GTE = "gte";
    String LT = "lt";
    String LTE = "lte";
    String NONE = "none";
}
