package com.cla.connector.media.scan;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;

import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by oren on 2/4/2018.
 */
public abstract class RecursiveActionBase<V> extends RecursiveAction  {


    protected TimeSource timeSource = new TimeSource();

    protected Predicate<? super String> fileTypesPredicate;
    protected Predicate<Pair<Long, Long>> scanActivePredicate;
    protected ScanTaskParameters scanParams;
    protected Consumer<ClaFilePropertiesDto> filePropsConsumer;
    protected Consumer<DirListingPayload> dirListingConsumer;
    protected Consumer<String> createScanTaskConsumer;
    protected ProgressTracker filePropsProgressTracker;
    protected V path;
	protected String mediaEntityId;
    protected boolean subFolderTask;

    public TimeSource getTimeSource() {
        return timeSource;
    }

    public void setTimeSource(TimeSource timeSource) {
        this.timeSource = timeSource;
    }

    public Predicate<? super String> getFileTypesPredicate() {
        return fileTypesPredicate;
    }

    public void setFileTypesPredicate(Predicate<? super String> fileTypesPredicate) {
        this.fileTypesPredicate = fileTypesPredicate;
    }

    public Predicate<Pair<Long, Long>> getScanActivePredicate() {
        return scanActivePredicate;
    }

    public void setScanActivePredicate(Predicate<Pair<Long, Long>> scanActivePredicate) {
        this.scanActivePredicate = scanActivePredicate;
    }

    public ScanTaskParameters getScanParams() {
        return scanParams;
    }

    public void setScanParams(ScanTaskParameters scanParams) {
        this.scanParams = scanParams;
    }

    public Consumer<ClaFilePropertiesDto> getFilePropsConsumer() {
        return filePropsConsumer;
    }

    public void setFilePropsConsumer(Consumer<ClaFilePropertiesDto> filePropsConsumer) {
        this.filePropsConsumer = filePropsConsumer;
    }

    public Consumer<DirListingPayload> getDirListingConsumer() {
        return dirListingConsumer;
    }

    public void setDirListingConsumer(Consumer<DirListingPayload> dirListingConsumer) {
        this.dirListingConsumer = dirListingConsumer;
    }

    public Consumer<String> getCreateScanTaskConsumer() {
        return createScanTaskConsumer;
    }

    public void setCreateScanTaskConsumer(Consumer<String> createScanTaskConsumer) {
        this.createScanTaskConsumer = createScanTaskConsumer;
    }

    public ProgressTracker getFilePropsProgressTracker() {
        return filePropsProgressTracker;
    }

    public void setFilePropsProgressTracker(ProgressTracker filePropsProgressTracker) {
        this.filePropsProgressTracker = filePropsProgressTracker;
    }

    public V getPath() {
        return path;
    }

    public void setPath(V path) {
        this.path = path;
    }

	public String getMediaEntityId() {
		return mediaEntityId;
	}

	public RecursiveActionBase<V> setMediaEntityId(String mediaEntityId) {
		this.mediaEntityId = mediaEntityId;
		return this;
	}
}
