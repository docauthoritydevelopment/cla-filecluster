package com.cla.connector.media.scan;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;

import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Created by oren on 2/4/2018.
 */
public abstract class RecursiveActionBuilderBase<T extends RecursiveActionBase<V>, V> {
    private Predicate<? super String> fileTypesPredicate;
    private Predicate<Pair<Long, Long>> scanActivePredicate;
    private ScanTaskParameters scanParams;
    private Consumer<ClaFilePropertiesDto> filePropsConsumer;
    private Consumer<DirListingPayload> dirListingConsumer;
    private Consumer<String> createScanTaskConsumer;
    private ProgressTracker filePropsProgressTracker;
    private V path;
	private String mediaEntityId;
    private boolean subFolderTask;

    public RecursiveActionBuilderBase withScanParams(ScanTaskParameters scanParams){
        this.scanParams = scanParams;
        return this;
    }

    public RecursiveActionBuilderBase withFilePropsConsumer(Consumer<ClaFilePropertiesDto> filePropsConsumer){
        this.filePropsConsumer = filePropsConsumer;
        return this;
    }

    public RecursiveActionBuilderBase withDirListingConsumer(Consumer<DirListingPayload> dirListingConsumer){
        this.dirListingConsumer = dirListingConsumer;
        return this;
    }

    public RecursiveActionBuilderBase withCreateScanTaskConsumer(Consumer<String> createScanTaskConsumer) {
        this.createScanTaskConsumer = createScanTaskConsumer;
        return this;
    }

    public RecursiveActionBuilderBase withFileTypesPredicate(Predicate<? super String> fileTypesPredicate){
        this.fileTypesPredicate = fileTypesPredicate;
        return this;
    }

    public RecursiveActionBuilderBase withJobStatePredicate(Predicate<Pair<Long, Long>> scanActivePredicate){
        this.scanActivePredicate = scanActivePredicate;
        return this;
    }

    public RecursiveActionBuilderBase withProgressTracker(ProgressTracker filePropsProgressTracker) {
        this.filePropsProgressTracker = filePropsProgressTracker;
        return this;
    }

    public RecursiveActionBuilderBase withPath(V path) {
        this.path = path;
        return this;
    }

    public RecursiveActionBuilderBase withMediaEntityId(String mediaEntityId) {
        this.mediaEntityId = mediaEntityId;
        return this;
    }

    public RecursiveActionBuilderBase asSubFolderTask(){
	    this.subFolderTask = true;
	    return this;
    }

    public abstract T build();

    protected T build(T action){
        action.fileTypesPredicate = this.fileTypesPredicate;
        action.scanActivePredicate = this.scanActivePredicate;
        action.scanParams = this.scanParams;
        action.filePropsConsumer = this.filePropsConsumer;
        action.dirListingConsumer = this.dirListingConsumer;
        action.createScanTaskConsumer = this.createScanTaskConsumer;
        action.filePropsProgressTracker = this.filePropsProgressTracker;
        action.path = path;
        action.mediaEntityId = mediaEntityId;
        action.subFolderTask = subFolderTask;
        return action;
    }
}