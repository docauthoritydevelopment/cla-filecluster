package com.cla.connector.utils.pckg;

import com.google.common.base.Strings;

public final class ZipPackagePathResolver implements PackagePathResolver {

    public static final String ZIP_IDENTIFIER = ".zip";

    private static final String ZIP_ENTRY_IDENTIFIER = ZIP_IDENTIFIER + PackagePathResolver.PACKAGE_SEPARATOR;

	@Override
	public String getPackageIdentifier() {
		return ZIP_IDENTIFIER;
	}

	@Override
    public boolean isPackageEntry(String filename) {
        return !Strings.isNullOrEmpty(filename) && filename.toLowerCase().contains(ZIP_ENTRY_IDENTIFIER);
    }

    @Override
    public String getPackageEntryPath(String filename) {
        return Strings.isNullOrEmpty(filename) ? null :
                filename.substring(filename.lastIndexOf(PackagePathResolver.PACKAGE_SEPARATOR) + PackagePathResolver.PACKAGE_SEPARATOR.length() + 1)
                        .replaceAll("\\\\", "/");
    }

    @Override
    public String getPackageRootPath(String filename) {
        if (isPackage(filename)) {
            return filename;
        } else if (isPackageEntry(filename)) {
            return filename.substring(0, filename.lastIndexOf(PackagePathResolver.PACKAGE_SEPARATOR));
        }
        return null;
    }

    @Override
    public boolean isPackage(String filename) {
        return !Strings.isNullOrEmpty(filename) && filename.toLowerCase().endsWith(getPackageIdentifier());
    }
}
