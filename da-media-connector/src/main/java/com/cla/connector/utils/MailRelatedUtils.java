package com.cla.connector.utils;

import org.apache.commons.io.output.ByteArrayOutputStream;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by: yael
 * Created on: 5/22/2019
 */
public class MailRelatedUtils {

    public static byte[] getMimeMessageBytes(MimeMessage mimeMessage) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            mimeMessage.writeTo(out);
        } catch (IOException | MessagingException e) {
            throw new RuntimeException("Error occurred while getting the content of MimeMessage", e);
        }
        return out.toByteArray();
    }

    public static MimeMessage getMimeMessage() {
        Properties p = System.getProperties();
        Session session = Session.getInstance(p);
        MimeMessage mimeMessage = new MimeMessage(session);
        return mimeMessage;
    }
}
