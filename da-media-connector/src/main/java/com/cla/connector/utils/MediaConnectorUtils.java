package com.cla.connector.utils;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;

/**
 * A collection of Utilities that help with scanning files and folders.
 * Created by uri on 09-May-17.
 */
public class MediaConnectorUtils {

    public static boolean isFolderExcluded(Path path, String baseName, List<FolderExcludeRuleDto> excludedRules, List<Path> pathSkipList, List<String> dirnameList2Skip, Long runId) {
        String pathStr = path.toString().toLowerCase();
        boolean isExcluded = isFolderExcluded(pathStr, baseName, excludedRules, dirnameList2Skip, runId);
        if (isExcluded || (pathSkipList != null && pathSkipList.contains(path))) {
            // skip path if list is not null and path is in list
            createScanError("Skipped. Path included in ignore list", null, pathStr, runId);
            return true;
        }

        return false;
    }

    public static boolean isFolderExcluded(String pathStr, String baseName, List<FolderExcludeRuleDto> excludedRules, List<String> dirnameList2Skip, Long runId) {
        if (excludedRules != null && excludedRules.size() > 0 && isExcludedByRule(excludedRules, runId, pathStr)) {
            return true;
        }
        final String dirFileNamePath = (dirnameList2Skip != null) ? baseName : null;
        if (dirFileNamePath != null && dirnameList2Skip.contains(dirFileNamePath)) {
            // skip path if list is not null and path is in list
            createScanError("Skipped. Dirname included in ignore list", null, pathStr, runId);
            return true;
        }
        return false;
    }

    private static boolean isExcludedByRule(List<FolderExcludeRuleDto> excludedRules, Long runId, String pathStr) {
        for (FolderExcludeRuleDto folderExcludeRule : excludedRules) {
            if (folderExcludeRule.isDisabled()) {
                continue;
            }
            String matchStringLC = folderExcludeRule.getMatchString().toLowerCase();
            switch (folderExcludeRule.getOperator().toLowerCase()) {
                case KendoFilterConstants.EQ:
                    if (matchStringLC.equalsIgnoreCase(pathStr)) {
                        createScanError("Skipped. Path matched folderExcludeRule (Equals "
                                + matchStringLC + ")", null, pathStr, runId);
                        return true;
                    }
                    break;
                case KendoFilterConstants.CONTAINS:
                    if (matchStringLC.contains(pathStr)) {
                        createScanError("Skipped. Path matched folderExcludeRule (Contains "
                                + matchStringLC + ")", null, pathStr, runId);
                        return true;
                    }
                    break;
                case KendoFilterConstants.ENDSWITH:
                    if (matchStringLC.endsWith(pathStr)) {
                        createScanError("Skipped. Path matched folderExcludeRule (Ends with "
                                + matchStringLC + ")", null, pathStr, runId);
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    public static ScanErrorDto createScanError(Exception ex, String path, Long runId) {
        final String SUFFIX = " while listing items in folder ";

        if (ex instanceof AccessDeniedException) {
            return createScanError("Access denied" + SUFFIX, ex, path, runId);
        } else if (ex instanceof FileSystemException) {
            return createScanError("File system error" + SUFFIX, ex, path, runId);
        } else if (ex instanceof FileNotFoundException) {
            return createScanError("File not found" + SUFFIX, ex, path, runId);
        } else if (ex instanceof IOException) {
            return createScanError("IO Error" + SUFFIX, ex, path, runId);
        } else {
            return createScanError("System error" + SUFFIX, ex, path, runId);
        }
    }

    public static ScanErrorDto createScanError(String text, Exception ex, String path, Long runId) {
        String exceptionText = Converters.getExceptionText(ex);
        ScanErrorDto scanErrorDto = ScanErrorDto.create(ScanErrorDto.ErrorSeverity.CRITICAL);
        scanErrorDto.setRunId(runId)
                .setPath(path)
                .setExceptionText(exceptionText)
                .setText(text);
        return scanErrorDto;
    }

    public static Predicate<? super String> createFileTypePredicate(ScanTaskParameters scanParams) {
        return FileTypeUtils.createFileTypesPredicate(
                        scanParams.getScanTypeSpecification());
    }
}
