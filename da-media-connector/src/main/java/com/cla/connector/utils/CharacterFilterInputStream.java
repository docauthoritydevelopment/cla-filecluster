package com.cla.connector.utils;

import com.google.common.collect.Lists;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Filter characters from underlying reader.
 * Not thread safe.
 *
 * @author oren.
 */
public class CharacterFilterInputStream extends FilterInputStream {

    // Representations:
    // Dec - &#DD; (D - decimal digit)
    // Hex - &#xXXXX; (X - hexadecimal digit)

    private static final Pattern UTF8_CODE_REGEX = Pattern.compile("&#x?([0-9A-F]+);");

    private final List<Byte[]> charsToFilter;

    private byte[] previouslyObtained;

    private int previouslyObtainedBytes = 0;

    private boolean reachedEOS;

    public CharacterFilterInputStream(InputStream in, List<String> charsToFilter) {
        this(in, charsToFilter, 10);
    }

    public CharacterFilterInputStream(InputStream in, List<String> charsToFilter, int lookAhead) {
        super(in);
        previouslyObtained = new byte[lookAhead];
        this.charsToFilter = charsToFilter.stream()
                .peek(chr -> {
                    if(chr.length() > lookAhead-1) {
                        throw new RuntimeException("Given character length (" + chr + ": " + chr.length() + ") is bigger than look ahead buffer size: " + lookAhead);
                    }
                })
                .map(this::fillUTFCodesIfNecessary)
                .flatMap(List::stream)
                .map(chr -> chr.getBytes(StandardCharsets.UTF_8))
                .map(this::wrapByteArr)
                .collect(Collectors.toList());
    }

    private List<String> fillUTFCodesIfNecessary(String utfChar) {
        final List<String> utfCodes = Lists.newArrayList(utfChar);
        Matcher matcher = UTF8_CODE_REGEX.matcher(utfChar);
        if (!matcher.find()) {
            return utfCodes;
        }

        String code = matcher.group(1);
        String utfChar2;
        if (utfChar.startsWith("&#x")) {
            utfChar2 = Integer.toString(Integer.parseInt(code, 16));
            utfChar2 = "&#" + utfChar2 + ";";
        } else {
            utfChar2 = Integer.toHexString(Integer.valueOf(code));
            utfChar2 = "&#x" + utfChar2 + ";";
        }
        utfCodes.add(utfChar2);

        return utfCodes;
    }

    private Byte[] wrapByteArr(byte[] bytes) {
        final int arrLength = bytes.length;
        final Byte[] output = new Byte[arrLength];
        for (int idx=0; idx<arrLength; idx++) {
            output[idx] = bytes[idx];
        }

        return output;
    }

    private int dumpPreviouslyObtained(byte[] target, int off, int len) {
        int lengthToCopy = len > previouslyObtainedBytes ? previouslyObtainedBytes : len;
        System.arraycopy(previouslyObtained, 0, target, off, lengthToCopy);
        if (lengthToCopy != previouslyObtainedBytes) {
            previouslyObtainedBytes -= lengthToCopy;
            System.arraycopy(previouslyObtained, 0, previouslyObtained, lengthToCopy, lengthToCopy - len);
        } else {
            previouslyObtainedBytes = 0;
        }

        return lengthToCopy;
    }

    private int fillBuffer(byte[] buf, int off, int len) throws IOException {
        int bytesFilled = 0;
        if (previouslyObtainedBytes > 0) {
            bytesFilled = dumpPreviouslyObtained(buf, off, len);
        }

        if (bytesFilled < len && !reachedEOS) { // buffer has room for more elements.
            int amount = in.read(buf, off+bytesFilled, len-bytesFilled);
            if (amount == -1) {
                reachedEOS = true;
            } else if (amount > -1) {
                bytesFilled += amount;
            }
        }

        return bytesFilled == 0 && reachedEOS ? -1 : bytesFilled;
    }

    private boolean isFilteredChar(Byte[] charToFilter, byte[] buf, int currentBufPos, int uptoIdx) {// length of remaining bytes to search in buffer (uptoIdx-currentPos) should be ge to charToFilter.length
        int pos;
        for (pos=0;
             currentBufPos+pos < uptoIdx && pos<charToFilter.length && charToFilter[pos].equals(buf[currentBufPos + pos]);
             pos++);

        if (currentBufPos + pos == uptoIdx && pos != charToFilter.length) {
            byte[] lookAhead = getLookAheadBuffer();
            for (int lookAheadBufPosition=0;
                 pos<charToFilter.length && lookAheadBufPosition < previouslyObtainedBytes && charToFilter[pos].equals(lookAhead[lookAheadBufPosition]);
                 pos++, lookAheadBufPosition++);
        }

        return pos == charToFilter.length;
    }

    private byte[] getLookAheadBuffer() {
        if (reachedEOS) {
            return new byte[0];
        }
        if (previouslyObtainedBytes == 0) {
            try {
                previouslyObtainedBytes = in.read(previouslyObtained);
                if (previouslyObtainedBytes == -1) {
                    reachedEOS = true;
                }
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }

        return previouslyObtained;
    }

    private int getFilteredCharLengthIfDetected(byte[] buf, int currentPos, int uptoIdx) throws IOException {
        Optional<Byte[]> detectedOpt = charsToFilter.stream()
                .filter(arr -> isFilteredChar(arr, buf, currentPos, uptoIdx))
                .findFirst();

        return detectedOpt.map(bytes -> bytes.length).orElse(0);
    }

    private int removeChar(byte[] buf, int currentBufIndex, int endBufIndex, int lengthToRemove) {
        int copyLength = endBufIndex - currentBufIndex - lengthToRemove;
        int removedBytes = lengthToRemove;

        if (copyLength > 0) {
            System.arraycopy(buf, currentBufIndex + lengthToRemove, buf, currentBufIndex, copyLength);
        } else {
            if (previouslyObtainedBytes > 0) {
                System.arraycopy(previouslyObtained, -copyLength, previouslyObtained, 0, previouslyObtainedBytes+copyLength);
                previouslyObtainedBytes += copyLength;
            }
            removedBytes = copyLength + lengthToRemove;
        }

        return removedBytes;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        int bytesRead = fillBuffer(b, off, len);
        if (bytesRead > -1) {
            for (int pos=off; pos < bytesRead; pos++) {
                int detectedCharLen = getFilteredCharLengthIfDetected(b, pos, len + off);
                if (detectedCharLen > 0) {
                    int bytesRemoved = removeChar(b, pos, off + len, detectedCharLen);
                    bytesRead -= bytesRemoved;
                    if (bytesRemoved >= detectedCharLen) {
                        pos--;
                    }
                }
            }
        }

        return bytesRead;
    }
}