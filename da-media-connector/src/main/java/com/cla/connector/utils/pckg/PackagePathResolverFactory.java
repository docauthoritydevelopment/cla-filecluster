package com.cla.connector.utils.pckg;

import com.cla.connector.utils.FileNamingUtils;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class PackagePathResolverFactory {

    private static PackagePathResolver nonSupportedPackageResolver = new PackagePathResolver() {
        @Override
        public boolean isPackageEntry(String filename) {
            return false;
        }

        @Override
        public String getPackageEntryPath(String filename) {
            throw new UnsupportedOperationException(String.format("getPackageEntryPath for %s", filename));
        }

        @Override
        public String getPackageRootPath(String filename) {
            throw new UnsupportedOperationException(String.format("getPackageRootPath for %s", filename));
        }

        @Override
        public boolean isPackage(String filename) {
            return false;
        }

		@Override
		public String getPackageIdentifier() {
			return "";
		}
	};

    private static Map<String, PackagePathResolver> registry = ImmutableMap.of("zip", new ZipPackagePathResolver());

    public static PackagePathResolver get(String filename) {
        if (filename == null) {
            return nonSupportedPackageResolver;
        }
        String filenameExtension;
        int indexOfPckgSeparator = filename.lastIndexOf(PackagePathResolver.PACKAGE_SEPARATOR);
        if (indexOfPckgSeparator > -1) {
            filenameExtension = FileNamingUtils.getFilenameExtension(filename.substring(0, indexOfPckgSeparator));
        } else {
            filenameExtension = FileNamingUtils.getFilenameExtension(filename);
        }
        return filenameExtension == null ? nonSupportedPackageResolver :
                registry.getOrDefault(filenameExtension.toLowerCase(), nonSupportedPackageResolver);
    }
}
