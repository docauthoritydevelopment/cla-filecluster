package com.cla.connector.utils.pckg;

public interface PackagePathResolver {

	String PACKAGE_SEPARATOR = "*";

	String getPackageIdentifier();

	boolean isPackageEntry(String filename);

    String getPackageEntryPath(String filename);

    String getPackageRootPath(String filename);

    boolean isPackage(String filename);
}
