package com.cla.connector.utils;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * Created by: yael
 * Created on: 2/13/2018
 */
public class TextUtils {

    private static final String PASSWORD_JSON_LOOKUP_REGEX = "\"password\":\"[^\"]+\"";
    private static final String PASSWORD_JSON_REPLACE_TO = "\"password\":\"******\"";
    private static final String PASSWORD_JSON_REPLACE_TO_EMPTY = "\"password\":\"\"";

    public static String removeUnprintableCharacters(String text) {
        StringBuilder newString = null;
        boolean modified = false;
        for (int offset = 0; offset < text.length();) {
            int codePoint = text.codePointAt(offset);
            offset += Character.charCount(codePoint);
            switch (Character.getType(codePoint)) {
                case Character.CONTROL:     // \p{Cc}
                case Character.FORMAT:      // \p{Cf}
                case Character.PRIVATE_USE: // \p{Co}
                case Character.SURROGATE:   // \p{Cs}
                case Character.UNASSIGNED:  // \p{Cn}
                case Character.LINE_SEPARATOR:
                case Character.PARAGRAPH_SEPARATOR:
                    if (!modified) {
                        modified = true;
                        newString = new StringBuilder((offset == 0) ? "" : text.substring(0, offset-1));
                    }
                    if (Character.isSpaceChar(codePoint)) {
                        newString.append(" ");
                    } else if (Character.isWhitespace(codePoint)) {
                        newString.append("\n");
                    }
                    break;
                default:
                    if (modified) {
                        newString.append(Character.toChars(codePoint));
                    }
                    break;
            }
        }
        return modified?newString.toString():text;
    }

    public static String obfuscatePassword(String jsonData) {
        return Optional.ofNullable(jsonData)
                .orElse("")
                .replaceAll(PASSWORD_JSON_LOOKUP_REGEX, PASSWORD_JSON_REPLACE_TO);
    }

    public static String removePassword(String jsonData) {
        return Optional.ofNullable(jsonData)
                .orElse("")
                .replaceAll(PASSWORD_JSON_LOOKUP_REGEX, PASSWORD_JSON_REPLACE_TO_EMPTY);
    }

    public static boolean containsNonBmpCodePoint(String str) {
        if (str == null) {
            return false;
        }
        return str.codePoints().anyMatch(codePoint -> !Character.isBmpCodePoint(codePoint));
    }

    /**
     * Returns a new string built from the given str parameter with any supplementary code points replaced by the given
     * replacement parameter.
     *
     * @see <a href = "http://www.oracle.com/us/technologies/java/supplementary-142654.html">
     *          Supplementary Characters in the Java Platform
     *     </a>
     */
    @SuppressWarnings("JavaDoc")
    public static String replaceNonBmpCodePoints(String str, String replacement) {
        if (str == null) {
            return null;
        }
        String finalReplacement = replacement == null ? "" : replacement; // effectively final for use in lambda
        StringBuilder resultBuilder = new StringBuilder(str.length());
        str.codePoints().forEach(
                codePoint -> {
                    if (Character.isBmpCodePoint(codePoint)) {
                        resultBuilder.appendCodePoint(codePoint);
                    } else {
                        resultBuilder.append(finalReplacement);
                    }
                });
        return resultBuilder.toString();
    }

    public static String requireNonNullNeitherEmpty(String s) {
        if (s == null) {
            throw new NullPointerException();
        }
        if (s.trim().isEmpty()) {
            throw new IllegalArgumentException();
        }
        return s;
    }

    public static String joinToStrings(String delimiter, Iterable<?> elements) {
        Objects.requireNonNull(delimiter);
        Objects.requireNonNull(elements);
        StringJoiner joiner = new StringJoiner(delimiter);
        elements.forEach(element -> joiner.add(element.toString()));
        return joiner.toString();
    }
}
