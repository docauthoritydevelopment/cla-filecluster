package com.cla.connector.utils;

import com.cla.connector.utils.pckg.PackagePathResolver;
import com.cla.connector.utils.pckg.PackagePathResolverFactory;
import com.google.common.base.Strings;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utils for handling different parsing tasks on file names and paths
 * Created by vladi on 2/9/2017.
 */
@SuppressWarnings("unused")
public class FileNamingUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileNamingUtils.class);

    /**
     * A replacement for slash ('/') character (which has other meaning in path names).
     * <p>
     * Looks like this '∕'
     */
    private static final String DIVISION_SLASH = new String(Character.toChars(0x2215));

    /**
     * A replacement for backslash ('\') character (which has other meaning in path names).
     * <p>
     * Looks like this '﹨'
     */
    private static final String SMALL_BACKSLASH = new String(Character.toChars(0xFE68));

    private static final String SEPARATOR_REPLACEMENT = "-";

    public static final int DEFAULT_STR_FIELD_SIZE = 256;

    public static final String EMPTY_EXTENSION_VALUE = "_empty_";

    private static Pattern pattern = Pattern.compile("[<>:|*/?\\\\]");

    public static String getFileNameFullPath(String fileName, String baseName) {
        return Strings.isNullOrEmpty(fileName) ? "" :
                fileName.substring(0, fileName.length() - (baseName == null ? 0 : baseName.length()));
    }

    public static String getFilenameBaseName(final String name) {
        return Strings.isNullOrEmpty(name) ? "" :
                FilenameUtils.getName(name);
    }

    public static String getFilenameExtension(final String name) {
        return Strings.isNullOrEmpty(name) ? "" :
                FilenameUtils.getExtension(name);
    }

    public static String calcExtension(String fileUrl) {
        String ext = StringUtils.lowerCase(trimToSize(getFilenameExtension(fileUrl)));
        if (Strings.isNullOrEmpty(ext)) {
            return EMPTY_EXTENSION_VALUE;
        }
        return ext;
    }

    public static String trimToSize(final String str) {
        if (Strings.isNullOrEmpty(str)) {
            return str;
        }
        int len = str.length();
        return (len < DEFAULT_STR_FIELD_SIZE) ?
                str : str.substring(0, DEFAULT_STR_FIELD_SIZE);
    }

    public static String getFilenamePath(final String name) {
        return Strings.isNullOrEmpty(name) ? "" :
                FilenameUtils.getFullPathNoEndSeparator(name);
    }

    private static String getFilenamePrefix(final String name) {
        return Strings.isNullOrEmpty(name) ? "" :
                FilenameUtils.getPrefix(name);
    }

    static String getPathPart(final String name, final int level) {
        if (level < 0 || Strings.isNullOrEmpty(name))
            return "";

        final String[] pathParts = FilenameUtils.getPath(name).split("[\\\\/]");
        if (level == 0) {
            return getFilenamePrefix(name) + pathParts[0];
        }

        return (level < pathParts.length) ? pathParts[level] : "";
    }

    /**
     * Replaces characters that are illegal in path names.
     * <p/>
     * Should be used in strings that did not necessarily come from path names (e.g. email subject) and are to be used
     * in path strings.
     * Replacements works as follows:
     * <li>Non-BMP (Supplementary) code points are replaced by a question mark character ('?')</li>
     * <li>Slash ('/') characters are replaced by a {@link FileNamingUtils#DIVISION_SLASH division slash} ('∕')</li>
     * <li>Backslash ('\') characters are replaced by a {@link FileNamingUtils#SMALL_BACKSLASH small backslash} ('﹨')</li>
     * <p/>
     *
     * If the path parameter is null, this method returns null.
     *
     * @param path A String with illegal characters replaced, or null if the given path was null
     * @return A string consisting of the given path string with the illegal characters replaces, or null if path was null
     */
    public static String replaceIllegalPathChars(String path) {
        if (path == null) {
            return null;
        }

        // replace non-BMP characters with '?'
        if (TextUtils.containsNonBmpCodePoint(path)) {
            logger.trace("Detected non-BMP characters in path string: {}", path);
            path = TextUtils.replaceNonBmpCodePoints(path, SEPARATOR_REPLACEMENT);
            logger.trace("replaced path string with non-BMP characters to: {}", path);
        }
        // replace possible file separators that are not meant as such
        path = path.replace("/", DIVISION_SLASH);
        path = path.replace("\\", SMALL_BACKSLASH);

        // reserved characters
        path = path.replace("<", SEPARATOR_REPLACEMENT);
        path = path.replace(">", SEPARATOR_REPLACEMENT);
        path = path.replace(":", SEPARATOR_REPLACEMENT);
        path = path.replace("\"", SEPARATOR_REPLACEMENT);
        path = path.replace("?", SEPARATOR_REPLACEMENT);
        path = path.replace("*", SEPARATOR_REPLACEMENT);
        path = path.replace("|", SEPARATOR_REPLACEMENT);

        return path;
    }

    public static boolean containsIllegalCharsForPath(String toExamine) {
        if (toExamine == null) {
            return false;
        }
        Matcher matcher = pattern.matcher(toExamine);
        return matcher.find();
    }

    public static String normalizeFileSharePath(String path) {
        Path normalize = Paths.get(path).toAbsolutePath().normalize();
        return convertPathToUniversalString(normalize);
    }

//TODO: Should be converted according to the media processor file system
    public static String convertPathToUniversalString(String path) {
        if (path == null) {
            return null;
        }
        path = FilenameUtils.separatorsToUnix(path);
        path = path.replace("//", "/");
        if (path.startsWith("file:/")) {
            path = path.substring(6);
        } else if (path.startsWith("http")) {
            path = path.replace(":/", "://"); // Regain lost '/' in order to re-standardize url.
        }

        if (!path.endsWith("/")) {
            path = path.concat("/");
        }
        return path;
    }

    public static String convertPathToUniversalString(Path path) {
        return convertPathToUniversalString(path.toString());
    }

    public static boolean isPackageEntry(String filename) {
        return PackagePathResolverFactory.get(filename).isPackageEntry(filename);
    }

	/**
	 * whether the entry is contained in a Package (e.g. zip), or a PST
	 */
	public static boolean isContainerEntry(String filename) {
		return !Strings.isNullOrEmpty(filename) && filename.contains(PackagePathResolver.PACKAGE_SEPARATOR) ;
	}

    public static String getLowestLevelContainerPath(String filename) {
	    return filename.substring(0, filename.lastIndexOf(PackagePathResolver.PACKAGE_SEPARATOR));
    }

    public static int getContainerNumber(String filename) {
        return StringUtils.countMatches(filename, PackagePathResolver.PACKAGE_SEPARATOR);
    }

    public static String getPackageEntryPath(String filename) {
        return PackagePathResolverFactory.get(filename).getPackageEntryPath(filename);
    }

    public static String getPackageRootPath(String filename) {
        return PackagePathResolverFactory.get(filename).getPackageRootPath(filename);
    }

    public static boolean isPackage(String filename) {
        return PackagePathResolverFactory.get(filename).isPackage(filename);
    }

    public static String  convertFromUnixToWindows(String path) {
        return FilenameUtils.separatorsToWindows(path);
    }

    public static String convertFromWindowsToUnix(String path) {
        String result = FilenameUtils.separatorsToUnix(path);
        if (path.startsWith("\\\\")) {
            result = result.substring(1);
        }
        return result;
    }

    public static String getParentDir(String path) {
        if (Strings.isNullOrEmpty(path)) return "";
        if (path.endsWith("/") || path.endsWith("\\")) {
            path = path.substring(0, path.length()-1);
        }
        int indexOf = path.lastIndexOf("/");
        if (indexOf < 0) {
            indexOf = path.lastIndexOf("\\");
        }
        if (indexOf <= 0) {
            return "";
        }
        return path.substring(0, indexOf+1);
    }

    public static String getParentNameOnly(String fileFullName) {
        String path = FileNamingUtils.getParentDir(fileFullName);
        final String[] pathParts = FilenameUtils.getPath(path).split("[\\\\/]");
        return pathParts.length == 0 ? "" : pathParts[pathParts.length-1];
    }
}
