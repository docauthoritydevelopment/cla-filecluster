package com.cla.connector.utils;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Time source to determine current time,
 * supports unit testing and adds several helpful features
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class TimeSource {

    private Random random = new Random();
    private long simulatedTime = 0;
    private String timeZone = "UTC";
    private int year = 1970;
    private int month = 1;
    private int day = 1;
    private int hour = 0;
    private int minute = 0;
    private int second = 0;

    public static TimeSource create(){
        return new TimeSource();
    }

    public TimeSource() {}

    public TimeSource(long simulatedTime) {
        this.simulatedTime = simulatedTime;
    }

    public TimeSource withSeed(long seed){
        random = new Random(seed);
        return this;
    }

    public long currentTimeMillis() {
        return simulatedTime == 0 ? System.currentTimeMillis() : simulatedTime;
    }

    public long currentTimeSeconds() {
        return TimeUnit.MILLISECONDS.toSeconds(currentTimeMillis());
    }

    public long millisSince(long historicalTimeMillis) {
        return currentTimeMillis() - historicalTimeMillis;
    }

    public long timeSecondsAgo(long secondsAgo) {
        return timeMillisAgo(TimeUnit.SECONDS.toMillis(secondsAgo));
    }

    public long timeMillisAgo(long millisAgo) {
        return currentTimeMillis() - millisAgo;
    }

    public long secondsSince(long historicalTimeMillis) {
        return TimeUnit.MILLISECONDS.toSeconds(currentTimeMillis() - historicalTimeMillis);
    }

    public long minutesSince(long historicalTimeMillis) {
        return TimeUnit.MILLISECONDS.toMinutes(currentTimeMillis() - historicalTimeMillis);
    }

    public long daysSince(long historicalTimeMillis) {
        return TimeUnit.MILLISECONDS.toDays(currentTimeMillis() - historicalTimeMillis);
    }

    public void setSimulatedTime(long simulatedTime) {
        this.simulatedTime = simulatedTime;
    }

    public TimeSource simulateRandomDateTime() {
        int randomMonth = randomMonth();
        setSimulatedDateTime(randomYear(), randomMonth, randomDay(randomMonth), randomHour(), randomMinOrSec(), randomMinOrSec());
        return this;
    }

    public TimeSource simulateRandomDateTime(long from, int within, TimeUnit unit) {
        Long gapUpperBound = unit.toMillis(within);
        long randomGap = random.nextInt(gapUpperBound.intValue());
        setSimulatedTime(from + randomGap);
        return this;
    }

    public int randomYear() {
        return 1970 + random.nextInt(50);
    }

    public int randomYear(int from, int to) {
        if (from < 1970){
            from = 1970;
        }
        return from + random.nextInt(to - from);
    }

    public int randomMonth() {
        return 1 + random.nextInt(11);
    }

    public int randomDay(int month) {
        switch (month) {
            case 2:
                return 1 + random.nextInt(27);
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 1 + random.nextInt(30);
            default:
                return 1 + random.nextInt(29);
        }
    }

    public int randomHour() {
        return 1 + random.nextInt(11);
    }

    public int randomMinOrSec() {
        return 1 + random.nextInt(59);
    }

    public TimeSource setSimulatedDateTime(int year, int month, int day, int hour, int minute, int second) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        init();
        return this;
    }

    public TimeSource setSimulatedDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
        init();
        return this;
    }

    public TimeSource setSimulatedTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        init();
        return this;
    }

    public TimeSource setSimulatedTimeZone(String timeZone) {
        this.timeZone = timeZone;
        return this;
    }

    public TimeSource simulateSleep(long millis) {
        this.simulatedTime += millis;
        return this;
    }

    private void init() {
        setSimulatedTime(
                TimeUnit.SECONDS.toMillis(LocalDateTime.of(year, month, day, hour, minute, second)
                        .atZone(TimeZone.getTimeZone(timeZone).toZoneId()).toEpochSecond()));
    }

}
