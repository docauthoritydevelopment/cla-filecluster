package com.cla.connector.utils;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Predicate;

import static com.cla.connector.domain.dto.file.FileType.*;

/**
 * Utilities for supported file types
 * Created by vladi on 2/28/2017.
 */
public class FileTypeUtils {

    private static final Logger logger = LoggerFactory.getLogger(FileTypeUtils.class);

    public static final String PST_FILE_EXT = "pst";
    public static final String ZIP_FILE_EXT = "zip";

    private static Map<String, FileType> scanFileTypesMap;
    private static List<String> wordFileExtensions = Lists.newArrayList("docx", "doc");
    private static List<String> excelFileExtensions = Lists.newArrayList("xlsx", "xls");
    private static List<String> pdfFileExtensions = Lists.newArrayList("pdf");
    private static List<String> packageFileExtensions = Lists.newArrayList(ZIP_FILE_EXT);
    private static List<String> pstFileExtensions = Lists.newArrayList(PST_FILE_EXT);
    private static List<String> mailFileExtensions = Lists.newArrayList("eml", ItemType.MESSAGE.getSpecialExtension());

    static {
        scanFileTypesMap = Maps.newHashMap();
        wordFileExtensions.forEach(s -> scanFileTypesMap.put(s, WORD));
        excelFileExtensions.forEach(s -> scanFileTypesMap.put(s, EXCEL));
        pdfFileExtensions.forEach(s -> scanFileTypesMap.put(s, PDF));
        packageFileExtensions.forEach(s -> scanFileTypesMap.put(s, PACKAGE));
        pstFileExtensions.forEach(s -> scanFileTypesMap.put(s, PST));
        mailFileExtensions.forEach(s -> scanFileTypesMap.put(s, MAIL));
    }

    public static synchronized void addExtension(String newFileType, FileType type) {
        switch (type) {
            case WORD:
                wordFileExtensions.add(newFileType);
                break;
            case EXCEL:
                excelFileExtensions.add(newFileType);
                break;
            case PDF:
                pdfFileExtensions.add(newFileType);
                break;
            case PACKAGE:
                packageFileExtensions.add(newFileType);
                break;
            case PST:
                pstFileExtensions.add(newFileType);
                break;
            case MAIL:
                mailFileExtensions.add(newFileType);
                break;
            default:
                break;
        }
        scanFileTypesMap.put(newFileType, type);
    }

    public static Map<String, FileType> getScanFileTypesMap() {
        return scanFileTypesMap;
    }

    public static Predicate<? super String> createFileTypesPredicate(ScanTypeSpecification scanTypeSpecification) {
        return path -> {
            Set<String> extsToIgnore = scanTypeSpecification.getExtensionsToIgnore();
            String ext = FileNamingUtils.getFilenameExtension(path).toLowerCase();
            if (extsToIgnore != null && extsToIgnore.contains(ext)) {
                if (logger.isDebugEnabled())
                    logger.debug("extension: {} is excluded. Skipping {}", ext, path);
                return false;
            }

            Set<String> extensionsToScan = scanTypeSpecification.getExtensionsToScan();
            List<FileType> typesToScan = scanTypeSpecification.getTypesToScan();
            boolean shouldScanOthers = typesToScan != null && typesToScan.contains(OTHER);

            boolean isPartOfZip = path.toLowerCase().indexOf("."+ZIP_FILE_EXT+"*") > 0;
            boolean isPartOfPst = path.toLowerCase().indexOf("."+PST_FILE_EXT+"*") > 0;
            boolean isZipAllowed = extensionsToScan != null && extensionsToScan.contains(ZIP_FILE_EXT);
            boolean isPstAllowed = extensionsToScan != null && extensionsToScan.contains(PST_FILE_EXT);

            boolean allowedByContainer = (!isPartOfZip && !isPartOfPst) || // not in container
                    (isPartOfZip && isZipAllowed) || (isPartOfPst && isPstAllowed); // in container and allowed

            return (extensionsToScan != null && extensionsToScan.contains(ext) && allowedByContainer) ||
                   (shouldScanOthers && isExtensionConsideredOther(ext));
        };
    }

    public static void getFileTypesToScan(Map<String, FileType> concreteScanFileTypesMap, List<FileType> typesToScan) {
        getScanFileTypesMap().entrySet().stream()
                .filter(e -> typesToScan.contains(e.getValue()))
                .forEach(e -> concreteScanFileTypesMap.put(e.getKey(), e.getValue()));
    }

    public static FileType getFileType(final String path) {
        String ext = FileNamingUtils.getFilenameExtension(path);
        return getScanFileTypesMap().entrySet().stream()
                .filter(e -> e.getKey().equalsIgnoreCase(ext))
                .map(Map.Entry::getValue).findAny().orElse(OTHER);
    }

    private static boolean isExtensionConsideredOther(String extension) {
        Objects.requireNonNull(extension);
        return !scanFileTypesMap.containsKey(extension);
    }

    @Deprecated
    public static Predicate<? super String> createFileTypePredicate(final List<FileType> typesToScan,
                                                                    final Collection<String> fileExts,
                                                                    Set<String> scanOtherIgnoreExtSet) {
        if (typesToScan == null || typesToScan.contains(OTHER)) {
            if (scanOtherIgnoreExtSet.isEmpty()) {
                return path -> true;
            }
            else {
                return (path -> {
                    String ext = FileNamingUtils.getFilenameExtension(path);
                    return ((ext == null) || !scanOtherIgnoreExtSet.contains(ext.toLowerCase()));
                });
            }
        }
        return (path -> {
            String ext = FileNamingUtils.getFilenameExtension(path);
            return ((ext != null) && fileExts.contains(ext.toLowerCase()));
        });
    }
}
