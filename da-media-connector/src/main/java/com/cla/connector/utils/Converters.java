package com.cla.connector.utils;

/**
 *
 * Created by vladi on 2/22/2017.
 */
public class Converters {

    public static String getExceptionText(final Throwable exception) {
        if (exception == null) {
            return null;
        }

        String eText = exception.getMessage();
        if (exception.getCause() != null) {
            Throwable c = exception.getCause();
            if (eText != null && c != null && c.getMessage() != null &&
                    eText.contains(c.getMessage()) && c.getCause() != null) {
                c = c.getCause();
            }
            if (c != null && c.getMessage() != null) {
                eText += ". Cause: " + c.getMessage();
            }
        }
        return eText;
    }

    public static int numberIntValue(Number n) {
        return n==null?0:n.intValue();
    }

    public static long numberLongValue(Number n) {
        return n==null?0l:n.longValue();
    }

}
