package com.cla.connector.progress;

/**
 * Used to update the progress of a job that comprises multiple execution stages.
 *
 * Created by Itai Marko.
 */
public interface ProgressTracker {

    /**
     * Initiates tracking of a stage, assigning it the given weight.
     * Should be called before the execution of each stage.
     *
     * @param stageWeight  the weight to assign to the stage
     */
    void startStageTracking(int stageWeight);

    /**
     * Sets an estimate of the total work that's going to be done by the current stage.
     * Should be called in the beginning of the execution of each stage.
     *
     * @param stageTotalEstimate  the estimate of the stage's work to be done
     */
    void setCurStageEstimatedTotal(long stageTotalEstimate);

    /**
     * Advances the progress of the job to indicate the given amount of work was completed.
     *
     * @param amount  the amount of work done that should be reported as progress
     * @return        true if progress was actually reported, false if it was just accumulated to be reported later
     */
    boolean incrementProgress(long amount);

    /**
     * Advances the progress of the job to indicate that the current stage is done.
     */
    void endCurStage();

    /**
     * Ends the progress tracking by this ProgressTracker.
     * Typically this means to report progress to 100%.
     */
    void endTracking();
}
