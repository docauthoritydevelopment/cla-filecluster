package com.cla.connector.progress;

/**
 * An empty implementation of {@link com.cla.connector.progress.ProgressTracker}.
 *
 * Created by Itai Marko.
 */
public class DummyProgressTracker implements ProgressTracker {

    @Override
    public void startStageTracking(int stageWeight) {}

    @Override
    public void setCurStageEstimatedTotal(long stageTotalEstimate) {}

    @Override
    public boolean incrementProgress(long amount) { return false; }

    @Override
    public void endCurStage() {}

    @Override
    public void endTracking() {}
}
