package com.cla.connector.mediaconnector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
// TODO Itai: Test this class
/**
 * A thread-safe turtle that holds the throttling state (the point in time until which to throttle) and provides
 * thread-safe methods to change this throttling state ({@link ThrottlingStartledTurtle#throttleFor(int)}) and
 * to wait (block) until the throttling period is over ({@link ThrottlingStartledTurtle#acquireThrottlingPermit()}).
 * <p/>
 * All threads that may get throttled by the same service should use the same instance of this class.
 *
 * <p/>
 * Created By Itai Marko
 */
public class ThrottlingStartledTurtle { // Break your teeth on this one!

    private static final Logger logger = LoggerFactory.getLogger(ThrottlingStartledTurtle.class);

    private long throttleUntil = 0L; // init to Epoch, in milliseconds
    private ReentrantLock lock = new ReentrantLock();
    private Condition isThrottled = lock.newCondition();

    /**
     * Enters a throttled state for the given amount of time and blocks until throttling time passes.
     * <p/>
     * Note that while this method blocks, another thread may call this method and prolong the throttling time.
     * In such a case all threads that are waiting for throttling to be over will block for the prolonged throttling time.
     *
     * @param secondsToThrottle     time to throttle in seconds, must be positive
     * @return true if returning after the throttling period passes, false if was interrupted before it passed
     */
    public boolean throttleFor(int secondsToThrottle) {
        long now = System.currentTimeMillis();
        if (secondsToThrottle <= 0) {
            throw new IllegalArgumentException("Can't throttle for a non-positive time. Got " + secondsToThrottle + " seconds");
        }
        logger.info("Throttling for {} seconds. Was previously set to throttle until {} millis since epoch (now it's {})", secondsToThrottle, throttleUntil, now);
        long requestedToThrottleUntil = now + SECONDS.toMillis(secondsToThrottle); // TODO Itai: It will be possible for this code to overflow approximately after 03:26:15 Fri Nov 22nd of year 224,227,946 IST (hopefully I won't be maintaining this code at that time..)

        lock.lock();
        try {
            if (throttleUntil < requestedToThrottleUntil) { // Only update if prolonging the throttling period
                logger.info("Prolonging the throttling time from {} to {} millis since epoch", throttleUntil, requestedToThrottleUntil);
                throttleUntil = requestedToThrottleUntil;
            }
            logger.info("Throttling      until {} millis since epoch (now it's: {})", throttleUntil, now);
            waitForThrottlingPeriodToPass();
            logger.info("Done Throttling until {} millis since epoch (now it's: {})", throttleUntil, System.currentTimeMillis());
            return true;

        } catch (InterruptedException e) {
            return handleInterruption();

        } finally {
            lock.unlock();
        }
    }

    /**
     * Checks if we're in a throttling state and if so, block until it ends
     * <p/>
     * Note that while this method blocks, another thread may prolong the throttling time by calling
     * {@link ThrottlingStartledTurtle#throttleFor(int)}. In such a case all threads that are waiting for throttling to
     * be over will continue to block for the prolonged throttling time.
     *
     */
    public boolean acquireThrottlingPermit() {
        lock.lock();
        try {
            logger.info("Waiting for throttling period to pass      until {} millis since epoch (now it's: {})", throttleUntil, System.currentTimeMillis());
            waitForThrottlingPeriodToPass();
            logger.info("Done waiting for throttling period to pass until {} millis since epoch (now it's: {})", throttleUntil, System.currentTimeMillis());
            return true;
        } catch (InterruptedException e) {
            return handleInterruption();
        } finally {
            lock.unlock();
        }
    }

    /**
     * MUST ACQUIRE THE LOCK BEFORE CALLING THIS METHOD!!
     */
    private void waitForThrottlingPeriodToPass() throws InterruptedException {
        long now = System.currentTimeMillis();
        long nanosToThrottle = MILLISECONDS.toNanos(throttleUntil - now);
        while (nanosToThrottle > 0) {
            logger.info("Waiting for {} nanos, until {} millis since epoch (now it's: {})", nanosToThrottle, throttleUntil, now); // A second log line from here (of the same thread) indicates either some other thread signaled this condition before timeout (not supposed to happen as no code signals this condition), or a spurious wakeup happened before timeout
            nanosToThrottle = isThrottled.awaitNanos(nanosToThrottle);
        }
    }

    private boolean handleInterruption() {
        long now;
        now = System.currentTimeMillis();
        long remainingThrottlingPeriod = throttleUntil-now;
        logger.warn("Throttling was interrupted. Was supposed to throttle for {} more millis, until {} millis since epoch (now it's: {})", remainingThrottlingPeriod, throttleUntil, now);
        return remainingThrottlingPeriod > 0;
    }
}
