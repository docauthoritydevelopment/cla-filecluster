package com.cla.connector.mediaconnector;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileContentDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.messages.DirListingPayload;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;

import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Media connector interface
 * Created by uri on 08/08/2016.
 */
public interface FileMediaConnector extends MediaConnector{

    ClaFilePropertiesDto getFileAttributes(String fileId) throws FileNotFoundException ;

    ClaFilePropertiesDto getMediaItemAttributes(@NotNull String mediaItemId) throws FileNotFoundException;

    ClaFilePropertiesDto getMediaItemAttributes(String mediaItemId, boolean fetchAcls) throws FileNotFoundException ;

    InputStream getInputStream(ClaFilePropertiesDto props) throws IOException;

    void streamMediaItems(ScanTaskParameters scanParams,
                          Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
                          Consumer<DirListingPayload> directoryListingConsumer,
                          Consumer<String> createScanTaskConsumer,
                          Predicate<Pair<Long, Long>> scanActivePredicate, ProgressTracker filePropsProgressTracker);

    void concurrentStreamMediaItems(ForkJoinPool forkJoinPool, ScanTaskParameters scanParams,
									Consumer<ClaFilePropertiesDto> filePropertiesConsumer,
									Consumer<DirListingPayload> directoryListingConsumer,
									Consumer<String> createScanTaskConsumer,
									Predicate<Pair<Long, Long>> scanActivePredicate,
                                    ProgressTracker filePropsProgressTracker);

    default void streamMediaChangeLog(
            ScanTaskParameters scanParams, String realPath, Long runId, long startChangeLogPosition,
            Consumer<MediaChangeLogDto> changeConsumer, Consumer<String> createScanTaskConsumer,
            Predicate<? super String> fileTypesPredicate) {

        throw new UnsupportedOperationException();
    }

    default void concurrentStreamMediaChangeLog(
            ForkJoinPool forkJoinPool, ScanTaskParameters scanParams, String realPath, Long runId,
            long startChangeLogPosition, Consumer<MediaChangeLogDto> changeConsumer,
            Consumer<String> createScanTaskConsumer, Predicate<? super String> fileTypesPredicate) {

        throw new UnsupportedOperationException();
    }

    boolean isDirectoryExists(String path, boolean ignoreAccessErrors);

    void fetchAcls(ClaFilePropertiesDto claFileProp);

    String getIdentifier();

	FileContentDto getFileContent(String filename, boolean forUserDownload) throws IOException;

	List<ServerResourceDto> browseSubFolders(String folderMediaEntityId);

	default Pair<FileContentDto,ClaFilePropertiesDto> getItemDtoAndContent(String mediaItemId, boolean forUserDownload) throws IOException {
        FileContentDto fileContent = getFileContent(mediaItemId, forUserDownload);
        //TODO: [Costa] file attributes should be cached to prevent unnecessary external api calls, it is already fetched in the GeneralIngestRequestHandlerProvider
        ClaFilePropertiesDto filePropsDto = getFileAttributes(mediaItemId);
        return Pair.of(fileContent, filePropsDto);
    }

    default Map<String, Pair<FileContentDto, ClaFilePropertiesDto>> getContainerFileDataList(
            String fileIdentifier, Set<String> expectedContainedFilesIdentifiers)
            throws IOException {

	    // default impl - just call the regular getItemDtoAndContent() method for non-container files and wrap in a map
        Pair<FileContentDto, ClaFilePropertiesDto> fileData = getItemDtoAndContent(fileIdentifier, false);
        return Collections.singletonMap(fileIdentifier, fileData);
    }

    default List<String> getSharePermissions(String rootFolderPath) throws Exception {
	    return new ArrayList<>();
    }
}