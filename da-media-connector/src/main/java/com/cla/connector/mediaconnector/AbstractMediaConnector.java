package com.cla.connector.mediaconnector;

import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;

import javax.validation.constraints.NotNull;
import java.io.FileNotFoundException;
import java.util.function.Consumer;

/**
 *
 * Created by vladi on 2/8/2017.
 */
public abstract class AbstractMediaConnector implements FileMediaConnector {

	protected ProgressTracker dummyTracker = new DummyProgressTracker();

	private long connectionDetailsRefreshed = 0L;

    @Override
    public ClaFilePropertiesDto getMediaItemAttributes(@NotNull String mediaItemId, final boolean fetchAcls)
            throws FileNotFoundException {

        ClaFilePropertiesDto fileProps = getMediaItemAttributes(mediaItemId);
        if (fileProps == null || !fetchAcls){
            return fileProps;
        }

        // if this is a folder - fetch ACLs
        // if this is a file - fetch ACLs only if it does not inherit the folder ACLs
        boolean doFetchAcls = fileProps.isFolder() ||
                AclInheritanceType.NONE.equals(fileProps.getAclInheritanceType());

        if (doFetchAcls) {
            fetchAcls(fileProps);
        }
        return fileProps;
    }

    public long getConnectionDetailsRefreshed() {
        return connectionDetailsRefreshed;
    }

    public void setConnectionDetailsRefreshed(long connectionDetailsRefreshed) {
        this.connectionDetailsRefreshed = connectionDetailsRefreshed;
    }

	protected Consumer<ClaFilePropertiesDto> getChangeLogConsumerWrapper(Consumer<MediaChangeLogDto> changeConsumer) {
		return (item) -> {
			MediaChangeLogDto changeLog = MediaChangeLogDto.create();
			changeLog.setFileName(item.getFileName());
			changeLog.setEventType(DiffType.RENAMED);
			changeLog.setFolder(item.isFolder());
			changeLog.setMediaItemId(item.getMediaItemId());
			changeLog.setAclInheritanceType(item.getAclInheritanceType());
			changeLog.setModTimeMilli(item.getModTimeMilli());
			changeLog.setOwnerName(item.getOwnerName());
			changeLog.setFileSize(item.getFileSize());
			changeConsumer.accept(changeLog);
		};
	}
}
