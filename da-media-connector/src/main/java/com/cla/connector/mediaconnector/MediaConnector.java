package com.cla.connector.mediaconnector;

import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaType;

import java.util.List;

public interface MediaConnector {

    MediaType getMediaType();

    List<ServerResourceDto> testConnection();

    void setConnectionDetailsRefreshed(long connectionDetailsRefreshed);

    long getConnectionDetailsRefreshed();

}
