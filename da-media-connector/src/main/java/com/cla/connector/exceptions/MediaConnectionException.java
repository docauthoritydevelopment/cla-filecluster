package com.cla.connector.exceptions;

import com.cla.connector.domain.dto.error.BadRequestType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by uri on 07/07/2015.
 */
public class MediaConnectionException extends RuntimeException {

    private Logger logger = LoggerFactory.getLogger(MediaConnectionException.class);
    private BadRequestType type;

    public MediaConnectionException(String requestField, Object value, BadRequestType type) {
        super("Bad Http Request - request field [" + requestField + "] has invalid value [" + value + "]");
        this.type = type;
        logger.warn(this.getMessage());
    }

    public MediaConnectionException(String requestField, Object value, Exception cause, BadRequestType type) {
        super("Bad Http Request - request field [" + requestField + "] has invalid value [" + value + "]", cause);
        this.type = type;
        logger.warn(this.getMessage(), cause);
    }

    public MediaConnectionException(String requestField, Object value, String reason, BadRequestType type) {
        super("Bad Http Request - request field [" + requestField + "] has invalid value [" + value + "] due to [" + reason + "]");
        this.type = type;
        logger.warn(this.getMessage(), reason);
    }

    public MediaConnectionException(String reason, Exception e, BadRequestType type) {
        super("Bad Http Request - [" + reason + "]", e);
        this.type = type;
        logger.warn(this.getMessage(), e);
    }

    public MediaConnectionException(String reason, BadRequestType type) {
        super(reason);
        this.type = type;
    }

    public BadRequestType getType() {
        return type;
    }

    public void setType(BadRequestType type) {
        this.type = type;
    }
}
