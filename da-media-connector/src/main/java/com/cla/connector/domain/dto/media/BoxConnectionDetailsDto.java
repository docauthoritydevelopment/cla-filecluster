package com.cla.connector.domain.dto.media;

public class BoxConnectionDetailsDto extends MediaConnectionDetailsDto {
	private String jwt;

	public BoxConnectionDetailsDto() {
		this.mediaType = MediaType.BOX;
	}

	public String getJwt() {
		return jwt;
	}

	public BoxConnectionDetailsDto setJwt(String jwt) {
		this.jwt = jwt;
		return this;
	}
}
