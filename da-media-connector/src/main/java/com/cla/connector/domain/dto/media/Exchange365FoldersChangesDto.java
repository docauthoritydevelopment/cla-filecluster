package com.cla.connector.domain.dto.media;

import java.util.List;

/**
 * Created By Itai Marko
 */
public class Exchange365FoldersChangesDto extends MediaChangeLogDto {

    private List<Exchange365ItemDto> FolderChangesDtos;

    private String mailboxUPN;

    @SuppressWarnings("unused") // needed for JSON serialization
    public Exchange365FoldersChangesDto() {
    }

    public Exchange365FoldersChangesDto(List<Exchange365ItemDto> folderChangesDtos) {
        FolderChangesDtos = folderChangesDtos;
    }

    public List<Exchange365ItemDto> getFolderChangesDtos() {
        return FolderChangesDtos;
    }

    @SuppressWarnings("unused") // needed for JSON serialization
    public void setFolderChangesDtos(List<Exchange365ItemDto> folderChangesDtos) {
        FolderChangesDtos = folderChangesDtos;
    }

    public String getMailboxUPN() {
        return mailboxUPN;
    }

    public void setMailboxUPN(String mailboxUPN) {
        this.mailboxUPN = mailboxUPN;
    }
}
