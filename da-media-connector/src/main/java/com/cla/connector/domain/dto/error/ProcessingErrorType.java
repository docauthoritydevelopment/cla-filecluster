package com.cla.connector.domain.dto.error;

import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

public enum ProcessingErrorType {
    ERR_NONE(1, "none", false, true),
    ERR_UNKNOWN(2, "unknown", false, true),
    ERR_LOGIC_ERROR(3, "logic", false, false),                     // not used
    ERR_ENCRYPTED(4, "encrypted", false, true),
    ERR_CORRUPTED(5, "corrupted", true, true),
    ERR_OLD_FORMAT(6, "old-format", false, true),
    ERR_WRONG_EXT(7, "wrong-extension", false, true),
    ERR_IO(8, "io", true, true),
    STRICT_OPEN_XML(9, "strict-open-xml", false, true),
    ERR_LIMIT_EXCEEDED(10, "limit-exceeded", true, true),
    ERR_INDEXING(11, "indexing", false, false),                    // not used
    ERR_UPDATE_PROPERTIES(12, "update-properties", false, false), // not used
    ERR_UPDATE_ACL(13, "update-acl", false, false),                // not used
    ERR_FILE_NOT_FOUND(14, "file-not-found", true, true),
    ERR_TOO_MANY_REQUESTS(15, "too-many-requests", true, false),
    ERR_SUSPECT_POISON(16, "suspect-poison", true, false),         // out of memory
    PACKAGE_CRAWL_INTERNAL_ERROR(17, "package-crawl-internal-error", true, true),
    PACKAGE_CRAWL_PASSWORD_PROTECTED(18, "package-crawl-password-protected", false, true),
    ERR_OFFLINE_SKIPPED(19, "offline-skipped", true, true),
    ERR_MEMORY_LIMIT_EXCEEDED(20, "memory-limit-exceeded", true, true),
    ERR_FAILED_OPEN_FILE(21, "failed-open-file", true, false),       // not used
    ERR_PARSING_FAILURE(22, "parsing-failure", true, true),
    ERR_OFFICE_XML_ERROR(23, "office-xml-file-error", true, true),
    ERR_FAILED_CONTENT_RETRIEVAL(24, "failed-content-retrieval", true, false), // not used
    ERR_NETWORK(25, "network-issue", true, true),
    //  ERR_FILE_EMPTY, //15
    ;

    private static final Map<String, ProcessingErrorType> textLookupMap = new HashMap<>();
    private static final Map<Integer, ProcessingErrorType> ordinalMapping = Maps.newHashMap();

    static {
        for (ProcessingErrorType processingErrorType : ProcessingErrorType.values()) {
            textLookupMap.put(processingErrorType.text, processingErrorType);
        }
        for (final ProcessingErrorType t : ProcessingErrorType.values()) {
            ordinalMapping.put(t.ordinal(), t);
        }
    }

    private final int value;
    private String text;
    private boolean fatal;
    private boolean isAssigned; // if can appear in db table

    ProcessingErrorType(int value, String text, boolean fatal, boolean forUiUsage) {
        this.value = value;
        this.text = text;
        this.fatal = fatal;
        this.isAssigned = forUiUsage;
    }

    public static ProcessingErrorType byText(String text) {
        return textLookupMap.get(text);
    }

    public String getText() {
        return text;
    }

    public boolean isFatal() {
        return fatal;
    }

    public int getValue() {
        return value;
    }

    public boolean isAssigned() {
        return isAssigned;
    }

    public static ProcessingErrorType ordinalOf(final int value) {
        final ProcessingErrorType mt = ProcessingErrorType.ordinalMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
