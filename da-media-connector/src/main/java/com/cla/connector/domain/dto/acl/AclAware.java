package com.cla.connector.domain.dto.acl;

import java.util.Collection;

public interface AclAware {

    Collection<String> getAclReadAllowed();
    Collection<String> getAclWriteAllowed();
    Collection<String> getAclReadDenied();
    Collection<String> getAclWriteDenied();

}
