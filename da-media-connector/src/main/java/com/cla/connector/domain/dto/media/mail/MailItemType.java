package com.cla.connector.domain.dto.media.mail;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Optional;

/**
 * Created By Itai Marko
 */
public enum MailItemType {
    FOLDER("fd") {
        @Override
        public Pair<String, String> splitEntryNameFromExt(String entryPath) {
            return Pair.of(entryPath, "");
        }
    }

    , EMAIL("em") {
        @Override
        public Pair<String, String> splitEntryNameFromExt(String entryPath) {
            return Pair.of(entryPath, "eml");
        }
    }

    , ATTACHMENT("at") {
        @Override
        public Pair<String, String> splitEntryNameFromExt(String entryPath) {
            int lastIndexOfDot = entryPath.lastIndexOf('.');
            if (lastIndexOfDot == -1) {
                return Pair.of(entryPath, "");
            } else {
                String entryName = entryPath.substring(0, lastIndexOfDot);
                String ext = entryPath.substring(lastIndexOfDot+1);
                return Pair.of(entryName, ext);
            }
        }
    }
    ;

    public final String shortName;

    MailItemType(String shortName) {
        this.shortName = shortName;
    }

    public abstract Pair<String, String> splitEntryNameFromExt(String entryPath);

    private static HashMap<String, MailItemType> shortNameToEntryType;

    static {
        MailItemType[] values = values();
        shortNameToEntryType = new HashMap<>(values.length, 1f);
        for (MailItemType entryType : values) {
            shortNameToEntryType.put(entryType.shortName, entryType);
        }
    }

    public static Optional<MailItemType> forShortName(String shortName) {
        return Optional.ofNullable(shortNameToEntryType.get(shortName));
    }
}
