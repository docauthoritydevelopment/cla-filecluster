package com.cla.connector.domain.dto.media;

import java.util.List;

/**
 * Created by: yael
 * Created on: 2/13/2019
 */
public interface MailPropertiesDto {
    EmailAddress getSender();
    List<EmailAddress> getRecipients();
    String getEmailSubject();
}
