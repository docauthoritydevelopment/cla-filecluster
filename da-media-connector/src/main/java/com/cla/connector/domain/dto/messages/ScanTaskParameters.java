package com.cla.connector.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DTO for scan task parameters
 * Created by vladi on 2/9/2017.
 */
@SuppressWarnings("WeakerAccess")
public class ScanTaskParameters implements PhaseMessagePayload {

    public static final String MAILBOX_UPN = "mailboxUpn";
    public static final String SYNC_STATE = "syncState";

    private Long runId;
    private String path;
    private boolean firstScan = true;
    private long lastScannedOn;
    private List<FolderExcludeRuleDto> excludedRules;
    private List<String> skipPaths;
    private List<String> skipDirnames;
    private MediaConnectionDetailsDto connectionDetails;
    private Long rootFolderId;
    private long jobId = 0;
    private ScanTypeSpecification scanTypeSpecification;
    private String username;
    private String password;

	private String rootFolderMediaEntityId;

    private transient boolean fullScan = true;
	private boolean fstLevelOnly = false;
	private long startScanTime;
	private String mailboxUPN;
	private String syncState;
	private Long fromDateFilter;

    private Long jobStateIdentifier;

    private boolean getSharePermissions;

    private boolean ignoreAccessErrors = false;

	/**
	 * Mutable, beware! (maybe should be immutable?)
	 */
	private Map<String,String> keyValuePairs = new HashMap<>();

    private ScanTaskParameters() {
    }

    public static ScanTaskParameters create() {
        return new ScanTaskParameters();
    }

    public ScanTaskParameters withPath(Path newPath) {
        return copy().setPath(newPath.toString());
    }

    public ScanTaskParameters copy() {
        return ScanTaskParameters.create()
                .setRunId(runId)
                .setRootFolderId(rootFolderId)
                .setPath(path)
                .setFirstScan(firstScan)
                .setLastScannedOnTimestamp(lastScannedOn)
                .setExcludedRules(excludedRules)
                .setSkipPaths(skipPaths)
                .setSkipDirnames(skipDirnames)
                .setScanTypeSpecification(new ScanTypeSpecification(scanTypeSpecification))
                .setJobId(jobId)
                .setUsername(username)
                .setPassword(password)
                .setFstLevelOnly(fstLevelOnly)
                .setStartScanTime(startScanTime)
				.setRootFolderMediaEntityId(rootFolderMediaEntityId)
				.setKeyValuePairs(keyValuePairs)
                .setMailboxUPN(mailboxUPN)
                .setSyncState(syncState)
                .setFromDateFilter(fromDateFilter)
                .setConnectionDetails(connectionDetails)
                .setJobStateIdentifier(jobStateIdentifier)
                .setGetSharePermissions(getSharePermissions)
                .setIgnoreAccessErrors(ignoreAccessErrors);

    }

	public String getRootFolderMediaEntityId() {
		return rootFolderMediaEntityId;
	}

	public ScanTaskParameters setRootFolderMediaEntityId(String rootFolderMediaEntityId) {
		this.rootFolderMediaEntityId = rootFolderMediaEntityId;
		return this;
	}

    public Long getRunId() {
        return runId;
    }

    public ScanTaskParameters setRunId(Long runId) {
        this.runId = runId;
        return this;
    }

    public long getLastScannedOnTimestamp() {
        return lastScannedOn;
    }

    public ScanTaskParameters setLastScannedOnTimestamp(long lastScannedOn) {
        this.lastScannedOn = lastScannedOn;
        return this;
    }

    public String getPath() {
        return path;
    }

    public ScanTaskParameters setPath(String path) {
        this.path = path;
        return this;
    }

    public List<FolderExcludeRuleDto> getExcludedRules() {
        return excludedRules;
    }

    public ScanTaskParameters setExcludedRules(List<FolderExcludeRuleDto> excludedRules) {
        this.excludedRules = excludedRules;
        return this;
    }

    public List<String> getSkipPaths() {
        return skipPaths;
    }

    public ScanTaskParameters setSkipPaths(List<String> skipPaths) {
        this.skipPaths = skipPaths;
        return this;
    }

    public List<String> getSkipDirnames() {
        return skipDirnames;
    }

    public ScanTaskParameters setSkipDirnames(List<String> skipDirnames) {
        this.skipDirnames = skipDirnames;
        return this;
    }

    public boolean isFirstScan() {
        return firstScan;
    }

    public ScanTaskParameters setFirstScan(boolean firstScan) {
        this.firstScan = firstScan;
        return this;
    }

    public MediaConnectionDetailsDto getConnectionDetails() {
        return connectionDetails;
    }

    public ScanTaskParameters setConnectionDetails(MediaConnectionDetailsDto connectionDetails) {
        this.connectionDetails = connectionDetails;
        return this;
    }

    public long getJobId() {
        return jobId;
    }

    public ScanTaskParameters setJobId(long jobId) {
        this.jobId = jobId;
        return this;
    }

    public boolean isFstLevelOnly() {
        return fstLevelOnly;
    }

    public ScanTaskParameters setFstLevelOnly(boolean fstLevelOnly) {
        this.fstLevelOnly = fstLevelOnly;
        return this;
    }

    public long getStartScanTime() {
        return startScanTime;
    }

    public ScanTaskParameters setStartScanTime(long startScanTime) {
        this.startScanTime = startScanTime;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public ScanTaskParameters setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public ScanTaskParameters setPassword(String password) {
        this.password = password;
        return this;
    }

    public ScanTypeSpecification getScanTypeSpecification() {
        return scanTypeSpecification;
    }

    public ScanTaskParameters setScanTypeSpecification(ScanTypeSpecification scanTypeSpecification) {
        this.scanTypeSpecification = scanTypeSpecification;
        return this;
    }

    public boolean isFullScan() {
        return fullScan;
    }

    public void setFullScan(boolean fullScan) {
        this.fullScan = fullScan;
    }

	public Map<String, String> getKeyValuePairs() {
		return keyValuePairs;
	}

	public ScanTaskParameters setKeyValuePairs(Map<String, String> keyValuePairs) {
		this.keyValuePairs = keyValuePairs;
		return this;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("runId", runId)
				.add("path", path)
				.add("firstScan", firstScan)
				.add("lastScannedOn", lastScannedOn)
				.add("excludedRules", excludedRules)
				.add("skipPaths", skipPaths)
				.add("skipDirnames", skipDirnames)
				.add("connectionDetails", connectionDetails)
				.add("rootFolderId", rootFolderId)
				.add("jobId", jobId)
				.add("scanTypeSpecification", scanTypeSpecification)
				.add("username", username)
				.add("password", password)
				.add("rootFolderMediaEntityId", rootFolderMediaEntityId)
				.add("fullScan", fullScan)
				.add("fstLevelOnly", fstLevelOnly)
				.add("startScanTime", startScanTime)
				.add("keyValuePairs", keyValuePairs)
				.toString();
	}

	@Override
    @JsonIgnore
    public ProcessingErrorType calculateProcessErrorType() {
        return ProcessingErrorType.ERR_NONE;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public ScanTaskParameters setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public String getMailboxUPN() {
        return mailboxUPN;
    }

    public ScanTaskParameters setMailboxUPN(String mailboxUPN) {
        this.mailboxUPN = mailboxUPN;
        return this;
    }

    public String getSyncState() {
        return syncState;
    }

    public ScanTaskParameters setSyncState(String syncState) {
        this.syncState = syncState;
        return this;
    }

    public Long getFromDateFilter() {
        return fromDateFilter;
    }

    public ScanTaskParameters setFromDateFilter(Long fromDateFilter) {
        this.fromDateFilter = fromDateFilter;
        return this;
    }

    public Long getJobStateIdentifier() {
        return jobStateIdentifier;
    }

    public ScanTaskParameters setJobStateIdentifier(Long jobStateIdentifier) {
        this.jobStateIdentifier = jobStateIdentifier;
        return this;
    }

    public boolean isGetSharePermissions() {
        return getSharePermissions;
    }

    public ScanTaskParameters setGetSharePermissions(boolean getSharePermissions) {
        this.getSharePermissions = getSharePermissions;
        return this;
    }

    public boolean isIgnoreAccessErrors() {
        return ignoreAccessErrors;
    }

    public ScanTaskParameters setIgnoreAccessErrors(boolean ignoreAccessErrors) {
        this.ignoreAccessErrors = ignoreAccessErrors;
        return this;
    }
}
