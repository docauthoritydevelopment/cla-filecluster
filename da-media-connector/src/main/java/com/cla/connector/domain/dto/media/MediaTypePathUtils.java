package com.cla.connector.domain.dto.media;

public interface MediaTypePathUtils {
	String toNormalizedFolderPath(String rawPath);
	String toNormalizedFilePath(String rawPath);
}
