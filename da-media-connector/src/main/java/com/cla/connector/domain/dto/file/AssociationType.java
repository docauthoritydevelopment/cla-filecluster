package com.cla.connector.domain.dto.file;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 1/22/2019
 */
public enum AssociationType {
    FILE(0),
    FOLDER(1),
    GROUP(2)
    ;

    private static final Map<Integer, AssociationType> intMapping = Maps.newHashMap();
    private final int value;

    AssociationType(int value) {
        this.value = value;
    }

    static {
        for (final AssociationType t : AssociationType.values()) {
            intMapping.put(t.value, t);
        }
    }

    public int toInt() {
        return value;
    }

    public static AssociationType valueOf(final int value) {
        final AssociationType mt = AssociationType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
