package com.cla.connector.domain.dto.media;

import com.cla.connector.utils.FileNamingUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 01/05/2016.
 */
public enum MediaType implements MediaTypePathUtils{
    FILE_SHARE(0, true, "\\", false, true){
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			if (!(rawPath.startsWith("/") || rawPath.startsWith(".") || rawPath.startsWith("\\") || rawPath.contains(":"))) {
				rawPath = getPathSeparator() + rawPath;
			}
			return FileNamingUtils.normalizeFileSharePath(rawPath);
		}

		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}

	},
	BOX(1, false, "/", true, true){
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			return FileNamingUtils.convertPathToUniversalString(rawPath);
		}
		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}

	},
	SHARE_POINT(2, false, "/", true, true){
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			return FileNamingUtils.convertPathToUniversalString(rawPath);
		}
		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}
	},
	GOOGLE(3, true, "/", true, true){
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			return FileNamingUtils.convertPathToUniversalString(rawPath);
		}
		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}
	},
	ONE_DRIVE(4, false, "/", true, false){
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			String path = FileNamingUtils.convertPathToUniversalString(rawPath);
			path = StringUtils.removeStart(path, "/");
			String localPath = StringUtils.substringAfter(path, "/");
			String user = StringUtils.substringBefore(path, "/");
			user = user.replaceAll("@", "_").replaceAll("\\.", "_");
			return user + (user.isEmpty() ? "" : "/") + localPath;
		}

		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}
	},
	EXCHANGE_365(5, false, "/", true, false) {
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			return FileNamingUtils.convertPathToUniversalString(rawPath);
		}
		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}
	},
	MIP(6, false, "/", false, false) {
		@Override
		public String toNormalizedFolderPath(String rawPath) {
			return FileNamingUtils.convertPathToUniversalString(rawPath);
		}
		@Override
		public String toNormalizedFilePath(String rawPath) {
			return StringUtils.stripEnd(toNormalizedFolderPath(rawPath), getPathSeparator());
		}
	}
	;

	private int value;

	// since we now no longer save the full file path, but only after the root folder path
	private boolean pathAsMediaEntity;
	// this is only relevant where the media entity id is path and not a specific identifier per media type

	private boolean supportsNativeChangeLog;

	private static final Map<Integer, MediaType> intMapping = new HashMap<>();

	private String pathSeparator;

	private boolean hasPath;

    static {
        for (final MediaType v : MediaType.values()) {
            intMapping.put(v.value, v);
        }
    }

    MediaType(int value, boolean pathAsMediaEntity, String pathSeparator, boolean supportsNativeChangeLog, boolean hasPath) {
        this.value = value;
        this.pathAsMediaEntity = pathAsMediaEntity;
        this.pathSeparator = pathSeparator;
        this.supportsNativeChangeLog = supportsNativeChangeLog;
        this.hasPath = hasPath;
    }

    public boolean isPathAsMediaEntity() {
        return pathAsMediaEntity;
    }

    public int getValue() {
        return value;
    }

    public static MediaType valueOf(final int value){
            final MediaType mt = MediaType.intMapping.get(value);
            if (mt == null) {
                throw new RuntimeException("Invalid value: " + value);
            }
            return mt;
        }

    public String getPathSeparator() {
        return pathSeparator;
    }

    public boolean isSupportsNativeChangeLog() {
        return supportsNativeChangeLog;
    }

	public boolean hasPath() {
		return hasPath;
	}
}
