package com.cla.connector.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Strings;

import java.util.List;

public class ScanSharePermissions implements PhaseMessagePayload {

    private Long rootFolderId;
    private List<String> sharePermissions;
    private String errorText;

    public ScanSharePermissions() {
    }

    public static ScanSharePermissions create() {
        return new ScanSharePermissions();
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public ScanSharePermissions setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public List<String> getSharePermissions() {
        return sharePermissions;
    }

    public ScanSharePermissions setSharePermissions(List<String> sharePermissions) {
        this.sharePermissions = sharePermissions;
        return this;
    }

    public String getErrorText() {
        return errorText;
    }

    public ScanSharePermissions setErrorText(String errorText) {
        this.errorText = errorText;
        return this;
    }

    @Override
    public ProcessingErrorType calculateProcessErrorType() {
        if (!Strings.isNullOrEmpty(this.errorText)) {
            return  ProcessingErrorType.ERR_UNKNOWN;
        }
        return ProcessingErrorType.ERR_NONE;
    }

    @Override
    public String toString() {
        return "ScanSharePermissions{" +
                "rootFolderId=" + rootFolderId +
                ", sharePermissions=" + sharePermissions +
                ", errorText='" + errorText + '\'' +
                '}';
    }
}
