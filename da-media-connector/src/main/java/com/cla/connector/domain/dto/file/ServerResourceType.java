package com.cla.connector.domain.dto.file;

/**
 * Created by uri on 14/04/2016.
 */
public enum ServerResourceType {
    DRIVE,SERVER,FOLDER,SHARE,FILE,SITE,LIBRARY
}
