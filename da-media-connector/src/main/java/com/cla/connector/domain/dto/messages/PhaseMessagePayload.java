package com.cla.connector.domain.dto.messages;


import com.cla.connector.domain.dto.error.ProcessingErrorType;

public interface PhaseMessagePayload {

    ProcessingErrorType calculateProcessErrorType();
}
