package com.cla.connector.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * Created by vladi on 2/20/2017.
 */
public abstract class ProcessingError {

    public enum ErrorSeverity{
        CRITICAL,   // errors that caused the process to stop
        WARNING,    // errors that partially prevented information acquisition
        INFO        // informational remarks
    }

    private ProcessingErrorType errorType = ProcessingErrorType.ERR_NONE;
    private ErrorSeverity severity;
    protected Long timestamp;
    protected String text;
    protected String exceptionText;
    protected String exceptionType;

    public ErrorSeverity getSeverity() {
        return severity;
    }

    public void setSeverity(ErrorSeverity severity) {
        this.severity = severity;
    }

    public Long getTimeStamp() {
        return timestamp;
    }

    public ProcessingError setTimeStamp(Long timeStamp) {
        this.timestamp = timeStamp;
        return this;
    }

    @JsonIgnore
    public boolean isCritical(){
        return ErrorSeverity.CRITICAL.equals(this.severity);
    }

    public ProcessingErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ProcessingErrorType errorType) {
        this.errorType = errorType;
    }

    public ProcessingError setExceptionText(String exceptionText) {
        this.exceptionText = exceptionText;
        return this;
    }

    public String getExceptionText() {
        return exceptionText;
    }

    public String getText() {
        return text;
    }

    public ProcessingError setText(String text) {
        this.text = text;
        return this;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public ProcessingError setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
        return this;
    }

    @Override
    public String toString() {
        return "ProcessingError{" +
                "errorType=" + errorType +
                ", severity=" + severity +
                ", timestamp=" + timestamp +
                ", text='" + text + '\'' +
                ", exceptionText='" + exceptionText + '\'' +
                ", exceptionType='" + exceptionType + '\'' +
                '}';
    }
}
