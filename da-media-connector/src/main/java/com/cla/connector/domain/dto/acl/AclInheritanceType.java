package com.cla.connector.domain.dto.acl;

/**
 * Created by uri on 29/08/2016.
 */
public enum AclInheritanceType {
    NONE, FOLDER;

    public boolean equals(String name){
        return this.name().equals(name);
    }
}

