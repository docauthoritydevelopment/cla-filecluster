package com.cla.connector.domain.dto.file;

import com.google.common.collect.Maps;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum FileType {
	WORD(1, true, true, true)
	,EXCEL(2, true, true, true)
	,TEXT(10, false, true, false)
	,CSV(11, true, true, false)
	,PDF(20, true, true, true)
	,SCANNED_PDF(21, false, true, false)
	,PACKAGE(33, false, false, false)
	,PST(40, false, false, false)
	,MAIL(41, false, true, false)
	,OTHER(99, true, true, false)
	;
	
	private static final Map<Integer, FileType> intMapping = Maps.newHashMap();
	private final int value;
	private boolean isMeaningfulFile;
	private boolean hasContent;
	private boolean isDocument;

	FileType(final int value, boolean isMeaningfulFile, boolean hasContent, boolean isDocument) {
		this.value = value;
		this.isMeaningfulFile = isMeaningfulFile;
		this.hasContent = hasContent;
		this.isDocument = isDocument;
	}

	static {
		for (final FileType t : FileType.values()) {
			intMapping.put(t.value, t);
		}
	}

	public int toInt() {
		return value;
	}

	public boolean isMeaningfulFile() {
		return isMeaningfulFile;
	}

	public boolean hasContent() {
		return hasContent;
	}

	public boolean isDocument() {
		return isDocument;
	}

	public boolean isContainer() {
		return this.equals(PACKAGE) || this.equals(PST);
	}

	public boolean equals(int value){
		return this.toInt() == value;
	}

	public static FileType valueOf(final int value) {
		final FileType mt = FileType.intMapping.get(value);
		if (mt == null) {
			throw new RuntimeException("Invalid value: " + value);
		}
		return mt;
	}

	public static Set<Integer> getMeaningfulFileTypes() {
		Set<Integer> result = new HashSet<>();
		for (final FileType t : FileType.values()) {
			if (t.isMeaningfulFile()) {
				result.add(t.toInt());
			}
		}
		return result;
	}
}
