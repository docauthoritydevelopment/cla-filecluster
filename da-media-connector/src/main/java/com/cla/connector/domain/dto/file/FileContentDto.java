package com.cla.connector.domain.dto.file;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by uri on 30/11/2016.
 */
public class FileContentDto implements Serializable {

    private InputStream inputStream;
    private Long fileLength;
    private String fileName;
    private byte[] content;

    public FileContentDto(InputStream inputStream) {
        this.inputStream = inputStream;
        this.fileLength = null;
        this.content = null;
    }

    public FileContentDto(InputStream inputStream, Long length) {
        this.inputStream = inputStream;
        this.fileLength = length;
        this.content = null;
    }

    public FileContentDto(InputStream inputStream, Long length, String fileName) {
        this.inputStream = inputStream;
        this.fileLength = length;
        this.fileName = fileName;
        this.content = null;
    }

    public FileContentDto(byte[] content, String fileName) {
        this.content = Objects.requireNonNull(content);
        this.fileName = fileName;
        this.fileLength = (long) content.length;
        this.inputStream = new ByteArrayInputStream(content);
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getFileLength() {
        return fileLength;
    }

    public void setFileLength(Long fileLength) {
        this.fileLength = fileLength;
    }

    public boolean isContentAlreadyLoaded() {
        return content != null;
    }

    public byte[] getContent() {
        return content;
    }
}
