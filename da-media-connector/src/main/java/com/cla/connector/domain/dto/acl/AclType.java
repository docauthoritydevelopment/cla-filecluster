package com.cla.connector.domain.dto.acl;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by uri on 25/08/2016.
 */
public enum AclType {
    READ_TYPE(0, true, true),
    WRITE_TYPE(1, false, true),
    DENY_READ_TYPE(2, true, false),
    DENY_WRITE_TYPE(3, false, false);

    private static final Map<Integer, AclType> intMapping = Maps.newHashMap();
    private final int value;
    private final boolean isRead;
    private final boolean isAllow;

    AclType(final int value, boolean isRead, boolean isAllow) {
        this.value = value;
        this.isRead = isRead;
        this.isAllow = isAllow;
    }

    static {
        for (final AclType t : AclType.values()) {
            intMapping.put(t.value, t);
        }
    }

    public int toInt() {
        return value;
    }

    public boolean isRead() {
        return isRead;
    }

    public boolean isAllow() {
        return isAllow;
    }

    public static AclType valueOf(final int value) {
        final AclType mt = AclType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
