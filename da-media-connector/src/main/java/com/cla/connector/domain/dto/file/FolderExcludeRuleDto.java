package com.cla.connector.domain.dto.file;

import java.io.Serializable;

/**
 * Created by uri on 12/04/2016.
 */
public class FolderExcludeRuleDto implements Serializable {

    Long id;

    String matchString;

    String operator;

    Long rootFolderId;

    private boolean disabled;

    public FolderExcludeRuleDto() {
    }

    public FolderExcludeRuleDto(String matchString, String operator, Long rootFolderId, boolean disabled) {
        this.matchString = matchString;
        this.operator = operator;
        this.rootFolderId = rootFolderId;
        this.disabled = disabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatchString() {
        return matchString;
    }

    public void setMatchString(String matchString) {
        this.matchString = matchString;
    }


    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public void setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public String toString() {
        return "FolderExcludeRuleDto{" +
                "id=" + id +
                ", matchString='" + matchString + '\'' +
                ", operator='" + operator + '\'' +
                ", rootFolderId=" + rootFolderId +
                ", disabled=" + disabled +
                '}';
    }
}
