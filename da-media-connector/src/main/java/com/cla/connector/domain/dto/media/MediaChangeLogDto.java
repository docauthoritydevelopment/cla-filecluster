package com.cla.connector.domain.dto.media;

import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * Created by uri on 02/11/2016.
 */
public class MediaChangeLogDto extends ClaFilePropertiesDto {

    private DiffType eventType;
    private String changeLogPosition;

    public MediaChangeLogDto() {
        super("", 0L);
    }

	public MediaChangeLogDto(String itemId, DiffType diff, ClaFilePropertiesDto filePropertiesDto) {
		this(itemId, diff);
		copyProperties(filePropertiesDto, this);
	}

	public static MediaChangeLogDto create() {
        return new MediaChangeLogDto();
    }

    public MediaChangeLogDto(String mediaItemId, DiffType eventType) {
        this();
        setMediaItemId(mediaItemId);
        this.eventType = eventType;
    }

    public DiffType getEventType() {
        return eventType;
    }

    public void setEventType(DiffType eventType) {
        this.eventType = eventType;
    }

    public String getChangeLogPosition() {
        return changeLogPosition;
    }

    public void setChangeLogPosition(String changeLogPosition) {
        this.changeLogPosition = changeLogPosition;
    }

    @JsonIgnore
    @Override
    public boolean isChangeLogSupported() {
        return true;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("MediaChangeLogDto{");
        sb.append(super.toString()).append(", ");
        sb.append("eventType=").append(eventType);
        sb.append(", changeLogPosition='").append(changeLogPosition).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
