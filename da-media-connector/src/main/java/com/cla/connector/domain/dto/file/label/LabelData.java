package com.cla.connector.domain.dto.file.label;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@RequiredArgsConstructor
public class LabelData {
	private final Map<String,String> rawLabelData;
	private final NativeLabel nativeLabel;
	@Setter
	private LabelDto daLabel;

}
