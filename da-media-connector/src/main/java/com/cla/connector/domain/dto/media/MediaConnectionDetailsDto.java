package com.cla.connector.domain.dto.media;

import com.cla.connector.domain.dto.JsonSerislizableVisible;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * DTO for media connections (Share Point, file system etc.)
 * Created by uri on 08/08/2016.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.MINIMAL_CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
public class MediaConnectionDetailsDto implements JsonSerislizableVisible {

    private Long id;
    private String name;
    private String username;
    protected MediaType mediaType;
    private String url;
    private String connectionParametersJson;
    private long timestamp;
    private boolean pstCacheEnabled;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getConnectionParametersJson() {
        return connectionParametersJson;
    }

    public void setConnectionParametersJson(String connectionParametersJson) {
        this.connectionParametersJson = connectionParametersJson;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isPstCacheEnabled() {
        return pstCacheEnabled;
    }

    public void setPstCacheEnabled(boolean pstCacheEnabled) {
        this.pstCacheEnabled = pstCacheEnabled;
    }

    @Override
    public String toString() {
        return "MediaConnectionDetailsDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
