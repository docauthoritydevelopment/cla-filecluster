package com.cla.connector.domain.dto.file;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;

import java.util.List;

/**
 * Created by uri on 15/11/2016.
 */
public class ClaFilePropertiesPageDto {

    List<ClaFilePropertiesDto> items;

    String nextPageIdentifier;

    public ClaFilePropertiesPageDto() {
    }

    public ClaFilePropertiesPageDto(List<ClaFilePropertiesDto> claFilePropertiesDtos, String nextPageIdentifier) {
        this.items = claFilePropertiesDtos;
        this.nextPageIdentifier = nextPageIdentifier;
    }

    public List<ClaFilePropertiesDto> getItems() {
        return items;
    }

    public void setItems(List<ClaFilePropertiesDto> items) {
        this.items = items;
    }

    public String getNextPageIdentifier() {
        return nextPageIdentifier;
    }

    public void setNextPageIdentifier(String nextPageIdentifier) {
        this.nextPageIdentifier = nextPageIdentifier;
    }
}
