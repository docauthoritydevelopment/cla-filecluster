package com.cla.connector.domain.dto.file;

import java.io.Serializable;

/**
 * Created by uri on 14/04/2016.
 */
public class ServerResourceDto implements Serializable {

    String fullName;

    String name;

    Boolean accessible;

    Boolean hasChildren;

    ServerResourceType type;

    String id;

    public ServerResourceDto() {
    }

    public ServerResourceDto(String fullName, String name) {
        this.fullName =fullName;
        this.name = name;
        this.id = fullName;
    }

	public ServerResourceDto(String fullName, String name, ServerResourceType type, String id) {
		this.fullName = fullName;
		this.name = name;
		this.type = type;
		this.id = id;
	}

	public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAccessible() {
        return accessible;
    }

    public void setAccessible(Boolean accessible) {
        this.accessible = accessible;
    }

    public Boolean getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public ServerResourceType getType() {
        return type;
    }

    public void setType(ServerResourceType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "ServerResourceDto{" +
                "fullName='" + fullName + '\'' +
                ", name='" + name + '\'' +
                ", accessible=" + accessible +
                ", hasChildren=" + hasChildren +
                ", type=" + type +
                '}';
    }
}
