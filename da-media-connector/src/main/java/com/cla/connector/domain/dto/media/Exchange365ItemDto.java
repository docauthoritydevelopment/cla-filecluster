package com.cla.connector.domain.dto.media;

import com.cla.connector.domain.dto.file.DiffType;

import java.util.List;

/**
 * Created By Itai Marko
 */
public class Exchange365ItemDto extends MediaChangeLogDto implements MailPropertiesDto {

    private String mailboxUpn;

    private String parentFolderId;

    private List<AttachmentInfo> attachmentInfos;

    private EmailAddress sender;

    private List<EmailAddress> recipients;

    private Long sentDate;

    private String emailSubject;

    @SuppressWarnings("unused") // needed for JSON serialization
    public Exchange365ItemDto() {
    }

    private Exchange365ItemDto(String mediaItemId, DiffType eventType) {
        super(mediaItemId, eventType);
    }

    public static Exchange365ItemDto create(String mediaItemId, DiffType eventType) {
        return new Exchange365ItemDto(mediaItemId, eventType);
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public void setMailboxUpn(String mailboxUpn) {
        this.mailboxUpn = mailboxUpn;
    }

    public void setParentFolderId(String parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public String getParentFolderId() {
        return parentFolderId;
    }

    public void setAttachmentInfos(List<AttachmentInfo> attachmentInfos) {
        this.attachmentInfos = attachmentInfos;
    }

    public List<AttachmentInfo> getAttachmentInfos() {
        return attachmentInfos;
    }

    public EmailAddress getSender() {
        return sender;
    }

    public void setSender(EmailAddress sender) {
        this.sender = sender;
    }

    public List<EmailAddress> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<EmailAddress> recipients) {
        this.recipients = recipients;
    }

    public Long getSentDate() {
        return sentDate;
    }

    public void setSentDate(Long sentDate) {// TODO Itai: add receivedDate (on which we filter from)?
        this.sentDate = sentDate;
    }

    @Override
    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }
    @Override
    public String toString() {
        return "Exchange365ItemDto{" +
                "mailboxUpn='" + mailboxUpn + '\'' +
                ", eventType=" + getEventType() +
                ", fileName=" + getFileName() +
                ", mediaItemId=" + getMediaItemId() +
                ", parentFolderId='" + parentFolderId + '\'' +
                ", attachmentInfos=" + attachmentInfos +
                ", sender=" + sender +
                ", recipients=" + recipients +
                ", sentDate=" + sentDate +
                '}';
    }
}
