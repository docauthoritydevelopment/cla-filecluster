package com.cla.connector.domain.dto.file;

public enum AssociableEntityType {
    DEPARTMENT("department"),
    DOC_TYPE("docType"),
    TAG("tag"),
    BUSINESS_LIST_ITEM("bizListItem"),
    DATA_ROLE("dataRole")
    ;

    private String entityName;

    AssociableEntityType(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getMsgCodeKey() {
        return "associable-entity-name." + entityName;
    }
}
