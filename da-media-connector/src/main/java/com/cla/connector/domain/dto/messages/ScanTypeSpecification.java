package com.cla.connector.domain.dto.messages;

import com.cla.connector.domain.dto.file.FileType;

import java.util.List;
import java.util.Set;

public class ScanTypeSpecification {

    private List<FileType> typesToScan;
    private Set<String> extensionsToScan;
    private Set<String> extensionsToIgnore; //TODO: shouldn't we just subtract them from extensionsToScan?

    public ScanTypeSpecification() {

    }

    public ScanTypeSpecification(List<FileType> typesToScan, Set<String> extensionsToScan, Set<String> extensionsToIgnore) {
        this.typesToScan = typesToScan;
        this.extensionsToScan = extensionsToScan;
        this.extensionsToIgnore = extensionsToIgnore;
    }

    public ScanTypeSpecification(ScanTypeSpecification other) {
        this(other.typesToScan, other.extensionsToScan, other.extensionsToIgnore);
    }


    public List<FileType> getTypesToScan() {
        return typesToScan;
    }

    public void setTypesToScan(List<FileType> typesToScan) {
        this.typesToScan = typesToScan;
    }

    public Set<String> getExtensionsToScan() {
        return extensionsToScan;
    }


    public void setExtensionsToScan(Set<String> extensionsToScan) {
        this.extensionsToScan = extensionsToScan;
    }

    public Set<String> getExtensionsToIgnore() {
        return extensionsToIgnore;
    }

    public void setExtensionsToIgnore(Set<String> extensionsToIgnore) {
        this.extensionsToIgnore = extensionsToIgnore;
    }

    @Override
    public String toString() {
        return "ScanTypeSpecification{" +
                "typesToScan=" + typesToScan +
                ", extensionsToScan=" + extensionsToScan +
                ", extensionsToIgnore=" + extensionsToIgnore +
                '}';
    }
}
