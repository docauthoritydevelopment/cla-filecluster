package com.cla.connector.domain.dto.file;

public enum DiffType {
    SIMILAR,
    NEW,
    ACL_UPDATED,
    CONTENT_UPDATED,
    DELETED,
    UNDELETED,
    RENAMED,
    FORCE_NO_DIFF,       // Either first scan or full scan was requested
    CREATED_UPDATED      // Either created or updated (AppServer should determine which)
}