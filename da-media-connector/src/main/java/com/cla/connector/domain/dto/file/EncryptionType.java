package com.cla.connector.domain.dto.file;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 10/10/2018
 */
public enum EncryptionType {
    PASSWORD_PROTECTED(1)
    ;

    private static final Map<Integer, EncryptionType> intMapping = Maps.newHashMap();
    private final int value;

    EncryptionType(final int value) {
        this.value = value;
    }

    static {
        for (final EncryptionType t : EncryptionType.values()) {
            intMapping.put(t.value, t);
        }
    }

    public int toInt() {
        return value;
    }

    public static EncryptionType valueOf(final int value) {
        final EncryptionType mt = EncryptionType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }
}
