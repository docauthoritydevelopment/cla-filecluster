package com.cla.connector.domain.dto.file.label;

import com.google.common.collect.Maps;

import java.util.Map;

public enum LabelingVendor {
    MicrosoftInformationProtection("MIP", true),
    DocAuthority("DA", true),
    ;

    private static final Map<String, LabelingVendor> nameMapping = Maps.newHashMap();

    private String name;
    private boolean isExternal;

    LabelingVendor(String name, boolean isExternal){
        this.name = name;
        this.isExternal = isExternal;
    }

    public boolean isExternal(){
        return isExternal;
    }

    public String getName() {
        return name;
    }

    static {
        for (final LabelingVendor t : LabelingVendor.values()) {
            nameMapping.put(t.name, t);
        }
    }

    public static LabelingVendor getByName(final String name) {
        return LabelingVendor.nameMapping.get(name);
    }
}
