package com.cla.connector.domain.dto.media.mail;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.util.*;

import static com.cla.connector.domain.dto.media.mail.MailItemType.*;
import static com.cla.connector.utils.TextUtils.joinToStrings;
import static com.cla.connector.utils.TextUtils.requireNonNullNeitherEmpty;

/**
 * Created By Itai Marko
 */
public final class MsGraphItemLocator {

    public static final String separator = "|"; // avoids slashes and base 64 chars

    private final String mailboxUpn;

    private final List<MailItemId> itemIDs;

    private MsGraphItemLocator(String mailboxUpn, MailItemId... itemIDs) {
        this.mailboxUpn = requireNonNullNeitherEmpty(mailboxUpn);
        if (Objects.requireNonNull(itemIDs).length < 1) {
            throw new IllegalArgumentException("Empty item IDs list");
        }
        this.itemIDs = Collections.unmodifiableList(Arrays.asList(itemIDs));
    }

    public String getMailboxUpn() {
        return mailboxUpn;
    }

    public MailItemId getItemId() {
        return itemIDs.get(itemIDs.size()-1);
    }

    public MailItemId getParentItemId() {
        int itemIdsCount = itemIDs.size();
        if (itemIdsCount < 2) {
            throw new RuntimeException("MsGraphItemLocator.getParentUid() invoked on instance with only " + itemIdsCount
                    + " itemIds. MsGraphItemLocator: " + toString());
        }

        return itemIDs.get(itemIdsCount-2);
    }

    public MailItemType getType() {
        return getItemId().getType();
    }


    @Override
    public String toString() {
        return mailboxUpn + separator + joinToStrings(separator, itemIDs);
    }

    public static MsGraphItemLocator fromString(String graphIdStr) {
        graphIdStr = requireNonNullNeitherEmpty(graphIdStr);
        List<String> tokens = Lists.newLinkedList();
        Splitter.on(separator).split(graphIdStr).forEach(tokens::add);

        if (tokens.size() < 2) { // TODO Itai: validate < 3 for attachment
            throw new IllegalArgumentException("Malformed MsGraphItemLocator: " + graphIdStr);
        }
        String mailboxUpn = requireNonNullNeitherEmpty(tokens.get(0));
        MailItemId[] itemIDs = tokens.stream()
                .skip(1) // note starting iteration from the second one
                .map(MailItemId::fromString)
                .toArray(MailItemId[]::new);
        return new MsGraphItemLocator(mailboxUpn, itemIDs);
    }

    public static MsGraphItemLocator forFolder(String mailboxUpn, String folderUid) {
        return forSingleIdEntity(mailboxUpn, folderUid, FOLDER);
    }

    public static MsGraphItemLocator forEmailMessage(String mailboxUpn, String messageUid) {
        return forSingleIdEntity(mailboxUpn, messageUid, EMAIL);
    }

    public static MsGraphItemLocator forEmailFileAttachment(
            String mailboxUpn, String fileAttachmentUid, String containingEmailUid) {

        MailItemId containingEmailItemId = new MailItemId(containingEmailUid, EMAIL);
        MailItemId fileAttachmentItemId = new MailItemId(fileAttachmentUid, ATTACHMENT);
        return new MsGraphItemLocator(mailboxUpn, containingEmailItemId, fileAttachmentItemId);
    }

    private static MsGraphItemLocator forSingleIdEntity(String mailboxUpn, String entityUid, MailItemType entityType) {
        MailItemId mailItemId = new MailItemId(entityUid, entityType);
        return new MsGraphItemLocator(mailboxUpn, mailItemId);
    }

    public static final class MailItemId {

        public static final String separator = "#"; // also avoids slashes and base 64 chars

        private final String uid;

        private final MailItemType type;

        private MailItemId(String uid, MailItemType type) {
            this.uid = requireNonNullNeitherEmpty(uid);
            this.type = Objects.requireNonNull(type);
        }

        public String getUid() {
            return uid;
        }

        public MailItemType getType() {
            return type;
        }

        @Override
        public String toString() {
            return type.shortName + separator + uid;
        }

        private static MailItemId fromString(String itemIdStr) {
            List<String> tokens = Lists.newArrayList();
            Splitter.on(separator).split(requireNonNullNeitherEmpty(itemIdStr)).forEach(tokens::add);

            if (tokens.size() != 2) {
                throw new IllegalArgumentException(
                        "Malformed MailItemId: " + itemIdStr + ", tokens: " + Joiner.on(",").join(tokens));
            }
            String typeShortName = tokens.get(0);
            Optional<MailItemType> mailItemTypeOpt =
                    MailItemType.forShortName(
                            requireNonNullNeitherEmpty(typeShortName));
            MailItemType type = mailItemTypeOpt
                    .orElseThrow(
                            () -> new IllegalArgumentException(
                                    "Invalid shortName: " + typeShortName + " in " + itemIdStr + ", tokens: "
                                            + Joiner.on(",").join(tokens)));
            String uid = requireNonNullNeitherEmpty(tokens.get(1));
            return new MailItemId(uid, type);
        }
    }
}
