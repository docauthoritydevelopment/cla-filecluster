package com.cla.connector.domain.dto.media;

/**
 * Holds metadata on email's file attachments.
 *
 * Created By Itai Marko
 */
public final class AttachmentInfo {

    private String id;

    private String filename;

    private Integer size;

    private String mimeType;

    private String itemSubject;

    @SuppressWarnings("unused") // needed
    public AttachmentInfo() {
    }

    public AttachmentInfo(String id, String filename, Integer size, String mimeType, String itemSubject) {
        this.id = id;
        this.filename = filename;
        this.size = size;
        this.mimeType = mimeType;
        this.itemSubject = itemSubject;
    }

    public String getId() {
        return id;
    }

    public String getFilename() {
        return filename;
    }

    public Integer getSize() {
        return size;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getItemSubject() {
        return itemSubject;
    }
}
