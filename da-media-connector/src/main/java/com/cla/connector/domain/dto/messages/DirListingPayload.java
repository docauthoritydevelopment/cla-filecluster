package com.cla.connector.domain.dto.messages;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Message payload containing list of files in folder
 */
public class DirListingPayload implements PhaseMessagePayload{

    private int page = 0;
    private boolean lastPage = true;
    private String folderPath;
    private List<String> directoryList = Lists.newLinkedList();

    public static DirListingPayload create(){
        return new DirListingPayload();
    }

    public String getFolderPath() {
        return folderPath;
    }

    public DirListingPayload setFolderPath(String folderPath) {
        this.folderPath = folderPath;
        return this;
    }

    public List<String> getDirectoryList() {
        return directoryList;
    }

    public DirListingPayload setDirectoryList(List<String> directoryList) {
        this.directoryList = directoryList;
        return this;
    }

    public DirListingPayload addPath(String path) {
        this.directoryList.add(path);
        return this;
    }

    public int size(){
        return directoryList.size();
    }

    public int getPage() {
        return page;
    }

    public DirListingPayload setPage(int page) {
        this.page = page;
        return this;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public DirListingPayload setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
        return this;
    }

    public DirListingPayload reset(){
        directoryList = Lists.newLinkedList();
        return this;
    }

    @Override
    public ProcessingErrorType calculateProcessErrorType() {
        return null;
    }
}
