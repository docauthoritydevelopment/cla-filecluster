package com.cla.connector.domain.dto.file;

import com.cla.connector.domain.dto.acl.AclAware;
import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.label.LabelData;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.*;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ClaFilePropertiesDto implements PhaseMessagePayload, AclAware, Serializable {

    private Long claFileId;
    private Long fileSize = -1L;

    private static final String DELIM = ",";
    private static final String PREFIX = "";
    private static final String SUFFIX = ";";

    private Collection<String> allowedReadPrincipalsNames = Sets.newHashSet();
    private Collection<String> deniedReadPrincipalsNames = Sets.newHashSet();
    private Collection<String> allowedWritePrincipalsNames = Sets.newHashSet();
    private Collection<String> deniedWritePrincipalsNames = Sets.newHashSet();
    private Map<String,String> metadata = Maps.newHashMap();

    private String contentSignature;            // signature of the whole file (or of the first part of it, if the size exceeds threshold)
    private String userContentSignature;        // signature of the user content, excluding metadata
    private Long contentMetadataId;  //in case file content already exists

    private long modTimeMilli;
    private long accessTimeMilli;
    private long creationTimeMilli;
    private String ownerName;
    private String aclSignature;

    private String mediaItemId;

    private String fileName;
    private String title;
    private AclInheritanceType aclInheritanceType;
    private boolean folder;
    private boolean offline;

    private String mime;
    private FileType type;

    private boolean inaccessible = false;

    private List<ScanErrorDto> scanErrorDtos = Lists.newLinkedList();

    private transient Path filePath = null;

    private transient List<LabelData> labelData;

    private transient List<String> externalMetadataToRem;

    private transient List<Long> daLabelIdsToRem;

    private transient List<String> daMetadataToRem;

    private transient List<String> daMetadataTypeToRem;

    public ClaFilePropertiesDto(String fileName, Long length) {
        this.fileName = fileName;
        if (length != null) {
            this.fileSize = length;
        }
    }

    protected ClaFilePropertiesDto() {
    }

    public static ClaFilePropertiesDto create() {
        return new ClaFilePropertiesDto();
    }

    public ClaFilePropertiesDto setPath(Path path) {
        this.filePath = path;
        String fullPath = path.isAbsolute() ? path.toString() : path.toAbsolutePath().normalize().toString();
        this.mediaItemId = fullPath;
        this.fileName = fullPath;
        return this;
    }

    public Long getClaFileId() {
        return claFileId;
    }

    public void setClaFileId(Long claFileId) {
        this.claFileId = claFileId;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getContentSignature() {
        return contentSignature;
    }

    public void setContentSignature(String contentSignature) {
        this.contentSignature = contentSignature;
    }

    public String getUserContentSignature() {
        return userContentSignature;
    }

    public void setUserContentSignature(String userContentSignature) {
        this.userContentSignature = userContentSignature;
    }

    @Override
    public Collection<String> getAclReadAllowed() {
        return allowedReadPrincipalsNames;
    }

    public void addAllowedReadPrincipalName(String principalsName) {
        allowedReadPrincipalsNames.add(principalsName);
    }

	public void addAllowedReadPrincipalNames(Collection<String> principalsNames) {
		allowedReadPrincipalsNames.addAll(principalsNames);
	}

    public Collection<String> getAclReadDenied() {
        return deniedReadPrincipalsNames;
    }

    public void addDeniedReadPrincipalName(String principalsName) {
        this.deniedReadPrincipalsNames.add(principalsName);
    }

    public void addDeniedReadPrincipalNames(Collection<String> principalsNames) {
        this.deniedReadPrincipalsNames.addAll(principalsNames);
    }

    @Override
    public Collection<String> getAclWriteAllowed() {
        return allowedWritePrincipalsNames;
    }

    public void addAllowedWritePrincipalName(String principalsName) {
        allowedWritePrincipalsNames.add(principalsName);
    }
    public void addAllowedWritePrincipalNames(Collection<String> principalsNames) {
        allowedWritePrincipalsNames.addAll(principalsNames);
    }

    @Override
    public Collection<String> getAclWriteDenied() {
        return deniedWritePrincipalsNames;
    }

    public void addDeniedWritePrincipalName(String principalsName) {
        deniedWritePrincipalsNames.add(principalsName);
    }
    public void addDeniedWritePrincipalNames(Collection<String> principalsNames) {
        deniedWritePrincipalsNames.addAll(principalsNames);
    }


    public void setModTimeMilli(long modTimeMilli) {
        this.modTimeMilli = modTimeMilli;
    }

    public long getModTimeMilli() {
        return modTimeMilli;
    }

    public long getAccessTimeMilli() {
        return accessTimeMilli;
    }

    public void setAccessTimeMilli(long accessTimeMilli) {
        this.accessTimeMilli = accessTimeMilli;
    }

    public void setCreationTimeMilli(long creationTimeMilli) {
        this.creationTimeMilli = creationTimeMilli;
    }

    public long getCreationTimeMilli() {
        return creationTimeMilli;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getAclSignature() {
        return aclSignature;
    }

    public void setAclSignature(String aclSignature) {
        this.aclSignature = aclSignature;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

	public Map<String, String> getMetadata() {
		return metadata;
	}

	public ClaFilePropertiesDto setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
		return this;
	}

	@Override
    public String toString() {
        return "ClaFilePropertiesDto{" + "claFileId=" + claFileId +
                ", fileSize=" + fileSize +
                ", allowedReadPrincipalsNames=" + allowedReadPrincipalsNames +
                ", deniedReadPrincipalsNames=" + deniedReadPrincipalsNames +
                ", allowedWritePrincipalsNames=" + allowedWritePrincipalsNames +
                ", deniedWritePrincipalsNames=" + deniedWritePrincipalsNames +
                ", contentSignature='" + contentSignature + '\'' +
                ", userContentSignature='" + userContentSignature + '\'' +
                ", contentMetadataId=" + contentMetadataId +
                ", modTimeMilli=" + modTimeMilli +
                ", accessTimeMilli=" + accessTimeMilli +
                ", creationTimeMilli=" + creationTimeMilli +
                ", ownerName='" + ownerName + '\'' +
                ", aclSignature='" + aclSignature + '\'' +
                ", mediaItemId='" + mediaItemId + '\'' +
                ", fileName='" + fileName + '\'' +
                ", aclInheritanceType=" + aclInheritanceType +
                ", folder=" + folder +
                ", mime='" + mime + '\'' +
                ", type=" + type +
                ", inaccessible=" + inaccessible +
                ", scanErrorDtos=" + scanErrorDtos +
                '}';
    }

    public String getMediaItemId() {
        return mediaItemId;
    }

    public void setMediaItemId(String mediaItemId) {
        this.mediaItemId = mediaItemId;
    }

    public AclInheritanceType getAclInheritanceType() {
        return aclInheritanceType;
    }

    public void setAclInheritanceType(AclInheritanceType aclInheritanceType) {
        this.aclInheritanceType = aclInheritanceType;
    }

    /**
     * Set's whether this instance represents a media item that should be considered as a folder.
     * <p>
     * By default {@link #isFolder()} and {@link #isFile()} are mutually exclusive, so this method sets isFolder()
     * to return the given argument and isFile() to return the opposite of the given argument.
     * There's no need to call both setFolder(boolean) and {@link #setFile(boolean)}
     * <p>
     * Extending classes may override this behaviour and implement isFile() and isFolder() to be non-mutually-exclusive.
     */
    public ClaFilePropertiesDto setFolder(boolean folder) {
        this.folder = folder;
        return this;
    }

    /**
     * Indicates whether this instance should be considered a folder.
     * <p>
     * By default, isFolder() and {@link #isFile()} are mutually exclusive.
     * <p>
     * However, extending classes may override this behaviour and implement isFile()
     * and isFolder() to be non-mutually-exclusive.
     */
    public boolean isFolder() {
        return folder;
    }

    /**
     * Set's whether this instance represents a media item that should be considered as a file.
     * <p>
     * By default {@link #isFile()} and {@link #isFolder()} are mutually exclusive, so this method sets isFile()
     * to return the given argument and isFolder() to return the opposite of the given argument.
     * There's no need to call both setFile(boolean) and {@link #setFolder(boolean)}
     * <p>
     * Extending classes may override this behaviour and implement isFile() and isFolder() to be non-mutually-exclusive.
     */
    public void setFile(boolean file) {
        setFolder(!file);
    }

    /**
     * Indicates whether this instance should be considered a file.
     * <p>
     * By default, isFile() and {@link #isFolder()} are mutually exclusive.
     * <p>
     * However, extending classes may override this behaviour and implement isFile()
     * and isFolder() to be non-mutually-exclusive.
     */
    public boolean isFile() {
        return !isFolder();
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public boolean isInaccessible() {
        return inaccessible;
    }

    public void setInaccessible(boolean inaccessible) {
        this.inaccessible = inaccessible;
    }

    public List<ScanErrorDto> getErrors() {
        return scanErrorDtos;
    }

    public ClaFilePropertiesDto addError(ScanErrorDto error) {
        scanErrorDtos.add(error);
        return this;
    }

    public boolean hasErrors() {
        return !scanErrorDtos.isEmpty();
    }

    public boolean hasCriticalErrors() {
        return !getCriticalErrors().isEmpty();
    }

    public List<ScanErrorDto> getCriticalErrors() {
        return scanErrorDtos.stream().filter(ScanErrorDto::isCritical).collect(Collectors.toList());
    }

    public List<ScanErrorDto> getNonCriticalErrors() {
        return scanErrorDtos.stream().filter(err -> !err.isCritical()).collect(Collectors.toList());
    }

    /**
     * @return a 28 chars string that represents a SHA1 of the Acl signature
     */
    public String calculateAclSignature() {
        MessageDigest sha1Digest = DigestUtils.getSha1Digest();
        updateDigestWithPrincipals(sha1Digest, allowedReadPrincipalsNames);
        updateDigestWithPrincipals(sha1Digest, allowedWritePrincipalsNames);
        updateDigestWithPrincipals(sha1Digest, deniedReadPrincipalsNames);
        updateDigestWithPrincipals(sha1Digest, deniedWritePrincipalsNames);
        aclSignature = Base64.encodeBase64String(sha1Digest.digest());
        return aclSignature;
    }

    private void updateDigestWithPrincipals(MessageDigest sha1Digest, Collection<String> principals) {
        sha1Digest.update((principals.stream().sorted().collect(Collectors.joining(DELIM, PREFIX, SUFFIX)))
                .getBytes());
    }

    @Override
    public ProcessingErrorType calculateProcessErrorType() {
        return hasErrors() ? ProcessingErrorType.ERR_UNKNOWN : ProcessingErrorType.ERR_NONE;
    }

    public Long getContentMetadataId() {
        return contentMetadataId;
    }

    public void setContentMetadataId(Long contentMetadataId) {
        this.contentMetadataId = contentMetadataId;
    }

    @JsonIgnore
    public Path getFilePath() {
        if (filePath == null) {
            if (mediaItemId != null) {
                filePath = Paths.get(mediaItemId);
            } else if (fileName != null) {
                filePath = Paths.get(fileName);
            }
        }
        return filePath;
    }

    public void setFilePath(Path filePath) {
        this.filePath = filePath;
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    public boolean isOffline() {
        return offline;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<LabelData> getLabelData() {
        return labelData;
    }

    public void setLabelData(List<LabelData> labelData) {
        this.labelData = labelData;
    }

    public List<String> getExternalMetadataToRem() {
        return externalMetadataToRem;
    }

    public void setExternalMetadataToRem(List<String> externalMetadataToRem) {
        this.externalMetadataToRem = externalMetadataToRem;
    }

    public List<Long> getDaLabelIdsToRem() {
        return daLabelIdsToRem;
    }

    public void setDaLabelIdsToRem(List<Long> daLabelIdsToRem) {
        this.daLabelIdsToRem = daLabelIdsToRem;
    }

    public List<String> getDaMetadataToRem() {
        return daMetadataToRem;
    }

    public void setDaMetadataToRem(List<String> daMetadataToRem) {
        this.daMetadataToRem = daMetadataToRem;
    }

    public List<String> getDaMetadataTypeToRem() {
        return daMetadataTypeToRem;
    }

    public void setDaMetadataTypeToRem(List<String> daMetadataTypeToRem) {
        this.daMetadataTypeToRem = daMetadataTypeToRem;
    }

    @JsonIgnore
    public boolean isChangeLogSupported() {
        return false;
    }

    public static void copyProperties(ClaFilePropertiesDto source, ClaFilePropertiesDto target){
		target.claFileId = source.claFileId;
		target.fileSize = source.fileSize;

		target.allowedReadPrincipalsNames = new HashSet(source.allowedReadPrincipalsNames);
		target.deniedReadPrincipalsNames = new HashSet(source.deniedReadPrincipalsNames);
		target.allowedWritePrincipalsNames = new HashSet(source.allowedWritePrincipalsNames);
		target.deniedWritePrincipalsNames = new HashSet(source.deniedWritePrincipalsNames);

		target.contentSignature = source.contentSignature;
		target.userContentSignature = source.userContentSignature;
		target.contentMetadataId = source.contentMetadataId;

		target.modTimeMilli = source.modTimeMilli;
		target.accessTimeMilli = source.accessTimeMilli;
		target.creationTimeMilli = source.creationTimeMilli;
		target.ownerName = source.ownerName;
		target.aclSignature = source.aclSignature;

		target.mediaItemId = source.mediaItemId;

		target.fileName = source.fileName;
		target.aclInheritanceType = source.aclInheritanceType;
		target.folder = source.folder;
		target.offline = source.offline;

		target.mime = source.mime;
		target.type = source.type;

		target.inaccessible = source.inaccessible;

		target.scanErrorDtos = new ArrayList<>(source.scanErrorDtos);

		target.filePath = source.filePath;
	}
}
