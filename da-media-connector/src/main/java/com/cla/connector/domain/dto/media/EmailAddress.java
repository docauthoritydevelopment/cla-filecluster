package com.cla.connector.domain.dto.media;

/**
 * Created By Itai Marko
 */
public class EmailAddress {
    private String name;
    private String address;

    public EmailAddress() { // added for serialization
    }

    public EmailAddress(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "EmailAddress{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
