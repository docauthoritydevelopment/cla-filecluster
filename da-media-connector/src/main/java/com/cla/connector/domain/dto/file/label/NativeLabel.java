package com.cla.connector.domain.dto.file.label;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class NativeLabel {
	private String nativeId;
	private String name;
	private LocalDateTime applicationDate;
	private LabelingVendor vendor;
}
