package com.cla.connector.domain.dto.file;

import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 1/29/2019
 */
public enum ItemType {
    MESSAGE(0, true, "_email_"),    // Should be included in defaultFileTypeCategories.csv as email.
    ATTACHMENT(1, true, null)
    ;

    private static final Map<Integer, ItemType> intMapping = Maps.newHashMap();
    private final int value;
    private final boolean emailRelated;
    private final String specialExtension;

    ItemType(final int value, final boolean emailRelated, String specialExtension) {
        this.value = value;
        this.emailRelated = emailRelated;
        this.specialExtension = specialExtension;
    }

    static {
        for (final ItemType t : ItemType.values()) {
            intMapping.put(t.value, t);
        }
    }

    public boolean isEmailRelated() {
        return emailRelated;
    }

    public int toInt() {
        return value;
    }

    public String getSpecialExtension() {
        return specialExtension;
    }

    public static ItemType valueOf(final int value) {
        final ItemType mt = ItemType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }

    public static List<ItemType> getAllEmailRelated() {
        List<ItemType> emailRelated = new ArrayList<>();
        for (final ItemType t : ItemType.values()) {
            if (t.emailRelated) {
                emailRelated.add(t);
            }
        }
        return emailRelated;
    }
}
