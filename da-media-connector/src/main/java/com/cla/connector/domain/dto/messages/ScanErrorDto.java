package com.cla.connector.domain.dto.messages;

/**
 *
 * Created by uri on 07/03/2016.
 */
public class ScanErrorDto extends ProcessingError{

    private Long rootFolderId;
    private String path;
    private Long runId;

    public static ScanErrorDto create(ErrorSeverity severity){
        ScanErrorDto scanErrorDto = new ScanErrorDto();
        scanErrorDto.setSeverity(severity);
        return scanErrorDto;
    }

    public ScanErrorDto setRootFolderId(Long rootFolderId) {
        this.rootFolderId = rootFolderId;
        return this;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }


    public ScanErrorDto setPath(String path) {
        this.path = path;
        return this;
    }

    public String getPath() {
        return path;
    }



    public Long getRunId() {
        return runId;
    }

    public ScanErrorDto setRunId(Long runId) {
        this.runId = runId;
        return this;
    }

    @Override
    public String toString() {
        return "ScanErrorDto{" +
                ", rootFolderId=" + rootFolderId +
                ", exceptionText='" + exceptionText + '\'' +
                ", path='" + path + '\'' +
                ", timeStamp=" + timestamp +
                ", runId=" + runId +
                '}';
    }
}
