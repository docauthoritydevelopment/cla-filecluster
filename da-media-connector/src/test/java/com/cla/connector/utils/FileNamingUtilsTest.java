package com.cla.connector.utils;


import com.cla.connector.utils.pckg.PackagePathResolver;
import com.cla.connector.utils.pckg.ZipPackagePathResolver;
import org.apache.commons.io.FilenameUtils;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

public class FileNamingUtilsTest {

    @Test
    public void testGetFileNameFullPath() {

        assertTrue(FileNamingUtils.getFileNameFullPath(null, null).equals(""));
        assertTrue(FileNamingUtils.getFileNameFullPath(null, "").equals(""));
        assertTrue(FileNamingUtils.getFileNameFullPath("", "").equals(""));
        assertTrue(FileNamingUtils.getFileNameFullPath("", null).equals(""));

        String file = "D:\\dev\\cla-filecluster\\appserver\\cqlsh.bat";
        String base = "cqlsh.bat";
        assertTrue(FileNamingUtils.getFileNameFullPath(file, base).equals("D:\\dev\\cla-filecluster\\appserver\\"));

        assertTrue(FileNamingUtils.getFileNameFullPath(file, "").equals(file));
        assertTrue(FileNamingUtils.getFileNameFullPath(file, null).equals(file));
    }

    @Test
    public void testGetFilenameBaseName() {
        assertTrue(FileNamingUtils.getFilenameBaseName(null).equals(""));
        assertTrue(FileNamingUtils.getFilenameBaseName("").equals(""));
        assertTrue(FileNamingUtils.getFilenameBaseName("D:\\dev\\cla-filecluster\\appserver\\cqlsh.bat").equals("cqlsh.bat"));
    }

    @Test
    public void testGetFilenameExtension() {
        assertTrue(FileNamingUtils.getFilenameExtension(null).equals(""));
        assertTrue(FileNamingUtils.getFilenameExtension("").equals(""));
        assertTrue(FileNamingUtils.getFilenameExtension("cqlsh.bat").equals("bat"));
        assertTrue(FileNamingUtils.getFilenameExtension("D:\\dev\\cla-filecluster\\appserver\\cqlsh.bat").equals("bat"));
    }

    @Test
    public void testGetFilenamePath() {
        assertTrue(FileNamingUtils.getFilenamePath(null).equals(""));
        assertTrue(FileNamingUtils.getFilenamePath("").equals(""));
        assertTrue(FileNamingUtils.getFilenamePath("cqlsh.bat").equals(""));
        assertTrue(FileNamingUtils.getFilenamePath("D:\\dev\\cla-filecluster\\appserver\\cqlsh.bat").equals("D:\\dev\\cla-filecluster\\appserver"));
    }

    @Test
    public void testGetFilenamePrefix() {
        assertTrue(FileNamingUtils.getFilenamePath(null).equals(""));
        assertTrue(FileNamingUtils.getFilenamePath("").equals(""));
        assertTrue(FileNamingUtils.getFilenamePath("cqlsh.bat").equals(""));
        assertTrue(FileNamingUtils.getFilenamePath("D:\\dev\\cla-filecluster\\appserver\\cqlsh.bat").equals("D:\\dev\\cla-filecluster\\appserver"));
    }

    @Test
    public void testGetPathPart() {
        assertTrue(FileNamingUtils.getPathPart(null, 1).equals(""));
        assertTrue(FileNamingUtils.getPathPart("", 1).equals(""));
        assertTrue(FileNamingUtils.getPathPart("cqlsh.bat", 1).equals(""));
        assertTrue(FileNamingUtils.getPathPart("D:\\dev\\cla-filecluster\\appserver\\cqlsh.bat", 2).equals("appserver"));
        assertTrue(FileNamingUtils.getPathPart("/cla-filecluster/appserver/acqlsh.bat", 1).equals("appserver"));
    }

    @Test
    public void testNormalizeFileSharePath() {
        assertTrue(FileNamingUtils.normalizeFileSharePath("").contains("/da-media-connector/"));
        assertTrue(FileNamingUtils.normalizeFileSharePath("cqlsh.bat").contains("/da-media-connector/cqlsh.bat"));
    }

    @Test
    public void testConvertPathToUniversalString() {
        assertTrue(FileNamingUtils.convertPathToUniversalString("\\dev\\cla-filecluster\\appserver\\").equals("/dev/cla-filecluster/appserver/"));
    }

    @Test
    public void testConvertPathToUniversal() {
        Path path = Paths.get("\\dev\\cla-filecluster\\appserver\\");
        assertTrue(FileNamingUtils.convertPathToUniversalString(path).equals("/dev/cla-filecluster/appserver/"));
    }

    @Test
    public void testStandartPackageFileNameResolving() {
        String rootFolder = "\\dev\\cla-filecluster\\appserver\\";
        String zipFileName = rootFolder + "a" + ZipPackagePathResolver.ZIP_IDENTIFIER;
        Assert.assertEquals(zipFileName, FileNamingUtils.getPackageRootPath(zipFileName));
        Assert.assertEquals(zipFileName,
                FileNamingUtils
                        .getPackageRootPath(zipFileName + PackagePathResolver.PACKAGE_SEPARATOR + "\\a.doc")
        );
    }

    @Test
    public void testUpperCasePackageFileNameResolving() {
        String rootFolder = "\\dev\\cla-filecluster\\appserver\\";
        String zipFileName = rootFolder + "a.ZIP";
        Assert.assertEquals(zipFileName, FileNamingUtils.getPackageRootPath(zipFileName));
        Assert.assertEquals(zipFileName,
                FileNamingUtils
                        .getPackageRootPath(zipFileName + PackagePathResolver.PACKAGE_SEPARATOR + "\\a.doc")
        );
    }

    @Test
    public void testMixedPackageFileNameResolving() {
        String rootFolder = "\\dev\\cla-filecluster\\appserver\\";
        String zipFileName = rootFolder + "a.ZiP";
        Assert.assertEquals(zipFileName, FileNamingUtils.getPackageRootPath(zipFileName));
        Assert.assertEquals(zipFileName,
                FileNamingUtils
                        .getPackageRootPath(zipFileName + PackagePathResolver.PACKAGE_SEPARATOR + "\\a.doc")
        );
    }

    @Test
    public void testParent() {
        Assert.assertEquals("", FileNamingUtils.getParentDir(""));
        Assert.assertEquals("", FileNamingUtils.getParentDir(null));
        Assert.assertEquals("", FileNamingUtils.getParentDir("\\dev"));
        Assert.assertEquals("\\dev\\", FileNamingUtils.getParentDir("\\dev\\cla-filecluster"));
        Assert.assertEquals("\\dev\\cla-filecluster\\", FileNamingUtils.getParentDir("\\dev\\cla-filecluster\\appserver\\"));
        Assert.assertEquals("\\dev\\cla-filecluster\\", FileNamingUtils.getParentDir("\\dev\\cla-filecluster\\appserver"));
        Assert.assertEquals("\\dev\\cla-filecluster\\", FileNamingUtils.getParentDir("\\dev\\cla-filecluster\\appserver.txt"));
    }

    @Test
    public void testParentNameOnly() {
        Assert.assertEquals("filecluster", FileNamingUtils.getParentNameOnly("\\dev\\filecluster\\appserver.txt"));
        Assert.assertEquals("d", FileNamingUtils.getParentNameOnly("\\a\\b\\c\\d\\appserver.txt"));
        Assert.assertEquals("", FileNamingUtils.getParentNameOnly("appserver.txt"));
    }
}
