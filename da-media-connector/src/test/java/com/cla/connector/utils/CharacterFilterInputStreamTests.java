package com.cla.connector.utils;

import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by oren on 6/10/2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class CharacterFilterInputStreamTests {

    @Test
    @SuppressWarnings("unchecked")
    public void test_nonFiltered() throws IOException {
        String str = "uthor>PDá„€çŸ¶Â¸&#x14;ã• çŸºá‰ŠçŸ¶p&#x2;</d:O";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream fis = new CharacterFilterInputStream(is, Collections.EMPTY_LIST)) {
            assertEquals(str, IOUtils.toString(fis, StandardCharsets.UTF_8));
        }
    }

    @Test
    public void test_OneFiltered() throws IOException {
        String str = "qwerty";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, Lists.newArrayList("e"))) {

            String filtered = IOUtils.toString(streamFilter, StandardCharsets.UTF_8);
            assertEquals(str.replaceAll("e", ""), filtered);
        }
    }

    @Test
    public void test_TwoNearByFiltered() throws IOException {
        String str = "qwerty";
        List<String> charToFilter = Lists.newArrayList("e","r");
        String charToFilterRegex = charToFilter.stream()
                .collect(Collectors.joining("|"));
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charToFilter)) {

            String filtered = IOUtils.toString(streamFilter, StandardCharsets.UTF_8);
            assertEquals(str.replaceAll(charToFilterRegex, ""), filtered);
        }
    }

    @Test
    public void test_TwoSeparatedFiltered() throws IOException {
        List<String> charToFilter = Lists.newArrayList("e","y");
        String charToFilterRegex = charToFilter.stream()
                .collect(Collectors.joining("|"));
        String str = "qwerty";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charToFilter)) {

            String filtered = IOUtils.toString(streamFilter, StandardCharsets.UTF_8);
            assertEquals(str.replaceAll(charToFilterRegex, ""), filtered);
        }
    }

    @Test
    public void test_UTF8Filtered() throws IOException {
        String str = "qᄀ矶we矶ᄀ矶rty";
        List<String> charToFilter = Lists.newArrayList("e","矶");
        String charToFilterRegex = charToFilter.stream()
                .collect(Collectors.joining("|"));
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charToFilter)) {

            String filtered = IOUtils.toString(streamFilter, StandardCharsets.UTF_8);
            assertEquals(str.replaceAll(charToFilterRegex, ""), filtered);
        }
    }

    @Test
    public void test_UTF8TwoNearByFiltered() throws IOException {
        String str = "qᄀ矶we矶ᄀ矶rty";
        List<String> charToFilter = Lists.newArrayList("e", "矶", "ᄀ");
        String charToFilterRegex = charToFilter.stream()
                .collect(Collectors.joining("|"));
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charToFilter)) {

            String filtered = IOUtils.toString(streamFilter, StandardCharsets.UTF_8);
            assertEquals(str.replaceAll(charToFilterRegex, ""), filtered);
        }
    }

    @Test
    public void test_UTF8CodedCharFiltered() throws IOException {
        String str = "uthor>PDá„€çŸ¶Â¸&#x14;&#x14;ã• çŸºá‰ŠçŸ¶p&#x2;</d:O";
        List<String> charToFilter = Lists.newArrayList("e", "&#x2;", "&#x14;", "Ÿ");
        String charToFilterRegex = charToFilter.stream()
                .collect(Collectors.joining("|"));
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream streamFilter = new CharacterFilterInputStream(is, charToFilter)) {

            String filtered = IOUtils.toString(streamFilter, StandardCharsets.UTF_8);
            assertEquals(str.replaceAll(charToFilterRegex, ""), filtered);
        }
    }

    @Test(expected = IOException.class)
    public void test_AllFiltered() throws IOException {
        String str = "qwerty";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterStream = new CharacterFilterInputStream(is, Lists.newArrayList("q","w","e","r","t","y"))) {

            String filtered = IOUtils.toString(filterStream, StandardCharsets.UTF_8);
            assertTrue(filtered.equals(""));
        }
    }

    @Test
    public void test_MultiFiltered() throws IOException {
        String str = "qwerty";
        List<String> filterList = Lists.newArrayList("q","e","t","y");
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterReader = new CharacterFilterInputStream(is, filterList)) {

            String filtered = IOUtils.toString(filterReader, StandardCharsets.UTF_8);
            assertEquals("wr", filtered);
            int expectedLength = str.length() - filterList.size();
            assertEquals(filtered.length(), expectedLength);
        }
    }

    @Test
    public void test_restrictedCharsFiltered() throws IOException {
        List<String> filter = Lists.newArrayList("&#20;");
        String str = "PDá„€çŸ¶Â¸&#x14;ã• çŸºá‰ŠçŸ¶p&#x2;";
        String expected = "PDá„€çŸ¶Â¸ã• çŸºá‰ŠçŸ¶p&#x2;";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterReader = new CharacterFilterInputStream(is, filter)) {

            String filtered = IOUtils.toString(filterReader, StandardCharsets.UTF_8);
            assertEquals(expected, filtered);
        }
    }

    @Test
    public void test_prefixCharsFiltered() throws IOException {
        String str = "qwerty";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterReader = new CharacterFilterInputStream(is, Lists.newArrayList("q", "w", "e"))) {

            String filtered = IOUtils.toString(filterReader, StandardCharsets.UTF_8);
            assertEquals("rty", filtered);
        }
    }

    @Test
    public void test_suffixCharsFiltered() throws IOException {
        String str = "qwerty";
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterReader = new CharacterFilterInputStream(is, Lists.newArrayList("r", "t", "y"))) {

            String filtered = IOUtils.toString(filterReader, StandardCharsets.UTF_8);
            assertEquals("qwe", filtered);
        }
    }

    @Test
    public void test_longStreamSplitCharsFiltered() throws IOException {
        String str = "PDá„€çŸ¶Â¸&#x14;ã• çŸºá‰ŠçŸ¶p&#x2;";
        String expected = "PDá„€çŸ¶Â¸ã• çŸºá‰ŠçŸ¶p";
        String filtered;
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterStream = new CharacterFilterInputStream(is, Lists.newArrayList("&#x14;", "&#x2;"))) {
            filtered = IOUtils.toString(filterStream, StandardCharsets.UTF_8);
        }
        assertEquals(expected, filtered);
    }

    @Test
    public void test_CharSpansToNextChink() throws IOException {
        String str = "1234567890";
        String expected = "1237890";
        String filtered;
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterStream = new CharacterFilterInputStream(is, Lists.newArrayList("456"))) {
            filtered = IOUtils.toString(filterStream, StandardCharsets.UTF_8);
        }
        assertEquals(expected, filtered);
    }

    @Test
    public void test_decimalCharCodeVsHexCharCodeFiltered() throws IOException {
        String str = "PDá„€çŸ¶Â¸&#x14;ã• çŸºá‰ŠçŸ¶p&#x2;";
        String expected = "PDá„€çŸ¶Â¸ã• çŸºá‰ŠçŸ¶p";
        String filtered;
        try (InputStream is = new ByteArrayInputStream(str.getBytes(StandardCharsets.UTF_8));
             CharacterFilterInputStream filterStream = new CharacterFilterInputStream(is, Lists.newArrayList("&#20;", "&#2;"))) {
            filtered = IOUtils.toString(filterStream, StandardCharsets.UTF_8);
        }
        assertEquals(expected, filtered);
    }
}