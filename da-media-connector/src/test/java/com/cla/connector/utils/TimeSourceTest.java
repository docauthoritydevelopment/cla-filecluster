package com.cla.connector.utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TimeSourceTest {

    @Test
    public void testRandomTimeBounds(){
        TimeSource timeSource = (new TimeSource()).withSeed(1234L);
        long now = timeSource.currentTimeMillis();
        timeSource.simulateRandomDateTime(now, 10, TimeUnit.DAYS);
        long upToTenDays = timeSource.currentTimeMillis();
        long gap = upToTenDays - now;
        Assert.assertTrue(gap <= TimeUnit.DAYS.toMillis(10));

        int randomYear = timeSource.randomYear(1976, 2018);
        Assert.assertTrue(randomYear >= 1976);
        Assert.assertTrue(randomYear <= 2018);

        timeSource = TimeSource.create().withSeed(89L);
        timeSource.setSimulatedDateTime(2000, 1, 1, 0, 0, 0);
        long creationTime = timeSource
                .simulateRandomDateTime(timeSource.currentTimeMillis(), 18*365, TimeUnit.DAYS).currentTimeMillis();
        long modTime = timeSource.simulateRandomDateTime(creationTime, 10, TimeUnit.DAYS).currentTimeMillis();
        long accessTime = timeSource.simulateRandomDateTime(creationTime, 14, TimeUnit.DAYS).currentTimeMillis();

        Assert.assertTrue(creationTime >= 946684800000L);
        Assert.assertTrue(creationTime <= 1530521178000L);

        Assert.assertTrue(modTime - creationTime <= TimeUnit.DAYS.toMillis(10));
        Assert.assertTrue(accessTime - modTime <= TimeUnit.DAYS.toMillis(14));
    }

    @Test
    public void testConsistency(){
        TimeSource timeSource = (new TimeSource()).withSeed(1234L);
        int randomYear = timeSource.randomYear(1976, 2018);
        timeSource = timeSource.withSeed(1234L);
        int newRandomYear = timeSource.randomYear(1976, 2018);
        Assert.assertEquals(newRandomYear, randomYear);

        timeSource = timeSource.withSeed(4321L);
        randomYear = timeSource.randomYear(1976, 2018);
        timeSource = timeSource.withSeed(4321L);
        newRandomYear = timeSource.randomYear(1976, 2018);
        Assert.assertEquals(newRandomYear, randomYear);
    }

}
