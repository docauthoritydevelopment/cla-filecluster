package com.cla.connector.utils;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import org.junit.Test;

import java.util.*;
import java.util.function.Predicate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for FileTypeUtils
 * Created by yael on 11/16/2017.
 */
public class FileTypeUtilsTest {

    @Test
    public void changeFileTypesSupported() throws Exception {
        FileTypeUtils.addExtension("xlt", FileType.EXCEL);
        FileTypeUtils.addExtension("xlsm", FileType.EXCEL);
        FileTypeUtils.addExtension("xltx", FileType.EXCEL);
        assertTrue(FileTypeUtils.getScanFileTypesMap().get("xlt") == FileType.EXCEL);
        assertTrue(FileTypeUtils.getScanFileTypesMap().get("xlsm") == FileType.EXCEL);
    }

    @Test
    public void testGetFileType() {
        assertTrue(FileTypeUtils.getFileType("src/test/resources/files/excel/EnterpriseFiles/103820OPMHoursSurveyJan01.xls").equals(FileType.EXCEL));
        assertTrue(FileTypeUtils.getFileType("103820OPMHoursSurveyJan01.xls").equals(FileType.EXCEL));
        assertTrue(FileTypeUtils.getFileType("103820OPMHoursSurveyJan01.doc").equals(FileType.WORD));
        assertTrue(FileTypeUtils.getFileType("103820OPMHoursSurveyJan01.pdf").equals(FileType.PDF));
        assertTrue(FileTypeUtils.getFileType("103820OPMHoursSurveyJan01.jpg").equals(FileType.OTHER));
    }

    @Test
    public void testGetTypesToScan() {
        // empty list
        Map<String, FileType> concreteScanFileTypesMap = new HashMap<>();
        List<FileType> typesToScan = new ArrayList<>();
        FileTypeUtils.getFileTypesToScan(concreteScanFileTypesMap, typesToScan);
        assertTrue(concreteScanFileTypesMap.isEmpty());
        assertTrue(typesToScan.isEmpty());

        // 1 supported file type
        typesToScan.add(FileType.PDF);
        FileTypeUtils.getFileTypesToScan(concreteScanFileTypesMap, typesToScan);
        assertTrue(concreteScanFileTypesMap.size() == 1);
        assertTrue(concreteScanFileTypesMap.containsValue(FileType.PDF));
        assertTrue(typesToScan.size() == 1);

        // 2 file types, 1 not supported
        typesToScan.add(FileType.TEXT);
        FileTypeUtils.getFileTypesToScan(concreteScanFileTypesMap, typesToScan);
        assertTrue(concreteScanFileTypesMap.size() == 1);
        assertTrue(concreteScanFileTypesMap.containsValue(FileType.PDF));
        assertTrue(typesToScan.size() == 2);

        // after adding type to be supported
        FileTypeUtils.addExtension("txt", FileType.TEXT);
        FileTypeUtils.getFileTypesToScan(concreteScanFileTypesMap, typesToScan);
        assertTrue(concreteScanFileTypesMap.size() == 2);
        assertTrue(concreteScanFileTypesMap.containsValue(FileType.TEXT));
        assertTrue(typesToScan.size() == 2);
    }

    @Test
    public void testCreatePredicate() {
        List<FileType> typesToScan;
        Set<String> extsToScan;
        Set<String> extsToIgnore;

        Predicate<? super String> predicate = FileTypeUtils.createFileTypesPredicate(new ScanTypeSpecification(null, null, null));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.doc"));

        extsToIgnore = new HashSet<>();
        extsToIgnore.add("txt");
        predicate = FileTypeUtils.createFileTypesPredicate(new ScanTypeSpecification(null, null, extsToIgnore));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.doc"));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.txt"));

        typesToScan = new ArrayList<>();
        typesToScan.add(FileType.PDF);
        extsToScan = new HashSet<>();
        extsToScan.add("pdf");
        predicate = FileTypeUtils.createFileTypesPredicate(new ScanTypeSpecification(typesToScan, extsToScan, extsToIgnore));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.doc"));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.txt"));
        assertTrue(predicate.test("103820OPMHoursSurveyJan01.pdf"));

        extsToScan.add("txt");
        predicate = FileTypeUtils.createFileTypesPredicate(new ScanTypeSpecification(typesToScan, extsToScan, extsToIgnore));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.doc"));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.txt"));
        assertTrue(predicate.test("103820OPMHoursSurveyJan01.pdf"));
    }

    @Test
    public void testCreatePredicateWithContainers() {
        List<FileType> typesToScan;
        Set<String> extsToScan;
        Set<String> extsToIgnore;

        typesToScan = new ArrayList<>();
        typesToScan.add(FileType.PDF);
        extsToScan = new HashSet<>();
        extsToIgnore = new HashSet<>();
        extsToScan.add("pdf");
        Predicate<? super String> predicate = FileTypeUtils.createFileTypesPredicate(new ScanTypeSpecification(typesToScan, extsToScan, extsToIgnore));

        assertFalse(predicate.test("103820OPMHoursSurveyJan01.doc"));
        assertFalse(predicate.test("103820OPMHoursSurveyJan01.txt"));
        assertTrue(predicate.test("103820OPMHoursSurveyJan01.pdf"));
        assertFalse(predicate.test("\\gg32\\AndreCalo.zip*\\Andrea Calo\\103820OPMHoursSurveyJan01.pdf"));
        assertFalse(predicate.test("\\andrew_lewis_000_1_1.pst*\\Top of Personal Folders\\lewis-a\\103820OPMHoursSurveyJan01.pdf"));

        extsToScan.add(FileTypeUtils.ZIP_FILE_EXT);
        assertTrue(predicate.test("\\gg32\\AndreCalo.zip*\\Andrea Calo\\103820OPMHoursSurveyJan01.pdf"));
        assertFalse(predicate.test("\\andrew_lewis_000_1_1.pst*\\Top of Personal Folders\\lewis-a\\103820OPMHoursSurveyJan01.pdf"));

        extsToScan.add(FileTypeUtils.PST_FILE_EXT);
        assertTrue(predicate.test("\\andrew_lewis_000_1_1.pst*\\Top of Personal Folders\\lewis-a\\103820OPMHoursSurveyJan01.pdf"));
    }
}
