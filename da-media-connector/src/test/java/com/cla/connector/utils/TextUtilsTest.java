package com.cla.connector.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextUtilsTest {

    @Test
    public void removeUnprintableCharactersTest() {
        String text = "the cat" + Character.toString((char)11) + "is in" + Character.toString((char)11) + "the car"; // vertical tab
        String result = TextUtils.removeUnprintableCharacters(text);
        assertEquals("the cat\nis in\nthe car", result);

        text = "the cat\ris in\rthe car";
        result = TextUtils.removeUnprintableCharacters(text);
        assertEquals("the cat\nis in\nthe car", result);


        text = "the cat" + Character.toString((char)14) + "is in" + Character.toString((char)14) + "the car"; // shift out
        result = TextUtils.removeUnprintableCharacters(text);
        assertEquals("the catis inthe car", result);

        text = "the cat" + "\u2028" + "is in" + "\u2028" + "the car"; // line separator
        result = TextUtils.removeUnprintableCharacters(text);
        assertEquals("the cat is in the car", result);

        text = "the cat" + "\u2029" + "is in" + "\u2029" + "the car"; // paragraph separator
        result = TextUtils.removeUnprintableCharacters(text);
        assertEquals("the cat is in the car", result);
    }
}