package com.cla.connector.utils;

import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.google.common.collect.Lists;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class MediaConnectorUtilsTest {

    @Test
    public void testIsFolderExcluded() {

        List<FolderExcludeRuleDto> nonEqualExcludedRules;
        List<Path> pathSkipList;
        List<String> dirnameList2Skip;

        String baseName = "cqlsh.bat";
        String pathStr = "cla-filecluster\\appserver\\cqlsh.bat";
        Path path = Paths.get(pathStr);

        // no limitations
        assertFalse(MediaConnectorUtils.isFolderExcluded(path, baseName, null, null, null, 1L));

        // with disabled rule
        nonEqualExcludedRules = new ArrayList<>();
        FolderExcludeRuleDto rule = new FolderExcludeRuleDto();
        rule.setDisabled(true);
        nonEqualExcludedRules.add(rule);
        assertFalse(MediaConnectorUtils.isFolderExcluded(path, baseName, nonEqualExcludedRules, null, null, 1L));

        // excluded with relevant rule
        rule = new FolderExcludeRuleDto();
        rule.setDisabled(false); rule.setOperator("contains"); rule.setMatchString("D:\\cla-filecluster\\appserver\\cqlsh.bat");
        nonEqualExcludedRules.add(rule);
        assertTrue(MediaConnectorUtils.isFolderExcluded(path, baseName, nonEqualExcludedRules, null, null, 1L));

        // with paths but not relevant
        pathSkipList = new ArrayList<>();
        Path p = Paths.get("D:\\cla-filecluster\\appserver\\cqlsh.bat");
        pathSkipList.add(p);
        assertFalse(MediaConnectorUtils.isFolderExcluded(path, baseName, null, pathSkipList, null, 1L));

        // excluded with relevant path
        pathSkipList.add(path);
        assertTrue(MediaConnectorUtils.isFolderExcluded(path, baseName, null, pathSkipList, null, 1L));

        // with skip list not relevant
        dirnameList2Skip = new ArrayList<>();
        dirnameList2Skip.add("D:\\cla-filecluster\\appserver\\cqlsh.bat");
        assertFalse(MediaConnectorUtils.isFolderExcluded(path, baseName, null, null, dirnameList2Skip, 1L));

        // excluded with relevant skip list
        dirnameList2Skip.add(baseName);
        assertTrue(MediaConnectorUtils.isFolderExcluded(path, baseName, null, null, dirnameList2Skip, 1L));
    }

    @Test
    public void testIsFolderExcludedHttpFileServer() {
        List<FolderExcludeRuleDto> equalExcludedRules = Lists.newArrayList();
        FolderExcludeRuleDto rule = new FolderExcludeRuleDto();
        rule.setDisabled(true);
        rule.setOperator("eq");
        rule.setMatchString("https://docauthority.sharepoint.com/shared documents/some_folder");
        equalExcludedRules.add(rule);

        String baseName = "some_folder";
        String pathStr = "https://docauthority.sharepoint.com/shared documents/some_folder";

        assertFalse(MediaConnectorUtils.isFolderExcluded(pathStr, baseName, null, null,  1L)); // disabled
        rule.setDisabled(false);
        assertTrue(MediaConnectorUtils.isFolderExcluded(pathStr, baseName, equalExcludedRules, null,  1L)); //

        pathStr = "https://docauthority.sharepoint.com/shared documents/some_other_folder";
        assertFalse(MediaConnectorUtils.isFolderExcluded(pathStr, baseName, equalExcludedRules, null,  1L));

        rule.setMatchString(rule.getMatchString() + "1");
        assertFalse(MediaConnectorUtils.isFolderExcluded(pathStr, baseName, equalExcludedRules, null,  1L));
    }
}
