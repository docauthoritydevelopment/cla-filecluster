#usage script [computername/ip] [share name]

[cmdletbinding()]

Param (
    [parameter(ValueFromPipeline=$True)]
    [string[]]$Computer,
    [parameter(ValueFromPipeline=$True)]
    [string[]]$share
)

$acl = @()

$objShareSec = Get-WMIObject -Class Win32_LogicalShareSecuritySetting -Filter "name='$share'"  -ComputerName $computer
try {
    $SD = $objShareSec.GetSecurityDescriptor().Descriptor

    foreach($ace in $SD.DACL){
        $UserName = $ace.Trustee.Name
        If ($ace.Trustee.Domain -ne $Null) {$UserName = "$($ace.Trustee.Domain)\$UserName"}
        If ($ace.Trustee.Name -eq $Null) {$UserName = $ace.Trustee.SIDString }
        $rule = New-Object Security.AccessControl.FileSystemAccessRule($UserName, $ace.AccessMask, $ace.AceType)
        $res = "{'user':'"
        $res += $UserName
        $res += "', 'mask':"
        $res += $ace.AccessMask
        $res += ", 'type':"
        $res += $ace.AceType
        $res += ", 'perm':'"
        $res += $rule.FileSystemRights
        $res += "'}"
        $ACL += $res
    } #end foreach ACE

} # end try
catch
{ Write-Error "Unable to obtain permissions for $share" }
$ACL