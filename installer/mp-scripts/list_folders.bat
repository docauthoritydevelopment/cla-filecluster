@ECHO OFF
set DIRNAME=%~dp0
IF "%OS%"=="Windows_NT" setlocal enabledelayedexpansion enableextensions

set CSV=0
if [%1]==[-csv] (
	set CSV=1
	shift
)
if [%1]==[] GOTO help
IF %CSV%==1 goto csv

FOR /F "tokens=*" %%G IN ( 'DIR /b %1' ) DO ( IF exist "%1\%%G\" echo %1\%%G )
REM FOR /F "tokens=1,2" %%G IN ( 'net view %1' ) DO ( IF "X%%H" == "XDisk" IF exist "%1\%%G\" echo %1\%%G )
goto end

:csv
echo PATH,NICK NAME,SCANNED FILE TYPES,CUSTOMER DATA CENTER,RESCAN,STORE LOCATION,STORE PURPOSE,STORE SECURITY,DESCRIPTION,MEDIA TYPE,MEDIA CONNECTION NAME,SCHEDULE GROUP NAME,EXTRACT BUSINESS LISTS
FOR /F "tokens=*" %%G IN ( 'DIR /b %1' ) DO ( IF exist "%1\%%G\" echo %1\%%G,%%G,WORD;EXCEL;PDF,default,FALSE,,,,%%G,FILE_SHARE,,default, )
goto end

:help
echo Usage:
echo %0 [-csv] [path]
echo.
echo     Lists top level folders under the given path.
echo        -csv   - Output is in CSV format that can be saved to a file and be used to import as 'RootFolders'.

:end
endlocal
exit /b 0
