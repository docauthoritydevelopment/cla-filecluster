@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  List SMB shares for a remote server (Windows)
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
if '%1' == '-h' goto usage
if '%1' == '-help' goto usage
if '%1' == '--help' goto usage

powershell.exe -ExecutionPolicy Unrestricted "& '%DIRNAME%\da_list_shares.ps1' %*"
goto finally

:usage
echo Usage:
echo    %0  options
echo.
echo    Options:
echo        -server [server-address]
echo        -csv [0 ^| 1]                      If 1, output will be in Root-Folders import CSV foramt, default: 0
echo        -sg [schedule-group-name]          Schedule group name for CSV, default: default
echo        -dc [data-center-name]             Data center name for CSV, default: default
echo.
echo Example:
echo.
echo    da_list_shares.bat -server 123.45.67.89 -csv 1 -sg 'my sg name' -dc 'my dc name'
goto finally

:finally
