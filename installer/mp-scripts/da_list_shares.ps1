﻿###
# List SMB shares for a remote server
# Written by Itay R. Sep, 2018
#
# Example:
#     powershell.exe -ExecutionPolicy Unrestricted "& '.\da_list_shares.ps1' -csv 1 -server 172.22.34.56"
#
###
param(
#output CSV format
    [bool]$csv = 0,
#Server address/name
    [string]$server,
#Schedule group value for CSV
    [string]$sg = 'default',
#Data center value for CSV
    [string]$dc = 'default'
)

$da_sg_name = $sg
$da_dc_name = $dc
$da_target = $server
$da_isCsv = $csv
if ($server.Substring(0,2) -eq '\\') {
    $da_target = $server.Remove(0,2)
}
## $shares = $output = Get-WmiObject -class Win32_Share -computer $target -Credential $creds -erroraction stop| Where-Object {(@('Remote Admin', 'Default share', 'Remote IPC', 'sysvol', 'printer drivers', 'Logon server share ' ) -notcontains $_.Description)}
$da_shares = Get-WmiObject -class Win32_Share -computer $da_target -erroraction stop| Where-Object {(@('Remote Admin', 'Default share', 'Remote IPC', 'sysvol', 'printer drivers', 'Logon server share ' ) -notcontains $_.Description)}
if ($da_isCsv) {
    "PATH,NICK NAME,SCANNED FILE TYPES,CUSTOMER DATA CENTER,RESCAN,STORE LOCATION,STORE PURPOSE,STORE SECURITY,DESCRIPTION,MEDIA TYPE,MEDIA CONNECTION NAME,SCHEDULE GROUP NAME,EXTRACT BUSINESS LISTS"
}
ForEach($da_share in $da_shares) {
    $da_share_path = "\\" + $da_target + "\" + $da_share.Name ;
    if ($da_isCsv) {
        "" + $da_share_path + "," + $da_share.Name + ",WORD;EXCEL;PDF," + $da_dc_name + ",FALSE,,,," + $da_share.Name + ",FILE_SHARE,," + $da_sg_name + ","
    } else {
        "$da_share_path"
    }
}
#Clean up variables
Remove-Variable da_*
