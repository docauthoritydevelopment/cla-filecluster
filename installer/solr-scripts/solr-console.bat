set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\solr-7.7.2
set SOLR_HOME=%DIRNAME%\solr
set SOLR_JAVA_MEM=-Xms4g -Xmx4g
REM set SOLR_PORT=8983
set "JRE_HOME=%~dp0%jre"

set JAVA_HOME=%JRE_HOME%

set CUSTOM_SOLR_DATA_DIR="%CLA_HOME%\data"

echo %CUSTOM_SOLR_DATA_DIR%
call "%CLA_HOME%\bin\solr.cmd" start -f -p 8983 -Dsolr.data.dir="%CUSTOM_SOLR_DATA_DIR%" %1 %2 %3
