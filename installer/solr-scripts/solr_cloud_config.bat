@ECHO off
IF "%OS%"=="Windows_NT" setlocal enabledelayedexpansion enableextensions

set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%\solr-7.7.2
set SOLR_CORES=%DIRNAME%\solr
set ZK_BIN=%DIRNAME%\zookeeper\bin
REM set SOLR_CLOUD_DIR=%DIRNAME%\solrCloud
set UPGRADE_TOOL=call %DIRNAME%\filecluster\filecluster-upgrade-schema.bat
set ZK_CLIENT_TIMEOUT=60000

REM Corelated with Solr service activation flags -DBI_node=1 -DSIM_node=1 (~ replaced with ! to work around CMD escaping challenges)
set BI_COLL_RULE="sysprop.BI_node:~0"
set SIM_COLL_RULE="sysprop.SIM_node:~0"

if [%1]==[createCollection] GOTO createCollection
if [%1]==[zk-create-collections] goto zk-create-collections
if [%1]==[zk-create-collections-if-needed] goto zk-create-collections-if-needed
if [%1]==[zk-create-collections-if-needed2] goto zk-create-collections-if-needed2
if [%1]==[zk-create-collection] GOTO zk-create-collection
if [%1]==[zk-create-collection-if-needed] GOTO zk-create-collection-if-needed
if [%1]==[configCore] GOTO ConfigCore
if [%1]==[configCores] GOTO ConfigCores
if [%1]==[zk-config-collection] GOTO zk-config-collection
if [%1]==[zk-config-collections] GOTO zk-config-collections
if [%1]==[createCollections] goto createCollections
if [%1]==[create-dist-aliases] GOTO create-dist-aliases
if [%1]==[list-nodes] GOTO list-nodes
if [%1]==[installer-create-collections] GOTO installer-create-collections
if [%1]==[installer-create-collections2] GOTO installer-create-collections2
REM if [%1]==[createCores] goto createCores
REM if [%1]==[createCore] goto createCore
if [%1]==[removeSolrRoot] goto removeSolrRoot
if [%1]==[makeRoot] goto MakeRoot
if [%1]==[ReloadCollections] goto ReloadCollections
if [%1]==[config-reload] goto configReloadCollections
if [%1]==[print-collection-status] goto printCollectionsStatus
if [%1]==[get-async-operation-status] goto getAsyncOperationStatus
if [%1]==[split-shard] goto splitShard
if [%1]==[split-and-move-shard] goto splitAndMoveShard
if [%1]==[replicate-collection] goto replicateCollection
if [%1]==[force-leader] goto forceLeader
if [%1]==[list-aliases] goto listAlias
if [%1]==[create-alias] goto createAlias
if [%1]==[delete-alias] goto deleteAlias
if [%1]==[move-replica] goto moveReplica
if [%1]==[align-shards] goto alignShards
if [%1]==[migrate-collection] goto migrateCollection
if [%1]==[migrate-collection-2017-10] goto migrateCollection201710
if [%1]==[backup] goto backup
if [%1]==[backup-all] goto backupAll
if [%1]==[restore] goto restore
if [%1]==[restore-all] goto restoreAll
if [%1]==[remove-task-id] goto removeTaskId
if [%1]==[remove-all-task-ids] goto removeAllTaskIds
if [%1]==[delete-down-replicas] goto deleteDownReplicas
if [%1]==[delete-legacy-replicas] goto deleteLegacyDownReplicas
if [%1]==[delete-fornax-migrated-replicas] goto deleteFornaxMigratedReplicas
if [%1]==[delete-fornax-migrated-collections] goto deleteFornaxMigratedCollections
echo No Valid Parameter given (%1)
goto help

:MakeRoot
IF '%2'=='' GOTO help
IF '%3'=='' GOTO help
REM call "%CLA_HOME%\bin\solr.cmd" zk mkroot %3 -z %2
set SOLR_ADDRESS=%2
%UPGRADE_TOOL% solr %SOLR_ADDRESS% zk-mkdirs %3
goto end

:createCollection
echo Create %2 on %3 shards
call "%CLA_HOME%\bin\solr.cmd" create_collection -c %2 -d %SOLR_CORES%\%2 -n %2 -shards %3
goto end

:zk-create-collection
set SOLR_ADDRESS=%2
echo Config collection %3 to zk %2
%UPGRADE_TOOL% solr %SOLR_ADDRESS% zk-upconfig %SOLR_CORES%\%3\conf %3
echo Create collection %3 with %4 shards, rule %5
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection %3 %3 %4 %5
goto end

:zk-create-collection-if-needed
set SOLR_ADDRESS=%2
echo Config collection %3 to zk %2
%UPGRADE_TOOL% solr %SOLR_ADDRESS% zk-upconfig %SOLR_CORES%\%3\conf %3
echo Create collection %3 with %4 shards, rule %5
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-collection-if-needed %3 %3 %4 %5
goto end

REM :createCore
REM echo Create %2 Core
REM call "%CLA_HOME%\bin\solr.cmd" create_core -c %2 -d %SOLR_CORES%\%2
REM EXIT /b

:ConfigCore
echo Config %2
call "%CLA_HOME%\bin\solr.cmd" zk upconfig -d %SOLR_CORES%\%2 -n %2 -z %3
goto end

:zk-config-collection
echo Config collection %3 to zk %2
set SOLR_ADDRESS=%2
%UPGRADE_TOOL% solr %SOLR_ADDRESS% zk-upconfig %SOLR_CORES%\%3\conf %3
goto end

:removeSolrRoot
IF '%2'=='' GOTO help
call "%CLA_HOME%\bin\solr.cmd" zk rm  -r /%3 -z %2
goto end

:configReloadCollections
echo Configure cores
REM CALL %0 configCores %2
CALL %0 zk-config-collections %2
echo Reload cores on zookeeper
CALL %0 ReloadCollections %2
goto end

:ReloadCollections
set SOLR_ADDRESS=%2
echo Reload all collections
%UPGRADE_TOOL% solr %SOLR_ADDRESS% reload-collections
echo cores on zookeeper reloaded
goto end

:ConfigCores
echo upload configuration to zookeeper
IF '%2'=='' GOTO help
CALL %0 configCore AnalysisData %2
CALL %0 configCore CatFiles %2
CALL %0 configCore ContentMetadata %2
CALL %0 configCore GrpHelper %2
CALL %0 configCore CatFolders %2
CALL %0 configCore BizList %2
CALL %0 configCore FileGroups %2
CALL %0 configCore Extraction %2
CALL %0 configCore FileContent %2
CALL %0 configCore FileState %2
CALL %0 configCore Groups %2
CALL %0 configCore PVS %2
CALL %0 configCore Titles %2
CALL %0 configCore UnmatchedContent %2
goto end

:zk-config-collections
echo upload configuration to zookeeper
IF '%2'=='' GOTO help
CALL %0 zk-config-collection %2 AnalysisData
CALL %0 zk-config-collection %2 CatFiles
CALL %0 zk-config-collection %2 ContentMetadata
CALL %0 zk-config-collection %2 GrpHelper
CALL %0 zk-config-collection %2 CatFolders
CALL %0 zk-config-collection %2 BizList
CALL %0 zk-config-collection %2 FileGroups
CALL %0 zk-config-collection %2 Extraction
CALL %0 zk-config-collection %2 FileContent
CALL %0 zk-config-collection %2 FileState
CALL %0 zk-config-collection %2 Groups
CALL %0 zk-config-collection %2 PVS
CALL %0 zk-config-collection %2 Titles
CALL %0 zk-config-collection %2 UnmatchedContent
goto end

:printCollectionsStatus
set SOLR_ADDRESS=%2
REM echo Printing collections' status
%UPGRADE_TOOL% solr %SOLR_ADDRESS% dump-collection-status
goto end

:deleteDownReplicas
set SOLR_ADDRESS=%2
REM echo Deleting all down-state replicas
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-down-replicas
goto end

:deleteLegacyDownReplicas
set SOLR_ADDRESS=%2
REM echo Deleting all down-state replicas with missing 'type' - i.e. not upgraded to Solr7 format
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-legacy-down-replicas
goto end

:deleteFornaxMigratedReplicas
set SOLR_ADDRESS=%2
REM echo Deleting all replicas for collections copied during Electra to Fornax migration
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-fornax-migrated-replicas
goto end

:deleteFornaxMigratedCollections
set SOLR_ADDRESS=%2
REM echo Deleting all collections copied during Electra to Fornax migration
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-fornax-migrated-collections
goto end

:getAsyncOperationStatus
set SOLR_ADDRESS=%2
set REQUEST_ID=%3
set VERBOSITY=%4
echo Get operation status %REQUEST_ID% / %VERBOSITY%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% async-operation-status %REQUEST_ID% %VERBOSITY%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:splitShard
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
%UPGRADE_TOOL% solr %SOLR_ADDRESS% split-shard %COLLECTION_NAME% %SHARD%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:splitAndMoveShard
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set TARGET_NODE=%5
set VERBOSITY=%6
%UPGRADE_TOOL% solr %SOLR_ADDRESS% split-and-move-shard %COLLECTION_NAME% %SHARD% %TARGET_NODE% %VERBOSITY%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:replicateCollection
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set TARGET_NODE=%5
IF '%5'=='' (
    set TARGET_NODE_STR=''
) ELSE (
    set TARGET_NODE_STR=to node %TARGET_NODE%
)
echo Replicating collection %COLLECTION_NAME% / %SHARD% %TARGET_NODE_STR%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% replicate-collection %COLLECTION_NAME% %SHARD% %TARGET_NODE%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:forceLeader
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set TARGET_NODE=%5
echo Forcing %TARGET_NODE% to take lead for %COLLECTION_NAME%/%SHARD%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% force-leader %COLLECTION_NAME% %SHARD% %TARGET_NODE%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:listAlias
set SOLR_ADDRESS=%2
echo Listing collections aliases
%UPGRADE_TOOL% solr %SOLR_ADDRESS% list-aliases
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:list-nodes
set SOLR_ADDRESS=%2
echo Listing Live nodes
%UPGRADE_TOOL% solr %SOLR_ADDRESS% list-nodes
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:createAlias
set SOLR_ADDRESS=%2
set ALIAS_NAME=%3
set COLLECTIONS_TO_ALIAS=%4
echo Creating alias %ALIAS_NAME% for collections %COLLECTIONS_TO_ALIAS%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% create-alias %ALIAS_NAME% %COLLECTIONS_TO_ALIAS%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:deleteAlias
set SOLR_ADDRESS=%2
set ALIAS_NAME=%3
echo Deleting alias %ALIAS_NAME%
%UPGRADE_TOOL% solr %SOLR_ADDRESS% delete-alias %ALIAS_NAME%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:moveReplica
set SOLR_ADDRESS=%2
set COLLECTION_NAME=%3
set SHARD=%4
set SOURCE_NODE=%5
set TARGET_NODE=%6
echo Moving %COLLECTION_NAME% %SHARD% from %SOURCE_NODE% to %TARGET_NODE% %7 %8
%UPGRADE_TOOL% solr %SOLR_ADDRESS% move-replica %COLLECTION_NAME% %SHARD% %SOURCE_NODE% %TARGET_NODE% %7 %8
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
goto end

:alignShards
set SOLR_ADDRESS=%2
set OUTFILE=%3
echo Generating collection/shard alignment script.
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards "PVS,AnalysisData,ContentMetadata,GrpHelper" %OUTFILE%
REM %UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards "Extraction,CatFiles,BizList,FileGroups" %OUTFILE% append
%UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards "Extraction,CatFiles,BizList" %OUTFILE% append
ECHO exit /b 0 >> %OUTFILE%
goto end

:migrateCollection201710
:migrateCollection
if [%1]==[migrate-collection] set SOLR_ACTION=migrate-collection
if [%1]==[migrate-collection-2017-10] set SOLR_ACTION=migrate-2017-10
set SOLR_ADDRESS=%2
set SRC_COLLECTION=%3
set DEST_COLLECTION=%4
if [%5]==[create] (
    echo Creating %DEST_COLLECTION% with configuration %6 with %7 shards ...
    REM TODO: convert to use upgrade tool
    call "%CLA_HOME%\bin\solr.cmd" create_collection -c %DEST_COLLECTION% -d %6 -n %DEST_COLLECTION% -shards %7
    IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL%
    SET TEMP_BAT=align_%DEST_COLLECTION%.bat
    %UPGRADE_TOOL% solr %SOLR_ADDRESS% align-shards %SRC_COLLECTION%,%DEST_COLLECTION% %TEMP_BAT%
    CALL %TEMP_BAT% do-align
    set CURSOR_MARK="*"
    set PAGE_SIZE=%8
    echo Collection %DEST_COLLECTION% created
) else (
    set CURSOR_MARK=%5
    set PAGE_SIZE=%6
)
echo Migrating collection ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% "%SOLR_ACTION%" %SRC_COLLECTION% %DEST_COLLECTION% %CURSOR_MARK% %PAGE_SIZE%
goto end

:backup
set SOLR_ADDRESS=%2
set COLLECTION=%3
set TARGET_PATH=%4
echo Backing up collection ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% backup %COLLECTION% %TARGET_PATH%
goto end

:backupAll
set SOLR_ADDRESS=%2
set TARGET_PATH=%3
echo Backing up all collections ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% backup-all %TARGET_PATH%
goto end

:restore
set RETURN_TO=restoreGo
goto preparations

:restoreGo
set SOLR_ADDRESS=%2
set COLLECTION=%3
set TARGET_PATH=%4
set NEW_COLLECTION_NAME=%5
echo Restoring collection ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% restore %COLLECTION% %TARGET_PATH% %NEW_COLLECTION_NAME%
goto end

:restoreAll
set RETURN_TO=restoreAllGo
goto preparations

:restoreAllGo
set SOLR_ADDRESS=%2
set TARGET_PATH=%3
set COLLECTION_SUFFIX=%4
echo Restoring all collections ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% restore-all %TARGET_PATH% %COLLECTION_SUFFIX%
goto end

:removeTaskId
set SOLR_ADDRESS=%2
set TASK_ID=%3
echo Removing task id ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% remove-task-id %TASK_ID%
goto end

:removeAllTaskIds
set SOLR_ADDRESS=%2
echo Removing all task ids ...
%UPGRADE_TOOL% solr %SOLR_ADDRESS% remove-all-task-ids
goto end


:preparations
echo Please make sure solr data-dir contains:
echo    solr.xml
echo    zoo.cfg
echo    ..\lib\jars
pause
goto %RETURN_TO%


REM --- Add more options before this line

:help
echo Usage:
echo.
echo Collections administration:
echo Create all collections: %0 zk-create-collections [zookeeperaddress] [numberOfShards]
echo Create all collections: %0 zk-create-collections-if-needed [zookeeperaddress] [numberOfShards]
echo Create all collections: %0 installer-create-collections [zookeeperaddress] [numberOfShards]
echo Create all collections: %0 installer-create-collections2 [zookeeperaddress] [BI numberOfShards] [SIM numberOfShards]
echo Create distributed architecture aliases: %0 create-dist-aliases [zookeeperaddress]
echo Create a single collections: %0 zk-create-collection [zookeeperaddress] [collection-name] [numberOfShards] [optional-rule]
echo Create a single collections: %0 zk-create-collection-if-needed [zookeeperaddress] [collection-name] [numberOfShards] [optional-rule]
echo Configure all collections: %0 zk-config-collections [zookeeperaddress]
echo Configure and reload all collections: %0 config-reload [zookeeperaddress]
echo Print collections' status: %0 print-collection-status [zookeeper-address]
echo.
echo ZooKeeper root configuraiton:
echo Make Root: %0 makeRoot [zookeeperhost] [root]
echo Remove Solr root: %0 removeSolrRoot [zookeeperaddress] [root]
echo.
echo Shard administration:
echo Split shard: %0 split-shard [zookeeper-address] [collection-name] [shard]
echo Split and move shard:
echo      %0 split-and-move-shard [zookeeper-address] [collectionName] [shard] [targetNode]
echo          [optional: timeout-sec] [optional: true for verbosity]
echo      -- target-node ends with '_solr'
echo Add replica: %0 replicate-collection [zookeeper-address] [collection-name] [shard] [optional: target-node]
echo      -- target-node ends with '_solr'
echo Move replica:
echo      %0 move-replica [zookeeper-address] [collection-name] [shard-name] [source-node] [target-node]
echo          [optional: timeout-sec] [optional: 'ok-if-exists'][optional: true for verbosity]
echo      -- nodes end with '_solr'
echo Force replica to lead: %0 force-leader [zookeeper-address] [collection-name] [shard] [target-node] [optional: timeout-sec]
echo      -- target-node ends with '_solr'
echo.
echo Migrate collection:
echo      %0 migrate-collection [zookeeper-address] [src-collection-name] [dest-collection-name]
echo           [optional: 'create' conf-dir shards] [optional: cursor=from-cursor-mark]
echo.
echo Align shards: %0 align-shards [zookeeper-address] [dest_file]
echo.
echo Collection aliases:
echo List collection alias: %0 list-aliases [zookeeper-address]
echo Create/update collection alias: %0 create-alias [zookeeper-address] [alias] [collection-names]
echo Deleted collection alias: %0 delete-alias [zookeeper-address] [alias]
echo.
echo Backup and restore:
echo Backup collection: %0 backup [zookeeper-address] [collection] [backup directory]
echo Backup all collections: %0 backup-all [zookeeper-address] [backup directory]
echo Restore backed-up collection: %0 restore [zookeeper-address] [collection] [backup directory] [optional: collection new name]
echo Restore all backed-up collections: %0 restore-all [zookeeper-address] [backup directory] [optional: suffix to add to collection names]
echo.
echo Print request status: %0 get-async-operation-status [zookeeper-address] [request-id] [optional: true for verbosity]
echo        -- If both request-id and verbosity omitted, will show last failures.
echo.
echo Examples:
echo     %0 makeRoot localhost:2181 solr
echo     %0 split-shard localhost:2181 AnalysisData shard1
echo     %0 split-and-move-shard localhost:2181 Titles shard1_0 localhost:8983_solr true
echo     %0 move-replica localhost:2181 Titles shard1 localhost:8983_solr localhost:8984_solr true
echo     %0 replicate-collection localhost:2181 AnalysisData shard1_1 localhost:8983_solr
echo     %0 force-leader localhost:2181 AnalysisData shard1_1 localhost:8983_solr
echo     %0 migrate-collection zk-ip:port Extraction Extraction-mod create 1        - creates the new collection with 1 shard
echo     %0 migrate-collection zk-ip:port Extraction Extraction-mod cursor=A123BCD  - continue the migration from the given cursor mark

REM echo Remove task id: %0 remove-task-id [zookeeper-address] [task-id]
REM echo Remove all task ids: %0 remove-all-task-ids [zookeeper-address]


exit /b 1
goto end

:createCollections
set SHARDS=%2
IF '%2'=='' SHARDS=1
echo Create Collections with %SHARDS% shards
CALL %0 createCollection AnalysisData %SHARDS%
CALL %0 createCollection CatFiles %SHARDS%
CALL %0 createCollection ContentMetadata %SHARDS%
CALL %0 createCollection GrpHelper %SHARDS%
CALL %0 createCollection CatFolders %SHARDS%
CALL %0 createCollection BizList %SHARDS%
CALL %0 createCollection FileGroups %SHARDS%
CALL %0 createCollection Extraction %SHARDS%
CALL %0 createCollection FileContent %SHARDS%
CALL %0 createCollection FileState %SHARDS%
CALL %0 createCollection Groups %SHARDS%
CALL %0 createCollection PVS %SHARDS%
CALL %0 createCollection Titles %SHARDS%
CALL %0 createCollection UnmatchedContent %SHARDS%
goto end

:installer-create-collections
REM Running installer-create-collections
CALL %0 zk-create-collections-if-needed %2 %3
CALL %0 create-dist-aliases %2
goto end

:installer-create-collections2
REM Running installer-create-collections2
CALL %0 zk-create-collections-if-needed2 %2 %3 %4
CALL %0 create-dist-aliases %2
goto end

:create-dist-aliases
set SOLR_ADDRESS=%2
echo Create Distributed read-write collection aliases
REM CALL %0 delete-alias %SOLR_ADDRESS% PVS_Read
REM CALL %0 delete-alias %SOLR_ADDRESS% PVS_Write
CALL %0 create-alias %SOLR_ADDRESS% PVS_Read PVS
CALL %0 create-alias %SOLR_ADDRESS% PVS_Write PVS
goto end


:zk-create-collections
set SOLR_ADDRESS=%2
set SHARDS=%3
IF '%2'=='' SHARDS=1
echo Create Collections with %SHARDS% shards
CALL %0 zk-create-collection %SOLR_ADDRESS% AnalysisData %SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% CatFiles %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% ContentMetadata %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% GrpHelper %SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% CatFolders %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% BizList %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% FileGroups %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% Extraction %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% FileContent %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% FileState %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% Groups %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% PVS %SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% Titles %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection %SOLR_ADDRESS% UnmatchedContent %SHARDS% %SIM_COLL_RULE%
goto end

:zk-create-collections-if-needed
set SOLR_ADDRESS=%2
set SHARDS=%3
IF '%2'=='' SHARDS=1
echo Create Collections with %SHARDS% shards
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% AnalysisData %SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% CatFiles %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% ContentMetadata %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% GrpHelper %SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% CatFolders %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% BizList %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% FileGroups %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% Extraction %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% FileContent %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% FileState %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% Groups %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% PVS %SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% Titles %SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% UnmatchedContent %SHARDS% %SIM_COLL_RULE%
goto end

:zk-create-collections-if-needed2
set SOLR_ADDRESS=%2
set BI_SHARDS=%3
set SIM_SHARDS=%4
IF '%2'=='' SHARDS=1
echo Create Collections with %BI_SHARDS%/%SIM_SHARDS% BI/SIM shards
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% AnalysisData %SIM_SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% CatFiles %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% ContentMetadata %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% GrpHelper %SIM_SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% CatFolders %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% BizList %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% FileGroups %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% Extraction %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% FileContent %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% FileState %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% Groups %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% PVS %SIM_SHARDS% %SIM_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% Titles %BI_SHARDS% %BI_COLL_RULE%
CALL %0 zk-create-collection-if-needed %SOLR_ADDRESS% UnmatchedContent %SIM_SHARDS% %SIM_COLL_RULE%
goto end

:end
exit /b 0