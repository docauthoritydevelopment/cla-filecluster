@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  filecluster startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal


set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%~dp0

@rem Add default JVM options here. You can also use JAVA_OPTS and CLA_FILECLUSTER_OPTS to pass JVM options to this script.
set "JMX_CONFIG_FILE=%APP_HOME%\config\jmx.properties"

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"
REM set "DEBUG_OPTS=-Dcom.sun.management.config.file=%JMX_CONFIG_FILE% -XX:+UnlockCommercialFeatures -XX:+FlightRecorder"
set "DEBUG_OPTS=-Dcom.sun.management.config.file=%JMX_CONFIG_FILE% -XX:+FlightRecorder"
set "JAVA_OPTS=-Xms2g -Xmx2g -Xss512k"


@rem Find java.exe
SET JAVA_HOME=%~dp0\..\jre
set JAVA_EXE=%JAVA_HOME%\bin\java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init

set CLASSPATH=%APP_HOME%\lib\*

@rem Execute da-mng-agent
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %DEBUG_OPTS%  -classpath "%CLASSPATH%" com.cla.damanage.DaManageAgent %1 %2 %3

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable CLA_FILECLUSTER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%CLA_FILECLUSTER_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
