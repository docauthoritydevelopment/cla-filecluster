@echo off
rem Licensed to the Apache Software Foundation (ASF) under one or more
rem contributor license agreements.  See the NOTICE file distributed with
rem this work for additional information regarding copyright ownership.
rem The ASF licenses this file to You under the Apache License, Version 2.0
rem (the "License"); you may not use this file except in compliance with
rem the License.  You may obtain a copy of the License at
rem
rem     http://www.apache.org/licenses/LICENSE-2.0
rem
rem Unless required by applicable law or agreed to in writing, software
rem distributed under the License is distributed on an "AS IS" BASIS,
rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
rem See the License for the specific language governing permissions and
rem limitations under the License.

rem ---------------------------------------------------------------------------
rem NT Service Install/Uninstall script
rem
rem Options
rem install                Install the service using Tomcat8 as service name.
rem                        Service is installed using default settings.
rem remove                 Remove the service from the System.
rem
rem name        (optional) If the second argument is present it is considered
rem                        to be new service name
rem ---------------------------------------------------------------------------

setlocal

set "SELF=%~dp0%solr-service.bat"
set "COMPONENT_NAME=solr"
set "JVMMS=3g"
set "JVMMX=3g"
rem SET "JMX_PORT=7005"
set GC_LOG_OPTS=-verbose:gc;-XX:+PrintHeapAtGC;-XX:+PrintGCDetails;-XX:+PrintGCDateStamps;-XX:+PrintGCTimeStamps;-XX:+PrintTenuringDistribution;-XX:+PrintGCApplicationStoppedTime

rem set GC_TUNE=-XX:+UseG1GC;-XX:+ParallelRefProcEnabled;-XX:G1HeapRegionSize=8m;-XX:MaxGCPauseMillis=200;-XX:+UseLargePages;-XX:+AggressiveOpts

REM These GC settings have shown to work well for a number of common Solr workloads
set GC_TUNE=-XX:-UseSuperWord;-XX:NewRatio=3;-XX:SurvivorRatio=4;-XX:TargetSurvivorRatio=90;-XX:MaxTenuringThreshold=8;-XX:+UseConcMarkSweepGC;-XX:+UseParNewGC;-XX:ConcGCThreads=4;-XX:ParallelGCThreads=4
set GC_TUNE=%GC_TUNE%;-XX:+CMSScavengeBeforeRemark;-XX:PretenureSizeThreshold=64m;-XX:CMSFullGCsBeforeCompaction=1
set GC_TUNE=%GC_TUNE%;-XX:+UseCMSInitiatingOccupancyOnly;-XX:CMSInitiatingOccupancyFraction=50;-XX:CMSTriggerPermRatio=80
set GC_TUNE=%GC_TUNE%;-XX:CMSMaxAbortablePrecleanTime=6000;-XX:+CMSParallelRemarkEnabled;-XX:+ParallelRefProcEnabled;-XX:+AggressiveOpts

rem set "REMOTE_JMX_OPTS=-Dcom.sun.management.jmxremote;-Dcom.sun.management.jmxremote.port=%JMX_PORT%;-Dcom.sun.management.jmxremote.local.only=false;-Dcom.sun.management.jmxremote.ssl=false;-Dcom.sun.management.jmxremote.authenticate=false"

REM set "SOLR_JVM_OPTIONS=-XX:+UnlockCommercialFeatures;-XX:+FlightRecorder;%GC_TUNE%"
set "SOLR_JVM_OPTIONS=-XX:+FlightRecorder;%GC_TUNE%"

rem Guess CATALINA_HOME if not defined
set "CURRENT_DIR=%cd%"
if not "%CATALINA_HOME%" == "" goto gotHome
set "CATALINA_HOME=%cd%"
if exist "%CATALINA_HOME%\bin\tomcat8.exe" goto okHome
rem CD to the upper dir
cd ..
set "CATALINA_HOME=%cd%"
:gotHome
if exist "%CATALINA_HOME%\bin\tomcat8.exe" goto okHome
echo The tomcat8.exe was not found...
echo The CATALINA_HOME environment variable is not defined correctly.
echo This environment variable is needed to run this program
goto end
:okHome
rem Set JRE_HOME to the install4j JRE
set "JRE_HOME=%~dp0%jre"
goto okJavaHome
rem Make sure prerequisite environment variables are set
if not "%JAVA_HOME%" == "" goto gotJdkHome
if not "%JRE_HOME%" == "" goto gotJreHome
echo Neither the JAVA_HOME nor the JRE_HOME environment variable is defined
echo Service will try to guess them from the registry.
goto okJavaHome
:gotJreHome
if not exist "%JRE_HOME%\bin\java.exe" goto noJavaHome
if not exist "%JRE_HOME%\bin\javaw.exe" goto noJavaHome
goto okJavaHome
:gotJdkHome
if not exist "%JAVA_HOME%\jre\bin\java.exe" goto noJavaHome
if not exist "%JAVA_HOME%\jre\bin\javaw.exe" goto noJavaHome
if not exist "%JAVA_HOME%\bin\javac.exe" goto noJavaHome
if not "%JRE_HOME%" == "" goto okJavaHome
rem set "JRE_HOME=%JAVA_HOME%\jre"

goto okJavaHome
:noJavaHome
echo The JAVA_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
echo NB: JAVA_HOME should point to a JDK not a JRE
goto end
:okJavaHome
rem if not "%CATALINA_BASE%" == "" goto gotBase
set "CATALINA_BASE=%CATALINA_HOME%\%COMPONENT_NAME%"
:gotBase

set "EXECUTABLE=%CATALINA_HOME%\bin\tomcat8.exe"

rem Set default Service name
set SERVICE_NAME=DocAuthority-%COMPONENT_NAME%
set DISPLAYNAME=DocAuthority-%COMPONENT_NAME%

if "x%1x" == "xx" goto displayUsage
set SERVICE_CMD=%1
shift
if "x%1x" == "xx" goto checkServiceCmd
:checkUser
if "x%1x" == "x/userx" goto runAsUser
if "x%1x" == "x--userx" goto runAsUser
set SERVICE_NAME=%1
set DISPLAYNAME=Apache Tomcat 8.0 %1
shift
if "x%1x" == "xx" goto checkServiceCmd
goto checkUser
:runAsUser
shift
if "x%1x" == "xx" goto displayUsage
set SERVICE_USER=%1
shift
runas /env /savecred /user:%SERVICE_USER% "%COMSPEC% /K \"%SELF%\" %SERVICE_CMD% %SERVICE_NAME%"
goto end
:checkServiceCmd
if /i %SERVICE_CMD% == install goto doInstall
if /i %SERVICE_CMD% == remove goto doRemove
if /i %SERVICE_CMD% == uninstall goto doRemove
echo Unknown parameter "%SERVICE_CMD%"
:displayUsage
echo.
echo Usage: service.bat install/remove [service_name] [/user username]
goto end

:doRemove
rem Remove the service
echo Removing the service '%SERVICE_NAME%' ...
echo Using CATALINA_BASE:    "%CATALINA_BASE%"

"%EXECUTABLE%" //DS//%SERVICE_NAME% ^
    --LogPath "%CATALINA_BASE%\logs"
if not errorlevel 1 goto removed
echo Failed removing '%SERVICE_NAME%' service
goto end
:removed
echo The service '%SERVICE_NAME%' has been removed
goto end

:doInstall
rem Install the service

rem Try to use the server jvm
set "JVM=%JRE_HOME%\bin\server\jvm.dll"
if exist "%JVM%" goto foundJvm
rem Try to use the client jvm
set "JVM=%JRE_HOME%\bin\client\jvm.dll"
if exist "%JVM%" goto foundJvm
echo Warning: Neither 'server' nor 'client' jvm.dll was found at JRE_HOME.
set JVM=auto
:foundJvm
echo Using JVM:              "%JVM%"

set SOLR_HOME=%CATALINA_BASE%\solr
set SOLR_DATA_DIR=%CATALINA_BASE%\data
SET SOLR_LOG_DIR=%CATALINA_BASE%\logs
SET SOLR_LOG_FILE=%CATALINA_BASE%\conf\log4j.properties
set "JMX_CONFIG_FILE=%CATALINA_BASE%\conf\jmx.properties"

echo Installing the service '%SERVICE_NAME%' ...
echo Using CATALINA_HOME:    "%CATALINA_HOME%"
echo Using CATALINA_BASE:    "%CATALINA_BASE%"
echo Using JAVA_HOME:        "%JAVA_HOME%"
echo Using JRE_HOME:         "%JRE_HOME%"
echo Using SOLR_HOME:        "%SOLR_HOME%"
echo Using SOLR_DATA_DIR:    "%SOLR_DATA_DIR%"
echo Using SOLR_LOG_DIR:     "%SOLR_LOG_DIR%"
echo Using SOLR_LOG_FILE:    "%SOLR_LOG_FILE%"
echo Using SOLR_JVM_OPTIONS: "%SOLR_JVM_OPTIONS%"


set "CLASSPATH=%CATALINA_HOME%\bin\bootstrap.jar;%CATALINA_BASE%\bin\tomcat-juli.jar"
if not "%CATALINA_HOME%" == "%CATALINA_BASE%" set "CLASSPATH=%CLASSPATH%;%CATALINA_HOME%\bin\tomcat-juli.jar"

"%EXECUTABLE%" //IS//%SERVICE_NAME% ^
    --Description "The DocAuthority solr server supports the advanced analysis and BI product capabilities" ^
    --DisplayName "%DISPLAYNAME%" ^
    --Install "%EXECUTABLE%" ^
    --LogPath "%CATALINA_BASE%\logs" ^
    --StdOutput auto ^
    --StdError auto ^
    --Classpath "%CLASSPATH%" ^
    --Jvm "%JVM%" ^
    --Startup auto ^
    --StartMode jvm ^
    --StopMode jvm ^
    --StartPath "%CATALINA_HOME%" ^
    --StopPath "%CATALINA_HOME%" ^
    --StartClass org.apache.catalina.startup.Bootstrap ^
    --StopClass org.apache.catalina.startup.Bootstrap ^
    --StartParams start ^
    --StopParams stop ^
    --JvmOptions "%SOLR_JVM_OPTIONS%;-Dcom.sun.management.config.file=%JMX_CONFIG_FILE%;-Dlog4j.configuration=file:%SOLR_LOG_FILE%;-Dsolr.log=%SOLR_LOG_DIR%;-Dsolr.solr.home=%SOLR_HOME%;-Dsolr.data.dir=%SOLR_DATA_DIR%;-Dcatalina.home=%CATALINA_HOME%;-Dcatalina.base=%CATALINA_BASE%;-Djava.endorsed.dirs=%CATALINA_HOME%\endorsed;-Djava.io.tmpdir=%CATALINA_BASE%\temp;-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager;-Djava.util.logging.config.file=%CATALINA_BASE%\conf\logging.properties" ^
    --JvmMs %JVMMS% ^
    --JvmMx %JVMMX%
if not errorlevel 1 goto installed
echo Failed installing '%SERVICE_NAME%' service
goto end
:installed
echo The service '%SERVICE_NAME%' has been installed.

:end
cd "%CURRENT_DIR%"
