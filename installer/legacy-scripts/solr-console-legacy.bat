@echo off
setlocal

set DIRNAME=%~dp0

#Change this to the correct data dir
set SOLR_DATA_DIR=C:\da-data\solr

#Change this to the correct memory requirements
set MEM_OPTS=-Xms3g -Xmx3g

echo Start/Stop Solr batch file initiated
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%
set JMX_REMOTE_PORT=7104

set SOLR_HOME=%CLA_HOME%\solr\solr

set "CATALINA_HOME=%cd%"

set "JRE_HOME=%~dp0%jre"

set JAVA_HOME=%JRE_HOME%
set "CATALINA_BASE=%CATALINA_HOME%\solr"

set TOMCAT_BIN=%CATALINA_HOME%\bin

echo SOLR_DATA_DIR=%SOLR_DATA_DIR%
set JAVA_OPTS=-Dsolr.solr.home="%SOLR_HOME%" -Dsolr.data.dir="%SOLR_DATA_DIR%"

SET SOLR_LOG_DIR=%CLA_HOME%\solr\logs
SET SOLR_LOG_FILE=%CLA_HOME%\solr\conf\log4j.properties

set CATALINA_OPTS=%CATALINA_OPTS% -XX:+FlightRecorder

set GC_LOG_OPTS=-verbose:gc;-XX:+PrintHeapAtGC;-XX:+PrintGCDetails;-XX:+PrintGCDateStamps;-XX:+PrintGCTimeStamps;-XX:+PrintTenuringDistribution;-XX:+PrintGCApplicationStoppedTime

REM set GC_TUNE=-XX:+UseG1GC;-XX:+ParallelRefProcEnabled;-XX:G1HeapRegionSize=8m;-XX:MaxGCPauseMillis=200;-XX:+UseLargePages;-XX:+AggressiveOpts

REM These GC settings have shown to work well for a number of common Solr workloads
set GC_TUNE=-XX:-UseSuperWord;-XX:NewRatio=3;-XX:SurvivorRatio=4;-XX:TargetSurvivorRatio=90;-XX:MaxTenuringThreshold=8;-XX:+UseConcMarkSweepGC;-XX:+UseParNewGC;-XX:ConcGCThreads=4;-XX:ParallelGCThreads=4
set GC_TUNE=%GC_TUNE%;-XX:+CMSScavengeBeforeRemark;-XX:PretenureSizeThreshold=64m;-XX:CMSFullGCsBeforeCompaction=1
set GC_TUNE=%GC_TUNE%;-XX:+UseCMSInitiatingOccupancyOnly;-XX:CMSInitiatingOccupancyFraction=50;-XX:CMSTriggerPermRatio=80
set GC_TUNE=%GC_TUNE%;-XX:CMSMaxAbortablePrecleanTime=6000;-XX:+CMSParallelRemarkEnabled;-XX:+ParallelRefProcEnabled;-XX:+AggressiveOpts

set JMXOPT= 
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.authenticate=false
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.ssl=false
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT%
REM set JMXOPT=%JMXOPT% -Djava.rmi.server.hostname=%EXTERN_IP%

set "CATALINA_OPTS=%MEM_OPTS% %JMXOPT% -Dsolr.solr.home=%SOLR_HOME% -Dsolr.data.dir=%SOLR_DATA_DIR% -Dcom.sun.management.config.file=%JMX_CONFIG_FILE% -Dlog4j.configuration=file:%SOLR_LOG_FILE%"
rem set "CATALINA_OPTS=%MEM_OPTS% %JMXOPT% -Dsolr.solr.home=%SOLR_HOME% -Dsolr.data.dir=%SOLR_DATA_DIR%"

rem set "CATALINA_OPTS=%MEM_OPTS% %JMXOPT% -Dlog4j.configuration=file:%SOLR_LOG_FILE% -Dsolr.log=%SOLR_LOG_DIR% -Djava.io.tmpdir=%CATALINA_BASE%\temp"

rem set CATALINA_OPTS="%MEM_OPTS% %SOLR_JVM_OPTIONS%;-Dcom.sun.management.config.file=%JMX_CONFIG_FILE%;-Dlog4j.configuration=file:%SOLR_LOG_FILE%;-Dsolr.log=%SOLR_LOG_DIR%;-Dsolr.solr.home=%SOLR_HOME%;-Dsolr.data.dir=%SOLR_DATA_DIR%;-Dcatalina.home=%CATALINA_HOME%;-Dcatalina.base=%CATALINA_BASE%;-Djava.endorsed.dirs=%CATALINA_HOME%\endorsed;-Djava.io.tmpdir=%CATALINA_BASE%\temp;-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager;-Djava.util.logging.config.file=%CATALINA_BASE%\conf\logging.properties"

echo Using CATALINA_HOME:    "%CATALINA_HOME%"
echo Using CATALINA_BASE:    "%CATALINA_BASE%"
echo Using JAVA_HOME:        "%JAVA_HOME%"
echo Using JRE_HOME:         "%JRE_HOME%"
echo Using SOLR_HOME:        "%SOLR_HOME%"
echo Using SOLR_DATA_DIR:    "%SOLR_DATA_DIR%"
echo Using SOLR_LOG_DIR:     "%SOLR_LOG_DIR%"
echo Using SOLR_LOG_FILE:    "%SOLR_LOG_FILE%"

echo Run "%TOMCAT_BIN%\catalina.bat"
call "%TOMCAT_BIN%\catalina.bat" start %1 %2 %3
endlocal
goto done

:done
