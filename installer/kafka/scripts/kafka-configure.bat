set DIRNAME=%~dp0
set "JRE_HOME=%~dp0%jre"
set JAVA_HOME=%JRE_HOME%

set KAFKA_HOME=%DIRNAME%\kafka
set KAFKA_BIN=%KAFKA_HOME%\bin\windows
set KAFKA_CONFIG=%KAFKA_HOME%\config\server.properties
set ZOOKEEPER_ADDRESS=localhost:2181
set INGEST_RESULT_RETENTION=7200000

echo CREATE SCAN_REQUESTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 2 --topic scan.requests
echo CREATE SCAN_RESULTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 4 --topic scan.results
echo CREATE INGEST_REQUESTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 4 --topic ingest.requests
echo CREATE INGEST_RESULTS with retention of %INGEST_RESULT_RETENTION%ms
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 4 --config delete.retention.ms=%INGEST_RESULT_RETENTION% --topic ingest.results
echo CREATE CTRL REQUESTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 1 --topic control.requests
echo CREATE CTRL RESULTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 1 --topic control.results
echo CREATE FILE_CONTENT_REQUESTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 2 --topic filecontent.requests
echo CREATE FILE_CONTENT_RESULTS
call %KAFKA_BIN%\kafka-topics.bat --create --zookeeper %ZOOKEEPER_ADDRESS% --replication-factor 1 --partitions 4 --topic filecontent.results

echo FINISHED
