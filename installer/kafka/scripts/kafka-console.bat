set DIRNAME=%~dp0
set KAFKA_HOME=%DIRNAME%\kafka
set KAFKA_CONFIG=%KAFKA_HOME%\config\server.properties
set "JRE_HOME=%~dp0%jre"
set JAVA_HOME=%JRE_HOME%

call "%KAFKA_HOME%\bin\windows\kafka-server-start.bat" %KAFKA_CONFIG%
