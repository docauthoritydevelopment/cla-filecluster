package com.cla.sql;

import com.cla.filecluster.installer.ClaInstallUtils;
import com.cla.filecluster.installer.ClaWindowsProcessControl;
import com.install4j.api.Util;
import org.apache.commons.exec.CommandLine;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by uri on 14/07/2016.
 */
@Ignore
public class TestClaInstallUtils {

    @Test
    public void testMySqlServiceNameExtraction() {
        String mySqlServiceName = ClaInstallUtils.findMySqlServiceName();
        Assert.assertTrue("Service name:"+mySqlServiceName,mySqlServiceName.startsWith("MySQL"));
        String serviceBinaryPath = ClaInstallUtils.findServiceBinaryPath(mySqlServiceName);
        Assert.assertTrue("BinaryPath:"+serviceBinaryPath,serviceBinaryPath.contains(".exe"));
    }

    @Test
    public void testMySqlExtractConfiguration() {
        String mySqlServiceName = ClaInstallUtils.findMySqlServiceName();
        String serviceBinaryPath = ClaInstallUtils.findServiceBinaryPath(mySqlServiceName);
        //C:\PROGRA~1\MySQL\MYSQLS~1.6\bin\mysqld.exe --defaults-file="E:\ProgramData\MySQL\MySQL Server 5.6\my.ini" MySQL56
        CommandLine commandLine = CommandLine.parse(serviceBinaryPath);
        String myIni = null;
        for (String s : commandLine.getArguments()) {
            if (s.contains("--defaults-file")) {
                Matcher matcher = Pattern.compile(".+=\"?(.+)").matcher(s);
                if (matcher.find()) {
                    myIni = matcher.group(1);
                    if (myIni.endsWith("\"")) {
                        myIni = myIni.substring(0,myIni.length()-1);
                    }
                }
                else {
                    Util.logError(null,"Failed to extract MySQL ini file location from "+s);
                }
            }
        }
        Assert.assertNotNull(myIni);
        Path myIniPath = Paths.get(myIni);
        Assert.assertTrue(Files.exists(myIniPath));
    }

    @Test @Ignore
    public void testClaWindowsProcessControl() {
        boolean isRunning = ClaWindowsProcessControl.isProcessRuinning("Xfirefox.exe");
        boolean isStopped = ClaWindowsProcessControl.killProcess("Xfirefox.exe");
        Util.logInfo(null,String.format("isRunning = %b, isStopped = %b", isRunning, isStopped));
    }

}
