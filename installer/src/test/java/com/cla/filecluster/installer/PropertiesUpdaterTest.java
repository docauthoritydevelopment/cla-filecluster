package com.cla.filecluster.installer;

import com.cla.filecluster.installer.PropertiesUpdater.NewlyAddedPropsExtractor;
import com.cla.filecluster.installer.PropertiesUpdater.UpdateMarkInserter;
import org.junit.Test;

import java.io.*;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import static com.cla.filecluster.installer.PropertiesUpdater.SEPARATING_LINE;
import static org.junit.Assert.*;

/**
 * Created By Itai Marko
 */
public class PropertiesUpdaterTest {

    @Test
    public void testExtractExistingPropKeysUsingJavaUtilProperties() throws IOException {
        Properties props = new Properties();
        props.setProperty("propKey01", "propVal01");
        props.setProperty("propKey02", "propVal02");
        StringWriter writer = new StringWriter(36);
        props.store(writer, null);
        String propsStr = writer.toString();
        BufferedReader reader = new BufferedReader(new StringReader(propsStr));

        Clock clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        NewlyAddedPropsExtractor propsExtractor = PropertiesUpdater
                .create(clock)
                .extractExistingPropKeys(reader);
        Set<String> existingPropKeys = propsExtractor.getExistingPropKeys();
        Set<String> expectedPropKeys = props.stringPropertyNames();
        assertEquals(expectedPropKeys, existingPropKeys);
    }

    @Test
    public void testExtractNewlyAddedProps() throws IOException {
        String comment01 = "# Andromeda Properties:" + System.lineSeparator();
        String propKey01 = "prop\\=Key\\:01";
        String propVal01 = "prop\\!Val01";
        String prop01 = propKey01 + "=" +  propVal01 + System.lineSeparator();
        String propKey02 = "prop\\ Key\\\\\\#02";
        String propVal02 = "propVal02";
        String prop02 = propKey02 + "=" + propVal02 + System.lineSeparator();
        String prop03WithComment =
                "# propKey03 added in Castor" + System.lineSeparator()
                + "propKey03=propVal03" + System.lineSeparator();
        String prop04NoComment = "prop\\ Key04=prop\\ Val04" + System.lineSeparator();

        String oldProps =
                comment01
                + prop01
                + prop02;

        String newProps =
                comment01
                + prop01
                + "# Some new comment that's not adjacent to any new property" + System.lineSeparator()
                + System.lineSeparator()
                + prop03WithComment
                + prop02
                + prop04NoComment;

        BufferedReader oldReader = new BufferedReader(new StringReader(oldProps));
        BufferedReader newReader = new BufferedReader(new StringReader(newProps));

        Clock clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());
        NewlyAddedPropsExtractor propsExtractor = PropertiesUpdater
                .create(clock)
                .extractExistingPropKeys(oldReader);
        Set<String> existingPropKeys = propsExtractor.getExistingPropKeys();
        Set<String> expectedPropKeys = new HashSet<>(Arrays.asList(propKey01, propKey02));
        assertEquals(expectedPropKeys, existingPropKeys);

        UpdateMarkInserter updateMarker = propsExtractor.extractNewlyAddedProps(newReader);
        String newlyAddedProps = updateMarker.getNewlyAddedProps();
        String expectedNewlyAddedProps = prop03WithComment + prop04NoComment;
        assertEquals(expectedNewlyAddedProps, newlyAddedProps);

        PropertiesUpdater updater = updateMarker.generateUpdateMark("Andromeda", "Castor");
        newlyAddedProps = updater.getNewlyAddedProps();
        String now = ZonedDateTime.now(clock).format(DateTimeFormatter.RFC_1123_DATE_TIME);
        String updateMark = System.lineSeparator() +
                SEPARATING_LINE + System.lineSeparator()
                + "# Updated on " + now + " from Andromeda to Castor" + System.lineSeparator()
                + SEPARATING_LINE
                + System.lineSeparator()
                + System.lineSeparator();
        expectedNewlyAddedProps = updateMark + expectedNewlyAddedProps;
        assertEquals(expectedNewlyAddedProps, newlyAddedProps);

        StringWriter updateWriter = new StringWriter(54 + updateMark.length());
        BufferedWriter bufferedUpdateWriter = new BufferedWriter(updateWriter);
        bufferedUpdateWriter.write(oldProps);
        updater.update(bufferedUpdateWriter);
        String updatedProps = updateWriter.toString();
        String expectedUpdatedProps = oldProps + expectedNewlyAddedProps;
        assertEquals(expectedUpdatedProps, updatedProps);
    }
}