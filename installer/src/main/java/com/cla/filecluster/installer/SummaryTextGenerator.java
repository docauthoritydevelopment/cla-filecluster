package com.cla.filecluster.installer;

import com.install4j.api.Util;
import com.install4j.api.context.InstallerContext;

/**
 * Created by uri on 30-Apr-17.
 */
public class SummaryTextGenerator {

    /**
     * Called by Install4j
     * @param context
     */
    public static void generateSummaryText(InstallerContext context) {
        //TODO create variables: installation.appserver.summary.text field
        generateAppServerSummary(context);
        generateSolrSummary(context);
        generateBrokerSummary(context);
        //TODO create variables: installation.zookeeper.summary.text field
        //TODO create variables: installation.mysql.summary.text field
        generateMediaProcessorSummary(context);
        generateUiSummary(context);
//        ${compiler:filecluster.name}:
//        MySQL Host/Port: ${installer:filecluster.mysql.host}:${installer:filecluster.mysql.port}
//        MySQL Schema: ${installer:filecluster.mysql.schema}
//        MySQL Schema creation: ${installer:filecluster.mysql.schema.reset}
//        MySQL URL: ${installer:filecluster.mysql.url}
//        ${installer:summary.mysql.data.relocation}
//        Service user: ${installer:filecluster.service.user}
//        Service ports: ${installer:filecluster.service.port.http}, ${installer:filecluster.service.port.https}, ${installer:filecluster.service.port.jmx}
//        Memory: ${installer:filecluster.memory.usage}gb
//        Admin user: ${installer:filecluster.admin.user}
//
//        Solr:
//        Service ports: ${installer:solr.service.port.http}, ${installer:solr.service.port.jmx}
//        Solr data dir: ${installer:solr.data.directory}
//        Memory: ${installer:solr.memory.usage}gb
//
//
//        Zookeeper:
//        ZooKeeper Address: ${installer:solr.zookeeper.address}
//

    }

    private static void generateUiSummary(InstallerContext context) {
//        Ui:
//        HTTP Port: ${installer:ui.port.http}
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ui:\n");
        if (ClaInstallUtils.isUIComponentSelected(context)) {
            stringBuilder.append("   * Selected").append("\n");
        }
        Object variable = context.getVariable("ui.port.http");
        stringBuilder.append("HTTP Port: ").append(variable);
        context.setVariable("installation.ui.summary.text",stringBuilder.toString());

    }
    private static void generateBrokerSummary(InstallerContext context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ActiveMQ installed").append("\n");
        if (ClaInstallUtils.isActiveMQComponentSelected(context)) {
            stringBuilder.append("   * Selected").append("\n");
        }
        Object dataDir = context.getVariable("activemq.dir.data");
        stringBuilder.append("Data directory:").append(dataDir).append("\n");

        Object msgport = context.getVariable("activemq.address.port");
        stringBuilder.append("Messaging listening port:").append(msgport).append("\n");

        Object conaddr = context.getVariable("activemq.console.listening.address");
        Object conport = context.getVariable("activemq.console.listening.port");
        stringBuilder.append("Console listening address:").append(conaddr).append(":").append(conport);
        context.setVariable("installation.broker.summary.text",stringBuilder.toString());
    }

    private static void generateAppServerSummary(InstallerContext context) {
        Util.logInfo(null,"Generate AppServer Summary Text");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AppServer:\n");
        if (ClaInstallUtils.isFileClusterComponentSelected(context)) {
            stringBuilder.append("   * Selected").append("\n");
        }

        Object datadir = context.getVariable("filecluster.kvdb.dir");
        stringBuilder.append("Appserver data directory:").append(datadir).append("\n");

        Object mySqlHost = context.getVariable("filecluster.mysql.host");
        Object mySqlPort = context.getVariable("filecluster.mysql.port");
        stringBuilder.append("MySQL Host/Port:").append(mySqlHost).append(":").append(mySqlPort);
        stringBuilder.append("\n");

        Object mySqlSchema = context.getVariable("filecluster.mysql.schema");
        stringBuilder.append("MySQL Schema:").append(mySqlSchema).append("\n");

//        if (context.getBooleanVariable("filecluster.mysql.schema.reset")) {
        if (context.getBooleanVariable("filecluster.isInitDB")) {
            stringBuilder.append(" * Create/Reset mySql Schema: ")
            .append(context.getBooleanVariable("filecluster.mysql.schema.reset")?"Yes":"No").append("\n");
        }
        Object mySqlUrl = context.getVariable("filecluster.mysql.url");
        stringBuilder.append("MySQL URL:").append(mySqlUrl).append("\n");

        Object dataRelocationSummary = context.getVariable("summary.mysql.data.relocation");
        if (dataRelocationSummary != null) {
            stringBuilder.append(dataRelocationSummary).append("\n");
        }

        Object serviceUser = context.getVariable("filecluster.service.user");
        stringBuilder.append("Service user:").append(serviceUser).append("\n");

        Object httpPort = context.getVariable("filecluster.service.port.http");
        stringBuilder.append("Http port:").append(httpPort).append("\n");

        Object memoryUsage = context.getVariable("filecluster.memory.usage");
        stringBuilder.append("Memory:").append(memoryUsage).append("gb\n");

        Object adminUser = context.getVariable("filecluster.admin.user");
        stringBuilder.append("Admin user:").append(adminUser);

        context.setVariable("installation.appserver.summary.text",stringBuilder.toString());

    }

    private static void generateSolrSummary(InstallerContext context) {
        Util.logInfo(null,"Generate Solr Summary Text");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Solr:\n");
        if (ClaInstallUtils.isSolrComponentSelected(context)) {
            stringBuilder.append("   * Solr Selected").append("\n");
        }
        if (ClaInstallUtils.isZookeeperComponentSelected(context)) {
            stringBuilder.append("   * Zookeeper Selected").append("\n");
        }

        stringBuilder.append("Service port:");
        Object port = context.getVariable("solr.service.port.http");
        stringBuilder.append(port).append("\n");

        stringBuilder.append("Solr data dir:");
        Object dataDir = context.getVariable("solr.data.directory");
        stringBuilder.append(dataDir).append("\n");

        Object memoryUsage = context.getVariable("solr.memory.usage");
        stringBuilder.append("Memory:").append(memoryUsage).append("gb\n");

        Object zookeeperAddress = context.getVariable("solr.zookeeper.address");
        stringBuilder.append("Zookeeper address:").append(zookeeperAddress).append("\n");

        Object zkdataDir = context.getVariable("zookeeper.dir.data");
        stringBuilder.append("Zookeeper data directory:").append(zkdataDir).append("\n");
        Object zktrlogDir = context.getVariable("zookeeper.dir.trlog");
        stringBuilder.append("Zookeeper trans. log directory:").append(zktrlogDir).append("\n");

        String solrZKHostAddress = (String)context.getVariable("solr.host.address");
        boolean isSolrZKHostAddress = context.getBooleanVariable("solr.set.address");
        stringBuilder.append("Solr local address:").append(isSolrZKHostAddress?solrZKHostAddress:"not-set");

        context.setVariable("installation.solr.summary.text",stringBuilder.toString());
    }

    public static void generateMediaProcessorSummary(InstallerContext context) {
        Util.logInfo(null,"Generate Media Processor Summary Text");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Media Processor:\n");
        if (ClaInstallUtils.isMediaProcessorComponentSelected(context)) {
            stringBuilder.append("   * Selected").append("\n");
        }

        Object datadir = context.getVariable("mediaprocessor.kvdb.dir");
        stringBuilder.append("Media Processor data directory:").append(datadir).append("\n");

        boolean defaultUser = context.getBooleanVariable("mediaprocessor.service.user.use.default");
        if (defaultUser) {
            stringBuilder.append("Service user:").append("System User").append("\n");
        }
        else {
            Object variable = context.getVariable("mediaprocessor.service.user");
            stringBuilder.append("Service user:").append(variable).append("\n");
        }
        Object memoryUsage = context.getVariable("mediaproc.memory.usage");
        stringBuilder.append("Memory: ").append(memoryUsage);

        context.setVariable("installation.mediaprocessor.summary.text",stringBuilder.toString());
    }
}
