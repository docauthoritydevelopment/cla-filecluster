package com.cla.filecluster.installer;

import com.install4j.api.ServiceInfo;
import com.install4j.api.SystemInfo;
import com.install4j.api.Util;
import com.install4j.api.context.Context;
import com.install4j.api.context.InstallationComponentSetup;
import com.install4j.api.context.InstallerContext;
import com.install4j.api.context.UserCanceledException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.PumpStreamHandler;

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Clock;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by uri on 13/07/2015.
 */
public class ClaInstallUtils {

    private static final String FILECLUSTER_COMPONENT_ID = "filecluster";
    private static final String VCREDIST_COMPONENT_ID = "vcredist";
    private static final String SOLR_COMPONENT_ID = "solr";
    private static final String ZOOKEEPER_COMPONENT_ID = "Zookeeper";
    private static final String ACTIVEMQ_COMPONENT_ID = "ActiveMQ";
    private static final String MEDIAPROCESSOR_COMPONENT_ID = "MediaProcessor";
    private static final String UI_COMPONENT_ID = "ui";
    private static final String MYSQL_MIGRATION_COMPONENT_ID = "MySqlMig";

    private static Map<String, Boolean> componentSelectionSnapshot = new HashMap<>();

    public static boolean isIsFressInstallation() {
        return fressInstallation;
    }

    private static boolean fressInstallation;

    public static void getCurrentComponentSnapshot (Context context, boolean isFressInstallation) {
        ClaInstallUtils.fressInstallation = isFressInstallation;
        if (componentSelectionSnapshot.isEmpty()) { // make sure only first activation counts
            Collection<InstallationComponentSetup> components = context.getInstallationComponents();
            components.forEach(c -> componentSelectionSnapshot.put(c.getId(), c.isSelected() && !isFressInstallation));
        }
        Util.logInfo(null,"Initial components selection: " +
                componentSelectionSnapshot.entrySet().stream()
                        .map(e->e.getKey()+(e.getValue()?"+":"-"))
                        .collect(Collectors.joining(",","{","}")));
        Util.logInfo(null,"Initial compiler variables: " +
                context.getVariableNames().stream().filter(v->context.getCompilerVariable(v)!=null)
                        .map(v->v+"="+context.getCompilerVariable(v).toString())
                        .collect(Collectors.joining(",","{","}")));
        Util.logInfo(null,"Initial runtime variables: " +
                context.getVariableNames().stream().filter(v->context.getVariable(v)!=null)
                        .map(v->v+"="+context.getVariable(v).toString())
                        .collect(Collectors.joining(",","{","}")));
    }

    public static Collection<String> getAllInterfaceAddresses() {
        Set<String> res = new HashSet<>();
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface inf = interfaces.nextElement();
                Enumeration<InetAddress> ifAddresses = inf.getInetAddresses();
                while (ifAddresses.hasMoreElements()) {
                    InetAddress addr = ifAddresses.nextElement();
                    res.add(addr.getHostAddress());
                    res.add(addr.getCanonicalHostName());
                    Util.logInfo(null,"Found interface address: " + inf.getDisplayName() + "[" + inf.getName() + "] <" + addr.getCanonicalHostName() + " "  + addr.getHostAddress() + ">");
                }
            }
        } catch (SocketException e) {
            Util.logError(null," Error retrieving network interface list");
        }
        return res.stream().sorted((s1,s2)->addressCompareKey(s1).compareTo(addressCompareKey(s2))).collect(Collectors.toList());
    }

    /**
     * Updates oldPropsFile with newly added properties found in newPropsFile.
     * <p/>
     * The update essentially appends an update-mark at the end of oldPropsFile followed by the newly added
     * properties, with their adjacent comments, at the end of the oldPropsFile.
     * <p/>
     * A <b>newly added property</b> is a property found in the newPropsFile whose key is not the key of any property in
     * oldPropsFile.
     * <p/>
     * A <b>property's adjacent comment</b> is a comment that's located immediately above the property. It may span
     * multiple lines. An empty line or a line that consist of only whitespaces are not considered a comment line.
     * <p/>
     * An <b>update-mark</b> is a 3 line comment that separate the old untouched properties that were in the
     * old file, from the new addition. Its first and last lines are just separating lines. Its middle line states the
     * date and time of the update (in the specified timeZone), from which and to which version the upgrade is done.
     * A null timeZone is allowed, in which case the date and time of the update-mark will be formatted according to the
     * JVM's default timeZone.
     * <p/>
     * This method adheres to the format of Java properties file, as described in the javadoc of
     * {@link Properties#load(Reader)} method, with one exception:
     * This method assumes a property is entirely contained within one line. It cannot span several lines by escaping
     * the newline character.
     *
     * @param oldPropsFile The file to update
     * @param newPropsFile The file from which to find newly added properties
     * @param oldVersion   The version from which the upgrade is done. Stated in the update-mark
     * @param newVersion   The version to which the upgrade is done. Stated in the update-mark
     * @param timeZone     The timeZone to format the time and date of the update-mark. May be null
     * @throws IOException Propagated in case it is thrown when accessing the files
     */
    public static void updatePropertiesFile(
            Path oldPropsFile, Path newPropsFile,
            String oldVersion, String newVersion, ZoneId timeZone) throws IOException {

        Util.logInfo(null, "Updating new properties file " + newPropsFile + " from old " + oldPropsFile);

        try (BufferedReader oldPropsBr = Files.newBufferedReader(oldPropsFile);
             BufferedReader newPropsBr = Files.newBufferedReader(newPropsFile);
             BufferedWriter oldPropsBw = Files.newBufferedWriter(oldPropsFile, StandardOpenOption.WRITE, StandardOpenOption.APPEND)) {

            PropertiesUpdater
                    .create(timeZone != null ? Clock.system(timeZone) : Clock.systemDefaultZone())
                    .extractExistingPropKeys(oldPropsBr)
                    .extractNewlyAddedProps(newPropsBr)
                    .generateUpdateMark(oldVersion, newVersion)
                    .update(oldPropsBw);

            Util.logInfo(null, "Finished updating properties file.");
        }
        catch(Exception e){
            Util.logError(e, "Failed to update properties file.");
        }
    }
    public static void updatePropertiesFile(
            String oldPropsFileStr, String newPropsFileStr,
            String oldVersion, String newVersion, ZoneId timeZone) throws IOException {
        updatePropertiesFile(
                Paths.get(oldPropsFileStr), Paths.get(newPropsFileStr),
                oldVersion, newVersion, timeZone);
    }

    public static String getMissingProperties(String oldPropsFileStr, String newPropsFileStr) throws IOException {
        return getMissingProperties(Paths.get(oldPropsFileStr), Paths.get(newPropsFileStr));
    }
    public static String getMissingProperties(Path oldPropsFile, Path newPropsFile) throws IOException {

        try (BufferedReader oldPropsBr = Files.newBufferedReader(oldPropsFile);
             BufferedReader newPropsBr = Files.newBufferedReader(newPropsFile)) {

            return PropertiesUpdater
                    .create()
                    .extractExistingPropKeys(oldPropsBr)
                    .extractNewlyAddedProps(newPropsBr)
                    .getNewlyAddedProps();
        }
    }

    private static String addressCompareKey(String s) {
        char c0 = s.charAt(0);
        char c1 = s.charAt(1);
        char c2 = s.charAt(2);
        char c3 = s.charAt(2);
        char c4 = s.charAt(2);
        boolean isRawAddr = c1 == '.' || c2 == '.'  || c3 == '.' || c1 == ':' || c2 == ':' || c3 == ':' || c4 == ':';
        return (isRawAddr?"0":"1") + String.format("%03d", s.length()) + s;
    }

    private static boolean isComponentSelected(Context context, String id) {
        InstallationComponentSetup fileclusterComponent = context.getInstallationComponentById(id);
        return fileclusterComponent.isSelected();
    }

    private static boolean isComponentAdded(Context context, String id) {
        InstallationComponentSetup fileclusterComponent = context.getInstallationComponentById(id);
        Boolean wasSelected = componentSelectionSnapshot.get(id);
        return fileclusterComponent.isSelected() && !wasSelected;
    }

    private static boolean isComponentRemoved(Context context, String id) {
        InstallationComponentSetup fileclusterComponent = context.getInstallationComponentById(id);
        Boolean wasSelected = componentSelectionSnapshot.get(id);
        return wasSelected && !fileclusterComponent.isSelected();
    }

    public static boolean isFileClusterComponentSelected(Context context) {
        return isComponentSelected(context, FILECLUSTER_COMPONENT_ID);
    }

    public static boolean isVcRedistComponentSelected(Context context) {
        return isComponentSelected(context, VCREDIST_COMPONENT_ID);
    }

    public static boolean isSolrComponentSelected(Context context) {
        return isComponentSelected(context, SOLR_COMPONENT_ID);
    }

    public static boolean isZookeeperComponentSelected(Context context) {
        return isComponentSelected(context, ZOOKEEPER_COMPONENT_ID);
    }

    public static boolean isActiveMQComponentSelected(Context context) {
        return isComponentSelected(context, ACTIVEMQ_COMPONENT_ID);
    }

    public static boolean isMediaProcessorComponentSelected(Context context) {
        return isComponentSelected(context, MEDIAPROCESSOR_COMPONENT_ID);
    }

    public static boolean isUIComponentSelected(Context context) {
        return isComponentSelected(context, UI_COMPONENT_ID);
    }

    public static boolean isMySqlMigrationSelected(Context context) {
        return isComponentSelected(context, MYSQL_MIGRATION_COMPONENT_ID);
    }

    public static boolean isFileClusterComponentAdded(Context context) {
        return isComponentAdded(context, FILECLUSTER_COMPONENT_ID);
    }

    public static boolean isVcRedistComponentAdded(Context context) {
        return isComponentAdded(context, VCREDIST_COMPONENT_ID);
    }

    public static boolean isSolrComponentAdded(Context context) {
        return isComponentAdded(context, SOLR_COMPONENT_ID);
    }

    public static boolean isZookeeperComponentAdded(Context context) {
        return isComponentAdded(context, ZOOKEEPER_COMPONENT_ID);
    }

    public static boolean isActiveMQComponentAdded(Context context) {
        return isComponentAdded(context, ACTIVEMQ_COMPONENT_ID);
    }

    public static boolean isMediaProcessorComponentAdded(Context context) {
        return isComponentAdded(context, MEDIAPROCESSOR_COMPONENT_ID);
    }

    public static boolean isUIComponentAdded(Context context) {
        return isComponentAdded(context, UI_COMPONENT_ID);
    }

    public static boolean isMySqlMigrationAdded(Context context) {
        return isComponentAdded(context, MYSQL_MIGRATION_COMPONENT_ID);
    }

    public static boolean isFileClusterComponentRemoved(Context context) {
        return isComponentRemoved(context, FILECLUSTER_COMPONENT_ID);
    }

    public static boolean isVcRedistComponentRemoved(Context context) {
        return isComponentRemoved(context, VCREDIST_COMPONENT_ID);
    }

    public static boolean isSolrComponentRemoved(Context context) {
        return isComponentRemoved(context, SOLR_COMPONENT_ID);
    }

    public static boolean isZookeeperComponentRemoved(Context context) {
        return isComponentRemoved(context, ZOOKEEPER_COMPONENT_ID);
    }

    public static boolean isActiveMQComponentRemoved(Context context) {
        return isComponentRemoved(context, ACTIVEMQ_COMPONENT_ID);
    }

    public static boolean isMediaProcessorComponentRemoved(Context context) {
        return isComponentRemoved(context, MEDIAPROCESSOR_COMPONENT_ID);
    }

    public static boolean isUIComponentRemoved(Context context) {
        return isComponentRemoved(context, UI_COMPONENT_ID);
    }

    public static boolean isMySqlMigrationRemoved(Context context) {
        return isComponentRemoved(context, MYSQL_MIGRATION_COMPONENT_ID);
    }

    public static void loadDefaultValues(Context context) {
        loadDefaultValues(context, true);
    }
    public static void loadDefaultValues(Context context, boolean runMemoryCalc) {
        String build_number = context.getCompilerVariable("BUILD_NUMBER");
        String version = context.getCompilerVariable("FILECLUSTER_VERSION");

        context.setVariable("filecluster.version",version+"."+build_number);
//        context.setVariable("filecluster.service.port.http", 8080L);
//        context.setVariable("filecluster.service.port.https", 8443L);
//        context.setVariable("filecluster.service.port.jmx", 7004L);
//
//        context.setVariable("solr.service.port.http", 8983L);
//        context.setVariable("solr.service.port.https", 8984L);
//        context.setVariable("solr.service.port.jmx", 7104L);

//        context.setVariable("ui.port.http", 9000L);
        context.setVariable("filecluster.upgrade.schema.execute", true);

//        context.setVariable("fileprocessor.service.port.http", 8280L);
//        context.setVariable("fileprocessor.service.port.jmx", 7014L);
        if (runMemoryCalc) {
            calculateDefaultMemoryRecommendations(context);
        }
    }

    /**
     *
     * @param context
     */
    public static void createSummaryConfiguration(Context context) {

    }

    public static void calculateDefaultMemoryRecommendations(Context context) {

//        boolean separateProcess = context.getBooleanVariable("fileprocessor.separate.process");
        boolean mediaProcessorComponentSelected = isMediaProcessorComponentSelected(context);
        boolean solrSelected = isSolrComponentSelected(context);

        int physicalMemoryInGigaByte = calculatePhysicalMemory();
        context.setVariable("memory.physical.giga", physicalMemoryInGigaByte);
        int appServerRecommanded = calculateAppServerMemoryRecommandation(mediaProcessorComponentSelected, solrSelected, physicalMemoryInGigaByte);
        context.setVariable("filecluster.memory.usage", appServerRecommanded);
        int remainingMemory = physicalMemoryInGigaByte - appServerRecommanded;
        int solrMem = 0;
        int mpMem = 0;
        if (mediaProcessorComponentSelected && solrSelected) {
            //Split the rest between solr and the media processor
            solrMem = remainingMemory / 3;
            context.setVariable("solr.memory.usage", solrMem);
            mpMem = remainingMemory / 3;
            context.setVariable("mediaproc.memory.usage", mpMem);
        }
        else if (mediaProcessorComponentSelected) {
            mpMem = remainingMemory / 2;
            context.setVariable("mediaproc.memory.usage", mpMem);
        }
        else if (solrSelected){
            solrMem = remainingMemory / 2;
            context.setVariable("solr.memory.usage", solrMem);
        }
        Util.logInfo(null,"Calc default memory recommendations: solr=" + solrMem + " mp=" + mpMem + " fc=" + appServerRecommanded);
    }

    private static int calculateAppServerMemoryRecommandation(boolean mediaProcessorComponentSelected, boolean solrSelected, int physicalMemoryInGigaByte) {
        int appServerRecommanded = (physicalMemoryInGigaByte / 2);
        if (mediaProcessorComponentSelected &&  solrSelected ) {
            appServerRecommanded = (physicalMemoryInGigaByte / 4) -1;
        }
        else if (mediaProcessorComponentSelected || solrSelected ) {
            appServerRecommanded = (physicalMemoryInGigaByte / 3) - 1;
        }
        appServerRecommanded = Math.max(2,appServerRecommanded);
        appServerRecommanded = Math.min(7,appServerRecommanded);
        return appServerRecommanded;
    }

    private static int calculatePhysicalMemory() {
        int physicalMemoryInGigaByte = 6;
        try {
            com.sun.management.OperatingSystemMXBean os = (com.sun.management.OperatingSystemMXBean)
                    java.lang.management.ManagementFactory.getOperatingSystemMXBean();
            long physicalMemorySize = os.getTotalPhysicalMemorySize();
            physicalMemoryInGigaByte = Long.valueOf(physicalMemorySize / 1000000000).intValue();
        } catch (Exception e) {
            Util.showErrorMessage("Failed to extract physical memory size: " + e.getMessage());
            Util.logError(e, "Failed to extract physical memory size: " + e.getMessage());
        }
        return physicalMemoryInGigaByte;
    }

    public static boolean checkIfServiceIsRunning(File file) {
        //Check if the service is running
        if (ServiceInfo.isServiceRunning(file)) {
            return true;
        }
        return false;
    }

    public static void calculateDefaultMySqlTarget(Context context) {
        String sourceFolder = (String) context.getVariable("mysql.data.migration.source");
        String targetFolder = sourceFolder;
        File driveD = new File("d:\\");
        if (driveD.exists()) {
            targetFolder = "d:\\ProgramData\\MySQL";
        }
        if (targetFolder != null && !targetFolder.isEmpty()) {
            long freeDiskSpace = SystemInfo.getFreeDiskSpace(new File(targetFolder));
            freeDiskSpace = freeDiskSpace / (1024 * 1024 * 1024);
            context.setVariable("mysql.data.migration.target.freespace",freeDiskSpace+" GB");
        }
        context.setVariable("mysql.data.migration.target",targetFolder);
    }

    public static void copySourceToTarget(Context context) {
        String sourceFolder = (String) context.getVariable("mysql.data.migration.source");
        if (sourceFolder != null && !sourceFolder.isEmpty()) {
            long freeDiskSpace = SystemInfo.getFreeDiskSpace(new File(sourceFolder));
            freeDiskSpace = freeDiskSpace / (1024 * 1024 * 1024);
            context.setVariable("mysql.data.migration.target.freespace",freeDiskSpace+" GB");
        }
        context.setVariable("mysql.data.migration.target",sourceFolder);

    }

    public static void createMySqlDataRelocationSummaryText(Context context) {
        StringBuilder stringBuilder = new StringBuilder("MySql data relocation: ");
        boolean enableMigration = (Boolean) context.getVariable("mysql.data.migration.enable");
        if (enableMigration) {
            stringBuilder.append("move to ").append(context.getVariable("mysql.data.migration.target"));
        }
        else {
            stringBuilder.append("no");
        }
        context.setVariable("summary.mysql.data.relocation",stringBuilder.toString());
    }

    public static String findServiceBinaryPath(String serviceName) {
        String queryResponse = executeCommandLineSafely("sc qc "+serviceName);
        //BINARY_PATH_NAME   : C:\PROGRA~1\MySQL\MYSQLS~1.6\bin\mysqld.exe --defaults-file="E:\ProgramData\MySQL\MySQL Server 5.6\my.ini" MySQL56
        if (queryResponse.contains("BINARY_PATH_NAME")) {
            Util.logInfo(null,"Found binary path in configuration");
            Matcher matcher = Pattern.compile("BINARY_PATH_NAME\\s*:\\s*(.+)").matcher(queryResponse);
            if (matcher.find()) {
                String path = matcher.group(1);
                Util.logInfo(null,"Found binary path: ["+path+"] for service "+serviceName);
                return path;
            }
        }
        return null;
    }

    public static String findMySqlServiceName() {
        String queryResponse = executeCommandLineSafely("sc query");
        //SERVICE_NAME: MySQL56
        if (queryResponse.contains("MySQL")) {
            Util.logInfo(null,"Found MySQL service");
            Matcher matcher = Pattern.compile("SERVICE_NAME:\\s+(MySQL.*)").matcher(queryResponse);
            if (matcher.find()) {
                String serviceName = matcher.group(1);
                Util.logInfo(null,"Found MySQL service: ["+serviceName+"]");
                return serviceName;
            }
        }
        return null;
    }

    public static String executeCommandLineSafely(String line) {
        DefaultExecutor executor = new DefaultExecutor();
        ExecuteWatchdog watchdog = new ExecuteWatchdog(30000);
        executor.setWatchdog(watchdog);
        Util.logInfo(null,"Execute command line: "+line);
        try {
            CommandLine cmdLine = CommandLine.parse(line);
            OutputStream outputStream = new ByteArrayOutputStream();
            PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
            executor.setStreamHandler(streamHandler);
            int exitValue = executor.execute(cmdLine);
            String s = outputStream.toString();
            Util.logInfo(null,s);
            return s;
        } catch (IOException e) {
            Util.logError(e,"Failed to extract MySQL service name (IO error)");
            return "";
        }
    }

    public static void extractExistingMySqlParams(Context context) {
        Util.logInfo(null,"Extract existing MySql params");
        String mySqlServiceName = findMySqlServiceName();
        if (mySqlServiceName == null) {
            return;
        }
        context.setVariable("mysql.data.migration.service.name",mySqlServiceName);
        extractMySqlParametersFromServiceName(context, mySqlServiceName);

    }

    public static void extractMySqlParametersFromServiceName(Context context, String mySqlServiceName) {
        String serviceBinaryPath = findServiceBinaryPath(mySqlServiceName);
        //C:\PROGRA~1\MySQL\MYSQLS~1.6\bin\mysqld.exe --defaults-file="E:\ProgramData\MySQL\MySQL Server 5.6\my.ini" MySQL56
        CommandLine commandLine = CommandLine.parse(serviceBinaryPath);
        String executable = commandLine.getExecutable();
        Util.logInfo(null,"MySQL executable: "+executable);
        context.setVariable("mysql.data.migration.executable",executable);
        String myIni = null;
        for (String s : commandLine.getArguments()) {
            if (s.contains("--defaults-file")) {
                Matcher matcher = Pattern.compile(".+=\"?(.+)").matcher(s);
                if (matcher.find()) {
                    myIni = matcher.group(1);
                    if (myIni.endsWith("\"")) {
                        myIni = myIni.substring(0,myIni.length()-1);
                    }
                }
                else {
                    Util.logError(null,"Failed to extract MySQL ini file location from "+s);
                }
            }
        }
        if (myIni != null) {
            context.setVariable("mysql.data.migration.ini",myIni);
            Path myIniPath = Paths.get(myIni);
            if (Files.exists(myIniPath)) {
                readMyIniPath(context, myIniPath);
            }
        }
    }

    public static boolean readMyIniFile(Context context) {
        Util.logInfo(null,"Read MyIni file");
        String myIni = "C:\\ProgramData\\MySQL\\MySQL Server 5.7\\my.ini";
        String executable = "C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin\\mysqld.exe";
        Path myIniPath = Paths.get(myIni);
        if (Files.exists(myIniPath)) {
            readMyIniPath(context, myIniPath);
        }
        else {
            myIni = "C:\\ProgramData\\MySQL\\MySQL Server 5.6\\my.ini";
            executable = "C:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\mysqld.exe";
            myIniPath = Paths.get(myIni);
            if (Files.exists(myIniPath)) {
                readMyIniPath(context, myIniPath);
                context.setVariable("mysql.data.migration.service.name","MySQL56");
            }
            else {
                myIni = "";
                context.setVariable("mysql.data.migration.source","");
                context.setVariable("mysql.data.migration.source","");
                context.setVariable("mysql.data.migration.source.freespace","");
                context.setVariable("mysql.data.migration.target.freespace","");
            }
        }
        context.setVariable("mysql.data.migration.ini",myIni);
        context.setVariable("mysql.data.migration.executable",executable);

        return true;
    }

    public static void readMyIniPath(Context context, Path myIniPath) {
        Path mySqlDataSource = myIniPath.getParent();
        context.setVariable("mysql.data.migration.source",mySqlDataSource.toAbsolutePath().toString());
        try {
            List<String> strings = Files.readAllLines(myIniPath);
            for (String string : strings) {
                if (string.startsWith("datadir=")) {
                    Util.logInfo(null,"Found dataDir "+string);
                    String dataDir = string.substring("datadir=".length()).trim();
                    context.setVariable("mysql.data.migration.source.datadir",dataDir);
                    long freeDiskSpace = SystemInfo.getFreeDiskSpace(new File(dataDir));
                    freeDiskSpace = freeDiskSpace / (1024 * 1024 * 1024);
                    context.setVariable("mysql.data.migration.source.freespace",freeDiskSpace+" GB");
                }
            }

        } catch (IOException e) {
            Util.logError(e,"Failed to read MySql ini file");
        }
    }

    public static String calculateMySQLUrl(Context context) {
        String host = (String) context.getVariable("filecluster.mysql.host");
        String port = (String) context.getVariable("filecluster.mysql.port");
        String schema = (String) context.getVariable("filecluster.mysql.schema");
        //jdbc:mysql://localhost:3306/cla?autoReconnect=true&useUnicode=true&createDatabaseIfNotExist=true&characterEncoding=utf-8
        StringBuilder sb = new StringBuilder();
        sb.append("jdbc:mysql://").append(host).append(":").append(port);
        sb.append("/").append(schema);
        sb.append("?autoReconnect=true&useUnicode=true&createDatabaseIfNotExist=true&characterEncoding=utf-8");
        Util.logInfo(null, "MySQL URL=" + sb.toString());
        return sb.toString();
    }

    public static String extractSqlInfoLines(String sqlInfo) {
        String[] split = sqlInfo.split("\n");
        StringBuilder result = new StringBuilder();
        for (String s : split) {
            if (s.contains("*")) {
                result.append(s).append("\n");
            }
        }
        String string = result.toString();
        Util.logInfo(null,"Sql INFO lines: "+string);
        return string;
    }

    public static boolean validateRequiredDependenciesExist(InstallerContext context) {
        File dependenciesFile = new File("user","dependencies.txt");
        if (!dependenciesFile.exists()) {
            Util.logError(null,"Failed to find the dependencies file. Problem in build");
            return true;
        }
        File installationDirectory = context.getInstallationDirectory();
        File dependenciesDirectory = new File(installationDirectory,"filecluster/lib");
        if (!dependenciesDirectory.exists()) {
            Util.logError(null,"Dependencies directory not found in project");
            return true;
        }
        String[] existingDependencies = dependenciesDirectory.list();
        try {
            FileReader fr = new FileReader(dependenciesFile);
            char[] buf = new char[30000];
            int read = fr.read(buf);
            String dependenciesFileContent = new String(buf,0,read);
            for (String dependency : dependenciesFileContent.split("\\n")) {
                boolean inList = verifyDependencyInList(existingDependencies, dependency);
                if (!inList) {
                    Util.showErrorMessage("Patch cannot be installed to upgrade this version." +
                            "\nA full upgrade package is required (missing dependency "+dependency+")");
                    return true; //TODO - return false to prevent patcher from continuing
                }
            }

        } catch (FileNotFoundException e) {
            Util.logError(e,"Failed to read the dependencies file (not found)");
            return true;
        } catch (IOException e) {
            Util.logError(e,"Failed to read the dependencies file");
            return true;
        }
        return true;
    }

    public static boolean verifyDependencyInList(String[] existingDependencies, String dependency) {
        for (String existingDependency : existingDependencies) {
            if (existingDependency.equals(dependency)) {
                Util.logInfo(null,"Verified dependency file "+dependency+" exists");
                return true;
            }
        }
        Util.logInfo(null,"Dependency file "+dependency+" doesn't exist");
        return false;
    }

    public static boolean waitForHttpConnection(String connectionAddress, long timeout) throws InterruptedException {
        long start = System.currentTimeMillis();
        boolean reachable = false;
        do {
            URI uri = URI.create(connectionAddress);
            String host = uri.getHost();
            int port = uri.getPort();
            reachable = true;
            try (Socket s = new Socket(host, Integer.valueOf(port))) {
            } catch (Exception e) {
                Thread.sleep(1000);
                reachable = false;
            }
        }
        while (reachable == false && System.currentTimeMillis() - start < timeout);
        return reachable;
    }

    /**
     * Called by Installer to verify that solr can be connected
     * @param connectionAddress
     * @return
     */
    public static void solrReachableUserDecision(String connectionAddress) {
        while (!isSolrReachable(connectionAddress, true, 1, 0));
    }

    public static void solrReachableUserDecision(String connectionAddress, int numRetries, long waitAfterFailureMs) {
        while (!isSolrReachable(connectionAddress, true, numRetries, waitAfterFailureMs));
    }

    /**
     * Called by Installer to verify that solr can be connected
     * @param connectionAddress
     * @return
     */
    public static boolean isSolrReachable(String connectionAddress) {
        return isSolrReachable(connectionAddress, false, 1, 0);
    }
    public static boolean isSolrReachable(String connectionAddress, int numRetries, long waitAfterFailureMs) {
        return isSolrReachable(connectionAddress, false, numRetries, waitAfterFailureMs);
    }
    public static boolean isSolrReachable(String connectionAddress, boolean isUserDecision, int numRetries, long waitAfterFailureMs) {
        int  checkCounter = 0;
        boolean connectionOk = true;
        String msg = "";
        Util.logInfo(null, "isSolrReachable(" + connectionAddress + "," + (isUserDecision?"true":"false") + "," + numRetries + "," + waitAfterFailureMs + ")");
        do {
            checkCounter++;
            if (!connectionOk && waitAfterFailureMs > 0) {
                Util.logInfo(null, "Sleep for " + waitAfterFailureMs + "ms" + "("+ checkCounter + " out of " + numRetries + ")");
                try {
                    Thread.sleep(waitAfterFailureMs);
                } catch (InterruptedException e) {}
            }
            connectionOk = true;
            if (connectionAddress.contains("http")) {
                //This is a standard Solr server
                URI uri = URI.create(connectionAddress);
                String host = uri.getHost();
                int port = uri.getPort();
                Util.logInfo(null, "Check solr connection to " + host + ":" + port);
                try (Socket s = new Socket(host, Integer.valueOf(port))) {
                } catch (UnknownHostException e) {
                    msg = "Solr host " + host + ":" + port + " is unreachable";
                    connectionOk = false;
                    Util.logInfo(null, "Check solr connection #" + checkCounter + " failed with error "+ e.getMessage());
//                    if (!isUserDecision) {
//                        Util.showErrorMessage("Solr host " + host + ":" + port + " is unreachable");
//                        return true;
//                    }
//                    return showRetryContinueMessage("Solr host " + host + ":" + port + " is unreachable");
                } catch (IOException e) {
                    msg = "Error while trying to connect to Solr host " + host + ":" + port + " (" + e.getMessage() + ")";
                    connectionOk = false;
                    Util.logInfo(null, "Check solr connection #" + checkCounter + " failed with error "+ e.getMessage());
//                    if (!isUserDecision) {
//                        Util.showErrorMessage("Error while trying to connect to Solr host " + host + ":" + port + " (" + e.getMessage() + ")");
//                        return true;
//                    }
//                    return showRetryContinueMessage("Error while trying to connect to Solr host " + host + ":" + port + " (" + e.getMessage() + ")");
                }
            } else {
                //Zookeeper server
                String[] split = connectionAddress.split("/");
                String[] split1 = split[0].split(":");
                String host = split1[0];
                String port = split1[1];
                Util.logInfo(null, "Check zookeeper connection to " + host + ":" + port);
                try (Socket s = new Socket(host, Integer.valueOf(port))) {

                } catch (UnknownHostException e) {
                    msg = "Zookeeper host " + host + ":" + port + " is unreachable";
                    connectionOk = false;
                    Util.logInfo(null, "Check zookeeper connection #" + checkCounter + " failed with error "+ e.getMessage());
//                    if (!isUserDecision) {
//                        Util.showErrorMessage("Zookeeper host " + host + ":" + port + " is unreachable");
//                        return true;
//                    }
//                    return showRetryContinueMessage("Zookeeper host " + host + ":" + port + " is unreachable");
                } catch (IOException e) {
                    msg = "Error while trying to connect to Zookeeper host " + host + ":" + port + " (" + e.getMessage() + ")";
                    connectionOk = false;
                    Util.logInfo(null, "Check zookeeper connection #" + checkCounter + " failed with error "+ e.getMessage());
//                    if (!isUserDecision) {
//                        Util.showErrorMessage("Error while trying to connect to Zookeeper host " + host + ":" + port + " (" + e.getMessage() + ")");
//                        return true;
//                    }
//                    return showRetryContinueMessage("Error while trying to connect to Zookeeper host " + host + ":" + port + " (" + e.getMessage() + ")");
                }
            }
        } while (!connectionOk && checkCounter<numRetries);
        if (!connectionOk) {
            if (!isUserDecision) {
                Util.showErrorMessage(msg);
                return true;
            }
            return showRetryContinueMessage(msg);
        }
        return true;
    }

    private static boolean showRetryContinueMessage(String message) {
        // 'Continue' = true
        String[] options = new String[] {"Retry","Continue"};
        try {
            int response = Util.showOptionDialog(message, options, JOptionPane.WARNING_MESSAGE);
            if (response == 1) {
                return true;
            }
        } catch (UserCanceledException e) {
            return true;
        }
        return false;
    }


    public static boolean isActiveMQReachable(String connectionAddress) {
        //Zookeeper server
        String[] split = connectionAddress.split("/");
        String[] split1 = split[0].split(":");
        String host = split1[0];
        String port = split1[1];
        try (Socket s = new Socket(host, Integer.valueOf(port))) {

        } catch (UnknownHostException e) {
            Util.showErrorMessage("ActiveMQ address "+host+":"+port+" is unreachable");
            return false;
        } catch (IOException e) {
            Util.showErrorMessage("Error while trying to connect to ActiveMQ host "+host+":"+port+" ("+e.getMessage()+")");
            return false;
        }
        return true;
    }

    public static boolean isKafkaReachable(String connectionAddress) {
        //Zookeeper server
        String[] split = connectionAddress.split("/");
        String[] split1 = split[0].split(":");
        String host = split1[0];
        String port = split1[1];
        try (Socket s = new Socket(host, Integer.valueOf(port))) {

        } catch (UnknownHostException e) {
            Util.showErrorMessage("Kafka address "+host+":"+port+" is unreachable");
            return false;
        } catch (IOException e) {
            Util.showErrorMessage("Error while trying to connect to kafka host "+host+":"+port+" ("+e.getMessage()+")");
            return false;
        }
        return true;
    }

    static final String[] preferredRoots = {"D:\\", "E:\\", "F:\\"};
    static final String[] preferredFolders = {"da-data", "da_date", "data"};

    public static void determineSolrDefaultDataDir(InstallerContext context) {
        try {
            String df = null;
            for (String pf : preferredFolders) {
                for (String pr : preferredRoots) {
                    if (df == null && Files.isDirectory(Paths.get(pr + pf))) {
                        df = pr + pf + "\\solr";
                        Util.logInfo(null,"Found preferred folder "+ df);
                    }
                }
            }
            if (df == null) {
                //CHECK IF DRIVE D exists
                //If drive D exists, set solr.data.directory.manual to true and solr.data.directory to d:\dadata\solr

                File[] roots = File.listRoots();
                for (File root : roots) {
                    if (df == null && "D:\\".equalsIgnoreCase(root.getPath())) {
                        Util.logInfo(null,"Found file root "+ root.getPath());
                        df = "D:\\da-data\\solr";
                    }
                }
            }
            if (df != null) {
                context.setVariable("solr.data.directory", df);
                context.setVariable("solr.data.directory.manual", true);
            }
        } catch (Exception e) {
            Util.logError(e,"Failed to determine solr default data dir");
        }
    }

    /**
     * Creating new collection 'AnalysisData' using command:
     http://localhost:8983/solr/admin/collections?action=CREATE&name=AnalysisData&numShards=1&replicationFactor=1&maxShardsPerNode=1&collection.configName=AnalysisData
     {
     "responseHeader":{
     "status":0,
     "QTime":4161},
     "success":{"172.31.0.197:8983_solr":{
     "responseHeader":{
     "status":0,
     "QTime":2916},
     "core":"AnalysisData_shard1_replica1"}}}
     * @param context
     * @return
     */
    public static boolean verifySolrCoresConfigured(InstallerContext context) {
        File installDir = context.getInstallationDirectory();
        File stdout = new File(installDir,"installation_logs\\configure_solr_cores.stdout");

        if (stdout.exists()) {
            //TODO 2.0 read the file and process the output
        }
        return true;

    }


    public static String getDefaultLocalHostName() {
        String bestAddress = "localhost";
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            bestAddress = localhost.getCanonicalHostName();
            // Just in case this host has multiple IP addresses....
            InetAddress[] allMyIps = InetAddress.getAllByName(localhost.getCanonicalHostName());
            if (allMyIps != null && allMyIps.length > 1) {
                for (InetAddress myIp : allMyIps) {
                    Util.logInfo(null,myIp.getCanonicalHostName());
                }
            }
        } catch (UnknownHostException e) {
        }
        return bestAddress;
    }

    public static void calcluateSolrAddress(InstallerContext context) {
        String solrAddress = (String)context.getVariable("solr.address");
        if (solrAddress == null || solrAddress.length() == 0) {
            boolean cloud = context.getBooleanVariable("solr.cloud.install");
            Util.logInfo(null,"Automatically set solr address (cloud = "+cloud+")");
            if (cloud) {
                String zookeeperAddress = (String)context.getVariable("solr.zookeeper.address");
                context.setVariable("solr.address",zookeeperAddress);
            }
            else {
                context.setVariable("solr.address","http://localhost:${installer:solr.service.port.http}/solr");
            }
        }
    }
}
