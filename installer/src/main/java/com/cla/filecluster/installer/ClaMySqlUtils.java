package com.cla.filecluster.installer;

import com.install4j.api.Util;
import com.install4j.api.context.InstallerContext;
import com.install4j.api.context.UserCanceledException;

import javax.swing.*;
import java.io.File;
import java.io.FilenameFilter;
import java.sql.*;

/**
 * Created by uri on 14/10/2015.
 */
public class ClaMySqlUtils {

    public static void dropMySqlSchema(InstallerContext context) {
        String url = ClaInstallUtils.calculateMySQLUrl(context);
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception e) {
            Util.showErrorMessage("Cannot connect the database! ("+e.getMessage()+") "+e.getClass().getName());
        }

        String user = context.getVariable("filecluster.mysql.user").toString();
        String password = context.getVariable("filecluster.mysql.password").toString();
        String schema = context.getVariable("filecluster.mysql.schema").toString();
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, user, password);
            Util.logInfo(null, "Database connected!");
            Statement stmt = connection.createStatement();
            String sql = "drop schema if exists "+schema;
            stmt.execute(sql);
        } catch (SQLException e) {
            Util.showErrorMessage("Cannot drop the schema on the database! ("+e.getMessage()+") "+e.getClass().getName());
        }
    }

    /**
     * Called by install4J
     * @param context
     * @return
     */
    public static boolean testMySqlConnection(InstallerContext context) {
        String url = ClaInstallUtils.calculateMySQLUrl(context);
        Util.logInfo(null,"Validating MySql URL: "+url);
        context.setVariable("filecluster.mysql.url",url);
        context.setVariable("filecluster.mysql.schema.reset",true);
        // Load the Connector/J driver
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            Util.showErrorMessage("Cannot connect the database! ("+e.getMessage()+") "+e.getClass().getName());
        } catch (IllegalAccessException e) {
            Util.showErrorMessage("Cannot connect the database! ("+e.getMessage()+") "+e.getClass().getName());
        } catch (ClassNotFoundException e) {
            Util.showErrorMessage("Cannot connect the database! ("+e.getMessage()+") "+e.getClass().getName());
        }

        // Establish connection to MySQL
        String user = context.getVariable("filecluster.mysql.user").toString();
        String password = context.getVariable("filecluster.mysql.password").toString();
        String schema = context.getVariable("filecluster.mysql.schema").toString();
        Connection connection;
        try {
            DriverManager.setLoginTimeout(10);
            connection = DriverManager.getConnection(url, user, password);
            Util.logInfo(null, "Database connected!");
        } catch (SQLException e) {
            Util.showErrorMessage("Cannot connect the database! (" + e.getMessage() + "). Error code: "+e.getErrorCode()+" (exception: "+ e.getClass().getName()+")");
            return false;
        }
        context.setVariable("filecluster.mysql.schema.exists",false);
        boolean isInitDB = context.getBooleanVariable("filecluster.isInitDB");
        try {
            Statement stmt = connection.createStatement();
            String sql;
            sql = "show tables";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                //Retrieve by column name
                String name = rs.getString(1);
                if ("users".equalsIgnoreCase(name)) {
                    context.setVariable("filecluster.mysql.schema.exists",true);
                    context.setVariable("filecluster.mysql.schema.reset",false);
                    if (!isInitDB) {
                        return true;
                    }
                    if (showContinueCancelMessage("Schema " + schema + " already exists on mySql server.")) {
                        return false;
                    }
                }
            }
        } catch (SQLException e) {
            Util.showErrorMessage("Cannot check if the database already has defined schema ("+e.getMessage()+") "+e.getClass().getName());
        }
        if (!isInitDB && showContinueCancelMessage("Schema " + schema + " doesn't exists on mySql server. Schema yet to be created...")) {
            return false;
        }
        return true;
    }

    public static int getNumberOfUpgradeScripts(InstallerContext context) {
        File scriptDir = new File(context.getInstallationDirectory(),"filecluster"+File.separator+"upgrade-mysql");
        Util.logInfo(null,"Looking for upgrade-scripts under: "+scriptDir.getPath());
        if (scriptDir.exists() && scriptDir.isDirectory()) {
            Util.logInfo(null,"Found upgrade-mysql dir");
            FilenameFilter filenameFilter = getSqlFilter();
            return scriptDir.list(filenameFilter).length;
        }
        else {
            Util.logInfo(null,"Failed to find upgrade-scripts directory");
        }
        return 0;
    }

    public static String getUpgradeScript(InstallerContext context, int number) {
        File scriptDir = new File(context.getInstallationDirectory(),"filecluster"+File.separator+"upgrade-mysql");
        FilenameFilter filenameFilter = getSqlFilter();
        File s = scriptDir.listFiles(filenameFilter)[number];
        Util.logInfo(null,"Upgrade script: "+s.getAbsolutePath());
        return s.getAbsolutePath();
    }

    private static FilenameFilter getSqlFilter() {
        return new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.endsWith(".sql");
                }
            };
    }

    private static boolean showContinueCancelMessage(String message) {
        // 'Cancel' = true
        String[] options = new String[] {"Continue","Cancel"};
        try {
            int response = Util.showOptionDialog(message, options, JOptionPane.WARNING_MESSAGE);
            if (response == 1) {
                return true;
            }
        } catch (UserCanceledException e) {
            return true;
        }
        return false;
    }
}
