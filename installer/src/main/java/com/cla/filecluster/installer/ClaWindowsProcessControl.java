package com.cla.filecluster.installer;

import com.install4j.api.Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClaWindowsProcessControl {

    public static boolean isProcessRuinning(String imageName) {
        boolean processRunning = false;
        Process proc = null;
        String[] killMediaProcessorScript = new String[]{"cmd.exe", "/c", "tasklist", "/NH", "/FO", "CSV", "|", "find", "/C", '"' + imageName + '"'};  //to list processes and find name
        try {
            proc = Runtime.getRuntime().exec(killMediaProcessorScript);
            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            List<String> lines = new ArrayList<String>();
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
            String reason = lines.stream().collect(Collectors.joining("\n"));
//            System.out.println(reason);
            proc.waitFor();
            processRunning = proc.exitValue() == 0;
        } catch (IOException e) {
            Util.logError(null, String.format("Failed to run exec to list process %s, reason: %s", imageName, e.getMessage()));
        } catch (InterruptedException e) {
            Util.logError(null, String.format("Interrupted will waiting for list exec on service %s to finish , reason: %s", imageName, e.getMessage()));
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }
        return processRunning;
    }

    public static boolean killProcess(String imageName) {
        boolean processKilled = false;
        Process proc = null;
        String[] killMediaProcessorScript = new String[]{"cmd.exe", "/c", "taskkill", "/F", "/IM", imageName};  //to kill process
        try {
            proc = Runtime.getRuntime().exec(killMediaProcessorScript);
            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            List<String> lines = new ArrayList<String>();
            String line = br.readLine();
            while (line != null) {
                lines.add(line);
                line = br.readLine();
            }
            String reason = lines.stream().collect(Collectors.joining("\n"));
//            System.out.println(reason);
            proc.waitFor();
            processKilled = proc.exitValue() == 0;
            if (!processKilled) {
                Util.logError(null, String.format("Failed to kill process %s, reason: %s", imageName, reason));
            } else {
                Util.logInfo(null, String.format("Killed process %s successfully", imageName));
            }
        } catch (IOException e) {
            Util.logError(null, String.format("Failed to run exec to kill process %s, reason: %s", imageName, e.getMessage()));
        } catch (InterruptedException e) {
            Util.logError(null, String.format("Interrupted will waiting for kill exec on service %s to finish , reason: %s", imageName, e.getMessage()));
        } finally {
            if (proc != null) {
                proc.destroy();
            }
        }
        return processKilled;
    }

}