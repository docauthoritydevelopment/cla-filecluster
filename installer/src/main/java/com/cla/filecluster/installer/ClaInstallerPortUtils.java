package com.cla.filecluster.installer;

import com.install4j.api.Util;
import com.install4j.api.context.InstallerContext;

/**
 * Created by uri on 13/07/2015.
 */
public class ClaInstallerPortUtils {

    public static void calculateAllExtraPorts(InstallerContext context, String serviceName) {
        try {
            Long httpPort = (Long)context.getVariable(serviceName+".service.port.http");
            //Default for AJP is 8009
            context.setVariable(serviceName+".service.port.ajp",httpPort-71);
            //Default for shutdown is 8005
            context.setVariable(serviceName+".service.port.shutdown",httpPort-75);
        } catch (Exception e) {
            Util.logError(e,"Caught exception while calculating all extra ports ("+e.getMessage()+")");
            String stackTrace = Util.getAnnotatedStackTrace(e);
            Util.showErrorMessage("Caught exception while calculating all extra ports ("+e.getMessage()+")\n"+stackTrace);
        }
    }
}
