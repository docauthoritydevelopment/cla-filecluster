package com.cla.filecluster.installer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created By Itai Marko
 */
class PropertiesUpdater {

    static final String SEPARATING_LINE = "# ============================================================================================================================================";

    private final Clock clock;
    private Set<String> alreadyExistingPropKeys;
    private StringBuilder additionBuilder;

    private PropertiesUpdater(Clock clock) {
        this.clock = Objects.requireNonNull(clock);
    }

    static ExistingPropsExtractor create() {
        return create(Clock.systemDefaultZone());
    }
    static ExistingPropsExtractor create(Clock clock) {
        return new Builder(clock);
    }

    private static class Builder implements ExistingPropsExtractor, NewlyAddedPropsExtractor, UpdateMarkInserter {

        private final PropertiesUpdater propertiesUpdater;

        private Builder(Clock clock) {
            propertiesUpdater = new PropertiesUpdater(clock);
        }

        @Override
        public NewlyAddedPropsExtractor extractExistingPropKeys(BufferedReader oldPropsReader) throws IOException {
            propertiesUpdater.alreadyExistingPropKeys = new HashSet<>();
            String line;
            while ((line = oldPropsReader.readLine()) != null) {
                int i = indexOfFirstNonWhitespaceChar(line);
                if (i == -1 || isCommentIndicator(line.charAt(i))) {
                    continue;
                }
                line = line.substring(i);
                propertiesUpdater.alreadyExistingPropKeys.add(extractPropertyKey(line));
            }
            return this;
        }

        @Override
        public UpdateMarkInserter extractNewlyAddedProps(BufferedReader newPropsReader) throws IOException {
            if (propertiesUpdater.alreadyExistingPropKeys == null)
                throw new IllegalArgumentException("extractExistingPropKeys() must be called first");

            propertiesUpdater.additionBuilder = new StringBuilder();
            String line;
            List<String> lines = new ArrayList<>(800); // big enough to hold our appserver's application.properties for the near future
            while ((line = newPropsReader.readLine()) != null) {
                lines.add(line);
            }

            boolean lastLineWasNewlyAdded = false;
            for (int i = lines.size()-1; i >= 0; i--) {
                line = lines.get(i);
                int j = indexOfFirstNonWhitespaceChar(line);
                if (j == -1) { // an empty or whitespaces line
                    lastLineWasNewlyAdded = false;
                    continue;
                }
                if (isCommentIndicator(line.charAt(j))) { // a comment line
                    if (lastLineWasNewlyAdded) { // add comments that precede a newly added property
                        propertiesUpdater.additionBuilder.insert(0, line + System.lineSeparator());
                    }
                } else { // a property line
                    String key = extractPropertyKey(line.substring(j));
                    if (propertiesUpdater.alreadyExistingPropKeys.contains(key)) {
                        lastLineWasNewlyAdded = false;
                    } else {
                        propertiesUpdater.additionBuilder.insert(0, line + System.lineSeparator());
                        lastLineWasNewlyAdded = true;
                    }
                }
            }
            return this;
        }

        @Override
        public PropertiesUpdater generateUpdateMark(String oldVersion, String newVersion) {
            propertiesUpdater.additionBuilder.insert(0, createUpdateMark(oldVersion, newVersion, propertiesUpdater.clock));
            return propertiesUpdater;
        }

        @Override
        public Set<String> getExistingPropKeys() {
            return propertiesUpdater.alreadyExistingPropKeys;
        }

        @Override
        public String getNewlyAddedProps() {
            return propertiesUpdater.additionBuilder.toString();
        }

        private static int indexOfFirstNonWhitespaceChar(String line) {
            for (int i = 0; i < line.length() ; i++) {
                if (!isWhiteSpace(line.charAt(i))) {
                    return i;
                }
            }
            return -1;
        }

        private static String extractPropertyKey(String line) {
            int i = indexOfKeyTerminator(line);
            return line.substring(0, i);
        }

        private static int indexOfKeyTerminator(String line) {
            boolean isEscaped = false;
            int i=0;
            for(; i < line.length(); i++) {
                char c = line.charAt(i);
                if (isKeyTerminator(c) && !isEscaped) {
                    return i;
                }
                if (c == '\\') {
                    isEscaped = !isEscaped;
                }
                else {
                    isEscaped = false;
                }
            }
            return i;
        }

        private static boolean isWhiteSpace(char c) {
            return c == ' ' || c == '\t' || c == '\f';
        }

        private static boolean isCommentIndicator(char c) {
            return c == '#' || c == '!';
        }

        private static boolean isKeyTerminator(char c) {
            return c == '=' || c == ':' || isWhiteSpace(c);
        }

        private static String createUpdateMark(String oldVersion, String newVersion, Clock clock) {
            return System.lineSeparator() +
            SEPARATING_LINE +
            System.lineSeparator() +
            "# Updated on " + ZonedDateTime.now(clock).format(DateTimeFormatter.RFC_1123_DATE_TIME) +
            " from " + oldVersion +
            " to " + newVersion +
            System.lineSeparator() +
            SEPARATING_LINE +
            System.lineSeparator() +
            System.lineSeparator();
        }
    }

    void update(BufferedWriter updater) throws IOException {
        updater.append(additionBuilder);
        updater.flush();
    }

    String getNewlyAddedProps() {
        return additionBuilder.toString();
    }

    interface ExistingPropsExtractor {
        NewlyAddedPropsExtractor extractExistingPropKeys(BufferedReader oldPropsReader) throws IOException;
    }

    interface NewlyAddedPropsExtractor {
        UpdateMarkInserter extractNewlyAddedProps(BufferedReader newPropsReader) throws IOException;
        Set<String> getExistingPropKeys();
    }

    interface UpdateMarkInserter {
        PropertiesUpdater generateUpdateMark(String oldVersion, String newVersion);
        String getNewlyAddedProps();
    }
}
