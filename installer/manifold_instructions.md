* Download latest ManifoldCF bindaries and put under 3rd-party
* Download latest ManifoldCF library bindaries and put under 3rd-party
* Merge changes of example dir contents to installer/mcf/bin dir (merge smartly)

* Upload mcf libraries (mcf....jar) from /lib to nexus 3rd-party repo
* Upload mcf connector-common library from /connector-common-lib to nexus 3rd-party repo
* Update manifold version in the build.mcf file
 
See: 
* https://manifoldcf.apache.org/release/trunk/en_US/how-to-build-and-deploy.html#Running+ManifoldCF