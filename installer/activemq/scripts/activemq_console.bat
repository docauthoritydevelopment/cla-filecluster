set DIRNAME=%~dp0
set ACTIVEMQ_DIR=%DIRNAME%\apache-activemq
set "JRE_HOME=%~dp0%jre"
set JAVA_HOME=%JRE_HOME%

rem set ACTIVEMQ_CONF=%DIRNAME%\..\installer\activemq\conf
rem set ACTIVEMQ_SUNJMX_START=-Dcom.sun.management.jmxremote.port=7044 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

pushd %ACTIVEMQ_DIR%
call bin\activemq start
popd