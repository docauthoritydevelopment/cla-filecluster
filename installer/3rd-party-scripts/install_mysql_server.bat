@echo off
if NOT [%1] == [install-mysql] goto usage
@echo on
set datadirConf=
if [%2] == [] goto usage
if [%3] == [] goto execute
set "datadirConf=;datadir=%3"
SHIFT
if [%3] == [] goto execute
set "appUserConf=:type=user;username=da_user;password=%3;role=DBManager"

:execute
set DIRNAME=%~dp0
set password=%2
set installer_msi="%~dp0\mysql-installer-%edition%-%version%.0.msi"
REM gradleset installer="%DIRNAME%\MySQL Installer for Windows\MySQLInstallerConsole.exe"
rem set installer="C:\Program Files (x86)\MySQL\MySQL Installer for Windows\MySQLInstallerConsole.exe"
set installer="%ProgramFiles(x86)%\MySQL\MySQL Installer for Windows\MySQLInstallerConsole.exe"
set "version=5.7.20"
set "edition=commercial"

echo installing mysql installer
msiexec /i %installer_msi% /quiet

REM echo installing mysql server files
REM msiexec /i server_5625_winx64 /quiet

echo installing mysql service instance
REM %installer% install server;%version%;X64:*:port=3307;openfirewall=true;passwd=%password%%datadirConf% -silent
%installer% install -silent server;%version%;x64:*:type=config;enable_tcpip=true;port=3306;rootpasswd=%password%%datadirConf%%appUserConf%

goto finally

:usage
@echo off
echo. 
echo ====================================================== 
echo. 
echo    MySQL server - DocAuthority install script
echo.
echo    Usage %0 install-mysql [root password] [datadir] [app-user password]
echo. 
echo. 
echo    Datadir and app-user password parameters are optional
echo. 
echo =====================================================
echo. 
goto finally

:finally
exit /b 0
