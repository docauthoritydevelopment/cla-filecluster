@echo off
if '%1' == '' goto usage

set DIRNAME=%~dp0
echo "Installing vcredist"
call %DIRNAME%\vcredist_x64.exe /passive /norestart
echo "Installing MySQL Server"
call install_mysql_server.bat
echo "DONE"
goto finally
:usage
@echo off
echo. 
echo ====================================================== 
echo. 
echo    DocAuthority 3rd party install script
echo. 
echo. 
echo    Usage %0 [mysql root password] [mysql datadir]
echo. 
echo. 
echo    mysql Datadir parameter is optional 
echo. 
echo =====================================================
echo. 
goto finally

:finally