@ECHO OFF
set DIRNAME=%~dp0
IF "%OS%"=="Windows_NT" setlocal enabledelayedexpansion enableextensions

set DISABLE_VAL=
if [%1]==[-h] goto help
if [%1]==[set] ( 
	set DISABLE_VAL=0
	goto update
)
if [%1]==[clear] (
	set DISABLE_VAL=1
	goto update
)
if [%1]==[query] goto query
if NOT [%1]==[] goto help
goto query

:update
fsutil behavior set disablelastaccess %DISABLE_VAL%
IF %ERRORLEVEL% NEQ 0 ECHO Exited with code %ERRORLEVEL% . Try run as administrator.
goto end

:query
set STATE=OFF
FOR /F "tokens=3" %%G IN ( 'fsutil behavior query disablelastaccess' ) DO ( IF [%%G]==[0] set STATE=ON )
REM fsutil behavior query disablelastaccess
ECHO file-system access-time support is %STATE%
goto end

:help
echo Usage:
echo %0         		- Display current access-time support status
echo %0 set/clear 		- activate/disable access-time support

:end
endlocal
exit /b 0
