DocAuthority Instllation Checklist
													March, 2018

1. Pre-installation:
	a. Preparation
		- Set folders view options: show hidden, don't hide extensions
		- Identify downloaded SW packages location
		- Identify installation folder location
		- Identify MySql and Solr data location
		- Identify service account username and password (verify it is a domain account)
		- Check CPU, mem, disk + write down.
		- Install/verify: firefox/chrome, PDF reader, office
		- Identify network interfaces and plan internal and external connectivity interfaces

	b. Server configuration:
		1) Exclude data + log folders from AntiVirus/Anti-malware/endpoint-protection tools
		2) Set service account full-control permission to the installation folder
			 icacls <install-dir> /grant <DOMAIN\service-account>:F /Q

2. Tools installation:
	1) Notepad++
	npp.7.5.1.Installer.x64.exe

3. DB installation:
	1) Extrat MySql ZIPs

	2)  Modify root and da_app DB passowrds in the command line below
		Update datadir location in the command below

	3) Run sequencially:
		a. MS Visual C++ runtime redistributable
			vc_redist_2015.x64.exe
		b. MySql Installer
			msiexec /i "MySQL Installer V971123-01\mysql-installer-commercial-5.7.20.0.msi" /quiet
		c. MySql Server
			"C:\Program Files (x86)\MySQL\MySQL Installer for Windows\MySQLInstallerConsole.exe" install server;5.7.20;x64:*:type=config;enable_tcpip=true;port=3306;rootpasswd=root;datadir="D:\data\mysql":type=user;username=da_app;password=da_app;role=DBManager -silent
		d. MySql Workbench package
			msiexec /i "MySQL Workbench 6.3.9 MSI V841085-01\mysql-workbench-commercial-6.3.9-winx64.msi" /quiet
		e. MySql Workbench install
			"C:\Program Files (x86)\MySQL\MySQL Installer for Windows\MySQLInstallerConsole.exe" install workbench;6.3.9;x64  -silent
			
		f. Activate MySQL Workbench to verify DB instllation
		
		g. Update da_app user priviliges:
			GRANT CREATE ROUTINE,ALTER ROUTINE,EXECUTE,REFERENCES ON *.* TO 'da_app'@'%';

			-- SET GLOBAL log_bin_trust_function_creators = 1; -- (if not set at my.ini)

		h. Tune DB:
			1) Stop MySql service, copy data dir to new location
				C:\ProgramData\MySQL\MySQL Server 5.7\my.ini
				max_allowed_packet=32M
				tmp_table_size=774M
				innodb_log_buffer_size=16M
				innodb_buffer_pool_size=1G
				datadir=<data-dir-location>

			2) Start MySql service, verify DB connectivity

2. Install DA software:
	a. Install readiness tool
		DA_ScanReadinessTool_x_x_x_xxxx.exe

	b. Install product
		DocAuthority_windows-x64_x_x_x_xxxx.exe	

3. Post installation:
	a. Backup ZK configuration
	b. Update DB Password
	c. Set fs preserve accessTime mode and restart
	d. Update MP application.properties to preserve-access-time
	- Review search patterns configuration - UK applicable settings
	- UI SSL certificate (*.intra.saga.com) + setup internal DNS name
	- Double check: Exclude data + log folders from AntiVirus scan
	- Get ServerId to allow license key generation
	- Install license key
	
4. Run test scans:
	- Verify permissions
	- Verify access time preservations
	- Reset DB

4. ActiveDirectory integration planning: 
	- Identify domain names (internal + external)
	- Update app.properties: ldap.user-tokens.add-domain-prefix.map=testing.da.com:TESTING
	- Prepare AD access credentials
	- Plan roles related AD groups
	- Review default user access-permissions
	
	
	
Usefull links:
MS File Checksum Integrity Verifier utility
	http://download.microsoft.com/download/c/f/4/cf454ae0-a4bb-4123-8333-a1b6737712f7/windows-kb841290-x86-enu.exe
	