@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  Get components logs as ZIP (script for Windows)
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
if '%1' == '-h' goto usage
if '%1' == '-help' goto usage
if '%1' == '--help' goto usage

powershell.exe -ExecutionPolicy Unrestricted "& '%DIRNAME%\da_getlogs.ps1' %*"
goto finally

:usage
echo Usage:
echo    %0  options
echo.
echo    Options:
echo        -target [target-log-zip-location]     default: C:\Temp\yyyymmdd
echo        -installdir [da-install-locaion]  default: C:\DocAuthority
echo        -prefix [zip-files-prefix]        default: da
echo        -daystokeep [num-of-days]         get only files modified within the recent days. Default: no limit
echo        -maxsize [max-size-MB]            get only files smaller than specified value in MB. Default: no limit
goto finally

:finally
