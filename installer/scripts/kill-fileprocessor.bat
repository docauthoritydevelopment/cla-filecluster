@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  Force kill of the file-processor service processes (which should be immediatly restarted by service manager)
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

if NOT '%1' == 'do-kill' goto usage

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%~dp0

set LOG_FILE="logs\kill_fileprocessor.log"
set IMAGE_TO_KILL=DocAuthority-fileprocessor.exe
set OUTPUT_LOG="logs\output.log"

:init
ECHO Creating file to alert fileProcessing it should continue crawling
copy /Y logs\file_processor_operation.log logs\outofmemory.log || ( ECHO %time% ERROR [%0] Could not copy file_processor_operation.log (%ERRORLEVEL%) >> %OUTPUT_LOG% )
:doKill
ECHO %time% INFO "Killing %IMAGE_TO_KILL%"
taskkill /F /IM %IMAGE_TO_KILL% >> %LOG_FILE% || ( ECHO %time% ERROR [%0] Could not taskkill %IMAGE_TO_KILL% (%ERRORLEVEL%) >> %OUTPUT_LOG% )
timeout /t 10 /NOBREAK
tasklist  /FI "IMAGENAME eq %IMAGE_TO_KILL%" | find /I "docau" >> %LOG_FILE%
if "%ERRORLEVEL%"=="0" goto end
ECHO %time% ERROR "Service processes did not restarted after kill" >> %LOG_FILE%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable CLA_FILECLUSTER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%CLA_FILECLUSTER_EXIT_CONSOLE%" exit 1
exit /b 1

:usage
echo Usage:
echo        %0 do-kill
exit /b 2

:mainEnd
