@ECHO off
REM ##########################################################################
REM
REM  Run Upgrade scripts
REM
REM ##########################################################################

REM Set local scope for the variables with windows NT shell
REM if "%OS%"=="Windows_NT" setlocal
setlocal

set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%

if NOT '%1' == 'execute' (
    if NOT '%1' == 'baseline' (
        if NOT '%1' == 'drop-schema' (
            if NOT '%1' == 'solr' (
                if NOT '%1' == 'info' goto usage
            )
        )
    )
)

IF NOT [%1] == [solr] (
echo.
echo DocAuthority Upgrade scripts executor
echo Start processing...
echo     action=%1
echo     database-url=%2
echo     user=%3
echo.
echo Initialising...
)

if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%~dp0\upgradeTools
REM echo APP_HOME=%APP_HOME%

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"

set "JAVA_OPTS="
@rem Find java.exe
SET JAVA_HOME_LOCAL=%DIRNAME%\..\jre
set JAVA_EXE=%JAVA_HOME_LOCAL%\bin\java.exe
if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA installation not found (%JAVA_HOME_LOCAL%)
goto fail

:init
set CLASSPATH=%APP_HOME%\lib\*
@rem Execute cla-filecluster
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% -Dlog4j.configurationFile="%CLA_HOME%\upgradeTool_log4j2.xml" -classpath "%CLASSPATH%" com.cla.filecluster.scripts.RunUpgradeSqlScripts %*

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable CLA_FILECLUSTER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%CLA_FILECLUSTER_EXIT_CONSOLE%" exit 1
exit /B 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal
goto finally

:usage
echo. 
echo DocAuthority Database upgrade tool
echo. 
echo Usage:
echo     %0 [action] 
echo     %0 [action] [database-url] [user] [password]
echo. 
echo     [action]           - The action to do: [execute,info,baseline,solr]
echo                        [execute]: run all upgrade scripts not previously executed
echo                        [solr]: run specific solr-cloud operations:
echo                                create-collection-if-needed, create-collection, delete-collections,
echo                                reload-collections, reload-collection
echo                        [info]: see a list of all pending upgrade scripts
echo                        [baseline]: run the baseline script (part of basic installation)
echo     [database-url] 	- A full JDBC url of the database: (jdbc:mysql://localhost:3306/docauthority)
goto finally

:finally
exit /B 0