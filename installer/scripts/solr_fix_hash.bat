@ECHO off
REM ##########################################################################
REM
REM  Run fix doc folder and file hash in solr
REM
REM ##########################################################################

REM Set local scope for the variables with windows NT shell
REM if "%OS%"=="Windows_NT" setlocal
setlocal
if NOT '%1' == 'do-fix' goto usage
REM Note that SHIFT impact %1 %2 ... but not %*
SHIFT

set argC=0
for %%x in (%*) do Set /A argC+=1

IF %argC% GTR 10 goto usage

set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%

if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%~dp0
REM echo APP_HOME=%APP_HOME%

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"

set "JAVA_OPTS="
@rem Find java.exe
SET JAVA_HOME_LOCAL=%DIRNAME%\..\jre
set JAVA_EXE=%JAVA_HOME_LOCAL%\bin\java.exe
if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA installation not found (%JAVA_HOME_LOCAL%)
goto fail

:init
set CLASSPATH=%APP_HOME%\lib\*
@rem Execute cla-filecluster
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% -classpath "%CLASSPATH%" com.cla.filecluster.rfshifter.RecalcHashFilesFolders %1 %2 %3 %4 %5 %6 %7 %8

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable CLA_FILECLUSTER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%CLA_FILECLUSTER_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal
goto finally

:usage
echo.
echo DocAuthority Solr fix doc folder and file hash if needed.
echo.
echo Attention:
echo   Tool should be used and activated by authorised personnel and after reading the related documentation !!
echo.
echo Usage:
echo     %0 do-fix [config path optional]
echo.
goto finally

:finally
