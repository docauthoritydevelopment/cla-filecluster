#
# This file contains DocAuthority specific MySql tuning parameters
#
# The primary my.ini file should be updated with the following line adding the below configuration into the MySqld configuration.
# !include <full-path>	
# For example:
# !include D:/da-data/mysql/config/da_my-ini_config.txt
[mysqld]

# The following line is system specific. Size of memory should be set to about 70% of the machine's phisical memory for a MySql dedicated machine deployment.
innodb_buffer_pool_size=8G

query_cache_type=0
query_cache_size=0

table_open_cache=2000
tmp_table_size=774M
thread_cache_size=10
myisam_max_sort_file_size=100G

myisam_sort_buffer_size=114M

key_buffer_size=8M
read_buffer_size=64K
read_rnd_buffer_size=256K

innodb_file_format=Barracuda
innodb_log_buffer_size=32M
innodb_flush_log_at_trx_commit=2
max_connect_errors=10000000
max_connections=451
binlog_cache_size=1M
max_heap_table_size=500M
sort_buffer_size=5M
join_buffer_size=5M
innodb_log_file_size=300M
innodb_max_dirty_pages_pct=90
log_bin_trust_function_creators=1

##BINLOGS
expire_logs_days=12
max_binlog_size=100M
log-slave-updates=true

slave-skip-errors=0,1062
relay-log=server01-relay-bin
relay-log-index=server01-relay-bin.index
log-bin=server01-bin
binlog_format=row
innodb_locks_unsafe_for_binlog=1
