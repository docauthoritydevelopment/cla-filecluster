﻿###
# Take snapshot of components configuration on a specific server/node, mainly for backup.
# Written by Neil R. Dec-2017
#
# Example:
#     powershell.exe -ExecutionPolicy Unrestricted "& '.\da_bu_config.ps1' -target myZipLocation -installdir D:\DocAuthority -prefix newCo"
#
###
#
param(
#Set a Zip File Prefix if needed.
    [string]$prefix = 'da',
#Set the Installation Folder
    [string]$installdir = 'C:\DocAuthority',
#Set the Output Folder
    [string]$target = '',
#Set number of days to keep - 0 - all
    [int]$daystokeep,
#Set the maximum size a log file should be.
#So if the log file is smaller than the maxsize in MB specified it will be grabbed.
    [int]$maxsize
)

$da_timestamp = Get-Date -Format yyMMdd-HHmm
$da_dstfolder = $target
if ($da_dstfolder -eq '') {
    $da_dstfolder = 'C:\TEMP\DA_Conf_BU_' + $da_timestamp
}

#Let's set some variables ;)
$retrieve_default = $true
$da_dirstoget = @{
    ui = @{
        name = 'DocAuthority User Interface';
        srcfolder = 'da-ui\server\config';
        retrieve = $retrieve_default
    };
    mng = @{
        name = 'DocAuthority Management Agent';
        srcfolder = 'mngAgent\config';
        retrieve = $retrieve_default
    };
    mp = @{
        name = 'DocAuthority Media Processor';
        srcfolder = 'mediaProcessor\config';
        retrieve = $retrieve_default
    };
    fc = @{
        name = 'DocAuthority FileCluster';
        srcfolder = 'filecluster\config';
        retrieve = $retrieve_default
    };
    amq = @{
        name = 'DocAuthority Apache ActiveMQ';
        srcfolder = 'apache-activemq\conf';
        retrieve = $retrieve_default
    };
    zk = @{
        name = 'DocAuthority ZooKeeper';
        srcfolder = 'zookeeper\conf';
        retrieve = $retrieve_default
    };
    solr = @{
        name = 'DocAuthority Solr';
        srcfolder = 'solr-7.7.2\server\logs';
        retrieve = $retrieve_default
    }
}

$da_continue = $true
If(-Not (Test-path $installdir)) {
    "Installation dir $installdir doesn't exist"
    $da_continue = $false
}
#Let's get some logs if the user has selected Y
If($da_continue) {
    ""
    If(-Not (Test-path $da_dstfolder)) {
        "Creating packed logs destination folder: $da_dstfolder"
        mkdir $da_dstfolder > $null
    } else {
        "Packed logs destination folder: $da_dstfolder"
    }
    ""

    #Zipping!
    $da_tmpsrcfolder = $da_dstfolder + "\_temp"
    If(-Not (Test-path $da_tmpsrcfolder)) {mkdir $da_tmpsrcfolder > $null}
    $da_zipbasename =  $prefix + "_conf_" + $da_timestamp + ".zip"
    ForEach($da_key in $da_dirstoget.Keys) {
        $da_service = $da_dirstoget[$da_key]
        If($da_service.retrieve) {
            "Getting configuration from: " + $da_dirstoget[$da_key].name + "."
            $da_source = "$installdir\" + $da_service.srcfolder

            $da_sub_tmpsrcfolder = $da_tmpsrcfolder + "\" + $da_key
            If(Test-path $da_sub_tmpsrcfolder) {Remove-Item $da_sub_tmpsrcfolder -Recurse}
            mkdir $da_sub_tmpsrcfolder > $null
            try {
                Copy-Item -literalPath $da_source -destination $da_sub_tmpsrcfolder -Recurse
            } catch {
                "Failed to get configuration for: " + $da_dirstoget[$da_key].name
                "Could not find specified installation folder: '" + $installdir + "'."
                exit 1
            }
        }
    }
    $da_source = $da_tmpsrcfolder
    $da_destination = $da_dstfolder + "\" + $da_zipbasename

    If(Test-path $da_destination) {
        Remove-item $da_destination
    }
    Add-Type -assembly "system.io.compression.filesystem"
    try {
        [io.compression.zipfile]::CreateFromDirectory($da_source, $da_destination)
    } catch [System.IO.IOException] {
    }
    Remove-Item $da_tmpsrcfolder -Recurse

    ""
    "Done."
    "Packed configuration snapshot can be found at $da_destination"
} else {
}

#Clean up variables
Remove-Variable da_*
