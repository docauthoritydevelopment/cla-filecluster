﻿###
# Gather logs from various components installed on the host machine to allow easy delivery to da-support
# Written by Neil R. Dec-2017
#
# Example:
#     powershell.exe -ExecutionPolicy Unrestricted "& '.\da_getlogs.ps1' -target myZipLocation -installdir D:\DocAuthority -prefix newCo"
#
###
param(
    #Set a Zip File Prefix if needed.
    [string]$prefix = 'da',
    #Set the Installation Folder
    [string]$installdir = 'C:\DocAuthority',
    #Set the Output Folder
    [string]$target = '',
    #Set number of days to keep - 0 - all
    [int]$daystokeep,
    #Set the maximum size a log file should be.
    #So if the log file is smaller than the maxsize in MB specified it will be grabbed.
    [int]$maxsize
)

$da_timestamp = Get-Date -Format yyMMdd-HHmm
$da_logfolder = $target
if ($da_logfolder -eq '') {
	$da_logfolder = 'C:\TEMP\DALogs_' + $da_timestamp
}

#Process the provided Time String so that Powershell can use it.
if($daystokeep -ne 0) {
#	$da_newer = $da_timestamp.AddDays(-$daystokeep)
	$da_newer = ([datetime]::Today).AddDays(1-$daystokeep)
}

#Let's set some variables ;)
$da_logstoget = @{
    ui = @{
        name = 'DocAuthority User Interface';
        logfolder = 'da-ui\log';
        retrieve = $false
    };
    mng = @{
        name = 'DocAuthority Management Agent';
        logfolder = 'mngAgent\logs';
        retrieve = $false
    };
    mp = @{
        name = 'DocAuthority Media Processor';
        logfolder = 'mediaProcessor\logs';
        retrieve = $false
    };
    fc = @{
        name = 'DocAuthority FileCluster';
        logfolder = 'filecluster\logs';
        retrieve = $false
    };
    amq = @{
        name = 'DocAuthority Apache ActiveMQ';
        logfolder = 'apache-activemq\data';
        retrieve = $false
    };
    solr = @{
        name = 'DocAuthority Solr';
        logfolder = 'solr-7.7.2\server\logs';
        retrieve = $false
    }
}

$da_continue = $false
If(-Not (Test-path $installdir)) {
	"Installation dir $installdir doesn't exist"
} else {
	""
	"Please select the DocAuthority logs you would like to capture. (Default is No)"
	ForEach($da_key in $da_logstoget.Keys) {
		$da_service = $da_logstoget[$da_key]
        If(Test-path $da_service.logfolder) {
            $da_Readhost = Read-Host $da_service.name "? ( y / n )"
            Switch ($da_ReadHost)
             {
               Y {$da_service.retrieve=$true; $da_continue=$true}
               N {$da_service.retrieve=$false}
               Default {$da_service.retrieve=$false}
             }
		 }
	}
	If(-Not $da_continue) {
		"Nothing to do ..."
	}
}
#Let's get some logs if the user has selected Y
If($da_continue) {
    ""
    If(-Not (Test-path $da_logfolder)) {
		"Creating packed logs destination folder: $da_logfolder"
        mkdir $da_logfolder > $null
	} else {
		"Packed logs destination folder: $da_logfolder"
    }
	""

    #Zipping!
    ForEach($da_key in $da_logstoget.Keys) {
        $da_service = $da_logstoget[$da_key]
        If($da_service.retrieve) {
			$da_zipbasename =  $prefix + "_" + $da_key + "_logs_" + $da_timestamp + ".zip"
            "Getting logs from: " + $da_logstoget[$da_key].name + " into " + $da_zipbasename + " ..."
            $da_source = "$installdir\" + $da_service.logfolder

            $da_tmplogfolder = $da_logfolder + "\temp"
            If(-Not (Test-path $da_tmplogfolder)) {mkdir $da_tmplogfolder > $null}
            try {
                ForEach($da_file in (Get-ChildItem -Path $da_source)) {
                    $da_copyfile = $true
                    if($maxsize -ne 0) {
                        If(($da_file.Length/1MB) -gt $maxsize){$da_copyfile = $false}
                    }
					if($daystokeep -ne 0) {
                        If($da_file.LastWriteTime -lt $da_newer){$da_copyfile = $false}
                    }
                    if($da_copyfile){Copy-Item ($da_source + "\" + $da_file) $da_tmplogfolder}
                }
            } catch {
                "Failed to get logs for: " + $da_logstoget[$da_key].name
                "Could not find specified installation folder: '" + $installdir + "'."
                exit 1
            }
            $da_source = $da_tmplogfolder
            $da_destination = $da_logfolder + "\" + $da_zipbasename

            If(Test-path $da_destination) {
                Remove-item $da_destination
            }
            Add-Type -assembly "system.io.compression.filesystem"
            try {
                [io.compression.zipfile]::CreateFromDirectory($da_source, $da_destination) 
            } catch [System.IO.IOException] {
            }
    
            Remove-Item $da_tmplogfolder -Recurse
        }
    }
    ""
    "Done."
    "Packed logs can be found at $da_logfolder"
} else {
}

#Clean up variables
Remove-Variable da_*
