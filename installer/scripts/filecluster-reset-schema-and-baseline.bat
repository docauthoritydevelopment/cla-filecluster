@echo off
if NOT '%1' == 'do-reset' goto usage

pushd filecluster
echo Dropping schema
call filecluster-upgrade-schema.bat drop-schema
echo Create New schema
call filecluster-reset-schema.bat do-reset
echo Create Baseline the new schema
call filecluster-upgrade-schema.bat baseline
popd
goto finally

:usage
echo Usage:
echo        %0 do-reset
goto finally

:finally
