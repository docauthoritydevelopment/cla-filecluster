@ECHO off
REM ##########################################################################
REM
REM  Run KVDB rebuilder
REM
REM usage:
REM only one parameter which is path to place the new kvdb
REM
REM ##########################################################################

REM Set local scope for the variables with windows NT shell
REM if "%OS%"=="Windows_NT" setlocal
setlocal

set argC=0
for %%x in (%*) do Set /A argC+=1

set STORE_PREFIX=appserver
IF %argC% == 1 goto argsok2
IF %argC% == 2 goto argsok1
goto usage

:argsok1
IF /I "X%2" == "XFC" (
  goto argsok2
)
IF /I "X%2" == "XMP" (
  set STORE_PREFIX=media-proc
  goto argsok2
)
goto usage

:argsok2
set VERSION=3.0.0
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%

if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%~dp0
REM echo APP_HOME=%APP_HOME%

set "DEFAULT_JVM_OPTS=-Dfile.encoding=UTF8"

@rem set "JAVA_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000"
set JAVA_OPTS=
@rem Find java.exe
SET JAVA_HOME_LOCAL=%DIRNAME%\..\jre
set JAVA_EXE=%JAVA_HOME_LOCAL%\bin\java.exe
set KVDB_REBUILDER_JAR_PATH=%APP_HOME%\lib\kvdb-rebuild-%VERSION%.jar
if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA installation not found (%JAVA_HOME_LOCAL%)
goto fail

:init
set CLASSPATH=%APP_HOME%\lib\*
set CONFIGPATH=%APP_HOME%\config
@rem Execute cla-filecluster
echo Running kvdb-rebuild-%VERSION% on %1 %2 (%STORE_PREFIX%)
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% -DconfigPath="%CONFIGPATH%" -jar "%KVDB_REBUILDER_JAR_PATH%" rebuild-kvdb "%1" %STORE_PREFIX%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable CLA_FILECLUSTER_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%CLA_FILECLUSTER_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal
goto finally

:usage
echo.
echo DocAuthority KVDB rebuilder tool
echo.
echo Usage:
echo     %0 [new-kvdb-path] [fc^|mp]
echo.
echo     [new-kvdb-path]: The path where the new KVDB should be created
echo     [fc^|mp]:        Optional target for the rebuilt kvdb. Default: fc
echo.
echo Examples:
echo     %0 ..\data\fc_data\new_kvdb
echo     %0 temp\new_mp_kvdb mp
goto finally


:finally

