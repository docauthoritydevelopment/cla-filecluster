@ECHO OFF
REM BATCH upgrade script
REM Work directory is <INSTALL-DIR>/filecluster
REM
echo.
echo ===
echo === This script is redundant. Upgrade is performed automatically during FileCluster upgrade.
echo ===
echo.
goto finally
REM
if NOT [%1] == [do-create-deprecated] goto usage
REM
REM
REM This file is meant to be activated MANUALLY after updated installer has finished installation.
REM
REM pushd ..
SET LOGF=installation_logs\%0.log
SET ZK_ADDESS=%2
SET SHARDS=%3

call solr_cloud_config.bat createCollection BizList %SHARDS%
call solr_cloud_config.bat createCollection UnmatchedContent %SHARDS%
call solr_cloud_config.bat createCollection CatFolders %SHARDS%

REM call solr_cloud_config.bat createCore BizList
REM call solr_cloud_config.bat createCore UnmatchedContent
REM call solr_cloud_config.bat createCore CatFolders

IF NOT '%SHARDS%'=='1' (
  call solr_cloud_config.bat align-shards %ZK_ADDESS% my_alignment.bat
  call my_alignment.bat do-align
)
REM popd

goto finally

:usage
echo This script should be activated on one of the Solr machines from the DocAuthority installation directory.
echo It will create the newly added collections for a system upgraded from version 2.0.x
echo.
echo Usage:
echo        %0 do-create-deprecated ^<zookeeper-address^> ^<num-shards^>
echo.
echo   Examples:
echo        %0 do-create localhost:2181/solr 1
echo        %0 do-create 123.45.67.89:2181/solr 3
echo.
goto finally

:finally
exit /b 0
