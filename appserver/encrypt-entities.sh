#!/bin/bash
echo "script should be updated. sorry"
exit 1
echo "Starting Encryptor..."
VERSION='1.0.0'
DIRNAME=`dirname $0`
CLA_HOME=`cd $DIRNAME/.;pwd;`
export CLA_HOME;

if [ "$1" == "" ]; then
    echo "No input file - Will use the default one as configured in the properties file."
else
	echo "Override Input file to $1"
    export clearedEntitiesFile=$1
fi

if [ "$2" == "" ]; then
    echo "No output file - Will use the default one as configured in the properties file."
else
	echo "Override Output file to $2"
    export entitiesFile=$2
fi

java -Dspring.profiles.active=encryptor -jar $CLA_HOME/build/libs/cla-filecluster-$VERSION.jar $1 $2 $3
