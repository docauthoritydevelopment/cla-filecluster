@echo off
if NOT '%1' == 'do-reset' goto usage

echo Resetting extracted_entities schema...
set VERSION=3.0.0
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%
set LIB_ROOT=.\build\libs\
set JMX_REMOTE_PORT=7005
REM EXTERN_IP=`wget -qO- http://ipecho.net/plain ; echo`
set EXTERN_IP=54.69.192.94

set JVMOPT=-Dspring.profiles.active=recreateschema -Dfile.encoding=UTF8

set JMXOPT= 
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.authenticate=false
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.ssl=false
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT%
REM set JMXOPT=%JMXOPT% -Djava.rmi.server.hostname=%EXTERN_IP%

set MEM_OPT=-Xms1g -Xmx1g
set GC_OPT=
REM set GC_OPT="$GC_OPT -XX:+UseParNewGC"
REM set #GC_OPT="$GC_OPT -XX:+UseConcMarkSweepGC"
REM set #GC_OPT="$GC_OPT -XX:SurvivorRatio=6"
REM set #GC_OPT="$GC_OPT -XX:+CMSParallelRemarkEnabled"
REM set #GC_OPT="$GC_OPT -XX:CMSInitiatingOccupancyFraction=75"
REM set #GC_OPT="$GC_OPT -XX:+UseCMSInitiatingOccupancyOnly"


REM ###########################
REM Section added to allow Java 11 support in parallel to other branches using Java 8.
REM Itay R. May 2019, Solr7 migration
set JAVA11_HOME=%ProgramW6432%\Java\jdk-11.0.3
IF NOT EXIST "%JAVA11_HOME%" (
   echo Could not find Java 11 home at "%JAVA11_HOME%"
   exit /b 1
)
Echo Using Java 11!
set JAVA_HOME=%JAVA11_HOME%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe
if not exist "%JAVA_EXE%" goto usage

SET CP=-cp .;.\config;%LIB_ROOT%;%LIB_ROOT%lib\;%LIB_ROOT%.\appserver-%VERSION%-fat.jar;%CONTENT_DIR%

"%JAVA_EXE%" %MEM_OPT% %JVMOPT% %GC_OPT% %JMXOPT% %CP% org.springframework.boot.loader.JarLauncher %1 %2 %3 %4
goto finally

:usage
echo Usage:
echo        %0 do-reset
goto finally

:finally
