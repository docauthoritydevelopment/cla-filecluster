@echo off
echo Starting entity-extraction Server...

set VERSION=3.0.0
set DIRNAME=%~dp0
set CLA_HOME=%DIRNAME%..
set LIB_ROOT=.\build\libs

set LIB_ROOT=.\build\libs\
set JMX_REMOTE_PORT=7005
REM EXTERN_IP=`wget -qO- http://ipecho.net/plain ; echo`
set EXTERN_IP=54.69.192.94

set JVMOPT=-Dspring.profiles.active=win -Dfile.encoding=UTF8

set JMXOPT= 
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.authenticate=false
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.ssl=false
set JMXOPT=%JMXOPT% -Dcom.sun.management.jmxremote.port=%JMX_REMOTE_PORT%
REM set JMXOPT=%JMXOPT% -Djava.rmi.server.hostname=%EXTERN_IP%

set MEM_OPT=-Xms1g -Xmx2g
set GC_OPT=
REM set GC_OPT="$GC_OPT -XX:+UseParNewGC"
REM set #GC_OPT="$GC_OPT -XX:+UseConcMarkSweepGC"
REM set #GC_OPT="$GC_OPT -XX:SurvivorRatio=6"
REM set #GC_OPT="$GC_OPT -XX:+CMSParallelRemarkEnabled"
REM set #GC_OPT="$GC_OPT -XX:CMSInitiatingOccupancyFraction=75"
REM set #GC_OPT="$GC_OPT -XX:+UseCMSInitiatingOccupancyOnly"

SET CP=-cp .;.\config;%LIB_ROOT%\;%LIB_ROOT%\lib\;%LIB_ROOT%\cla-entity-extractor-%VERSION%.jar

java %MEM_OPT% %JVMOPT% %GC_OPT% %JMXOPT% %CP% org.springframework.boot.loader.JarLauncher %1 %2 %3 %4
