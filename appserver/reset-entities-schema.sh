#!/bin/bash

if [ "$1" != "do-reset" ]; then
    echo "use reset-entities-schema.sh do-reset"
    echo "this will reset all your data"
	exit
fi

echo Reseting extracted_entities schema...

DIRNAME=`dirname $0`
CLA_HOME=`cd $DIRNAME/..;pwd;`
export CLA_HOME;

LIB_ROOT=$CLA_HOME/libs

#EXTERN_IP=`wget -qO- http://ipecho.net/plain ; echo`
EXTERN_IP=54.69.192.94
export EXTERN_IP
export JMX_REMOTE_PORT=7005
JMX_OPTS="-Dcom.sun.management.jmxremote"
JMX_OPTS="$JMX_OPTS -Dcom.sun.management.jmxremote.authenticate=false"
JMX_OPTS="$JMX_OPTS -Dcom.sun.management.jmxremote.ssl=false"
JMX_OPTS="$JMX_OPTS -Djava.rmi.server.hostname=$EXTERN_IP"
JMX_OPTS="$JMX_OPTS -Dcom.sun.management.jmxremote.port=$JMX_REMOTE_PORT"
export JMX_OPTS
MEM_OPT="-Xms1g -Xmx2g"
export MEM_OPT

JVMOPT="-Dspring.profiles.active=recreateschema -Dfile.encoding=UTF8"

#CP='-cp "$DIRNAME/lib/*"'

java $MEM_OPT $JVMOPT -Dfile.encoding=UTF8 $JMX_OPTS -cp "$DIRNAME/lib/*" com.cla.filecluster.Application $1 $2 $3 $4

#java $MEM_OPT $JVMOPT -Dfile.encoding=UTF8 $JMX_OPTS $CP org.springframework.boot.loader.JarLauncher $1 $2 $3 $4
#java $MEM_OPT $JVMOPT -Dfile.encoding=UTF8 $JMX_OPTS $CP org.springframework.boot.loader.JarLauncher $1 $2 $3 $4
