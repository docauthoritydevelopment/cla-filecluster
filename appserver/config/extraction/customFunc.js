var customFunc = function(dobString) {
	age = calculateAge(dobString);
	if (age < 13)
		return "MU13"
	if (age>=13 && age < 18)
		return "MU18"
	return "";
}

function calculateAge(dobString) {
    var dob = new Date(dobString);
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
    var age = currentYear - dob.getFullYear();

    if(birthdayThisYear > currentDate) {
        age--;
    }

    return age;
}