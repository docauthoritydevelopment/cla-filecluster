<?xml version="1.0" encoding="UTF-8"?>

<configuration scan="true">

    <!-- GENERAL -->

    <property file="./config/application.properties"/>

    <springProfile name="test">
        <property file="./config/application-test.properties"/>
    </springProfile>

    <conversionRule conversionWord="clr" converterClass="org.springframework.boot.logging.logback.ColorConverter" />
    <conversionRule conversionWord="wex" converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />

    <property name="DIAGNOSTICS_PATTERN" value="%d{dd HH:mm:ss.SSS} %m%n%ex"/>
    <property name="FILE_LOG_PATTERN" value="%d{dd HH:mm:ss.SSS} %5p [%t] %45.45logger{44}:%L : %m%n%ex"/>
    <property name="CONSOLE_LOG_PATTERN" value="%clr(%d{dd HH:mm:ss.SSS}){faint} %clr(%5p) %clr([%t]){faint} %clr(%45.45logger{44}:%L){cyan} %clr(:){faint} %m%n%ex"/>

    <!-- AUDIT -->

    <appender name="audit_db_appender" class="com.cla.filecluster.service.audit.AuditDBAppender">
        <connectionSource class="com.cla.filecluster.service.audit.Connector">
                <driverClass>com.mysql.jdbc.Driver</driverClass>
        </connectionSource>
    </appender>

    <appender name="audit_appender" class="ch.qos.logback.classic.AsyncAppender" >
        <queueSize>1024</queueSize>
        <discardingThreshold>0</discardingThreshold>
        <appender-ref ref="audit_db_appender" />
        <neverBlock>true</neverBlock>
    </appender>

    <logger name="audit" level="INFO" additivity="false" >
        <appender-ref ref="audit_appender" />
    </logger>

    <!-- DIAGNOSTICS -->

    <springProfile name="dev,prod">
        <appender name="diagnostics_sync" class="ch.qos.logback.core.rolling.RollingFileAppender">
            <File>./logs/diagnostics.log</File>
            <encoder>
                <pattern>${DIAGNOSTICS_PATTERN}</pattern>
            </encoder>
            <rollingPolicy class="com.cla.common.utils.FixedWindowRollingPolicyNoLimit">
                <maxIndex>99</maxIndex>
                <FileNamePattern>./logs/diagnostics.%i.log</FileNamePattern>
            </rollingPolicy>
            <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                <MaxFileSize>10MB</MaxFileSize>
            </triggeringPolicy>
        </appender>
        <appender name="diagnostics" class="ch.qos.logback.classic.AsyncAppender">
            <queueSize>10000</queueSize>
            <discardingThreshold>0</discardingThreshold>
            <includeCallerData>true</includeCallerData>
            <neverBlock>false</neverBlock>
            <maxFlushTime>30000</maxFlushTime>
            <appender-ref ref="diagnostics_sync" />
        </appender>
    </springProfile>



    <!-- FILE -->

    <!-- Appender is buffered using an AsyncAppender -->
    <springProfile name="test,dev,prod,default,fileprocessor">
        <appender name="file_sync" class="ch.qos.logback.core.rolling.RollingFileAppender">
            <File>./logs/filecluster.log</File>
            <encoder>
                <pattern>${FILE_LOG_PATTERN}</pattern>
            </encoder>
            <rollingPolicy class="com.cla.common.utils.FixedWindowRollingPolicyNoLimit">
                <maxIndex>99</maxIndex>
                <FileNamePattern>./logs/filecluster.%i.log</FileNamePattern>
            </rollingPolicy>
            <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                <MaxFileSize>10MB</MaxFileSize>
            </triggeringPolicy>
        </appender>
    </springProfile>
    <springProfile name="recreateschema">
        <appender name="file_sync" class="ch.qos.logback.core.rolling.RollingFileAppender">
            <File>./logs/recreate_schema.log</File>
            <encoder>
                <pattern>${FILE_LOG_PATTERN}</pattern>
            </encoder>
            <rollingPolicy class="com.cla.common.utils.FixedWindowRollingPolicyNoLimit">
                <maxIndex>99</maxIndex>
                <FileNamePattern>./logs/recreate_schema.%i.log</FileNamePattern>
            </rollingPolicy>
            <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                <MaxFileSize>10MB</MaxFileSize>
            </triggeringPolicy>
        </appender>
    </springProfile>
    <springProfile name="encryptor">
        <appender name="file_sync" class="ch.qos.logback.core.rolling.RollingFileAppender">
            <File>./logs/encryptor.log</File>
            <encoder>
                <pattern>${FILE_LOG_PATTERN}</pattern>
            </encoder>
            <rollingPolicy class="com.cla.common.utils.FixedWindowRollingPolicyNoLimit">
                <maxIndex>99</maxIndex>
                <FileNamePattern>./logs/encryptor.%i.log</FileNamePattern>
            </rollingPolicy>
            <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
                <MaxFileSize>10MB</MaxFileSize>
            </triggeringPolicy>
        </appender>
    </springProfile>

    <appender name="file" class="ch.qos.logback.classic.AsyncAppender">
        <queueSize>10000</queueSize>
        <discardingThreshold>0</discardingThreshold>
        <includeCallerData>true</includeCallerData>
        <neverBlock>false</neverBlock>
        <maxFlushTime>30000</maxFlushTime>
        <appender-ref ref="file_sync" />
    </appender>

    <!-- STDOUT -->

    <springProfile name="test,recreateschema,encryptor">
        <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
            <Target>System.out</Target>
            <encoder>
                <pattern>${CONSOLE_LOG_PATTERN}</pattern>
            </encoder>
        </appender>
    </springProfile>

    <springProfile name="dev">
        <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
            <Target>System.out</Target>
            <encoder>
                <pattern>${CONSOLE_LOG_PATTERN}</pattern>
            </encoder>
            <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
                <level>WARN</level>
            </filter>
        </appender>
    </springProfile>

    <springProfile name="prod,default,fileprocessor">
        <appender name="stdout" class="ch.qos.logback.core.rolling.RollingFileAppender">
            <file>./logs/fc_stdout.log</file>
            <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
                <fileNamePattern>./logs/fc_stdout-%d{yyyy-MM-dd}.%i.log</fileNamePattern>
                <timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                    <maxFileSize>10MB</maxFileSize>
                </timeBasedFileNamingAndTriggeringPolicy>
                <maxHistory>30</maxHistory>
                <totalSizeCap>300MB</totalSizeCap>
            </rollingPolicy>
            <encoder>
                <pattern>${CONSOLE_LOG_PATTERN}</pattern>
            </encoder>
            <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
                <level>WARN</level>
            </filter>
        </appender>
    </springProfile>

    <!-- LEVELS -->

    <springProfile name="dev,prod">
        <logger name="com.cla.filecluster.service.diagnostics.LogWriterListener" additivity="false" level="WARN">
            <appender-ref ref="diagnostics"/>
        </logger>
    </springProfile>

    <logger name="org.apache.pdfbox.pdmodel.graphics.xobject" level="OFF"/>
    <logger name="com" level="WARN"/>
    <logger name="org.hibernate.type" level="INFO"/>
    <logger name="com.cla.filecluster.repository.solr.SolrPvEntriesIngesterRepository" level="DEBUG"/>
    <logger name="com.cla.common.media.files.word.ClaWordXmlExtractor" level="INFO"/>
    <logger name="com.cla.common.media.files.word.ClaWord6Extractor" level="INFO"/>
    <logger name="com.cla.filecluster.service.crawler.pv.SimilarityMapper" level="DEBUG"/>
    <logger name="org.hibernate.tool.hbm2ddl" level="INFO"/>
    <logger name="org.apache.pdfbox.util.operator.pagedrawer.BeginInlineImage" level="ERROR"/>
    <logger name="com.cla.filecluster.util.remote.FormAuthenticatedRestTemplate" level="INFO"/>
    <logger name="org.apache.pdfbox.util.operator" additivity="false" level="WARN">
        <appender-ref ref="file"/>
    </logger>
    <logger name="org.apache.fontbox" additivity="false" level="WARN">
        <appender-ref ref="file"/>
    </logger>
    <logger name="org.apache.cxf.jaxrs.utils.JAXRSUtils" additivity="false" level="WARN">
        <appender-ref ref="file"/>
    </logger>
    <logger name="com.cla.filecluster.config.AppServerJmsConfig" level="INFO"/>
    <logger name="org.apache.solr.client.solrj.impl.CloudSolrClient" level="OFF"/>
    <logger name="org.springframework" level="INFO"/>
    <logger name="org.apache.cxf.service.factory.ReflectionServiceFactoryBean" level="WARN"/>
    <logger name="org.springframework.retry.support" level="INFO"/>
    <logger name="org.apache.pdfbox.util.PDFStreamEngine" level="ERROR"/>
    <logger name="org.apache.pdfbox.pdmodel.graphics.color" level="OFF"/>
    <logger name="org" level="WARN"/>
    <logger name="com.cla.common.allocation.IngestionResourceManager" level="TRACE"/>
    <logger name="com.datastax.driver.core.Cluster" level="ERROR"/>
    <logger name="com.cla" level="DEBUG"/>
    <logger name="org.hibernate.hql.internal.ast.HqlSqlWalker" level="ERROR"/>
    <logger name="org.hibernate.SQL" level="INFO"/>
    <logger name="org.apache.pdfbox.filter" level="OFF"/>
    <logger name="com.cla.common.media.files.excel.ExcelFilesServiceImpl" level="INFO"/>
    <logger name="com.cla.filecluster.repository.solr.SolrPvAnalyzerRepository" level="DEBUG"/>
    <logger name="com.cla.common.domain.dto.mediaproc.excel.ExcelSheet" level="INFO"/>
    <logger name="com.cla.common.media.files.word" level="INFO"/>
    <logger name="com.cla.filecluster.policy.factsenricher.SolrFactsEnricher" level="INFO"/>
    <logger name="com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService" level="DEBUG"/>
    <logger name="com.cla.filecluster.jobmanager.service.JobManagerService" level="DEBUG"/>
    <logger name="com.cla.filecluster.mediaproc.connectors.MediaProcessorCommListeners" level="INFO"/>
    <logger name="com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService" level="INFO"/>
    <logger name="com.cla.filecluster.mediaproc.ScanItemConsumer" level="INFO"/>
    <logger name="com.cla.filecluster.config.AppServerJmsConfig$2" level="INFO"/>
    <logger name="com.cla.config.jms.JmsErrorHandlingTemplate" level="INFO"/>
    <logger name="com.cla.filecluster.service.files.FilesDataPersistenceService" level="INFO"/>
    <logger name="com.cla.filecluster.service.categories.FileCatService" level="INFO"/>
    <!--logger name="com.cla.filecluster.repository.solr.infra.AclCatFileSolrClientAdapter" level="TRACE"/-->
    <logger name="org.apache.http.wire" level="INFO"/> <!-- prevent passwords from being logged -->
    <!--logger name="com.cla.filecluster.config.security" level="TRACE"/-->
    <!--logger name="org.springframework.security" level="TRACE"/-->

    <springProfile name="test,recreateschema,encryptor">
        <logger name="com.cla.filecluster.kpi.PerformanceKpiRecorder" level="ERROR"/>
        <logger name="com.cla.filecluster.repository.solr.SolrFileCatRepository" level="INFO"/>
    </springProfile>
    <springProfile name="dev,prod,default,fileprocessor">
        <logger name="com.cla.filecluster.kpi.PerformanceKpiRecorder" level="ERROR"/>
    </springProfile>


    <springProfile name="prod,default,fileprocessor">
        <logger name="FileTailer" level="INFO" additivity="false" >
            <appender-ref ref="file"/>
        </logger>
    </springProfile>

    <root level="WARN">
        <appender-ref ref="file"/>
        <appender-ref ref="stdout"/>
    </root>
</configuration>