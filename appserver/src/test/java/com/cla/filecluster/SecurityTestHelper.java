package com.cla.filecluster;

import com.cla.common.domain.dto.security.Authority;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.domain.dto.security.UserType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.domain.entity.security.*;
import com.cla.filecluster.service.security.UserService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertNotNull;

public class SecurityTestHelper {

    private static Map<String,Role> rolesMap;

    static{
        rolesMap = Maps.newHashMap();
        for (Authority auth : Authority.values()){
            Role role = new Role(auth.getName(), auth.getDescription(), auth.getName());
            rolesMap.put(role.getName(), role);
        }
    }

    public static void authenticate(User user){
        UserDto userDto = UserService.convertUser(user, false, true);
        AuthenticationHelper.authenticateAs(userDto);
    }

    /**
     * Builder for User object
     */
    public static class UserBuilder{
        private long id;
        private String username;
        private UserType userType;
        private List<FuncRoleBuilder> funcRoles = Lists.newArrayList();
        private List<BizRoleBuilder> bizRoles = Lists.newArrayList();
        private List<String> accessTokens = Lists.newArrayList();

        public static UserBuilder create(){
            return new UserBuilder();
        }

        public UserBuilder withId(long id){
            this.id = id;
            return this;
        }

        public UserBuilder withUsername(String username){
            this.username = username;
            return this;
        }

        public UserBuilder withFuncRole(FuncRoleBuilder funcRole){
            this.funcRoles.add(funcRole);
            return this;
        }

        public UserBuilder withBizRole(BizRoleBuilder bizRole){
            this.bizRoles.add(bizRole);
            return this;
        }

        public UserBuilder withFuncRoles(FuncRoleBuilder ... funcRoles){
            this.funcRoles = Arrays.asList(funcRoles);
            return this;
        }

        public UserBuilder withAccessTokens(String ... acls){
            this.accessTokens = Arrays.asList(acls);
            return this;
        }

        public UserBuilder withUserType(UserType userType){
            this.userType = userType;
            return this;
        }

        public User build(){
            User user = new User();
            assertNotNull(username);
            user.setId(id);
            user.setUsername(this.username);
            user.setFunctionalRoles(funcRoles.stream().map(FuncRoleBuilder::build).collect(Collectors.toSet()));
            user.setBizRoles(bizRoles.stream().map(BizRoleBuilder::build).collect(Collectors.toSet()));
            if (userType != null){
                user.setUserType(userType);
            }
            if (accessTokens != null && accessTokens.size() > 0){
                user.setAccessTokens(Sets.newHashSet(accessTokens));
            }
            return user;
        }
    }

    /**
     * Builder for FunctionalRole object
     */
    public static abstract class CompositeRoleBuilder<T extends CompositeRole, S extends CompositeRoleBuilder<T,S>>{

        private long id;
        private String name;
        private List<Authority> authorities;


        public S withId(long id){
            this.id = id;
            return (S)this;
        }

        public S withName(String name){
            this.name = name;
            return (S)this;
        }

        public S withAuthorities(Authority ... auths){
            this.authorities = Arrays.asList(auths);
            return (S)this;
        }

        public CompositeRole build(){
            T compRole = getNewInstance();
            assertNotNull(name);
            compRole.setId(id);
            compRole.setName(name);
            authorities.forEach(auth -> compRole.getTemplate().getRoles().add(rolesMap.get(auth.getName())));
            return compRole;
        }

        protected abstract T getNewInstance();
    }

    public static class FuncRoleBuilder extends CompositeRoleBuilder<FunctionalRole, FuncRoleBuilder>{
        public static FuncRoleBuilder create(){
            return new FuncRoleBuilder();
        }

        @Override
        protected FunctionalRole getNewInstance() {
            FunctionalRole f = new FunctionalRole();
            f.setTemplate(TemplateRoleBuilder.create());
            return f;
        }

        @Override
        public FunctionalRole build() {
            return (FunctionalRole)super.build();
        }
    }

    public static class BizRoleBuilder extends CompositeRoleBuilder<BizRole, BizRoleBuilder>{
        public static BizRoleBuilder create(){
            return new BizRoleBuilder();
        }

        @Override
        protected BizRole getNewInstance() {
            BizRole b = new BizRole();
            b.setTemplate(TemplateRoleBuilder.create());
            return b;
        }

        @Override
        public BizRole build() {
            return (BizRole)super.build();
        }
    }

    public static class TemplateRoleBuilder {
        public static RoleTemplate create() {
            return new RoleTemplate();
        }
    }
}
