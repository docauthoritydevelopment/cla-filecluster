package com.cla.filecluster.config.security;

import com.cla.filecluster.domain.entity.security.BizRole;
import com.cla.filecluster.domain.entity.security.CompositeRole;
import com.cla.filecluster.service.security.BizRoleService;
import com.cla.filecluster.service.security.DomainAccessHandler;
import com.cla.filecluster.service.security.LdapGroupMappingService;
import com.cla.filecluster.service.security.UserService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class CustomUserDetailsContextMapperTest {

    @InjectMocks
    private CustomUserDetailsContextMapper customUserDetailsContextMapper;

    @Mock
    private LdapGroupMappingService ldapGroupMappingService;

    @Mock
    private UserService userService;

    @Mock
    private BizRoleService bizRoleService;

    @Before
    public void init() {
        DomainAccessHandler domainAccessHandler = new DomainAccessHandler();
        domainAccessHandler.setAccessTokensDomainNameJoinString("\\\\");
        domainAccessHandler.setAccessTokensDomainNameMapStrings(new String[] {"testing.da.com:TESTING"});
        domainAccessHandler.setAccessTokensOriginalDomainNameJoinString("@");
        domainAccessHandler.init();

        setField(customUserDetailsContextMapper, "allowNoRoleUser", true);
        setField(customUserDetailsContextMapper, "defaultRoleName", "viewer");
        setField(customUserDetailsContextMapper, "addDomainNameToAccessTokens", true);
        setField(customUserDetailsContextMapper, "domainAccessHandler", domainAccessHandler);
        setField(customUserDetailsContextMapper, "accessTokenTypeToSave", "both");

        when(userService.getCurrentUserDomain()).thenReturn("testing.da.com");
        BizRole defaultBizRole = new BizRole();
        defaultBizRole.setName("viewer");
        when(bizRoleService.getBizRoleByName("viewer")).thenReturn(defaultBizRole);
    }

    @Test
    public void testMapUserFromContextNoRoles() {
        DirContextOperations ctx = new DirContextAdapter();
        ctx.addAttributeValue("cn", "baba");
        String username = "baba@testing.da.com";
        List<? extends GrantedAuthority > authorities = new ArrayList<>();

        ArgumentCaptor<Set<String>> accessCaptor = ArgumentCaptor.forClass(Set.class);
        ArgumentCaptor<Set<CompositeRole>> rolesCaptor = ArgumentCaptor.forClass(Set.class);
        customUserDetailsContextMapper.mapUserFromContext(ctx, username, authorities);
        verify(userService, atLeastOnce()).createLdapUserIfAbsentInCurrentDomain(
                eq("baba"), eq("baba@testing.da.com"), rolesCaptor.capture(), accessCaptor.capture());
        assertEquals(1, rolesCaptor.getValue().size());
        assertEquals(2, accessCaptor.getValue().size());
        assertEquals("viewer", rolesCaptor.getValue().iterator().next().getName());
        assertTrue(accessCaptor.getValue().contains("TESTING\\\\baba@testing.da.com"));
        assertTrue(accessCaptor.getValue().contains("baba@testing.da.com"));
    }

    @Test
    public void testConcatDomainPrefix() {
        String token = "baba";
        String domainName = "testing.da.com";
        Set<String> res = customUserDetailsContextMapper.concatDomainPrefix(token, domainName);
        assertEquals(2, res.size());
        assertTrue(res.contains("TESTING\\\\baba"));
        assertTrue(res.contains("baba@testing.da.com"));

        token = "baba@testing.da.com";
        domainName = "testing.da.com";
        res = customUserDetailsContextMapper.concatDomainPrefix(token, domainName);
        assertEquals(2, res.size());
        assertTrue(res.contains("TESTING\\\\baba@testing.da.com"));
        assertTrue(res.contains("baba@testing.da.com"));

        // domain not mapped - use default
        token = "baba@gaga.da.com";
        domainName = "gaga.da.com";
        res = customUserDetailsContextMapper.concatDomainPrefix(token, domainName);
        assertEquals(2, res.size());
        assertTrue(res.contains("gaga.da.com\\\\baba@gaga.da.com"));
        assertTrue(res.contains("baba@gaga.da.com"));
    }

    @Test
    public void testHandleUserWithNoBizRolesUseDefault() {
        Set<CompositeRole> userDaRoles = Sets.newHashSet();
        customUserDetailsContextMapper.handleUserWithNoBizRoles("gaga", userDaRoles);
        assertEquals(1, userDaRoles.size());
        assertEquals("viewer", userDaRoles.iterator().next().getName());
    }

    @Test(expected = BadCredentialsException.class)
    public void testHandleUserWithNoBizRolesEmptyNotAllowed() {
        setField(customUserDetailsContextMapper, "allowNoRoleUser", false);
        try {
            Set<CompositeRole> userDaRoles = Sets.newHashSet();
            customUserDetailsContextMapper.handleUserWithNoBizRoles("gaga", userDaRoles);
        } finally {
            setField(customUserDetailsContextMapper, "allowNoRoleUser", true);
        }
    }
}