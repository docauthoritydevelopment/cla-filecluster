package com.cla.filecluster.config.security;

import com.cla.filecluster.domain.entity.security.FunctionalItem;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.service.security.PermissionsService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Sets;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.annotation.Annotation;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.access.AccessDecisionVoter.ACCESS_DENIED;
import static org.springframework.security.access.AccessDecisionVoter.ACCESS_GRANTED;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class PermissionsValidationVoterTest {

    @Mock
    private PermissionsService permissionsService;

    @InjectMocks
    private PermissionsValidationVoter permissionsValidationVoter;

    @Test
    public void voteNoParams() {
        String[] requiredRoles = {"ADMIN", "TECH"};
        Class<?>[] parameterTypes = new Class<?>[0];
        Annotation[][] allParameterAnnotations = new Annotation[0][0];
        Object[] arguments = new Object[0];

        int res = permissionsValidationVoter.vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
        assertEquals(ACCESS_GRANTED, res);
    }

    @Test
    public void voteNoFuncRoles() {
        String[] requiredRoles = {"ADMIN", "TECH"};
        Class<?>[] parameterTypes = new Class<?>[1];
        Annotation[][] allParameterAnnotations = new Annotation[1][1];
        Object[] arguments = new Object[1];

        parameterTypes[0] = String.class;
        allParameterAnnotations[0][0] = new MyRequestParam();
        arguments[0] = "fghfghf";

        int res = permissionsValidationVoter.vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
        assertEquals(ACCESS_GRANTED, res);
    }

    @Test
    public void voteWithFuncRolesNoAccess() {
        String[] requiredRoles = {"ADMIN", "TECH"};
        Class<?>[] parameterTypes = new Class<?>[1];
        Annotation[][] allParameterAnnotations = new Annotation[1][1];
        Object[] arguments = new Object[1];

        parameterTypes[0] = Long.class;
        allParameterAnnotations[0][0] = new MyFuncRole();
        arguments[0] = 15L;

        when(permissionsService.getAccess(anySet(), anyLong(), anyString())).thenReturn(ACCESS_DENIED);

        int res = permissionsValidationVoter.vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
        assertEquals(AccessDecisionVoter.ACCESS_DENIED, res);
    }

    @Test
    public void voteWithFuncRolesWithAccess() {
        String[] requiredRoles = {"ADMIN", "TECH"};
        Class<?>[] parameterTypes = new Class<?>[1];
        Annotation[][] allParameterAnnotations = new Annotation[1][1];
        Object[] arguments = new Object[1];

        parameterTypes[0] = Long.class;
        allParameterAnnotations[0][0] = new MyFuncRole();
        arguments[0] = 15L;

        when(permissionsService.getAccess(anySet(), anyLong(), anyString())).thenReturn(ACCESS_GRANTED);

        int res = permissionsValidationVoter.vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
        assertEquals(ACCESS_GRANTED, res);
        verify(permissionsService, times(1)).getAccess(anySet(), anyLong(), anyString());
    }

    @Test
    public void voteWithFuncRolesParamNoAccess() {
        String[] requiredRoles = {"ADMIN", "TECH"};
        Class<?>[] parameterTypes = new Class<?>[1];
        Annotation[][] allParameterAnnotations = new Annotation[0][0];
        Object[] arguments = new Object[1];

        parameterTypes[0] = FunctionalItem.class;
        arguments[0] = new MyFuncItem();

        when(permissionsService.getAccess(anySet(), Mockito.any(FunctionalItem.class))).thenReturn(ACCESS_DENIED);

        int res = permissionsValidationVoter.vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
        assertEquals(AccessDecisionVoter.ACCESS_DENIED, res);
        verify(permissionsService, times(1)).getAccess(anySet(), Mockito.any(FunctionalItem.class));
    }

    @Test
    public void voteWithFuncRolesParamAccess() {
        String[] requiredRoles = {"ADMIN", "TECH"};
        Class<?>[] parameterTypes = new Class<?>[1];
        Annotation[][] allParameterAnnotations = new Annotation[0][0];
        Object[] arguments = new Object[1];

        parameterTypes[0] = FunctionalItem.class;
        arguments[0] = new MyFuncItem();

        when(permissionsService.getAccess(anySet(), Mockito.any(FunctionalItem.class))).thenReturn(ACCESS_GRANTED);

        int res = permissionsValidationVoter.vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
        assertEquals(AccessDecisionVoter.ACCESS_GRANTED, res);

        verify(permissionsService, times(1)).getAccess(anySet(), Mockito.any(FunctionalItem.class));
    }

    class MyFuncItem implements FunctionalItem {
        @Override
        public Set<FunctionalRole> getFunctionalRoles() {
            FunctionalRole role = new FunctionalRole();
            return Sets.newHashSet(role);
        }
    }

    class MyFuncRole implements FunctionalItemId {
        @Override
        public String value() {
            return FunctionalItem.FILE;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return FunctionalItemId.class;
        }
    }

    class MyRequestParam implements RequestParam {

        @Override
        public String value() {
            return null;
        }

        @Override
        public String name() {
            return null;
        }

        @Override
        public boolean required() {
            return false;
        }

        @Override
        public String defaultValue() {
            return null;
        }

        @Override
        public Class<? extends Annotation> annotationType() {
            return RequestParam.class;
        }
    }
}