package com.cla.filecluster.repository.solr.query;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.client.solrj.SolrQuery;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import static com.cla.common.constants.ContentMetadataFieldType.GROUP_ID;

@Category(UnitTestCategory.class)
public class SolrQueryGeneratorTest {

    @Test
    public void testOrQuery(){
        Criteria criteria = Criteria.or(
                Criteria.create(GROUP_ID, SolrOperator.MISSING, null),
                Criteria.create(GROUP_ID, SolrOperator.NE, 13));
        SolrQuery query = Query.create()
                .addFilterWithCriteria(criteria)
                .build();
        Assert.assertEquals("q=*:*&fq=((-groupId:[*+TO+*]))+OR+(-groupId:13)", query.toString());
    }

    @Test
    public void testNestedQuery(){
        Criteria idCrit = Criteria.create(ContentMetadataFieldType.ID, SolrOperator.EQ, 1);
        Criteria andCrit = Criteria.and(
                Criteria.create(ContentMetadataFieldType.FILE_COUNT, SolrOperator.GT, 1),
                Criteria.create(ContentMetadataFieldType.FILE_COUNT, SolrOperator.LT, 10));

        Criteria criteria = Criteria.or(
                idCrit, andCrit,
                Criteria.create(GROUP_ID, SolrOperator.MISSING, null),
                Criteria.create(GROUP_ID, SolrOperator.NE, 13));
        SolrQuery query = Query.create()
                .addFilterWithCriteria(criteria)
                .build();
        Assert.assertEquals("q=*:*&fq=(id:1)+OR+((fileCount:{1+TO+*})+AND+(fileCount:{*+TO+10}))+OR+((-groupId:[*+TO+*]))+OR+(-groupId:13)", query.toString());
    }

    @Test
    public void testDateQuery(){
        long epoch = 1555588450000L;
        String expectedResult = "q=*:*&fq=last_record_update:{*+TO+2019\\-04\\-18T11\\:54\\:10Z}";

        Query query = Query.create()
                .addFilterWithCriteria(
                        Criteria.create(CatFileFieldType.LAST_RECORD_UPDATE, SolrOperator.LT, epoch));
        SolrQuery sq = query.build();
        Assert.assertEquals(expectedResult, sq.toString());

        Date date = new Date(epoch);
        query = Query.create()
                .addFilterWithCriteria(
                        Criteria.create(CatFileFieldType.LAST_RECORD_UPDATE, SolrOperator.LT, date));
        sq = query.build();
        Assert.assertEquals(expectedResult, sq.toString());

        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond((new Long(epoch/1000)).intValue(), 0, ZoneOffset.UTC);
        query = Query.create()
                .addFilterWithCriteria(
                        Criteria.create(CatFileFieldType.LAST_RECORD_UPDATE, SolrOperator.LT, localDateTime));
        sq = query.build();
        Assert.assertEquals(expectedResult, sq.toString());
    }
}
