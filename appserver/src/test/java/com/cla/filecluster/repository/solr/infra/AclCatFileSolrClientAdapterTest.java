package com.cla.filecluster.repository.solr.infra;

import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.common.domain.dto.security.Authority;
import com.cla.common.domain.dto.security.UserType;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.access.AccessDeniedException;

import java.nio.file.attribute.AclEntryType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import static com.cla.filecluster.SecurityTestHelper.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class AclCatFileSolrClientAdapterTest {

    @Mock
    private SolrClientAdapter client;

    @Mock
    private SharePermissionService sharePermissionService;

    private AclCatFileSolrClientAdapter adapter;

    @Before
    public void init(){
        adapter = new AclCatFileSolrClientAdapter(client, sharePermissionService,
                Lists.newArrayList("\\Everyone"), false, 131241);
    }

    @After
    public void destroy() {
        Mockito.reset(sharePermissionService);
    }

    /**
     * Test access for users with functional role that contains ViewFiles permission
     * @throws Exception N/A
     */
    @Test
    public void testViewAllFilesFuncRole() throws Exception{

        //-------------------------- set up -------------------------------------------------------------------
        User user1 = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(1L)
                        .withName("func_1")
                        .withAuthorities(Authority.ViewFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(2L)
                        .withName("func_2")
                        .withAuthorities(Authority.ViewFiles))
                .build();

        //-------------------------- verifications ------------------------------------------------------------
        testStringQuery(user1, "*:*&fq=dataRoles:(frole.1.* frole.2.*)");
        testSolrQuery(user1, "dataRoles:(frole.1.* frole.2.*)");
    }

    @Test(expected = AccessDeniedException.class)
    public void testNoAccess() throws Exception{

        //-------------------------- set up -------------------------------------------------------------------
        User user = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withAccessTokens("\\Everyone", "Administrators")
                .build();

        authenticate(user);
        //-------------------------- operation ----------------------------------------------------------------
        adapter.query(new SolrQuery("*:*"));
    }

    @Test
    public void testViewPermittedBizRole() throws Exception {

        //-------------------------- set up -------------------------------------------------------------------
        User user = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withBizRole(BizRoleBuilder.create().withName("BizRole_1").withAuthorities(Authority.ViewPermittedFiles))
                .withAccessTokens("\\Everyone", "Administrators")
                .build();

        //-------------------------- verifications ------------------------------------------------------------
        testStringQuery(user, "*:*&fq=search_acl_read:(\"\\everyone\" \"administrators\")&fq=-search_acl_denied_read:(\"\\everyone\" \"administrators\")");
        testSolrQuery(user, "search_acl_read:(\"\\everyone\" \"administrators\")", "-search_acl_denied_read:(\"\\everyone\" \"administrators\")");
    }

    @Test
    public void testViewPermittedBizRoleWithDenyRootFolder() throws Exception {
        //-------------------------- set up -------------------------------------------------------------------

        Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions = new HashMap<>();
        rootFolderSharePermissions.put(3L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\RND", AclEntryType.ALLOW)));
        when(sharePermissionService.getAllSharePermissions()).thenReturn(rootFolderSharePermissions);

        User user = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withBizRole(BizRoleBuilder.create().withName("BizRole_1").withAuthorities(Authority.ViewPermittedFiles))
                .withAccessTokens("\\Everyone", "Administrators")
                .build();

        //-------------------------- verifications ------------------------------------------------------------
        testStringQuery(user, "*:*&fq=search_acl_read:(\"\\everyone\" \"administrators\")&fq=-search_acl_denied_read:(\"\\everyone\" \"administrators\")&fq=-rootFolderId:(3)");
        testSolrQuery(user, "search_acl_read:(\"\\everyone\" \"administrators\")", "-search_acl_denied_read:(\"\\everyone\" \"administrators\")", "-rootFolderId:(3)");
    }

    @Test
    public void testViewPermittedBizRoleWithDenyRootFolders() throws Exception {
        //-------------------------- set up -------------------------------------------------------------------

        Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions = new HashMap<>();
        rootFolderSharePermissions.put(3L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\RND", AclEntryType.ALLOW)));
        rootFolderSharePermissions.put(4L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\RND", AclEntryType.ALLOW)));
        when(sharePermissionService.getAllSharePermissions()).thenReturn(rootFolderSharePermissions);

        User user = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withBizRole(BizRoleBuilder.create().withName("BizRole_1").withAuthorities(Authority.ViewPermittedFiles))
                .withAccessTokens("\\Everyone", "Administrators")
                .build();

        //-------------------------- verifications ------------------------------------------------------------
        testStringQuery(user, "*:*&fq=search_acl_read:(\"\\everyone\" \"administrators\")&fq=-search_acl_denied_read:(\"\\everyone\" \"administrators\")&fq=-rootFolderId:(3 4)");
        testSolrQuery(user, "search_acl_read:(\"\\everyone\" \"administrators\")", "-search_acl_denied_read:(\"\\everyone\" \"administrators\")", "-rootFolderId:(3 4)");
    }

    @Test
    public void testSystemUser() throws Exception {

        //-------------------------- set up -------------------------------------------------------------------
        User user = UserBuilder.create()
                .withId(2L)
                .withUsername("user_2")
                .withUserType(UserType.SYSTEM)
                .build();

        testStringQuery(user,"*:*");
        testSolrQuery(user);
    }

    @Test
    public void testMixedFuncRoles() throws Exception {

        //-------------------------- set up -------------------------------------------------------------------
        UserBuilder builder = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withAccessTokens("\\Everyone", "Administrators")
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(1L)
                        .withName("func_f1")
                        .withAuthorities(Authority.ViewFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(2L)
                        .withName("func_f2")
                        .withAuthorities(Authority.ViewFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(3L)
                        .withName("func_p1")
                        .withAuthorities(Authority.ViewPermittedFiles));

        User user1 = builder.build();

        builder = builder.withFuncRole(FuncRoleBuilder.create()
                .withId(4L)
                .withName("func_p2")
                .withAuthorities(Authority.ViewPermittedFiles));

        User user2 = builder.build();

        //-------------------------- verifications ------------------------------------------------------------
        String expected1 = "dataRoles:(frole.1.* frole.2.*) OR (dataRoles:frole.3.* " +
                ") OR ( search_acl_read:(\"\\everyone\" \"administrators\") AND -search_acl_denied_read:(\"\\everyone\" \"administrators\"))";

        testStringQuery(user1, "*:*&fq=" + expected1);
        testSolrQuery(user1, expected1);

        String expected2 = "dataRoles:(frole.1.* frole.2.*) OR (dataRoles:(frole.3.* frole.4.*) " +
                ") OR ( search_acl_read:(\"\\everyone\" \"administrators\") AND -search_acl_denied_read:(\"\\everyone\" \"administrators\"))";

        testStringQuery(user2, 1, "*:*&fq=" + expected2);
        testSolrQuery(user2, 1, expected2);
    }

    @Test
    public void testMixedFuncRolesAndSharedPerm() throws Exception {

        Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions = new HashMap<>();
        rootFolderSharePermissions.put(3L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\RND", AclEntryType.ALLOW)));
        rootFolderSharePermissions.put(4L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\RND", AclEntryType.ALLOW)));
        when(sharePermissionService.getAllSharePermissions()).thenReturn(rootFolderSharePermissions);

        //-------------------------- set up -------------------------------------------------------------------
        UserBuilder builder = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withAccessTokens("\\Everyone", "Administrators")
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(1L)
                        .withName("func_f1")
                        .withAuthorities(Authority.ViewFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(2L)
                        .withName("func_f2")
                        .withAuthorities(Authority.ViewFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(3L)
                        .withName("func_p1")
                        .withAuthorities(Authority.ViewPermittedFiles));

        User user1 = builder.build();

        builder = builder.withFuncRole(FuncRoleBuilder.create()
                .withId(4L)
                .withName("func_p2")
                .withAuthorities(Authority.ViewPermittedFiles));

        User user2 = builder.build();

        //-------------------------- verifications ------------------------------------------------------------
        String expected1 = "dataRoles:(frole.1.* frole.2.*) OR (dataRoles:frole.3.* " +
                ") OR ( search_acl_read:(\"\\everyone\" \"administrators\") AND -search_acl_denied_read:(\"\\everyone\" \"administrators\") AND -rootFolderId:(3 4))";

        testStringQuery(user1, "*:*&fq=" + expected1);
        testSolrQuery(user1, expected1);

        String expected2 = "dataRoles:(frole.1.* frole.2.*) OR (dataRoles:(frole.3.* frole.4.*) " +
                ") OR ( search_acl_read:(\"\\everyone\" \"administrators\") AND -search_acl_denied_read:(\"\\everyone\" \"administrators\") AND -rootFolderId:(3 4))";

        testStringQuery(user2, 1, "*:*&fq=" + expected2);
        testSolrQuery(user2, 1, expected2);
    }

    @Test
    public void testViewPermittedFuncRoles() throws Exception {

        //-------------------------- set up -------------------------------------------------------------------
        User user1 = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withAccessTokens("\\Everyone", "Administrators")
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(1L)
                        .withName("func_f1")
                        .withAuthorities(Authority.ViewPermittedFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(2L)
                        .withName("func_f2")
                        .withAuthorities(Authority.ViewPermittedFiles))
                .build();

        //-------------------------- verifications ------------------------------------------------------------
        testStringQuery(user1, "*:*&fq=" + "dataRoles:(frole.1.* frole.2.*)" +
                "&fq=search_acl_read:(\"\\everyone\" \"administrators\")" +
                "&fq=-search_acl_denied_read:(\"\\everyone\" \"administrators\")");

        testSolrQuery(user1,"dataRoles:(frole.1.* frole.2.*)",
                "search_acl_read:(\"\\everyone\" \"administrators\")",
                "-search_acl_denied_read:(\"\\everyone\" \"administrators\")");
    }

    @Test
    public void testMixedBizAndFuncRoles() throws Exception {

        //-------------------------- set up -------------------------------------------------------------------
        User user1 = UserBuilder.create()
                .withId(1L)
                .withUsername("user_1")
                .withAccessTokens("\\Everyone", "Administrators")
                .withBizRole(BizRoleBuilder.create().
                        withName("BizRole_1")
                        .withAuthorities(Authority.ViewPermittedFiles))
                .withFuncRole(FuncRoleBuilder.create()
                        .withId(1L)
                        .withName("func_f1")
                        .withAuthorities(Authority.ViewFiles))
                .build();

        //-------------------------- verifications ------------------------------------------------------------
        testStringQuery(user1, "*:*&fq=(search_acl_read:(\"\\everyone\" \"administrators\") " +
                "AND -search_acl_denied_read:(\"\\everyone\" \"administrators\")) OR (dataRoles:frole.1.*)");

        testSolrQuery(user1,"(search_acl_read:(\"\\everyone\" \"administrators\") " +
                "AND -search_acl_denied_read:(\"\\everyone\" \"administrators\")) OR (dataRoles:frole.1.*)");
    }

    private void testStringQuery(User user, String expectedFilterQuery) throws Exception {
        testStringQuery(user, 0, expectedFilterQuery);
    }

    private void testStringQuery(User user, int captureIndex, String expectedQuery) throws Exception {
        authenticate(user);
        //-------------------------- operation ----------------------------------------------------------------
        adapter.deleteByQuery("*:*");
        //-------------------------- verifications ------------------------------------------------------------
        ArgumentCaptor<String> resultCaptor = ArgumentCaptor.forClass(String.class);
        verify(client, atLeastOnce()).deleteByQuery(resultCaptor.capture());

        String strQuery = captureSingleArgument(resultCaptor, captureIndex);
        assertEquals(expectedQuery, strQuery);
    }

    private void testSolrQuery(User user, String ... expectedFilterQuery) throws Exception{
        testSolrQuery(user, 0, expectedFilterQuery);
    }

    private void testSolrQuery(User user, int captureIndex, String ... expectedFilterQuery) throws Exception {
        authenticate(user);
        //-------------------------- operation ----------------------------------------------------------------
        adapter.query(new SolrQuery("*:*"));
        //-------------------------- verifications ------------------------------------------------------------
        ArgumentCaptor<SolrQuery> queryCaptor = ArgumentCaptor.forClass(SolrQuery.class);
        verify(client, atLeastOnce()).query(queryCaptor.capture());
        SolrQuery solrQueryParam = captureSingleArgument(queryCaptor, captureIndex);

        assertEquals("*:*", solrQueryParam.getQuery());

        String[] filterQueries = solrQueryParam.getFilterQueries();
        if (expectedFilterQuery != null && expectedFilterQuery.length > 0) {
            assertEquals(expectedFilterQuery.length, filterQueries.length);
            IntStream.range(0, expectedFilterQuery.length).forEach(index -> assertEquals(expectedFilterQuery[index], filterQueries[index]));
        }
    }

    private <T> T captureSingleArgument(ArgumentCaptor<T> queryCaptor, int index) {
        List<T> queries = queryCaptor.getAllValues();
        T param = queries.get(index);
        assertNotNull(param);
        return param;
    }

    private RootFolderSharePermissionDto createSharePermission(long mask, String user, AclEntryType type) {
        RootFolderSharePermissionDto dto = new RootFolderSharePermissionDto();
        dto.setMask(mask);
        dto.setAclEntryType(type);
        dto.setUser(user);
        return dto;
    }
}
