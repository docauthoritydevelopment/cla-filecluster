package com.cla.filecluster.repository.solr;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.Range;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class SolrFacetQueryJsonTest {

    @Test
    public void testFuncQueryString() {
        SolrFacetQueryJson jsonFacet = new SolrFacetQueryJson();

        ImmutableMap.of(
                "0", "50000",
                "50000", "100000",
                "100000", "10000000")
                .forEach((from, to) -> {
                    SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.SIZE.getSolrName() + from);
                    innerFacet.setType(FacetType.QUERY);
                    innerFacet.setQueryValueString(Range.create(from, to).toString());
                    innerFacet.setFunc(FacetFunction.SUM);
                    innerFacet.setField(CatFileFieldType.SIZE);
                    jsonFacet.addFacet(innerFacet);
                });

        String result = "{facet : {" +
                "\"size0\":{type : query,q : 'size:[0 TO 50000]',facet : {size_sum:\"sum(size)\"}}," +
                "\"size50000\":{type : query,q : 'size:[50000 TO 100000]',facet : {size_sum:\"sum(size)\"}}," +
                "\"size100000\":{type : query,q : 'size:[100000 TO 10000000]',facet : {size_sum:\"sum(size)\"}}" +
                "}}";
        assertEquals(result, jsonFacet.toFuncQueryString());
    }

    @Test
    public void testFuncQueryStringArray() {
        List<SolrFacetQueryJson> jsonFacetObj = new ArrayList<>();

        ImmutableMap.of(
                "0", "50000",
                "50000", "100000",
                "100000", "10000000")
                .forEach((from, to) -> {
                    SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.SIZE.getSolrName() + from);
                    innerFacet.setType(FacetType.QUERY);
                    innerFacet.setQueryValueString(Range.create(from, to).toString());
                    innerFacet.setFunc(FacetFunction.SUM);
                    innerFacet.setField(CatFileFieldType.SIZE);
                    jsonFacetObj.add(innerFacet);
                });

        String jsonFacetRes = jsonFacetObj.stream().map(SolrFacetQueryJson::toFuncQueryString)
                .collect(Collectors.joining(", ","{", "}"));

        String result = "{" +
                "\"size0\":{type : query,q : 'size:[0 TO 50000]',facet : {size_sum:\"sum(size)\"}}, " +
                "\"size50000\":{type : query,q : 'size:[50000 TO 100000]',facet : {size_sum:\"sum(size)\"}}, " +
                "\"size100000\":{type : query,q : 'size:[100000 TO 10000000]',facet : {size_sum:\"sum(size)\"}}" +
                "}";
        assertEquals(result, jsonFacetRes);
    }
}