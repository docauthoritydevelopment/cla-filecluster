package com.cla.filecluster.batch.writer.utils;

import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.Silent.class)
public class TaskCreationTest {

    @Mock
    private SequenceIdGenerator generator;

    private AtomicLong sequence = new AtomicLong(0);

    @Test
    public void testTaskCreation(){
        when(generator.nextSequence(any(SequenceIdGenerator.SequenceType.class))).thenReturn(sequence.getAndIncrement());
        List<SimpleTask> batch = Lists.newArrayList();
        SimpleTask validationFailingTask = createBasicSimpleTask();
        validationFailingTask.setItemId(null);
        batch.add(createBasicSimpleTask());
        batch.add(validationFailingTask);

        TimeSource timeSource = TimeSource.create();
        timeSource.setSimulatedTime(1553685019162L);

        Field field;
        try {
            field = TaskCreation.class.getDeclaredField("timeSource");
            field.setAccessible(true);
            field.set(null, timeSource);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String statement = TaskCreation.batchedTaskStatement(generator, batch)
                .orElseThrow(() -> new RuntimeException("got empty statement. tasks not valid"));

        Assert.assertEquals("INSERT INTO `jm_tasks` (`id`, `created_ondb`, `deleted`, `retries`, " +
                "`run_context`, `state`, `state_update_time`, `type`, `item_id`, `job_id`, `json_data`, `name`, " +
                "`part_id`, `version`, path) VALUES ('0', '2019-03-27 13:10:19.162',false, '0', '1', '0', " +
                "'1553685019162', '31', '11', '1', null, null, '1', '1', '/a/b/c')", statement);
    }

    private SimpleTask createBasicSimpleTask(){
        SimpleTask simpleTask = new SimpleTask(TaskType.INGEST_FILE, 1L);
        simpleTask.setDeleted(false);
        simpleTask.setRetries(0);
        simpleTask.setRunContext(1L);
        simpleTask.setState(TaskState.NEW);
        simpleTask.setStateUpdateTime(1553685019162L);
        simpleTask.setItemId(11);
        simpleTask.setJobId(1L);
        simpleTask.setVersion(1);
        simpleTask.setPath("/a/b/c");
        return simpleTask;
    }
}
