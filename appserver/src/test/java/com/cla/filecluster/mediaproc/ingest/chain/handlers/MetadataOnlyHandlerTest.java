package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MetadataOnlyHandlerTest {

    @Mock
    protected FilesDataPersistenceService filesDataPersistenceService;

    @InjectMocks
    private MetadataOnlyHandler metadataOnlyHandler;

    @Test
    public void testShouldHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        Assert.assertTrue(metadataOnlyHandler.shouldHandle(context));

        IngestChainHandlersTestUtils.addWordPayload(context);
        Assert.assertFalse(metadataOnlyHandler.shouldHandle(context));

        context.getPayload().setMetadataOnly(true);
        Assert.assertTrue(metadataOnlyHandler.shouldHandle(context));
    }

    @Test
    public void testHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        Assert.assertFalse(metadataOnlyHandler.handle(context));

        verify(filesDataPersistenceService, times(1)).updateFileProperties(eq(1L),
                eq(1L), eq(1L), any(ClaFilePropertiesDto.class), any(IngestBatchStore.class));

    }

}
