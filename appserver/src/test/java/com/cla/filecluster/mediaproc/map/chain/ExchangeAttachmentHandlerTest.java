package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.FileToDel;
import com.cla.filecluster.mediaproc.map.ItemToCreate;
import com.cla.filecluster.mediaproc.map.MapDeleteHandler;
import com.cla.filecluster.mediaproc.map.MapExchangeServices;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class ExchangeAttachmentHandlerTest {

    @Mock
    private MapExchangeServices mapExchangeServices;

    @Mock
    private MapDeleteHandler mapDeleteHandler;

    @Mock
    private DocStoreService docStoreService;

    @InjectMocks
    private ExchangeAttachmentHandler exchangeAttachmentHandler;

    @Before
    public void init() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);
    }

    @Test
    public void testShouldHandleNoAtt() {
        MapResultContext mapContext = getContext();
        assertTrue(!exchangeAttachmentHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleNewAtt() {
        MapResultContext mapContext = getContextWithNewFile();
        assertTrue(exchangeAttachmentHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleNewAttAndDel() {
        MapResultContext mapContext = getContextWithDelAtt(true);
        assertTrue(exchangeAttachmentHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleDelAtt() {
        MapResultContext mapContext = getContextWithDelAtt(false);
        assertTrue(exchangeAttachmentHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandleNewAtt() {
        MapResultContext mapContext = getContextWithNewFile();
        verifyCallsHandle(mapContext, 0);
    }

    @Test
    public void testHandleNewAndDelAtt() {
        MapResultContext mapContext = getContextWithDelAtt(true);
        verifyCallsHandle(mapContext, 1);
    }

    @Test
    public void testHandleDelAtt() {
        MapResultContext mapContext = getContextWithDelAtt(false);
        verifyCallsHandle(mapContext, 1);
    }

    @Test
    public void testHandleSeveralDelAtt() {
        MapResultContext mapContext = getContextWithDelAtt(false);
        mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToDelete().add(createDelAtt(6L));
        verifyCallsHandle(mapContext, 2);
    }

    public void verifyCallsHandle(MapResultContext mapContext, int delTimes) {

        List<Triple<FileToDel, FileEntity, String>> attToDel = new ArrayList<>();

        for (int i = 0; i < delTimes; ++i) {
            attToDel.add(Triple.of(new FileToDel((long)i,
                    mapContext.getScanResultMessage().getRunContext(), null, null),
                    new FileEntity(), ""));
        }

        when(mapExchangeServices.handleAttachmentDeletion(
                anyCollection(), any(RootFolder.class),
                anyLong(), anyLong(), anyLong())).thenReturn(attToDel);

        exchangeAttachmentHandler.handle(mapContext);
        verify(mapExchangeServices, times(1)).handleAttachmentCreation(anyMap(), any(Pair.class),
                any(RootFolder.class), anyLong(), anyLong(), anyLong(), anyMap());

        verify(mapExchangeServices, times(1)).handleAttachmentDeletion(
                delTimes == 0 ? isNull(Collection.class) : anyCollection(),
                any(RootFolder.class), anyLong(), anyLong(), anyLong());

        verify(mapDeleteHandler, times(delTimes)).handleFileToDelete(any(Pair.class), anyLong(),
                anyLong(), any(FileToDel.class), any(FileEntity.class), anyString());
    }

    private MapResultContext getContextWithDelAtt(boolean useNewAtt) {
        MapResultContext mapContext = getContextWithNewFile();

        List<SolrFileEntity> attachmentsToDelete = new ArrayList<>();
        attachmentsToDelete.add(createDelAtt(3L));

        ItemToCreate item = new ItemToCreate(mapContext.getSolrInputDocumentFileAndFolderId().getDocFileToCreate(), null,
                useNewAtt ? mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToCreate() : new HashMap<>(),
                attachmentsToDelete);

        mapContext.setSolrInputDocumentFileAndFolderId(item);

        return mapContext;
    }

    private MapResultContext getContextWithNewFile() {
        MapResultContext mapContext = getContext();

        Map<String, FileCatBuilder> attachmentsToCreate = new HashMap<>();
        attachmentsToCreate.put("1", createNewAtt(1L));
        attachmentsToCreate.put("2", createNewAtt(2L));

        ItemToCreate item = new ItemToCreate(new SolrInputDocument(), null, attachmentsToCreate, null);
        item.getDocFileToCreate().setField(CatFileFieldType.FOLDER_ID.getSolrName(), SolrRepositoryUtils.setValueMap(2L));
        item.getDocFileToCreate().setField(CatFileFieldType.SCOPED_TAGS.getSolrName(), SolrRepositoryUtils.setValueMap("scopetag"));
        item.getDocFileToCreate().setField(CatFileFieldType.TAGS.getSolrName(), SolrRepositoryUtils.setValueMap("tag"));

        mapContext.setSolrInputDocumentFileAndFolderId(item);

        return mapContext;
    }

    private SolrFileEntity createDelAtt(Long fileId) {
        SolrFileEntity entity = new SolrFileEntity();
        entity.setFileId(fileId);
        return entity;
    }

    private FileCatBuilder createNewAtt(Long fileId) {
        FileCatBuilder builder = new FileCatBuilder();
        builder.setFileId(fileId);
        return builder;
    }

    private MapResultContext getContext() {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setKvdbKey("dfgd");
        mapContext.setFolderId(2L);
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }
}