package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.mediaproc.map.ItemToCreate;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.cla.common.domain.dto.messages.MessagePayloadType.SCAN_DELETION;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class EndMapForFileTest {

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private EndMapForFile endMapForFile;

    @After
    public void clean() {
        Mockito.reset(mapServiceUtils);
        Mockito.reset(mapBatchResultsProcessor);
        Mockito.reset(eventBus);
    }

    @Test
    public void testNewFile() {
        MapResultContext mapContext = getContextWithNewFile();
        endMapForFile.handle(mapContext);

        verify(mapServiceUtils, times(1)).handleFinishMapForNewFile(
                any(SolrInputDocument.class), anyLong(), any(FileType.class),
                anyString(), anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyList());

        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(
                any(Pair.class), any(FileToAdd.class));

        verify(eventBus, times(0)).broadcast(anyString(), any(Event.class));
    }

    @Test
    public void testExistingFileWithFileEntity() {
        MapResultContext mapContext = getContext();
        mapContext.setFileEntity(new FileEntity());

        endMapForFile.handle(mapContext);

        verify(mapServiceUtils, times(0)).handleFinishMapForNewFile(
                any(SolrInputDocument.class), anyLong(), any(FileType.class),
                anyString(), anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyList());

        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(
                any(Pair.class), any(FileToAdd.class));

        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));
    }

    @Test
    public void testExistingFileWithNoFileEntity() {
        MapResultContext mapContext = getContext();

        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(new FileEntity());

        endMapForFile.handle(mapContext);

        verify(mapServiceUtils, times(1)).getFileEntity(anyLong(), anyString());

        verify(mapServiceUtils, times(0)).handleFinishMapForNewFile(
                any(SolrInputDocument.class), anyLong(), any(FileType.class),
                anyString(), anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyList());

        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(
                any(Pair.class), any(FileToAdd.class));

        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));
    }


    @Test
    public void testDelete() {
        MapResultContext mapContext = getContext();
        mapContext.getScanResultMessage().setPayloadType(SCAN_DELETION);

        endMapForFile.handle(mapContext);

        verify(mapServiceUtils, times(0)).handleFinishMapForNewFile(
                any(SolrInputDocument.class), anyLong(), any(FileType.class),
                anyString(), anyLong(), anyLong(), anyLong(), anyLong(), anyLong(), anyList());

        verify(mapBatchResultsProcessor, times(0)).collectCatFiles(
                any(Pair.class), any(FileToAdd.class));

        verify(eventBus, times(0)).broadcast(anyString(), any(Event.class));
    }

    private MapResultContext getContextWithNewFile() {
        MapResultContext mapContext = getContext();

        ItemToCreate item = new ItemToCreate(new SolrInputDocument(), null, null, null);
        item.getDocFileToCreate().setField(CatFileFieldType.FOLDER_ID.getSolrName(), SolrRepositoryUtils.setValueMap(2L));

        mapContext.setSolrInputDocumentFileAndFolderId(item);

        return mapContext;
    }

    private MapResultContext getContext() {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setFileId(5L);
        mapContext.setKvdbKey("dfgd");
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setJobId(6L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setClaFileId(5L);
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }
}