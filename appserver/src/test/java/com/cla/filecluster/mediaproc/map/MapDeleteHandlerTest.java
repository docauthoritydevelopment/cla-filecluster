package com.cla.filecluster.mediaproc.map;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.kvdb.KeyValueDaoConfig;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.cla.test.filecluster.kvdb.KFileEntityDao;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapDeleteHandlerTest {

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private ScanDiffEventProvider scanDiffEventProvider;

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private MapExchangeServices mapExchangeServices;

    @Mock
    private KeyValueDatabaseRepository kvdbRepo;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private MapDeleteHandler mapDeleteHandler;

    private static HashMap<String, FileEntity> keysToFileEntity = new HashMap<>();

    @Before
    public void set() {
        KFileEntityDao dao = new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity);
        when(kvdbRepo.getFileEntityDao()).thenReturn(dao);
    }

    @After
    public void clean() {
        Mockito.reset(kvdbRepo);
        Mockito.reset(docStoreService);
        Mockito.reset(mapServiceUtils);
        Mockito.reset(scanDiffEventProvider);
        Mockito.reset(mapExchangeServices);
        Mockito.reset(docFolderService);
        Mockito.reset(fileCatService);
        Mockito.reset(mapBatchResultsProcessor);
    }

    @Test
    public void testHandleDeletedFilesForUndeletedFile() {

        Pair<Long, Long> batchTaskId = Pair.of(67L, 7L);

        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileId(66L);
        keysToFileEntity.put("file", fileEntity);


        List<SolrFileEntity> fileEntities = new ArrayList<>();
        SolrFileEntity file = new SolrFileEntity();
        file.setFileId(66L);
        fileEntities.add(file);

        when(fileCatService.findByFileIds(any())).thenReturn(fileEntities);

        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(4L);
        when(docStoreService.getRootFolderCached(4L)).thenReturn(rootFolder);

        mapDeleteHandler.handleDeletedFiles(6L, 7L, 4L, 67L, "file");

        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(batchTaskId,
                new FileToAdd(null, fileEntity, "file", 4L, 6L));

        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, 66L, "file");

        verify(mapBatchResultsProcessor, times(1)).collectDeletedFiles(batchTaskId,
                new FileToDel(66L, 6L, null, null));

        verify(mapBatchResultsProcessor, times(1)).collectContentMetadataFileCountToUpdate(batchTaskId, 66L);

        assertTrue(fileEntity.isDeleted());
    }

    @Test
    public void testHandleDeletedFilesForFileAlreadyHandled() {

        Pair<Long, Long> batchTaskId = Pair.of(67L, 7L);

        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileId(66L);
        keysToFileEntity.put("file", fileEntity);


        List<SolrFileEntity> fileEntities = new ArrayList<>();
        SolrFileEntity file = new SolrFileEntity();
        file.setFileId(66L);
        fileEntities.add(file);

        when(fileCatService.findByFileIds(any())).thenReturn(fileEntities);

        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(4L);
        when(docStoreService.getRootFolderCached(4L)).thenReturn(rootFolder);

        when(mapBatchResultsProcessor.fileAlreadySetForDelete(any())).thenReturn(true);

        mapDeleteHandler.handleDeletedFiles(6L, 7L, 4L, 67L, "file");

        verify(mapBatchResultsProcessor, times(0)).collectCatFiles(batchTaskId,
                new FileToAdd(null, fileEntity, "file", 4L, 6L));

        verify(scanDiffEventProvider, times(0)).generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, 66L, "file");

        verify(mapBatchResultsProcessor, times(0)).collectDeletedFiles(batchTaskId,
                new FileToDel(66L, 6L, null, null));

        verify(mapBatchResultsProcessor, times(0)).collectContentMetadataFileCountToUpdate(batchTaskId, 66L);

        assertTrue(fileEntity.isDeleted());
    }

    @Test
    public void testHandleDeletedFilesForUndeletedFolder() {

        Pair<Long, Long> batchTaskId = Pair.of(67L, 7L);

        FileEntity folderEntity = new FileEntity();
        folderEntity.setFolderId(6L);
        folderEntity.setFolder(true);
        folderEntity.setFile(false);
        keysToFileEntity.put("folder", folderEntity);

        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileId(66L);
        keysToFileEntity.put("file", fileEntity);

        DocFolder df = new DocFolder();
        when(docFolderService.findById(anyLong())).thenReturn(df);

        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(4L);
        rootFolder.setRealPath("");
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(4L)).thenReturn(rootFolder);

        when(docFolderService.getChildrenDocFolders(any())).thenReturn(new ArrayList<>());

        List<SolrFileEntity> fileEntities = new ArrayList<>();
        SolrFileEntity file = new SolrFileEntity();
        file.setFileId(66L);
        file.setMediaEntityId("file");
        fileEntities.add(file);

        when(fileCatService.findAllForDocFolders(any())).thenReturn(fileEntities);
        when(fileCatService.findByFileIds(any())).thenReturn(fileEntities);

        mapDeleteHandler.handleDeletedFiles(6L, 7L, 4L, 67L, "folder");

        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(batchTaskId, new FileToAdd(null, folderEntity, "folder", 4L, 6L));

        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(batchTaskId,
                new FileToAdd(null, fileEntity, "file", 4L, 6L));

        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, 66L, "file");

        verify(mapBatchResultsProcessor, times(1)).collectDeletedFiles(batchTaskId,
                new FileToDel(66L, 6L, null, null));

        verify(mapBatchResultsProcessor, times(1)).collectContentMetadataFileCountToUpdate(batchTaskId, 66L);

        assertTrue(fileEntity.isDeleted());

        assertTrue(folderEntity.isDeleted());
    }
}