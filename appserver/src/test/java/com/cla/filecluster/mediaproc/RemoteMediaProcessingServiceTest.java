package com.cla.filecluster.mediaproc;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

public class RemoteMediaProcessingServiceTest {

    @Test
    public void testShouldGetSharePermissions() {
        RemoteMediaProcessingService remoteMediaProcessingService = new RemoteMediaProcessingService();
        SharePermissionService sharePermissionService = new SharePermissionService();

        ReflectionTestUtils.setField(sharePermissionService, "sharePermissionFeatureEnabled", true);
        ReflectionTestUtils.setField(remoteMediaProcessingService, "sharePermissionService", sharePermissionService);

        String path = "\\\\DALT-YAEL\\Enron\\pattern";
        boolean res = remoteMediaProcessingService.shouldGetSharePermissions(MediaType.FILE_SHARE, path, 0);
        Assert.assertTrue(res);

        path = "D:\\Enron\\aabCopy";
        res = remoteMediaProcessingService.shouldGetSharePermissions(MediaType.FILE_SHARE, path, 0);
        Assert.assertFalse(res);

        path = "\\\\DALT-YAEL\\d$\\pattern";
        res = remoteMediaProcessingService.shouldGetSharePermissions(MediaType.FILE_SHARE, path, 0);
        Assert.assertFalse(res);

        path = "\\\\4.5.6.7\\d$\\pattern";
        res = remoteMediaProcessingService.shouldGetSharePermissions(MediaType.FILE_SHARE, path, 0);
        Assert.assertFalse(res);

        path = "\\\\4.5.6.7\\dg\\pattern";
        res = remoteMediaProcessingService.shouldGetSharePermissions(MediaType.FILE_SHARE, path, 0);
        Assert.assertTrue(res);

        path = "\\\\4.5.6.7\\D$\\pattern";
        res = remoteMediaProcessingService.shouldGetSharePermissions(MediaType.FILE_SHARE, path, 0);
        Assert.assertFalse(res);
    }
}