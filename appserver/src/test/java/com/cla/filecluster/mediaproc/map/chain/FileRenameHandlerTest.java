package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.MapRenameHandler;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FileRenameHandlerTest {

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private MapRenameHandler mapRenameHandler;

    @InjectMocks
    private FileRenameHandler fileRenameHandler;

    @Before
    public void init() {
        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileId(44L);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fileEntity);

        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(1L)).thenReturn(rootFolder);

        rootFolder = new RootFolder();
        rootFolder.setId(2L);
        rootFolder.setMediaType(MediaType.SHARE_POINT);
        rootFolder.setReingestTimeStampMs(6L);
        when(docStoreService.getRootFolderCached(2L)).thenReturn(rootFolder);
    }

    @Test
    public void testShouldHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        assertTrue(!fileRenameHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleReScanCng() {
        MapResultContext mapContext = getContext();
        mapContext.setDiffType(DiffType.CONTENT_UPDATED);
        assertTrue(!fileRenameHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleReScanAcl() {
        MapResultContext mapContext = getContext();
        mapContext.setDiffType(DiffType.ACL_UPDATED);
        assertTrue(!fileRenameHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleNewScan() {
        MapResultContext mapContext = getContext();
        mapContext.setDiffType(DiffType.NEW);
        mapContext.setFstScan(true);
        mapContext.setNewFile(true);
        assertTrue(!fileRenameHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleReScanRename() {
        MapResultContext mapContext = getContext();
        mapContext.setDiffType(DiffType.RENAMED);
        assertTrue(fileRenameHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandleFileShare() {
        MapResultContext mapContext = getContext();
        mapContext.setFileShare(true);
        testHandle(mapContext, 0);
    }

    @Test
    public void testHandleReingestNoFS() {
        MapResultContext mapContext = getContext(2L);
        mapContext.setFileShare(false);
        testHandle(mapContext, 1);
    }

    private void testHandle(MapResultContext mapContext, int ingestTimes) {
        when(mapRenameHandler.handleFileOrDeletedFolderRename(anyLong(), any(RootFolder.class),
                anyLong(), anyLong(), any(ClaFilePropertiesDto.class), any(FileEntity.class), anyBoolean())).thenReturn(true);

        fileRenameHandler.handle(mapContext);

        verify(mapRenameHandler, times(1)).handleFileOrDeletedFolderRename(anyLong(), any(RootFolder.class),
                anyLong(), anyLong(), any(ClaFilePropertiesDto.class), any(FileEntity.class), anyBoolean());

        verify(mapRenameHandler, times(ingestTimes)).createIngestTaskForRenamed(anyLong(), any(RootFolder.class),
                anyLong(), any(ScanResultMessage.class), any(ClaFilePropertiesDto.class), anyString(), any(FileEntity.class));

        assertTrue(mapContext.getFileEntity() != null);
    }

    private MapResultContext getContext() {
        return getContext(1L);
    }

    private MapResultContext getContext(Long rfId) {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setKvdbKey("dfgd");
        mapContext.setFstScan(false);
        mapContext.setNewFile(false);
        mapContext.setBelongToRF(true);
        mapContext.setAlreadyHandled(false);
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(rfId);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFileSize(55L);
        prop.setAclSignature("acl");
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }
}