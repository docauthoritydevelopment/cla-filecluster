package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.mediaproc.map.ItemToCreate;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class ApplyFolderAssociationHandlerTest {

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private AssociationsService associationsService;

    @InjectMocks
    private ApplyFolderAssociationHandler applyFolderAssociationHandler;

    @Before
    public void init() {
        SolrFolderEntity folder = new SolrFolderEntity();
        when(docFolderService.getOneById(1L)).thenReturn(Optional.of(folder));

        folder = new SolrFolderEntity();
        List<String> associations = new ArrayList<>();
        associations.add("{\"creationDate\":1565852296460,\"fileTagId\":44,\"implicit\":false,\"originFolderId\":2,\"originDepthFromRoot\":0}");
        associations.add("{\"creationDate\":1565852302313,\"docTypeId\":2,\"implicit\":false,\"originFolderId\":2,\"originDepthFromRoot\":0}");
        associations.add("{\"creationDate\":1565852307257,\"functionalRoleId\":8,\"implicit\":false,\"originFolderId\":2,\"originDepthFromRoot\":0}");
        folder.setAssociations(associations);
        when(docFolderService.getOneById(2L)).thenReturn(Optional.of(folder));
    }

    @Test
    public void testShouldHandleNoNewFile() {
        MapResultContext mapContext = getContext(1L);
        assertTrue(!applyFolderAssociationHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleNewFile() {
        MapResultContext mapContext = getContextWithNewFile(1L);
        assertTrue(applyFolderAssociationHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandleNewFileNoAss() {
        MapResultContext mapContext = getContextWithNewFile(1L);
        applyFolderAssociationHandler.handle(mapContext);
        verify(associationsService, times(0)).
                addTagDataToDocument(any(SolrInputDocument.class),
                        anyLong(), isNull(), isNull(), isNull(), anyBoolean(), any(SolrFolderEntity.class));
    }

    @Test
    public void testHandleNewFileWithAss() {
        MapResultContext mapContext = getContextWithNewFile(2L);
        applyFolderAssociationHandler.handle(mapContext);
        verify(associationsService, times(1)).
                addTagDataToDocument(any(SolrInputDocument.class),
                        anyLong(), isNull(), isNull(), isNull(), anyBoolean(), any(SolrFolderEntity.class));
    }

    private MapResultContext getContextWithNewFile(Long folderId) {
        MapResultContext mapContext = getContext(folderId);

        ItemToCreate item = new ItemToCreate(new SolrInputDocument(), null, null, null);
        item.getDocFileToCreate().setField(CatFileFieldType.FOLDER_ID.getSolrName(), SolrRepositoryUtils.setValueMap(folderId));

        mapContext.setSolrInputDocumentFileAndFolderId(item);

        return mapContext;
    }

    private MapResultContext getContext(Long folderId) {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setKvdbKey("dfgd");
        mapContext.setFolderId(folderId);
        mapContext.setFileId(6L);
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }
}