package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class UnDeleteFileHandlerTest {

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private FileCatService fileCatService;

    @InjectMocks
    private UnDeleteFileHandler unDeleteFileHandler;

    @Before
    public void init() {
        FileEntity fileEntity = new FileEntity();
        fileEntity.setFileId(44L);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fileEntity);
    }

    @Test
    public void testShouldHandleScanAcl() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(false);
        mapContext.setDiffType(DiffType.ACL_UPDATED);
        assertTrue(!unDeleteFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleReScanCng() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(false);
        mapContext.setDiffType(DiffType.CONTENT_UPDATED);
        assertTrue(!unDeleteFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleUndelete() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(false);
        mapContext.setDiffType(DiffType.UNDELETED);
        assertTrue(unDeleteFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleFstScan() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(true);
        assertTrue(!unDeleteFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void handleRegularFile() {
        MapResultContext mapContext = getContext();
        testHandle(mapContext, 1);
    }

    @Test
    public void handlePackageFile() {
        MapResultContext mapContext = getContext();
        mapContext.getScanResultMessage().setPackagedScan(true);
        testHandle(mapContext, 0);
    }

    private void testHandle(MapResultContext mapContext, int ingestTimes) {
        unDeleteFileHandler.handle(mapContext);

        verify(mapServiceUtils, times(ingestTimes)).createIngestTask(isNull(SolrInputDocument.class), anyString(),  anyLong(),
                any(FileType.class), anyLong(), anyLong(), anyLong(), anyLong(), isNull(IngestTaskJsonProperties.class));


        verify(fileCatService, times(1)).
                undeleteRecord(any(FileEntity.class), any(ClaFilePropertiesDto.class));

        assertTrue(mapContext.getFileEntity() != null);
    }

    private MapResultContext getContext() {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setKvdbKey("dfgd");
        mapContext.setFstScan(false);
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }
}