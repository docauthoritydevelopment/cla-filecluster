package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.mediaproc.map.MapBatchTaskHandler;
import com.cla.filecluster.mediaproc.map.chain.*;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.MailboxCache;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.mediaproc.map.*;
import com.cla.kvdb.*;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.cla.test.filecluster.kvdb.KFileEntityDao;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.solr.common.SolrInputDocument;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.anyCollection;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@RunWith(MockitoJUnitRunner.Silent.class)
@Category(UnitTestCategory.class)
public class ScanItemConsumerTest {

    @Mock
    private FileCrawlerExecutionDetailsService runService;

    @Mock
    private JobManagerService jobManagerService;

    @Mock
    private KeyValueDatabaseRepository kvdbRepo;

    @Mock
    private RootFolderService rootFolderService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private ScanErrorsService scanErrorsService;

    @Mock
    private ScanDiffEventProvider scanDiffEventProvider;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Mock
    private SolrFileCatRepository solrFileCatRepository;

    @Mock
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Mock
    private com.cla.filecluster.util.query.DBTemplateUtils DBTemplateUtils;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private FilesDataPersistenceService filesDataPersistenceService;

    @Mock
    private EventBus eventBus;

    @Mock
    private MapBatchTaskHandler mapBatchTaskHandler;

    @InjectMocks
    private PreProcessHandler preProcessHandler;

    @InjectMocks
    private FolderHandler folderHandler;

    @InjectMocks
    private NewFileHandler newFileHandler;

    @InjectMocks
    private AclUpdateFileHandler aclUpdateFileHandler;

    @InjectMocks
    private UnDeleteFileHandler unDeleteFileHandler;

    @InjectMocks
    private ContentUpdateFileHandler contentUpdateFileHandler;

    @InjectMocks
    private DeleteFileHandler deleteFileHandler;

    @InjectMocks
    private EndMapForFile endMapForFile;

    @InjectMocks
    private FileRenameHandler fileRenameHandler;

    @InjectMocks
    private MapErrorHandler mapErrorHandler;

    @InjectMocks
    private ExchangeAttachmentHandler exchangeAttachmentHandler;

    @InjectMocks
    private SpecialResponseHandler specialResponseHandler;

    @InjectMocks
    private MapAclHandler mapAclHandler;

    @InjectMocks
    private ApplyFolderAssociationHandler applyFolderAssociationHandler;

    @InjectMocks
    private CalcGeneralDataHandler calcGeneralDataHandler;

    @InjectMocks
    private MapDeleteHandler mapDeleteHandler;

    @InjectMocks
    private MapServiceUtils mapServiceUtils;

    @InjectMocks
    private FolderRenameHandler folderRenameHandler;

    @Mock
    private MapExchangeServices mapExchangeServices;

    @Mock
    private MailboxCache mailboxCache;

    @InjectMocks
    private MapRenameHandler mapRenameHandler;

    @InjectMocks
    private MapAddHandler mapAddHandler;

    @InjectMocks
    private ScanItemConsumerChain scanItemConsumer;

    private static HashMap<String, FileEntity> keysToFileEntity = new HashMap<>();

    private static Path filePath = Paths.get("./src/test/resources/files/analyzeTest/LoremIpsum.docx");
    private static String fileFullPath = filePath.toAbsolutePath().normalize().toString();

    @Before
    public void init() {
        KpiRecording kpiRecording = new KpiRecording("", "", 1, 1L, performanceKpiRecorder, "");
        when(performanceKpiRecorder.startRecording(anyString(), anyString(), anyInt())).thenReturn(kpiRecording);
        when(docStoreService.getRootFolderIngestTypePredicate(any())).thenReturn(path -> {
            return true;
        });
        when(kvdbRepo.getRepoName(any(), any())).thenReturn("ghjfgh");
        setField(mapRenameHandler, "mapDeleteHandler", mapDeleteHandler);
        setField(mapDeleteHandler, "mapServiceUtils", mapServiceUtils);

        setField(calcGeneralDataHandler, "mapServiceUtils", mapServiceUtils);
        setField(folderHandler, "mapServiceUtils", mapServiceUtils);
        setField(newFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(newFileHandler, "mapAddHandler", mapAddHandler);
        setField(aclUpdateFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(contentUpdateFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(unDeleteFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(endMapForFile, "mapServiceUtils", mapServiceUtils);
        setField(deleteFileHandler, "mapDeleteHandler", mapDeleteHandler);
        setField(folderHandler, "mapRenameHandler", mapRenameHandler);
        setField(fileRenameHandler, "mapServiceUtils", mapServiceUtils);
        setField(fileRenameHandler, "mapRenameHandler", mapRenameHandler);
        setField(folderRenameHandler, "mapRenameHandler", mapRenameHandler);

        setField(scanItemConsumer, "preProcessHandler", preProcessHandler);
        setField(scanItemConsumer, "specialResponseHandler", specialResponseHandler);
        setField(scanItemConsumer, "calcGeneralDataHandler", calcGeneralDataHandler);
        setField(scanItemConsumer, "folderHandler", folderHandler);
        setField(scanItemConsumer, "folderRenameHandler", folderRenameHandler);
        setField(scanItemConsumer, "newFileHandler", newFileHandler);
        setField(scanItemConsumer, "deleteFileHandler", deleteFileHandler);
        setField(scanItemConsumer, "aclUpdateFileHandler", aclUpdateFileHandler);
        setField(scanItemConsumer, "contentUpdateFileHandler", contentUpdateFileHandler);
        setField(scanItemConsumer, "fileRenameHandler", fileRenameHandler);
        setField(scanItemConsumer, "unDeleteFileHandler", unDeleteFileHandler);
        setField(scanItemConsumer, "mapAclHandler", mapAclHandler);
        setField(scanItemConsumer, "applyFolderAssociationHandler", applyFolderAssociationHandler);
        setField(scanItemConsumer, "exchangeAttachmentHandler", exchangeAttachmentHandler);
        setField(scanItemConsumer, "endMapForFile", endMapForFile);


        scanItemConsumer.init();
    }

    @After
    public void resetMocks() {
        Mockito.reset(runService);
        Mockito.reset(jobManagerService);
        Mockito.reset(kvdbRepo);
        Mockito.reset(docFolderService);
        Mockito.reset(docStoreService);
        Mockito.reset(scanErrorsService);
        Mockito.reset(scanDiffEventProvider);
        Mockito.reset(fileCatService);
        Mockito.reset(filesDataPersistenceService);
        keysToFileEntity.clear();
    }

    @Test
    public void testFileFirstScan() {
        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setPath(filePath);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L,
                true, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessages.add(scanResultMessage);
        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(1L);
        DocFolderService.DocFolderCacheValue cacheValue = new DocFolderService.DocFolderCacheValue(folder);
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).thenReturn(cacheValue);
        ClaFile claFile = new ClaFile();
        claFile.setId(1L);
        when(fileCatService.useExistingOrCreateRecord(nullable(ClaFile.class), anyLong(), anyString(), any(), any(), anyLong())).thenReturn(claFile);

        //LambdaMatcher<ClaFileState> scannedMatcher = new LambdaMatcher<>(e -> e.equals(ClaFileState.SCANNED));
        when(filesDataPersistenceService.resolveContentMetadata(anyLong(), any(SolrFileEntity.class), any(ClaFilePropertiesDto.class), any(), any(), any()))
                .then(invocation -> {
                    ContentMetadata metadata = new ContentMetadata();
                    metadata.setState(ClaFileState.SCANNED);
                    metadata.setId(111L);
                    return metadata;
                });

        FileCatBuilder fileCatBuilder = FileCatBuilder.create();
        fileCatBuilder.setId("111!1")
                .setFileId(1L)
                .setContentId(111L)
                .setDocFolderId(111L);

        //   .addModifier(MEDIA_ENTITY_ID, SET, filePath.toString());

        when(fileCatService.getBuilderForCreateUpdateRecord(nullable(ClaFile.class), anyLong(), anyLong(), nullable(List.class), any(ClaFilePropertiesDto.class),
                nullable(Long.class))).then(invocation -> fileCatBuilder);

        setRootFolder();

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(3)).broadcast(topicCaptor.capture(), eventCaptor.capture());

        List<Event> events = eventCaptor.getAllValues();

        assertTrue(events.get(2).getEventType().equals(com.cla.eventbus.events.EventType.JOB_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));

        verifyIngestNew();
    }

    @Test
    public void testFileReScanUndeleted() {
        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setPath(filePath);
        payload.setType(FileType.WORD);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L,
                false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessages.add(scanResultMessage);
        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));

        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString()))
                .thenReturn(createFolderCacheValue(1, null, null));
        setRootFolder();

        FileEntity f = new FileEntity();
        f.setFileId(5L);
        f.setDeleted(true);
        keysToFileEntity.put(fileFullPath, f);

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);
        verifyIngestUpdDel();
        payload.setClaFileId(5L);
        verify(fileCatService, times(1)).undeleteRecord(f, payload);
        //verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_UNDELETED, 5L, fileFullPath); code for this was removed, unreliable
    }

    @Test
    public void testFileReScanUpdated() {
        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setPath(filePath);
        payload.setType(FileType.WORD);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L,
                false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessages.add(scanResultMessage);
        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString()))
                .thenReturn(createFolderCacheValue(1, null, null));
        setRootFolder();

        FileEntity f = new FileEntity();
        f.setFileId(5L);
        f.setLastModified(System.currentTimeMillis());
        f.setSize(50L);
        keysToFileEntity.put(fileFullPath, f);

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(3)).broadcast(topicCaptor.capture(), eventCaptor.capture());

        List<Event> events = eventCaptor.getAllValues();

        assertTrue(events.get(1).getEventType().equals(com.cla.eventbus.events.EventType.RUN_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));

        verifyIngestUpdDel();

        payload.setClaFileId(5L);
        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_UPDATED, 5L, fileFullPath);
    }

    @Test
    public void testFileReScanAclUpdated() {
        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFileSize(50L);
        payload.setType(FileType.WORD);
        payload.setModTimeMilli(50000L);
        payload.setPath(filePath);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L,
                false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessages.add(scanResultMessage);
        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString()))
                .thenReturn(createFolderCacheValue(1, null, null));
        setRootFolder();
        FileEntity f = new FileEntity();
        f.setFileId(5L);
        f.setSize(50L);
        f.setLastModified(50000L);
        f.setAclSignature("5y545tt");
        keysToFileEntity.put(fileFullPath, f);

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);
        verifyIngestUpdDel();

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(3)).broadcast(topicCaptor.capture(), eventCaptor.capture());

        List<Event> events = eventCaptor.getAllValues();

        assertTrue(events.get(1).getEventType().equals(com.cla.eventbus.events.EventType.RUN_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));

        payload.setClaFileId(5L);
        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_ACL_UPDATED, 5L, fileFullPath);
    }

    private void setRootFolder() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        rootFolder.setMediaConnectionDetails(getMediaConnectionDetails(MediaType.FILE_SHARE));
        when(docStoreService.getRootFolderCached(1L)).thenReturn(rootFolder);
    }

    @NotNull
    private MediaConnectionDetails getMediaConnectionDetails(MediaType fileShare) {
        MediaConnectionDetails connectionDetails = new MediaConnectionDetails();
        connectionDetails.setMediaType(fileShare);
        return connectionDetails;
    }

    @Test
    public void testFileReScanAclAndContentUpdated() {
        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFileSize(50L);
        payload.setModTimeMilli(50000L);
        payload.setPath(filePath);
        payload.setType(FileType.WORD);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L,
                false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessages.add(scanResultMessage);
        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString()))
                .thenReturn(createFolderCacheValue(1, null, null));
        setRootFolder();

        FileEntity f = new FileEntity();
        f.setFileId(5L);
        f.setSize(60L);
        f.setLastModified(50000L);
        f.setAclSignature("5y545tt");
        keysToFileEntity.put(fileFullPath, f);

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(3)).broadcast(topicCaptor.capture(), eventCaptor.capture());

        List<Event> events = eventCaptor.getAllValues();

        assertTrue(events.get(1).getEventType().equals(com.cla.eventbus.events.EventType.RUN_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));

        verifyIngestUpdDel();
        payload.setClaFileId(5L);

        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_UPDATED, 5L, fileFullPath);
        //verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_ACL_UPDATED, 5L, fileFullPath); ???
    }

    @Test
    public void testFileReScanDeleted() {
        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        ItemDeletionPayload payload = new ItemDeletionPayload();
        payload.setFilename(fileFullPath);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L,
                false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessage.setPayloadType(MessagePayloadType.SCAN_DELETION);
        scanResultMessages.add(scanResultMessage);
        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString()))
                .thenReturn(createFolderCacheValue(1, null, null));
        setRootFolder();

        FileEntity f = new FileEntity();
        f.setFileId(5L);
        f.setDeleted(false);
        keysToFileEntity.put(fileFullPath, f);

        SolrFileEntity e = new SolrFileEntity();
        e.setFileId(f.getFileId());
        e.setMediaEntityId(fileFullPath);
        when(fileCatService.findByFileIds(any())).thenReturn(Lists.newArrayList(e));

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        verify(mapBatchResultsProcessor).collectDeletedFiles(any(), any());
        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, 5L, fileFullPath);
    }

    private DocFolderService.DocFolderCacheValue createFolderCacheValue(long id, List<String> parentsInfo, String mediaEntityId) {
        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(id);
        folder.setParentsInfo(parentsInfo);
        folder.setMediaEntityId(mediaEntityId);
        return new DocFolderService.DocFolderCacheValue(folder);
    }

    @Test
    public void testFolderMovedOutToUnRegisteredRootFolder() {
        String sourceRFPath = "ro-admin2@docauthority.onmicrosoft.com/path/source/";
        String targetRFPath = "ro-admin1@docauthority.onmicrosoft.com/diff-path/target/";
        String mediaItemId = "media-item-id";
        String subFolder = "sub-folder/";

        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        MediaChangeLogDto payload = new MediaChangeLogDto(mediaItemId, DiffType.RENAMED);
        payload.setFileName(targetRFPath + subFolder);
        payload.setFolder(true);
        payload.setClaFileId(555L);
        payload.setMediaItemId(mediaItemId);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L, false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessage.setPayloadType(MessagePayloadType.SCAN_CHANGES);
        scanResultMessages.add(scanResultMessage);

        DocFolder oldDocFolder = new DocFolder();
        oldDocFolder.setId(78L);
        oldDocFolder.setPath(sourceRFPath + subFolder);
        oldDocFolder.setRootFolderId(1L);

        RootFolder srcRootFolder = new RootFolder();
        srcRootFolder.setPath(sourceRFPath);
        srcRootFolder.setRealPath("");
        srcRootFolder.setId(1L);
        srcRootFolder.setMediaConnectionDetails(getMediaConnectionDetails(MediaType.ONE_DRIVE));
        srcRootFolder.setMediaType(MediaType.ONE_DRIVE);

        RootFolder targetRootFolder = new RootFolder();
        targetRootFolder.setPath(targetRFPath);
        targetRootFolder.setId(2L);
        targetRootFolder.setRealPath("");
        targetRootFolder.setMediaType(MediaType.ONE_DRIVE);

        KFileEntityDao kFileEntityDao = new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity);

        List<DocFolder> subDocFolders = LongStream.range(0, 6)
                .mapToObj(i -> {
                    DocFolder folder = new DocFolder();
                    folder.setId(i);
                    return folder;
                })
                .collect(Collectors.toList());

        when(docStoreService.getRootFolderCached(1L)).thenReturn(srcRootFolder);

        SolrFolderEntity solrFolderEntity = new SolrFolderEntity();
        solrFolderEntity.setId(1L);
        DocFolderService.DocFolderCacheValue cacheVal = new DocFolderService.DocFolderCacheValue(solrFolderEntity);

        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).thenReturn(cacheVal);
        when(docFolderService.findById(payload.getClaFileId())).thenReturn(oldDocFolder);
        when(kvdbRepo.getFileEntityDao()).thenReturn(kFileEntityDao);
        when(docFolderService.getChildrenDocFolders(nullable(List.class))).thenReturn(subDocFolders);

        List<SolrFileEntity> files = IntStream.range(0, 10)
                .mapToObj(i -> {
                    SolrFileEntity file = new SolrFileEntity();
                    file.setFolderId(oldDocFolder.getId());
                    file.setFileId((long) i);
                    file.setMediaEntityId(UUID.randomUUID().toString());
                    return file;
                })
                .map(file -> {
                    FileEntity f = new FileEntity();
                    f.setFileId(file.getFileId());
                    keysToFileEntity.put(file.getMediaEntityId(), f);
                    return file;
                })
                .collect(Collectors.toList());

        FileEntity f = new FileEntity();
        f.setFolderId(payload.getClaFileId());
        f.setFolder(true);
        f.setFile(false);
        keysToFileEntity.put(mediaItemId, f);

        kFileEntityDao.executeInReadonlyTransaction(null, store -> {
            keysToFileEntity.entrySet().stream()
                    .forEach(entry -> store.put(entry.getKey(), entry.getValue()));
            return null;
        });

        when(fileCatService.findAllForDocFolders(anyCollection())).thenReturn(files);

        when(fileCatService.findByFileIds(any())).thenReturn(files);
        when(mailboxCache.getMailBoxes(1L)).thenReturn(
                Sets.newHashSet("ro-admin2@docauthority.onmicrosoft.com/path/source/"));
        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        verify(docFolderService, times(subDocFolders.size() + 1 /* moved doc folder and its descendants */)).markDocFolderAsDeleted(anyLong());
        verify(mapBatchResultsProcessor, times(10)).collectDeletedFiles(any(), any());
    }

    @Test
    public void testFileMovedOutOfRootFolder() {
        String sourceRFPath = "ro-admin2@docauthority.onmicrosoft.com/path/source";
        String targetRFPath = "ro-admin2@docauthority.onmicrosoft.com/path/target/";
        String mediaItemId = "1234321";
        String subFolder = "sub-folder/";

        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        MediaChangeLogDto payload = new MediaChangeLogDto(mediaItemId, DiffType.RENAMED);
        payload.setFileName(targetRFPath + subFolder + "file.pdf");
        payload.setType(FileType.PDF);
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, 1L, false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessage.setPayloadType(MessagePayloadType.SCAN_CHANGES);
        scanResultMessages.add(scanResultMessage);

        RootFolder srcRootFolder = new RootFolder();
        srcRootFolder.setPath(sourceRFPath);
        srcRootFolder.setId(1L);
        srcRootFolder.setMediaType(MediaType.SHARE_POINT);

        srcRootFolder.setMediaConnectionDetails(getMediaConnectionDetails(MediaType.SHARE_POINT));

        RootFolder targetRootFolder = new RootFolder();
        targetRootFolder.setPath(targetRFPath);
        targetRootFolder.setId(2L);
        targetRootFolder.setMediaType(MediaType.FILE_SHARE);

        DocFolder newDocFolder = new DocFolder();
        newDocFolder.setPath(targetRFPath + subFolder);
        newDocFolder.setId(32188L);
        newDocFolder.setRootFolderId(targetRootFolder.getId());

        SolrFolderEntity newSolrFolder = new SolrFolderEntity();
        newSolrFolder.setPath(targetRFPath + subFolder);
        newSolrFolder.setId(32188L);
        newSolrFolder.setRootFolderId(2L);

        ClaFile file = new ClaFile(sourceRFPath + subFolder + "file.pdf", FileType.PDF);
        file.setId(5L);
        file.setRootFolderId(srcRootFolder.getId());
        file.setBaseName("file.pdf");
        DocFolder oldDocFolder = new DocFolder();
        oldDocFolder.setId(88L);
        oldDocFolder.setPath(sourceRFPath + subFolder);
        file.setDocFolderId(oldDocFolder.getId());

        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        when(fileCatService.getFileObject(file.getId())).thenReturn(file);

        when(docStoreService.getRootFolderCached(srcRootFolder.getId())).thenReturn(srcRootFolder);
        SolrFolderEntity solrFolderEntity = new SolrFolderEntity();
        solrFolderEntity.setId(1L);
        DocFolderService.DocFolderCacheValue cacheVal = new DocFolderService.DocFolderCacheValue(solrFolderEntity);
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).thenReturn(cacheVal);
        when(docFolderService.findById(1L)).thenReturn(newDocFolder);
        when(docFolderService.getFolderByPath(targetRootFolder.getId(), targetRFPath + subFolder)).thenReturn(solrFolderEntity);
        when(docFolderService.getOneById(1L)).thenReturn(Optional.of(solrFolderEntity));

        when(docFolderService.getById(oldDocFolder.getId())).thenReturn(oldDocFolder);
        when(docFolderService.getById(newDocFolder.getId())).thenReturn(newDocFolder);
        when(mailboxCache.getMailBoxes(1L)).thenReturn(Sets.newHashSet(
                "ro-admin2@docauthority.onmicrosoft.com/path/source"));
        FileEntity f = new FileEntity();
        f.setFileId(file.getId());
        keysToFileEntity.put(mediaItemId, f);

        SolrFileEntity e = new SolrFileEntity();
        e.setFileId(file.getId());
        when(fileCatService.findByFileIds(any())).thenReturn(Lists.newArrayList(e));

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        Assert.assertEquals(srcRootFolder.getId(), file.getRootFolderId());
        verify(mapBatchResultsProcessor).collectDeletedFiles(any(), any());
    }

    @Test
    public void testFileMovedIntoRootFolder() {
        String sourceRFPath = "/path/source/";
        String targetRFPath = "/path/target/";
        String mediaItemId = "1234321";
        String subFolder = "sub-folder/";

        RootFolder srcRootFolder = new RootFolder();
        srcRootFolder.setPath(sourceRFPath);
        srcRootFolder.setId(1L);
        srcRootFolder.setMediaType(MediaType.FILE_SHARE);

        RootFolder targetRootFolder = new RootFolder();
        targetRootFolder.setPath(targetRFPath);
        targetRootFolder.setId(2L);
        targetRootFolder.setMediaType(MediaType.FILE_SHARE);

        Collection<ScanResultMessage> scanResultMessages = new ArrayList<>();
        MediaChangeLogDto payload = new MediaChangeLogDto(mediaItemId, DiffType.RENAMED);
        payload.setFileName(targetRFPath + subFolder + "file.pdf");
        ScanResultMessage scanResultMessage = new ScanResultMessage(payload, targetRootFolder.getId(), false, 1L, 1L, 1L);
        scanResultMessage.setDeltaScan(true);
        scanResultMessage.setPayloadType(MessagePayloadType.SCAN_CHANGES);
        scanResultMessages.add(scanResultMessage);

        DocFolder newDocFolder = new DocFolder();
        newDocFolder.setPath(targetRFPath + subFolder);
        newDocFolder.setId(32188L);
        newDocFolder.setRootFolderId(targetRootFolder.getId());

        ClaFile file = new ClaFile(sourceRFPath + subFolder + "file.pdf", FileType.PDF);
        file.setId(5L);
        file.setRootFolderId(targetRootFolder.getId());
        file.setBaseName(payload.getFileName());

        when(kvdbRepo.getFileEntityDao()).thenReturn(new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity));
        when(docStoreService.getRootFolderCached(targetRootFolder.getId())).thenReturn(targetRootFolder);
        when(fileCatService.useExistingOrCreateRecord(any(ClaFile.class), anyLong(), anyString(), any(), any(), eq(targetRootFolder.getId()))).thenReturn(file);

        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(newDocFolder.getId());
        DocFolderService.DocFolderCacheValue cacheValue = new DocFolderService.DocFolderCacheValue(folder);
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).thenReturn(cacheValue);

        FileCatBuilder fileCatBuilder = FileCatBuilder.create();
        fileCatBuilder.setId("111!1")
                .setFileId(1L)
                .setContentId(111L)
                .setDocFolderId(111L);

        when(fileCatService.getBuilderForCreateUpdateRecord(nullable(ClaFile.class), anyLong(), anyLong(), nullable(List.class), any(ClaFilePropertiesDto.class),
                nullable(Long.class))).then(invocation -> fileCatBuilder);

        scanItemConsumer.processScanResultMessages(1L, scanResultMessages);

        Assert.assertEquals(targetRootFolder.getId(), file.getRootFolderId());
    }

    private void verifyIngestNew() {
        ArgumentCaptor<FileToAdd> fileCreateCapture = ArgumentCaptor.forClass(FileToAdd.class);
        // For some reason createTask is called twice, once from test..
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(any(), fileCreateCapture.capture());
        assertTrue(fileCreateCapture.getValue().getDoc() != null);
        SolrInputDocument doc = fileCreateCapture.getValue().getDoc();
        assertTrue(doc.getFieldValue("taskState") != null);
    }

    private void verifyIngestUpdDel() {
        ArgumentCaptor<FileToAdd> fileCreateCapture = ArgumentCaptor.forClass(FileToAdd.class);
        // For some reason createTask is called twice, once from test..
        verify(mapBatchResultsProcessor, times(2)).collectCatFiles(any(), fileCreateCapture.capture());
        assertTrue(fileCreateCapture.getAllValues().get(0).getFileIdToIngest() != null);
    }
}