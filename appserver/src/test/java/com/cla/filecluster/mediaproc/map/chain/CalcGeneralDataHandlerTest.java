package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class CalcGeneralDataHandlerTest {

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @InjectMocks
    private CalcGeneralDataHandler calcGeneralDataHandler;

    @Before
    public void init() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);
    }

    @After
    public void clean() {
        Mockito.reset(mapServiceUtils);
        Mockito.reset(mapBatchResultsProcessor);
    }

    @Test
    public void testShouldHandleScan() {
        MapResultContext mapContext = getContext();
        assertTrue(calcGeneralDataHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        msg.setPayload(new ItemDeletionPayload());
        assertTrue(!calcGeneralDataHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandleNewFileReScan() {
        MapResultContext mapContext = getContext();
        calcGeneralDataHandler.handle(mapContext);
        assertTrue(mapContext.getKvdbKey() != null);
        assertTrue(mapContext.getFileShare());
        assertTrue(mapContext.getDiffType() == DiffType.NEW);
        assertTrue(mapContext.getBelongToRF());
        assertTrue(!mapContext.getAlreadyHandled());
        assertTrue(mapContext.getNewFile());
        assertTrue(!mapContext.getFstScan());
    }

    @Test
    public void testHandleOldFileReScan() {
        MapResultContext mapContext = getContext();

        ClaFilePropertiesDto prop = (ClaFilePropertiesDto)mapContext.getScanResultMessage().getPayload();
        prop.setFileSize(55L);

        FileEntity fileEntity = new FileEntity();
        fileEntity.setSize(66L);

        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fileEntity);

        calcGeneralDataHandler.handle(mapContext);
        assertTrue(mapContext.getKvdbKey() != null);
        assertTrue(mapContext.getFileShare());
        assertTrue(mapContext.getDiffType() == DiffType.CONTENT_UPDATED);
        assertTrue(mapContext.getBelongToRF());
        assertTrue(!mapContext.getAlreadyHandled());
        assertTrue(!mapContext.getNewFile());
        assertTrue(!mapContext.getFstScan());
    }

    @Test
    public void testHandleNewFileFstScan() {
        MapResultContext mapContext = getContext();
        mapContext.getScanResultMessage().setFirstScan(true);
        calcGeneralDataHandler.handle(mapContext);
        assertTrue(mapContext.getKvdbKey() != null);
        assertTrue(mapContext.getFileShare());
        assertTrue(mapContext.getDiffType() == DiffType.NEW);
        assertTrue(mapContext.getBelongToRF());
        assertTrue(!mapContext.getAlreadyHandled());
        assertTrue(mapContext.getNewFile());
        assertTrue(mapContext.getFstScan());
    }

    private MapResultContext getContext() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFile(true);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }
}