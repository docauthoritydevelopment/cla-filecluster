package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static com.cla.filecluster.mediaproc.ingest.chain.handlers.IngestChainHandlersTestUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class OtherExtractedContentHandlerTest {

    private static final String CONTENT = "This is the content";
    private static final String HASH = "This is the hash";

    @Mock
    private NGramTokenizerHashed nGramTokenizerHashed;

    @InjectMocks
    private OtherExtractedContentHandler handler;

    @Test
    public void testShouldHandle(){
        IngestResultContext context = createSimpleContext();
        addOtherPayload(context);
        assertTrue(handler.shouldHandle(context));

        addWordPayload(context);
        Assert.assertFalse(handler.shouldHandle(context));
    }

    @Test
    public void testHandle(){
        when(nGramTokenizerHashed.getHash(CONTENT, null)).thenReturn(HASH);

        ReflectionTestUtils.setField(handler, "isTokenMatcherActive", true);
        IngestResultContext context = createSimpleContext();
        OtherFile otherFile = addOtherPayload(context);
        otherFile.setExtractedContent(CONTENT);

        assertTrue(handler.handle(context));
        assertEquals(HASH, otherFile.getHashedContent());
    }

}
