package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ExcelAnalysisMetadata;
import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.mediaproc.AnalysisMetadata;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.eventbus.EventBus;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.crawler.analyze.MaturityLevelService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Set;

import static com.cla.filecluster.mediaproc.ingest.chain.handlers.IngestChainHandlersTestUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FileRecordHandlerTest {

    private static final String ANALYSIS_GROUP = "My Analysis Group ID 1";
    private static final String USER_GROUP = "My User Group ID 2";
    private static final int MATURITY_LEVEL = 25;

    @Mock
    private MaturityLevelService maturityLevelService;

    @Mock
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Mock
    private IngestBatchResultsProcessor ingestResultCollector;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private FileRecordHandler fileRecordHandler;

    @Test
    public void testShouldHandle(){
        assertTrue(fileRecordHandler.shouldHandle(createSimpleContext()));
    }

    @SuppressWarnings("unchecked")
    @Before
    public void init(){
        when(filesDataPersistenceService.saveFileRecordInSolr(anyLong(), any(SolrFileEntity.class),
                nullable(PvAnalysisData.class), anyList(), anyLong(), nullable(java.util.List.class),
                nullable(ContentMetadata.class), anyInt(), any(IngestBatchStore.class)))
                .thenAnswer((Answer<SolrFileEntity>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (SolrFileEntity) args[1];
                });

        when(filesDataPersistenceService.saveOtherFileRecordInSolr(
                anyLong(), any(SolrFileEntity.class), nullable(ClaFilePropertiesDto.class),
                nullable(Set.class), anyList(),  nullable(ContentMetadata.class), any(IngestBatchStore.class)))
                .thenAnswer((Answer<SolrFileEntity>) invocation -> {
                    Object[] args = invocation.getArguments();
                    return (SolrFileEntity) args[1];
                });

        when(maturityLevelService.getPartMaturityLevel(any(ClaSuperCollection.class)))
                .thenReturn(MATURITY_LEVEL);

        when(maturityLevelService.workbookMaturityLevel(any(PvAnalysisData.class)))
                .thenReturn(MATURITY_LEVEL);
    }

    @Test
    public void testHandleNonIngestedExcel(){
        IngestResultContext context = createSimpleContext();
        ExcelIngestResult excelIngestResult = addExcelPayload(context);
        excelIngestResult.setMetadata(null);
        createAndAddFileEntity(context, 1L);
        fileRecordHandler.handle(context);
        assertEquals(ClaFileState.SCANNED_AND_STOPPED, ClaFileState.valueOf(context.getFile().getState()));

        context = createSimpleContext();
        excelIngestResult = addExcelPayload(context);
        ExcelWorkbookMetaData metaData = new ExcelWorkbookMetaData();
        metaData.setNumOfSheets(-1);
        excelIngestResult.setMetadata(metaData);
        createAndAddFileEntity(context, 1L);
        fileRecordHandler.handle(context);
        assertEquals(ClaFileState.SCANNED_AND_STOPPED, ClaFileState.valueOf(context.getFile().getState()));
    }

    @Test
    public void testIngestWord(){
        IngestResultContext context = createSimpleContext();
        WordIngestResult wordIngestResult = addWordPayload(context);
        context.setOriginalState(ClaFileState.SCANNED);
        ContentMetadata contentMetadata = new ContentMetadata();
        contentMetadata.setId(1L);
        context.setContentMetadata(contentMetadata);

        context.getWordResultMessage().getPayload().setLongHistograms(new ClaSuperCollection<>());
        wordIngestResult.setMetadata(new WordfileMetaData());
        wordIngestResult.setLongHistograms(new ClaSuperCollection<>());

        fileRecordHandler.handle(context);

        assertEquals(MATURITY_LEVEL, context.getMaturityLevel());

        SolrFileEntity file = context.getFile();
        assertEquals(ClaFileState.INGESTED, ClaFileState.valueOf(file.getState()));
    }

    @Test
    public void testIngestExcel(){
        IngestResultContext context = createSimpleContext();
        ExcelIngestResult excelIngestResult = addExcelPayload(context);
        context.setOriginalState(ClaFileState.SCANNED);
        ContentMetadata contentMetadata = new ContentMetadata();
        contentMetadata.setId(1L);
        context.setContentMetadata(contentMetadata);

        fileRecordHandler.handle(context);
        assertEquals(0, context.getMaturityLevel());

        PvAnalysisData data = new PvAnalysisData(1L, new ClaSuperCollection<>(), new AnalysisMetadata());
        context.setPvAnalysisData(data);
        ExcelWorkbookMetaData metaData = new ExcelWorkbookMetaData();
        metaData.setNumOfSheets(3);
        excelIngestResult.setMetadata(metaData);
        excelIngestResult.setFileCollections(new FileCollections());
        excelIngestResult.setSheetsMetadata(new ExcelAnalysisMetadata());
        fileRecordHandler.handle(context);
        assertEquals(MATURITY_LEVEL, context.getMaturityLevel());

        SolrFileEntity file = context.getFile();
        assertEquals(ClaFileState.INGESTED, ClaFileState.valueOf(file.getState()));
    }

    @Test
    public void testIngestOther(){
        IngestResultContext context = createSimpleContext();
        addOtherPayload(context);
        context.setOriginalState(ClaFileState.SCANNED);
        ContentMetadata contentMetadata = new ContentMetadata();
        contentMetadata.setId(1L);
        context.setContentMetadata(contentMetadata);

        fileRecordHandler.handle(context);

        //noinspection unchecked
        verify(filesDataPersistenceService, times(1)).saveOtherFileRecordInSolr(
                anyLong(), any(SolrFileEntity.class), nullable(ClaFilePropertiesDto.class),
                nullable(Set.class), anyList(), eq(contentMetadata), any(IngestBatchStore.class));

        SolrFileEntity file = context.getFile();
        assertEquals(ClaFileState.INGESTED, ClaFileState.valueOf(file.getState()));
    }

    @Test
    public void testReingestWord(){
        IngestResultContext context = createSimpleContext();
        addWordPayload(context);
        context.setOriginalState(ClaFileState.INGESTED);
        ContentMetadata contentMetadata = new ContentMetadata();
        contentMetadata.setState(ClaFileState.ANALYSED);
        context.setContentMetadata(contentMetadata);

        contentMetadata.setAnalysisGroupId("My Analysis Group ID 1");
        contentMetadata.setUserGroupId("My User Group ID 2");

        fileRecordHandler.handle(context);

        SolrFileEntity file = context.getFile();
        assertEquals(ClaFileState.ANALYSED.name(), file.getState());
        assertEquals(ANALYSIS_GROUP, file.getAnalysisGroupId());
        assertEquals(USER_GROUP, file.getUserGroupId());

        verify(ingestResultCollector, times(1)).collectMarkGroupAsDirty(
                anyLong(), eq(ANALYSIS_GROUP));

        verify(ingestResultCollector, times(1)).collectMarkGroupAsDirty(
                anyLong(), eq(USER_GROUP));
    }


}
