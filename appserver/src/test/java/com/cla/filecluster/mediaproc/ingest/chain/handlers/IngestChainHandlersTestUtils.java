package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.messages.*;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

class IngestChainHandlersTestUtils {

    static ClaFilePropertiesDto createFileProperties(String name, long size){
        return new ClaFilePropertiesDto(name, size);
    }

    static void createAndAddFileEntity(IngestResultContext context, long fileId){
        SolrFileEntity file = new SolrFileEntity();
        file.setFileId(fileId);
        ReflectionTestUtils.invokeMethod(context.getIngestBatchStore(), "addFileEntities", Lists.newArrayList(file));
    }

    static IngestResultContext createSimpleContext(){

        IngestResultMessage resultMessage = new IngestResultMessage();
        resultMessage.setClaFileId(1L);
        resultMessage.setTaskId(1L);
        resultMessage.setRunContext(1L);
        resultMessage.setJobId(1L);
        resultMessage.setContentMetadataId(1L);

        ClaFilePropertiesDto fileProps = createFileProperties("MyFile", 100L);
        resultMessage.setFileProperties(fileProps);

        IngestResultContext context = IngestResultContext.create(resultMessage);
        IngestBatchStore batchStore = new IngestBatchStore();
        ReflectionTestUtils.setField(context, "ingestBatchStore", batchStore);

        createAndAddFileEntity(context,1L);

        return context;
    }

    @SuppressWarnings("unchecked")
    static WordIngestResult addWordPayload(IngestResultContext context) {
        WordIngestResult payload = new WordIngestResult();
        context.getResultMessage().setPayload(payload);
        return payload;
    }

    @SuppressWarnings("unchecked")
    static ExcelIngestResult addExcelPayload(IngestResultContext context) {
        ExcelIngestResult payload = new ExcelIngestResult();
        context.getResultMessage().setPayload(payload);
        return payload;
    }

    @SuppressWarnings("unchecked")
    static OtherFile addOtherPayload(IngestResultContext context) {
        OtherFile payload = new OtherFile();
        context.getResultMessage().setPayload(payload);
        return payload;
    }

    static ExcelSheetIngestResult addSheet(ExcelIngestResult excelIngestResult){
        List<ExcelSheetIngestResult> sheets = excelIngestResult.getSheets();
        if (sheets == null) {
            sheets = Lists.newArrayList();
            ReflectionTestUtils.setField(excelIngestResult, "sheets", sheets);
        }
        ExcelSheetIngestResult sheet = new ExcelSheetIngestResult();
        sheets.add(sheet);
        return sheet;
    }

    static ExcelParseParams getExcelParseParams(){
        return new ExcelParseParams(true, 1,
                Sets.newHashSet(), true, true);
    }

    static IngestError createError(ProcessingErrorType type){
        IngestError error = IngestError.create();
        error.setErrorType(type);
        return error;
    }

}
