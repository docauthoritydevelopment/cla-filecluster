package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.file.ScanProgressPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class SpecialResponseHandlerTest {

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private SpecialResponseHandler specialResponseHandler;

    @Test
    public void testHandleDirList() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DIR_LISTING);
        assertTrue(!specialResponseHandler.handle(mapContext));
    }

    @Test
    public void testHandleProgress() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_PROGRESS);
        ScanProgressPayload payload = new ScanProgressPayload(15);
        msg.setPayload(payload);
        assertTrue(!specialResponseHandler.handle(mapContext));

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(1)).broadcast(topicCaptor.capture(), eventCaptor.capture());
        assertTrue(eventCaptor.getValue().getEventType().equals(EventType.JOB_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));
    }

    @Test
    public void testHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        assertTrue(specialResponseHandler.handle(mapContext));
    }

    @Test
    public void testHandleScan() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN);
        assertTrue(specialResponseHandler.handle(mapContext));
    }
}