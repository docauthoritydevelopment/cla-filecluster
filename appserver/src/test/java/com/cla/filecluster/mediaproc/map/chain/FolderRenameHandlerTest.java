package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.*;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.function.Supplier;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.*;

/**
 * Test for link FolderRenameHandler, verify it handles only fileshare rename folder and all remote scan rename besides
 * exchange
 */
@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FolderRenameHandlerTest {

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private MapRenameHandler mapRenameHandler;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private FolderRenameHandler folderRenameHandler;

    @Before
    public void init() {
        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(7L);
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).
                thenReturn(new DocFolderService.DocFolderCacheValue(folder));

    }

    @After
    public void clean() {
        Mockito.reset(mapServiceUtils);
        Mockito.reset(eventBus);
        Mockito.reset(mapRenameHandler);
        Mockito.reset(docFolderService);
    }


    @Test
    public void testHandleInvalidPayload() {
        // Prepare RF
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);

        // Prepare irrelevant message for the handler
        ScanResultMessage msg = createMsg(null, MessagePayloadType.INGEST_PDF);
        MapResultContext mapContext = getContext(msg); // should continue to next link
        boolean res = folderRenameHandler.handle(mapContext);
        assertTrue(res);
    }

    @Test
    public void testHandleFolderReScanRenameFileShare() {
        // Prepare RF
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);

        // Prepare cla file share payload
        ClaFilePropertiesDto prop = getProp(true, ClaFilePropertiesDto::create);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.RENAMED);
        mapContext.setNewFile(false);
        FileEntity fe = getFileEntity(true);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderRenameHandler.handle(mapContext);
        assertTrue(!res);
        verify(docFolderService, times(1)).createDocFolderIfNeededWithCache(eq(1L), anyString());
        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));
    }


    @Test
    public void testHandleFolderReScanRenameRemoteMedia() {
        // Prepare RF
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.ONE_DRIVE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);

        // Prepare remote media OD change event
        MediaChangeLogDto prop = getProp(true, MediaChangeLogDto::create);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN_CHANGES);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.RENAMED);
        mapContext.setNewFile(false);
        FileEntity fe = getFileEntity(true);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderRenameHandler.handle(mapContext);
        assertTrue(!res);
        verify(mapRenameHandler, times(1)).handleFolderMove(eq(prop), anyLong(), eq(1L), anyLong());
        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));
    }

    @Test
    public void testHandleFolderReScanRenameRemoteMediaExchange() {
        // Prepare RF
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.EXCHANGE_365);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);

        // Prepare remote media exchange 365 change event
        ClaFilePropertiesDto prop = getProp(true, MediaChangeLogDto::create);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN_CHANGES);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.RENAMED);
        mapContext.setNewFile(false);
        FileEntity fe = getFileEntity(true);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderRenameHandler.handle(mapContext);
        assertTrue(res);
        verify(docFolderService, times(0)).createDocFolderIfNeededWithCache(anyLong(), anyString());
        verify(mapRenameHandler, times(0)).handleFolderMove(any(), anyLong(), anyLong(), anyLong());
        verify(eventBus, times(0)).broadcast(anyString(), any(Event.class));
    }


    private FileEntity getFileEntity(boolean isFolder) {
        FileEntity fe = new FileEntity();

        if (isFolder) {
            fe.setFolderId(5L);
            fe.setFolder(true);
            fe.setFile(false);
        } else {
            fe.setFileId(5L);
        }
        return fe;
    }

    private <T extends ClaFilePropertiesDto> T getProp(boolean folder, Supplier<T> supplier) {
        T prop = supplier.get();
        if (folder) {
            prop.setFolder(true);
            prop.setFile(false);
        }
        prop.setFileName("fghfghf");
        return prop;
    }

    private MapResultContext getContext(ScanResultMessage msg) {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setBatchIdentifierNumber(7L);
        mapContext.setScanResultMessage(msg);
        mapContext.setAlreadyHandled(false);
        mapContext.setKvdbKey("ergrthj657u");
        mapContext.setBelongToRF(true);
        return mapContext;
    }

    private ScanResultMessage createMsg(PhaseMessagePayload payload, MessagePayloadType type) {
        ScanResultMessage msg = new ScanResultMessage();
        msg.setRootFolderId(1L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setJobId(6L);
        msg.setPayloadType(type);
        msg.setPayload(payload);
        return msg;
    }
}