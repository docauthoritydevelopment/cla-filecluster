package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.ExcelSheetIngestResult;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import static com.cla.filecluster.mediaproc.ingest.chain.handlers.IngestChainHandlersTestUtils.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class PatternSearchHandlerTest {

    private static final String  PATTERN1 = "MyPattern1";
    private static final String  PATTERN2 = "MyPattern2";
    private static final String  PATTERN3 = "MyPattern3";
    private static final String  ORIGINAL_CONTENT = "My Original Content";

    @Mock
    protected ClaFileSearchPatternService searchPatternService;

    @InjectMocks
    private PatternSearchHandler patternSearchHandler;

    @Before
    public void init(){
        Set<SearchPatternCountDto> searchPatternsCounting = Sets.newHashSet();
        SearchPatternCountDto dto = new SearchPatternCountDto(1, PATTERN1,
                10, Lists.newArrayList());
        searchPatternsCounting.add(dto);
        when(searchPatternService.getSearchPatternsCounting(ORIGINAL_CONTENT))
                .thenReturn(searchPatternsCounting);
    }

    @Test
    public void testShouldHandle(){
        IngestResultContext context = createSimpleContext();
        ReflectionTestUtils.setField(patternSearchHandler, "patternSearchActive", true);
        assertTrue(patternSearchHandler.shouldHandle(context));
        ReflectionTestUtils.setField(patternSearchHandler, "patternSearchActive", false);
        Assert.assertFalse(patternSearchHandler.shouldHandle(context));
    }

    @Test
    public void testHandleWord(){
        IngestResultContext context = createSimpleContext();
        WordIngestResult wordIngestResult = addWordPayload(context);
        Set<SearchPatternCountDto> searchPatternsCounting = Sets.newHashSet();
        SearchPatternCountDto dto = new SearchPatternCountDto(1, PATTERN2,
                10, Lists.newArrayList());
        searchPatternsCounting.add(dto);
        wordIngestResult.setSearchPatternsCounting(searchPatternsCounting);

        assertTrue(patternSearchHandler.handle(context));

        Set<SearchPatternCountDto> fromContext = context.getSearchPatternsCounting();
        assertNotNull(fromContext);
        assertTrue(fromContext.size() > 0);
        SearchPatternCountDto fromContextDto = fromContext.stream().findFirst().get();
        assertEquals(PATTERN2, fromContextDto.getPatternName());

        wordIngestResult.setOriginalContent(ORIGINAL_CONTENT);
        wordIngestResult.setSearchPatternsCounting(null);
        patternSearchHandler.handle(context);
        fromContext = context.getSearchPatternsCounting();
        assertNotNull(fromContext);
        assertTrue(fromContext.size() > 0);
        fromContextDto = fromContext.stream().findFirst().get();
        assertEquals(PATTERN1, fromContextDto.getPatternName());
    }

    @Test
    public void testHandleExcel(){
        IngestResultContext context = createSimpleContext();
        ExcelIngestResult excelIngestResult = addExcelPayload(context);

        Set<SearchPatternCountDto> searchPatternsCounting2 = Sets.newHashSet();
        SearchPatternCountDto dto2 = new SearchPatternCountDto(1, PATTERN2,
                10, Lists.newArrayList());
        searchPatternsCounting2.add(dto2);

        Set<SearchPatternCountDto> searchPatternsCounting3 = Sets.newHashSet();
        SearchPatternCountDto dto3 = new SearchPatternCountDto(1, PATTERN3,
                10, Lists.newArrayList());
        searchPatternsCounting3.add(dto3);

        ExcelSheetIngestResult excelSheet2 = addSheet(excelIngestResult);
        excelSheet2.setSearchPatternsCounting(searchPatternsCounting2);

        ExcelSheetIngestResult excelSheet3 = addSheet(excelIngestResult);
        excelSheet3.setSearchPatternsCounting(searchPatternsCounting3);

        assertTrue(patternSearchHandler.handle(context));

        Set<SearchPatternCountDto> fromContext = context.getSearchPatternsCounting();
        assertNotNull(fromContext);
        assertEquals(2, fromContext.size());

        AtomicInteger found = new AtomicInteger(0);
        fromContext.forEach(dto -> {
            if (PATTERN2.equals(dto.getPatternName()) || PATTERN3.equals(dto.getPatternName())){
                found.incrementAndGet();
            }
        });
        assertEquals(2, found.get());
    }

    @Test
    public void testHandleOther(){
        IngestResultContext context = createSimpleContext();
        OtherFile otherIngestResult = addOtherPayload(context);
        Set<SearchPatternCountDto> searchPatternsCounting = Sets.newHashSet();
        SearchPatternCountDto dto = new SearchPatternCountDto(1, "MyPattern2",
                10, Lists.newArrayList());
        searchPatternsCounting.add(dto);
        otherIngestResult.setSearchPatternsCounting(searchPatternsCounting);
        assertTrue(patternSearchHandler.handle(context));
        Set<SearchPatternCountDto> fromContext = context.getSearchPatternsCounting();
        assertNotNull(fromContext);
        assertTrue(fromContext.size() > 0);
        SearchPatternCountDto fromContextDto = fromContext.stream().findFirst().get();
        assertEquals("MyPattern2", fromContextDto.getPatternName());
    }
}
