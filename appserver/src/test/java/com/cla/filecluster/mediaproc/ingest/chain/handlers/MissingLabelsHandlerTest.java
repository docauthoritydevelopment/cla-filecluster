package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.label.LabelingService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MissingLabelsHandlerTest {

    @Mock
    private LabelingService labelingService;

    @InjectMocks
    private MissingLabelsHandler missingLabelsHandler;

    private IngestResultContext context;

    @Before
    public void init(){
        context = IngestChainHandlersTestUtils.createSimpleContext();
    }

    @Test
    public void testShouldHandle(){
        Assert.assertTrue(missingLabelsHandler.shouldHandle(context));
    }

    @Test
    public void testHandle(){
        boolean continueHandling = missingLabelsHandler.handle(context);
        Assert.assertTrue(continueHandling);

        verify(labelingService, times(1)).enrichEntitiesForIngest(
                any(SolrFileEntity.class), any(ClaFilePropertiesDto.class));

    }

}
