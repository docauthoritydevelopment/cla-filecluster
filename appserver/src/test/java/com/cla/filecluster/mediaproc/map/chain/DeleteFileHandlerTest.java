package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.filecluster.mediaproc.map.MapDeleteHandler;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class DeleteFileHandlerTest {

    @Mock
    private MapDeleteHandler mapDeleteHandler;

    @InjectMocks
    private DeleteFileHandler deleteFileHandler;

    @Test
    public void testShouldHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        assertTrue(deleteFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleScan() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN);
        assertTrue(!deleteFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setBatchIdentifierNumber(56L);
        ScanResultMessage msg = new ScanResultMessage();
        msg.setRunContext(1L);
        msg.setTaskId(1L);
        msg.setRootFolderId(1L);
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        ItemDeletionPayload payload = new ItemDeletionPayload();
        msg.setPayload(payload);
        payload.setFilename("fileToDel");

        deleteFileHandler.handle(mapContext);

        verify(mapDeleteHandler, times(1)).handleDeletedFiles(1L, 1L,
                1L, 56L, "fileToDel");
    }
}