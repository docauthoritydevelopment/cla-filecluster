package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapErrorHandlerTest {

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private MapErrorHandler mapErrorHandler;

    @Test
    public void testHandle() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setJobId(6L);

        boolean res = mapErrorHandler.handle(mapContext);

        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));

        assertEquals(false, res);
    }
}