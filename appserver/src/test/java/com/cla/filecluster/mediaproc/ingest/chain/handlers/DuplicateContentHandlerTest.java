package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class DuplicateContentHandlerTest {

    @Mock
    private FilesDataPersistenceService filesDataPersistenceService;

    @InjectMocks
    private DuplicateContentHandler duplicateContentHandler;

    @Test
    public void testShouldHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        assertFalse(duplicateContentHandler.shouldHandle(context));
        context.getResultMessage().setDuplicateContentFound(true);
        Assert.assertTrue(duplicateContentHandler.shouldHandle(context));
    }

    @Test(expected = RuntimeException.class)
    public void testHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        context.getResultMessage().setDuplicateContentFound(true);
        assertFalse(duplicateContentHandler.handle(context));
        verify(filesDataPersistenceService, times(1)).saveDuplicateContentFile(
                any(IngestResultMessage.class), any(IngestBatchStore.class));

        context.getResultMessage().setContentMetadataId(null);
        duplicateContentHandler.handle(context);
    }
}
