package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.cla.filecluster.mediaproc.ingest.chain.handlers.IngestChainHandlersTestUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class ContentMetadataHandlerTest {

    @Mock
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Mock
    private FileGroupService fileGroupService;

    @InjectMocks
    private ContentMetadataHandler contentMetadataHandler;

    @Test
    public void testShouldHandle(){
        assertTrue(contentMetadataHandler.shouldHandle(createSimpleContext()));
    }

    @Test
    public void testWordHandle(){

        ContentMetadata contentMetadata = new ContentMetadata();

        when(filesDataPersistenceService.resolveContentMetadata(anyLong(), any(SolrFileEntity.class),
                any(ClaFilePropertiesDto.class), any(IngestBatchStore.class),
                eq(null), eq(null)))
                .thenReturn(contentMetadata);

        when(filesDataPersistenceService.updateContentMetadata(eq(contentMetadata), any(SolrFileEntity.class),
                any(ClaFilePropertiesDto.class))).thenReturn(contentMetadata);

        contentMetadata.setState(ClaFileState.SCANNED);
        IngestResultContext context = createSimpleContext();

        WordIngestResult wordIngestResult = addWordPayload(context);
        WordfileMetaData metadata = new WordfileMetaData();
        metadata.setFsFileSize(500L);
        metadata.setNumChars(1976);
        metadata.setNumPages(3);
        metadata.setNumLines(40);
        metadata.setNumParagraphs(5);
        metadata.setNumWords(200);
        wordIngestResult.setMetadata(metadata);

        assertTrue(contentMetadataHandler.handle(context));

        assertEquals(ClaFileState.SCANNED, context.getOriginalState());
        assertEquals(ClaFileState.INGESTED, contentMetadata.getState());
        verify(filesDataPersistenceService, times(1)).updateContentMetadata(
                eq(contentMetadata), eq(context.getFile()), any(ClaFilePropertiesDto.class));

        verify(filesDataPersistenceService, times(1)).collectMetadataToBatch(
                anyLong(), eq(contentMetadata), any(ClaFilePropertiesDto.class), anyBoolean(), anyLong());

        assertEquals(500, contentMetadata.getFsFileSize().longValue());
        assertEquals(1976, contentMetadata.getItemsCountL1().longValue());
        assertEquals(200, contentMetadata.getItemsCountL2().longValue());
        assertEquals(3, contentMetadata.getItemsCountL3().longValue());
    }

    @Test
    public void testExcelHandle(){

        ContentMetadata contentMetadata = new ContentMetadata();

        when(filesDataPersistenceService.resolveContentMetadata(anyLong(), any(SolrFileEntity.class),
                any(ClaFilePropertiesDto.class), any(IngestBatchStore.class),
                eq(null), eq(null)))
                .thenReturn(contentMetadata);

        when(filesDataPersistenceService.updateContentMetadata(eq(contentMetadata), any(SolrFileEntity.class),
                any(ClaFilePropertiesDto.class))).thenReturn(contentMetadata);

        contentMetadata.setState(ClaFileState.SCANNED);
        IngestResultContext context = createSimpleContext();

        ExcelIngestResult excelIngestResult = addExcelPayload(context);
        ExcelWorkbookMetaData metadata = new ExcelWorkbookMetaData();
        metadata.setFsFileSize(500L);
        metadata.setNumOfSheets(3);
        metadata.setNumOfCells(3000);
        metadata.setNumOfBlanks(1000);
        metadata.setNumOfFormulas(25);
        metadata.setNumOfErrors(5);
        excelIngestResult.setMetadata(metadata);

        assertTrue(contentMetadataHandler.handle(context));

        assertEquals(ClaFileState.SCANNED, context.getOriginalState());
        assertEquals(ClaFileState.INGESTED, contentMetadata.getState());
        verify(filesDataPersistenceService, times(1)).updateContentMetadata(
                eq(contentMetadata), eq(context.getFile()), any(ClaFilePropertiesDto.class));

        assertEquals(500, contentMetadata.getFsFileSize().longValue());

        assertEquals(3000, contentMetadata.getItemsCountL1().longValue());
        assertEquals(25, contentMetadata.getItemsCountL2().longValue());
        assertEquals(3, contentMetadata.getItemsCountL3().longValue());
    }

    @Test
    public void testOtherHandle(){

        ContentMetadata contentMetadata = new ContentMetadata();

        when(filesDataPersistenceService.resolveContentMetadata(anyLong(), any(SolrFileEntity.class),
                any(ClaFilePropertiesDto.class), any(IngestBatchStore.class),
                eq(null), eq(null)))
                .thenReturn(contentMetadata);

        when(filesDataPersistenceService.updateContentMetadata(eq(contentMetadata), any(SolrFileEntity.class),
                any(ClaFilePropertiesDto.class))).thenReturn(contentMetadata);

        contentMetadata.setState(ClaFileState.SCANNED);
        IngestResultContext context = createSimpleContext();

        OtherFile otherFile = addOtherPayload(context);
        otherFile.setTextLength(1000);

        assertTrue(contentMetadataHandler.handle(context));

        assertEquals(ClaFileState.SCANNED, context.getOriginalState());
        assertEquals(ClaFileState.INGESTED, contentMetadata.getState());
        verify(filesDataPersistenceService, times(1)).updateContentMetadata(
                eq(contentMetadata), eq(context.getFile()), any(ClaFilePropertiesDto.class));

        assertEquals(1000, contentMetadata.getItemsCountL2().longValue());
    }
}
