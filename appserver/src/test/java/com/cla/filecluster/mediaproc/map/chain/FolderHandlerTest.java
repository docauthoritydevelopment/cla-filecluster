package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.domain.dto.messages.ProcessingError;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.*;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FolderHandlerTest {

    @Mock
    private RootFolderService rootFolderService;

    @Mock
    private MapDeleteHandler mapDeleteHandler;

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private MapRenameHandler mapRenameHandler;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private MapAddHandler mapAddHandler;

    @Mock
    private ScanErrorsService scanErrorsService;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private FolderHandler folderHandler;

    @Before
    public void init() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);

        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(7L);
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).
                thenReturn(new DocFolderService.DocFolderCacheValue(folder));

        Pair<Long, SolrInputDocument> newFolderData = Pair.of(7L, new SolrInputDocument());
        when(mapAddHandler.handleFolderAddItem(anyString(), anyLong(), anyLong(), any(ClaFilePropertiesDto.class)))
                .thenReturn(newFolderData);
    }

    @After
    public void clean() {
        Mockito.reset(mapServiceUtils);
        Mockito.reset(mapBatchResultsProcessor);
        Mockito.reset(eventBus);
        Mockito.reset(mapAddHandler);
        Mockito.reset(mapDeleteHandler);
        Mockito.reset(mapRenameHandler);
        Mockito.reset(fileCatService);
        Mockito.reset(docFolderService);
        Mockito.reset(scanErrorsService);
    }

    @Test
    public void testHandleDeleteFile() {
        FileEntity fe = getFileEntity(false);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);
        ItemDeletionPayload payload = new ItemDeletionPayload();
        payload.setFilename("fhfghfgh");
        ScanResultMessage msg = createMsg(payload, MessagePayloadType.SCAN_DELETION);
        MapResultContext mapContext = getContext(msg);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
        assertTrue(mapContext.getFileId().equals(5L));
        verify(mapDeleteHandler, times(0)).handleDeletedFiles(anyLong(), anyLong(), anyLong(),
                anyLong(), anyString());
    }

    @Test
    public void testHandleDeleteFolder() {
        FileEntity fe = getFileEntity(true);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);
        ItemDeletionPayload payload = new ItemDeletionPayload();
        payload.setFilename("fhfghfgh");
        ScanResultMessage msg = createMsg(payload, MessagePayloadType.SCAN_DELETION);
        MapResultContext mapContext = getContext(msg);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(!res);
        verify(mapDeleteHandler, times(1)).handleDeletedFiles(anyLong(), anyLong(), anyLong(),
                anyLong(), anyString());
    }

    @Test
    public void testHandleInvalidPayload() {
        ScanResultMessage msg = createMsg(null, MessagePayloadType.INGEST_PDF);
        MapResultContext mapContext = getContext(msg);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(!res);
    }

    @Test
    public void testHandleFileFstScan() {
        ClaFilePropertiesDto prop = getProp(false);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        msg.setFirstScan(true);
        MapResultContext mapContext = getContext(msg);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
        verify(mapAddHandler, times(0)).handleFolderAddItem(anyString(), anyLong(), anyLong(), any(ClaFilePropertiesDto.class));
        verify(docFolderService, times(1)).createDocFolderIfNeededWithCache(anyLong(), anyString());
        verify(mapBatchResultsProcessor, times(0)).collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testHandleFolderFstScan() {
        ClaFilePropertiesDto prop = getProp(true);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        msg.setFirstScan(true);
        MapResultContext mapContext = getContext(msg);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(!res);
        verify(mapAddHandler, times(1)).handleFolderAddItem(anyString(), anyLong(), anyLong(), any(ClaFilePropertiesDto.class));
        verify(docFolderService, times(0)).createDocFolderIfNeededWithCache(anyLong(), anyString());
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testHandleFolderReScanAcl() {
        ClaFilePropertiesDto prop = getProp(true);
        prop.setAclSignature("hjghjhgjghj");
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.ACL_UPDATED);
        FileEntity fe = getFileEntity(true);
        fe.setAclSignature("dfgdfgfdgdfg");
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderHandler.handle(mapContext);
        assertTrue(!res);
        verify(docFolderService, times(1)).updateFolderAcls(anyLong(), any(ClaFilePropertiesDto.class), anyBoolean(), anyLong());
    }

    @Test
    public void testHandleFileReScanAcl() {
        ClaFilePropertiesDto prop = getProp(false);
        prop.setAclSignature("hjghjhgjghj");
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.ACL_UPDATED);
        FileEntity fe = getFileEntity(false);
        fe.setAclSignature("dfgdfgfdgdfg");
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
    }

    @Test
    public void testHandleFolderReScanUndelete() {
        ClaFilePropertiesDto prop = getProp(true);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.UNDELETED);
        FileEntity fe = getFileEntity(true);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderHandler.handle(mapContext);
        assertTrue(!res);
        verify(docFolderService, times(1)).updateFolderAcls(anyLong(), any(ClaFilePropertiesDto.class), anyBoolean(), anyLong());
        verify(docFolderService, times(1)).markDocFolderAsNotDeleted(anyLong());
    }

    @Test
    public void testHandleFolderReScanRename() {
        ClaFilePropertiesDto prop = getProp(true);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.RENAMED);
        mapContext.setNewFile(false);
        FileEntity fe = getFileEntity(true);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
        verify(mapRenameHandler, times(0)).handleFileOrDeletedFolderRename(anyLong(), any(RootFolder.class), anyLong(),
                anyLong(), any(ClaFilePropertiesDto.class), any(FileEntity.class), anyBoolean());
    }

    @Test
    public void testHandleFileReScanRename() {
        ClaFilePropertiesDto prop = getProp(false);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.RENAMED);
        mapContext.setNewFile(false);
        FileEntity fe = getFileEntity(false);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
        verify(fileCatService, times(1)).handleFolderForFileRename(
                any(RootFolder.class), any(ClaFilePropertiesDto.class), anyLong());
        assertNotNull(mapContext.getFileId());
    }

    @Test
    public void testHandleFileReScanNew() {
        ClaFilePropertiesDto prop = getProp(false);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.NEW);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
        verify(mapAddHandler, times(0)).handleFolderAddItem(anyString(), anyLong(), anyLong(), any(ClaFilePropertiesDto.class));
        verify(docFolderService, times(1)).createDocFolderIfNeededWithCache(anyLong(), anyString());
        verify(mapBatchResultsProcessor, times(0)).collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testHandleFileReScanContentUpd() {
        ClaFilePropertiesDto prop = getProp(false);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        MapResultContext mapContext = getContext(msg);
        mapContext.setDiffType(DiffType.CONTENT_UPDATED);
        prop.setModTimeMilli(666666L);
        FileEntity fe = getFileEntity(false);
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fe);

        boolean res = folderHandler.handle(mapContext);
        assertTrue(res);
        assertTrue(fe.getLastModified().equals(666666L));
    }

    @Test
    public void testHandleFstScanErr() {
        ClaFilePropertiesDto prop = getProp(true);
        ScanErrorDto err = ScanErrorDto
                .create(ProcessingError.ErrorSeverity.CRITICAL)
                .setRootFolderId(1L)
                .setPath("reretge");
        err.setText("err").setExceptionText("err");
        prop.addError(err);
        ScanResultMessage msg = createMsg(prop, MessagePayloadType.SCAN);
        msg.setFirstScan(true);
        MapResultContext mapContext = getContext(msg);
        boolean res = folderHandler.handle(mapContext);
        assertTrue(!res);

        verify(scanErrorsService, times(1)).addScanError(anyString(), anyString(),
                isNull(), anyString(), anyLong(), anyLong(), any(MediaType.class));
        verify(eventBus, times(2)).broadcast(anyString(), any(Event.class));
    }

    private FileEntity getFileEntity(boolean isFolder) {
        FileEntity fe = new FileEntity();

        if (isFolder) {
            fe.setFolderId(5L);
            fe.setFolder(true);
            fe.setFile(false);
        } else {
            fe.setFileId(5L);
        }
        return fe;
    }

    private ClaFilePropertiesDto getProp(boolean isFolder) {
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        if (isFolder) {
            prop.setFolder(true);
            prop.setFile(false);
        }
        prop.setFileName("fghfghf");
        return prop;
    }

    private MapResultContext getContext(ScanResultMessage msg) {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setBatchIdentifierNumber(7L);
        mapContext.setScanResultMessage(msg);
        mapContext.setAlreadyHandled(false);
        mapContext.setKvdbKey("ergrthj657u");
        mapContext.setBelongToRF(true);
        return mapContext;
    }

    private ScanResultMessage createMsg(PhaseMessagePayload payload, MessagePayloadType type) {
        ScanResultMessage msg = new ScanResultMessage();
        msg.setRootFolderId(1L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setJobId(6L);
        msg.setPayloadType(type);
        msg.setPayload(payload);
        return msg;
    }
}