package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class PdfFileTypeHandlerTest {

    @InjectMocks
    private PdfFileTypeHandler pdfFileTypeHandler;

    @Test
    public void testShouldHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        SolrFileEntity file = context.getFile();

        file.setType(FileType.WORD.toInt());
        Assert.assertFalse(pdfFileTypeHandler.shouldHandle(context));

        file.setType(FileType.PDF.toInt());
        Assert.assertTrue(pdfFileTypeHandler.shouldHandle(context));

        file.setType(FileType.SCANNED_PDF.toInt());
        Assert.assertTrue(pdfFileTypeHandler.shouldHandle(context));
    }

    @Test
    public void testHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        IngestChainHandlersTestUtils.addWordPayload(context);
        SolrFileEntity file = context.getFile();
        file.setType(FileType.PDF.toInt());
        IngestResultMessage<WordIngestResult> wordResultMessage = context.getWordResultMessage();
        WordIngestResult payload = wordResultMessage.getPayload();

        payload.setScanned(false);
        Assert.assertTrue(pdfFileTypeHandler.handle(context));
        Assert.assertEquals(FileType.PDF.toInt(), file.getType());

        payload.setScanned(true);
        Assert.assertTrue(pdfFileTypeHandler.handle(context));
        Assert.assertEquals(FileType.SCANNED_PDF.toInt(), file.getType());

        Assert.assertTrue(pdfFileTypeHandler.handle(context));
        Assert.assertEquals(FileType.SCANNED_PDF.toInt(), file.getType());

        payload.setScanned(false);
        Assert.assertTrue(pdfFileTypeHandler.handle(context));
        Assert.assertEquals(FileType.PDF.toInt(), file.getType());
    }


}
