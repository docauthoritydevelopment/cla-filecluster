package com.cla.filecluster.mediaproc.map;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapServiceUtilsTest {

    @Mock
    private KeyValueDatabaseRepository kvdbRepo;

    @Mock
    private JobManagerService jobManagerService;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Mock
    private FileCrawlerExecutionDetailsService runService;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private MapServiceUtils mapServiceUtils;

    @After
    public void clean() {
        Mockito.reset(mapBatchResultsProcessor);
        Mockito.reset(eventBus);
        Mockito.reset(docStoreService);
    }

    @Test
    public void testCreateIngestTaskInvalidType() {
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> false);
        mapServiceUtils.createIngestTask(new SolrInputDocument(), "path", 1L, FileType.PDF,
                3L, 3L, 57L, 67L, new IngestTaskJsonProperties());
        verify(jobManagerService, times(0)).getJobIdCached(anyLong(), any(JobType.class));
    }

    @Test
    public void testCreateIngestTaskTypeNoContent() {
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> true);
        mapServiceUtils.createIngestTask(new SolrInputDocument(), "path", 1L, FileType.PACKAGE,
                3L, 3L, 57L, 67L, new IngestTaskJsonProperties());
        verify(jobManagerService, times(0)).getJobIdCached(anyLong(), any(JobType.class));
    }

    @Test
    public void testCreateIngestTaskNoIngestJob() {
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> true);
        when(jobManagerService.getJobIdCached(anyLong(), any(JobType.class))).thenReturn(null);
        CrawlRun run = new CrawlRun();
        when(runService.getCrawlRunFromCache(anyLong())).thenReturn(run);
        mapServiceUtils.createIngestTask(new SolrInputDocument(), "path", 1L, FileType.PDF,
                3L, 3L, 57L, 67L, new IngestTaskJsonProperties());
        verify(mapBatchResultsProcessor, times(0)).
                collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testCreateIngestTaskWithNoDoc() {
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> true);
        mapServiceUtils.createIngestTask(null, "path", 1L, FileType.PDF,
                3L, 3L, 57L, 67L, new IngestTaskJsonProperties());
        verify(mapBatchResultsProcessor, times(1)).
                collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testCreateIngestTaskWithDoc() {
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> true);
        SolrInputDocument doc = new SolrInputDocument();
        mapServiceUtils.createIngestTask(doc, "path", 1L, FileType.PDF,
                3L, 3L, 57L, 67L, new IngestTaskJsonProperties());
        verify(mapBatchResultsProcessor, times(0)).
                collectCatFiles(any(Pair.class), any(FileToAdd.class));
        assertTrue(MapServiceUtils.getFieldValueFromSolrInputDoc(doc, CatFileFieldType.CURRENT_RUN_ID).equals(3L));
    }

    @Test
    public void testHandleNewFileNoContent() {
        mapServiceUtils.handleFinishMapForNewFile(new SolrInputDocument(), 5L, FileType.PACKAGE, "path", 2L,
        3L, 7L, 9L, 15L, null);
        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));
        verify(docStoreService, times(0)).getRootFolderIngestTypePredicate(anyLong());
    }

    @Test
    public void testHandleNewFileWithContent() {
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> true);
        mapServiceUtils.handleFinishMapForNewFile(new SolrInputDocument(), 5L, FileType.PDF, "path", 2L,
                3L, 7L, 9L, 15L, Lists.newArrayList());
        verify(eventBus, times(1)).broadcast(anyString(), any(Event.class));
        verify(docStoreService, times(1)).getRootFolderIngestTypePredicate(anyLong());
    }
}