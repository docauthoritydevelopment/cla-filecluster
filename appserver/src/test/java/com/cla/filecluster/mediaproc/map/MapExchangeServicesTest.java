package com.cla.filecluster.mediaproc.map;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.kvdb.KeyValueDaoConfig;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.kvdb.domain.FileEntityUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.cla.test.filecluster.kvdb.KFileEntityDao;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapExchangeServicesTest {

    @Mock
    private FileCatService fileCatService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private RootFolderService rootFolderService;

    @Mock
    private KeyValueDatabaseRepository kvdbRepo;

    @Mock
    private EventBus eventBus;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @InjectMocks
    private MapRenameHandler mapRenameHandler = Mockito.mock(MapRenameHandler.class);

    @InjectMocks
    private MapExchangeServices mapExchangeServices;

    private Random random = new Random(System.currentTimeMillis());

    private static HashMap<String, FileEntity> keysToFileEntity = new HashMap<>();

    @Before
    public void set() {
        when(fileCatService.getNextAvailableFileId()).thenReturn(random.nextLong());
        when(fileCatService.getNextAvailableContentId()).thenReturn(random.nextLong());
        KFileEntityDao dao = new KFileEntityDao(KeyValueDaoConfig.create(), eventBus, keysToFileEntity);
        when(kvdbRepo.getFileEntityDao()).thenReturn(dao);
        when(mapRenameHandler.handleFolderMove(any(), anyLong(), anyLong(), anyLong())).thenCallRealMethod();
    }

    @After
    public void clean() {
        Mockito.reset(kvdbRepo);
        Mockito.reset(rootFolderService);
        Mockito.reset(docFolderService);
        Mockito.reset(fileCatService);
        Mockito.reset(mapBatchResultsProcessor);
    }

    @Test
    public void testHandleEmailAttachmentsNew() {
        Map<String, FileCatBuilder> attachments = new HashMap<>();

        FileCatBuilder builder = new FileCatBuilder();
        Exchange365ItemDto prop = new Exchange365ItemDto();
        prop.setFileName("att1");
        prop.setMediaItemId("att1");
        builder.setClaFilePropertiesDto(prop);
        attachments.put("att1", builder);

        builder = new FileCatBuilder();
        prop = new Exchange365ItemDto();
        prop.setFileName("att2");
        prop.setMediaItemId("att2");
        builder.setClaFilePropertiesDto(prop);
        attachments.put("att2", builder);

        Collection<SolrFileEntity> attachmentsToDelete = mapExchangeServices.handleEmailAttachments(attachments, null);
        assertTrue(attachmentsToDelete.isEmpty());

        assertTrue(attachments.get("att1").getId() != null);
        assertTrue(attachments.get("att2").getId() != null);

        assertTrue(attachments.get("att1").getFileId() != null);
        assertTrue(attachments.get("att2").getFileId() != null);

        assertTrue(attachments.get("att1").getFileType().equals(FileType.OTHER));
        assertTrue(attachments.get("att2").getFileType().equals(FileType.OTHER));
    }

    @Test
    public void testHandleEmailAttachmentsExists() {
        Map<String, FileCatBuilder> attachments = new HashMap<>();

        FileCatBuilder builder = new FileCatBuilder();
        Exchange365ItemDto prop = new Exchange365ItemDto();
        prop.setFileName("att1");
        prop.setMediaItemId("att1");
        builder.setClaFilePropertiesDto(prop);
        attachments.put("att1", builder);

        builder = new FileCatBuilder();
        prop = new Exchange365ItemDto();
        prop.setFileName("att2");
        prop.setMediaItemId("att2");
        builder.setClaFilePropertiesDto(prop);
        attachments.put("att2", builder);

        List<SolrFileEntity> attachmentEntities = new ArrayList<>();

        SolrFileEntity att1 = new SolrFileEntity();
        att1.setMediaEntityId("att1");
        att1.setType(FileType.OTHER.toInt());
        attachmentEntities.add(att1);

        SolrFileEntity att3 = new SolrFileEntity();
        att3.setMediaEntityId("att3");
        att3.setType(FileType.WORD.toInt());
        attachmentEntities.add(att3);


        when(fileCatService.findEmailAttachments(77L)).thenReturn(attachmentEntities);

        Collection<SolrFileEntity> attachmentsToDelete = mapExchangeServices.handleEmailAttachments(attachments, 77L);
        assertTrue(!attachmentsToDelete.isEmpty());
        assertTrue(attachmentsToDelete.contains(att3));

        assertTrue(attachments.get("att1").getId() != null);
        assertTrue(attachments.get("att2").getId() != null);

        assertTrue(attachments.get("att1").getFileId() != null);
        assertTrue(attachments.get("att2").getFileId() != null);

        assertTrue(attachments.get("att1").getFileType().equals(FileType.OTHER));
        assertTrue(attachments.get("att2").getFileType().equals(FileType.OTHER));
    }

    @Test
    public void handleExchangeTestFileFolderInKvdb() {
        ScanResultMessage scanResultMessage = new ScanResultMessage();
        scanResultMessage.setRootFolderId(55L);
        Exchange365ItemDto prop = new Exchange365ItemDto();
        prop.setEventType(DiffType.RENAMED);
        prop.setFileName("fghdfhdfhd");
        prop.setFile(true);
        prop.setParentFolderId("fghfhf");
        scanResultMessage.setPayload(prop);

        FileEntity folder = new FileEntity();
        folder.setPath("D:/dev/fghfhf");
        keysToFileEntity.put("fghfhf", folder);

        boolean res = mapExchangeServices.handleExchange(scanResultMessage, 45L);
        assertFalse(res);
        assertEquals(prop.getFileName(), "D:/dev/fghfhf/fghdfhdfhd");
    }

    @Test
    public void handleExchangeTestFileFolderInSolr() {
        ScanResultMessage scanResultMessage = new ScanResultMessage();
        scanResultMessage.setRootFolderId(55L);
        Exchange365ItemDto prop = new Exchange365ItemDto();
        prop.setEventType(DiffType.RENAMED);
        prop.setFileName("fghdfhdfhd");
        prop.setFile(true);
        prop.setParentFolderId("fghfhf");
        scanResultMessage.setPayload(prop);

        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setPath("D:/dev/fghfhf/");
        when(docFolderService.getFolderByMediaEntityId(55L, "fghfhf")).thenReturn(folder);

        boolean res = mapExchangeServices.handleExchange(scanResultMessage, 45L);
        assertFalse(res);
        assertEquals(prop.getFileName(), "D:/dev/fghfhf/fghdfhdfhd");
    }

    @Test
    public void handleExchangeTestFolderMoved() {
        ScanResultMessage scanResultMessage = new ScanResultMessage();
        scanResultMessage.setRootFolderId(55L);
        scanResultMessage.setRunContext(1L);
        Exchange365ItemDto prop = new Exchange365ItemDto();
        prop.setEventType(DiffType.RENAMED);
        prop.setFileName("fghfhf");
        prop.setFile(false);
        prop.setFolder(true);
        prop.setMediaItemId("fghfhf");
        scanResultMessage.setPayload(prop);

        FileEntity folder = new FileEntity();
        folder.setFolderId(45L);
        folder.setPath("fghfhf2");
        keysToFileEntity.put("fghfhf", folder);

        boolean res = mapExchangeServices.handleExchange(scanResultMessage, 4566666L);
        assertTrue(res);
        verify(fileCatService, times(1)).handleFolderMove(45L, "fghfhf2", "fghfhf", 55L);

        assertEquals(folder.getPath(), "fghfhf");
        assertEquals(folder.getLastScanned().longValue(), 4566666L);
    }

    @Test
    public void handleExchangeTestFolderNotMoved() {
        ScanResultMessage scanResultMessage = new ScanResultMessage();
        scanResultMessage.setRootFolderId(55L);
        Exchange365ItemDto prop = new Exchange365ItemDto();
        prop.setEventType(DiffType.RENAMED);
        prop.setFileName("fghfhf");
        prop.setFile(false);
        prop.setFolder(true);
        prop.setMediaItemId("fghfhf");
        scanResultMessage.setPayload(prop);

        FileEntity folder = new FileEntity();
        folder.setFolderId(45L);
        folder.setPath("fghfhf");
        keysToFileEntity.put("fghfhf", folder);

        boolean res = mapExchangeServices.handleExchange(scanResultMessage, 4566666L);
        assertFalse(res);
        verify(fileCatService, times(0)).handleFolderMove(anyLong(), anyString(), anyString(), anyLong());
    }

    @Test
    public void testFindAttachmentsToDelete() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setMediaType(MediaType.EXCHANGE_365);
        SolrFileEntity entity = new SolrFileEntity();
        entity.setItemType(0);
        entity.setNumOfAttachments(1);
        entity.setFileId(7L);

        List<SolrFileEntity> attachments = new ArrayList<>();
        SolrFileEntity att1 = new SolrFileEntity();
        att1.setMediaEntityId("att1");
        att1.setType(FileType.OTHER.toInt());
        attachments.add(att1);
        when(fileCatService.findEmailAttachments(7L)).thenReturn(attachments);

        FileEntity att1Entity = new FileEntity();
        keysToFileEntity.put("att1", att1Entity);

        Map<SolrFileEntity, FileEntity> res = mapExchangeServices.findAttachmentsToDelete(rootFolder, entity, "store");
        assertFalse(res.isEmpty());
        assertEquals(1, res.size());
        assertTrue(res.keySet().contains(att1));
        assertEquals(res.get(att1), att1Entity);
    }

    @Test
    public void testHandleAttachmentCreation() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(76858L);
        rootFolder.setMediaType(MediaType.EXCHANGE_365);
        Map<String, FileCatBuilder> attachmentsToCreate = new HashMap<>();

        FileCatBuilder builder1 = new FileCatBuilder();
        Exchange365ItemDto prop1 = new Exchange365ItemDto();
        prop1.setFileName("att1");
        prop1.setMediaItemId("att1");
        prop1.setSentDate(56456L);
        builder1.setFileId(888L);
        builder1.setId("34!888");
        builder1.setClaFilePropertiesDto(prop1);
        attachmentsToCreate.put("att1", builder1);

        FileCatBuilder builder2 = new FileCatBuilder();
        Exchange365ItemDto prop2 = new Exchange365ItemDto();
        prop2.setFileName("att2");
        prop2.setMediaItemId("att2");
        prop2.setSentDate(56456L);
        builder2.setFileId(999L);
        builder2.setId("56!999");
        builder2.setClaFilePropertiesDto(prop2);
        attachmentsToCreate.put("att2", builder2);

        Pair<Long, Long> batchTaskId = Pair.of(67L, 9L);

        Map<CatFileFieldType, Object> associationData = new HashMap<>();
        mapExchangeServices.handleAttachmentCreation(attachmentsToCreate, batchTaskId,
                rootFolder, 45L, 24L, 56547568L, associationData);


        SolrInputDocument doc1 = builder1.buildAtomicUpdate();
        FileEntity attFileEntity1 = FileEntityUtils.createFileEntity(888L, 24L, 56547568L, prop1, "att1");
        FileToAdd att1Add = new FileToAdd(doc1, attFileEntity1, "att1", 76858L, 45L);
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(batchTaskId, att1Add);

        SolrInputDocument doc2 = builder1.buildAtomicUpdate();
        FileEntity attFileEntity2 = FileEntityUtils.createFileEntity(999L, 24L, 56547568L, prop2, "att2");
        FileToAdd att2Add = new FileToAdd(doc2, attFileEntity2, "att2", 76858L, 45L);
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(batchTaskId, att2Add);
    }

    @Test
    public void testHandleAttachmentDeletion() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(76858L);
        rootFolder.setMediaType(MediaType.EXCHANGE_365);

        List<SolrFileEntity> attachmentsToDelete = new ArrayList<>();
        SolrFileEntity toDel = new SolrFileEntity();
        toDel.setFullName("drfgdfgdfg");
        toDel.setFileId(7L);
        toDel.setMediaEntityId("drfgdfgdfg");
        attachmentsToDelete.add(toDel);
        List<Triple<FileToDel, FileEntity, String>> res = mapExchangeServices.handleAttachmentDeletion(
                attachmentsToDelete, rootFolder, 45L, 24L, 56547568L);

        assertFalse(res.isEmpty());
        assertEquals(1, res.size());

        Triple<FileToDel, FileEntity, String> res1 = res.get(0);
        assertEquals("drfgdfgdfg", res1.getRight());

        assertEquals(7L, res1.getMiddle().getFileId().longValue());
        assertEquals(24L, res1.getMiddle().getFolderId().longValue());
        assertEquals("drfgdfgdfg", res1.getMiddle().getPath());

        assertEquals(7L, res1.getLeft().getFileId().longValue());
        assertEquals(45L, res1.getLeft().getRunId().longValue());
    }
}