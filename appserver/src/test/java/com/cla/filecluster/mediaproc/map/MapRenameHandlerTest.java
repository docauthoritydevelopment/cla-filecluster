package com.cla.filecluster.mediaproc.map;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.eventbus.EventBus;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.cla.connector.domain.dto.file.DiffType.RENAMED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapRenameHandlerTest {

    @Mock
    private FileCatService fileCatService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private MapDeleteHandler mapDeleteHandler;

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private MapRenameHandler mapRenameHandler;

    @After
    public void clean() {
        Mockito.reset(mapDeleteHandler);
        Mockito.reset(eventBus);
        Mockito.reset(docFolderService);
        Mockito.reset(fileCatService);
        Mockito.reset(mapServiceUtils);
    }

    @Test
    public void testRenameFolderInRF() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(678L);

        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFile(false);
        payload.setFolder(true);
        payload.setMediaItemId("dfgdfgd");
        payload.setFileName("dfgdfgd");

        FileEntity oldFileEntity = new FileEntity();
        oldFileEntity.setFolder(true);
        oldFileEntity.setFolderId(67L);
        oldFileEntity.setFile(false);

        boolean updateOldFileEntity = mapRenameHandler.handleFileOrDeletedFolderRename(4L, rootFolder, 8L, 78L,
                payload, oldFileEntity, true);

        // Verify it does require old file entity update and it did not pass through any of the handlers
        // This will only be handled in the FolderRenameHandler link
        Assert.assertTrue(updateOldFileEntity);
        verify(docFolderService, times(0))
                .createDocFolderIfNeededWithCache(678L, "dfgdfgd/");

        verify(mapDeleteHandler, times(0))
                .removeFilesFromFolder(anyLong(), anyLong(), anyLong(), anyLong(), anyLong());

        verify(eventBus, times(0)).broadcast(any(), any());
    }

    @Test
    public void testRenameFolderNotInRF() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(678L);

        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFile(false);
        payload.setFolder(true);
        payload.setMediaItemId("dfgdfgd");
        payload.setFileName("dfgdfgd");

        FileEntity oldFileEntity = new FileEntity();
        oldFileEntity.setFolder(true);
        oldFileEntity.setFolderId(67L);
        oldFileEntity.setFile(false);

        mapRenameHandler.handleFileOrDeletedFolderRename(4L, rootFolder, 8L, 78L,
                payload, oldFileEntity, false);

        verify(docFolderService, times(0))
                .createDocFolderIfNeededWithCache(anyLong(), anyString());

        verify(mapDeleteHandler, times(1))
                .removeFilesFromFolder(4L, 8L, 78L, 678L, 67L);

        verify(eventBus, times(0)).broadcast(any(), any());
    }

    @Test
    public void testRenameFileInRF() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(678L);

        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFile(true);
        payload.setFolder(false);
        payload.setMediaItemId("dfgdfgd");
        payload.setFileName("dfgdfgd");

        FileEntity oldFileEntity = new FileEntity();
        oldFileEntity.setFileId(67L);

        mapRenameHandler.handleFileOrDeletedFolderRename(4L, rootFolder, 8L, 78L,
                payload, oldFileEntity, true);

        verify(fileCatService, times(1))
                .processFileRename(rootFolder, payload, 67L);

        verify(eventBus, times(1)).broadcast(any(), any());
    }

    @Test
    public void testRenameFileNotInRF() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(678L);

        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFile(true);
        payload.setFolder(false);
        payload.setMediaItemId("dfgdfgd");
        payload.setFileName("dfgdfgd");

        FileEntity oldFileEntity = new FileEntity();
        oldFileEntity.setFileId(67L);

        mapRenameHandler.handleFileOrDeletedFolderRename(4L, rootFolder, 8L, 78L,
                payload, oldFileEntity, false);

        verify(mapDeleteHandler, times(1))
                .handleDeletedFiles(8L, 78L, 678L, 4L, "dfgdfgd");

        verify(eventBus, times(0)).broadcast(any(), any());
    }

    @Test
    public void testCreateIngestTaskForRenamed() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(678L);

        ScanResultMessage scanResultMessage = new ScanResultMessage();
        scanResultMessage.setTaskId(78L);

        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFileName("fdghdfgdf");
        payload.setType(FileType.WORD);

        FileEntity oldFileEntity = new FileEntity();
        oldFileEntity.setFileId(67L);

        when(mapServiceUtils.getFileEntity(678L, "fdghdfgdf")).thenReturn(oldFileEntity);

        mapRenameHandler.createIngestTaskForRenamed(4L, rootFolder, 8L,
                scanResultMessage, payload, "fdghdfgdf", oldFileEntity);

        verify(mapServiceUtils, times(0)).getFileEntity(anyLong(), anyString());

        verify(mapServiceUtils, times(1)).createIngestTask(null, "fdghdfgdf", 678L, FileType.WORD,
                4L, 8L, 78L, 67L, IngestTaskJsonProperties.forDiffType(RENAMED));
    }

    @Test
    public void testCreateIngestTaskForRenamedEmptyFileEntity() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(678L);

        ScanResultMessage scanResultMessage = new ScanResultMessage();
        scanResultMessage.setTaskId(78L);

        ClaFilePropertiesDto payload = ClaFilePropertiesDto.create();
        payload.setFileName("fdghdfgdf");
        payload.setType(FileType.WORD);

        FileEntity oldFileEntity = new FileEntity();
        oldFileEntity.setFileId(67L);

        when(mapServiceUtils.getFileEntity(678L, "fdghdfgdf")).thenReturn(oldFileEntity);

        mapRenameHandler.createIngestTaskForRenamed(4L, rootFolder, 8L,
                scanResultMessage, payload, "fdghdfgdf", null);

        verify(mapServiceUtils, times(1)).getFileEntity(anyLong(), anyString());

        verify(mapServiceUtils, times(1)).createIngestTask(null, "fdghdfgdf", 678L, FileType.WORD,
                4L, 8L, 78L, 67L, IngestTaskJsonProperties.forDiffType(RENAMED));
    }
}