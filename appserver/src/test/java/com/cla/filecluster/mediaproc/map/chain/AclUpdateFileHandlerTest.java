package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class AclUpdateFileHandlerTest {

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private ScanDiffEventProvider scanDiffEventProvider;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private AclUpdateFileHandler aclUpdateFileHandler;

    @Before
    public void init() {
        FileEntity fileEntity = new FileEntity();
        fileEntity.setSize(66L);
        fileEntity.setFileId(44L);
        fileEntity.setLastScanned(68L);
        fileEntity.setLastModified(67L);
        fileEntity.setAclSignature("acl2");
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(fileEntity);
    }

    @Test
    public void testShouldHandleScanAcl() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(false);
        mapContext.setDiffType(DiffType.ACL_UPDATED);
        assertTrue(aclUpdateFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleReScanCng() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(false);
        mapContext.setDiffType(DiffType.CONTENT_UPDATED);
        assertTrue(!aclUpdateFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleFstScan() {
        MapResultContext mapContext = getContext();
        mapContext.setFstScan(true);
        assertTrue(!aclUpdateFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        msg.setPayload(new ItemDeletionPayload());
        assertTrue(!aclUpdateFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void handleRegularFile() {
        MapResultContext mapContext = getContext();
        testHandle(mapContext, 1);
    }

    @Test
    public void handlePackageFile() {
        MapResultContext mapContext = getContext();
        mapContext.getScanResultMessage().setPackagedScan(true);
        testHandle(mapContext, 0);
    }

    private void testHandle(MapResultContext mapContext, int ingestTimes) {
        aclUpdateFileHandler.handle(mapContext);

        verify(mapServiceUtils, times(ingestTimes)).createIngestTask(isNull(SolrInputDocument.class), anyString(),  anyLong(),
                any(FileType.class), anyLong(), anyLong(), anyLong(), anyLong(), any(IngestTaskJsonProperties.class));

        verify(scanDiffEventProvider, times(1)).generateScanDiffEvent(
                EventType.SCAN_DIFF_ACL_UPDATED, 44L, "ghjg");

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(1)).broadcast(topicCaptor.capture(), eventCaptor.capture());
        assertTrue(eventCaptor.getValue().getEventType().equals(com.cla.eventbus.events.EventType.RUN_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));

        assertTrue(mapContext.getFileEntity() != null);
        assertTrue(mapContext.getFileEntity().getSize() == 66L);
        assertTrue(mapContext.getFileEntity().getAclSignature().equals("acl"));
    }

    private MapResultContext getContext() {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setKvdbKey("dfgd");
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFileSize(66L);
        prop.setAclSignature("acl");
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);
        msg.setPayload(prop);
        return mapContext;
    }

}