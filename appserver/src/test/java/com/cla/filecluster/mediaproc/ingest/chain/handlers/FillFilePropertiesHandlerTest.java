package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.cla.filecluster.mediaproc.ingest.chain.handlers.IngestChainHandlersTestUtils.createSimpleContext;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FillFilePropertiesHandlerTest {

    @Mock
    protected FilesDataPersistenceService filesDataPersistenceService;

    @InjectMocks
    private FillFilePropertiesHandler filePropertiesHandler;

    @Test
    public void testShouldHandle(){
        Assert.assertTrue(filePropertiesHandler.shouldHandle(createSimpleContext()));
    }

    @Test
    public void testHandle(){
        IngestResultContext context = createSimpleContext();
        PstEntryPropertiesDto props = new PstEntryPropertiesDto();
        context.getIngestResultMessage().setFileProperties(props);

        EmailAddress emailAddress = new EmailAddress();
        emailAddress.setName("Name");
        emailAddress.setAddress("name@domain.com");
        props.setSender(emailAddress);

        boolean continueHandling = filePropertiesHandler.handle(context);
        Assert.assertTrue(continueHandling);

        verify(filesDataPersistenceService, times(1)).fillFileProperties(
                any(ClaFilePropertiesDto.class), any(SolrFileEntity.class));

        SolrFileEntity file = context.getFile();
        assertEquals("name@domain.com", file.getSenderAddress());
        assertEquals("name", file.getSenderName());
    }

}
