package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.ItemToCreate;
import com.cla.filecluster.mediaproc.map.MapAddHandler;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class NewFileHandlerTest {

    @Mock
    private MapServiceUtils mapServiceUtils;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private MapAddHandler mapAddHandler;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private NewFileHandler newFileHandler;

    @Before
    public void init() {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setId(1L);
        rootFolder.setMediaType(MediaType.FILE_SHARE);
        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);
    }

    @After
    public void clean() {
        Mockito.reset(mapServiceUtils);
    }

    @Test
    public void testShouldHandleNewScan() {
        MapResultContext mapContext = getContext(true, DiffType.NEW);
        assertTrue(newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleNewFile() {
        MapResultContext mapContext = getContext(false, DiffType.NEW);
        assertTrue(newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleCreateUpdFile() {
        MapResultContext mapContext = getContext(false, DiffType.CREATED_UPDATED);
        assertTrue(newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleAclUpd() {
        MapResultContext mapContext = getContext(false, DiffType.ACL_UPDATED);
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleContentUpd() {
        MapResultContext mapContext = getContext(false, DiffType.CONTENT_UPDATED);
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleSimilar() {
        MapResultContext mapContext = getContext(false, DiffType.SIMILAR);
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleRenameNewSameRF() {
        MapResultContext mapContext = getContext(false, DiffType.RENAMED);
        mapContext.setNewFile(true);
        mapContext.setBelongToRF(true);
        mapContext.setAlreadyHandled(false);
        assertTrue(newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleRenameNewDiffRF() {
        MapResultContext mapContext = getContext(false, DiffType.RENAMED);
        mapContext.setNewFile(true);
        mapContext.setBelongToRF(false);
        mapContext.setAlreadyHandled(false);
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleRenameExisting() {
        MapResultContext mapContext = getContext(false, DiffType.RENAMED);
        mapContext.setNewFile(false);
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleUndeleteNew() {
        MapResultContext mapContext = getContext(false, DiffType.UNDELETED);
        assertTrue(newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleUndeleteOld() {
        MapResultContext mapContext = getContext(false, DiffType.UNDELETED);
        mapContext.setKvdbKey("fdgf");
        when(mapServiceUtils.getFileEntity(anyLong(), anyString())).thenReturn(new FileEntity());
        assertTrue(!newFileHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandle() {
        MapResultContext mapContext = getContext(true, DiffType.NEW);
        addFileData(mapContext);

        ItemToCreate item = new ItemToCreate(new SolrInputDocument(), null, null, null);
        item.getDocFileToCreate().setField(CatFileFieldType.FOLDER_ID.getSolrName(), SolrRepositoryUtils.setValueMap(2L));
        when(mapAddHandler.addScannedItem(1L, 1L,
                (ClaFilePropertiesDto)mapContext.getScanResultMessage().getPayload())).thenReturn(item);

        newFileHandler.handle(mapContext);
        verify(mapAddHandler, times(1)).addScannedItem(1L, 1L,
                (ClaFilePropertiesDto)mapContext.getScanResultMessage().getPayload());

        ArgumentCaptor<Event> eventCaptor = ArgumentCaptor.forClass(Event.class);
        ArgumentCaptor<String> topicCaptor = ArgumentCaptor.forClass(String.class);
        verify(eventBus, times(1)).broadcast(topicCaptor.capture(), eventCaptor.capture());
        assertTrue(eventCaptor.getValue().getEventType().equals(EventType.RUN_PROGRESS));
        assertTrue(topicCaptor.getValue().equals(Topics.MAP));

        assertTrue(mapContext.getFileEntity() != null);
        assertTrue(mapContext.getSolrInputDocumentFileAndFolderId() != null);
        assertTrue(mapContext.getFolderId().equals(2L));
    }

    private void addFileData(MapResultContext mapContext) {
        ScanResultMessage msg = mapContext.getScanResultMessage();
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        msg.setPayload(prop);
    }

    private MapResultContext getContext(boolean fstScan, DiffType diffType) {
        MapResultContext mapContext = new MapResultContext();
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        mapContext.setFstScan(fstScan);
        mapContext.setDiffType(diffType);
        return mapContext;
    }
}