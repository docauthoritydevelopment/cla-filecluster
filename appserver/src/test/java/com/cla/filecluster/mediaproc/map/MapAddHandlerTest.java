package com.cla.filecluster.mediaproc.map;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.AttachmentInfo;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapAddHandlerTest {

    @Mock
    private FileCatService fileCatService;

    @Mock
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private MapExchangeServices mapExchangeServices;

    @InjectMocks
    private MapAddHandler mapAddHandler;

    private static Long rootFolderId = 5L;
    private static Long runId = 2L;
    private static Long docFolderId = 89L;

    @Before
    public void init() {
        when(performanceKpiRecorder.startRecording(anyString(), anyString(), anyInt())).thenReturn(
                new KpiRecording("", "", 1, System.currentTimeMillis(),
                        performanceKpiRecorder, Thread.currentThread().getName()));

        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(docFolderId);
        DocFolderService.DocFolderCacheValue cacheValue = new DocFolderService.DocFolderCacheValue(folder);
        when(docFolderService.createDocFolderIfNeededWithCache(anyLong(), anyString())).thenReturn(cacheValue);
    }

    @After
    public void clean() {
        Mockito.reset(docFolderService);
    }

    @Test(expected = RuntimeException.class)
    public void testAddItemNoProp() {
        mapAddHandler.addScannedItem(rootFolderId, runId, null);
    }

    @Test
    public void testAddFolder() {
        ClaFilePropertiesDto claFileProp = ClaFilePropertiesDto.create();
        claFileProp.setFolder(true);
        claFileProp.setFileName("fgdd");

        ItemToCreate res = mapAddHandler.addScannedItem(rootFolderId, runId, claFileProp);

        verify(docFolderService, times(1)).updateDocFolderProperties(
                docFolderId, claFileProp, runId, true, true, rootFolderId);

        assertTrue(res.getFolderId().equals(docFolderId));
    }

    @Test
    public void testAddRegularNewFile() {
        ClaFilePropertiesDto claFileProp = ClaFilePropertiesDto.create();
        claFileProp.setFile(true);
        claFileProp.setFileName("fgdd.doc");

        FileCatBuilder builder = FileCatBuilder.create();
        builder.setId("5!5");

        when(fileCatService.getBuilderForCreateUpdateRecord(
                isNull(), anyLong(), anyLong(), isNull(), any(ClaFilePropertiesDto.class), isNull())).thenReturn(builder);

        ItemToCreate res = mapAddHandler.addScannedItem(rootFolderId, runId, claFileProp);

        verify(fileCatService, times(1)).getByFileHashAll(rootFolderId, "fgdd.doc");

        verify(mapExchangeServices, times(1)).handleEmailAttachments(anyMap(), isNull());

        verify(fileCatService, times(0)).getFileObject(anyLong());

        verify(fileCatService, times(1)).useExistingOrCreateRecord(null,
                docFolderId, "fgdd.doc",
                FileType.WORD, claFileProp, rootFolderId);

        assertTrue(res.getFolderId() == null);
        assertTrue(res.getDocFileToCreate() != null);
        assertTrue(res.getAttachmentsToCreate() != null && res.getAttachmentsToCreate().isEmpty());
        assertTrue(res.getAttachmentsToDelete() != null && res.getAttachmentsToDelete().isEmpty());
    }

    @Test
    public void testAddRegularExistFile() {
        ClaFilePropertiesDto claFileProp = ClaFilePropertiesDto.create();
        claFileProp.setFile(true);
        claFileProp.setFileName("fgdd.doc");

        FileCatBuilder builder = FileCatBuilder.create();
        builder.setId("5!5");

        when(fileCatService.getBuilderForCreateUpdateRecord(
                isNull(), anyLong(), anyLong(), isNull(), any(ClaFilePropertiesDto.class), isNull())).thenReturn(builder);

        ClaFile file = new ClaFile();
        file.setId(78L);
        when(fileCatService.useExistingOrCreateRecord(null,
                docFolderId, "fgdd.doc",
                FileType.WORD, claFileProp, rootFolderId)).thenReturn(file);

        ItemToCreate res = mapAddHandler.addScannedItem(rootFolderId, runId, claFileProp);

        verify(fileCatService, times(1)).getByFileHashAll(rootFolderId, "fgdd.doc");

        verify(mapExchangeServices, times(1)).handleEmailAttachments(anyMap(), isNull());

        verify(fileCatService, times(1)).getFileObject(78L);

        verify(fileCatService, times(1)).useExistingOrCreateRecord(null,
                docFolderId, "fgdd.doc",
                FileType.WORD, claFileProp, rootFolderId);

        assertTrue(res.getFolderId() == null);
        assertTrue(res.getDocFileToCreate() != null);
        assertTrue(res.getAttachmentsToCreate() != null && res.getAttachmentsToCreate().isEmpty());
        assertTrue(res.getAttachmentsToDelete() != null && res.getAttachmentsToDelete().isEmpty());
    }

    @Test
    public void testAddRegularNewEmailWithAttachment() {
        Exchange365ItemDto claFileProp = createEmailProp();

        FileCatBuilder builder = createBuilder(claFileProp);

        when(fileCatService.getBuilderForCreateUpdateRecord(
                isNull(), anyLong(), anyLong(), isNull(), any(ClaFilePropertiesDto.class), isNull())).thenReturn(builder);

        ItemToCreate res = mapAddHandler.addScannedItem(rootFolderId, runId, claFileProp);

        verify(fileCatService, times(1)).getByFileHashAll(rootFolderId, "fgdd.eml");

        verify(mapExchangeServices, times(1)).handleEmailAttachments(anyMap(), isNull());

        verify(fileCatService, times(0)).getFileObject(anyLong());

        verify(fileCatService, times(1)).useExistingOrCreateRecord(null,
                docFolderId, "fgdd.eml",
                FileType.MAIL, claFileProp, rootFolderId);

        assertTrue(res.getFolderId() == null);
        assertTrue(res.getDocFileToCreate() != null);
        assertTrue(res.getAttachmentsToCreate() != null && res.getAttachmentsToCreate().size() == 1);
        assertTrue(res.getAttachmentsToDelete() != null && res.getAttachmentsToDelete().isEmpty());
    }

    @Test
    public void testAddRegularNewEmailWithAttachmentDel() {
        Exchange365ItemDto claFileProp = createEmailProp();

        FileCatBuilder builder = createBuilder(claFileProp);

        when(fileCatService.getBuilderForCreateUpdateRecord(
                isNull(), anyLong(), anyLong(), isNull(), any(ClaFilePropertiesDto.class), isNull())).thenReturn(builder);

        Collection<SolrFileEntity> attachmentsToDelete = Lists.newArrayList(new SolrFileEntity());
        when(mapExchangeServices.handleEmailAttachments(anyMap(), isNull())).thenReturn(attachmentsToDelete);

        ItemToCreate res = mapAddHandler.addScannedItem(rootFolderId, runId, claFileProp);

        verify(fileCatService, times(1)).getByFileHashAll(rootFolderId, "fgdd.eml");

        verify(mapExchangeServices, times(1)).handleEmailAttachments(anyMap(), isNull());

        verify(fileCatService, times(0)).getFileObject(anyLong());

        verify(fileCatService, times(1)).useExistingOrCreateRecord(null,
                docFolderId, "fgdd.eml",
                FileType.MAIL, claFileProp, rootFolderId);

        assertTrue(res.getFolderId() == null);
        assertTrue(res.getDocFileToCreate() != null);
        assertTrue(res.getAttachmentsToCreate() != null && res.getAttachmentsToCreate().size() == 1);
        assertTrue(res.getAttachmentsToDelete() != null && res.getAttachmentsToDelete().size() == 1);
    }

    private Exchange365ItemDto createEmailProp() {
        Exchange365ItemDto claFileProp = new Exchange365ItemDto();
        claFileProp.setFile(true);
        claFileProp.setFileName("fgdd.eml");
        claFileProp.setSentDate(5654654L);
        claFileProp.setMailboxUpn("mailbox");
        claFileProp.setModTimeMilli(4L);
        claFileProp.setAccessTimeMilli(4L);
        claFileProp.setCreationTimeMilli(7L);

        List<AttachmentInfo> attachmentInfos = Lists.newArrayList(
                new AttachmentInfo("1", "name.doc", 55, "mime", "subject"));
        claFileProp.setAttachmentInfos(attachmentInfos);
        return claFileProp;
    }

    private FileCatBuilder createBuilder(ClaFilePropertiesDto claFileProp) {
        FileCatBuilder builder = FileCatBuilder.create();
        builder.setId("5!5");
        builder.setClaFilePropertiesDto(claFileProp);
        builder.setRootFolderId(rootFolderId).setDocFolderId(docFolderId);
        return builder;
    }
}