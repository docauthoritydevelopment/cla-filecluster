package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SuppressWarnings("unchecked")
@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class IngestionErrorHandlerTest {

    @Mock
    protected FilesDataPersistenceService filesDataPersistenceService;

    @InjectMocks
    private IngestionErrorHandler ingestionErrorHandler;

    @Mock
    private MessageHandler messageHandler;

    @Mock
    private JobManagerService jobManagerService;

    @Mock
    private JobManagerAppService jobManagerAppService;

    @Mock
    private IngestErrorService ingestErrorService;

    @Test
    public void testDetectError(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        assertFalse(ingestionErrorHandler.shouldHandle(context));

        context.getResultMessage().addError(
                IngestChainHandlersTestUtils.createError(ProcessingErrorType.ERR_UNKNOWN));

        // still false, because the result doesn't have a payload
        // and the error type is not one of the 3 error types that
        // are always detected first: ERR_FILE_NOT_FOUND, ERR_LIMIT_EXCEEDED, ERR_OFFLINE_SKIPPED
        assertFalse(ingestionErrorHandler.shouldHandle(context));

        testErrorShouldHandle(context, ProcessingErrorType.ERR_FILE_NOT_FOUND);

        // reset errors and test ERR_LIMIT_EXCEEDED
        context = IngestChainHandlersTestUtils.createSimpleContext();
        testErrorShouldHandle(context, ProcessingErrorType.ERR_LIMIT_EXCEEDED);

        // reset errors and test ERR_OFFLINE_SKIPPED
        context = IngestChainHandlersTestUtils.createSimpleContext();
        testErrorShouldHandle(context, ProcessingErrorType.ERR_OFFLINE_SKIPPED);

        // check error on payload
        context = IngestChainHandlersTestUtils.createSimpleContext();
        WordIngestResult payload = new WordIngestResult();
        payload.setErrorType(ProcessingErrorType.ERR_CORRUPTED);
        context.getResultMessage().setPayload(payload);
        assertTrue(ingestionErrorHandler.shouldHandle(context));
        assertEquals(ProcessingErrorType.ERR_CORRUPTED, context.getErrorType());

        // reset errors and test other kind of error
        context = IngestChainHandlersTestUtils.createSimpleContext();
        context.getResultMessage().setPayload(new WordIngestResult());
        testErrorShouldHandle(context, ProcessingErrorType.ERR_IO);
    }

    @Test
    public void testHandle(){
        IngestResultContext context = IngestChainHandlersTestUtils.createSimpleContext();
        context.getResultMessage().addError(
                IngestChainHandlersTestUtils.createError(ProcessingErrorType.ERR_FILE_NOT_FOUND));
        assertTrue(ingestionErrorHandler.shouldHandle(context));
        assertFalse(ingestionErrorHandler.handle(context));
        verify(ingestErrorService, times(1)).
                saveError(1L, 1L, context.getFile(), ProcessingErrorType.ERR_FILE_NOT_FOUND, null, null);
        verify(jobManagerService, times(0)).handleFailedFinishJob(1L, "Ingest failed");

        context = IngestChainHandlersTestUtils.createSimpleContext();
        context.getResultMessage().addError(
                IngestChainHandlersTestUtils.createError(ProcessingErrorType.ERR_IO));
        IngestChainHandlersTestUtils.addWordPayload(context);

        assertTrue(ingestionErrorHandler.shouldHandle(context));
        assertFalse(ingestionErrorHandler.handle(context));
        verify(filesDataPersistenceService, times(1))
                .addIngestionErrorRecord(context.getResultMessage(), ProcessingErrorType.ERR_IO,
                        ClaFileState.SCANNED_AND_STOPPED, context.getIngestBatchStore());

    }

    private void testErrorShouldHandle(IngestResultContext context, ProcessingErrorType type){
        context.getResultMessage().addError(
                IngestChainHandlersTestUtils.createError(type));

        assertTrue(ingestionErrorHandler.shouldHandle(context));
        assertEquals(type, context.getErrorType());
    }
}
