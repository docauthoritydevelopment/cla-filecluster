package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.mediaproc.map.ItemToCreate;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class MapAclHandlerTest {

    @Mock
    private FileCatService fileCatService;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @InjectMocks
    private MapAclHandler mapAclHandler;

    @After
    public void clean() {
        Mockito.reset(fileCatService);
    }

    @Test
    public void testShouldHandleNoFileData() {
        MapResultContext mapContext = new MapResultContext();
        assertTrue(!mapAclHandler.shouldHandle(mapContext));
    }

    @Test
    public void testShouldHandleNoAclData() {
        assertTrue(!mapAclHandler.shouldHandle(getContext(false)));
    }

    @Test
    public void testShouldHandleWithAclData() {
        assertTrue(mapAclHandler.shouldHandle(getContext(true)));
    }

    @Test
    public void testShouldHandleDelete() {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setFileId(5L);
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setPayloadType(MessagePayloadType.SCAN_DELETION);
        msg.setPayload(new ItemDeletionPayload());
        assertTrue(!mapAclHandler.shouldHandle(mapContext));
    }

    @Test
    public void testHandleExistingWithAclData() {
        SolrFileEntity file = new SolrFileEntity();
        file.setId("5!5");
        when(fileCatService.getFileEntity(5L)).thenReturn(file);

        mapAclHandler.handle(getContext(true));

        verify(fileCatService, times(1)).updateAcls(any(SolrInputDocument.class), any(Set.class));
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testHandleNewWithAclData() {
        MapResultContext mapContext = getContext(true);
        ItemToCreate item = new ItemToCreate(new SolrInputDocument(), null, null, null);
        mapContext.setSolrInputDocumentFileAndFolderId(item);
        mapAclHandler.handle(mapContext);
        verify(fileCatService, times(1)).updateAcls(any(SolrInputDocument.class), any(Set.class));
        verify(mapBatchResultsProcessor, times(0)).collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    @Test
    public void testHandleNewWithAclDataAndAttachments() {
        MapResultContext mapContext = getContext(true);
        Map<String, FileCatBuilder> attachmentsToCreate = new HashMap<>();
        attachmentsToCreate.put("1", new FileCatBuilder());
        attachmentsToCreate.put("2", new FileCatBuilder());
        ItemToCreate item = new ItemToCreate(new SolrInputDocument(), null, attachmentsToCreate, null);
        mapContext.setSolrInputDocumentFileAndFolderId(item);
        mapAclHandler.handle(mapContext);
        verify(fileCatService, times(1)).updateAcls(any(SolrInputDocument.class), any(Set.class));
        verify(fileCatService, times(2)).updateAcls(any(FileCatBuilder.class), any(Set.class));
        verify(mapBatchResultsProcessor, times(0)).collectCatFiles(any(Pair.class), any(FileToAdd.class));
    }

    private MapResultContext getContext(boolean withAcl) {
        MapResultContext mapContext = new MapResultContext();
        mapContext.setFileId(5L);
        mapContext.setKvdbKey("dfgd");
        ScanResultMessage msg = new ScanResultMessage();
        mapContext.setScanResultMessage(msg);
        msg.setRootFolderId(1L);
        mapContext.setBatchIdentifierNumber(45654L);
        msg.setRunContext(1L);
        msg.setTaskId(33L);
        msg.setPayloadType(MessagePayloadType.SCAN);
        msg.setRunContext(1L);
        ClaFilePropertiesDto prop = ClaFilePropertiesDto.create();
        prop.setMediaItemId("dfgd");
        prop.setFileName("ghjg");
        prop.setFile(true);
        prop.setType(FileType.WORD);
        prop.setFolder(false);

        if (withAcl) {
            prop.addAllowedReadPrincipalNames(Lists.newArrayList("yegff", "trggg"));
            prop.addAllowedWritePrincipalName("trggg");
        }

        msg.setPayload(prop);
        return mapContext;
    }
}