package com.cla.filecluster.test.service;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagTypeAndTagsDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.CounterReportDto;
import com.cla.common.domain.dto.report.StringListReportDto;
import com.cla.common.domain.dto.report.StringReportDto;
import com.cla.common.domain.dto.report.SummaryReportDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.repository.solr.FileDTOSearchRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.api.FileTagApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This is a service that is used by the Performance Tests.
 * It is a replacement for a utility class
 * Created by uri on 23/06/2015.
 */

@Service
@Lazy
public class PerformanceTestService {

    public static final String SRC_TEST_RESOURCES_FILES_MCF = "/src/test/resources/files/mcf";

    public static final String SRC_TEST_RESOURCES_FILES_GRADUAL = "/src/test/resources/files/excel/gradual";

    private static final String SRC_TEST_RESOURCES_FILES_VARIOUS_OTHER = "/src/test/resources/files/various/other";

    public static final String SRC_TEST_RESOURCES_FILES_CONTINUE = "./src/test/resources/files/continue";

    public static final String SRC_TEST_RESOURCES_FILES_ANALYZE_TEST = "/src/test/resources/files/analyzeTest";
    public static final String SRC_TEST_RESOURCES_FILES_REMOTE = "/src/test/resources/files/remote";
    public static final String SRC_TEST_RESOURCES_FILES_ANALYZE_TEST_SUBFOLDER = SRC_TEST_RESOURCES_FILES_ANALYZE_TEST + "/subFolder";

    public static final String SRC_TEST_RESOURCES_FILES_WORD_XML = "/src/test/resources/files/wordXml";

    public static final String SRC_TEST_RESOURCES_FILES_VARIOUS = "/src/test/resources/files/various";

    public static final String SRC_TEST_RESOURCES_FILES = "/src/test/resources/files";
    private static final String ASSET_TYPE = "Asset Type";

    private static final String SHARE_POINT_URL = "http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com";
    private static final String SHARE_POINT_SITE = "/sites/test";
    private static final String SHARE_POINT_USER = "da.test";
    private static final String SHARE_POINT_PASSWORD = "doc4DocsTest";
    private static final String SHARE_POINT_DOMAIN = "simple.docauthority.com";
    private static final String SHARE_POINT_NAME = "sharePointConnection1";

    private static final String SHARE_POINT_TEST_LIBRARY = "YAL";

    public static final String SITES_TEST_RESCAN_TESTS_LIBRARY = SHARE_POINT_URL + SHARE_POINT_SITE + "/" + "Rescan tests";

    public static final String SITES_TEST_TEST_LIBRARY = SHARE_POINT_URL + SHARE_POINT_SITE + "/" + SHARE_POINT_TEST_LIBRARY;


    public static final String SITES_TEST_TEST_LIBRARY_SUBFOLDER = SHARE_POINT_URL + SHARE_POINT_SITE + "/" + SHARE_POINT_TEST_LIBRARY + "/subFolder";


    private static final String SHARE_POINT_365_URL = "https://docauthority.sharepoint.com";
    private static final String SHARE_POINT_365_SITE = "/tests";
    private static final String SHARE_POINT_365_USER = "ro-admin2";
    private static final String SHARE_POINT_365_PASSWORD = "doc4Sharepoint4";
    private static final String SHARE_POINT_365_DOMAIN = "docauthority.onmicrosoft.com";

    private static final String SHARE_POINT_365_BASIC_LIBRARY_TITLE = "basic";

    private static final String SHARE_POINT_365_RESCAN_LIBRARY_TITLE = "simple event tests";

    public static final String SHAREPOINT_365_TEST_LIBRARY = SHARE_POINT_365_URL + SHARE_POINT_365_SITE + "/" + SHARE_POINT_365_BASIC_LIBRARY_TITLE;

    public static final String SHAREPOINT_365_RESCAN_LIBRARY = SHARE_POINT_365_URL + SHARE_POINT_365_SITE + "/" + SHARE_POINT_365_RESCAN_LIBRARY_TITLE;

    public static final String ANALYZED_ROOT_FOLDER_PATH = "." + SRC_TEST_RESOURCES_FILES_ANALYZE_TEST;

    @Autowired
    private FileTagApiController fileTagApiController;

    @Autowired
    private FileTagService fileTagService;

    private static final Logger logger = LoggerFactory.getLogger(PerformanceTestService.class);

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private StartScanProducer startScanProducer;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private FileDTOSearchRepository fileDTOSearchRepository;

    @Autowired
    private ScheduledOperationService scheduledOperationService;


//    public static DocAuthorityKafkaEmbedded embeddedKafka = new DocAuthorityKafkaEmbedded(1,true,
////            ClaBaseTests.TEST_TOPIC,
//            ClaBaseTests.SCAN_REQUEST_TOPIC,
//            ClaBaseTests.SCAN_RESULT_TOPIC,
//            ClaBaseTests.INGEST_REQUEST_TOPIC,
//            ClaBaseTests.INGEST_RESULT_TOPIC,
//            ClaBaseTests.MEDIA_PROC_NOTIFICATIONS_TOPIC);

//    static {
//        try {
//            logger.info("Init Embedded Kafka server");
//            embeddedKafka.before();
//            Runtime.getRuntime().addShutdownHook(new Thread() {
//                public void run() {
//                    logger.info("Teardown embedded kafka server");
//                    embeddedKafka.after();
//                    logger.info("Teardown embedded kafka server complete");
//                    // shutdown logic
//                }
//            });
//        } catch (Exception e) {
//            logger.error("Failed to init embedded Kafka server ({})",e.getMessage(),e);
//        }
//    }


    private boolean preparedSqlSchema = false;

    @Autowired
    private TestSqlService testSqlService;

    public void prepareSystem() {
        prepareSqlSchema();
    }

    @Transactional
    public void prepareSqlSchema() {
        if (!preparedSqlSchema) {
            testSqlService.runPostBaseLine();
            //fileCrawlerFolderScannerService.prepareCrawlTempTables();
            preparedSqlSchema = true;
        }
    }

    public static void deleteFolder(Path directory) throws IOException {
        if (!Files.exists(directory)) {
            return;
        }
        Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {

            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);
                return FileVisitResult.CONTINUE;
            }

        });
    }

    public RootFolderDto analyzeOtherSubFolder() {
        final String path = "." + SRC_TEST_RESOURCES_FILES_VARIOUS_OTHER;
        opStateService.setStopOnError(true);
        verifyNoRun(path);
        RootFolderDto rootFolderDto = analyzeFolder(path, CrawlerType.MEDIA_PROCESSOR);
        verifyNoRun(path);
        return rootFolderDto;
    }

    public RootFolderDto analyzeReferenceFiles2() {
        final String path = ANALYZED_ROOT_FOLDER_PATH;
        logger.debug("analyzeReferenceFiles2: scanning {}", path);
        opStateService.setStopOnError(true);
        RootFolderDto rootFolderDto = analyzeFolder(path, CrawlerType.MEDIA_PROCESSOR);
        verifyNoRun(path);
        filesDataPersistenceService.commitSolr();
        fileDTOSearchRepository.commit();
        verifyGroupsInDatabase();
        verifyNoFilesInScannedState();

        int counter = filesApiController.getNoTagsSolrMissMatch();
        if (counter>0) {
            logger.debug("analyzeReferenceFiles2: found {} mismatches", counter);
        }
        return rootFolderDto;
    }

    private void verifyNoFilesInScannedState() {
        //Get the first 30
        Map<String, String> params = new HashMap<>();
        params.put("take","1000");
        List<ClaFileDto> content = filesApiController.findFilesDeNormalized(params).getContent();
        List<ClaFileDto> collect = content.stream().filter(f -> ClaFileState.SCANNED.equals(f.getState())).collect(Collectors.toList());
        if (collect.size() > 0) {
            logger.error("Found {} files in SCANNED state although system should be after a full scan",collect.size());
            collect.forEach(f-> logger.error("File is in SCANNED state {} ",f));
            Assert.fail("Found "+collect.size()+" files in SCANNED state although system should be after a full scan");
        }
    }

    private void verifyNoRun(String rootFolderPath) { //TODO Liora - just verification is needed
        String path = Paths.get(rootFolderPath).toAbsolutePath().normalize().toString();
        RootFolderDto rootFolderDto = rootFolderService.findRootFolder(path,false);

        if (rootFolderDto==null) {
            logger.info("Could not find RootFolder {}", rootFolderPath);
        } else {
            Long lastRunId = rootFolderDto.getLastRunId();
            if (lastRunId==null) {
                logger.info("No lastRun for RootFolder {}", rootFolderDto);
            } else {
                logger.info("Waiting for run {} of RootFolder {}", lastRunId, rootFolderDto);
                jobManagerAppService.waitForRunToFinish(lastRunId);
                logger.info("End wait for run {} of RootFolder {}", lastRunId, rootFolderDto);
            }
        }
    }

    public void verifyNoRunForScheduleGroup() {

    }

    private void verifyGroupsInDatabase() {
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        if (groupsFileCount.getContent().size() == 0) {
            throw new RuntimeException("No groups in database after analyzing folder");
        }
    }

    public RootFolderDto analyzeFolder(String path) {
        return analyzeFolder(path,CrawlerType.MEDIA_PROCESSOR);
    }

    public RootFolderDto analyzeFolder(String path, CrawlerType crawlerType) {
        try {
            path = Paths.get(path).toAbsolutePath().normalize().toString();
            final List<String> opList = Lists.newArrayList("scan", "ingest", "analyze");
            RootFolderDto rootFolderDto = rootFolderService.findRootFolder(path,true);
            if (rootFolderDto != null) {
                //Root folder was already scanned before
                logger.info("Root folder {} was already scanned before", rootFolderDto.getPath());
                long count = filesApiController.countFilesDeNormalized(null);
                logger.info("There are now {} files in Solr", count);
                return rootFolderDto;
            }
            long start = System.currentTimeMillis();
            logger.info("scan files in path {}", path);
            RootFolderDto folderDto = createRootFolderIfNeeded(path, crawlerType);
            DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
            dataSourceRequest.setWait(true);
            startScanProducer.startScanRootFolder(folderDto.getId(), dataSourceRequest.isWait(), opList, false);

            FileTagDto createdFileTagDto = createRiskFileTag(path+System.currentTimeMillis());

            rootFolderDto = rootFolderService.findRootFolder(path,true);
            if (rootFolderDto == null) {
                logger.error("Unable to find root folder by path {}",path);
                throw new RuntimeException("Unable to find root folder by path "+path);
            }
            //Tag all files in the folder
            tagAllFilesInRootFolder(rootFolderDto, createdFileTagDto);
            printSuiteStartBanner();
            long duration = System.currentTimeMillis() - start;
            if (duration > 5000) {

                logger.warn("Full run on rootFolder {} took {}ms", path, duration);
            }
            solrFileCatRepository.commit();
            return rootFolderDto;
        } catch (RuntimeException e) {
            logger.error("Exception while trying to analyze folder " + e.getMessage(), e);
            opStateService.informRunStoppedOnException(e);
            throw e;
        }
    }

//    private List<RootFolderDto> createRootFoldersIfNeeded(String path) {
////        List<RootFolderDto> rootFolderDtos = docStoreService.createRootFoldersIfNeeded(path);
//        String[] rootFolderPaths = path.split(",");
//        List<RootFolderDto> rootFolderDtos = new ArrayList<>();
//        for (String rootFolderPath : rootFolderPaths) {
//            RootFolderDto rootFolder = createRootFolderIfNeeded(rootFolderPath);
//            rootFolderDtos.add(rootFolder);
//        }
//        return rootFolderDtos;
//    }

    public RootFolderDto createRootFolderIfNeeded(String rootFolderPath, CrawlerType crawlerType) {
        String normalizedPath = FileNamingUtils.normalizeFileSharePath(rootFolderPath);
        RootFolderDto rootFolderDto = rootFolderService.findRootFolder(normalizedPath,true);
        if (rootFolderDto == null) {
            //TODO vladi: bring back FileType.values() when ZIP is fixed
            List<FileType> fileTypes = Arrays.stream(FileType.values())
                    .filter(val -> val != FileType.PACKAGE)
                    .filter(val -> val != FileType.OTHER)
                    .collect(Collectors.toList());
//            rootFolder = fileTreeAppService.createFileShareRootFolder(rootFolderPath, FileType.values());
            logger.info("Create fileShare root folder {}",rootFolderPath);
            rootFolderDto = createFileShareRootFolder(rootFolderPath, crawlerType, fileTypes.toArray(new FileType[0]));
        }
        return rootFolderDto;
    }

    public RootFolderDto createFileShareRootFolder(String scanPath, CrawlerType crawlerType, FileType... fileTypes) {
        Path absolutePath = Paths.get(scanPath).toAbsolutePath().normalize();
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath(absolutePath.toString());
        rootFolderDto.setRescanActive(true);
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        rootFolderDto.setCrawlerType(crawlerType);
        rootFolderDto.setScannedFilesCountCap(1000);
        if (fileTypes.length > 0) {
            rootFolderDto.setFileTypes(fileTypes);
            rootFolderDto.setIngestFileTypes(fileTypes);
        }

        Long scheduledGroupId = scheduleGroupService.getScheduleGroupsNames().keySet().iterator().next();

        return rootFolderService.createRootFolder(rootFolderDto,scheduledGroupId);
    }

    private void tagAllFilesInRootFolder(RootFolderDto rootFolderDto, FileTagDto createdFileTagDto) {
        DocFolderDto docFolderDto = fileTreeAppService.findDocFolder(rootFolderDto.getId(), "/");
        if (docFolderDto == null) {
            logger.error("Failed to find the rootFolder base DocFolder. Problem scanning {}", rootFolderDto);
            throw new RuntimeException("Failed to find the rootFolder base DocFolder. Problem scanning " + rootFolderDto);
        }
        logger.info("Tag All files in folder {} (root: {}) using tag {}", docFolderDto, rootFolderDto.getPath(), createdFileTagDto);
        FileTagDto fileTag = fileTagService.getFromCache(createdFileTagDto.getId());
        fileTaggingAppService.addFileTagToFolder(docFolderDto.getId(), fileTag,null);
        scheduledOperationService.runOperations();
    }

    private FileTagDto createRiskFileTag(String tagName) {
        logger.info("Create file tag {} of Risk type", tagName);
        FileTagType fileTagType = fileTaggingAppService.getFileTagType(ASSET_TYPE);
        if (fileTagType == null) {
            throw new RuntimeException("Can't find tag Type " + ASSET_TYPE);
        }
        return fileTaggingAppService.createFileTag(tagName, "Tag created by PerformanceTestService", fileTagType, FileTagAction.MANUAL);
    }

    private void printSuiteStartBanner() {
        logger.info(":'######::'##::::'##:'####:'########:'########::'######::'########::::'###::::'########::'########:");
        logger.info("'##... ##: ##:::: ##:. ##::... ##..:: ##.....::'##... ##:... ##..::::'## ##::: ##.... ##:... ##..::");
        logger.info(".##:::..:: ##:::: ##:: ##::::: ##:::: ##::::::: ##:::..::::: ##:::::'##:. ##:: ##:::: ##:::: ##::::");
        logger.info(". ######:: ##:::: ##:: ##::::: ##:::: ######:::. ######::::: ##::::'##:::. ##: ########::::: ##::::");
        logger.info(":..... ##: ##:::: ##:: ##::::: ##:::: ##...:::::..... ##:::: ##:::: #########: ##.. ##:::::: ##::::");
        logger.info("'##::: ##: ##:::: ##:: ##::::: ##:::: ##:::::::'##::: ##:::: ##:::: ##.... ##: ##::. ##::::: ##::::");
        logger.info(". ######::. #######::'####:::: ##:::: ########:. ######::::: ##:::: ##:::: ##: ##:::. ##:::: ##::::");
        logger.info(":......::::.......:::....:::::..:::::........:::......::::::..:::::..:::::..::..:::::..:::::..:::::");
    }

    public void printRunBanner() {
        logger.info("  _ __ _   _ _ __");
        logger.info("  | '__| | | | '_ \\");
        logger.info("  | |  | |_| | | | |");
        logger.info("  |_|   \\__,_|_| |_|");
        logger.info("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
    }

    public RootFolderDto analyzeReferenceFilesVarious() {
        opStateService.setStopOnError(true);
        final String path = "." + SRC_TEST_RESOURCES_FILES_VARIOUS;

        RootFolderDto rootFolderDto = rootFolderService.findRootFolder(path,true);
        if (rootFolderDto != null) {
            //Root folder was already scanned before
            logger.info("Root folder {} was already scanned before", rootFolderDto.getPath());
            long count = filesApiController.countFilesDeNormalized(null);
            logger.info("There are now {} files in Solr", count);
            return rootFolderDto;
        }
        rootFolderDto = createRootFolderIfNeeded(path, CrawlerType.MEDIA_PROCESSOR);
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(), createWaitParams());
        printSuiteStartBanner();
        return rootFolderDto;
    }

    public FilterDescriptor createConcreteFilter(String field, Object value, String operator) {
        FilterDescriptor filter = new FilterDescriptor();
        filter.setField(field);
        filter.setValue(value);
        filter.setOperator(operator);
        return filter;
    }

    public FilterDescriptor createJoinFilter(String logic, FilterDescriptor... filterDescriptors) {
        FilterDescriptor joinFilter = new FilterDescriptor();
        joinFilter.setLogic(logic);
        for (FilterDescriptor filterDescriptor : filterDescriptors) {
            joinFilter.getFilters().add(filterDescriptor);
        }
        return joinFilter;
    }

    public AggregationCountFolderDto findAggregationCountFolderForRF(List<AggregationCountFolderDto> content, Long rfId) {
        for (AggregationCountFolderDto aggregationCountFolderDto : content) {
            if (aggregationCountFolderDto.getItem().getDepthFromRoot() == 0 && aggregationCountFolderDto.getItem().getRootFolderId().equals(rfId)) {
                return aggregationCountFolderDto;
            }
        }
        logger.warn("Failed to find AggregationCountFolderDto for root folder {}", rfId);
        return null;
    }

    public DocFolderDto findDocFolderDtoOfRootFolder(List<DocFolderDto> content, Long rfId) {
        String winPath = File.separator;
        for (DocFolderDto docFolderDto : content) {
            if (docFolderDto.getPathNoPrefix().equals(winPath) && docFolderDto.getRootFolderId().equals(rfId)) {
                return docFolderDto;
            }
        }
        logger.warn("Failed to find AggregationCountFolderDto for root folder {}", rfId);
        return null;
    }

    public CounterReportDto findCounterReport(List<CounterReportDto> counterReportDtos, String reportName) {
        for (CounterReportDto counterReportDto : counterReportDtos) {
            if (reportName.equals(counterReportDto.getName())) {
                return counterReportDto;
            }
        }
        return null;
    }

    public StringReportDto findStringReport(SummaryReportDto summaryReportDto, String reportName) {
        for (StringReportDto stringReportDto : summaryReportDto.getStringReportDtos()) {
            if (reportName.equals(stringReportDto.getName())) {
                return stringReportDto;
            }
        }
        return null;
    }

    public StringListReportDto findStringListReport(SummaryReportDto scanConfiguration, String reportName) {
        for (StringListReportDto stringListReportDto : scanConfiguration.getStringListReportDtos()) {
            if (reportName.equals(stringListReportDto.getName())) {
                return stringListReportDto;
            }
        }
        return null;
    }


    public ClaFileDto findFileDto(PageImpl<ClaFileDto> filesDeNormalized, long fileId) {
        for (ClaFileDto fileDto : filesDeNormalized.getContent()) {
            if (fileDto.getId() == fileId) {
                return fileDto;
            }
        }
        return null;
    }

    public FileTagDto findSensitiveFileTag() {
        List<FileTagTypeAndTagsDto> allFileTagTypesAndChildren = fileTagApiController.getAllFileTagTypesAndChildren(null);
        for (FileTagTypeAndTagsDto fileTagTypeAndTagsDto : allFileTagTypesAndChildren) {
            if (fileTagTypeAndTagsDto.getFileTagTypeDto().isSensitive()) {
                if (fileTagTypeAndTagsDto.getFileTags().size() > 0) {
                    return fileTagTypeAndTagsDto.getFileTags().get(0);
                }
            }
        }

        return null;
    }

    public Map<String, String> createFilterParams(String field, Object value) {
        Map<String, String> params = new HashMap<>();
        params.put(DataSourceRequest.BASE_FILTER_FIELD, field);
        params.put(DataSourceRequest.BASE_FILTER_VALUE, value.toString());
        return params;
    }


    public SharePointConnectionDetailsDto createSharePointConnectionDetails() {
        SharePointConnectionParametersDto connectionParametersDto = new SharePointConnectionParametersDto();
        connectionParametersDto.setPassword(SHARE_POINT_PASSWORD);
        connectionParametersDto.setDomain(SHARE_POINT_DOMAIN);
        connectionParametersDto.setUrl(SHARE_POINT_URL + SHARE_POINT_SITE);
        connectionParametersDto.setUsername(SHARE_POINT_USER);
        SharePointConnectionDetailsDto sharePointConnectionDetailsDto = new SharePointConnectionDetailsDto();
        sharePointConnectionDetailsDto.setName(SHARE_POINT_NAME);
        sharePointConnectionDetailsDto.setSharePointConnectionParametersDto(connectionParametersDto);
        return sharePointConnectionDetailsDto;
    }

    public SharePointConnectionDetailsDto createSharePoint365ConnectionDetails() {
        SharePointConnectionParametersDto connectionParametersDto = new SharePointConnectionParametersDto();
        connectionParametersDto.setPassword(SHARE_POINT_365_PASSWORD);
        connectionParametersDto.setDomain(null);
        connectionParametersDto.setUrl(SHARE_POINT_365_URL + SHARE_POINT_365_SITE);
        connectionParametersDto.setUsername(SHARE_POINT_365_USER + "@" + SHARE_POINT_365_DOMAIN);
        SharePointConnectionDetailsDto sharePointConnectionDetailsDto = new SharePointConnectionDetailsDto();
        sharePointConnectionDetailsDto.setSharePointConnectionParametersDto(connectionParametersDto);
        return sharePointConnectionDetailsDto;
    }

    public SharePointConnectionDetailsDto createSharePoint365MainSiteConnectionDetails() {
        SharePointConnectionParametersDto connectionParametersDto = new SharePointConnectionParametersDto();
        connectionParametersDto.setPassword(SHARE_POINT_365_PASSWORD);
        connectionParametersDto.setDomain(null);
        connectionParametersDto.setUrl(SHARE_POINT_365_URL);
        connectionParametersDto.setUsername(SHARE_POINT_365_USER + "@" + SHARE_POINT_365_DOMAIN);
        SharePointConnectionDetailsDto sharePointConnectionDetailsDto = new SharePointConnectionDetailsDto();
        sharePointConnectionDetailsDto.setSharePointConnectionParametersDto(connectionParametersDto);
        return sharePointConnectionDetailsDto;
    }

    public Map<String,String> createWaitParams() {
        Map<String,String> params = new HashMap<>();
        params.put("op", StartScanProducer.SCAN_INGEST_ANALYZE);
        params.put("wait", "true");
        return params;
    }

}
