package com.cla.filecluster.test.service;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by uri on 13-Apr-17.
 */
@Service
public class TestSqlService {

    Logger logger = LoggerFactory.getLogger(TestSqlService.class);
    private static final String TEST_BASELINE_SCRIPT = "./src/test/resources/test_baseline.sql";


    @Autowired
    private EntityManager entityManager;

    @Transactional
    public void runPostBaseLine() {
        File scriptFile = new File(TEST_BASELINE_SCRIPT);
        if (!scriptFile.exists()) {
            throw new RuntimeException("Missing baseline script "+scriptFile.getAbsolutePath());
        }
        logger.warn("Run PostBaseLine at {}",scriptFile.getAbsolutePath());
        Session session = entityManager.unwrap(Session.class);
        logger.debug("Using session {}",session);
        session.doWork(new Work() {
            @Override
            public void execute(Connection connection) throws SQLException {
                logger.warn("Run PostBaseLine in connection");
                if (connection.isClosed()) {
                    logger.error("Can't execute - connection closed");
                }
                ScriptRunner scriptRunner = new ScriptRunner(connection,false,false);
                try {
                    logger.debug("Run the script at {}",scriptFile);
                    FileReader reader = new FileReader(scriptFile);
                    scriptRunner.runScript(reader);
                } catch (Exception e) {
                    logger.error("Failed to execute test baseline script",e);
                }
            }
        });
    }


}
