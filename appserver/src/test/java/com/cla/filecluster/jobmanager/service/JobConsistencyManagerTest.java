package com.cla.filecluster.jobmanager.service;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.mediaproc.AsyncIngestProducerService;
import com.cla.filecluster.mediaproc.BizListExtractionManager;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.analyze.AnalyzeFinalizeService;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.crawler.scan.ScanFinalizeService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class JobConsistencyManagerTest {

    @Mock
    private JobManagerService jobManager;

    @Mock
    private  JobCoordinator jobCoordinator;

    @Mock
    private DBTemplateUtils dbTemplateUtils;

    @Mock
    private FileCrawlerExecutionDetailsService runService;

    @Mock
    private MediaProcessorCommService mediaProcessorCommService;

    @Mock
    private AsyncIngestProducerService asyncIngestProducerService;

    @Mock
    private IngestFinalizationService ingestFinalizationService;

    @Mock
    private StartScanProducer startScanProducer;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private ScanFinalizeService scanFinalizeService;

    @Mock
    private AnalyzeFinalizeService analyzeFinalizeService;

    @Mock
    private BizListExtractionManager bizListExtractionManager;

    @Mock
    private JobManagerAppService jobManagerAppService;

    @InjectMocks
    private JobConsistencyManager jobConsistencyManager;

    @Before
    public void init() {
        ReflectionTestUtils.setField(jobConsistencyManager, "inProgressJobTimeoutSec", 3600);
        ReflectionTestUtils.setField(jobConsistencyManager, "enqueuedTaskTimeoutSec", 600);
        ReflectionTestUtils.setField(jobConsistencyManager, "handleRestartDelayMs", 60000);
        ReflectionTestUtils.setField(jobConsistencyManager, "maxTaskRetries", 3);
        ReflectionTestUtils.setField(jobConsistencyManager, "pausedJobsUpdatedThresholdSec", 60);
        ReflectionTestUtils.setField(jobConsistencyManager, "isToolExecutionMode", false);
        ReflectionTestUtils.setField(jobConsistencyManager, "debugCachedJobsMap", false);
    }

    @After
    public void resetMocks() {
        Mockito.reset(jobManager);
        Mockito.reset(runService);
        Mockito.reset(startScanProducer);
        Mockito.reset(docStoreService);
        Mockito.reset(scanFinalizeService);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testPauseRun() {
        CrawlRun crawlRun = new CrawlRun(); crawlRun.setHasChildRuns(true); crawlRun.setId(7L);
        when(runService.getCrawlRun(7L)).thenReturn(crawlRun);
        List<CrawlRun> childCrawlRuns = new ArrayList<>();
        CrawlRun crawlRunChild = new CrawlRun(); crawlRunChild.setId(8L); crawlRunChild.setHasChildRuns(false);
        childCrawlRuns.add(crawlRunChild);
        when(runService.getCrawlRun(8L)).thenReturn(crawlRunChild);
        when(runService.getChildCrawlRuns(7L)).thenReturn(childCrawlRuns);

        doAnswer(invocation -> {
            ((Runnable)invocation.getArguments()[0]).run();
            return null;
        }).when(dbTemplateUtils).doInTransaction(any(Runnable.class), any(Integer.class));

        jobConsistencyManager.pauseRun(7L, PauseReason.USER_INITIATED);
        verify(runService, times(1)).handleFinishedRun(8L, PauseReason.USER_INITIATED);
    }

    @Test
    public void testResumeRun() {
        CrawlRun crawlRun = new CrawlRun();
        crawlRun.setHasChildRuns(false);
        crawlRun.setRunStatus(RunStatus.PAUSED);
        when(runService.getCrawlRun(7L)).thenReturn(crawlRun);
        List<SimpleJob> runJobs = new ArrayList<>();
        SimpleJob sj = new SimpleJob(); sj.setState(JobState.IN_PROGRESS); sj.setId(7L);
        runJobs.add(sj);
        when(jobManager.getRunJobsObjects(7L)).thenReturn(runJobs);
        jobConsistencyManager.resumeRun(7L);
        verify(runService, times(1)).handleFinishedRun(7L, PauseReason.SYSTEM_INITIATED);
    }

    @Test
    public void testPauseAnalyze() {
        List<SimpleJob> startedJobs = new ArrayList<>();
        SimpleJob job = new SimpleJob();
        job.setId(5L); job.setType(JobType.ANALYZE); job.setRunContext(3L);
        startedJobs.add(job);

        when(jobManager.getIdleJobs(anyLong(), anyCollection())).thenReturn(startedJobs);
        jobConsistencyManager.pauseAllJobs(PauseReason.USER_INITIATED);
        verify(jobManager, times(1)).pauseJob(5L, 3L,"", false);

        when(jobManager.getJobIdCached(5L, JobType.ANALYZE)).thenReturn(5L);
        assertFalse(jobConsistencyManager.isJobPaused(5L, JobType.ANALYZE));
    }

    @Test
    public void testPauseScan() {
        List<SimpleJob> startedJobs = new ArrayList<>();
        SimpleJob job = new SimpleJob();
        job.setId(6L); job.setType(JobType.SCAN); job.setRunContext(3L);
        startedJobs.add(job);

        when(jobManager.getIdleJobs(anyLong(), anyCollection())).thenReturn(startedJobs);
        jobConsistencyManager.pauseAllJobs(PauseReason.USER_INITIATED);
        verify(jobManager, times(1)).pauseJob(6L, 3L, "", false);

        when(jobManager.getJobIdCached(6L, JobType.SCAN)).thenReturn(6L);
        assertTrue(jobConsistencyManager.isJobPaused(6L, JobType.SCAN));
    }


    @Test
    public void testStopPausedRun() {
        CrawlRun crawlRun = new CrawlRun(); crawlRun.setHasChildRuns(false); crawlRun.setRunStatus(RunStatus.PAUSED);
        when(runService.getCrawlRun(8L)).thenReturn(crawlRun);
        List<SimpleJobDto> runJobs = new ArrayList<>();
        SimpleJobDto sj = new SimpleJobDto(); sj.setState(JobState.IN_PROGRESS); sj.setId(8L);
        runJobs.add(sj);
        when(jobManager.getRunJobs(8L)).thenReturn(runJobs);
        jobConsistencyManager.stopPausedRun(8L, PauseReason.USER_INITIATED);
        verify(jobManager, times(1)).updateJobState(8L, JobState.DONE, JobCompletionStatus.STOPPED, null, true);
        verify(runService, times(1)).handleFinishedRun(8L, PauseReason.USER_INITIATED);
    }
}
