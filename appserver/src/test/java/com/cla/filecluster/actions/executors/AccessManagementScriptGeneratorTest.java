package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
@Category(UnitTestCategory.class)
public class AccessManagementScriptGeneratorTest {

    @Test
    public void testRemovePermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.removePermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Remove User ff Permission [W] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testAddPermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.addPermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant User ff Permission [W] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testSetFullControlPermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.setFullControlPermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant Only User ff Permission [F] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testSetWritePermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.setWritePermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant Only User ff Permission [W] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testSetReadPermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.setReadPermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant Only User ff Permission [R] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testAddFullControlPermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.addFullControlPermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant User ff Permission [F] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testAddWritePermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.addWritePermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant User ff Permission [W] on /goo"};
        testResult(linesToTest);
    }

    @Test
    public void testAddReadPermission() throws Exception {
        AccessManagementScriptGenerator instance = getInstance();
        instance.addReadPermission(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "echo Permissions additions","echo Grant User ff Permission [R] on /goo"};
        testResult(linesToTest);
    }

    private Properties getProperties() {
        Properties p = new Properties();
        p.setProperty("users", "[ff]");
        return p;
    }

    private BaseActionParams getBaseActionParams() {
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/goo");
        return actions;
    }

    private void testResult(String[] linesToTest) throws Exception {
        Path fileCreated = Paths.get("./temp/actions/access_management_script_0005_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        for (String line : linesToTest) {
            assertTrue(lines.contains(line));
        }
        Files.delete(fileCreated);
    }

    private AccessManagementScriptGenerator getInstance() {
        ActionHelper actionHelper = new TestActionHelper();
        AccessManagementScriptGenerator instance = new AccessManagementScriptGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        return instance;
    }
}
