package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
@Category(UnitTestCategory.class)
public class ReportingCsvGeneratorTest {

    @Test
    public void test() throws Exception {
        ActionHelper actionHelper = new TestActionHelper();
        ReportingCsvGenerator instance = new ReportingCsvGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/goo");
        actions.setClaFileId(6L);
        Properties p = new Properties();
        instance.reportFile(actions, p);
        instance.apply();
        Path fileCreated = Paths.get("./temp/actions/file_report.csv");
        List<String> lines = Files.readAllLines(fileCreated);
        assertTrue(lines.contains("6,/goo"));
        Files.delete(fileCreated);
    }
}
