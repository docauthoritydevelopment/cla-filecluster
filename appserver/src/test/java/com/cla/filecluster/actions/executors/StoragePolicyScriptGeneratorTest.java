package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.common.domain.dto.action.DispatchActionRequestDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.filecluster.actions.base.DocAuthorityAction;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.actions.ActionClassExecutorDefinition;
import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.domain.entity.actions.FileActionDispatch;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.repository.jpa.actions.ActionClassExecutorDefinitionRepository;
import com.cla.filecluster.repository.jpa.actions.ActionDefinitionRepository;
import com.cla.filecluster.repository.jpa.actions.ActionExecutionStatusRepository;
import com.cla.filecluster.service.actions.*;
import com.cla.filecluster.service.api.actions.ActionsApiController;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Maps;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.Silent.class)
public class StoragePolicyScriptGeneratorTest {

    private static final String SHAREPOINT_URL = "https://docauthority.sharepoint.com";

    @InjectMocks
    private ActionsAppService actionsAppService;

    @Mock
    private ActionExecutionStatusRepository actionExecutionStatusRepository;

    @InjectMocks
    private ActionsApiController actionsApiController;

    @Mock
    private ActionDefinitionService actionDefinitionService;

    @InjectMocks
    private FileActionsEngineService actionsEngineService;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private AssociationsService associationsService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private GroupsService groupsService;

    @Mock
    private UserService userService;

    @Mock
    private ActionDefinitionRepository actionDefinitionRepository;

    @InjectMocks
    private ActionExecutionStatusService actionExecutionStatusService;

    @Mock
    private ActionClassExecutorService actionClassExecutorService;

    @Mock
    private ActionClassExecutorDefinitionRepository actionClassExecutorDefinitionRepository;

    private static final String LOCAL_DIR = "D:\\temp";

    private static final String TESTS_FOLDER = "./temp/actions/Tests";

    private static volatile long TRIGGER_ID_COUNT = 0;

    //@BeforeClass
    public static void initClass() throws IOException {
        Path testDir = Paths.get(LOCAL_DIR);
        if (!Files.exists(testDir)) {
            Files.createDirectory(testDir);
        }
    }

    @Before
    public void init() {
        actionsApiController = new ActionsApiController();
        ReflectionTestUtils.setField(actionsApiController, "actionsAppService", actionsAppService);

        ReflectionTestUtils.setField(actionsAppService, "fileActionsEngineService", actionsEngineService);
        ReflectionTestUtils.setField(actionsAppService, "actionResultsFolder", TESTS_FOLDER);
        ReflectionTestUtils.setField(actionsAppService, "actionExecutionStatusService", actionExecutionStatusService);
        ReflectionTestUtils.setField(actionsAppService, "docStoreService", docStoreService);
        ReflectionTestUtils.setField(actionsAppService, "docFolderService", docFolderService);

        ReflectionTestUtils.setField(actionDefinitionService, "actionDefinitionRepository", actionDefinitionRepository);
        ReflectionTestUtils.setField(actionExecutionStatusService, "actionExecutionStatusRepository", actionExecutionStatusRepository);
        ReflectionTestUtils.setField(actionClassExecutorService, "actionClassExecutorDefinitionRepository", actionClassExecutorDefinitionRepository);

        when(associationsService.getAssociationRelatedData(any(ClaFile.class))).thenReturn(
                new AssociationRelatedData(new ArrayList<>(), new ArrayList<>(), new HashMap<>()));
        when(associationsService.getAssociationRelatedData(any(Collection.class))).thenReturn(
                new AssociationRelatedData(new ArrayList<>(), new ArrayList<>(), new HashMap<>(), new HashMap<>()));
    }

    //    @After
    public void cleanUp() throws IOException {
        Files.list(Paths.get(TESTS_FOLDER))
                .filter(item -> !Files.isDirectory(item))
                .forEach(file -> {
                    try {
                        Files.delete(file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Test
    public void testCopy() throws Exception {
        ActionHelper actionHelper = new TestActionHelper();
        StoragePolicyScriptGenerator instance = new StoragePolicyScriptGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/");
        Properties p = new Properties();
        p.setProperty("target", "ddd");
        p.setProperty(DocAuthorityAction.ROOT_PATH, "/boo");
        p.setProperty(DocAuthorityAction.RELATIVE_PATH, "ff");
        p.setProperty(DocAuthorityAction.BASE_NAME, "rfr");
        instance.copyFile(actions, p);
        instance.apply();
        Path fileCreated = Paths.get("./temp/actions/storagePolicyScript_0005_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated)
                .stream().map(String::trim).collect(Collectors.toList());
        assertTrue(lines.contains("REM Single file copy"));
        assertTrue(lines.contains("%COPYCMD% \"rfr\" \"/boo\\ff\\\" \"ddd\\_boo\\ff\\\""));
        Files.delete(fileCreated);
    }

    @Test
    public void testMove() throws Exception {
        ActionHelper actionHelper = new TestActionHelper();
        StoragePolicyScriptGenerator instance = new StoragePolicyScriptGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/");
        Properties p = new Properties();
        p.setProperty("target", "ddd");
        p.setProperty(DocAuthorityAction.ROOT_PATH, "/boo");
        p.setProperty(DocAuthorityAction.RELATIVE_PATH, "ff");
        p.setProperty(DocAuthorityAction.BASE_NAME, "rfr");
        instance.moveFile(actions, p);
        instance.apply();
        Path fileCreated = Paths.get("./temp/actions/storagePolicyScript_0005_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated)
                .stream().map(String::trim).collect(Collectors.toList());
        assertTrue(lines.contains("REM Single file copy"));
        assertTrue(lines.contains("%MOVECMD% \"rfr\" \"/boo\\ff\\\" \"ddd\\_boo\\ff\\\""));
        Files.delete(fileCreated);
    }

    private ActionDefinition getAndPrepareActionDefinition(ActionClass actionClass, String actionId, String actionJavaMethodName) {
        ActionDefinition actionDefinition = new ActionDefinition();
        actionDefinition.setActionClass(actionClass);
        actionDefinition.setId(actionId);
        actionDefinition.setJavaMethodName(actionJavaMethodName);
        when(actionDefinitionService.findActionDefinition(actionDefinition.getId())).thenReturn(actionDefinition);
        when(actionDefinitionRepository.getOne(actionDefinition.getId())).thenReturn(actionDefinition);
        return actionDefinition;
    }

    private ActionDefinition getAndPrepareCopyActionDefinition(ActionClass actionClass) {
        return getAndPrepareActionDefinition(actionClass, "COPY_FILE", "copyFile");
    }

    private ActionDefinition getAndPrepareMoveActionDefinition(ActionClass actionClass) {
        return getAndPrepareActionDefinition(actionClass, "MOVE_FILE", "moveFile");
    }

    private ActionClassExecutorDefinition getAndPrepareActionClassExecutorDefinition(ActionDefinition actionDefinition) {
        ActionClassExecutorDefinition aced = new ActionClassExecutorDefinition();
        aced.setActionClass(actionDefinition.getActionClass());
        aced.setId("STORAGE_POLICY_SCRIPT_GENERATOR");
        aced.setJavaClassName("com.cla.filecluster.actions.executors.StoragePolicyScriptGenerator");
        aced.setName("Storage Policy Script Generator");
        when(actionClassExecutorService.getDefaultActionClassExecutorDefinition(actionDefinition.getActionClass())).thenCallRealMethod();
        return aced;
    }

    private ActionExecutionTaskStatus getAndPrepareActionExecutionTaskStatus() {
        ActionExecutionTaskStatus status = new ActionExecutionTaskStatus();
        status.setId(TRIGGER_ID_COUNT++);
        when(actionExecutionStatusRepository.save(any(ActionExecutionTaskStatus.class))).thenReturn(status);
        when(actionExecutionStatusRepository.findById(status.getId())).thenReturn(Optional.of(status));

        return status;
    }

    private DispatchActionRequestDto getDispatchActionRequestDto(ActionDefinition actionDefinition, String targetDir) {
        DispatchActionRequestDto dto = new DispatchActionRequestDto();
        dto.setActionDefinitionId(actionDefinition.getId());
        Properties prop = new Properties();
        prop.setProperty("target", targetDir);
        dto.setParameters(prop);

        return dto;
    }

    @Test
    @Ignore
    public void testDownloadFile() throws Exception {
        ActionDefinition actionDefinition = getAndPrepareCopyActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("home_depot_darwin.docx", "ENRONGAS(1200).xlsx")
                .setFolderMediaType(MediaType.SHARE_POINT)
                .setUrl("https://docauthority.sharepoint.com")
                .setPath("/Shared Documents/OBY0");

        RootFolder rf = builder.getRootFolder();

        List<ClaFile> fileCollection = builder.build(rf);

        List<SolrFileEntity> fileEntityCollection = builder.buildEntities(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(ClaFile::getId)
                .collect(Collectors.toList());
        when(fileCatService.findByFileIds(fileIds)).thenReturn(fileEntityCollection);
        when(fileCatService.fromEntities(fileEntityCollection)).thenReturn(fileCollection);

        when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));

        ArgumentCaptor<FileActionDispatch> fileActionDispatchArgumentCaptor = ArgumentCaptor.forClass(FileActionDispatch.class);


        PageRequest pageRequest = PageRequest.of(0, FileActionsEngineService.ACTION_PAGE_SIZE);


        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining(",")));

        actionsApiController.executeActionOnFilter(params, getDispatchActionRequestDto(actionDefinition, LOCAL_DIR));

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url \"https://docauthority.sharepoint.com\"") > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.DOWNLOAD_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + LOCAL_DIR + "\"") > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);

        fileCollection
                .stream()
                .map(file -> file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }

    @Test
    @Ignore
    public void testUploadFile() throws Exception {
        String targetPath = "/Shared Documents/Actions target";
        ActionDefinition actionDefinition = getAndPrepareCopyActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("home_depot_darwin - Copy.docx", "ENRONGAS(1200) - Copy.xlsx")
                .setPath(LOCAL_DIR)
                .setFolderMediaType(MediaType.FILE_SHARE);

        RootFolder rf = builder.getRootFolder();

        List<ClaFile> fileCollection = builder.build(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(fc -> fc.getId())
                .collect(Collectors.toList());
        when(fileCatService.findByFilesIds(fileIds)).thenReturn(fileCollection);

        when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));

        ArgumentCaptor<FileActionDispatch> fileActionDispatchArgumentCaptor = ArgumentCaptor.forClass(FileActionDispatch.class);


        PageRequest pageRequest = new PageRequest(0, FileActionsEngineService.ACTION_PAGE_SIZE);


        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(id -> id.toString())
                        .collect(Collectors.joining(",")));

        DispatchActionRequestDto dto = getDispatchActionRequestDto(actionDefinition, SHAREPOINT_URL + targetPath);
        dto.getParameters().setProperty(FileActionsEngineService.TARGET_TYPE, MediaType.SHARE_POINT.name());
        actionsApiController.executeActionOnFilter(params, dto);

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        String target = "\"https://docauthority.sharepoint.com\"";
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url " + target) > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.UPLOAD_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + targetPath + "\"") > -1);
        fileCollection
                .stream()
                .map(file -> file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }

    @Test
    @Ignore
    public void testSharePointRemoteCopyFile() throws Exception {
        String targetPath = "/Shared Documents/Actions target";
        ActionDefinition actionDefinition = getAndPrepareCopyActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        RootFolder rf = new RootFolder();
        rf.setId(3333L);
        rf.setMediaType(MediaType.SHARE_POINT);
        rf.setMediaConnectionDetails(new MediaConnectionDetails());
        rf.getMediaConnectionDetails().setUrl("https://docauthority.sharepoint.com");
        rf.setRealPath("https://docauthority.sharepoint.com/Shared Documents/Actions target/src");
        when(docStoreService.getRootFolderCached(nullable(Long.class))).thenReturn(rf);

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("IFERCFeb.xlsx", "IFERCJan.xlsx", "IFERCnov.xlsx")
                .setUrl("https://docauthority.sharepoint.com")
                .setPath("/Shared Documents/Actions target/src")
                .setFolderMediaType(MediaType.SHARE_POINT);

        List<ClaFile> fileCollection = builder.build(rf);

        List<SolrFileEntity> fileEntityCollection = builder.buildEntities(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(ClaFile::getId)
                .collect(Collectors.toList());
        when(fileCatService.findByFileIds(fileIds)).thenReturn(fileEntityCollection);
        when(fileCatService.fromEntities(fileEntityCollection)).thenReturn(fileCollection);

        when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();
        when(docStoreService.getRootFoldersCached(any())).thenReturn(Lists.newArrayList(rf));

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));

        List<FileActionDispatch> savedFileActionsDispatches = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(0, FileActionsEngineService.ACTION_PAGE_SIZE);


        DocFolder docFolder = new DocFolder();
        docFolder.setId(66L);
        docFolder.setRealPath(rf.getRealPath());
        docFolder.setRootFolderId(rf.getId());
        when(docFolderService.findByIds(any())).thenReturn(Lists.newArrayList(docFolder));

        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(id -> id.toString())
                        .collect(Collectors.joining(",")));

        DispatchActionRequestDto dto = getDispatchActionRequestDto(actionDefinition, SHAREPOINT_URL + "/Shared Documents/Actions target");
        dto.getParameters().setProperty(FileActionsEngineService.TARGET_TYPE, MediaType.SHARE_POINT.name());
        actionsApiController.executeActionOnFilter(params, dto);

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        String target = "\"https://docauthority.sharepoint.com\"";
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url " + target) > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.COPY_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + targetPath + "\"") > -1);
        fileCollection
                .stream()
                .map(file -> rf.getRealPath() + file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }

    @Test
    @Ignore
    public void testOneDriveRemoteCopyFile() throws Exception {
        String targetPath = "/Shared Documents/Actions target";
        ActionDefinition actionDefinition = getAndPrepareCopyActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("IFERCFeb.xlsx", "IFERCJan.xlsx", "IFERCnov.xlsx")
                .setUrl("https://docauthority.sharepoint.com")
                .setPath("/Shared Documents/Actions target/src")
                .setFolderMediaType(MediaType.ONE_DRIVE);

        RootFolder rf = builder.getRootFolder();

        List<ClaFile> fileCollection = builder.build(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(ClaFile::getId)
                .collect(Collectors.toList());
        when(fileCatService.findByFilesIds(fileIds)).thenReturn(fileCollection);

        when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));

        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(id -> id.toString())
                        .collect(Collectors.joining(",")));

        DispatchActionRequestDto dto = getDispatchActionRequestDto(actionDefinition, SHAREPOINT_URL + "/Shared Documents/Actions target");
        dto.getParameters().setProperty(FileActionsEngineService.TARGET_TYPE, MediaType.SHARE_POINT.name());
        actionsApiController.executeActionOnFilter(params, dto);

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        String target = "\"https://docauthority.sharepoint.com\"";
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url " + target) > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.COPY_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + targetPath + "\"") > -1);
        fileCollection
                .stream()
                .map(file -> file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }

    @Test
    @Ignore
    public void testMoveFromRemote() throws Exception {
        ActionDefinition actionDefinition = getAndPrepareMoveActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("home_depot_darwin.docx", "ENRONGAS(1200).xlsx")
                .setFolderMediaType(MediaType.SHARE_POINT)
                .setUrl("https://docauthority.sharepoint.com")
                .setPath("/Shared Documents/OBY0");

        RootFolder rf = builder.getRootFolder();

        List<ClaFile> fileCollection = builder.build(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(ClaFile::getId)
                .collect(Collectors.toList());
        when(fileCatService.findByFilesIds(fileIds)).thenReturn(fileCollection);
         when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));

        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(Object::toString)
                        .collect(Collectors.joining(",")));

        actionsApiController.executeActionOnFilter(params, getDispatchActionRequestDto(actionDefinition, LOCAL_DIR));

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url \"https://docauthority.sharepoint.com\"") > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.MOVE_DOWNLOAD_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + LOCAL_DIR + "\"") > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);

        fileCollection
                .stream()
                .map(file -> file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }

    @Test
    @Ignore
    public void testMoveToRemote() throws Exception {
        String targetPath = "/Shared Documents/Actions target";
        ActionDefinition actionDefinition = getAndPrepareMoveActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("home_depot_darwin - Copy.docx", "ENRONGAS(1200) - Copy.xlsx")
                .setPath(LOCAL_DIR)
                .setFolderMediaType(MediaType.FILE_SHARE);

        RootFolder rf = builder.getRootFolder();

        List<ClaFile> fileCollection = builder.build(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(fc -> fc.getId())
                .collect(Collectors.toList());
        when(fileCatService.findByFilesIds(fileIds)).thenReturn(fileCollection);
        when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));

        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(id -> id.toString())
                        .collect(Collectors.joining(",")));

        DispatchActionRequestDto dto = getDispatchActionRequestDto(actionDefinition, SHAREPOINT_URL + targetPath);
        dto.getParameters().setProperty(FileActionsEngineService.TARGET_TYPE, MediaType.SHARE_POINT.name());
        actionsApiController.executeActionOnFilter(params, dto);

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        String target = "\"https://docauthority.sharepoint.com\"";
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url " + target) > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.MOVE_UPLOAD_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + targetPath + "\"") > -1);
        fileCollection
                .stream()
                .map(file -> file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }

    @Test
    @Ignore
    public void testRemoteMoveFile() throws Exception {
        String targetPath = "/Shared Documents/Actions target";
        ActionDefinition actionDefinition = getAndPrepareMoveActionDefinition(ActionClass.STORAGE_POLICY);

        UserDto user = new UserDto();
        user.setId(1111L);
        when(userService.getCurrentUser()).thenReturn(user);
        ActionExecutionTaskStatus status = getAndPrepareActionExecutionTaskStatus();

        RootFolder rf = new RootFolder();
        rf.setId(3333L);
        rf.setMediaType(MediaType.SHARE_POINT);
        rf.setMediaConnectionDetails(new MediaConnectionDetails());
        rf.getMediaConnectionDetails().setUrl("https://docauthority.sharepoint.com");
        rf.setRealPath("https://docauthority.sharepoint.com/Shared Documents/Actions target/src");
        when(docStoreService.getRootFolderCached(any())).thenReturn(rf);

        ClaFileCollectionBuilder builder = ClaFileCollectionBuilder.create()
                .setFileNames("IFERCFeb.xlsx", "IFERCJan.xlsx", "IFERCnov.xlsx")
                .setUrl("https://docauthority.sharepoint.com")
                .setPath("/Shared Documents/Actions target/src")
                .setFolderMediaType(MediaType.SHARE_POINT);

        List<ClaFile> fileCollection = builder.build(rf);

        List<SolrFileEntity> fileEntityCollection = builder.buildEntities(rf);

        List<Long> fileIds = fileCollection.stream()
                .map(ClaFile::getId)
                .collect(Collectors.toList());
        when(fileCatService.findByFileIds(fileIds)).thenReturn(fileEntityCollection);
        when(fileCatService.fromEntities(fileEntityCollection)).thenReturn(fileCollection);
        when(actionClassExecutorService.createActionClassExecutorInstance(eq(actionDefinition.getActionClass()), any(ActionHelper.class))).thenCallRealMethod();

        ActionClassExecutorDefinition aced = getAndPrepareActionClassExecutorDefinition(actionDefinition);
        when(actionClassExecutorDefinitionRepository.findByActionClass(actionDefinition.getActionClass())).thenReturn(Lists.newArrayList(aced));
        when(docStoreService.getRootFoldersCached(any())).thenReturn(Lists.newArrayList(rf));

        Map<String, String> params = Maps.newHashMap();
        params.put("fileId",
                fileIds.stream()
                        .map(id -> id.toString())
                        .collect(Collectors.joining(",")));

        DocFolder docFolder = new DocFolder();
        docFolder.setId(66L);
        docFolder.setRealPath(rf.getRealPath());
        docFolder.setRootFolderId(rf.getId());
        when(docFolderService.findByIds(any())).thenReturn(Lists.newArrayList(docFolder));

        DispatchActionRequestDto dto = new DispatchActionRequestDto();
        dto.setActionDefinitionId(actionDefinition.getId());
        Properties prop = new Properties();
        prop.setProperty("target", SHAREPOINT_URL + "/Shared Documents/Actions target");
        prop.setProperty(FileActionsEngineService.TARGET_TYPE, MediaType.SHARE_POINT.name());
        dto.setParameters(prop);
        actionsApiController.executeActionOnFilter(params, dto);

        // Validations
        Path fileCreated = Paths.get(TESTS_FOLDER, "storagePolicyScript_" + String.format("%04d", status.getId()) + "_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        List<String> uploadCmds = lines.stream()
                .map(String::trim)
                .filter(line -> line.startsWith("%COPY_EXT_CMD%"))
                .collect(Collectors.toList());
        Assert.assertEquals(1, uploadCmds.size());
        String cmd = uploadCmds.get(0);
        String target = "\"https://docauthority.sharepoint.com\"";
        Assert.assertTrue(cmd.indexOf("--type SHARE_POINT") > -1);
        Assert.assertTrue(cmd.indexOf("--url " + target) > -1);
        Assert.assertTrue(cmd.indexOf("--action " + StoragePolicyScriptGenerator.MOVE_OPERATION) > -1);
        Assert.assertTrue(cmd.indexOf("--username") > -1);
        Assert.assertTrue(cmd.indexOf("--password") > -1);
        Assert.assertTrue(cmd.indexOf("--target \"" + targetPath + "\"") > -1);
        fileCollection
                .stream()
                .map(file -> rf.getRealPath() + file.getFullPath() + file.getFileName())
                .forEach(file -> Assert.assertTrue(cmd.indexOf("--src \"" + file + "\"") > -1));
    }


    static class ClaFileCollectionBuilder {

        private MediaType mediaType;

        private String url;

        private String fileNames[];

        private String path;

        public static ClaFileCollectionBuilder create() {
            return new ClaFileCollectionBuilder();
        }

        public ClaFileCollectionBuilder setFileNames(String... fileNames) {
            this.fileNames = fileNames;
            return this;
        }

        public ClaFileCollectionBuilder setFolderMediaType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public ClaFileCollectionBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        public ClaFileCollectionBuilder setPath(String path) {
            this.path = path;
            return this;
        }

        public RootFolder getRootFolder() {
            RootFolder root = new RootFolder();
            if (url != null) {
                MediaConnectionDetails details = new MediaConnectionDetails();
                details.setUrl(url);
                root.setMediaConnectionDetails(details);
            } else {
                url = "";
            }
            root.setRealPath(url + path);
            root.setMediaType(mediaType);
            return root;
        }

        public List<ClaFile> build(RootFolder root) {
            Random rand = new SecureRandom();
            List<ClaFile> output = Lists.newArrayList();
            for (String fileName : fileNames) {
                ClaFile file = new ClaFile(fileName, resolveFileType(fileName));
                file.setId(rand.nextLong());
                file.setFullPathProperties(root.getRealPath() + "/" + file.getFileName());
                file.setBaseName(fileName);
                file.setRootFolderId(3333L);
                file.setDocFolderId(66L);
                output.add(file);
            }
            return output;
        }

        private List<SolrFileEntity> buildEntities(RootFolder root) {
            Random rand = new SecureRandom();
            List<SolrFileEntity> output = Lists.newArrayList();
            for (String fileName : fileNames) {
                SolrFileEntity file = new SolrFileEntity();
                file.setFileId(rand.nextLong());
                file.setType(resolveFileType(fileName).toInt());
                file.setFullName(root.getRealPath() + "/" + fileName);
                file.setFullPath(root.getRealPath());
                file.setBaseName(fileName);
                file.setFolderName("");
                file.setSortName(FileCatUtils.calcSortName(fileName));
                file.setRootFolderId(3333L);
                file.setFolderId(66L);
                output.add(file);
            }
            return output;
        }

        private FileType resolveFileType(String filename) {
            if (filename.matches(".*\\.doc[x]{0,1}")) {
                return FileType.WORD;
            } else if (filename.matches(".*\\.xls[x]{0,1}")) {
                return FileType.EXCEL;
            } else if (filename.matches(".*\\.pdf")) {
                return FileType.PDF;
            }

            return FileType.OTHER;
        }
    }
}
