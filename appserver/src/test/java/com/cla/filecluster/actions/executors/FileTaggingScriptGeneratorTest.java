package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
@Category(UnitTestCategory.class)
public class FileTaggingScriptGeneratorTest {

    @Test
    public void testRemPropertyValue() throws Exception {
        FileTaggingScriptGenerator instance = getInstance();
        instance.remPropertyValue(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "ECHO tag files...", "     ECHO Skip rem tags  on property ff to file \"/goo\""};
        testResult(linesToTest);
    }

    @Test
    public void testSetPropertyValue() throws Exception {
        FileTaggingScriptGenerator instance = getInstance();
        instance.setPropertyValues(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "ECHO tag files...", "     ECHO Skip set tags  on property ff to file \"/goo\""};
        testResult(linesToTest);
    }

    @Test
    public void testAddPropertyValue() throws Exception {
        FileTaggingScriptGenerator instance = getInstance();
        instance.addPropertyValue(getBaseActionParams(), getProperties());
        instance.apply();
        String[] linesToTest = {
                "ECHO tag files...", "     ECHO Skip add tags  on property ff to file \"/goo\""};
        testResult(linesToTest);
    }

    private Properties getProperties() {
        Properties p = new Properties();
        p.setProperty("property", "ff");
        return p;
    }

    private BaseActionParams getBaseActionParams() {
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/goo");
        return actions;
    }

    private void testResult(String[] linesToTest) throws Exception {
        Path fileCreated = Paths.get("./temp/actions/taggingPolicyScript_0005_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated)
                .stream().map(String::trim).collect(Collectors.toList());
        for (String line : linesToTest) {
            assertTrue(lines.contains(line.trim()));    // ignore leasing/trailing white spaces
        }
        Files.delete(fileCreated);
    }

    private FileTaggingScriptGenerator getInstance() {
        ActionHelper actionHelper = new TestActionHelper();
        FileTaggingScriptGenerator instance = new FileTaggingScriptGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        return instance;
    }
}
