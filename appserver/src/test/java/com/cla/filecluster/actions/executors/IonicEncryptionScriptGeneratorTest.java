package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.filecluster.actions.base.DocAuthorityAction;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
@Category(UnitTestCategory.class)
public class IonicEncryptionScriptGeneratorTest {

    @Test
    public void testEncrypt() throws Exception {
        ActionHelper actionHelper = new TestActionHelper();
        IonicEncryptionScriptGenerator instance = new IonicEncryptionScriptGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/");
        Properties p = new Properties();
        p.setProperty("target", "ddd");
        p.setProperty(DocAuthorityAction.ROOT_PATH, "/boo");
        p.setProperty(DocAuthorityAction.RELATIVE_PATH, "ff");
        p.setProperty(DocAuthorityAction.BASE_NAME, "rfr");
        instance.encryptFile(actions, p);
        instance.apply();
        Path fileCreated = Paths.get("./temp/actions/ionicEncryptionScript_0005_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated)
                .stream().map(String::trim).collect(Collectors.toList());   // resilience to leading/trailing spaces
        assertTrue(lines.contains("REM Single file encryption"));
//        assertTrue(lines.contains("     %IONIC_ENC_CMD_NO_MARKINGS% encrypt \"/\" \"ddd\\_boo\\ff\\rfr\" \"ddd\\_boo\\ff\\\""));
        assertTrue(lines.contains("%IONIC_ENC_CMD_NO_MARKINGS% encrypt \"/\" \"ddd\\_boo\\ff\\rfr\" \"ddd\\_boo\\ff\\\"")); // trimmed
        Files.delete(fileCreated);
    }

    @Test
    public void testDecrypt() throws Exception {
        ActionHelper actionHelper = new TestActionHelper();
        IonicEncryptionScriptGenerator instance = new IonicEncryptionScriptGenerator();
        instance.init(actionHelper);
        ActionExecutionTaskStatusDto dto = new ActionExecutionTaskStatusDto();
        dto.setActionTriggerId(5L);
        instance.prepare(dto);
        BaseActionParams actions = new BaseActionParams();
        actions.setFileUrl("/");
        Properties p = new Properties();
        p.setProperty("target", "ddd");
        p.setProperty(DocAuthorityAction.ROOT_PATH, "/boo");
        p.setProperty(DocAuthorityAction.RELATIVE_PATH, "ff");
        p.setProperty(DocAuthorityAction.BASE_NAME, "rfr");
        instance.decryptFile(actions, p);
        instance.apply();
        Path fileCreated = Paths.get("./temp/actions/ionicDecryptionScript_0005_0001.bat");
        List<String> lines = Files.readAllLines(fileCreated);
        assertTrue(lines.contains("REM Single file encryption"));
        assertTrue(lines.contains("     %IONIC_ENC_CMD_NO_MARKINGS% decrypt \"/\" \"ddd\\_boo\\ff\\rfr\" \"ddd\\_boo\\ff\\\""));
        Files.delete(fileCreated);
    }
}
