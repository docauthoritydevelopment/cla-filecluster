package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.ActionHelper;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by: yael
 * Created on: 12/3/2017
 */
public class TestActionHelper implements ActionHelper {

    private String pathStr = "./temp/actions";
    private Path path = Paths.get(pathStr);

    public TestActionHelper() {
        if (!path.toFile().exists()) {
            path.toFile().mkdirs();
        }
    }

    @Override
    public File getActionClassReportsFolder(long actionTriggerId, ActionClass actionClass) {
        return path.toFile();
    }

    @Override
    public File getActionClassScriptsFolder(ActionExecutionTaskStatusDto actionTrigger, ActionClass actionClass) {
        return path.toFile();
    }

    @Override
    public Stream<Path> getAllActionClassFolders(long actionTriggerId) {
        List<Path> result = new ArrayList<>();
        result.add(path.toAbsolutePath().normalize());
        return result.stream();
    }

    @Override
    public String extractSingleParameter(String parameterName, Properties actionParameters, String actionId, String path) {
        return actionParameters.getProperty(parameterName);
    }

    @Override
    public String extractSingleParameterIfExists(String parameterName, Properties actionParameters, String actionId, String path) {
        return actionParameters.getProperty(parameterName);
    }

    @Override
    public String getBackupFolder() {
        return pathStr;
    }

    @Override
    public Map<String, Object> getActionExecutorProperties(String actionExecutorName) {
        return new HashMap<>();
    }

    @Override
    public List<String> extractParameterValues(String parameterName, Properties actionParameters, String actionId, String path) {
        String parameter = extractSingleParameter(parameterName, actionParameters, actionId, path);
        parameter = parameter.replace("[", "");
        parameter = parameter.replace("]", "");
        String[] split = StringUtils.split(parameter, ',');
        return Arrays.asList(split);
    }
}
