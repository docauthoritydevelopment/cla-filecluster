package com.cla.filecluster.upgrade;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Make sure that baseline version in application.properties is inline with the latest Upgrade Operation version
 */
@Category(UnitTestCategory.class)
public class UpgradeVersionBaselineTest {

    @Test
    public void testBaselineVersion(){
        String baselineVersion = null;
        Properties properties = new Properties();
        File propertiesFile = new File("./config","application.properties");
        if (!propertiesFile.exists()) {
            throw new RuntimeException("Failed to find application.properties file ("+propertiesFile.getAbsolutePath()+")");
        }
        try {
            properties.load(new FileInputStream(propertiesFile));
            baselineVersion = properties.getProperty("upgrade.baseline-version");
        } catch (IOException e) {
            Assert.fail("Failed to load properties.");
        }

        Assert.assertNotNull(baselineVersion);

        Reflections reflections = new Reflections("com.cla.filecluster.upgrade.operations");
        Set<Class<? extends UpgradeOperation>> upgradeOperationClasses = reflections.getSubTypesOf(UpgradeOperation.class);

        // scan all the upgrade operation classes, collect the metadatas
        List<UpgradeOperationMetadata> metadatas = upgradeOperationClasses.stream().
                filter(upgradeOperationClass -> !Modifier.isAbstract(upgradeOperationClass.getModifiers()) &&
                        !ReflectionUtils.withAnnotation(PermanentOperation.class).apply(upgradeOperationClass))
                .map(UpgradeOperationMetadata::create).collect(Collectors.toList());

        Optional<Integer> maxVersionOp = metadatas.stream()
                .map(UpgradeOperationMetadata::getVersion)
                .max(Integer::compare);

        Assert.assertTrue(maxVersionOp.isPresent());

        // make sure the maximal version of all upgrade operations is the same
        // as the baseline version property in application.properties
        Assert.assertEquals(Integer.valueOf(baselineVersion), maxVersionOp.get());
    }


}
