package com.cla.filecluster.metadata;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.metadata.conf.MetadataMatcher;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;

@Category(UnitTestCategory.class)
public class MetadataTranslatorTest {

    @Test
    public void testConsts() {
        String separator = ";";
        MetadataMatcher matcher = new MetadataMatcher();
        matcher.setTypeMatcher("security" + separator + ".*");
        matcher.setTypeReplacement("Secure");
        matcher.setValueMatcher(".*" + separator + "sensitive");
        matcher.setValueReplacement("Sensitive");
        String type = "security";
        String value = "sensitive";


        Pair<String, String> result = MetadataTranslator.translate(
                 matcher,  type,  value,  separator);
        assertTrue(result != null);
        assertEquals("Secure", result.getKey());
        assertEquals("Sensitive", result.getValue());
    }

    @Test
    public void testGroup() {
        String separator = ";";
        MetadataMatcher matcher = new MetadataMatcher();
        matcher.setTypeMatcher("security" + separator + ".*");
        matcher.setTypeReplacement("Secure");
        matcher.setValueMatcher("security" + separator + "(.*)");
        matcher.setValueReplacement("Sensitive-$1");
        String type = "security";
        String value = "gaga";


        Pair<String, String> result = MetadataTranslator.translate(
                matcher,  type,  value,  separator);
        assertTrue(result != null);
        assertEquals("Secure", result.getKey());
        assertEquals("Sensitive-gaga", result.getValue());
    }
}