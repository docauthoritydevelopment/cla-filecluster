package com.cla.filecluster.metadata;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;

@Category(UnitTestCategory.class)
public class MetadataTranslationServiceTest {

    private MetadataTranslationService service = null;

    @Before
    public void init() {
        if (service == null) {
            service = new MetadataTranslationService();
            service.setMetadataSeparator(";");
            service.setMetadataTransformerFilePath("./config/metadata/metadata-translation.json");
            service.init();
        }
    }

    @Test
    public void testMatchNoProp() {
        FileCatBuilder builder = FileCatBuilder.create();
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        service.setMetadata(builder, props);
        assertEquals(null, builder.getDaMetadata());
        assertEquals(null, builder.getDaMetadataType());
        assertEquals(null, builder.getGeneralMetadata());
        assertEquals(null, builder.getRawMetadata());
        assertEquals(null, builder.getActionExpected());
    }

    @Test
    public void testMatchEmptyProp() {
        FileCatBuilder builder = FileCatBuilder.create();
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        props.setMetadata(Maps.newConcurrentMap());
        service.setMetadata(builder, props);
        assertEquals(null, builder.getDaMetadata());
        assertEquals(null, builder.getDaMetadataType());
        assertEquals(null, builder.getGeneralMetadata());
        assertEquals(null, builder.getRawMetadata());
        assertEquals(null, builder.getActionExpected());
    }

    @Test
    public void testMatchRelevantProp() {
        FileCatBuilder builder = FileCatBuilder.create();
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        props.setMetadata(Maps.newConcurrentMap());
        props.getMetadata().put("MipIntegration", "baba");
        service.setMetadata(builder, props);
        assertEquals(Lists.newArrayList("Sensitivity;baba"), builder.getDaMetadata());
        assertEquals(Lists.newArrayList("Sensitivity"), builder.getDaMetadataType());
        assertEquals(Lists.newArrayList("MipIntegration;baba"), builder.getGeneralMetadata());
        assertEquals("{\"MipIntegration\":\"baba\"}", builder.getRawMetadata());
        assertEquals(null, builder.getActionExpected());
    }

    @Test
    public void testMatchNoRelevantProp() {
        FileCatBuilder builder = FileCatBuilder.create();
        ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
        props.setMetadata(Maps.newConcurrentMap());
        props.getMetadata().put("fff", "jjj");
        service.setMetadata(builder, props);
        assertEquals(null, builder.getDaMetadata());
        assertEquals(null, builder.getDaMetadataType());
        assertEquals(Lists.newArrayList("fff;jjj") , builder.getGeneralMetadata());
        assertEquals("{\"fff\":\"jjj\"}", builder.getRawMetadata());
        assertEquals(null, builder.getActionExpected());
    }
}