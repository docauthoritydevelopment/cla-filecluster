package com.cla.filecluster.util;

import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.nio.file.attribute.AclEntryType;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class PermissionCheckUtilsTest {

    @Test
    public void testHasPermission() {
        // read
        assertTrue(PermissionCheckUtils.hasPermission(1179817L, 131241L));
        assertTrue(PermissionCheckUtils.hasPermission(2032127L, 131241L));
        assertTrue(PermissionCheckUtils.hasPermission(1245631L, 131241L));
        assertTrue(PermissionCheckUtils.hasPermission(197055L, 131241L));
        assertTrue(PermissionCheckUtils.hasPermission(1L, 131241L));
        assertFalse(PermissionCheckUtils.hasPermission(2L, 131241L));
        assertTrue(PermissionCheckUtils.hasPermission(32L, 131241L));

        // write
        assertFalse(PermissionCheckUtils.hasPermission(1179817L, 69910L));
        assertTrue(PermissionCheckUtils.hasPermission(2032127L, 69910L));
        assertTrue(PermissionCheckUtils.hasPermission(1245631L, 69910L));
        assertTrue(PermissionCheckUtils.hasPermission(197055L, 69910L));
        assertFalse(PermissionCheckUtils.hasPermission(1L, 69910L));
        assertTrue(PermissionCheckUtils.hasPermission(2L, 69910L));
        assertFalse(PermissionCheckUtils.hasPermission(32L, 69910L));
    }

    @Test
    public void testGetRootFolderAccessDenied() {
        Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions = new HashMap<>();
        Set<String> userAccessTokens = Sets.newHashSet();
        List<String> genericTokens = new ArrayList<>();

        Set<Long> res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertTrue(res.isEmpty());

        rootFolderSharePermissions.put(2L, Lists.newArrayList(createSharePermission(1L, "Everyone", AclEntryType.ALLOW)));
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertFalse(res.isEmpty());
        assertEquals(Lists.newArrayList(2L), res);

        genericTokens.add("\\\\everyone");
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertTrue(res.isEmpty());
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, true);
        assertFalse(res.isEmpty());
        assertEquals(Lists.newArrayList(2L), res);

        rootFolderSharePermissions.put(3L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\Administrators", AclEntryType.ALLOW)));
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertFalse(res.isEmpty());
        assertEquals(Lists.newArrayList(3L), res);

        userAccessTokens.add("BUILTIN\\Administrators");
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertTrue(res.isEmpty());

        rootFolderSharePermissions.put(4L, Lists.newArrayList(createSharePermission(1L, "BUILTIN\\Administrators", AclEntryType.DENY)));
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertFalse(res.isEmpty());
        assertEquals(Lists.newArrayList(4L), res);

        rootFolderSharePermissions.put(6L, Lists.newArrayList(createSharePermission(2L, "BUILTIN\\Administrators", AclEntryType.ALLOW)));
        res = PermissionCheckUtils.getRootFolderAccessDenied(rootFolderSharePermissions, userAccessTokens, genericTokens,131241L, false);
        assertFalse(res.isEmpty());
        assertEquals(Lists.newArrayList(4L, 6L), res);
    }

    private RootFolderSharePermissionDto createSharePermission(long mask, String user, AclEntryType type) {
        RootFolderSharePermissionDto dto = new RootFolderSharePermissionDto();
        dto.setMask(mask);
        dto.setAclEntryType(type);
        dto.setUser(user);
        return dto;
    }
}