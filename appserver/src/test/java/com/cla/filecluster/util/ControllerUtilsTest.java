package com.cla.filecluster.util;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.base.Strings;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(UnitTestCategory.class)
public class ControllerUtilsTest {

    @Test
    public void testCache() {
        ControllerUtils cu = new ControllerUtils();
        cu.putInCache("vv","gg");
        Object res = cu.getFromCache("vv");
        assertTrue(res instanceof String);
        assertEquals("gg", res);

        res = cu.getFromCache("vvdf");
        assertTrue(res == null);
    }

    @Test
    public void testGetStatesList() {
        Map<String, String> params = new HashMap<>();
        List<RunStatus> res = ControllerUtils.getStatesList(params);
        assertTrue(res == null);

        params.put("states", "FINISHED_SUCCESSFULLY,PAUSED");
        res = ControllerUtils.getStatesList(params);
        assertTrue(res != null);
        assertEquals(2, res.size());
    }

    @Test
    public void testGetParamByName() {
        Map<String, String> params = new HashMap<>();
        params.put("states", "FINISHED_SUCCESSFULLY,PAUSED");
        String name = ";fghfgh";
        String res = ControllerUtils.getParamByName(params, name);
        assertTrue(Strings.isNullOrEmpty(res));
        name = "states";
        res = ControllerUtils.getParamByName(params, name);
        assertTrue(!Strings.isNullOrEmpty(res));
    }

    @Test
    public void testRoundUpDivision() {
        int res = ControllerUtils.roundUpDivision(5L, 2L);
        assertEquals(3, res);

        res = ControllerUtils.roundUpDivision(4L, 2L);
        assertEquals(2, res);
    }
}