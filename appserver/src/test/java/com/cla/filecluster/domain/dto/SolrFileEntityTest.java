package com.cla.filecluster.domain.dto;

import com.cla.common.constants.CatFileFieldType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Date;
import java.util.List;

@Category(UnitTestCategory.class)
public class SolrFileEntityTest extends SolrEntityTest<SolrFileEntity, CatFileFieldType>{

    @Before
    public void init(){
        beanFields = prepareMetadata();
    }

    @Test
    public void testSolrFileBeanAgainstEnum() {
        super.testSolrBeanAgainstEnum();
    }

    @Test
    public void testSerialization(){
        Date date = new Date();

        SolrFileEntity entity = new SolrFileEntity();
        entity.setId("123!123");
        entity.setAccessDate(date);
        entity.setAclRead(Lists.newArrayList("ACL1", "ACL2"));

        SolrInputDocument doc = binder.toSolrInputDocument(entity);

        assertField(CatFileFieldType.ID, entity.getId(), doc);
        assertField(CatFileFieldType.LAST_ACCESS, entity.getAccessDate(), doc);
        assertField(CatFileFieldType.ACL_READ, entity.getAclRead(), doc);
        //TODO: expand
    }

    @Override
    protected Class<CatFileFieldType> getEnumClass() {
        return CatFileFieldType.class;
    }

    @Override
    protected Class<SolrFileEntity> getBeanClass() {
        return SolrFileEntity.class;
    }

    @Override
    protected List<CatFileFieldType> getEnumExcludeList() {
        return Lists.newArrayList(CatFileFieldType.FULL_NAME_SEARCH);
    }

}