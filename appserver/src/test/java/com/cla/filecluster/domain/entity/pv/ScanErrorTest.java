package com.cla.filecluster.domain.entity.pv;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class ScanErrorTest {

    @Test
    public void createNew() {
        String longText = RandomStringUtils.random(5000, true, true);

        ScanError err = new ScanError(longText, longText,6L, longText, 6L, 6L, MediaType.FILE_SHARE);

        Assert.assertNotEquals(longText, err.getPath());
        Assert.assertNotEquals(longText, err.getExceptionText());
        Assert.assertNotEquals(longText, err.getText());

        String cutText = StringUtils.substring(longText, 0, ScanError.MAX_TEXT_LENGTH);
        Assert.assertEquals(cutText, err.getPath());
        Assert.assertEquals(cutText, err.getExceptionText());
        Assert.assertEquals(cutText, err.getText());

        String shortText = RandomStringUtils.random(500, true, true);

        err = new ScanError(shortText, shortText,6L, shortText, 6L, 6L, MediaType.FILE_SHARE);

        Assert.assertEquals(shortText, err.getPath());
        Assert.assertEquals(shortText, err.getExceptionText());
        Assert.assertEquals(shortText, err.getText());

        String exactText = RandomStringUtils.random(ScanError.MAX_TEXT_LENGTH, true, true);

        err = new ScanError(exactText, exactText,6L, exactText, 6L, 6L, MediaType.FILE_SHARE);

        Assert.assertEquals(exactText, err.getPath());
        Assert.assertEquals(exactText, err.getExceptionText());
        Assert.assertEquals(exactText, err.getText());
    }

}