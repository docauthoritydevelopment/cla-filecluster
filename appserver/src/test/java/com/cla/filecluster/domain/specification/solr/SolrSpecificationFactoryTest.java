package com.cla.filecluster.domain.specification.solr;

import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePointType;
import com.cla.common.domain.dto.date.TimePeriod;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import com.cla.filecluster.repository.jpa.date.SimpleDateRangeItemRepository;
import com.cla.filecluster.service.date.DateRangeAppService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

/**
 * This is a STUB for a test of SolrSpecificationFactory
 * We don't really need this test, since what we really want is
 * to kill the SolrSpecificationFactory and dance on its grave.
 * (replace it with QueryBuilders)
 * But till then - this is a place for experiments.
 */
@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class SolrSpecificationFactoryTest {

    private ObjectMapper mapper;

    @Mock
    private SimpleDateRangeItemRepository simpleDateRangeItemRepository;

    @InjectMocks
    private SolrSpecificationFactory factory;

    @Before
    public void init(){
        mapper = new ObjectMapper();
//        SimpleDateRangeItem dateRange = new SimpleDateRangeItem();
//        when(simpleDateRangeItemRepository.getOne(anyLong())).thenReturn(dateRange);
    }

    @Test
    public void convertSimpleDateRangeToFilterValueTest(){
        SimpleDateRangeItem dateRange = createDateRange();
        String dateRangeJson;
        try {
            List<DateRangeItemDto> dateRangeItemDtos = DateRangeAppService.convertSimpleDateRangeItems(Lists.newArrayList(dateRange), false);
            dateRangeJson = mapper.writeValueAsString(dateRangeItemDtos.get(0));
            String solrString = factory.convertSimpleDateRangeToFilterValue(dateRangeJson);
            Assert.assertFalse(Strings.isNullOrEmpty(solrString));
            Assert.assertEquals("[2018-11-12T15:30:35Z TO NOW-1MONTHS]", solrString);
        } catch (JsonProcessingException e) {
            Assert.fail(e.getMessage());
        }
    }

    private SimpleDateRangeItem createDateRange(){
        DateRangePoint startPoint = new DateRangePoint();
        startPoint.setType(DateRangePointType.ABSOLUTE);
        startPoint.setAbsoluteTime(new Date(1542036635000L));

        DateRangePoint endPoint = new DateRangePoint();
        endPoint.setType(DateRangePointType.RELATIVE);
        endPoint.setRelativePeriod(TimePeriod.MONTHS);
        endPoint.setRelativePeriodAmount(1);

        SimpleDateRangeItem dateRange = new SimpleDateRangeItem();
        dateRange.setId(1L);
        dateRange.setName("DR1");
        dateRange.setStart(startPoint);
        dateRange.setEnd(endPoint);
        return dateRange;
    }

}
