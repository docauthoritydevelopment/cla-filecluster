package com.cla.filecluster.domain.dto;

import com.cla.common.constants.SolrCoreSchema;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.junit.Assert;
import org.reflections.Reflections;
import org.reflections.scanners.FieldAnnotationsScanner;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.*;

public abstract class SolrEntityTest<T extends SolrEntity, E extends Enum> {

    protected Map<String, Class<?>> beanFields;

    protected DocumentObjectBinder binder = new DocumentObjectBinder();

    protected abstract Class<E> getEnumClass();

    protected abstract Class<T> getBeanClass();

    protected List<E> getEnumExcludeList() {
        return new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    protected void testSolrBeanAgainstEnum() {
        Map<String, E> enumMap = Maps.newHashMap();
        Enum[] values = getEnumClass().getEnumConstants();
        for (Enum entry : values) {
            if (!getEnumExcludeList().contains(entry)) {
                enumMap.put(((SolrCoreSchema) entry).getSolrName(), (E) entry);
            }
        }

        for (String key : enumMap.keySet()){
            Class<?> type = beanFields.get(key);
            E solrField = enumMap.get(key);
            Assert.assertNotNull("Failed to find field " + key, type);
            matchType(type, solrField);
        }

    }

    protected Map<String, Class<?>> prepareMetadata(){
        Map<String, Class<?>> metadata = Maps.newHashMap();

        Reflections ref = new Reflections(getBeanClass().getPackage().getName(), new FieldAnnotationsScanner());
        Set<Field> fields = ref.getFieldsAnnotatedWith(org.apache.solr.client.solrj.beans.Field.class);
        String fieldName;
        Class<?> fieldType;
        for (Field field : fields){
            if (!field.getDeclaringClass().equals(getBeanClass())){
                continue;
            }
            org.apache.solr.client.solrj.beans.Field annotation =
                    field.getAnnotation(org.apache.solr.client.solrj.beans.Field.class);

            fieldName = (annotation.value().equals(DocumentObjectBinder.DEFAULT)) ?
                    field.getName() : annotation.value();

            fieldType = field.getType();
            metadata.put(fieldName, fieldType);
        }
        return metadata;
    }

    private void matchType(Class<?> type, E entryEnum){
        SolrCoreSchema entry = (SolrCoreSchema)entryEnum;
        if (type.equals(entry.getType())){
            return;
        }

        boolean assertVal = true;
        if (type.equals(Date.class) || type.equals(LocalDateTime.class)){
            assertVal = entry.getType().equals(Date.class) || entry.getType().equals(LocalDateTime.class);
        }

        if (Collection.class.isAssignableFrom(type)){
            return;
        }

        Assert.assertTrue("Type mismatch in field [" + entry + "]: " + type + " vs " + entry.getType(),
                assertVal);
    }

    @SuppressWarnings("unchecked")
    protected void assertField(E field, Object expectedValue, SolrInputDocument doc){
        SolrCoreSchema entry = (SolrCoreSchema)field;
        SolrInputField inputField = doc.getField(entry.getSolrName());
        if (Collection.class.isAssignableFrom(expectedValue.getClass())){
            Set checkSet = Sets.newHashSet();
            Set expectedSet = Sets.newHashSet();
            checkSet.addAll((Collection)inputField.getValue());
            expectedSet.addAll((Collection)expectedValue);
            Assert.assertTrue(checkSet.equals(expectedSet));
        }
        else {
            Assert.assertEquals(expectedValue, inputField.getValue());
        }
    }
}
