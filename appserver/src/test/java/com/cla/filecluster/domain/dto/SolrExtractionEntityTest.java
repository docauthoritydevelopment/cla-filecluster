package com.cla.filecluster.domain.dto;

import com.cla.common.constants.ExtractionFieldType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.SolrInputDocument;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Date;

@Category(UnitTestCategory.class)
public class SolrExtractionEntityTest extends SolrEntityTest<SolrExtractionEntity, ExtractionFieldType>{

    @Before
    public void init(){
        beanFields = prepareMetadata();
    }

    @Test
    public void testSolrFileBeanAgainstEnum() {
        super.testSolrBeanAgainstEnum();
    }

    @Test
    public void testSerialization(){
        Date date = new Date();

        SolrExtractionEntity entity = new SolrExtractionEntity();
        entity.setId("123");
        entity.setAccessDate(date);
        entity.setContent("Lorem ipsum");

        SolrInputDocument doc = binder.toSolrInputDocument(entity);

        assertField(ExtractionFieldType.ID, entity.getId(), doc);
        assertField(ExtractionFieldType.LAST_ACCESS_DATE, entity.getAccessDate(), doc);
        assertField(ExtractionFieldType.CONTENT, entity.getContent(), doc);
        //TODO: expand
    }

    @Override
    protected Class<ExtractionFieldType> getEnumClass() {
        return ExtractionFieldType.class;
    }

    @Override
    protected Class<SolrExtractionEntity> getBeanClass() {
        return SolrExtractionEntity.class;
    }
}
