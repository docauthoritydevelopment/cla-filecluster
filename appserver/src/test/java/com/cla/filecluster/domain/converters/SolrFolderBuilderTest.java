package com.cla.filecluster.domain.converters;

import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@Category(FastTestCategory.class)
public class SolrFolderBuilderTest {

    @Test
    public void testSolrFolderBuilder(){
        SolrFolderBuilder builder = SolrFolderBuilder.create();

        SolrFolderEntity entity = builder.setId(4L)
                .setPath("\\a\\b\\c\\d\\e\\f")
                .setPathDescriptorType(PathDescriptorType.WINDOWS)
                .setRootDepth(4)
                .setRootFolderId(2L)
                .setRootFolderPath("\\a1")
                .setParentFolderId(3L)
                .setDeleted(false)
                .setParentInfo(Lists.newArrayList("0.3","1.4"))
                .setType(FolderType.REGULAR)
                .setAclSignature("123123")
                .setAllowedReadPrincipalsNames(Lists.newArrayList("A","B"))
                .setAllowedWritePrincipalsNames(Lists.newArrayList("C","D"))
                .setDeniedReadPrincipalsNames(Lists.newArrayList("E","F"))
                .setDeniedWritePrincipalsNames(Lists.newArrayList("G","H"))
                .setFolderHash("asdasdasd")
                .setMediaEntityId("\\a\\b\\c\\d\\e\\f")
                .build();

        assertNotNull(entity);
        assertEquals(4L, entity.getId().longValue());
        assertEquals("/a/b/c/d/e/f/", entity.getPath());
        assertEquals("\\a\\b\\c\\d\\e\\f", entity.getRealPath());
        assertEquals("asdasdasd", entity.getFolderHash());
        assertEquals(2L, entity.getRootFolderId().longValue());
        assertEquals(3L, entity.getParentFolderId().longValue());
        assertEquals(3L, entity.getDepthFromRoot().longValue());
        assertEquals(3, entity.getParentsInfo().size());
        assertEquals(PathDescriptorType.WINDOWS.name(), entity.getPathDescriptorType());
        assertEquals(false, entity.getDeleted());
        assertEquals(FolderType.REGULAR.ordinal(), entity.getType());
    }
}
