package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.StrategyType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Test ingest task producer
 * Created by vladi on 3/16/2017.
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class FairTaskProducingStrategyTest {

    @Mock
    private JobManagerService jobManager;

    @Mock
    private SolrTaskService solrTaskService;

    private FairTaskProducingStrategy producer;

    @Before
    public void init(){
        TaskProducingStrategyConfig config = new TaskProducingStrategyConfig();
        config.setSelectedStrategy(StrategyType.FAIR);
        config.setIngestTasksQueueLimit(50);
        config.setAnalyzeTasksQueueLimit(200);
        config.setExclusiveMode(JobType.INGEST);
        config.setTimeoutGraceSec(TimeUnit.HOURS.toSeconds(1));
        producer = new FairTaskProducingStrategy(jobManager, solrTaskService, config);
    }

    /**
     * Test fair distribution of enqueued tasks between the different run contexts.
     * Test the case where all the contexts have more tasks to insert than the queue limit permits
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testUniformTasksDistributionNoGap(){
        Table<Long, TaskState, Integer> table = prepareDistribution(100, 200, 300,
                10, 5, 2);
        Map<Long, Integer> distribution = ReflectionTestUtils.invokeMethod(producer, "getTasksDistribution", table, TaskType.INGEST_FILE, null);
        Assert.assertNotNull(distribution);
        Assert.assertEquals(11, distribution.get(1L).intValue());
        Assert.assertEquals(11, distribution.get(2L).intValue());
        Assert.assertEquals(11, distribution.get(3L).intValue());
    }

    /**
     * Test fair distribution of enqueued tasks between the different run contexts.
     * Test the case where only one context has more tasks to insert than the queue limit permits
     * and the other tasks have less
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testTasksDistributionWithGaps(){
        Table<Long, TaskState, Integer> table = prepareDistribution(5, 200, 3,
                3, 5, 1);
        Map<Long, Integer> distribution = ReflectionTestUtils.invokeMethod(producer, "getTasksDistribution", table, TaskType.INGEST_FILE, null);
        Assert.assertNotNull(distribution);
        Assert.assertEquals(5, distribution.get(1L).intValue());
        Assert.assertEquals(33, distribution.get(2L).intValue());
        Assert.assertEquals(3, distribution.get(3L).intValue());
    }

    /**
     * Test fair distribution of enqueued tasks between the different run contexts.
     * Test the case where only one context has more tasks to insert than the queue limit permits
     * and the other tasks have less
     */
    @SuppressWarnings("unchecked")
    @Test
    public void testUnderstuffedTasksDistribution(){
        Table<Long, TaskState, Integer> table = prepareDistribution(5, 6, 3,
                3, 5, 1);
        Map<Long, Integer> distribution = ReflectionTestUtils.invokeMethod(producer, "getTasksDistribution", table, TaskType.INGEST_FILE, null);
        Assert.assertNotNull(distribution);
        Assert.assertEquals(5, distribution.get(1L).intValue());
        Assert.assertEquals(6, distribution.get(2L).intValue());
        Assert.assertEquals(3, distribution.get(3L).intValue());
    }

    @SuppressWarnings("SameParameterValue")
    private Table<Long, TaskState, Integer> prepareDistribution(int firstNewCount, int secondNewCount, int thirdNewCount,
                                                                int firstEnqCount, int secondEnqCount, int thirdEnqCount){

        HashBasedTable<Long, TaskState, Integer> table = HashBasedTable.create();
        table.put(1L, TaskState.NEW, firstNewCount);
        table.put(2L, TaskState.NEW, secondNewCount);
        table.put(3L, TaskState.NEW, thirdNewCount);

        table.put(1L, TaskState.ENQUEUED, firstEnqCount);
        table.put(2L, TaskState.ENQUEUED, secondEnqCount);
        table.put(3L, TaskState.ENQUEUED, thirdEnqCount);

        return table;
    }
}
