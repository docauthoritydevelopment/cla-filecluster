package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.security.Authority;
import com.cla.filecluster.domain.entity.filetag.TagAssociation;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.security.*;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.security.Authority.Const.VIEW_CONTENT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;
import static org.springframework.security.access.AccessDecisionVoter.ACCESS_DENIED;
import static org.springframework.security.access.AccessDecisionVoter.ACCESS_GRANTED;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class PermissionsServiceTest {

    private static final String BIZ_ROLES_CSV_PATH = "./config/defaultOperRoles.csv";

    @Mock
    private UserService userService;

    @Mock
    private LdapGroupMappingService ldapGroupMappingService;

    @Mock
    private FileGroupService groupRepo;

    @Mock
    private AssociationsService associationsService;

    @InjectMocks
    private PermissionsService service = new PermissionsService();

    private Map<String,Role> rolesMap;
    private Map<String,BizRole> bizRolesMap;

    private User adminUser;
    private User emptyViewerUser;
    private User funcRoleViewerUser;

    @Before
    public void init(){

        rolesMap = initRoles();

        CsvReader csvReader = new CsvReader();
        ReflectionTestUtils.setField(csvReader, "consequtiveFailuresLimit", 10);
        ReflectionTestUtils.setField(csvReader, "numOfFailuresToLog", 0);

        bizRolesMap = Maps.newHashMap();
        try {
            Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, BIZ_ROLES_CSV_PATH, new String[]{});
            csvReader.readAndProcess(BIZ_ROLES_CSV_PATH, row ->
                    createBizRoleFromRow(row, headersLocationMap), null, null);
        }
        catch(Exception e){
            fail(e.getMessage());
        }

        FunctionalRole funcRole = new FunctionalRole();
        funcRole.setName("func");
        funcRole.setId(1L);
        funcRole.setTemplate(new RoleTemplate());
        funcRole.getTemplate().setName("temp");
        funcRole.getTemplate().getRoles().add(rolesMap.get(Authority.ViewFiles.getName()));
        funcRole.getTemplate().getRoles().add(rolesMap.get(Authority.MngGroups.getName()));

        adminUser = new User();
        adminUser.setBizRoles(Sets.newHashSet(bizRolesMap.get("admin")));

        emptyViewerUser = new User();
        emptyViewerUser.setBizRoles(Sets.newHashSet(bizRolesMap.get("viewer")));

        funcRoleViewerUser = new User();
        funcRoleViewerUser.setBizRoles(Sets.newHashSet(bizRolesMap.get("viewer")));
        funcRoleViewerUser.setFunctionalRoles(Sets.newHashSet(funcRole));

        FileGroup emptyFileGroup = new FileGroup();
        FileGroup funcFileGroup = new FileGroup();

        when(groupRepo.getById("emptyGroup")).thenReturn(emptyFileGroup);
        when(groupRepo.getById("funcGroup")).thenReturn(funcFileGroup);

        TagAssociation funcRoleAss = new TagAssociation();
        funcRoleAss.setFunctionalRole(funcRole);
        funcRoleAss.setDeleted(false);
    }

    @Test
    public void testGetAccessBasic(){
        when(userService.getCurrentUser()).thenReturn(UserService.convertUser(emptyViewerUser, false));
        int access = service.getAccess(requiredPermissions(), "emptyGroup", FunctionalItem.GROUP);
        assertEquals(ACCESS_GRANTED, access);
        access = service.getAccess(requiredPermissions(VIEW_CONTENT), "emptyGroup", FunctionalItem.GROUP);
        assertEquals(ACCESS_DENIED, access);

        access = service.getAccess(requiredPermissions(), "funcGroup", FunctionalItem.GROUP);
        assertEquals(ACCESS_GRANTED, access);
        access = service.getAccess(requiredPermissions(VIEW_CONTENT), "funcGroup", FunctionalItem.GROUP);
        assertEquals(ACCESS_DENIED, access);
    }

    private void createBizRoleFromRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues[0].startsWith("#")) {
            return;
        }
        String name = fieldsValues[headersLocationMap.get("name")].trim();
        String displayName = fieldsValues[headersLocationMap.get("displayName")].trim();
        String description = fieldsValues[headersLocationMap.get("description")].trim();
        String rolesString = fieldsValues[headersLocationMap.get("roles")];
        rolesString = StringUtils.trim(rolesString);
        ArrayList<String> rolesList = rolesString != null ?
                Lists.newArrayList(rolesString.split(",")) : Lists.newArrayList();

        Set<Role> roles = rolesList.stream().map(roleName -> rolesMap.get(roleName)).collect(Collectors.toSet());

        RoleTemplate template = new RoleTemplate();
        template.setId(1L);
        template.setName(name);
        template.setDescription(name);
        template.setRoles(roles);

        BizRole bizRole = new BizRole();
        bizRole.setName(name);
        bizRole.setDisplayName(displayName);
        bizRole.setDescription(description);
        bizRole.setTemplate(template);

        bizRolesMap.put(name, bizRole);
    }

    private Map<String,Role> initRoles(){
        Map<String,Role> rolesMap = Maps.newHashMap();
        for (Authority auth : Authority.values()){
            Role role = new Role(auth.getName(), auth.getDescription(), auth.getName());
            rolesMap.put(role.getName(), role);
        }
        return rolesMap;
    }

    private Set<String> requiredPermissions(String ... authorities){
        return Arrays.stream(authorities).collect(Collectors.toSet());
    }

    @Test
    public void testMatchPermissionsMulti() {
        Set<String> requiredPermissions = Sets.newHashSet("ADMIN", "TECH");
        Set<String> userPermissions = Sets.newHashSet("TECH");
        int res = service.matchPermissions(requiredPermissions, userPermissions);
        assertEquals(ACCESS_GRANTED, res);
    }

    @Test
    public void testMatchPermissionsSingle() {
        Set<String> requiredPermissions = Sets.newHashSet("TECH");
        Set<String> userPermissions = Sets.newHashSet("TECH");
        int res = service.matchPermissions(requiredPermissions, userPermissions);
        assertEquals(ACCESS_GRANTED, res);
    }

    @Test
    public void testMatchPermissionsSingleMissing() {
        Set<String> requiredPermissions = Sets.newHashSet("TECH");
        Set<String> userPermissions = Sets.newHashSet("TECH2");
        int res = service.matchPermissions(requiredPermissions, userPermissions);
        assertEquals(ACCESS_DENIED, res);
    }

    @Test
    public void testMatchPermissionsMultiMandatoryMissing() {
        Set<String> requiredPermissions = Sets.newHashSet("+ADMIN", "TECH");
        Set<String> userPermissions = Sets.newHashSet("TECH");
        int res = service.matchPermissions(requiredPermissions, userPermissions);
        assertEquals(ACCESS_DENIED, res);
    }

    @Test
    public void testMatchPermissionsMandatory() {
        Set<String> requiredPermissions = Sets.newHashSet("+TECH");
        Set<String> userPermissions = Sets.newHashSet("TECH");
        int res = service.matchPermissions(requiredPermissions, userPermissions);
        assertEquals(ACCESS_GRANTED, res);
    }
}
