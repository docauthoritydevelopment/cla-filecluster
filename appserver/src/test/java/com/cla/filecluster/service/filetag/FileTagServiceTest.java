package com.cla.filecluster.service.filetag;

import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.filetag.FileTagRepository;
import com.cla.filecluster.repository.jpa.scope.BasicScopeRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.security.UserService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FileTagServiceTest {

    @Mock
    FileTagRepository fileTagRepository;

    @Mock
    DocFolderService docFolderService;

    @Mock
    private UserService userService;

    @Mock
    private BasicScopeRepository basicSopeRepository;

    @Mock
    private MessageHandler messageHandler;

    @InjectMocks
    private FileTagService fileTagService;

    @Before
    public void init() {
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @Test(expected = BadRequestException.class)
    public void testCreateFileTagNameNull() {
        FileTagType fileTagType = new FileTagType();
        fileTagService.createFileTag(null, null, fileTagType , null, null, null);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateFileTagNameEmpty() {
        FileTagType fileTagType = new FileTagType();
        fileTagService.createFileTag("", null, fileTagType , null, null, null);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateFileTagNameSpaces() {
        FileTagType fileTagType = new FileTagType();
        fileTagService.createFileTag("  ", null, fileTagType , null, null, null);
    }

    @Test
    public void testCreateFileTagNameOK() {
        FileTagType fileTagType = new FileTagType();
        fileTagType.setName("dfhgdfg");

        FileTag fileTag = new FileTag();
        when(fileTagRepository.save(any(FileTag.class))).thenReturn(fileTag);

        fileTagService.createFileTag("fgsdfsd", null, fileTagType , null, null, null);
    }
}