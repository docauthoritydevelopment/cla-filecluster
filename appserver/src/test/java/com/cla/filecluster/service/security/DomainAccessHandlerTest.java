package com.cla.filecluster.service.security;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class DomainAccessHandlerTest {

    private DomainAccessHandler domainAccessHandler;

    @Before
    public void init() {
        domainAccessHandler = new DomainAccessHandler();
        domainAccessHandler.setAccessTokensDomainNameJoinString("\\\\");
        domainAccessHandler.setAccessTokensDomainNameMapStrings(new String[]{"testing.da.com:TESTING"});
        domainAccessHandler.setAccessTokensOriginalDomainNameJoinString("@");
        domainAccessHandler.setAccessTokensDomainNameSplitString("\\");
        domainAccessHandler.init();
    }

    @Test
    public void testGetFileShareToken() {
        assertEquals("TESTING\\\\aaa", domainAccessHandler.getFileShareToken("aaa", "testing.da.com"));
        assertEquals("TESTING\\\\aaa@testing.da.com", domainAccessHandler.getFileShareToken("aaa@testing.da.com", "testing.da.com"));
        assertEquals("45y54y54y54y\\\\babab", domainAccessHandler.getFileShareToken("babab", "45y54y54y54y"));
    }

    @Test
    public void testGetEmailToken() {
        assertEquals("aaa@testing.da.com", domainAccessHandler.getEmailToken("aaa", "testing.da.com"));
        assertEquals("aaa@testing.da.com", domainAccessHandler.getEmailToken("aaa@testing.da.com", "testing.da.com"));
    }

    @Test
    public void testGetAclInEmailFormat() {
        assertEquals("aaa", domainAccessHandler.getAclInEmailFormat("aaa"));
        assertEquals("\\\\Everyone", domainAccessHandler.getAclInFileShareFormat("\\\\Everyone"));
        assertEquals("aaa@testing.da.com", domainAccessHandler.getAclInEmailFormat("aaa@testing.da.com"));
        assertEquals("aaa@testing.da.com", domainAccessHandler.getAclInEmailFormat("TESTING\\\\aaa"));
        assertEquals("45y54y54y54y\\\\babab", domainAccessHandler.getAclInEmailFormat("45y54y54y54y\\\\babab"));
        assertEquals("aaa@testing.da.com", domainAccessHandler.getAclInEmailFormat("TESTING\\aaa"));
    }

    @Test
    public void testGetAclInFileShareFormat() {
        assertEquals("aaa", domainAccessHandler.getAclInFileShareFormat("aaa"));
        assertEquals("\\\\Everyone", domainAccessHandler.getAclInFileShareFormat("\\\\Everyone"));
        assertEquals("TESTING\\aaa", domainAccessHandler.getAclInFileShareFormat("aaa@testing.da.com"));
        assertEquals("TESTING\\\\aaa", domainAccessHandler.getAclInFileShareFormat("TESTING\\\\aaa"));
        assertEquals("babab@45y54y54y54y", domainAccessHandler.getAclInFileShareFormat("babab@45y54y54y54y"));
        assertEquals("45y54y54y54y\\\\babab", domainAccessHandler.getAclInFileShareFormat("45y54y54y54y\\\\babab"));
    }
}