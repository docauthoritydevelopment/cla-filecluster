package com.cla.filecluster.service.files.filetree;

import com.cla.connector.utils.FileNamingUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.*;

@Category(UnitTestCategory.class)
public class DocFolderUtilsTest {

    @Test
    public void testExtractFolderFromPath() {
        assertEquals("\\\\172.31.41.248\\Enron\\Extraction\\c\\Chris Germany", DocFolderUtils.extractFolderFromPath("\\\\172.31.41.248\\Enron\\Extraction\\c\\Chris Germany\\2000tran05.xls"));
        assertEquals("\\\\172.31.41.248\\Enron\\Extraction", DocFolderUtils.extractFolderFromPath("\\\\172.31.41.248\\Enron\\Extraction\\c"));
        assertEquals("\\\\172.31.41.248\\Enron\\Extraction\\", DocFolderUtils.extractFolderFromPath("\\\\172.31.41.248\\Enron\\Extraction\\c\\"));
        assertEquals("\\Enron\\Extraction\\", DocFolderUtils.extractFolderFromPath("\\Enron\\Extraction\\c\\"));
        assertEquals("\\", DocFolderUtils.extractFolderFromPath("\\c\\"));
        assertEquals("", DocFolderUtils.extractFolderFromPath("\\c"));
        assertEquals("", DocFolderUtils.extractFolderFromPath("\\"));
        assertEquals("", DocFolderUtils.extractFolderFromPath(""));
        assertEquals("/172.31.41.248/Enron/Extraction", DocFolderUtils.extractFolderFromPath("/172.31.41.248/Enron/Extraction/c"));
        assertEquals("/Enron/Extraction/", DocFolderUtils.extractFolderFromPath("/Enron/Extraction/c/"));
        assertEquals("/", DocFolderUtils.extractFolderFromPath("/c/"));
        assertEquals("", DocFolderUtils.extractFolderFromPath("/c"));
        assertEquals("", DocFolderUtils.extractFolderFromPath("/"));
        assertEquals("", DocFolderUtils.extractFolderFromPath(""));


        String rfRealPath = "\\\\172.31.41.248\\giltestfolder\\pattern\\";
        String fileUrl = "\\\\172.31.41.248\\giltestfolder\\pattern\\id.docx";
        String folderUrlToAdd = fileUrl.startsWith(rfRealPath) ? fileUrl.substring(rfRealPath.length()) : fileUrl;
        assertEquals("", DocFolderUtils.extractFolderFromPath(folderUrlToAdd));

        fileUrl = "\\\\172.31.41.248\\giltestfolder\\pattern\\";
        folderUrlToAdd = fileUrl.startsWith(rfRealPath) ? fileUrl.substring(rfRealPath.length()) : fileUrl;
        assertEquals("", DocFolderUtils.extractFolderFromPath(folderUrlToAdd));
    }
}