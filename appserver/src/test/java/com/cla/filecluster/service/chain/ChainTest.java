package com.cla.filecluster.service.chain;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitTestCategory.class)
public class ChainTest {

    @Test
    public void testRun() {
        Chain<ChainContextTest> testChain = new Chain<>("test");
        testChain.next(new ChainLink1());
        testChain.next(new ChainLink2());

        ChainContextTest context = new ChainContextTest();
        testChain.handle(context);

        assertTrue(context.getCalled().size() == 4);
        assertTrue(context.getCalled().contains(0));
        assertTrue(context.getCalled().contains(1));
        assertTrue(context.getCalled().contains(2));
        assertTrue(context.getCalled().contains(3));
        assertTrue(!context.errorOccurred());
    }

    @Test
    public void testRunWithSkip() {
        Chain<ChainContextTest> testChain = new Chain<>("test");
        testChain.next(new ChainLink1());
        testChain.next(new ChainLinkDontDo());
        testChain.next(new ChainLink2());

        ChainContextTest context = new ChainContextTest();
        testChain.handle(context);

        assertTrue(context.getCalled().size() == 4);
        assertTrue(context.getCalled().contains(0));
        assertTrue(context.getCalled().contains(1));
        assertTrue(context.getCalled().contains(2));
        assertTrue(context.getCalled().contains(3));
        assertTrue(!context.errorOccurred());
    }

    @Test
    public void testRunWithStop() {
        Chain<ChainContextTest> testChain = new Chain<>("test");
        testChain.next(new ChainLink1());
        testChain.next(new ChainLinkStop());
        testChain.next(new ChainLink2());

        ChainContextTest context = new ChainContextTest();
        testChain.handle(context);

        assertTrue(context.getCalled().size() == 4);
        assertTrue(context.getCalled().contains(0));
        assertTrue(context.getCalled().contains(1));
        assertTrue(context.getCalled().contains(4));
        assertTrue(context.getCalled().contains(5));
        assertTrue(!context.errorOccurred());
    }

    @Test
    public void testRunErrNoHandler() {
        Chain<ChainContextTest> testChain = new Chain<>("test");
        testChain.next(new ChainLink1());
        testChain.next(new ChainLinkEx());

        ChainContextTest context = new ChainContextTest();
        testChain.handle(context);

        assertTrue(context.getCalled().size() == 2);
        assertTrue(context.getCalled().contains(0));
        assertTrue(context.getCalled().contains(1));
        assertTrue(context.errorOccurred());
    }

    @Test
    public void testRunErr() {
        Chain<ChainContextTest> testChain = new Chain<>("test");
        testChain.next(new ChainLink1());
        testChain.next(new ChainLinkEx());
        testChain.setErrorHandler(new ChainLinkErr());

        ChainContextTest context = new ChainContextTest();
        testChain.handle(context);

        assertTrue(context.getCalled().size() == 3);
        assertTrue(context.getCalled().contains(0));
        assertTrue(context.getCalled().contains(1));
        assertTrue(context.getCalled().contains(-1));
        assertTrue(context.errorOccurred());
    }

    /////////////////////////////////// classes /////////////////////////////////////////////////////////////////////////

    class ChainLink1 implements ChainLink<ChainContextTest> {
        @Override
        public boolean handle(ChainContextTest context) {
            context.getCalled().add(1);
            return true;
        }

        @Override
        public boolean shouldHandle(ChainContextTest context) {
            context.getCalled().add(0);
            return true;
        }
    }

    class ChainLink2 implements ChainLink<ChainContextTest> {
        @Override
        public boolean handle(ChainContextTest context) {
            context.getCalled().add(3);
            return true;
        }

        @Override
        public boolean shouldHandle(ChainContextTest context) {
            context.getCalled().add(2);
            return true;
        }
    }

    class ChainLinkStop implements ChainLink<ChainContextTest> {
        @Override
        public boolean handle(ChainContextTest context) {
            context.getCalled().add(5);
            return false;
        }

        @Override
        public boolean shouldHandle(ChainContextTest context) {
            context.getCalled().add(4);
            return true;
        }
    }

    class ChainLinkDontDo implements ChainLink<ChainContextTest> {
        @Override
        public boolean handle(ChainContextTest context) {
            context.getCalled().add(6);
            return true;
        }

        @Override
        public boolean shouldHandle(ChainContextTest context) {
            return false;
        }
    }

    class ChainLinkEx implements ChainLink<ChainContextTest> {
        @Override
        public boolean handle(ChainContextTest context) {
            throw new RuntimeException("err");
        }

        @Override
        public boolean shouldHandle(ChainContextTest context) {
            return true;
        }
    }

    class ChainLinkErr implements ChainLink<ChainContextTest> {
        @Override
        public boolean handle(ChainContextTest context) {
            context.getCalled().add(-1);
            return true;
        }

        @Override
        public boolean shouldHandle(ChainContextTest context) {
            return true;
        }
    }

    class ChainContextTest implements ChainContext {
        List<Integer> called = new ArrayList<>();
        boolean errorOccurred = false;

        public List<Integer> getCalled() {
            return called;
        }

        @Override
        public boolean errorOccurred() {
            return errorOccurred;
        }

        @Override
        public void setErrorOccurred(boolean errorOccurred) {
            this.errorOccurred = errorOccurred;
        }
    }
}