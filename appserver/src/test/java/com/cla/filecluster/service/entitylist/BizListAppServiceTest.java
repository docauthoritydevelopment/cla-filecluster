package com.cla.filecluster.service.entitylist;

import com.cla.common.domain.dto.bizlist.BizListItemState;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.BizListSchema;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.test.filecluster.it.CustomPageImpl;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class BizListAppServiceTest {

    @Mock
    private BizListService bizListService;

    @Mock
    private BizListItemService bizListItemService;

    @InjectMocks
    private BizListAppService bizListAppService;

    @Before
    public void init() {
        ReflectionTestUtils.setField(bizListAppService, "predefinedBizListName", "Popular Names US");
        ReflectionTestUtils.setField(bizListAppService, "predefinedBizListType", "CONSUMERS");
        ReflectionTestUtils.setField(bizListAppService, "predefinedBizListTemplate", "NameOnly");
        ReflectionTestUtils.setField(bizListAppService, "predefinedBizListSource", "./config/extraction/poc_consumer_names_hashed.csv");
    }

    @After
    public void resetMocks() {
        Mockito.reset(bizListService);
        Mockito.reset(bizListItemService);
    }

    @Test
    public void testCreateDefaultBizList() {
        BizList bizList = new BizList();
        bizList.setId(1);
        bizList.setActive(true);
        bizList.setBizListItemType(BizListItemType.CONSUMERS);
        bizList.setBizListSolrId("blt.1");
        bizList.setRulesFileName("./config/extraction/extraction-rules-NameOnly.txt");
        bizList.setTemplate("NameOnly");
        bizList.setDescription("predefined bizList");
        when(bizListService.createBizList(any())).thenReturn(bizList);
        when(bizListService.findById(1)).thenReturn(bizList);
        BizListSchema bizListSchema = new BizListSchema();
        String[] headers = {"NAME", "CNAME", "ID"};
        bizListSchema.setHeaders(headers);
        when(bizListService.loadCsvFile(any(), any(), any())).thenReturn(bizListSchema);
        CustomPageImpl<SimpleBizListItem> simpleBizListItems = new CustomPageImpl<SimpleBizListItem>();
        List<SimpleBizListItem> content = new ArrayList<>();
        simpleBizListItems.setContent(content);
        when(bizListItemService.getSimpleBizListItems(any(), any())).thenReturn(simpleBizListItems);
        bizListAppService.createDefaultBizList();
        verify(bizListItemService, times(1)).updateBizListItemsState(content, BizListItemState.ACTIVE);
    }
}
