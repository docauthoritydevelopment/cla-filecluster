package com.cla.filecluster.service.convertion;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
@Category(UnitTestCategory.class)
public class FileDtoFieldConverterTest {

    @Test
    public void testSearchClientNoRuleAndNoTerm() {
        boolean addBizListTerm = false;
        BizListItemType bizListItemType = BizListItemType.CLIENTS;
        Long bizListId = 4l;
        String bizListItemId = "4_23";
        String ruleName = null;
        String res = FileDtoFieldConverter.convertBizListItemToSolrValue(addBizListTerm,  bizListItemType,  bizListId,  bizListItemId,  ruleName);
        Assert.assertEquals("bli.1.4.4_23", res);
    }

    @Test
    public void testSearchClientNoRuleAndTerm() {
        boolean addBizListTerm = true;
        BizListItemType bizListItemType = BizListItemType.CLIENTS;
        Long bizListId = 4l;
        String bizListItemId = "4_23";
        String ruleName = null;
        String res = FileDtoFieldConverter.convertBizListItemToSolrValue(addBizListTerm,  bizListItemType,  bizListId,  bizListItemId,  ruleName);
        Assert.assertEquals("blt.1.4 bli.1.4.4_23", res);
    }

    @Test
    public void testSearchClientRuleAndNoTerm() {
        boolean addBizListTerm = false;
        BizListItemType bizListItemType = BizListItemType.CLIENTS;
        Long bizListId = 4l;
        String bizListItemId = "4_23";
        String ruleName = "rulename";
        String res = FileDtoFieldConverter.convertBizListItemToSolrValue(addBizListTerm,  bizListItemType,  bizListId,  bizListItemId,  ruleName);
        Assert.assertEquals("bli.1.4.4_23 erb.1.rulename.4_23 er.rulename", res);
    }

    @Test
    public void testSearchClientRuleAndTerm() {
        boolean addBizListTerm = true;
        BizListItemType bizListItemType = BizListItemType.CLIENTS;
        Long bizListId = 4l;
        String bizListItemId = "4_23";
        String ruleName = "rulename";
        String res = FileDtoFieldConverter.convertBizListItemToSolrValue(addBizListTerm,  bizListItemType,  bizListId,  bizListItemId,  ruleName);
        Assert.assertEquals("blt.1.4 bli.1.4.4_23 erb.1.rulename.4_23 er.rulename", res);
    }
}
