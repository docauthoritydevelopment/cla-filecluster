package com.cla.filecluster.service.categories;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@Category(UnitTestCategory.class)
public class FileCatServiceTest {


    private FileCatService fileCatService = new FileCatService();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testDeSerialization(){
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            URL resource = classLoader.getResource("json/catFile.json");
            Assert.assertNotNull(resource);
            File jsonFile = new File(resource.getFile());
            SolrFileEntity solrFileEntity = objectMapper.readValue(jsonFile, SolrFileEntity.class);
            Assert.assertNotNull(solrFileEntity);

            ClaFile fromEntity = fileCatService.getFromEntity(solrFileEntity);
            Assert.assertNotNull(fromEntity);
            Assert.assertEquals(13030, fromEntity.getId().longValue());
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testClaFileRank(){
        ClaFile file1 = new ClaFile();
        file1.setId(1L);
        ClaFile file2 = new ClaFile();
        assertGreaterRank(file1, file2);

        file2.setId(2L);
        file2.setContentMetadataId(2L);
        assertGreaterRank(file2, file1);

        file1.setContentMetadataId(1L);
        file1.setRootFolderId(1L);
        assertGreaterRank(file1, file2);

        file2.setRootFolderId(2L);
        file2.setDocFolderId(3L);
        assertGreaterRank(file2, file1);

        file1.setDocFolderId(3L);
        file1.setMediaType(MediaType.BOX);
        assertGreaterRank(file1, file2);

        List<String> assoc = Lists.newArrayList();
        assoc.add("ass1");
        file2.setMediaType(MediaType.BOX);
        file2.setAssociations(assoc);
        assertGreaterRank(file2, file1);

        file1.setAssociations(assoc);
        file1.setAnalysisGroupId("123");
        assertGreaterRank(file1, file2);

        file2.setAnalysisGroupId("123");
        file2.setUserGroupId("324");
        assertGreaterRank(file2, file1);

        file1.setUserGroupId("324");
        file1.setState(ClaFileState.SCANNED);
        file2.setState(null);
        assertGreaterRank(file1, file2);

        file2.setState(ClaFileState.INGESTED);
        assertGreaterRank(file2, file1);

        file1.setState(ClaFileState.ANALYSED);
        assertGreaterRank(file1, file2);

        file2.setState(ClaFileState.ANALYSED);
        file2.setFullPath("a/b/c");
        assertGreaterRank(file2, file1);

        file1.setFullPath("a/b/c");
        file1.setBaseName("asdasd.docx");
        assertGreaterRank(file1, file2);
    }

    private void assertGreaterRank(ClaFile bigger, ClaFile smaller){
        Assert.assertTrue(fileCatService.rankClaFileRecord(bigger) > fileCatService.rankClaFileRecord(smaller));
    }

}
