package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.RoleRepository;
import com.cla.filecluster.repository.jpa.security.UserRepository;
import com.cla.filecluster.repository.jpa.security.UserSavedDataRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private AuthenticationProvider authenticationProvider;

    @Mock
    private EventAuditService eventAuditService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BizRoleService bizRoleService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private MessageHandler messageHandler;

    @Mock
    private UserSavedDataRepository userSavedDataRepository;

    @Before
    public void init() {
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @Test(expected = BadParameterException.class)
    public void testCreateUserNameNull() {
        UserDto userDto = new UserDto("any",null,"testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateUserNameEmpty() {
        UserDto userDto = new UserDto("any","","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateUserNameSpaces() {
        UserDto userDto = new UserDto("any","  ","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test
    public void testCreateUserNameContainsLdapDomainDelim() {
        Arrays.stream(UserService.LdapDomainDelim.values()).forEach(ldapDomainDelim -> {
            try {
                String badUsername = "username" + ldapDomainDelim;
                UserDto userDto = new UserDto("name", badUsername, "testCreateUserNameContainsLdapDomainDelim", "admin");
                userService.createUserOnCurrentAccount(userDto);
                Assert.fail("UserService should not allow to create a user with username: " + badUsername + " that contains: " + ldapDomainDelim);
            } catch (BadRequestException e) {
                // this is expected
            }
        });
    }

    @Test
    public void testCreateUserNameOk() {
        UserDto userDto = new UserDto("any","gfsdgfs","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateNameNull() {
        UserDto userDto = new UserDto(null,"any","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateNameEmpty() {
        UserDto userDto = new UserDto("","any","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateNameSpaces() {
        UserDto userDto = new UserDto("   ","any","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test
    public void testCreateNameOk() {
        UserDto userDto = new UserDto("any","any","testCreateDeleteUser","admin");
        userService.createUserOnCurrentAccount(userDto);
    }

    @Test
    public void validEmail() {
        assertEquals(null, userService.validEmail(null));
        assertEquals(null, userService.validEmail(""));
        assertEquals(null, userService.validEmail("    "));
        assertEquals("rtyy@gmail.com", userService.validEmail("rtyy@gmail.com"));
        assertEquals("rtyy@gmail.com", userService.validEmail("  rtyy@gmail.com  "));
    }

    @Test(expected = BadRequestException.class)
    public void invalidEmail() {
        userService.validEmail("drfgdgdfg");
    }

    @Test(expected = BadRequestException.class)
    public void invalidEmail2() {
        userService.validEmail("drfgdgdfg.com");
    }
}