package com.cla.filecluster.service.categories;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Category(UnitTestCategory.class)
public class AssociationUtilsTest {

    @Test
    public void testSetActiveAssociationValues() {
        List<String> ids = Lists.newArrayList(
                "g.2.17.MANUAL.f1;g.2.15.MANUAL.f0",
                "dt.g.1.3..file;dt.g.1.5..f1;dt.g.1.2..f0",
                "g.2.18.MANUAL.file;g.2.17.MANUAL.f1;g.2.15.MANUAL.f0",
                "dt.g.1.5..f1;dt.g.1.2..f0",
                "g.2.15.MANUAL.f0",
                "dept.15.f0",
                "dept.5.f1;dept.15.f0",
                "dt.g.1.2..f0",
                null);
        Set<String> activeAssociations = new HashSet<>();

        AssociationUtils.setActiveAssociationValues(ids, activeAssociations);
        assertEquals(8, activeAssociations.size());
        assertTrue(activeAssociations.contains("g.2.17"));
        assertTrue(activeAssociations.contains("dt.g.1.3"));
        assertTrue(activeAssociations.contains("g.2.18"));
        assertTrue(activeAssociations.contains("dt.g.1.5"));
        assertTrue(activeAssociations.contains("g.2.15"));
        assertTrue(activeAssociations.contains("dept.15"));
        assertTrue(activeAssociations.contains("dept.5"));
        assertTrue(activeAssociations.contains("dt.g.1.2"));
    }
}