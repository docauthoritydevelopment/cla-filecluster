package com.cla.filecluster.service.plugin;

import com.cla.common.domain.dto.messages.action.ActionPluginConfig;
import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import com.cla.filecluster.domain.entity.alert.Alert;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by: yael
 * Created on: 1/15/2018
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class ActionsPluginManagerTest {

    private BlockingQueue<ActionPluginRequest> pluginActionQueue;

    @InjectMocks
    private ActionsPluginManager actionsPluginManager;

    @Mock
    private ApplicationContext applicationContext;

    @Mock
    private AlertDBActionPlugin alertDBActionPlugin;

    @Before
    public void init() {
        pluginActionQueue = new LinkedBlockingQueue<>();
        ReflectionTestUtils.setField(actionsPluginManager, "threadPoolSize", 1);
        ReflectionTestUtils.setField(actionsPluginManager, "actionPluginRequestQueue", pluginActionQueue);
        actionsPluginManager.init();
    }

    @After
    public void cleanup() {
        actionsPluginManager.destroy();
    }

    @Test
    public void test() {
        ActionPluginConfig config = new ActionPluginConfig("alertDBActionPlugin");
        when(applicationContext.getBean(config.getPluginId(), ActionPlugin.class)).thenReturn(alertDBActionPlugin);
        Alert alert = new Alert();
        ActionPluginRequest request = new ActionPluginRequest(alert, config);
        pluginActionQueue.add(request);
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
        }
        verify(alertDBActionPlugin, times(1)).handleAction(request);
    }
}
