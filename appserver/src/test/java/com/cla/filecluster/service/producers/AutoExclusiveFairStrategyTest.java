package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.StrategyType;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.when;

/**
 * Created by: yael
 * Created on: 1/25/2018
 */
@RunWith(MockitoJUnitRunner.class)
public class AutoExclusiveFairStrategyTest {

    @Mock
    private IngestFinalizationService ingestFinalizationService;

    @Mock
    private UserService userService;

    @Mock
    private SolrTaskService solrTaskService;

    @Mock
    private JobManagerService jobManager;

    @After
    public void reset_mocks() {
        Mockito.reset(jobManager);
        Mockito.reset(userService);
        Mockito.reset(ingestFinalizationService);
    }

    private TaskProducingStrategyConfig getConfig() {
        TaskProducingStrategyConfig config = new TaskProducingStrategyConfig();
        config.setSelectedStrategy(StrategyType.AUTO_EXCLUSIVE);
        config.setIngestTasksQueueLimit(1000);
        config.setAnalyzeTasksQueueLimit(1000);
        config.setExclusiveMode(JobType.INGEST);
        config.setTimeoutGraceSec(TimeUnit.HOURS.toSeconds(1));
        config.setCooldownTasksThreshold(10);
        config.setHighThreshold(1000);
        config.setLowThreshold(200);
        config.setPercentageThreshold(20);
        return config;
    }

    private AutoExclusiveFairStrategy getAutoExclusiveFairStrategy() {
        return new AutoExclusiveFairStrategy(userService, ingestFinalizationService, jobManager, solrTaskService, getConfig());
    }

    private List<SimpleTask> generateListBySize(int size) {
        List<SimpleTask> tasks = new ArrayList<>();
        SimpleTask task = new SimpleTask();
        for (int i = 0; i < size; ++i) {
            tasks.add(task);
        }
        return tasks;
    }

    private Table<Long, TaskState, Number> generateTableBySize(int size) {
        return generateTableBySize(size, TaskState.NEW);
    }

    private Table<Long, TaskState, Number> generateTableBySize(int size, TaskState state) {
        Table<Long, TaskState, Number> taskStatesByType = HashBasedTable.create();
        for (int i = 0; i < size; ++i) {
            taskStatesByType.put(5L, state, 6L);
        }
        return taskStatesByType;
    }

    private Table<Long, TaskState, Number> setJobManagerResults(int analyzeSize, int ingestSize) {
        when(jobManager.getTasksByTypeAndState(TaskType.ANALYZE_CONTENT, TaskState.NEW, JobState.IN_PROGRESS, JobState.PENDING)).thenReturn(generateListBySize(analyzeSize));
        when(jobManager.getTasksByTypeAndState(TaskType.INGEST_FILE, TaskState.NEW, JobState.IN_PROGRESS, JobState.PENDING)).thenReturn(generateListBySize(ingestSize));
        Table<Long, TaskState, Number> taskStatesByType = generateTableBySize(30);
        when(jobManager.getTaskStatesByType(TaskType.INGEST_FILE)).thenReturn(taskStatesByType);
        return taskStatesByType;
    }

    @Test
    public void testSwitchToAnalyzeHighThreshold() {
        AutoExclusiveFairStrategy st = getAutoExclusiveFairStrategy();
        Assert.assertEquals(JobType.INGEST, st.getCurrentExclusiveMode());
        Table<Long, TaskState, Number> taskStatesByType = setJobManagerResults(2000, 200);
        st.nextTaskGroup(TaskType.ANALYZE_CONTENT, taskStatesByType, null);
        Assert.assertEquals(JobType.ANALYZE, st.getCurrentExclusiveMode());
    }

    @Test
    public void testSwitchToAnalyzeLowThreshold() {
        AutoExclusiveFairStrategy st = getAutoExclusiveFairStrategy();
        Assert.assertEquals(JobType.INGEST, st.getCurrentExclusiveMode());
        Table<Long, TaskState, Number> taskStatesByType = setJobManagerResults(400, 100);
        st.nextTaskGroup(TaskType.ANALYZE_CONTENT, taskStatesByType, null);
        Assert.assertEquals(JobType.ANALYZE, st.getCurrentExclusiveMode());
    }

    @Test
    public void testDontSwitchToAnalyzeThresholdNotReached() {
        AutoExclusiveFairStrategy st = getAutoExclusiveFairStrategy();
        Assert.assertEquals(JobType.INGEST, st.getCurrentExclusiveMode());
        Table<Long, TaskState, Number> taskStatesByType = setJobManagerResults(100, 100);
        Collection<SimpleTask> tasks = st.nextTaskGroup(TaskType.ANALYZE_CONTENT, taskStatesByType, null);
        Assert.assertEquals(JobType.INGEST, st.getCurrentExclusiveMode());
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void testSwitchToAnalyzeZero() {
        AutoExclusiveFairStrategy st = getAutoExclusiveFairStrategy();
        Assert.assertEquals(JobType.INGEST, st.getCurrentExclusiveMode());
        Table<Long, TaskState, Number> taskStatesByType = setJobManagerResults(10, 0);
        st.nextTaskGroup(TaskType.ANALYZE_CONTENT, taskStatesByType, null);
        Assert.assertEquals(JobType.ANALYZE, st.getCurrentExclusiveMode());
    }
}
