package com.cla.filecluster.service.crawler.analyze;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by: yael
 * Created on: 12/4/2017
 */
@Category(UnitTestCategory.class)
public class SimpleElectionStrategyTest extends JoinGroupsStrategyTestUtil {

    @Test
    public void testSimpleStrategy(){
        init();
        SimpleElectionStrategy strat = new SimpleElectionStrategy();
        JoinGroupsElectionResult result = strat.electTargetGroup(Lists.newArrayList(group10, group1000, groupWithAssoc1), null, null);
        Assert.assertEquals(group10, result.getElectedGroup());
        Assert.assertEquals(2, result.getAcceptedGroups().size());
        Assert.assertTrue(result.getAcceptedGroups().containsAll(Lists.newArrayList(group1000, groupWithAssoc1)));
        Assert.assertEquals(0, result.getRejectedGroups().size());

        result = strat.electTargetGroup(Lists.newArrayList(group1000, group10, groupWithAssoc1), null, null);
        Assert.assertEquals(group1000, result.getElectedGroup());
    }
}
