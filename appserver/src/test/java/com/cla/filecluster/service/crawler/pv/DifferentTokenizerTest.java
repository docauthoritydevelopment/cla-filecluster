package com.cla.filecluster.service.crawler.pv;

import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.utils.Pair;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by: yael
 * Created on: 2/15/2018
 */
@Category(UnitTestCategory.class)
public class DifferentTokenizerTest {

    private ExtractTokenizer extractTokenizer;
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Before
    public void init() {
        extractTokenizer = new ExtractTokenizer();
        nGramTokenizerHashed = new NGramTokenizerHashed();

        List<String> removalStrs = new ArrayList<>();
        removalStrs.add("999999999");

        ReflectionTestUtils.setField(extractTokenizer, "removalStrs", removalStrs);
        ReflectionTestUtils.setField(extractTokenizer, "ignoreChars", "[\\-\\']");
        ReflectionTestUtils.setField(extractTokenizer, "separatorChars", "");
        ReflectionTestUtils.setField(extractTokenizer, "minValueLength", 3);

        ReflectionTestUtils.setField(nGramTokenizerHashed, "removalStrs", removalStrs);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "ignoreChars", "[\\-\\']");
        ReflectionTestUtils.setField(nGramTokenizerHashed, "separatorChars", "");
        ReflectionTestUtils.setField(nGramTokenizerHashed, "minValueLength", 3);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "reuseTextTokenizer", true);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "removeValuesBellowLength", "2");
    }

    @Test
    public void test1() {
        Pair<String, String> res = test("10/1/01"); // SHOULD BE TRUE!
        assertFalse(res.getKey().equals(res.getValue())); // cause of bug !!! extract tokenizer should not do zeroLeadingNumber replacement
    }

    @Test
    public void test2() {
        Pair<String, String> res = test("enron");
        assertTrue(res.getKey().equals(res.getValue()));
    }

    @Test
    public void test3() {
        Pair<String, String> res = test("en;ron");
        assertTrue(res.getKey().equals(res.getValue()));
    }

    public Pair<String, String> test(String phrase) {
        StringBuilder sb = new StringBuilder();
        sb.append("phrase '");
        sb.append(phrase);

        String hashed1 = extractTokenizer.getHash(phrase);
        sb.append("'\n hashed (extractTokenizer.getHash): ");
        sb.append(hashed1);

        String hashed2 = nGramTokenizerHashed.getHash(phrase, null);
        sb.append("'\n hashed CORRECT (nGramTokenizerHashed.getHash): ");
        sb.append(hashed2);

        System.out.println(sb.toString());
        return Pair.of(hashed1, hashed2);
    }
}
