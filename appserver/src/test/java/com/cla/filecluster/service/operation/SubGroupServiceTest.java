package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.pv.RefinementRuleDto;
import com.cla.common.domain.dto.pv.RefinementRuleField;
import com.cla.common.domain.dto.pv.RefinementRulePredicate;
import com.cla.common.domain.dto.pv.RuleOperator;
import com.cla.filecluster.LambdaMatcher;
import com.cla.filecluster.domain.dto.FileDTOSearchResult;
import com.cla.filecluster.domain.dto.SolrExtractionEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.FileDTOSearchRepository;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.RefinementRuleService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.extractor.entity.ContentSearchTextService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.concurrent.Callable;
import java.util.*;


import static com.cla.filecluster.service.operation.SubGroupService.OTHERS_SUFFIX;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class SubGroupServiceTest {

    @Mock
    private RefinementRuleService refinementRuleService;

    @Mock
    private GroupsService groupsService;

    @Mock
    private FileDTOSearchRepository fileDTOSearchRepository;

    @Mock
    private ContentSearchTextService contentSearchTextService;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private ContentMetadataService contentMetadataService;

    @Mock
    private GroupsApplicationService groupsApplicationService;

    @Mock
    private SolrFileGroupRepository fileGroupRepository;

    @Mock
    private DBTemplateUtils DBTemplateUtils;

    @InjectMocks
    private SubGroupService subGroupService = new SubGroupService();

    private GroupLockService groupLockService;

    @Before
    public void init() {
        groupLockService = new GroupLockService();
        groupLockService.init();
        ReflectionTestUtils.setField(subGroupService, "groupLockService", groupLockService);
        doAnswer(invocation -> {
            Runnable arg0 = (Runnable)invocation.getArguments()[0];
            arg0.run();
            return null;
        }).when(DBTemplateUtils).doInTransaction(any(Runnable.class), anyInt());

        doAnswer(invocation -> {
            Callable arg0 = (Callable)invocation.getArguments()[0];
            return arg0.call();
        }).when(DBTemplateUtils).doInTransaction(any(Callable.class), anyInt());

        //when(DBTemplateUtils.doInTransaction(any(Callable.class)))
         //       .then(i -> ((Callable<Page<ContentMetadata>>)i.getArguments()[0]).call());
    }

    @After
    public void resetMocks() {
        Mockito.reset(fileGroupRepository);
        Mockito.reset(groupsApplicationService);
        Mockito.reset(contentMetadataService);
        Mockito.reset(fileDTOSearchRepository);
        Mockito.reset(groupsService);
        Mockito.reset(refinementRuleService);
        Mockito.reset(contentSearchTextService);
    }

    @Test
    public void applyRefinementRulesForGroup() {
        FileGroup rawAnalysisGroup = getRawAnalysisGroup();
        when(groupsService.findById(rawAnalysisGroup.getId())).thenReturn(rawAnalysisGroup);

        FileGroup aSubGroup = getSubGroup(rawAnalysisGroup, "sub1");

        List<RefinementRuleDto> rules = getRules(rawAnalysisGroup.getId());
        rules.get(0).setSubgroupId(aSubGroup.getId());
        when(refinementRuleService.getByRawAnalysisGroupIdAll(rawAnalysisGroup.getId())).thenReturn(rules);

        when(groupsService.findSubGroupsForRefinedGroup(rawAnalysisGroup.getId())).thenReturn(Collections.singletonList(aSubGroup));

        List<ContentMetadata> contents = new ArrayList<>();
        contents.add(new ContentMetadata()); contents.get(0).setId(1L);
        List<ContentMetadata> contents2 = new ArrayList<>();
        when(contentMetadataService.findByAnalysisGroup(anyString(), anyInt(), anyInt())).thenReturn(contents, contents2);

        FileDTOSearchResult res = new FileDTOSearchResult();
        when(fileDTOSearchRepository.find(any(),any())).thenReturn(res);

        FileGroup othersGroup = getOthersGroup(rawAnalysisGroup);
        when(groupsService.createNewAnalysisSubGroup(rawAnalysisGroup.getName() + OTHERS_SUFFIX, rawAnalysisGroup.getId(), true)).thenReturn(othersGroup);

        subGroupService.firstStepMoveAllBackToParent(rawAnalysisGroup.getId());
        subGroupService.sndStepManageRules(rawAnalysisGroup.getId());
        subGroupService.trdStepMoveFilesToSubGroups(rawAnalysisGroup.getId());

        verify(contentMetadataService, times(1)).changeAnalysisGroupIdAll(rawAnalysisGroup.getId(), aSubGroup.getId());
        verify(contentMetadataService, times(1)).changeUserGroupIdAll(rawAnalysisGroup.getId(), aSubGroup.getId());

        LambdaMatcher<String> raw = new LambdaMatcher<>(e -> e.equalsIgnoreCase(rawAnalysisGroup.getId()));
        LambdaMatcher<Set<String>> subList = new LambdaMatcher<>(e -> e.size() == 1 && e.contains(aSubGroup.getId()));

        //verify(groupsApplicationService, times(1)).updateAnalysisGroupsCatFile(Mockito.argThat(raw), Mockito.argThat(subList)); TODO
        //verify(groupsApplicationService, times(1)).updateUserGroupsCatFile(Mockito.argThat(raw), Mockito.argThat(subList)); TODO

        LambdaMatcher<List<Long>> l = new LambdaMatcher<>(e-> e.size() == 1 && e.contains(1L));
        LambdaMatcher<String> sub = new LambdaMatcher<>(e -> e.equalsIgnoreCase(othersGroup.getId()));

        verify(contentMetadataService, times(1)).updateAnalysisGroupIdByContentIds(Mockito.argThat(l), Mockito.argThat(sub));
        verify(contentMetadataService, times(1)).updateUserGroupIdByContentIds(Mockito.argThat(l), Mockito.argThat(sub), Mockito.argThat(raw));

        verify(groupsService, times(1)).updateAnalysisGroupsCatFileByContents(Mockito.argThat(sub), Mockito.argThat(l),Mockito.eq(true));
        verify(groupsService, times(1)).updateUserGroupsCatFileByContents(Mockito.argThat(raw), Mockito.argThat(sub), Mockito.argThat(l), Mockito.eq(true));

        //verify(fileCatService, times(4)).commitToSolr();
        verify(groupsService, times(1)).updateGroupFiles(othersGroup.getId(), null, 1L,0);

        verify(groupsService, times(1)).updateGroupHasNoFiles(rawAnalysisGroup.getId());
    }

    @Test
    public void moveContentsFromRawToSubGroupsByRules() {

        FileGroup rawAnalysisGroup = getRawAnalysisGroup();
        when(groupsService.findById(rawAnalysisGroup.getId())).thenReturn(rawAnalysisGroup);

        List<RefinementRuleDto> rules = getRules(rawAnalysisGroup.getId());
        when(refinementRuleService.getByRawAnalysisGroupIdActive(rawAnalysisGroup.getId())).thenReturn(rules);

        when(groupsService.findSubGroupsForRefinedGroup(rawAnalysisGroup.getId())).thenReturn(new ArrayList<>());

        List<ContentMetadata> contents = new ArrayList<>();
        contents.add(new ContentMetadata()); contents.get(0).setId(1L);
        List<ContentMetadata> contents2 = new ArrayList<>();
        when(contentMetadataService.findByAnalysisGroup(anyString(), anyInt(), anyInt())).thenReturn(contents, contents2);

        FileDTOSearchResult res = new FileDTOSearchResult();
        when(fileDTOSearchRepository.find(any(),any())).thenReturn(res);

        FileGroup othersGroup = getOthersGroup(rawAnalysisGroup);
        when(groupsService.createNewAnalysisSubGroup(rawAnalysisGroup.getName() + OTHERS_SUFFIX, rawAnalysisGroup.getId(), true)).thenReturn(othersGroup);

        subGroupService.moveContentsFromRawToSubGroupsByRules(rawAnalysisGroup.getId());

        LambdaMatcher<List<Long>> l = new LambdaMatcher<>(e-> e.size() == 1 && e.contains(1L));
        LambdaMatcher<String> sub = new LambdaMatcher<>(e -> e.equalsIgnoreCase(othersGroup.getId()));
        LambdaMatcher<String> raw = new LambdaMatcher<>(e -> e.equalsIgnoreCase(rawAnalysisGroup.getId()));

        verify(contentMetadataService, times(1)).updateAnalysisGroupIdByContentIds(Mockito.argThat(l), Mockito.argThat(sub));
        verify(contentMetadataService, times(1)).updateUserGroupIdByContentIds(Mockito.argThat(l), Mockito.argThat(sub), Mockito.argThat(raw));

        verify(groupsService, times(1)).updateAnalysisGroupsCatFileByContents(Mockito.argThat(sub), Mockito.argThat(l), Mockito.eq(true));
        verify(groupsService, times(1)).updateUserGroupsCatFileByContents(Mockito.argThat(raw), Mockito.argThat(sub), Mockito.argThat(l), Mockito.eq(true));
        //verify(fileCatService, times(2)).commitToSolr();
        verify(groupsService, times(1)).updateGroupFiles(othersGroup.getId(), null, 1L,0);
    }

    @Test
    public void renameOthersGroupIfExists() {

        FileGroup rawAnalysisGroup = getRawAnalysisGroup();

        FileGroup othersGroup = getOthersGroup(rawAnalysisGroup);

        List<FileGroup> subs = new ArrayList<>();  subs.add(othersGroup);
        when(groupsService.findSubGroupsForRefinedGroup(rawAnalysisGroup.getId())).thenReturn(subs);

        subGroupService.renameOthersGroupIfExists(rawAnalysisGroup);

        verify(groupsService, times(1)).updateGroupName(othersGroup.getId(), rawAnalysisGroup.getName() + OTHERS_SUFFIX);
    }

    @Test
    public void getFileSubGroupOther() {

        FileGroup rawAnalysisGroup = getRawAnalysisGroup();

        when(groupsService.findById(rawAnalysisGroup.getId())).thenReturn(rawAnalysisGroup);
        List<RefinementRuleDto> rules = getRules(rawAnalysisGroup.getId());
        FileGroup othersGroup = getOthersGroup(rawAnalysisGroup);
        List<FileGroup> subGroupsList = Collections.singletonList(othersGroup);

        when(refinementRuleService.getByRawAnalysisGroupIdActive(rawAnalysisGroup.getId())).thenReturn(rules);
        when(groupsService.findSubGroupsForRefinedGroup(rawAnalysisGroup.getId())).thenReturn(subGroupsList);

        FileDTOSearchResult res = new FileDTOSearchResult();
        when(fileDTOSearchRepository.find(any(),any())).thenReturn(res);

        String result = subGroupService.getFileSubGroup(rawAnalysisGroup.getId(), 1L);
        assertEquals(othersGroup.getId(), result);
    }

    @Test
    public void getFileSubGroupFailCheck() {

        FileGroup rawAnalysisGroup = getRawAnalysisGroup();

        boolean res = groupLockService.getLockForRawAnalysisGroup(rawAnalysisGroup.getId());
        try {
            assertEquals(true, res);
            String result = subGroupService.getFileSubGroup(rawAnalysisGroup.getId(), 1L);
            assertEquals(null, result);
        } finally {
            groupLockService.releaseLockForRawAnalysisGroup(rawAnalysisGroup.getId());
        }
    }

    @Test
    public void getFileSubGroupNotOther() {

        FileGroup rawAnalysisGroup = getRawAnalysisGroup();

        when(groupsService.findById(rawAnalysisGroup.getId())).thenReturn(rawAnalysisGroup);
        List<RefinementRuleDto> rules = getRules(rawAnalysisGroup.getId());
        FileGroup othersGroup = getOthersGroup(rawAnalysisGroup);
        List<FileGroup> subGroupsList = Collections.singletonList(othersGroup);

        when(refinementRuleService.getByRawAnalysisGroupIdActive(rawAnalysisGroup.getId())).thenReturn(rules);
        when(groupsService.findSubGroupsForRefinedGroup(rawAnalysisGroup.getId())).thenReturn(subGroupsList);
        FileGroup newSubGroup = getSubGroup(rawAnalysisGroup, "sub1");
        when(groupsService.createNewAnalysisSubGroup(newSubGroup.getName(), rawAnalysisGroup.getId(), true)).thenReturn(newSubGroup);

        SolrExtractionEntity entity = new SolrExtractionEntity();
        entity.setId("1");
        FileDTOSearchResult res = new FileDTOSearchResult(
                Collections.singletonList(entity),null,null,null);
        when(fileDTOSearchRepository.find(any(),any())).thenReturn(res);

        when(contentSearchTextService.convertContentSearchText(any(), anyBoolean())).thenReturn("565DFBDHDHDHDD");

        String result = subGroupService.getFileSubGroup(rawAnalysisGroup.getId(), 1L);
        assertEquals(newSubGroup.getId(), result);
        verify(refinementRuleService, times(1)).updateRefinementRuleSubGroup(rules.get(0).getId(), newSubGroup.getId());
    }

    private List<RefinementRuleDto> getRules(String rawAnalysisGroupId) {

        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.CONTAINS);
        p.setField(RefinementRuleField.CONTENT);

        List<RefinementRuleDto> rules = new ArrayList<>();
        RefinementRuleDto r = new RefinementRuleDto();
        r.setActive(true); r.setPriority(1);
        r.setRawAnalysisGroupId(rawAnalysisGroupId);
        r.setSubgroupName("sub1");
        r.setPredicate(p);
        rules.add(r);

        return rules;
    }

    private FileGroup getRawAnalysisGroup() {
        FileGroup rawAnalysisGroup = new FileGroup();
        rawAnalysisGroup.setId("fghf5555555hfghf"); rawAnalysisGroup.setName("dfgdfgdfg");
        rawAnalysisGroup.setHasSubgroups(true);
        rawAnalysisGroup.setGroupType(GroupType.ANALYSIS_GROUP);
        return rawAnalysisGroup;
    }

    private FileGroup getOthersGroup(FileGroup rawAnalysisGroup) {
        FileGroup othersGroup = new FileGroup();
        othersGroup.setName(rawAnalysisGroup.getId() + OTHERS_SUFFIX);
        othersGroup.setId("fghfhfghfdddddddd");
        othersGroup.setGroupType(GroupType.SUB_GROUP);
        othersGroup.setRawAnalysisGroupId(rawAnalysisGroup.getId());
        return othersGroup;
    }

    private FileGroup getSubGroup(FileGroup rawAnalysisGroup, String name) {
        FileGroup othersGroup = new FileGroup();
        othersGroup.setName(name);
        othersGroup.setId("ddddddd"+name);
        othersGroup.setGroupType(GroupType.SUB_GROUP);
        othersGroup.setRawAnalysisGroupId(rawAnalysisGroup.getId());
        return othersGroup;
    }
}