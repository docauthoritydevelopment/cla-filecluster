package com.cla.filecluster.service.crawler;

import com.cla.common.domain.dto.pv.RefinementRuleDto;
import com.cla.common.domain.dto.pv.RefinementRuleField;
import com.cla.common.domain.dto.pv.RefinementRulePredicate;
import com.cla.common.domain.dto.pv.RuleOperator;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.RefinementRule;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.RefinementRuleRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class RefinementRuleServiceTest {

    @Mock
    private RefinementRuleRepository refinementRuleRepository;

    @Mock
    private GroupsService groupsService;

    @Mock
    private EventAuditService eventAuditService;

    @Mock
    private ExtractTokenizer extractTokenizer;

    @Mock
    private MessageHandler messageHandler;

    @InjectMocks
    private RefinementRuleService refinementRuleService;

    private Random r = new Random(System.currentTimeMillis());

    @Before
    public void init() {
        when(refinementRuleRepository.save(any(RefinementRule.class))).thenAnswer(i -> {
            RefinementRule rule = (RefinementRule)i.getArguments()[0];

            if (rule.getId() == null || rule.getId() == 0) {
                Long id = Math.abs(r.nextLong());
                if (id == 0) id = Math.abs(r.nextLong());
                rule.setId(id);
            }
            return rule;
        });
        when(refinementRuleRepository.getOne(any(Long.class))).thenAnswer(i -> {
            Long ruleId = (Long)i.getArguments()[0];
            RefinementRule rule = getRule();
            rule.setId(ruleId);
            return rule;
        });
        when(refinementRuleRepository.findById(any(Long.class))).thenAnswer(i -> {
            Long ruleId = (Long)i.getArguments()[0];
            RefinementRule rule = getRule();
            rule.setId(ruleId);
            return Optional.of(rule);
        });
        refinementRuleService.init();
        when(extractTokenizer.getHash(anyString())).thenAnswer(i -> i.getArguments()[0]);
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @After
    public void resetMocks() {
        Mockito.reset(groupsService);
        Mockito.reset(refinementRuleRepository);
    }

    @Test(expected = BadRequestException.class)
    public void ruleEmpty() {
        RefinementRuleDto rule = new RefinementRuleDto();
        refinementRuleService.createRefinementRule(rule);
    }

    @Test(expected = BadRequestException.class)
    public void ruleNoPriority() {
        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setRawAnalysisGroupId("dfgdfg");
        rule.setPredicate(getPredicate());
        refinementRuleService.createRefinementRule(rule);
    }

    @Test(expected = BadRequestException.class)
    public void ruleNoParent() {
        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setPredicate(getPredicate());
        refinementRuleService.createRefinementRule(rule);
    }

    @Test(expected = BadRequestException.class)
    public void createRuleParentDoesntExists() {
        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdf33g");
        rule.setPredicate(getPredicate());
        refinementRuleService.createRefinementRule(rule);
    }

    @Test
    public void updateRuleSubGroupNameAfterCreated() {
        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setId(5L);
        rule.setSubgroupName("name");
        rule.setSubgroupId("dfgdfgd");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdf33g");
        rule.setPredicate(getPredicate());

        RefinementRule one = new RefinementRule();
        one.setId(5L);
        one.setSubgroupName("name22");
        one.setSubgroupId("dfgdfgd");
        one.setPriority(1);
        one.setRawAnalysisGroupId("dfgdf33g");

        when(refinementRuleRepository.findById(5L)).thenReturn(Optional.of(one));

        refinementRuleService.updateRefinementRule(rule, true);

        verify(groupsService, times(1)).updateGroupNameInDB("dfgdfgd","name");
    }

    @Test
    public void updateRule() {
        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setId(5L);
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdf33g");
        rule.setPredicate(getPredicate());

        RefinementRule one = new RefinementRule();
        one.setId(5L);
        one.setSubgroupName("namerrr");
        one.setPriority(2);
        one.setRawAnalysisGroupId("dfgdf33g");

        when(refinementRuleRepository.findById(5L)).thenReturn(Optional.of(one));

        RefinementRuleDto res = refinementRuleService.updateRefinementRule(rule, true);
        assertEquals(1, res.getPriority());

    }

    @Test
    public void createRuleSuccess() {
        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdfg");
        rule.setPredicate(getPredicate());

        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());
        refinementRuleService.createRefinementRule(rule);
    }

    @Test(expected = BadRequestException.class)
    public void create2RulesSamePriority() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());

        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdfg");
        rule.setPredicate(getPredicate());
        refinementRuleService.createRefinementRule(rule);

        rule.setSubgroupName("222");
        refinementRuleService.createRefinementRule(rule);
    }

    @Test(expected = BadRequestException.class)
    public void create2RulesSameSubGroupName() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());

        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdfg");
        rule.setPredicate(getPredicate());
        refinementRuleService.createRefinementRule(rule);

        rule.setPriority(2);
        refinementRuleService.createRefinementRule(rule);
    }

    @Test
    public void create2Rules() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());

        RefinementRuleDto rule = new RefinementRuleDto();
        rule.setSubgroupName("name");
        rule.setPriority(1);
        rule.setRawAnalysisGroupId("dfgdfg");
        rule.setPredicate(getPredicate());
        refinementRuleService.createRefinementRule(rule);

        rule.setPriority(2);
        rule.setSubgroupName("name2");
        refinementRuleService.createRefinementRule(rule);
    }

    @Test
    public void testCreateByPhrase() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());
        List<String> phrases = new ArrayList<>();
        phrases.add("enron"); phrases.add("copper");
        List<RefinementRuleDto> rules = refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "fghf5555555hfghf", "dfgdfgdfg");
        assertEquals(2, rules.size());
        phrases.add("tin"); phrases.add("daily"); phrases.remove("enron");
        rules = refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "fghf5555555hfghf", "dfgdfgdfg");
        assertEquals(3, rules.size());
    }

    @Test
    public void testCreateByPhraseRemoveEmpty() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());
        List<String> phrases = new ArrayList<>();
        phrases.add("enron"); phrases.add("copper"); phrases.add(""); phrases.add("  "); phrases.add(null);
        List<RefinementRuleDto> rules = refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "fghf5555555hfghf", "dfgdfgdfg");
        assertEquals(2, rules.size());
    }

    @Test
    public void testCreateByPhraseRemoveStopWord() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());
        List<String> phrases = new ArrayList<>();
        phrases.add("enron"); phrases.add("copper"); phrases.add(" or "); phrases.add("and"); phrases.add(null);
        List<RefinementRuleDto> rules = refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "fghf5555Stop555hfghf", "dfgdfgdfg");
        assertEquals(2, rules.size());
    }

    @Test
    public void testCreateByPhraseRemoveMultiWord() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());
        List<String> phrases = new ArrayList<>();
        phrases.add("enron"); phrases.add("copper"); phrases.add(" ga ga "); phrases.add("enron sucks"); phrases.add(null);
        List<RefinementRuleDto> rules = refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "fghf555Multi5555hfghf", "dfgdfgdfg");
        assertEquals(2, rules.size());
    }

    @Test
    public void testCreateByPhraseDoubleWord() {
        when(groupsService.findById(any())).thenReturn(getRawAnalysisGroup());
        List<String> phrases = new ArrayList<>();
        phrases.add("enron"); phrases.add("copper"); phrases.add(" ga ga "); phrases.add("enron");

        boolean threwEx = false;
        try {
            refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "ffdsfsDoublefsdgfgf", "dfgdfgdfg");
        } catch (BadRequestException ex) {
            threwEx = true;
        }

        if (!threwEx) {
            Assert.fail("should have failed on double phrase");
        }
        assertEquals(0, refinementRuleService.getByRawAnalysisGroupId("ffdsfsDoublefsdgfgf").size()); // cache should be empty

        phrases.remove("enron");
        List<RefinementRuleDto> rules = refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, "ffdsfsDoublefsdgfgf", "dfgdfgdfg");
        assertEquals(2, rules.size());
    }

    private RefinementRule getRule() {
        RefinementRule r = new RefinementRule();
        r.setRawAnalysisGroupId("fghf5555555hfghf");
        return r;
    }

    private RefinementRulePredicate getPredicate() {
        RefinementRulePredicate p = new RefinementRulePredicate();
        p.setValues(Collections.singletonList("gsdfgs"));
        p.setOperator(RuleOperator.CONTAINS);
        p.setField(RefinementRuleField.CONTENT);
        return p;
    }

    private FileGroup getRawAnalysisGroup() {
        FileGroup rawAnalysisGroup = new FileGroup();
        rawAnalysisGroup.setId("fghf5555555hfghf"); rawAnalysisGroup.setName("dfgdfgdfg");
        rawAnalysisGroup.setHasSubgroups(true);
        return rawAnalysisGroup;
    }
}