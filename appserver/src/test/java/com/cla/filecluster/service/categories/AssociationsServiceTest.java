package com.cla.filecluster.service.categories;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.solr.common.SolrInputDocument;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class AssociationsServiceTest {

    @Mock
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Mock
    private FileTagService fileTagService;

    @Mock
    private DocTypeService docTypeService;

    @Mock
    private FunctionalRoleService functionalRoleService;

    @Mock
    private BizListItemService bizListItemService;

    @Mock
    private SolrFileGroupRepository fileGroupRepository;

    @InjectMocks
    private AssociationsService associationsService;

    @Before
    public void init() {
        KpiRecording kpiRecording = new KpiRecording("", "", 1, 1L, performanceKpiRecorder,"");
        when(performanceKpiRecorder.startRecording(anyString(), anyString(), anyInt())).thenReturn(kpiRecording);

        setField(associationsService, "populateTags2", true);
    }

    @After
    public void tearDown() {
        Mockito.reset(fileTagService);
        Mockito.reset(docTypeService);
        Mockito.reset(bizListItemService);
        Mockito.reset(functionalRoleService);
        Mockito.reset(fileGroupRepository);
    }

    @Test
    public void testAddTagDataToDocumentNoGroupsNoAssociations() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile(null, null);
        SolrFolderEntity folder = getFolder(null);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), null, null,false, folder);
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.TAGS,
                CatFileFieldType.TAGS_2,
                CatFileFieldType.SCOPED_TAGS,
                CatFileFieldType.DOC_TYPES,
                CatFileFieldType.ACTIVE_ASSOCIATIONS,
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderAssociation() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile(null, null);
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), null, null,false, folder);
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6..f0", "g.4.14.MANUAL.vdt.f0"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6", "g.4.14"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.TAGS,
                CatFileFieldType.TAGS_2,
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderDeptAssociation() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile(null, null);
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"departmentId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), null, null,false, folder);
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dept.6"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.TAGS,
                CatFileFieldType.TAGS_2,
                CatFileFieldType.DOC_TYPES,
                CatFileFieldType.SCOPED_TAGS,
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, "dept.6.f0");
    }

    @Test
    public void testAddTagDataToDocumentFolderAssociations() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile(null, null);
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), null, null,false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6", "g.4.43"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentGroupTagAssociation() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        SolrFolderEntity folder = getFolder(null);

        List<String> group1Associations = Lists.newArrayList(
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}"
        );
        fileGroup("g1", group1Associations);

        setFileTag(7L, 15L, true);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet( "7.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet( "type.7 tag.15.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet( "g.7.15.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet( "g.7.15"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.DOC_TYPES,
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentGroupDocTypeAssociation() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        SolrFolderEntity folder = getFolder(null);

        List<String> group1Associations = Lists.newArrayList(
                "{\"creationDate\":1568630495137,\"docTypeId\":7,\"implicit\":false}"
        );
        fileGroup("g1", group1Associations);

        setDocType(7L, "dt.6.7");

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet( "dt.6.7"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet( "dt.g.6.7..fileGroup"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet( "dt.g.6.7"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.TAGS,
                CatFileFieldType.TAGS_2,
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupAssociations() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        List<String> group1Associations = Lists.newArrayList(
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}"
        );
        fileGroup("g1", group1Associations);

        setFileTag(7L, 15L, true);
        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "7.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.7 tag.15.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0", "g.7.15.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6", "g.4.43", "g.7.15"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupSingleTagAssociations() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        List<String> associations = Lists.newArrayList(
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        List<String> group1Associations = Lists.newArrayList(
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}"
        );
        fileGroup("g1", group1Associations);

        setFileTag(4L, 15L, true);
        setFileTag(4L, 43L, true);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "4.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.4 tag.15.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet( "g.4.15.MANUAL.fileGroup;g.4.43.MANUAL.f0"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet( "g.4.15"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.DOC_TYPES,
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupsAssociations() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g2");
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        List<String> group1Associations = Lists.newArrayList(
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}"
        );
        fileGroup("g1", group1Associations);

        List<String> group2Associations = Lists.newArrayList(
                "{\"creationDate\":1568630501375,\"fileTagId\":45,\"implicit\":false}"
        );
        fileGroup("g2", group2Associations);

        setFileTag(7L, 15L, true);
        setFileTag(8L, 45L, true);
        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g2", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "7.15.MANUAL", "8.45.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.7 tag.15.MANUAL", "type.8 tag.45.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0", "g.7.15.MANUAL.fileGroup", "g.8.45.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6", "g.4.43", "g.7.15", "g.8.45"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupAssociationsDts() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        List<String> groupAssociations = Lists.newArrayList(
                "{\"creationDate\":1568630495137,\"docTypeId\":7,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}"
        );

        fileGroup("g1", groupAssociations);

        setFileTag(7L, 15L, true);
        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);
        setDocType(7L, "dt.6.7");

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "7.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.7 tag.15.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6.7..fileGroup;dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0", "g.7.15.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6", "dt.6.7"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6.7", "g.4.43", "g.7.15"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupAssociationsDtsSame() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        List<String> groupAssociations = Lists.newArrayList(
                "{\"creationDate\":1568630495137,\"docTypeId\":7,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}"
        );

        fileGroup("g1", groupAssociations);

        setFileTag(7L, 15L, true);
        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);
        setDocType(7L, "dt.7");

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "7.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.7 tag.15.MANUAL"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.7..fileGroup;dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0", "g.7.15.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6", "dt.7"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.7", "g.4.43", "g.7.15"));
        chkValuesEmpty(doc, Lists.newArrayList(
                CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupAssociationsOther() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}");
        SolrFolderEntity folder = getFolder(associations);

        List<String> groupAssociations = Lists.newArrayList(
                "{\"creationDate\":1568630495137,\"docTypeId\":7,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"functionalRoleId\":88,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"simpleBizListItemSid\":\"sid\",\"implicit\":false}"
        );

        fileGroup("g1", groupAssociations);

        setFileTag(7L, 15L, true);
        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);
        setDocType(7L, "dt.6.7");
        setDataRole(88L);
        setBizListItem("sid", 56L);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "7.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.7 tag.15.MANUAL", "bli.0.56.sid", "frole.88"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6.7..fileGroup;dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0", "g.7.15.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6", "dt.6.7"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6.7", "g.4.43", "g.7.15"));
        chkValue(doc, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, Sets.newHashSet("frole.88.fileGroup"));
        chkValue(doc, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, Sets.newHashSet("bli.0.56.sid.fileGroup"));
        chkDeptValue(doc, null);
    }

    @Test
    public void testAddTagDataToDocumentFolderGroupAssociationsOthers() {
        SolrInputDocument doc = new SolrInputDocument();
        SolrFileEntity claFile = getFile("g1", "g1");
        List<String> associations = Lists.newArrayList("{\"creationDate\":1568630495137,\"docTypeId\":6,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"fileTagId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"functionalRoleId\":43,\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}",
                "{\"creationDate\":1568630501375,\"simpleBizListItemSid\":\"sid2\",\"implicit\":false,\"originFolderId\":19527,\"originDepthFromRoot\":0}"
                );
        SolrFolderEntity folder = getFolder(associations);

        List<String> groupAssociations = Lists.newArrayList(
                "{\"creationDate\":1568630495137,\"docTypeId\":7,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"fileTagId\":15,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"functionalRoleId\":88,\"implicit\":false}",
                "{\"creationDate\":1568630501375,\"simpleBizListItemSid\":\"sid\",\"implicit\":false}"
        );

        fileGroup("g1", groupAssociations);

        setFileTag(7L, 15L, true);
        setFileTag(4L, 43L, true);
        setFileTag(4L, 14L, true);
        setDocType(6L, 4L, 14L,true);
        setDocType(7L, "dt.6.7");
        setDataRole(43L);
        setDataRole(88L);
        setBizListItem("sid2", 7L);
        setBizListItem("sid", 56L);

        associationsService.addTagDataToDocument(doc, claFile.getFileId(), new ArrayList<>(), "g1", "g1",false, folder);
        chkValue(doc, CatFileFieldType.TAGS, Sets.newHashSet("4.43.MANUAL", "7.15.MANUAL"));
        chkValue(doc, CatFileFieldType.TAGS_2, Sets.newHashSet("type.4 tag.43.MANUAL", "type.7 tag.15.MANUAL", "bli.0.56.sid", "frole.88", "frole.43", "bli.0.7.sid2"));
        chkValue(doc, CatFileFieldType.SCOPED_TAGS, Sets.newHashSet("dt.g.6.7..fileGroup;dt.g.6..f0", "g.4.43.MANUAL.f0;g.4.14.MANUAL.vdt.f0", "g.7.15.MANUAL.fileGroup"));
        chkValue(doc, CatFileFieldType.DOC_TYPES, Sets.newHashSet("dt.6", "dt.6.7"));
        chkValue(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, Sets.newHashSet("dt.g.6.7", "g.4.43", "g.7.15"));
        chkValue(doc, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, Sets.newHashSet("frole.88.fileGroup", "frole.43.f0"));
        chkValue(doc, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, Sets.newHashSet("bli.0.56.sid.fileGroup", "bli.0.7.sid2.f0"));
        chkDeptValue(doc, null);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void fileGroup(String id, List<String> associations) {
        FileGroup userGroup = new FileGroup();
        userGroup.setId(id);
        userGroup.setAssociations(associations);
        when(fileGroupRepository.getById(id)).thenReturn(Optional.of(userGroup));
    }

    private SolrFileEntity getFile(String groupId, String userGroupId) {
        SolrFileEntity entity = new SolrFileEntity();
        entity.setFileId(5L);
        entity.setFolderId(87L);
        entity.setAnalysisGroupId(groupId);
        entity.setUserGroupId(userGroupId);
        return entity;
    }

    private SolrFolderEntity getFolder(List<String> associations) {
        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(87L);
        folder.setAssociations(associations);
        return folder;
    }

    private void setFileTag(Long tagTypeId, Long tagId, boolean singleValue) {
        FileTagDto fileTag = new FileTagDto();
        FileTagTypeDto type = new FileTagTypeDto();
        fileTag.setId(tagId); fileTag.setType(type); fileTag.setFileTagAction(FileTagAction.MANUAL);
        type.setId(tagTypeId); type.setSingleValueTag(singleValue);
        when(fileTagService.getFromCache(tagId)).thenReturn(fileTag);
    }

    private FileTag createTag(Long tagTypeId, Long tagId, boolean singleValue) {
        FileTag fileTag = new FileTag();
        FileTagType tagType = new FileTagType();
        fileTag.setAction(FileTagAction.MANUAL);
        tagType.setId(tagTypeId);
        tagType.setSingleValueTag(singleValue);
        fileTag.setId(tagId);
        fileTag.setType(tagType);
        return fileTag;
    }

    private void setDocType(Long docTypeId) {
        setDocType( docTypeId, null, null, true, null);
    }

    private void setDocType(Long docTypeId, String identifier) {
        setDocType( docTypeId, null, null, true, identifier);
    }

    private void setDocType(Long docTypeId, Long tagTypeId, Long tagId, boolean singleValue) {
        setDocType( docTypeId, tagTypeId, tagId, singleValue, null);
    }

    private void setDocType(Long docTypeId, Long tagTypeId, Long tagId, boolean singleValue, String identifier) {
        DocTypeDto dto = new DocTypeDto();
        dto.setId(docTypeId);
        dto.setDocTypeIdentifier(identifier != null ? identifier : "dt."+docTypeId);
        dto.setSingleValue(true);
        when(docTypeService.getFromCache(docTypeId)).thenReturn(dto);

        DocType docType = new DocType();
        docType.setSingleValue(true);
        if (tagTypeId != null) {
            FileTagSetting fileTagSettings = new FileTagSetting();
            fileTagSettings.setDocType(docType);
            FileTag fileTagForDt = createTag(tagTypeId, tagId, singleValue);
            fileTagSettings.setFileTag(fileTagForDt);
            docType.setFileTagSettings(Sets.newHashSet(fileTagSettings));
        } else {
            docType.setFileTagSettings(Sets.newHashSet());
        }
        when(docTypeService.getDocTypeByIdWithTagSettings(docTypeId)).thenReturn(docType);
    }

    private void setDataRole(Long frId) {
        FunctionalRoleDto fr = new FunctionalRoleDto();
        fr.setId(frId);
        when(functionalRoleService.getFromCache(frId)).thenReturn(fr);
    }

    private void setBizListItem(String sid, Long listId) {
        SimpleBizListItemDto bli = new SimpleBizListItemDto();
        bli.setBizListId(listId);
        bli.setType(BizListItemType.CONSUMERS);
        bli.setBusinessId(sid);
        bli.setId(sid);
        when(bizListItemService.getFromCache(sid)).thenReturn(bli);
    }

    private void chkDeptValue(SolrInputDocument doc, String value) {
        String dept = doc.containsKey(CatFileFieldType.DEPARTMENT_ID.getSolrName()) ?
                (String) doc.getField(CatFileFieldType.DEPARTMENT_ID.getSolrName()).getValue() : null;
        if (value == null) {
            if (dept != null) assertTrue(dept.isEmpty());
        } else {
            assertEquals(value, dept);
        }
    }

    private void chkValue(SolrInputDocument doc, CatFileFieldType field, Set<String> expected) {
        Set<String> val = doc.containsKey(field.getSolrName()) ?
                (Set<String>) doc.getField(field.getSolrName()).getValue() : null;
        assertTrue(val != null);
        assertEquals(field.getSolrName(), expected, val);
    }

    private void chkValuesEmpty(SolrInputDocument doc, List<CatFileFieldType> fields) {
        fields.forEach(field -> chkValueEmpty(doc, field));
    }

    private void chkValueEmpty(SolrInputDocument doc, CatFileFieldType field) {
        Set<String> val = doc.containsKey(field.getSolrName()) ?
                (Set<String>) doc.getField(field.getSolrName()).getValue() : null;
        if (val != null) assertTrue(val.isEmpty());
    }
}