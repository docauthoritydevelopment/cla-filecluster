package com.cla.filecluster.service.operation;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by: yael
 * Created on: 5/27/2018
 */
@Category(UnitTestCategory.class)
public class GroupLockServiceTest {

    @Test
    public void testCantDoubleLock() {
        GroupLockService service = new GroupLockService();
        service.init();

        // lock group 1
        boolean result = service.getLockForRawAnalysisGroup("agfdhdgfhdfd");
        assertEquals(true, result);

        // lock group 2
        result = service.getLockForRawAnalysisGroup("agfdhdgfhdfd2222");
        assertEquals(true, result);

        // lock group 1 again
        result = service.getLockForRawAnalysisGroup("agfdhdgfhdfd");
        assertEquals(false, result);

        // release lock on both groups
        service.releaseLockForRawAnalysisGroup("agfdhdgfhdfd");
        service.releaseLockForRawAnalysisGroup("agfdhdgfhdfd2222");

        // lock group 2
        result = service.getLockForRawAnalysisGroup("agfdhdgfhdfd2222");
        assertEquals(true, result);

        // release lock group 2
        service.releaseLockForRawAnalysisGroup("agfdhdgfhdfd2222");
    }

    @Test
    public void testGroupLockIndication() {

        GroupLockService service = new GroupLockService();
        service.init();

        // lock group 1
        boolean result = service.getLockForRawAnalysisGroup("agfdhdgfhdfd");
        assertEquals(true, result);

        // check lock
        result = service.isRawAnalysisGroupLockTaken("agfdhdgfhdfd");
        assertEquals(true, result);

        // release lock group 1
        service.releaseLockForRawAnalysisGroup("agfdhdgfhdfd");

        // check lock
        result = service.isRawAnalysisGroupLockTaken("agfdhdgfhdfd");
        assertEquals(false, result);
    }

    @Test
    public void testMultiThreadLock() {
        GroupLockService service = new GroupLockService();
        service.init();

        ExecutorService execService = Executors.newFixedThreadPool(10);
        List<Future<Boolean>> futures = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            Callable<Boolean> task = () -> service.getLockForRawAnalysisGroup("agfdhdgfhdfd");
            futures.add(execService.submit(task));
        }

        int success = 0;
        int failure = 0;
        for (Future<Boolean> future : futures) {
            try {
                if (future.get()) {success++ ;} else { failure++;}
            } catch (Exception e) {
                fail(e.getMessage());
            }
        }
        execService.shutdown();
        assertEquals(1, success);
        assertEquals(9, failure);
    }
}
