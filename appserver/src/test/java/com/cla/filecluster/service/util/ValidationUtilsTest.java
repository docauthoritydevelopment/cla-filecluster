package com.cla.filecluster.service.util;

import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;

@Category(UnitTestCategory.class)
public class ValidationUtilsTest {

    @Test
    public void testValidNotEmpty() {
        ValidationUtils.validateStringParameter("name", "err1", "err2");
    }

    @Test
    public void testValidEmpty() {
        ValidationUtils.validateStringParameter("", true, "err1", "err2");
    }

    @Test(expected = BadRequestException.class)
    public void testInValidEmpty() {
        ValidationUtils.validateStringParameter("", "err1", "err2");
    }

    @Test
    public void testValidRemoveInvalidCharsAndTrim() {
        assertEquals("name", ValidationUtils.validateStringParameter(" name''", "err1", "err2"));
    }

    @Test
    public void testValidRemoveInvalidChars() {
        assertEquals("name", ValidationUtils.validateStringParameter( " name\" ", "err1", "err2"));
    }

    @Test
    public void testValidReplaceInvalidChars() {
        assertEquals("&gt;name&lt;", ValidationUtils.validateStringParameter(">name<", "err1", "err2"));
    }

    @Test
    public void testValidateStringNotEmpty() {
        ValidationUtils.validateStringNotEmpty("fgf", "err");
    }

    @Test(expected = BadRequestException.class)
    public void testValidateStringEmpty() {
        ValidationUtils.validateStringNotEmpty("", "err");
    }

    @Test(expected = BadRequestException.class)
    public void testValidateStringNull() {
        ValidationUtils.validateStringNotEmpty(null, "err");
    }

    @Test(expected = BadRequestException.class)
    public void testValidateStringEmptyAfterTrim() {
        ValidationUtils.validateStringNotEmpty("  ", "err");
    }

    @Test
    public void testIsNumeric() {
        assertEquals(true, ValidationUtils.isNumeric("342"));
        assertEquals(true, ValidationUtils.isNumeric("-342"));
        assertEquals(true, ValidationUtils.isNumeric("3.42"));
        assertEquals(true, ValidationUtils.isNumeric("-3.42"));
        assertEquals(true, ValidationUtils.isNumeric(".342"));
        assertEquals(false, ValidationUtils.isNumeric("3-42"));
        assertEquals(false, ValidationUtils.isNumeric("abc"));
    }
}