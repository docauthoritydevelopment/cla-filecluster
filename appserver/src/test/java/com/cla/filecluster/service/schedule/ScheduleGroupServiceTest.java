package com.cla.filecluster.service.schedule;

import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.repository.jpa.schedule.RootFolderScheduleRepository;
import com.cla.filecluster.repository.jpa.schedule.ScheduleGroupRepository;
import com.cla.filecluster.repository.jpa.schedule.SchedulePresetRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class ScheduleGroupServiceTest {

    @InjectMocks
    private ScheduleGroupService scheduleGroupService;

    @Mock
    private ScheduleGroupRepository scheduleGroupRepository;

    @Mock
    private RootFolderScheduleRepository rootFolderScheduleRepository;

    @Mock
    private RootFolderRepository rootFolderRepository;

    @Mock
    private SchedulePresetRepository scheduleConfigurationRepository;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private CrawlRunRepository crawlRunRepository;

    @Mock
    private MessageHandler messageHandler;

    @Mock
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Before
    public void init() {
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @Test(expected = BadParameterException.class)
    public void testValidNameNull() {
        scheduleGroupService.isValidForScheduleGroupCreation(null);
    }

    @Test(expected = BadParameterException.class)
    public void testValidNameEmpty() {
        scheduleGroupService.isValidForScheduleGroupCreation("");
    }

    @Test(expected = BadParameterException.class)
    public void testValidNameSpaces() {
        scheduleGroupService.isValidForScheduleGroupCreation(" ");
    }

    @Test
    public void testValidNameGood() {
        scheduleGroupService.isValidForScheduleGroupCreation("fghfghfhfg");
    }
}