package com.cla.filecluster.service.audit;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.filecluster.domain.dto.AuditDto;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.security.UserService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test for EventAuditService
 * Created by yael on 11/14/2017.
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class EventAuditServiceTest {

    @InjectMocks
    private EventAuditService eventAuditService;

    @Mock
    private UserService userService;

    @Mock
    private AuditWrapper auditWrapper;

    @Before
    public void init() {
        ReflectionTestUtils.setField(eventAuditService, "subProcess", false);
        when(userService.getPrincipalUsername()).thenReturn("stam");
    }

    @After
    public void resetMocks() {
        Mockito.reset(auditWrapper);
    }

    @Test
    public void testAddOneEvent() {
        ArgumentCaptor<Object> resultCaptor = ArgumentCaptor.forClass(Object.class);
        eventAuditService.audit(AuditType.ACTION_EVENT, "Testing audit event", AuditAction.UPDATE);
        verify(auditWrapper, times(1)).audit(resultCaptor.capture());
        testResult(resultCaptor.getValue(), AuditType.ACTION_EVENT, AuditAction.UPDATE, "Testing audit event",
                null, null,
                null, null,
                null
        );
    }

    @Test
    public void testAddOneWithObjectTypeEvent() {
        ArgumentCaptor<Object> resultCaptor = ArgumentCaptor.forClass(Object.class);
        eventAuditService.audit(AuditType.ACTION_EVENT, "Testing audit event", AuditAction.CREATE, String.class, "String-Test object id");
        verify(auditWrapper, times(1)).audit(resultCaptor.capture());
        testResult(resultCaptor.getValue(), AuditType.ACTION_EVENT, AuditAction.CREATE, "Testing audit event",
                "class java.lang.String", "String-Test object id",
                null, null,
                null
        );
    }

    @Test
    public void testAddOneObjectTypeAndOnObjectAndParamsEvent() {
        ArgumentCaptor<Object> resultCaptor = ArgumentCaptor.forClass(Object.class);
        eventAuditService.auditDual(AuditType.ACTION_EVENT, "Testing audit event", AuditAction.CREATE, String.class, "String-Test object id", Integer.class, Integer.MAX_VALUE, "param1", "param2");
        verify(auditWrapper, times(1)).audit(resultCaptor.capture());
        testResult(resultCaptor.getValue(), AuditType.ACTION_EVENT, AuditAction.CREATE, "Testing audit event",
                "class java.lang.String", "String-Test object id",
                "class java.lang.Integer", "2147483647",
                "[\"param1\",\"param2\"]"
        );
    }

    @Test
    public void testAuditRootFolder() {
        ArgumentCaptor<Object> resultCaptor = ArgumentCaptor.forClass(Object.class);
        RootFolderDto rootFolder = new RootFolderDto();
        eventAuditService.audit(AuditType.ROOT_FOLDER,"Update root folder", AuditAction.UPDATE,
                RootFolderDto.class, rootFolder.getId(), rootFolder);
        verify(auditWrapper, times(1)).audit(resultCaptor.capture());
        testResult(resultCaptor.getValue(), AuditType.ROOT_FOLDER, AuditAction.UPDATE, "Update root folder",
                "class com.cla.common.domain.dto.filetree.RootFolderDto", null,
                null, null,
                "[{\"rescanActive\":false,\"folderExcludeRulesCount\":0,\"reingestTimeStampMs\":0,\"shareFolderPermissionError\":false,\"deleted\":false,\"reingest\":false}]"
        );
    }

    @Test
    public void testAuditDualFolderExcludeRule() {
        ArgumentCaptor<Object> resultCaptor = ArgumentCaptor.forClass(Object.class);
        RootFolderDto rootFolder = new RootFolderDto();
        rootFolder.setId(5L);
        FolderExcludeRuleDto folderExcludeRuleDto = new FolderExcludeRuleDto();
        folderExcludeRuleDto.setId(4L);
        eventAuditService.auditDual(AuditType.ROOT_FOLDER,"Update root folder excluded folders", AuditAction.UPDATE,
                RootFolderDto.class, rootFolder.getId(),FolderExcludeRuleDto.class, folderExcludeRuleDto.getId(), folderExcludeRuleDto);
        verify(auditWrapper, times(1)).audit(resultCaptor.capture());
        testResult(resultCaptor.getValue(), AuditType.ROOT_FOLDER, AuditAction.UPDATE, "Update root folder excluded folders",
                "class com.cla.common.domain.dto.filetree.RootFolderDto", "5",
                "class com.cla.connector.domain.dto.file.FolderExcludeRuleDto", "4",
                "[{\"id\":4,\"matchString\":null,\"operator\":null,\"rootFolderId\":null,\"disabled\":false}]"
        );
    }

    private void testResult(Object val, AuditType type, AuditAction action,
                            String msg, String objType, String objId, String onObjType, String onObjId, String params) {
        Assert.assertTrue(val instanceof AuditDto);
        AuditDto auditDto = (AuditDto)val;
        Assert.assertTrue(auditDto.getUsername().equals("stam"));
        Assert.assertTrue(auditDto.getAuditType() == type);
        Assert.assertTrue(auditDto.getCrudAction() == action);
        Assert.assertTrue(auditDto.getMessage().equals(msg));
        Assert.assertTrue(objType == null || auditDto.getObjectType().equals(objType));
        Assert.assertTrue(objId == null || auditDto.getObjectId().equals(objId));
        Assert.assertTrue(onObjType == null || auditDto.getOnObjectType().equals(onObjType));
        Assert.assertTrue(onObjId == null || auditDto.getOnObjectId().equals(onObjId));
        Assert.assertTrue(params == null || auditDto.getParams().equals(params));
    }
}
