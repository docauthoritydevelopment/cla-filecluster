package com.cla.filecluster.service.progress;

import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Sets;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class ProgressServiceTest {

    @InjectMocks
    private ProgressService progressService;

    @Before
    public void init() {
        if (AuthenticationHelper.getCurrentAuthentication() == null) {
            AuthenticationHelper.authenticateAs(new UserDto("admin", "aa", "123"));
        }

        ReflectionTestUtils.setField(progressService, "maxProgresses", 1000);
        ReflectionTestUtils.setField(progressService, "progressExpireTimeMinutes", 1440);
        progressService.init();
    }

    @Test
    public void simpleProgressTest() {
        // create progress
        String progressId = progressService.createNewProgress(ProgressType.TEST,"initial", 0, null);
        ProgressService.Handler progress = progressService.createHandler(progressId);
        assertEquals("initial", progressService.getProgress(progressId).getMessage());
        assertEquals(0, progressService.getProgress(progressId).getPercentage());

        // increment progress
        progress.inc(10, "bla...");
        progress.inc(10, "executing");
        assertEquals("executing", progressService.getProgress(progressId).getMessage());
        assertEquals(20, progressService.getProgress(progressId).getPercentage());

        // set progress
        progress.set(30);
        assertEquals("executing", progressService.getProgress(progressId).getMessage());
        assertEquals(30, progressService.getProgress(progressId).getPercentage());
    }

    @Test
    public void getProgressesTest() {
        // create some progresses
        List<String> progresses = new ArrayList<>();
        progresses.add(progressService.createNewProgress(ProgressType.TEST1,"initial_11", 0, null));
        progresses.add(progressService.createNewProgress(ProgressType.TEST1,"initial_12", 0, null));
        progresses.add(progressService.createNewProgress(ProgressType.TEST1,"initial_13", 0, null));
        progresses.add(progressService.createNewProgress(ProgressType.TEST1,"initial_14", 0, null));
        progresses.add(progressService.createNewProgress(ProgressType.TEST2,"initial_21", 0, null));
        progresses.add(progressService.createNewProgress(ProgressType.TEST2,"initial_22", 0, null));
        progresses.add(progressService.createNewProgress(ProgressType.TEST2,"initial_23", 0, null));

        Set<ProgressBean> resultProgresses;

        // get all, make sure we have 7
        resultProgresses = progressService.getProgresses();
        assertEquals(7, resultProgresses.size());

        // get by type test1, make sure we have 4
        resultProgresses = progressService.getProgresses(ProgressType.TEST1);
        assertEquals(4, resultProgresses.size());

        // get by type test2, make sure we have 3
        resultProgresses = progressService.getProgresses(ProgressType.TEST2);
        assertEquals(3, resultProgresses.size());

        // get by one id, make sure we got the one.
        ProgressBean result = progressService.getProgress(progresses.get(2));
        assertNotNull(result);
        assertEquals("initial_13", result.getMessage());

        // get by three ids, make sure we have the right ones
        resultProgresses = progressService.getProgresses(Arrays.asList(progresses.get(0), progresses.get(2), progresses.get(5)));
        Set<String> expectedMessages = Sets.newHashSet("initial_11", "initial_13", "initial_22");
        resultProgresses.forEach(p -> assertTrue(expectedMessages.contains(p.getMessage())));
    }


    @Test
    public void compositeProgressTest() throws InterruptedException {

        // create progress
        String progressId = progressService.createNewProgress(ProgressType.TEST,"initial", 0, null);
        ProgressService.Handler progress = progressService.createHandler(progressId);
        assertEquals("initial", progressService.getProgress(progressId).getMessage());
        assertEquals(0, progressService.getProgress(progressId).getPercentage());

        // add composite progresses
        String childProgressId1 = progressService.createChildProgress(progressId, 1, "first child progress - starting");
        String childProgressId2 = progressService.createChildProgress(progressId, 1, "second child progress - starting");
        ProgressService.Handler childProgress1 = progressService.createHandler(childProgressId1);
        ProgressService.Handler childProgress2 = progressService.createHandler(childProgressId2);
        childProgress1.inc(50, "child 1 - half way there");
        childProgress2.inc(20, "child 2 - 20% done");
        Thread.sleep(600);
        assertEquals(35, progressService.getProgress(progressId).getCompositePercentage());
        childProgress2.inc(10, "child 2 - executing");
        Thread.sleep(600);
        assertEquals(40, progressService.getProgress(progressId).getCompositePercentage());

        // test father progress override
        progress.set(100, "Done");
        progress.setDone();
        Thread.sleep(600);
        assertTrue(progressService.getProgress(progressId).isDone());
        assertEquals(100, progressService.getProgress(progressId).getPercentage());
    }

}