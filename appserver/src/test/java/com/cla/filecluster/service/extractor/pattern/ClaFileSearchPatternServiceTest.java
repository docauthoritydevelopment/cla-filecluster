package com.cla.filecluster.service.extractor.pattern;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.IsraeliIdTextSearchPattern;
import com.cla.common.domain.dto.filesearchpatterns.PatternType;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.filecluster.domain.entity.searchpatterns.PatternCategory;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.repository.file.TextSearchPatternImplementationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.PatternCategoryRepository;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class ClaFileSearchPatternServiceTest {

    @Mock
    private TextSearchPatternImplementationRepository textSearchPatternImplementationRepository;

    @InjectMocks
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Mock
    private TextSearchPatternRepository textSearchPatternRepository;

    @Mock
    private MediaProcessorCommService mediaProcessorCommService;

    @Mock
    private PatternCategoryRepository patternCategoryRepository;

    @Mock
    private MessageHandler messageHandler;

    @Before
    public void init() {
        IsraeliIdTextSearchPattern ccp = new IsraeliIdTextSearchPattern(5, "name");
        Collection<TextSearchPatternImpl> patterns = new ArrayList<>();
        patterns.add(ccp);
        String[] ignoreValuesList = {};
        ReflectionTestUtils.setField(claFileSearchPatternService, "noDatabase", false);
        ReflectionTestUtils.setField(claFileSearchPatternService, "patternSearchActive", true);
        ReflectionTestUtils.setField(claFileSearchPatternService, "countDistinctMatches", true);
        ReflectionTestUtils.setField(claFileSearchPatternService, "ignoreValuesList", ignoreValuesList);
        claFileSearchPatternService.init();
        when(textSearchPatternImplementationRepository.findAll()).thenReturn(patterns);
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @Test
    public void testCount() {
        String text = "\n057845851\ndfgdgdfg gfjghj sefsef 56566 hghf 66";
        Set<SearchPatternCountDto> found = claFileSearchPatternService.getSearchPatternsCounting(text, true);
        assertEquals(1, found.size());
        SearchPatternCountDto f = found.iterator().next();
        assertEquals(1, f.getPatternCount());
        assertEquals(1, f.getMatches().size());
        assertEquals("057845851", f.getMatches().get(0));
    }

    @Test
    public void testCountSeveralAsOne() {
        String text = "\n057845851\ndfgdgdfg gfjghj sefsef \n057845851\n 56566 hghf 66";
        Set<SearchPatternCountDto> found = claFileSearchPatternService.getSearchPatternsCounting(text, true);
        assertEquals(1, found.size());
        SearchPatternCountDto f = found.iterator().next();
        assertEquals(1, f.getPatternCount());
        assertEquals(1, f.getMatches().size());
        assertEquals("057845851", f.getMatches().get(0));
    }

    @Test
    public void testCountSeveral() {
        ReflectionTestUtils.setField(claFileSearchPatternService, "countDistinctMatches", false);
        claFileSearchPatternService.init();
        String text = "\n057845851\ndfgdgdfg gfjghj sefsef\n057845851\n56566 hghf 66";
        Set<SearchPatternCountDto> found = claFileSearchPatternService.getSearchPatternsCounting(text, true);
        assertEquals(1, found.size());
        SearchPatternCountDto f = found.iterator().next();
        assertEquals(2, f.getPatternCount());
        assertEquals(2, f.getMatches().size());
        assertEquals("057845851", f.getMatches().get(0));
        ReflectionTestUtils.setField(claFileSearchPatternService, "countDistinctMatches", true);
    }

    @Test
    public void testCountZeroIgnoreList() {
        String[] ignoreValuesList = {"\\b057845851\\b"};
        ReflectionTestUtils.setField(claFileSearchPatternService, "ignoreValuesList", ignoreValuesList);
        claFileSearchPatternService.init();
        String text = "\n057845851\ndfgdgdfg gfjghj sefsef\n057845851\n56566 hghf 66";
        Set<SearchPatternCountDto> found = claFileSearchPatternService.getSearchPatternsCounting(text, true);
        assertEquals(0, found.size());
        String[] ignoreValuesListAft = {};
        ReflectionTestUtils.setField(claFileSearchPatternService, "ignoreValuesList", ignoreValuesListAft);
    }

    @Test(expected = BadRequestException.class)
    public void createNameMissing() {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
    }

    @Test(expected = BadRequestException.class)
    public void createNameEmpty() {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        textSearchPatternDto.setName("");
        claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
    }

    @Test(expected = BadRequestException.class)
    public void createNameSpaces() {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        textSearchPatternDto.setName("  ");
        claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
    }

    @Test(expected = BadRequestException.class)
    public void createPatternEmpty() {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        textSearchPatternDto.setName("fgdgd");
        claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
    }

    @Test(expected = BadRequestException.class)
    public void createPatternTypeEmpty() {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        textSearchPatternDto.setName("fgdgd");
        textSearchPatternDto.setPattern("\\b\\d{5,9}\\b");
        claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
    }

    @Test
    public void createOk() {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        textSearchPatternDto.setName("fgdgd");
        textSearchPatternDto.setPattern("\\b\\d{5,9}\\b");
        textSearchPatternDto.setPatternType(PatternType.REGEX);
        textSearchPatternDto.setSubCategoryId(2);
        TextSearchPattern theNew = new TextSearchPattern();
        theNew.setId(56);
        when(textSearchPatternRepository.save(any(TextSearchPattern.class))).thenReturn(theNew);
        when(textSearchPatternImplementationRepository.findById(56)).thenReturn(new TextSearchPatternImpl(56,"fgdgd","\\b\\d{5,9}\\b"));
        PatternCategory pCategory = new PatternCategory();
        pCategory.setId(2);
        pCategory.setName("Test");
        Optional<PatternCategory> optionalPatternCategory= Optional.of(pCategory);
        when(patternCategoryRepository.findById(2)).thenReturn(optionalPatternCategory);
        claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
    }
}