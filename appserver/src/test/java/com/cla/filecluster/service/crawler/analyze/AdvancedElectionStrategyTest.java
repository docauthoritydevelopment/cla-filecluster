package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Created by: yael
 * Created on: 12/4/2017
 */
@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class AdvancedElectionStrategyTest extends JoinGroupsStrategyTestUtil {

    @Mock
    private SolrFileGroupRepository fileGroupRepository;

    @Mock
    private FileTagService fileTagService;

    @Test
    public void testStrategyChooseMostFiles() {
        SolrFileGroupEntity f1 = createFileGroup(400L);

        SolrFileGroupEntity f2 = createFileGroup(500L);

        SolrFileGroupEntity f3 = createFileGroup(600L);

        Collection<SolrFileGroupEntity> candidateGroups = new ArrayList<>();
        candidateGroups.add(f1);
        candidateGroups.add(f2);
        candidateGroups.add(f3);
        AdvancedElectionStrategy advancedElectionStrategy = new AdvancedElectionStrategy();
        JoinGroupsElectionResult result = advancedElectionStrategy.electTargetGroup(candidateGroups, fileGroupRepository, fileTagService);
        assertTrue(result.getElectedGroup().equals(f3));
        assertTrue(result.getAcceptedGroups().size() == 2);
        assertTrue(result.getRejectedGroups().size() == 0);
    }

    @Test
    public void testStrategyChooseMostRolesWhenFewFiles() {
        SolrFileGroupEntity f1 = createFileGroup(4L, "f1");
        AssociationEntity dta = new AssociationEntity();
        dta.setDocTypeId(5L);
        f1.setAssociations(Lists.newArrayList(AssociationsService.convertFromAssociationEntity(dta)));

        SolrFileGroupEntity f2 = createFileGroup(5L, "f2");
        f2.setAssociations(Lists.newArrayList(
                AssociationsService.convertFromAssociationEntity(createBusinessIdTagAssociation()),
                AssociationsService.convertFromAssociationEntity(createBusinessIdTagAssociation())));

        SolrFileGroupEntity f3 = createFileGroup(6L, "f3");

        Collection<SolrFileGroupEntity> candidateGroups = new ArrayList<>();
        candidateGroups.add(f1);
        candidateGroups.add(f2);
        candidateGroups.add(f3);
        AdvancedElectionStrategy advancedElectionStrategy = new AdvancedElectionStrategy();
        JoinGroupsElectionResult result = advancedElectionStrategy.electTargetGroup(candidateGroups, fileGroupRepository, fileTagService);
        assertTrue(result.getElectedGroup().equals(f2));
        assertTrue(result.getAcceptedGroups().size() == 2);
        assertTrue(result.getRejectedGroups().size() == 0);
    }

    @Test
    public void testStrategyNoReject() {
        Collection<SolrFileGroupEntity> candidateGroups = new ArrayList<>();

        SolrFileGroupEntity f1 = createFileGroup();
        candidateGroups.add(f1);
        f1.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f2 = createFileGroup();
        candidateGroups.add(f2);
        f2.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f3 = createFileGroup();
        candidateGroups.add(f3);
        f3.setAssociations(generateFileGroupAssociations());

        AdvancedElectionStrategy advancedElectionStrategy = new AdvancedElectionStrategy();
        JoinGroupsElectionResult result = advancedElectionStrategy.electTargetGroup(candidateGroups, fileGroupRepository, fileTagService);
        assertTrue(result.getElectedGroup() != null);
        assertTrue(result.getAcceptedGroups().size() == 2);
        assertTrue(result.getRejectedGroups().size() == 0);
    }

    @Test
    public void testStrategyWithReject() {
        Collection<SolrFileGroupEntity> candidateGroups = new ArrayList<>();

        SolrFileGroupEntity f1 = createFileGroup();
        candidateGroups.add(f1);
        f1.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f2 = createFileGroup();
        candidateGroups.add(f2);
        f2.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f3 = createFileGroup();
        candidateGroups.add(f3);
        f3.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity reject = createFileGroup(7000L);
        candidateGroups.add(reject);

        AdvancedElectionStrategy advancedElectionStrategy = new AdvancedElectionStrategy();
        advancedElectionStrategy.setRejectSizeThreshold(6000);
        JoinGroupsElectionResult result = advancedElectionStrategy.electTargetGroup(candidateGroups, fileGroupRepository, fileTagService);
        assertTrue(result.getElectedGroup() != null);
        if (result.getElectedGroup().getId().equals(reject.getId())) {
            assertTrue(result.getAcceptedGroups().size() == 3);
        } else {
            assertTrue(result.getAcceptedGroups().size() == 2);
            assertTrue(result.getRejectedGroups().size() == 1);
            assertTrue(result.getRejectedGroups().iterator().next().equals(reject));
        }
    }

    @Test
    public void testStrategyWithRejectSubGroup() {
        Collection<SolrFileGroupEntity> candidateGroups = new ArrayList<>();

        SolrFileGroupEntity f1 = createFileGroup();
        candidateGroups.add(f1);
        f1.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f2 = createFileGroup();
        candidateGroups.add(f2);
        f2.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f3 = createFileGroup();
        candidateGroups.add(f3);
        f3.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity reject = createFileGroup(0L);
        reject.setType(GroupType.SUB_GROUP.toInt());
        candidateGroups.add(reject);

        AdvancedElectionStrategy advancedElectionStrategy = new AdvancedElectionStrategy();
        advancedElectionStrategy.setRejectSizeThreshold(6000);
        JoinGroupsElectionResult result = advancedElectionStrategy.electTargetGroup(candidateGroups, fileGroupRepository, fileTagService);
        assertTrue(result.getElectedGroup() != null);
        assertTrue(result.getAcceptedGroups().size() == 2);
        assertTrue(result.getRejectedGroups().size() == 1);
        assertTrue(result.getRejectedGroups().iterator().next().equals(reject));
    }

    @Test
    public void testStrategyWithRejectRawAnalysisGroup() {
        Collection<SolrFileGroupEntity> candidateGroups = new ArrayList<>();

        SolrFileGroupEntity f1 = createFileGroup();
        candidateGroups.add(f1);
        f1.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f2 = createFileGroup();
        candidateGroups.add(f2);
        f2.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity f3 = createFileGroup();
        candidateGroups.add(f3);
        f3.setAssociations(generateFileGroupAssociations());

        SolrFileGroupEntity reject = new SolrFileGroupEntity();
        reject.setType(GroupType.ANALYSIS_GROUP.toInt());
        reject.setHasSubgroups(true);
        reject.setNumOfFiles(0L);
        candidateGroups.add(reject);

        AdvancedElectionStrategy advancedElectionStrategy = new AdvancedElectionStrategy();
        advancedElectionStrategy.setRejectSizeThreshold(6000);
        JoinGroupsElectionResult result = advancedElectionStrategy.electTargetGroup(candidateGroups, fileGroupRepository, fileTagService);
        assertTrue(result.getElectedGroup() != null);
        assertTrue(result.getAcceptedGroups().size() == 2);
        assertTrue(result.getRejectedGroups().size() == 1);
        assertTrue(result.getRejectedGroups().iterator().next().equals(reject));
    }

    @Test
    public void testAdvancedStrategy(){
        init();
        AdvancedElectionStrategy strat = new AdvancedElectionStrategy();
        strat.setRejectSizeThreshold(100);

        FileTagDto tag = new FileTagDto();
        FileTagTypeDto type = new FileTagTypeDto();
        tag.setType(type);
        type.setSystem(false);
        when(fileTagService.getFromCache(60L)).thenReturn(tag);

        FileTagDto tag2 = new FileTagDto();
        FileTagTypeDto type2 = new FileTagTypeDto();
        tag2.setType(type2);
        type2.setSystem(true);
        when(fileTagService.getFromCache(6L)).thenReturn(tag2);

        JoinGroupsElectionResult result = strat.electTargetGroup(
                Lists.newArrayList(group10, group1000, groupWithAssoc1, groupWithAssoc2, groupWithSystemAssoc, groupWithDeletedAssoc),
                fileGroupRepository, fileTagService);
        Assert.assertEquals(groupWithAssoc1, result.getElectedGroup());
        Assert.assertEquals(4, result.getAcceptedGroups().size());
        Assert.assertEquals(1, result.getRejectedGroups().size());
        Assert.assertTrue(result.getAcceptedGroups().containsAll(
                Sets.newHashSet(group10, groupWithAssoc2, groupWithSystemAssoc, groupWithDeletedAssoc)));
        Assert.assertTrue(result.getRejectedGroups().contains(group1000));
    }

    private AssociationEntity createBusinessIdTagAssociation() {
        AssociationEntity bl = new AssociationEntity();
        bl.setFunctionalRoleId(6L);
        return bl;
    }

    private SolrFileGroupEntity createFileGroup() {
        SolrFileGroupEntity f = new SolrFileGroupEntity();
        f.setNumOfFiles((long) random.nextInt(5000));
        f.setId(String.valueOf(random.nextLong()));
        f.setHasSubgroups(false);
        f.setType(GroupType.ANALYSIS_GROUP.toInt());
        return f;
    }

    private SolrFileGroupEntity createFileGroup(long numOfFiles) {
        return createFileGroup(numOfFiles, null);
    }

    private SolrFileGroupEntity createFileGroup(long numOfFiles, String id) {
        SolrFileGroupEntity f = new SolrFileGroupEntity();
        f.setNumOfFiles(numOfFiles);
        f.setHasSubgroups(false);
        f.setId(id != null ? id : String.valueOf(random.nextLong()));
        f.setType(GroupType.ANALYSIS_GROUP.toInt());
        return f;
    }

    private List<String> generateFileGroupAssociations() {
        List<String> associations = new ArrayList<>();
        int top = random.nextInt(3);
        for (int i = 0; i < top; ++i) {
            associations.add(AssociationsService.convertFromAssociationEntity(createBusinessIdTagAssociation()));
        }
        return associations;
    }
}
