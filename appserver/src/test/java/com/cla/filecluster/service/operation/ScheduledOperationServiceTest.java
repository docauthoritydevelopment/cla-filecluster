package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationPriority;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.group.ScheduledOperationRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.HashMap;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class ScheduledOperationServiceTest {

    @InjectMocks
    private ScheduledOperationService scheduledOperationService;

    @Mock
    private ScheduledOperationRepository scheduledOperationRepository;

    @Mock
    private ComponentsAppService componentsAppService;

    @Mock
    private DBTemplateUtils dBTemplateUtils;

    @Mock
    private UserService userService;

    @Mock
    private MessageHandler messageHandler;

    @Before
    public void init() {
        CreateJoinedGroupOperation op1 = new CreateJoinedGroupOperation();
        StartupFileStatusChangeOperation op2 = new StartupFileStatusChangeOperation();
        ReflectionTestUtils.setField(scheduledOperationService, "operations", Arrays.asList(op1, op2));
        ReflectionTestUtils.setField(scheduledOperationService, "isSchedulingActive", true);
        when(messageHandler.getMessage(any())).thenReturn("err");
        op1.setMessageHandler(messageHandler);
    }

    @After
    public void resetMocks() {
        Mockito.reset(scheduledOperationRepository);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateNoType() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOpData(new HashMap<>());
        data.getOpData().put(CreateJoinedGroupOperation.GROUP_NAME_PARAM, "sdfsfs");
        data.getOpData().put(CreateJoinedGroupOperation.CHILD_GROUPS_PARAM, "sdfsfs5675675");
        data.getOpData().put(CreateJoinedGroupOperation.NEW_GROUP_ID_PARAM, "78i676u6sdfsfs5675675");
        data.getOpData().put(CreateJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM, "sdfsfs5675675");
        data.setUserOperation(true);
        scheduledOperationService.createNewOperation(data);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateNoParams() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.CREATE_JOINED_GROUP);
        data.setUserOperation(true);
        scheduledOperationService.createNewOperation(data);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateMissingParam() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.CREATE_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(CreateJoinedGroupOperation.GROUP_NAME_PARAM, "sdfsfs");
        data.setUserOperation(true);
        scheduledOperationService.createNewOperation(data);
    }

    @Test
    public void testCreateOk() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.CREATE_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(CreateJoinedGroupOperation.GROUP_NAME_PARAM, "sdfsfs");
        data.getOpData().put(CreateJoinedGroupOperation.CHILD_GROUPS_PARAM, "sdfsfs5675675");
        data.getOpData().put(CreateJoinedGroupOperation.NEW_GROUP_ID_PARAM, "78i676u6sdfsfs5675675");
        data.getOpData().put(CreateJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM, "sdfsfs5675675");
        data.setUserOperation(true);
        scheduledOperationService.createNewOperation(data);
    }

    @Test
    public void testCreateOkNoParamsType() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.STARTUP_FILE_STATUS_CNG);
        data.setUserOperation(false);
        data.setPriority(ScheduledOperationPriority.HIGH);
        scheduledOperationService.createNewOperation(data);
    }
}