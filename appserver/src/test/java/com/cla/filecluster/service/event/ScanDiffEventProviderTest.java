package com.cla.filecluster.service.event;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventProviderType;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.event.ScanDiffEvent;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class ScanDiffEventProviderTest {

    @Mock
    private Queue<DAEvent> eventQueue;

    @Mock
    private EmptyEventNormalizer normalizer;

    @InjectMocks
    private ScanDiffEventProvider scanDiffEventProvider;

    @Before
    public void init() {
        when(normalizer.normalize(any())).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void testHandleEvent() {
        scanDiffEventProvider.generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, 5L, "path");
        ArgumentCaptor<DAEvent> resultCaptor = ArgumentCaptor.forClass(DAEvent.class);
        verify(eventQueue, times(1)).add(resultCaptor.capture());
        DAEvent event = resultCaptor.getValue();
        assertTrue(event instanceof ScanDiffEvent);
        assertEquals(EventProviderType.DOCAUTHORITY, event.getEventProvider());
        assertEquals(EventType.SCAN_DIFF_DELETED, event.getEventType());
        assertTrue(event.getCreated() > 0);
        assertTrue(event.getReported() > 0);
        assertTrue(5L == ((ScanDiffEvent)event).getPayload().getFileId());
        assertEquals("path", ((ScanDiffEvent)event).getPayload().getPath());
    }

}
