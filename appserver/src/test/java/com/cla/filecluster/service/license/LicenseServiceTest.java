package com.cla.filecluster.service.license;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.filecluster.domain.entity.license.LicenseResource;
import com.cla.filecluster.repository.jpa.license.DocAuthorityLicenseRepository;
import com.cla.filecluster.repository.jpa.license.LicenseResourceRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class LicenseServiceTest {

    @InjectMocks
    private LicenseService licenseService;

    @Mock
    private DocAuthorityLicenseRepository docAuthorityLicenseRepository;

    @Mock
    private LicenseResourceRepository licenseResourceRepository;

    @Mock
    private MessageHandler messageHandler;

    @Before
    public void init() {
        ReflectionTestUtils.setField(licenseService, "noDatabase", false);
        when(messageHandler.getMessage(anyString(), anyList())).thenReturn("err");
    }

    @After
    public void resetMocks() {
        Mockito.reset(licenseResourceRepository);
        Mockito.reset(docAuthorityLicenseRepository);
    }

    @Test
    public void importLicenseTestOk() {
        Map<String, String> restrictions = new LinkedHashMap<>();
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setCreationDate(645645646);
        docAuthorityLicenseDto.setRestrictions(restrictions);
        restrictions.put("crawlerExpiry", "*");
        restrictions.put("loginExpiry", "5464466546");
        restrictions.put("maxFiles", "*");
        restrictions.put("maxRootFolders", "17");

        ArgumentCaptor<LicenseResource> resCaptor = ArgumentCaptor.forClass(LicenseResource.class);
        boolean res = licenseService.importLicense(docAuthorityLicenseDto, true);
        assertTrue(res);

        verify(licenseResourceRepository, times(4)).save(resCaptor.capture());
        List<LicenseResource> capturedLicenses = resCaptor.getAllValues();
        assertEquals("*", capturedLicenses.get(0).getValue());
        assertEquals("5464466546", capturedLicenses.get(1).getValue());
        assertEquals("*", capturedLicenses.get(2).getValue());
        assertEquals("17", capturedLicenses.get(3).getValue());
    }

    @Test
    public void importLicenseTestNeedsTrim() {
        Map<String, String> restrictions = new LinkedHashMap<>();
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setCreationDate(645645646);
        docAuthorityLicenseDto.setRestrictions(restrictions);
        restrictions.put("crawlerExpiry", "*");
        restrictions.put("loginExpiry", " 5464466546 ");
        restrictions.put("maxFiles", "*");
        restrictions.put("maxRootFolders", "17");
        restrictions.put("serverId", "1760-dfgdsss");

        ArgumentCaptor<LicenseResource> resCaptor = ArgumentCaptor.forClass(LicenseResource.class);
        boolean res = licenseService.importLicense(docAuthorityLicenseDto, true);
        assertTrue(res);

        verify(licenseResourceRepository, times(5)).save(resCaptor.capture());
        List<LicenseResource> capturedLicenses = resCaptor.getAllValues();
        assertEquals("*", capturedLicenses.get(0).getValue());
        assertEquals("5464466546", capturedLicenses.get(1).getValue());
        assertEquals("*", capturedLicenses.get(2).getValue());
        assertEquals("17", capturedLicenses.get(3).getValue());
        assertEquals("1760-dfgdsss", capturedLicenses.get(4).getValue());
    }

    @Test(expected = IllegalArgumentException.class)
    public void importLicenseTestNotNumber() {
        Map<String, String> restrictions = new HashMap<>();
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setCreationDate(645645646);
        docAuthorityLicenseDto.setRestrictions(restrictions);
        restrictions.put("loginExpiry", "5464466 546");
        licenseService.importLicense(docAuthorityLicenseDto, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void importLicenseTestEmpty() {
        Map<String, String> restrictions = new HashMap<>();
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setCreationDate(645645646);
        docAuthorityLicenseDto.setRestrictions(restrictions);
        restrictions.put("loginExpiry", " ");
        licenseService.importLicense(docAuthorityLicenseDto, true);
    }
}