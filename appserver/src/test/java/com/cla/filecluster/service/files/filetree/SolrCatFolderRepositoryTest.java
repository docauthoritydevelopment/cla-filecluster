package com.cla.filecluster.service.files.filetree;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.filecluster.domain.converters.SolrFolderBuilder;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by: yael
 * Created on: 3/27/2018
 */
@Ignore
@Category(FastTestCategory.class)
public class SolrCatFolderRepositoryTest extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(SolrCatFolderRepositoryTest.class);

    @Autowired
    private SolrCatFolderRepository solrFolderRepository;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Test
    public void testDirectFileAndSubFolder() {
        RootFolderDto rootFolderDto = rootFolderDtoAnalyze;
        List<SolrFolderEntity> folders = solrFolderRepository.getByFilterCriterias(
                Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderDto.getId()));
        List<Long> folderIds = folders.stream().map(SolrFolderEntity::getId).collect(Collectors.toList());
        List<DocFolder> docFolders = folders.stream().map(SolrCatFolderRepository::getFromEntity).collect(Collectors.toList());
        Map<Long, Long> res = solrFileCatRepository.getFoldersDirectFileNumber(folderIds);
        System.out.println("direct files");
        System.out.println(res);
        res = solrFolderRepository.getFoldersSubFoldersNumber(folderIds);
        System.out.println("sub folders");
        System.out.println(res);
        res = solrFileCatRepository.getFoldersAllFileNumber(docFolders);
        System.out.println("all files");
        System.out.println(res);
        long count = solrFolderRepository.getTotalCount(null);
        System.out.println(count);
    }

    @Test
    public void testGetFolderParent() {
        Long childId = 5555L;
        Long parentId = 7777L;
        Long rootFolderId = 888L;

        SolrFolderEntity folderEntity = SolrFolderBuilder.create()
                .setId(parentId)
                .setPath("D:\\realpath")
                .setPathDescriptorType(PathDescriptorType.WINDOWS)
                .setRootFolderId(rootFolderId)
                .setRootDepth(0)
                .build();

        solrFolderRepository.save(folderEntity);

        folderEntity = SolrFolderBuilder.create()
                .setId(childId)
                .setPath("D:\\realpath\\nn")
                .setPathDescriptorType(PathDescriptorType.WINDOWS)
                .setRootFolderId(rootFolderId)
                .setRootDepth(1)
                .setParentFolderId(parentId)
                .build();

        solrFolderRepository.save(folderEntity);

        solrFolderRepository.softCommitNoWaitFlush();

        solrFolderRepository.getOneById(childId);

        solrFolderRepository.deleteById(childId.toString());
        solrFolderRepository.deleteById(parentId.toString());
        solrFolderRepository.softCommitNoWaitFlush();
    }

    @Test
    public void testFolderParentInfo() {
        Long id = 9999L;
        Long rootFolderId = 999L;

        SolrFolderEntity folderEntity = SolrFolderBuilder.create()
                .setId(id)
                .setPath("D:\\realpath\\ddd")
                .setPathDescriptorType(PathDescriptorType.WINDOWS)
                .setRootFolderId(rootFolderId)
                .setRootDepth(0)
                .build();

        folderEntity.setParentsInfo(null);
        solrFolderRepository.save(folderEntity);
        solrFolderRepository.softCommitNoWaitFlush();

        solrFolderRepository.updateFoldersParentsInformationNoParent(rootFolderId);
        Optional<SolrFolderEntity> ent = solrFolderRepository.getOneById(id.toString());
        assertTrue(ent.isPresent());
        assertEquals("2.9999", ent.get().getParentsInfo());
        solrFolderRepository.deleteById(id.toString());
        solrFolderRepository.softCommitNoWaitFlush();
    }
}
