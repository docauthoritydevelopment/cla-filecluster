package com.cla.filecluster.service.filetag;

import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.jpa.scope.BasicScopeRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class FileTaggingAppServiceTest {

    @Mock
    private FileTagService fileTagService;

    @Mock
    private SolrFileCatRepository solrFileCatRepository;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private DocTypeService docTypeService;

    @Mock
    private GroupsService groupsService;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private FileTagTypeService fileTagTypeService;

    @Mock
    private BizListItemService bizListItemService;

    @Mock
    private ExecutionManagerService executionManagerService;

    @Mock
    private SolrSpecificationFactory solrSpecificationFactory;

    @Mock
    private BasicScopeRepository basicScopeRepository;

    @Mock
    private MessageHandler messageHandler;

    @InjectMocks
    private FileTaggingAppService fileTaggingAppService;

    @Before
    public void init() {
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @Test(expected = BadParameterException.class)
    public void testCreateNameNull() {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setName(null);
        fileTaggingAppService.createFileTagType(fileTagTypeDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateNameEmpty() {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setName("");
        fileTaggingAppService.createFileTagType(fileTagTypeDto);
    }

    @Test(expected = BadParameterException.class)
    public void testCreateNameSpaces() {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setName("");
        fileTaggingAppService.createFileTagType(fileTagTypeDto);
    }

    @Test
    public void testCreateNameOk() {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setName("dfgdfgdf");

        FileTagType fileTagType = new FileTagType();
        fileTagType.setId(1L);

        when(fileTagTypeService.createFileTagType(anyString(),
                nullable(String.class), anyBoolean(), anyBoolean(),
                anyBoolean(), anyBoolean(), any())).thenReturn(fileTagType);

        fileTaggingAppService.createFileTagType(fileTagTypeDto);
    }
}