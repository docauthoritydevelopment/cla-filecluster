package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.domain.dto.group.GroupType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.service.categories.AssociationsService;

import java.util.*;
import java.util.stream.IntStream;

public class JoinGroupsStrategyTestUtil {

    Random random = new Random();

    SolrFileGroupEntity group1000;
    SolrFileGroupEntity group10;
    SolrFileGroupEntity groupWithAssoc1;
    SolrFileGroupEntity groupWithAssoc2;
    SolrFileGroupEntity groupWithSystemAssoc;
    SolrFileGroupEntity groupWithDeletedAssoc;


    public void init(){

        List<String> docTypeAssociations = new ArrayList<>();
        AssociationEntity dta = new AssociationEntity();
        dta.setDocTypeId(77L);
        IntStream.range(1, 10).forEach(i -> docTypeAssociations.add(AssociationsService.convertFromAssociationEntity(dta)));

        List<FunctionalRole> funcRoles = new ArrayList<>();
        IntStream.range(1, 10).forEach(i -> funcRoles.add(createFuncRole()));

        List<String> deletedTagAssociations = new ArrayList<>();

        List<String> systemTagAssociations = new ArrayList<>();
        IntStream.range(1, 10).forEach(i -> systemTagAssociations.add(createTagAssoc(true)));

        List<String> regularTagAssociations = new ArrayList<>();
        IntStream.range(1, 10).forEach(i -> regularTagAssociations.add(createTagAssoc(false)));


        group1000 = createFileGroup(1000L, "group1000");

        group10 = createFileGroup(10L, null);

        groupWithAssoc1 = createFileGroup(90L, "groupWithAssoc1");
        groupWithAssoc1.setAssociations(regularTagAssociations);

        groupWithAssoc2 = createFileGroup(80L, "groupWithAssoc2");
        groupWithAssoc2.setAssociations(regularTagAssociations);

        groupWithSystemAssoc = createFileGroup(95L, "groupWithSystemAssoc");
        groupWithSystemAssoc.setAssociations(systemTagAssociations);

        groupWithDeletedAssoc = createFileGroup(99L, "groupWithDeletedAssoc");
        groupWithDeletedAssoc.setNumOfFiles(99L);
        groupWithDeletedAssoc.setAssociations(deletedTagAssociations);
    }

    private SolrFileGroupEntity createFileGroup(long numOfFiles, String id) {
        SolrFileGroupEntity f = new SolrFileGroupEntity();
        f.setNumOfFiles(numOfFiles);
        f.setHasSubgroups(false);
        f.setId(id != null ? id : String.valueOf(random.nextLong()));
        f.setType(GroupType.ANALYSIS_GROUP.toInt());
        return f;
    }

    private String createTagAssoc(boolean system){
        AssociationEntity assoc = new AssociationEntity();
        assoc.setFileTagId(system ? 6L : 60L);
        return AssociationsService.convertFromAssociationEntity(assoc);
    }

    private FunctionalRole createFuncRole(){
        FunctionalRole role = new FunctionalRole();
        role.setName("Name" + random.nextInt(1000));
        return role;
    }
}
