package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filetree.EntityCredentialsDto;
import com.cla.common.domain.dto.filetree.EntityType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RsaKey;
import com.cla.common.utils.EncryptionUtils;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.filetree.EntityCredentials;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.jpa.dataCenter.CustomerDataCenterRepository;
import com.cla.filecluster.repository.jpa.filetree.EntityCredentialsRepository;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class EntityCredentialsServiceTest {

    @Mock
    EntityCredentialsRepository entityCredentialsRepository;

    @Mock
    private CustomerDataCenterRepository customerDataCenterRepository;

    @Mock
    private DocStoreService docStoreService;

    @InjectMocks
    private EntityCredentialsService entityCredentialsService = new EntityCredentialsService();

    private static final String TEST_USERNAME = "dfgdfgdfgdf";
    private static final String TEST_PASSWORD = "dfgdfgdfgdf";

    @Test
    public void testCreateEntityCredentialsNoEncryption() {
        EntityCredentialsDto entityCredentialsDto = new EntityCredentialsDto();
        entityCredentialsDto.setEntityType(EntityType.ROOT_FOLDER);
        RootFolderDto rootFolder = new RootFolderDto();
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        rootFolder.setCustomerDataCenterDto(customerDataCenterDto);

        final ArgumentCaptor<EntityCredentials> captor = ArgumentCaptor.forClass(EntityCredentials.class);

        EntityCredentials dbObj = new EntityCredentials();
        dbObj.setEntityType(EntityType.ROOT_FOLDER.getName());

        when(entityCredentialsRepository.save(captor.capture())).thenReturn(dbObj);

        entityCredentialsService.createEntityCredentials(entityCredentialsDto, rootFolder);

        final EntityCredentials argument = captor.getValue();
        verify(entityCredentialsRepository, atLeastOnce()).save(argument);

        Assert.assertTrue(argument.getModifiedTimeStampMs() > 0);
        Assert.assertTrue(argument.getCreatedTimeStampMs() > 0);
    }

    @Test
    public void testCreateEntityCredentialsWithEncryption() throws Exception {

        // generate a key
        RsaKey keys = EncryptionUtils.generateNewKeysComplete();

        EntityCredentialsDto entityCredentialsDto = new EntityCredentialsDto();
        entityCredentialsDto.setEntityType(EntityType.ROOT_FOLDER);
        entityCredentialsDto.setUsername(TEST_USERNAME);
        entityCredentialsDto.setPassword(TEST_PASSWORD);
        RootFolderDto rootFolder = new RootFolderDto();
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setPublicKey(keys.getPublicKey());
        rootFolder.setCustomerDataCenterDto(customerDataCenterDto);

        final ArgumentCaptor<EntityCredentials> captor = ArgumentCaptor.forClass(EntityCredentials.class);

        EntityCredentials dbObj = new EntityCredentials();
        dbObj.setEntityType(EntityType.ROOT_FOLDER.getName());

        when(entityCredentialsRepository.save(captor.capture())).thenReturn(dbObj);

        entityCredentialsService.createEntityCredentials(entityCredentialsDto, rootFolder);

        final EntityCredentials argument = captor.getValue();
        verify(entityCredentialsRepository, atLeastOnce()).save(argument);

        Assert.assertTrue(TEST_PASSWORD.equals(EncryptionUtils.decryptUsingPrivateKeyStr(keys.getPrivateKey(), argument.getPassword())));
        Assert.assertTrue(argument.getModifiedTimeStampMs() > 0);
        Assert.assertTrue(argument.getCreatedTimeStampMs() > 0);
    }

    @Test
    public void testModifyEntityCredentialsNoEncryption() {
        EntityCredentialsDto entityCredentialsDto = new EntityCredentialsDto();
        entityCredentialsDto.setEntityType(EntityType.ROOT_FOLDER);
        entityCredentialsDto.setId(1L);
        RootFolderDto rootFolder = new RootFolderDto();
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        rootFolder.setCustomerDataCenterDto(customerDataCenterDto);

        final ArgumentCaptor<EntityCredentials> captor = ArgumentCaptor.forClass(EntityCredentials.class);

        EntityCredentials dbObj = new EntityCredentials();
        dbObj.setEntityType(EntityType.ROOT_FOLDER.getName());
        dbObj.setId(1L);

        when(entityCredentialsRepository.save(captor.capture())).thenReturn(dbObj);
        when(entityCredentialsRepository.getOne(1L)).thenReturn(dbObj);

        entityCredentialsService.updateEntityCredentials(entityCredentialsDto, rootFolder);

        final EntityCredentials argument = captor.getValue();
        verify(entityCredentialsRepository, atLeastOnce()).save(argument);

        Assert.assertTrue(argument.getModifiedTimeStampMs() > 0);
    }

    @Test
    public void testModifyEntityCredentialsWithEncryption() throws Exception {

        // generate a key
        RsaKey keys = EncryptionUtils.generateNewKeysComplete();

        EntityCredentialsDto entityCredentialsDto = new EntityCredentialsDto();
        entityCredentialsDto.setEntityType(EntityType.ROOT_FOLDER);
        entityCredentialsDto.setId(1L);
        entityCredentialsDto.setUsername(TEST_USERNAME);
        entityCredentialsDto.setPassword(TEST_PASSWORD);
        entityCredentialsDto.setShouldEncryptPassword(true);
        RootFolderDto rootFolder = new RootFolderDto();
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setPublicKey(keys.getPublicKey());
        rootFolder.setCustomerDataCenterDto(customerDataCenterDto);

        final ArgumentCaptor<EntityCredentials> captor = ArgumentCaptor.forClass(EntityCredentials.class);

        EntityCredentials dbObj = new EntityCredentials();
        dbObj.setId(1L);
        dbObj.setEntityType(EntityType.ROOT_FOLDER.getName());

        when(entityCredentialsRepository.save(captor.capture())).thenReturn(dbObj);
        when(entityCredentialsRepository.getOne(1L)).thenReturn(dbObj);

        entityCredentialsService.updateEntityCredentials(entityCredentialsDto, rootFolder);

        final EntityCredentials argument = captor.getValue();
        verify(entityCredentialsRepository, atLeastOnce()).save(argument);

        Assert.assertTrue(TEST_PASSWORD.equals(EncryptionUtils.decryptUsingPrivateKeyStr(keys.getPrivateKey(), argument.getPassword())));
        Assert.assertTrue(argument.getModifiedTimeStampMs() > 0);
    }

    @Test
    public void testCreateKeysFirstTime() throws Exception {
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setId(1L);

        RootFolder folder = new RootFolder();
        folder.setId(1L);
        List<RootFolder> rootFolders = Collections.singletonList(folder);

        CustomerDataCenter customerDataCenter = new CustomerDataCenter();
        customerDataCenter.setId(1L);

        when(docStoreService.getRootFoldersForDataCenter(1L)).thenReturn(rootFolders);
        when(customerDataCenterRepository.getOne(1L)).thenReturn(customerDataCenter);

        RsaKey newKey = entityCredentialsService.generateDataCenterNewKeys(customerDataCenterDto, null);
        Assert.assertTrue(newKey != null && newKey.getPrivateKey() != null && newKey.getPrivateKey().length() > 5);
        verify(customerDataCenterRepository, atLeastOnce()).getOne(1L);
        verify(customerDataCenterRepository, atLeastOnce()).save(customerDataCenter);
    }

    @Test
    public void testCreateKeysNotFirstTimeNoOldKey() throws Exception {
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setId(1L);
        customerDataCenterDto.setPublicKey("dfgdfgdfgdfgd");

        RootFolder folder = new RootFolder();
        folder.setId(1L);
        List<RootFolder> rootFolders = Collections.singletonList(folder);

        CustomerDataCenter customerDataCenter = new CustomerDataCenter();
        customerDataCenter.setId(1L);

        EntityCredentials c = new EntityCredentials();
        c.setId(1L);
        List<EntityCredentials> credentials = Collections.singletonList(c);

        when(docStoreService.getRootFoldersForDataCenter(1L)).thenReturn(rootFolders);
        when(customerDataCenterRepository.getOne(1L)).thenReturn(customerDataCenter);
        when(entityCredentialsRepository.findForEntities(Collections.singletonList(1L), EntityType.ROOT_FOLDER.getName())).thenReturn(credentials);

        RsaKey newKey = entityCredentialsService.generateDataCenterNewKeys(customerDataCenterDto, null);
        Assert.assertTrue(newKey != null && newKey.getPrivateKey() != null && newKey.getPrivateKey().length() > 5);
        verify(entityCredentialsRepository, atLeastOnce()).deleteForEntityIds(Collections.singletonList(1L), EntityType.ROOT_FOLDER.getName());
        verify(customerDataCenterRepository, atLeastOnce()).getOne(1L);
        verify(customerDataCenterRepository, atLeastOnce()).save(customerDataCenter);
    }

    @Test
    public void testCreateKeysNotFirstTimeWithOldKey() throws Exception {

        // generate an old key
        RsaKey keys = EncryptionUtils.generateNewKeysComplete();

        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setId(1L);
        customerDataCenterDto.setPublicKey(keys.getPublicKey());

        RootFolder folder = new RootFolder();
        folder.setId(1L);
        List<RootFolder> rootFolders = Collections.singletonList(folder);

        CustomerDataCenter customerDataCenter = new CustomerDataCenter();
        customerDataCenter.setId(1L);

        String passwordToEncrypt = TEST_PASSWORD;
        EntityCredentials c = new EntityCredentials();
        c.setId(1L);
        c.setUsername(TEST_USERNAME);
        c.setPassword(EncryptionUtils.encryptUsingPublicKeyStr(keys.getPublicKey(), passwordToEncrypt));
        List<EntityCredentials> credentials = Collections.singletonList(c);

        when(docStoreService.getRootFoldersForDataCenter(1L)).thenReturn(rootFolders);
        when(customerDataCenterRepository.getOne(1L)).thenReturn(customerDataCenter);
        when(entityCredentialsRepository.findForEntities(Collections.singletonList(1L), EntityType.ROOT_FOLDER.getName())).thenReturn(credentials);

        final ArgumentCaptor<EntityCredentials> captor = ArgumentCaptor.forClass(EntityCredentials.class);

        RsaKey newKey = entityCredentialsService.generateDataCenterNewKeys(customerDataCenterDto, keys.getPrivateKey());
        Assert.assertTrue(newKey != null && newKey.getPrivateKey() != null && newKey.getPrivateKey().length() > 5);
        verify(entityCredentialsRepository, atLeastOnce()).save(captor.capture());
        verify(customerDataCenterRepository, atLeastOnce()).getOne(1L);
        verify(customerDataCenterRepository, atLeastOnce()).save(customerDataCenter);

        final EntityCredentials argument = captor.getValue();
        Assert.assertTrue(passwordToEncrypt.equals(EncryptionUtils.decryptUsingPrivateKeyStr(newKey.getPrivateKey(), argument.getPassword())));
        Assert.assertTrue(argument.getModifiedTimeStampMs() > 0);
    }
}