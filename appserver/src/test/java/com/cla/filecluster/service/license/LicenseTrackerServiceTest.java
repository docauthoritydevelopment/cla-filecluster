package com.cla.filecluster.service.license;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.entity.license.LicenseResource;
import com.cla.filecluster.repository.jpa.license.DocAuthorityLicenseRepository;
import com.cla.filecluster.repository.jpa.license.LicenseResourceRepository;
import com.cla.filecluster.repository.jpa.license.LicenseTrackerRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import io.netty.handler.codec.json.JsonObjectDecoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class LicenseTrackerServiceTest {

    @InjectMocks
    private LicenseTrackerService licenseTrackerService;

    @InjectMocks
    private LicenseService licenseService;

    @Mock
    private DocAuthorityLicenseRepository docAuthorityLicenseRepository;

    @Mock
    private LicenseResourceRepository licenseResourceRepository;

    @Mock
    private LicenseTrackerRepository licenseTrackerRepository;

    @Mock
    private MessageHandler messageHandler;

    @Before
    public void init() {
        ReflectionTestUtils.setField(licenseService, "licenseResourceRepository", licenseResourceRepository);
        ReflectionTestUtils.setField(licenseTrackerService, "licenseService", licenseService);
        ReflectionTestUtils.setField(licenseTrackerService, "licenseTrackerRepository", licenseTrackerRepository);
        setLicenseServerId();
    }

    @After
    public void resetMocks() {
        Mockito.reset(licenseResourceRepository);
        Mockito.reset(docAuthorityLicenseRepository);
        Mockito.reset(licenseTrackerRepository);
    }

    @Test
    public void testSetLicenseTracker() {
        assertNull(licenseTrackerService.getTrackerValue("notFound"));
        assertEquals(0l, licenseTrackerService.getTrackerUpdateTime("notFound"));

        assertNull(licenseTrackerService.setTrackerValue("key1", "value1-1", true));
        assertEquals(licenseTrackerService.setTrackerValue("key1", "value1-2", true), "value1-1");
        long diffTime = System.currentTimeMillis() - licenseTrackerService.getTrackerUpdateTime("key1");
        assertTrue(diffTime >= 0 && diffTime < 1000);

        licenseTrackerService.setTrackerValue("key2", "value2-1", false);
        licenseTrackerService.setTrackerValue("key3", "value3-1", false);

        licenseTrackerService.flushAll(true);
    }

    private class MyData implements Serializable {
        String strVal;
        Long longVal;
        int intVal;

        public MyData(String strVal, Long longVal, int intVal) {
            this.strVal = strVal;
            this.longVal = longVal;
            this.intVal = intVal;
        }
    }

    @Test
    public void getSetJsonLicenseTracker() {
        Long md1 = 123456L;
        licenseTrackerService.setTrackerLongValue("jsonKey1", md1, false);
        Serializable str1 = licenseTrackerService.getTrackerValue("jsonKey1");
        Long md2 = licenseTrackerService.getTrackerLongValue("jsonKey1");
        assertEquals(md1, md2);
    }

    private boolean setLicenseServerId() {
        Map<String, String> restrictions = new LinkedHashMap<>();
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setCreationDate(645645646);
        docAuthorityLicenseDto.setRestrictions(restrictions);
        restrictions.put("crawlerExpiry", "*");
        restrictions.put("loginExpiry", " 5464466546 ");
        restrictions.put("maxFiles", "*");
        restrictions.put("maxRootFolders", "17");
        restrictions.put("serverId", "1760-dfgdsss");
        boolean res = licenseService.importLicense(docAuthorityLicenseDto, true);
        return res;
    }
}