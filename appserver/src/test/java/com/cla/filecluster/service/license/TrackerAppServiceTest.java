package com.cla.filecluster.service.license;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.repository.jpa.license.DocAuthorityLicenseRepository;
import com.cla.filecluster.repository.jpa.license.LicenseResourceRepository;
import com.cla.filecluster.repository.jpa.license.LicenseTrackerRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.scan.TrackingKeys;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class TrackerAppServiceTest {

    @InjectMocks
    private LicenseTrackerService licenseTrackerService;

    @InjectMocks
    private LicenseService licenseService;

    @InjectMocks
    private TrackerAppService trackerAppService;

    @Mock
    private DocAuthorityLicenseRepository docAuthorityLicenseRepository;

    @Mock
    private LicenseResourceRepository licenseResourceRepository;

    @Mock
    private LicenseTrackerRepository licenseTrackerRepository;

    @Mock
    private MessageHandler messageHandler;

    @Before
    public void init() {
        ReflectionTestUtils.setField(licenseService, "licenseResourceRepository", licenseResourceRepository);
        ReflectionTestUtils.setField(licenseTrackerService, "licenseService", licenseService);
        ReflectionTestUtils.setField(licenseTrackerService, "licenseTrackerRepository", licenseTrackerRepository);
        ReflectionTestUtils.setField(trackerAppService, "licenseTrackerService", licenseTrackerService);
    }

    @After
    public void resetMocks() {
        Mockito.reset(licenseResourceRepository);
        Mockito.reset(docAuthorityLicenseRepository);
        Mockito.reset(licenseTrackerRepository);
        setLicenseServerId();
    }

    @Test
    public void trackerAppCountersTest() {
        assertEquals(0L, trackerAppService.getLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY));
        trackerAppService.setLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY, 123);
        assertEquals(123, trackerAppService.getLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY));
        assertEquals(123 + 456, trackerAppService.incrementLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY, 456));
        assertEquals(123 + 456, trackerAppService.getLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY));

        trackerAppService.setLongCount(TrackingKeys.rootFolderVolumeKey(5), 234);
        assertEquals(234, trackerAppService.getLongCount(TrackingKeys.rootFolderVolumeKey(5)));
        assertEquals(0L, trackerAppService.getLongCount(TrackingKeys.rootFolderVolumeKey(8)));
        assertEquals(234 + 567, trackerAppService.incrementLongCount(TrackingKeys.rootFolderVolumeKey(5), 567));
        assertEquals(234 + 567, trackerAppService.getLongCount(TrackingKeys.rootFolderVolumeKey(5)));

        trackerAppService.setLongCount(TrackingKeys.mediaVolumeKey(MediaType.FILE_SHARE), 345);
        assertEquals(345, trackerAppService.getLongCount(TrackingKeys.mediaVolumeKey(MediaType.FILE_SHARE)));
        assertEquals(0L, trackerAppService.getLongCount(TrackingKeys.mediaVolumeKey(MediaType.BOX)));
        assertEquals(345 + 678, trackerAppService.incrementLongCount(TrackingKeys.mediaVolumeKey(MediaType.FILE_SHARE), 678));
        assertEquals(345 + 678, trackerAppService.getLongCount(TrackingKeys.mediaVolumeKey(MediaType.FILE_SHARE)));
    }

    private boolean setLicenseServerId() {
        Map<String, String> restrictions = new LinkedHashMap<>();
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setCreationDate(645645646);
        docAuthorityLicenseDto.setRestrictions(restrictions);
        restrictions.put("crawlerExpiry", "*");
        restrictions.put("loginExpiry", " 5464466546 ");
        restrictions.put("maxFiles", "*");
        restrictions.put("maxRootFolders", "17");
        restrictions.put("serverId", "1760-dfgdsss");
        boolean res = licenseService.importLicense(docAuthorityLicenseDto, true);
        return res;
    }
}