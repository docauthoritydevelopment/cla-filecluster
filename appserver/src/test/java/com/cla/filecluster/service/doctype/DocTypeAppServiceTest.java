package com.cla.filecluster.service.doctype;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.util.categories.FileCatUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class DocTypeAppServiceTest {

    private DocTypeAppService docTypeAppService = new DocTypeAppService();

    @SuppressWarnings({"unchecked"})
    @Test
    public void testExtractGroupedDocTypeIdentifiers(){
        FacetField f = new FacetField("name1");
        f.add("dt.g.61..fileGroup;dt.g.6..f0", 42625L);
        f.add("dt.g.21..f0", 1086L);
        f.add("dt.g.61..fileGroup", 864);
        f.add("dt.g.6..f0", 751);
        f.add("dt.g.61..fileGroup;dt.g.57..f0", 346);
        f.add("dt.g.60..f0", 72);
        f.add("dt.g.61..fileGroup;dt.g.60..f0", 39);
        f.add("dt.g.6.7..f0", 35);
        f.add("dt.g.61..fileGroup;dt.g.6.7..f0", 31);
        f.add("dt.g.61..fileGroup;dt.g.6..f3;dt.g.6..f0", 8);
        f.add("dt.g.1..fileGroup;dt.g.21..f0", 3);
        f.add("dt.g.6..fileGroup;dt.g.21..f0", 3);
        f.add("dt.21", 1);
        f.add("dt.57", 1);
        f.add("dt.g.57..f2", 1);
        f.add("dt.g.61..fileGroup;dt.g.21..f0", 1);

        try {
            Method method = DocTypeAppService.class.getDeclaredMethod("extractGroupedDocTypeIdentifiers", FacetField.class);
            method.setAccessible(true);
            List<Pair<String, List<FacetField.Count>>> pairs =
                    (List<Pair<String, List<FacetField.Count>>>) method.invoke(docTypeAppService, f);
            Assert.assertEquals(8, pairs.size());
            Assert.assertEquals(7, pairs.get(0).getValue().size());
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            Assert.fail(e.getMessage());
        }
    }

}
