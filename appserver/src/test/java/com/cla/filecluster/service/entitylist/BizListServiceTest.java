package com.cla.filecluster.service.entitylist;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.BizListSchema;
import com.cla.filecluster.repository.jpa.entitylist.BizListRepository;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class BizListServiceTest {

    @Mock
    private BizListRepository bizListRepository;

    @Mock
    private EntityListSchemaLoader entityListSchemaLoader;

    @InjectMocks
    private BizListService bizListService;

    @Before
    public void init() {
    }

    @After
    public void resetMocks() {
        Mockito.reset(bizListRepository);
        Mockito.reset(entityListSchemaLoader);
    }

    @Test
    public void testPopulateSchema() {
        BizList bizList = new BizList();
        bizList.setId(1);
        bizList.setActive(true);
        bizList.setBizListItemType(BizListItemType.CONSUMERS);
        bizList.setBizListSolrId("blt.1");
        bizList.setRulesFileName("./config/extraction/extraction-rules-NameOnly.txt");
        bizList.setTemplate("NameOnly");
        bizList.setDescription("predefined bizList");
        bizList.setBizListSource("source");
        when(bizListRepository.findByName("Popular Names US")).thenReturn(bizList);
        BizListSchema schema = new BizListSchema();
        when(entityListSchemaLoader.loadCsvSchema(bizList)).thenReturn(schema);
        BizList byName = bizListService.findByName("Popular Names US", true);
        assertTrue(byName.getBizListSchema().equals(schema));
    }
}
