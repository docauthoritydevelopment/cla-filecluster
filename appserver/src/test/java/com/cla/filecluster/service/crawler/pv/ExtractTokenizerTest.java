package com.cla.filecluster.service.crawler.pv;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by: yael
 * Created on: 12/4/2017
 */
@Category(UnitTestCategory.class)
public class ExtractTokenizerTest {

    private ExtractTokenizer extractTokenizer;

    @Before
    public void init() {
        extractTokenizer = new ExtractTokenizer();
        List<String> removalStrs = new ArrayList<>();
        removalStrs.add("999999999");
        ReflectionTestUtils.setField(extractTokenizer, "removalStrs", removalStrs);

        ReflectionTestUtils.setField(extractTokenizer, "ignoreChars", "[\\-\\']");
        ReflectionTestUtils.setField(extractTokenizer, "separatorChars", "");
        ReflectionTestUtils.setField(extractTokenizer, "minValueLength", 3);
        extractTokenizer.setDebugHashMapper(new HashMap<>());
    }

    @Test
    public void testGetHashList() {
        String input = "A Cat is LOoking for some one to eAt";
        List<Long> result = extractTokenizer.getHashList(input, 4);
        assertTrue(result.size() == 2);
    }

    @Test
    public void testGetNgrams() {
        String input = "A Cat is LOoking for some one to eAt";
        List<String> result = extractTokenizer.getNgrams(input, 4);
        assertTrue(result.size() == 2);
        assertTrue(result.contains("cat looking some one"));
        assertTrue(result.contains("looking some one eat"));

        result = extractTokenizer.getNgrams(input, 3);
        assertTrue(result.size() == 3);
        assertTrue(result.contains("cat looking some"));
        assertTrue(result.contains("looking some one"));
        assertTrue(result.contains("some one eat"));
    }

    @Test
    public void testGetHash() {
        ReflectionTestUtils.setField(extractTokenizer, "separatorChars", "_");
        String input = "a cat-999999999-is- looking_for some_one to eat";
        String result = extractTokenizer.getHash(input);
        String[] words = result.split(" ");
        assertEquals(6, words.length);
        for (String s : words) {
            assertTrue(s.matches("[A-Z0-9]{10,20}"));
        }
    }

    @Test
    public void testFilter() {
        ReflectionTestUtils.setField(extractTokenizer, "separatorChars", "_");
        String input = "a cat-999999999-is- looking_for some_one to eat";
        String result = extractTokenizer.filter(input);
        assertEquals("a cat is looking for some one to eat", result);

        input = "-999999999-__";
        result = extractTokenizer.filter(input);
        assertEquals("   ", result);

        input = "-''";
        result = extractTokenizer.filter(input);
        assertEquals(null, result);
    }

    @Test
    public void testFillTokens() {
        String input = "a cat went to the market to buy milk";
        Collection<Long> tokens = new ArrayList<>();
        List<String> result = extractTokenizer.fillTokens(input, tokens);
        Map<Long, String> debugger = extractTokenizer.getDebugHashMapper();
        assertTrue(result.contains("cat"));
        assertTrue(result.contains("went"));
        assertTrue(!result.contains("a"));
        assertTrue(!result.contains("to"));
        assertTrue(result.size() == 6);
        assertTrue(tokens.size() == 6);
        assertTrue(debugger.size() == 6);
        assertTrue(debugger.containsKey(tokens.iterator().next()));
        assertTrue(debugger.containsValue("cat"));
    }

    @Test
    public void testTrimToken() {
        assertEquals("ghfghfh", ExtractTokenizer.trimToken("ghfghfh"));
        assertEquals("ghfghfh", ExtractTokenizer.trimToken("    ghfghfh"));
        assertEquals("ghfghfh", ExtractTokenizer.trimToken("    ghfghfh    "));
        assertEquals("ghf ghfh", ExtractTokenizer.trimToken("    ghf ghfh"));
    }

    @Test
    public void testHebrewSimpleStem() {
        assertEquals("ghfghfh", ExtractTokenizer.hebrewSimpleStem("\ud791ghfghfh"));
        assertEquals("ghfghfh", ExtractTokenizer.hebrewSimpleStem("\ud79cghfghfh"));
        assertEquals(null, ExtractTokenizer.hebrewSimpleStem("ghfghfh"));
        assertEquals(null, ExtractTokenizer.hebrewSimpleStem("ghf\ud791ghfh"));
    }
}
