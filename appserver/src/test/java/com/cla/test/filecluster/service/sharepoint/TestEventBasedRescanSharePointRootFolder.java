package com.cla.test.filecluster.service.sharepoint;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.it.categories.SharePointTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.data.domain.PageImpl;

import java.util.Map;

@Category(SharePointTestCategory.class)
@Ignore
public class TestEventBasedRescanSharePointRootFolder extends  TestEventBasedRescan {

    private static final String TEST_FILE_NAME = "FAMSS-livingwill - 2.doc";

    private void configureSharePointConnector() {
        logger.info("Configure SharePoint Connector");
        sharePointConnectionDetails = performanceTestService.createSharePointConnectionDetails();
        if (!mediaConnectionDetailsService.isConnectionDetailsExists(sharePointConnectionDetails.getName())) {
            sharePointConnectionDetails = (SharePointConnectionDetailsDto) sharePointApiController.configureSharePointConnectionDetails(sharePointConnectionDetails);
        }
    }

    @Test
    public void testEventBasedRescanMechanism() {
        configureSharePointConnector();
        performanceTestService.printRunBanner();
        RootFolderDto rootFolderDto = configureRootFolder(PerformanceTestService.SITES_TEST_RESCAN_TESTS_LIBRARY);
        docStoreService.updateRootFolderFoldersCount(rootFolderDto.getId(),1); // So rescan is triggered instead of initial scan
        logger.info("Scan rootFolder {}",rootFolderDto);
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(),performanceTestService.createWaitParams());
        Map<String, String> rootFolderIdFilter = performanceTestService.createFilterParams("rootFolderId", String.valueOf(rootFolderDto.getId()));
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(rootFolderIdFilter);
        Assert.assertThat("There should be at least one folder with files",foldersFileCount.getContent().size(), Matchers.greaterThan(0));
        PageImpl<ClaFileDto> fileCounts = filesApiController.findFilesDeNormalized(performanceTestService.createFilterParams("baseName", TEST_FILE_NAME));
        ClaFileDto claFileDto = fileCounts.getContent().get(0);
        claFileDto.setBaseName("testEventBasedRescanMechanism");
        updateClaFileProperties(claFileDto);
        logger.info("Rescan rootFolder again - for one new event");
        updateRootFolderNickName(rootFolderDto,RESCAN_EVENTS_SYSTEM_TESTS + "100");
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(), performanceTestService.createWaitParams());
        fileCounts = filesApiController.findFilesDeNormalized(performanceTestService.createFilterParams("baseName", TEST_FILE_NAME));
        Assert.assertThat("Verify that the file with the updated name exists",fileCounts.getContent().size(),Matchers.equalTo(1));
        Assert.assertEquals(TEST_FILE_NAME,fileCounts.getContent().get(0).getBaseName());

//        fileCounts = filesApiController.findFilesDeNormalized(performanceTestService.createFilterParams("baseName", "testEventBasedRescanMechanism"));
//        Assert.assertThat(fileCounts.getContent().size(),Matchers.equalTo(0));
//
//        logger.debug("Yet another rerun (of 10 events)");
//        updateRootFolderNickName(rootFolderDto,FileScannerService.RESCAN_EVENTS_SYSTEM_TESTS+"10");
//        fileCrawlerApiController.rerunOnRootFolder(rootFolderDto.getId(),performanceTestService.createWaitParams());
//
//        logger.debug("Yet another rerun (of remaining events)");
//        updateRootFolderNickName(rootFolderDto,FileScannerService.RESCAN_EVENTS_SYSTEM_TESTS+"100");
//        fileCrawlerApiController.rerunOnRootFolder(rootFolderDto.getId(),performanceTestService.createWaitParams());

    }

    @Override
    protected String getFolderNickName() {
        return RESCAN_EVENTS_SYSTEM_TESTS + "8";
    }


}
