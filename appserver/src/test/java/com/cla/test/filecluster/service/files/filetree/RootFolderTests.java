package com.cla.test.filecluster.service.files.filetree;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.generic.ImportRowResultDto;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.exceptions.EntityAlreadyExistsException;
import com.cla.filecluster.service.api.CustomerDataCenterApiController;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.ScheduleApiController;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.files.filetree.RootfolderSchemaLoader;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.commons.compress.utils.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by uri on 01/11/2015.
 */
@Category(FastTestCategory.class)
public class RootFolderTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(RootFolderTests.class);

    @Autowired
    private ScheduleApiController scheduleApiController;

    @Autowired
    private RootfolderSchemaLoader rootfolderSchemaLoader;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private CustomerDataCenterApiController customerDataCenterApiController;

    public static final String ENTITIES_FOLDER = "./src/test/resources/entities";

    private SharePointConnectionDetailsDto  sharePointConnectionDetailsDto;

    @Before
    public void scanBaseDir() {
      //  defaultDocStore = docStoreService.getDefaultDocStoreAsDto();
     //   rootFolderDto = performanceTestService.analyzeReferenceFiles2();
        SharePointConnectionDetailsDto connection = performanceTestService.createSharePointConnectionDetails();
        if (!mediaConnectionDetailsService.isConnectionDetailsExists(connection.getName())) {
            sharePointConnectionDetailsDto = (SharePointConnectionDetailsDto) mediaConnectionDetailsService.configureMicrosoftConnectionDetails(connection);
            Assert.assertNotNull(sharePointConnectionDetailsDto.getId());
        }

    }

    private CustomerDataCenterDto createCustomerDataCenter(String name, Boolean defaultDataCenter) {
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setName(name);
        customerDataCenterDto.setDefaultDataCenter(defaultDataCenter);
        customerDataCenterDto = customerDataCenterApiController.createCustomerDataCenter(customerDataCenterDto);
        return customerDataCenterDto;
    }

    @Test
    public void testListRootFolders() {
        testStartLog(logger);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<RootFolderDto> rootFolderDtos = fileTreeAppService.listAllRootFoldersInDefaultDocStore(dataSourceRequest);
        logger.debug("Got a total of {} root folders",rootFolderDtos.getNumberOfElements());
        Assert.assertThat("There should be at least one element",rootFolderDtos.getContent(), Matchers.not(Matchers.emptyIterableOf(RootFolderDto.class)));
    }

    @Ignore     // TODO: @@ fix this test
    @Test(expected = EntityAlreadyExistsException.class)
    public void testAddExistingRootFolder() {
        testStartLog(logger);
        String scanPath = "."+PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST;

        RootFolderDto rootFolderDto = performanceTestService.createFileShareRootFolder(scanPath, CrawlerType.MEDIA_PROCESSOR);
      //  throw new RuntimeException("We shouldn't reach this state: "+rootFolderDto.getPath());
    }

    @Test
    public void testAddAndRemoveRootFolder() {
        testStartLog(logger);
        String scanPath = "."+PerformanceTestService.SRC_TEST_RESOURCES_FILES_GRADUAL;
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        rootFolderDto.setPath(scanPath);

        Long scheduledGroupId = scheduleGroupService.getScheduleGroupsNames().keySet().iterator().next();

        rootFolderDto = rootFolderService.createRootFolder(rootFolderDto,scheduledGroupId);

        FolderExcludeRuleDto folderExcludeRuleDto = new FolderExcludeRuleDto();
        folderExcludeRuleDto.setMatchString(scanPath + "/subFolder");
        fileTreeApiController.addRootFolderExcludeRule(rootFolderDto.getId(),folderExcludeRuleDto);
        Page<FolderExcludeRuleDto> folderExcludeRuleDtos = fileTreeApiController.listFolderExcludeRulesByRootFolder(rootFolderDto.getId(), null);
        Assert.assertEquals(1,folderExcludeRuleDtos.getContent().size());
        Assert.assertEquals("Verify new exclude folder rule is enabled",folderExcludeRuleDtos.getContent().get(0).isDisabled(),false);
        logger.debug("Delete root folder: {}",rootFolderDto.getId());
        fileTreeApiController.deleteRootFolder(rootFolderDto.getId());
    }

    @Test
    public void testFilterNewRootFolders() {
        testStartLog(logger);
        long totalElements = fileTreeApiController.listRootFolders(null).getTotalElements();
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor filter = new FilterDescriptor("numberOfFoldersFound","0","eq");
        dataSourceRequest.setFilter(filter);
        Page<RootFolderDto> rootFolderDtos = fileTreeAppService.listAllRootFoldersInDefaultDocStore(dataSourceRequest);
        logger.debug("Got a total of {} root folders",rootFolderDtos.getNumberOfElements());
        for (RootFolderDto folderDto : rootFolderDtos) {
            if (folderDto.getNumberOfFoldersFound() != null) {
                Assert.assertEquals("Verify all returned root folders doesn't have any scanned folders",0L,
                        folderDto.getNumberOfFoldersFound().longValue());
            }
        }
        filter = new FilterDescriptor("numberOfFoldersFound","0","gt");
        dataSourceRequest.setFilter(filter);
        Page<RootFolderDto> rootFolderDtos2 = fileTreeAppService.listAllRootFoldersInDefaultDocStore(dataSourceRequest);
        logger.debug("Got a total of {} root folders",rootFolderDtos2.getNumberOfElements());

        Assert.assertEquals("Verify all folders are returned",totalElements,rootFolderDtos.getTotalElements()+rootFolderDtos2.getTotalElements());
    }

    @Test
    public void testUpdateRootFolder() {
        testStartLog(logger);
        RootFolderDto rootFolderDto2 = new RootFolderDto();
        rootFolderDto2.setPath("testUpdateRootFolder");
        rootFolderDto2.setMediaType(MediaType.FILE_SHARE);
        Long scheduledGroupId = scheduleGroupService.getScheduleGroupsNames().keySet().iterator().next();
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto2, scheduledGroupId);
        Assert.assertEquals(MediaType.FILE_SHARE,rootFolder.getMediaType());
        RootFolderDto rootFolderDtoUpdated = fileTreeApiController.updateRootFolder(rootFolder.getId(), rootFolderDto2);
        Assert.assertEquals(MediaType.FILE_SHARE,rootFolderDtoUpdated.getMediaType());
    }

    @Ignore
    @Test
    public void testCreateAndUpdateRootFolderWithDataCenter() {
        testStartLog(logger);
        CustomerDataCenterDto customerDataCenterDto = createCustomerDataCenter("testCreateAndUpdateRootFolderWithDataCenter", false);
        RootFolderDto rootFolderDto2 = new RootFolderDto();
        rootFolderDto2.setPath("testCreateAndUpdateRootFolderWithDataCenter");
        rootFolderDto2.setMediaType(MediaType.FILE_SHARE);
        rootFolderDto2.setCustomerDataCenterDto(customerDataCenterDto);

        RootFolderDto rootFolderDto = fileTreeApiController.createRootFolder(rootFolderDto2, null);

        Assert.assertEquals("Rootfolder should be created with specific data center  ", rootFolderDto.getCustomerDataCenterDto().getId(),customerDataCenterDto.getId());

        CustomerDataCenterDto customerDataCenterDto2 = createCustomerDataCenter("testCreateAndUpdateRootFolderWithDataCenter2", false);
        rootFolderDto.setCustomerDataCenterDto(customerDataCenterDto2);
        RootFolderDto updatedRootFolderDto = fileTreeApiController.updateRootFolder(rootFolderDto.getId(), rootFolderDto);
        Assert.assertEquals("Rootfolder should be updated with specific data center  ", updatedRootFolderDto.getCustomerDataCenterDto().getId(),customerDataCenterDto2.getId());


    }


    @Test
    public void testImportRootfoldersFromCsv_validation_WarningsFound() throws IOException {
        logger.info("testImportRootfoldersFromCsv_validation_WarningsFound");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(true,rawData,100,false,true);

        //total 3 rows in file. 1 ok, 2 with warnings
        Assert.assertEquals("Should have accept validation for 1 rootfolders without warnings",1,importSummaryResultDto.getImportedRowCount());
        Assert.assertEquals("Should have no errors for import validation",0,importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no duplications for import validation",0,importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have 3 warnings for import validation",3,importSummaryResultDto.getWarningRowCount());

        Assert.assertNotNull(importSummaryResultDto.getWarningImportRowResultDtos());
        ImportRowResultDto itemRow =  importSummaryResultDto.getWarningImportRowResultDtos().stream()
                .filter(x-> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("D:/Missoula/Admin/"))
                .findFirst().get();
        Assert.assertNotNull("Should have warning message for schedule group",itemRow.getMessage());

        Assert.assertNotNull(importSummaryResultDto.getWarningImportRowResultDtos());
        ImportRowResultDto itemRow1 =  importSummaryResultDto.getWarningImportRowResultDtos().stream()
                .filter(x-> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("C:/CatFiles/"))
                .findFirst().get();
        Assert.assertNotNull("Should have warning message for schedule group",itemRow1.getMessage());

        ImportRowResultDto itemRow2 =  importSummaryResultDto.getWarningImportRowResultDtos().stream()
                .filter(x-> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("D:/MissingFileTypes/"))
                .findFirst().get();
        Assert.assertNotNull("Should have warning message for missing FILE TYPES",itemRow2.getMessage());
    }



    @Test
    public void testImportRootfoldersFromCsv_MissingRequiredFieldsForRowItem() throws IOException {
        testStartLog(logger);
        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import_with_errors.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(false,rawData,null,false);

        RootFolderDto rootFolderDto = rootFolderService.findRootFolder("C:/MissingRequiredFieldsTest/",true);
        Assert.assertNull("Rootfolder with errors should not be imported ", rootFolderDto);
        Assert.assertEquals("Total errors ",1, importSummaryResultDto.getErrorRowCount());
        Assert.assertNotNull("Imported rootfolder error message ", importSummaryResultDto.getErrorImportRowResultDtos().get(0).getMessage());
        Assert.assertEquals("Total imports should be zero ",0, importSummaryResultDto.getImportedRowCount());
    }

    @Test
    public void testImportRootfoldersFromCsv_validation_MissingHeaderRequiredField() throws IOException {
        logger.info("testImportRootfoldersFromCsv_MissingHeaderRequiredField");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import_missing_required_headers.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(true,rawData,100,false);

        Assert.assertNotNull("Should have error import message cause of missing required headers",importSummaryResultDto.getResultDescription());
        Assert.assertTrue("Should have error import cause of missing required headers",importSummaryResultDto.isImportFailed());
    }

  //  @Test
    public void testImportRootfoldersFromCsv() throws IOException {
        logger.info("testImportRootfoldersFromCsv");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(false,rawData,null,false);
        Assert.assertEquals("Should have no errors for import ",0,importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no duplications for import ",0,importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have 3 imports with warnings",3,importSummaryResultDto.getWarningRowCount());
        Assert.assertEquals("Should have imported 1 rootfolders without warnings",1,importSummaryResultDto.getImportedRowCount());
        Assert.assertNotNull("Should have row headers for import csv",importSummaryResultDto.getRowHeaders());



        RootFolderDto rootFolderDto = rootFolderService.findRootFolder("C:/CatFiles/",false);
        Assert.assertNotNull("Rootfolder with schedule group not found was imported and inserted o db", rootFolderDto);
        Assert.assertEquals("MediaType was imported correctly",MediaType.FILE_SHARE, rootFolderDto.getMediaType());
        Assert.assertEquals("isRescanActive was imported correctly", true,rootFolderDto.isRescanActive());
        Assert.assertEquals("getPath was imported correctly", "C:/CatFiles/",rootFolderDto.getPath());
        Assert.assertEquals("2 file types are expected", 2,rootFolderDto.getFileTypes().length);

        RootFolderDto warningRootFolderDto = rootFolderService.findRootFolder("D:/Missoula/Admin/",false);
        Assert.assertNotNull("Rootfolder with schedule group not found was imported and inserted o db", warningRootFolderDto);
        Assert.assertEquals("MediaType was imported correctly", MediaType.FILE_SHARE, warningRootFolderDto.getMediaType());
        Assert.assertEquals("NickName was imported correctly", "Admin",warningRootFolderDto.getNickName());
        Assert.assertEquals("StoreLocation was imported correctly", StoreLocation.LOCAL_DATACENTER,warningRootFolderDto.getStoreLocation());
        Assert.assertEquals("3 file types are expected", 3,warningRootFolderDto.getFileTypes().length);

        RootFolderDto warning2RootFolderDto = rootFolderService.findRootFolder("D:/MissingFileTypes/",false);
        Assert.assertNotNull("Rootfolder with missing file types was imported and inserted o db", warning2RootFolderDto);
    }
  //  @Test
    public void testImportRootfoldersFromCsv_ScheduleGroup_Found() throws IOException {
        logger.info("testImportRootfoldersFromCsv_ScheduleGroup_Found");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import3.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);

        ScheduleGroupDto schedule = new ScheduleGroupDto();
        schedule.setName("testImportRootfolderScheduleGroupFromCsv");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(schedule);
        Assert.assertNotNull(scheduleGroup.getId());

        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(false,rawData,null,false);

        RootFolderDto rootFolderDto = rootFolderService.findRootFolder("C:/ScheduleGroup/a/",false);

        RootFolderSummaryInfo rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderDto.getId());

        Assert.assertNotNull("Imported rootfolder should have schedule group",rootFolderSummaryInfo.getScheduleGroupDtos());
        ScheduleGroupDto scheduleGroupDto = rootFolderSummaryInfo.getScheduleGroupDtos().stream().findFirst().get();
        Assert.assertEquals("Schedule group was imported correctly", "testImportRootfolderScheduleGroupFromCsv",scheduleGroupDto.getName());

    }

    @Test
    public void testImportRootfoldersFromCsv_ScheduleGroup_NotFound() throws IOException {
        logger.info("testImportRootfoldersFromCsv_ScheduleGroup_NotFound");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import2.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(false,rawData,null,false,true);


        RootFolderDto rootFolderDto = rootFolderService.findRootFolder("D:/ScheduleGroup/NotFound/",true);

        RootFolderSummaryInfo rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderDto.getId());

        Assert.assertNotNull(rootFolderSummaryInfo.getScheduleGroupDtos());
        ScheduleGroupDto scheduleGroupDto = rootFolderSummaryInfo.getScheduleGroupDtos().stream().findFirst().get();
        Assert.assertEquals("Schedule group name was imported but not found, create missing schedule ", "Daily",scheduleGroupDto.getName());
    }

   // @Test
    public void testImportRootfoldersFromCsv_Duplications() throws IOException {
        logger.info("testImportRootfoldersFromCsv_Duplications");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import_with_dup.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = rootfolderSchemaLoader.doImport(false,rawData,null,false);

        Assert.assertEquals("Should have no errors for import ",0,importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no imports with warnings",0,importSummaryResultDto.getWarningRowCount());
        Assert.assertEquals("Should have imported 2 rootfolders without warnings",2,importSummaryResultDto.getImportedRowCount());
        Assert.assertEquals("Should have 1 duplications for import ",1,importSummaryResultDto.getDuplicateRowCount());



        RootFolderDto rootFolderDto = rootFolderService.findRootFolder("D:/Dup1/",true);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        ScheduleGroupDto origScheduleGroupDto = scheduleAppService.listRootFolderScheduleGroups(rootFolderDto.getId(),dataSourceRequest).getContent().get(0);
        Assert.assertNotNull("Root folder with dup was imported", rootFolderDto);

        ImportSummaryResultDto importSummaryResultDto2 =  rootfolderSchemaLoader.doImport(false,rawData,null,false);
        Assert.assertEquals("Should have no errors for import ",0,importSummaryResultDto2.getErrorRowCount());
        Assert.assertEquals("Should have 3 duplications for import ",3,importSummaryResultDto2.getDuplicateRowCount());
        Assert.assertEquals("Should have no imports with warnings",0,importSummaryResultDto2.getWarningRowCount());
        Assert.assertEquals("Should have imported 0 rootfolders without warnings",0,importSummaryResultDto2.getImportedRowCount());

        ScheduleGroupDto schedule = new ScheduleGroupDto();
        schedule.setName("testImportRootfoldersFromCsv_Duplications");
        ScheduleGroupDto scheduleGroup = scheduleAppService.createScheduleGroup(schedule);
        Assert.assertNotNull(scheduleGroup.getId());
        scheduleAppService.attachRootFolderToSchedulingGroup(scheduleGroup.getId(),rootFolderDto.getId());
        Assert.assertTrue("Should attach new schedule group to existing rootfolder",scheduleAppService.isRootFolderAttachedToScheduleGroup(rootFolderDto.getId(),scheduleGroup.getId()));

        importSummaryResultDto =  rootfolderSchemaLoader.doImport(false,rawData,null,true);
         rootFolderDto = rootFolderService.findRootFolder("D:/Dup1/",true);

        Assert.assertEquals("Rootfolder imported should be updated with the schedule group from file",origScheduleGroupDto.getId(),scheduleAppService.listRootFolderScheduleGroups(rootFolderDto.getId(),dataSourceRequest).getContent().get(0).getId());

        Assert.assertEquals("Should have no errors for import ",0,importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no imports with warnings",0,importSummaryResultDto.getWarningRowCount());
        Assert.assertEquals("Should have imported 2 rootfolders without warnings",2,importSummaryResultDto.getImportedRowCount());
        Assert.assertEquals("Should have 1 duplications for import ",1,importSummaryResultDto.getDuplicateRowCount());

    }

    @Test
    public void testImportRootfoldersFromCsv_validation_Duplications() throws IOException {
        logger.info("testImportRootfoldersFromCsv_validation_Duplications");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import_with_dup.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);

        ImportSummaryResultDto validateImportSummaryResultDto =  rootfolderSchemaLoader.doImport(true,rawData,null,false);

        Assert.assertEquals("Should have accept validation for 0 rootfolders without warnings",3,validateImportSummaryResultDto.getImportedRowCount());
        Assert.assertEquals("Should have no errors for import validation",0,validateImportSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have 0 dup for import validation",0,validateImportSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have 0 warnings for import validation",0,validateImportSummaryResultDto.getWarningRowCount());
    }

  //  @Test
    public void testImportRootfoldersFromCsv_validation_MediaConnection() throws IOException {
        logger.info("testImportRootfoldersFromCsv_validation_MediaConnection");


        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import4.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto =  rootfolderSchemaLoader.doImport(true,rawData,null,false);

        Assert.assertEquals("Should have 0 errors for import validation",0,importSummaryResultDto.getErrorRowCount()); //1 connection not found
        Assert.assertEquals("Should have no duplications for import validation",0,importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have 2 warnings for import validation",2,importSummaryResultDto.getWarningRowCount());
        Assert.assertEquals("Should have accept validation for 1 rootfolders without warnings",1,importSummaryResultDto.getImportedRowCount());

    }

  //  @Test
    public void testImportRootfoldersFromCsv_MediaConnection() throws IOException {
        logger.info("testImportRootfoldersFromCsv_MediaConnection");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import4.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);

        ImportSummaryResultDto importSummaryResultDto =  rootfolderSchemaLoader.doImport(false,rawData,null,false);
        Assert.assertEquals("Should have 0 errors for import ",0,importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no duplications for import ",0,importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have 2 imports with warnings",2,importSummaryResultDto.getWarningRowCount());
        Assert.assertEquals("Should have imported 1 rootfolders without warnings",1,importSummaryResultDto.getImportedRowCount());

        RootFolderDto rootFolderDto = rootFolderService.findRootFolder("http://test/",true);
        Assert.assertEquals("Imported rootfolder should have media connection id set",sharePointConnectionDetailsDto.getId(),rootFolderDto.getMediaConnectionDetailsId());

        RootFolderDto rootFolderDtoWithMediaConnectionNotFound = rootFolderService.findRootFolder("http://test2/",true);
        Assert.assertNotNull("Imported rootfolder with stub media connection",rootFolderDtoWithMediaConnectionNotFound.getMediaConnectionDetailsId());

        MediaConnectionDetailsDto stubConnection = mediaConnectionDetailsService.getMediaConnectionDetailsById(rootFolderDtoWithMediaConnectionNotFound.getMediaConnectionDetailsId());
        Assert.assertNotNull("Imported rootfolder with stub media connection was created",stubConnection);
        Assert.assertEquals("Imported rootfolder stub media connection should be created with the right name","SharePointConnectionNotFound", stubConnection.getName());

        RootFolderDto rootFolderDtoWithNoMediaConnection = rootFolderService.findRootFolder("http://noMediaConnection/",true);
        Assert.assertNotNull("Imported rootfolder with default stub media connection",rootFolderDtoWithNoMediaConnection.getMediaConnectionDetailsId());
        MediaConnectionDetailsDto stubDefaultConnection = mediaConnectionDetailsService.getMediaConnectionDetailsById(rootFolderDtoWithNoMediaConnection.getMediaConnectionDetailsId());
        Assert.assertNotNull("Imported rootfolder with stub default media connection was created",stubDefaultConnection);
        Assert.assertEquals("Imported rootfolder with no media connection should be created with default name",
                RootfolderSchemaLoader.SHARE_POINT_STUB_DEFAULT_CONNECTION_NAME, stubDefaultConnection.getName());

    }

    @Test
    public void testImportRootfoldersFromCsv_validation_limitResults() throws IOException {
        logger.info("testImportRootfoldersFromCsv_validation_limitResults");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "rootfolders_for_import5.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);

        ImportSummaryResultDto importSummaryResultDto =  rootfolderSchemaLoader.doImport(true,rawData,5,false,true);
        Assert.assertEquals("Should have 0 errors for import ",0,importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no duplications for import ",0,importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have 16 imports with warnings",45,importSummaryResultDto.getWarningRowCount());
     //   Assert.assertEquals("Should have 5 import rows with warnings",5,importSummaryResultDto.getWarningImportRowResultDtos().size());
     //   Assert.assertEquals("Should have imported 29 rootfolders without warnings",29,importSummaryResultDto.getImportedRowCount());

    }

}
