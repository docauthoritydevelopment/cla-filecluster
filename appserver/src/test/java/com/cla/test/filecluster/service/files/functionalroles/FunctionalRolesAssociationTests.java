package com.cla.test.filecluster.service.files.functionalroles;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDetailsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleSummaryInfoDto;
import com.cla.common.domain.dto.security.RoleTemplateDto;
import com.cla.common.domain.dto.security.RoleTemplateType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.api.FunctionalRoleApiController;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.security.RoleTemplateService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Category(FastTestCategory.class)
@Ignore
public class FunctionalRolesAssociationTests  extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(FunctionalRolesAssociationTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    FunctionalRoleApiController functionalRoleApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private RoleTemplateService roleTemplateService;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    private static RoleTemplateDto templateToUse = null;

    public FunctionalRoleSummaryInfoDto createFunctionalRoleDto(String name) {
        FunctionalRoleDto functionalRoleDto = new FunctionalRoleDto();
        functionalRoleDto.setName(name);
        functionalRoleDto.setDisplayName(name);
        functionalRoleDto.setDescription(name);
        functionalRoleDto.setManagerUsername(name);
        FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto = new FunctionalRoleSummaryInfoDto();
        functionalRoleSummaryInfoDto.setFunctionalRoleDto(functionalRoleDto);
        functionalRoleDto.setTemplate(templateToUse);
        return functionalRoleApiController.createFunctionalRole(functionalRoleSummaryInfoDto);
    }

    @Before
    public void templateCreate() {
        if (templateToUse == null) {
            RoleTemplateDto template = new RoleTemplateDto();
            template.setName("name57575");
            template.setDisplayName("name57575");
            template.setDescription("name57575");
            template.setTemplateType(RoleTemplateType.NONE);
            templateToUse = roleTemplateService.createRoleTemplate(template);
        }
    }

    @Test
    public void testAssociateFileWithFunctionalRole() {
        FunctionalRoleSummaryInfoDto functionalRoleDto = createFunctionalRoleDto("testAssociateFileWithFunctionalRole");
        logger.info("testAssociateFileWithFunctionalRole");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.debug("Associate file {} with functionalRole {}",claFileDto.getId(),functionalRoleDto);
        functionalRoleApiController.associateFunctionalRoleWithFile(functionalRoleDto.getFunctionalRoleDto().getId(),claFileDto.getId());
        logger.debug("Check that file is associated with functionalRole");
        ClaFileDetailsDto fileDetailsById = filesApplicationService.findFileDetailsById(claFileDto.getId());
        ClaFileDto claFileDto2 = fileDetailsById.getFileDto();
//        ClaFileDto claFileDto2 = claFileDtos.getContent().get(0);
        logger.debug("Verify that file {} has association",claFileDto2.getId());
        Assert.assertThat(claFileDto2.getAssociatedFunctionalRoles().size(), Matchers.greaterThan(0));
        FunctionalRoleDto functionalRoleDto2 = claFileDto2.getAssociatedFunctionalRoles()
                .stream().findFirst().orElseGet(null);
        Assert.assertNotNull(functionalRoleDto2.getManagerUsername());
        Assert.assertNotNull(functionalRoleDto2.getName());

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("functionalRoleId", functionalRoleDto.getFunctionalRoleDto().getId(),"eq"));
        claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("Check functionalRoleId filter solr files",claFileDtos.getNumberOfElements(), Matchers.greaterThan(0));

        ClaFileDto claFileDto1 = claFileDtos.getContent().get(0);
        Assert.assertThat(claFileDto1.getAssociatedFunctionalRoles().size(), Matchers.greaterThan(0));
        FunctionalRoleDto functionalRoleDto3 = claFileDto1.getAssociatedFunctionalRoles().stream()
                .findFirst().orElseGet(null);
        Assert.assertNotNull("Check functionalRoleId filter solr files has association", functionalRoleDto3.getManagerUsername());
        Assert.assertNotNull(functionalRoleDto3.getName());
    }

    @Test
    public void testRemoveFunctionalRoleFileAssociation() {
        FunctionalRoleSummaryInfoDto functionalRoleDto = createFunctionalRoleDto("testRemoveFunctionalRoleFileAssociation");
        logger.info("testRemoveFunctionalRoleFileAssociation");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.debug("Associate file {} with functionalRole {}",claFileDto.getId(),functionalRoleDto.getFunctionalRoleDto());
        functionalRoleApiController.associateFunctionalRoleWithFile(functionalRoleDto.getFunctionalRoleDto().getId(),claFileDto.getId());
        functionalRoleApiController.removeFunctionalRoleAssociationFromFile(functionalRoleDto.getFunctionalRoleDto().getId(),claFileDto.getId());

        logger.debug("Check that file is not associated with functionalRole");
        claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto2 = claFileDtos.getContent().get(0);
        logger.debug("Verify that file {} doesn't have functionalRole association",claFileDto2);
        long count = claFileDto2.getAssociatedFunctionalRoles().stream()
                .filter(f -> f.getId().equals(functionalRoleDto.getFunctionalRoleDto().getId()))
                .count();
        Assert.assertEquals("File is still associated with functionalRole",0,count);


    }

    @Test
    public void testAssociateFolderWithFunctionalRole() {
        FunctionalRoleSummaryInfoDto functionalRoleDto = createFunctionalRoleDto("testAssociateFolderWithFunctionalRole");
        logger.info("testAssociateFolderWithFunctionalRole");
        Page<DocFolderDto> docFolderDtos = fileTreeAppService.listAllFoldersInDefaultDocStore(DataSourceRequest.build(null));
        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.info("Associate functionalRole with folder {}",docFolderDto);
        functionalRoleApiController.associateFunctionalRoleWithDocFolder(functionalRoleDto.getFunctionalRoleDto().getId(),docFolderDto.getId());

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("functionalRoleId",functionalRoleDto.getFunctionalRoleDto().getId(),"eq"));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("Verify at least one file returned from functionalRole filter",claFileDtos.getNumberOfElements(),Matchers.greaterThan(0));

        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        FunctionalRoleDto functionalRoleDto1 = claFileDto.getAssociatedFunctionalRoles().stream()
                .findFirst().orElseGet(null);
        Assert.assertThat("Verify name is not empty", functionalRoleDto1.getName(), Matchers.not(Matchers.isEmptyOrNullString()));
        Assert.assertThat("Verify ManagerUsername is not empty", functionalRoleDto1.getManagerUsername(), Matchers.notNullValue());
        logger.debug("There should be at least one file with implicit association");
        long count = claFileDtos.getContent().stream().filter(f -> f.getAssociatedFunctionalRoles().stream()
                .filter(FunctionalRoleDto::getImplicit).count()>0).count();
        Assert.assertThat("At least one implicit func role for file", count,Matchers.greaterThan(0L));

    }

    @Test
    public void testAssociateGroupWithFunctionalRole() {
        FunctionalRoleSummaryInfoDto functionalRoleDto = createFunctionalRoleDto("testAssociateGroupWithFunctionalRole");
        logger.info("testAssociateGroupWithFunctionalRole");
        FacetPage<AggregationCountItemDTO<GroupDto>> groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        GroupDto firstGroup = groups.getContent().get(0).getItem();
        logger.debug("Associate group {} with FunctionalRole {}",firstGroup.getId(),functionalRoleDto);

        functionalRoleApiController.associateFunctionalRoleWithGroup(functionalRoleDto.getFunctionalRoleDto().getId(),firstGroup.getId());
        groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        GroupDto firstGroup2 = groups.getContent().get(0).getItem();
        logger.debug("Check that group {} is associated with FunctionalRole",firstGroup2.getId());
        Assert.assertThat("Check that group has at least 1 associaction",firstGroup2.getAssociatedFunctionalRoles().size(),
                Matchers.greaterThan(0));
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("groupId",firstGroup.getId(),"eq"));
        logger.debug("Check that files of group {} are associated with FunctionalRole",firstGroup2.getId());
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("Check that group has at least 1 file in response",claFileDtos.getNumberOfElements(),Matchers.greaterThan(0));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        Assert.assertThat("Check that file has associations", claFileDto.getAssociatedFunctionalRoles().size(),
                Matchers.greaterThan(0));

    }
    @Test
    public void testRemoveGroupAssociation() {
        logger.info("testRemoveGroupAssociation");
        FunctionalRoleSummaryInfoDto functionalRoleDto = createFunctionalRoleDto("testRemoveGroupAssociation");
        FacetPage<AggregationCountItemDTO<GroupDto>> groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        GroupDto firstGroup = groups.getContent().get(0).getItem();
        logger.debug("Associate group {} with FunctionalRole {}",firstGroup.getId(),functionalRoleDto);
        functionalRoleApiController.associateFunctionalRoleWithGroup(functionalRoleDto.getFunctionalRoleDto().getId(),firstGroup.getId());

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("functionalRoleId",functionalRoleDto.getFunctionalRoleDto().getId(),"eq"));

        logger.debug("Remove bizListItem from group");
        functionalRoleApiController.removeFunctionalRoleAssociationFromGroup(functionalRoleDto.getFunctionalRoleDto().getId(),firstGroup.getId());

        groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        firstGroup = groups.getContent().get(0).getItem();

        long count = firstGroup.getAssociatedFunctionalRoles().stream()
                .filter(f -> f.getId().equals(functionalRoleDto.getFunctionalRoleDto().getId())).count();
        Assert.assertEquals("No func role for group after removal", count,0);


    }
}
