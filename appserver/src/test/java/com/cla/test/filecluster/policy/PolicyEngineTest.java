package com.cla.test.filecluster.policy;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.messages.action.ActionPluginConfig;
import com.cla.common.domain.dto.messages.action.PluginInput;
import com.cla.common.predicate.CompareOperation;
import com.cla.common.predicate.KeyValuePredicate;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.entity.alert.Alert;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.policy.factsenricher.FactsEnricher;
import com.cla.filecluster.policy.plugin.transformer.AlertTransformer;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.cla.test.filecluster.policy.input.ActionPluginsDefinitionBuilder;
import com.cla.test.filecluster.policy.input.PolicyInputBuilder;
import com.cla.test.filecluster.policy.output.PolicyEngineOutputVerifier;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Test all possible flows for a policy
 */
@Category(UnitTestCategory.class)
public class PolicyEngineTest {

    @Test
    public void testFactsEnricher() throws InterruptedException {
        PolicyEngineOutputVerifier.newVerifier()
                .withFactsEnrichers(
                    Lists.newArrayList(new FactsEnricher() {
                        @Override
                        public void enrich(DAEvent daEvent, Map factsMap) {
                            factsMap.put("key1", "value1");
                        }
                        @Override
                        public boolean validateEvent(DAEvent daEvent) {
                            return true;
                        }
                    }))
                .withPoliciesInput(Lists.newArrayList())
                .expectedFact("key1", "value1")
                .verifyOn(Lists.newArrayList(
                        EventType.SCAN_DIFF_UPDATED
                ));
    }

    @Test
    public void testActivatedPoliciesByRelevantEventTypes() throws InterruptedException {
        List<PolicyConfig> policies = Lists.newArrayList(
                PolicyInputBuilder.newBuilder()
                        .withName("p1")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_UPDATED))
                        .withPredicates(Lists.newArrayList())
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p2")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_DELETED, EventType.SCAN_DIFF_UPDATED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p3")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_DELETED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p4")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_UNDELETED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate()))
                        .build());


        PolicyEngineOutputVerifier.newVerifier()
                .withPoliciesInput(policies)
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_UPDATED, "p1", "p2")
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_DELETED, "p2", "p3")
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_ACL_UPDATED)
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_UNDELETED, "p4")
                .verifyOn(Lists.newArrayList(
                        EventType.SCAN_DIFF_UPDATED,
                        EventType.SCAN_DIFF_UNDELETED,
                        EventType.SCAN_DIFF_DELETED,
                        EventType.SCAN_DIFF_ACL_UPDATED));
    }


    @Test
    public void testPredicates() throws InterruptedException {
        List<PolicyConfig> policies = Lists.newArrayList(
                PolicyInputBuilder.newBuilder()
                        .withName("p1")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_UPDATED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p2")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_DELETED, EventType.SCAN_DIFF_UPDATED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate(), new AllFalsePredicate()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p3")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_UNDELETED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate(), new AllTruePredicate()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p4")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_ACL_UPDATED))
                        .withPredicates(Lists.newArrayList(new AllFalsePredicate(), new AllFalsePredicate()))
                        .build());

        PolicyEngineOutputVerifier.newVerifier()
                .withPoliciesInput(policies)
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_UPDATED, "p1")
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_DELETED)
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_UNDELETED, "p3")
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_ACL_UPDATED)
                .verifyOn(Lists.newArrayList(
                        EventType.SCAN_DIFF_UPDATED,
                        EventType.SCAN_DIFF_DELETED,
                        EventType.SCAN_DIFF_UNDELETED));
    }


    @Test
    public void testCorrectPluginsActivated() throws InterruptedException {
        List<PolicyConfig> policies = Lists.newArrayList(
                PolicyInputBuilder.newBuilder()
                        .withName("p1")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_UPDATED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate()))
                        .withActionPluginsDefinitions(Lists.newArrayList(
                                ActionPluginsDefinitionBuilder.newBuilder()
                                        .withActionPluginConfigs(Lists.newArrayList(new ActionPluginConfig("config1")))
                                        .withPluginInputTransformer(daEvent -> new DummyInput1())
                                        .build()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p2")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_UNDELETED))
                        .withPredicates(Lists.newArrayList(new AllTruePredicate()))
                        .withActionPluginsDefinitions(Lists.newArrayList(
                                ActionPluginsDefinitionBuilder.newBuilder()
                                        .withActionPluginConfigs(Lists.newArrayList(
                                                new ActionPluginConfig("config2"),
                                                new ActionPluginConfig("config3")))
                                        .withPluginInputTransformer(daEvent -> new DummyInput2())
                                        .build()))
                        .build(),
                PolicyInputBuilder.newBuilder()
                        .withName("p3")
                        .withEventTypes(Lists.newArrayList(EventType.SCAN_DIFF_ACL_UPDATED))
                        .withPredicates(Lists.newArrayList(new AllFalsePredicate()))
                        .withActionPluginsDefinitions(Lists.newArrayList(
                                ActionPluginsDefinitionBuilder.newBuilder()
                                        .withActionPluginConfigs(Lists.newArrayList(
                                                new ActionPluginConfig("config4"),
                                                new ActionPluginConfig("config5")))
                                        .withPluginInputTransformer(daEvent -> new DummyInput2())
                                        .build()))
                        .build());

        PolicyEngineOutputVerifier.newVerifier()
                .withPoliciesInput(policies)
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_UPDATED, "p1")
                .expectedPoliciesActivatedForEvent(EventType.SCAN_DIFF_UNDELETED, "p2")
                .expectedActionPluginRequests(Lists.newArrayList(
                        Pair.of("config1", DummyInput1.class),
                        Pair.of("config2", DummyInput2.class),
                        Pair.of("config3", DummyInput2.class)))
                .verifyOn(Lists.newArrayList(
                        EventType.SCAN_DIFF_UPDATED,
                        EventType.SCAN_DIFF_DELETED,
                        EventType.SCAN_DIFF_UNDELETED));
    }

    @Test
    public void testAlertFlow() {
        List<PolicyConfig> policies = Lists.newArrayList(
                PolicyInputBuilder.newBuilder()
                        .withName("Alert Policy")
                        .withEventTypes(Lists.newArrayList(EventType.values()))
                        .withPredicates(Lists.newArrayList(new KeyValuePredicate("tags", CompareOperation.CONTAINS, Lists.newArrayList("5.67.MANUAL"))))
                        .withActionPluginsDefinitions(Lists.newArrayList(
                                ActionPluginsDefinitionBuilder.newBuilder()
                                        .withActionPluginConfigs(Lists.newArrayList(new ActionPluginConfig("alertDBActionPlugin")))
                                        .withPluginInputTransformer(new AlertTransformer(
                                                ImmutableMap.of(
                                                        EventType.SCAN_DIFF_UPDATED, AlertSeverity.CRITICAL,
                                                        EventType.SCAN_DIFF_ACL_UPDATED, AlertSeverity.MEDIUM,
                                                        EventType.SCAN_DIFF_DELETED, AlertSeverity.HIGH,
                                                        EventType.SCAN_DIFF_UNDELETED, AlertSeverity.LOW
                                                )))
                                        .build()))
                        .build());

        PolicyEngineOutputVerifier policyEngineOutputVerifier = PolicyEngineOutputVerifier.newVerifier()
                .withFactsEnrichers(Lists.newArrayList(new FactsEnricher() {
                    @Override
                    public void enrich(DAEvent daEvent, Map factsMap) {
                        factsMap.put("tags", Lists.newArrayList("5.67.MANUAL"));
                    }
                    @Override
                    public boolean validateEvent(DAEvent daEvent) {
                        return true;
                    }
                }))
                .withPoliciesInput(policies)
                .expectedFact("tags", Lists.newArrayList("5.67.MANUAL"));

        Arrays.stream(EventType.values()).forEach(eventType -> {
            policyEngineOutputVerifier.expectedPoliciesActivatedForEvent(eventType, "Alert Policy");
            policyEngineOutputVerifier.expectedActionPluginRequests(Lists.newArrayList(
                    Pair.of("alertDBActionPlugin", Alert.class)));
        });


        policyEngineOutputVerifier.verifyOn(Lists.newArrayList(EventType.values()));
    }


    private static class DummyInput1 implements PluginInput {

    }

    private static class DummyInput2 implements PluginInput {

    }

    private static class AllTruePredicate<V> extends KeyValuePredicate {

        public AllTruePredicate() {
            super(null, null, null);
        }

        @Override
        public boolean accept(Map<String, Object> data) {
            return true;
        }


    }

    private static class AllFalsePredicate<V> extends KeyValuePredicate {

        public AllFalsePredicate() {
            super(null, null, null);
        }

        @Override
        public boolean accept(Map<String, Object> data) {
            return false;
        }

    }
}
