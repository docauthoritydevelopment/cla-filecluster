package com.cla.test.filecluster.service.files.doctype;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.doctype.DocTypeDetailsDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.api.*;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Created by uri on 15/12/2015.
 */

@Category(FastTestCategory.class)
public class DocTypeAppServiceTests extends ClaBaseTests {

    @Autowired
    private DocTypeAppService docTypeAppService;

    @Autowired
    private DocTypeApiController docTypeApiController;


    private static final Logger logger = LoggerFactory.getLogger(DocTypeAppServiceTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FileTagApiController fileTagApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    private RootFolderDto rootFolderDto;


    @Before
    public void scanBaseDir() {
        rootFolderDto = rootFolderDtoAnalyze;
    }

    @Test
    public void testAddDocTypeToFile() {
        testStartLog(logger);

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);
        String docTypeName = "Salary Compensation Plans";
        DocTypeDto docTypeDto = docTypeAppService.getDocTypeByName(docTypeName);
        Assert.assertNotNull("Could not find docType with name: " + docTypeName, docTypeDto);
        logger.debug("Assign docType {} to file {}", docTypeDto, claFileDto);
        docTypeAppService.assignDocTypeToFile(docTypeDto.getId(), claFileDto.getId(), null);
        logger.debug("Check that docType was added to MySQL");
        filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        long fileId = claFileDto.getId();
        claFileDto = performanceTestService.findFileDto(filesDeNormalized, fileId);
        logger.debug("Verify that file {} has the docType", claFileDto);
        Set<DocTypeDto> assignedDocTypes = claFileDto.getDocTypeDtos();
        Assert.assertThat("There should be at least one docType (mySQL)", assignedDocTypes.size(), Matchers.greaterThan(0));

        logger.debug("Check that docType was added to Solr");
        boolean docTypeInSolr = docTypeAppService.isDocTypeFileInSolr(claFileDto.getId(), docTypeDto.getId(), null);
        Assert.assertTrue("DocType should be in Solr", docTypeInSolr);
        docTypeAppService.removeDocTypeFromFile(docTypeDto.getId(), claFileDto.getId(), null);
    }

    @Test
    public void testAddDocTypeToGroup() {
        testStartLog(logger);
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        GroupDto groupDto = groupDtos.getContent().get(0);
        if (groupDto.getNumOfFiles() == 0) {
            throw new RuntimeException("Got a group with 0 files. This is a bug");
        }
        DocTypeDto docTypeDto = docTypeApiController.addDocType(new DocTypeDto("testAddDocTypeToGroup"));
        logger.debug("Assign docType {} to group {} with {} files", docTypeDto, groupDto.getGroupName(), groupDto.getNumOfFiles());

        docTypeAppService.assignDocTypeToGroup(groupDto.getId(), docTypeDto.getId(), null);
        //Now Verify
        Page<ClaFileDto> claFileDtos = filesApiController.ListFilesByGroup(null, groupDto.getId());
        if (claFileDtos.getContent().size() == 0) {
            throw new RuntimeException("Got 0 files from the group. This is a bug");
        }
        logger.debug("Verify that {} files have assigned docType", claFileDtos.getContent().size());
        claFileDtos.forEach(claFileDto -> verifyDocType(docTypeDto, claFileDto));

        //Apply to solr and verify
        scheduledOperationService.runOperations();

        logger.debug("Get DocType count");
        Map<String,String> params = new HashMap<>();
        params.put(DataSourceRequest.TAKE,"31");
        FacetPage<AggregationCountItemDTO<DocTypeDto>> docTypes = docTypeApiController.findDocTypesFileCount(params);
        logger.debug("Get DocType count - Got {} docTypes ", docTypes.getContent().size());
        Assert.assertThat("There should be docTypes in the count response", docTypes.getContent().size(), Matchers.greaterThan(0));
        logger.debug("At least one docType should have count >0 and count2 > 0");
        for (AggregationCountItemDTO<DocTypeDto> docType : docTypes.getContent()) {
            if (docType.getCount() > 0 && docType.getSubtreeCount() > 0) {
                docTypeAppService.removeDocTypeFromGroup(groupDto.getId(), docTypeDto.getId(), null);
                return;
            }
            logger.debug("Get DocType {} with count1={} and count2={}", docType.getItem().getName(), docType.getCount(), docType.getCount2());
        }
        Assert.fail("There are some counters with value = 0 for at least one docType results.");
    }

    @Test
    public void testUserAssignsDocTypeToFolder1() {
        testStartLog(logger);
        DocTypeDto docTypeDto = docTypeApiController.addDocType(new DocTypeDto("testUserAssignsDocTypeToFolder1"));

        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto docFolderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());
//        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.debug("Assign doc type {} to docFolder {}", docTypeDto.toString(), docFolderDto);
        docTypeApiController.assignDocTypeToFolder(docFolderDto.getId(), docTypeDto.getId());
        scheduledOperationService.runOperations();
        logger.debug("Verify files in root folder");
        Page<ClaFileDto> claFileDtos = fileTreeApiController.listFilesInFolder(docFolderDto.getId(), null);
        claFileDtos.forEach(claFileDto -> verifyDocType(docTypeDto, claFileDto));

        logger.debug("Verify files in sub folders");
        Page<DocFolderDto> subFolders = fileTreeApiController.listFoldersFromSubFolder(docFolderDto.getId(), null);
        Page<ClaFileDto> claFileDtos2 = fileTreeApiController.listFilesInFolder(subFolders.getContent().get(0).getId(), null);
        claFileDtos2.forEach(claFileDto -> verifyDocType(docTypeDto, claFileDto));

        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();
        //
        logger.debug("Verify that files have docType in Solr");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Assert.assertNotNull(docFolderDto);
        String fieldName = FileDtoFieldConverter.DtoFields.docTypeId.name();
        FilterDescriptor filter = performanceTestService.createConcreteFilter(fieldName, String.valueOf(docTypeDto.getId()), "eq");
        dataSourceRequest.setFilter(filter);
        PageImpl<ClaFileDto> claFileDtos1 = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.info("There are {} files with the tag", claFileDtos1.getContent().size());
        Assert.assertThat("There should be at least one file with the required docType", claFileDtos.getNumberOfElements(), Matchers.greaterThan(0));
        docTypeApiController.removeDocTypeFromFolder(docFolderDto.getId(), docTypeDto.getId());
    }

    @Test
    public void testUserAssignsDocTypeToFolder2() {
        testStartLog(logger);
        DocTypeDto docTypeDto = docTypeApiController.addDocType(new DocTypeDto("testUserAssignsDocTypeToFolder2"));

        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto docFolderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());
//        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.debug("Assign doc type {} to docFolder {}", docTypeDto.toString(), docFolderDto.getId());
        docTypeApiController.assignDocTypeToFolder(docFolderDto.getId(), docTypeDto.getId());

        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();

        logger.info("Verify that files have implicit docType {} in Solr", docTypeDto.getId());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setPageSize(1000);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        for (ClaFileDto claFileDto : claFileDtos) {
            for (DocTypeDto typeDto : claFileDto.getDocTypeDtos()) {
                if (typeDto.getId().equals(docTypeDto.getId()) && typeDto.isImplicit()) {
                    logger.info("Found a file with the required docType in implicit mode");
                    docTypeApiController.removeDocTypeFromFolder(docFolderDto.getId(), docTypeDto.getId());
                    return;
                }
            }
        }
        throw new RuntimeException("Couldn't find the implicit docType");
    }

    @Test
    public void testUserAssignsDocTypeToFolder3() {
        testStartLog(logger);
        DocTypeDto docTypeDto = docTypeApiController.addDocType(new DocTypeDto("testUserAssignsDocTypeToFolder3"));

        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto subFolderDto = docFolderDtos.getContent().get(1);

        logger.debug("Assign doc type {} to docFolder {}", docTypeDto.toString(), subFolderDto.getPath() + File.separator + subFolderDto.getName());
        docTypeApiController.assignDocTypeToFolder(subFolderDto.getId(), docTypeDto.getId());

        DocFolderDto rootFolderDto = docFolderDtos.getContent().get(0);

        logger.debug("Assign doc type {} to docFolder {}", docTypeDto.toString(), rootFolderDto.getPath() + File.separator + rootFolderDto.getName());
        docTypeApiController.assignDocTypeToFolder(rootFolderDto.getId(), docTypeDto.getId());

        logger.info("Verify that folders have explicit docType {} in Solr", docTypeDto.getId());

        docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);

        DocFolderDto rootFolderDto2 = docFolderDtos.getContent().get(0);
        logger.debug("verify doc type {} is explicitly assigned to docFolder {}", docTypeDto.toString(), rootFolderDto2.getPath() + File.separator + rootFolderDto2.getName());
        verifyDocType(docTypeDto, rootFolderDto2.getDocTypeDtos(), false);


        DocFolderDto subFolderDto2 = docFolderDtos.getContent().get(1);
        logger.debug("verify doc type {} is explicitly assigned to docFolder {}", docTypeDto.toString(), subFolderDto2.getPath() + File.separator + subFolderDto2.getName());
        verifyDocType(docTypeDto, subFolderDto2.getDocTypeDtos(), false);

        docTypeApiController.removeDocTypeFromFolder(rootFolderDto.getId(), docTypeDto.getId());
        docTypeApiController.removeDocTypeFromFolder(subFolderDto.getId(), docTypeDto.getId());
    }

    private void verifyDocType(DocTypeDto docTypeDto, Set<DocTypeDto> docTypeDtos, boolean implicit) {
        for (DocTypeDto typeDto : docTypeDtos) {
            if (typeDto.isImplicit() == implicit && Objects.equals(typeDto.getId(), docTypeDto.getId())) {
                return;
            }
        }
        throw new AssertionError("Failed to find docType " + docTypeDto + " (implicit: " + implicit + ") in docTypeList (" + docTypeDtos.size() + ")");
    }

    private void verifyDocType(DocTypeDto docTypeDto, ClaFileDto claFileDto) {
        for (DocTypeDto typeDto : claFileDto.getDocTypeDtos()) {
            if (docTypeDto.getId().equals(typeDto.getId())) {
                return;
            }
        }
        logger.error("file " + claFileDto.getId() + " doesn't have the required docType " + docTypeDto);
        for (DocTypeDto typeDto : claFileDto.getDocTypeDtos()) {
            logger.debug("docType: " + typeDto);
        }
        throw new RuntimeException("file " + claFileDto + " doesn't have the required docType " + docTypeDto);
    }

    @Test
    public void testRemoveDocTypeFromFile() {
        testStartLog(logger);
        DocTypeDto docTypeDto = docTypeApiController.addDocType(new DocTypeDto("testRemoveDocTypeFromFile"));

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);
        logger.debug("Assign docType {} to file {}", docTypeDto, claFileDto);
        docTypeAppService.assignDocTypeToFile(docTypeDto.getId(), claFileDto.getId(), null);

        logger.debug("Remove docType {} from file {}", docTypeDto, claFileDto);
        docTypeAppService.removeDocTypeFromFile(docTypeDto.getId(), claFileDto.getId(), null);

        logger.debug("Check that docType is not in MySQL");
        filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        long fileId = claFileDto.getId();
        claFileDto = performanceTestService.findFileDto(filesDeNormalized, fileId);
        logger.debug("Verify that file {} doesn't have the docType", claFileDto);
        Set<DocTypeDto> assignedDocTypes = claFileDto.getDocTypeDtos();
        for (DocTypeDto assignedDocType : assignedDocTypes) {
            if (assignedDocType.getId().equals(docTypeDto.getId())) {
                throw new RuntimeException("Found the docType that should have been removed");
            }
        }
        logger.debug("Check that docType is not in Solr");
        boolean docTypeInSolr = docTypeAppService.isDocTypeFileInSolr(claFileDto.getId(), docTypeDto.getId(), null);
        Assert.assertFalse("DocType should not be in Solr", docTypeInSolr);
    }

    @Test
    public void testFilterGroupByDocType() {
        testStartLog(logger);
        DocTypeDto docTypeDto = docTypeApiController.addDocType(new DocTypeDto("testFilterGroupByDocType"));
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        GroupDto groupDto = groupDtos.getContent().get(0);
        logger.debug("Assign docType {} to group {}", docTypeDto, groupDto);

        docTypeAppService.assignDocTypeToGroup(groupDto.getId(), docTypeDto.getId(), null);

        //Apply to solr and verify
        scheduledOperationService.runOperations();

        //Now Verify
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        String value = String.valueOf(docTypeDto.getId());
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("subDocTypeId", value, "eq"));
        FacetPage<AggregationCountItemDTO<GroupDto>> result = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
        logger.info("testFilterGroupByDocType - Verify results contain files");

        Assert.assertThat("There should be files in the response", result.getContent().size(), Matchers.greaterThan(0));
        docTypeAppService.removeDocTypeFromGroup(groupDto.getId(), docTypeDto.getId(), null);
    }

    @Test
    public void testMoveDocType() {
        testStartLog(logger);

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);

        Page<DocTypeDto> allDocTypes = docTypeApiController.getAllDocTypes(null);
        DocTypeDto parentDocType = allDocTypes.getContent().get(0);

        DocTypeDto newDocType = new DocTypeDto("testFilterGroupByDocType1");
        DocTypeDto docTypeDto = docTypeApiController.addDocType(newDocType);

        logger.debug("Assign docType {} to file {}", docTypeDto, claFileDto);
        docTypeAppService.assignDocTypeToFile(docTypeDto.getId(), claFileDto.getId(), null);

        docTypeApiController.moveDocType(docTypeDto.getId(), parentDocType.getId());

        fileTagApiController.applyTagsToSolr(true, true, 10000);
        scheduledOperationService.runOperations();
        Map<String, String> params = new HashMap<>();
        params.put("baseFilterField", "docTypeId");
        params.put("baseFilterValue", String.valueOf(docTypeDto.getId()));
        filesDeNormalized = filesApiController.findFilesDeNormalized(params);

        Assert.assertNotNull(filesDeNormalized.getContent());
        Assert.assertTrue(filesDeNormalized.getContent().size() > 0);

        ClaFileDto claFileDto2 = filesDeNormalized.getContent().get(0);
        logger.debug("Verify file has docType");
        for (DocTypeDto typeDto : claFileDto2.getDocTypeDtos()) {
            if (Objects.equals(typeDto.getId(), docTypeDto.getId())) {
                logger.debug("Found required docType");
                docTypeAppService.removeDocTypeFromFile(docTypeDto.getId(), claFileDto.getId(), null);
                return;
            }
        }
        throw new RuntimeException("Failed to find the docType");

    }

    @Test
    public void testDeleteDocType() {
        testStartLog(logger);
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);

        DocTypeDto newDocType = new DocTypeDto("testFilterGroupByDocType");
        DocTypeDto docTypeDto = docTypeApiController.addDocType(newDocType);

        docTypeAppService.assignDocTypeToFile(docTypeDto.getId(), claFileDto.getId(), null);

        docTypeApiController.removeAllDocTypeAssociations(docTypeDto.getId(), true);
        fileTagApiController.applyTagsToSolr(true, true, 10000);
        scheduledOperationService.runOperations();
        docTypeApiController.deleteDocType(docTypeDto.getId(), true);
    }

    @Test
    public void testDocTypeDetails() {
        testStartLog(logger);
        DocTypeDto newDocType = new DocTypeDto("testFilterGroupByDocType2");
        DocTypeDto docTypeDto = docTypeApiController.addDocType(newDocType);

        DocTypeDetailsDto docTypeDetails = docTypeApiController.getDocTypeDetails(docTypeDto.getId(), false);
        Assert.assertEquals(docTypeDto.getId(), docTypeDetails.getDocTypeDto().getId());
        DocTypeDetailsDto servicesDocType = docTypeApiController.getDocTypeDetailsByName("Legal", true);
    }


}
