package com.cla.test.filecluster.service.sharepoint;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.media.SharePointApiController;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

public abstract class TestEventBasedRescan extends ClaBaseTests {

    protected final static Logger logger = LoggerFactory.getLogger(TestEventBasedRescan.class);

    @Autowired
    protected PerformanceTestService performanceTestService;

    @Autowired
    protected MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    protected SharePointApiController sharePointApiController;

    @Autowired
    protected PlatformTransactionManager transactionManager;

    @Autowired
    protected SolrFileCatRepository solrFileCatRepository;

    @Autowired
    protected FileCatService fileCatService;

    @Autowired
    protected DocStoreService docStoreService;

    @Autowired
    protected JobManagerApiController jobManagerApiController;

    @Autowired
    protected FilesApiController filesApiController;

    @Autowired
    protected FileTreeApiController fileTreeApiController;

    protected SharePointConnectionDetailsDto sharePointConnectionDetails;

    protected void updateClaFileProperties(ClaFileDto claFileDto) {
        doInTransaction(() -> innerUpdateClaFileProperties(claFileDto));
        //TODO: apply those changes to SOLR, this code used to mark the file as dirty
    }

    protected RootFolderDto updateRootFolderNickName(RootFolderDto rootFolderDto, String newNickName) {
        rootFolderDto.setNickName(newNickName);
        return fileTreeApiController.updateRootFolder(rootFolderDto.getId(),rootFolderDto);
    }

    protected abstract String getFolderNickName();

    protected RootFolderDto configureRootFolder(String path) {
        logger.info("Configure RootFolder");

        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setCrawlerType(CrawlerType.MEDIA_PROCESSOR);
        rootFolderDto.setPath(path);
        rootFolderDto.setNickName(getFolderNickName());
        rootFolderDto.setMediaType(MediaType.SHARE_POINT);
        rootFolderDto.setMediaConnectionDetailsId(sharePointConnectionDetails.getId());
        rootFolderDto.setScannedFilesCountCap(200);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, null);
        logger.debug("Configured rootFolder {}",rootFolder);
        return rootFolder;

    }

    private void innerUpdateClaFileProperties(ClaFileDto claFileDto) {

        SolrFileEntity entity = fileCatService.getFileEntity(claFileDto.getId());

        solrFileCatRepository.atomicUpdateSingleField(
                entity.getId(), CatFileFieldType.BASE_NAME, SolrOperator.SET, claFileDto.getBaseName()
        );

        ClaFile claFile = fileCatService.getFromEntity(entity);
        claFile.setBaseName(claFileDto.getBaseName());
        claFile.recalculateHash();

        solrFileCatRepository.atomicUpdateSingleField(
                entity.getId(), CatFileFieldType.FILE_NAME_HASHED, SolrOperator.SET, claFile.getFileNameHashed()
        );
        solrFileCatRepository.softCommitNoWaitFlush();
    }

    /**
     * Execute runnable in separate transaction
     * @param runnable runnable to execute
     */
    private void doInTransaction(Runnable runnable){
        TransactionTemplate template = new TransactionTemplate(transactionManager);

        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    runnable.run();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

}
