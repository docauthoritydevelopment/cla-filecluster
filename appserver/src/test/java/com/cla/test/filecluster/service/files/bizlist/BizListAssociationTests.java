package com.cla.test.filecluster.service.files.bizlist;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.BizListDto;
import com.cla.common.domain.dto.bizlist.BizListItemState;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.file.ClaFileDetailsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.api.BizListApiController;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.BizListTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 *
 * Created by uri on 20/01/2016.
 */

@Category(BizListTestCategory.class)
public class BizListAssociationTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(BizListAssociationTests.class);

    @Autowired
    private BizListApiController bizListApiController;

    @Autowired
    private BizListAppService bizListAppService;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    private BizListDto bizList;

    @Before
    public void setUp() {
        bizList = bizListAppService.getBizListByName("BizListAssociationTests");
        if (bizList == null) {
            bizList = bizListApiController.createBizList(new BizListDto("BizListAssociationTests", BizListItemType.CLIENTS));
        }
    }

    @Test
    public void testAssociateFileWithBizListItem() {
        SimpleBizListItemDto bizListItem = createBizListItem(bizList,"Electric Power Supply Association","EPSA");
        logger.info("testAssociateFileWithBizListItem");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.debug("Associate file {} with bizListItem {}",claFileDto.getId(),bizListItem);
        bizListApiController.associateBizListItemWithFile(bizList.getId(),bizListItem.getId(),claFileDto.getId());
        logger.debug("Check that file is associated with bizList item");
        ClaFileDetailsDto fileDetailsById = filesApplicationService.findFileDetailsById(claFileDto.getId());
        ClaFileDto claFileDto2 = fileDetailsById.getFileDto();
//        ClaFileDto claFileDto2 = claFileDtos.getContent().get(0);
        logger.debug("Verify that file {} has association",claFileDto2.getId());
        Assert.assertThat(claFileDto2.getAssociatedBizListItems().size(), Matchers.greaterThan(0));
        SimpleBizListItemDto simpleBizListItemDto2 = claFileDto2.getAssociatedBizListItems().get(0);
        Assert.assertNotNull(simpleBizListItemDto2.getState());
        Assert.assertNotNull(simpleBizListItemDto2.getName());

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("bizListItemAssociationId", bizListItem.getId(),"eq"));
        claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat(claFileDtos.getNumberOfElements(), Matchers.greaterThan(0));

        ClaFileDto claFileDto1 = claFileDtos.getContent().get(0);
        Assert.assertThat(claFileDto1.getAssociatedBizListItems().size(), Matchers.greaterThan(0));
        SimpleBizListItemDto simpleBizListItemDto = claFileDto1.getAssociatedBizListItems().get(0);
        Assert.assertNotNull(simpleBizListItemDto.getState());
        Assert.assertNotNull(simpleBizListItemDto.getName());
    }

    @Test
    public void testRemoveFileAssociation() {
        SimpleBizListItemDto bizListItemDto = createBizListItem(bizList,"IBM","IBM");
        logger.info("testRemoveFileAssociation");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.debug("Associate file {} with bizListItem {}",claFileDto.getId(),bizListItemDto);
        bizListApiController.associateBizListItemWithFile(bizList.getId(),bizListItemDto.getId(),claFileDto.getId());
        bizListApiController.removeBizListItemAssociationFromFile(bizList.getId(),bizListItemDto.getId(),claFileDto.getId());
        fileCatService.commitToSolr();
        logger.debug("Check that file is not associated with bizList item");
        claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto2 = claFileDtos.getContent().get(0);
        logger.debug("Verify that file {} doesn't have association",claFileDto2);
        for (SimpleBizListItemDto simpleBizListItemDto : claFileDto2.getAssociatedBizListItems()) {
            if (simpleBizListItemDto.getId().equals(bizListItemDto.getId())) {
                throw new RuntimeException("File is still associated with bizListItem "+simpleBizListItemDto);
            }
        }

    }

    @Test @Ignore
    public void testAssociateFolderWithBizListItem() {
        logger.info("testAssociateFolderWithBizListItem");
        SimpleBizListItemDto bizListItem = createBizListItem(bizList,"TAF","TAF");
        Page<DocFolderDto> docFolderDtos = fileTreeAppService.listAllFoldersInDefaultDocStore(DataSourceRequest.build(null));
        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.info("Associate bizListItem with folder {}",docFolderDto);
        bizListApiController.associateBizListItemWithDocFolder(bizList.getId(),bizListItem.getId(),docFolderDto.getId());
        scheduledOperationService.runOperations();

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("bizListItemAssociationId",bizListItem.getId(),"eq"));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("Verify at least one file returned",claFileDtos.getNumberOfElements(),Matchers.greaterThan(0));

        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        int counter = 0;
        while (claFileDto.getAssociatedBizListItems().size() == 0 && counter < 5){
            counter++;
            try {
                logger.debug("Test synchronization problem, sleeping for 500ms. for {} time.", counter);
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
            claFileDto = claFileDtos.getContent().get(0);
        }

        DataSourceRequest dataSourceRequest2 = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("bizListItemAssociationType", bizList.getBizListItemType().toString(),"eq"));
        PageImpl<ClaFileDto> claFileDtos2 = filesApplicationService.listFilesDeNormalized(dataSourceRequest2);
        Assert.assertThat("Verify at least one file returned",claFileDtos2.getNumberOfElements(),Matchers.greaterThan(0));

        Assert.assertTrue("Verify that list of associations is not empty", claFileDto.getAssociatedBizListItems().size() > 0);
        Assert.assertThat("Verify name is not empty",claFileDto.getAssociatedBizListItems().get(0).getName(),Matchers.not(Matchers.isEmptyOrNullString()));
        Assert.assertThat("Verify state is not empty",claFileDto.getAssociatedBizListItems().get(0).getState(),Matchers.notNullValue());
        logger.debug("There should be at least one file with implicit association");
        for (ClaFileDto fileDto : claFileDtos) {
            for (SimpleBizListItemDto simpleBizListItemDto : fileDto.getAssociatedBizListItems()) {
                if (simpleBizListItemDto.isImplicit()) {
                    logger.info("Found implicit association {}",simpleBizListItemDto);
                    return;
                }
            }
        }
        throw new RuntimeException("Failed to find an implicit association");
    }

    @Test
    public void testAssociateGroupWithBizListItem() {
        logger.info("testAssociateGroupWithBizListItem");
        SimpleBizListItemDto bizListItem = createBizListItem(bizList,"ABC","ABC");
        FacetPage<AggregationCountItemDTO<GroupDto>> groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        GroupDto firstGroup = groups.getContent().get(0).getItem();
        logger.debug("Associate group {} with bizListItem {}",firstGroup.getId(),bizListItem);

        bizListApiController.associateBizListItemWithGroup(bizList.getId(),bizListItem.getId(),firstGroup.getId());
        groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        GroupDto firstGroup2 = groups.getContent().get(0).getItem();
        logger.debug("Check that group {} is associated with bizList item",firstGroup2.getId());
        Assert.assertThat("Check that group has at least 1 associaction",firstGroup2.getAssociatedBizListItems().size(),Matchers.greaterThan(0));
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("groupId",firstGroup.getId(),"eq"));
        logger.debug("Check that files of group {} are associated with bizList item",firstGroup2.getId());
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("Check that group has at least 1 file in response",claFileDtos.getNumberOfElements(),Matchers.greaterThan(0));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        Assert.assertThat("Check that file has associations",claFileDto.getAssociatedBizListItems().size(), Matchers.greaterThan(0));

        logger.debug("Check that fileCount facet works as intended");
        scheduledOperationService.runOperations();
        FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> bizListItemsAssociationFileCounts = bizListAppService.findBizListItemsAssociationFileCounts(DataSourceRequest.build(null), bizList.getId());
        Assert.assertThat("Verify file counts return at least one file",bizListItemsAssociationFileCounts.getContent().size(),Matchers.greaterThan(0));
        Assert.assertTrue("Verify file counts returns the bizListId "+bizListItem.getId(),checkFacetForBizListItem(bizListItem, bizListItemsAssociationFileCounts));

    }

    @Test
    public void testRemoveGroupAssociation() {
        logger.info("testRemoveGroupAssociation");
        SimpleBizListItemDto bizListItem = createBizListItem(bizList,"testRemoveGroupAssociation","testRemoveGroupAssociation");
        FacetPage<AggregationCountItemDTO<GroupDto>> groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        GroupDto firstGroup = groups.getContent().get(0).getItem();
        logger.debug("Associate group {} with bizListItem {}",firstGroup.getId(),bizListItem);
        bizListApiController.associateBizListItemWithGroup(bizList.getId(),bizListItem.getId(),firstGroup.getId());
        scheduledOperationService.runOperations();
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("bizListItemAssociationId",bizListItem.getId(),"eq"));
        FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> bizListItemsAssociationFileCounts = bizListAppService.findBizListItemsAssociationFileCounts(dataSourceRequest, bizList.getId());
        Assert.assertThat("Verify file counts return at least one file",bizListItemsAssociationFileCounts.getContent().size(),Matchers.greaterThan(0));

        logger.debug("Remove bizListItem from group");
        bizListApiController.removeBizListItemAssociationFromGroup(bizList.getId(),bizListItem.getId(),firstGroup.getId());
        scheduledOperationService.runOperations();
        groups = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest.build(null));
        firstGroup = groups.getContent().get(0).getItem();

        logger.debug("Check that group doesn't have association");
        for (SimpleBizListItemDto simpleBizListItemDto : firstGroup.getAssociatedBizListItems()) {
            if (simpleBizListItemDto.getId().equals(bizListItem.getId())) {
                throw new RuntimeException("Group is still associated with bizListItem");
            }
        }

        logger.debug("Check that files are not marked with bizListItem");

        dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("bizListItemAssociationId",bizListItem.getId(),"eq"));
        bizListItemsAssociationFileCounts = bizListAppService.findBizListItemsAssociationFileCounts(dataSourceRequest, bizList.getId());
        Assert.assertThat("Verify no file counts returned",bizListItemsAssociationFileCounts.getContent().size(),Matchers.equalTo(0));

    }

    public boolean checkFacetForBizListItem(SimpleBizListItemDto bizListItem, FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> bizListItemsAssociationFileCounts) {
        for (AggregationCountItemDTO<SimpleBizListItemDto> simpleBizListItemDtoAggregationCountItemDTO : bizListItemsAssociationFileCounts.getContent()) {
            if (simpleBizListItemDtoAggregationCountItemDTO.getItem().getId().equals(bizListItem.getId())) {
                return true;
            }
        }
        return false;
    }

    public SimpleBizListItemDto createBizListItem(BizListDto bizList,String name, String businessId) {
        SimpleBizListItemDto entityDto = new SimpleBizListItemDto();
        entityDto.setName(name);
        entityDto.setBusinessId(businessId);
        entityDto.setState(BizListItemState.ACTIVE);
        return bizListAppService.createBizListItem(bizList.getId(), entityDto);
    }


}
