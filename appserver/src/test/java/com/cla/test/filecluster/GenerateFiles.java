package com.cla.test.filecluster;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Created by: yael
 * Created on: 12/14/2017
 */
public class GenerateFiles {

    private static Random r = new Random();

    public static void createRandomWordFile(String fullPath) {
        List<String> lines = new ArrayList<>();

        List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();
        garbageCollectorMXBeans.stream().mapToLong(gcmx -> gcmx.getCollectionTime()).filter(l -> l>0).sum();

        int times = r.nextInt(10) + 1;

        for (int i = 0; i < times; ++i) {
            lines.add(UUID.randomUUID().toString());
        }

        try {
            GenerateFiles.createWordFile(lines, fullPath);
        } catch (Exception e) {e.printStackTrace();}
    }

    public static void createWordFile(List<String> lines, String fileName) throws IOException {

        XWPFDocument document = new XWPFDocument();
        document.createStyles();

        File file = new File(fileName);

        FileOutputStream out = new FileOutputStream(file);
        XWPFParagraph paragraph = document.createParagraph();
        XWPFRun run = paragraph.createRun();

        for (String line : lines) {
            run.setText(line);
            run.addBreak();
        }

        document.write(out);
        out.close();

    }
}
