package com.cla.test.filecluster.filecrawler;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.StoreLocation;
import com.cla.common.domain.dto.filetree.StoreSecurity;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.TimeSource;
import com.cla.eventbus.EventBus;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.mediaproc.map.chain.*;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.mediaproc.map.*;
import com.cla.kvdb.KeyValueDaoConfig;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.kvdb.KeyValueFileEntityDao;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.commons.io.FileUtils;
import org.apache.solr.common.SolrInputDocument;
import org.assertj.core.util.Lists;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.mockito.Mockito.*;
import static org.springframework.test.util.ReflectionTestUtils.setField;

/**
 * Created by oren on 12/18/2017.
 */
@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class ReScanChangeLogTest {

    private static final String KVDB_DIR = "./temp/kvdbtest/rescan";

    private Random random = new SecureRandom();

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private FileCrawlerExecutionDetailsService runService;

    @Mock
    private FileTreeApiController fileTreeApiController;

    @Mock
    private JobManagerService jobManagerService;

    @Mock
    private RootFolderRepository rootFolderRepository;

    @Mock
    private DocFolderService docFolderService;

    @Mock
    private FilesDataPersistenceService filesDataPersistenceService;

    @Mock
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Mock
    private ScanDiffEventProvider scanDiffEventProvider;

    @Mock
    private FileCatService fileCatService;

    @Mock
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Mock
    private RootFolderService rootFolderService;

    @Mock
    private com.cla.filecluster.util.query.DBTemplateUtils DBTemplateUtils;

    @Mock
    private EventBus eventBus;

    @InjectMocks
    private PreProcessHandler preProcessHandler;

    @InjectMocks
    private FolderHandler folderHandler;

    @InjectMocks
    private FolderRenameHandler folderRenameHandler;

    @InjectMocks
    private NewFileHandler newFileHandler;

    @InjectMocks
    private AclUpdateFileHandler aclUpdateFileHandler;

    @InjectMocks
    private UnDeleteFileHandler unDeleteFileHandler;

    @InjectMocks
    private ContentUpdateFileHandler contentUpdateFileHandler;

    @InjectMocks
    private DeleteFileHandler deleteFileHandler;

    @InjectMocks
    private EndMapForFile endMapForFile;

    @InjectMocks
    private FileRenameHandler fileRenameHandler;

    @InjectMocks
    private MapErrorHandler mapErrorHandler;

    @InjectMocks
    private ExchangeAttachmentHandler exchangeAttachmentHandler;

    @InjectMocks
    private SpecialResponseHandler specialResponseHandler;

    @InjectMocks
    private MapAclHandler mapAclHandler;

    @InjectMocks
    private ApplyFolderAssociationHandler applyFolderAssociationHandler;

    @InjectMocks
    private CalcGeneralDataHandler calcGeneralDataHandler;

    @InjectMocks
    private KeyValueDatabaseRepository kvdbRepo = new KeyValueDatabaseRepository();

    @InjectMocks
    private MapDeleteHandler mapDeleteHandler;

    @InjectMocks
    private MapServiceUtils mapServiceUtils;

    @Mock
    private MapExchangeServices mapExchangeServices;

    @Mock
    private MapBatchTaskHandler mapBatchTaskHandler;

    @InjectMocks
    private MapRenameHandler mapRenameHandler;

    @InjectMocks
    private MapAddHandler mapAddHandler;

    @InjectMocks
    private ScanItemConsumerChain scanItemConsumer = new ScanItemConsumerChain();

    private KeyValueFileEntityDao instance;

    @After
    public void tearDown() throws IOException {
        kvdbRepo.getFileEntityDao().dropDb();
        kvdbRepo.getFileEntityDao().close();

        // resetMocks()
        Mockito.reset(mapBatchResultsProcessor);
        Mockito.reset(fileTreeApiController);
        Mockito.reset(jobManagerService);
        Mockito.reset(docStoreService);
        Mockito.reset(fileCatService);
    }

    @Before
    public void init() {

        try {
            File f = new File(KVDB_DIR);
            if (f.isDirectory()) {
                FileUtils.cleanDirectory(f);
            }
        } catch (IOException e) {
            throw new RuntimeException("Can not clean directory " + KVDB_DIR, e);
        }
        KeyValueDaoConfig config = KeyValueDaoConfig.create()
                .setBasePath(KVDB_DIR)
                .setDuplicateEntries(false);


        instance = KeyValueFileEntityDao.getInstance(config, eventBus);
        setField(kvdbRepo, "fileEntityDao", instance);

        setField(mapServiceUtils, "kvdbRepo", kvdbRepo);
        setField(mapDeleteHandler, "kvdbRepo", kvdbRepo);
        setField(mapDeleteHandler, "mapServiceUtils", mapServiceUtils);

        setField(calcGeneralDataHandler, "mapServiceUtils", mapServiceUtils);
        setField(folderHandler, "mapServiceUtils", mapServiceUtils);
        setField(folderRenameHandler,"mapRenameHandler",mapRenameHandler);
        setField(newFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(newFileHandler, "mapAddHandler", mapAddHandler);
        setField(aclUpdateFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(contentUpdateFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(unDeleteFileHandler, "mapServiceUtils", mapServiceUtils);
        setField(endMapForFile, "mapServiceUtils", mapServiceUtils);
        setField(deleteFileHandler, "mapDeleteHandler", mapDeleteHandler);


        setField(scanItemConsumer, "preProcessHandler", preProcessHandler);
        setField(scanItemConsumer, "specialResponseHandler", specialResponseHandler);
        setField(scanItemConsumer, "calcGeneralDataHandler", calcGeneralDataHandler);
        setField(scanItemConsumer, "folderHandler", folderHandler);
        setField(scanItemConsumer, "folderRenameHandler", folderRenameHandler);
        setField(scanItemConsumer, "newFileHandler", newFileHandler);
        setField(scanItemConsumer, "deleteFileHandler", deleteFileHandler);
        setField(scanItemConsumer, "aclUpdateFileHandler", aclUpdateFileHandler);
        setField(scanItemConsumer, "contentUpdateFileHandler", contentUpdateFileHandler);
        setField(scanItemConsumer, "fileRenameHandler", fileRenameHandler);
        setField(scanItemConsumer, "unDeleteFileHandler", unDeleteFileHandler);
        setField(scanItemConsumer, "mapAclHandler", mapAclHandler);
        setField(scanItemConsumer, "applyFolderAssociationHandler", applyFolderAssociationHandler);
        setField(scanItemConsumer, "exchangeAttachmentHandler", exchangeAttachmentHandler);
        setField(scanItemConsumer, "endMapForFile", endMapForFile);
        setField(scanItemConsumer, "timeSource", new TimeSource());

        KpiRecording kpiRecording = new KpiRecording("", "", 1, 1L, performanceKpiRecorder,"");
        when(performanceKpiRecorder.startRecording(anyString(), anyString(), anyInt())).thenReturn(kpiRecording);
        when(docStoreService.getRootFolderIngestTypePredicate(anyLong())).thenReturn(path -> true);

        scanItemConsumer.init();
    }

    private RootFolder getRootFolder() {
        RootFolder rootFolder = new RootFolder();
        Long rootFolderId = Math.abs(random.nextLong());
        rootFolder.setId(rootFolderId);
        rootFolder.setBaseDepth(1);
        rootFolder.setMediaType(MediaType.FILE_SHARE);

        String name = kvdbRepo.getRepoName(rootFolderId, "appserver");
        instance.executeInTransaction(null, name, store -> true);

        return rootFolder;
    }

    private ScanResultMessage getScanResultMessage(String mediaItemId, String fullFileName, DiffType diffType, Long rootFolderId, Long runContext) {
        Long jobId = Math.abs(random.nextLong());
        Long taskId = Math.abs(random.nextLong());

        MediaChangeLogDto changeLog = new MediaChangeLogDto(mediaItemId, diffType);
        changeLog.setFileName(fullFileName);
        changeLog.setFolder(false);
        return new ScanResultMessage(changeLog, rootFolderId, false, runContext, taskId, jobId);
    }

    private ScanResultMessage addNewFile(String fileName, Long runContext) {
        RootFolder rootFolder = getRootFolder();
        MediaConnectionDetails connectionDetails = new MediaConnectionDetails();
        connectionDetails.setMediaType(MediaType.ONE_DRIVE);
        rootFolder.setMediaConnectionDetails(connectionDetails);
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath("D:\\Data");
        rootFolderDto.setRescanActive(true);
        rootFolderDto.setStoreLocation(StoreLocation.LOCAL_DATACENTER);
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        rootFolderDto.setStoreSecurity(StoreSecurity.CLEAR);
        rootFolderDto.setNickName("Data");
        rootFolderDto.setScannedFilesCountCap(-1);
        rootFolderDto.setFileTypes(new FileType[]{FileType.WORD, FileType.EXCEL, FileType.PDF});
        rootFolderDto.setIngestFileTypes(new FileType[]{FileType.WORD, FileType.EXCEL, FileType.PDF});
        CustomerDataCenterDto dataCenterDto = new CustomerDataCenterDto();
        dataCenterDto.setId(1);
        dataCenterDto.setName("default");
        dataCenterDto.setLocation("global");
        dataCenterDto.setDescription("global");
        dataCenterDto.setDefaultDataCenter(true);
        rootFolderDto.setCustomerDataCenterDto(dataCenterDto);

        when(fileTreeApiController.createRootFolder(rootFolderDto, 1L)).then(ans -> {
            RootFolderDto dto = ans.getArgument(0);
            dto.setId(Math.abs(random.nextLong()));
            return dto;
        });

        rootFolderDto = fileTreeApiController.createRootFolder(rootFolderDto, 1L);

        long ingestJobId = Math.abs(random.nextLong());
        when(jobManagerService.getJobIdCached(anyLong(), eq(JobType.SCAN))).then(ans -> ans.getArgument(0));
        when(jobManagerService.getJobIdCached(anyLong(), eq(JobType.INGEST))).then(ans -> ingestJobId);

        when(docStoreService.getRootFolderCached(anyLong())).thenReturn(rootFolder);
        SolrFolderEntity solrFolder = new SolrFolderEntity();
        solrFolder.setId(random.nextLong());
        solrFolder.setParentsInfo(Lists.newArrayList("d1.d2.d3"));
        solrFolder.setMediaEntityId("");
        DocFolderService.DocFolderCacheValue docFolderCache = new DocFolderService.DocFolderCacheValue(solrFolder);
        doReturn(docFolderCache).when(docFolderService).createDocFolderIfNeededWithCache(anyLong(), anyString());

        String fullFileName = rootFolderDto.getPath() + "\\" + fileName;

        FileCatBuilder builder = new FileCatBuilder();
        builder.setId("112!56").setFileId(56L).setContentId(112L).setDocFolderId(112L);

        when(fileCatService.getBuilderForCreateUpdateRecord(nullable(ClaFile.class), anyLong(), anyLong(), nullable(List.class), any(ClaFilePropertiesDto.class),
                nullable(Long.class))).then(invocation -> builder);

        Collection<ScanResultMessage> scanResultMsg = Lists.newArrayList();
        ScanResultMessage changeLogMsg = getScanResultMessage(UUID.randomUUID().toString(), fullFileName, DiffType.NEW, rootFolderDto.getId(), runContext);
        scanResultMsg.add(changeLogMsg);
        MediaChangeLogDto changeLog = (MediaChangeLogDto) changeLogMsg.getPayload();

        Long claFileId = Math.abs(random.nextLong());
        ClaFile claFile = new ClaFile();
        claFile.setFullPathProperties(changeLog.getFileName());
        claFile.setMediaEntityId(changeLog.getMediaItemId());
        claFile.setId(claFileId);
        doReturn(claFile).when(fileCatService).useExistingOrCreateRecord(nullable(ClaFile.class), any(Long.class), any(String.class), any(FileType.class), any(ClaFilePropertiesDto.class), any(Long.class));

        kvdbRepo.getFileEntityDao().createStore(mapServiceUtils.getStoreName(rootFolderDto.getId()));

        scanItemConsumer.processScanResultMessages(1L, scanResultMsg);
        return changeLogMsg;
    }

    @Test
    public void testChangeLogAddFile() {
        Long runContext = Math.abs(random.nextLong());
        addNewFile("file.xlsx", runContext);

        ArgumentCaptor<FileToAdd> fileCreateCapture = ArgumentCaptor.forClass(FileToAdd.class);
        // For some reason createTask is called twice, once from test..
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(any(), fileCreateCapture.capture());
        Assert.assertTrue(fileCreateCapture.getValue().getDoc() != null);
        SolrInputDocument doc = fileCreateCapture.getValue().getDoc();
        Assert.assertTrue(doc.getFieldValue("taskState") != null);
    }

    @Test
    public void testChangeLogDeleteFile() {
        Long runContext = Math.abs(random.nextLong());
        String deletedFileName = "deletedFile.docx";
        ScanResultMessage addFileScanResultMsg = addNewFile(deletedFileName, runContext);
        String itemId = ((ClaFilePropertiesDto) addFileScanResultMsg.getPayload()).getMediaItemId();
        ItemDeletionPayload itemDeletedPayload = new ItemDeletionPayload(itemId);
        ScanResultMessage message = new ScanResultMessage(itemDeletedPayload,
                addFileScanResultMsg.getRootFolderId(),
                false,
                addFileScanResultMsg.getRunContext(),
                addFileScanResultMsg.getTaskId(), addFileScanResultMsg.getJobId());

        String storeName = mapServiceUtils.getStoreName(addFileScanResultMsg.getRootFolderId());
        kvdbRepo.getFileEntityDao().createStore(storeName);

        FileEntity entity = new FileEntity();
        entity.setFileId(1L);
        kvdbRepo.getFileEntityDao().executeInTransaction(77L, storeName, fileEntityStore -> {
            fileEntityStore.put(itemId, entity);
            return true;
        });
        verify(mapBatchResultsProcessor, times(1)).collectCatFiles(any(), any());

        SolrFileEntity entitySolrFile = new SolrFileEntity();
        entitySolrFile.setFileId(1L);
        when(fileCatService.findByFileIds(any())).thenReturn(Lists.newArrayList(entitySolrFile));
        when(mapBatchResultsProcessor.fileAlreadySetForDelete(any())).thenReturn(false);

        Collection<ScanResultMessage> results = Lists.newArrayList(message);
        scanItemConsumer.processScanResultMessages(1L, results);
        verify(mapBatchResultsProcessor, times(2)).collectCatFiles(any(), any());
        verify(mapBatchResultsProcessor, times(1)).collectContentMetadataFileCountToUpdate(any(), any());
        verify(mapBatchResultsProcessor, times(1)).collectDeletedFiles(any(), any());
    }
}

