package com.cla.test.filecluster.filecrawler;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;

/**
 * Created by: yael
 * Created on: 1/3/2018
 */
@Category(FastTestCategory.class)
public class ScanMultiTaskTest extends ClaBaseTests {

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    private static RootFolderDto rootFolderDto;

    @Before
    public void scanBaseDir() {
        ReflectionTestUtils.setField(remoteMediaProcessingService, "scanTaskDepth", 2);
        rootFolderDto = performanceTestService.analyzeFolder("./src/test/resources/files/scanMultiTest");
    }

    @After
    public void cleanup() {
        deleteRootFolder(rootFolderDto.getId());
        ReflectionTestUtils.setField(remoteMediaProcessingService, "scanTaskDepth", 0);
    }

    @Test
    @Ignore
    public void testScanSuccess() {
        RootFolderDto rootFolder = fileTreeApiController.getRootFolder(rootFolderDto.getId());
        int counter = 0;
        while (rootFolder.getLastRunId() == null && counter < 5){
            try {
                Thread.sleep(200);
                rootFolder = fileTreeApiController.getRootFolder(rootFolderDto.getId());
                counter++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Assert.assertNotNull("last run id should be populated", rootFolder.getLastRunId());
        List<SimpleJob> jobs = jobManagerService.getJobs(rootFolder.getLastRunId(), JobType.SCAN, rootFolderDto.getId());
        Assert.assertEquals("There should be only one scan job for run", 1, jobs.size());
        SimpleJob job = jobs.get(0);
        int tasksNotDoneCounter = jobManagerService.getNotDoneTasksForJobByType(job.getId(), TaskType.SCAN_ROOT_FOLDER);
        Assert.assertEquals("There should be only one scan job for run", 0, tasksNotDoneCounter);
        List<SimpleTask> tasks = jobManagerService.getTaskByJobIdAndTaskTypeAndStates(job.getId(), TaskType.SCAN_ROOT_FOLDER, TaskState.DONE);
        Assert.assertEquals("there should be done tasks", 5, tasks.size());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("rootFolderId", rootFolderDto.getId(), KendoFilterConstants.CONTAINS));
        PageImpl<ClaFileDto> files = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertEquals("number of files scanned in root folder", 5, files.getTotalElements());
    }
}
