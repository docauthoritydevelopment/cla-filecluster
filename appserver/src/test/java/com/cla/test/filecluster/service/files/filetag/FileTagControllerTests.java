package com.cla.test.filecluster.service.files.filetag;

import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.*;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.FileTagApiController;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.FileCategoryApplicationService;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.solr.common.SolrDocument;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by uri on 25/08/2015.
 */
@Category(FastTestCategory.class)
public class FileTagControllerTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(FileTagControllerTests.class);

    @Autowired
    private FileTagApiController fileTagApiController;

    @Autowired
    private DocTypeAppService docTypeAppService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FileCategoryApplicationService fileCategoryApplicationService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private DocFolderService docFolderService;

    private RootFolderDto rootFolderDto;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(600); // 120 seconds max per method tested

    @Before
    public void scanBaseDir() {
        rootFolderDto = rootFolderDtoAnalyze;
    }

    @Test
    public void testUserTagsFile() {
        logger.debug("testUserTagsFile");
        DocTypeDto docTypeDto = getFirstDocTypeDto();
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testUserTagsFile", "Test Users Tags a file", docTypeDto.getId());
        Assert.assertEquals("verify new tag has required name", "testUserTagsFile", createdFileTagDto.getName());
        Assert.assertEquals("verify new tag has required docType name", docTypeDto.getName(), createdFileTagDto.getDocTypeContextName());
        Assert.assertEquals("verify new tag has required docType ide", (long) docTypeDto.getId(), (long) createdFileTagDto.getDocTypeContextId());

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<ClaFileDto> claFileDtos = fileCategoryApplicationService.listFilesWithSolrFilter(dataSourceRequest);
        ClaFileDto fileDto = claFileDtos.getContent().get(0);

        logger.debug("Add the tag {} to file {}", createdFileTagDto.getName(), fileDto.getFileName());
        fileTagApiController.addTagToFile(fileDto.getId(), createdFileTagDto.getId());

        //now verify
        Collection<FileTagDto> fileTagsForFile = getFileTagsForFile(fileDto.getId());
        Assert.assertNotNull("FileTagsForFile should not be null", fileTagsForFile);
        assertThat("There should be at least one tag", fileTagsForFile.size(), Matchers.greaterThanOrEqualTo(1));
        verifyFileTag(createdFileTagDto, fileTagsForFile);
        Collection<FileTagDto> fileTagsForFileSolr = getFileTagsDtoForFileFromSolr(fileDto.getId());
        verifyFileTag(createdFileTagDto, fileTagsForFileSolr);
        fileTagApiController.removeTagFromFile(fileDto.getId(), createdFileTagDto.getId());
    }

    @Test
    public void testGetTagCount() {
        logger.debug("testGetTagCount");
        FacetPage<AggregationCountItemDTO<FileTagDto>> tagsFileCount = fileTagApiController.findTagsFileCount(null, null);
        logger.debug("Got a total of {} tags with file counts", tagsFileCount.getNumberOfElements());
        Map<String, String> params = new HashMap<>();
        params.put("filter", "{\"logic\":\"and\",\"filters\":[{\"field\":\"file.tagType\",\"operator\":\"eq\",\"value\":1}]}");
        FacetPage<AggregationCountItemDTO<FileTagDto>> tagsFileCount2 = fileTagApiController.findTagsFileCount(params, null);
        logger.debug("Got a total of {} tags with file counts", tagsFileCount2.getNumberOfElements());
    }

    @Test
    public void testUserRemovesTagFromFile() {
        logger.debug("testUserRemovesTagFromFile");
        DocTypeDto docTypeDto = getFirstDocTypeDto();
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testUserRemovesTagFromFile", "Test Users Removes a Tags from a file", docTypeDto.getId());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<ClaFileDto> claFileDtos = fileCategoryApplicationService.listFilesWithSolrFilter(dataSourceRequest);
        ClaFileDto fileDto = claFileDtos.getContent().get(0);

        logger.debug("Add the tag {} to file {}", createdFileTagDto.getName(), fileDto.getFileName());
        fileTagApiController.addTagToFile(fileDto.getId(), createdFileTagDto.getId());

        logger.debug("Remove the tag {} from the file {}", createdFileTagDto.getName(), fileDto.getFileName());
        fileTagApiController.removeTagFromFile(fileDto.getId(), createdFileTagDto.getId());

        //now verify
        Collection<FileTagDto> fileTagsForFile = getFileTagsForFile(fileDto.getId());
        Assert.assertNotNull("FileTagsForFile should not be null", fileTagsForFile);
        verifyFileTagRemoved(createdFileTagDto, fileTagsForFile);
        Collection<FileTagDto> fileTagsForFileSolr = getFileTagsDtoForFileFromSolr(fileDto.getId());
        verifyFileTagRemoved(createdFileTagDto, fileTagsForFileSolr);

    }

    @Test
    public void testUserTagsGroup() {
        logger.info("testUserTagsGroup");
        DocTypeDto docTypeDto = getFirstDocTypeDto();
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testUserTagsGroup", "Test Users Tags a group", docTypeDto.getId());
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        GroupDto groupDto = groupDtos.getContent().get(0);
        logger.debug("Add tag {} to group {}", createdFileTagDto.getName(), groupDto);
        fileTagApiController.addTagToGroup(groupDto.getId(), createdFileTagDto.getId());
        //Apply to solr and verify
        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();
        Page<ClaFileDto> claFileDtos = filesApiController.ListFilesByGroup(null, groupDto.getId());
//        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        for (ClaFileDto fileDto : claFileDtos) {
            verifySolrDocContainsTag(createdFileTagDto, fileDto);
        }
        fileTagApiController.removeTagFromGroup(groupDto.getId(), createdFileTagDto.getId());
    }

    private void verifySolrDocContainsTag(FileTagDto createdFileTagDto, ClaFileDto claFileDto) {
        Long fileId = claFileDto.getId();
        logger.debug("Verify that file {} has the new tag (Solr)", claFileDto);
        Collection<FileTagDto> fileTagsForFileSolr = getFileTagsDtoForFileFromSolr(fileId);
        verifyFileTag(createdFileTagDto, fileTagsForFileSolr);

        logger.debug("Get File tag count");
        FacetPage<AggregationCountItemDTO<FileTagDto>> tagsFileCount = fileTagApiController.findTagsFileCount(null, null);
        logger.debug("Got {} File tags", tagsFileCount.getContent().size());
        Assert.assertThat("There should be file tags in the count response", tagsFileCount.getContent().size(), Matchers.greaterThan(0));
    }

    @Test
    public void testUserRemovesTagFromGroup() {
        logger.info("testUserRemovesTagFromGroup");
        DocTypeDto docTypeDto = getFirstDocTypeDto();
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testUserRemovesTagBusinessId", "Test Users Remove a tag from business ID", docTypeDto.getId());
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        GroupDto groupDto = groupDtos.getContent().get(0);
        fileTagApiController.addTagToGroup(groupDto.getId(), createdFileTagDto.getId());

        fileTagApiController.removeTagFromGroup(groupDto.getId(), createdFileTagDto.getId());
        //Now Verify
        logger.debug("Verify all tags were removed from SQL");
        Page<ClaFileDto> claFileDtos = filesApiController.ListFilesByGroup(null, groupDto.getId());
        claFileDtos.forEach(claFileDto -> verifyFileTagRemoved(createdFileTagDto, claFileDto.getFileTagDtos()));
        //Remove from Solr
        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();

        logger.debug("Verify all tags were removed from Solr");
        claFileDtos.forEach(claFileDto -> {
            logger.debug("Checking file {}", claFileDto);
            Collection<FileTagDto> fileTagsForFileSolr = getFileTagsDtoForFileFromSolr(claFileDto.getId());
            verifyFileTagRemoved(createdFileTagDto, fileTagsForFileSolr);
        });
    }

    @Test
    public void testUserTagsFolder1() {
        logger.info("testUserTagsFolder1");
        DocTypeDto docTypeDto = getFirstDocTypeDto();
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testUserTagsFolder", "Test Users Tags a folder", docTypeDto.getId());
        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto docFolderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());
//        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.debug("Tag root folder {} id {}", docFolderDto.getPath(), docFolderDto.getId());
        fileTagApiController.addTagToFolder(docFolderDto.getId(), createdFileTagDto.getId());
        scheduledOperationService.runOperations();

        logger.debug("Verify files in root folder");
        Page<ClaFileDto> claFileDtos = fileTreeApiController.listFilesInFolder(docFolderDto.getId(), null);
        claFileDtos.forEach(claFileDto -> verifyFileTag(createdFileTagDto, claFileDto.getFileTagDtos()));

        logger.debug("List subFolders");
        Page<DocFolderDto> subFolders = fileTreeApiController.listFoldersFromSubFolder(docFolderDto.getId(), null);
        logger.debug("Verify files in sub folders");
        Page<ClaFileDto> claFileDtos2 = fileTreeApiController.listFilesInFolder(subFolders.getContent().get(0).getId(), null);
        logger.debug("Verify file-tags in files");
        claFileDtos2.forEach(claFileDto -> verifyFileTag(createdFileTagDto, claFileDto.getFileTagDtos()));

        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();
        //
        logger.debug("Verify that files are tagged in Solr");

        claFileDtos = fileTreeApiController.listFilesInFolderRecursive(docFolderDto.getId(), null);
        claFileDtos.forEach(claFileDto -> {
            Collection<FileTagDto> fileTagsForFile = getFileTagsDtoForFileFromSolr(claFileDto.getId());
            verifyFileTag(createdFileTagDto, fileTagsForFile);
        });
        fileTagApiController.removeTagFromFolder(docFolderDto.getId(), createdFileTagDto.getId());
        logger.info("Finished test testUserTagsFolder1");
    }

    @Test
    public void testGetFileTagTypes() {
        logger.info("testGetFileTagTypes");
        List<FileTagTypeDto> allFileTagTypes = fileTagApiController.getAllFileTagTypes();
        Assert.assertThat("There should be more than one file tag type", allFileTagTypes.size(), Matchers.greaterThan(1));
    }

    @Test
    public void testGetFileTags() {
        logger.debug("testGetFileTags");
        List<FileTagTypeDto> allFileTagTypes = fileTagApiController.getAllFileTagTypes();
        FileTagTypeDto fileTagTypeDto = allFileTagTypes.get(0);

        FileTagDto createdFileTagDto = fileTagApiController.createFileTag(fileTagTypeDto.getId(), "testGetFileTags", "testGetFileTags", null, null);
        logger.debug("Newly created file tag id {}", createdFileTagDto.getId());
        List<FileTagDto> allFileTags = fileTagApiController.getAllFileTags(fileTagTypeDto.getId());
        Assert.assertThat("There should be file tags", allFileTags.size(), Matchers.greaterThan(0));

    }

    @Test
    public void testUpdateFileTagDescription() {
        logger.debug("testUpdateFileTagDescription");
        FileTagTypeDto fileTagTypeDto = fileTagApiController.getAllFileTagTypes().get(0);
        FileTagDto createdFileTagDto = fileTagApiController.createFileTag(fileTagTypeDto.getId(), "testUpdateFileTag", "testUpdateFileTag", null, null);
        logger.info("testUpdateFileTag - created file tag by id {}", createdFileTagDto.getId());
        FileTagDto updateDto = new FileTagDto();
        String updatedDescription = "my new description";
        updateDto.setName(createdFileTagDto.getName());
        updateDto.setDescription(updatedDescription);
        fileTagApiController.updateFileTagById(createdFileTagDto.getId(), updateDto);
        FileTagDto updatedFileTag = fileTagApiController.getFileTagById(createdFileTagDto.getId());
        Assert.assertEquals("Verify description is updated", updatedDescription, updatedFileTag.getDescription());
    }

    @Test
    public void testUserRemovesTagFromFolder1() {
        logger.info("testUserRemovesTagFromFolder1");
        DocTypeDto docTypeDto = getFirstDocTypeDto();
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testUserRemovesTagFolder", "Test Users Remove a tag from a folder", docTypeDto.getId());
        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        logger.debug("Tag root folder");
        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        fileTagApiController.addTagToFolder(docFolderDto.getId(), createdFileTagDto.getId());

        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();

        logger.debug("Remove tag from root folder");
        fileTagApiController.removeTagFromFolder(docFolderDto.getId(), createdFileTagDto.getId());

        logger.debug("Apply tags to solr");
        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();

        logger.debug("Verify files in root folder removed");
        Page<ClaFileDto> claFileDtos = fileTreeApiController.listFilesInFolder(docFolderDto.getId(), null);
        claFileDtos.forEach(claFileDto -> verifyFileTagRemoved(createdFileTagDto, claFileDto.getFileTagDtos()));

        logger.debug("Verify files in sub folder removed");
        Page<ClaFileDto> claFileDtos2 = fileTreeApiController.listFilesInFolder(docFolderDtos.getContent().get(1).getId(), null);
        claFileDtos2.forEach(claFileDto -> verifyFileTagRemoved(createdFileTagDto, claFileDto.getFileTagDtos()));

        logger.debug("Apply tags to solr");
        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();

        logger.debug("Verify that files are not tagged in Solr");
        claFileDtos = fileTreeApiController.listFilesInFolderRecursive(docFolderDto.getId(), null);
        claFileDtos.forEach(claFileDto -> {
            Collection<FileTagDto> fileTagsForFile = getFileTagsDtoForFileFromSolr(claFileDto.getId());
            verifyFileTagRemoved(createdFileTagDto, fileTagsForFile);
        });
        logger.info("testUserRemovesTagFromFolder1 - done");

    }

    @Test
    public void testFacetFileTagTypes() {
        logger.info("testFacetFileTagTypes");

        FileTagDto createdFileTagDto = fileTagApiController.createFileTag(12, "testFacetFileTagTypes", "", null, null);

        tagFirstFile(createdFileTagDto);

        FacetPage<AggregationCountItemDTO<FileTagTypeDto>> tagTypesFileCount = fileTagApiController.findTagTypesFileCount(null, null);
        Assert.assertThat("There should be at least one facet", tagTypesFileCount.getContent().size(), Matchers.greaterThan(0));
        for (AggregationCountItemDTO<FileTagTypeDto> fileTagTypeDtoAggregationCountItemDTO : tagTypesFileCount.getContent()) {
            logger.debug("Got {} :{}", fileTagTypeDtoAggregationCountItemDTO.getItem(), fileTagTypeDtoAggregationCountItemDTO.getCount());
        }

    }

    @Test
    public void testFacetFileTagTypes2() {
        logger.info("testFacetFileTagTypes2");
        //Let's use the first 2 tag types
        FileTagDto createdFileTagDto = fileTagApiController.createFileTag(11, "testFacetFileTagTypes2-1", "", null, null);
        tagFirstFile(createdFileTagDto);
        FileTagDto createdFileTagDto2 = fileTagApiController.createFileTag(12, "testFacetFileTagTypes2-2", "", null, null);
        tagFirstFile(createdFileTagDto2);

        FacetPage<AggregationCountItemDTO<FileTagTypeDto>> tagTypesFileCount = fileTagApiController.findTagTypesFileCount(null, null);
        AggregationCountItemDTO<FileTagTypeDto> fileTagTypeDtoAggregationCountItemDTO = tagTypesFileCount.getContent().get(1);
        long tagTypeId = fileTagTypeDtoAggregationCountItemDTO.getItem().getId();
        Map<String, String> params = new HashMap<>();
        params.put(DataSourceRequest.SELECTED_ITEM_ID, String.valueOf(tagTypeId));
        FacetPage<AggregationCountItemDTO<FileTagTypeDto>> tagTypesFileCount2 = fileTagApiController.findTagTypesFileCount(params, null);
        logger.debug("Lists should be identical");
        for (int i = 0; i < tagTypesFileCount.getContent().size(); i++) {
            FileTagTypeDto item = tagTypesFileCount.getContent().get(i).getItem();
            FileTagTypeDto item2 = tagTypesFileCount2.getContent().get(i).getItem();
            Assert.assertEquals(item, item2);
        }
    }

    @Test
    public void testListFacetFileTagsBySelectedId() {
        logger.info("testListFacetFileTagsBySelectedId");
        logger.debug("Create 50 tags and use them");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        final long dummyTagId = createDummyTagType("DummyTagType",
                false, true, false, false)
                .getId();
        Page<ClaFileDto> claFileDtos = fileCategoryApplicationService.listFilesWithSolrFilter(dataSourceRequest);
        ClaFileDto fileDto = claFileDtos.getContent().get(0);

        long startOp = System.currentTimeMillis();
        List<Long> tags = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            FileTagDto createdFileTagDto = fileTagApiController.createFileTag(dummyTagId, "testListFacetFileTagsBySelectedId-" + i, "", null, null);
            tags.add(createdFileTagDto.getId());
        }
        logger.debug("create new tags took {}", (System.currentTimeMillis()-startOp));
        startOp = System.currentTimeMillis();
        addTagsToFile(fileDto.getId(), tags);
        logger.debug("add the tags to the file took {}", (System.currentTimeMillis()-startOp));

        logger.debug("Show the 2nd page");
        Map<String, String> params = new HashMap<>();
        params.put("page", "2");
        FacetPage<AggregationCountItemDTO<FileTagDto>> tagsFileCount = fileTagApiController.findTagsFileCount(params, null);
        Assert.assertEquals("Check that page number is 2", 2, tagsFileCount.getPageNumber());
        Assert.assertThat("Check that page size is not 50", tagsFileCount.getPageSize(), Matchers.lessThan(50));
        startOp = System.currentTimeMillis();
        removeAssociationsByTagType(dummyTagId);
        logger.debug("remove tags from the file took {}", (System.currentTimeMillis()-startOp));
    }

    private void addTagsToFile(Long fileId, List<Long> tags) {
        List<FileTag> fileTags = fileTagService.findByIds(tags);
        for (FileTag fileTag : fileTags) {
            fileTagApiController.addTagToFileByScope(fileId, fileTag, fileTag.getType().getScope());
        }
    }

    private void tagFirstFile(FileTagDto createdFileTagDto) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<ClaFileDto> claFileDtos = fileCategoryApplicationService.listFilesWithSolrFilter(dataSourceRequest);
        ClaFileDto fileDto = claFileDtos.getContent().get(0);

        logger.debug("Add the tag {} to file {}", createdFileTagDto.getName(), fileDto.getFileName());
        fileTagApiController.addTagToFile(fileDto.getId(), createdFileTagDto.getId());
    }

    private DocTypeDto getFirstDocTypeDto() {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<DocTypeDto> allDocTypesAsDto = docTypeAppService.getAllDocTypesAsDto(dataSourceRequest);
        return allDocTypesAsDto.getContent().get(0);
    }


    private void verifyFileTag(FileTagDto createdFileTagDto, Collection<FileTagDto> fileTagsForFile) {
        StringBuilder debugInfo = new StringBuilder();
        for (FileTagDto fileTagDto : fileTagsForFile) {
            if (fileTagDto.getName().equals(createdFileTagDto.getName())) {
                return;
            }
            debugInfo.append(fileTagDto.getName()).append(",");
        }
        throw new RuntimeException("Failed to find fileTag " + createdFileTagDto.getId() + " by name " + createdFileTagDto.getName() + " in list of file tags: [" + debugInfo + "]");
    }

    private void verifyFileTagRemoved(FileTagDto createdFileTagDto, Collection<FileTagDto> fileTagsForFile) {
        StringBuilder debugInfo = new StringBuilder();
        for (FileTagDto fileTagDto : fileTagsForFile) {
            if (fileTagDto.getName().equals(createdFileTagDto.getName())) {
                throw new RuntimeException("verifyFileTagRemoved - Found fileTag by name " + createdFileTagDto.getName() + " in list of file tags: " + debugInfo);
            }
            debugInfo.append(fileTagDto.getName()).append(",");
        }
        logger.debug(debugInfo.toString());
    }

    @Test
    public void testAddRemoveAddTagToFolder() {
        logger.info("testAddRemoveAddTagToFolder");
        FileTagDto createdFileTagDto = createDefaultTypeFileTag("testAddRemoveAddTagToFolder", "", null);
        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.debug("Tag folder {}", docFolderDto);
        fileTagApiController.addTagToFolder(docFolderDto.getId(), createdFileTagDto.getId());

        logger.info("Remove tag from folder");
        fileTagApiController.removeTagFromFolder(docFolderDto.getId(), createdFileTagDto.getId());

        logger.info("Add Tag to folder {} again", docFolderDto);
        fileTagApiController.addTagToFolder(docFolderDto.getId(), createdFileTagDto.getId());

        logger.debug("Apply tags to solr");
        fileTagApiController.applyTagsToSolr(true, true, 1000);
        scheduledOperationService.runOperations();

        logger.info("Verify folder has tag");
        DocFolderDto folderDetails = fileTreeApiController.getFolderDetails(docFolderDto.getId());
        boolean found = false;
        for (FileTagDto fileTagDto : folderDetails.getFileTagDtos()) {
            if (fileTagDto.getId().equals(createdFileTagDto.getId())) {
                logger.debug("Found tag on folder");
                found = true;
            }
        }
        if (!found) {
            throw new AssertionError("Failed to find tag " + createdFileTagDto.getId() + " in folder " + folderDetails.getId());
        }
        logger.debug("Verify files has tag");
        Page<ClaFileDto> claFileDtos = fileTreeApiController.listFilesInFolder(docFolderDto.getId(), null);
        claFileDtos.forEach(claFileDto -> verifyFileTag(createdFileTagDto, claFileDto.getFileTagDtos()));
        fileTagApiController.removeTagFromFolder(docFolderDto.getId(), createdFileTagDto.getId());
    }

    @Test
    public void testAddExplicitToImplicit() {
        logger.info("testAddExplicitToImplicit");
        final long dummyTagTypeId = createDummyTagType("DummyTagType",
                false, false, false, false)
                .getId();
        final FileTagDto createdFileTagDto = createDummyTag(dummyTagTypeId, "dummyTag");

        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto parentFolderDto = docFolderDtos.getContent().get(0);
        logger.info("Add tag {} to root folder {}", createdFileTagDto.getId(), parentFolderDto);
        fileTagApiController.addTagToFolder(parentFolderDto.getId(), createdFileTagDto.getId());

        DocFolderDto childFolderDto = docFolderDtos.getContent().get(1);
        logger.info("Add the same tag to child folder {}", childFolderDto);
        fileTagApiController.addTagToFolder(childFolderDto.getId(), createdFileTagDto.getId());

        logger.info("Verify child folder has explicit tag");
        DocFolderDto folderDetails = fileTreeApiController.getFolderDetails(childFolderDto.getId());
        boolean found = false;
        for (FileTagDto fileTagDto : folderDetails.getFileTagDtos()) {
            if (fileTagDto.getId().equals(createdFileTagDto.getId())) {
                logger.debug("Found tag on folder");
                found = true;
            }
        }
        if (!found) {
            throw new AssertionError("Failed to find tag " + createdFileTagDto.getId() + " in folder " + folderDetails.getId());
        }
        found = false;
        logger.debug("There are {} associations on the folder", folderDetails.getFileTagAssociations().size());
        for (FileTagAssociationDto fileTagAssociationDto : folderDetails.getFileTagAssociations()) {
            logger.debug("Tag association on folder: {}", fileTagAssociationDto);
            if (fileTagAssociationDto.getFileTagDto().getId().equals(createdFileTagDto.getId())) {
                if (fileTagAssociationDto.getFileTagSource() == FileTagSource.FOLDER_EXPLICIT) {
                    logger.debug("Found explicit tag on folder");
                    found = true;
                }
            }
        }
        if (!found) {
            throw new AssertionError("Failed to find explicit tag " + createdFileTagDto.getId() + " in folder " + folderDetails.getId());
        }
        fileTagApiController.removeTagFromFolder(parentFolderDto.getId(), createdFileTagDto.getId());
        fileTagApiController.removeTagFromFolder(childFolderDto.getId(), createdFileTagDto.getId());
    }

    /**
     * Create a uniqeue dummy tag into the system
     */
    private FileTagDto createDummyTag(long fileTagTypeId, String tagName) {
        final String uuid = UUID.randomUUID().toString();
        String name = tagName + " - " + uuid;
        return fileTagApiController.createFileTag(fileTagTypeId, name, name, null, null);
    }

    /**
     * Create unique dummyTag type into the system
     */
    private FileTagTypeDto createDummyTagType(String tagTypeName,
                                              boolean singleValue,
                                              boolean sensitive,
                                              boolean hidden,
                                              boolean system) {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setSingleValueTag(singleValue);
        fileTagTypeDto.setName(tagTypeName + " - " + UUID.randomUUID().toString());
        fileTagTypeDto.setSensitive(sensitive);
        fileTagTypeDto.setDescription("Auto generated " + tagTypeName);
        fileTagTypeDto.setHidden(hidden);
        fileTagTypeDto.setSystem(system);
        return fileTagApiController.createFileTagType(fileTagTypeDto);
    }

    @Test
    public void testCreateAndUpdateTagType() {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setName("testCreateAndUpdateTagType");
        fileTagTypeDto.setDescription("testCreateAndUpdateTagType");
        fileTagTypeDto.setSensitive(true);
        FileTagTypeDto fileTagType = fileTagApiController.createFileTagType(fileTagTypeDto);
        Assert.assertEquals(fileTagTypeDto.getName(), fileTagType.getName());
        Assert.assertEquals(fileTagTypeDto.getDescription(), fileTagType.getDescription());
        Assert.assertEquals(fileTagTypeDto.isSensitive(), fileTagType.isSensitive());

        fileTagType.setName("testCreateAndUpdateTagType2");
        FileTagTypeDto fileTagTypeDto1 = fileTagApiController.updateFileTagType(fileTagType.getId(), fileTagType);
        Assert.assertEquals("testCreateAndUpdateTagType2", fileTagTypeDto1.getName());

        logger.info("Delete the tag type");
        fileTagApiController.deleteFileTagTypeById(fileTagTypeDto1.getId());
    }

    private FileTagDto createDefaultTypeFileTag(String name, String description, Long docTypeId) {
        FileTagType fileTagType = fileTaggingAppService.getFileTagType(fileTagTypeService.getDefaultTypeNameTagTypeName());
        FileTagDto docTypeFileTag = fileTaggingAppService.createFileTag(name, description, fileTagType, docTypeId, FileTagAction.MANUAL);
        return docTypeFileTag;
    }

    private Set<FileTagDto> getFileTagsForFile(long fileId) {
        Set<Long> fileTagIds = new HashSet<>();
        Set<FileTagDto> fileTags = new HashSet<>();
        ClaFile claFile = fileCatService.getFileObject(fileId);
        //Check if the file has explicit fileTags
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(fileId, claFile.getAssociations(), AssociationType.FILE);
        if (associations != null) {
            associations.stream().filter(a -> a.getFileTagId() != null).forEach(fta -> fileTagIds.add(fta.getFileTagId()));
        }
        fileTagIds.forEach(tagId -> fileTags.add(fileTagService.getFromCache(tagId)));
        //Check if the group has explicit fileTags
        if (claFile.getUserGroupId() != null) {
            GroupDetailsDto groupObj = groupsApiController.findGroupById(claFile.getUserGroupId());
            groupObj.getGroupDto().getFileTagAssociations().stream()
                    .filter(a -> a.getFileTagDto() != null).forEach(fta -> fileTags.add(fta.getFileTagDto()));
        }
        //Check if the docFolder has explicit fileTags
        if (claFile.getDocFolderId() != null) {
            DocFolder df = docFolderService.getById(claFile.getDocFolderId());
            List<AssociationEntity> folderAssociations = AssociationsService.convertFromAssociationEntity(df.getId(), df.getAssociations(), AssociationType.FOLDER);
            List<AssociableEntityDto> entities = associationsService.getEntitiesForAssociations(folderAssociations);
            folderAssociations.stream().filter(a -> a.getFileTagId() != null).forEach(fta -> fileTags.add(
                    (FileTagDto) AssociationUtils.getEntityFromList(
                            FileTagDto.class, fta.getFileTagId() ,entities)
            ));
        }
        return fileTags;
    }

    private Set<FileTagDto> convertToDto(Set<FileTag> fileTags) {
        Set<FileTagDto> result = new HashSet<>();
        fileTags.forEach(fileTag -> result.add(FileTagService.convertFileTagToDto(fileTag)));
        return result;
    }

    private Set<FileTagDto> getFileTagsDtoForFileFromSolr(long fileId) {
        ClaFile claFile = fileCatService.getFileObject(fileId);
        if (claFile == null) {
            throw new BadRequestException("fileId", String.valueOf(fileId), "Unable to locate file by fileId " + fileId, BadRequestType.ITEM_NOT_FOUND);
        }
        Set<FileTag> tags = getFileTagsForFileFromSolr(fileId);
        return FileTagService.convertFileTagsToDto(tags);
    }

    private Set<FileTag> getFileTagsForFileFromSolr(long fileId) {
        logger.debug("Get File Tags for file {} from Solr", fileId);
        SolrDocument categoryFile = fileCatService.findCategoryFile(fileId);
        if (categoryFile == null) {
            throw new RuntimeException("Get File Tags for file " + fileId + ". Unable to find file by fileId in fileCat Solr core");
        }
        @SuppressWarnings("unchecked")
        List<String> solrTagIds = (List<String>) categoryFile.get("tags");
        if (solrTagIds == null) {
            //No tags
            return new HashSet<>();
        }
        return findTagsBySolrIds(solrTagIds);
    }

    private Set<FileTag> findTagsBySolrIds(List<String> solrTagIds) {
        List<Long> tagIds = solrTagIds.stream()
                .filter(solrTagId -> !"dummy".equals(solrTagId))
                .map(AssociationIdUtils::extractTagIdFromSolrIdentifier)
                .collect(Collectors.toList());
        return fileTagService.getFileTagsById(tagIds);
    }

    public void removeAssociationsByTagType(long fileTagTypeId) {
        FileTagType fileTagType = fileTagTypeService.getFileTagType(fileTagTypeId);
        logger.warn("Removing all associations by tag type {}", fileTagType);
        int deletedInPage;
        do {
            deletedInPage = deleteFileAssociationsByTagTypePage(fileTagType, fileTagType.getScope());
        }
        while (deletedInPage > 0);
    }

    private int deleteFileAssociationsByTagTypePage(FileTagType fileTagType, BasicScope basicScope) {
        Long scopeId = basicScope == null ? null : basicScope.getId();
        logger.info("Delete file associations by tag type {}", fileTagType);
        List<FileTag> tags = fileTagService.getFileTagsByType(fileTagType.getId());
        List<Long> tagIds = tags.stream().map(FileTag::getId).collect(Collectors.toList());
        Map<Long, List<String>> filesWithTags = new HashMap<>();
        for (FileTag tag : tags) {
            filesWithTags.putAll(associationsService.getAssociationFromSolrForFile("fileTagId", String.valueOf(tag.getId()), 0,1000));
        }

        int files = 0;
        Set<Long> fileIds = filesWithTags.keySet();
        List<SolrFileEntity> fileObjs = fileCatService.findByFileIds(fileIds);
        Map<Long, SolrFileEntity> fileById = fileObjs.stream().collect(Collectors.toMap(SolrFileEntity::getFileId, r -> r));

        for (Map.Entry<Long, List<String>> entry : filesWithTags.entrySet()) {
            List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(
                    entry.getKey(), entry.getValue(), AssociationType.FILE);

            for (AssociationEntity association : associations) {
                if (association.getFileTagId() != null && tagIds.contains(association.getFileTagId())) {
                    if ((association.getAssociationScopeId() == null && scopeId == null) ||
                            (scopeId != null && scopeId.equals(association.getAssociationScopeId()))) {
                        SolrFileEntity fileEntity = fileById.get(entry.getKey());
                        associationsService.removeAssociationFromFile(fileEntity,
                                fileTagService.getFromCache(association.getFileTagId()),
                                association, SolrCommitType.SOFT_NOWAIT);

                        files++;
                    }
                }
            }
        }
        logger.info("Delete file assocations by tag type {} for {} files", fileTagType, files);
        fileCatService.commitToSolr();
        return files;
    }
}
