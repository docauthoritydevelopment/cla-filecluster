package com.cla.test.filecluster.service.files.histogram;

import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.solr.common.util.Base64;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

/**
 * Tests for ClaHistogramCollection class
 * Created by yael on 11/16/2017.
 */
@Category(UnitTestCategory.class)
public class LongHistogramArrayTest {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(LongHistogramArrayTest.class);


    @Test
    public void TestRealHist() {
        final String pvFile1 = "./src/test/resources/histogram/pv1.txt";    // \\a\\Andrea Ring\\ifercdec.xls
        final String pvFile2 = "./src/test/resources/histogram/pv2.txt";    // \\a\\Andrea Ring\\IFERCJan.xls
        int maturity=31;
        String metadata1="EAEBAGphdmEudXRpbC5BcnJheUxpc/QBAQMBRmVicnVhcnkgMjAwsAEBamF2YS51dGlsLkhhc2hNYfABAQMEEQEABADUBgLgAgEAAQEDAUluc2lkZSBGRVJDJ3MgR2FzIE1hcmtldCBSZXBvcnQgbW9udGhseSBiaWR3ZWVrIHByaWNlIGZpbGUuIKABgjABAg==";
        String metadata2="EAEBAGphdmEudXRpbC5BcnJheUxpc/QBAQMBRmVicnVhcnkgMjAwsAEBamF2YS51dGlsLkhhc2hNYfABAQMEEQEABACGBgLCAgEAAQEDAUluc2lkZSBGRVJDJ3MgR2FzIE1hcmtldCBSZXBvcnQgbW9udGhseSBiaWR3ZWVrIHByaWNlIGZpbGUuIKABgjABAg==";
        String pvCollections1Str = null;
        String pvCollections2Str = null;
        try {
            pvCollections1Str = new String(Files.readAllBytes(Paths.get(pvFile1)), "UTF8");
            pvCollections2Str = new String(Files.readAllBytes(Paths.get(pvFile2)), "UTF8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        PvAnalysisData pvc1 = PvAnalysisDataUtils.convertBinaryToPvCollectionsAnalysisData(50L,
                Base64.base64ToByteArray(pvCollections1Str) , Base64.base64ToByteArray(metadata1), maturity);
        PvAnalysisData pvc2 = PvAnalysisDataUtils.convertBinaryToPvCollectionsAnalysisData(55L,
                Base64.base64ToByteArray(pvCollections2Str) , Base64.base64ToByteArray(metadata2), maturity);

        FileCollections pva1 = pvc1.getPvCollections().getArraysCollections();
        FileCollections pva2 = pvc2.getPvCollections().getArraysCollections();

        ClaHistogramCollection<Long> hc1_CellValues = pvc1.getPvCollections().getCollections().get("February 2000").getCollection("PV_CellValues");
        ClaHistogramCollection<Long> hc2_CellValues = pvc2.getPvCollections().getCollections().get("February 2000").getCollection("PV_CellValues");
        ClaHistogramCollection<Long> ha1_CellValues = pva1.getCollections().get("February 2000").getCollection("PV_CellValues");
        ClaHistogramCollection<Long> ha2_CellValues = pva2.getCollections().get("February 2000").getCollection("PV_CellValues");
        float nsrc12_CellValues = hc1_CellValues.getNormalizedSimilarityRatio(hc2_CellValues);
        float nsrc21_CellValues = hc2_CellValues.getNormalizedSimilarityRatio(hc1_CellValues);
        float nsra12_CellValues = ha1_CellValues.getNormalizedSimilarityRatio(ha2_CellValues);
        float nsra21_CellValues = ha2_CellValues.getNormalizedSimilarityRatio(ha1_CellValues);
        assertEquals(nsrc12_CellValues, nsrc21_CellValues, 0.00001);
        assertEquals(nsrc12_CellValues, nsra12_CellValues, 0.00001);
        float src12_CellValues = hc1_CellValues.getSimilarityRatio(hc2_CellValues);
        float src21_CellValues = hc2_CellValues.getSimilarityRatio(hc1_CellValues);
        float sra12_CellValues = ha1_CellValues.getSimilarityRatio(ha2_CellValues);
        float sra21_CellValues = ha2_CellValues.getSimilarityRatio(ha1_CellValues);
        assertEquals(src12_CellValues, src21_CellValues, 0.00001);
        assertEquals(src12_CellValues, sra12_CellValues, 0.00001);
        int osc12_CellValues = hc1_CellValues.getOrderedSimilarityCount(hc2_CellValues);
        int osc21_CellValues = hc2_CellValues.getOrderedSimilarityCount(hc1_CellValues);
        int osa12_CellValues = ha1_CellValues.getOrderedSimilarityCount(ha2_CellValues);
        int osa21_CellValues = ha2_CellValues.getOrderedSimilarityCount(ha1_CellValues);
        assertEquals(osc12_CellValues, osc21_CellValues);
        assertEquals(osc12_CellValues, osa12_CellValues);

        float nsrc11_CellValues = hc1_CellValues.getSimilarityRatio(hc1_CellValues);
        float nsra11_CellValues = ha1_CellValues.getSimilarityRatio(ha1_CellValues);
        float src11_CellValues = hc1_CellValues.getNormalizedSimilarityRatio(hc1_CellValues);
        float sra11_CellValues = ha1_CellValues.getNormalizedSimilarityRatio(ha1_CellValues);
        assertEquals(1f, nsrc11_CellValues, 0.00001);
        assertEquals(1f, nsra11_CellValues, 0.00001);
        assertEquals(1f, src11_CellValues, 0.00001);
        assertEquals(1f, sra11_CellValues, 0.00001);

        ClaHistogramCollection<Long> hc1_ExcelStyles = pvc1.getPvCollections().getCollections().get("February 2000").getCollection("PV_ExcelStyles");
        ClaHistogramCollection<Long> hc2_ExcelStyles = pvc2.getPvCollections().getCollections().get("February 2000").getCollection("PV_ExcelStyles");
        ClaHistogramCollection<Long> ha1_ExcelStyles = pva1.getCollections().get("February 2000").getCollection("PV_ExcelStyles");
        ClaHistogramCollection<Long> ha2_ExcelStyles = pva2.getCollections().get("February 2000").getCollection("PV_ExcelStyles");
        float src12_ExcelStyles = hc1_ExcelStyles.getSimilarityRatio(hc2_ExcelStyles);
        float sra12_ExcelStyles = ha1_ExcelStyles.getSimilarityRatio(ha2_ExcelStyles);
        assertEquals(src12_ExcelStyles, sra12_ExcelStyles, 0.00001);
        int osc12_ExcelStyles = hc1_ExcelStyles.getOrderedSimilarityCount(hc2_ExcelStyles);
        int osc21_ExcelStyles = hc2_ExcelStyles.getOrderedSimilarityCount(hc1_ExcelStyles);
        assertEquals(osc12_ExcelStyles, osc21_ExcelStyles);
        int osa12_ExcelStyles = ha1_ExcelStyles.getOrderedSimilarityCount(ha2_ExcelStyles);
        int osa21_ExcelStyles = ha2_ExcelStyles.getOrderedSimilarityCount(ha1_ExcelStyles);
        assertEquals(osc12_ExcelStyles, osa12_ExcelStyles);
        assertEquals(osa12_ExcelStyles, osa21_ExcelStyles);
    }

    @Ignore
    @Test
    public void TestRealHistSpeed() {
        final String pvFile1 = "./src/test/resources/histogram/pv1.txt";    // \\a\\Andrea Ring\\ifercdec.xls
        final String pvFile2 = "./src/test/resources/histogram/pv2.txt";    // \\a\\Andrea Ring\\IFERCJan.xls
        int maturity = 31;
        String metadata1 = "EAEBAGphdmEudXRpbC5BcnJheUxpc/QBAQMBRmVicnVhcnkgMjAwsAEBamF2YS51dGlsLkhhc2hNYfABAQMEEQEABADUBgLgAgEAAQEDAUluc2lkZSBGRVJDJ3MgR2FzIE1hcmtldCBSZXBvcnQgbW9udGhseSBiaWR3ZWVrIHByaWNlIGZpbGUuIKABgjABAg==";
        String metadata2 = "EAEBAGphdmEudXRpbC5BcnJheUxpc/QBAQMBRmVicnVhcnkgMjAwsAEBamF2YS51dGlsLkhhc2hNYfABAQMEEQEABACGBgLCAgEAAQEDAUluc2lkZSBGRVJDJ3MgR2FzIE1hcmtldCBSZXBvcnQgbW9udGhseSBiaWR3ZWVrIHByaWNlIGZpbGUuIKABgjABAg==";
        String pvCollections1Str = null;
        String pvCollections2Str = null;
        try {
            pvCollections1Str = new String(Files.readAllBytes(Paths.get(pvFile1)), "UTF8");
            pvCollections2Str = new String(Files.readAllBytes(Paths.get(pvFile2)), "UTF8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        int outerCount = 10;
        int innerCount = 50000;

        PvAnalysisData pvc1 = PvAnalysisDataUtils.convertBinaryToPvCollectionsAnalysisData(50L,
                Base64.base64ToByteArray(pvCollections1Str), Base64.base64ToByteArray(metadata1), maturity);
        PvAnalysisData pvc2 = PvAnalysisDataUtils.convertBinaryToPvCollectionsAnalysisData(55L,
                Base64.base64ToByteArray(pvCollections2Str), Base64.base64ToByteArray(metadata2), maturity);

        FileCollections pva1 = pvc1.getPvCollections().getArraysCollections();
        FileCollections pva2 = pvc2.getPvCollections().getArraysCollections();

        ClaHistogramCollection<Long> hc1_CellValues = pvc1.getPvCollections().getCollections().get("February 2000").getCollection("PV_TextNgrams");
        ClaHistogramCollection<Long> hc2_CellValues = pvc2.getPvCollections().getCollections().get("February 2000").getCollection("PV_TextNgrams");
        ClaHistogramCollection<Long> ha1_CellValues = pva1.getCollections().get("February 2000").getCollection("PV_TextNgrams");
        ClaHistogramCollection<Long> ha2_CellValues = pva2.getCollections().get("February 2000").getCollection("PV_TextNgrams");

        for (int j = 0; j < outerCount; ++j) {
            float nsrc12_CellValues;
            Long time1 = System.nanoTime();
            for (int i = 0; i < innerCount; ++i) {
                nsrc12_CellValues = hc1_CellValues.getNormalizedSimilarityRatio(hc2_CellValues);
            }
            time1 = System.nanoTime() - time1;
//        float nsrc21_CellValues = hc2_CellValues.getNormalizedSimilarityRatio(hc1_CellValues);
            float nsra12_CellValues;
            Long time2 = System.nanoTime();
            for (int i = 0; i < innerCount; ++i) {
                nsra12_CellValues = ha1_CellValues.getNormalizedSimilarityRatio(ha2_CellValues);
            }
            time2 = System.nanoTime() - time2;
//        float nsra21_CellValues = ha2_CellValues.getNormalizedSimilarityRatio(ha1_CellValues);
            float src12_CellValues;
            Long time3 = System.nanoTime();
            for (int i = 0; i < innerCount; ++i) {
                src12_CellValues = hc1_CellValues.getSimilarityRatio(hc2_CellValues);
            }
            time3 = System.nanoTime() - time3;
//        float src21_CellValues = hc2_CellValues.getSimilarityRatio(hc1_CellValues);
            float sra12_CellValues;
            Long time4 = System.nanoTime();
            for (int i = 0; i < innerCount; ++i) {
                sra12_CellValues = ha1_CellValues.getSimilarityRatio(ha2_CellValues);
            }
            time4 = System.nanoTime() - time4;
//        float sra21_CellValues = ha2_CellValues.getSimilarityRatio(ha1_CellValues);
            int osc12_CellValues;
            Long time5 = System.nanoTime();
            for (int i = 0; i < innerCount; ++i) {
                osc12_CellValues = hc1_CellValues.getOrderedSimilarityCount(hc2_CellValues);
            }
            time5 = System.nanoTime() - time5;
//        int osc21_CellValues = hc2_CellValues.getOrderedSimilarityCount(hc1_CellValues);
            int osa12_CellValues;
            Long time6 = System.nanoTime();
            for (int i = 0; i < innerCount; ++i) {
                osa12_CellValues = ha1_CellValues.getOrderedSimilarityCount(ha2_CellValues);
            }
            time6 = System.nanoTime() - time6;
            int osa21_CellValues = ha2_CellValues.getOrderedSimilarityCount(ha1_CellValues);

            logger.info("Time1={} Time2={} Time3={} Time4={} Time5={} Time6={}",
                    TimeUnit.NANOSECONDS.toMillis(time1),
                    TimeUnit.NANOSECONDS.toMillis(time2),
                    TimeUnit.NANOSECONDS.toMillis(time3),
                    TimeUnit.NANOSECONDS.toMillis(time4),
                    TimeUnit.NANOSECONDS.toMillis(time5),
                    TimeUnit.NANOSECONDS.toMillis(time6));
        }
    }
}
