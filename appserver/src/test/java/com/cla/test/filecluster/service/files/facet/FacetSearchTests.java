package com.cla.test.filecluster.service.files.facet;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.service.api.FileCategoryApiController;
import com.cla.filecluster.service.api.FileTagApiController;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.FileUserApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.categories.FileCategoryApplicationService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Category(FastTestCategory.class)
public class FacetSearchTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(FacetSearchTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FileCategoryApiController fileCategoryApiController;

    @Autowired
    private FileCategoryApplicationService fileCategoryApplicationService;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private FileTagApiController fileTagApiController;

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    @Autowired
    private SolrClientFactory solrClientFactory;

    @Autowired
    private FileUserApiController fileUserApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    private RootFolderDto rootFolderDto;

    @Before
    public void setUp() {
        rootFolderDto = rootFolderDtoAnalyze;
    }


    @Test
    public void testFacetField() {
        Map<String, String> params = new HashMap<>();
        params.put("facetField", "groupId");
        FacetPage<AggregationCountItemDTO<String>> aggregationCountDTOs = fileCategoryApiController.listFileFacetsByField(params);
        logger.debug("Found results : {} ", aggregationCountDTOs.getNumberOfElements());
        Assert.assertThat("There should be at least 2 groups", aggregationCountDTOs.getNumberOfElements(), Matchers.greaterThanOrEqualTo(2));
        Assert.assertThat("First group should contain count > 0", aggregationCountDTOs.getContent().get(0).getCount(), Matchers.greaterThan(0L));
    }

    @Test
    public void testFacetFieldWithFilter() {
        FilterDescriptor filterDescriptor = new FilterDescriptor();
        filterDescriptor.setField("type");
        filterDescriptor.setValue(FileType.WORD);
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setFilter(filterDescriptor);
        FacetPage<AggregationCountItemDTO<String>> aggregationCountDTOs = fileCategoryApplicationService.listFileFacetsByField(dataSourceRequest, "groupId");
        logger.debug("testFacetFieldWithFilter - Found results : {} ", aggregationCountDTOs.getNumberOfElements());
        int firstCount = aggregationCountDTOs.getNumberOfElements();
        Assert.assertThat("There should be at least 2 groups", aggregationCountDTOs.getNumberOfElements(), Matchers.greaterThanOrEqualTo(2));
        List<AggregationCountItemDTO<String>> filteredList = aggregationCountDTOs.getContent().stream().filter(fl -> fl.getCount() > 0).collect(Collectors.toList());
        Assert.assertThat("filteredList  should contain less then " + firstCount + " items", filteredList.size(), Matchers.lessThan(firstCount));
    }

    @Test
    public void testFindGroupsFileCount() {
        logger.debug("testFindGroupsFileCount");
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        logger.debug("testFindGroupsFileCount - got {} groups", groupsFileCount.getNumberOfElements());
        Assert.assertThat("There should be at least 2 groups", groupsFileCount.getNumberOfElements(), Matchers.greaterThanOrEqualTo(2));
        AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO = groupsFileCount.getContent().get(0);
        Assert.assertThat("Every group should have at least 1 file", groupDtoAggregationCountItemDTO.getCount(), Matchers.greaterThan(0L));
        GroupDto item = groupDtoAggregationCountItemDTO.getItem();
        logger.debug("Group {} should have {} files in solr", item.getId(), item.getNumOfFiles());
        long numOfFiles = item.getNumOfFiles();
        Assert.assertEquals("group should have at numOfFiles=count", numOfFiles, groupDtoAggregationCountItemDTO.getCount());
    }

    @Test
    public void testFindGroupsFileCountFilterFolder() {
        logger.debug("testFindGroupsFileCountFilterFolder");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor filter = new FilterDescriptor();
        filter.setField("fullPath");
        filter.setValue("subFolder");
        filter.setOperator(KendoFilterConstants.CONTAINS);
        dataSourceRequest.setFilter(filter);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
        logger.debug("testFindGroupsFileCount - got {} groups", groupsFileCount.getNumberOfElements());
        //Assert.assertEquals("There should be exactly 1 group", 1, groupsFileCount.getNumberOfElements()); //why one group? there are 2 files with very different contents...
        Assert.assertThat("There should be at least 1 group", groupsFileCount.getNumberOfElements(), Matchers.greaterThanOrEqualTo(1));
        AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO = groupsFileCount.getContent().get(0);
        logger.debug("testFindGroupsFileCount - got {} files in the group", groupDtoAggregationCountItemDTO.getCount());
        Assert.assertThat("group should have at least one file", groupDtoAggregationCountItemDTO.getCount(), Matchers.greaterThan(0L));
        Assert.assertNotNull("group should have a name", groupDtoAggregationCountItemDTO.getItem().getGroupName());

        logger.debug("test filter group name (" + groupDtoAggregationCountItemDTO.getItem().getGroupName() + ")");
        filter.setField("groupName");
        filter.setValue(groupDtoAggregationCountItemDTO.getItem().getGroupName());
        groupsFileCount = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
        if (groupsFileCount.getNumberOfElements() != 1) {
            logger.error("There should be exactly {} group, but was {}", 1, groupsFileCount.getNumberOfElements());
        }
        logger.warn(">>>>> ATTENTION! This test doesn't pass, it is disabled temporarily. Should find out why strange grouping happens here!");
//        Assert.assertEquals("There should be exactly 1 group", 1, groupsFileCount.getNumberOfElements());

    }

    @Test
    public void testFindFoldersFileCount() {
        logger.debug("testFindFoldersFileCount");
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(null);
        logger.debug("testFindFoldersFileCount - got {} folders", foldersFileCount.getNumberOfElements());
        Assert.assertThat("There should be at least 2 folders", foldersFileCount.getNumberOfElements(), Matchers.greaterThanOrEqualTo(2));
        AggregationCountFolderDto countItemDTO = foldersFileCount.getContent().get(0);
        Assert.assertThat("Every countItemDTO (" + countItemDTO.getItem().getName() + ") should have at least 1 file", countItemDTO.getNumOfDirectFiles(), Matchers.greaterThan(0));
    }

    @Test
    public void testFindFoldersTreeFileCount() {
        logger.debug("testFindFoldersTreeFileCount");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeAppService.findFoldersFileCountTreeRoot(dataSourceRequest);
        logger.debug("testFindFoldersTreeFileCount - got {} folders", foldersFileCount.getNumberOfElements());
        Assert.assertThat("There should be at least 1 folder", foldersFileCount.getNumberOfElements(), Matchers.greaterThan(0));
        AggregationCountFolderDto countItemDTO = performanceTestService.findAggregationCountFolderForRF(foldersFileCount.getContent(), rootFolderDto.getId());
//        AggregationCountFolderDto countItemDTO = foldersFileCount.getContent().get(0);
        Assert.assertThat("Every countItemDTO (" + countItemDTO.getItem().getName() + ") should have at least 1 file", countItemDTO.getNumOfAllFiles(), Matchers.greaterThan(0));
        for (AggregationCountFolderDto docFolderDtoAggregationCountItemDTO : foldersFileCount.getContent()) {
            DocFolderDto dto = docFolderDtoAggregationCountItemDTO.getItem();
            Assert.assertEquals("item should be a root folder (node)", 0, dto.getDepthFromRoot().intValue());
            Assert.assertNotNull(dto.getFolderType());
        }
        FacetPage<AggregationCountFolderDto> foldersFileCountTreeNodeChildren = fileTreeAppService.findFoldersFileCountTreeNodeChildren(dataSourceRequest, countItemDTO.getItem().getId());
        logger.debug("testFindFoldersTreeFileCount - got {} folders from children", foldersFileCountTreeNodeChildren.getNumberOfElements());
        Assert.assertThat("There should be at least 1 folder", foldersFileCountTreeNodeChildren.getNumberOfElements(), Matchers.greaterThan(0));
    }

    @Test
    public void testListOwnersFileCount() {
        logger.debug("testListOwnersFileCount");
        FacetPage<AggregationCountItemDTO<FileUserDto>> ownerFileCounts = fileUserApiController.getOwnerFileCounts(null);
        logger.debug("testListOwnersFileCount - got {} owners", ownerFileCounts.getNumberOfElements());
        Assert.assertThat("There should be at least 1 owner", ownerFileCounts.getNumberOfElements(), Matchers.greaterThan(0));
        AggregationCountItemDTO<FileUserDto> fileUserDtoAggregationCountItemDTO = ownerFileCounts.getContent().get(0);
        logger.debug("First owner: {} {}", fileUserDtoAggregationCountItemDTO.getItem().getUser(), fileUserDtoAggregationCountItemDTO.getCount());
        Assert.assertThat("Owner should not be empty / null", fileUserDtoAggregationCountItemDTO.getItem().getUser(), Matchers.not(Matchers.isEmptyOrNullString()));
        Assert.assertThat("Owner should have files", fileUserDtoAggregationCountItemDTO.getCount(), Matchers.greaterThan(0L));

    }

    @Test
    public void testComplexFilterMechanism() {
        logger.debug("testComplexFilterMechanism");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);

        FilterDescriptor subFolderFilter = performanceTestService.createConcreteFilter("fullPath", "subFolder", KendoFilterConstants.CONTAINS);
        FilterDescriptor excelTypeFilter = performanceTestService.createConcreteFilter("type", FileType.EXCEL.toString(), KendoFilterConstants.EQ);
        FilterDescriptor joinFilter = performanceTestService.createJoinFilter("and", subFolderFilter, excelTypeFilter);

        FilterDescriptor nameFilter = performanceTestService.createConcreteFilter("fileName", "Document", KendoFilterConstants.CONTAINS);
        FilterDescriptor baseFilter = performanceTestService.createJoinFilter("or", joinFilter, nameFilter);

        dataSourceRequest.setFilter(baseFilter);

        PageImpl<ClaFileDto> claFileDtosResult = filesApplicationService.listFilesDeNormalized(dataSourceRequest);

        /*FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
        logger.debug("The filter used is: 'fileName contains Document' || ( fullPath contains subFolder and type = EXCEL )");
        logger.debug("testFindGroupsFileCount - got {} groups", groupsFileCount.getNumberOfElements());
        for (AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO : groupsFileCount.getContent()) {
            logger.debug("Got group: {}",groupDtoAggregationCountItemDTO.getItem());
        }*/

        Assert.assertThat("There should be at least 2 files", claFileDtosResult.getNumberOfElements(), Matchers.greaterThanOrEqualTo(2));
    }


    @Test
    public void testFacetTagWithContentFilter() {
        if (solrClientFactory.isSolrCloud()) {
            logger.warn("TODO 2.0 - FIX CONTENT FILTER IN SOLR CLOUD");
            return;
        }
        logger.info(testName.getMethodName());
        FacetPage<AggregationCountItemDTO<FileTagDto>> tagsFileCount = fileTagApiController.findTagsFileCount(null, null);
        logger.debug("Got a result of {} tags", tagsFileCount.getContent().size());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("contentFilter", "aaa", "contains"));
        FacetPage<AggregationCountItemDTO<FileTagDto>> aggregationCountItemDTOFacetPage = fileTaggingAppService.listTagsFileCountWithFileFilter(dataSourceRequest, null);
        logger.debug("Got a result of {} tags", aggregationCountItemDTOFacetPage.getContent().size());

    }

    @Test
    public void testFacetGroupsWithNeqGroupsFilter() {
        testStartLog(logger);
        Map<String, String> params = new HashMap<>();
        FilterDescriptor filterDescriptor = new FilterDescriptor("group.id", "*", "neq");
        String filter = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);
        params.put("filter", filter);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(params);
        logger.debug("Got {} groups", groupsFileCount.getContent().size());
        Assert.assertThat(groupsFileCount.getContent(), Matchers.emptyIterable());
    }

}
