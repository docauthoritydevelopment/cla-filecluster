package com.cla.test.filecluster.service.files.doctype;

import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.scope.UserScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.repository.jpa.doctype.DocTypeRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by uri on 23/08/2015.
 */

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class DocTypeServiceTests {

    @InjectMocks
    private DocTypeService docTypeService;

    @Mock
    private DocTypeRepository docTypeRepository;

    @Mock
    private MessageHandler messageHandler;

    @Mock
    private ScopeService scopeService;

    @Before
    public void init() {
        when(messageHandler.getMessage(any())).thenReturn("err");
    }

    @Test
    public void convertFromDto() {
        when(scopeService.getOrCreateScope(any())).thenAnswer(i -> i.getArguments()[0]);
        when(docTypeRepository.save(any(DocType.class))).thenAnswer(i -> {
            DocType docType = (DocType) i.getArguments()[0];
            docType.setId(88L);
            return docType;
        });
        DocType parent = new DocType();
        parent.setId(6L);
        parent.setDocTypeIdentifier("idp");
        when(docTypeRepository.getOne(6L)).thenReturn(parent);

        DocTypeDto dto = new DocTypeDto();
        dto.setId(2L);
        dto.setName("nnnn");
        dto.setAlternativeName("aaaaa");
        dto.setParentId(6L);
        dto.setDescription("ddd");
        User user = new User();
        user.setName("user");
        dto.setScope(new UserScope());
        dto.getScope().setScopeBindings(user);
        dto.setSingleValue(true);
        dto.setUniqueName("uuukkk");

        DocType docType = docTypeService.createDocType(dto);
        assertEquals(new Long(docType.getId()), new Long(88));
        assertEquals(docType.getAlternativeName(), dto.getAlternativeName());
        assertEquals(docType.getDescription(), dto.getDescription());
        assertEquals(docType.getName(), dto.getName());
        assertEquals(docType.getUniqueName(), dto.getUniqueName());
        assertEquals(new Long(docType.getParentDocType().getId()), dto.getParentId());
        //assertEquals(docType.isSingleValue(), dto.isSingleValue());
        assertEquals(docType.getDocTypeIdentifier(), "idp.88");
        assertEquals(((User) docType.getScope().getScopeBindings()).getName(), ((User) dto.getScope().getScopeBindings()).getName());
    }

    @Test(expected = BadParameterException.class)
    public void doNotAllowEmptyName() {
        DocTypeDto dto = new DocTypeDto();
        dto.setId(2L);
        dto.setName("");
        dto.setAlternativeName("aaaaa");
        dto.setParentId(6L);
        dto.setDescription("ddd");
        User user = new User();
        user.setName("user");
        dto.setScope(new UserScope());
        dto.getScope().setScopeBindings(user);
        dto.setSingleValue(true);
        dto.setUniqueName(null);

        docTypeService.createDocType(dto);
    }

    @Test(expected = BadParameterException.class)
    public void doNotAllowSpacesName() {
        DocTypeDto dto = new DocTypeDto();
        dto.setId(2L);
        dto.setName("  ");
        dto.setAlternativeName("aaaaa");
        dto.setParentId(6L);
        dto.setDescription("ddd");
        User user = new User();
        user.setName("user");
        dto.setScope(new UserScope());
        dto.getScope().setScopeBindings(user);
        dto.setSingleValue(true);
        dto.setUniqueName(null);

        docTypeService.createDocType(dto);
    }

    @Test
    public void testConvertToDto() {
        DocType docType = new DocType();
        docType.setAlternativeName("alt");
        //docType.setBizList(new BizList()); docType.getBizList().setName("bz");
        docType.setDescription("desc");
        docType.setName("nm");
        docType.setUniqueName("uq");
        //docType.setDocTypeIdentifier("id");
        //docType.setDocTypeRiskInformation(new DocTypeRiskInformation());
        //docType.getDocTypeRiskInformation().setName("risk");
        docType.setParentDocType(new DocType());
        docType.getParentDocType().setId(9L);
        User user = new User();
        user.setName("user");
        docType.setScope(new UserScope());
        docType.getScope().setScopeBindings(user);
        docType.getScope().setId(6L);
        docType.setSingleValue(true);
        //docType.setTemplate(new DocTypeTemplate()); docType.getTemplate().setName("temp");
        docType.setId(5L);
        DocTypeDto dto = DocTypeService.convertDocTypeToDtoNoPath(docType, docType.getScope());

        assertEquals(new Long(docType.getId()), dto.getId());
        assertEquals(docType.getAlternativeName(), dto.getAlternativeName());
        assertEquals(docType.getDescription(), dto.getDescription());
        assertEquals(docType.getName(), dto.getName());
        assertEquals(docType.getUniqueName(), dto.getUniqueName());
        assertEquals(new Long(docType.getParentDocType().getId()), dto.getParentId());
        assertEquals(docType.isSingleValue(), dto.isSingleValue());
        assertEquals(((User) docType.getScope().getScopeBindings()).getName(), ((UserDto) dto.getScope().getScopeBindings()).getName());
    }
}
