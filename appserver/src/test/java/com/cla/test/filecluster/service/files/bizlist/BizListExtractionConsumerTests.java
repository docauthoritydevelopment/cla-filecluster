package com.cla.test.filecluster.service.files.bizlist;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.*;
import com.cla.common.domain.dto.file.ClaFileDetailsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.exceptions.BadImportException;
import com.cla.filecluster.service.api.BizListApiController;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.mediaproc.BizListExtractionManager;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.managers.CatFileExtractionAppService;
import com.cla.filecluster.service.files.managers.FileClusterAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.BizListTestCategory;
import org.apache.commons.compress.utils.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 *
 * Created by uri on 23/08/2015.
 */
@Category(BizListTestCategory.class)
public class BizListExtractionConsumerTests extends ClaBaseTests {

    private static final String SAMPLE_CONSUMER_LIST_FILE = "poc_consumer_names_test_hashed.csv";
    private static final String ENTITIES_FOLDER = "./src/test/resources/entities";

    private final static Logger logger = LoggerFactory.getLogger(BizListExtractionConsumerTests.class);

    @Autowired
    private BizListService bizListService;

    @Autowired
    private BizListAppService bizListAppService;

    @Autowired
    private BizListApiController bizListApiController;

    @Autowired
    private BizListExtractionManager bizListExtractionManager;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private CatFileExtractionAppService catFileExtractionAppService;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FileClusterAppService fileClusterAppService;

    private final String TEMPLATE_NAME = "TestConsumers";

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Test
    public void testCreateConsumersListAndPopulate() throws Exception {
        BizListDto bizListDto = new BizListDto();
        bizListDto.setName("testCreateConsumersList");
        bizListDto.setDescription("testCreateConsumersList D");
        bizListDto.setBizListItemType(BizListItemType.CONSUMERS);
        bizListDto.setTemplate(TEMPLATE_NAME);
        BizListDto bizList = bizListAppService.createBizList(bizListDto);
        Assert.assertEquals(bizListDto.getName(),bizList.getName());
        Assert.assertEquals(bizListDto.getBizListItemType(), bizList.getBizListItemType());

        List<BizListDto> allBizLists = bizListApiController.getAllBizLists(BizListItemType.CONSUMERS.name());
        Assert.assertThat(allBizLists,Matchers.not(Matchers.emptyIterable()));
        for (BizListDto bizListDto1 : allBizLists) {
            logger.debug("BizList: {}",bizListDto1);
            Assert.assertThat("Verify template is not empty",bizListDto1.getTemplate(),Matchers.not(Matchers.isEmptyOrNullString()));
        }

        populateBizList(bizList);

        Page<? extends SimpleBizListItemDto> simpleBizListItemDtos = bizListAppService.listBizListItems(bizList.getId(), DataSourceRequest.build(null));
        Assert.assertThat("All lines from the csv should be visible ", simpleBizListItemDtos.getNumberOfElements(), Matchers.greaterThan(0));
        SimpleBizListItemDto simpleBizListItemDto = simpleBizListItemDtos.getContent().get(0);
        if (simpleBizListItemDto.getEntityAliases() != null) {
            Assert.assertThat("There should be no aliases",simpleBizListItemDto.getEntityAliases().size(),Matchers.equalTo(0));
        }
        Assert.assertNotNull(simpleBizListItemDto.getBusinessId());
    }

    public void populateBizList(BizListDto bizList) throws IOException {
        logger.info("Load encrypted consumer list from CSV");
        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + SAMPLE_CONSUMER_LIST_FILE);
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        BizListImportResultDto bizListImportResultDto = bizListAppService.importCsvFile(bizList.getId(), rawData);
        Assert.assertTrue("The import Id should not be null", bizListImportResultDto.getImportId() > 0);
        Assert.assertTrue("At least one row should have been imported", bizListImportResultDto.getImportedRowCount() > 0);

        logger.info("Move to ACTIVE");
        bizListAppService.updateBizListItemsState(bizList.getId(), bizListImportResultDto.getImportId(), null, BizListItemState.ACTIVE);
    }

    @Test
    public void testExtractConsumerBizListEntities() throws IOException {
        logger.debug("testExtractConsumerBizListEntities");
        BizListDto bizListDto = new BizListDto();
        bizListDto.setName("testExtractConsumerBizListEntities");
        bizListDto.setDescription("testExtractConsumerBizListEntities D");
        bizListDto.setBizListItemType(BizListItemType.CONSUMERS);
        bizListDto.setTemplate(TEMPLATE_NAME);
        BizListDto bizList = bizListAppService.createBizList(bizListDto);
        populateBizList(bizList);
        extractBizList(bizList);
        FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> bizListItemsExtractionFileCounts =
                bizListApiController.findBizListItemsExtractionFileCounts(bizList.getId(), null);
        logger.debug("Got {} bizList file counts",bizListItemsExtractionFileCounts.getContent().size());
        Assert.assertThat("There should be at least one result",bizListItemsExtractionFileCounts.getContent().size(),Matchers.greaterThan(0));

        FacetPage<AggregationCountItemDTO<String>> bizListExtractionRulesFileCounts = bizListApiController.findBizListExtractionRulesFileCounts(null);
        Assert.assertThat("Verify at least one rule returned",bizListExtractionRulesFileCounts.getContent().size(),Matchers.greaterThan(0));

        String ruleName = bizListExtractionRulesFileCounts.getContent().get(0).getItem();
        logger.debug("Got extraction rule: "+ruleName);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("extractionRule",ruleName,"eq"));
        logger.debug("Verify filtering files by extraction rule works");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("Verify at least one file returned",claFileDtos.getContent().size(),Matchers.greaterThan(0));

        logger.debug("Verify file contains extracted entities");
        List<ExtractionSummaryDto> bizListExtractionSummary = claFileDtos.getContent().get(0).getBizListExtractionSummary();
        for (ExtractionSummaryDto extractionSummaryDto : bizListExtractionSummary) {
            logger.debug("Extraction Summary: {}", extractionSummaryDto);
            Assert.assertNotNull(extractionSummaryDto.getBizListItemType());
            Assert.assertNotNull(extractionSummaryDto.getBizListId());
            List<SimpleBizListItemDto> sampleExtractions = extractionSummaryDto.getSampleExtractions();
            Assert.assertNotNull(sampleExtractions);
            Assert.assertThat("There should be at least 1 sample extraction", sampleExtractions.size(),Matchers.greaterThan(0));
            for (SimpleBizListItemDto sampleExtraction : sampleExtractions) {
                Assert.assertNotNull(sampleExtraction.getName());
            }

        }
        Assert.assertThat("Verify at least one extraction summary was returned",bizListExtractionSummary.size(), Matchers.greaterThan(0));
        ClaFileDetailsDto fileDetailsById = filesApplicationService.findFileDetailsById(claFileDtos.getContent().get(0).getId());
        Assert.assertThat("Verify at least one extraction summary was returned",fileDetailsById.getFileDto().getBizListExtractionSummary().size(), Matchers.greaterThan(0));
    }

    @Test(expected = BadImportException.class)
    public void testImportWrongTemplate() throws Exception {
        logger.debug("testImportWrongTemplate");
        BizListDto bizListDto = new BizListDto();
        bizListDto.setName("testImportWrongTemplate");
        bizListDto.setDescription("testImportWrongTemplate D");
        bizListDto.setBizListItemType(BizListItemType.CONSUMERS);
        bizListDto.setTemplate("ElCentro");
        BizListDto bizList = bizListAppService.createBizList(bizListDto);
        logger.info("Load encrypted consumer list from CSV");
        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "poc_sample_encrypted.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        BizListImportResultDto bizListImportResultDto = bizListAppService.importCsvFile(bizList.getId(), rawData);

        logger.info("Returned import result: {}",bizListImportResultDto);

    }

    @Test
    public void testCreateAndDeleteBizList() {
        logger.debug("testCreateAndDeleteBizList");
        BizListDto bizListDto = new BizListDto();
        bizListDto.setName("testCreateAndDeleteBizList");
        bizListDto.setDescription("testCreateAndDeleteBizList D");
        bizListDto.setBizListItemType(BizListItemType.CONSUMERS);
        BizListDto bizList = bizListAppService.createBizList(bizListDto);
        logger.info("Delete bizLIst {}",bizList.getId());
        bizListApiController.deleteBizList(bizList.getId());
        for (BizListDto listDto : bizListApiController.getAllBizLists(null)) {
            if (listDto.getId() == bizList.getId()) {
                throw new RuntimeException("Failed to delete bizList Id "+bizList.getId());
            }
        }

    }

    public void extractBizList(BizListDto bizList) {
        logger.info("Create BizList Job.");
        bizListExtractionManager.startExtractRunForBizListAsync(bizList.getId());
        logger.info("Create and run tasks. ");
        bizListExtractionManager.processNextTasksGroup();
        logger.info("Update Solr data for bizList");
        catFileExtractionAppService.updateCatFilesBizListExtractions(bizList.getId(), null);
        logger.info("BizList should be updated on Solr");
    }

    public SimpleBizListItemDto createBizListItem(BizListDto bizList) {
        return createBizListItem(bizList,"Electric Power Supply Association","EPSA");
    }

    public SimpleBizListItemDto createBizListItem(BizListDto bizList, String name, String businessId) {
        SimpleBizListItemDto entityDto = new SimpleBizListItemDto();
        entityDto.setName(name);
        entityDto.setBusinessId(businessId);
        entityDto.setState(BizListItemState.ACTIVE);
        return bizListAppService.createBizListItem(bizList.getId(), entityDto);
    }

}
