package com.cla.test.filecluster.service.files.group;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.AttachFilesToGroupDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.filecluster.domain.converters.SolrFileGroupBuilder;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.specification.FilterBuildUtils;
import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

/**
 * Created by shtand on 25/10/2015.
 */
@Category(FastTestCategory.class)
public class GroupApiTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(GroupApiTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private FileCatService fileCatService;

    private static int testNum;
    private static int testRan = 0;

    private static RootFolderDto rootFolderDto;
    private static FileGroup f1;
    private static ClaFile file;

    @Before
    public void scanBaseDir() {
        if (testRan == 0) {
            testNum = getUnitTestNumber();
            rootFolderDto = rootFolderDtoAnalyze;

            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setName("GroupApiTests1");
            f1 = fileGroupRepository.createGroup(builder, fileGroupRepository.getNextAvailableId(), GroupType.USER_GROUP);

            DataSourceRequest dataSourceRequest = new DataSourceRequest();
            dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId", "*", "NEQ"));
            PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
            logger.info("Got {} un-grouped files", claFileDtos.getContent().size());

            AttachFilesToGroupDto attachFilesToGroupDto = new AttachFilesToGroupDto();
            attachFilesToGroupDto.setGroupId(f1.getId());

            ClaFileDto claFileDto = getFileWithContent(claFileDtos);

            List<Long> fileIds = Lists.newArrayList(claFileDto.getId());
            attachFilesToGroupDto.setFileIds(fileIds);
            GroupDetailsDto group = filesApiController.attachFilesToGroup(attachFilesToGroupDto, performanceTestService.createWaitParams());
            scheduledOperationService.runOperations();
            fileGroupRepository.getById(group.getGroupDto().getId())
                    .ifPresent(fgAfter -> f1.setNumOfFiles(fgAfter.getNumOfFiles()));
            file = fileCatService.getFileObject(claFileDto.getId());
            f1.setFirstMemberId(file.getId());
            updateGroup(f1.getNumOfFiles(), file.getId(), f1.getId());
        }
        ++testRan;
    }

    private void updateGroup(long numOfFiles, Long firstMember, String id) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(id);
        builder.setNumOfFiles(numOfFiles);
        builder.setFirstMemberId(firstMember);
        fileGroupRepository.saveGroupChanges(builder);
    }

    @After
    public void destroyGroups() {
        if (testRan == testNum) {
            ungroupFile(file.getId(), file.getContentMetadataId(), f1.getId());
        }
    }

    @Test
    public void testChangeGroupName() {
        logger.info("testChangeGroupName");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(performanceTestService.createWaitParams());

        GroupDto groupDto = null;

        FacetPage<AggregationCountItemDTO<GroupDto>> aggregationCountItemDTOFacetPage2 = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
        for (AggregationCountItemDTO gg : aggregationCountItemDTOFacetPage2.getContent()) {
            if (((GroupDto) gg.getItem()).getGroupType() == GroupType.USER_GROUP) {
                groupDto = (GroupDto) gg.getItem();
                groupDto.setNumOfFiles(gg.getCount());
                break;
            }
        }

        logger.debug("Update name of group {} with {} files to test123", groupDto.getId(), groupDto.getNumOfFiles());
        groupsApplicationService.updateGroupName(groupDto.getId(), "test123", dataSourceRequest.isWait());

        fileCatService.commitToSolr();
        FilterDescriptor filterDescriptor = new FilterDescriptor("group.name", "test123", KendoFilterConstants.EQ);
        dataSourceRequest.setFilter(filterDescriptor);
        FacetPage<AggregationCountItemDTO<GroupDto>> aggregationCountItemDTOFacetPage = groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
        Assert.assertTrue("chk some groups were returned", aggregationCountItemDTOFacetPage.getContent().size() > 0);
        AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO = aggregationCountItemDTOFacetPage.getContent().get(aggregationCountItemDTOFacetPage.getContent().size() - 1);
        String groupName = groupDtoAggregationCountItemDTO.getItem().getGroupName();
        String parentGroupName = groupDtoAggregationCountItemDTO.getItem().getParentGroupName();
        Assert.assertTrue(groupName.equals("test123") || parentGroupName.equals("test123"));

        dataSourceRequest.setFilter(filterDescriptor);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        Assert.assertEquals("Check that claFiles from solr have a new updated name", "test123", claFileDto.getGroupName());
    }


    @Test
    public void testListRootFoldersInGroup() {
        logger.info("testListRootFoldersInGroup");
        DataSourceRequest dataSourceRequest2 = DataSourceRequest.build(null);
        Page<AggregationCountItemDTO<RootFolderDto>> aggregationCountItemDTOs = groupsApplicationService.listGroupRootFolders(f1.getId(), dataSourceRequest2);
        Assert.assertThat("There should be at least one root folder", aggregationCountItemDTOs.getNumberOfElements(), Matchers.greaterThan(0));
    }

    @Test
    public void testGroupJoinFilter() {
        logger.info("testGroupJoinFilter");
        FacetPage<AggregationCountFolderDto> originalFoldersFileCount = fileTreeApiController.findFoldersFileCount(null);
        DocFolderDto baseFolder = originalFoldersFileCount.getContent().get(originalFoldersFileCount.getContent().size() - 1).getItem();
        logger.debug("Using baseFolder id {} and path {}", baseFolder.getId(), baseFolder.getPath());
        DataSourceRequest dataSourceRequest = getDataSourceRequestJoinFilter(baseFolder.getId());
        FacetPage<AggregationCountItemDTO<GroupDto>> originalGroupsUnderBasedFolder = groupsApplicationService.listGroupsFileCountWithFileFilter(dataSourceRequest);
        logger.info("Got {} groups", originalGroupsUnderBasedFolder.getContent().size());


        logger.info("Now check contained/uncontained groups");
        dataSourceRequest = getDataSourceRequestJoinFilter(baseFolder.getId());
        dataSourceRequest.setGroupJoinFilter(new FilterDescriptor("subFolderId", baseFolder.getId(), "eq"));
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount1WithGroupJoinFilter = groupsApplicationService.listGroupsFileCountWithFileFilter(dataSourceRequest);
        logger.info("Got {} uncontained groups", groupsFileCount1WithGroupJoinFilter.getContent().size());
        for (AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO : groupsFileCount1WithGroupJoinFilter.getContent()) {
            logger.debug("Got uncontained group: {}", groupDtoAggregationCountItemDTO.getItem());
        }

        dataSourceRequest = getDataSourceRequestJoinFilter(baseFolder.getId());
        dataSourceRequest.setGroupJoinFilter(new FilterDescriptor("subFolderId", baseFolder.getId(), "neq"));
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount2WithGroupJoinFilter = groupsApplicationService.listGroupsFileCountWithFileFilter(dataSourceRequest);
        logger.info("Got {} contained groups", groupsFileCount2WithGroupJoinFilter.getContent().size());
        for (AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO : groupsFileCount2WithGroupJoinFilter.getContent()) {
            logger.debug("Got contained group: {}", groupDtoAggregationCountItemDTO.getItem());
        }
        Assert.assertEquals("Verify same number of groups returned", originalGroupsUnderBasedFolder.getNumberOfElements(), groupsFileCount1WithGroupJoinFilter.getNumberOfElements() + groupsFileCount2WithGroupJoinFilter.getNumberOfElements());

        logger.info("Check alternative fully contained filter format");
        dataSourceRequest = getDataSourceRequestJoinFilter(baseFolder.getId());
        FilterDescriptor groupJoinFilter2 = new FilterDescriptor(SolrQueryBuilder.GROUP_FC_PREFIX + "subFolderId", baseFolder.getId(), "neq");
        FilterDescriptor joinedFilter = FilterBuildUtils.createAndFilter(dataSourceRequest.getFilter(), groupJoinFilter2);
        dataSourceRequest.setFilter(joinedFilter);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount1WithGroupJoinFilter2 = groupsApplicationService.listGroupsFileCountWithFileFilter(dataSourceRequest);
        logger.info("Got {} contained groups", groupsFileCount1WithGroupJoinFilter2.getContent().size());
        Assert.assertEquals("Verify same number of groups returned", groupsFileCount2WithGroupJoinFilter.getNumberOfElements(), groupsFileCount1WithGroupJoinFilter2.getNumberOfElements());
    }

    private DataSourceRequest getDataSourceRequestJoinFilter(Long baseFolderId) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("subFolderId", baseFolderId, "eq"));
        return dataSourceRequest;
    }
}
