package com.cla.test.filecluster.service.files.functionalroles;

import com.cla.common.domain.dto.security.*;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.FunctionalRoleApiController;
import com.cla.filecluster.service.security.RoleTemplateService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Category(FastTestCategory.class)
public class FunctionalRoleTests  extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(FunctionalRoleTests.class);

    @Autowired
    private FunctionalRoleApiController functionalRoleApiController;

    @Autowired
    private RoleTemplateService roleTemplateService;

    public FunctionalRoleSummaryInfoDto createFunctionalRoleDto(String name) {
        return createFunctionalRoleDto(name, null);
    }

    public FunctionalRoleSummaryInfoDto createFunctionalRoleDto(String name, RoleTemplateDto template) {
        testStartLog(logger);
        FunctionalRoleDto functionalRoleDto = new FunctionalRoleDto();
        functionalRoleDto.setName(name);
        functionalRoleDto.setDisplayName(name);
        functionalRoleDto.setDescription(name);

        if (template == null) {
            template = new RoleTemplateDto();
            template.setName(name);
            template.setDisplayName(name);
            template.setDescription(name);
            template.setTemplateType(RoleTemplateType.NONE);
        }
        functionalRoleDto.setTemplate(template);

        FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto = new FunctionalRoleSummaryInfoDto();
        functionalRoleSummaryInfoDto.setFunctionalRoleDto(functionalRoleDto);
        return functionalRoleSummaryInfoDto;
    }

    private FunctionalRoleSummaryInfoDto createFunctionalRoleInDB(FunctionalRoleSummaryInfoDto testCreateFunctionalRole) {
        if (testCreateFunctionalRole.getFunctionalRoleDto().getTemplate().getId() == null) {
            RoleTemplateDto r = roleTemplateService.createRoleTemplate(testCreateFunctionalRole.getFunctionalRoleDto().getTemplate());
            testCreateFunctionalRole.getFunctionalRoleDto().setTemplate(r);
        }
        FunctionalRoleSummaryInfoDto updated = functionalRoleApiController.createFunctionalRole(testCreateFunctionalRole);
        return updated;
    }

    @Test
    public void testCreateFunctionalRoleWithNoLdapGroup() {
        testStartLog(logger);
        String name = "testCreateFunctionalRoleWithNoLdapGroupRole";
        FunctionalRoleSummaryInfoDto testCreateFunctionalRole = createFunctionalRoleDto(name);
        FunctionalRoleSummaryInfoDto updated = createFunctionalRoleInDB(testCreateFunctionalRole);
        Assert.assertNotNull("Create functional role should be created ", updated);
        Page<FunctionalRoleSummaryInfoDto> functionalRoleSummaryInfoDtos = functionalRoleApiController.listFunctionalRoleSummaryInfoDto(null);
        List<FunctionalRoleSummaryInfoDto> content = functionalRoleSummaryInfoDtos.getContent();
        Assert.assertThat("Should return function role", content.size(), Matchers.greaterThan(0));
        Optional<FunctionalRoleSummaryInfoDto> first = content.stream().filter(r -> r.getFunctionalRoleDto().getName().equals(name)).findFirst();
        Assert.assertTrue("Should return 1 function role with the created name", first.isPresent());
        Assert.assertTrue("Should not have any assigned LDAP group", first.get().getLdapGroupMappingDtos().isEmpty());
    }

    @Test
    public void testCreateFunctionalRoleWithLdapGroup() {
        testStartLog(logger);
        String name = "testCreateFunctionalRoleWithLdapGroup";
        String managerName = "managerOfFR";
        FunctionalRoleSummaryInfoDto testCreateFunctionalRole = createFunctionalRoleDto(name);
        LdapGroupMappingDto ldapGroupMappingDto = new LdapGroupMappingDto();
        ldapGroupMappingDto.setGroupName("testCreateFunctionalRoleWithLdapGroup");
        testCreateFunctionalRole.getFunctionalRoleDto().setManagerUsername(managerName);
        testCreateFunctionalRole.setLdapGroupMappingDtos(Arrays.asList(ldapGroupMappingDto));
        FunctionalRoleSummaryInfoDto updated = createFunctionalRoleInDB(testCreateFunctionalRole);
        Assert.assertNotNull(updated);
        Assert.assertEquals(name, updated.getFunctionalRoleDto().getName());
        Assert.assertEquals(1, updated.getLdapGroupMappingDtos().size());

        Page<FunctionalRoleSummaryInfoDto> functionalRoleSummaryInfoDtos = functionalRoleApiController.listFunctionalRoleSummaryInfoDto(null);
        List<FunctionalRoleSummaryInfoDto> content = functionalRoleSummaryInfoDtos.getContent();
        List<FunctionalRoleSummaryInfoDto> theSummaryInfo = content.stream().filter(s -> s.getFunctionalRoleDto().getName().equals(name)).collect(Collectors.toList());
        Assert.assertEquals("Added functionalRole should be returned in list",1, theSummaryInfo.size());
        Assert.assertEquals( "Manager userName should be added correctly to functionalRole", managerName, theSummaryInfo.get(0).getFunctionalRoleDto().getManagerUsername());
        List<LdapGroupMappingDto> mapping = theSummaryInfo.get(0).getLdapGroupMappingDtos().stream().filter(m -> m.getGroupName().equals(ldapGroupMappingDto.getGroupName())).collect(Collectors.toList());
        Assert.assertEquals( "Mapping should be added for functionalRole", mapping.get(0).getGroupName(),ldapGroupMappingDto.getGroupName());

    }

    @Test
    public void testUpdateFunctionalRoleWithLdapGroup() {
        testStartLog(logger);
        FunctionalRoleSummaryInfoDto testCreateFunctionalRole = createFunctionalRoleDto("testUpdateFunctionalRoleWithLdapGroup1");
        LdapGroupMappingDto ldapGroupMappingDto2 = new LdapGroupMappingDto();
        ldapGroupMappingDto2.setGroupName("testUpdateFunctionalRoleWithLdapGroup1");
        testCreateFunctionalRole.getFunctionalRoleDto().setManagerUsername("managerName");
        testCreateFunctionalRole.setLdapGroupMappingDtos(Arrays.asList(ldapGroupMappingDto2));
        FunctionalRoleSummaryInfoDto updated1 = createFunctionalRoleInDB(testCreateFunctionalRole);
        Assert.assertNotNull(updated1);
        String updatedManager="UpdatedMng";
        String updatedName="updatedName";

        updated1.getFunctionalRoleDto().setName(updatedName);
        updated1.getFunctionalRoleDto().setDisplayName(updatedName);
        updated1.getFunctionalRoleDto().setManagerUsername(updatedManager);
        FunctionalRoleSummaryInfoDto updated = functionalRoleApiController.updateFunctionalRole(updated1.getFunctionalRoleDto().getId(),updated1);
        Assert.assertEquals("Update functionalRole should with name",updatedName,updated.getFunctionalRoleDto().getName());
        Assert.assertEquals("Update functionalRole should with displayed name",updatedName,updated.getFunctionalRoleDto().getDisplayName());
        Assert.assertEquals("Update functionalRole should with ManagerUsername",updatedManager,updated.getFunctionalRoleDto().getManagerUsername());

        String ldapGroupName = "testUpdateFunctionalRoleWithLdapGroup2";
        LdapGroupMappingDto ldapGroupMappingDto = new LdapGroupMappingDto();
        ldapGroupMappingDto.setGroupName("testUpdateFunctionalRoleWithLdapGroup2");
        updated.setLdapGroupMappingDtos(Arrays.asList(ldapGroupMappingDto));
        FunctionalRoleSummaryInfoDto updated2 = functionalRoleApiController.updateFunctionalRole(updated.getFunctionalRoleDto().getId(),updated);
        Assert.assertEquals("Update functionalRole mapping ",1,updated.getLdapGroupMappingDtos().size());
        Assert.assertEquals("Update functionalRole mapping ldapGroupName",ldapGroupName,updated.getLdapGroupMappingDtos().get(0).getGroupName());
    }

    @Test
    public void testCreateFuncRoleSameNameDeleted() {
        testStartLog(logger);
        FunctionalRoleSummaryInfoDto funcRoleDto = createFunctionalRoleInDB(createFunctionalRoleDto("testCreateFuncRoleSameNameDeleted"));
        Assert.assertNotNull(funcRoleDto);
        functionalRoleApiController.deleteFunctionalRole(funcRoleDto.getFunctionalRoleDto().getId());
        FunctionalRoleSummaryInfoDto funcRoleDto2 = createFunctionalRoleInDB(createFunctionalRoleDto("testCreateFuncRoleSameNameDeleted", funcRoleDto.getFunctionalRoleDto().getTemplate()));
        Assert.assertNotNull(funcRoleDto2);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateFuncRoleSameNameFail() {
        testStartLog(logger);
        FunctionalRoleSummaryInfoDto funcRoleDto = createFunctionalRoleInDB(createFunctionalRoleDto("testCreateFuncRoleSameNameFail"));
        Assert.assertNotNull(funcRoleDto);
        createFunctionalRoleInDB(createFunctionalRoleDto("testCreateFuncRoleSameNameFail", funcRoleDto.getFunctionalRoleDto().getTemplate()));
    }

}
