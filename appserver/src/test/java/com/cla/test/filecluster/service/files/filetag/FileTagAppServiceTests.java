package com.cla.test.filecluster.service.files.filetag;

import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageImpl;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * Created by uri on 25/08/2015.
 */
@Category(FastTestCategory.class)
public class FileTagAppServiceTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(FileTagAppServiceTests.class);

    @Resource
    private PerformanceTestService performanceTestService;

    @Resource
    private FileTaggingAppService fileTaggingAppService;

    @Resource
    private FilesApplicationService filesApplicationService;

    private RootFolderDto rootFolderDto;

    @Before
    public void scanBaseDir() {
        rootFolderDto = rootFolderDtoAnalyze;
    }

    @Test
    public void testCreateFileTagIfNeeded() {
        List<FileTagTypeDto> fileTagTypes = fileTaggingAppService.getFileTagTypes(false, null);
        FileTagTypeDto fileTagType = fileTagTypes.get(0);
        FileTag fileTagIfNeeded = fileTaggingAppService.createFileTagIfNeeded("testCreateFileTagIfNeeded", "testCreateFileTagIfNeeded", fileTagType.getId(), FileTagAction.MANUAL);
        FileTag fileTagIfNeeded2 = fileTaggingAppService.createFileTagIfNeeded("testCreateFileTagIfNeeded", "testCreateFileTagIfNeeded", fileTagType.getId(), FileTagAction.MANUAL);
        Assert.assertEquals(fileTagIfNeeded.getId(), fileTagIfNeeded2.getId());
        fileTagTypes = fileTaggingAppService.getFileTagTypes(false, null);
        for (FileTagTypeDto tagType : fileTagTypes) {
            if (tagType.getId() == fileTagType.getId()) {
                Assert.assertThat("TagType tag items count should be > 0", tagType.getTagItemsCount(), Matchers.greaterThan(0));
            }
        }

    }

    @Test
    public void testMismatchFileTagFilterOnFiles() {
        logger.info("testMismatchFileTagFilter");
        List<FileTagTypeDto> fileTagTypes = fileTaggingAppService.getFileTagTypes(false, null);
        FileTagTypeDto fileTagType = fileTagTypes.get(0);
        FileTag firstTag = fileTaggingAppService.createFileTagIfNeeded("testMismtachFileTagFilter1", "testCreateFileTagIfNeeded1", fileTagType.getId(), FileTagAction.MANUAL);
        FileTag secondTag = fileTaggingAppService.createFileTagIfNeeded("testMismtachFileTagFilter2", "testCreateFileTagIfNeeded2", fileTagType.getId(), FileTagAction.MANUAL);
        logger.info("tag a file with the two tags {} and {} on tag type {}", firstTag, secondTag, fileTagType);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(DataSourceRequest.build(null));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.info("tag the file {} with the two tags", claFileDto.getFileName());
        Long taggedFileId = claFileDto.getId();
        fileTaggingAppService.addFileTagsToFile(taggedFileId,
                FileTagService.convertFileTagsToDto(Lists.newArrayList(firstTag, secondTag))
                ,SolrCommitType.HARD, null);
        logger.info("look for files with tag mismatch");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("file.tagTypeMismatch", String.valueOf(fileTagType.getId()), "EQ"));
        PageImpl<ClaFileDto> result = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("There should be at least one file", result.getNumberOfElements(), Matchers.greaterThan(0));
        for (ClaFileDto fileDto : result) {
            logger.debug("Got fileDto {} with tag mismatch", fileDto);
            if (Objects.equals(taggedFileId, fileDto.getId())) {
                logger.info("Got the required file in the results");
                return;
            }
        }
        throw new RuntimeException("testMismatchFileTagFilter - didn't found the required file " + taggedFileId + " in the mismatch filter");
    }

//    private void verifyFileTag(FileTagDto createdFileTagDto, Collection<FileTagDto> fileTagsForFile) {
//        StringBuilder debugInfo = new StringBuilder();
//        for (FileTagDto fileTagDto : fileTagsForFile) {
//            if (fileTagDto.getName().equals(createdFileTagDto.getName())) {
//                return;
//            }
//            debugInfo.append(fileTagDto.getName()).append(",");
//        }
//        throw new RuntimeException("Failed to find fileTag by name "+createdFileTagDto.getName()+" in list of file tags: "+debugInfo);
//    }

//    private void verifyFileTagRemoved(FileTagDto createdFileTagDto, Collection<FileTagDto> fileTagsForFile) {
//        StringBuilder debugInfo = new StringBuilder();
//        for (FileTagDto fileTagDto : fileTagsForFile) {
//            if (fileTagDto.getName().equals(createdFileTagDto.getName())) {
//                throw new RuntimeException("verifyFileTagRemoved - Found fileTag by name "+createdFileTagDto.getName()+" in list of file tags: "+debugInfo);
//            }
//            debugInfo.append(fileTagDto.getName()).append(",");
//        }
//        logger.debug(debugInfo.toString());
//    }

}
