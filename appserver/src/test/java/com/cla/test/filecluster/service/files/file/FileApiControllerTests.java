package com.cla.test.filecluster.service.files.file;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDetailsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.FileAclDataDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.specification.FilterBuildUtils;
import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(FastTestCategory.class)
public class FileApiControllerTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(FileApiControllerTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private SolrClientFactory solrClientFactory;

    @Test
    public void testFileListGroupMillionEntries() {
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        GroupDto groupDto = groupDtos.getContent().get(0);
        Page<ClaFileDto> claFileDtos = filesApiController.ListFilesByGroup(null, groupDto.getId());
        logger.debug("Got {} files in the first group", claFileDtos.getNumberOfElements());
    }

    @Test
    public void testFileTypeCount() {
        FacetPage<AggregationCountItemDTO> fileTypeCounts = filesApiController.findFileTypeCounts(null);
        for (AggregationCountItemDTO fileType : fileTypeCounts.getContent()) {
            logger.debug("Got fileType: {}",fileType);
        }
        //There should be at least one file
        Assert.assertThat("There should be at least one file type", fileTypeCounts.getContent().size(), Matchers.greaterThan(0));
    }

    @Test
    public void testFileAccessHistogram() {
        List<AggregationCountItemDTO<String>> accessHistogram = filesApiController.createAccessHistogram(null);
        //There should be at least one file
        Assert.assertThat("There should be at least histogram value", accessHistogram.size(), Matchers.greaterThan(0));
        for (AggregationCountItemDTO<String> stringAggregationCountItemDTO : accessHistogram) {
            logger.debug("{}: {}",stringAggregationCountItemDTO.getItem(),stringAggregationCountItemDTO.getCount());
        }
    }

    @Test
    public void testFilteredFileAccessHistogram() {
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        String groupId = groupsFileCount.getContent().get(0).getItem().getId();
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId",groupId,"eq"));
        List<AggregationCountItemDTO<String>> accessHistogram = filesApplicationService.createAccessHistogram(dataSourceRequest);
        //There should be at least one file
        logger.debug("Got {} results",accessHistogram.size());
        Assert.assertThat("There should be at least histogram value", accessHistogram.size(), Matchers.greaterThan(0));
        for (AggregationCountItemDTO<String> stringAggregationCountItemDTO : accessHistogram) {
            logger.debug("{}: {}",stringAggregationCountItemDTO.getItem(),stringAggregationCountItemDTO.getCount());
        }

    }

    @Test
    public void testTagMismatch() {
        logger.info("testTagMismatch");
        //http://52.25.111.69:9000/api/file/dn?baseFilterValue=39635668-4afe-4d5e-bd2c-34c26f08c378&baseFilterField=groupId&take=30&skip=0&page=1&pageSize=30&filter=%7b%22logic%22:%22and%22,%22filters%22:%5b%7b%22field%22:%22file.tagTypeMismatch%22,%22operator%22:%22eq%22,%22value%22:1%7d%5d%7d
        //http://52.25.187.121:9000/api/file/dn?baseFilterValue=82953a45-d57d-40ad-aaf8-d2b0350db832&baseFilterField=groupId&take=30&skip=0&page=1&pageSize=30&filter=%7B%22logic%22%3A%22and%22%2C%22filters%22%3A%5B%7B%22field%22%3A%22file.tagTypeMismatch%22%2C%22operator%22%3A%22eq%22%2C%22value%22%3A1%7D%5D%7D
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("file.tagTypeMismatch",1,"eq"));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.debug("Got {} mismatch files",claFileDtos.getNumberOfElements());
    }

    @Test
    public void testTagMismatchWithFilter() {
        logger.info("testTagMismatchWithFilter");
        //http://52.25.111.69:9000/api/file/dn?baseFilterValue=39635668-4afe-4d5e-bd2c-34c26f08c378&baseFilterField=groupId&take=30&skip=0&page=1&pageSize=30&filter=%7b%22logic%22:%22and%22,%22filters%22:%5b%7b%22field%22:%22file.tagTypeMismatch%22,%22operator%22:%22eq%22,%22value%22:1%7d%5d%7d
        //http://52.25.187.121:9000/api/file/dn?baseFilterValue=82953a45-d57d-40ad-aaf8-d2b0350db832&baseFilterField=groupId&take=30&skip=0&page=1&pageSize=30&filter=%7B%22logic%22%3A%22and%22%2C%22filters%22%3A%5B%7B%22field%22%3A%22file.tagTypeMismatch%22%2C%22operator%22%3A%22eq%22%2C%22value%22%3A1%7D%5D%7D
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor tagTypeMismatch = performanceTestService.createConcreteFilter("file.tagTypeMismatch", 1, "eq");
        FilterDescriptor typeFilter = performanceTestService.createConcreteFilter("type", FileType.EXCEL, "eq");
        dataSourceRequest.setFilter(performanceTestService.createJoinFilter("AND",typeFilter,tagTypeMismatch));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.debug("Got {} mismatch files",claFileDtos.getNumberOfElements());
    }

    @Test
    public void testGetFileDetails() {
        logger.info("testGetFileDetails");
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        for (ClaFileDto claFileDto : filesDeNormalized.getContent()) {
            if (claFileDto.getContentId() == null) {
                logger.info("File {} got no content (empty). Skipping", claFileDto);
            } else {
                logger.info("Testing the details of the file {}", claFileDto);
                ClaFileDetailsDto fileDetailsById = null;
                try {
                    fileDetailsById = filesApiController.findFileDetailsById(claFileDto.getId());
                } catch (Exception e) {
                    logger.error("findFileDetailsById({}) has failed.", claFileDto.getId());
                    continue;
                }
                FileAclDataDto aclDataDto = fileDetailsById.getAclDataDto();
                Assert.assertNotNull("AclDataDto should not be null", aclDataDto);
                List<String> aclReadData = aclDataDto.getAclReadData();
                if (aclReadData != null) {
                    Assert.assertThat("There should be acl read data elements", aclReadData.size(), Matchers.greaterThan(0));
                }

                ClaFileState testedFileState = fileDetailsById.getFileDto().getState();
                FileType testedFileType = fileDetailsById.getFileDto().getType();
                boolean emptyFile =  fileDetailsById.getFileDto().getFsFileSize() == null ||  fileDetailsById.getFileDto().getFsFileSize() == 0;
                ClaFileState expectedFileState = (!emptyFile && (testedFileType.equals(FileType.WORD) || testedFileType.equals(FileType.EXCEL) || testedFileType.equals(FileType.PDF))) ? ClaFileState.ANALYSED : ClaFileState.INGESTED;
                Assert.assertEquals("ClaFile "+fileDetailsById.getFileDto()+" should have been in "+ expectedFileState +" state",
                        expectedFileState, testedFileState);
            }
        }
    }

    @Test
    public void testGetFileCount() {
        logger.info("testGetFileCount");
        Map<String, String> params = new HashMap<>();
        //params.put("take","0"); if we do this, we set rows to zero...
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(params);
        Assert.assertThat(filesDeNormalized.getTotalElements(),Matchers.greaterThan(0L));
    }

    @Test
    public void testFileSize() {
        logger.debug("testFileSize");
        Map<String, String> params = new HashMap<>();
        params.put("take","0");
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(params);
        for (ClaFileDto claFileDto : filesDeNormalized) {
            Assert.assertThat("Verify file "+claFileDto+" has fileSize", claFileDto.getFsFileSize(),Matchers.greaterThan(0L));

        }

    }

    @Test
    public void testCollapseDuplicates() {
        logger.debug(this.testName.getMethodName());

        Map<String, String> params = new HashMap<>();
        params.put("collapseDuplicates","true");
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(params);
        Assert.assertThat("There should be at least one result",filesDeNormalized.getContent().size(),Matchers.greaterThan(0));
    }

    @Test
    public void testFilesByContentId() {
        logger.info("testFilesByContent");
        List<ClaFileDto> content = filesApiController.findFilesDeNormalized(null).getContent();
        ClaFileDto claFileDto = content.get(0);
        List<ClaFileDto> claFileDtos = filesApiController.listFilesOfSpecificContentId(claFileDto.getContentId());
        Assert.assertThat("There should be at least one file in the filesByContentId list",claFileDtos.size(),Matchers.greaterThan(0));
    }

    @Test
    public void testSearchSingleContent() {
        logger.info("testSearchSingleContent");
        if (solrClientFactory.isSolrCloud()) {
            //TODO 2.0 - fix the content feature in Cloud mode
            logger.warn("Content search is not working in Solr cloud");
            return;
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setContentFilter("California");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("There should be at least one result",claFileDtos.getContent().size(),Matchers.greaterThan(0));
    }

    @Test
    public void testSearchMultipleContents() {
        logger.info("testSearchMultipleContents");
        if (solrClientFactory.isSolrCloud()) {
            //TODO 2.0 - fix the content feature in Cloud mode
            logger.warn("Content search is not working in Solr cloud");
            return;
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor filter1 = new FilterDescriptor(FilterDescriptor.CONTENT_FILTER,"California","eq");
        FilterDescriptor filter2 = new FilterDescriptor(FilterDescriptor.CONTENT_FILTER,"Staff","eq");
        FilterDescriptor baseFilter = FilterBuildUtils.createAndFilter(filter1,filter2);
        dataSourceRequest.setFilter(baseFilter);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("There should be at least one result",claFileDtos.getContent().size(),Matchers.greaterThan(0));
    }


    @Ignore
    @Test
    public void testFilterUnassociatedTags() {
        // Should be uncommendted after resolving Jira#____
        final String path = PerformanceTestService.ANALYZED_ROOT_FOLDER_PATH;
        logger.debug("testFilterUnassociatedTags");

        Map<String, String> params = new HashMap<>();
        FilterDescriptor filterDescriptor = new FilterDescriptor("file.tags","*","neq");
        String filterString = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);
        params.put("filter",filterString);
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(params);
        logger.debug("Got back {} files",filesDeNormalized.getContent().size());
        int counter = filesApiController.getNoTagsSolrMissMatch();
        Assert.assertEquals(0, counter);
    }

    @Test
    public void testPdfFile() {
        testStartLog(logger);
        Map<String, String> filterParams = performanceTestService.createFilterParams("extension", "pdf");
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(filterParams);
        logger.debug("Got back {} files",filesDeNormalized.getContent().size());
        for (ClaFileDto claFileDto : filesDeNormalized.getContent()) {
            logger.debug("Got back file {}",claFileDto);
        }
        Assert.assertThat("There should be at least 1 pdf file",filesDeNormalized.getContent().size(),Matchers.greaterThan(0));
    }
}
