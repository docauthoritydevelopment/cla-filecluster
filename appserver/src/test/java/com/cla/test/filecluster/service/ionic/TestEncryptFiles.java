package com.cla.test.filecluster.service.ionic;

import com.cla.test.filecluster.ClaTestUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.ionicsecurity.sdk.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by uri on 15/09/2016.
 * TODO
 */
@Ignore
@Category(UnitTestCategory.class)
public class TestEncryptFiles {

    private static final String ionicSamplesPath = "./src/test/resources/files/ionic/";
    public static final String unencrypted_doc_sample = ionicSamplesPath + "unencrypted doc.doc";
    public static final String encrypted_pdf_sample = ionicSamplesPath + "encrypted pdf.pdf";
    public static final String encrypted_docx_sample = ionicSamplesPath + "encrypted docx.docx";
    public static final String encrypted_excelX_sample = ionicSamplesPath + "encrypted excelX.xlsx";

    AgentSdk agentSdk;

    @Before
    public void before() throws Exception {
        String absolutePath = new File(".").getCanonicalPath() + "\\build\\nativeLibs\\";
        if (!new File(absolutePath).exists()) {
            throw new RuntimeException("Missing nativeLibs folder - run a build first");
        }
        ClaTestUtils.addLibraryPath(absolutePath);
        agentSdk = AgentSdk.initialize(null);// Must be called before any Ionic operations
    }

    @Test
    public void testEncryptString() {
        CryptoRng ionicRng = new com.ionicsecurity.sdk.CryptoRng();
        byte aesKeyBytes[] = new byte[32];
        ionicRng.rand(aesKeyBytes);

// Apply the key to an AES Cipher
        com.ionicsecurity.sdk.AesCtrCipher aesCipher = new com.ionicsecurity.sdk.AesCtrCipher();
        aesCipher.setKey(aesKeyBytes);

// Encrypt a sample string
        java.lang.String stringToEncrypt = "Demo Text";
        java.lang.String encryptedString = aesCipher.encryptToBase64(stringToEncrypt);

        System.out.println("Plain Text: " + stringToEncrypt);
        System.out.println("Ionic Encrypted Text: " + encryptedString);
    }

    @Test
    public void testDecryptFile() throws IOException {
        Agent agent = new Agent();
        agent.initialize();
        AutoFileCipher autoFileCipher = new AutoFileCipher(agent);
        byte[] bytes = Files.readAllBytes(Paths.get(encrypted_excelX_sample));
        FileCryptoFileInfo fileInfo = FileCrypto.getFileInfo(bytes);
        System.out.println("File is encrypted by key: "+fileInfo.getKeyId());
        byte[] decrypt = autoFileCipher.decrypt(bytes);

    }

    @Test
    public void testCheckIfFileIsEncrypted() throws IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(encrypted_excelX_sample));
        FileCryptoFileInfo fileInfo = FileCrypto.getFileInfo(bytes);
        Assert.assertTrue(fileInfo.isEncrypted());

        bytes = Files.readAllBytes(Paths.get(encrypted_pdf_sample));
        fileInfo = FileCrypto.getFileInfo(bytes);
        Assert.assertTrue(fileInfo.isEncrypted());

        bytes = Files.readAllBytes(Paths.get(encrypted_docx_sample));
        fileInfo = FileCrypto.getFileInfo(bytes);
        Assert.assertTrue(fileInfo.isEncrypted());

        bytes = Files.readAllBytes(Paths.get(unencrypted_doc_sample));
        fileInfo = FileCrypto.getFileInfo(bytes);
        Assert.assertFalse(fileInfo.isEncrypted());
    }
}
