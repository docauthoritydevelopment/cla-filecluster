package com.cla.test.filecluster.service.files.executorpool;

import com.cla.filecluster.util.executors.ThreadPoolBlockingExecutor;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by uri on 16/12/2015.
 */
public class ThreadPoolBlockingExecutorTests {

    private Logger logger = LoggerFactory.getLogger(ThreadPoolBlockingExecutorTests .class);

    @Test
    @Ignore
    public void testBlockingExecutor() {
        logger.info("testBlockingExecutor");
        int blockingExecutorPoolSize = 10;
        int blockingExecutorQueueSize = 20;
        long idleTimeBeforeShutdown = 1;
        ThreadPoolBlockingExecutor threadPoolBlockingExecutor = new ThreadPoolBlockingExecutor(
                        "test",
                        blockingExecutorPoolSize,
                        blockingExecutorQueueSize,
                        idleTimeBeforeShutdown,true);

        logger.info("Push 100 tasks into the executor");
        for (int i=0; i< 100; i++) {
            logger.debug("push {} into the executor",i);
            int value = i;
            threadPoolBlockingExecutor.execute(() -> this.analysisSimulator(value));
        }
        logger.info("Pushed 100 tasks into the executor");
        threadPoolBlockingExecutor.getActiveCount();
        threadPoolBlockingExecutor.blockUntilProcessingFinished();
        logger.info("DONE");
    }

    public void analysisSimulator(int value) {
        try {
            Thread.sleep(100);
            logger.debug("analysisSimulator start: {}",value);
            Thread.sleep(3000);
            logger.debug("analysisSimulator end: {}",value);
        } catch (InterruptedException e) {
            logger.error(e.getMessage(),e);
        }
    }
}
