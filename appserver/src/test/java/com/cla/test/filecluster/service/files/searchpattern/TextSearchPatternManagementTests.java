package com.cla.test.filecluster.service.files.searchpattern;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.repository.file.TextSearchPatternImplementationRepository;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.*;

/**
 * Created by uri on 26/06/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class TextSearchPatternManagementTests {

    private Logger logger = LoggerFactory.getLogger(TextSearchPatternManagementTests.class);

    @InjectMocks
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Mock
    private MediaProcessorCommService mediaProcessorCommService;

    @Mock
    private TextSearchPatternImplementationRepository textSearchPatternImplementationRepository;

    private static  Map<Integer, TextSearchPatternImpl> textSearchPatternMap = new ConcurrentHashMap<>();

    @BeforeClass
    public static void initRulse() {
        try {
            textSearchPatternMap.put(2, new TextSearchPatternImpl(
                    2, "email-pattern",  "\\b[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})\\b", true));
            textSearchPatternMap.put(11, new TextSearchPatternImpl(
                    11, "ipv4-addr",  "(?:[^.\\-\\w])(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?![\\-\\.\\w])", true));
            textSearchPatternMap.put(12, new TextSearchPatternImpl(
                    12, "ipv6-addr", "(?<![:\\.\\w])(?:[A-Fa-f0-9]{1,4}:){7}[A-Fa-f0-9]{1,4}(?![:\\.\\w])", true));
            textSearchPatternMap.put(7, TextSearchPatternImplementationRepository.getTextSearchPatternImpl(
                    "com.cla.common.domain.dto.filesearchpatterns.SSNTextSearchPattern", 7, "ssn", true, null, true));
            textSearchPatternMap.put(4, new TextSearchPatternImpl(
                    4, "uk-driving-lic", "\\b[A-Z0-9]{5}\\d[0156]\\d([0][1-9]|[12]\\d|3[01])\\d[A-Z0-9]{3}[A-Z]{2}\\b", true));
            textSearchPatternMap.put(3, TextSearchPatternImplementationRepository.getTextSearchPatternImpl(
                    "com.cla.common.domain.dto.filesearchpatterns.UkNationalInsuranceTextSearchPattern", 3, "uk-ni", true, null, true));
            textSearchPatternMap.put(5, new TextSearchPatternImpl(
                    5, "uk-plate",
                    "(\\b(([ACDGHKLNPSWY][A-HJ-PR-Y]|[BEMORVX][A-Y]|F[A-HJ-NPR-TV-Y])(0[2-9]|1[0-7]|5[1-9]|6[0-7])[ ]?[A-HJ-PR-Z][A-HJ-PR-Z][A-HJ-PR-Z])\\b)|(\\b([A-HJ-NP-TV-Y]\\d{1,3}[ ]?[A-Z][A-HJ-PR-Y][A-HJ-PR-Y])\\b)|(\\b([A-Z][A-HJ-PR-Y][A-HJ-PR-Y][ ]?\\d{1,3}[A-HJ-NP-TV-Y])\\b)|(\\b((QNI|[A-Z]([A-HJ-PR-Y]Z|I[ABGJLW]|[JOUX]I))[ ]?\\d{1,4})\\b)", true));

        } catch (Exception e) {
            System.out.println("failed test "+ e);
            fail();
        }
    }

    @Before
    public void init() {
        ReflectionTestUtils.setField(claFileSearchPatternService, "patternSearchActive", true);
        claFileSearchPatternService.init();
        Mockito.when(textSearchPatternImplementationRepository.findAll()).thenReturn(textSearchPatternMap.values());
    }


    @Test
    public void testSearchPatternCounting(){
        logger.info("TextSearchPatternManagementTests:testSearchPatternCounting");
        String fileText = "333456789 aaa yanai@tikalk.com 123-45-6789 ccc yanaif@gmail.com 3232 5199999999999999";
        final Set<SearchPatternCountDto> searchPatternsCounting = claFileSearchPatternService.getSearchPatternsCounting(fileText);
        logger.debug(searchPatternsCounting.toString());
        assertFalse("There should be at least one search pattern returned",searchPatternsCounting.isEmpty());
        int i = 0;
        for (SearchPatternCountDto s : searchPatternsCounting) {
            if (s.getPatternName().equals("ssn")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else if (s.getPatternName().equals("email-pattern")) {
                i++;
                assertTrue(s.getPatternCount() == 2);
            } else {
                fail("there shouldnt be anything else");
            }
        }
        assertTrue(i == 2);
    }

    @Test
    public void testSearchPatternCountingWithIP(){
        logger.info("TextSearchPatternManagementTests:testSearchPatternCountingWithIP");
        String fileText = "333456789 aaa yanai@tikalk.com 123-45-6789 ccc yanaif@gmail.com 3232 5199999999999999 123.154.034.054";
        final Set<SearchPatternCountDto> searchPatternsCounting = claFileSearchPatternService.getSearchPatternsCounting(fileText);
        logger.debug(searchPatternsCounting.toString());
        assertFalse("There should be at least one search pattern returned",searchPatternsCounting.isEmpty());

        int i = 0;
        for (SearchPatternCountDto s : searchPatternsCounting) {
            if (s.getPatternName().equals("ipv4-addr")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else if (s.getPatternName().equals("ssn")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else if (s.getPatternName().equals("email-pattern")) {
                i++;
                assertTrue(s.getPatternCount() == 2);
            } else {
                fail("there shouldnt be anything else");
            }
        }
        assertTrue(i == 3);
    }

    @Test
    public void testSearchPatternCountingWithUK(){
        logger.info("TextSearchPatternManagementTests:testSearchPatternCountingWithUK");
        String fileText = "asd RR 12 34 56 fghfhf BG 12 34 56 C  dfgdfgdg 2001:0db8:85a3:0000:0000:8a2e:0370:7334 RR 12 34 56 C MORGA657054SM9IJ";
        final Set<SearchPatternCountDto> searchPatternsCounting = claFileSearchPatternService.getSearchPatternsCounting(fileText);
        logger.debug(searchPatternsCounting.toString());
        assertFalse("There should be at least one search pattern returned",searchPatternsCounting.isEmpty());

        int i = 0;
        for (SearchPatternCountDto s : searchPatternsCounting) {
            if (s.getPatternName().equals("ipv6-addr")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else if (s.getPatternName().equals("uk-driving-lic")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else if (s.getPatternName().equals("uk-ni")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else {
                fail("there shouldnt be anything else");
            }
        }
        assertTrue(i == 3);
    }

    @Test
    public void testSearchPatternEmail(){
        logger.info("TextSearchPatternManagementTests:testSearchPatternCountingWithUK");
        final Set<SearchPatternCountDto> searchPatternsCounting =
                claFileSearchPatternService.getSearchPatternsCounting("333456789 aaa yanai@tikalk.com 123-45-6789 ccc yanaif@gmail.com 3232 5199999999999999");
        logger.debug(searchPatternsCounting.toString());
        assertFalse(searchPatternsCounting.isEmpty());
        int i = 0;
        for (SearchPatternCountDto s : searchPatternsCounting) {
            if (s.getPatternName().equals("ssn")) {
                i++;
                assertTrue(s.getPatternCount() == 1);
            } else if (s.getPatternName().equals("email-pattern")) {
                i++;
                assertTrue(s.getPatternCount() == 2);
            } else {
                fail("there shouldnt be anything else");
            }
        }
        assertTrue(i == 2);
    }

}