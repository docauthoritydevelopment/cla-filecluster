package com.cla.test.filecluster.service.files.usergroups;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagTypeAndTagsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.NewJoinedGroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.api.FileTagApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.upgrade.UpgradeSchemaService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.util.*;
import java.util.stream.Collectors;

@Category(FastTestCategory.class)
@Ignore
public class TestUserGroups extends ClaBaseTests {

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private FileTagApiController fileTagController;

    @Autowired
    private UpgradeSchemaService upgradeSchemaService;

    @Autowired
    private PerformanceTestService performanceTestService;

    private static final Logger logger = LoggerFactory.getLogger(TestUserGroups.class);

    private static RootFolderDto rootFolderDto;

    private static int testNum;

    private static int testRan = 0;

    @Before
    public void scanBaseDir() {
        if (testRan == 0) {
            testNum = getUnitTestNumber();
            rootFolderDto = rootFolderDtoAnalyze;
        }
        ++testRan;
    }



    @Test
    public void testUserGroupsExist() {
        testStartLog(logger);
        Map<String, String> params = new HashMap<>();
        params.put(DataSourceRequest.TAKE, "1000");
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(params);
        List<ClaFileDto> filesWithGroup = filesDeNormalized.getContent().stream().
                filter(f -> f.getAnalysisGroupId() != null).collect(Collectors.toList());
        for (ClaFileDto claFileDto : filesWithGroup) {
            Assert.assertNotNull("Verify file " + claFileDto + " has user group id", claFileDto.getGroupId());
        }
    }

    @Test
    public void testCreateJoinedGroup() {

        testStartLog(logger);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        logger.debug("Got {} groups", groupsFileCount.getContent().size());
        GroupDto firstGroup = groupsFileCount.getContent().get(0).getItem();
        GroupDto secondGroup = groupsFileCount.getContent().get(1).getItem();
        logger.debug("Join groups {} and {} to a single userGroup", firstGroup, secondGroup);
        NewJoinedGroupDto groupDto = new NewJoinedGroupDto();
        groupDto.setGroupName("testCreateJoinedGroup");
        groupDto.setChildrenGroupIds(Lists.newArrayList(firstGroup.getId(), secondGroup.getId()));
        logger.debug("Create Joined groups");
        GroupDetailsDto groupDetailsDto = groupsApiController.createJoinedGroup(groupDto, performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        GroupDto joinedGroupDto = groupDetailsDto.getGroupDto();
        Assert.assertNotNull(joinedGroupDto.getId());
        Assert.assertEquals(groupDto.getGroupName(), joinedGroupDto.getGroupName());
        Assert.assertNotNull(groupDetailsDto.getChildrenGroups());
        Assert.assertThat(groupDetailsDto.getChildrenGroups().size(), Matchers.greaterThan(0));

        GroupDetailsDto groupDetailsDto1 = groupsApiController.findGroupById(joinedGroupDto.getId());
        List<String> subGroupIds = groupDetailsDto1.getChildrenGroups().stream().map(GroupDto::getId).collect(Collectors.toList());
        Assert.assertTrue(subGroupIds.contains(firstGroup.getId()));
        Assert.assertTrue(subGroupIds.contains(secondGroup.getId()));
        Assert.assertEquals(firstGroup.getNumOfFiles() + secondGroup.getNumOfFiles(), groupDetailsDto1.getGroupDto().getNumOfFiles());
//        fileTagController.applyTagsToSolr(true,true,1000);
        logger.debug("Delete joined group");
        groupsApiController.deleteJoinedGroup(groupDetailsDto1.getGroupDto().getId(), performanceTestService.createWaitParams());
//        fileTagController.applyTagsToSolr(true,true,1000);
    }

    @Test
    public void testRenameGroup() {
        logger.info(testName.getMethodName());
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        logger.debug("Got {} groups", groupsFileCount.getContent().size());
        GroupDto firstGroup = groupsFileCount.getContent().get(0).getItem();
        GroupDto secondGroup = groupsFileCount.getContent().get(1).getItem();
        logger.debug("Join groups {} and {} to a single userGroup", firstGroup, secondGroup);
        NewJoinedGroupDto groupDto = new NewJoinedGroupDto();
        groupDto.setGroupName("testRenameGroup");
        groupDto.setChildrenGroupIds(Lists.newArrayList(firstGroup.getId(), secondGroup.getId()));
        logger.debug("Create Joined groups");
        GroupDetailsDto groupDetailsDto = groupsApiController.createJoinedGroup(groupDto, performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        GroupDto joinedGroup = groupDetailsDto.getGroupDto();
        joinedGroup.setGroupName("testRenameGroup-renamed");
        logger.info("Update group name");
        groupsApiController.updateGroupName(joinedGroup, performanceTestService.createWaitParams());
        logger.debug("Check that group name changed");
        fileTagController.applyTagsToSolr(true, true, 1000);
        GroupDetailsDto groupDetailsDto1 = groupsApiController.findGroupById(joinedGroup.getId());
        Assert.assertEquals("testRenameGroup-renamed", groupDetailsDto1.getGroupDto().getGroupName());
        Map<String, String> params = Maps.newHashMap();
        params.put(DataSourceRequest.BASE_FILTER_FIELD, "groupName");
        params.put(DataSourceRequest.BASE_FILTER_VALUE, "testRenameGroup-renamed");
        groupsFileCount = groupsApiController.findGroupsFileCount(params);
        for (AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO : groupsFileCount.getContent()) {
            logger.debug("Group {} with {} files", groupDtoAggregationCountItemDTO.getItem(), groupDtoAggregationCountItemDTO.getCount());
        }

        Assert.assertEquals(1, groupsFileCount.getContent().size());
        logger.debug("Delete joined group");
        groupsApiController.deleteJoinedGroup(joinedGroup.getId(), params);
        fileTagController.applyTagsToSolr(true, true, 1000);
    }

    @Test
    public void testAddGroupToUserGroup() {
        testStartLog(logger);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        logger.debug("Got {} groups", groupsFileCount);
        GroupDto firstGroup = groupsFileCount.getContent().get(0).getItem();
        GroupDto secondGroup = groupsFileCount.getContent().get(1).getItem();
        logger.debug("Groups to join: {} and {}", firstGroup.getId(), secondGroup.getId());
        logger.debug("Join group {} to a single userGroup", firstGroup);
        NewJoinedGroupDto newJoinedGroupDto = new NewJoinedGroupDto();
        newJoinedGroupDto.setGroupName("testAddGroupToUserGroup");
        newJoinedGroupDto.setChildrenGroupIds(Lists.newArrayList(firstGroup.getId()));
        logger.debug("add associations to sub-groups");
        List<FileTagTypeAndTagsDto> allFileTagTypesAndChildren = fileTagController.getAllFileTagTypesAndChildren(null);

        FileTagTypeAndTagsDto fileTagTypeAndTagsDto = allFileTagTypesAndChildren.stream()
                .filter(f -> f.getFileTags().size() > 0)
                .findFirst().orElse(null);

        FileTagDto fileTagDto = fileTagTypeAndTagsDto.getFileTags().get(0);
        logger.debug("Add tag {} to group {}", fileTagDto, firstGroup.getId());
        fileTagController.addTagToGroup(firstGroup.getId(), fileTagDto.getId());

        logger.debug("Create Joined groups (with group {})", firstGroup.getId());
        GroupDetailsDto groupDetailsDto = groupsApiController.createJoinedGroup(newJoinedGroupDto, performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        GroupDto joinedGroupDto = groupDetailsDto.getGroupDto();

        logger.debug("Add group {} to joined group {}", secondGroup, joinedGroupDto);
        newJoinedGroupDto.setChildrenGroupIds(Lists.newArrayList(secondGroup.getId()));
        GroupDetailsDto result = groupsApiController.addGroupsToJoinedGroup(joinedGroupDto.getId(), newJoinedGroupDto,
                performanceTestService.createWaitParams());

        logger.debug("Verify joined group has 2 groups");

        Assert.assertNotNull(result.getGroupDto().getId());
        Assert.assertEquals("testAddGroupToUserGroup", result.getGroupDto().getGroupName());
        Assert.assertNotNull(result.getChildrenGroups());
        Assert.assertThat(result.getChildrenGroups().size(), Matchers.greaterThan(1));

        logger.debug("Verify that the specific groups are in");
        List<String> subGroupIds = result.getChildrenGroups().stream()
                .map(GroupDto::getId).collect(Collectors.toList());
        Assert.assertTrue(subGroupIds.contains(firstGroup.getId()));
        Assert.assertTrue(subGroupIds.contains(secondGroup.getId()));

        logger.debug("Verify the added tag is in the new group");
        Optional<FileTagAssociationDto> first = result.getGroupDto().getFileTagAssociations().stream().filter(f -> f.getFileTagDto().getId() == fileTagDto.getId()).findFirst();
        Assert.assertTrue("The tag " + fileTagDto.getId() + " should be attached to the result user group", first.isPresent());
//        fileTagController.applyTagsToSolr(true,true,1000);

        Assert.assertEquals(firstGroup.getNumOfFiles() + secondGroup.getNumOfFiles(), result.getGroupDto().getNumOfFiles());

        logger.debug("Delete joined group");
        groupsApiController.deleteJoinedGroup(result.getGroupDto().getId(), performanceTestService.createWaitParams());

    }

    @Test
    public void testRemoveGroupFromJoinedGroup() {
        testStartLog(logger);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        logger.debug("Got {} groups", groupsFileCount.getContent().size());
        GroupDto firstGroup = groupsFileCount.getContent().get(0).getItem();
        GroupDto secondGroup = groupsFileCount.getContent().get(1).getItem();
        logger.debug("Join groups {} and {} to a single userGroup", firstGroup, secondGroup);
        NewJoinedGroupDto groupDto = new NewJoinedGroupDto();
        groupDto.setGroupName("testRemoveGroupFromJoinedGroup");
        groupDto.setChildrenGroupIds(Lists.newArrayList(firstGroup.getId(), secondGroup.getId()));
        logger.debug("Create Joined groups");
        GroupDetailsDto groupDetailsDto = groupsApiController.createJoinedGroup(groupDto, performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        GroupDto joinedGroupDto = groupDetailsDto.getGroupDto();

        groupsApiController.removeGroupFromJoinedGroup(joinedGroupDto.getId(), firstGroup.getId(), performanceTestService.createWaitParams());

        logger.info("Verify group was removed");
        GroupDetailsDto firstGroup2 = groupsApiController.findGroupById(firstGroup.getId());
        Assert.assertFalse(firstGroup2.getGroupDto().getDeleted());
        Assert.assertNull(firstGroup2.getGroupDto().getParentGroupName());

        GroupDetailsDto groupDetailsDto1 = groupsApiController.findGroupById(joinedGroupDto.getId());
        List<String> subGroupIds = groupDetailsDto1.getChildrenGroups().stream().map(GroupDto::getId).collect(Collectors.toList());
        Assert.assertFalse(subGroupIds.contains(firstGroup.getId()));
        Assert.assertTrue(subGroupIds.contains(secondGroup.getId()));
        Assert.assertEquals(secondGroup.getNumOfFiles(), groupDetailsDto1.getGroupDto().getNumOfFiles());
//        fileTagController.applyTagsToSolr(true,true,1000);
        logger.debug("Delete joined group");
        groupsApiController.deleteJoinedGroup(groupDetailsDto1.getGroupDto().getId(), performanceTestService.createWaitParams());
//        fileTagController.applyTagsToSolr(true,true,1000);
    }

//    @Test
//    @Ignore
//    public void testUpgradeSchemaHelperCode() {
//        upgradeSchemaService.executeScript40();
//    }


    @Test
    public void testListGroupsByIds() {
        testStartLog(logger);
        List<GroupDto> groupDtos = groupsApiController.listGroupsByIds(new ArrayList<>());
        Assert.assertThat(groupDtos, Matchers.empty());
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        List<String> groupIds = groupsFileCount.getContent().stream().map(f -> f.getItem().getId()).collect(Collectors.toList());
        groupDtos = groupsApiController.listGroupsByIds(groupIds);
        Assert.assertEquals(groupIds.size(), groupDtos.size());
    }
}
