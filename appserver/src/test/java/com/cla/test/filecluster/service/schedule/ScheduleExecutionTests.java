package com.cla.test.filecluster.service.schedule;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.schedule.CrawlerPhaseBehavior;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.ScheduleApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.files.managers.FileClusterAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.RescanMechanismTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by uri on 04/05/2016.
 */
@Category(RescanMechanismTestCategory.class)
public class    ScheduleExecutionTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(SchedulingConfigurationTests.class);

    @Autowired
    ScheduleApiController scheduleApiController;

    @Autowired
    JobManagerApiController jobManagerApiController;

    @Autowired
    FileTreeApiController fileTreeApiController;

    @Autowired
    FileClusterAppService fileClusterAppService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private GroupsApiController groupsApiController;

    private RootFolderDto rootFolderDto;

    @BeforeClass
    public static void beforeClass() {
        printToErr(ScheduleExecutionTests.class.getName());
    }

    @Before
    public void scanBaseDir() {
        rootFolderDto = rootFolderDtoAnalyze;
    }

    @Test //(timeout = 90000)
    public void testExecuteRescanOnScheduleGroup() {
        testStartLog(logger);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        int groupsCount = groupsFileCount.getContent().size();
        logger.debug("There are {} groups before the test", groupsCount);
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto("testExecuteRescanOnScheduleGroup", CrawlerPhaseBehavior.EVERY_ROOT_FOLDER);
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);

        FacetPage<AggregationCountItemDTO<GroupDto>> newGroupsFileCount = groupsApiController.findGroupsFileCount(null);
        int newGroupsCount = newGroupsFileCount.getContent().size();
        logger.debug("There are {} groups after the test", newGroupsCount);
        Assert.assertThat("There should be Groups after rescan",newGroupsCount,Matchers.greaterThan(0));
        //TODO 2.0 - return this check
       // Assert.assertThat("Groups shouldn't be lost during the rescans",newGroupsCount,Matchers.greaterThanOrEqualTo(groupsCount));
    }

    @Test(timeout = 60000)
    public void testEmptyScheduleGroup() {
        testStartLog(logger);
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto("testEmptyScheduleGroup", CrawlerPhaseBehavior.EVERY_ROOT_FOLDER);
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        //Scan empty group
        Map<String, String> params = new HashMap<>();
        params.put("wait","true");
        params.put("scheduleGroupIds",String.valueOf(scheduleGroup.getId()));
        jobManagerApiController.startScheduleGroup(params);
        Page<CrawlRunDetailsDto> crawlRunHistory = jobManagerApiController.listRuns(null);
        Assert.assertThat("There should be at least one crawl run in history",crawlRunHistory.getContent().size(), Matchers.greaterThan(0));
    }
}
