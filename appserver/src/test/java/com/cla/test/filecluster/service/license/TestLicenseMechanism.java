package com.cla.test.filecluster.service.license;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.filecluster.service.api.LicenseApiController;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by uri on 28/06/2016.
 */
@Category(FastTestCategory.class)
public class TestLicenseMechanism extends ClaBaseTests {

    Logger logger = LoggerFactory.getLogger(TestLicenseMechanism.class);

    @Autowired
    LicenseApiController licenseApiController;

    @Test
    public void testLastAppliedLicense() {
        testStartLog(logger);
        DocAuthorityLicenseDto license = licenseApiController.getLastAppliedLicense();
        logger.debug("Default license should be loaded");
        Assert.assertNotNull(license.getId());
        Assert.assertFalse(license.isLoginExpired());
        //TODO - datapopulator should create a default license
    }

    @Test
    public void testLicenseNotExpired() {
        Boolean loginExpired = licenseApiController.isLoginExpired();
        Assert.assertFalse(loginExpired);
    }

    @Test
    public void testLoadLicense() {
        String licensePart= "eyJpZCI6MTQ2NzExNTY2Mzc5NCwiY3JlYXRpb25EYXRlIjoxNDY3MTE1NjYzNzk0LCJhcHBseURhdGUiOjAsInJlZ2lzdGVyZWRUbyI6IlRlc3RzIiwiaXNzdWVyIjoiVXJpIiwicmVzdHJpY3Rpb25zIjp7Im1heFJvb3RGb2xkZXJzIjoiMTAiLCJjcmF3bGVyRXhwaXJ5IjoiMTU1MzQ0MDA2MzAwMCIsIm1heEZpbGVzIjoiMTAwMDAwMCIsImxvZ2luRXhwaXJ5IjoiMTU1MzQ0MDA2MzAwMCJ9fQ==";
        String signature = "3fpkIBwaJuZHlA6EIkN3Xk2pE7o=";
        licenseApiController.importLicenseString(licensePart, signature, true);
        DocAuthorityLicenseDto license = licenseApiController.getLastAppliedLicense();
        logger.debug("license should be loaded");
        Assert.assertNotNull(license.getId());
    }
}
