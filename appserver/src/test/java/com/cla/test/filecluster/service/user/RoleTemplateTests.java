package com.cla.test.filecluster.service.user;

import com.cla.common.domain.dto.security.*;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.users.UserApiController;
import com.cla.filecluster.service.security.RoleTemplateService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created by: yael
 * Created on: 11/30/2017
 */
@Category(FastTestCategory.class)
public class RoleTemplateTests  extends ClaBaseTests {

    @Autowired
    private UserApiController userApiController;

    @Autowired
    private RoleTemplateService roleTemplateService;

    private static final Logger logger = LoggerFactory.getLogger(RoleTemplateTests.class);

    @Test
    public void testGetRoleTemplates() {
        testStartLog(logger);
        List<RoleTemplateDto> templates = roleTemplateService.getRoleTemplates();
        Assert.assertThat("There should be at least 1 role template",templates, Matchers.not(Matchers.emptyIterable()));
    }

    @Test
    public void testGetRoleTemplatesAndRules() {
        testStartLog(logger);
        List<RoleTemplateDto> templates = roleTemplateService.getRoleTemplates();
        Assert.assertThat("There should be at least 1 role template",templates, Matchers.not(Matchers.emptyIterable()));
        RoleTemplateDto t = templates.iterator().next();
        logger.debug("Got template: {}",t);
        t = roleTemplateService.getRoleTemplate(t.getId());
        Assert.assertTrue("There should be at least 1 role for a template", t.getSystemRoleDtos().size() > 0);
    }

    @Test
    public void testCreateRoleTemplate() {
        testStartLog(logger);
        RoleTemplateDto template = createRoleTemplate("testCreateRoleTemplate");
        Assert.assertNotNull(template);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateRoleTemplateDuplicateFailure() {
        testStartLog(logger);
        RoleTemplateDto template = createRoleTemplate("testCreateRoleTemplateDuplicateFailure");
        Assert.assertNotNull(template);
        createRoleTemplate("testCreateRoleTemplateDuplicateFailure");
    }

    @Test
    public void testCreateRoleTemplateDuplicateDelete() {
        testStartLog(logger);
        RoleTemplateDto template = createRoleTemplate("testCreateRoleTemplateDuplicateDelete");
        Assert.assertNotNull(template);
        roleTemplateService.deleteRoleTemplate(template.getId());
        RoleTemplateDto template2 = createRoleTemplate("testCreateRoleTemplateDuplicateDelete");
        Assert.assertNotNull(template2);
    }

    @Test
    public void testDeleteRoleTemplate() {
        testStartLog(logger);
        RoleTemplateDto roleTemplateDto = createRoleTemplate("testDeleteRoleTemplate");
        Assert.assertNotNull(roleTemplateDto.getId());
        roleTemplateService.deleteRoleTemplate(roleTemplateDto.getId());
        List<RoleTemplateDto> roleTemplates = roleTemplateService.getRoleTemplates();
        for (RoleTemplateDto t : roleTemplates) {
            if (Objects.equals(t.getId(), roleTemplateDto.getId())) {
                throw new AssertionError("Found deleted RoleTemplate in list");
            }
        }
    }

    @Test(expected = BadRequestException.class)
    public void testDeleteRoleTemplateFail() {
        testStartLog(logger);
        RoleTemplateDto roleTemplateDto = createRoleTemplate("testDeleteRoleTemplateFail");
        Assert.assertNotNull(roleTemplateDto.getId());

        BizRoleDto bizRoleDto = new BizRoleDto();
        bizRoleDto.setName("testDeleteRoleTemplateFailBR1");
        bizRoleDto.setDisplayName("testDeleteRoleTemplateFailBR1");
        bizRoleDto.setDescription("testDeleteRoleTemplateFailBR1");
        bizRoleDto.setTemplate(roleTemplateDto);
        bizRoleDto = userApiController.createBizRole(bizRoleDto);

        List<CompositeRoleDto> cr = roleTemplateService.rolesForTemplate(roleTemplateDto.getId());
        Assert.assertTrue(cr.size() == 1);
        Assert.assertEquals(cr.get(0).getId(), bizRoleDto.getId());

        roleTemplateService.deleteRoleTemplate(roleTemplateDto.getId());
    }

    @Test
    public void testUpdateRoleTemplate() {
        testStartLog(logger);
        RoleTemplateDto roleTemplateDto = createRoleTemplate("testUpdateRoleTemplate");
        roleTemplateDto.setName("Updated");
        roleTemplateDto.setDisplayName("Updated");
        RoleTemplateDto updated = roleTemplateService.updateRoleTemplate(roleTemplateDto);
        Assert.assertEquals("Updated",updated.getName());
    }

    @Test(expected = BadRequestException.class)
    public void testUpdateRoleTemplateNameInUse() {
        testStartLog(logger);
        RoleTemplateDto roleTemplateDto = createRoleTemplate("testUpdateRoleTemplateNameInUse");
        Assert.assertNotNull(roleTemplateDto);
        roleTemplateDto = createRoleTemplate("testUpdateRoleTemplateNameInUse2");
        Assert.assertNotNull(roleTemplateDto);
        roleTemplateDto.setDisplayName("testUpdateRoleTemplateNameInUse");
        roleTemplateService.updateRoleTemplate(roleTemplateDto);
    }

    @Test(expected = BadRequestException.class)
    public void testUpdateTemplateTypeNotAllowed() {
        testStartLog(logger);
        RoleTemplateDto roleTemplateDto = createRoleTemplate("testUpdateRoleTemplateNameInUse");
        Assert.assertNotNull(roleTemplateDto);
        RoleTemplateType originalTemplateType = roleTemplateDto.getTemplateType();
        RoleTemplateType newTemplateType = getOtherTemplateType(originalTemplateType);
        roleTemplateDto.setTemplateType(newTemplateType);
        roleTemplateService.updateRoleTemplate(roleTemplateDto);
    }

    private RoleTemplateType getOtherTemplateType(RoleTemplateType original) {
        RoleTemplateType[] values = RoleTemplateType.values();
        int newOrdinal = (original.ordinal()+1) % values.length;
        return values[newOrdinal];
    }

    private RoleTemplateDto createRoleTemplate(String name) {
        List<SystemRoleDto> rolesWithAuthorities = userApiController.getRolesWithAuthorities();
        Set<SystemRoleDto> roles = Collections.singleton(rolesWithAuthorities.get(0));

        RoleTemplateDto r = new RoleTemplateDto();
        r.setName(name);
        r.setDisplayName(name);
        r.setDescription(name);
        r.setTemplateType(RoleTemplateType.NONE);
        r.setSystemRoleDtos(roles);
        return roleTemplateService.createRoleTemplate(r);
    }
}
