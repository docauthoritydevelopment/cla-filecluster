package com.cla.test.filecluster.service.files.histogram;

import com.cla.common.domain.dto.mediaproc.AnalysisMetadata;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.solr.common.util.Base64;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.util.Assert;

@Category(UnitTestCategory.class)
public class BackwardCompatabilityTest {

    private static final String BELLATRIX_P01_ANALYSIS_METADATA = "H4sIAAAAAAAAAIxSMW/TQBR+cRPRhrQNRSAxIEUIVbDYHbqgIEEwNrVwStVkYIOTfYlNzz5zd6YOQ6SOTEhsDOwIsTIhVSAkmGGDrerCwn/gnU1EhATiJL/T8/vue+99773+AQ0pYDPgiRkwYuKd8NQMeUJivBQ3RzGjplMElPVSwiYyln2qSEgUmb45br06fPvUAMOHVRlRqqSbM7ZNEioVtP0H5BGxchUzy4+l6vqwUoFmBApW5zB9knWLf5aS0DAmmeCB+Wcphx+fvPz6LD0woOZDXU0yquCijwwWMlkVk1UxWchkudjUEFHdIpsKuPD3lDMg/DprAFjjiq7aZCQdm06aJ/PBTEHDuWs7Pqp6uoTp5syeEGSiVSgOvpx//om8WICaB3UZP6ZFhk9r+3VtFZzb8nade3ZExJjKznpnkIugcgpkPPWbcYvICCVrnPj2/sPZ+58XwHChyTgJXRIoLjxYUpGgMuIsLLJr18sCW/uLaNs600OYQgsZr/6P3FSPv1qCgZ6glv0myr5+bNz+fvTuyIBFD1oYZXKwF2cZDT04icLcGd1AkfakB83SszViFnKE4AK95dJzuUhyRqQPyyPcoTKNXiQFa9WOaLWtgRJxOtabhHVlXNJwGCtGpe4GJ78kZ8/0j3aprD5X8LNnU6o617Y5L/yZnV3P9rZvdYZOf8fvDZ3O5qXe5UJBbaP4CQAA//8=";

    @Test
    public void testBellatrixP01HistogramDeserialized() {
        byte[] data = Base64.base64ToByteArray(BELLATRIX_P01_ANALYSIS_METADATA);
        AnalysisMetadata analysisMetadata = (AnalysisMetadata) PvAnalysisDataUtils.
                legacyDeserializeFromByteArray(data, true,
                        ImmutableMap.of("com.cla.common.domain.dto.FileType", "com.cla.connector.domain.dto.file.FileType"));
        Assert.notNull(analysisMetadata, "Analysis metadata cannot be null");
    }

    @Test(expected = Exception.class)
    public void testBellatrixP01HistogramDeserializedFailed() {
        byte[] data = Base64.base64ToByteArray(BELLATRIX_P01_ANALYSIS_METADATA);
        PvAnalysisDataUtils.
                legacyDeserializeFromByteArray(data, true, Maps.newHashMap());
    }


}
