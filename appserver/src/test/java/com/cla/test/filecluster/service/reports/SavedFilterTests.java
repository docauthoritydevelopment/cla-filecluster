package com.cla.test.filecluster.service.reports;

import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.service.api.FilterApiController;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

/**
 * Created by uri on 09/11/2015.
 */
@Category(FastTestCategory.class)
public class SavedFilterTests extends ClaBaseTests {

    @Autowired
    private FilterApiController filterApiController;

    @Test
    public void testCreateFilters() {
        SavedFilterDto newFilter1 = new SavedFilterDto();
        newFilter1.setRawFilter("{name:test1}");
        newFilter1.setName("testFilter1");
        newFilter1.setFilterDescriptor(new FilterDescriptor());
        newFilter1.setGlobalFilter(true);
        SavedFilterDto savedFilter1 = filterApiController.createSavedFilter(newFilter1);

        SavedFilterDto newFilter2 = new SavedFilterDto();
        newFilter2.setRawFilter("{name:test2}");
        newFilter2.setFilterDescriptor(new FilterDescriptor());
        newFilter2.setName("testFilter2");
        newFilter2.setGlobalFilter(true);

        SavedFilterDto savedFilter2 = filterApiController.createSavedFilter(newFilter2);

        Page<SavedFilterDto> savedFilterDtos = filterApiController.listSavedFilters(null);

        Assert.assertThat("there should be at least 2 filters",savedFilterDtos.getContent().size(), Matchers.greaterThanOrEqualTo(2));
    }
}
