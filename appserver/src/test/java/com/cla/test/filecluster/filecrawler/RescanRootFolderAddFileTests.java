package com.cla.test.filecluster.filecrawler;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.service.api.*;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.GenerateFiles;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * Created by uri on 15/03/2016.
 */
@Category(FastTestCategory.class)
public class RescanRootFolderAddFileTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(RescanRootFolderAddFileTests.class);

    private static final String SCAN_FOLDER_1 = "./build/scan/1";

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private UiServicesApiController uiServicesApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    DocStoreService docStoreService;

    private CrawlerType crawlerType = CrawlerType.MEDIA_PROCESSOR;
    private static RootFolderDto rootFolderDto;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(120); // 60 seconds max per method tested (45 was not enough)

    private void prepareScanFolder() throws IOException {
        File file = new File(SCAN_FOLDER_1);
        file.mkdirs();
        //Remove files from previous tests
        for (File file1 : file.listFiles()) {
            file1.delete();
        }

        createFile("update.docx");
        createFile("update_acl.docx");
    }

    private void createFile(String fileName) {
        GenerateFiles.createRandomWordFile(SCAN_FOLDER_1 + "/" + fileName);
    }

    @Before
    public void setup() throws Exception {
        printGroupCount();
        prepareScanFolder();
        testStartLog(logger);
        rootFolderDto = performanceTestService.analyzeFolder(SCAN_FOLDER_1, crawlerType);
    }

    @After
    public void cleanup() throws Exception {
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(SCAN_FOLDER_1));
    }

    @Test
    public void testRescanRootFolderAddFile() {
        logger.debug("testRescanRootFolderAddFile - Create new doc file");
        String addedFile = "AddFile" + System.currentTimeMillis() + ".docx";
        Path target = Paths.get(SCAN_FOLDER_1, addedFile);
        createFile(addedFile);

        logger.info("Rescan root folder after adding new file {}",addedFile);
        rescanFolder(rootFolderDto.getId());

        ClaFileDto newlyAddedFileDto = verifyFileExistsInIndex(rootFolderDto, target);

        logger.info("testRescanRootFolderNoChanges");
        rescanFolder(rootFolderDto.getId());

        logger.info("Now add another duplicate and check");
        String addedFile2 = "AddFile" + System.currentTimeMillis() + ".docx";
        Path target2 = Paths.get(SCAN_FOLDER_1, addedFile2);
        createFile(addedFile2);
        rescanFolder(rootFolderDto.getId());
        if (newlyAddedFileDto.getGroupId() != null) {
            logger.debug("Verify newly added file {} has the same groupId {}", target, newlyAddedFileDto.getGroupId());
            DataSourceRequest groupDataSourceRequest = DataSourceRequest.build(null);
            groupDataSourceRequest.setFilter(new FilterDescriptor("groupId", newlyAddedFileDto.getGroupId(), "eq"));
            groupDataSourceRequest.setPageSize(10000);
            PageImpl<ClaFileDto> groupFiles = filesApplicationService.listFilesDeNormalized(groupDataSourceRequest);
            logger.debug("Got a total of {} files in group", groupFiles.getContent().size());
            Optional<ClaFileDto> first1 = groupFiles.getContent().stream().filter(f -> f.getBaseName().equals(target.getFileName().toString())).findFirst();
            if (!first1.isPresent()) {
                groupFiles.getContent().forEach(f -> logger.debug("File: {}", f.toString()));
            }
            Assert.assertTrue("Filter by group should contain the new file " + target, first1.isPresent());
        }
    }

    private ClaFileDto verifyFileExistsInIndex(RootFolderDto rootFolderDto, Path target) {
        logger.info("Verify file exists under fullPath: " + rootFolderDto.getPath());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("rootFolderId", rootFolderDto.getId(), KendoFilterConstants.EQ));
        dataSourceRequest.setPageSize(10000);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        boolean found = false;
        logger.info("Looking for file {} in solr under path {}", target.getFileName().toString(), rootFolderDto.getPath());
        ClaFileDto newlyAddedFileDto = null;
        for (ClaFileDto claFileDto : claFileDtos.getContent()) {
            logger.debug("Got file {}", claFileDto);
            //Find the file
            if (claFileDto.getBaseName().equals(target.getFileName().toString())) {
                newlyAddedFileDto = claFileDto;
                found = true;
            }

            if (claFileDto.getContentId() == null) {
                logger.info("File {} got no content (empty). Skipping", claFileDto);
            } else {
                // get file from db so data is up to date
                FileType testedFileType = claFileDto.getType();
                boolean emptyFile = claFileDto.getFsFileSize() == null || claFileDto.getFsFileSize() == 0;
                ClaFileState expectedFileState = (!emptyFile && (testedFileType.equals(FileType.WORD) || testedFileType.equals(FileType.EXCEL) || testedFileType.equals(FileType.PDF))) ? ClaFileState.ANALYSED : ClaFileState.INGESTED;
                Assert.assertEquals("ClaFile " + claFileDto + " should have been in " + expectedFileState + " state", expectedFileState, claFileDto.getState());
            }
        }
        if (!found) {
            throw new RuntimeException("Failed to find file " + target.getFileName() + " in solr");
        }
        return newlyAddedFileDto;
    }

    public void printGroupCount() {
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        logger.info("Got a total of {} groups", groupDtos.getContent().size());
    }

    private void waitABit() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {}
    }

    public void rescanFolder(long rootFolderId) {
        logger.info("Rescan folder {}", rootFolderId);
        Map<String, String> params = new HashMap<>();
        params.put("op", StartScanProducer.SCAN_INGEST_ANALYZE);
        params.put("wait", "true");
        params.put("rootFolderIds",String.valueOf(rootFolderId));
        RootFolderDto rootFolderDto = docStoreService.getRootFolderDto(rootFolderId);
        logger.info("Testing rescan for rootFolder: {}", rootFolderDto);
        jobManagerApiController.startScanRootFolders(params);

        waitABit();

        RootFolderSummaryInfo rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderId);
        CrawlRunDetailsDto crawlRunDetailsDto = rootFolderSummaryInfo.getCrawlRunDetailsDto();
        logger.debug("crawlRunDetailsDto={}", crawlRunDetailsDto);
        int retryCount = 3;
        while (!RunStatus.FINISHED_SUCCESSFULLY.equals(crawlRunDetailsDto.getRunOutcomeState()) && retryCount-- > 0) {
            waitABit();
            rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderId);
            crawlRunDetailsDto = rootFolderSummaryInfo.getCrawlRunDetailsDto();
            logger.debug("Retry ({}): crawlRunDetailsDto={}", retryCount, crawlRunDetailsDto);
        }
        Assert.assertEquals("Run did not finish successfully", RunStatus.FINISHED_SUCCESSFULLY, crawlRunDetailsDto.getRunOutcomeState());
    }


    private UserPrincipal changeAccess(final UserPrincipal user, Path file) {
        try {
            AclFileAttributeView view = Files.getFileAttributeView(file, AclFileAttributeView.class);
            AclEntry accessEntry = createAccessACLEntry(user);
            List<AclEntry> acl = view.getAcl();
            UserPrincipal removedPrincipal = acl.stream()
                    .filter(a -> (a.permissions().contains(AclEntryPermission.WRITE_DATA) && !a.principal().getName().toLowerCase().contains("administrator")))
                    .map(AclEntry::principal).findFirst().orElse(null);
            if (removedPrincipal != null) {
                // remove write permissions for first principal found
                acl = acl.stream().map(a ->
                        (AclEntry)((!a.type().equals(AclEntryType.ALLOW) || !a.principal().equals(removedPrincipal)) ? a :
                                AclEntry.newBuilder().setType(a.type()).setPrincipal(a.principal())
                                        .setPermissions(a.permissions().stream().filter(p->!(p.toString().toLowerCase()
                                                .contains("write"))).collect(Collectors.toSet()))
                                        .setFlags(a.flags())
                                        .build() ) )
                        .collect(Collectors.toList());
            }
            acl.add(0, accessEntry); // insert at head in case there are any DENY entries
            view.setAcl(acl);
            logger.debug("Changed permissions for file {}: added {}, removed {}", file.toString(), user.getName(), removedPrincipal.getName());
            return removedPrincipal;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to grant access for " + file.toString() + " to " + user.getName(), e);
        }
    }

    private AclEntry createAccessACLEntry(UserPrincipal user) {
        AclEntry entry = AclEntry
                .newBuilder()
                .setType(AclEntryType.ALLOW)
                .setPrincipal(user)
                .setPermissions(AclEntryPermission.DELETE_CHILD,
                        AclEntryPermission.WRITE_NAMED_ATTRS,
                        AclEntryPermission.EXECUTE,
                        AclEntryPermission.WRITE_DATA,
                        AclEntryPermission.WRITE_ATTRIBUTES,
                        AclEntryPermission.READ_ATTRIBUTES,
                        AclEntryPermission.APPEND_DATA,
                        AclEntryPermission.READ_DATA,
                        AclEntryPermission.READ_NAMED_ATTRS,
                        AclEntryPermission.READ_ACL,
                        AclEntryPermission.SYNCHRONIZE,
                        AclEntryPermission.DELETE)
                .setFlags(AclEntryFlag.FILE_INHERIT,
                        AclEntryFlag.DIRECTORY_INHERIT)
                .build();
        return entry;
    }

}
