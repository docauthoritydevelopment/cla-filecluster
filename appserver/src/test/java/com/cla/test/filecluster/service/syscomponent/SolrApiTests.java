package com.cla.test.filecluster.service.syscomponent;

import com.cla.common.domain.dto.components.solr.SolrCollectionDto;
import com.cla.common.domain.dto.components.solr.SolrNodeDto;
import com.cla.common.domain.dto.components.solr.SolrShardDto;
import com.cla.filecluster.service.api.SolrComponentsApiController;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by uri on 01-Jun-17.
 */
@Category(FastTestCategory.class)
@Ignore
public class SolrApiTests  extends ClaBaseTests {

    @Autowired
    private SolrComponentsApiController solrComponentsApiController;

    private static final Logger logger = LoggerFactory.getLogger(SolrApiTests.class);

    @Test
    public void testListCollections() {
        testStartLog(logger);
        List<SolrCollectionDto> collections = solrComponentsApiController.listCollections();
        for (SolrCollectionDto collection : collections) {
            logger.debug(collection.toString());
        }
        Assert.assertThat(collections.size(), Matchers.greaterThan(1));
    }

    @Test
    public void testListCollectionShards() {
        testStartLog(logger);
        List<SolrShardDto> testCatFiles = solrComponentsApiController.listCollectionShards("TestCatFiles");
        for (SolrShardDto testCatFile : testCatFiles) {
            logger.debug("Live node: {}",testCatFile);
        }
    }

    @Test
    public void testListCollectionNodes() {
        testStartLog(logger);
        List<SolrNodeDto> testCatFiles = solrComponentsApiController.listCollectionNodes("TestCatFiles");
        for (SolrNodeDto testCatFile : testCatFiles) {
            logger.debug("Live node: {}",testCatFile);
        }

    }

}
