package com.cla.test.filecluster.policy.output;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.event.ScanDiffEvent;
import com.cla.common.domain.dto.event.ScanDiffPayload;
import com.cla.common.domain.dto.messages.action.ActionPluginConfig;
import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import com.cla.common.domain.dto.messages.action.PluginInput;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.policy.PolicyEngine;
import com.cla.filecluster.policy.PolicyMappings;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.policy.domain.dto.ActionPluginsDefinition;
import com.cla.filecluster.policy.factsenricher.FactsEnricher;
import com.google.common.collect.Lists;
import org.junit.Assert;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

/**
 * Verify accroding to given policies and event input, if infact the expected policies were activated with the correct
 * plugins and input
 */
public class PolicyEngineOutputVerifier {

    private List<PolicyConfig> policies = new LinkedList<>();
    private Map<EventType, List<String>> expectedEventTypeToPolicyNames = new HashMap<>();
    private List<FactsEnricher> factsEnrichers = new LinkedList<>();
    private PolicyMappings policyMappings = new PolicyMappings();
    private int concurrency = 1;
    private Map<String, Object> expectedFacts = new HashMap<>();
    private List<Pair<String, Class<? extends PluginInput>>> expectedPluginActionRequests = new LinkedList<>();

    public static PolicyEngineOutputVerifier newVerifier() {
        return new PolicyEngineOutputVerifier();
    }

    private PolicyEngineOutputVerifier() {

    }

    public PolicyEngineOutputVerifier withPoliciesInput(List<PolicyConfig> policies) {
        this.policies = policies;
        return this;
    }

    public PolicyEngineOutputVerifier expectedPoliciesActivatedForEvent(EventType eventType, String... policyNames) {
        expectedEventTypeToPolicyNames.put(eventType, Lists.newArrayList(policyNames));
        return this;
    }

    public void verifyOn(List<EventType> eventTypes) {
        BlockingQueue<DAEvent> eventQueue = new LinkedBlockingQueue<>();
        List<DAEvent> eventList = eventTypes.stream().map(this::buildDaEvent).collect(Collectors.toList());
        eventQueue.addAll(eventList);

        BlockingQueue<ActionPluginRequest> actionPluginQueue = new LinkedBlockingQueue<>();
        if (!expectedEventTypeToPolicyNames.isEmpty()) {
            policies.forEach(p -> {
                ActionPluginsDefinition actionPluginsDefinition = new ActionPluginsDefinition();
                actionPluginsDefinition.setPluginInputTransformer(daEvent -> new EventByPolicyInput(daEvent.getEventType(), p));
                actionPluginsDefinition.setActionPluginConfigs(Lists.newArrayList(new ActionPluginConfig("dummyConfig", new HashMap<>())));
                p.getActionPluginsDefinitions().add(actionPluginsDefinition);
            });
        }
        policyMappings.loadPolicies(policies);
        PolicyEngine policyEngine = new PolicyEngine(this.factsEnrichers, policyMappings, eventQueue, actionPluginQueue, concurrency);
        policyEngine.start();
        try {
            while (!eventQueue.isEmpty()) {
                Thread.sleep(1000);
            }
            Thread.sleep(1000);
            verifyResults(eventList, actionPluginQueue);
        } catch (InterruptedException e) {
            Assert.fail("Failed to assert results, interrupted: " + e.getMessage());
        }
    }

    public PolicyEngineOutputVerifier expectedActionPluginRequests(List<Pair<String, Class<? extends PluginInput>>> expectedPluginActionRequests) {
        this.expectedPluginActionRequests = expectedPluginActionRequests;
        return this;
    }

    private void verifyResults(List<DAEvent> eventList, BlockingQueue<ActionPluginRequest> actionPluginQueue) throws InterruptedException {
        if (expectedFacts.isEmpty()) {
            eventList.forEach(ev -> Assert.assertTrue(ev.getFactsMap().isEmpty()));
        } else {
            eventList.forEach(ev -> Assert.assertTrue(ev.getFactsMap().equals(expectedFacts)));
        }

        List<ActionPluginRequest> pluginActionRequests = new LinkedList<>();
        while (!actionPluginQueue.isEmpty()) {
            ActionPluginRequest pluginActionRequest = actionPluginQueue.take();
            // Case EventByPolicyInput meaning remove matching expected event type to policy name
            if (pluginActionRequest.getInput() instanceof EventByPolicyInput) {
                List<String> policyNames = expectedEventTypeToPolicyNames.get(((EventByPolicyInput) pluginActionRequest.getInput()).eventType);
                PolicyConfig policy = ((EventByPolicyInput) pluginActionRequest.getInput()).policy;
                policyNames.remove(policy.getName());
            } else { // Otherwise add the plugin action request for later verifying the expectedPluginActionRequests
                if (expectedPluginActionRequests.isEmpty()) {
                    Assert.fail("Not expecting active policies but policy plugin was activated");
                }
                pluginActionRequests.add(pluginActionRequest);
            }
        }

        // Assert expectedEventTypeToPolicyNames is empty - meaning found all expected policies.
        Assert.assertTrue(expectedEventTypeToPolicyNames.values().stream().allMatch(List::isEmpty));

        // Assert all request were correctly retrieved
        Assert.assertTrue(pluginActionRequests.stream()
                .allMatch(par -> expectedPluginActionRequests.contains(
                        Pair.of(par.getConfig().getPluginId(), par.getInput().getClass()))));
    }

    private DAEvent buildDaEvent(EventType eventType) {
        ScanDiffEvent scanDiffEvent = new ScanDiffEvent();
        scanDiffEvent.setEventType(eventType);
        scanDiffEvent.setDescription("Scan diff " + eventType);
        ScanDiffPayload payload = new ScanDiffPayload();
        payload.setFileId(new Date().getTime());
        payload.setPath("path:/" + payload.getFileId());
        scanDiffEvent.setPayload(payload);
        return scanDiffEvent;
    }

    public PolicyEngineOutputVerifier withFactsEnrichers(List<FactsEnricher> factsEnrichers) {
        this.factsEnrichers = factsEnrichers;
        return this;
    }

    public PolicyEngineOutputVerifier withConcurrency(int concurrency) {
        this.concurrency = concurrency;
        return this;
    }


    public PolicyEngineOutputVerifier expectedFact(String key, Object value) {
        expectedFacts.put(key, value);
        return this;
    }


    private class EventByPolicyInput implements PluginInput {

        private final PolicyConfig policy;
        private final EventType eventType;

        private EventByPolicyInput(EventType eventType, PolicyConfig policy) {
            this.eventType = eventType;
            this.policy = policy;
        }
    }
}
