package com.cla.test.filecluster.policy.input;

import com.cla.common.domain.dto.messages.action.ActionPluginConfig;
import com.cla.filecluster.policy.domain.dto.ActionPluginsDefinition;
import com.cla.filecluster.policy.plugin.transformer.ActionPluginInputTransformer;

import java.util.List;

public class ActionPluginsDefinitionBuilder {

    private ActionPluginsDefinition actionPluginsDefinition = new ActionPluginsDefinition();

    private ActionPluginsDefinitionBuilder(){

    }

    public ActionPluginsDefinitionBuilder withPluginInputTransformer(ActionPluginInputTransformer pluginInputTransformer) {
        actionPluginsDefinition.setPluginInputTransformer(pluginInputTransformer);
        return this;
    }

    public ActionPluginsDefinitionBuilder withActionPluginConfigs(List<ActionPluginConfig> actionPluginConfigs) {
        actionPluginsDefinition.setActionPluginConfigs(actionPluginConfigs);
        return this;
    }

    public ActionPluginsDefinition build(){
        return actionPluginsDefinition;
    }

    public static ActionPluginsDefinitionBuilder newBuilder() {
        return new ActionPluginsDefinitionBuilder();
    }
}
