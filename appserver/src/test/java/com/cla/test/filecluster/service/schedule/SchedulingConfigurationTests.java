package com.cla.test.filecluster.service.schedule;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.common.domain.dto.schedule.ScheduleType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.EntityAlreadyExistsException;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.ScheduleApiController;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by uri on 02/05/2016.
 */
@Category(FastTestCategory.class)
public class SchedulingConfigurationTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(SchedulingConfigurationTests.class);

    @Autowired
    private ScheduleApiController scheduleApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Test
    public void testCreateScheduleGroup() {
        logger.info("testCreateScheduleGroup");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testCreateScheduleGroup");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        Assert.assertNotNull(scheduleGroup.getId());
        Page<ScheduleGroupDto> scheduleGroupDtos = scheduleApiController.listAllScheduleGroups(null);
        Optional<ScheduleGroupDto> first = scheduleGroupDtos.getContent().stream().filter(f -> f.getId() == scheduleGroup.getId()).findFirst();
        Assert.assertTrue(first.isPresent());
    }

    @Test
    public void testListDefaultScheduleGroup() {
        logger.info("testListDefaultScheduleGroup");
        String defaultScheduleGroupName = ScheduleGroupService.DEFAULT_SCHEDULE_GROUP;
        Page<ScheduleGroupDto> scheduleGroupDtos = scheduleApiController.listAllScheduleGroups(null);
        Optional<ScheduleGroupDto> first = scheduleGroupDtos.getContent().stream().filter(f -> f.getName().equals(defaultScheduleGroupName)).findFirst();
        Assert.assertTrue(first.isPresent());
        List<Long> rootFoldersIds = first.get().getRootFolderIds();
        Assert.assertNotNull("There should be rootFolders list under the default group", rootFoldersIds);
        Assert.assertThat("There should be at least one root folder under the default group", rootFoldersIds.size(), Matchers.greaterThan(0));
        ScheduleConfigDto scheduleConfigDto = first.get().getScheduleConfigDto();
        Assert.assertEquals("The default schedule group is daily", ScheduleType.DAILY, scheduleConfigDto.getScheduleType());
        Assert.assertNotNull(first.get().getCronTriggerString());

        logger.debug("Check that the schedule group is returned for the filtered api");
        Map<String, String> params = new HashMap<>();
        params.put(ScheduleApiController.WITH_ROOT_FOLDERS, "true");
        Page<ScheduleGroupSummaryInfoDto> scheduleGroupSummaryInfoDtos = scheduleApiController.listScheduleGroupsSummaryInfo(params);
        Optional<ScheduleGroupSummaryInfoDto> scheduleGroupSummaryInfoDto = scheduleGroupSummaryInfoDtos.getContent().stream().filter(f -> f.getScheduleGroupDto().getName().equals(defaultScheduleGroupName)).findFirst();
        Assert.assertTrue(scheduleGroupSummaryInfoDto.isPresent());

    }

    @Test
    public void testUpdateScheduleGroup() {
        logger.info("testUpdateScheduleGroup");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testUpdateScheduleGroup");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        scheduleGroupDto.setActive(false);
        scheduleGroupDto.setName("testUpdateScheduleGroup - updated");
        scheduleGroupDto.setScheduleConfigDto(new ScheduleConfigDto(ScheduleType.HOURLY, 10, 2));
        scheduleApiController.updateScheduleGroup(scheduleGroup.getId(), scheduleGroupDto);
        Page<ScheduleGroupDto> scheduleGroupDtos = scheduleApiController.listAllScheduleGroups(null);
        Optional<ScheduleGroupDto> first = scheduleGroupDtos.getContent().stream().filter(f -> f.getId() == scheduleGroup.getId()).findFirst();
        Assert.assertTrue(first.isPresent());
        ScheduleGroupDto actual = first.get();
        Assert.assertEquals(false, actual.isActive());
        Assert.assertEquals("testUpdateScheduleGroup - updated", actual.getName());
        Assert.assertEquals("0 10 */2 * * *", actual.getCronTriggerString());
        Assert.assertEquals(ScheduleType.HOURLY, actual.getScheduleConfigDto().getScheduleType());
    }

    @Test
    public void testAttachRootFolderToSchedulingGroup() {
        logger.info("testAttachRootFolderToSchedulingGroup");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testAttachRootFolderToSchedulingGroup");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath("testAttachRootFolderToSchedulingGroup");
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, scheduleGroup.getId());
        Long rootFolderId = rootFolder.getId();
//        scheduleApiController.attachRootFolderToSchedulingGroup(scheduleGroup.getId(), rootFolderId);
        Page<ScheduleGroupDto> scheduleGroupDtos = scheduleApiController.listAllScheduleGroups(null);
        Optional<ScheduleGroupDto> first = scheduleGroupDtos.getContent().stream().filter(f -> f.getId() == scheduleGroup.getId()).findFirst();
        Assert.assertTrue(first.isPresent());
        ScheduleGroupDto actual = first.get();
        Assert.assertThat("There should be a single attached root folder", actual.getRootFolderIds().size(), Matchers.equalTo(1));

        Page<ScheduleGroupDto> scheduleGroupDtos1 = scheduleApiController.listRootFolderScheduleGroups(rootFolderId, null);
        for (ScheduleGroupDto groupDto : scheduleGroupDtos1.getContent()) {
            logger.debug("Got: {} with {} rootFolders", groupDto.toString(), groupDto.getRootFolderIds().size());
        }

        Assert.assertEquals("There should be one schedule group returned", 1, scheduleGroupDtos1.getContent().size());
        List<Long> rootFoldersIds = scheduleGroupDtos1.getContent().get(0).getRootFolderIds();
        Assert.assertThat("Schedule group should contain root folder id", rootFoldersIds, Matchers.contains(rootFolderId));
    }

    @Test
    public void testAttachMultipleRootFolders() {
        logger.info("testAttachMultipleRootFolders");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testAttachMultipleRootFolders");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);

        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath("testAttachMultipleRootFolders1");
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, scheduleGroup.getId());

        RootFolderDto rootFolderDto2 = new RootFolderDto();
        rootFolderDto2.setPath("testAttachMultipleRootFolders2");
        rootFolderDto2.setMediaType(MediaType.FILE_SHARE);
        RootFolderDto rootFolder2 = fileTreeApiController.createRootFolder(rootFolderDto2, scheduleGroup.getId());

        Page<ScheduleGroupSummaryInfoDto> scheduleGroupSummaryInfoDtos = scheduleApiController.listScheduleGroupsSummaryInfo(null);
        List<ScheduleGroupSummaryInfoDto> filteredList = scheduleGroupSummaryInfoDtos.getContent().stream().filter(f -> f.getScheduleGroupDto().getId() == scheduleGroup.getId()).collect(Collectors.toList());
        Assert.assertEquals(1, filteredList.size());
        for (ScheduleGroupSummaryInfoDto scheduleGroupSummaryInfoDto : scheduleGroupSummaryInfoDtos) {
            logger.debug(scheduleGroupSummaryInfoDto.toString());
            Assert.assertNotNull(scheduleGroupSummaryInfoDto.getRunStatus());
        }

        Page<RootFolderDto> rootFolderDtos = scheduleApiController.listScheduleGroupRootFolders(scheduleGroup.getId(), null);
        Assert.assertThat("There should be rootFolders", rootFolderDtos.getContent().size(), Matchers.greaterThan(0));
        Page<RootFolderSummaryInfo> rootFolderSummaryInfos = scheduleApiController.listScheduleGroupRootFoldersSummary(scheduleGroup.getId(), null);
        Assert.assertThat("There should be rootFolders summaries", rootFolderSummaryInfos.getContent().size(), Matchers.greaterThan(0));
        Map<String, String> params = new HashMap<>();
        params.put(DataSourceRequest.SELECTED_ITEM_ID, String.valueOf(rootFolder2.getId()));
        rootFolderSummaryInfos = scheduleApiController.listScheduleGroupRootFoldersSummary(scheduleGroup.getId(), params);
        Assert.assertThat("There should be rootFolders summaries", rootFolderSummaryInfos.getContent().size(), Matchers.greaterThan(0));

        params = new HashMap<>();
        int length = rootFolder2.getRealPath().length();
        params.put("namingSearchTerm", rootFolder2.getRealPath().substring(length - 5));
        rootFolderSummaryInfos = scheduleApiController.listScheduleGroupRootFoldersSummary(scheduleGroup.getId(), params);
        Assert.assertThat("There should be rootFolders summaries", rootFolderSummaryInfos.getContent().size(), Matchers.greaterThan(0));
        boolean found = false;
        for (RootFolderSummaryInfo folderSummaryInfo : rootFolderSummaryInfos) {
            if (folderSummaryInfo.getRootFolderDto().getId().equals(rootFolder2.getId())) {
                found = true;
            }
        }
        Assert.assertTrue(found);

        params = new HashMap<>();
        params.put("namingSearchTerm", "yyyyy");
        rootFolderSummaryInfos = scheduleApiController.listScheduleGroupRootFoldersSummary(scheduleGroup.getId(), params);
        Assert.assertEquals("There should be no rootFolders summaries with this search term", rootFolderSummaryInfos.getContent().size(), 0);
    }


    @Test
    public void testDetachRootFolderFromSchedulingGroup() {
        logger.info("testDetachRootFolderFromSchedulingGroup");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testDetachRootFolderFromSchedulingGroup");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath("testDetachRootFolderFromSchedulingGroup");
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, scheduleGroup.getId());
        scheduleApiController.detachRootFolderFromSchedulingGroup(scheduleGroup.getId(), rootFolder.getId());
        ScheduleGroupDto scheduleGroupDtoResult = scheduleApiController.listScheduleGroup(scheduleGroup.getId());
        Assert.assertNotNull(scheduleGroupDtoResult);
        Assert.assertThat("There should be no attached root folders", scheduleGroupDtoResult.getRootFolderIds().size(), Matchers.equalTo(0));
    }

    @Test
    public void testDetachRootFolderFromSchedulingGroupWithPausedRun() {
        logger.info("testDetachRootFolderFromSchedulingGroupWithPausedRun");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testDetachRootFolderFromSchedulingGroupWithPausedRun");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setPath("testDetachRootFolderFromSchedulingGroupWithPausedRun");
        rootFolderDto.setMediaType(MediaType.FILE_SHARE);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, scheduleGroup.getId());
        CrawlRun pcr = new CrawlRun();
        pcr.setRunStatus(RunStatus.PAUSED);
        pcr = crawlRunRepository.save(pcr);
        CrawlRun cr = new CrawlRun();
        cr.setRunStatus(RunStatus.PAUSED);
        cr.setParentRunId(pcr.getId());
        cr.setRootFolderId(rootFolder.getId());
        cr = crawlRunRepository.save(cr);
        docStoreService.attachRunToRootFolder(cr.getId(), rootFolder.getId());
        scheduleApiController.detachRootFolderFromSchedulingGroup(scheduleGroup.getId(), rootFolder.getId());
        cr = crawlRunRepository.findById(cr.getId()).orElse(null);
        Assert.assertTrue(cr.getParentRunId() == null);
    }

    @Test
    public void testDeleteSchedulingGroup() {
        logger.info("testDeleteSchedulingGroup");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testDeleteSchedulingGroup");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        scheduleApiController.deleteScheduleGroup(scheduleGroup.getId());
    }

    @Test(expected = BadParameterException.class)
    public void testCreateDuplicateScheduleGroup() {
        logger.info("testCreateDuplicateScheduleGroup");
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName("testCreateDuplicateScheduleGroup");
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        ScheduleGroupDto scheduleGroup2 = scheduleApiController.createScheduleGroup(scheduleGroupDto);
    }

    @Test
    public void testActivateScheduleGroup() {
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setActive(true);
        scheduleGroupDto.setName("testActivateScheduleGroup");
        ScheduleConfigDto scheduleConfigDto = new ScheduleConfigDto();
        scheduleConfigDto.setScheduleType(ScheduleType.DAILY);
        scheduleConfigDto.setEvery(6);
        scheduleConfigDto.setHour(1);
        scheduleConfigDto.setMinutes(1);
        scheduleGroupDto.setScheduleConfigDto(scheduleConfigDto);
        ScheduleGroupDto scheduleGroup = scheduleApiController.createScheduleGroup(scheduleGroupDto);
        Long id = scheduleGroup.getId();
        ScheduleGroupDto scheduleGroupDto1 = scheduleApiController.listScheduleGroup(id);
        Assert.assertEquals(true, scheduleGroupDto1.isActive());
        scheduleGroupDto.setActive(false);
        scheduleApiController.updateScheduleGroup(id, scheduleGroupDto);
        Assert.assertFalse(scheduleAppService.isGroupScheduled(scheduleGroupDto1.getId()));
    }
}
