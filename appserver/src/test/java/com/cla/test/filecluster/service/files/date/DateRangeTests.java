package com.cla.test.filecluster.service.files.date;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.date.*;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.api.DateRangeApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

/**
 * test CRUD on feature, not needed, not deleted so we can see usage of feature 
 * Created by uri on 05/06/2016.
 */
@Category(FastTestCategory.class)
@Ignore
public class DateRangeTests extends ClaBaseTests {

    @Autowired
    DateRangeApiController dateRangeApiController;

    private Logger logger = LoggerFactory.getLogger(DateRangeTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Test
    public void testDefaultDateRange() {
        logger.debug(testName.getMethodName());
        Page<DateRangePartitionDto> dateRangeSetDtos = dateRangeApiController.listDateRangePartitions(null);
        Assert.assertThat(dateRangeSetDtos.getContent().size(), Matchers.greaterThan(0));
        DateRangePartitionDto dateRangePartitionDto = dateRangeSetDtos.getContent().get(0);
        Assert.assertNotNull(dateRangePartitionDto.getId());
        Assert.assertNotNull(dateRangePartitionDto.getName());
        Assert.assertNotNull(dateRangePartitionDto.getDescription());
        DateRangePartitionDto dateRangePartitionDto1 = dateRangeApiController.listDateRangePartitionItems(dateRangePartitionDto.getId(), null);
        Assert.assertNotNull(dateRangePartitionDto1);
        Assert.assertThat(dateRangePartitionDto1.getDateRangeItemDtos().size(), Matchers.equalTo(8));   // Should be number of items in defaultDateRange.csv

        DateRangeItemDto prevDateRangeItemDto = null;
        for (DateRangeItemDto dateRangeItemDto : dateRangePartitionDto1.getDateRangeItemDtos()) {
            if (dateRangeItemDto.getEnd() == null) {
                logger.info("DateRangeItem {} has null end",dateRangeItemDto);
                Assert.assertEquals("The null end period should be 'now'",0,dateRangeItemDto.getStart().getRelativePeriodAmount().intValue());
                continue;
            }
            Assert.assertTrue("partition dateRangeItems start should be before end",
                    getDateValueEstimate(dateRangeItemDto.getStart()) <= getDateValueEstimate(dateRangeItemDto.getEnd()));
            if (prevDateRangeItemDto == null) {
                prevDateRangeItemDto = new DateRangeItemDto(dateRangeItemDto);
            } else {
                // check time line continuity
                Assert.assertTrue("partition dateRangeItems should be ordered", getDateValueEstimate(dateRangeItemDto.getEnd()) <= getDateValueEstimate(prevDateRangeItemDto.getStart()));
            }
        }
    }

    public Long relativePeriodToAbsolute(DateRangePointDto dateRangePointDto) {
        return (dateRangePointDto.getType() == DateRangePointType.ABSOLUTE) ? null :
                (System.currentTimeMillis() - TimePeriod.periodsInMs(dateRangePointDto.getRelativePeriod()) * dateRangePointDto.getRelativePeriodAmount());
    }


    public Long getDateValueEstimate(DateRangePointDto dateRangePointDto) {
        return (dateRangePointDto.getType() == DateRangePointType.ABSOLUTE) ? dateRangePointDto.getAbsoluteTime() :
                relativePeriodToAbsolute(dateRangePointDto);
    }

    @Test
    public void testUpdateDateRange() {
        logger.debug(testName.getMethodName());

        logger.debug("Create a date partition");
        DateRangePartitionDto dateRangePartitionDto  = new DateRangePartitionDto();
        dateRangePartitionDto.setDescription("testUpdateDateRange");
        dateRangePartitionDto.setName("testUpdateDateRange");
        List<DateRangeItemDto> dateRangeItemDtos = new ArrayList<>();

        long startOf1990 = LocalDateTime.of(1990, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;
        dateRangeItemDtos.add(new DateRangeItemDto("1990",new DateRangePointDto(startOf1990),null));
        long startOf2015 = LocalDateTime.of(2015, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;
        dateRangeItemDtos.add(new DateRangeItemDto("2015",new DateRangePointDto(startOf2015),null));
        long startOf2016 = LocalDateTime.of(2016, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;
        dateRangeItemDtos.add(new DateRangeItemDto("2016",new DateRangePointDto(startOf2016),null));
        dateRangePartitionDto.setDateRangeItemDtos(dateRangeItemDtos);
        dateRangePartitionDto = dateRangeApiController.createDatePartition(dateRangePartitionDto);

//        Page<DateRangePartitionDto> dateRangeSetDtos = dateRangeApiController.listDateRangePartitions(null);
//        DateRangePartitionDto dateRangePartitionDto = dateRangeSetDtos.getContent().get(0);

        logger.info("Update the date range partition {}",dateRangePartitionDto);

        dateRangePartitionDto.setName("Updated");
        dateRangeItemDtos = new ArrayList<>();
        DateRangePointDto last3Months = new DateRangePointDto(TimePeriod.MONTHS, 3);
        dateRangeItemDtos.add(new DateRangeItemDto("Last 3 months", last3Months, null));
        DateRangePointDto last12Months = new DateRangePointDto(TimePeriod.MONTHS, 12);
        dateRangeItemDtos.add(new DateRangeItemDto("Last year", last12Months, null));
        DateRangePointDto last3Years = new DateRangePointDto(TimePeriod.MONTHS, 36);
        dateRangeItemDtos.add(new DateRangeItemDto("Last 3 years", last3Years, null));
        dateRangePartitionDto.setDateRangeItemDtos(dateRangeItemDtos);
        dateRangeApiController.updateDateRangePatition(dateRangePartitionDto.getId(), dateRangePartitionDto);

        logger.info("Verify that the date range partition was updated");
        DateRangePartitionDto dateRangePartitionDto1 = dateRangeApiController.listDateRangePartitionItems(dateRangePartitionDto.getId(), null);
        Assert.assertNotNull(dateRangePartitionDto1);
        Assert.assertEquals(3, dateRangePartitionDto1.getDateRangeItemDtos().size());
        for (DateRangeItemDto dateRangeItemDto : dateRangePartitionDto1.getDateRangeItemDtos()) {
            Assert.assertNotNull(dateRangeItemDto.getId());
            Assert.assertNotNull(dateRangeItemDto.getName());
            Assert.assertNotNull(dateRangeItemDto.getStart());
        }

        DateRangeItemDto prevDateRangeItemDto = null;
        for (DateRangeItemDto dateRangeItemDto : dateRangePartitionDto1.getDateRangeItemDtos()) {
            Assert.assertTrue("partition dateRangeItems start should be before end",
                    dateRangeItemDto.getEnd() == null || getDateValueEstimate(dateRangeItemDto.getStart()) <= getDateValueEstimate(dateRangeItemDto.getEnd()));
            if (prevDateRangeItemDto == null) {
                prevDateRangeItemDto = new DateRangeItemDto(dateRangeItemDto);
            } else {
                // check time line continuity
                Assert.assertTrue("partition dateRangeItems should be ordered", getDateValueEstimate(dateRangeItemDto.getEnd()) <= getDateValueEstimate(prevDateRangeItemDto.getStart()));
            }
        }

        Page<DateRangePartitionDto> dateRangePartitionDtos = dateRangeApiController.listDateRangePartitions(null);
        Optional<DateRangePartitionDto> updated = dateRangePartitionDtos.getContent().stream().filter(f -> f.getName().equals("Updated")).findFirst();
        Assert.assertTrue("Date Partition name should have been updated",updated.isPresent());
    }

    @Test
    public void testFindDateRangeLastModifiedFileCount() {
        logger.info(testName.getMethodName());
        FacetPage<AggregationCountItemDTO<DateRangeItemDto>> dateRangeLastModifiedFileCount = dateRangeApiController.findDateRangeLastModifiedFileCount(1l, null);
        Assert.assertThat("there should be at least one result", dateRangeLastModifiedFileCount.getContent().size(), Matchers.greaterThan(0));
        long numberOfFiles = 0;
        long numberOfFilesUnFiltered = 0;
        for (AggregationCountItemDTO<DateRangeItemDto> dateRangeItemDtoAggregationCountItemDTO : dateRangeLastModifiedFileCount.getContent()) {
            logger.debug("{}: {}", dateRangeItemDtoAggregationCountItemDTO.getItem(), dateRangeItemDtoAggregationCountItemDTO.getCount());
            numberOfFiles = numberOfFiles + dateRangeItemDtoAggregationCountItemDTO.getCount();
            numberOfFilesUnFiltered = numberOfFilesUnFiltered + dateRangeItemDtoAggregationCountItemDTO.getItem().getTotalCount();
            DateRangePointDto end = dateRangeItemDtoAggregationCountItemDTO.getItem().getEnd();
            if (end != null) {
                Assert.assertThat(end.getAbsoluteTime(), Matchers.greaterThan(0l));
            }
        }
        Assert.assertThat("there should be at least one file ", numberOfFiles, Matchers.greaterThan(0l));
        Assert.assertThat("there should be at least one file in total count", numberOfFilesUnFiltered, Matchers.greaterThan(0l));
    }

    @Test
    public void testFindDateRangeCreatedFileCount() {
        testStartLog(logger);
        FacetPage<AggregationCountItemDTO<DateRangeItemDto>> dateRangeLastModifiedFileCount = dateRangeApiController.findDateRangeCreatedFileCount(1l, null);
        for (AggregationCountItemDTO<DateRangeItemDto> dateRangeItemDtoAggregationCountItemDTO : dateRangeLastModifiedFileCount.getContent()) {
            DateRangeItemDto item = dateRangeItemDtoAggregationCountItemDTO.getItem();
            logger.debug("{}: {}", item, dateRangeItemDtoAggregationCountItemDTO.getCount());
            Map<String, String> params = new HashMap<>();
            params.put(DataSourceRequest.BASE_FILTER_FIELD, FileDtoFieldConverter.DtoFields.creationDate.toString());
            params.put(DataSourceRequest.BASE_FILTER_VALUE, String.valueOf(item.getId()));
            filesApiController.findFilesDeNormalized(params);
        }
    }

    @Test
    public void testCreateNewDateRangePartition() {
        testStartLog(logger);
        DateRangePartitionDto dateRangePartitionDto  = new DateRangePartitionDto();
        dateRangePartitionDto.setDescription("testCreateNewDateRangePartition");
        dateRangePartitionDto.setName("testCreateNewDateRangePartition");
        List<DateRangeItemDto> dateRangeItemDtos = new ArrayList<>();

        long startOf1990 = LocalDateTime.of(1990, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;
        dateRangeItemDtos.add(new DateRangeItemDto("1990",new DateRangePointDto(startOf1990),null));
        long startOf2015 = LocalDateTime.of(2015, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;
        dateRangeItemDtos.add(new DateRangeItemDto("2015",new DateRangePointDto(startOf2015),null));
        long startOf2016 = LocalDateTime.of(2016, 1, 1, 0, 0).toEpochSecond(ZoneOffset.UTC) * 1000;
        dateRangeItemDtos.add(new DateRangeItemDto("2016",new DateRangePointDto(startOf2016),null));

        dateRangePartitionDto.setDateRangeItemDtos(dateRangeItemDtos);
        DateRangePartitionDto datePartition = dateRangeApiController.createDatePartition(dateRangePartitionDto);
        logger.info("Verify that date partition was created as intended");
        Assert.assertThat(datePartition.getDateRangeItemDtos().size(),Matchers.equalTo(dateRangeItemDtos.size()));
        Assert.assertNotNull(datePartition.getId());

        logger.info("Verify that date partition is usable");
        FacetPage<AggregationCountItemDTO<DateRangeItemDto>> dateRangeLastModifiedFileCount = dateRangeApiController.findDateRangeLastModifiedFileCount(datePartition.getId(), null);
        Assert.assertThat(dateRangeLastModifiedFileCount.getContent().size(),Matchers.greaterThan(0));
        for (AggregationCountItemDTO<DateRangeItemDto> dateRangeItemDtoAggregationCountItemDTO : dateRangeLastModifiedFileCount.getContent()) {
            logger.debug(dateRangeItemDtoAggregationCountItemDTO.toString());
        }

        logger.info("verify that delete date range partition works");
        dateRangeApiController.deleteDateRangePartition(datePartition.getId());
    }

    @Test
    public void testCreateNewDateRangePartitionContinuous() {
        testStartLog(logger);
        DateRangePartitionDto dateRangePartitionDto  = new DateRangePartitionDto();
        dateRangePartitionDto.setContinuousRanges(true);
        dateRangePartitionDto.setDescription("testCreateNewDateRangePartition");
        dateRangePartitionDto.setName("testCreateNewDateRangePartition");
        List<DateRangeItemDto> dateRangeItemDtos = new ArrayList<>();

        DateRangePointDto startOfTime = new DateRangePointDto();
        startOfTime.setType(DateRangePointType.EVER);
        dateRangeItemDtos.add(new DateRangeItemDto("Today",new DateRangePointDto(TimePeriod.DAYS,0),null));
        dateRangeItemDtos.add(new DateRangeItemDto("Last 3 months",new DateRangePointDto(TimePeriod.MONTHS,3),null));
        dateRangeItemDtos.add(new DateRangeItemDto("Last Year",new DateRangePointDto(TimePeriod.YEARS,1),null));
        dateRangeItemDtos.add(new DateRangeItemDto("Start of time",startOfTime,null));
        dateRangePartitionDto.setDateRangeItemDtos(dateRangeItemDtos);
        DateRangePartitionDto datePartition = dateRangeApiController.createDatePartition(dateRangePartitionDto);
        logger.info("Verify that date partition was created as intended");
        Assert.assertThat(datePartition.getDateRangeItemDtos().size(),Matchers.equalTo(dateRangeItemDtos.size()));
        Assert.assertNotNull(datePartition.getId());

        logger.info("Verify that date partition is usable");
        FacetPage<AggregationCountItemDTO<DateRangeItemDto>> dateRangeLastModifiedFileCount = dateRangeApiController.findDateRangeLastModifiedFileCount(datePartition.getId(), null);
        Assert.assertThat(dateRangeLastModifiedFileCount.getContent().size(),Matchers.greaterThan(0));
        for (AggregationCountItemDTO<DateRangeItemDto> dateRangeItemDtoAggregationCountItemDTO : dateRangeLastModifiedFileCount.getContent()) {
            logger.debug(dateRangeItemDtoAggregationCountItemDTO.toString());
        }

        logger.info("verify that delete date range partition works");
        dateRangeApiController.deleteDateRangePartition(datePartition.getId());
    }

}
