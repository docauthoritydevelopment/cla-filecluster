package com.cla.test.filecluster.service.reports;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.report.*;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.api.SummaryReportsApiController;
import com.cla.filecluster.service.api.SummaryReportsApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.ReportingTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.report.ReportItems.*;
import static org.junit.Assert.fail;

/**
 * Created by uri on 08/10/2015.
 */
@Category(ReportingTestCategory.class)
public class SummaryReportTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(SummaryReportTests.class);

    @Autowired
    private SummaryReportsApiController summaryReportsApiController;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private SummaryReportsApplicationService summaryReportsApplicationService;

    private RootFolderDto rootFolderDto;

    @Before
    public void scanBaseDir() {
        logger.info("scanBaseDir");
        rootFolderDto = rootFolderDtoAnalyze;
    }

    @Test
    public void testScanStatistics() throws Exception {
        logger.info("testScanStatistics");

        try {
            SummaryReportDto scanStatistics = summaryReportsApiController.getScanStatistics();
            List<CounterReportDto> counterReportDtos = scanStatistics.getCounterReportDtos();

            CounterReportDto scannedFilesCounter = performanceTestService.findCounterReport(counterReportDtos, ReportItems.SCANNED_FILES);
            Assert.assertThat("Verify scanned files counter >0", scannedFilesCounter.getCounter(), Matchers.greaterThan(0L));

            CounterReportDto analyzedFilesCounter = performanceTestService.findCounterReport(counterReportDtos, ReportItems.ANALYZED_FILES);
            Assert.assertThat("Verify Analyzed files counter >0", analyzedFilesCounter.getCounter(), Matchers.greaterThan(0L));

            logger.info("Find counter reports distinct owners");
            CounterReportDto distinctOwners = performanceTestService.findCounterReport(counterReportDtos, ReportItems.DISTINCT_OWNERS);
            Assert.assertThat("Verify distinct owner counter >0", distinctOwners.getCounter(), Matchers.greaterThan(0L));

            CounterReportDto distinctAclRead = performanceTestService.findCounterReport(counterReportDtos, ReportItems.DISTINCT_ACL_READ);
            Assert.assertThat("Verify acl read counter >0", distinctAclRead.getCounter(), Matchers.greaterThan(0L));

            logger.info("testScanStatistics - finished");
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAggregatedFileSize() {
        logger.info("testAggregatedFileSize");
        Pair<Double, Long> aggFileSize = summaryReportsApiController.getAggregatedFileSize();
        logger.info("testAggregatedFileSize - finished {} / {}", aggFileSize.getKey(), aggFileSize.getValue());
        Assert.assertNotNull("Agg size should not be null", aggFileSize.getKey());
        Assert.assertNotNull("File count not be null", aggFileSize.getValue());
    }

    @Test
    public void testExcelSheetsStats() {
        logger.info("testExcelSheetsStats");
        List<CounterReportDto> excelStats = summaryReportsApiController.getExcelStats();
        logger.info("testExcelSheetsStats {}",excelStats);
        logger.info("testExcelSheetsStats - finished {}", excelStats.stream().map(c->"["+c.getName()+":"+c.getCounter()+"]").collect(Collectors.joining(",")));
        Assert.assertTrue("Total sheets should be positive", excelStats.stream()
                .filter(c->EXCEL_SHEETS_AVG.equals(c.getName())).findFirst().get().getCounter() > 0);
        Assert.assertTrue("Total cells should be positive", excelStats.stream()
                .filter(c->EXCEL_CELLS_AVG.equals(c.getName())).findFirst().get().getCounter() > 0);
        Assert.assertTrue("Max sheets should be positive", excelStats.stream()
                .filter(c->EXCEL_SHEETS_MAX.equals(c.getName())).findFirst().get().getCounter() > 0);
        Assert.assertTrue("Max cells should be positive", excelStats.stream()
                .filter(c->EXCEL_CELLS_MAX.equals(c.getName())).findFirst().get().getCounter() > 0);
    }

    @Test
    public void testScanConfiguration() {
        logger.info("testScanConfiguration");
        SummaryReportDto scanConfiguration = summaryReportsApiController.getScanConfiguration();

        StringReportDto stringReport = performanceTestService.findStringReport(scanConfiguration, ReportItems.SCAN_USER_ACCOUNT);
        Assert.assertThat("Verify scan user is not null/empty", stringReport.getValue(), Matchers.not(Matchers.isEmptyOrNullString()));

        StringListReportDto rootList = performanceTestService.findStringListReport(scanConfiguration, ReportItems.SCANNED_ROOT_FOLDERS);
        Assert.assertNotNull("Verify root list returned", rootList);
        String rootFolderPath = FileNamingUtils.convertFromWindowsToUnix(rootFolderDto.getPath());
        Assert.assertThat("Verify scan root folder contains the root path "+rootFolderPath, rootList.getStringList(), Matchers.hasItem(rootFolderPath));

        logger.info("testScanConfiguration - finished");
    }

    @Test
    public void testScanCriteria() {
        logger.info("testScanCriteria");
        SummaryReportDto scanCriteria = summaryReportsApiController.getScanCriteria();
        MultiValueListDto multiValueListDto = scanCriteria.getMultiValueListDto();

        Assert.assertNotNull("Verify entityLists returned", multiValueListDto);


        Assert.assertThat("Verify entityLists has more then 0 items", multiValueListDto.getData().size(), Matchers.greaterThan(0));

    }

    @Test
    public void testGroupSizeHistogram() {
        logger.info("TestGroupSizeHistogram");
        MultiValueListDto groupSizeHistogram = summaryReportsApiController.getGroupSizeHistogram(false);
        Assert.assertThat("Verify we got at least one item in the histogram",groupSizeHistogram.getData().size(),Matchers.greaterThan(0));
        for (CounterReportDto counterReportDto : groupSizeHistogram.getData()) {
            logger.debug(counterReportDto.getName());
        }

        MultiValueListDto groupSizeFilesHistogram = summaryReportsApiController.getGroupSizeHistogram(true);
        Assert.assertThat("Verify we got at least one item in the histogram",groupSizeFilesHistogram.getData().size(),Matchers.greaterThan(0));
        for (CounterReportDto counterReportDto : groupSizeFilesHistogram.getData()) {
            logger.debug(counterReportDto.getName());
        }
    }

    @Test
    public void testFileTypeHistogram() throws Exception {
        logger.info("testFileTypeHistogram");
        try {
            MultiValueListDto fileTypesHistogram = summaryReportsApiController.getFileTypesHistogram(null);
            Assert.assertThat("Verify we got at least one item in the histogram",fileTypesHistogram.getData().size(),Matchers.greaterThan(0));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFileAccessDateHistogram() throws Exception {
        logger.info("testFileAccessDateHistogram");
        try {
            MultiValueListDto histogram = summaryReportsApiController.getFileAccessDateHistogram(null);
            Assert.assertThat("Verify we got at least one item in the histogram",histogram.getData().size(),Matchers.greaterThan(0));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAggregatedProcessedStats() {
        List<CounterReportDto> stats = summaryReportsApplicationService.createExcelStats();
        summaryReportsApplicationService.addAggregatedProcessedFilesSize(stats);
        long excelCellsMax = stats.stream().filter(s->s.getName().equals(ReportItems.EXCEL_CELLS_AVG))
                .map(CounterReportDto::getCounter).findFirst().orElse(-1L);
        long ingestedFiles = stats.stream().filter(s->s.getName().equals(ReportItems.PROCESSED_FILES_COUNT))
                .map(CounterReportDto::getCounter).findFirst().orElse(-1L);
        long analyzableDocs = stats.stream().filter(s->s.getName().equals(ReportItems.PROCESSED_FILES_COUNT_DOCUMENTS))
                .map(CounterReportDto::getCounter).findFirst().orElse(-1L);
        logger.debug("testAggregatedProcessedStats: excelCellsMax={}, ingestedFiles={}, analyzableDocs={}", excelCellsMax, ingestedFiles, analyzableDocs);
        Assert.assertTrue("Stats basic sanity checks", excelCellsMax > 50 && analyzableDocs > 5 && ingestedFiles >= analyzableDocs); // yael excelCellsMax was > 5000 but kept failing on and off
    }
}
