package com.cla.test.filecluster.policy;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.event.EventType;
import com.cla.filecluster.policy.PolicyMappings;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;
import java.util.UUID;

@Category(UnitTestCategory.class)
public class PolicyMappingsTest {

    @Test
    public void testPolicyMappings() {
        List<PolicyConfig> policies = Lists.newArrayList(
                dummyPolicy(EventType.SCAN_DIFF_UPDATED),
                dummyPolicy(EventType.SCAN_DIFF_DELETED, EventType.SCAN_DIFF_UPDATED),
                dummyPolicy(EventType.SCAN_DIFF_DELETED),
                dummyPolicy(EventType.SCAN_DIFF_UNDELETED));
        PolicyMappings policyMappings = new PolicyMappings();
        policyMappings.loadPolicies(policies);
        Assert.assertEquals(2, policyMappings.getPolicies(EventType.SCAN_DIFF_UPDATED).size());
        Assert.assertEquals(2, policyMappings.getPolicies(EventType.SCAN_DIFF_DELETED).size());
        Assert.assertTrue(policyMappings.getPolicies(EventType.SCAN_DIFF_ACL_UPDATED) == null || policyMappings.getPolicies(EventType.SCAN_DIFF_ACL_UPDATED).isEmpty());
        Assert.assertEquals(1, policyMappings.getPolicies(EventType.SCAN_DIFF_UNDELETED).size());
    }

    private PolicyConfig dummyPolicy(EventType... eventTypes) {
        PolicyConfig policyConfig = new PolicyConfig();
        policyConfig.setName(UUID.randomUUID().toString());
        policyConfig.setRelevantEventTypes(Lists.newArrayList(eventTypes));
        return policyConfig;
    }
}
