package com.cla.test.filecluster.service.files.file;

import com.cla.common.allocation.ExcelMemoryAllocationStrategy;
import com.cla.common.allocation.PdfMemoryAllocationStrategy;
import com.cla.common.domain.dto.FileMediaType;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.domain.dto.mediaproc.word.WordFile;
import com.cla.common.media.files.pdf.ClaNativePdfExtractor;
import com.cla.common.media.files.word.ClaWord6Extractor;
import com.cla.common.media.files.word.ClaWordXmlExtractor;
import com.cla.common.utils.signature.ContentAndSignature;
import com.cla.common.utils.signature.ContentSignHelper;
import com.cla.common.utils.tokens.DictionaryHashedTokensConsumerImpl;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.common.media.files.Extractor;
import com.cla.common.media.files.excel.streaming.ExcelStreamingFilesExtractor;
import com.cla.common.media.files.pdf.ClaPdfExtractor;
import com.cla.common.media.files.word.ClaWordExtractor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Tests user content signature.
 * When the file metadata changes, but the actual user content does not -
 * verify that the global file signature has changed, but the user content signature stayed the same
 *
 * Created by vladi on 1/2/2017.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
@Category(UnitTestCategory.class)
public class UserContentSigTest {

    private Logger logger = LoggerFactory.getLogger(UserContentSigTest.class);
    private static final String BASE_PATH = "./src/test/resources/files/";
    private static final String EMPTY_SIG = Base64.encodeBase64String(DigestUtils.sha1("".getBytes()));

    private ContentSignHelper signHelper = new ContentSignHelper(25000000);

    private NGramTokenizerHashed nGramTokenizerHashed;
    private ClaPdfExtractor claPdfExtractor;
    private ClaWordExtractor claWordExtractor;
    private ExcelStreamingFilesExtractor excelStreamingFilesExtractor;
    private DictionaryHashedTokensConsumerImpl hashedTokensConsumer;

    @Before
    public void init() {
        nGramTokenizerHashed = new NGramTokenizerHashed();

        List<String> removalStrs = new ArrayList<>();
        removalStrs.add("999999999");

        ReflectionTestUtils.setField(nGramTokenizerHashed, "removalStrs", removalStrs);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "ignoreChars", "[\\-\\']");
        ReflectionTestUtils.setField(nGramTokenizerHashed, "separatorChars", "");
        ReflectionTestUtils.setField(nGramTokenizerHashed, "minValueLength", 3);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "reuseTextTokenizer", true);
        ReflectionTestUtils.setField(nGramTokenizerHashed, "removeValuesBellowLength", "2");

        hashedTokensConsumer = new DictionaryHashedTokensConsumerImpl(new HashSet<>(), true);

        claPdfExtractor = getClaPdfExtractor();
        claWordExtractor = getClaWordExtractor();
        excelStreamingFilesExtractor = getExcelStreamingFilesExtractor();

        WordFile.setNGramTokenizer(nGramTokenizerHashed);
        ExcelParseParams.setNGramTokenizer(nGramTokenizerHashed);
    }

    @Test
    public void testWordXmlSignatures() {
        String path1 = BASE_PATH + "analyzeTest/LoremIpsum.docx";
        String path2 = BASE_PATH + "analyzeTest/LoremIpsumMDChanged.docx";

        testSignatures(claWordExtractor, path1, path2);
    }

    @Test
    public void testWord6Signatures() {
        String path1 = BASE_PATH + "analyzeTest/LoremIpsum.doc";
        String path2 = BASE_PATH + "analyzeTest/LoremIpsumMDChanged.doc";

        testSignatures(claWordExtractor, path1, path2);
    }

    @Test
    public void testPdfSignatures() {
        String path1 = BASE_PATH + "analyzeTest/LoremIpsum.pdf";
        String path2 = BASE_PATH + "analyzeTest/LoremIpsumMDChanged.pdf";

        testSignatures(claPdfExtractor, path1, path2);
    }

    @Test
    public void testExcelSignatures() {
        String path1 = BASE_PATH + "analyzeTest/LoremIpsum.xlsx";
        String path2 = BASE_PATH + "analyzeTest/LoremIpsumMDChanged.xlsx";

        testSignatures(excelStreamingFilesExtractor, path1, path2);
    }

    private void testSignatures(Extractor extractor, String path1, String path2) {
        ClaFilePropertiesDto claFilePropertiesDto1 = extractFileProperties(extractor, path1);
        logger.debug("Got ClaFile propertiesDto1: {}",claFilePropertiesDto1);
        ClaFilePropertiesDto claFilePropertiesDto2 = extractFileProperties(extractor, path2);
        logger.debug("Got ClaFile propertiesDto2: {}",claFilePropertiesDto2);
        assertNotEquals(claFilePropertiesDto1.getContentSignature(), claFilePropertiesDto2.getContentSignature());
        assertNotNull(claFilePropertiesDto1.getUserContentSignature());
        assertNotNull(claFilePropertiesDto2.getUserContentSignature());
        assertNotEquals(EMPTY_SIG, claFilePropertiesDto1.getUserContentSignature());
        assertNotEquals(EMPTY_SIG, claFilePropertiesDto2.getUserContentSignature());
        assertEquals(claFilePropertiesDto1.getUserContentSignature(), claFilePropertiesDto2.getUserContentSignature());
    }

    private ClaFilePropertiesDto extractFileProperties(Extractor extractor, String path) {
        Optional<ClaFilePropertiesDto> props1 = FileShareMediaUtils.getMediaProperties(path, true, true);
        Assert.assertTrue("Failed to get file properties.", props1.isPresent());
        ClaFilePropertiesDto props = props1.get();

        logger.debug("Read ClaFileProperties result: {}", props);
        try(
                FileInputStream fileExtractInputStream = new FileInputStream(path);
                FileInputStream fileSignInputStream = new FileInputStream(path)
        ) {
            ContentAndSignature contAndSign = signHelper.readAndSign(fileSignInputStream, props.getFileSize().intValue(),
                    props.getFileName());
            props.setContentSignature(contAndSign.getSignature());
            extractor.extract(fileExtractInputStream, path, true, props);
            logger.debug("Extractor file properties result: {}", props);
        }
        catch (IOException e) {
            logger.error("IO Exception while extracting file properties from {}",path,e);
            throw new RuntimeException("IO Exception while extracting file properties from "+path,e);
        }

        return props;
    }

    private ClaWordExtractor getClaWordExtractor() {
        return new ClaWordExtractor() {
            @Override
            protected ClaWord6Extractor createClaWord6Extractor() {
                ClaWord6Extractor e = new ClaWord6Extractor();
                ReflectionTestUtils.setField(e, "bodyTextNgramSize", (short)5);
                ReflectionTestUtils.setField(e, "nGramTokenizerHashed", nGramTokenizerHashed);
                ReflectionTestUtils.setField(e, "userContentSignatureEnabled", true);
                ReflectionTestUtils.setField(e, "isTokenMatcherActive", true);
                ReflectionTestUtils.setField(e, "tableRowForTitleExtraction", 4);
                ReflectionTestUtils.setField(e, "shouldCleanUnprintableChars", true);
                ReflectionTestUtils.setField(e, "headingsTextNgramSize", (short)3);
                ReflectionTestUtils.setField(e, "maxHeadingLength", 85);
                ReflectionTestUtils.setField(e, "minHeadingLength", 5);
                ReflectionTestUtils.setField(e, "paragraphLabelMaxTokens", 4);
                ReflectionTestUtils.setField(e, "titleExtractionIgnoreRightJustified", true);
                ReflectionTestUtils.setField(e, "titleExtractionMaxCount", 10);
                ReflectionTestUtils.setField(e, "isGetFontSizeStats", false);
                ReflectionTestUtils.setField(e, "wordDocStartWordsLimit", 1000);
                ReflectionTestUtils.setField(e, "wordDocStartParagraphsLimit", 30);
                ReflectionTestUtils.setField(e, "wordIngestionWordsLimit", 0);
                ReflectionTestUtils.setField(e, "wordIngestionParagraphsLimit", 0);
                ReflectionTestUtils.setField(e, "patternSearchActive", true);
                ReflectionTestUtils.setField(e, "useFormattedPatterns", false);
                e.setHashedTokensConsumer(hashedTokensConsumer);
                return e;
            }

            @Override
            protected ClaWordXmlExtractor createClaWordXmlExtractor() {
                ClaWordXmlExtractor e = new ClaWordXmlExtractor();
                ReflectionTestUtils.setField(e, "bodyTextNgramSize", (short)5);
                ReflectionTestUtils.setField(e, "nGramTokenizerHashed", nGramTokenizerHashed);
                ReflectionTestUtils.setField(e, "patternSearchActive", true);
                ReflectionTestUtils.setField(e, "useFormattedPatterns", false);
                ReflectionTestUtils.setField(e, "headingsTextNgramSize", (short)3);
                ReflectionTestUtils.setField(e, "maxHeadingLength", 85);
                ReflectionTestUtils.setField(e, "minHeadingLength", 5);
                ReflectionTestUtils.setField(e, "paragraphLabelMaxTokens", 4);
                ReflectionTestUtils.setField(e, "titleExtractionIgnoreRightJustified", true);
                ReflectionTestUtils.setField(e, "titleExtractionMaxCount", 10);
                ReflectionTestUtils.setField(e, "debTitleExtractionAddStyleToken", false);
                ReflectionTestUtils.setField(e, "isGetFontSizeStats", false);
                ReflectionTestUtils.setField(e, "tableRowForTitleExtraction", 4);
                ReflectionTestUtils.setField(e, "wordDocStartWordsLimit", 10);
                ReflectionTestUtils.setField(e, "wordDocStartParagraphsLimit", 30);
                ReflectionTestUtils.setField(e, "wordIngestionWordsLimit", 0);
                ReflectionTestUtils.setField(e, "xmlFilteringSizeThreshold", 1000000);
                ReflectionTestUtils.setField(e, "xmlFilteringMaxObjects", 900000);
                ReflectionTestUtils.setField(e, "wordIngestionParagraphsLimit", 0);
                ReflectionTestUtils.setField(e, "userContentSignatureEnabled", true);
                ReflectionTestUtils.setField(e, "isTokenMatcherActive", true);

                e.setHashedTokensConsumer(hashedTokensConsumer);
                return e;
            }

        };
    }

    private ClaPdfExtractor getClaPdfExtractor() {
        return new ClaPdfExtractor() {
            @Override
            protected ClaNativePdfExtractor createClaPdfExtractor() {
                ClaNativePdfExtractor e = new ClaNativePdfExtractor();
                ReflectionTestUtils.setField(e, "bodyTextNgramSize", (short)5);
                ReflectionTestUtils.setField(e, "nGramTokenizerHashed", nGramTokenizerHashed);
                ReflectionTestUtils.setField(e, "pdfMaxFileSize", 52428800);
                ReflectionTestUtils.setField(e, "pdfIgnoreMaxFileSize", false);
                ReflectionTestUtils.setField(e, "userContentSignatureEnabled", true);
                ReflectionTestUtils.setField(e, "patternSearchActive", true);
                ReflectionTestUtils.setField(e, "isTokenMatcherActive", true);
                e.setHashedTokensConsumer(hashedTokensConsumer);

                PdfMemoryAllocationStrategy allocationStrategy = new PdfMemoryAllocationStrategy() {
                    public int getOverheadMemoryAllocationMB() {
                        return 0;
                    }
                    public boolean pageMemAllocationActive(){
                        return false;
                    }
                };


                ReflectionTestUtils.setField(e, "allocationStrategy", allocationStrategy);
                return e;
            }

        };
    }

    private ExcelStreamingFilesExtractor getExcelStreamingFilesExtractor() {
        ExcelStreamingFilesExtractor e = new ExcelStreamingFilesExtractor();
        ReflectionTestUtils.setField(e, "bufferedInputStreamSize", 100000);
        ReflectionTestUtils.setField(e, "useTableHeadingsAsTitles", true);
        ReflectionTestUtils.setField(e, "singleTokenNumberedTitleRateReduction", 3);
        ReflectionTestUtils.setField(e, "patternSearchActive", true);
        ReflectionTestUtils.setField(e, "useFormattedPatterns", false);
        ReflectionTestUtils.setField(e, "userContentSignatureEnabled", true);
        ReflectionTestUtils.setField(e, "isTokenMatcherActive", true);
        ReflectionTestUtils.setField(e, "maxExcelFileSize", 0);
        ReflectionTestUtils.setField(e, "excelMaxRecordsCount", 400000);
        ReflectionTestUtils.setField(e, "excelRecordFilteringThreshold", 5000);
        ReflectionTestUtils.setField(e, "filterRecordsFromWorkBookBeforeCreation", true);
        ReflectionTestUtils.setField(e, "xmlFilteringSizeThreshold", 1000000);
        ReflectionTestUtils.setField(e, "xmlFilteringMaxObjects", 1000000);
        String[] ignorePrefix = {"sheet","\u05D2\u05DC\u05D9\u05D5\u05DF"};
        ReflectionTestUtils.setField(e, "ignoredSheetNamePrefixesAsTitles", ignorePrefix);

        ExcelMemoryAllocationStrategy allocationStrategy = new ExcelMemoryAllocationStrategy() {
            public int calculateRequiredMemory(long fileSize, FileMediaType fileMediaType, String path,
                                               OPCPackage opcPackage, ExcelWorkbook excelWorkbook){
                return 0;
            }

            public int calculateBaseRequiredMemory(long fileSize, FileMediaType fileMediaType) {
                return 0;
            }
        };
        ReflectionTestUtils.setField(e, "allocationStrategy", allocationStrategy);
        e.init();

        return e;
    }

}
