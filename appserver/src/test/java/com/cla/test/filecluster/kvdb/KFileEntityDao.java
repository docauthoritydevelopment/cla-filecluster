package com.cla.test.filecluster.kvdb;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.eventbus.EventBus;
import com.cla.kvdb.*;

import java.util.HashMap;
import java.util.List;

/**
 * Created by: yael
 * Created on: 9/11/2019
 */
public class KFileEntityDao extends KeyValueFileEntityDao {

    private HashMap<String, FileEntity> keysToFileEntity;

    public KFileEntityDao(KeyValueDaoConfig config, EventBus eventBus, HashMap<String, FileEntity> keysToFileEntity) {
        super(config, eventBus);
        this.keysToFileEntity = keysToFileEntity;
    }

    @Override
    public void init() {

    }

    @Override
    public <T> T executeInTransaction(Long RunId, String storeName, Executable<T> executable) {
        return executable.execute(new KFileEntityStore(keysToFileEntity));
    }

    @Override
    public <T> T executeInReadonlyTransaction(String storeName, Executable<T> executable) {
        return executable.execute(new KFileEntityStore(keysToFileEntity));
    }

    @Override
    public List<String> getAllStoreNames() {
        return null;
    }

    @Override
    public boolean storeExists(String storeName) {
        return true;
    }

    @Override
    public void createStore(String storeName) {
    }

    @Override
    public void iterateInTransaction(Long runId, String storeName, boolean readOnly, KvdbIterable iterable) {

    }

    @Override
    public FileEntityStore openTransaction(String storeName, boolean readOnly) {
        return null;
    }

    @Override
    public void commit() {

    }

    @Override
    public void flush() {

    }

    @Override
    public void revert() {

    }

    @Override
    public void abort() {

    }

    @Override
    public void dropDb() {

    }

    @Override
    public void close() {

    }
}
