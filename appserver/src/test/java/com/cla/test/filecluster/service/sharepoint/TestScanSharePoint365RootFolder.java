package com.cla.test.filecluster.service.sharepoint;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.FileUserApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.media.SharePointApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.SharePointTestCategory;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Category(SharePointTestCategory.class)
public class TestScanSharePoint365RootFolder extends ClaBaseTests {

    public static final String DAUSER1 = "dauser1@docauthority.onmicrosoft.com";
    private static final Logger logger = LoggerFactory.getLogger(TestScanSharePoint365RootFolder.class);

    @Autowired
    private SharePointApiController sharePointApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private FileUserApiController fileUserApiController;

    private SharePointConnectionDetailsDto sharePointConnectionDetailsDto;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @BeforeClass
    public static void beforeClass() {
        printToErr(TestScanSharePoint365RootFolder.class.getName());
    }

    @Before
    public void scanBaseDir() {
        performanceTestService.prepareSqlSchema();
//        referenceRootFolderDto = performanceTestService.analyzeReferenceFiles2();
        logger.info("***********************************************************************");
    }

    @Test
    @Ignore
    public void testScanSharePoint365Site() {
        configureSharePointConnector();
        performanceTestService.printRunBanner();
        List<ServerResourceDto> serverResourceDtos = sharePointApiController.listServerFolders(sharePointConnectionDetailsDto.getId(), null, 1L);
        ServerResourceDto coreSite = serverResourceDtos.get(0);
        RootFolderDto rootFolderDto = configureRootFolder(coreSite.getFullName());
        rootFolderDto.setScannedFilesCountCap(10);
        fileTreeApiController.updateRootFolder(rootFolderDto.getId(),rootFolderDto);
        scanRootFolder(rootFolderDto);

        logger.info("Verify that all folders have mediaItemId");
        Map<String, String> params = performanceTestService.createFilterParams("rootFolderId",String.valueOf(rootFolderDto.getId()));

        logger.info("Check that share point folder was scanned as expected");
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(params);
        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            DocFolderDto item = aggregationCountFolderDto.getItem();
            Assert.assertNotNull("Folder "+item+" is missing mediaItemId",item.getMediaItemId());
        }

        logger.info("Rescan site");
        scanRootFolder(rootFolderDto);


    }

    @Test(timeout = 180000)
    public void testScanSharePoint365RootFolder() {
        configureSharePointConnector();
        performanceTestService.printRunBanner();
        List<ServerResourceDto> serverResourceDtos = sharePointApiController.listServerFolders(sharePointConnectionDetailsDto.getId(), null, 1L);
        ServerResourceDto coreSite = serverResourceDtos.get(0);
        logger.debug("Got coreSite: {}",coreSite);
        serverResourceDtos = sharePointApiController.listServerFolders(sharePointConnectionDetailsDto.getId(), coreSite.getFullName(), 1L);
        String path = null;
        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
            logger.debug("Got server resource: {}",serverResourceDto);
            if (serverResourceDto.getFullName().toLowerCase().contains("basic")) {
                logger.info("Got library: {}",serverResourceDto);
                path = serverResourceDto.getFullName();
            }
        }
        Assert.assertNotNull(path);
        RootFolderDto rootFolderDto = configureRootFolder(path);
        scanRootFolder(rootFolderDto);

        Map<String, String> params = performanceTestService.createFilterParams("rootFolderId",String.valueOf(rootFolderDto.getId()));

        logger.info("Check that share point folder was scanned as expected");
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(params);
        Assert.assertThat("There should be at least one folder with files",foldersFileCount.getContent().size(), Matchers.greaterThan(0));
        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            logger.debug(aggregationCountFolderDto.toString());
        }
        logger.info("***********************************************************************************");
        logger.info("*************************     Scan the rootFolder again   *************************");
        logger.info("***********************************************************************************");
        scanRootFolder(rootFolderDto);

        foldersFileCount = fileTreeApiController.findFoldersFileCount(params);
        Assert.assertThat("There should be at least one folder with files",foldersFileCount.getContent().size(), Matchers.greaterThan(0));

        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            DocFolderDto item = aggregationCountFolderDto.getItem();
            if (StringUtils.isEmpty(item.getRootFolderNickName())) {
                throw new AssertionError("Missing nickName on docFolder");
            }
        }


        logger.info("Verify that we got PDF files");
        Map rootFolderFilterParams = performanceTestService.createFilterParams("rootFolderId", String.valueOf(rootFolderDto.getId()));
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(rootFolderFilterParams);
        List<ClaFileDto> pdfFiles = filesDeNormalized.getContent().stream().filter(f -> f.getBaseName().endsWith("pdf")).collect(Collectors.toList());
        Assert.assertThat("There should be at least one PDF file",pdfFiles.size(),Matchers.greaterThan(0));
        logger.info("Verify that we got the special permissions file");
        Optional<ClaFileDto> floridaFileOptional = pdfFiles.stream().
                filter(f -> f.getRootFolderId() == rootFolderDto.getId()).
                filter(f -> f.getBaseName().equals("Florida Advance Directive.pdf")).findFirst();
        Assert.assertTrue("Verify that the file: [Florida Advance Directive.pdf] exists",floridaFileOptional.isPresent());
        ClaFileDto floridaFile = floridaFileOptional.get();
        logger.debug("testListAclReadFileCount on file {}",floridaFile);
        Map filterParams = performanceTestService.createFilterParams("file.id",String.valueOf(floridaFile.getId()));

        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/Shared%20Documents?RootFolder=%2Fsites%2Ftest%2FShared%20Documents%2FAdmin
    }

private void scanRootFolder(RootFolderDto rootFolderDto) {
        logger.info("Scan rootFolder {}",rootFolderDto);
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(),performanceTestService.createWaitParams());
    }

    private RootFolderDto configureRootFolder(String path) {
        logger.info("Configure RootFolder path {} on media connection {}",path,sharePointConnectionDetailsDto.getId());

        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setCrawlerType(CrawlerType.MEDIA_PROCESSOR);
        rootFolderDto.setPath(path);
        rootFolderDto.setNickName("DocAuthority SharePoint");
        rootFolderDto.setMediaType(MediaType.SHARE_POINT);
        rootFolderDto.setMediaConnectionDetailsId(sharePointConnectionDetailsDto.getId());
        rootFolderDto.setScannedFilesCountCap(200);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, null);
        logger.debug("Configured rootFolder {}",rootFolder);
        return rootFolder;
    }

    private void configureSharePointConnector() {
        logger.info("Configure SharePoint 365 Connector");
        sharePointConnectionDetailsDto = performanceTestService.createSharePoint365ConnectionDetails();
        TestResultDto testResultDto = sharePointApiController.testSharePointConnection(sharePointConnectionDetailsDto.getSharePointConnectionParametersDto(), Optional.empty(), 1L);
        Assume.assumeTrue(testResultDto.isResult());

//        if (!testResultDto.isResult()) {
//            throw new RuntimeException("Failed to connect to sharePoint 365: "+testResultDto.getReason());
//        }
        sharePointConnectionDetailsDto = (SharePointConnectionDetailsDto) sharePointApiController.configureSharePointConnectionDetails(sharePointConnectionDetailsDto);
    }
}
