package com.cla.test.filecluster.service.files.file;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.group.AttachFilesToGroupDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.NewJoinedGroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.util.*;

@Category(FastTestCategory.class)
public class FileGroupOverrideTests extends ClaBaseTests {

   @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private FileCatService fileCatService;

    private Logger logger = LoggerFactory.getLogger(FileGroupOverrideTests.class);

    @Test(expected = BadRequestException.class)
    public void testCreateGroupFromUnGroupedFile() {
        testStartLog(logger);
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId","*","NEQ"));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.info("Got {} un-grouped files",claFileDtos.getContent().size());

        AttachFilesToGroupDto attachFilesToGroupDto = new AttachFilesToGroupDto();
        attachFilesToGroupDto.setGroupName("testCreateGroupFromUnGroupedFile");
        ClaFileDto claFileDto = getFileWithContent(claFileDtos);
        List<Long> fileIds = Lists.newArrayList(claFileDto.getId());
        attachFilesToGroupDto.setFileIds(fileIds);
        GroupDetailsDto groupDetailsDto = filesApiController.attachFilesToGroup(attachFilesToGroupDto, performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        logger.info("Verify that group {} was created",groupDetailsDto);
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId",groupDetailsDto.getGroupDto().getId(),"EQ"));
        PageImpl<ClaFileDto> claFileDtosResult = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.info("Got {} grouped files",claFileDtosResult.getContent().size());
        Assert.assertThat("There should be exactly 1 file in the group", claFileDtosResult.getContent().size(),Matchers.equalTo(1));
        ClaFileDto claFileDto1 = claFileDtosResult.getContent().get(0);
        filesApiController.detachFileFromGroup(Lists.newArrayList(claFileDto1.getId()), performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        PageImpl<ClaFileDto> claFileDtosResult2 = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.info("Got {} grouped files",claFileDtosResult2.getContent().size());
        Assert.assertThat("There should be no files in the group", claFileDtosResult2.getContent().size(),Matchers.equalTo(0));
        ungroupFile(claFileDto.getId(), claFileDto.getContentId(), groupDetailsDto.getGroupDto().getId());
    }

    @Test
    public void testCreateGroupFromUnGroupedFile2() {
        testStartLog(logger);
        solrFileCatRepository.commit();
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId","*","NEQ"));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.info("Got {} un-grouped files",claFileDtos.getContent().size());

        AttachFilesToGroupDto attachFilesToGroupDto = new AttachFilesToGroupDto();
        ClaFileDto claFileDto = getFileWithContent(claFileDtos);
        logger.info("Attaching ungrouped file {} to group",claFileDto);
        List<Long> fileIds = Lists.newArrayList(claFileDto.getId());
        attachFilesToGroupDto.setFileIds(fileIds);
        FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(null);
        GroupDto originalGroup = groupsFileCount.getContent().get(0).getItem();
        String groupId = originalGroup.getId();
        long origFileNum = groupsFileCount.getContent().get(0).getCount();
        attachFilesToGroupDto.setGroupId(groupId);

        logger.info("Attach the file to the group {}",groupsFileCount.getContent().get(0));
        GroupDetailsDto groupDetailsDto = filesApiController.attachFilesToGroup(attachFilesToGroupDto, performanceTestService.createWaitParams());
        scheduledOperationService.runOperations();
        solrFileCatRepository.commit();
        logger.info("Verify that group {} was created",groupDetailsDto);
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId",groupDetailsDto.getGroupDto().getId(),"EQ"));
        PageImpl<ClaFileDto> claFileDtosResult = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.info("Got {} grouped files",claFileDtosResult.getTotalElements());

        // all files with this content were moved
        List<ClaFileDto> fileMoved = filesApplicationService.listFilesOfSpecificContentId(claFileDto.getContentId());

        logger.info("group {} files {} content {} files {}",originalGroup.getId(),origFileNum, claFileDto.getContentId(),fileMoved.size() );

        long expectedSize = origFileNum + fileMoved.size();
        Assert.assertThat("There should be exactly " + expectedSize + " files in the group", claFileDtosResult.getTotalElements(),Matchers.equalTo(expectedSize));

        groupDetailsDto = groupsApiController.findGroupById(groupDetailsDto.getGroupDto().getId());
        Long actualGroupSize = groupDetailsDto.getGroupDto().getNumOfFiles();
        Assert.assertThat("Group size should be " + expectedSize, actualGroupSize, Matchers.equalTo(expectedSize));

        Assert.assertEquals("Group name should be " + originalGroup.getGroupName(), groupDetailsDto.getGroupDto().getGroupName(),originalGroup.getGroupName());
        ungroupFile(claFileDto.getId(), claFileDto.getContentId(), groupDetailsDto.getGroupDto().getId());
    }

    @Test
    public void testDetachFileFromGroup() {
        testStartLog(logger);

        FacetPage<AggregationCountItemDTO<GroupDto>> fileCount = groupsApiController.findGroupsFileCount(null);
        Assert.assertTrue("Expected more than one group", fileCount.getContent().size() > 1);

        GroupDto first = fileCount.getContent().get(0).getItem();
        GroupDto second = fileCount.getContent().get(1).getItem();
        List<String> groupIds = Arrays.asList(first.getId(), second.getId());
        NewJoinedGroupDto newJoinedGroupDto = new NewJoinedGroupDto("a joined group");
        newJoinedGroupDto.setChildrenGroupIds(groupIds);

        GroupDetailsDto groupDetails = groupsApiController.createJoinedGroup(newJoinedGroupDto, new HashMap<>());
        logger.info("Created joined group with id {}", groupDetails.getGroupDto().getId());
        scheduledOperationService.runOperations();

        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId", groupDetails.getGroupDto().getId(),"EQ"));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Long fileIdToBeDetached = claFileDtos.getContent().get(0).getId();

        filesApiController.detachFileFromGroup(Lists.newArrayList(fileIdToBeDetached), null);
        scheduledOperationService.runOperations();
        SolrFileEntity detachedFile = fileCatService.getFileEntity(fileIdToBeDetached);
        String fileUserGroupId = detachedFile.getUserGroupId();
        Assert.assertFalse("Detached file user-group should be different than original user-group",
                fileUserGroupId.equals(first.getId()) || fileUserGroupId.equals(second.getId()));


        fileCount = groupsApiController.findGroupsFileCount(null);
        AggregationCountItemDTO<GroupDto> generatedGroup = fileCount.getContent().stream()
                .filter(aGroup -> fileUserGroupId.equals(aGroup.getItem().getId()))
                .findAny().orElseThrow(() ->new RuntimeException("Couldn't find detached group"));

        int contentFileCount = -1;
        if (detachedFile.getContentId() != null) {
            ContentMetadata cmdOpt = filesDataPersistenceService.getOneById(Long.parseLong(detachedFile.getContentId()));
            if (cmdOpt != null) {
                contentFileCount = cmdOpt.getFileCount();
            }
        }

        Assert.assertEquals(generatedGroup.getCount(), contentFileCount);
        groupsApiController.deleteJoinedGroup(groupDetails.getGroupDto().getId(), null);
        scheduledOperationService.runOperations();
    }
}
