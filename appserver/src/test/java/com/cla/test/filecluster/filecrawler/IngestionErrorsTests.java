package com.cla.test.filecluster.filecrawler;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.cla.filecluster.service.api.ErrorApiController;
import com.cla.filecluster.service.api.UiServicesApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

@Category(FastTestCategory.class)
public class IngestionErrorsTests extends ClaBaseTests{

    private final static Logger logger = LoggerFactory.getLogger(IngestionErrorsTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private ErrorApiController errorApiController;

    private static RootFolderDto rootFolderDto;

    @Before
    public void scanBaseDir() {
        rootFolderDto = rootFolderDtoAnalyze;
    }

    @Test(timeout = 900000)
    public void testGetIngestionErrors() {
        logger.info("testGetIngestionErrors - start");
        Page<FileProcessingErrorDto> ingestionErrors = errorApiController.getIngestionErrors(null);
        logger.info("Got a total of {} errors",ingestionErrors.getNumberOfElements());
        for (FileProcessingErrorDto ingestionError : ingestionErrors) {
            logger.debug("Ingestion error: "+ingestionError.toString());
        }
    }
}
