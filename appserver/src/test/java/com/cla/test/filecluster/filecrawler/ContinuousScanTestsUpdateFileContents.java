package com.cla.test.filecluster.filecrawler;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.UiServicesApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Maps;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@Category(FastTestCategory.class)
public class ContinuousScanTestsUpdateFileContents extends ClaBaseTests {

    private static final String SCAN_FOLDER_UC = "./build/scan/uc";
    private static final String SAMPLE_FILE_1 = "Julie Simon Document.DOC";
    private static final String SAMPLE_FILE_2 = "Julie Simon Document - Updated.DOC";

    @Autowired
    private PerformanceTestService performanceTestService;
    private Logger logger = LoggerFactory.getLogger(ContinuousScanTestsUpdateFileContents.class);

    private CrawlerType crawlerType = CrawlerType.MEDIA_PROCESSOR;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    private UiServicesApiController uiServicesApiController;

    private static final Map<String, String> DATA_SOURCE_REQUEST_PARAMS = Maps.newHashMap("take", "999");

    @Autowired
    private GroupsApiController groupsApiController;

    private void prepareScanFolder() throws IOException {
        (new File(SCAN_FOLDER_UC)).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, SAMPLE_FILE_2).toAbsolutePath().normalize();
        Path target = Paths.get(SCAN_FOLDER_UC, SAMPLE_FILE_2).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, SAMPLE_FILE_1).toAbsolutePath().normalize();
        target = Paths.get(SCAN_FOLDER_UC, "update.doc").toAbsolutePath().normalize();
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        target = Paths.get(SCAN_FOLDER_UC, "update_acl.doc").toAbsolutePath().normalize();
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
    }

    @Test
    public void testRescanRootFolderUpdateFileContents() throws IOException {
        prepareScanFolder();
        Path target = Paths.get(SCAN_FOLDER_UC, "update.doc").toAbsolutePath().normalize();
        logger.info("testRescanRootFolderUpdateFileContents");
        RootFolderDto rootFolderDto = performanceTestService.analyzeFolder(SCAN_FOLDER_UC, crawlerType);
        overrideFileWithNewContent(target);

        long newSize = Files.size(target);

        rescanFolder(rootFolderDto.getId());

        logger.info("Verify file exists under fullPath: " + rootFolderDto.getPath());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        String filePath = target.toAbsolutePath().normalize().toString();

        verifyFileProperties(newSize, dataSourceRequest, filePath.substring(rootFolderDto.getRealPath().length()));

        printGroupCount();
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(SCAN_FOLDER_UC));
    }

    private void verifyFileProperties(long newSize, DataSourceRequest dataSourceRequest, String filePath) {
        logger.info("Looking for file {} in solr", filePath);
        dataSourceRequest.setFilter(new FilterDescriptor("fullName", filePath, KendoFilterConstants.EQ));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("The file should exist", claFileDtos.getContent().size(), Matchers.greaterThan(0));
        Assert.assertThat("There should be exactly one file in the system by that name",claFileDtos.getContent().size(),Matchers.equalTo(1));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.info("Looking at the properties of file: {}", claFileDto);
        //Assert.assertThat("File size should be the updated size", claFileDto.getFsFileSize(), Matchers.equalTo(newSize));
    }




    private void overrideFileWithNewContent(Path target) throws IOException {
        logger.info("Update file {} and rescan",target);
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_VARIOUS, SAMPLE_FILE_2).toAbsolutePath().normalize();
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
    }

    public void rescanFolder(long rootFolderId) {
        logger.info("Rescan folder {}", rootFolderId);
        Map<String, String> params = new HashMap<>();
        params.put("op", StartScanProducer.SCAN_INGEST_ANALYZE);
        params.put("wait", "true");
        params.put("rootFolderIds", String.valueOf(rootFolderId));
        jobManagerApiController.startScanRootFolders(params);
        //Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY,fileCrawlerApiController.getFileCrawlingState().getLastRunStatus());
        //TODO 2.0 instead of reading fiel crawling state, read the root folder last crawl run status

    }

    public void printGroupCount() {
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        logger.info("Got a total of {} groups", groupDtos.getContent().size());
    }

}
