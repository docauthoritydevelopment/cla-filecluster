package com.cla.test.filecluster.service.files.security;

import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.security.Role;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.entity.security.UserSavedData;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.users.UserApiController;
import com.cla.filecluster.service.security.RoleService;
import com.cla.filecluster.service.security.UserService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.*;

@Category(FastTestCategory.class)
public class UserServiceTest extends ClaBaseTests {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;

    @Autowired
    private UserApiController userApiController;

	@Value("${accountsAdminProps}")
	private String[] accountsAdminProps;

    @Rule
    public ExpectedException thrown = ExpectedException.none();


    private final static Logger logger = LoggerFactory.getLogger(UserServiceTest.class);

	@Test
	public void testUserCRUD() {
		final List<Role> roles = roleService.getRolesWithAuthorities();
        Set<SystemRoleDto> systemRoleDtos = RoleService.convertRolesToSet(roles);
        final List<User> allUsers = userService.getAllUsers();
        UserDto userDto = new UserDto("moshe", "moshe", "111", "admin", systemRoleDtos.toArray(new SystemRoleDto[]{}));
        final User createdUser = userService.createUserOnCurrentAccount(userDto);
		assertEquals(allUsers.size()+1, userService.getAllUsers().size());

        userDto.setName("popo");
        userDto.setId(createdUser.getId());
//		userDto.setUserRole(UserRole.ADMIN);
        userService.updateUser(userDto.getId(), userDto);
		assertEquals("popo",userService.getUserByIdWithRoles(createdUser.getId(),false).getName());
		
		userService.deleteUserById(createdUser.getId());
		assertNull(userService.getUserByIdWithRoles(createdUser.getId(),false));
	}
	
	@Test
	public void testAddRemoveRole() {
		final List<Role> roles = roleService.getRolesWithAuthorities();
		final User user = userService.createUserOnCurrentAccount(new UserDto("kuku111", "kuku111", "111","the role"));
		userService.addRoleToUser(user.getId(),roles.get(0).getId());
		assertTrue(userService.getUserByIdWithRoles(user.getId(),false).getRoles().contains(roles.get(0)));
		
		userService.removeRoleFromUser(user.getId(),roles.get(0).getId());
		assertFalse(userService.getUserByIdWithRoles(user.getId(),false).getRoles().contains(roles.get(0)));
	}
	
	@Test
	public void testChangePassword() {
		final String hashedPassword = userService.getAccountsAdminDto().getPassword();
		userService.changeCurrentUserPassword("12345");
		assertNotEquals(hashedPassword,userService.getAccountsAdminDto().getPassword());
		userService.changeCurrentUserPassword(accountsAdminProps[2]);
	}

    @Test
    public void testUserSavedData() {
        String savedData = "string data";
        userService.addUserSavedData("testUserSavedData", savedData);
        UserSavedData testUserSavedData = userService.getUserSavedData("testUserSavedData");
        Assert.assertEquals(savedData,testUserSavedData.getData());

        String savedData2 = "{\"asb\":\"sdgsdg\"}";
        userService.addUserSavedData("testUserSavedData2", savedData2);
        testUserSavedData = userService.getUserSavedData("testUserSavedData2");
        Assert.assertEquals(savedData2,testUserSavedData.getData());

    }

	@Test
	public void testUserOverridesSavedData() {
		String savedData = "string data";
		userService.addUserSavedData("testUserOverridesSavedData", savedData);
		String savedData2 = "string data 2";
		userService.addUserSavedData("testUserOverridesSavedData", savedData2);
		UserSavedData testUserSavedData = userService.getUserSavedData("testUserOverridesSavedData");
		Assert.assertEquals(savedData2,testUserSavedData.getData());
	}

    @Test
    public void testCreateDeleteUser() {
        testStartLog(logger);
        UserDto userDto = new UserDto("testCreateDeleteUser","testCreateDeleteUser","testCreateDeleteUser","admin");
        User createdUser = userService.createUserOnCurrentAccount(userDto);
        logger.debug("Delete user {}",createdUser);
        userService.archiveUserById(createdUser.getId());
        List<User> allUsers = userService.getAllUsers();
        for (User user: allUsers) {
            if (user.getName().equals(createdUser.getName())) {
                throw new AssertionError("User was not archived successfully");
            }
        }

    }

    @Test
    public void testCreateUserWithSameUserName() {
        testStartLog(logger);

        thrown.expect(BadRequestException.class);
        thrown.expect(hasProperty("type"));
        thrown.expect(hasProperty("type", is(BadRequestType.ITEM_ALREADY_EXISTS)));

        UserDto userDto = new UserDto("testCreateUserWithSameUserName","testCreateUserWithSameUserName","testCreateUserWithSameUserName","admin");
        UserDto userOnCurrentAccount = userApiController.createUserOnCurrentAccount(userDto);
        userApiController.createUserOnCurrentAccount(userDto);
    }
}
