package com.cla.test.filecluster;

import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.producers.FairTaskProducingStrategy;
import com.cla.filecluster.service.producers.SimpleTasksStrategy;
import com.cla.filecluster.service.producers.TaskProducingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

import com.cla.filecluster.Application;

@EnableWebSecurity
@Configuration
@Import(Application.class)
public class TestsConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(TestsConfiguration.class);

    @Value("${analyze.tasksQueueLimit:100}")
    private int tasksQueueLimit;

    @Autowired
    private JobManagerService jobManager;

}
