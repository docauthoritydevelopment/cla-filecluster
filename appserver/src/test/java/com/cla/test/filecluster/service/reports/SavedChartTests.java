package com.cla.test.filecluster.service.reports;

import com.cla.common.domain.dto.report.DisplayType;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.report.UserViewDataService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.ReportingTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;


/**
 * Created by uri on 12/11/2015.
 */
@Category(ReportingTestCategory.class)
public class SavedChartTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(SavedChartTests.class);

    @Autowired
    private UserViewDataService userViewDataService;

    @Test
    public void testModifyChart() {
        UserViewDataDto newChartDto = new UserViewDataDto();
        newChartDto.setName("Test chart");
        newChartDto.setDisplayType(DisplayType.CHART);
        logger.debug("Create a chart by name {}", newChartDto.getName());
        UserViewDataDto savedChart = userViewDataService.createUserViewData(newChartDto);
        Assert.assertThat("Verify new chart has id", savedChart.getId(), Matchers.greaterThanOrEqualTo(0l));
        Assert.assertEquals("Verify new chart has required name", savedChart.getName(), newChartDto.getName());

        logger.debug("Now modify the chart");
        savedChart.setName("Test chart modified");
        UserViewDataDto updatedChart = userViewDataService.updateUserViewData(savedChart);

        Assert.assertEquals("verify new name was updated to object","Test chart modified",updatedChart.getName());

        Page<UserViewDataDto> savedChartDtos = userViewDataService.listUserViewData(DataSourceRequest.build(null));
        logger.debug("Verify updated chart has new name");
        for (UserViewDataDto savedChartDto : savedChartDtos) {
            logger.debug("Chart name: {}",savedChartDto.getName());
            if (savedChartDto.getName().equals("Test chart modified")) {
                return;
            }
        }
        throw new RuntimeException("Test failed - couldn't find a chart by the required name");
    }
}
