package com.cla.test.filecluster.filecrawler;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.service.api.*;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by uri on 15/03/2016.
 */
@Ignore
@Category(FastTestCategory.class)
public class ContinuousScanRemoveFilesTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(ContinuousScanRemoveFilesTests.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private UiServicesApiController uiServicesApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;


    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private KeyValueDatabaseRepository repository;

    private static final String scanRemoveFileFolder = "./build/scan/2";
    private static final String scanFolder3 = "./build/scan/3";

    private static final String sampleFile1 = "Julie Simon Document.DOC";
    private static final String sampleFile2 = "Julie Simon Document - Updated.DOC";

    private static final Map<String,String> dataSourceRequestParams = new HashMap<String, String>() {{ put("take","999"); }};

    private CrawlerType crawlerType = CrawlerType.MEDIA_PROCESSOR;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(45); // 45 seconds max per method tested (30 was not enough)


    @Test
    public void testRescanRootFolderRemoveFile() throws IOException {
        new File(scanRemoveFileFolder).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, sampleFile1).toAbsolutePath().normalize();
        Path target = Paths.get(scanRemoveFileFolder, sampleFile1).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        logger.info("testRescanRootFolderRemoveFile");
        performanceTestService.printRunBanner();
        RootFolderDto rootFolderDto = performanceTestService.analyzeFolder(scanRemoveFileFolder, crawlerType);
        logger.debug("Remove {} doc file", sampleFile1);

        Files.delete(target);

        logger.info("**************************************************************");
        logger.info("**************************************************************");
        logger.info("**************************************************************");

        rescanFolder(rootFolderDto.getId());

        String repoName = repository.getRepoName(rootFolderDto.getId(), "appserver");
        final boolean[] foundDeleted = {false};
        repository.getFileEntityDao().iterateInTransaction(null, repoName, true, iterator -> {
            Pair<String, FileEntity> pair = iterator.next();
            while(pair != null){
                if (pair.getValue().isDeleted()){
                    foundDeleted[0] = true;
                    break;
                }
                pair = iterator.next();
            }
        });
        Assert.assertTrue("Should contain at least one deleted file", foundDeleted[0]);

        String fullName = target.toAbsolutePath().toString();
        logger.info("Verify file doesn't exist: {}", fullName);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("fullName", fullName, KendoFilterConstants.EQ));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.error(claFileDto.toString());
        }

        Assert.assertThat(claFileDtos, Matchers.emptyIterable());
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(scanRemoveFileFolder));
    }



    @Test
    public void testRescanRootFolderRemoveFolderWithFile() throws IOException {
        new File(scanRemoveFileFolder).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, sampleFile1).toAbsolutePath().normalize();
        Path target = Paths.get(scanRemoveFileFolder, sampleFile1).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        logger.debug("Create new doc file under a new folder");
        target = Paths.get(scanRemoveFileFolder + "/newFolder", "remFF" + System.currentTimeMillis() + ".doc");
        new File(scanRemoveFileFolder + "/newFolder").mkdir();
        Files.copy(source, target);

        logger.info("testRescanRootFolderRemoveFolderWithFile");
        performanceTestService.printRunBanner();
        RootFolderDto rootFolderDto = performanceTestService.analyzeFolder(scanRemoveFileFolder, crawlerType);
        logger.info("Remove folder and file");
        Files.delete(target);
        new File(scanRemoveFileFolder + "/newFolder").delete();

        logger.debug("**************************************************************");
        logger.debug("**************************************************************");
        logger.debug("**************************************************************");
        rescanFolder(rootFolderDto.getId());

/*
        logger.info("Verify one new delete event in file change log");
        Page<CrawlFileChangeLogDto> fileChangeLogEvents = fileCrawlerApiController.getFileChangeLogEvents(rootFolderDto.getId(), dataSourceRequestParams);
        Assert.assertThat("Verify at least one processed event in changLog", fileChangeLogEvents.getContent().size(), Matchers.greaterThan(0));
        Optional<CrawlFileChangeLogDto> first = fileChangeLogEvents.getContent().stream()
                .filter(CrawlFileChangeLogDto::isProcessed)
                .filter(f -> f.getCrawlEventType().equals(CrawlEventType.DELETE))
                .findFirst();
        if (!first.isPresent()) {
            fileChangeLogEvents.getContent().forEach(f -> logger.error("Crawl changeLog event: {}", f.toString()));
        }
        Assert.assertTrue("There should be at least one processed DELETE FILE event", first.isPresent());
        logger.debug("ChangeLog event: {}", first.get());
        */

        String fullName = target.toAbsolutePath().toString();
        logger.info("Verify file doesn't exist: {}", fullName);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("fullName", fullName, KendoFilterConstants.EQ));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.error(claFileDto.toString());
        }

        Assert.assertThat(claFileDtos, Matchers.emptyIterable());
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(scanRemoveFileFolder));
    }


    @Test
    public void testRemoveFolderAddAgain() throws IOException {
        new File(scanFolder3).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, sampleFile1).toAbsolutePath().normalize();
        Path target = Paths.get(scanFolder3, sampleFile1).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        logger.debug("Create new doc file under a new folder");
        target = Paths.get(scanFolder3 + "/newFolder", "remAddF" + System.currentTimeMillis() + ".doc");
        new File(scanFolder3 + "/newFolder").mkdir();
        Files.copy(source, target);

        logger.info("testRemoveFolderAddAgain");
        performanceTestService.printRunBanner();
        RootFolderDto rootFolderDto = performanceTestService.analyzeFolder(scanFolder3, crawlerType);
        logger.info("Remove folder and file");
        Files.delete(target);
        new File(scanFolder3 + "/newFolder").delete();
        logger.debug("****************************************************");
        logger.debug("****************************************************");
        logger.debug("****************************************************");

        rescanFolder(rootFolderDto.getId());
        logger.info("Add folder and file again");
        new File(scanFolder3 + "/newFolder").mkdir();
        Files.copy(source, target);
        rescanFolder(rootFolderDto.getId());

        String fullName = target.toAbsolutePath().toString();
        logger.info("Verify file exists: {}", fullName);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("fullName", fullName, KendoFilterConstants.EQ));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat(claFileDtos.getContent().size(), Matchers.greaterThan(0));
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(scanFolder3));
    }

    @Test
    public void testRemoveFileAddAgain() throws IOException {
        performanceTestService.printRunBanner();
        new File(scanFolder3).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, sampleFile1).toAbsolutePath().normalize();
        Path target = Paths.get(scanFolder3, sampleFile1).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        logger.debug("Create new doc file");
        target = Paths.get(scanFolder3, "remAdd" + System.currentTimeMillis() + ".doc");
        Files.copy(source, target);

        logger.info("testRemoveFileAddAgain");
        performanceTestService.printRunBanner();
        RootFolderDto rootFolderDto = performanceTestService.analyzeFolder(scanFolder3, crawlerType);
        logger.info("Remove file");
        Files.delete(target);

        rescanFolder(rootFolderDto.getId());
        logger.info("Add file again");
        Files.copy(source, target);
        logger.debug("**********************************************");
        logger.debug("**********************************************");
        logger.debug("**********************************************");
        rescanFolder(rootFolderDto.getId());

        String fullName = target.toAbsolutePath().toString();
        logger.info("Verify file exists: {}", fullName);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("fullName", fullName, KendoFilterConstants.EQ));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat(claFileDtos.getContent().size(), Matchers.greaterThan(0));
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(scanFolder3));
    }

    public void printGroupCount() {
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        logger.info("Got a total of {} groups", groupDtos.getContent().size());
    }

    public void rescanFolder(long rootFolderId) {
        logger.info("Rescan folder {}", rootFolderId);
        Map<String, String> params = new HashMap<>();
        params.put("op", StartScanProducer.SCAN_INGEST_ANALYZE);
        params.put("wait", "true");
        params.put("rootFolderIds",String.valueOf(rootFolderId));
        jobManagerApiController.startScanRootFolders(params);
      //  Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY,jobManagerApiController.getFileCrawlingState().getLastRunStatus());
    }



}
