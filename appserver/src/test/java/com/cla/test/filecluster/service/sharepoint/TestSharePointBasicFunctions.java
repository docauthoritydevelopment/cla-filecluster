package com.cla.test.filecluster.service.sharepoint;

import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.ClaFilePropertiesPageDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.connector.mediaconnector.microsoft.sharepoint.SharePointMediaConnector;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.media.SharePointApiController;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.media.MediaConnectorFactory;
import com.cla.filecluster.service.media.MicrosoftMediaAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.SharePointTestCategory;
import com.independentsoft.share.ServiceException;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * Created by uri on 07/08/2016.
 */
@SuppressWarnings("OptionalGetWithoutIsPresent")
@Category(SharePointTestCategory.class)
public class TestSharePointBasicFunctions extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(TestSharePointBasicFunctions.class);

    //    public static final String SITES_TEST_SHARED_DOCUMENTS_DEV_SAMPLES = "/sites/test/Shared Documents/dev samples";
//    public static final String SITES_TEST_SHARED_DOCUMENTS_PERSONAL = "/sites/test/Shared Documents/personal";
//    public static final String SITES_TEST_SHARED_DOCUMENTS_DEV_SAMPLES_MCF = "/sites/test/Shared Documents/dev samples/mcf";
//    public static final String SITES_TEST_SHARED_DOCUMENTS = "/sites/test/Shared Documents";
    private static final String SITES_TEST_SHARED_DOCUMENTS_DEV_SAMPLES_DOCX_FILE = "/sites/test/Shared Documents/dev samples/docx/GMI.docx";
    private static final String SITES_TEST_DOCUMENTS_LIBRARY = "/sites/test/Shared Documents";

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private SharePointApiController sharePointApiController;

    @Autowired
    private MicrosoftMediaAppService microsoftMediaAppService;

    @Autowired
    private MediaConnectorFactory mediaConnectorFactory;

    @Autowired
    private MediaConnectionDetailsService detailsService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    private long connectionId;
    private long connection365Id;
    private long connection365MainSiteId;

    private SharePointMediaConnector wrapper;
    private SharePointMediaConnector wrapper365;
    private SharePointMediaConnector wrapper365MainSite;

    @BeforeClass
    public static void beforeClass() {
        printToErr(TestSharePointBasicFunctions.class.getName());
    }


    @Before
    public void setUp() {
        testStartLog(logger);

        SharePointConnectionDetailsDto sharePointConnectionDetails = performanceTestService.createSharePointConnectionDetails();
        sharePointConnectionDetails = (SharePointConnectionDetailsDto) sharePointApiController.configureSharePointConnectionDetails(sharePointConnectionDetails);
        connectionId = sharePointConnectionDetails.getId();

        SharePointConnectionDetailsDto sharePointConnectionDetails2 = performanceTestService.createSharePoint365ConnectionDetails();
        sharePointConnectionDetails2 = (SharePointConnectionDetailsDto) sharePointApiController.configureSharePointConnectionDetails(sharePointConnectionDetails2);
        connection365Id = sharePointConnectionDetails2.getId();

        SharePointConnectionDetailsDto sharePointConnectionDetails3 = performanceTestService.createSharePoint365MainSiteConnectionDetails();
        sharePointConnectionDetails3 = (SharePointConnectionDetailsDto) sharePointApiController.configureSharePointConnectionDetails(sharePointConnectionDetails3);
        connection365MainSiteId = sharePointConnectionDetails3.getId();

        // get the wrappers
        SharePointConnectionDetailsDto details = detailsService.getSharePointConnectionDetailsById(connectionId, false);
        wrapper = mediaConnectorFactory.getMediaConnector(details);

        SharePointConnectionDetailsDto details365 = detailsService.getSharePointConnectionDetailsById(connection365Id, false);
        wrapper365 = mediaConnectorFactory.getMediaConnector(details365);

        SharePointConnectionDetailsDto details365MainSite = detailsService.getSharePointConnectionDetailsById(connection365MainSiteId, false);
        wrapper365MainSite = mediaConnectorFactory.getMediaConnector(details365MainSite);

    }

    @Test
    public void testListSharePoint365ServerFolders() {
        TestResultDto testResultDto = sharePointApiController.testSharePointConnection(null, Optional.of(connection365Id), 1L);
        Assume.assumeTrue(testResultDto.isResult());
        List<ServerResourceDto> serverResourceDtos = sharePointApiController.listServerFolders(connection365Id,null, 1L);
        logger.info("Got list of server resource DTO's:");
        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
            logger.debug("Server Resource: {}", serverResourceDto);
        }

        serverResourceDtos = sharePointApiController.listServerFolders(connection365MainSiteId,null, 1L);
        logger.info("Got list of server resource DTO's for 365 main site:");
        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
            logger.debug("Server Resource: {}", serverResourceDto);
        }

    }

    @Test
    public void testListSharePointServerFolders() {
        testStartLog(logger);
        List<ServerResourceDto> serverResourceDtos = sharePointApiController.listServerFolders(connectionId, null, 1L);
        logger.info("Got list of server resource DTO's:");
        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
            logger.debug("Server Resource: {}", serverResourceDto);
        }
        Assert.assertThat("There should be exactly one response",serverResourceDtos.size(),Matchers.equalTo(1));
        ServerResourceDto serverResourceDto = serverResourceDtos.get(0);
        List<ServerResourceDto> libraries = sharePointApiController.listServerFolders(connectionId,serverResourceDto.getFullName(), 1L);
        logger.info("Got list of libraries DTO's:");
        for (ServerResourceDto library : libraries) {
            logger.debug("Server Resource: {}", library);
        }
        Assert.assertThat("There should be at least one library",libraries.size(),Matchers.greaterThan(0));
        ServerResourceDto sharedDocuments = libraries.stream()
                .filter(f -> f.getFullName()
                        .contains("Shared Documents"))
                .findFirst().get();
        List<ServerResourceDto> folders = sharePointApiController.listServerFolders(connectionId, sharedDocuments.getFullName(), 1L);

        logger.info("Got list of folder DTO's:");
        for (ServerResourceDto folder : folders) {
            logger.debug("Got folder: {}",folder);
        }
        Assert.assertThat("There should be at least one folder",folders.size(),Matchers.greaterThan(0));
    }

    @Test
    public void testListServerFolders() throws FileNotFoundException {
        SharePointConnectionDetailsDto connectionDetails = detailsService.getSharePointConnectionDetailsById(connectionId, false);
        List<ServerResourceDto> serverResourceDtos = microsoftMediaAppService.listFoldersUnderPath(connectionDetails, Optional.empty(), 1L, false);
        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
            if (serverResourceDto.getName().contains("Documents")) {
                String listIdByName = wrapper.getFolderProperties(null, serverResourceDto.getFullName()).getListId();
                logger.info("Got list ID: {} for path {}",listIdByName,serverResourceDto.getFullName());
                List<ServerResourceDto> serverResourceDtos1 = microsoftMediaAppService
                        .listFoldersUnderPath(connectionDetails, Optional.of(serverResourceDto.getFullName()), null, false);
                logger.debug("Got {} folders",serverResourceDtos1.size());
                for (ServerResourceDto resourceDto : serverResourceDtos1) {
                    logger.debug("Got folder: {}",resourceDto);
                }
//                sharePointMediaAppService.getListItemCount(connectionId,serverResourceDto.getName());
            }
        }

    }
//    @Test
//    public void testDownloadFile() {
//        testStartLog(logger);
//        SharePointConnectionParametersDto connectionDetails = performanceTestService.createSharePointConnectionDetails();
//        sharePointApiController.configureMicrosoftConnectionDetails(connectionDetails);
//        sharePointApiController.downloadSharePointFile(SITES_TEST_SHARED_DOCUMENTS_DEV_SAMPLES_DOCX_FILE);
//    }

    /* @Test
     public void testGetFileProperties() {
         ClaFilePropertiesDto claFilePropertiesDto = sharePointApiController.getFileProperties(connectionId,SITES_TEST_SHARED_DOCUMENTS_DEV_SAMPLES_DOCX_FILE);
         logger.info("got file properties: {}",claFilePropertiesDto);
     }
 */
    @Test
    public void testGetListItemsPaged() throws FileNotFoundException {
        String libraryListId = wrapper.getFolderProperties(null, SITES_TEST_DOCUMENTS_LIBRARY).getListId();
        ClaFilePropertiesPageDto claFilePropertiesPageDto = wrapper.listItems(libraryListId, 5, null, null);
        logger.info("Get the first page");
        Assert.assertThat("there should be at least 1 item",claFilePropertiesPageDto.getItems().size(),Matchers.greaterThan(0));
        for (ClaFilePropertiesDto filePropertiesDto : claFilePropertiesPageDto.getItems()) {
            logger.debug(filePropertiesDto.toString());
        }
        logger.info("Get the second page");
        String pageIdentifier = claFilePropertiesPageDto.getNextPageIdentifier();
        claFilePropertiesPageDto = wrapper.listItems(libraryListId,2, pageIdentifier, null);
        Assert.assertThat("there should be at least 1 item",claFilePropertiesPageDto.getItems().size(),Matchers.greaterThan(0));
        for (ClaFilePropertiesDto filePropertiesDto : claFilePropertiesPageDto.getItems()) {
            logger.debug(filePropertiesDto.toString());
        }
    }

//    @Test
//    public void testGetFolderMediaItemId() throws FileNotFoundException, ServiceException {
//        testStartLog(logger);
//        SharePointMediaConnector wrapper = sharePointMediaAppService.createWrapperIfNeeded(connectionId);
//        String s = wrapper.convertFileNameIfNeeded(PerformanceTestService.SITES_TEST_TEST_LIBRARY_SUBFOLDER);
//        String folderMediaItemId = wrapper.getFolderMediaItemId(s);
//    }

    //    @Test
    public void testGetSharePoint365FolderMediaItemId() throws FileNotFoundException, ServiceException {
//        String s = wrapper365.convertFileNameIfNeeded(PerformanceTestService.SHARE_POINT_365_SITE + "/" + PerformanceTestService.SHARE_POINT_365_BASIC_LIBRARY);
//        String folderMediaItemId = wrapper365.getFolderMediaItemId(s);
//        Assert.assertNotNull(folderMediaItemId);
    }

//    @Test
//    public void testListFilesOnSharedFolder() {
//        listFoldersAndFilesRecursive(SITES_TEST_SHARED_DOCUMENTS_DEV_SAMPLES_MCF);
//    }

//    public void listFoldersAndFilesRecursive(String path) {
//        List<ServerResourceDto> sharedFolderDtos = sharePointApiController.listServerFolders(connectionId,path);
//        List<ServerResourceDto> serverResourceDtos = sharePointApiController.listFilesUnderFolder(connectionId,path);
//        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
//            logger.debug("File {} under folder {}",serverResourceDto,path);
//            sharePointMediaAppService.getFileDetails(listId, serverResourceDto.getFullName());
//        }
//
//        for (ServerResourceDto sharedFolderDto : sharedFolderDtos) {
//            logger.debug("Folder: {}",sharedFolderDto);
//            logger.debug("Look for files");
//            if (sharedFolderDto.getHasChildren()) {
//                listFoldersAndFilesRecursive(sharedFolderDto.getFullName());
//            }
//        }
//    }

    @Test
    public void testListSharePointUsers() {
        wrapper.listPrincipals();
    }

//    @Test
//    public void testGetListPermissions() {
//        testStartLog(logger);
//        List<SharePointRoleAssignment> listPermissions = sharePointMediaAppService.getListPermissions(connectionId,"97b2f9ca-6440-4909-9034-48cb272ca838");
//        for (SharePointRoleAssignment listPermission : listPermissions) {
//            logger.debug(listPermission.toString());
//        }
//
//    }

    @Test
    public void testIllegalRootFolderPath() {
        List<MediaConnectionDetailsDto> mediaConnectionDetailsDtoList = fileTreeApiController.listMediaConnectionDetailsOptions(MediaType.SHARE_POINT);
        MediaConnectionDetailsDto mediaConnectionDetailsDto = mediaConnectionDetailsDtoList.get(0);
        Assert.assertNotNull(mediaConnectionDetailsDto.getUrl());
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setCrawlerType(CrawlerType.MEDIA_PROCESSOR);
        rootFolderDto.setPath(mediaConnectionDetailsDto.getUrl());
        rootFolderDto.setMediaType(MediaType.SHARE_POINT);
        rootFolderDto.setMediaConnectionDetailsId(mediaConnectionDetailsDto.getId());
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, null);
        logger.debug("Configured rootFolder {}",rootFolder);

        Page<RootFolderDto> rootFolderDtos = fileTreeApiController.listRootFolders(null);

        Page<RootFolderSummaryInfo> rootFolderSummaryInfos = fileTreeApiController.listRootFoldersSummary(null);

        for (RootFolderDto folderDto : rootFolderDtos.getContent()) {
            if (folderDto.getMediaType().equals(MediaType.SHARE_POINT)) {
                Assert.assertNotNull(folderDto.getMediaConnectionDetailsId());
            }
        }
        for (RootFolderSummaryInfo rootFolderSummaryInfo : rootFolderSummaryInfos.getContent()) {
            if (rootFolderSummaryInfo.getRootFolderDto().getMediaType().equals(MediaType.SHARE_POINT)) {
                Assert.assertNotNull(rootFolderSummaryInfo.getRootFolderDto().getMediaConnectionDetailsId());
            }

        }

    }

    @Test
    @Ignore
    public void testGetLastChangeToken() throws Exception {
        String libraryListId = wrapper.getFolderProperties(null, "Shared Documents").getListId();
        String lastChange = wrapper.getLastChange(null, libraryListId);
        logger.info("Last change is: {}",lastChange);

        logger.info("Now stream all changes and verify last");
        Stream<MediaChangeLogDto> sharePointChangeStream = wrapper.streamSharePointChanges(null, libraryListId, null);
        final MediaChangeLogDto[] realLastChange = {null};
        sharePointChangeStream.forEach(f -> realLastChange[0] = f);
        logger.debug("Real last change is: {}",realLastChange[0]);
    }

    @Test
    @Ignore
    public void testLibraryChanges() throws Exception {
        String libraryListId = wrapper.getFolderProperties(null, "Shared Documents").getListId();
        int count = 5;
        List<MediaChangeLogDto> changes = wrapper.getChanges(null, libraryListId, null, count);
        Assert.assertThat(changes.size(), Matchers.greaterThan(0));
        for (MediaChangeLogDto change: changes) {
            logger.debug(change.toString());
        }
        MediaChangeLogDto lastChange = changes.get(changes.size() - 1);
        logger.info("Get the second page from token: {}",lastChange.getChangeLogPosition());
        List<MediaChangeLogDto> changes2 = wrapper.getChanges(null, libraryListId, lastChange.getChangeLogPosition(),count);
        for (MediaChangeLogDto change: changes2) {
            logger.debug(change.toString());
        }

    }


//    @Test
//    public void testSmallLibraryChanges() throws FileNotFoundException, ServiceException {
//        testStartLog(logger);
//        SharePointMediaConnector wrapper = sharePointMediaAppService.createWrapperIfNeeded(connectionId);
//        String libraryListId = wrapper.getLibraryListId("YAL");
//        List<SharePointChange> changes = wrapper.getChanges(libraryListId, null,100);
//        for (SharePointChange change: changes) {
//            logger.debug(change.toString());
//        }
//    }


//    @Test
//    public void testConnectSharePointDirectly() throws IOException {
//        testStartLog(logger);
//        SharePointMediaConnector wrapper = sharePointMediaAppService.createWrapperIfNeeded();
//        String s = wrapper.sendRequestToSharePoint("/lists('97b2f9ca-6440-4909-9034-48cb272ca838')?$expand=RoleAssignments/Member,RoleAssignments/RoleDefinitionBindings");
//        logger.warn(s);
//    }
}
