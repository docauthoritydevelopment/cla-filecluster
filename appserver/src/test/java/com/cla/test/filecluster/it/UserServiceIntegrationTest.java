package com.cla.test.filecluster.it;

import com.cla.common.domain.dto.security.BizRoleDto;
import com.cla.common.domain.dto.security.PasswordUpdateDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.domain.dto.security.UserState;
import com.cla.filecluster.service.api.users.UserApiController;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

@Category(FastTestCategory.class)
public class UserServiceIntegrationTest extends AbstractIntegrationTest {

    private Logger logger = LoggerFactory.getLogger(UserServiceIntegrationTest.class);

    @Value("${accountsAdminProps}")
    private String[] accountsAdminProps;

    @Autowired
    UserApiController userApiController;

    @Test
    public void testUserNoRoles() {
        UserDto userDto = new UserDto("testUserNoRoles", "testUserNoRoles", "testUserNoRoles");
        UserDto userOnCurrentAccount = userApiController.createUserOnCurrentAccount(userDto);
        RestTemplate restTemplateForUser = createRestTemplateForUser("testUserNoRoles", "testUserNoRoles");
        UserDto result = restTemplateForUser.getForObject(getBaseUrl() + "/api/user/current", UserDto.class);
        assertEquals("testUserNoRoles", result.getName());
    }

    @Test
    public void testDisabledUser() {
        UserDto userDto = new UserDto("testDisabledUser", "testDisabledUser", "testDisabledUser");
        userDto.setUserState(UserState.DISABLED);
        UserDto userOnCurrentAccount = userApiController.createUserOnCurrentAccount(userDto);
        try {
            RestTemplate restTemplateForUser = createRestTemplateForUser("testDisabledUser", "testDisabledUser");
        } catch (HttpClientErrorException e) {
            Assert.assertEquals(HttpStatus.UNAUTHORIZED, e.getStatusCode());
            return;
        }
        throw new AssertionError("An 401 error should have been found");
    }

    @Test
    public void testUserCRUD() {
        final BizRoleDto[] bizRoles = adminRest().getForObject(getBaseUrl() + "/api/user/bizroles", BizRoleDto[].class);

        final String usersUrl = getBaseUrl() + "/api/user";

        ParameterizedTypeReference<PagedResources<UserDto>> responseType = new ParameterizedTypeReference<PagedResources<UserDto>>() {
        };
        ResponseEntity<PagedResources<UserDto>> exchange = adminRest().exchange(usersUrl, HttpMethod.GET, null, responseType);
        PagedResources<UserDto> userDtos = exchange.getBody();

        final UserDto createdUser = new UserDto("moshe", "moshe", "111", "admin");
        createdUser.setBizRoleDtos(Sets.newHashSet(bizRoles));
        adminRest().put(usersUrl + "/new", createdUser);

        ResponseEntity<PagedResources<UserDto>> exchange2 = adminRest().exchange(usersUrl, HttpMethod.GET, null, responseType);
        PagedResources<UserDto> userDtos2 = exchange2.getBody();

//        final UserDto[] allUsers2 = adminRest().getForObject(usersUrl, UserDto[].class);
        final UserDto foundCreatedUser = userDtos2.getContent().stream().filter(u -> createdUser.getName().equals(u.getName())).findAny().orElse(null);
        assertNotNull("new name", foundCreatedUser);

        assertEquals(userDtos.getContent().size() + 1, userDtos2.getContent().size());

        foundCreatedUser.setName("popo");
//		adminRest().put(usersUrl+"/new", createdUser);
        String url = usersUrl + "/" + foundCreatedUser.getId();
        final UserDto user1 = adminRest().postForObject(url, foundCreatedUser, UserDto.class);
        assertEquals("popo", adminRest().getForObject(url, UserDto.class).getName());

        adminRest().delete(url);
        assertNull(adminRest().getForObject(url, UserDto.class));
    }

    @Test
    public void testAddRemoveRole() {
        final String usersUrl = getBaseUrl() + "/api/user";

        final BizRoleDto[] bizRoles = adminRest().getForObject(getBaseUrl() + "/api/user/bizroles", BizRoleDto[].class);

        UserDto userDto = new UserDto("kuku", "kuku", "111", "system");
        userDto.setBizRoleDtos(Sets.newHashSet(bizRoles));
        adminRest().put(usersUrl + "/new", userDto);

        ParameterizedTypeReference<PagedResources<UserDto>> responseType = new ParameterizedTypeReference<PagedResources<UserDto>>() {
        };

        ResponseEntity<PagedResources<UserDto>> exchange = adminRest().exchange(usersUrl, HttpMethod.GET, null, responseType);
        PagedResources<UserDto> userDtos = exchange.getBody();
//        Page<UserDto> userDtos = adminRest().getForObject(usersUrl, typeReference.getClass());
        Collection<UserDto> content = userDtos.getContent();
        final UserDto user = content.stream().filter(u -> u.getName().equals("kuku")).findAny().orElse(null);
        Assert.assertNotNull(user);

        BizRoleDto bizRoleDto = bizRoles[0];
        logger.info("Add role to user");

        String bizRoleUrl = usersUrl + "/" + user.getId() + "/bizrole/" + bizRoleDto.getId();
        adminRest().put(bizRoleUrl, null);
        UserDto userDto1 = adminRest().getForObject(usersUrl + "/" + user.getId(), UserDto.class);
        assertTrue("User " + userDto1 + " should have role " + bizRoleDto, userDto1.getBizRoleDtos().contains(bizRoleDto));
//		
        logger.info("remove role to user");
        adminRest().delete(bizRoleUrl);
        UserDto userDto2 = adminRest().getForObject(usersUrl + "/" + user.getId(), UserDto.class);
        assertFalse("User should no longer have role" + bizRoleDto, userDto2.getBizRoleDtos().contains(bizRoleDto));

        adminRest().delete(usersUrl + "/" + user.getId());
    }

    @Test
    public void testChangePassword() {
        final String usersUrl = getBaseUrl() + "/api/user";
        final String updatedPass = "12345";
        PasswordUpdateDto passwordUpdate = new PasswordUpdateDto(updatedPass, accountsAdminProps[2]);
        adminRest().postForObject(usersUrl + "/current/password", passwordUpdate, String.class);
        try {
            createRestTemplateForUser(accountsAdminProps[1], accountsAdminProps[2]);
            fail("Un-authorised exeption was expected");
        } catch (final Exception e) {
            assertTrue(e instanceof HttpClientErrorException);
            assertTrue(e.getMessage().startsWith("401"));
        }
        final RestTemplate restAdmin2 = createRestTemplateForUser(accountsAdminProps[1], updatedPass);
        passwordUpdate = new PasswordUpdateDto(accountsAdminProps[2], updatedPass);
        adminRest().postForObject(usersUrl + "/current/password", passwordUpdate, String.class);        // restore password
        createRestTemplateForUser(accountsAdminProps[1], accountsAdminProps[2]);
    }
}
