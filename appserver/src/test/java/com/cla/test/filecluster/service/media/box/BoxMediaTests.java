package com.cla.test.filecluster.service.media.box;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.media.BoxMediaApiController;
import com.cla.filecluster.service.media.BoxMediaAppService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Category(FastTestCategory.class)
@Ignore
public class BoxMediaTests extends ClaBaseTests {

    private final static Logger logger = LoggerFactory.getLogger(BoxMediaTests.class);

    @Autowired
    private BoxMediaApiController boxMediaApiController;

    @Autowired
    private BoxMediaAppService boxMediaAppService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Test
    public void testListBoxFoldersUnderRoot() {
        testStartLog(logger);
        Assume.assumeTrue(boxMediaAppService.isBoxApiFileExists());
        List<ServerResourceDto> serverResourceDtos = boxMediaApiController.listServerFolders(0,0, null);
        Assert.assertThat(serverResourceDtos.size(), Matchers.greaterThan(0));
        for (ServerResourceDto serverResourceDto : serverResourceDtos) {
            logger.debug("Folder: {}",serverResourceDto);
        }
        ServerResourceDto subFolderDto = serverResourceDtos.get(0);
        logger.debug("List folders under {}",subFolderDto.getFullName());
        List<ServerResourceDto> childrenFolders = boxMediaApiController.listServerFolders(0,0,subFolderDto.getFullName());
        for (ServerResourceDto serverResourceDto : childrenFolders) {
            logger.debug("Folder: {}",serverResourceDto);
        }
    }

    @Test
    public void testAddBoxRootFolderAndScan() {
        testStartLog(logger);
        Assume.assumeTrue(boxMediaAppService.isBoxApiFileExists());
        List<ServerResourceDto> serverResourceDtos = boxMediaApiController.listServerFolders(0,0,null);
        logger.debug("Travel to /scan_samples");
        Optional<ServerResourceDto> scanSamples = serverResourceDtos.stream().filter(f -> f.getName().equalsIgnoreCase("scan_samples")).findFirst();
        String path = String.valueOf(scanSamples.get());
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setMediaType(MediaType.BOX);
        rootFolderDto.setPath(path);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, null);
        Assert.assertNotNull(rootFolder.getId());
        HashMap<String, String> params = new HashMap<>();
        params.put("wait","true");
        jobManagerApiController.startScanRootFolder(rootFolder.getId(), params);
    }


}
