package com.cla.test.filecluster.service.files.excel;

import com.cla.common.allocation.ExcelMemoryAllocationStrategy;
import com.cla.common.allocation.IngestionResourceManager;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.common.media.files.excel.ExcelFilesServiceImpl;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.dto.excel.ExcelSimilarity;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;
import com.cla.connector.mediaconnector.fileshare.FileShareMediaUtils;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Optional;

import static org.junit.Assert.assertTrue;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class ClaExcelServiceTests {

	@Mock
	private NGramTokenizerHashed nGramTokenizerHashed;

	@Mock
	private IngestionResourceManager ingestionResourceManager;

	@Mock
	private ExcelMemoryAllocationStrategy allocationStrategy;

	@InjectMocks
	private ExcelFilesServiceImpl excelFilesService;

	@Before
	public void init() {

		ReflectionTestUtils.setField(excelFilesService, "isTokenMatcherActive", true);
		ReflectionTestUtils.setField(excelFilesService, "patternSearchActive", true);
		ReflectionTestUtils.setField(excelFilesService, "maxExcelFileSize", 0);
		ReflectionTestUtils.setField(excelFilesService, "useTableHeadingsAsTitles", true);
		ReflectionTestUtils.setField(excelFilesService, "excelWorkbooksMemoryAllocationMB", 900);
		ReflectionTestUtils.setField(excelFilesService, "singleTokenNumberedTitleRateReduction", 3);

		String[] ignoredSheetNamePrefixesAsTitles = {"sheet","\u05D2\u05DC\u05D9\u05D5\u05DF"};
		ReflectionTestUtils.setField(excelFilesService, "ignoredSheetNamePrefixesAsTitles", ignoredSheetNamePrefixesAsTitles);

		excelFilesService.init();
	}

	private static final String othersPath = "src/test/resources/files/excel/others/";

	public ExcelWorkbook ingestWorkbook(String path, InputStream is, boolean extractMetadata) throws FileNotFoundException {
		Optional<ClaFilePropertiesDto> mediaProperties = FileShareMediaUtils.getMediaProperties(path, true, true);
		return excelFilesService.ingestWorkbook(path, is, extractMetadata, mediaProperties.orElse(null));
	}

	@Test
	public void testTwoFiles1() throws IOException{
		final String path1 = othersPath + "122900 HPL.xls";
		final String path2 = othersPath + "122200 HPL.xls";

		final ExcelWorkbook ingestedWorkbook1 = ingestWorkbook(path1,new FileInputStream(path1),true);
		final ExcelWorkbook ingestedWorkbook2 = ingestWorkbook(path2,new FileInputStream(path2),true);

		final ExcelSimilarity excelSimilarity = new ExcelSimilarity(ingestedWorkbook1, ingestedWorkbook2);

		assertTrue(ingestedWorkbook1.getSheets().size() == 4);
		assertTrue(ingestedWorkbook1.getSheets().get("0").getMetaData().getNumOfCells() == 142);

		assertTrue(ingestedWorkbook2.getSheets().size() == 4);
		assertTrue(ingestedWorkbook2.getSheets().get("0").getMetaData().getNumOfCells() == 138);

		assertTrue(excelSimilarity.getProposedTitlesA().containsKey("Regulatory"));
		assertTrue(excelSimilarity.getProposedTitlesB().containsKey("Regulatory"));
		assertTrue(excelSimilarity.getPotentialSimilarityRate() == 1);
	}

	@Test
	public void testTwoFiles2() throws IOException {

		final String path1 = othersPath + "belcooil.xls";
		final String path2 = othersPath + "GasDailyEastPG&E.xls";
		
		final ExcelWorkbook ingestedWorkbook1 = ingestWorkbook(path1,new FileInputStream(path1),true);
		final ExcelWorkbook ingestedWorkbook2 = ingestWorkbook(path2,new FileInputStream(path2),true);

		final ExcelSimilarity excelSimilarity = new ExcelSimilarity(ingestedWorkbook1, ingestedWorkbook2);

		assertTrue(ingestedWorkbook1.getSheets().size() == 4);
		assertTrue(ingestedWorkbook1.getSheets().get("0").getMetaData().getNumOfCells() == 11);

		assertTrue(ingestedWorkbook2.getSheets().size() == 3);
		assertTrue(ingestedWorkbook2.getSheets().get("0").getMetaData().getNumOfCells() == 10);

		assertTrue(excelSimilarity.getProposedTitlesA().containsKey("QUERY DATA"));
		assertTrue(excelSimilarity.getProposedTitlesB().containsKey("QUERY DATA"));
		assertTrue(excelSimilarity.getPotentialSimilarityRate() == 1);
	}

	@Test
	public void testXlsAndXlsxFiles() throws IOException{

		final String path1 = othersPath + "122900 HPL.xls";
		final String path2 = othersPath + "122900 HPL.xlsx";
		
		final ExcelWorkbook ingestedWorkbook1 = ingestWorkbook(path1,new FileInputStream(path1),true);
		final ExcelWorkbook ingestedWorkbook2 = ingestWorkbook(path2,new FileInputStream(path2),true);

		final ExcelSimilarity excelSimilarity = new ExcelSimilarity(ingestedWorkbook1, ingestedWorkbook2);

		assertTrue(ingestedWorkbook1.getSheets().size() == 4);
		assertTrue(ingestedWorkbook1.getSheets().get("0").getMetaData().getNumOfCells() == 142);

		assertTrue(ingestedWorkbook2.getSheets().size() == 4);
		assertTrue(ingestedWorkbook2.getSheets().get("0").getMetaData().getNumOfCells() == 142);

		assertTrue(excelSimilarity.getProposedTitlesA().containsKey("Regulatory"));
		assertTrue(excelSimilarity.getProposedTitlesB().containsKey("Regulatory"));
		assertTrue(excelSimilarity.getPotentialSimilarityRate() == 1);
	}
	
	@Test
	public void testFileAttributes() throws IOException{
		final Path path = FileSystems.getDefault().getPath(othersPath + "belcooil.xls");
		ClaFilePropertiesDto cfp = FileShareMediaUtils.getMediaProperties(path, true, true);
		assertTrue(cfp.getFileSize() > 1000);
		assertTrue(cfp.getAccessTimeMilli() > 1000);
	}		

}
