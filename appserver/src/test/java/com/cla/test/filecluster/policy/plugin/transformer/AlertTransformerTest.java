package com.cla.test.filecluster.policy.plugin.transformer;

import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.event.ScanDiffEvent;
import com.cla.common.domain.dto.event.ScanDiffPayload;
import com.cla.filecluster.domain.entity.alert.Alert;
import com.cla.filecluster.policy.plugin.transformer.AlertTransformer;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import com.google.common.collect.ImmutableMap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class AlertTransformerTest {

    @Test
    public void testAlertTransform() {
        AlertTransformer alertTransformer = new AlertTransformer(ImmutableMap.of(
                EventType.SCAN_DIFF_UPDATED, AlertSeverity.CRITICAL,
                EventType.SCAN_DIFF_ACL_UPDATED, AlertSeverity.MEDIUM,
                EventType.SCAN_DIFF_DELETED, AlertSeverity.HIGH,
                EventType.SCAN_DIFF_UNDELETED, AlertSeverity.LOW
        ));

        ScanDiffEvent scanDiffEvent = buildDiffEvent("path", 1L, "Diff description", EventType.SCAN_DIFF_UPDATED);
        Alert alert = alertTransformer.transform(scanDiffEvent);
        Assert.assertEquals(AlertSeverity.CRITICAL, alert.getSeverity());
        Assert.assertEquals(EventType.SCAN_DIFF_UPDATED, alert.getEventType());
        Assert.assertEquals(expectedMessage(scanDiffEvent), alert.getMessage());

        scanDiffEvent = buildDiffEvent("path", 1L, "Diff description", EventType.SCAN_DIFF_ACL_UPDATED);
        alert = alertTransformer.transform(scanDiffEvent);
        Assert.assertEquals(AlertSeverity.MEDIUM, alert.getSeverity());
        Assert.assertEquals(EventType.SCAN_DIFF_ACL_UPDATED, alert.getEventType());
        Assert.assertEquals(expectedMessage(scanDiffEvent), alert.getMessage());

        scanDiffEvent = buildDiffEvent("path", 1L, "Diff description", EventType.SCAN_DIFF_DELETED);
        alert = alertTransformer.transform(scanDiffEvent);
        Assert.assertEquals(AlertSeverity.HIGH, alert.getSeverity());
        Assert.assertEquals(EventType.SCAN_DIFF_DELETED, alert.getEventType());
        Assert.assertEquals(expectedMessage(scanDiffEvent), alert.getMessage());

        scanDiffEvent = buildDiffEvent("path", 1L, "Diff description", EventType.SCAN_DIFF_UNDELETED);
        alert = alertTransformer.transform(scanDiffEvent);
        Assert.assertEquals(AlertSeverity.LOW, alert.getSeverity());
        Assert.assertEquals(EventType.SCAN_DIFF_UNDELETED, alert.getEventType());
        Assert.assertEquals(expectedMessage(scanDiffEvent), alert.getMessage());
    }

    private String expectedMessage(ScanDiffEvent scanDiffEvent) {
        return String.format(AlertTransformer.SCAN_DIFF_ALERT_MESSAGE_PATTERN, scanDiffEvent.getEventType().getValue(), scanDiffEvent.getPayload().getPath(), scanDiffEvent.getPayload().getFileId());
    }

    private ScanDiffEvent buildDiffEvent(String path, long fileId, String description, EventType eventType) {
        ScanDiffEvent scanDiffEvent = new ScanDiffEvent();
        ScanDiffPayload payload = new ScanDiffPayload();
        payload.setPath(path);
        payload.setFileId(fileId);
        scanDiffEvent.setPayload(payload);
        scanDiffEvent.setDescription(description);
        scanDiffEvent.setEventType(eventType);
        return scanDiffEvent;
    }
}
