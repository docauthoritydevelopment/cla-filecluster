package com.cla.test.filecluster.service.syscomponent;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;

import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;

/**
 * Created by liora on 05/06/2017.
 */
@Category(UnitTestCategory.class)
public class CustomerDataCenterTests {

    @Test
    public void testConvertToDto() {
        CustomerDataCenter dc = new CustomerDataCenter();
        dc.setId(1L);
        dc.setDefault(true);
        dc.setLocation("loc");
        dc.setName("nm");
        dc.setDescription("desc");
        dc.setPublicKey("pub");

        CustomerDataCenterDto dto = CustomerDataCenterService.convertCustomerDataCenterToDto(dc);

        assertEquals(new Long(dc.getId()), new Long(dto.getId()));
        assertEquals(dc.getName(), dto.getName());
        assertEquals(dc.getDescription(), dto.getDescription());
        assertEquals(dc.getLocation(), dto.getLocation());
        assertEquals(dc.getPublicKey(), dto.getPublicKey());
        assertEquals(dc.isDefault(), dto.getDefaultDataCenter());
    }

    @Test
    public void convertFromDto() {
        CustomerDataCenterDto dto = new CustomerDataCenterDto();
        dto.setId(1L);
        dto.setDefaultDataCenter(true);
        dto.setLocation("loc");
        dto.setName("nm");
        dto.setDescription("desc");
        dto.setPublicKey("pub");

        CustomerDataCenter dc = CustomerDataCenterService.convertToCustomerDataCenter(dto);
        //assertEquals(new Long(dc.getId()), new Long(dto.getId()));
        assertEquals(dc.getName(), dto.getName());
        assertEquals(dc.getDescription(), dto.getDescription());
        assertEquals(dc.getLocation(), dto.getLocation());
        assertEquals(dc.getPublicKey(), dto.getPublicKey());
        assertEquals(dc.isDefault(), dto.getDefaultDataCenter());
    }
}
