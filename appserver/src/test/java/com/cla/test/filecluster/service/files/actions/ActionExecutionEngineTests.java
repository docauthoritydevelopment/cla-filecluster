package com.cla.test.filecluster.service.files.actions;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.filecluster.actions.executors.StoragePolicyScriptGenerator;
import com.cla.common.domain.dto.action.ActionDefinitionDto;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.common.domain.dto.action.DispatchActionRequestDto;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.service.api.*;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.api.actions.ActionsApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.ActionsEngineTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Category(ActionsEngineTestCategory.class)
public class ActionExecutionEngineTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(ActionExecutionEngineTests.class);
    private static final String REPORT_FILE_DETAILS = "REPORT_FILE_DETAILS";
    private static final String COPY_FILE = "COPY_FILE";

    @Autowired
    private ActionsApiController actionsApiController;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FileTagApiController fileTagApiController;

    @Autowired
    private DocTypeApiController docTypeApiController;

    @BeforeClass
    public static void beforeClass() throws IOException {
        printToErr(ActionExecutionEngineTests.class.getName());
        logger.info("Delete temp/actions folder");
        Path directory = Paths.get("./temp/actions");
        PerformanceTestService.deleteFolder(directory);
    }

    @Test(timeout = 60000)
    public void testReportOnFile() {
        logger.info("testReportOnFile");
        ClaFileDto claFileDto = filesApiController.findFilesDeNormalized(null).getContent().get(0);
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, REPORT_FILE_DETAILS);

        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());

        Map<String, String> params = createParams(String.valueOf(claFileDto.getId()), "fileId");

        logger.debug("Immediate action - report on file {} using action {}", claFileDto.getId(), actionDefinitionDto);
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionTriggerDetails.getActionTriggerId();
        //ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        /*logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());
        Assert.assertEquals(actionTriggerDetails.getType(), ActionTriggerType.IMMEDIATE);
        Assert.assertEquals(RunStatus.RUNNING, actionTriggerDetails.getStatus());*/

        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        Assert.assertEquals(actionTriggerDetails.getStatus(), RunStatus.FINISHED_SUCCESSFULLY);
        Assert.assertEquals(actionTriggerDetails.getProgressMark(), 1);
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);
    }

    @Test(timeout = 60000)
    public void testReportOnGroup() {
        testStartLog(logger);
        GroupDto groupDto = groupsApiController.findGroupsFileCount(null).getContent().get(0).getItem();
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, "ADD_PERMISSION");
        logger.debug("Immediate action - add read permission on group {} using action {}", groupDto.getId(), actionDefinitionDto);
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        Map<String, String> params = createParams(groupDto.getId(), "groupId");
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "user", "administrator");
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionTriggerDetails.getActionTriggerId();
        //ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());
        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        logger.debug("Action trigger details: {}", actionTriggerDetails);
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, actionTriggerDetails.getStatus());
        Assert.assertThat(actionTriggerDetails.getProgressMark(), Matchers.greaterThan(0L));
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);
    }

    @Test(timeout = 60000)
    public void testReportOnFolder() {
        logger.info("testReportOnFolder");
        List<AggregationCountFolderDto> content = fileTreeApiController.findFoldersFileCount(null).getContent();
        Assert.assertTrue(content.size() > 0);
        DocFolderDto baseFolder = content.get(0).getItem();
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, REPORT_FILE_DETAILS);
        logger.debug("Immediate action - set read permission on folder {} using action {}", baseFolder.getId(), actionDefinitionDto);
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "columns", "[test]");
        Map<String, String> params = createParams(String.valueOf(baseFolder.getId()), "subfolderId");
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionTriggerDetails.getActionTriggerId();
        logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());
        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, actionTriggerDetails.getStatus());
        Assert.assertThat(actionTriggerDetails.getProgressMark(), Matchers.greaterThan(0L));
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);
    }

    @Test
    @Ignore
    public void testReportOnFilter() {
        logger.info("testReportOnFilter");
        List<AggregationCountFolderDto> content = fileTreeApiController.findFoldersFileCount(null).getContent();
        Assert.assertTrue(content.size() > 0);
        DocFolderDto filterFolder = content.get(0).getItem();
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, REPORT_FILE_DETAILS);
        FilterDescriptor filterDescriptor =
                new FilterDescriptor(FileDtoFieldConverter.DtoFields.folderId.name(), String.valueOf(filterFolder.getId()), "eq");
        logger.debug("Immediate action - set read permission on filter {} using action {}", filterDescriptor, actionDefinitionDto);

        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        Map<String, String> params = new HashMap<>();
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "columns", "[test]");
        dispatchActionRequestDto.getParameters().setProperty("filter", convertFilterDescriptorToJson(filterDescriptor));
        dispatchActionRequestDto.getParameters().setProperty("wait", "true");
        params.put("filter", convertFilterDescriptorToJson(filterDescriptor));
        ActionExecutionTaskStatusDto actionExecutionTaskStatusDto = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionExecutionTaskStatusDto.getActionTriggerId();
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());
        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, actionTriggerDetails.getStatus());
        Assert.assertThat(actionTriggerDetails.getProgressMark(), Matchers.greaterThan(0L));
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);
    }

    @Test
    public void testCopyFileAbsoluteFolder() throws IOException {
        logger.info("testCopyFileAbsoluteFolder");
        ClaFileDto claFileDto = filesApiController.findFilesDeNormalized(null).getContent().get(0);
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, COPY_FILE);
        logger.debug("Immediate action - copy file {} using action {}", claFileDto.getId(), actionDefinitionDto);
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        Path targetFolder = Paths.get("./build").toAbsolutePath().normalize();
        logger.info("Copy the files to {}", targetFolder);
        dispatchActionRequestDto.getParameters().setProperty("target", targetFolder.toString());
        Map<String, String> params = createParams(String.valueOf(claFileDto.getId()), "fileId");
        ActionExecutionTaskStatusDto actionExecutionTaskStatusDto = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        ActionExecutionTaskStatusDto lastRunningActionExecution = actionsApiController.getLastRunningActionExecution(null);
        logger.debug("Got action trigger details: {}", lastRunningActionExecution.getActionTriggerId());
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, lastRunningActionExecution.getStatus());
        Assert.assertEquals(lastRunningActionExecution.getProgressMark(), 1);
        Assert.assertEquals(lastRunningActionExecution.getErrorsCount(), 0);

        logger.info("Check that script was created successfully");
        String scriptFolders = lastRunningActionExecution.getScriptFolders();
        Assert.assertThat("Verify script folder is returned", scriptFolders, Matchers.not(Matchers.isEmptyOrNullString()));
        Assert.assertTrue("Verify folder was created", Files.exists(Paths.get(scriptFolders)));
        String scriptName = StoragePolicyScriptGenerator.generateScriptName(lastRunningActionExecution.getActionTriggerId(), new File(scriptFolders), 1);
        byte[] scriptBytes = Files.readAllBytes(Paths.get(scriptName));
        String scriptContent = new String(scriptBytes);
        Assert.assertThat("Verify script is not empty", scriptContent, Matchers.not(Matchers.isEmptyOrNullString()));

        Assert.assertThat("Verify script contains the request file name", scriptContent, Matchers.containsString(claFileDto.getBaseName()));
        Assert.assertThat("Verify script contains the target folder", scriptContent, Matchers.containsString(targetFolder.toString()));
    }

    @Test
    public void testCopyFileSameFolder() throws IOException {
        logger.info("testCopyFileSameFolder");
        ClaFileDto claFileDto = filesApiController.findFilesDeNormalized(null).getContent().get(0);
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, COPY_FILE);
        logger.debug("Immediate action - copy file {} using action {}", claFileDto.getId(), actionDefinitionDto);

        logger.info("Copy the files to same folder, same name .bak");
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        dispatchActionRequestDto.getParameters().setProperty("target", "${fullPath}${baseName}.bak");
        Map<String, String> params = createParams(String.valueOf(claFileDto.getId()), "fileId");
        ActionExecutionTaskStatusDto actionExecutionTaskStatusDto = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        ActionExecutionTaskStatusDto lastRunningActionExecution = actionsApiController.getLastRunningActionExecution(null);
        logger.debug("Got action trigger details: {}", lastRunningActionExecution.getActionTriggerId());
        Assert.assertEquals(lastRunningActionExecution.getStatus(), RunStatus.FINISHED_SUCCESSFULLY);
        Assert.assertEquals(lastRunningActionExecution.getProgressMark(), 1);
        Assert.assertEquals(lastRunningActionExecution.getErrorsCount(), 0);

        logger.info("Check that script was created successfully");
        String scriptFolders = lastRunningActionExecution.getScriptFolders();
        Assert.assertThat("Verify script folder is returned", scriptFolders, Matchers.not(Matchers.isEmptyOrNullString()));
        Assert.assertTrue("Verify folder was created", Files.exists(Paths.get(scriptFolders)));
        String scriptName = StoragePolicyScriptGenerator.generateScriptName(lastRunningActionExecution.getActionTriggerId(), new File(scriptFolders), 1);
        byte[] scriptBytes = Files.readAllBytes(Paths.get(scriptName));
        String scriptContent = new String(scriptBytes);
        Assert.assertThat("Verify script is not empty", scriptContent, Matchers.not(Matchers.isEmptyOrNullString()));

        Assert.assertThat("Verify script contains the request file name", scriptContent, Matchers.containsString(claFileDto.getFileName()));
        String newName = claFileDto.getFileName() + ".bak";
        Assert.assertThat("Verify script contains the target folder", scriptContent, Matchers.containsString(newName));
    }

    @Test(timeout = 60000)
    public void testModifyTagsOnGroup() {
        Path targetFolder = Paths.get("./build").toAbsolutePath().normalize();

        logger.info(testName.getMethodName());
        GroupDto groupDto = groupsApiController.findGroupsFileCount(null).getContent().get(0).getItem();
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, "SET_PROPERTY_VALUE");
        logger.debug("Immediate action - SET_PROPERTY_VALUE on group {} using action {}", groupDto.getId(), actionDefinitionDto);
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "value", "coolDlpTag");
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "property", "TAG");
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "tempFolder", targetFolder.toString());
        Map<String, String> params = createParams(groupDto.getId(), "groupId");
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionTriggerDetails.getActionTriggerId();
        logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());
        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        logger.debug(actionTriggerDetails.toString());
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, actionTriggerDetails.getStatus());
        Assert.assertThat(actionTriggerDetails.getProgressMark(), Matchers.greaterThan(0L));
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);

    }

    @Test(timeout = 60000)
    public void testModifyTagsOnGroup2() {
        Path targetFolder = Paths.get("./build").toAbsolutePath().normalize();

        logger.info(testName.getMethodName());
        GroupDto groupDto = groupsApiController.findGroupsFileCount(null).getContent().get(0).getItem();
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, "SET_PROPERTY_VALUE");
        logger.debug("Immediate action - SET_PROPERTY_VALUE on group {} using action {}", groupDto.getId(), actionDefinitionDto);
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "property", "TAG");
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "tempFolder", targetFolder.toString());
        Map<String, String> params = createParams(groupDto.getId(), "groupId");
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionTriggerDetails.getActionTriggerId();
        logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());
        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        logger.debug(actionTriggerDetails.toString());
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, actionTriggerDetails.getStatus());
        Assert.assertThat(actionTriggerDetails.getProgressMark(), Matchers.greaterThan(0L));
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);
    }

    private DispatchActionRequestDto createAction(String actionId) {
        DispatchActionRequestDto dispatchActionRequestDto = new DispatchActionRequestDto();
        dispatchActionRequestDto.setActionDefinitionId(actionId);
        dispatchActionRequestDto.setParameters(new Properties());
        return dispatchActionRequestDto;
    }

    private Map<String, String> createParams(String baseFilterValue, String baseFilterField) {
        Map<String, String> params = new HashMap<>();
        params.put("baseFilterValue", baseFilterValue);
        params.put("baseFilterField", baseFilterField);
        params.put("wait", "true");
        return params;
    }

    @Test(timeout = 60000)
    public void testIonicEncryptFiles() {
        FileTagDto fileTagDto = performanceTestService.findSensitiveFileTag();
        DocTypeDto docTypeDto = docTypeApiController.getAllDocTypes(null).getContent().get(1);
        Path targetFolder = Paths.get("./build").toAbsolutePath().normalize();

        testStartLog(logger);
        GroupDto groupDto = groupsApiController.findGroupsFileCount(null).getContent().get(0).getItem();
        fileTagApiController.addTagToGroup(groupDto.getId(), fileTagDto.getId());
        docTypeApiController.assignDocTypeToGroup(groupDto.getId(), docTypeDto.getId());
        ActionDefinitionDto actionDefinitionDto = actionsApiController.findAction(null, "ENCRYPT_FILE");
        DispatchActionRequestDto dispatchActionRequestDto = createAction(actionDefinitionDto.getId());
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "dataMarkings", "test-sensitive:true");
        dispatchActionRequestDto.getParameters().setProperty(ActionsApiController.PARAMETER_PREFIX + "target", targetFolder.toString());

        logger.debug("Immediate action - ENCRYPT_FILE on group {} using action {}", groupDto.getId(), actionDefinitionDto);
        Map<String, String> params = createParams(groupDto.getId(), "groupId");
        ActionExecutionTaskStatusDto actionTriggerDetails = actionsApiController.executeActionOnFilter(params, dispatchActionRequestDto);
        long actionTriggerId = actionTriggerDetails.getActionTriggerId();
        logger.debug("Got action trigger details: {}", actionTriggerDetails.getActionTriggerId());

        actionTriggerDetails = actionsApiController.getActionTriggerDetails(actionTriggerId);
        Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY, actionTriggerDetails.getStatus());
        Assert.assertThat(actionTriggerDetails.getProgressMark(), Matchers.greaterThan(0L));
        Assert.assertEquals(actionTriggerDetails.getErrorsCount(), 0);
        docTypeApiController.removeDocTypeFromGroup(groupDto.getId(), docTypeDto.getId());
    }

    public static String convertFilterDescriptorToJson(FilterDescriptor filterDescriptor) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(filterDescriptor);
        } catch (IOException e) {
            logger.error("Failed to convert filterDescriptor to FILTER_JSON (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert filterDescriptor to FILTER_JSON (IO Exception): " + e.getMessage());
        }
    }
}
