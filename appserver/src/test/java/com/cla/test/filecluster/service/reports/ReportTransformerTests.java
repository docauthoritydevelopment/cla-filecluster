package com.cla.test.filecluster.service.reports;

import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 03/08/2016.
 */
@Category(UnitTestCategory.class)
public class ReportTransformerTests {

    Logger logger = LoggerFactory.getLogger(ReportTransformerTests.class);

    @Test
    public void testReportTransformer() throws ScriptException {

        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        
        Map<String,Double> input = new HashMap<>();
        input.put("numFound",100D);
        input.put("riskTypeFound",20D);
        Map<String,Double> output = new HashMap<>();

        // define a different script context
        ScriptContext newContext = new SimpleScriptContext();
        newContext.setBindings(engine.createBindings(), ScriptContext.ENGINE_SCOPE);
        Bindings engineScope = newContext.getBindings(ScriptContext.ENGINE_SCOPE);

        // set the variable to a different value in another scope
        engineScope.put("input", input);
        engineScope.put("output", output);


        String script = "output.result=input.riskTypeFound  / input.numFound;";
        logger.info("Evaluating");
        engine.eval(script,engineScope);
        logger.info("Finished. Result: {}",output);
    }
}
