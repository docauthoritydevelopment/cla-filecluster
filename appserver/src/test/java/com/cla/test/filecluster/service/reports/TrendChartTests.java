package com.cla.test.filecluster.service.reports;

import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.report.*;
import com.cla.filecluster.service.api.DocTypeApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.reports.TrendChartApiController;
import com.cla.filecluster.service.report.TrendChartAppService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.ReportingTestCategory;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

/**
 * Created by uri on 27/07/2016.
 */
@Category(ReportingTestCategory.class)
public class TrendChartTests extends ClaBaseTests {

    @Autowired
    private TrendChartApiController trendChartApiController;

    @Autowired
    private TrendChartAppService trendChartAppService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private DocTypeApiController docTypeApiController;

    private  Logger logger = LoggerFactory.getLogger(TrendChartTests.class);

    @Test
    public void testConfigureTrendChart() {
        testStartLog(logger);
        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(60000l);
        newTrendChart.setName("testConfigureTrendChart");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.groupId.name());
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        Assert.assertNotNull(trendChart.getId());
        Page<TrendChartDto> trendChartDtos = trendChartApiController.listTrendCharts(null);
        Assert.assertThat(trendChartDtos.getContent().size(), Matchers.greaterThanOrEqualTo(1));
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,null,null,null,null,null,null);
        Assert.assertNotNull(trendChartView);
        Assert.assertNotNull(trendChartView.getTrendChartDto());
    }

    @Test
    public void testDeleteTrendChart() {
        testStartLog(logger);
        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(60000l);
        newTrendChart.setName("testDeleteTrendChart");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.docTypeId.name());
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        Assert.assertNotNull(trendChart.getId());
        trendChartApiController.deleteTrendChart(trendChart.getId(), performanceTestService.createWaitParams());
        Page<TrendChartDto> trendChartDtos = trendChartApiController.listTrendCharts(null);
        for (TrendChartDto trendChartDto : trendChartDtos) {
            Assert.assertNotEquals("TrendChart should have been deleted", trendChartDto.getId(), trendChart.getId());
        }
    }

    @Test
    public void testCollectTrendChartData() {
        testStartLog(logger);
        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(100L);
        newTrendChart.setName("testCollectTrendChartData");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.docTypeId.name());
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        logger.info("Collect trendChart data before adding docType");
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());

        Page<DocTypeDto> allDocTypes = docTypeApiController.getAllDocTypes(null);
        DocTypeDto docTypeDto = allDocTypes.getContent().get(1);
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);
        logger.info("Add docType {} to file {}",docTypeDto,claFileDto);
        docTypeApiController.assignDocTypeToFile(docTypeDto.getId(),claFileDto.getId());
        logger.info("Collect trendChart data after adding docType");
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());

        logger.info("Now view the chart data");
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,null,null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken on 100ms resolution",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        List<TrendChartSeriesDto> trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat(trendChartSeriesDtoList.size(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {} values: {}",trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());

        }
        logger.debug("View chart data on lower resolution");
        trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,1000L,null,null,null,null,null);
        trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat("on 1000ms resolution",trendChartSeriesDtoList.size(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {} values: {}",trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());
        }
        trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,30000L,null,null,null,null,null);
        trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat("on 3000ms resolution",trendChartSeriesDtoList.size(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {} values: {}",trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());

        }
        docTypeApiController.removeDocTypeFromFile(docTypeDto.getId(),claFileDto.getId());
    }

    @Test
    public void testFabricateData() {
        testStartLog(logger);
        TrendChartDto newTrendChart = new TrendChartDto();
        long dayResolution = 3600 * 1000 * 24l;
        newTrendChart.setViewResolutionMs(dayResolution);
        newTrendChart.setName("testFabricateData");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.id.name());
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);

        logger.info("Fabricate trendChart data");
        trendChartAppService.fabricateTrendChartData(trendChart.getId(),90);

        logger.info("Now view the chart data");
        Long startTime = LocalDateTime.now().minusDays(90).toEpochSecond(ZoneOffset.UTC) * 1000;
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),startTime,null,null,null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        List<TrendChartSeriesDto> trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat(trendChartSeriesDtoList.size(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {} values: {}",trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());
        }

        logger.info("View the chart data in 7 days resolution");
        trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),startTime,null,dayResolution * 7, null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat(trendChartSeriesDtoList.size(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {} values: {}",trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());
        }

        logger.info("Create view only trendChart (with collectorId");
        TrendChartDto newTrendChart2 = new TrendChartDto();
        newTrendChart2.setViewResolutionMs(dayResolution);
        newTrendChart2.setName("show other view on data");
        newTrendChart2.setSortType(SortType.NAME);
        newTrendChart2.setChartDisplayType(ChartDisplayType.BAR_HORIZONTAL);
        newTrendChart2.setChartValueFormatType(ChartValueFormatType.PERCENTAGE);
        newTrendChart2.setSeriesType(FileDtoFieldConverter.DtoFields.id.name());
        newTrendChart2.setTrendChartCollectorId(trendChart.getId());
        TrendChartDto trendChart2 = trendChartApiController.createTrendChart(newTrendChart2);
        logger.info("We should have data already");
        logger.info("View the chart data in 7 days resolution");
        trendChartView = trendChartApiController.getTrendChartView(trendChart2.getId(),startTime,null,dayResolution * 7, null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken",trendChartView.getSampleCount(),Matchers.greaterThan(0));

    }

    @Test
    public void testCollectTwoFilters() {
        testStartLog(logger);
        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(100l);
        newTrendChart.setName("testCollectTwoFilters");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.PERCENTAGE);
        newTrendChart.setSeriesType("file.id");
        String filterName = "tagType1";
        newTrendChart.setFilters("[{} , {\"name\":\"" + filterName + "\",\"field\":\"tagType\",\"value\":1,\"operator\":\"eq\"} ]");
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        logger.info("Collect trendChart data before adding tags");
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis()+100);

        logger.info("Now view the chart data");
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,null,null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken on 100ms resolution",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        List<TrendChartSeriesDto> trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat(trendChartSeriesDtoList.size(),Matchers.equalTo(2));
        TrendChartSeriesDto trendChartSeriesDto1 = trendChartSeriesDtoList.get(0);
        TrendChartSeriesDto trendChartSeriesDto2 = trendChartSeriesDtoList.get(1);
        Assert.assertEquals(filterName,trendChartSeriesDto2.getFilter());
        Assert.assertEquals("0",trendChartSeriesDto1.getFilter());
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {}.{} values: {}",trendChartSeriesDto.getFilter(),trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());
        }
    }

    @Test
    public void testTrendChartWithTransformer() {
        testStartLog(logger);

        String totalName = "total";
        String filterName = "riskType";
        String filters = "[{\"name\":\"" + totalName + "\"} , {\"name\":\"" + filterName + "\",\"field\":\"tagType\",\"value\":1,\"operator\":\"eq\"} ]";

        String script = "output.safe = input.total - input.riskType;";

        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(60000l);
        newTrendChart.setName("testTrendChartWithTransformer");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        newTrendChart.setSeriesType("file.id");
        newTrendChart.setFilters(filters);
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        trendChartAppService.updateTrendChartTransformerScript(trendChart.getId(),script);
        logger.info("fabricate values");
        trendChartAppService.fabricateTrendChartData(trendChart.getId(),7);
        logger.info("Now view the chart data");
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,null,null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        List<TrendChartSeriesDto> trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        Assert.assertThat(trendChartSeriesDtoList.size(),Matchers.equalTo(1));
        TrendChartSeriesDto trendChartSeriesDto1 = trendChartSeriesDtoList.get(0);
        Assert.assertEquals("safe",trendChartSeriesDto1.getFilter());
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoList) {
            Assert.assertNotNull(trendChartSeriesDto.getId());
            Assert.assertThat(trendChartSeriesDto.getValues().length,Matchers.greaterThan(0));
            logger.debug("TrendChart series {}.{} values: {}",trendChartSeriesDto.getFilter(),trendChartSeriesDto.getId(),trendChartSeriesDto.getValues());
        }
    }

    @Test
    public void testFacetTrendChartWithTransformer() {
        testStartLog(logger);
        String totalName = "total";
        String filterName = "riskType";
        String filters = "[{\"name\":\"" + totalName + "\"} , {\"name\":\"" + filterName + "\",\"field\":\"tagType\",\"value\":1,\"operator\":\"eq\"} ]";
        String script = "output.safe = {};\n" +
                "for (facet in input.total) {\n" +
                "        output.safe[facet] = input.total[facet] - (input.riskType[facet]?input.riskType[facet]:0);\t\n" +
                "}\n";

        logger.info("Create the trend chart");

        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(60000L);
        newTrendChart.setName("testFacetTrendChartWithTransformer");
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.folderId.name());
        newTrendChart.setFilters(filters);
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        logger.info("Update trendChart transformer script");
        trendChartAppService.updateTrendChartTransformerScript(trendChart.getId(),script);
        logger.info("fabricate values");
        trendChartAppService.fabricateTrendChartData(trendChart.getId(),7);
        logger.info("Now view the chart data");
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,null,null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartView.getTrendChartSeriesDtoList()) {
            String values = Arrays.toString(trendChartSeriesDto.getValues());
            logger.debug("Series name: {} id {} filter: {}",trendChartSeriesDto.getName(),trendChartSeriesDto.getId(),trendChartSeriesDto.getFilter());
            logger.debug("Values: {}", values);
        }

    }

    @Test
    public void testFacetTrendChartWithTransformerTags() {
        testStartLog(logger);
        String totalName = "total";
//        String filters = "[{\"name\":\"" + totalName + "\",'field':'tagType', 'value':2, 'operator':'eq' }]";
        //{'name':'total' , 'filters' : [{'field':'tagType', 'value':'2', 'operator':'eq'} , {'field':'facetPrefix', 'value':'2.', 'operator':'eq'}]}
        String filters = "{'name':'total' , 'filters' : " +
                "[{'field':'tagType', 'value':'2', 'operator':'eq'} , " +
                "{'field':'facetPrefix', 'value':'2.', 'operator':'eq'}]}";
        String script = "output.half = {};\n" +
                "for (facet in input.total) {\n" +
                "        output.half[facet] = input.total[facet]/2;\t\n" +
                "}\n";

        logger.info("Create the trend chart");

        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(60000l);
        newTrendChart.setName("testFacetTrendChartWithTransformerTags");
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.tags.name());
        newTrendChart.setFilters(filters);
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        logger.info("Update trendChart transformer script");
        trendChartAppService.updateTrendChartTransformerScript(trendChart.getId(),script);
        logger.info("fabricate values");
        trendChartAppService.fabricateTrendChartData(trendChart.getId(),7);
        logger.info("Now view the chart data");
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChart.getId(),null,null,null,null,null,null,null,null);
        logger.info("Verify trendChart view has at least one sample {}",trendChartView);
        Assert.assertThat("Verify at least one sample was taken",trendChartView.getSampleCount(),Matchers.greaterThan(0));
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartView.getTrendChartSeriesDtoList()) {
            String values = Arrays.toString(trendChartSeriesDto.getValues());
            logger.debug("Series name: {} id {} filter: {}",trendChartSeriesDto.getName(),trendChartSeriesDto.getId(),trendChartSeriesDto.getFilter());
            logger.debug("Values: {}", values);
        }

    }

    @Test
    public void testUpdateTrendChart() {
        testStartLog(logger);
        TrendChartDto newTrendChart = new TrendChartDto();
        newTrendChart.setViewResolutionMs(60000l);
        newTrendChart.setName("testUpdateTrendChart");
        newTrendChart.setSortType(SortType.NAME);
        newTrendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        newTrendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        newTrendChart.setSeriesType(FileDtoFieldConverter.DtoFields.tags.name());
        TrendChartDto trendChart = trendChartApiController.createTrendChart(newTrendChart);
        logger.info("Update trend chart");
        trendChart.setName("testUpdateTrendChart2");
        TrendChartDto trendChartDto = trendChartApiController.updateTrendChart(trendChart);
        Assert.assertNotNull(trendChartDto.getId());
        Assert.assertEquals("testUpdateTrendChart2",trendChartDto.getName());
    }

    @Test
    public void testExistingTrendCharts() {
        testStartLog(logger);
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());
        Page<TrendChartDto> trendChartDtos = trendChartApiController.listTrendCharts(null);
        verifyTrendChartExists(trendChartDtos.getContent(),"Risk assignment");
        verifyTrendChartExists(trendChartDtos.getContent(),"Doc Types");
        TrendChartDto trendChartDto = verifyTrendChartExists(trendChartDtos.getContent(), "DocType Assignment Ratio");
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChartDto.getId(), null, null, null, null,null,null,null,null);
        List<TrendChartSeriesDto> trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        TrendChartSeriesDto trendChartSeriesDto = trendChartSeriesDtoList.get(0);
        ArrayList<String> actual = Lists.newArrayList(trendChartSeriesDtoList.get(0).getName(), trendChartSeriesDtoList.get(1).getName());
        for (TrendChartSeriesDto chartSeriesDto : trendChartSeriesDtoList) {
            logger.debug("Chart Series: {}",chartSeriesDto);
        }

        Assert.assertThat(actual,Matchers.hasItem("ratio"));
    }

    @Test
    public void testExistingTrendChartsMonthly() {
        testStartLog(logger);
        long startTime = 1455263243000L;     // Fri, 12 Feb 2016 07:47:23 GMT
        long endTime = 1466236043000L;       // Sat, 18 Jun 2016 07:47:23 GMT
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());
        Page<TrendChartDto> trendChartDtos = trendChartApiController.listTrendCharts(null);
        TrendChartDto trendChartDto = verifyTrendChartExists(trendChartDtos.getContent(), "DocType Assignment Ratio");
        trendChartAppService.fabricateTrendChartData((trendChartDto.getTrendChartCollectorId()==null)?trendChartDto.getId():trendChartDto.getTrendChartCollectorId(),
                150, endTime);
        TrendChartDataSummaryDto trendChartView = trendChartApiController.getTrendChartView(trendChartDto.getId(),
                startTime, endTime,
                null,
                null,
                TimeResolutionType.TR_month,
                1,0L,1);
        List<TrendChartSeriesDto> trendChartSeriesDtoList = trendChartView.getTrendChartSeriesDtoList();
        TrendChartSeriesDto trendChartSeriesDto = trendChartSeriesDtoList.get(0);
        Assert.assertEquals(5, trendChartView.getSampleCount());    // 5 months
//        Assert.assertEquals(true,"ratio".equals(trendChartSeriesDtoList.get(0).getName()) || "ratio".equals(trendChartSeriesDtoList.get(1).getName()));
    }


    private TrendChartDto verifyTrendChartExists(List<TrendChartDto> trendChartDtos, String name) {
        for (TrendChartDto trendChartDto : trendChartDtos) {
            if (name.equals(trendChartDto.getName())) {
                return trendChartDto;
            }
        }
        throw new RuntimeException("Failed to find trendChart by name "+name);
    }
}
