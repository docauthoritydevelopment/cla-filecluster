package com.cla.test.filecluster.it;

import com.cla.filecluster.util.remote.FormAuthenticatedRestTemplate;
import com.cla.test.filecluster.ClaBaseTests;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes = Application.class)
//@WebAppConfiguration
//@IntegrationTest("server.port:0")
//@ActiveProfiles("webapp,test")
public abstract class AbstractIntegrationTest extends ClaBaseTests {


	@Value("${accountsAdminProps}")
	private String[] accountsAdminProps;

	@Value("http://localhost:${local.server.port}")
	private String baseUrl;
	
	private RestTemplate restTemplateForAccountsAdmin;
	
	

	@Before
	public void setUp() {
		restTemplateForAccountsAdmin = createRestTemplateForUser(accountsAdminProps[1],accountsAdminProps[2]);
	}

	private String getAuthCookie(final String username,final String password) {
		final HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		final HttpEntity<String> requestEntity = new HttpEntity<String>("username=" + username + "&password=" + password,requestHeaders);
		final ResponseEntity<String> rs = new RestTemplate().exchange(baseUrl + "/login", HttpMethod.POST,requestEntity, String.class);
		assertEquals(HttpStatus.OK, rs.getStatusCode());
		final String authCookie = rs.getHeaders().getFirst("Set-Cookie").split(" ")[0];
		assertNotNull(authCookie);
		assertFalse(authCookie.isEmpty());
		return authCookie;
	}	

	protected RestTemplate adminRest() {
		return restTemplateForAccountsAdmin;
	}
	
	
	
	protected RestTemplate createRestTemplateForUser(final String username,final String password) {
		final String authCookie = getAuthCookie(username,password);
		return new FormAuthenticatedRestTemplate(authCookie);
	}

	

}
