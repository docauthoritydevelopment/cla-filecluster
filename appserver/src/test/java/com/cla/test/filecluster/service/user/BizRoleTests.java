package com.cla.test.filecluster.service.user;

import com.cla.common.domain.dto.security.*;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.users.UserApiController;
import com.cla.filecluster.service.security.BizRoleService;
import com.cla.filecluster.service.security.RoleTemplateService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.*;

/**
 * Created by uri on 12/09/2016.
 */
@Category(FastTestCategory.class)
public class BizRoleTests extends ClaBaseTests {

    @Autowired
    private UserApiController userApiController;

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private RoleTemplateService roleTemplateService;

    private static final Logger logger = LoggerFactory.getLogger(BizRoleTests.class);

    @Test
    public void testGetBizRoles() {
        testStartLog(logger);
        List<BizRoleDto> bizRoles = userApiController.getBizRoles();
        Assert.assertThat("There should be at least 1 bizRole",bizRoles, Matchers.not(Matchers.emptyIterable()));
        for (BizRoleDto bizRole : bizRoles) {
            logger.debug("Got bizRole: {}",bizRole);
        }
    }

    @Test
    public void testGetRoles() {
        testStartLog(logger);
        List<SystemRoleDto> rolesWithAuthorities = userApiController.getRolesWithAuthorities();
        Assert.assertThat("There should be at least 1 role",rolesWithAuthorities, Matchers.not(Matchers.emptyIterable()));
        for (SystemRoleDto roleWithAuthority : rolesWithAuthorities) {
            logger.debug("Got role: {}",roleWithAuthority);
        }
    }

    @Test
    public void testCreateBizRole() {
        testStartLog(logger);
        BizRoleDto bizRoleDto = createBizRole("testCreateBizRole");
        Assert.assertNotNull(bizRoleDto);
    }

    @Test
    public void testCreateBizRoleSameNameDeleted() {
        testStartLog(logger);
        BizRoleDto bizRoleDto = createBizRole("testCreateBizRoleSameNameDeleted");
        Assert.assertNotNull(bizRoleDto);
        userApiController.deleteBizRole(bizRoleDto.getId());
        BizRoleDto bizRoleDto2 = createBizRole("testCreateBizRoleSameNameDeleted", bizRoleDto.getTemplate());
        Assert.assertNotNull(bizRoleDto2);
    }

    @Test(expected = BadRequestException.class)
    public void testCreateBizRoleSameNameFail() {
        testStartLog(logger);
        BizRoleDto bizRoleDto = createBizRole("testCreateBizRoleSameNameFail");
        Assert.assertNotNull(bizRoleDto);
        createBizRole("testCreateBizRoleSameNameFail");
    }

    @Test
    public void testDeleteBizRole() {
        testStartLog(logger);
        BizRoleDto bizRoleDto = createBizRole("testDeleteBizRole");
        Assert.assertNotNull(bizRoleDto.getId());
        userApiController.deleteBizRole(bizRoleDto.getId());
        List<BizRoleDto> bizRoles = userApiController.getBizRoles();
        for (BizRoleDto bizRole : bizRoles) {
            if (Objects.equals(bizRole.getId(), bizRoleDto.getId())) {
                throw new AssertionError("Found deleted bizRole in list");
            }
        }
    }

    @Test
    public void testUpdateBizRole() {
        testStartLog(logger);
        BizRoleDto bizRoleDto = createBizRole("testUpdateBizRole");
        bizRoleDto.setName("Updated");
        bizRoleDto.setDisplayName("Updated");
        BizRoleSummaryInfoDto bizRoleSummaryInfoDto = bizRoleService.updateBizRole(bizRoleDto,null);
        Assert.assertEquals("Updated",bizRoleSummaryInfoDto.getBizRoleDto().getName());
    }

    public BizRoleDto createBizRole(String name) {
        return createBizRole(name, null);
    }

    public BizRoleDto createBizRole(String name, RoleTemplateDto template) {

        List<SystemRoleDto> rolesWithAuthorities = userApiController.getRolesWithAuthorities();
        Set<SystemRoleDto> roles = Collections.singleton(rolesWithAuthorities.get(0));

        if (template == null) {
            template = new RoleTemplateDto();
            template.setName(name);
            template.setDisplayName(name);
            template.setDescription(name);
            template.setTemplateType(RoleTemplateType.NONE);
            template.setSystemRoleDtos(roles);
            template = roleTemplateService.createRoleTemplate(template);
        }

        testStartLog(logger);
        BizRoleDto bizRoleDto = new BizRoleDto();
        bizRoleDto.setName(name);
        bizRoleDto.setDisplayName(name);
        bizRoleDto.setDescription(name);
        bizRoleDto.setTemplate(template);
        bizRoleDto = userApiController.createBizRole(bizRoleDto);
        return bizRoleDto;
    }

    @Test
    public void testAddRemoveBizRoleToUser() {
        testStartLog(logger);
        UserDto userDto = new UserDto("testAddRemoveBizRoleToUser","testAddRemoveBizRoleToUser","testAddRemoveBizRoleToUser");
        userDto = userApiController.createUserOnCurrentAccount(userDto);
        BizRoleDto bizRoleDto = createBizRole("testAddRemoveBizRoleToUser");
        userApiController.addBizRoleToUser(userDto.getId(),bizRoleDto.getId());
        UserDto updatedUser = userApiController.getUserByIdWithRoles(userDto.getId());
        Assert.assertThat(updatedUser.getBizRoleDtos(),Matchers.hasItem(bizRoleDto));

        Page<UserDto> bizRoleAssignedUsers = userApiController.getCompRoleAssignedUsers(bizRoleDto.getId(),null);
        Assert.assertEquals(bizRoleAssignedUsers.getContent().size(),1);

        userApiController.removeBizRoleFromUser(userDto.getId(),bizRoleDto.getId());
        updatedUser = userApiController.getUserByIdWithRoles(userDto.getId());
        Assert.assertThat(updatedUser.getBizRoleDtos(),Matchers.not(Matchers.hasItem(bizRoleDto)));

        bizRoleAssignedUsers = userApiController.getCompRoleAssignedUsers(bizRoleDto.getId(),null);
        Assert.assertEquals(bizRoleAssignedUsers.getContent().size(),0);

    }

    @Test
    public void testUpdateUserRemoveBizRole() {
        testStartLog(logger);
        BizRoleDto bizRoleDto = createBizRole("testUpdateUserRemoveBizRole");

        UserDto userDto = new UserDto("testUpdateUserRemoveBizRole","testUpdateUserRemoveBizRole","testUpdateUserRemoveBizRole");
        userDto.setBizRoleDtos(Sets.newHashSet(bizRoleDto));
        UserDto userOnCurrentAccount = userApiController.createUserOnCurrentAccount(userDto);
        Assert.assertTrue(userOnCurrentAccount.getBizRoleDtos().contains(bizRoleDto));

        logger.info("now remove the role");

        userOnCurrentAccount.setBizRoleDtos(new HashSet<>());
        UserDto userDto1 = userApiController.updateUser(userOnCurrentAccount.getId(), userOnCurrentAccount);
        Assert.assertFalse(userDto1.getBizRoleDtos().contains(bizRoleDto));

    }
}
