package com.cla.test.filecluster.kvdb;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.kvdb.FileEntityStore;

import java.util.HashMap;
import java.util.function.BiConsumer;

/**
 * Created by: yael
 * Created on: 9/11/2019
 */
public class KFileEntityStore implements FileEntityStore {

    private HashMap<String, FileEntity> keysToFileEntity;

    public KFileEntityStore(HashMap<String, FileEntity> keysToFileEntity) {
        this.keysToFileEntity = keysToFileEntity;
    }

    @Override
    public boolean put(String key, FileEntity value) {
        boolean containsKey = keysToFileEntity.containsKey(key);
        keysToFileEntity.put(key, value);
        return containsKey;
    }

    @Override
    public FileEntity get(String key) {
        return keysToFileEntity.get(key);
    }

    @Override
    public long count() {
        return keysToFileEntity.size();
    }

    @Override
    public void delete(String key) {
        keysToFileEntity.remove(key);
    }

    @Override
    public void forEach(BiConsumer<String, FileEntity> action) {

    }
}
