package com.cla.test.filecluster;

import com.cla.filecluster.Application;
import com.cla.mediaproc.MediaProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@EnableWebSecurity
@Configuration
@Import({Application.class, MediaProcessor.class})
public class TestsWithMPConfiguration {

    
}
