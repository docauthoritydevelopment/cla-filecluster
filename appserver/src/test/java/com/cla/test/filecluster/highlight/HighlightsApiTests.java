package com.cla.test.filecluster.highlight;

import com.cla.common.domain.dto.extraction.FileHighlightsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.filecluster.service.files.managers.EntityHighlighterApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by shtand on 04/10/2015.
 */
@Category(FastTestCategory.class)
public class HighlightsApiTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(HighlightsApiTests.class);

    @Autowired
    private EntityHighlighterApiController entityHighlighterApiController;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private PerformanceTestService performanceTestService;

    private static RootFolderDto rootFolderDto;

    private static int testNum;

    private static int testRan = 0;

    @Before
    public void setUp() {
        if (testRan == 0) {
            testNum = getUnitTestNumber();
            rootFolderDto = performanceTestService.analyzeFolder("./src/test/resources/files/highlight");
        }
        ++testRan;
    }

    @After
    public void cleanup() {
        if (testRan == testNum) {
            deleteRootFolder(rootFolderDto.getId());
        }
    }

    @Test
    public void testHighlightsTextInWordFile() {

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().stream().filter(f-> f.getBaseName().equalsIgnoreCase("test1.doc")).findFirst().get();
        long fileId = claFileDto.getId();
        Map<String,String> params = new HashMap<>();
        params.put("take","10");
        FilterDescriptor filterDescriptor = new FilterDescriptor("contentFilter","business advisers","contains");
        String filterString = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);
        params.put("filter",filterString);
        FileHighlightsDto highlights = entityHighlighterApiController.findHighlightsInFiles(fileId,100,params,null);
        Assert.assertTrue("highlights should not be null",highlights != null);
        logger.debug("Got {} highlights in the first file for the word 'and'", highlights.getSnippets());
        Assert.assertTrue("highlight word snippets found",highlights.getSnippets().size()==1);
    }

    @Test
    public void testHighlightsTextInExcelFileWithLocations() {

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().stream().filter(f-> f.getBaseName().equalsIgnoreCase("test2.xls")).findFirst().get();
        long fileId = claFileDto.getId();
        Map<String,String> params = new HashMap<>();
        params.put("take","10");
        FilterDescriptor filterDescriptor = new FilterDescriptor("contentFilter","curve session","contains");
        String filterString = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);
        params.put("filter",filterString);
        FileHighlightsDto highlights = entityHighlighterApiController.findHighlightsInFiles(fileId,100, params,null);
        logger.debug("highlights: {}",highlights);
        Assert.assertTrue("highlights should not be null",highlights != null);
        logger.debug("Got {} highlights in the first file for the word 'and'", highlights.getSnippets());
        Assert.assertEquals("highlight execl snippets found", 2, highlights.getSnippets().size());
        Assert.assertEquals("highlight execl sheet locations ",1, highlights.getLocations().size());
        Assert.assertEquals("highlight execl cells locations found for two content search words", 2, highlights.getLocations().get("Sheet1").size());
    }

    @Ignore
    @Test
    public void testHighlightPatternsFile() {

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);
        long fileId = claFileDto.getId();
        Map<String,String> params = new HashMap<>();
        params.put("take","10");
        FilterDescriptor filterDescriptor = new FilterDescriptor("contentFilter","contains","American Public Energy");
        String filterString = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);
        params.put("filter",filterString);
        FileHighlightsDto highlights = entityHighlighterApiController.extractionHighlightFilePatterns2(fileId,100,params,null);
         logger.debug("Got {} highlights in the first file for the word 'and'", highlights.getSnippets());
        Assert.assertNotNull("highlights found",highlights);
    }

    @Ignore
    @Test
    public void testHighlightEntityExtractionFile() {
//  must run after extract

        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(null);
        ClaFileDto claFileDto = filesDeNormalized.getContent().get(0);
        long fileId = claFileDto.getId();
        Map<String,String> params = new HashMap<>();
        params.put("take","10");
        FileHighlightsDto highlights = entityHighlighterApiController.extractionHighlightEntity(fileId,100,params,null);
        logger.debug("Got {} highlights extracted entites", highlights.getSnippets().size()==6);
        Assert.assertNotNull("highlights found",highlights);
    }
}
