package com.cla.test.filecluster.service.schedule;

import com.cla.common.domain.dto.generic.ImportRowResultDto;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.common.domain.dto.schedule.*;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.repository.jpa.schedule.RootFolderScheduleRepository;
import com.cla.filecluster.repository.jpa.schedule.ScheduleGroupRepository;
import com.cla.filecluster.repository.jpa.schedule.SchedulePresetRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.service.schedule.ScheduleSchemaLoader;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.apache.commons.compress.utils.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.concurrent.Callable;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by liora on 03/05/2017.
 */
@RunWith(MockitoJUnitRunner.class)
@Category(UnitTestCategory.class)
public class ScheduleImportTests {

    private static final String ENTITIES_FOLDER = "./src/test/resources/entities";

    private Logger logger = LoggerFactory.getLogger(SchedulingConfigurationTests.class);

    private ScheduleSchemaLoader scheduleSchemaLoader;

    private CsvReader csvReader;

    @InjectMocks
    private ScheduleGroupService scheduleGroupService;

    @Mock
    ScheduleGroupRepository scheduleGroupRepository;

    @Mock
    RootFolderScheduleRepository rootFolderScheduleRepository;

    @Mock
    SchedulePresetRepository scheduleConfigurationRepository;

    @Mock
    private DocStoreService docStoreService;

    @Mock
    private DBTemplateUtils dBTemplateUtils;

    @Mock
    private MessageHandler messageHandler;

    @Before
    public void init() {
        when(messageHandler.getMessage(any())).thenReturn("err");
        csvReader = new CsvReader();
        scheduleSchemaLoader = new ScheduleSchemaLoader();
        ReflectionTestUtils.setField(scheduleSchemaLoader, "scheduleGroupService", scheduleGroupService);
        ReflectionTestUtils.setField(scheduleSchemaLoader, "csvReader", csvReader);
        ReflectionTestUtils.setField(scheduleSchemaLoader, "dBTemplateUtils", dBTemplateUtils);
        ReflectionTestUtils.setField(scheduleSchemaLoader, "messageHandler", messageHandler);
        when(scheduleGroupRepository.save(any(ScheduleGroup.class))).thenAnswer(i -> ((ScheduleGroup) i.getArguments()[0]));
        when(dBTemplateUtils.doInTransaction(any(Callable.class))).then(i -> ((Callable<Boolean>) i.getArguments()[0]).call());
    }

    @Test
    public void testImportFromCsv_validation_ErrorsFound() throws IOException {
        logger.info("testImportFromCsv_validation_WarningsFound");

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "scheduleGroups_for_import1.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = scheduleSchemaLoader.doImport(true, rawData, 200, false);
        logger.debug("import result {}", importSummaryResultDto);

        Assert.assertEquals("Should have accept validation for 6 schedule without warnings", 5, importSummaryResultDto.getImportedRowCount());
        Assert.assertEquals("Should have 4 errors for import validation", 5, importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no duplications for import validation", 0, importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have no warnings for import validation", 0, importSummaryResultDto.getWarningRowCount());

        Assert.assertNotNull(importSummaryResultDto.getErrorImportRowResultDtos());
        ImportRowResultDto itemRow = importSummaryResultDto.getErrorImportRowResultDtos().stream()
                .filter(x -> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("ParseTypeEnumError"))
                .findFirst().get();
        Assert.assertNotNull("Should have error message for schedule group", itemRow.getMessage());

        Assert.assertNotNull(importSummaryResultDto.getErrorImportRowResultDtos());
        ImportRowResultDto itemRow1 = importSummaryResultDto.getErrorImportRowResultDtos().stream()
                .filter(x -> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("ScheduleWithMssingType"))
                .findFirst().get();
        Assert.assertNotNull("Should have error message for schedule group with missing type", itemRow1.getMessage());

        ImportRowResultDto itemRow2 = importSummaryResultDto.getErrorImportRowResultDtos().stream()
                .filter(x -> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("HourlyWithMissingHours"))
                .findFirst().get();
        Assert.assertNotNull("Should have error message for hourly missing hours", itemRow2.getMessage());

        ImportRowResultDto itemRow3 = importSummaryResultDto.getErrorImportRowResultDtos().stream()
                .filter(x -> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("ParseIntError"))
                .findFirst().get();
        Assert.assertNotNull("Should have error message for weekly with missing days of the week", itemRow3.getMessage());
        ImportRowResultDto itemRow4 = importSummaryResultDto.getErrorImportRowResultDtos().stream()
                .filter(x -> x.getRowDto().getFieldsValues()[0].equalsIgnoreCase("WeeklyWithMissingDaysOfTheWeek"))
                .findFirst().get();
        Assert.assertNotNull("Illegal chron expression, every hour should be greater than 0", itemRow4.getMessage());
    }

    @Test
    public void testImportFromCsv() throws IOException {
        logger.info("testImportFromCsv");

        ArgumentCaptor<ScheduleGroup> scheduleGroupCaptor = ArgumentCaptor.forClass(ScheduleGroup.class);

        InputStream fileInputStream = new FileInputStream(ENTITIES_FOLDER + File.separator + "scheduleGroups_for_import1.csv");
        byte[] rawData = IOUtils.toByteArray(fileInputStream);
        ImportSummaryResultDto importSummaryResultDto = scheduleSchemaLoader.doImport(false, rawData, null, false);
        verify(scheduleGroupRepository, times(5)).save(scheduleGroupCaptor.capture());

        logger.debug("import result {}", importSummaryResultDto);
        Assert.assertEquals("Should have accept validation for 5 schedule without warnings", 5, importSummaryResultDto.getImportedRowCount());
        Assert.assertEquals("Should have 5 errors for import validation", 5, importSummaryResultDto.getErrorRowCount());
        Assert.assertEquals("Should have no duplications for import validation", 0, importSummaryResultDto.getDuplicateRowCount());
        Assert.assertEquals("Should have no warnings for import validation", 0, importSummaryResultDto.getWarningRowCount());

        List<ScheduleGroup> capturedScheduleGroups = scheduleGroupCaptor.getAllValues();
        Assert.assertEquals(5, capturedScheduleGroups.size());

        ScheduleGroup scheduleGroup = capturedScheduleGroups.get(0);
        Assert.assertNotNull("Schedule group was imported and inserted to db", scheduleGroup);
        Assert.assertEquals("name was imported correctly", "DailyTestImport", scheduleGroup.getName());
        Assert.assertEquals("isActive was imported correctly", true, scheduleGroup.isActive());
        Assert.assertEquals("getSchedulingDescription was imported correctly", "Desc", scheduleGroup.getSchedulingDescription());
        Assert.assertEquals("getAnalyzePhaseBehavior was imported correctly", CrawlerPhaseBehavior.EVERY_ROOT_FOLDER, scheduleGroup.getAnalyzePhaseBehavior());
        Assert.assertEquals("0 37 2 */1 * *", scheduleGroup.getCronTriggerString());
        Assert.assertEquals("{\"scheduleType\":\"DAILY\",\"hour\":2,\"minutes\":37,\"every\":1,\"daysOfTheWeek\":null,\"customCronConfig\":null}", scheduleGroup.getSchedulingJson());

        scheduleGroup = capturedScheduleGroups.get(4);
        Assert.assertEquals("name was imported correctly", "WeeklyTestImport", scheduleGroup.getName());
        Assert.assertNotNull("Schedule group was imported and inserted to db", scheduleGroup);
        Assert.assertEquals("isActive was imported correctly", true, scheduleGroup.isActive());
        Assert.assertEquals("0 0 1 * * MON,THU", scheduleGroup.getCronTriggerString());

        scheduleGroup = capturedScheduleGroups.get(1);
        Assert.assertEquals("name was imported correctly", "default123", scheduleGroup.getName());
        Assert.assertNotNull("Schedule group was imported and inserted to db", scheduleGroup);
        Assert.assertEquals("isActive was imported correctly", false, scheduleGroup.isActive());
        Assert.assertEquals("0 0 0 */1 * *", scheduleGroup.getCronTriggerString());

        scheduleGroup = capturedScheduleGroups.get(2);
        Assert.assertEquals("name was imported correctly", "HourlyTestImport", scheduleGroup.getName());
        Assert.assertEquals("0 14 */1 * * *", scheduleGroup.getCronTriggerString());

        scheduleGroup = capturedScheduleGroups.get(3);
        Assert.assertEquals("name was imported correctly", "HourlyWithInvalidHour", scheduleGroup.getName());
        Assert.assertEquals("0 14 */1 * * *", scheduleGroup.getCronTriggerString());
    }
}
