package com.cla.test.filecluster.service.sharepoint;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.*;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.filecluster.service.api.*;
import com.cla.filecluster.service.api.media.SharePointApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.SharePointTestCategory;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Category(SharePointTestCategory.class)
public class TestScanSharePointRootFolder extends ClaBaseTests {

    public static final String SHARE_TEST = "simpleda\\sharetest";
    private final static Logger logger = LoggerFactory.getLogger(TestScanSharePointRootFolder.class);

    @Autowired
    SharePointApiController sharePointApiController;

    @Autowired
    FileTreeApiController fileTreeApiController;

    @Autowired
    PerformanceTestService performanceTestService;

    @Autowired
    FilesApiController filesApiController;

    @Autowired
    FileUserApiController fileUserApiController;

    private SharePointConnectionDetailsDto sharePointConnectionDetails;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @BeforeClass
    public static void beforeClass() {
        printToErr(TestScanSharePointRootFolder.class.getName());
    }

    @Before
    public void scanBaseDir() {
        performanceTestService.prepareSqlSchema();
//        referenceRootFolderDto = performanceTestService.analyzeReferenceFiles2();
        logger.info("***********************************************************************");
    }

    @Test
    @Ignore("Takes too much time")
    public void testScanSharePointRootSubFolder() {
        configureSharePointConnector();
        RootFolderDto rootFolderDto = configureRootFolder(PerformanceTestService.SITES_TEST_TEST_LIBRARY_SUBFOLDER);
        scanRootFolder(rootFolderDto);
        Map<String, String> params = new HashMap<>();
        params.put(DataSourceRequest.BASE_FILTER_FIELD, "rootFolderId");
        params.put(DataSourceRequest.BASE_FILTER_VALUE, String.valueOf(rootFolderDto.getId()));
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(params);
        Assert.assertThat("There should be at least one folder", foldersFileCount.getContent().size(), Matchers.greaterThan(0));
        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            logger.debug(aggregationCountFolderDto.toString());
        }
        logger.info("Rerun on rootFolder");
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(), performanceTestService.createWaitParams());

        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/Shared%20Documents?RootFolder=%2Fsites%2Ftest%2FShared%20Documents%2FAdmin
    }

    @Test
    public void testScanSharePointRootFolder() {
        configureSharePointConnector();
        RootFolderDto rootFolderDto = configureRootFolder(PerformanceTestService.SITES_TEST_TEST_LIBRARY);
        scanRootFolder(rootFolderDto);

        //Test scan counter bug
   //     checkRunPhases();


        Map<String, String> params = performanceTestService.createFilterParams("rootFolderId", String.valueOf(rootFolderDto.getId()));

        logger.info("Check that share point folder was scanned as expected");
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(params);
        Assert.assertThat("There should be at least one folder with files", foldersFileCount.getContent().size(), Matchers.greaterThan(0));
        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            logger.debug(aggregationCountFolderDto.toString());
        }
        logger.info("***********************************************************************************");
        logger.info("*************************     Scan the rootFolder again   *************************");
        logger.info("***********************************************************************************");
        scanRootFolder(rootFolderDto);

        foldersFileCount = fileTreeApiController.findFoldersFileCount(params);
        Assert.assertThat("There should be at least one folder with files", foldersFileCount.getContent().size(), Matchers.greaterThan(0));

        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            DocFolderDto item = aggregationCountFolderDto.getItem();
            if (StringUtils.isEmpty(item.getRootFolderNickName())) {
                throw new AssertionError("Missing nickName on docFolder");
            }
        }


        logger.info("Verify that we got PDF files");
        Map rootFolderFilterParams = performanceTestService.createFilterParams("rootFolderId", String.valueOf(rootFolderDto.getId()));
        PageImpl<ClaFileDto> filesDeNormalized = filesApiController.findFilesDeNormalized(rootFolderFilterParams);
        List<ClaFileDto> pdfFiles = filesDeNormalized.getContent().stream().filter(f -> f.getBaseName().endsWith("pdf")).collect(Collectors.toList());
        Assert.assertThat(pdfFiles.size(), Matchers.greaterThan(0));
        logger.info("Verify that we got the special permissions file");
        Optional<ClaFileDto> floridaFileOptional = pdfFiles.stream().
                filter(f -> f.getBaseName().equals("Florida Advance Directive.pdf")).findFirst();
        Assert.assertTrue("Verify that the file: {} exists", floridaFileOptional.isPresent());
        ClaFileDto floridaFile = floridaFileOptional.get();
        logger.debug("testListAclReadFileCount on file {}", floridaFile);
        Map filterParams = performanceTestService.createFilterParams("file.id", String.valueOf(floridaFile.getId()));
        //todo: ask uri
        /*FacetPage<AggregationCountItemDTO<FileUserDto>> aclFileCount = fileUserApiController.getReadAclFileCount(filterParams);
        Optional<AggregationCountItemDTO<FileUserDto>> first = aclFileCount.getContent().stream().filter(f -> f.getItem().getUser().equals(SHARE_TEST)).findFirst();
        if (!first.isPresent()) {
            for (AggregationCountItemDTO<FileUserDto> fileUserDtoAggregationCountItemDTO : aclFileCount.getContent()) {
                logger.debug("Got ACL: {}", fileUserDtoAggregationCountItemDTO.getItem().getUser());
            }
            throw new AssertionError(SHARE_TEST + " user is missing from file");
        }*/

        //http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/Shared%20Documents?RootFolder=%2Fsites%2Ftest%2FShared%20Documents%2FAdmin

    }

//    public void checkRunPhases() {
//        Page<CrawlRunDetailsDto> crawlRunHistory = jobManagerApiController.getCrawlRunHistory(null);
//        CrawlRunDetailsDto crawlRunDetailsDto = crawlRunHistory.getContent().get(0);
//        Page<PhaseDetailsDto> crawlRunPhaseHistory = jobManagerApiController.getCrawlRunPhaseHistory(crawlRunDetailsDto.getId(), null);
//        PhaseDetailsDto scanPhase = crawlRunPhaseHistory.getContent().get(0);
//        logger.debug("Verify that scan counter > 0 on scan phase: {}", scanPhase);
//        Assert.assertThat(scanPhase.getPhaseCounter(), Matchers.greaterThan(0));
//    }


    private void scanRootFolder(RootFolderDto rootFolderDto) {
        logger.info("Scan rootFolder {}", rootFolderDto);
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(), performanceTestService.createWaitParams());
    }

    private RootFolderDto configureRootFolder(String path) {
        logger.info("Configure RootFolder");

        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setCrawlerType(CrawlerType.MEDIA_PROCESSOR);
        rootFolderDto.setPath(path);
        rootFolderDto.setNickName("DocAuthority SharePoint");
        rootFolderDto.setMediaType(MediaType.SHARE_POINT);
        rootFolderDto.setMediaConnectionDetailsId(sharePointConnectionDetails.getId());
        rootFolderDto.setScannedFilesCountCap(200);
        RootFolderDto rootFolder = fileTreeApiController.createRootFolder(rootFolderDto, null);
        logger.debug("Configured rootFolder {}", rootFolder);
        return rootFolder;
    }

    private void configureSharePointConnector() {
        logger.info("Configure SharePoint Connector");
        sharePointConnectionDetails = performanceTestService.createSharePointConnectionDetails();
        sharePointConnectionDetails = (SharePointConnectionDetailsDto) sharePointApiController.configureSharePointConnectionDetails(sharePointConnectionDetails);
    }
}
