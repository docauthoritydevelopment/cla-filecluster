package com.cla.test.filecluster;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.repository.jpa.MissedFileMatchRepository;
import com.cla.filecluster.repository.solr.GroupSearchRepository;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.collect.Lists;
import org.bitbucket.radistao.test.annotation.AfterAllMethods;
import org.bitbucket.radistao.test.annotation.BeforeAllMethods;
import org.bitbucket.radistao.test.runner.BeforeAfterSpringTestRunner;
import org.junit.*;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Method;
import java.util.*;

@RunWith(BeforeAfterSpringTestRunner.class)
@ContextConfiguration(classes = TestsWithMPConfiguration.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = "server.port=19080")
@ActiveProfiles("webapp,test")
public abstract class ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(ClaBaseTests.class);

    private static final String ADMIN_CREDENTIALS = "123";

    @Rule
    public TestName testName = new TestName();

    @Autowired
    private UserService userService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    protected ScheduledOperationService scheduledOperationService;

    @Autowired
    protected NGramTokenizerHashed nGramTokenizerHashed;

    @Autowired
    private ApplicationContext appContext;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrContentMetadataRepository solrContentMetadataRepository;

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Autowired
    private MissedFileMatchRepository missedFileMatchRepository;

    @Autowired
    private GroupSearchRepository groupSearchRepository;

    @Autowired
    private RootFolderService rootFolderService;

    @Value("http://localhost:${local.server.port}")
    private String baseUrl;

    public static final String RESCAN_EVENTS_SYSTEM_TESTS = "rescan_events_system_tests";
    public static final String TEST_TOPIC = "templateTopic";
    public static final String SCAN_REQUEST_TOPIC = "test.scan.requests";
    public static final String SCAN_RESULT_TOPIC = "test.scan.results";
    public static final String INGEST_REQUEST_TOPIC = "test.ingest.requests";
    public static final String INGEST_RESULT_TOPIC = "test.ingest.results";
    public static final String MEDIA_PROC_NOTIFICATIONS_TOPIC = "mediaproc.notifications";

    public static boolean isStart = true;
    protected static RootFolderDto rootFolderDtoAnalyze;

    public String getBaseUrl() {
        return baseUrl;
    }

    @BeforeAllMethods
    public void setupBeforeAll() {
        if (isStart) {
            isStart = false;
            logger.warn("START TEST SETUP =============================================================");
            UserDto userDto = userService.getAccountsAdminDto();
            ClaUserDetails principal = new ClaUserDetails(userDto);
            SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(principal, ADMIN_CREDENTIALS));
            performanceTestService.prepareSystem();
            rootFolderDtoAnalyze = performanceTestService.analyzeReferenceFiles2();
            filesDataPersistenceService.commitSolr();
            logger.warn("TEST SETUP DONE =============================================================");
        }
    }

    @AfterAllMethods
    public void tearDownAfterAll() {
    }

    @BeforeClass
    public static void start() {
        logger.debug("==> START TEST");
    }

    @AfterClass
    public static void end() {
        logger.debug("==> END TEST");
    }

    @Before
    public void init() {
        UserDto userDto = userService.getAccountsAdminDto();
        ClaUserDetails principal = new ClaUserDetails(userDto);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(principal, ADMIN_CREDENTIALS));
    }

    public void testStartLog(Logger logger) {
        logger.info("LogTestStart: {}", this.testName.getMethodName());
    }

    protected static void printToErr(String className) {
        System.err.println(className);
    }

    protected void deleteRootFolder(Long id) {
        rootFolderService.deleteRootFolder(id);
        String storeName = kvdbRepo.getRepoName(id, "appserver");
        if (kvdbRepo.getFileEntityDao().storeExists(storeName)) {
            kvdbRepo.getFileEntityDao().deleteStore(storeName);
        }
    }

    public int getUnitTestNumber() {
        final List<Method> methods = new ArrayList<>();
        Class<?> klass = this.getClass();
        while (klass != Object.class) { // need to iterated thought hierarchy in order to retrieve methods from above the current instance
            // iterate though the list of methods declared in the class represented by klass variable, and add those annotated with the specified annotation
            final List<Method> allMethods = new ArrayList<>(Arrays.asList(klass.getDeclaredMethods()));
            for (final Method method : allMethods) {
                if (method.isAnnotationPresent(Test.class) && !method.isAnnotationPresent(Ignore.class)) {
                    methods.add(method);
                }
            }
            // move to the upper class in the hierarchy in search for more methods
            klass = klass.getSuperclass();
        }
        return methods.size();
    }

    public ClaFileDto getFileWithContent(PageImpl<ClaFileDto> claFileDtos) {
        ClaFileDto claFileDto = null;
        for (ClaFileDto file : claFileDtos.getContent()) {
            if (file.getFsFileSize() > 0) {
                claFileDto = file;
                break;
            } else {
                logger.warn("yael file size issue id {} size {} content {}",file.getId(),file.getFsFileSize(),file.getContentId());
            }
        }
        return claFileDto;
    }

    public void ungroupFile(Long fileId, Long contentId, String groupId) {

        String compositeId = FileCatCompositeId.of(contentId, fileId).toString();

        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.ID, SolrOperator.EQ, compositeId)));
        fileCatService.updateFieldByQuery(CatFileFieldType.USER_GROUP_ID.getSolrName(), null, SolrFieldOp.SET, null, filterQueries, true);
        fileCatService.updateFieldByQuery(CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName(), null, SolrFieldOp.SET, null, filterQueries, true);

        contentMetadataService.updateAnalysisGroupIdByContentIds(Lists.newArrayList(contentId), null);
        contentMetadataService.updateUserGroupIdByContentIds(Lists.newArrayList(contentId), null, groupId);
    }
}
