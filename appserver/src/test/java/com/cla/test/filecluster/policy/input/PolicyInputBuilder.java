package com.cla.test.filecluster.policy.input;

import com.cla.common.domain.dto.event.EventType;
import com.cla.common.predicate.KeyValuePredicate;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.policy.domain.dto.ActionPluginsDefinition;

import java.util.List;

/**
 * For testing input builder pattern for policies
 */
public class PolicyInputBuilder {

    private PolicyConfig policyConfig = new PolicyConfig();

    public static PolicyInputBuilder newBuilder() {
        return new PolicyInputBuilder();
    }

    private PolicyInputBuilder() {

    }


    public PolicyInputBuilder withEventTypes(List<EventType> eventTypes) {
        policyConfig.setRelevantEventTypes(eventTypes);
        return this;
    }

    public PolicyInputBuilder withName(String name) {
        policyConfig.setName(name);
        return this;
    }

    public PolicyInputBuilder withPredicates(List<KeyValuePredicate> predicates) {
        policyConfig.setPredicates(predicates);
        return this;
    }

    public PolicyInputBuilder withActionPluginsDefinitions(List<ActionPluginsDefinition> actionPluginsDefinitions) {
        policyConfig.setActionPluginsDefinitions(actionPluginsDefinitions);
        return this;
    }

    public PolicyConfig build() {
        return policyConfig;
    }

}
