package com.cla.test.filecluster.it;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;

import com.cla.common.domain.dto.RawDocumentMessageDto;

public class RawDocumentMessageDtoServiceIntegrationTest extends AbstractIntegrationTest{

	@Test
	@Ignore
	public void testPutAndDelete() throws FileNotFoundException, IOException {
		final RawDocumentMessageDto rawDocumentMessageDto = new RawDocumentMessageDto("Distribution.docx","abc",IOUtils.toByteArray(new FileInputStream("./src/test/resources/files/wordXml/one/01032 Distribution.docx")),null,null,null,false);
		adminRest().put(getBaseUrl()+"/docs", rawDocumentMessageDto);
		adminRest().delete(getBaseUrl()+"/docs?url="+ rawDocumentMessageDto.getUrl());
	}
	
	
}
