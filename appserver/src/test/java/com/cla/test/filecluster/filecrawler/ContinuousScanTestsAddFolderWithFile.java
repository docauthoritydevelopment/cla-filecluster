package com.cla.test.filecluster.filecrawler;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.service.api.*;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Maps;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by uri on 15/03/2016.
 */
@Category(FastTestCategory.class)
public class ContinuousScanTestsAddFolderWithFile extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(ContinuousScanTestsAddFolderWithFile.class);

    private static final String SCAN_FOLDER_1 = "./build/scan/1";

    private static final String SAMPLE_FILE_1 = "Julie Simon Document.DOC";
    private static final String SAMPLE_FILE_2 = "Julie Simon Document - Updated.DOC";

    private static final Map<String,String> DATA_SOURCE_REQUEST_PARAMS = Maps.newHashMap("take", "999");

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    private UiServicesApiController uiServicesApiController;


    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private FileCrawlerExecutionDetailsService runService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    private CrawlerType crawlerType = CrawlerType.MEDIA_PROCESSOR;

    private RootFolderDto rootFolderDto;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(720); // 60 seconds max per method tested (45 was not enough)

    @After
    public void cleanup() throws Exception {
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(SCAN_FOLDER_1));
    }

    @Test
    public void testRescanRootFolderAddFolderWithFile() throws IOException {
        testStartLog(logger);

        (new File(SCAN_FOLDER_1)).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, SAMPLE_FILE_1).toAbsolutePath().normalize();
        Path target = Paths.get(SCAN_FOLDER_1, SAMPLE_FILE_1).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        logger.info("testRescanRootFolderAddFolderWithFile");
        rootFolderDto = performanceTestService.analyzeFolder(SCAN_FOLDER_1, crawlerType);
        logger.debug("Create new doc file under a new folder");
        target = Paths.get(SCAN_FOLDER_1 + "/newFolder", "addF" + System.currentTimeMillis() + ".doc");
        new File(SCAN_FOLDER_1 + "/newFolder").mkdir();
        Files.copy(source, target);

        rescanFolder(rootFolderDto.getId());

        logger.info("Verify one new event in file change log");
        // TODO 2.0: verify according to the stats (on the crawl run?);

        CrawlRunDetailsDto runDetails = runService.getRunDetails(2L);

        logger.info("Verify file exists under fullPath: " + rootFolderDto.getPath());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setFilter(new FilterDescriptor("rootFolderId", rootFolderDto.getId(), KendoFilterConstants.EQ));
        dataSourceRequest.setPageSize(10000);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        boolean found = false;
        logger.info("Looking for file {} in solr under path {}", target.getFileName().toString(), rootFolderDto.getPath());
        for (ClaFileDto claFileDto : claFileDtos.getContent()) {
            logger.debug("Got file {}", claFileDto);
            //Find the file
            if (claFileDto.getBaseName().equals(target.getFileName().toString())) {
                found = true;
            }
        }
        if (!found) {
            throw new RuntimeException("Failed to find file " + target.getFileName() + " in solr");
        }

    }


    public void printGroupCount() {
        Page<GroupDto> groupDtos = groupsApiController.listAllGroups(null);
        logger.info("Got a total of {} groups", groupDtos.getContent().size());
    }

    private void rescanFolder(long rootFolderId) {
        logger.info("Rescan folder {}", rootFolderId);
        Map<String, String> params = new HashMap<>();
        params.put("op", StartScanProducer.SCAN_INGEST_ANALYZE);
        params.put("wait", "true");
      //  params.put("rootFolderIds",String.valueOf(rootFolderId));
        jobManagerApiController.startScanRootFolder(rootFolderId,params);
        //Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY,fileCrawlerApiController.getFileCrawlingState().getLastRunStatus());
        //TODO 2.0 instead of reading fiel crawling state, read the root folder last crawl run status

    }



}
