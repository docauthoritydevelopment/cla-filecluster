package com.cla.test.filecluster.service.files.word;

import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.MissedFileMatch;
import com.cla.filecluster.jobmanager.service.CoordinationLockDeniedException;
import com.cla.filecluster.repository.jpa.MissedFileMatchRepository;
import com.cla.filecluster.service.crawler.analyze.AnalyzeFinalizeService;
import com.cla.filecluster.service.crawler.analyze.FileGroupingService;
import com.cla.filecluster.service.crawler.naming.GroupNamingService;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.IngestAnalyzeMechanismTestCategory;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@Category(IngestAnalyzeMechanismTestCategory.class)
public class ClaAnalyzeTests extends ClaBaseTests {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ClaAnalyzeTests.class);

    @Autowired
    private FileGroupingService fileGroupingService;

    @Autowired
    private GroupNamingService groupNamingService;

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private MissedFileMatchRepository missedFileMatchRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AnalyzeFinalizeService analyzeFinalizeService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Rule
    public Timeout globalTimeout = Timeout.seconds(90); // 60 seconds max per method tested (45 was not enough)

    @Test
    public void testAllIngestAndAnalyze() {
        testStartLog(logger);
        long start = System.currentTimeMillis();

        logger.info("Finished analyze in {} sec", (System.currentTimeMillis() - start) / 1000);
        Pair<Long, List<FileGroup>> groupRes = fileGroupService.findAllReportFullPage(0, 1000);
        final List<FileGroup> gList = groupRes.getValue();
        logger.debug("existing file groups {}", gList);
        assertTrue("Analyzer should have created at least 2 groups of files - instead created: " + gList.size(), gList.size() >= 2);
        assertTrue("Analyzer should have created a group with at least 2 files [" +
                gList.get(0).getName() + ": " + gList.get(0).getNumOfFiles() + "]", gList.get(0).getNumOfFiles() >= 2);
        assertTrue(gList.get(1).getNumOfFiles() >= 2);
        assertFalse(gList.get(0).getGeneratedName().isEmpty());
        assertFalse(gList.get(1).getGeneratedName().isEmpty());

        final MissedFileMatch mfm = missedFileMatchRepository.save(new MissedFileMatch(1L, 2L,
                MissedFileMatch.Reason.REASON_PRE_MATCHED));

        try {
            final MissedFileMatch mfm2 = missedFileMatchRepository.save(new MissedFileMatch(2L, 1L,
                    MissedFileMatch.Reason.REASON_PRE_MATCHED));
        } catch (final Exception e) {
            assertTrue(e instanceof DataIntegrityViolationException);
        }

        final Page<MissedFileMatch> p = missedFileMatchRepository.findByFileIdPaged(1L, PageRequest.of(0, 10));
        assertTrue(p.getContent().size() == 2);    // was 1 when files_idx constraint was in place
    }

    @Test
    @Ignore
    public void testWordIngestAndAnalyze() {
        logger.info(testName.toString());
        long start = System.currentTimeMillis();
        final String path = "./src/test/resources/files/word";
        performanceTestService.analyzeFolder(path);
        //TODO 2.0 - verify that word files exist in the system

//        List<RootFolderDto> rootFolderDtos = fileScannerService.scanFilesByPath(path, Arrays.asList(FileType.WORD));
//        RootFolderDto rootFolderDto = rootFolderDtos.get(0);
//        logger.info("Finished scan in {} sec", (System.currentTimeMillis() - start) / 1000);
//        long mid = System.currentTimeMillis();
//        applicationContext.getBean(LegacyIngestManager.class).ingestFiles(rootFolderDto.getId());
//        logger.info("Finished ingest in {} sec", (System.currentTimeMillis() - mid) / 1000);
//        applicationContext.getBean(LegacyIngestManager.class).finalizeIngestion(null);
//        mid = System.currentTimeMillis();
//        legacyFileAnalyzerService.analyzeFilesContent(null);
//        logger.info("Finished analyze in {} sec", (System.currentTimeMillis() - mid) / 1000);
        logger.info("Total test time: {}", System.currentTimeMillis() - start);
    }

    @Test
    @Ignore
    public void testExcelIngestAndAnalyze() {
        logger.info(testName.toString());
        long start = System.currentTimeMillis();
        final String path = "./src/test/resources/files/excel/one";
        performanceTestService.analyzeFolder(path);
        //TODO 2.0 - verify that excel files exist in the system

//        List<RootFolderDto> rootFolderDtos = fileScannerService.scanFilesByPath(path, Arrays.asList(FileType.EXCEL));
//        RootFolderDto rootFolderDto = rootFolderDtos.get(0);
//        logger.info("Finished scan in {} sec", (System.currentTimeMillis() - start) / 1000);
//        start = System.currentTimeMillis();
//        applicationContext.getBean(LegacyIngestManager.class).ingestFiles(rootFolderDto.getId());
//        logger.info("Finished ingest in {} sec", (System.currentTimeMillis() - start) / 1000);
//        applicationContext.getBean(LegacyIngestManager.class).finalizeIngestion(null);
//        start = System.currentTimeMillis();
//        legacyFileAnalyzerService.analyzeFilesContent(null);
        logger.info("Finished analyze in {} sec", (System.currentTimeMillis() - start) / 1000);
    }

    @Test
    @Ignore
    public void testPdfIngestAndAnalyze() {
        logger.info(testName.toString());
        long start = System.currentTimeMillis();
        final String path = "./src/test/resources/files/pdf";
        performanceTestService.analyzeFolder(path);
        //TODO 2.0 verify that PDF files exist in the file tree
//        List<RootFolderDto> rootFolderDtos = fileScannerService.scanFilesByPath(path, Arrays.asList(FileType.PDF));
//        RootFolderDto rootFolderDto = rootFolderDtos.get(0);
//        logger.info("Finished scan in {} sec", (System.currentTimeMillis() - start) / 1000);
//        start = System.currentTimeMillis();
//        applicationContext.getBean(LegacyIngestManager.class).ingestFiles(rootFolderDto.getId());
//        logger.info("Finished ingest in {} sec", (System.currentTimeMillis() - start) / 1000);
//        applicationContext.getBean(LegacyIngestManager.class).finalizeIngestion(null);
//        start = System.currentTimeMillis();
//        legacyFileAnalyzerService.analyzeFilesContent(null);
        logger.info("Finished analyze in {} sec", (System.currentTimeMillis() - start) / 1000);
    }

    @Test
    public void testNaming() {
        logger.info(testName.toString());
        groupNamingService.proposeGroupsNames(true, 0.1, null);
    }

    @Test
    public void testIndexTitles() {
        logger.info(testName.toString());
//		groupNamingService.reindexTitles(true);
        ProgressTracker progressTracker = new DummyProgressTracker();
        try {
            groupNamingService.fullGroupNamingByGroups(true, true, -1, true, null, progressTracker);
        } catch (CoordinationLockDeniedException e) {
            throw new RuntimeException("Unexpected coordination lock denial:", e);
        }
    }


//	@Test
//	public void testExcelExtractor(){
//		long start = System.currentTimeMillis();
//		final String path = "./src/test/resources/files/excel/two";
//		fileGroupingService.ingestExcelFiles(path);
//		logger.info("Finished ingest in {} sec",(System.currentTimeMillis()-start)/1000);
//		start = System.currentTimeMillis();
//		fileGroupingService.analyzeWordFiles();		
//		logger.info("Finished analyze in {} sec",(System.currentTimeMillis()-start)/1000);
//	}

}
