package com.cla.test.filecluster.filecrawler;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.mediaproc.service.managers.MediaProcessorStateService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Paths;

/**
 *
 * Created by uri on 14-Mar-17.
 */
@Category(FastTestCategory.class)
public class RemoteScanTests extends ClaBaseTests {

    private static final Logger logger = LoggerFactory.getLogger(RemoteScanTests.class);

    @Autowired
    PerformanceTestService performanceTestService;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    MediaProcessorStateService mediaProcessorStateService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Test(timeout = 90000)
    public void testRemoteScanEmptyFolder() throws Exception {
        testStartLog(logger);
        String dummyPath = "c:\\dummyPath\\testRemoteScan";
        RootFolderDto rootFolderDto = performanceTestService.createRootFolderIfNeeded(dummyPath, CrawlerType.MEDIA_PROCESSOR);
        logger.info("Remote run on root folder {}",rootFolderDto);
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(), performanceTestService.createWaitParams());
        Assert.assertThat("There should be at least 1 request",mediaProcessorStateService.getStartScanTaskRequests(), Matchers.greaterThan(0L));
        deleteRootFolder(rootFolderDto.getId());
    }

    @Test(timeout = 90000)
    public void testRemoteScanRealFolder() throws Exception {
        testStartLog(logger);
//        performanceTestService.printKafkaDetails();
        String path = Paths.get(".\\"+PerformanceTestService.SRC_TEST_RESOURCES_FILES_REMOTE).toAbsolutePath().normalize().toString();
        RootFolderDto rootFolderDto = performanceTestService.createRootFolderIfNeeded(path, CrawlerType.MEDIA_PROCESSOR);
        logger.info("Remote run on root folder {}",rootFolderDto);
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(),performanceTestService.createWaitParams());
        Assert.assertThat("There should be at least 1 request",mediaProcessorStateService.getStartScanTaskRequests(), Matchers.greaterThan(0L));
        Thread.sleep(100); //Give time for the run state to update
        verifyRunComplete(rootFolderDto);

        logger.info("Remote scan finished - now rescan");
        logger.debug("*********************************************************************************************");
        jobManagerApiController.startScanRootFolder(rootFolderDto.getId(),performanceTestService.createWaitParams());
        deleteRootFolder(rootFolderDto.getId());
    }

    private void verifyRunComplete(RootFolderDto rootFolderDto) {
        RootFolderSummaryInfo rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderDto.getId());
        CrawlRunDetailsDto crawlRunDetailsDto = rootFolderSummaryInfo.getCrawlRunDetailsDto();
        logger.debug("Got crawl run details {}", crawlRunDetailsDto);
        int count = 5;
        while (count-- > 0 && (crawlRunDetailsDto == null || !RunStatus.FINISHED_SUCCESSFULLY.equals(crawlRunDetailsDto.getRunOutcomeState()))) {
            logger.debug("Run may take 1-2 seconds to update. waiting {} more seconds for run {} and rf {}", count,
                    rootFolderSummaryInfo.getRootFolderDto().getLastRunId(), rootFolderSummaryInfo.getRootFolderDto().getId());
            try {
                Thread.sleep(1000);
                rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderDto.getId());
                crawlRunDetailsDto = rootFolderSummaryInfo.getCrawlRunDetailsDto();
                logger.debug("Got crawl run details {}", crawlRunDetailsDto);
            } catch (InterruptedException e) {
            }
        }
        Assert.assertEquals("Run should have finished successfully", RunStatus.FINISHED_SUCCESSFULLY, crawlRunDetailsDto.getRunOutcomeState());

        Integer numberOfFoldersFound = rootFolderSummaryInfo.getRootFolderDto().getNumberOfFoldersFound();
        Assert.assertThat("There should be at least one folder under rootFolder " + rootFolderDto.getId(), numberOfFoldersFound, Matchers.greaterThan(0));
    }


}
