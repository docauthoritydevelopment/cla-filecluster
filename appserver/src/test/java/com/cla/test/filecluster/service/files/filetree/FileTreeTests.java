package com.cla.test.filecluster.service.files.filetree;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.group.AttachFilesToGroupDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.filecluster.domain.converters.SolrFileGroupBuilder;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.service.api.FileTreeApiController;
import com.cla.filecluster.service.api.FileUserApiController;
import com.cla.filecluster.service.api.FilesApiController;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by uri on 18/08/2015.
 */

@Category(FastTestCategory.class)
public class FileTreeTests extends ClaBaseTests {

    private final static Logger logger = LoggerFactory.getLogger(FileTreeTests.class);

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private FileTreeApiController fileTreeApiController;

    @Autowired
    private GroupsApiController groupsApiController;

    @Autowired
    private SolrClientFactory solrClientFactory;

    @Autowired
    private FileUserApiController fileUserApiController;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private FilesApiController filesApiController;

    @Autowired
    private FileCatService fileCatService;

    private static DocStoreDto defaultDocStore;
    private static int testRan = 0;
    private static int testNum;

    private static RootFolderDto rootFolderDto;
    private static FileGroup f1;
    private static ClaFile file;

    @Before
    public void scanBaseDir() {
        if (testRan == 0) {
            testNum = getUnitTestNumber();
            defaultDocStore = docStoreService.getDefaultDocStoreAsDto();
            rootFolderDto = rootFolderDtoAnalyze;

            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setName("FileTreeTests1");
            f1 = fileGroupRepository.createGroup(builder, fileGroupRepository.getNextAvailableId(), GroupType.USER_GROUP);

            DataSourceRequest dataSourceRequest = new DataSourceRequest();
            dataSourceRequest.setFilter(performanceTestService.createConcreteFilter("groupId", "*", "NEQ"));
            PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
            logger.info("Got {} un-grouped files", claFileDtos.getContent().size());

            ClaFileDto claFileDto = getFileWithContent(claFileDtos);
            AttachFilesToGroupDto attachFilesToGroupDto = new AttachFilesToGroupDto();
            attachFilesToGroupDto.setGroupId(f1.getId());
            List<Long> fileIds = Lists.newArrayList(claFileDto.getId());
            attachFilesToGroupDto.setFileIds(fileIds);
            GroupDetailsDto groupDetailsDto = filesApiController.attachFilesToGroup(attachFilesToGroupDto, performanceTestService.createWaitParams());
            scheduledOperationService.runOperations();

            f1.setNumOfFiles(groupDetailsDto.getGroupDto().getNumOfFiles());
            file = fileCatService.getFileObject(claFileDto.getId());
            f1.setFirstMemberId(file.getId());
            updateGroup(f1.getNumOfFiles(), file.getId(), f1.getId());


        }
        ++testRan;
    }

    private void updateGroup(long numOfFiles, Long firstMember, String id) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(id);
        builder.setNumOfFiles(numOfFiles);
        builder.setFirstMemberId(firstMember);
        fileGroupRepository.saveGroupChanges(builder);
    }

    @After
    public void destroyGroups() {
        if (testRan == testNum) {
            ungroupFile(file.getId(), file.getContentMetadataId(), f1.getId());
        }
    }

    @Test
    public void testRootFolderSummaryInfo() {
        logger.info("testSummaryInfo");
        RootFolderSummaryInfo rootFolderSummaryInfo = fileTreeApiController.listRootFolderSummary(rootFolderDto.getId());
        Assert.assertNotNull(rootFolderSummaryInfo);
        assertEquals(rootFolderDto.getId(), rootFolderSummaryInfo.getRootFolderDto().getId());

        logger.debug("Check multiple root folders results");
        Page<RootFolderSummaryInfo> rootFolderSummaryInfos = fileTreeApiController.listRootFoldersSummary(null);
        Assert.assertNotNull(rootFolderSummaryInfos);
        Assert.assertThat(rootFolderSummaryInfos.getContent().size(), Matchers.greaterThan(0));
        boolean found = false;
        for (RootFolderSummaryInfo folderSummaryInfo : rootFolderSummaryInfos) {
            if (folderSummaryInfo.getRootFolderDto().getId().equals(rootFolderDto.getId())) {
                found = true;
            }
        }
        Assert.assertTrue(found);

        logger.debug("Check multiple root folders results with search term");
        found = false;
        HashMap<String, String> params = new HashMap<>();
        int length = rootFolderDto.getRealPath().length();
        try {
            String encodedNamingSearchTerm = java.net.URLEncoder.encode(rootFolderDto.getRealPath().substring(length - 5), "UTF-8");
            params.put("namingSearchTerm", encodedNamingSearchTerm);
        } catch (UnsupportedEncodingException e) {
        }
        rootFolderSummaryInfos = fileTreeApiController.listRootFoldersSummary(params);
        Assert.assertNotNull(rootFolderSummaryInfos);
        Assert.assertThat(rootFolderSummaryInfos.getContent().size(), Matchers.greaterThan(0));
        for (RootFolderSummaryInfo folderSummaryInfo : rootFolderSummaryInfos) {
            if (folderSummaryInfo.getRootFolderDto().getId().equals(rootFolderDto.getId())) {
                found = true;
            }
        }
        Assert.assertTrue(found);

        params = new HashMap<>();
        params.put("namingSearchTerm", "yyyyy");
        rootFolderSummaryInfos = fileTreeApiController.listRootFoldersSummary(params);
        assertEquals("There should be no rootFolders summaries with this search term", rootFolderSummaryInfos.getContent().size(), 0);
    }

    @Test
    public void testFileTree1() {
        logger.info("testFileTree1");
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), dataSourceRequest);
        logger.info("Got {} elements and {} totalElements docFolders in response", docFolderDtos.getNumberOfElements(), docFolderDtos.getTotalElements());
        Assert.assertTrue("Expect total doc folders to be more than 1", docFolderDtos.getTotalElements() > 1);
        Assert.assertTrue("Expect number of doc folders to be more than 1", docFolderDtos.getNumberOfElements() > 1);

        DocFolderDto docFolderDto = getRootFolderDto(docFolderDtos);
//        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);

        logger.debug("First returned folder: path={} name={}", docFolderDto.getPath(), docFolderDto.getName());
        Assert.assertTrue("Expect first returned folder to be root (no parent folder)", docFolderDto.getParentFolderId() == null);
        //Assert.assertTrue("Expect first returned folder to be root (folder name) " + docFolderDto.getName(), docFolderDto.getName().equals("analyzeTest"));
        //String rootPath = FilenameUtils.separatorsToUnix(docFolderDto.getPath());
        //Assert.assertTrue("Expect first returned folder to be root (path) " + rootPath, rootPath.endsWith(PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST + "/"));

        Map<Long, Long> subFolderNumForFolders = docFolderService.getFoldersSubFoldersNumber(Collections.singletonList(docFolderDto.getId()));
        Assert.assertTrue("Expect that root folder has numberOfChildren >0", subFolderNumForFolders.get(docFolderDto.getId()) > 0);
    }

    public DocFolderDto getRootFolderDto(Page<DocFolderDto> docFolderDtos) {
        return docFolderDtos.getContent().stream().filter(f -> rootFolderDto.getId().equals(f.getRootFolderId()) && f.getDepthFromRoot() == 0).findFirst().get();
    }

    @Test
    public void testFileTreeFromDir() {
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), dataSourceRequest);
        logger.info("Got {} elements and {} totalElements docFolders in response", docFolderDtos.getNumberOfElements(), docFolderDtos.getTotalElements());
        Assert.assertTrue("Expect total doc folders to be more than 1", docFolderDtos.getTotalElements() > 1);
        Assert.assertTrue("Expect number of doc folders to be more than 1", docFolderDtos.getNumberOfElements() > 1);

        DocFolderDto rootFolderDto = getRootFolderDto(docFolderDtos);
//        DocFolderDto rootFolderDto = docFolderDtos.getContent().get(0);

        DocFolder folderById = docFolderService.findById(rootFolderDto.getId());
        Map<Long, Long> subFolderNumForFolders = docFolderService.getFoldersSubFoldersNumber(Collections.singletonList(rootFolderDto.getId()));
        Assert.assertTrue("Expect that root folder has numberOfChildren >0", subFolderNumForFolders.get(rootFolderDto.getId()) > 0);
        docFolderDtos = docFolderService.listFolders(folderById, dataSourceRequest);
        logger.info("Got {} elements and {} totalElements docFolders in response", docFolderDtos.getNumberOfElements(), docFolderDtos.getTotalElements());
        Assert.assertTrue("Expect total doc folders to be more than 0", docFolderDtos.getTotalElements() > 0);
        Assert.assertTrue("Expect number of doc folders to be more than 0", docFolderDtos.getNumberOfElements() > 0);

    }

    @Test
    public void testFileTreeDepth() {
        logger.info("testFileTreeDepth");
        List<RootFolder> rootFolders = docStoreService.getRootFolders();
        logger.debug("There are currently {} root folders", rootFolders.size());
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), dataSourceRequest);
        DocFolderDto docFolderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());
//        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        int baseDepth = docFolderDto.getDepthFromRoot();
        dataSourceRequest.setMaxDepth(baseDepth);
        docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), dataSourceRequest);
        Assert.assertThat("Expect only root folder to return (" + docFolderDtos.getNumberOfElements() + ")", docFolderDtos.getNumberOfElements(), Matchers.lessThanOrEqualTo(rootFolders.size()));
        dataSourceRequest.setMaxDepth(baseDepth + 1);
        docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), dataSourceRequest);
        Assert.assertTrue("Expect several folders to return (" + docFolderDtos.getTotalElements() + ")", docFolderDtos.getTotalElements() > 1);

    }

    @Test
    public void testListFilesByFolder() {
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), new DataSourceRequest());
        DocFolderDto docFolderDto = getRootFolderDto(docFolderDtos);
//        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        Page<ClaFileDto> claFileDtos = filesApplicationService.listFilesByFolder(docFolderDto.getId(), dataSourceRequest);
        Assert.assertTrue("Expect several files to return (" + claFileDtos.getTotalElements() + ")", claFileDtos.getTotalElements() > 1);

        Page<ClaFileDto> claFileDtosRecursive = filesApplicationService.listFilesByFolderRecursive(docFolderDto.getId(), dataSourceRequest);
        Assert.assertTrue("Expect several files to return in recursive call (" + claFileDtosRecursive.getTotalElements() + ")", claFileDtosRecursive.getTotalElements() > 1);

        Assert.assertTrue("Expect more files in recursive call than non recursive call", claFileDtosRecursive.getTotalElements() > claFileDtos.getTotalElements());

    }

    @Test
    public void testGetParentFolder() {
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), new DataSourceRequest());
        DocFolderDto parentFolderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());
//        DocFolderDto parentFolderDto = docFolderDtos.getContent().get(0);
        Page<DocFolderDto> childFolderDtos = fileTreeApiController.listFoldersFromSubFolder(parentFolderDto.getId(), null);
        DocFolderDto childFolderDto = childFolderDtos.getContent().get(0);

        DocFolderDto parentFolderFromChild = docFolderService.findParentFolder(childFolderDto.getId());

        assertEquals("Parent folder from child should equal parent folder from list", parentFolderDto.getId(), parentFolderFromChild.getId());

        DocFolderDto childFolderDto2 = docFolderService.getDtoById(childFolderDto.getId());
    }

    @Test
    public void testRelativeDepth() {
        logger.info("testRelativeDepth");
        List<RootFolder> rootFolders = docStoreService.getRootFolders();

        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setRelDepth(1);
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), dataSourceRequest);
        Assert.assertThat("Expect only root folder to return (" + docFolderDtos.getNumberOfElements() + ")", docFolderDtos.getNumberOfElements(), Matchers.lessThanOrEqualTo(rootFolders.size()));

        DocFolderDto folderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());
        Assert.assertNotNull("Base docFolder under rootFolder " + rootFolderDto + " should exist", folderDto);
        Long rootId = folderDto.getId();
        DocFolder folderById = docFolderService.findById(rootId);

        docFolderDtos = docFolderService.listFolders(folderById, dataSourceRequest);
        Assert.assertTrue("Expect subfolders to return: " + docFolderDtos.getNumberOfElements(), docFolderDtos.getNumberOfElements() > 0);
    }

    @Test
    public void testListGroupsByFolder() {
        logger.info("testListGroupsByFolder");
        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.debug("List listGroupDetailsInFolder under the folder {}", docFolderDto);
        Page<AggregationCountItemDTO<GroupDto>> aggregationCountItemDTOs = fileTreeApiController.listGroupDetailsInFolder(docFolderDto.getId(), null);
        Assert.assertThat("verify we got at least one group", aggregationCountItemDTOs.getContent(), Matchers.not(Matchers.emptyIterable()));
        AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO = aggregationCountItemDTOs.getContent().get(0);
        Assert.assertThat("verify group has at least 1 file in counters", groupDtoAggregationCountItemDTO.getCount(), Matchers.greaterThan(0L));
    }

    @Test
    public void testListGroupsByFolderRecursive() {
        logger.info("testListGroupsByFolderRecursive (Recursive(Recursive(Recursive(Recursive(Recursive)))))");
        Page<DocFolderDto> docFolderDtos = fileTreeApiController.listFoldersInDefaultDocStore(null);
        DocFolderDto docFolderDto = docFolderDtos.getContent().get(0);
        logger.debug("listGroupDetailsInFolderRecursive under docFolder {}", docFolderDto);
        Page<AggregationCountItemDTO<GroupDto>> aggregationCountItemDTOs = fileTreeApiController.listGroupDetailsInFolderRecursive(docFolderDto.getId(), null);
        Assert.assertThat("verify we got at least 1 group", aggregationCountItemDTOs.getContent(), Matchers.not(Matchers.emptyIterable()));
        AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO = aggregationCountItemDTOs.getContent().get(0);
        Assert.assertThat("verify group " + groupDtoAggregationCountItemDTO + " counters > 0", groupDtoAggregationCountItemDTO.getCount(), Matchers.greaterThan(0L));
    }

    @Test
    public void testListFoldersByGroup() {
        logger.info("testListFoldersByGroup");
        Page<AggregationCountItemDTO<DocFolderDto>> aggregationCountItemDTOs = groupsApiController.listDocFoldersInGroup(f1.getId(), null);
        Assert.assertNotEquals("verify aggeration counters returns > 0 items ", 0, aggregationCountItemDTOs.getTotalElements());
        AggregationCountItemDTO<DocFolderDto> groupDtoAggregationCountItemDTO = aggregationCountItemDTOs.getContent().get(0);
        Assert.assertThat("verify first aggregation counter count > 0", groupDtoAggregationCountItemDTO.getCount(), Matchers.greaterThan(0L));
    }

    @Test
    public void testListFilesDeNormalized() {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertTrue("Expect several files to return (" + claFileDtos.getTotalElements() + ")", claFileDtos.getTotalElements() > 1);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.debug(claFileDto.getId() + " " + claFileDto.getFileName());
        }
    }

    @Test
    public void testListFilesDeNormalizedFilterSize() {
        logger.debug("testListFilesDeNormalizedFilterSize");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor filterDescriptor = performanceTestService.createConcreteFilter("size", "20000", "GTE");
        dataSourceRequest.setFilter(filterDescriptor);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertTrue("Expect several files to return (" + claFileDtos.getTotalElements() + ")", claFileDtos.getTotalElements() > 1);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.debug(claFileDto.getId() + " " + claFileDto.getFileName());
        }
    }

    @Test
    public void testListFilesDeNormalizedFilterCreationDateSolrDate() {
        //Solr date is always in: YYYY-MM-DDThh:mm:ssZ
        testListFilesDeNormalizedFilterCreationDate(CatFileFieldType.CREATION_DATE, "2000-12-31T23:59:59Z", KendoFilterConstants.GT);
    }

    @Test //todo - what kind of date it is?
    public void testListFilesDeNormalizedFilterCreationDateUTC() {
        testListFilesDeNormalizedFilterCreationDate(CatFileFieldType.APP_CREATION_DATE, "986114709", KendoFilterConstants.LT);
    }

    private void testListFilesDeNormalizedFilterCreationDate(CatFileFieldType field, String dateValue, String operator) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor filterDescriptor = performanceTestService.createConcreteFilter(field.getSolrName(), dateValue, operator);
        dataSourceRequest.setFilter(filterDescriptor);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertTrue("Expect several files to return (" + claFileDtos.getTotalElements() + ")",
                claFileDtos.getTotalElements() > 1);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.debug(claFileDto.getId() + " " + claFileDto.getFileName());
        }
    }

    @Test
    public void testListFilesDeNormalizedContentSearch() {
        testStartLog(logger);
        if (solrClientFactory.isSolrCloud()) {
            //TODO 2.0 - fix the content search mechanism in SolrCloud
            logger.warn("TODO 2.0 - fix the content search mechanism in SolrCloud");
            return;
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setContentFilter("Suggested");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertTrue("Expect several files to return (" + claFileDtos.getTotalElements() + ")", claFileDtos.getTotalElements() > 1);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.debug(claFileDto.getId() + " " + claFileDto.getFileName());
        }
    }


    @Test
    public void testListFilesDeNormalizedContentSearchQuotes() {
        testStartLog(logger);
        if (solrClientFactory.isSolrCloud()) {
            //TODO 2.0 - fix the content search mechanism in SolrCloud
            logger.warn("TODO 2.0 - fix the content search mechanism in SolrCloud");
            return;
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setContentFilter("\"refund obligation\"");
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertTrue("Expect several files to return (" + claFileDtos.getTotalElements() + ")", claFileDtos.getTotalElements() > 0);
        for (ClaFileDto claFileDto : claFileDtos) {
            logger.debug(claFileDto.getId() + " " + claFileDto.getFileName());
        }
    }

    @Test
    public void testComplexFilterMechanism2() {
        logger.info("testComplexFilterMechanism2");
        FacetPage<AggregationCountItemDTO<FileUserDto>> ownerFileCounts = fileUserApiController.getOwnerFileCounts(null);
        String user = ownerFileCounts.getContent().get(0).getItem().getUser();
        logger.debug("Test a complex filter with owner {}", user);

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        PageImpl<ClaFileDto> allFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.debug("testComplexFilterMechanism2 - without filters there are {} files", allFileDtos.getNumberOfElements());

        FilterDescriptor subFolderFilter = performanceTestService.createConcreteFilter("fullPath", "subFolder", KendoFilterConstants.CONTAINS);
        FilterDescriptor ownerFilter = performanceTestService.createConcreteFilter("file.ownerId", user, KendoFilterConstants.EQ);
        FilterDescriptor baseFilter = performanceTestService.createJoinFilter("and", subFolderFilter, ownerFilter);

        dataSourceRequest.setFilter(baseFilter);
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.debug("testComplexFilterMechanism2 - got {} files", claFileDtos.getNumberOfElements());

        Assert.assertThat("File counts should be different", claFileDtos.getNumberOfElements(), Matchers.lessThan(allFileDtos.getNumberOfElements()));
    }

    @Test
    public void testComplexFilterMechanism3() {
        logger.info("testComplexFilterMechanism3");
        if (solrClientFactory.isSolrCloud()) {
            //TODO 2.0 - Content filter doesn't work in Solr cloud
            return;
        }
        String textSearch = "bids";
        DataSourceRequest docFolderDataSourceRequest = DataSourceRequest.build(null);
        Page<DocFolderDto> docFolderDtos = docFolderService.listAllFolders(defaultDocStore.getId(), docFolderDataSourceRequest);
        DocFolderDto docFolderDto = performanceTestService.findDocFolderDtoOfRootFolder(docFolderDtos.getContent(), rootFolderDto.getId());

        logger.debug("Test a complex filter with content filter for {} and folderId={} (folder={})", textSearch, docFolderDto.getId(), docFolderDto.getName());

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor contentFilter = performanceTestService.createConcreteFilter("contentFilter", textSearch, KendoFilterConstants.CONTAINS);
        FilterDescriptor baseFilter = performanceTestService.createJoinFilter("and", contentFilter);

        dataSourceRequest.setFilter(baseFilter);
        dataSourceRequest.setBaseFilterField("folderId");
        dataSourceRequest.setBaseFilterValue(String.valueOf(docFolderDto.getId()));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        logger.debug("testComplexFilterMechanism3 - got {} files", claFileDtos.getNumberOfElements());

        Assert.assertThat("There should be at least one file", claFileDtos.getNumberOfElements(), Matchers.greaterThan(0));
    }


    @Test
    public void testUngroupedFilter() {
        logger.info("testUngroupedFilter");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        FilterDescriptor baseFilter = performanceTestService.createConcreteFilter("groupId", "*", KendoFilterConstants.NEQ);
        dataSourceRequest.setFilter(baseFilter);
        long unGroupedFiles = filesApplicationService.countFilesDeNormalized(dataSourceRequest);
        logger.info("Got {} ungrouped files", unGroupedFiles);
    }

    @Test
    public void testListServerBaseFolders() {
        logger.debug("testListServerBaseFolders");
        List<ServerResourceDto> strings = fileTreeApiController.listServerFolders(null, null);
        Assert.assertThat("There should be at least one server base folder", strings.size(), Matchers.greaterThan(0));
        for (ServerResourceDto serverResourceDto : strings) {
            logger.debug("Server base folder: {}", serverResourceDto);
        }
    }

    @Test
    public void testRootFolderNickName() {
        testStartLog(logger);
        Long rootFolderId = rootFolderDto.getId();
        RootFolderDto rootFolder = fileTreeApiController.getRootFolder(rootFolderId);
        rootFolder.setNickName("Test RootFolder");
        fileTreeApiController.updateRootFolder(rootFolderId, rootFolder);
        Map<String, String> filterParams = performanceTestService.createFilterParams("rootFolderId", rootFolderId);
        FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeApiController.findFoldersFileCount(filterParams);
        Assert.assertThat(foldersFileCount.getContent().size(), Matchers.greaterThan(0));
        DocFolderDto item = foldersFileCount.getContent().get(0).getItem();
        assertEquals("Test RootFolder", item.getRootFolderNickName());
        assertEquals(rootFolder.getPath().length(), item.getRootFolderLength());
        for (AggregationCountFolderDto aggregationCountFolderDto : foldersFileCount.getContent()) {
            item = aggregationCountFolderDto.getItem();
            //String substring = item.getPath().substring(item.getRootFolderLength());
            logger.debug(item.getPath());
        }
    }

}
