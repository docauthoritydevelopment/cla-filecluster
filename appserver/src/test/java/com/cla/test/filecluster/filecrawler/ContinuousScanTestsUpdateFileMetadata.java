package com.cla.test.filecluster.filecrawler;

import com.cla.connector.constants.KendoFilterConstants;
import com.cla.common.domain.dto.file.ClaFileDetailsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.CrawlerType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.service.api.JobManagerApiController;
import com.cla.filecluster.service.api.UiServicesApiController;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.test.service.PerformanceTestService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import org.apache.commons.io.FileUtils;
import org.assertj.core.util.Maps;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by uri on 18-Jun-17.
 */
@Category(FastTestCategory.class)
public class ContinuousScanTestsUpdateFileMetadata extends ClaBaseTests {

    private static final String SCAN_FOLDER_1 = "./build/scan/1";
    private static final String SAMPLE_FILE_2 = "Julie Simon Document - Updated.DOC";
    private static final String SAMPLE_FILE_1 = "Julie Simon Document.DOC";
    private static final Map<String,String> DATA_SOURCE_REQUEST_PARAMS = Maps.newHashMap("take", "999");
    private static final CrawlerType crawlerType = CrawlerType.MEDIA_PROCESSOR;

    private Logger logger = LoggerFactory.getLogger(ContinuousScanTestsUpdateFileMetadata.class);

    @Autowired
    private PerformanceTestService performanceTestService;

    @Autowired
    private JobManagerApiController jobManagerApiController;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private UiServicesApiController uiServicesApiController;

    private static RootFolderDto rootFolderDto;

    @After
    public void cleanup() throws Exception {
        deleteRootFolder(rootFolderDto.getId());
        FileUtils.deleteDirectory(new File(SCAN_FOLDER_1));
    }

    private void prepareScanFolder() throws IOException {
        (new File(SCAN_FOLDER_1)).mkdirs();
        Path source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, SAMPLE_FILE_2).toAbsolutePath().normalize();
        Path target = Paths.get(SCAN_FOLDER_1, SAMPLE_FILE_2).toAbsolutePath().normalize();
        if (!Files.exists(target)) {
            Files.copy(source, target);
        }
        source = Paths.get("." + PerformanceTestService.SRC_TEST_RESOURCES_FILES_ANALYZE_TEST, SAMPLE_FILE_1).toAbsolutePath().normalize();
        target = Paths.get(SCAN_FOLDER_1, "update.doc").toAbsolutePath().normalize();
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        target = Paths.get(SCAN_FOLDER_1, "update_acl.doc").toAbsolutePath().normalize();
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
    }

    @Test
    @Ignore
    public void testRescanRootFolderUpdateFileMetadata() throws IOException {
        prepareScanFolder();
        Path target = Paths.get(SCAN_FOLDER_1, "update_acl.doc").toAbsolutePath().normalize();
        logger.info("testRescanRootFolderUpdateFileMetadata");
        rootFolderDto = performanceTestService.analyzeFolder(SCAN_FOLDER_1, crawlerType);
        logger.debug("Update file metadata");

        logger.debug("Add write permission to NETWORK user");
        UserPrincipalLookupService lookupservice = FileSystems.getDefault().getUserPrincipalLookupService();
        UserPrincipal userPrincipal = lookupservice.lookupPrincipalByName("NETWORK");
        UserPrincipal removedPrincipal = changeAccess(userPrincipal, target);
        rescanFolder(rootFolderDto.getId());

        logger.info("Verify file exists under fullPath: " + rootFolderDto.getPath());
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        String filePath = target.toAbsolutePath().normalize().toString();
        logger.info("Looking for file {} in solr", filePath);
        dataSourceRequest.setFilter(new FilterDescriptor("fullName", filePath.substring(rootFolderDto.getRealPath().length()), KendoFilterConstants.EQ));
        PageImpl<ClaFileDto> claFileDtos = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
        Assert.assertThat("The file should exist", claFileDtos.getContent().size(), Matchers.greaterThan(0));
        ClaFileDto claFileDto = claFileDtos.getContent().get(0);
        logger.info("Looking at the properties of file: {}", claFileDto);

        ClaFileDetailsDto fileDetailsById = filesApplicationService.findFileDetailsById(claFileDto.getId());
        Optional<String> updatedAcl = fileDetailsById.getAclDataDto().getAclWriteData().stream().filter(f -> f.contains("NETWORK")).findFirst();
        if (!updatedAcl.isPresent()) {
            logger.error("Could not find NETWORK in updated file ACLs: {}",
                    fileDetailsById.getAclDataDto().getAclWriteData().stream().collect(Collectors.joining(",")));
            throw new RuntimeException("Couldn't find NETWORK write permission on updated ClaFile: "+claFileDto);
        }
        updatedAcl = fileDetailsById.getAclDataDto().getAclWriteData().stream().filter(f -> f.contains(removedPrincipal.getName())).findFirst();
        if (updatedAcl.isPresent()) {
            logger.error("Found unexpected {} in updated file ACLs: {}",
                    removedPrincipal.getName(),
                    fileDetailsById.getAclDataDto().getAclWriteData().stream().collect(Collectors.joining(",")));
            throw new RuntimeException("Found unexpected "+removedPrincipal.getName()+" write permission on updated ClaFile: " + claFileDto);
        }

    }

    private UserPrincipal changeAccess(final UserPrincipal user, Path file) {
        try {
            AclFileAttributeView view = Files.getFileAttributeView(file, AclFileAttributeView.class);
            AclEntry accessEntry = createAccessACLEntry(user);
            List<AclEntry> acl = view.getAcl();
            UserPrincipal removedPrincipal = acl.stream()
                    .filter(a -> (a.permissions().contains(AclEntryPermission.WRITE_DATA) && !a.principal().getName().toLowerCase().contains("administrator")))
                    .map(AclEntry::principal).findFirst().orElse(null);
            if (removedPrincipal != null) {
                // remove write permissions for first principal found
                acl = acl.stream().map(a ->
                        (AclEntry)((!a.type().equals(AclEntryType.ALLOW) || !a.principal().equals(removedPrincipal)) ? a :
                                AclEntry.newBuilder().setType(a.type()).setPrincipal(a.principal())
                                        .setPermissions(a.permissions().stream().filter(p->!(p.toString().toLowerCase()
                                                .contains("write"))).collect(Collectors.toSet()))
                                        .setFlags(a.flags())
                                        .build() ) )
                        .collect(Collectors.toList());
            }
            acl.add(0, accessEntry); // insert at head in case there are any DENY entries
            view.setAcl(acl);
            logger.debug("Changed permissions for file {}: added {}, removed {}", file.toString(), user.getName(), removedPrincipal.getName());
            return removedPrincipal;
        } catch (IOException e) {
            throw new IllegalStateException("Unable to grant access for " + file.toString() + " to " + user.getName(), e);
        }
    }

    private void rescanFolder(long rootFolderId) {
        logger.info("Rescan folder {}", rootFolderId);
        Map<String, String> params = new HashMap<>();
        params.put("op", StartScanProducer.SCAN_INGEST_ANALYZE);
        params.put("wait", "true");
        params.put("rootFolderIds",String.valueOf(rootFolderId));
        jobManagerApiController.startScanRootFolders(params);
        //Assert.assertEquals(RunStatus.FINISHED_SUCCESSFULLY,fileCrawlerApiController.getFileCrawlingState().getLastRunStatus());
        //TODO 2.0 instead of reading fiel crawling state, read the root folder last crawl run status

    }

    private AclEntry createAccessACLEntry(UserPrincipal user) {
        AclEntry entry = AclEntry
                .newBuilder()
                .setType(AclEntryType.ALLOW)
                .setPrincipal(user)
                .setPermissions(AclEntryPermission.DELETE_CHILD,
                        AclEntryPermission.WRITE_NAMED_ATTRS,
                        AclEntryPermission.EXECUTE,
                        AclEntryPermission.WRITE_DATA,
                        AclEntryPermission.WRITE_ATTRIBUTES,
                        AclEntryPermission.READ_ATTRIBUTES,
                        AclEntryPermission.APPEND_DATA,
                        AclEntryPermission.READ_DATA,
                        AclEntryPermission.READ_NAMED_ATTRS,
                        AclEntryPermission.READ_ACL,
                        AclEntryPermission.SYNCHRONIZE,
                        AclEntryPermission.DELETE)
                .setFlags(AclEntryFlag.FILE_INHERIT,
                        AclEntryFlag.DIRECTORY_INHERIT)
                .build();
        return entry;
    }


}
