package com.cla.test.filecluster.jobmanager.service;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerAppTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.test.filecluster.ClaBaseTests;
import com.cla.test.filecluster.it.categories.FastTestCategory;
import com.google.common.collect.Sets;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Category(FastTestCategory.class)
public class JobManagerServiceTests extends ClaBaseTests {

    private Logger logger = LoggerFactory.getLogger(JobManagerServiceTests.class);

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private JobManagerAppTaskService jobManagerAppTaskService;

    @Autowired
    private JobManagerService jobManagerService;

    @Test
    public void testCreateNewJob() {
        logger.info("testCreateNewJob");

        SimpleJobDto jobDto = jobManagerService.createRootFolderJob(1,10000, JobType.SCAN);

        IntStream.range(0,120).forEach(i-> jobManagerService.createRootFolderJob(i,i*100000, JobType.SCAN));
        List<SimpleJob> jobList = jobManagerService.getJobsByIds(Sets.newHashSet(jobDto.getId()));
        SimpleJob job = jobList.get(0);
        SimpleTask scanTask = jobManagerService.createTask(null, job.getId(), TaskType.SCAN_ROOT_FOLDER, null);
        job.setAcceptingTasks(false);

        Page<SimpleTask> tasks = jobManagerAppTaskService.getAllJobTasksPage(job, 0, 1000);
        Page<SimpleTask> newTasks = jobManagerAppTaskService.getJobTasksByStatePage(job.getId(), job.getTasksPartition(), Arrays.asList(TaskState.NEW), 0, 1000);
        Page<SimpleTask> doneTasks = jobManagerAppTaskService.getJobTasksByStatePage(job.getId(), job.getTasksPartition(), Arrays.asList(TaskState.DONE), 0, 1000);

        Assert.assertThat(job.getState(), Matchers.equalTo(JobState.NEW));
        Assert.assertThat(tasks.getContent().size(), Matchers.equalTo(1));
        Assert.assertThat(newTasks.getContent().size(), Matchers.equalTo(1));
        Assert.assertThat(doneTasks.getContent().size(), Matchers.equalTo(0));
        Assert.assertNotNull(job.getCreatedOnDB());
        Assert.assertNotNull(job.getStateUpdateTime());
        Assert.assertNotNull(tasks.iterator().next().getCreatedOnDB());
        Assert.assertNotNull(tasks.iterator().next().getStateUpdateTime());

        jobManagerService.updateTasksStatus(newTasks.getContent().stream().map(SimpleTask::getId).collect(Collectors.toList()), TaskState.DONE);
        Page<SimpleTask> doneTasks2 = jobManagerAppTaskService.getJobTasksByStatePage(job.getId(), job.getTasksPartition(), Arrays.asList(TaskState.DONE), 0, 1000);
        Assert.assertThat(doneTasks2.getContent().size(), Matchers.equalTo(1));
    }

    @Test
    public void testTaskPartitions(){
        List<Long> partitions = jobManagerService.getTaskTablePartitionIds();
        long currentPartitionId = jobManagerService.currentPartitionId();
        for (Long partition : partitions) {
            Map<JobState, Long> jobsPerState = jobManagerService.getTasksPartitionJobState(partition);
            long unDone = jobsPerState.entrySet().stream().filter(e->!JobState.DONE.equals(e.getKey())).mapToLong(e->e.getValue()).sum();
            logger.info("{} tasks in un-done jobs for partition {}{}. Jobs states task count: {}", unDone, partition, (partition.intValue() == currentPartitionId ? " (current)":""),
                    jobsPerState.entrySet().stream().map(e->"{"+e.getKey()+":"+e.getValue()+"}").collect(Collectors.joining(",")));
        }
    }

}
