package com.cla.test.filecluster.service.syscomponent;

import com.cla.common.domain.dto.components.ClaComponentDto;
import com.cla.common.domain.dto.components.ClaComponentStatusDto;
import com.cla.common.domain.dto.components.ClaComponentType;
import com.cla.common.domain.dto.components.ComponentState;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;
import com.cla.filecluster.domain.entity.monitor.ClaComponentStatus;
import com.cla.filecluster.repository.jpa.monitor.ClaComponentRepository;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.cla.test.filecluster.it.categories.UnitTestCategory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.booleanThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@Category(UnitTestCategory.class)
@RunWith(MockitoJUnitRunner.class)
public class SysComponentTests {

    @InjectMocks
    private SysComponentService sysComponentService;

    @Mock
    private ClaComponentRepository claComponentRepository;

    @Mock
    private CustomerDataCenterService customerDataCenterService;

    @Before
    public void init() {
        sysComponentService.init();
        ReflectionTestUtils.setField(sysComponentService, "freeSpaceThreshold", 15);
        ReflectionTestUtils.setField(sysComponentService, "folderMaxSizeThreshold", 700);
    }

    @After
    public void resetMocks() {
        Mockito.reset(claComponentRepository);
        Mockito.reset(customerDataCenterService);
    }

    @Test
    public void testConvertToDto() {
        ClaComponent c = new ClaComponent();
        c.setActive(true);
        c.setComponentType(ClaComponentType.MEDIA_PROCESSOR.name());
        c.setLocation("loc");
        c.setCustomerDataCenter(new CustomerDataCenter());
        c.getCustomerDataCenter().setId(2L);
        c.getCustomerDataCenter().setName("cd");
        c.setExternalNetAddress("ex");
        c.setInstanceId("iid");
        c.setLocalNetAddress("loc");
        c.setState(ComponentState.OK);
        c.setClaComponentType(ClaComponentType.MEDIA_PROCESSOR);
        c.setId(5L);
        c.setLastResponseDate(System.currentTimeMillis());
        c.setStateChangeDate(System.currentTimeMillis());
        c.setCreatedOnDB();

        ClaComponentDto dto = SysComponentService.convertClaComponent(c);
        assertEquals(new Long(c.getId()), new Long(dto.getId()));
        assertEquals(c.getClaComponentType(), dto.getClaComponentType());
        assertEquals(c.getCustomerDataCenter().getId(), dto.getCustomerDataCenterDto().getId());
        assertEquals(c.getState(), dto.getState());
        assertEquals(c.getInstanceId(), dto.getInstanceId());
        assertEquals(c.getComponentType(), dto.getComponentType());
        assertEquals(c.getLocation(), dto.getLocation());
        assertEquals(c.getLocalNetAddress(), dto.getLocalNetAddress());
        assertEquals(c.getLastResponseDate(), dto.getLastResponseDate());
        assertEquals(c.getStateChangeDate(), dto.getStateChangeDate());
        assertEquals(c.getCreatedOnDB(), dto.getCreatedOnDB());
        assertEquals(c.getExternalNetAddress(), dto.getExternalNetAddress());
        assertEquals(c.getActive(), dto.getActive());
    }

    @Test
    public void convertFromDtoAndBack() {

        when(claComponentRepository.save(any(ClaComponent.class))).thenAnswer(i -> {
            ClaComponent cc = (ClaComponent)i.getArguments()[0];
            cc.setId(88L);
            cc.setCreatedOnDB();
            return cc;
        });

        CustomerDataCenter ddc = new CustomerDataCenter();
        ddc.setName("dfgdfg");
        ddc.setId(2L);

        CustomerDataCenterDto dc = new CustomerDataCenterDto();
        dc.setId(2L);
        dc.setName("dfgdfg");

        when(customerDataCenterService.getCustomerDataCenterById(2L)).thenReturn(ddc);

        ClaComponentDto dto = new ClaComponentDto();
        dto.setActive(true);
        dto.setComponentType(ClaComponentType.MEDIA_PROCESSOR.name());
        dto.setLocation("loc");
        dto.setCustomerDataCenterDto(dc);
        dto.setExternalNetAddress("ex");
        dto.setInstanceId("iid");
        dto.setLocalNetAddress("loc");
        dto.setState(ComponentState.OK);
        dto.setClaComponentType(ClaComponentType.MEDIA_PROCESSOR);
        dto.setLastResponseDate(System.currentTimeMillis());
        dto.setStateChangeDate(System.currentTimeMillis());

        ClaComponentDto res = sysComponentService.createSysComponent(dto);
        assertEquals(new Long(res.getId()), new Long(88));
        assertEquals(res.getClaComponentType(), dto.getClaComponentType());
        assertEquals(res.getCustomerDataCenterDto().getId(), dto.getCustomerDataCenterDto().getId());
        assertEquals(res.getState(), dto.getState());
        assertEquals(res.getInstanceId(), dto.getInstanceId());
        assertEquals(res.getComponentType(), dto.getComponentType());
        assertEquals(res.getLocation(), dto.getLocation());
        assertEquals(res.getLocalNetAddress(), dto.getLocalNetAddress());
//        assertEquals(res.getLastResponseDate(), dto.getLastResponseDate());
        assertEquals(res.getStateChangeDate(), dto.getStateChangeDate());
        assertEquals(res.getExternalNetAddress(), dto.getExternalNetAddress());
        assertEquals(res.getActive(), dto.getActive());
    }

    @Test
    public void testConvertStatusToDto() {
        ClaComponentStatus stat = new ClaComponentStatus();
        stat.setId(5L);
        stat.setExtraMetrics("ext");
        stat.setClaComponentId(2L);
        stat.setComponentState(ComponentState.FAULTY);
        stat.setComponentType(ClaComponentType.BROKER.name());
        stat.setTimestamp(System.currentTimeMillis());

        ClaComponentStatusDto dto = SysComponentService.convertClaComponentStatus(stat);
        assertEquals(new Long(stat.getId()), dto.getId());
        assertEquals(stat.getComponentType(), dto.getComponentType());
        assertEquals(stat.getClaComponentId(), dto.getClaComponentId());
        assertEquals(stat.getComponentState(), dto.getComponentState());
        assertEquals(stat.getExtraMetrics(), dto.getExtraMetrics());
        assertEquals(new Long(stat.getTimestamp()), dto.getTimestamp());
    }

    @Test
    public void convertStatusFromDto() {
        ClaComponentStatusDto dto = new ClaComponentStatusDto();
        dto.setId(5L);
        dto.setExtraMetrics("ext");
        dto.setClaComponentId(2L);
        dto.setComponentState(ComponentState.FAULTY);
        dto.setComponentType(ClaComponentType.BROKER.name());
        dto.setTimestamp(System.currentTimeMillis());

        ClaComponentStatus stat = new ClaComponentStatus();
        sysComponentService.updateComponentStatusFromDto(dto, stat);

        assertEquals(stat.getComponentType(), dto.getComponentType());
        assertEquals(stat.getClaComponentId(), dto.getClaComponentId());
        assertEquals(stat.getComponentState(), dto.getComponentState());
        assertEquals(stat.getExtraMetrics(), dto.getExtraMetrics());
        assertEquals(new Long(stat.getTimestamp()), dto.getTimestamp());
    }

    @Test
    public void testCalculateOversizedFolders() {
        Map<String, Integer> measuredFolders = new HashMap<>();
        measuredFolders.put("small", 70);
        measuredFolders.put("medium", 200);
        measuredFolders.put("large", 600);
        measuredFolders.put("xlarge", 800);
        Map<String, Integer> res = sysComponentService.calculateOversizedFolders(measuredFolders);
        assertEquals(1, res.size());
        int size = res.values().iterator().next();
        assertEquals(800, size);
        String dd = res.keySet().iterator().next();
        assertEquals("xlarge", dd);
    }

    @Test
    public void testCalculateLowSpaceDrives() {
        List<DriveDto.DriveCapacityMetrics> driveCapacityMetricsList = new ArrayList<>();
        DriveDto.DriveCapacityMetrics m = new DriveDto.DriveCapacityMetrics();
        m.setDriveId("C");
        m.setFreeSpace(5);
        m.setTotalSpace(100);
        driveCapacityMetricsList.add(m);

        m = new DriveDto.DriveCapacityMetrics();
        m.setDriveId("D");
        m.setFreeSpace(20);
        m.setTotalSpace(100);
        driveCapacityMetricsList.add(m);

        m = new DriveDto.DriveCapacityMetrics();
        m.setDriveId("E");
        m.setFreeSpace(40);
        m.setTotalSpace(100);
        driveCapacityMetricsList.add(m);

        Map<DriveDto.DriveCapacityMetrics, Integer> res = sysComponentService.calculateLowSpaceDrives(driveCapacityMetricsList);
        assertEquals(1, res.size());
        int free = res.values().iterator().next();
        assertEquals(5, free);
        DriveDto.DriveCapacityMetrics dd = res.keySet().iterator().next();
        assertEquals("C", dd.getDriveId());
    }

    @Test
    public void testIsComponentOutOfMemoryOneFaultyRecord() {
        List<Object[]> records = new ArrayList<>();
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":5}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 5L));

        when(claComponentRepository.findDriveDataForTypeAndTime(anyInt(), anyLong())).thenReturn(records);
        boolean res = sysComponentService.isComponentOutOfMemory(ClaComponentType.ZOOKEEPER);
        assertTrue(res);
    }

    @Test
    public void testIsComponentOutOfMemoryOneOkRecord() {
        List<Object[]> records = new ArrayList<>();
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":50}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 5L));

        when(claComponentRepository.findDriveDataForTypeAndTime(anyInt(), anyLong())).thenReturn(records);
        boolean res = sysComponentService.isComponentOutOfMemory(ClaComponentType.ZOOKEEPER);
        assertFalse(res);
    }

    @Test
    public void testIsComponentOutOfMemoryNewOkRecord() {
        List<Object[]> records = new ArrayList<>();
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":50}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 50L));
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":5}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 5L));

        when(claComponentRepository.findDriveDataForTypeAndTime(anyInt(), anyLong())).thenReturn(records);
        boolean res = sysComponentService.isComponentOutOfMemory(ClaComponentType.ZOOKEEPER);
        assertFalse(res);
    }

    @Test
    public void testIsComponentOutOfMemoryNewFaultyRecord() {
        List<Object[]> records = new ArrayList<>();
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":50}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 50L));
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":5}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 500L));

        when(claComponentRepository.findDriveDataForTypeAndTime(anyInt(), anyLong())).thenReturn(records);
        boolean res = sysComponentService.isComponentOutOfMemory(ClaComponentType.ZOOKEEPER);
        assertTrue(res);
    }

    @Test
    public void testIsComponentOutOfMemoryOneFaultyOneOkRecords() {
        List<Object[]> records = new ArrayList<>();

        records.add(createRecord(3L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":50}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 50L));
        records.add(createRecord(3L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":5}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 5L));

        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":50}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 50L));
        records.add(createRecord(5L, "{\"folder_sizes\":{}, \n" +
                "\"drive_capacities\":[\n" +
                "{\"drive_id\": \"C\", \"total_space\":100, \"free_space\":5}\n" +
                "], \"machine_id\":\"172.31.22.199\"}", 500L));

        when(claComponentRepository.findDriveDataForTypeAndTime(anyInt(), anyLong())).thenReturn(records);
        boolean res = sysComponentService.isComponentOutOfMemory(ClaComponentType.ZOOKEEPER);
        assertTrue(res);
    }

    private Object[] createRecord(Long id, String dd, Long timestamp) {
        Object[] record = new Object[3];
        record[0] = BigInteger.valueOf(id);
        record[1] = dd;
        record[2] = BigInteger.valueOf(timestamp);
        return record;
    }
}


