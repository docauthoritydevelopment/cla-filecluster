package com.cla.test.filecluster.service.syscomponent;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.request.CoreStatus;
import org.apache.solr.client.solrj.request.SolrPing;
import org.apache.solr.client.solrj.response.CoreAdminResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by oren on 1/23/2018.
 */
public class VerifyDependenciesActive {


    public static void verifySolrActive() throws SolrServerException, IOException {

        SolrClient solrClient = new HttpSolrClient.Builder().withBaseSolrUrl("http://localhost:8983/solr").build();
        CoreAdminRequest.getCoreStatus("PVS",   solrClient);

    }
}
