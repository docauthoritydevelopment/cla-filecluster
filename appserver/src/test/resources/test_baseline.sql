--create index entity_idx on acl_items (entity(255));

--create index path_idx on root_folder (path(255));
--create index path_idx on doc_folder (path(255));
--create index real_path_idx on doc_folder (real_path(255));
--create index del_path_idx on doc_folder (deleted,path(250));
--create index del_real_path_idx on doc_folder (deleted,real_path(250));
--CREATE INDEX `deleted_pFolder_pInfo_idx` ON `doc_folder` (deleted,parent_folder_id,parents_info(240));
--CREATE INDEX `parentsInfo_idx` ON `doc_folder` (parents_info(250));


-- Add foreign key constrains that are not part of the objects definition
--ALTER TABLE `cla_files_md` ADD CONSTRAINT FK_cla_files_md_package_top_file_id FOREIGN KEY (package_top_file_id) REFERENCES cla_files_md(id);
--ALTER TABLE `doc_folder` ADD CONSTRAINT FK_doc_folder_package_top_file_id FOREIGN KEY (package_top_file_id) REFERENCES cla_files_md(id);

-- Set SimpleTask table to be partitioned based on part_id column (JobManager.SimpleTask)
alter table jm_tasks modify column `id` bigint(20) NOT NULL, drop primary key;
alter table jm_tasks add primary key (`id`,`part_id`);
alter table jm_tasks PARTITION  by list(`part_id`) ( partition p0 values in (0));

