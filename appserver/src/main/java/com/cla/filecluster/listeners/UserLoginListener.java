package com.cla.filecluster.listeners;

import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Created by oren on 10/26/2017.
 */
@Component
public class UserLoginListener implements ApplicationListener<AbstractAuthenticationEvent> {

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        if (event instanceof AbstractAuthenticationFailureEvent) {
            String remoteIP = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getHeader("X-FORWARDED-FOR");
//                event instanceof AuthenticationFailureBadCredentialsEvent ||
//                event instanceof AuthenticationFailureCredentialsExpiredEvent ||
//                event instanceof AuthenticationFailureDisabledEvent ||
//                event instanceof AuthenticationFailureExpiredEvent ||
//                event instanceof AuthenticationFailureLockedEvent ||
            Object principal = event.getAuthentication().getPrincipal();
            String principalName = event.getAuthentication().getName();
            Object authenticationDetails = event.getAuthentication().getDetails();
            eventAuditService.audit(AuditType.LOGIN, "Failed login attempt for username " + principalName, AuditAction.FAILED,
                    String.class, principalName,
                    event.getClass().toString().substring("class org.springframework.security.authentication.event.".length()), remoteIP);
            userService.markLoginFailure(principalName, principal, authenticationDetails);
        } else if (event instanceof InteractiveAuthenticationSuccessEvent) {
            String remoteIP = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getHeader("X-FORWARDED-FOR");
            String msg = String.format("User %s has logged in from %s.", userService.getPrincipalUsername(), remoteIP);
            eventAuditService.audit(AuditType.LOGIN, msg, AuditAction.SUCCEEDED, User.class, userService.getCurrentUser().getId(),
                    userService.getCurrentUser(), remoteIP);
            userService.markLoginSuccess();
        }
    }
}
