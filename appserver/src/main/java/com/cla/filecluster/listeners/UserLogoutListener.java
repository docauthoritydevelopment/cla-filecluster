package com.cla.filecluster.listeners;

import com.cla.common.domain.dto.security.UserType;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.web.session.HttpSessionDestroyedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by oren on 10/29/2017.
 */
@Component
public class UserLogoutListener implements ApplicationListener<HttpSessionDestroyedEvent>{
    private static Logger logger = LoggerFactory.getLogger(UserLogoutListener.class);

    @Autowired
    private EventAuditService eventAuditService;

    @Override
    public void onApplicationEvent(HttpSessionDestroyedEvent event) {
        if (UserService.getCurrentUserDetails().isPresent()) {
            eventAuditService.audit(AuditType.LOGOUT, "User logged out", AuditAction.SUCCEEDED, User.class, UserService.getCurrentUserDetails().get().getUser().getId());
            logger.info("Auto logout user {}", UserService.getCurrentUserDetails().get().getUser().getId());
        } else {
            if (event.getSecurityContexts() != null && !event.getSecurityContexts().isEmpty()) {
                ClaUserDetails user = (ClaUserDetails) event.getSecurityContexts().get(0).getAuthentication().getPrincipal();
                if (!user.getUser().getUserType().equals(UserType.SYSTEM)) {
                    eventAuditService.audit(AuditType.LOGOUT, "Session timed out", AuditAction.AUTO, User.class, user.getUser().getId(), user.getUsername());
                    logger.info("Auto logout user {} {}", user.getUser().getId(), user.getUsername());
                }
            } else {
                logger.trace("Auto logout unknown user");
            }
        }
    }
}
