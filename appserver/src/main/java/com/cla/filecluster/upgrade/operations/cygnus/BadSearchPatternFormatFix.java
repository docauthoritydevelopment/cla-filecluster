package com.cla.filecluster.upgrade.operations.cygnus;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.DeepPageRequest;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 4/11/2019
 */
@Upgrade(targetVersion = 6, step = 6)
public class BadSearchPatternFormatFix extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-bad-search-pattern-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-bad-search-pattern-files.page-size:10000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 6 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        opData.put(START_LIMIT, String.valueOf(from));

        try {

            // get folders attachment files
            Query q = Query.create();
            q.addField(CatFileFieldType.EXTRACTED_PATTERNS_IDS);
            q.addField(CatFileFieldType.ID);
            q.addSort(CatFileFieldType.ID, Sort.Direction.ASC);
            q.setRows(pageSize);
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.EXTRACTED_PATTERNS_IDS, SolrOperator.EQ, "com.cla.filecluster.domain.entity.pv.SearchPatternCount*")));
            q.setStart(0);
            QueryResponse queryResponse = solrFileCatRepository.runQuery(q.build());
            SolrDocumentList results = queryResponse.getResults();
            from += results.size();
            List<SolrInputDocument> docsToSave = new ArrayList<>();
            results.forEach(doc -> {
                SolrInputDocument document = new SolrInputDocument();
                document.setField(CatFileFieldType.ID.getSolrName(), doc.getFieldValue(CatFileFieldType.ID.getSolrName()));

                List<String> patterns = (List<String>)doc.getFieldValue(CatFileFieldType.EXTRACTED_PATTERNS_IDS.getSolrName());
                List<String> newPatterns = new ArrayList<>();

                patterns.forEach(pattern -> {
                    if (pattern.startsWith("com.cla.filecluster.domain.entity.pv.SearchPatternCount")) {
                        newPatterns.add(calculateNewPattern(pattern));
                    } else {
                        newPatterns.add(pattern.substring(1, pattern.length()-1));
                    }
                });

                String value = "[" + newPatterns.stream().collect(Collectors.joining(",")) + "]";

                Map<String, Object> fieldModifier = new HashMap<>(1);
                fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
                document.setField(CatFileFieldType.EXTRACTED_PATTERNS_IDS.getSolrName(), fieldModifier);

                docsToSave.add(document);
            });

            if (docsToSave.size() > 0) {
                solrFileCatRepository.saveAll(docsToSave);
                solrFileCatRepository.softCommitNoWaitFlush();
            }

            if (results.size() == 0) {
                logger.warn("Got 0 results - stopping");
                isJobFinished = true;
                logger.info("Files migration done for {} search patterns in files", (from));
            } else {
                logger.info("Ended current migration cycle for {} search patterns", (results.size()));
            }

            opData.put(START_LIMIT, String.valueOf(from));
        } catch (Exception e) {
            logger.info("search patterns migration issue", e);
            throw e;
        }
    }

    private String calculateNewPattern(String oldPattern) {
        String patternId = getData("patternId", oldPattern, false);
        String patternName = getData("patternName", oldPattern, true);
        String patternCount = getData("patternCount", oldPattern, false);

        if (patternId == null || patternName == null || patternCount == null) {
            logger.warn("found bad pattern, cant fix so remove {}"+oldPattern);
            return null;
        }

        return "{\"patternId\":"+patternId+",\"patternName\":\""+patternName+"\",\"patternCount\":"+patternCount+"}";
    }

    private String getData(String label, String data, boolean isString) {
        int start = data.indexOf(label);
        if (start > 0) {
            start += label.length() + 1;
            int end = data.indexOf(",", start);
            if (end < 0) {
                end = data.indexOf("}", start);
            }
            if (isString) {
                String res = data.substring(start, end);
                return res.substring(1, res.length()-1);
            }
            return data.substring(start, end);
        }
        return null;
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
