package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Sets;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by: yael
 * Created on: 5/28/2018
 */
@Upgrade(targetVersion = 5, step = 4)
public class DocFolderAclMigrationUpgradeOperation extends UpgradeOperation {
    private static String START_LIMIT = "START_LIMIT";


    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    protected SolrCatFolderRepository solrCatFolderRepository;

    @Value("${migration.folder-acl.page-size:10000}")
    private int pageSize;

    @Value("${migration.logging.folder-acl.limit:100}")   // 0 - no log; -1 - log all
    private int loggingItemsLimit;

    private boolean isJobFinished = false;

    private int loggingItemsCounter = 0;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));
        Integer to = from + pageSize;

        logger.info("starting migration from {}", from);

        EntityManager em = emf.createEntityManager();
        try {
            List<Object[]> results = em.createNativeQuery("show tables like 'df_acl_items'").getResultList();
            if (results == null || results.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table df_acl_items doesnt exist");
                return;
            }

            results = em.createNativeQuery("SELECT " +
                    "id" +             // 00
                    ",doc_folder_id" +   // 01
                    ",entity" +
                    ",`level`" +
                    ",`type` " +        // 04
                    "FROM df_acl_items a " +
                    "WHERE id > " + from +
                    " limit " + pageSize).getResultList();

            int rows = 0;
            long recordId = 0;
            Set<String> uniquenessTestSet = Sets.newHashSet();

            int duplicateAclsCount = 0;
            List<SolrInputDocument> foldersToUpdate = new LinkedList<>();
            for (Object[] folderData : results) {
                BigInteger recordIdBigInt = (BigInteger)folderData[0];
                recordId = recordIdBigInt.longValue();

                String uniqueKey = createUniqueKey(folderData);
                if (!uniquenessTestSet.contains(uniqueKey)) {
                    uniquenessTestSet.add(uniqueKey);
                    SolrInputDocument document = createDocument(folderData);
                    if (document != null) {
                        foldersToUpdate.add(document);

                        if (logger.isTraceEnabled() && (loggingItemsLimit < 0 || loggingItemsCounter < loggingItemsLimit)) {
                            logger.trace("Upgrade CatFolder ACL: \n    row={}\n    doc={}",
                                    IntStream.range(0, folderData.length)
                                            .mapToObj(i -> (i + ":" + (folderData[i] == null ? "null" : folderData[i].toString())))
                                            .collect(Collectors.joining(",")),
                                    document.toString());
                            loggingItemsCounter++;
                        }
                    } else {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Upgrade: got null document for row: {}",
                                    IntStream.range(0, folderData.length)
                                            .mapToObj(i -> (i + ":" + (folderData[i] == null ? "null" : folderData[i].toString())))
                                            .collect(Collectors.joining(",")));
                        }
                    }
                }
                else{
                    duplicateAclsCount++;
                }
                rows++;
            }

            if (!foldersToUpdate.isEmpty()) {
                solrCatFolderRepository.createFoldersFromDocuments(foldersToUpdate);
                solrCatFolderRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(recordId));

            if (duplicateAclsCount > 0){
                logger.warn("{} duplicate ACLs were found and ignored.", duplicateAclsCount);
            }

            if (rows < pageSize) {
                logger.info("acl folder migration done for {} records", to);
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", to);
            }
        } catch (Exception e) {
            logger.info("acl folder migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private String createUniqueKey(Object[] folderData){
        BigInteger folderId = (BigInteger)folderData[1];
        String entity = (String)folderData[2];
        AclType type = AclType.valueOf((Integer)folderData[4]);
        return folderId + "_" + type + "_" + entity;
    }

    private SolrInputDocument createDocument(Object[] fileData) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger folderId = (BigInteger)fileData[1];
        solrInputDocument.setField(CatFoldersFieldType.ID.getSolrName(), folderId.longValue());

        AclType type = AclType.valueOf((Integer)fileData[4]);
        switch (type) {
            case READ_TYPE:
                addField(solrInputDocument, CatFoldersFieldType.ACL_READ, fileData[2]);
                break;
            case WRITE_TYPE:
                addField(solrInputDocument, CatFoldersFieldType.ACL_WRITE, fileData[2]);
                break;
            case DENY_READ_TYPE:
                addField(solrInputDocument, CatFoldersFieldType.ACL_DENIED_READ, fileData[2]);
                break;
            case DENY_WRITE_TYPE:
                addField(solrInputDocument, CatFoldersFieldType.ACL_DENIED_WRITE, fileData[2]);
                break;
        }

        return solrInputDocument;
    }

    private void addField(SolrInputDocument solrInputDocument, CatFoldersFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.ADD.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
