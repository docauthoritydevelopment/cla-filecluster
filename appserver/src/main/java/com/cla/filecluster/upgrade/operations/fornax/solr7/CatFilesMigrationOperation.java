package com.cla.filecluster.upgrade.operations.fornax.solr7;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.utils.EmailUtils;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.cla.filecluster.service.security.DomainAccessHandler;
import com.cla.filecluster.upgrade.MigrateCollectionDataUpgradeOperation;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Responsible to migrate all cat files to solr 7 (handled not stored fields)
 */
@Upgrade(targetVersion = 9, step = 3)
public class CatFilesMigrationOperation extends MigrateCollectionDataUpgradeOperation {

    private static final String SOURCE_COLLECTION_NAME = "CatFiles";

    @Value("${solr7.migration.cat-files.enabled:true}")
    private boolean enabled;

    @Value("${solr.catFile.expiredUserKey:ExpiredUser}")
    private String expiredUserKey;

    @Value("${solr7.migration.email.parse-x500-addresses:true}")
    private boolean upgradeX500Addresses;

    @Value("${solr7.migration.email.x500.extract-non-domain-addresses:true}")
    private boolean extractNonDomainX500CN;

    @Autowired
    private DomainAccessHandler domainAccessHandler;

    private String expiredUserKeyLower = null;

    @Override
    protected void beforeIterationStart() {
        super.beforeIterationStart();
        if (expiredUserKeyLower == null) {
            expiredUserKeyLower = expiredUserKey.toLowerCase();
        }
    }

    @Override
    // Handle non 'stored' fields: search_acl_read, search_acl_denied_read
    protected SolrInputDocument documentConverter(SolrDocument doc) {
        SolrInputDocument transformedDocument = super.documentConverter(doc);

        Collection<String> acls = (Collection<String>)doc.getFieldValue(CatFileFieldType.ACL_READ.getSolrName());
        if (acls != null) {
            Set<String> afterTransform = new HashSet<>();
            for (String acl : acls) {
                if (!acl.equals(expiredUserKey)) {
                    acl = domainAccessHandler.getAclInEmailFormat(acl);
                }
                afterTransform.add(acl);
            }
            Collection<String> aclsLower = afterTransform.stream().map(String::toLowerCase).filter(f->!f.equals(expiredUserKeyLower)).collect(Collectors.toList());
            transformedDocument.setField(CatFileFieldType.ACL_READ.getSolrName(), afterTransform);
            transformedDocument.setField(CatFileFieldType.SEARCH_ACL_READ.getSolrName(), aclsLower);     // replace value set by super.documentConverter()
        }

        acls = (Collection<String>)doc.getFieldValue(CatFileFieldType.ACL_DENIED_READ.getSolrName());
        if (acls != null) {
            Set<String> afterTransform = new HashSet<>();
            for (String acl : acls) {
                if (!acl.equals(expiredUserKey)) {
                    acl = domainAccessHandler.getAclInEmailFormat(acl);
                }
                afterTransform.add(acl);
            }
            Collection<String> aclsLower = afterTransform.stream().map(String::toLowerCase).filter(f->!f.equals(expiredUserKeyLower)).collect(Collectors.toList());
            transformedDocument.setField(CatFileFieldType.ACL_DENIED_READ.getSolrName(), afterTransform);
            transformedDocument.setField(CatFileFieldType.SEARCH_ACL_DENIED_READ.getSolrName(), aclsLower);     // replace value set by super.documentConverter()
        }

        acls = (Collection<String>)doc.getFieldValue(CatFileFieldType.ACL_WRITE.getSolrName());
        if (acls != null) {
            Set<String> afterTransform = new HashSet<>();
            for (String acl : acls) {
                if (!acl.equals(expiredUserKey)) {
                    acl = domainAccessHandler.getAclInEmailFormat(acl);
                }
                afterTransform.add(acl);
            }
            transformedDocument.setField(CatFileFieldType.ACL_WRITE.getSolrName(), afterTransform);
        }

        acls = (Collection<String>)doc.getFieldValue(CatFileFieldType.ACL_DENIED_WRITE.getSolrName());
        if (acls != null) {
            Set<String> afterTransform = new HashSet<>();
            for (String acl : acls) {
                if (!acl.equals(expiredUserKey)) {
                    acl = domainAccessHandler.getAclInEmailFormat(acl);
                }
                afterTransform.add(acl);
            }
            transformedDocument.setField(CatFileFieldType.ACL_DENIED_WRITE.getSolrName(), afterTransform);
        }

        Collection<String> scopedTags = (Collection<String>)doc.getFieldValue(CatFileFieldType.SCOPED_TAGS.getSolrName());
        if (scopedTags != null && !scopedTags.isEmpty()) {
            Set<String> afterTransform = new HashSet<>(scopedTags);
            transformedDocument.setField(CatFileFieldType.SCOPED_TAGS.getSolrName(), afterTransform);
        }

        Collection<String> activeAssoc = (Collection<String>)doc.getFieldValue(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName());
        if (activeAssoc != null && !activeAssoc.isEmpty()) {
            Set<String> afterTransform = new HashSet<>(activeAssoc);
            transformedDocument.setField(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName(), afterTransform);
        }

        String fullName = (String)doc.getFieldValue(CatFileFieldType.FULL_NAME.getSolrName());
        transformedDocument.setField(CatFileFieldType.FULL_NAME.getSolrName(), fullName);
        transformedDocument.setField(CatFileFieldType.FULL_NAME_SEARCH.getSolrName(), fullName.toLowerCase());

        transformedDocument.setField(CatFileFieldType.FOLDER_NAME.getSolrName(), FileNamingUtils.getParentNameOnly(fullName.toLowerCase()));

        if (upgradeX500Addresses) {
            String senderAddr = (String) doc.getFieldValue(CatFileFieldType.SENDER_ADDRESS.getSolrName());
            if (senderAddr != null) {
                transformedDocument.setField(CatFileFieldType.SENDER_ADDRESS.getSolrName(),
                        EmailUtils.getEmailAddressValue(senderAddr, upgradeX500Addresses, extractNonDomainX500CN));
                transformedDocument.setField(CatFileFieldType.SENDER_DOMAIN.getSolrName(),
                        EmailUtils.extractX500DomainAddress(senderAddr, extractNonDomainX500CN));
            }
            List<String> srcRecipientsAddr = (List<String>) doc.getFieldValue(CatFileFieldType.RECIPIENTS_ADDRESSES.getSolrName());
            if (srcRecipientsAddr != null && !srcRecipientsAddr.isEmpty()) {
                Set<String> recipientsAddresses = new HashSet<>(srcRecipientsAddr.size());
                Set<String> recipientsDomains = new HashSet<>(srcRecipientsAddr.size());
                srcRecipientsAddr.forEach(recipient -> {
                    recipientsAddresses.add(EmailUtils.getEmailAddressValue(recipient, upgradeX500Addresses, extractNonDomainX500CN));
                    recipientsDomains.add(EmailUtils.extractX500DomainAddress(recipient, extractNonDomainX500CN));
                });
                transformedDocument.setField(CatFileFieldType.RECIPIENTS_ADDRESSES.getSolrName(), Lists.newArrayList(recipientsAddresses));
                transformedDocument.setField(CatFileFieldType.RECIPIENTS_DOMAINS.getSolrName(), Lists.newArrayList(recipientsDomains));
            }
        }

        return transformedDocument;
    }

    private static void copySender(PstEntryPropertiesDto pstProps, SolrFileEntity file, boolean parseX500Addresses, boolean extractNonDomainX500CN) {
        EmailAddress sender = pstProps.getSender();
        if (sender != null) {
            file.setSenderName(EmailUtils.getMailName(sender.getName()));
            file.setSenderAddress(EmailUtils.getEmailAddressValue(sender.getAddress(), parseX500Addresses, extractNonDomainX500CN));
            file.setSenderDomain(EmailUtils.extractDomainFromEmailAddress(sender.getAddress(), parseX500Addresses, extractNonDomainX500CN));
            file.setSenderFullAddress(ModelIngestUtils.getEmailJson(sender));
        }
    }

    private static void copyRecipients(PstEntryPropertiesDto pstProps, SolrFileEntity file, boolean parseX500Addresses, boolean extractNonDomainX500CN) {
        List<EmailAddress> recipients = pstProps.getRecipients();
        if (recipients != null && !recipients.isEmpty()) {
            Set<String> recipientsNames = new HashSet<>(recipients.size());
            Set<String> recipientsAddresses = new HashSet<>(recipients.size());
            Set<String> recipientsDomains = new HashSet<>(recipients.size());
            List<String> recipientsFullAddresses = new ArrayList<>(recipients.size());
            recipients.forEach(recipient -> {
                String name = EmailUtils.getRecipientName(recipient);
                if (name != null) {
                    recipientsNames.add(name);
                }
                recipientsAddresses.add(EmailUtils.getEmailAddressValue(recipient.getAddress(), parseX500Addresses, extractNonDomainX500CN));
                recipientsDomains.add(EmailUtils.extractDomainFromEmailAddress(recipient.getAddress(), parseX500Addresses, extractNonDomainX500CN));
                recipientsFullAddresses.add(ModelIngestUtils.getEmailJson(recipient));
            });
            file.setRecipientsNames(new ArrayList<>(recipientsNames));
            file.setRecipientsAddresses(new ArrayList<>(recipientsAddresses));
            file.setRecipientsDomains(new ArrayList<>(recipientsDomains));
            file.setRecipientsFullAddresses(recipientsFullAddresses);
        }
    }

    // ---------------- Keep all common definitions below this line ---
    private static final String DESTINATION_COLLECTION_NAME = SOURCE_COLLECTION_NAME + "_2";
    private static final String COLLECTION_ALIAS = SOURCE_COLLECTION_NAME;

    @Value("${solr7.migration.collections.page-size:5000}")
    private int pageSize;

    @Value("${solr7.migration.collections.commit-after:true}")
    private boolean commitWhenDone;

    @Override
    protected boolean commitWhenDone() {
        return commitWhenDone;
    }

    @Override
    protected int getPageSize() {
        return pageSize;
    }

    @Override
    protected boolean isEnabled() {
        return enabled;
    }

    @Override
    protected String getSourceCollectionName() {
        return SOURCE_COLLECTION_NAME;
    }

    @Override
    protected String getDestinationCollectionName() {
        return DESTINATION_COLLECTION_NAME;
    }

    @Override
    protected String getCollectionliasName() {
        return COLLECTION_ALIAS;
    }
}
