package com.cla.filecluster.upgrade.operations.castor.contentids;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by: Nadav
 */
@Upgrade(targetVersion = 5, step = 10)
public class FixEmptyContentIdsUpgradeOperation extends UpgradeOperation {

    private static final Logger logger = LoggerFactory.getLogger(FixEmptyContentIdsUpgradeOperation.class);

    private static final String FIX_EMPTY_CONTENT_IDS_PAGE_INDEX_KEY = "fix-empty-content-ids.page-index";
    private static final String FIX_EMPTY_CONTENT_IDS_UPGRADE_START_INDICATION = "0";
    private static final String FIX_EMPTY_CONTENT_IDS_CURRENT_PROGRESS = "fix-empty-content-ids.current-progress";
    private static final String FIX_EMPTY_CONTENT_IDS_TOTAL_ELEMENTS = "fix-empty-content-ids.total-elements";


    @Value("${upgrade-operation.fix-empty-content-ids.page-size:5000}")
    private int pageSize;

    @Value("${upgrade-operation.fix-empty-content-ids.enabled:true}")
    private boolean enabled;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private ContentMetadataService contentMetadataService;

    private SolrQuery missingContentIdsQuery;

    private boolean isJobFinished = false;

    @Override
    protected void beforeIterationStart() {
        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade operation is not enabled, skipped");
            return;
        }
        // Count query
        Query query = Query.create()
                .setRows(0)
                .addFilterWithCriteria(CatFileFieldType.CONTENT_ID, SolrOperator.MISSING, null)
                .addFilterWithCriteria(CatFileFieldType.DELETED, SolrOperator.EQ, false);

        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        long resultsSize = queryResponse.getResults().getNumFound();
        if (resultsSize == 0) {
            logger.info("No empty ingested content ids to fix, skipping upgrade");
            isJobFinished = true;
        } else {
            int numOfPages = resultsSize < pageSize ? 1 : (int) Math.ceil((double) resultsSize / pageSize);
            logger.info("About to fix empty content ids for {} files by pages of {}: total of {} pages",
                    resultsSize, pageSize, numOfPages);
        }
        metadata.getOpData().put(FIX_EMPTY_CONTENT_IDS_TOTAL_ELEMENTS, String.valueOf(resultsSize));
        missingContentIdsQuery = query.setRows(pageSize).build();
    }

    @Override
    public void upgrade(TransactionStatus status) {
        Map<String, String> opData = metadata.getOpData();
        Integer pageIndex = Integer.valueOf(opData.
                getOrDefault(FIX_EMPTY_CONTENT_IDS_PAGE_INDEX_KEY, FIX_EMPTY_CONTENT_IDS_UPGRADE_START_INDICATION));
        Integer currentProgress = Integer.valueOf(opData.
                getOrDefault(FIX_EMPTY_CONTENT_IDS_CURRENT_PROGRESS, FIX_EMPTY_CONTENT_IDS_UPGRADE_START_INDICATION));


        // Get files with ingested state, deleted = false and content id is missing
        List<SolrFileEntity> entities = solrFileCatRepository.find(missingContentIdsQuery);
        if (!entities.isEmpty()) {
            try {
                logger.info("Page: {}, Generating {} new files with new content ids", pageIndex, entities.size());
                List<String> deleteByIds = entities.stream().map(SolrFileEntity::getId).collect(Collectors.toList());

                // First save files with new content metadata id
                entities.forEach(this::fixEntityContentId);
                solrFileCatRepository.saveAllEntities(entities);
                solrFileCatRepository.commit();

                // Second delete old entities
                logger.info("Page: {}, Deleting {} old with files with empty content ids", pageIndex, deleteByIds.size());
                solrFileCatRepository.deleteByIds(deleteByIds);
                solrFileCatRepository.commit();

            } catch (Throwable t) {
                logger.error(String.format("Failed to run page %d", pageIndex), t);
            } finally {
                // Update progress
                currentProgress += entities.size();
                opData.put(FIX_EMPTY_CONTENT_IDS_CURRENT_PROGRESS, String.valueOf(currentProgress));
                opData.put(FIX_EMPTY_CONTENT_IDS_PAGE_INDEX_KEY, String.valueOf(pageIndex + 1));
            }

        } else {
            logger.info("Done! no more content ids to fix");
        }

        isJobFinished = entities.isEmpty();
    }

    private void fixEntityContentId(SolrFileEntity fileEntity) {
        // Set new id
        Long nextAvailableId = contentMetadataService.getNextAvailableId();
        fileEntity.setId(FileCatCompositeId.of(nextAvailableId, fileEntity.getFileId()).toString());
        fileEntity.setContentId(String.valueOf(nextAvailableId));

        // Change state back to scan if needed
        ClaFileState claFileState = ClaFileState.valueOf(fileEntity.getState());
        switch (claFileState) {
            case INGESTED:
            case ANALYSED: // Not reasonable but to be on the safe size
                fileEntity.setState(ClaFileState.SCANNED.name());
        }
        if (claFileState == ClaFileState.INGESTED) {
            fileEntity.setState(ClaFileState.SCANNED.name());
        }
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}