package com.cla.filecluster.upgrade.operations.electra;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 6/19/2019
 */
@Upgrade(targetVersion = 8, step = 1)
public class IngestTaskMigrationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.migrate-ingest-task.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-ingest-task.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    private static String SQL_QUERY_PREFIX = "select " +
            "t.`retries`," +  // 0
            "t.`run_context`," + // 1
            "t.`state`," + // 2
            "t.`state_update_time`," + // 3
            "t.`item_id`," + // 4
            "cast(t.json_data as char)," + // 5
            "t.`name`," + // 6
            "t.id " + // 7
            " from jm_tasks t, jm_jobs j, crawl_runs c " +
            " where c.run_status in ('RUNNING', 'PAUSED') and c.run_type = 'ROOT_FOLDER' and j.run_context = c.id " +
            " and j.type = 'INGEST' and j.state != 'DONE' and t.job_id = j.id " +
            " and t.state < 3 and t.item_id > 0 and t.type = 31"
            ;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 1 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        long from = Long.parseLong(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        try {
            Query query = em.createNativeQuery(SQL_QUERY_PREFIX +
                    " and t.id > " + from +
                    " order by t.id limit " + pageSize);
            List<Object[]> results = query.getResultList();
            int rows = results.isEmpty() ? 0 : handleResults(results);
            long currMax = results.isEmpty() ?  from + pageSize : ((BigInteger)results.get(results.size()-1)[7]).longValue();

            opData.put(START_LIMIT, String.valueOf(currMax));

            if (results.isEmpty()) {
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (rows));
            }
        } catch (Exception e) {
            logger.error("ingest task migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results) {
        int rows = 0;

        List<Long> fileIds = new ArrayList<>();
        for (Object[] taskData : results) {
            if (taskData[4] != null) {
                BigInteger fileId = (BigInteger) taskData[4];
                fileIds.add(fileId.longValue());
            }
        }

        if (fileIds.isEmpty()) {
            logger.debug("No fileIds to process");
            return 0;
        }

        com.cla.filecluster.repository.solr.query.Query query =
                com.cla.filecluster.repository.solr.query.Query.create()
                        .addField(CatFileFieldType.ID)
                        .addField(CatFileFieldType.FILE_ID)
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.IN, fileIds))
                        .setRows(fileIds.size())
                ;
        List<SolrFileEntity> files = solrFileCatRepository.find(query.build());
        Map<Long, String> fileIdsToEntityId = files.stream()
                .collect(Collectors.toMap(SolrFileEntity::getFileId, SolrFileEntity::getId));

        List<SolrInputDocument> filesDataToUpdate = new LinkedList<>();
        for (Object[] taskData : results) {
            SolrInputDocument document = createDocument(taskData, fileIdsToEntityId);
            if (document != null) {
                filesDataToUpdate.add(document);
            }
            rows++;
        }

        if (!filesDataToUpdate.isEmpty()) {
            solrFileCatRepository.saveAll(filesDataToUpdate);
            solrFileCatRepository.softCommitNoWaitFlush();
        }
        return rows;
    }

    private SolrInputDocument createDocument(Object[] taskData, Map<Long, String> fileIdsToEntityId) {
        try {
            SolrInputDocument doc = new SolrInputDocument();
            BigInteger fileId = (BigInteger) taskData[4];
            doc.setField(CatFileFieldType.ID.getSolrName(), fileIdsToEntityId.get(fileId.longValue()));

            int retries = taskData[0] == null ? 0 : (Integer) taskData[0];
            BigInteger runContext = (BigInteger) taskData[1];
            int state = (int) taskData[2];
            long cngTime = taskData[3] == null ? System.currentTimeMillis() : ((BigInteger) taskData[3]).longValue();
            String params = taskData[5] == null ? null : (String) taskData[5];

            doc.setField(CatFileFieldType.CURRENT_RUN_ID.getSolrName(), SolrRepositoryUtils.setValueMap(runContext.longValue()));
            doc.setField(CatFileFieldType.TASK_STATE.getSolrName(), SolrRepositoryUtils.setValueMap(TaskState.of(state).name()));
            doc.setField(CatFileFieldType.TASK_STATE_DATE.getSolrName(), SolrRepositoryUtils.setValueMap(cngTime));
            doc.setField(CatFileFieldType.RETRIES.getSolrName(), SolrRepositoryUtils.setValueMap(retries));
            if (params != null) {
                doc.setField(CatFileFieldType.TASK_PARAM.getSolrName(), SolrRepositoryUtils.setValueMap(params));
            }
            return doc;
        } catch (Exception e) {
            logger.error("problem with task for file {} an run {}", taskData[4], taskData[1], e);
            return null;
        }
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
