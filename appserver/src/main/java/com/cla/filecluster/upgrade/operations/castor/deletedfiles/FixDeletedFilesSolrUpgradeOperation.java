package com.cla.filecluster.upgrade.operations.castor.contentids.deletedfiles;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by: yael
 * Created on: 10/7/2018
 */
@Upgrade(targetVersion = 5, step = 5)
public class FixDeletedFilesSolrUpgradeOperation extends UpgradeOperation {
    @Autowired
    protected SolrFileCatRepository solrFileCatRepository;
    private boolean isJobFinished = false;
    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        List<String> filters = new ArrayList<>();
        filters.add("-deleted:*");
        solrFileCatRepository.updateFieldByQuery(CatFileFieldType.DELETED.getSolrName(), "false", SolrFieldOp.SET, SolrQueryGenerator.ALL_QUERY, filters, SolrCommitType.SOFT_NOWAIT);
        isJobFinished = true;
    }
    @Override
    public boolean isIterative(){
        return false;
    }
    @Override
    public boolean isJobFinished(){
        return isJobFinished;
    }
    @Override
    public boolean operatesOnSolr(){
        return true;
    }
}