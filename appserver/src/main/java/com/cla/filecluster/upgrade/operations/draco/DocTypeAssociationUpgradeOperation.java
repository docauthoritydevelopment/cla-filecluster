package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.*;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.operation.ApplyFolderAssociationOnFiles;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.FOLDER_IDS;
import static com.cla.common.constants.CatFileFieldType.USER_GROUP_ID;

@SuppressWarnings("FieldCanBeLocal")
@Upgrade(targetVersion = 7, step = 4)
public class DocTypeAssociationUpgradeOperation extends UpgradeOperation {
    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private AssociationsService associationsService;

    @Value("${associations.apply-folder-associations-retry:5}")
    private int retryForApplyFolderAssociationToFiles;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.migrate-association-doctype.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-association-doctype.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    private static String SQL_QUERY_PREFIX = "SELECT " +
            "a.id, " + // 00
            "a.association_time_stamp, " + // 01
            "a.deleted, " + // 02
            "a.files_should_be_dirty, " + // 03
            "a.implicit, " + // 04
            "a.association_source, " + // 05
            "a.doc_type_id, " + // 06
            "a.dirty, " + // 07
            "a.origin_folder_id, " + // 08
            "a.doc_type_association_scope_id, " + // 09
            "a.folder_id, " + // 10
            "a.cla_file_id, " + // 11
            "a.origin_depth_from_root, " + // 12
            "a.group_id " + // 13
            "from doc_type_association a "
            ;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 4 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        try {
            List<Object[]> tableExistsResult = em.createNativeQuery("show tables like 'doc_type_association'").getResultList();
            if (tableExistsResult == null || tableExistsResult.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table doc_type_association doesnt exist");
                return;
            }

            Query query = em.createNativeQuery("select max(id) from doc_type_association");
            Object res = query.getSingleResult();
            BigInteger maxId = (BigInteger)res;

            query = em.createNativeQuery(SQL_QUERY_PREFIX +
                    "where a.id >= " + from + " and a.id < " + (from + pageSize));
            List<Object[]> results = query.getResultList();
            int rows = results.isEmpty() ? 0 : handleResults(results);

            opData.put(START_LIMIT, String.valueOf(from + pageSize));

            if (maxId == null || (from + pageSize) > maxId.longValue()) {
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (rows));
            }
        } catch (Exception e) {
            logger.info("doc type association migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results) {
        int rows = 0;
        List<SolrInputDocument> filesDataToUpdate = new LinkedList<>();
        List<SolrInputDocument> foldersDataToUpdate = new LinkedList<>();
        logger.trace("Upgrade: page with {} results. First res. has {} columns",
                results.size(), results.isEmpty() ? 0 : results.get(0).length);

        List<Long> fileIds = new ArrayList<>();
        List<Long> folderIds = new ArrayList<>();
        for (Object[] docTypeAssociationData : results) {
            if (docTypeAssociationData[11] != null) {
                BigInteger fileId = (BigInteger) docTypeAssociationData[11];
                fileIds.add(fileId.longValue());
            } else if (docTypeAssociationData[10] != null) { // folder
                BigInteger folderId = (BigInteger)docTypeAssociationData[10];
                folderIds.add(folderId.longValue());
            }
        }

        com.cla.filecluster.repository.solr.query.Query query;
        Map<Long, SolrFileEntity> filesById = null;
        if (fileIds.size() > 0) {
            query = com.cla.filecluster.repository.solr.query.Query.create()
                            .addField(CatFileFieldType.ID)
                            .addField(CatFileFieldType.FILE_ID)
                            .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.IN, fileIds))
                            .setRows(fileIds.size());
            List<SolrFileEntity> files = fileIds.isEmpty() ? new ArrayList<>() : fileCatService.find(query.build());
            filesById = files.stream()
                    .collect(Collectors.toMap(SolrFileEntity::getFileId, r -> r));
        }

        Map<Long, SolrFolderEntity> foldersById = null;
        if (folderIds.size() > 0) {
            query = com.cla.filecluster.repository.solr.query.Query.create()
                    .addField(CatFoldersFieldType.ID)
                    .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID, SolrOperator.IN, folderIds))
                    .setRows(folderIds.size());

            List<SolrFolderEntity> folders = solrCatFolderRepository.find(query.build());
            foldersById = folders.stream()
                    .collect(Collectors.toMap(SolrFolderEntity::getId, r -> r));
        }

        for (Object[] docTypeAssociationData : results) {
            if (docTypeAssociationData[11] != null && filesById != null) { // file
                SolrInputDocument document = createFileDocument(docTypeAssociationData, filesById);
                if (document != null) {
                    filesDataToUpdate.add(document);
                }
            } else if (docTypeAssociationData[10] != null && foldersById != null) { // folder
                SolrInputDocument document = createFolderDocument(docTypeAssociationData, foldersById);
                if (document != null) {
                    foldersDataToUpdate.add(document);
                }
            } else if (docTypeAssociationData[13] != null) { // group
                handleGroupAssociationIfNeeded(docTypeAssociationData);
            }
            rows++;
        }

        if (!filesDataToUpdate.isEmpty()) {
            fileCatService.saveAll(filesDataToUpdate);
            fileCatService.softCommitNoWaitFlush();
        }

        if (!foldersDataToUpdate.isEmpty()) {
            solrCatFolderRepository.saveAll(foldersDataToUpdate);
            solrCatFolderRepository.softCommitNoWaitFlush();
        }
        return rows;
    }

    private SolrInputDocument createFileDocument(Object[] fileAssociationData, Map<Long, SolrFileEntity> filesById) {

        // deleted association
        if (fileAssociationData[2] != null && ((boolean)fileAssociationData[2])) {
            return null;
        }

        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger fileId = (BigInteger)fileAssociationData[11];
        SolrFileEntity fileEntity = filesById.get(fileId.longValue());

        if (fileEntity == null) {
            logger.debug("found doc type association with no file for id {}", fileId);
            return null;
        }

        solrInputDocument.setField(CatFoldersFieldType.ID.getSolrName(), fileEntity.getId());

        AssociationEntity entity = new AssociationEntity();
        entity.setCreationDate(((BigInteger)fileAssociationData[1]).longValue());

        if (fileAssociationData[6] != null) {
            entity.setDocTypeId(((BigInteger)fileAssociationData[6]).longValue());
        }

        if (fileAssociationData[9] != null) {
            entity.setAssociationScopeId(((BigInteger)fileAssociationData[9]).longValue());
        }

        String json = AssociationsService.convertFromAssociationEntity(entity);
        addField(solrInputDocument, CatFileFieldType.FILE_ASSOCIATIONS, json, SolrFieldOp.ADD);

        DocType docType = docTypeService.getDocTypeById(entity.getDocTypeId());
        if (docType.getFileTagSettings() != null && !docType.getFileTagSettings().isEmpty()) {
            for (FileTagSetting fts : docType.getFileTagSettings()) {
                if (!fts.getFileTag().getType().isSingleValueTag()) {
                    String identifier = AssociationIdUtils.createScopedTagIdentifier(fts.getFileTag(), null);
                    addField(solrInputDocument, CatFileFieldType.SCOPED_TAGS, identifier, SolrFieldOp.REMOVE);
                    identifier = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fts.getFileTag(), null, TagPriority.FILE);
                    addField(solrInputDocument, CatFileFieldType.SCOPED_TAGS, identifier, SolrFieldOp.ADD);
                }
            }
        }

        return solrInputDocument;
    }

    private SolrInputDocument createFolderDocument(Object[] folderAssociationData,  Map<Long, SolrFolderEntity> foldersById) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger folderId = (BigInteger)folderAssociationData[10];

        if (!foldersById.containsKey(folderId.longValue())) {
            logger.debug("found doc type association with no folder for id {}", folderId);
            return null;
        }

        solrInputDocument.setField(CatFoldersFieldType.ID.getSolrName(), folderId.longValue());

        AssociationEntity entity = new AssociationEntity();
        entity.setCreationDate(((BigInteger)folderAssociationData[1]).longValue());
        entity.setImplicit((boolean)folderAssociationData[4]);

        if (folderAssociationData[6] != null) {
            entity.setDocTypeId(((BigInteger)folderAssociationData[6]).longValue());
        }

        if (folderAssociationData[9] != null) {
            entity.setAssociationScopeId(((BigInteger)folderAssociationData[9]).longValue());
        }

        if (folderAssociationData[8] != null) {
            entity.setOriginFolderId(((BigInteger)folderAssociationData[8]).longValue());
        }

        if (folderAssociationData[12] != null) {
            entity.setOriginDepthFromRoot((Integer)folderAssociationData[12]);
        }

        String json = AssociationsService.convertFromAssociationEntity(entity);

        boolean isDirty = (((boolean)folderAssociationData[3]) || ((boolean)folderAssociationData[7]));

        // deleted association
        if (folderAssociationData[2] != null && ((boolean)folderAssociationData[2])) {
            if (!entity.getImplicit() && isDirty) {
                createOperation(folderId.longValue(), json, SolrFieldOp.REMOVE);
            } else {
                return null;
            }
        } else { // undeleted association
            addField(solrInputDocument, CatFoldersFieldType.FOLDER_ASSOCIATIONS, json, SolrFieldOp.ADD);

            // association dirty
            if (isDirty) {
                createOperation(folderId.longValue(), json, SolrFieldOp.ADD);
            }
        }

        if (!entity.getImplicit()) {
            DocType docType = docTypeService.getDocTypeById(entity.getDocTypeId());
            if (docType.getFileTagSettings() != null && !docType.getFileTagSettings().isEmpty()) {
                for (FileTagSetting fts : docType.getFileTagSettings()) {
                    if (!fts.getFileTag().getType().isSingleValueTag()) {
                        String solrKey = AssociationIdUtils.createScopedTagIdentifier(fts.getFileTag(), null);
                        String folderParentsInfo = entity.getOriginDepthFromRoot() + "." + entity.getOriginFolderId();
                        updateFolderAssociationByQuery(solrKey, folderParentsInfo, CatFileFieldType.SCOPED_TAGS, false);
                        if (!(boolean)folderAssociationData[2]) { // not deleted
                            solrKey = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fts.getFileTag(), null, TagPriority.folder(entity.getOriginDepthFromRoot()));
                            updateFolderAssociationByQuery(solrKey, folderParentsInfo, CatFileFieldType.SCOPED_TAGS, true);
                        }
                    }
                }
            }
        }

        return solrInputDocument;
    }

    private void handleGroupAssociationIfNeeded(Object[] groupAssociationData) {
        if (groupAssociationData[6] != null) {
            String groupId = (String)groupAssociationData[13];
            boolean deleted = (boolean)groupAssociationData[2];
            Long docTypeId = ((BigInteger)groupAssociationData[6]).longValue();
            DocType docType = docTypeService.getDocTypeById(docTypeId);
            if (docType.getFileTagSettings() != null && !docType.getFileTagSettings().isEmpty()) {
                for (FileTagSetting fts : docType.getFileTagSettings()) {
                    if (!fts.getFileTag().getType().isSingleValueTag()) {
                        String solrKey = AssociationIdUtils.createScopedTagIdentifier(fts.getFileTag(), null);
                        updateGroupAssociationByQuery(solrKey, groupId, CatFileFieldType.SCOPED_TAGS, false);
                        if (!deleted) {
                            solrKey = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fts.getFileTag(), null, TagPriority.FILE_GROUP);
                            updateGroupAssociationByQuery(solrKey, groupId, CatFileFieldType.SCOPED_TAGS, true);
                        }
                    }
                }
            }
        }
    }

    private void updateFolderAssociationByQuery(String solrKey, String folderParentsInfo, CatFileFieldType field, boolean addTag) {
        try {
            Criteria filterQuery = Criteria.create(FOLDER_IDS, SolrOperator.EQ, folderParentsInfo);
            associationsService.updateAssociationByQuery(filterQuery, addTag, solrKey, solrKey, field, false, SolrCommitType.SOFT_NOWAIT);
        } catch (Exception e) {
            logger.error("fail to remove solr key {} from {} for folder {}", solrKey, field, folderParentsInfo, e);
        }
    }

    private void updateGroupAssociationByQuery(String solrKey, String groupId, CatFileFieldType field, boolean addTag) {
        try {
            Criteria filterQuery = Criteria.create(USER_GROUP_ID, SolrOperator.EQ, groupId);
            associationsService.updateAssociationByQuery(filterQuery, addTag, solrKey, solrKey, field, false, SolrCommitType.SOFT_NOWAIT);
        } catch (Exception e) {
            logger.error("fail to {} solr key {} from tags {} for group {}", addTag ? "add" : "remove", solrKey, field, groupId, e);
        }
    }

    private void addField(SolrInputDocument solrInputDocument, SolrCoreSchema type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    private void createOperation(Long docFolderId, String association, SolrFieldOp operation) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_FOLDER_ASSOCIATIONS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyFolderAssociationOnFiles.DOC_FOLDER_ID_PARAM, String.valueOf(docFolderId));
        data.getOpData().put(ApplyFolderAssociationOnFiles.ASSOCIATION_PARAM, association);
        data.getOpData().put(ApplyFolderAssociationOnFiles.ACTION_PARAM, operation.name());
        data.setUserOperation(true);
        data.setRetryLeft(retryForApplyFolderAssociationToFiles);
        scheduledOperationService.createNewOperation(data);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
