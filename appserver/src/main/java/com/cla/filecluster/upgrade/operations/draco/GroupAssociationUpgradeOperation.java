package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import static com.cla.common.constants.CatFileFieldType.USER_GROUP_ID;

@Upgrade(targetVersion = 7, step = 5)
public class GroupAssociationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private AssociationsService associationsService;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.migrate-association-groups.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-association-groups.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    private static String SQL_QUERY_PREFIX = "SELECT " +
            "a.id, " + // 00
            "a.association_time_stamp, " + // 01
            "a.deleted, " + // 02
            "a.files_should_be_dirty, " + // 03
            "a.solr_dirty, " + // 04
            "a.doc_type_id, " + // 05
            "a.file_tag_id, " + // 06
            "a.functional_role_id, " + // 07
            "a.tag_association_scope_id, " + // 08
            "a.simple_biz_list_item_sid, " + // 09
            "a.file_group_id " + // 10
            "from business_id_tag_association a ";

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 5 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        try {
            List<Object[]> tableExistsResult = em.createNativeQuery("show tables like 'business_id_tag_association'").getResultList();
            if (tableExistsResult == null || tableExistsResult.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table business_id_tag_association doesnt exist");
                return;
            }

            Query query = em.createNativeQuery("select max(id) from business_id_tag_association");
            Object res = query.getSingleResult();
            BigInteger maxId = (BigInteger)res;

            query = em.createNativeQuery(SQL_QUERY_PREFIX +
                    "where a.id >= " + from + " and a.id < " + (from + pageSize));
            List<Object[]> results = query.getResultList();
            int rows = results.isEmpty() ? 0 : handleResults(results);

            opData.put(START_LIMIT, String.valueOf(from + pageSize));

            if (maxId == null || (from + pageSize) > maxId.longValue()) {
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (rows));
            }
        } catch (Exception e) {
            logger.info("group association migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results) {
        int rows = 0;
        logger.trace("Upgrade: page with {} results. First res. has {} columns",
                results.size(), results.isEmpty() ? 0 : results.get(0).length);

        for (Object[] groupAssociationData : results) {
            handleAssociation(groupAssociationData);
            rows++;
        }
        return rows;
    }

    private void handleAssociation(Object[] groupAssociationData) {
        String groupId = (String)groupAssociationData[10];
        boolean deleted = (boolean)groupAssociationData[2];

        if (groupAssociationData[6] != null) { // tag
            FileTagDto tag = fileTagService.getFromCache(((BigInteger)groupAssociationData[6]).longValue());
            if (!tag.getType().isSingleValueTag()) {
                // recreate identifier for files
                String solrKey = AssociationIdUtils.createScopedTagIdentifier(tag, null);
                updateAssociationByQuery(solrKey, groupId, CatFileFieldType.SCOPED_TAGS, false);
                if (!deleted) {
                    solrKey = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, null, TagPriority.FILE_GROUP);
                    updateAssociationByQuery(solrKey, groupId, CatFileFieldType.SCOPED_TAGS, true);
                }
            }
        } else if (!deleted && groupAssociationData[7] != null) { // data role
            FunctionalRoleDto fr = functionalRoleService.getFromCache(((BigInteger)groupAssociationData[7]).longValue());
            String solrKey = AssociationIdUtils.createFunctionalRoleIdentfier(fr, TagPriority.FILE_GROUP);
            updateAssociationByQuery(solrKey, groupId, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, true);
        } else if (!deleted && groupAssociationData[9] != null) { // biz list
            SimpleBizListItemDto bl = bizListItemService.getFromCache((String)groupAssociationData[9]);
            String solrKey = AssociationIdUtils.createSimpleBizListItemIdentfier(bl, TagPriority.FILE_GROUP);
            updateAssociationByQuery(solrKey, groupId, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, true);
        }

    }

    private void updateAssociationByQuery(String solrKey, String groupId, CatFileFieldType field, boolean addTag) {
        try {
            Criteria filterQuery = Criteria.create(USER_GROUP_ID, SolrOperator.EQ, groupId);
            associationsService.updateAssociationByQuery(filterQuery, addTag, solrKey, solrKey, field, false, SolrCommitType.SOFT_NOWAIT);
        } catch (Exception e) {
            logger.error("fail to {} solr key {} from tags {} for group {}", addTag ? "add" : "remove", solrKey, field, groupId, e);
        }
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
