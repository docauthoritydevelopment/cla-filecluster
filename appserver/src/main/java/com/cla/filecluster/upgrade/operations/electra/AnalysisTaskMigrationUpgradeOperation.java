package com.cla.filecluster.upgrade.operations.electra;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 6/19/2019
 */
@Upgrade(targetVersion = 8, step = 2)
public class AnalysisTaskMigrationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrContentMetadataRepository solrContentMetadataRepository;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.migrate-analysis-task.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-analysis-task.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    private static String SQL_QUERY_PREFIX = "select " +
            "t.`retries`," +  // 0
            "t.`run_context`," + // 1
            "t.`state`," + // 2
            "t.`state_update_time`," + // 3
            "t.`item_id`," + // 4
            "t.`json_data`," + // 5
            "t.`name`," + // 6
            "t.id " + // 7
            " from jm_tasks t, jm_jobs j, crawl_runs c " +
            " where c.run_status in ('RUNNING', 'PAUSED') and c.run_type = 'ROOT_FOLDER' and j.run_context = c.id " +
            " and j.type = 'ANALYZE' and j.state != 'DONE' and t.job_id = j.id " +
            " and t.state < 3 and t.item_id > 0 and t.type = 51"
            ;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 2 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        long from = Long.parseLong(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        try {
            Query query = em.createNativeQuery(SQL_QUERY_PREFIX +
                    " and t.id > " + from +
                    " order by t.id limit " + pageSize);
            List<Object[]> results = query.getResultList();
            int rows = results.isEmpty() ? 0 : handleResults(results);
            long currMax = results.isEmpty() ?  from + pageSize : ((BigInteger)results.get(results.size()-1)[7]).longValue();

            opData.put(START_LIMIT, String.valueOf(currMax));

            if (results.isEmpty()) {
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (rows));
            }
        } catch (Exception e) {
            logger.error("analysis task migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results) {
        int rows = 0;

        List<SolrInputDocument> filesDataToUpdate = new LinkedList<>();
        for (Object[] taskData : results) {
            SolrInputDocument document = createDocument(taskData);
            if (document != null) {
                filesDataToUpdate.add(document);
            }
            rows++;
        }

        if (!filesDataToUpdate.isEmpty()) {
            solrContentMetadataRepository.saveAll(filesDataToUpdate);
            solrContentMetadataRepository.softCommitNoWaitFlush();
        }
        return rows;
    }

    private SolrInputDocument createDocument(Object[] taskData) {
        try {
            SolrInputDocument doc = new SolrInputDocument();
            BigInteger contentId = (BigInteger)taskData[4];
            doc.setField(ContentMetadataFieldType.ID.getSolrName(), String.valueOf(contentId.longValue()));

            int retries = taskData[0] == null ? 0 : (Integer)taskData[0];
            BigInteger runContext = (BigInteger)taskData[1];
            int state = (int)taskData[2];
            long cngTime = taskData[3] == null ? System.currentTimeMillis() : ((BigInteger)taskData[3]).longValue();

            doc.setField(ContentMetadataFieldType.CURRENT_RUN_ID.getSolrName(), SolrRepositoryUtils.setValueMap(runContext.longValue()));
            doc.setField(ContentMetadataFieldType.TASK_STATE.getSolrName(), SolrRepositoryUtils.setValueMap(TaskState.of(state).name()));
            doc.setField(ContentMetadataFieldType.TASK_STATE_DATE.getSolrName(), SolrRepositoryUtils.setValueMap(cngTime));
            doc.setField(ContentMetadataFieldType.RETRIES.getSolrName(), SolrRepositoryUtils.setValueMap(retries));
            return doc;
        } catch (Exception e) {
            logger.error("problem with task for content {} an run {}", taskData[4], taskData[1], e);
            return null;
        }
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
