package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 6/6/2019
 */
@Upgrade(targetVersion = 7, step = 9)
public class EmptyRFUpgradeOperation extends UpgradeOperation {
    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Value("${migration.empty-rf-fix.enabled:true}")
    private boolean enabled;

    @Value("${migration.empty-rf-fix.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 9 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        try {
            // get files with no rf
            Query q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.MISSING, null)));
            q.addField(CatFileFieldType.ID);
            q.addField(CatFileFieldType.FOLDER_ID);
            q.setStart(from);
            q.setRows(pageSize);
            q.addSort(CatFileFieldType.ID, Sort.Direction.ASC);
            List<SolrFileEntity> entities = solrFileCatRepository.find(q.build());

            List<Long> folderIds = entities.stream().map(SolrFileEntity::getFolderId).filter(Objects::nonNull).collect(Collectors.toList());
            if (folderIds != null && !folderIds.isEmpty()) {

                List<SolrFolderEntity> folders = solrCatFolderRepository.find(folderIds);
                Map<Long, Long> folderRfs = folders.stream().filter(Objects::nonNull).collect(Collectors.toMap(SolrFolderEntity::getId, SolrFolderEntity::getRootFolderId));

                List<SolrInputDocument> filesToUpdate = new LinkedList<>();
                entities.forEach(file -> {
                    if (file.getFolderId() == null) {
                        logger.warn("found file {} without folder id", file.getId());
                    } else {
                        SolrInputDocument document = new SolrInputDocument();
                        document.setField(CatFileFieldType.ID.getSolrName(), file.getId());
                        if (folderRfs.containsKey(file.getFolderId())) {
                            addField(document, CatFileFieldType.ROOT_FOLDER_ID, folderRfs.get(file.getFolderId()));
                            filesToUpdate.add(document);
                        } else {
                            logger.warn("found folder {} without root folder id", file.getFolderId());
                        }
                    }
                });

                if (!filesToUpdate.isEmpty()) {
                    solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                    solrFileCatRepository.softCommitNoWaitFlush();
                }
            } else {
                logger.warn("found files with no root folder and no folder in solr !!!");
            }

            opData.put(START_LIMIT, String.valueOf(from+pageSize));

            if (entities.size() < pageSize) {
                logger.info("Files migration done for {} files missing rf num", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} files  missing rf num", (from + pageSize));
            }

        } catch (Exception e) {
            logger.info(" missing rf files migration issue", e);
            throw e;
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
