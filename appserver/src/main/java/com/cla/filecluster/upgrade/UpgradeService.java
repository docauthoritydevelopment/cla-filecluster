package com.cla.filecluster.upgrade;

import ch.qos.logback.classic.Level;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.upgrade.UpgradeOperationState;
import com.cla.filecluster.domain.entity.upgrade.UpgradeOperationStep;
import com.cla.filecluster.repository.jpa.upgrade.UpgradeOperationsRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.scheduling.SchedulingManager;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Ints;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.jetbrains.annotations.NotNull;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Upgrade service.
 * Loads all the UpgradeOperations using reflection, runs all the relevant operations
 * (which have version/step greater than the current upgrade version/step).
 */
@Service
public class UpgradeService implements ApplicationContextAware {

    public static final Logger logger = LoggerFactory.getLogger(UpgradeService.class);

    private static final String NAME_PATTERN = "%s (%d.%d)";
    private static final String SOLR_LOGGER_LEVEL_KEY = "solrLoggerLevel";

    @Value("${upgrade.active:true}")
    private Boolean performUpgrade;

    @Value("${is.toolExecutionMode:false}")
    private boolean isToolExecutionMode;

    @Value("${upgrade.pingSolrRetry:60}")
    private int pingSolrRetryCount;

    // Current software version and step, should be updated only for first installation
    // and never for upgraded environments. For example, if version 2.1 is installed
    // from scratch, the application properties should include the following properties
    // with respective values of 2 and 1.
    // If the environment is upgraded from 2.1 to 3.4, the application properties should
    // still include the following properties with respective values of 2 and 1.
    //--------------------------------------------------------------
    @Value("${upgrade.baseline-version:0}")
    private int baselineVersion;

    @Value("${upgrade.baseline-step:0}")
    private int baselineStep;
    //--------------------------------------------------------------

    @Autowired
    private PlatformTransactionManager txManager;

    @Autowired
    private SystemSettingsService settingsService;

    @Autowired
    private UpgradeOperationsRepository upgradeRepo;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private Environment environment;

    @Autowired
    private SchedulingManager schedulingManager;

    private ApplicationContext applicationContext;

    private TimeSource timeSource = new TimeSource();

    private boolean solrUp = false;

    private boolean isTest = false;

    @EventListener
    @AutoAuthenticate
    public void handleContextRefresh(ContextRefreshedEvent event) {
        try {
            if (isToolExecutionMode) {
                logger.debug("ToolExecutionMode: upgrade service will not be activated.");
            } else {
                if (!performUpgrade) {
                    logger.info("Upgrade procedure was disabled.");
                    return;
                }

                logger.info("Starting upgrade procedure.");
                Map<String, Object> upgradeContext = new HashMap<>();
                executePreUpgradeOperations(upgradeContext);

                isTest = Arrays.asList(environment.getActiveProfiles()).contains("test");

                AutowireCapableBeanFactory autowireFactory = applicationContext.getAutowireCapableBeanFactory();
                TransactionTemplate tmpl = new TransactionTemplate(txManager);

                for (UpgradeOperationMetadata metadata : prepareUpgradeMetadatas()) {
                    executeOperation(metadata, autowireFactory, tmpl);
                }

                // permanent upgrade operations
                for (UpgradeOperationMetadata metadata : preparePermanentMetadatas()) {
                    executeOperation(metadata, autowireFactory, tmpl);
                }

                UpgradeService.logger.debug("Upgrade procedure finished.");

                executePostUpgradeOperations(upgradeContext);
            }
        } finally {
            // when upgrade is done - reactivate all spring scheduled methods
            schedulingManager.setSchedulingActive(true);
        }
    }

    private void executePreUpgradeOperations(Map<String, Object> upgradeContext) {
        logger.info("Setting {} logger to ERROR", SolrFileCatRepository.class.getSimpleName());
        ch.qos.logback.classic.Logger solrLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.cla.filecluster.repository.solr");
        upgradeContext.put(SOLR_LOGGER_LEVEL_KEY, solrLogger.getLevel());
        solrLogger.setLevel(Level.ERROR);
    }

    private void executePostUpgradeOperations(Map<String, Object> upgradeContext) {
        ch.qos.logback.classic.Logger solrLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger("com.cla.filecluster.repository.solr");
        Level originalLevel = (Level) upgradeContext.get(SOLR_LOGGER_LEVEL_KEY);
        logger.info("Ressting {} to original level {}", SolrFileCatRepository.class.getSimpleName(), originalLevel);
        solrLogger.setLevel(originalLevel);
    }

    private void executeOperation(UpgradeOperationMetadata metadata,
                                  AutowireCapableBeanFactory autowireFactory, TransactionTemplate tmpl) {
        try {
            String upgradeOperationName = String.format(NAME_PATTERN,
                    metadata.getOperationClass().getSimpleName(), metadata.getVersion(), metadata.getStep());

            Upgrade annotation = metadata.getOperationClass().getAnnotation(Upgrade.class);
            if (isTest) {
                if (!annotation.shouldRunOnTest()) {
                    logger.debug("upgrade operation {} will not run in test mode", upgradeOperationName);
                    return;
                }
            }

            logger.debug("Instantiating new {} (type:{})", upgradeOperationName, metadata.getOperationClass().getName());
            UpgradeOperation upgradeOperation;
            if (!annotation.withArgs()) {
                upgradeOperation = metadata.getOperationClass().newInstance();
                logger.debug("Autowiring {}", upgradeOperationName);
                autowireFactory.autowireBean(upgradeOperation);
            } else {
                logger.debug("Autowiring by constructor {}", upgradeOperationName);
                upgradeOperation = (UpgradeOperation) autowireFactory.autowire(metadata.getOperationClass(),
                        AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, true);
            }

            logger.debug("Setting context to {}", upgradeOperationName);
            upgradeOperation.setApplicationContext(applicationContext);

            logger.debug("Setting metadata to {}", upgradeOperationName);
            upgradeOperation.setMetadata(metadata);

            logger.info("START: Executing {}", upgradeOperationName);
            if (upgradeOperation.operatesOnSolr()) {
                waitForSolrIfNeeded();
            }

            if (upgradeOperation.isIterative()) {
                upgradeOperation.beforeIterationStart();
                int index = 1;
                do {
                    logger.debug("========= Iterative operation: iteration {}", index);
                    if (!upgradeOperation.isJobFinished()) {
                        runInTransaction(tmpl, upgradeOperation, metadata);
                    }
                    index++;
                } while (!upgradeOperation.isJobFinished());
            } else {
                runInTransaction(tmpl, upgradeOperation, metadata);
            }
            logger.info("DONE: Executing {}", upgradeOperationName);
        } catch (IllegalAccessException | InstantiationException e) {
            logger.error("Failed to execute upgrade operation.", e);
        }
    }

    private void waitForSolrIfNeeded() {
        if (solrUp) {
            return;
        }

        int pingCount = 0;
        logger.info("Verifying solr is up...");
        while (pingCount < pingSolrRetryCount) {
            logger.info("Ping request: try # {}", pingCount + 1);
            SolrPingResponse solrPingResponse = solrFileCatRepository.pingSolr();
            if (solrPingResponse != null) {
                logger.info("Solr is up!!!, proceeding with upgrade");
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // nothing to do
            }
            pingCount++;
        }

        if (pingCount == pingSolrRetryCount) {
            throw new RuntimeException("Solr is not up, cannot execute upgrade operations!!");
        }

        solrUp = true;
    }


    /**
     * Run upgrade operation in transaction
     *
     * @param tmpl             TX template
     * @param upgradeOperation upgrade operation object
     * @param metadata         upgrade operation metadata
     */
    private void runInTransaction(TransactionTemplate tmpl, UpgradeOperation upgradeOperation,
                                  UpgradeOperationMetadata metadata) {

        tmpl.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(@NotNull TransactionStatus status) {
                try {
                    upgradeOperation.upgrade(status);
                    if (metadata.isUpdateVersion()) {
                        if (upgradeOperation.isIterative()) {
                            updateUpgradeOperation(metadata,
                                    upgradeOperation.isJobFinished() ? UpgradeOperationState.DONE : UpgradeOperationState.IN_PROGRESS);
                        } else {
                            updateUpgradeOperation(metadata, UpgradeOperationState.DONE);
                        }
                    }
                } catch (Exception e) {
                    status.setRollbackOnly();
                    updateUpgradeOperation(metadata, UpgradeOperationState.FAILED);
                    logger.error("Upgrade failed at version {} step {}", metadata.getVersion(), metadata.getStep(), e);
                }
            }
        });
    }

    /**
     * Find all the classes that extend UpgradeOperation.
     * Extract upgrade operation metadatas, return sorted list of upgrade operations with version/step
     * greater than current one (or equal, if the latest operation wasn't finished)
     *
     * @return list of upgrade operation metadatas, sorted by version/step.
     */
    private List<UpgradeOperationMetadata> prepareUpgradeMetadatas() {
        Reflections reflections = new Reflections("com.cla.filecluster.upgrade.operations");
        Set<Class<? extends UpgradeOperation>> upgradeOperationClasses = reflections.getSubTypesOf(UpgradeOperation.class);

        logger.debug("prepareUpgradeMetadatas: upgradeOperationClasses={}",
                upgradeOperationClasses.stream()
                        .map(Class::getCanonicalName)
                        .collect(Collectors.joining(",")));

        int currentVersion = baselineVersion;
        int currentStep = baselineStep;
        boolean latestOperationAboveBaseline = false;
        Map<String, String> opDataMap = Maps.newHashMap();

        UpgradeOperationStep latestOperation = upgradeRepo.getLatestOperation();
        if (latestOperation != null) {

            // here we check if the latest operation retrieved from the DB has version.step above the baseline
            // baseline is the version.step of the current software version
            if (latestOperation.getUpgradeVersion() > baselineVersion) {
                currentVersion = latestOperation.getUpgradeVersion();
                latestOperationAboveBaseline = true;
            }

            if (latestOperation.getUpgradeVersion() >= baselineVersion && latestOperation.getUpgradeStep() > baselineStep) {
                currentStep = latestOperation.getUpgradeStep();
                latestOperationAboveBaseline = true;
            }

            // if we need to re-run the latest operation
            if (latestOperationAboveBaseline && latestOperation.getState() != UpgradeOperationState.DONE) {
                currentStep--; // make sure it is not filtered out below
                if (latestOperation.getOpData() != null) {
                    opDataMap = latestOperation.getOpData();
                }
            }
        }

        // scan all the upgrade operation classes, collect the metadatas
        List<UpgradeOperationMetadata> metadatas = upgradeOperationClasses.stream().
                filter(upgradeOperationClass -> !Modifier.isAbstract(upgradeOperationClass.getModifiers()) &&
                        !ReflectionUtils.withAnnotation(PermanentOperation.class).apply(upgradeOperationClass))
                .map(UpgradeOperationMetadata::create).collect(Collectors.toList());

        logger.debug("prepareUpgradeMetadatas: metadatas = {}",
                metadatas.stream()
                        .map(m -> m.getOperationClass().getCanonicalName() + "-" + m.getVersion() + "." + m.getStep())
                        .collect(Collectors.joining(",")));

        // sorter to sort the metadatas by version, step
        Ordering<UpgradeOperationMetadata> sorter = new Ordering<UpgradeOperationMetadata>() {
            @Override
            public int compare(UpgradeOperationMetadata left, UpgradeOperationMetadata right) {
                return Ints.compare(left.getVersion(), right.getVersion()) * 10 +
                        Ints.compare(left.getStep(), right.getStep());
            }
        };

        // filter out the irrelevant steps (either below baseline or the upgrade operations were already done)
        final Pair<Integer, Integer> version = Pair.of(currentVersion, currentStep);
        List<UpgradeOperationMetadata> sortedMetadatas = sorter.sortedCopy(metadatas).stream()
                .filter(metadata -> metadata.getVersion() > baselineVersion)      // first - must be above the baseline
                .filter(metadata -> metadata.getVersion() > version.getKey() ||   // second - must be above the version.step of the last executed operation
                        (metadata.getVersion() == version.getKey() && metadata.getStep() > version.getValue()))
                .collect(Collectors.toList());


        // if the latest operation was not finished - add operation data from the DB to the metadata object
        if (latestOperation != null && sortedMetadatas.size() > 0) {
            UpgradeOperationMetadata firstOperation = sortedMetadatas.get(0);
            if (latestOperation.getUpgradeVersion() == firstOperation.getVersion() &&
                    latestOperation.getUpgradeStep() == firstOperation.getStep()) {
                firstOperation.setOpData(opDataMap);
            }
        }

        return sortedMetadatas;
    }

    /**
     * Prepare metadatas of permanent operations, i.e. operations that should always run,
     * regardless of version
     *
     * @return list of operation metadatas
     */
    private List<UpgradeOperationMetadata> preparePermanentMetadatas() {
        Reflections reflections = new Reflections("com.cla.filecluster.upgrade.operations.permanent");
        Set<Class<? extends UpgradeOperation>> upgradeOperationClasses =
                reflections.getSubTypesOf(UpgradeOperation.class);

        logger.debug("preparePermanentMetadatas: upgradeOperationClasses={}",
                upgradeOperationClasses.stream()
                        .map(Class::getCanonicalName)
                        .collect(Collectors.joining(",")));

        List<UpgradeOperationMetadata> metadatas = upgradeOperationClasses.stream()
                .filter(aClass -> ReflectionUtils.withAnnotation(PermanentOperation.class).apply(aClass))
                .map(UpgradeOperationMetadata::create).collect(Collectors.toList());

        logger.debug("preparePermanentMetadatas: metadatas={}",
                metadatas.stream()
                        .map(m -> m.getOperationClass().getCanonicalName() + "-" + m.getVersion() + "." + m.getStep())
                        .collect(Collectors.joining(",")));

        Ordering<UpgradeOperationMetadata> sorter = new Ordering<UpgradeOperationMetadata>() {
            @Override
            public int compare(UpgradeOperationMetadata left, UpgradeOperationMetadata right) {
                return (left.getVersion() == right.getVersion()) ? Ints.compare(left.getStep(), right.getStep()) : Ints.compare(left.getVersion(), right.getVersion());
            }
        };

        return sorter.sortedCopy(metadatas);
    }

    /**
     * Update version/step in the DB
     *
     * @param metadata upgrade operation metadata
     * @param state    state to be assigned to the operation
     */
    private void updateUpgradeOperation(UpgradeOperationMetadata metadata, UpgradeOperationState state) {
        dbTemplateUtils.doInTransaction(() -> {
            UpgradeOperationStep step = upgradeRepo.getByVersion(metadata.getVersion(), metadata.getStep());
            if (step == null) {
                step = new UpgradeOperationStep();
                step.setUpgradeVersion(metadata.getVersion());
                step.setUpgradeStep(metadata.getStep());
                step.setStartTime(timeSource.currentTimeMillis());
            }

            if (state != step.getState()) {
                step.setStateUpdateTime(timeSource.currentTimeMillis());
                step.setState(state);
            }

            step.setOpData(metadata.getOpData());
            upgradeRepo.save(step);
        }, TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }


    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
