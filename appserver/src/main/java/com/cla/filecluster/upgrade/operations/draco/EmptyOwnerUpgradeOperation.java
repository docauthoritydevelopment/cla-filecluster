package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 6/4/2019
 */
@Upgrade(targetVersion = 7, step = 7)
public class EmptyOwnerUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.empty-owner.enabled:true}")
    private boolean enabled;

    private boolean isJobFinished = false;

    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 7 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();

        logger.info("Starting migration");

        try {
            List<String> filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.OWNER, SolrOperator.MISSING, null)));
            Integer cnged = solrFileCatRepository.updateFieldByQuery(
                    CatFileFieldType.OWNER.getSolrName(), FileCatUtils.EMPTY_VALUE, SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

            filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.OWNER, SolrOperator.EQ, "\"\"")));
            Integer cnged2 = solrFileCatRepository.updateFieldByQuery(
                    CatFileFieldType.OWNER.getSolrName(), FileCatUtils.EMPTY_VALUE, SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

            int total = (cnged == null ? 0 : cnged) + (cnged2 == null ? 0 : cnged2);

            opData.put(START_LIMIT, String.valueOf(total));
            logger.info("Files empty owner migration done for {} files", total);
            isJobFinished = true;

        } catch (Exception e) {
            logger.info("empty owner files migration issue", e);
            throw e;
        }
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
