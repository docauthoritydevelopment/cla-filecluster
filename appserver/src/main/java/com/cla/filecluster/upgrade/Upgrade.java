package com.cla.filecluster.upgrade;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for upgrade operations
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Upgrade {
    // target version
    int targetVersion() default 0;
    // step within the version upgrade
    int step();
    // SQL scripts to run
    String[] sqlScripts() default {};
    // should run when running tests
    boolean shouldRunOnTest() default false;

    boolean withArgs() default false;
}
