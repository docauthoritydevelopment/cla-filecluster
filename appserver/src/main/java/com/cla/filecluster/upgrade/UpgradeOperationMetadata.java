package com.cla.filecluster.upgrade;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.reflections.ReflectionUtils;

import java.util.List;
import java.util.Map;

/**
 * Metadata for upgrade operations
 */
public class UpgradeOperationMetadata {
    private Class<? extends UpgradeOperation> operationClass;
    private int version;
    private int step;
    private boolean updateVersion = true;
    private List<String> scriptNames;
    private Map<String,String> opData = Maps.newHashMap();

    protected UpgradeOperationMetadata() {
    }

    public static UpgradeOperationMetadata create(Class<? extends UpgradeOperation> operationClass){
        if (!operationClass.isAnnotationPresent(Upgrade.class)){
            throw new RuntimeException("Missing @Upgrade annotation on class " + operationClass.getSimpleName());
        }

        Upgrade annotation = operationClass.getAnnotation(Upgrade.class);

        boolean updateVersion = true;
        if (ReflectionUtils.withAnnotation(PermanentOperation.class).apply(operationClass)){
            updateVersion = false;
        }

        return new UpgradeOperationMetadata()
                .setOperationClass(operationClass)
                .setVersion(annotation.targetVersion())
                .setStep(annotation.step())
                .setScriptNames(Lists.newArrayList(annotation.sqlScripts()))
                .setUpdateVersion(updateVersion);
    }

    public Class<? extends UpgradeOperation> getOperationClass() {
        return operationClass;
    }

    public UpgradeOperationMetadata setOperationClass(Class<? extends UpgradeOperation> operationClass) {
        this.operationClass = operationClass;
        return this;
    }

    public int getVersion() {
        return version;
    }

    public UpgradeOperationMetadata setVersion(int version) {
        this.version = version;
        return this;
    }

    public int getStep() {
        return step;
    }

    public UpgradeOperationMetadata setStep(int step) {
        this.step = step;
        return this;
    }

    public List<String> getScriptNames() {
        return scriptNames;
    }

    public UpgradeOperationMetadata setScriptNames(List<String> scriptNames) {
        this.scriptNames = scriptNames;
        return this;
    }

    public boolean isUpdateVersion() {
        return updateVersion;
    }

    public UpgradeOperationMetadata setUpdateVersion(boolean updateVersion) {
        this.updateVersion = updateVersion;
        return this;
    }

    public Map<String, String> getOpData() {
        return opData;
    }

    public void setOpData(Map<String, String> opData) {
        this.opData = opData;
    }
}

