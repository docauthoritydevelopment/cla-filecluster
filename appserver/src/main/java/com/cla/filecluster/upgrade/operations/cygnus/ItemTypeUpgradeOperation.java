package com.cla.filecluster.upgrade.operations.cygnus;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.Map;

import static com.cla.common.constants.CatFileFieldType.ITEM_TYPE;

/**
 * Created by: yael
 * Created on: 1/30/2019
 */
@Upgrade(targetVersion = 6, step = 1)
public class ItemTypeUpgradeOperation extends UpgradeOperation {
    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-item-type-files.enabled:true}")
    private boolean enabled;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 1 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();

        logger.info("Starting migration");

        try {
            List<String> filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.EXTENSION, SolrOperator.EQ, "eml")));
            Integer cnged = solrFileCatRepository.updateFieldByQuery(
                    ITEM_TYPE.getSolrName(), String.valueOf(ItemType.MESSAGE.toInt()), SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

            filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.EXTENSION, SolrOperator.NE, "eml")));
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.BASE_NAME, SolrOperator.EQ, "[at#")));
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.FULLPATH, SolrOperator.EQ, "[em#")));

            Integer cnged2 = solrFileCatRepository.updateFieldByQuery(
                    ITEM_TYPE.getSolrName(), String.valueOf(ItemType.ATTACHMENT.toInt()), SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

            int total = (cnged == null ? 0 : cnged) + (cnged2 == null ? 0 : cnged2);

            opData.put(START_LIMIT, String.valueOf(total));
            logger.info("Files migration done for {} eml files and {} attachments item type", cnged, cnged2);
            isJobFinished = true;

        } catch (Exception e) {
            logger.info("item type files migration issue", e);
            throw e;
        }
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
