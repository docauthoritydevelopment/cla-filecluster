package com.cla.filecluster.upgrade.operations.fornax.solr7;

import com.cla.filecluster.upgrade.MigrateCollectionDataUpgradeOperation;
import com.cla.filecluster.upgrade.Upgrade;
import org.springframework.beans.factory.annotation.Value;

/**
 * Responsible to migrate all cat folders document to be with be with Id as string and not as long
 */
@Upgrade(targetVersion = 9, step = 5)
public class BizListMigrationOperation extends MigrateCollectionDataUpgradeOperation {

    private static final String SOURCE_COLLECTION_NAME = "BizList";

    @Value("${solr7.migration.biz-list.enabled:true}")
    private boolean enabled;

    // ---------------- Keep all common definitions below this line ---
    private static final String DESTINATION_COLLECTION_NAME = SOURCE_COLLECTION_NAME + "_2";
    private static final String COLLECTION_ALIAS = SOURCE_COLLECTION_NAME;

    @Value("${solr7.migration.collections.page-size:5000}")
    private int pageSize;

    @Value("${solr7.migration.collections.commit-after:true}")
    private boolean commitWhenDone;

    @Override
    protected boolean commitWhenDone() {
        return commitWhenDone;
    }

    @Override
    protected int getPageSize() {
        return pageSize;
    }

    @Override
    protected boolean isEnabled() {
        return enabled;
    }

    @Override
    protected String getSourceCollectionName() {
        return SOURCE_COLLECTION_NAME;
    }

    @Override
    protected String getDestinationCollectionName() {
        return DESTINATION_COLLECTION_NAME;
    }

    @Override
    protected String getCollectionliasName() {
        return COLLECTION_ALIAS;
    }
}
