package com.cla.filecluster.upgrade;

import com.cla.filecluster.repository.solr.infra.SolrClientAdapter;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.scripts.SolrCloudConfigurer;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Responsible to migrate all collection documents upon internal schema change
 */
public abstract class MigrateCollectionDataUpgradeOperation extends UpgradeOperation {

    private static final String MARK_CURSOR = "MARK_CURSOR";
    private static final String PAGE_INDEX = "PAGE_INDEX";
    private static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    private static final String TOTAL_NUM_OF_PAGES = "TOTAL_NUM_OF_PAGES";
    private static final String INDEX_SORT_FIELD = "id";

    private SolrClientAdapter dstSolrClientAdapter = null;
    private SolrClientAdapter srcSolrClientAdapter = null;

    @Autowired
    protected SolrClientFactory solrClientFactory;

    @Autowired
    protected SolrCloudConfigurer solrCloudConfigurer;

    protected abstract boolean isEnabled();
    protected abstract String getSourceCollectionName();
    protected abstract String getDestinationCollectionName();
    protected abstract String getCollectionliasName();
    protected abstract int getPageSize();
    protected abstract boolean commitWhenDone();

    protected String getIndexSortField() {
        return INDEX_SORT_FIELD;
    }

    protected boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        if (!solrClientFactory.isSolrCloud()) {// Skip if not solr cloud
            logger.info("Env is with local solr and not solr cloud, Skipping upgrade operation (version = {}, step = {})",
                    metadata.getVersion(), metadata.getStep());
            isJobFinished = true;
            return;
        }
        if (!isEnabled()) { // skip if not enabled
            isJobFinished = true;
            logger.info("Upgrade operation (version = {}, step = {}) not enabled, skipped",
                    metadata.getVersion(), metadata.getStep());
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        String fromCursorMark = opData.
                getOrDefault(MARK_CURSOR, CursorMarkParams.CURSOR_MARK_START);
        int pageIndex = Integer.valueOf(opData.get(PAGE_INDEX));
        int totalNumOfPages = Integer.valueOf(opData.get(TOTAL_NUM_OF_PAGES));
        if (pageIndex > totalNumOfPages) {
            Integer totalNumOfDocuments = Integer.valueOf(metadata.getOpData().get(TOTAL_AMOUNT));
            logger.info("Migrate operation id as string of {} documents by {} pages is done",
                    totalNumOfDocuments, totalNumOfPages);
            if (commitWhenDone()) {
                dstSolrClientAdapter.commit(false, true, true);
            }
            createCollectionAlias();
            isJobFinished = true;
            return;
        }
        try {
            logger.info("Start Migrating page {} out of {}", pageIndex, totalNumOfPages);
            String nextCursorMark = migratePage(dstSolrClientAdapter, srcSolrClientAdapter, fromCursorMark, pageIndex);
            opData.put(MARK_CURSOR, nextCursorMark);
            opData.put(PAGE_INDEX, String.valueOf(pageIndex + 1));
            logger.info("Done Migrating page {} out of {}", pageIndex, totalNumOfPages);
        } catch (Exception e) {
            logger.error(String.format("Failed to perform collection migration for collection %s page %d", getSourceCollectionName(), pageIndex), e);
            throw e;
        }
    }

    private void createCollectionAlias() {
        logger.info("Creating {} Alias to collection {}", getCollectionliasName(), getDestinationCollectionName());
        CollectionAdminResponse adminResponse;
        try {
            adminResponse = solrCloudConfigurer.createAlias(getCollectionliasName(), getDestinationCollectionName());
        } catch (Exception e) {
            isJobFinished = true;
            throw e;
        }
        if (!adminResponse.isSuccess() && adminResponse.getErrorMessages() != null) {   // known issue with SolrJ, null error message inidactes success
            isJobFinished = true;
            throw new RuntimeException("Failed to create alias "+ getCollectionliasName() + " to " + getDestinationCollectionName() + ". "
                    + adminResponse.getErrorMessages());
        }
    }

    protected String migratePage(SolrClientAdapter dstSolrClientAdapter, SolrClientAdapter srcSolrClientAdapter, String fromCursorMark, int pageIndex)
            throws Exception {
        SolrQuery solrQuery = new SolrQuery("*:*")
                .addSort(getIndexSortField(), SolrQuery.ORDER.asc)
                .setRows(getPageSize())
                .setStart(0);
        solrQuery.set("cursorMark", fromCursorMark);

        // First fetch the documents
        long start = System.currentTimeMillis();
        QueryResponse queryResponse = srcSolrClientAdapter.query(solrQuery);
        SolrDocumentList results = queryResponse.getResults();
        logger.info("Page {}: Fetched {} documents tp migrate in {} ms", pageIndex, results.size(),
                System.currentTimeMillis() - start);

        // Second transform docs
        start = System.currentTimeMillis();
        logger.info("Page {}: Start converting {} documents with id as string", pageIndex, results.size());
        List<SolrInputDocument> transformedDocs = results.stream()
                .map(this::documentConverter)
                .collect(Collectors.toList());
        dstSolrClientAdapter.add(transformedDocs);
        dstSolrClientAdapter.commit();
        logger.info("Page {}: Done saving  {} documents with id as string", pageIndex, results.size(),
                System.currentTimeMillis() - start);
        return queryResponse.getNextCursorMark();
    }

    protected SolrInputDocument documentConverter(SolrDocument doc) {
        SolrInputDocument transformedDocument = new SolrInputDocument();
        if (logger.isTraceEnabled()) {
            logger.trace("Converting doc with id={}: {}", doc.getFieldValues("id").toString(),
                doc.getFieldNames().stream()
                        .map(fieldName -> fieldName+":"+transformFieldValue(doc, fieldName))
                        .collect(Collectors.joining(",")));
        }
        doc.getFieldNames().stream()
                .forEach(fieldName ->
                        transformedDocument.setField(fieldName, transformFieldValue(doc, fieldName)));
        transformedDocument.removeField("_version_"); // Otherwise will fail on version conflict
        return transformedDocument;
    }

    protected Object transformFieldValue(SolrDocument doc, String fieldName) {
        return doc.getFieldValue(fieldName);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

    @Override
    protected void beforeIterationStart() {
        if (!isEnabled()) {
            return;
        }

        if (dstSolrClientAdapter != null && srcSolrClientAdapter != null){ // Means we already started
            return;
        }
        if (dstSolrClientAdapter == null) {
            dstSolrClientAdapter = solrClientFactory.getSolrClient(getDestinationCollectionName(), false);
            if (dstSolrClientAdapter == null) {
                throw new RuntimeException("Could not create solr client for " + getDestinationCollectionName());
            }
            logger.debug("Solr client for {} created", getDestinationCollectionName());
        }
        if (srcSolrClientAdapter == null) {
            srcSolrClientAdapter = solrClientFactory.getSolrClient(getSourceCollectionName(), false);
            if (srcSolrClientAdapter == null) {
                throw new RuntimeException("Could not create solr client for " + getSourceCollectionName());
            }
            logger.debug("Solr client for {} created", getSourceCollectionName());
        }

        // Get number of records on source collections
        if (!metadata.getOpData().containsKey(PAGE_INDEX)) {
            QueryResponse queryResponse = null;
            try {
                queryResponse = srcSolrClientAdapter.query(Query.create()
                        .setRows(0)
                        .setStart(0)
                        .build());
            } catch (Exception e) {
                throw new RuntimeException("Failed beforeIterationStart() on " + getDestinationCollectionName() + ". "
                        + e.getMessage(), e);
            }
            SolrDocumentList results = queryResponse.getResults();
            if (results.getNumFound() == 0) {
                logger.info("No docs to migrate  on collection {}", getSourceCollectionName());
                createCollectionAlias();
                isJobFinished = true;
                return;
            }

            long totalNumOfPages = (results.getNumFound() / getPageSize()) + 1;
            metadata.getOpData().put(PAGE_INDEX, String.valueOf(1));
            metadata.getOpData().put(TOTAL_AMOUNT, String.valueOf(results.getNumFound()));
            metadata.getOpData().put(TOTAL_NUM_OF_PAGES, String.valueOf(totalNumOfPages));
            logger.info("About to perform migration {} documents from {} to {} through {} iterations by page size of {}",
                    results.getNumFound(), getSourceCollectionName(), getDestinationCollectionName(), totalNumOfPages, getPageSize());
        }
    }

}
