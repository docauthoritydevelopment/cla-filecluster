package com.cla.filecluster.upgrade.operations.electra;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.schedule.ScheduledOperationPriority;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.repository.jpa.group.ScheduledOperationRepository;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.operation.ApplyFolderAssociationOnFiles;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 8/5/2019
 */
@Upgrade(targetVersion = 8, step = 4)
public class ReapplyAssociationsUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private SolrCatFolderRepository solrFolderRepository;

    @Autowired
    private ScheduledOperationRepository scheduledOperationRepository;

    @Value("${migration.apply-association.enabled:true}")
    private boolean enabled;

    @Value("${migration.apply-association.page-size:5000}")
    private int pageSize;

    @Value("${associations.apply-folder-associations-retry:5}")
    private int retryForApplyFolderAssociationToFiles;

    private boolean isJobFinished = false;

    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 4 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();

        logger.info("Starting migration");

        EntityManager em = emf.createEntityManager();
        EntityTransaction txn = em.getTransaction();

        try {
            txn.begin();

            int total = em.createNativeQuery("update tag_association set dirty = 1 where deleted = 0;").executeUpdate();

            total += em.createNativeQuery("update doc_type_association set dirty = 1 where deleted = 0 and group_id is not null;").executeUpdate();

            txn.commit();

            String cursor = "*";
            long cycle = 0;
            Integer from = 0;

            boolean shouldContinue = true;
            while (shouldContinue) {
                cycle++;
                List<ScheduledOperationMetadata> associationsToApply = new ArrayList<>();
                com.cla.filecluster.repository.solr.query.Query query =
                        com.cla.filecluster.repository.solr.query.Query.create()
                                .addField(CatFoldersFieldType.ID)
                                .addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                                .setCursorMark(cursor)
                                .setRows(pageSize)
                                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, false))
                                .addSort(CatFoldersFieldType.ID, Sort.Direction.ASC);
                ;
                QueryResponse resp = solrFolderRepository.runQuery(query.build());
                cursor = resp.getNextCursorMark();
                resp.getResults().forEach(fileDoc -> {
                    Long docFolderId = (Long)fileDoc.getFieldValue(CatFoldersFieldType.ID.getSolrName());
                    List<String> associationsObj = (List<String>)fileDoc.getFieldValue(CatFoldersFieldType.FOLDER_ASSOCIATIONS.getSolrName());
                    if (associationsObj != null && associationsObj.size() > 0) {
                        associationsObj.forEach(association -> {
                            if (association.contains("implicit\":false")) {
                                try {
                                    AssociationEntity entity = ModelDtoConversionUtils.getMapper().readValue(association, AssociationEntity.class);
                                    ScheduledOperationMetadata md = createFolderApplyOperation(docFolderId, entity, SolrFieldOp.ADD);
                                    associationsToApply.add(md);
                                } catch (Exception e) {
                                    logger.error("problem handling association {} for folder {}", association, docFolderId, e);
                                }
                            }
                        });
                    }
                });

                if (associationsToApply.size() > 0) {
                    total += associationsToApply.size();
                    scheduledOperationRepository.saveAll(associationsToApply);
                }

                if (resp.getResults().size() < pageSize) {
                    shouldContinue = false;
                }
                from += pageSize;
                logger.info("Ended current migration cycle {} for {} apply associations {}", cycle, from, associationsToApply.size());
            }

            opData.put(START_LIMIT, String.valueOf(total));
            logger.info("apply association migration done for {} associations", total);
            isJobFinished = true;

        } catch (Exception e) {
            logger.info("apply association migration issue", e);
            if (txn.isActive()) {
                txn.rollback();
            }
            throw e;
        }
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

    private ScheduledOperationMetadata createFolderApplyOperation(Long docFolderId, AssociationEntity entity, SolrFieldOp operation) {
        String association = AssociationsService.convertFromAssociationEntity(entity);
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_FOLDER_ASSOCIATIONS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyFolderAssociationOnFiles.DOC_FOLDER_ID_PARAM, String.valueOf(docFolderId));
        data.getOpData().put(ApplyFolderAssociationOnFiles.ASSOCIATION_PARAM, association);
        data.getOpData().put(ApplyFolderAssociationOnFiles.ACTION_PARAM, operation.name());
        data.setUserOperation(false);
        data.setPriority(ScheduledOperationPriority.LOW);
        data.setRetryLeft(retryForApplyFolderAssociationToFiles);
        data.setCreateTime(System.currentTimeMillis());
        data.setStep(0);
        return data;
    }
}
