package com.cla.filecluster.upgrade.operations.electra;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;

/**
 * Created by: yael
 * Created on: 9/23/2019
 */
@Upgrade(targetVersion = 8, step = 7)
public class FolderAssociationFixUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Value("${migration.fix-folder-association-implicit.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-folder-association-implicit.page-size:30000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 7 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration");

        String cursor = "*";
        long cycle = 0;
        try {
            boolean shouldContinue = true;
            while (shouldContinue) {
                cycle++;
                Query query = Query.create()
                        .addField(CatFoldersFieldType.ID)
                        .addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                        .addField(CatFoldersFieldType.DEPTH_FROM_ROOT)
                        .addField(CatFoldersFieldType.PARENTS_INFO)
                        .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS,
                                SolrOperator.PRESENT, null))
                        .setCursorMark(cursor)
                        .setRows(pageSize)
                        .addSort(CatFoldersFieldType.ID, Sort.Direction.ASC);
                SolrQuery solrQuery = query.build();
                QueryResponse resp = solrCatFolderRepository.runQuery(solrQuery);
                logger.trace("migration cycle {} query {}", cycle, solrQuery);
                cursor = resp.getNextCursorMark();
                logger.debug(" migration cycle {} got {} responses out of {}", cycle, resp.getResults().size(), resp.getResults().getNumFound());


                Map<Long, FolderData> implicitAssociations = new HashMap<>();
                Map<Long, FolderData> explicitAssociations = new HashMap<>();
                resp.getResults().forEach(folderDoc -> {
                    Long folderId = (Long) folderDoc.getFieldValue(CatFoldersFieldType.ID.getSolrName());
                    List<String> associations = (List<String>) folderDoc.getFieldValue(CatFoldersFieldType.FOLDER_ASSOCIATIONS.getSolrName());
                    Integer depthFromRoot = (Integer) folderDoc.getFieldValue(CatFoldersFieldType.DEPTH_FROM_ROOT.getSolrName());
                    List<String> parentInfo = (List<String>) folderDoc.getFieldValue(CatFoldersFieldType.PARENTS_INFO.getSolrName());
                    if (associations != null) {
                        for (String association : associations) {
                            AssociationEntity entity = AssociationsService.convertFromAssociationEntity(folderId, association, AssociationType.FOLDER);
                            if (entity.getImplicit()) {
                                FolderData imp = implicitAssociations.getOrDefault(folderId, new FolderData(depthFromRoot, parentInfo));
                                imp.addAssociation(entity);
                                implicitAssociations.put(folderId, imp);
                            } else {
                                FolderData imp = explicitAssociations.getOrDefault(folderId, new FolderData(depthFromRoot, parentInfo));
                                imp.addAssociation(entity);

                                explicitAssociations.put(folderId, imp);
                            }
                        }
                    }
                });

                handleExplicitAssociations(implicitAssociations, explicitAssociations);
                handleImplicitAssociations(implicitAssociations, explicitAssociations);

                if (resp.getResults().size() < pageSize) {
                    shouldContinue = false;
                }
                from += pageSize;
                solrCatFolderRepository.softCommitNoWaitFlush();
                logger.info("Ended current migration cycle {} for {} folder assoc fix", cycle, from);
            }

            opData.put(START_LIMIT, String.valueOf(from));
            logger.info("Files migration done for {} folder assoc fix", from);
            isJobFinished = true;
        } catch (Exception e) {
            logger.info("fix folder associations migration issue", e);
            throw e;
        }
    }

    private void handleImplicitAssociations(Map<Long, FolderData> implicitAssociations,
                                            Map<Long, FolderData> explicitAssociations) {

        List<SolrInputDocument> foldersToChange = new ArrayList<>();
        Map<Long, SolrFolderEntity> addedFoldersToChk = new HashMap<>();

        // for every folder with implicit associations
        for (Long folderId : implicitAssociations.keySet()) {
            FolderData imp = implicitAssociations.get(folderId);

            // for every implicit association
            for (AssociationEntity implicitAssoc : imp.associations) {
                boolean foundMemory = false;
                boolean shouldFix = true;

                // search an explicit association in memory to match, if possible
                if (explicitAssociations.containsKey(implicitAssoc.getOriginFolderId())) {
                    foundMemory = true;
                    FolderData fd = explicitAssociations.get(implicitAssoc.getOriginFolderId());
                    if (fd.associations != null) {
                        for (AssociationEntity exAssoc : fd.associations) {
                            // if exists, all is well
                            if (sameAssoc(exAssoc, implicitAssoc)) {
                                shouldFix = false;
                                break;
                            }
                        }
                    }
                }

                if (!foundMemory && addedFoldersToChk.containsKey(implicitAssoc.getOriginFolderId())) {
                    foundMemory = true;
                    SolrFolderEntity fd = addedFoldersToChk.get(implicitAssoc.getOriginFolderId());
                    if (fd.getAssociations() != null) {
                        for (String parentAssoc : fd.getAssociations()) {
                            AssociationEntity entity = AssociationsService.convertFromAssociationEntity(
                                    folderId, parentAssoc, AssociationType.FOLDER);
                            // if exists, all is well
                            if (sameAssoc(entity, implicitAssoc)) {
                                shouldFix = false;
                                break;
                            }
                        }
                    }
                }

                if (implicitAssoc.getOriginFolderId() == null) {
                    logger.warn("missing origin folder {} for folder {} and association {} will delete it",
                            implicitAssoc.getOriginFolderId(), folderId, implicitAssoc);
                }
                else if (!foundMemory) { // get parent from solr and test
                    Query query = Query.create()
                            .addField(CatFoldersFieldType.ID)
                            .addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                            .addField(CatFoldersFieldType.PARENTS_INFO)
                            .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID,
                                    SolrOperator.EQ, implicitAssoc.getOriginFolderId()))

                            .setRows(1);
                    logger.debug("going to solr to get missing data for implicit association {}", implicitAssoc);
                    SolrQuery solrQuery = query.build();
                    List<SolrFolderEntity> resp = solrCatFolderRepository.find(solrQuery);
                    SolrFolderEntity parent = resp.size() > 0 ? resp.get(0) : null;
                    if (parent != null) { // if exists, all is well
                        addedFoldersToChk.put(implicitAssoc.getOriginFolderId(), parent);
                        if (parent.getAssociations() != null) {
                            for (String parentAssoc : parent.getAssociations()) {
                                AssociationEntity entity = AssociationsService.convertFromAssociationEntity(
                                        folderId, parentAssoc, AssociationType.FOLDER);
                                // if exists, all is well
                                if (sameAssoc(entity, implicitAssoc)) {
                                    shouldFix = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        logger.warn("cant find parent folder {} for folder {} and association {} will delete it",
                                implicitAssoc.getOriginFolderId(), folderId, implicitAssoc);
                    }
                }

                // remove implicit association - as explicit no longer exists
                if (shouldFix) {
                    logger.trace("implicit association without explicit - remove {} of folder {}", implicitAssoc, folderId);
                    SolrInputDocument doc = createDoc(folderId);
                    String json = AssociationsService.convertFromAssociationEntity(implicitAssoc);
                    addField(doc, CatFoldersFieldType.FOLDER_ASSOCIATIONS, json, SolrFieldOp.REMOVE);
                    foldersToChange.add(doc);
                }
            }
        }

        if (foldersToChange.size() > 0) {
            logger.debug("removing orphan implicit associations {}", foldersToChange.size());
            solrCatFolderRepository.saveAll(foldersToChange);
        }
    }

    private SolrInputDocument createDoc(Long folderId) {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField(CatFoldersFieldType.ID.getSolrName(), folderId);
        return doc;
    }

    private void addField(SolrInputDocument solrInputDocument, CatFoldersFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    private void handleExplicitAssociations(Map<Long, FolderData> implicitAssociations,
                                            Map<Long, FolderData> explicitAssociations) {

        List<SolrFolderEntity> addedFoldersToChk = new ArrayList<>();
        int totalFixed = 0;

        // for every folder with explicit associations
        for (Long folderId : explicitAssociations.keySet()) {

            FolderData exp = explicitAssociations.get(folderId);

            // for every explicit association
            for (AssociationEntity explicitAssoc : exp.associations) {
                boolean foundMemory = false;
                boolean shouldFix = true;

                // search an implicit association in memory to match, if possible
                for (Long fdId : implicitAssociations.keySet()) {
                    FolderData fd = implicitAssociations.get(fdId);
                    if (!folderId.equals(fdId) && isChildFolder(exp.parentInfo, fd.parentInfo)) {
                        foundMemory = true;
                        if (fd.associations != null) {
                            for (AssociationEntity impAssoc : fd.associations) {
                                if (sameAssoc(impAssoc, explicitAssoc)) {
                                    // if has same creation date, all is well
                                    if (impAssoc.getCreationDate().equals(explicitAssoc.getCreationDate())) {
                                        shouldFix = false;
                                    }
                                    break;
                                }
                            }
                        }
                    }

                    if (foundMemory) {
                        break;
                    }
                }

                if (!foundMemory) {
                    for (SolrFolderEntity fd : addedFoldersToChk) {
                        if (!folderId.equals(fd.getId()) && isChildFolder(exp.parentInfo, fd.getParentsInfo())) {
                            foundMemory = true;
                            if (fd.getAssociations() != null) {
                                for (String impAssoc : fd.getAssociations()) {
                                    AssociationEntity entity = AssociationsService.convertFromAssociationEntity(
                                            folderId, impAssoc, AssociationType.FOLDER);
                                    if (sameAssoc(entity, explicitAssoc)) {
                                        // if has same creation date, all is well
                                        if (entity.getCreationDate().equals(explicitAssoc.getCreationDate())) {
                                            shouldFix = false;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        if (foundMemory) {
                            break;
                        }
                    }
                }

                // get implicit from solr and test
                if (!foundMemory) {
                    Query query = Query.create()
                            .addField(CatFoldersFieldType.ID)
                            .addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                            .addField(CatFoldersFieldType.PARENTS_INFO)
                            .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.PARENTS_INFO,
                                    SolrOperator.EQ, exp.depthFromRoot + "." + folderId))
                            .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID,
                                    SolrOperator.NE, folderId))
                            .setRows(1);
                    SolrQuery solrQuery = query.build();
                    logger.debug("going to solr to get missing data for explicit association");
                    List<SolrFolderEntity> resp = solrCatFolderRepository.find(solrQuery);
                    SolrFolderEntity son = resp.size() > 0 ? resp.get(0) : null;
                    if (son == null) { // no implicit associations exists cause no child folders
                        break;
                    } else { // find matching implicit association in son
                        addedFoldersToChk.add(son);
                        if (son.getAssociations() != null) {
                            for (String sonAssocStr : son.getAssociations()) {
                                AssociationEntity entity = AssociationsService.convertFromAssociationEntity(
                                        folderId, sonAssocStr, AssociationType.FOLDER);
                                if (sameAssoc(entity, explicitAssoc)) { // if has same creation date, all is well
                                    if (entity.getCreationDate().equals(explicitAssoc.getCreationDate())) {
                                        shouldFix = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (shouldFix) { // remove all invalid implicit associations and recreate them
                    logger.trace("going to solr to fix association {} of folder {}", explicitAssoc, folderId);
                    addRemoveSubFolderAssociation(SolrFieldOp.REMOVE_REGEX, explicitAssoc);
                    addRemoveSubFolderAssociation(SolrFieldOp.ADD, explicitAssoc);
                    ++totalFixed;
                }
            }
        }
        logger.debug("total explicit associations fixed {}", totalFixed);
    }

    private void addRemoveSubFolderAssociation(SolrFieldOp operation, AssociationEntity entity) {
        String pattern = entity.getOriginDepthFromRoot() + "." + entity.getOriginFolderId();
        String query = CatFoldersFieldType.PARENTS_INFO.getSolrName() + ":" + pattern;
        List<String> filters = Lists.newArrayList("-" + CatFoldersFieldType.ID.getSolrName() + ":" + entity.getOriginFolderId());
        String association = convertFromAssociationEntity(entity, operation);
        solrCatFolderRepository.updateFieldByQuery(CatFoldersFieldType.FOLDER_ASSOCIATIONS.getSolrName(), association,
                operation, query, filters, SolrCommitType.NONE);
    }

    private String convertFromAssociationEntity(AssociationEntity entity, SolrFieldOp operation) {
        entity.setImplicit(true);
        Long createDate = entity.getCreationDate();
        if (operation.equals(SolrFieldOp.REMOVE_REGEX)) {
            entity.setCreationDate(999999999999L);
        }
        String association = AssociationsService.convertFromAssociationEntity(entity);
        if (operation.equals(SolrFieldOp.REMOVE_REGEX)) {
            association = association.replace("999999999999", "[0-9]*");
            association = association.replace("{", ".*");
            association = association.replace("}", ".*");
            association = association.replace(":", "\\:");
            entity.setCreationDate(createDate);
        }
        return association;
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

    private boolean isChildFolder(List<String> parentInfoParent, List<String> parentInfoChild) {
        for (String pi : parentInfoParent) {
            if (!parentInfoChild.contains(pi)) {
                return false;
            }
        }
        return true;
    }

    private boolean sameAssoc(AssociationEntity one, AssociationEntity two) {
        return one != null && two != null &&
                Objects.equals(one.getDepartmentId(), two.getDepartmentId()) &&
                Objects.equals(one.getDocTypeId(), two.getDocTypeId()) &&
                Objects.equals(one.getFileTagId(), two.getFileTagId()) &&
                Objects.equals(one.getFunctionalRoleId(), two.getFunctionalRoleId()) &&
                Objects.equals(one.getSimpleBizListItemSid(), two.getSimpleBizListItemSid()) &&
                Objects.equals(one.getOriginFolderId(), two.getOriginFolderId()) &&
                Objects.equals(one.getOriginDepthFromRoot(), two.getOriginDepthFromRoot());
    }

    class FolderData {

        List<String> parentInfo;
        Integer depthFromRoot;
        List<AssociationEntity> associations;

        FolderData(Integer depthFromRoot, List<String> parentInfo) {
            this.depthFromRoot = depthFromRoot;
            this.parentInfo = parentInfo;
            associations = new ArrayList<>();
        }

        void addAssociation(AssociationEntity association) {
            this.associations.add(association);
        }
    }
}
