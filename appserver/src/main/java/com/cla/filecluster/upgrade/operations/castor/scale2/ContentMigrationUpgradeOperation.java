package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by: yael
 * Created on: 5/28/2018
 */
@Upgrade(targetVersion = 5, step = 3)
public class ContentMigrationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private SolrContentMetadataRepository solrContentMetadataRepository;

    @Value("${migration.content.page-size:5000}")
    private int pageSize;

    @Value("${migration.logging.content.limit:100}")   // 0 - no log; -1 - log all
    private int loggingItemsLimit;

    private boolean isJobFinished = false;

    private int loggingItemsCounter = 0;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("starting migration from {}", from);

        EntityManager em = emf.createEntityManager();
        try {
            List<Object[]> results = em.createNativeQuery("show tables like 'content_metadata'").getResultList();
            if (results == null || results.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table content_metadata doesnt exist");
                return;
            }

            results = em.createNativeQuery("SELECT " +
                    "id" +
                    ",analyze_hint" +
                    ",app_creation_date" +
                    ",author" +
                    ",company" +
                    ",content_signature" +
                    ",file_count" +
                    ",fs_file_size" + // 7
                    ",generating_app" +
                    ",group_dirty" +
                    ",items_countl1" +
                    ",items_countl2" +
                    ",items_countl3" +
                    ",keywords" +
                    ",last_ingest_timestamp" + // 14
                    ",population_dirty" +
                    ",sample_file_id" +
                    ",state" +
                    ",subject" +
                    ",template" +
                    ",title" +
                    ",type" +
                    ",user_content_signature" + // 22
                    ",file_group_id" +
                    ",user_group_id" +           // 24
                    " from content_metadata c " +
                    " where id > " + from +
                    " limit " + pageSize).getResultList();

            int rows = 0;
            long recordId = 0;
            List<SolrInputDocument> contentsToUpdate = new LinkedList<>();
            for (Object[] contentData : results) {
                BigInteger recordIdBigInt = (BigInteger)contentData[0];
                recordId = recordIdBigInt.longValue();
                SolrInputDocument document = createDocument(contentData);
                if (document != null) {
                    contentsToUpdate.add(document);

                    if (logger.isTraceEnabled() && (loggingItemsLimit < 0 || loggingItemsCounter < loggingItemsLimit)) {
                        logger.trace("Upgrade Content: \n    row={}\n    doc={}",
                                IntStream.range(0, contentData.length)
                                        .mapToObj(i -> (i + ":" + (contentData[i] == null ? "null"  :contentData[i].toString())))
                                        .collect(Collectors.joining(",")),
                                document.toString());
                        loggingItemsCounter++;
                    }
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Upgrade: got null document for row: {}",
                                IntStream.range(0, contentData.length)
                                        .mapToObj(i -> (i + ":" + (contentData[i] == null ? "null" : contentData[i].toString())))
                                        .collect(Collectors.joining(",")));
                    }
                }
                rows++;
            }
            if (!contentsToUpdate.isEmpty()) {
                solrContentMetadataRepository.createContentsFromDocuments(contentsToUpdate);
                solrContentMetadataRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(recordId));

            if (rows < pageSize) {
                logger.info("file migration done for {} records", (from+pageSize));
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (from+pageSize));
            }
        } catch (Exception e) {
            logger.info("content migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private SolrInputDocument createDocument(Object[] fileData) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger id = (BigInteger)fileData[0];
        solrInputDocument.setField(ContentMetadataFieldType.ID.getSolrName(), id.longValue());

        addField(solrInputDocument, ContentMetadataFieldType.TYPE, fileData[21]);
        addField(solrInputDocument, ContentMetadataFieldType.CONTENT_SIGNATURE, fileData[5]);

        if (fileData[22] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.USER_CONTENT_SIGNATURE, fileData[22]);
        }

        if (fileData[23] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.GROUP_ID, fileData[23]);
        }
        if (fileData[24] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.USER_GROUP_ID, fileData[24]);
        }

        if (fileData[1] != null) {
            AnalyzeHint hint = AnalyzeHint.valueOf((Integer)fileData[1]);
            addField(solrInputDocument, ContentMetadataFieldType.ANALYSIS_HINT, hint.name());
        }

        if (fileData[2] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.CREATION_DATE, fileData[2]);
        }

        if (fileData[7] != null) {
            BigInteger size = (BigInteger)fileData[7];
            addField(solrInputDocument, ContentMetadataFieldType.SIZE, size.longValue());
        }

        addField(solrInputDocument, ContentMetadataFieldType.FILE_COUNT, fileData[6]);

        if (fileData[17] != null) {
            ClaFileState state = ClaFileState.valueOf((int)fileData[17]);
            addField(solrInputDocument, ContentMetadataFieldType.PROCESSING_STATE, state.name());
        }

        if (fileData[3] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.AUTHOR, fileData[3]);
        }
        if (fileData[4] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.COMPANY, fileData[4]);
        }
        if (fileData[18] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.SUBJECT, fileData[18]);
        }
        if (fileData[20] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.TITLE, fileData[20]);
        }
        if (fileData[13] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.KEYWORDS, fileData[13]);
        }
        if (fileData[8] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.GENERATING_APP, fileData[8]);
        }
        if (fileData[19] != null) {
            addField(solrInputDocument, ContentMetadataFieldType.TEMPLATE, fileData[19]);
        }

        if (fileData[16] != null) {
            BigInteger sid = (BigInteger)fileData[16];
            addField(solrInputDocument, ContentMetadataFieldType.SAMPLE_FILE_ID, sid.longValue());
        }

        if (fileData[14] != null) {
            BigInteger lgt = (BigInteger)fileData[14];
            addField(solrInputDocument, ContentMetadataFieldType.LAST_INGEST_TIMESTAMP_MS, lgt.longValue());
        }

        addField(solrInputDocument, ContentMetadataFieldType.GROUP_DIRTY, fileData[9]);
        addField(solrInputDocument, ContentMetadataFieldType.POPULATION_DIRTY, fileData[15]);

        if (fileData[10] != null) {
            BigInteger ic1 = (BigInteger)fileData[10];
            addField(solrInputDocument, ContentMetadataFieldType.ITEMS_COUNT1, ic1.longValue());
        }

        if (fileData[11] != null) {
            BigInteger ic2 = (BigInteger)fileData[11];
            addField(solrInputDocument, ContentMetadataFieldType.ITEMS_COUNT2, ic2.longValue());
        }

        if (fileData[12] != null) {
            BigInteger ic3 = (BigInteger)fileData[12];
            addField(solrInputDocument, ContentMetadataFieldType.ITEMS_COUNT3, ic3.longValue());
        }

        return solrInputDocument;
    }

    private void addField(SolrInputDocument solrInputDocument, ContentMetadataFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
