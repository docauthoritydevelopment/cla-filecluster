package com.cla.filecluster.upgrade.operations.cygnus;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.filecluster.domain.converters.SolrEntityBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.pdfviewer.MapEntry;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 1/31/2019
 */
@Upgrade(targetVersion = 6, step = 2)
public class AttachmentNumUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Value("${migration.fix-attachment-num-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-attachment-num-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 2 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        try {
            // get folders attachment files
            Query q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ITEM_TYPE, SolrOperator.EQ, ItemType.ATTACHMENT.toInt())));
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false")));
            q.setFacetLimit(50000000);
            q.addFacetField(CatFileFieldType.FOLDER_ID);
            q.setFacetMinCount(1);
            q.setRows(0);
            QueryResponse queryResponse = solrFileCatRepository.runQuery(q.build());

            // save folder id and counter of attachments
            Map<Long, Long> result = new TreeMap<>();
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    String folderIdStr = c.getName();
                    long count = c.getCount();
                    result.put(Long.parseLong(folderIdStr), count);
                }
            }

            int toIndex = Math.min(from+pageSize, result.keySet().size());
            List<SolrInputDocument> filesToUpdate = new LinkedList<>();

            // get folder ids in page
            List<Long> folderIds = (new ArrayList(result.keySet())).subList(from, toIndex);
            if (folderIds.size() > 0) {

                // get folder path
                q = Query.create();
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false")));
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID, SolrOperator.IN, folderIds)));
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.TYPE, SolrOperator.EQ, FolderType.EML.toInt())));
                q.addField(CatFoldersFieldType.ID);
                q.addField(CatFoldersFieldType.REAL_PATH);
                List<SolrFolderEntity> folders = solrCatFolderRepository.find(q.build());
                Map<String, Long> paths = new HashMap<>();
                Map<String, Long> hashPath = new HashMap<>();
                folders.forEach(folder -> {
                            paths.put(folder.getRealPath(), folder.getId());
                            hashPath.put(SolrEntityBuilder.getHashedString(folder.getRealPath()), folder.getId());
                        }
                );

                // get eml files of folders
                q = Query.create();
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ITEM_TYPE, SolrOperator.EQ, ItemType.MESSAGE.toInt())));
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false")));
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_NAME_HASHED, SolrOperator.IN, hashPath.keySet())));
                q.addField(CatFileFieldType.FULL_NAME);
                q.addField(CatFileFieldType.FILE_NAME_HASHED);
                q.addField(CatFileFieldType.ID);
                List<SolrFileEntity> entities = solrFileCatRepository.find(q.build());

                // create document update to attachment number
                entities.forEach(entity -> {
                    Long folderId = paths.get(entity.getFullName());
                    if (folderId != null) {
                        SolrInputDocument document = new SolrInputDocument();
                        document.setField(CatFileFieldType.ID.getSolrName(), entity.getId());
                        addField(document, CatFileFieldType.NUM_OF_ATTACHMENTS, result.get(folderId));
                        filesToUpdate.add(document);
                    }
                });
            }

            if (!filesToUpdate.isEmpty()) {
                solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                solrFileCatRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(from+pageSize));

            if (folderIds.size() < pageSize) {
                logger.info("Files migration done for {} files attachment num", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} files attachment num", (from + pageSize));
            }

        } catch (Exception e) {
            logger.info("attachment num files migration issue", e);
            throw e;
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
