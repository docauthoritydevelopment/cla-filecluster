package com.cla.filecluster.upgrade.operations.cygnus;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;

/**
 * Created by: yael
 * Created on: 2/5/2019
 */
@Upgrade(targetVersion = 6, step = 5)
public class ContainerUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-container-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-container-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 5 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        try {

            Query q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ITEM_TYPE, SolrOperator.MISSING, "")));
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.PACKAGE_TOP_FILE_ID, SolrOperator.PRESENT, "")));
            q.addField(CatFileFieldType.ID);
            q.addField(CatFileFieldType.PACKAGE_TOP_FILE_ID);
            q.addField(CatFileFieldType.FULLPATH);
            q.setStart(from);
            q.setRows(pageSize);
            q.addSort(CatFileFieldType.ID, Sort.Direction.ASC);
            List<SolrFileEntity> entities = solrFileCatRepository.find(q.build());

            List<SolrInputDocument> filesToUpdate = new LinkedList<>();
            entities.forEach(file -> {
                SolrInputDocument document = new SolrInputDocument();
                document.setField(CatFileFieldType.ID.getSolrName(), file.getId());

                String type = FileNamingUtils.isPackageEntry(file.getFullPath()) ? FileType.PACKAGE.name() : FileType.PDF.name();

                List<String> containerIds = new ArrayList<>();
                containerIds.add(type+"."+file.getPackageTopFileId());
                addField(document, CatFileFieldType.CONTAINER_IDS, containerIds);

                filesToUpdate.add(document);
            });

            if (!filesToUpdate.isEmpty()) {
                solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                solrFileCatRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(from+pageSize));

            if (entities.size() < pageSize) {
                logger.info("Files migration done for {} container files", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} container files", (from + pageSize));
            }
        } catch (Exception e) {
            logger.info("Container files migration issue", e);
            throw e;
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
