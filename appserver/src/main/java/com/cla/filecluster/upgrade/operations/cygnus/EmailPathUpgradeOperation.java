package com.cla.filecluster.upgrade.operations.cygnus;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.pst.PstPath;
import com.cla.common.utils.EmailUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.mediaconnector.mail.MailRelated;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;

/**
 * Created by: yael
 * Created on: 2/5/2019
 */
@Upgrade(targetVersion = 6, step = 4)
public class EmailPathUpgradeOperation extends UpgradeOperation {
    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-email-path-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-email-path-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 4 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        try {

            Query q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ITEM_TYPE, SolrOperator.EQ, ItemType.MESSAGE.toInt())));
            q.addField(CatFileFieldType.ID);
            q.addField(CatFileFieldType.FULLPATH);
            q.addField(CatFileFieldType.FULL_NAME);
            q.addField(CatFileFieldType.BASE_NAME);
            q.addField(CatFileFieldType.MEDIA_ENTITY_ID);
            q.addField(CatFileFieldType.FOLDER_IDS);
            q.addField(CatFileFieldType.FOLDER_ID);
            q.addField(CatFileFieldType.PACKAGE_TOP_FILE_ID);
            q.addField(CatFileFieldType.SENDER_NAME);
            q.addField(CatFileFieldType.SENDER_ADDRESS);
            q.addField(CatFileFieldType.RECIPIENTS_ADDRESSES);
            q.addField(CatFileFieldType.RECIPIENTS_NAMES);
            q.setStart(from);
            q.setRows(pageSize);
            q.addSort(CatFileFieldType.ID, Sort.Direction.ASC);
            List<SolrFileEntity> entities = solrFileCatRepository.find(q.build());

            List<SolrInputDocument> filesToUpdate = new LinkedList<>();
            entities.forEach(file -> {
                try {
                    if (PstPath.isPstEntryPath(file.getMediaEntityId())) {
                        SolrInputDocument document = new SolrInputDocument();
                        document.setField(CatFileFieldType.ID.getSolrName(), file.getId());
                        if (file.getMediaEntityId() != null) {

                            PstPath pstPath = PstPath.fromString(file.getMediaEntityId());

                            String fullName = pstPath.getNonUniquePath();
                            String baseName = FileNamingUtils.getFilenameBaseName(fullName);
                            String fullPath = FileNamingUtils.getFileNameFullPath(fullName, baseName);
                            String extension = FileNamingUtils.getFilenameExtension(baseName);
                            String entryId = pstPath.getEntryId().isPresent() ? String.valueOf(pstPath.getEntryId().getAsLong()) : null;
                            baseName = MailRelated.changeSubjectToFileName(baseName.substring(0, baseName.length() - (extension.length() + 1)), entryId);
                            baseName += "." + extension;
                            addField(document, CatFileFieldType.FULL_NAME, fullPath + baseName);
                            addField(document, CatFileFieldType.BASE_NAME, baseName);
                            addField(document, CatFileFieldType.FULLPATH, FileNamingUtils.convertPathToUniversalString(fullPath));

                            List<String> containerIds = new ArrayList<>();
                            containerIds.add("PST." + file.getPackageTopFileId());
                            addField(document, CatFileFieldType.CONTAINER_IDS, containerIds);
                            addField(document, CatFileFieldType.TYPE, FileType.MAIL.toInt());

                            EmailAddress sender = new EmailAddress(file.getSenderName(), file.getSenderAddress());
                            addField(document, CatFileFieldType.SENDER_FULL_ADDRESS, ModelIngestUtils.getEmailJson(sender));
                            if (file.getSenderAddress() == null || file.getSenderAddress().trim().isEmpty()) {
                                addField(document, CatFileFieldType.SENDER_ADDRESS, EmailUtils.EMPTY_VALUE);
                                addField(document, CatFileFieldType.SENDER_DOMAIN, EmailUtils.EMPTY_VALUE);
                            } else if (!EmailUtils.containsDomain(file.getSenderAddress())) {
                                addField(document, CatFileFieldType.SENDER_DOMAIN, EmailUtils.UNKNOWN_VALUE);
                            }
                            if (file.getSenderName() == null || file.getSenderName().trim().isEmpty()) {
                                addField(document, CatFileFieldType.SENDER_NAME, EmailUtils.EMPTY_VALUE);
                            }

                            boolean updateRecipients = false;
                            List<String> recipients = new ArrayList<>();
                            Set<String> recipientsNames = new HashSet<>(recipients.size());
                            Set<String> recipientsAddresses = new HashSet<>(recipients.size());
                            Set<String> recipientsDomains = new HashSet<>(recipients.size());
                            if (file.getRecipientsAddresses() != null && !file.getRecipientsAddresses().isEmpty()) {
                                updateRecipients = true;
                                int i = 0;
                                for (String address : file.getRecipientsAddresses()) {
                                    String name = (file.getRecipientsNames() != null &&
                                            file.getRecipientsNames().size() > i ? file.getRecipientsNames().get(i) : "");
                                    EmailAddress obj = new EmailAddress(name, address);
                                    recipients.add(ModelIngestUtils.getEmailJson(obj));

                                    String nameToAdd = EmailUtils.getRecipientName(obj);
                                    if (nameToAdd != null) {
                                        recipientsNames.add(nameToAdd);
                                    }
                                    recipientsAddresses.add(EmailUtils.getEmailAddressValue(obj.getAddress(), false, false));
                                    recipientsDomains.add(EmailUtils.extractDomainFromEmailAddress(obj.getAddress(), false, false));

                                    ++i;
                                }
                            } else if (file.getRecipientsNames() != null && !file.getRecipientsNames().isEmpty()) {
                                updateRecipients = true;
                                for (String name : file.getRecipientsNames()) {
                                    EmailAddress obj = new EmailAddress(name, "");
                                    recipients.add(ModelIngestUtils.getEmailJson(obj));
                                    String nameToAdd = EmailUtils.getRecipientName(obj);
                                    if (nameToAdd != null) {
                                        recipientsNames.add(nameToAdd);
                                    }
                                    recipientsAddresses.add(EmailUtils.getEmailAddressValue(obj.getAddress(), false, false));
                                    recipientsDomains.add(EmailUtils.extractDomainFromEmailAddress(obj.getAddress(), false, false));
                                }
                            }

                            if (updateRecipients) {
                                addField(document, CatFileFieldType.RECIPIENTS_FULL_ADDRESSES, recipients);
                                addField(document, CatFileFieldType.RECIPIENTS_ADDRESSES, recipientsAddresses);
                                addField(document, CatFileFieldType.RECIPIENTS_DOMAINS, recipientsDomains);
                                addField(document, CatFileFieldType.RECIPIENTS_NAMES, recipientsNames);
                            }

                            filesToUpdate.add(document);
                        }
                    }
                } catch (Exception e) {
                    logger.error("problem handling file {}", file, e);
                }
            });

            if (!filesToUpdate.isEmpty()) {
                solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                solrFileCatRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(from+pageSize));

            if (entities.size() < pageSize) {
                logger.info("Files migration done for {} eml files", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} eml files", (from + pageSize));
            }
        } catch (Exception e) {
            logger.info("Eml files migration issue", e);
            throw e;
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
