package com.cla.filecluster.upgrade.operations.electra;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.ActiveAssociationUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 9/8/2019
 */
@Upgrade(targetVersion = 8, step = 6)
public class FileActiveAssocUpgradeOperation extends UpgradeOperation {

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.active-assoc-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.active-assoc-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 6 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);
        String cursor = "*";
        long cycle = 0;

        try {
            boolean shouldContinue = true;
            List<SolrInputDocument> filesToUpdate = new LinkedList<>();
            while (shouldContinue) {
                cycle++;
                com.cla.filecluster.repository.solr.query.Query query =
                        com.cla.filecluster.repository.solr.query.Query.create()
                                .addField(CatFileFieldType.ID)
                                .addField(CatFileFieldType.DEPARTMENT_ID)
                                .addField(CatFileFieldType.SCOPED_TAGS)
                                .addFilterWithCriteria(Criteria.or(
                                        Criteria.create(CatFileFieldType.DEPARTMENT_ID, SolrOperator.PRESENT, null),
                                        Criteria.create(CatFileFieldType.SCOPED_TAGS, SolrOperator.PRESENT, null)))
                                .setCursorMark(cursor)
                                .setRows(pageSize)
                                .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

                SolrQuery solrQuery = query.build();
                QueryResponse resp = solrFileCatRepository.runQuery(solrQuery);
                logger.trace("migration cycle {} query {}", cycle, solrQuery);
                cursor = resp.getNextCursorMark();
                logger.debug(" migration cycle {} got {} responses out of {}", cycle, resp.getResults().size(), resp.getResults().getNumFound());
                resp.getResults().forEach(fileDoc -> {
                    String dept = (String)fileDoc.getFieldValue(CatFileFieldType.DEPARTMENT_ID.getSolrName());
                    List<String> scopedTags = getFixScopedTags(fileDoc);
                    SolrInputDocument doc = new SolrInputDocument();
                    doc.setField(CatFileFieldType.ID.getSolrName(), fileDoc.getFieldValue(CatFileFieldType.ID.getSolrName()));
                    Set<String> activeAssoc = new HashSet<>();
                    if (dept != null) {
                        String val = ActiveAssociationUtils.getActiveAssociationIdentifier(dept);
                        activeAssoc.add(val);
                    }
                    if (scopedTags != null) {
                        scopedTags.forEach(id -> {
                            if (id != null) {
                                String val = ActiveAssociationUtils.getActiveAssociationIdentifier(id);
                                activeAssoc.add(val);
                            }
                        });
                        addField(doc, CatFileFieldType.SCOPED_TAGS, scopedTags, SolrFieldOp.SET);
                    }
                    addField(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, activeAssoc, SolrFieldOp.SET);
                    filesToUpdate.add(doc);
                });
                if (resp.getResults().size() < pageSize) {
                    shouldContinue = false;
                }
                from += pageSize;

                if (!filesToUpdate.isEmpty() && (!shouldContinue || filesToUpdate.size() > 200)) {
                    solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                    solrFileCatRepository.softCommitNoWaitFlush();
                    logger.debug("current migration cycle {} for {} active assoc files upd {}", cycle, from, filesToUpdate.size());
                    filesToUpdate.clear();
                }
                logger.info("Ended current migration cycle {} for {} active assoc", cycle, from);
            }

            opData.put(START_LIMIT, String.valueOf(from));
            logger.info("Files migration done for {} active assoc for files", from);
            isJobFinished = true;

        } catch (Exception e) {
            logger.info("active assoc for files migration issue", e);
            throw e;
        }
    }

    private List<String> getFixScopedTags(SolrDocument doc) {
        List<String> scopedTags = (List<String>)doc.getFieldValue(CatFileFieldType.SCOPED_TAGS.getSolrName());
        List<String> toRem = new ArrayList<>();
        if (scopedTags != null && !scopedTags.isEmpty()) {
            scopedTags.forEach(tag -> {
                if (tag.startsWith("dt.") && !tag.startsWith("dt.g.")) {
                    toRem.add(tag);
                } else if (tag.startsWith("g.")) {
                    int exists =
                            scopedTags.stream().filter(comp -> comp.contains(tag) && comp.length() > tag.length()).collect(Collectors.toList()).size();
                    if (exists > 0) {
                        toRem.add(tag);
                    }
                }
            });
            scopedTags.removeAll(toRem);
        }
        return scopedTags;
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }
}