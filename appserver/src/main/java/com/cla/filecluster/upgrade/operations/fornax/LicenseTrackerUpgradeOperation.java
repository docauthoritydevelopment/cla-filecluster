package com.cla.filecluster.upgrade.operations.fornax;

import com.cla.common.constants.CatFileFieldType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.scan.TrackingKeys;
import com.cla.filecluster.service.license.LicenseTrackerService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.client.solrj.response.FieldStatsInfo;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
@Upgrade(targetVersion = 9, step = 8)
public class LicenseTrackerUpgradeOperation extends UpgradeOperation {

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private LicenseTrackerService licenseTrackerService;

    @Value("${migration.migrate-license-tracker.enabled:true}")
    private boolean enabled;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 8 is disabled. Skipped");
            return;
        }

        try {
            QueryResponse resp = fileCatService.runQuery(Query.create().setRows(0)
                    .addParam("stats.facet", CatFileFieldType.ROOT_FOLDER_ID.getSolrName())
                    .setStatsField(CatFileFieldType.SIZE).build());
            FieldStatsInfo info = resp.getFieldStatsInfo().get(CatFileFieldType.SIZE.getSolrName());
            Long count = info.getCount();
            Long sum = ((Double) info.getSum()).longValue();

            licenseTrackerService.setTrackerLongValue(TrackingKeys.TOTAL_FILE_COUNT_KEY, count, false);
            licenseTrackerService.setTrackerLongValue(TrackingKeys.TOTAL_FILE_VOLUME_KEY, sum, false);

            List<FieldStatsInfo> perRf = info.getFacets().get(CatFileFieldType.ROOT_FOLDER_ID.getSolrName());
            perRf.forEach(rfStat -> {
                Long rootFolderId = Long.parseLong(rfStat.getName());
                Long countRf = rfStat.getCount();
                Long sumRf = ((Double) rfStat.getSum()).longValue();
                licenseTrackerService.setTrackerLongValue(TrackingKeys.rootFolderCountKey(rootFolderId), countRf, false);
                licenseTrackerService.setTrackerLongValue(TrackingKeys.rootFolderVolumeKey(rootFolderId), sumRf, false);
            });

            resp = fileCatService.runQuery(Query.create().setRows(0)
                    .addParam("stats.facet", CatFileFieldType.MEDIA_TYPE.getSolrName())
                    .setStatsField(CatFileFieldType.SIZE).build());
            info = resp.getFieldStatsInfo().get(CatFileFieldType.SIZE.getSolrName());
            List<FieldStatsInfo> perMT = info.getFacets().get(CatFileFieldType.MEDIA_TYPE.getSolrName());
            perMT.forEach(mtStat -> {
                Integer mediaTypeInt = Integer.parseInt(mtStat.getName());
                MediaType mediaType = MediaType.valueOf(mediaTypeInt);
                Long countMT = mtStat.getCount();
                Long sumMT = ((Double) mtStat.getSum()).longValue();
                licenseTrackerService.setTrackerLongValue(TrackingKeys.mediaCountKey(mediaType), countMT, false);
                licenseTrackerService.setTrackerLongValue(TrackingKeys.mediaVolumeKey(mediaType), sumMT, false);
            });

            licenseTrackerService.flushAll(true);
            isJobFinished = true;
        } catch (Exception e) {
            logger.info("license tracker migration issue", e);
            throw e;
        }
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
