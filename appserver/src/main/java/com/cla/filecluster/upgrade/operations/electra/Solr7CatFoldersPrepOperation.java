package com.cla.filecluster.upgrade.operations.electra;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Responsible to migrate all cat folders document to be with be with Id as string and not as long
 */
@Upgrade(targetVersion = 8, step = 5)
public class Solr7CatFoldersPrepOperation extends UpgradeOperation {

    private static final String MARK_CURSOR = "MARK_CURSOR";
    private static final String PAGE_INDEX = "PAGE_INDEX";
    private static final String TOTAL_AMOUNT = "TOTAL_AMOUNT";
    private static final String TOTAL_NUM_OF_PAGES = "TOTAL_NUM_OF_PAGES";

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Value("${migration.solr7-cat-folders-prep.enabled:false}")
    private boolean enabled;

    @Value("${migration.solr7-cat-folders-prep.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;


    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        if (!enabled) { // skip if not enabled
            isJobFinished = true;
            logger.info("Upgrade operation (version = {}, step = {}) not enabled, skipped",
                    metadata.getVersion(), metadata.getStep());
            return;
        }
        Map<String, String> opData = metadata.getOpData();
        String fromCursorMark = opData.
                getOrDefault(MARK_CURSOR, CursorMarkParams.CURSOR_MARK_START);
        int pageIndex = Integer.valueOf(opData.get(PAGE_INDEX));
        int totalNumOfPages = Integer.valueOf(opData.get(TOTAL_NUM_OF_PAGES));
        if (pageIndex > totalNumOfPages) {
            Integer totalNumOfDocuments = Integer.valueOf(metadata.getOpData().get(TOTAL_AMOUNT));
            logger.info("Update with id_as_string for {} documents by {} pages is done",
                    totalNumOfDocuments, totalNumOfPages);
            isJobFinished = true;
            return;
        }
        try {
            logger.info("Start upgrading page {} out of {}", pageIndex, totalNumOfPages);
            migrateIdAsString(opData, fromCursorMark, pageIndex, totalNumOfPages);
            logger.info("Done upgrading page {} out of {}", pageIndex, totalNumOfPages);
        } catch (Exception e) {
            logger.error(String.format("Failed to update documents with id_as_string for page %d", pageIndex), e);
            throw e;
        }


    }

    private void migrateIdAsString(Map<String, String> opData, String fromCursorMark, int pageIndex, int totalNumOfPages) {
        SolrQuery solrQuery = Query.create()
                .setCursorMark(fromCursorMark)
                .addSort(CatFoldersFieldType.ID, Sort.Direction.ASC)
                .setRows(pageSize)
                .setStart(0)
                .build();

        // First fetch the documents
        long start = System.currentTimeMillis();
        QueryResponse queryResponse = solrCatFolderRepository.runQuery(solrQuery);
        SolrDocumentList results = queryResponse.getResults();
        logger.info("Page {}: Fetched {} documents in {} ms", pageIndex, results.size(),
                System.currentTimeMillis() - start);

        // Second transform with id as string
        start = System.currentTimeMillis();
        logger.info("Page {}: Start updating {} documents with id_as_string", pageIndex, results.size());
        List<SolrInputDocument> transformedDocs = results.stream()
                .map(this::documentWithIdAsString)
                .collect(Collectors.toList());
        solrCatFolderRepository.saveAll(transformedDocs);
        solrCatFolderRepository.commit();
        opData.put(MARK_CURSOR, queryResponse.getNextCursorMark());
        opData.put(PAGE_INDEX, String.valueOf(pageIndex + 1));
        logger.info("Page {}: Done updating {} documents with id_as_string in {} ms", pageIndex, results.size(),
                System.currentTimeMillis() - start);
    }

    private SolrInputDocument documentWithIdAsString(SolrDocument doc) {
        SolrInputDocument transformedDocument = new SolrInputDocument();
        doc.getFieldNames().stream()
                .forEach(fieldName ->
                        transformedDocument.setField(fieldName, doc.getFieldValue(fieldName)));
        transformedDocument.removeField("_version_"); // Otherwise will fail on version conflict
        return transformedDocument;
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

    @Override
    protected void beforeIterationStart() {
        if (metadata.getOpData().containsKey(PAGE_INDEX)){ // Means already started
            return;
        }
        QueryResponse queryResponse = solrCatFolderRepository.runQuery(Query.create()
                .setRows(0)
                .setStart(0)
                .build());
        SolrDocumentList results = queryResponse.getResults();
        if (results.getNumFound() == 0) {
            logger.info("CatFolders is empty, skipping upgrade operation");
            isJobFinished = true;
            return;
        }

        long totalNumOfPages = (results.getNumFound() / pageSize) + 1;
        metadata.getOpData().put(PAGE_INDEX, String.valueOf(1));
        metadata.getOpData().put(TOTAL_AMOUNT, String.valueOf(results.getNumFound()));
        metadata.getOpData().put(TOTAL_NUM_OF_PAGES, String.valueOf(totalNumOfPages));
        logger.info("About to add id a string on {} documents through {} iterations by page size of {}",
                results.getNumFound(), totalNumOfPages, pageSize);

    }
}
