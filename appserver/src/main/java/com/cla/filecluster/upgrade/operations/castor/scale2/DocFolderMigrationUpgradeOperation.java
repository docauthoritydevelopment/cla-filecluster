package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.base.Strings;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;
import org.springframework.util.DigestUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by: yael
 * Created on: 5/28/2018
 */
@Upgrade(targetVersion = 5, step = 2)
public class DocFolderMigrationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Value("${migration.folder.page-size:5000}")
    private int pageSize;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    protected SolrCatFolderRepository solrCatFolderRepository;

    @Value("${migration.logging.folder.limit:100}")   // 0 - no log; -1 - log all
    private int loggingItemsLimit;

    private boolean isJobFinished = false;

    private int loggingItemsCounter = 0;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));
        Integer to = from + pageSize;

        logger.info("starting migration from {}", from);

        EntityManager em = emf.createEntityManager();
        try {
            List<Object[]> results = em.createNativeQuery("show tables like 'doc_folder'").getResultList();
            if (results == null || results.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table doc_folder doesnt exist");
                return;
            }

            results = em.createNativeQuery("SELECT " +
                    "c.id" +                        // 00
                    ",c.absolute_depth" +           // 01
                    ",c.acl_signature" +            // 02
                    ",c.deleted" +                  // 03
                    ",c.depth_from_root" +          // 04
                    ",c.folder_hashed" +            // 05
                    ",c.media_entity_id" +          // 06
                    ",c.name" +                     // 07
                    ",c.parents_info" +             // 08
                    ",c.path" +                     // 09
                    ",c.real_path" +                // 10
                    ",c.parent_folder_id" +         // 11
                    ",c.root_folder_id" +           // 12
                    ",c.path_descriptor_type" +     // 13
                    ",rf.path as rf_path" +             // 14
                    ",rf.real_path as rf_real_path" +   // 15
                    ",c.folder_type " +                 // 16
                    " FROM doc_folder c " +
                    " left join root_folder rf on rf.id = c.root_folder_id " +
                    " where c.id > " + from +
                    " limit " + pageSize).getResultList();

            int rows = 0;
            long recordId = 0;
            List<SolrInputDocument> foldersToUpdate = new LinkedList<>();
            for (Object[] folderData : results) {
                BigInteger recordIdBigInt = (BigInteger)folderData[0];
                recordId = recordIdBigInt.longValue();
                SolrInputDocument document = createDocument(folderData);
                if (document != null) {
                    foldersToUpdate.add(document);

                    if (logger.isTraceEnabled() && (loggingItemsLimit < 0 || loggingItemsCounter < loggingItemsLimit)) {
                        logger.trace("Upgrade CatFolder: \n    row={}\n    doc={}",
                                IntStream.range(0, folderData.length)
                                        .mapToObj(i -> (i + ":" + (folderData[i] == null ? "null" : folderData[i].toString())))
                                        .collect(Collectors.joining(",")),
                                document.toString());
                        loggingItemsCounter++;
                    }
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Upgrade: got null document for row: {}",
                                IntStream.range(0, folderData.length)
                                        .mapToObj(i -> (i + ":" + (folderData[i] == null ? "null" : folderData[i].toString())))
                                        .collect(Collectors.joining(",")));
                    }
                }
                rows++;
            }
            if (!foldersToUpdate.isEmpty()) {
                solrCatFolderRepository.createFoldersFromDocuments(foldersToUpdate);
                solrCatFolderRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(recordId));

            if (rows < pageSize) {
                logger.info("folder migration done for {} records", to);
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", to);
            }
        } catch (Exception e) {
            logger.info("folder migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private static String getHashedFolderString(final String path, final Long rootFolderId) {
        String pathString = FileNamingUtils.convertPathToUniversalString(path);
        return DigestUtils.md5DigestAsHex((rootFolderId + ":" + pathString).getBytes(StandardCharsets.UTF_8));
    }

    private SolrInputDocument createDocument(Object[] fileData) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger id = (BigInteger)fileData[0];
        solrInputDocument.setField(CatFoldersFieldType.ID.getSolrName(), id.longValue());

        String rfPath = (String)fileData[14];
        String rfRealPath = (String)fileData[15];
        if (rfRealPath.endsWith("\\") || rfRealPath.endsWith("/")) {
            rfRealPath = rfRealPath.substring(0, rfRealPath.length()-1);
        }

        String normalizedRfRealPath = FileNamingUtils.convertPathToUniversalString(rfRealPath);
        normalizedRfRealPath = FileNamingUtils.convertFromUnixToWindows(normalizedRfRealPath);
        if (normalizedRfRealPath.endsWith("\\") || normalizedRfRealPath.endsWith("/")) {
            normalizedRfRealPath = normalizedRfRealPath.substring(0, normalizedRfRealPath.length()-1);
        }

        String path = (String)fileData[9];
        path = FileNamingUtils.convertFromUnixToWindows(path);
        path = path.startsWith(normalizedRfRealPath) ? path.substring(normalizedRfRealPath.length()) : path;
        path = FileNamingUtils.convertPathToUniversalString(path);
        addField(solrInputDocument, CatFoldersFieldType.PATH, path);

        String realPath = (String)fileData[10];
        realPath = realPath.startsWith(rfPath) ? realPath.substring(rfPath.length()) : realPath;
        realPath = realPath.startsWith(rfRealPath) ? realPath.substring(rfRealPath.length()) : realPath;
        //realPath = !realPath.contains("\\\\") ? realPath.replace("\\","\\\\") : realPath;
        addField(solrInputDocument, CatFoldersFieldType.REAL_PATH, realPath);

        int folderType = (int)fileData[16];
        addField(solrInputDocument, CatFoldersFieldType.TYPE, folderType);

        String medEntId = (String)fileData[6];
        if (medEntId != null) {
            medEntId = medEntId.startsWith(rfPath) ? medEntId.substring(rfPath.length()) : medEntId;
            medEntId = medEntId.startsWith(rfRealPath) ? medEntId.substring(rfRealPath.length()) : medEntId;
            //medEntId = !medEntId.contains("\\\\") ? medEntId.replace("\\", "\\\\") : medEntId;
            addField(solrInputDocument, CatFoldersFieldType.MEDIA_ENTITY_ID, medEntId);
        } else {
            medEntId = realPath; // for deleted
            addField(solrInputDocument, CatFoldersFieldType.MEDIA_ENTITY_ID, medEntId);
        }

        addField(solrInputDocument, CatFoldersFieldType.NAME, fileData[7]);

        BigInteger rfid = (BigInteger)fileData[12];
        addField(solrInputDocument, CatFoldersFieldType.ROOT_FOLDER_ID, rfid.longValue());

        String hash = getHashedFolderString(path, rfid.longValue());
        if (hash == null || hash.isEmpty()) {
            logger.warn("folder with hash issue id {} path {} rf {}", id, path, rfid);
        }
        addField(solrInputDocument, CatFoldersFieldType.FOLDER_HASH, hash);

        if (fileData[11] != null) {
            BigInteger parentId = (BigInteger)fileData[11];
            addField(solrInputDocument, CatFoldersFieldType.PARENT_FOLDER_ID, parentId.longValue());
        } else if (fileData[8] != null) { // for deleted
            String data = (String) fileData[8];
            String[] parents = data.split("x");
            String parentId = null;
            int depthFromRoot = (int)fileData[4];
            for (String i : parents) {
                if (i.startsWith((depthFromRoot - 1) + ".")) {
                    parentId = i.substring(i.indexOf(".") + 1);
                }
            }
            if (!Strings.isNullOrEmpty(parentId)) {
                addField(solrInputDocument, CatFoldersFieldType.PARENT_FOLDER_ID, Long.parseLong(parentId));
            }
        }

        addField(solrInputDocument, CatFoldersFieldType.DEPTH_FROM_ROOT, fileData[4]);
        addField(solrInputDocument, CatFoldersFieldType.ABSOLUTE_DEPTH, fileData[1]);

        if (fileData[8] != null) {
            String data = (String) fileData[8];
            String[] parents = data.split("x");
            addField(solrInputDocument, CatFoldersFieldType.PARENTS_INFO, parents);
        }

        if (fileData[13] != null) {
            addField(solrInputDocument, CatFoldersFieldType.PATH_DESCRIPTOR_TYPE, fileData[13]);
        } else {
            addField(solrInputDocument, CatFoldersFieldType.PATH_DESCRIPTOR_TYPE, PathDescriptorType.WINDOWS.name());
        }

        addField(solrInputDocument, CatFoldersFieldType.ACL_SIGNATURE, fileData[2]);
        addField(solrInputDocument, CatFoldersFieldType.DELETED, fileData[3]);

        return solrInputDocument;
    }

    private void addField(SolrInputDocument solrInputDocument, CatFoldersFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
