package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
@Upgrade(targetVersion = 7, step = 1)
public class FileSortNameUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-sort-name-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-sort-name-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 1 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);
        String cursor = "*";
        long cycle = 0;

        try {
            boolean shouldContinue = true;
            List<SolrInputDocument> filesToUpdate = new LinkedList<>();
            while (shouldContinue) {
                cycle++;
                com.cla.filecluster.repository.solr.query.Query query =
                        com.cla.filecluster.repository.solr.query.Query.create()
                                .addField(CatFileFieldType.ID)
                                .addField(CatFileFieldType.SORT_NAME)
                                .setCursorMark(cursor)
                                .setRows(pageSize)
                                .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

                QueryResponse resp = solrFileCatRepository.runQuery(query.build());
                cursor = resp.getNextCursorMark();
                resp.getResults().forEach(fileDoc -> {
                    Object sortNameObj = fileDoc.getFieldValue(CatFileFieldType.SORT_NAME.getSolrName());
                    if (sortNameObj instanceof List && ((List)sortNameObj).size() > 1) {
                        SolrInputDocument doc = new SolrInputDocument();
                        doc.setField(CatFileFieldType.ID.getSolrName(), fileDoc.getFieldValue(CatFileFieldType.ID.getSolrName()));
                        addField(doc, CatFileFieldType.SORT_NAME, ((List)sortNameObj).get(0), SolrFieldOp.SET);
                        filesToUpdate.add(doc);
                    }
                });
                if (resp.getResults().size() < pageSize) {
                    shouldContinue = false;
                }
                from += pageSize;

                if (!filesToUpdate.isEmpty() && (!shouldContinue || filesToUpdate.size() > 200)) {
                    solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                    solrFileCatRepository.softCommitNoWaitFlush();
                    logger.debug("current migration cycle {} for {} sort name files upd {}", cycle, from, filesToUpdate.size());
                    filesToUpdate.clear();
                }
                logger.info("Ended current migration cycle {} for {} sort name", cycle, from);
            }

            opData.put(START_LIMIT, String.valueOf(from));
            logger.info("Files migration done for {} sort name files", from);
            isJobFinished = true;

        } catch (Exception e) {
            logger.info("Sort name files migration issue", e);
            throw e;
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
