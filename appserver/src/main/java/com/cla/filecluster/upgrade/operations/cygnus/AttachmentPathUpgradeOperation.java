package com.cla.filecluster.upgrade.operations.cygnus;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.pst.PstPath;
import com.cla.connector.mediaconnector.mail.MailRelated;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 2/4/2019
 */
@Upgrade(targetVersion = 6, step = 3)
public class AttachmentPathUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Value("${migration.fix-attachment-path-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.fix-attachment-path-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 3 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        try {
            Query q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.TYPE, SolrOperator.EQ, FolderType.EML.toInt())));
            q.addField(CatFoldersFieldType.ID);
            q.addField(CatFoldersFieldType.PATH);
            q.addField(CatFoldersFieldType.REAL_PATH);
            q.addField(CatFoldersFieldType.PARENT_FOLDER_ID);
            q.addField(CatFoldersFieldType.ROOT_FOLDER_ID);
            q.setRows(pageSize);
            q.setStart(from);
            q.addSort(CatFoldersFieldType.ID, Sort.Direction.ASC);
            List<SolrFolderEntity> folders = solrCatFolderRepository.find(q.build());

            if (folders.size() > 0) {

                List<Long> folderIds = folders.stream().map(SolrFolderEntity::getId).collect(Collectors.toList());
                Map<Long, SolrFolderEntity> foldersById = folders.stream().collect(Collectors.toMap(SolrFolderEntity::getId, r -> r));
                List<Long> rootFolderIds = folders.stream().map(SolrFolderEntity::getRootFolderId).collect(Collectors.toList());
                List<RootFolder> rootFolders = docStoreService.getRootFolders(rootFolderIds);
                Map<Long, RootFolder> rootFoldersById = rootFolders.stream().collect(Collectors.toMap(RootFolder::getId, r -> r));

                q = Query.create();
                q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.FOLDER_ID, SolrOperator.IN, folderIds)));
                q.addField(CatFileFieldType.ID);
                q.addField(CatFileFieldType.FULLPATH);
                q.addField(CatFileFieldType.FULL_NAME);
                q.addField(CatFileFieldType.BASE_NAME);
                q.addField(CatFileFieldType.MEDIA_ENTITY_ID);
                q.addField(CatFileFieldType.FOLDER_IDS);
                q.addField(CatFileFieldType.FOLDER_ID);
                q.addField(CatFileFieldType.PACKAGE_TOP_FILE_ID);
                q.setRows(500000000);
                List<SolrFileEntity> entities = solrFileCatRepository.find(q.build());

                List<SolrInputDocument> filesToUpdate = new LinkedList<>();
                entities.forEach(file -> {
                    SolrInputDocument document = new SolrInputDocument();
                    document.setField(CatFileFieldType.ID.getSolrName(), file.getId());
                    PstPath pstPath = PstPath.fromString(file.getMediaEntityId());

                    String fullName = pstPath.getNonUniquePath();
                    String baseName = FileNamingUtils.getFilenameBaseName(fullName);
                    String fullPath = FileNamingUtils.getFileNameFullPath(fullName, baseName);

                    PstPath containingEmailPstPath = pstPath.getParent();
                    OptionalLong optionalContainingEmailPstPath = containingEmailPstPath.getEntryId();
                    String containingEmailEntryId;
                    if (optionalContainingEmailPstPath.isPresent()) {
                        containingEmailEntryId = String.valueOf(optionalContainingEmailPstPath.getAsLong());
                    } else {
                        logger.error("Could not extract containing email entry ID of file Id: {}. Using empty string as containing email entry Id", file.getId());
                        containingEmailEntryId = "";
                    }

                    addField(document, CatFileFieldType.ITEM_SUBJECT, baseName);

                    OptionalLong optionalAttachmentEntryId = pstPath.getEntryId();
                    if (optionalAttachmentEntryId.isPresent()) {
                        long attachmentEntryId = optionalAttachmentEntryId.getAsLong();
                        baseName = MailRelated.addHashToBaseName(baseName, containingEmailEntryId + attachmentEntryId);
                    } else  {
                        logger.error("Could not extract attachment entry ID of file Id: {}. Using only containing email entry Id", file.getId());
                        baseName = MailRelated.addHashToBaseName(baseName, containingEmailEntryId);
                    }

                    addField(document, CatFileFieldType.FULL_NAME, fullPath+baseName);
                    addField(document, CatFileFieldType.BASE_NAME, baseName);
                    addField(document, CatFileFieldType.FULLPATH, FileNamingUtils.convertPathToUniversalString(fullPath));

                    SolrFolderEntity folder = foldersById.get(file.getFolderId());
                    addField(document, CatFileFieldType.FOLDER_ID, folder.getParentFolderId());
                    RootFolder rootFolder = rootFoldersById.get(folder.getRootFolderId());

                    String storeName = getStoreName(folder.getRootFolderId());
                    Long emailFileId = kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {

                        String baseNameEmail = FileNamingUtils.getFilenameBaseName(file.getMediaEntityId());
                        String fullPathEmail = FileNamingUtils.getFileNameFullPath(file.getMediaEntityId(), baseNameEmail);
                        fullPathEmail = fullPathEmail.endsWith("\\") ? fullPathEmail.substring(0, fullPathEmail.length() - 1) : fullPathEmail;

                        String key = rootFolder.getRealPath() + fullPathEmail;
                        FileEntity fileEntity = fileEntityStore.get(key);
                        if (fileEntity != null && fileEntity.isFile()) {
                            return fileEntity.getFileId();
                        }
                        return null;
                    });

                    List<String> containerIds = new ArrayList<>();
                    containerIds.add("PST." + file.getPackageTopFileId());
                    if (emailFileId != null) {
                        containerIds.add("MAIL." + emailFileId);
                    }
                    addField(document, CatFileFieldType.CONTAINER_IDS, containerIds);


                    String parentInfoToRem = null;
                    for (String p : file.getParentFolderIds()) {
                        String[] info = p.split("[.]");
                        if (info[1].equals(file.getFolderId().toString())) {
                            parentInfoToRem = p;
                        }
                    }
                    if (parentInfoToRem != null) {
                        file.getParentFolderIds().remove(parentInfoToRem);
                    }
                    addField(document, CatFileFieldType.FOLDER_IDS, file.getParentFolderIds());

                    filesToUpdate.add(document);
                });

                if (!filesToUpdate.isEmpty()) {
                    solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
                    solrFileCatRepository.softCommitNoWaitFlush();
                }

                markDocFoldersAsDeleted(folderIds);
            }

            opData.put(START_LIMIT, String.valueOf(from+pageSize));

            if (folders.size() < pageSize) {
                logger.info("Files migration done for {} eml folders", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} eml folders", (from + pageSize));
            }
        } catch (Exception e) {
            logger.info("Eml folders migration issue", e);
            throw e;
        }
    }

    private void markDocFoldersAsDeleted(Collection<Long> docFolderIds) {
        if (docFolderIds.size() > 0) {
            solrCatFolderRepository.updateFolderByIds(docFolderIds, CatFoldersFieldType.DELETED, Boolean.TRUE, SolrCommitType.SOFT_NOWAIT);
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    private String getStoreName(Long rootFolderId) {
        return KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
    }

        @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
