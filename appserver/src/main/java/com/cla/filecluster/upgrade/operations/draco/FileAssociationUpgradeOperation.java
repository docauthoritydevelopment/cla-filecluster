package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.extractor.pattern.RegulationsService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.categories.FileCatUtils;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 5/1/2019
 */
@Upgrade(targetVersion = 7, step = 3)
public class FileAssociationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private RegulationsService regulationsService;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.migrate-association-files.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-association-files.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    private Set<Long> patternTagTypes = new HashSet<>();

    private static String SQL_QUERY_PREFIX = "SELECT " +
            "a.id, " + // 00
            "a.association_time_stamp, " + // 01
            "a.dirty, " + // 02
            "a.deleted, " + // 03
            "a.doc_type_id, " + // 04
            "a.file_tag_id, " + // 05
            "a.simple_biz_list_item_sid, " + // 06
            "a.cla_file_id, " + // 07
            "a.functional_role_id, " + // 08
            "a.tag_association_scope_id " + // 09
            "from cla_file_tag_association a "
            ;

    private static String SQL_TAG_TYPES = "SELECT id FROM file_tag_type where name in ('Pattern','Pattern-many');";

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 3 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        long from = Long.parseLong(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        long start, timeTookSelect, timeTookHandle;
        try {
            List<Object[]> tableExistsResult = em.createNativeQuery(
                    "show tables like 'cla_file_tag_association'").getResultList();
            if (tableExistsResult == null || tableExistsResult.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table cla_file_tag_association doesnt exist");
                 return;
            }

            List<Object> tagTypesResult = em.createNativeQuery(SQL_TAG_TYPES).getResultList();
            if (tagTypesResult != null && !tagTypesResult.isEmpty()) {
                for (Object row : tagTypesResult) {
                    patternTagTypes.add(((BigInteger)row).longValue());
                }
            }

            start = System.currentTimeMillis();
            Query query = em.createNativeQuery("select max(id) from cla_file_tag_association");
            Object res = query.getSingleResult();
            BigInteger maxId = (BigInteger)res;

            query = em.createNativeQuery(SQL_QUERY_PREFIX +
                    " where a.id > " + from +
                    " order by a.id limit " + pageSize);
            List<Object[]> results = query.getResultList();
            timeTookSelect = System.currentTimeMillis() - start;
            start = System.currentTimeMillis();
            int rows = results.isEmpty() ? 0 : handleResults(results);
            timeTookHandle = System.currentTimeMillis() - start;
            long currMax = results.isEmpty() ?  from + pageSize :
                    ((BigInteger)results.get(results.size()-1)[0]).longValue();

            opData.put(START_LIMIT, String.valueOf(currMax));

            if (maxId == null || results.isEmpty() || currMax == maxId.longValue()) {
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records timeTookSelect {}ms timeTookHandle {}ms",
                        rows, timeTookSelect, timeTookHandle);
            }
        } catch (Exception e) {
            logger.error("file association migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results) {
        int rows = 0;
        List<SolrInputDocument> filesDataToUpdate = new LinkedList<>();
        logger.trace("Upgrade: page with {} results. First res. has {} columns",
                results.size(), results.isEmpty() ? 0 : results.get(0).length);

        List<Long> fileIds = new ArrayList<>();
        for (Object[] fileAssociationData : results) {
            if (fileAssociationData[7] != null) {
                BigInteger fileId = (BigInteger) fileAssociationData[7];
                fileIds.add(fileId.longValue());
            }
        }

        com.cla.filecluster.repository.solr.query.Query query =
                com.cla.filecluster.repository.solr.query.Query.create()
                        .addField(CatFileFieldType.ID)
                        .addField(CatFileFieldType.FILE_ID)
                        .addField(CatFileFieldType.EXTRACTED_PATTERNS_IDS)
                        .addField(CatFileFieldType.FILE_ASSOCIATIONS)
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.IN, fileIds))
                        .setRows(fileIds.size())
                ;
        List<SolrFileEntity> files = solrFileCatRepository.find(query.build());
        Map<Long, SolrFileEntity> filesById = files.stream()
                    .collect(Collectors.toMap(SolrFileEntity::getFileId, r -> r));

        for (Object[] fileAssociationData : results) {
            SolrInputDocument document = createDocument(fileAssociationData, filesById);
            if (document != null) {
                filesDataToUpdate.add(document);
            }
            rows++;
        }

        if (!filesDataToUpdate.isEmpty()) {
            solrFileCatRepository.saveAll(filesDataToUpdate);
            solrFileCatRepository.softCommitNoWaitFlush();
        }
        return rows;
    }

    private SolrInputDocument createDocument(Object[] fileAssociationData, Map<Long, SolrFileEntity> filesById) {

        // deleted association
        if (fileAssociationData[3] != null && ((boolean)fileAssociationData[3])) {
            return null;
        }

        if (fileAssociationData[7] == null){
            return null;
        }

        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger fileId = (BigInteger)fileAssociationData[7];
        SolrFileEntity fileEntity = filesById.get(fileId.longValue());

        if (fileEntity == null) {
            logger.debug("found association with no file for id {}", fileId);
            return null;
        }

        solrInputDocument.setField(CatFileFieldType.ID.getSolrName(), fileEntity.getId());

        AssociationEntity entity = new AssociationEntity();
        entity.setCreationDate(((BigInteger)fileAssociationData[1]).longValue());

        if (fileAssociationData[4] != null) {
            entity.setDocTypeId(((BigInteger)fileAssociationData[4]).longValue());
        } else if (fileAssociationData[5] != null) {
            entity.setFileTagId(((BigInteger)fileAssociationData[5]).longValue());
            FileTagDto tag = fileTagService.getFromCache(entity.getFileTagId());
            if (patternTagTypes.contains(tag.getType().getId())) {
                // remove search pattern tags from fields tags, tags2, scopedTags
                String identifier = AssociationIdUtils.createTagIdentifier(tag);
                addField(solrInputDocument, CatFileFieldType.TAGS, identifier, SolrFieldOp.REMOVE);
                identifier = AssociationIdUtils.createScopedTagIdentifier(tag, null);
                addField(solrInputDocument, CatFileFieldType.SCOPED_TAGS, identifier, SolrFieldOp.REMOVE);

                identifier = AssociationIdUtils.createTag2Identifier(tag);
                addField(solrInputDocument, CatFileFieldType.TAGS_2, identifier, SolrFieldOp.REMOVE);

                // create new search pattern fields
                resolveSearchPatterns(fileEntity)
                        .ifPresent(patterns ->
                                regulationsService.addRegulationsDataToDocument(
                                        solrInputDocument, patterns, true));


                return solrInputDocument;
            } else if (!tag.getType().isSingleValueTag()) {
                // recreate identifier
                String identifier = AssociationIdUtils.createScopedTagIdentifier(tag, null);
                addField(solrInputDocument, CatFileFieldType.SCOPED_TAGS, identifier, SolrFieldOp.REMOVE);
                identifier = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, null, TagPriority.FILE);
                addField(solrInputDocument, CatFileFieldType.SCOPED_TAGS, identifier, SolrFieldOp.ADD);
            }
        } else if (fileAssociationData[6] != null) {
            entity.setSimpleBizListItemSid((String)fileAssociationData[6]);
            // recreate identifier and populate field
            SimpleBizListItemDto bl = bizListItemService.getFromCache(entity.getSimpleBizListItemSid());
            String identifier = AssociationIdUtils.createSimpleBizListItemIdentfier(bl, TagPriority.FILE);
            addField(solrInputDocument, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, identifier, SolrFieldOp.ADD);
        } else if (fileAssociationData[8] != null) {
            entity.setFunctionalRoleId(((BigInteger)fileAssociationData[8]).longValue());
            // recreate identifier and populate field
            FunctionalRoleDto fr = functionalRoleService.getFromCache(entity.getFunctionalRoleId());
            String identifier = AssociationIdUtils.createFunctionalRoleIdentfier(fr, TagPriority.FILE);
            addField(solrInputDocument, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, identifier, SolrFieldOp.ADD);
        }

        if (fileAssociationData[9] != null) {
            entity.setAssociationScopeId(((BigInteger)fileAssociationData[9]).longValue());
        }

        String json = AssociationsService.convertFromAssociationEntity(entity);
        if (fileEntity.getAssociations() != null && fileEntity.getAssociations().contains(json)) {
            return null;
        }
        addField(solrInputDocument, CatFileFieldType.FILE_ASSOCIATIONS, json, SolrFieldOp.ADD);
        return solrInputDocument;
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    private Optional<Set<SearchPatternCountDto>> resolveSearchPatterns(SolrFileEntity entity) {
        // try with getting SearchPatternsCounting from the entity. if missing
        // parse the json from the ExtractedPatternsIds field.
        try {
            return Optional.ofNullable(entity.getExtractedPatternsIds())
                            .map(list -> list.stream()
                                    .map(FileCatUtils::convertFromJsonSearchPatterns)
                                    .filter(Objects::nonNull)
                                    .flatMap(Set::stream)
                                    .collect(Collectors.toSet()));
        } catch (Exception e) {
            logger.debug("error extracting patterns from entity: {} " +
                            "SearchPatternsCounting: {} ExtractedPatternsIds: {} err {}",
                    entity.getId(), entity.getSearchPatternsCounting(), entity.getExtractedPatternsIds(), e.getMessage());
        }
        return Optional.empty();
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
