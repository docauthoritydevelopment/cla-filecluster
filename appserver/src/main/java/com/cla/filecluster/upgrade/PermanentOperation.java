package com.cla.filecluster.upgrade;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark permanent upgrade operations, i.e. operations that are supposed to run on every upgrade,
 * regardless of the version
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PermanentOperation {
}
