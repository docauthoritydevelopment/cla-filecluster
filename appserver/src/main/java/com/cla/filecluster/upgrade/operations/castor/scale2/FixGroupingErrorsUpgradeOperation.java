package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.common.constants.ContentMetadataFieldType.*;

/**
 * Created by: yael
 * Created on: 5/28/2018
 */
@Upgrade(targetVersion = 5, step = 7)
public class FixGroupingErrorsUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private SolrContentMetadataRepository solrContentMetadataRepository;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-grouping.enabled:false}")
    private boolean enabled;

    @Value("${migration.fix-grouping.page-size:50}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled){
            isJobFinished = true;
            logger.info("Upgrade step 7 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();
        try {
            List<Object[]> tables = em.createNativeQuery("show tables like 'content_metadata'").getResultList();
            if (tables == null || tables.isEmpty()) {
                isJobFinished = true;
                logger.info("Exiting migration, table content_metadata does not exist");
                return;
            }

            if (from == 0){
                List<Object> totals = em.createNativeQuery("SELECT COUNT(DISTINCT c.file_group_id) " +
                        "FROM content_metadata c, file_groups f " +
                        "WHERE f.id = c.file_group_id " +
                        "AND f.deleted=1 " +
                        "AND c.file_count > 0").getResultList();

                if (totals.size() > 0) {
                    BigInteger total = (BigInteger)totals.get(0);
                    logger.info("Going to fix {} groups.", total.longValue());
                }
                else{
                    logger.info("Found no groups in need of fixing.");
                    isJobFinished = true;
                    return;
                }
            }

            List<Object> results = em.createNativeQuery("SELECT DISTINCT c.file_group_id " +
                    "FROM content_metadata c, file_groups f " +
                            "WHERE f.id = c.file_group_id " +
                            "AND f.deleted=1 " +
                            "AND c.file_count > 0 " +
                            "LIMIT " + from + ", " + pageSize).getResultList();

            List<String> groupIds = Lists.newLinkedList();

            for (Object result : results) {
                if (result != null && result instanceof String) {
                    String groupId = (String)result;
                    groupIds.add(groupId);
                }
            }
            if (results.size() != groupIds.size()) {
                logger.warn("Found {} groupIds within {} query results", groupIds.size(), results.size());
            }

            if (groupIds.size() > 0) {
                logger.debug("Fixing files with groupIds({}): {}", groupIds.size(),
                        groupIds.stream().collect(Collectors.joining(",")));
                solrContentMetadataRepository.updateFieldByQueryMultiFields(
                        new String[]{GROUP_ID.getSolrName(), USER_GROUP_ID.getSolrName(), PROCESSING_STATE.getSolrName()},
                        new String[]{null, null, ClaFileState.INGESTED.name()},
                        SolrOperator.SET.getOpName(),
                        SolrQueryGenerator.generateFilter(Criteria.create(GROUP_ID, SolrOperator.IN, groupIds)),
                        null, SolrCommitType.SOFT_NOWAIT);

                solrFileCatRepository.updateFieldByQueryMultiFields(
                        new String[]{CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName(),
                                CatFileFieldType.USER_GROUP_ID.getSolrName(),
                                CatFileFieldType.PROCESSING_STATE.getSolrName()},
                        new String[]{null, null, ClaFileState.INGESTED.name()},
                        SolrOperator.SET.getOpName(),
                        SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.IN, groupIds)),
                        null, SolrCommitType.SOFT_NOWAIT);
            }

            opData.put(START_LIMIT, String.valueOf(from + pageSize));

            if (groupIds.size() < pageSize) {
                logger.info("Contents migration done for {} groups", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} groups", (from + pageSize));
            }
        } catch (Exception e) {
            logger.info("Content migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }


    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
