package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.query.CriteriaQueryUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 11/29/2018
 */
@Upgrade(targetVersion = 5, step = 6)
public class IngestErrorMigrationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.ingest-error.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        long recordId = 0;
        try {
            List<Object[]> results = em.createNativeQuery("show tables like 'cla_files_md'").getResultList();
            if (results == null || results.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table cla_files_md doesnt exist");
                return;
            }

            results = em.createNativeQuery("SELECT " +
                    "a.id" +   // 00
                    ",a.cla_file_id" +         // 01
                    ",a.`full_path` as erpath " +         // 02
                    ",a.`file_type` " +        // 03
                    ",a.`root_folder_id` as arf " +   // 04
                    ",a.`folder_id` " +        // 05
                    ",a.`content_id` " +       // 06
                    ",f.content_metadata_id" +   // 07
                    ",f.doc_folder_id" +      // 08
                    ",f.root_folder_id" +     // 09
                    ",f.full_path" +          // 10
                    ",f.type " +               // 11
                    " FROM err_processed_files a " +
                    " left join cla_files_md f on a.cla_file_id = f.id " +
                    " where a.id > " + from +
                    " limit " + pageSize).getResultList();

            List<String> updates = new ArrayList<>();

            for (Object[] folderData : results) {
                BigInteger recordIdBigInt = (BigInteger)folderData[0];
                recordId = recordIdBigInt.longValue();
                if (folderData[4] == null && folderData[9] != null) {

                    String path = (String)folderData[10];
                    if (path != null) {
                        path = CriteriaQueryUtils.convertSlashesForQuerySearch(path);
                        path = path.replace("'", "''");
                    }

                    String update = "update err_processed_files set " +
                    "folder_id = " + folderData[8] + ", root_folder_id = "  +folderData[9] +  ", file_type = " +folderData[11]  +
                            " ,content_id = " + folderData[7]  + ", full_path = '" + path + "'" +
                    " where id = "  + folderData[0] +";";
                    updates.add(update);
                }
            }

            if (!updates.isEmpty()) {
                logger.trace("run sqls "+updates);
                em.getTransaction().begin();
                updates.forEach(u -> {
                    try {
                        em.createNativeQuery(u).executeUpdate();
                    } catch (Exception e) {
                        logger.error("problem executing {}", u, e);
                        throw e;
                    }
                });
                em.getTransaction().commit();
            }

            if (results.size() < pageSize) {
                logger.info("ingest error migration done for {} records out of {} in cycle", updates.size(), results.size());
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records out of {} in cycle", updates.size(), results.size());
            }

        } catch (Exception e) {
            logger.info("Ingest error migration issue", e);
        }
        finally {
            opData.put(START_LIMIT, String.valueOf(recordId));
            em.close();
        }
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return false;
    }
}
