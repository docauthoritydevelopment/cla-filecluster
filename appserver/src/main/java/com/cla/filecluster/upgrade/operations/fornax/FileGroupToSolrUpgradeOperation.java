package com.cla.filecluster.upgrade.operations.fornax;

import com.cla.common.constants.FileGroupsFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.schedule.ScheduledOperationPriority;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.operation.ApplyGroupAssociationOnFiles;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;

import static com.cla.common.constants.FileGroupsFieldType.*;
import static com.cla.common.constants.FileGroupsFieldType.GRANDPARENT_DIRTV;
import static com.cla.common.constants.SolrFieldOp.SET;

/**
 * Created by: yael
 * Created on: 9/9/2019
 */
@SuppressWarnings("FieldCanBeLocal")
@Upgrade(targetVersion = 9, step = 1)
public class FileGroupToSolrUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Value("${migration.migrate-file-group-solr.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-file-group-solr.page-size:5000}")
    private int pageSize;

    @Value("${associations.apply-group-associations-retry:5}")
    private int retryForApplyGroupAssociationToFiles;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 1 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        try {
            List<Object[]> tableExistsResult = em.createNativeQuery("show tables like 'file_groups'").getResultList();
            if (tableExistsResult == null || tableExistsResult.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table file_groups doesnt exist");
                return;
            }

            Query query = em.createNativeQuery(GROUP_SQL +
                    "limit " + from + " , " + pageSize);
            List<Object[]> results = query.getResultList();
            int rows = results.isEmpty() ? 0 : handleResults(results, em);

            opData.put(START_LIMIT, String.valueOf(from + pageSize));

            if (results.size() < pageSize) {
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (rows));
            }
        } catch (Exception e) {
            logger.info("file group migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results, EntityManager em) {
        List<SolrInputDocument> groupDataToUpdate = new LinkedList<>();
        
        List<String> groupIds = new ArrayList<>();
        for (Object[] groupData : results) {
            String groupId = (String)groupData[0];
            groupIds.add(groupId);
        }

        String inPhrase = String.join("','", groupIds);
        inPhrase = "('" + inPhrase + "')";

        Query query = em.createNativeQuery(DOC_TYPE_ASSOC_SQL + inPhrase);
        List<Object[]> dts = query.getResultList();
        Map<String, List<Object[]>> docTypesForGroup = toMapByGroup(dts, 9);

        query = em.createNativeQuery(OTHER_ASSOC_SQL + inPhrase);
        List<Object[]> assoc = query.getResultList();
        Map<String, List<Object[]>> assocForGroup = toMapByGroup(assoc, 7);

        for (Object[] groupData : results) {
            SolrInputDocument document = createGroupDocument(groupData, docTypesForGroup, assocForGroup);
            if (document != null) {
                groupDataToUpdate.add(document);
            }
        }

        if (!groupDataToUpdate.isEmpty()) {
            fileGroupRepository.saveAll(groupDataToUpdate);
            fileGroupRepository.softCommitNoWaitFlush();
        }
        
        return groupDataToUpdate.size();
    }

    private SolrInputDocument createGroupDocument(Object[] groupData, 
                                                  Map<String, List<Object[]>> docTypesForGroup, 
                                                  Map<String, List<Object[]>> assocForGroup) {

        SolrInputDocument solrInputDocument = new SolrInputDocument();
        String groupId = (String)groupData[0];
        solrInputDocument.setField(FileGroupsFieldType.ID.getSolrName(), groupId);

        addField(solrInputDocument, NAME, groupData[13], SET);
        addField(solrInputDocument, GENERATED_NAME, groupData[9], SET);
        addField(solrInputDocument, TYPE, groupData[11], SET);
        addField(solrInputDocument, DELETED, groupData[3], SET);
        addField(solrInputDocument, DIRTY, groupData[4], SET);

        if (groupData[2] != null) {
            addField(solrInputDocument, CREATION_DATE, ((BigInteger) groupData[2]).longValue(), SET);
        } else {
            addField(solrInputDocument, CREATION_DATE, System.currentTimeMillis(), SET);
        }
        addField(solrInputDocument, UPDATE_DATE, System.currentTimeMillis(), SET);

        if (groupData[14] != null) {
            addField(solrInputDocument, NUM_OF_FILES, ((BigInteger) groupData[14]).longValue(), SET);
        }

        if (groupData[1] != null) {
            addField(solrInputDocument, COUNT_LAST_NAMING, ((BigInteger) groupData[1]).longValue(), SET);
        }

        if (groupData[7] != null) {
            addField(solrInputDocument, FIRST_MEMBER_ID, ((BigInteger) groupData[7]).longValue(), SET);
        }

        if (groupData[6] != null) {
            addField(solrInputDocument, FIRST_MEMBER_CONTENT_ID, ((BigInteger) groupData[6]).longValue(), SET);
        }

        addField(solrInputDocument, RAW_ANALYSIS_GROUP_ID, groupData[18], SET);
        addField(solrInputDocument, PARENT_GROUP_ID, groupData[21], SET);
        addField(solrInputDocument, HAS_SUBGROUPS, groupData[12], SET);

        addField(solrInputDocument, FST_PROP_NAMES, groupData[8], SET);
        addField(solrInputDocument, PROP_NAMES, groupData[16], SET);
        addField(solrInputDocument, PROP_NEW_NAMES, groupData[17], SET);
        addField(solrInputDocument, SND_PROP_NAMES, groupData[19], SET);
        addField(solrInputDocument, TRD_PROP_NAMES, groupData[20], SET);

        addField(solrInputDocument, FILE_NAMESTV, groupData[5], SET);
        addField(solrInputDocument, PARENT_DIRTV, groupData[15], SET);
        addField(solrInputDocument, GRANDPARENT_DIRTV, groupData[10], SET);

        List<String> associations = new ArrayList<>();

        if (docTypesForGroup.containsKey(groupId)) {
            docTypesForGroup.get(groupId).forEach(dta -> {
                boolean isDirty = (((boolean)dta[3]) || ((boolean)dta[7]));
                boolean isDeleted = (boolean)dta[2];

                AssociationEntity entity = new AssociationEntity();
                entity.setDocTypeId(((BigInteger)dta[6]).longValue());
                entity.setCreationDate(((BigInteger)dta[1]).longValue());
                entity.setImplicit(false);
                if (dta[8] != null) {
                    entity.setAssociationScopeId(((BigInteger)dta[8]).longValue());
                }
                String json = AssociationsService.convertFromAssociationEntity(entity);

                if (isDirty) {
                    if (isDeleted) {
                        createOperation(groupId, json, SolrFieldOp.REMOVE);
                    } else {
                        createOperation(groupId, json, SolrFieldOp.ADD);
                    }
                }

                if (!isDeleted) {
                    associations.add(json);
                }

            });
        }

        if (assocForGroup.containsKey(groupId)) {
            assocForGroup.get(groupId).forEach(association -> {
                boolean isDirty = (boolean)association[2];
                boolean isDeleted = (boolean)association[3];

                AssociationEntity entity = new AssociationEntity();
                entity.setCreationDate(((BigInteger)association[1]).longValue());
                if (association[4] != null) {
                    entity.setDocTypeId(((BigInteger) association[4]).longValue());
                } else if (association[5] != null) {
                    entity.setFileTagId(((BigInteger)association[5]).longValue());
                } else if (association[6] != null) {
                    entity.setSimpleBizListItemSid((String) association[6]);
                } else if (association[8] != null) {
                    entity.setFunctionalRoleId(((BigInteger) association[8]).longValue());
                }
                if (association[9] != null) {
                    entity.setAssociationScopeId(((BigInteger)association[9]).longValue());
                }
                entity.setImplicit(false);
                String json = AssociationsService.convertFromAssociationEntity(entity);

                if (isDirty) {
                    if (isDeleted) {
                        createOperation(groupId, json, SolrFieldOp.REMOVE);
                    } else {
                        createOperation(groupId, json, SolrFieldOp.ADD);
                    }
                }

                if (!isDeleted) {
                    associations.add(json);
                }

            });
        }

        if (!associations.isEmpty()) {
            addField(solrInputDocument, GROUP_ASSOCIATIONS, associations, SET);
        }
        
        return solrInputDocument;
    }
    
    private Map<String, List<Object[]>> toMapByGroup(List<Object[]> assoc, int indexId) {
        Map<String, List<Object[]>> res = new HashMap<>();
        
        for (Object[] item : assoc) {
            String groupId = (String)item[indexId];
            List<Object[]> groupAssociations = res.getOrDefault(groupId, new ArrayList<>());
            groupAssociations.add(item);
            res.put(groupId, groupAssociations);
        }
        
        return res;
    }

    private void addField(SolrInputDocument solrInputDocument, SolrCoreSchema type, Object value, SolrFieldOp operation) {
        if (value != null) {
            Map<String, Object> fieldModifier = new HashMap<>(1);
            fieldModifier.put(operation.getOpName(), value);
            solrInputDocument.setField(type.getSolrName(), fieldModifier);
        }
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

    private void createOperation(String groupId, String association, SolrFieldOp operation) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_GROUP_ASSOCIATIONS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyGroupAssociationOnFiles.GROUP_ID_PARAM, groupId);
        data.getOpData().put(ApplyGroupAssociationOnFiles.ASSOCIATION_PARAM, association);
        data.getOpData().put(ApplyGroupAssociationOnFiles.ACTION_PARAM, operation.name());
        data.setUserOperation(true);
        data.setPriority(ScheduledOperationPriority.LOW);
        data.setRetryLeft(retryForApplyGroupAssociationToFiles);
        scheduledOperationService.createNewOperation(data);
    }

    private static String GROUP_SQL = "select " +
            "f.id, " + // 00
            "f.count_on_last_naming, " + // 01
            "f.created_ondb, " + // 02
            "f.deleted, " + // 03
            "f.dirty, " + // 04
            "f.file_namestv, " + // 05
            "f.first_member_content_id, " +  // 06
            "f.first_member_id, " + // 07
            "f.first_proposed_names, " + // 08
            "f.generated_name, " + // 09
            "f.grand_parent_dirtv, " + // 10
            "f.group_type, " + // 11
            "f.has_subgroups, " + // 12
            "f.name, " + // 13
            "f.num_of_files, " + // 14
            "f.parent_dirtv, " + // 15
            "f.proposed_names, " + // 16
            "f.proposed_new_names, " + // 17
            "f.raw_analysis_group_id, " + // 18
            "f.second_proposed_names, " + // 19
            "f.third_proposed_names, " + // 20
            "f.parent_group_id " + // 21
            "from file_groups f "
            ;

    private static String DOC_TYPE_ASSOC_SQL = "SELECT " +
            "a.id, " + // 00
            "a.association_time_stamp, " + // 01
            "a.deleted, " + // 02
            "a.files_should_be_dirty, " + // 03
            "a.implicit, " + // 04
            "a.association_source, " + // 05
            "a.doc_type_id, " + // 06
            "a.dirty, " + // 07
            "a.doc_type_association_scope_id, " + // 08
            "a.group_id " + // 09
            "from doc_type_association a where a.group_id is not null and a.group_id in "
            ;

    private static String OTHER_ASSOC_SQL = "SELECT " +
            "a.id, " + // 00
            "a.association_time_stamp, " + // 01
            "a.dirty, " + // 02
            "a.deleted, " + // 03
            "a.doc_type_id, " + // 04
            "a.file_tag_id, " + // 05
            "a.simple_biz_list_item_sid, " + // 06
            "a.association_object_id, " + // 07
            "a.functional_role_id, " + // 08
            "a.tag_association_scope_id " + // 09
            "from tag_association a where a.association_type = 2 and a.association_object_id in "
            ;

}
