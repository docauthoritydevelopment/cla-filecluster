package com.cla.filecluster.upgrade.operations.fornax.solr7;

import com.cla.filecluster.upgrade.MigrateCollectionDataUpgradeOperation;
import com.cla.filecluster.upgrade.Upgrade;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Value;

/**
 * Responsible to migrate all cat folders document to be with be with Id as string and not as long
 */
@Upgrade(targetVersion = 9, step = 2)
public class CatFoldersMigrationOperation extends MigrateCollectionDataUpgradeOperation {

    private static final String SOURCE_COLLECTION_NAME = "CatFolders";
    private static final String INDEX_SORT_FIELD = "id_as_string";

    @Value("${solr7.migration.cat-folders.enabled:true}")
    private boolean enabled;

    @Override
    protected SolrInputDocument documentConverter(SolrDocument doc) {
        SolrInputDocument transformedDocument = new SolrInputDocument();
        doc.getFieldNames().stream()
                .filter(fieldName -> !fieldName.equals("id_as_string"))
                .forEach(fieldName ->
                        transformedDocument.setField(fieldName, transformFieldValue(doc, fieldName)));
        transformedDocument.removeField("_version_"); // Otherwise will fail on version conflict
        // Handle non 'stored' fields: numOfSubFolders, numOfDirectFiles, numOfAllFiles
        // TODO: implement if these are in use !!!
        return transformedDocument;
    }

    @Override
    protected Object transformFieldValue(SolrDocument doc, String fieldName) {
        return fieldName.equals("id") ? String.valueOf(doc.getFieldValue(fieldName)) : doc.getFieldValue(fieldName);
    }

    @Override
    protected String getIndexSortField() {
        return INDEX_SORT_FIELD;
    }

    // ---------------- Keep all common definitions below this line ---
    private static final String DESTINATION_COLLECTION_NAME = SOURCE_COLLECTION_NAME + "_2";
    private static final String COLLECTION_ALIAS = SOURCE_COLLECTION_NAME;

    @Value("${solr7.migration.collections.page-size:5000}")
    private int pageSize;

    @Value("${solr7.migration.collections.commit-after:true}")
    private boolean commitWhenDone;

    @Override
    protected boolean commitWhenDone() {
        return commitWhenDone;
    }

    @Override
    protected int getPageSize() {
        return pageSize;
    }

    @Override
    protected boolean isEnabled() {
        return enabled;
    }

    @Override
    protected String getSourceCollectionName() {
        return SOURCE_COLLECTION_NAME;
    }

    @Override
    protected String getDestinationCollectionName() {
        return DESTINATION_COLLECTION_NAME;
    }

    @Override
    protected String getCollectionliasName() {
        return COLLECTION_ALIAS;
    }
}
