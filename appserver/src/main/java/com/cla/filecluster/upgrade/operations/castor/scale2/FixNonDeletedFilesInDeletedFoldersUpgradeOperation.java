package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 5/28/2018
 */
@Upgrade(targetVersion = 5, step = 8)
public class FixNonDeletedFilesInDeletedFoldersUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.fix-del-files.enabled:false}")
    private boolean enabled;

    @Value("${migration.fix-del-files.page-size:50}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled){
            isJobFinished = true;
            logger.info("Upgrade step 8 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();
        try {
            List<Object[]> tables = em.createNativeQuery("show tables like 'doc_folder'").getResultList();
            if (tables == null || tables.isEmpty()) {
                isJobFinished = true;
                logger.info("Exiting migration, table doc_folder does not exist");
                return;
            }

            if (from == 0){

                List<Object> totals = em.createNativeQuery("SELECT COUNT(DISTINCT df.id) " +
                        "FROM doc_folder df, cla_files_md f " +
                        "WHERE df.deleted = 1 AND f.doc_folder_id = df.id AND f.deleted = 0")
                        .getResultList();

                if (totals.size() > 0) {
                    BigInteger total = (BigInteger)totals.get(0);
                    logger.info("Going to fix {} groups.", total.longValue());
                }
                else{
                    logger.info("Found no groups in need of fixing.");
                    isJobFinished = true;
                    return;
                }
            }

            List<Object> results = em.createNativeQuery("SELECT DISTINCT df.id " +
                    "FROM doc_folder df, cla_files_md f " +
                    "WHERE df.deleted = 1 AND f.doc_folder_id = df.id AND f.deleted = 0 " +
                    "AND df.id > " + from +
                    " LIMIT " + pageSize).getResultList();

            List<Long> docFolderIds = Lists.newLinkedList();

            long recordId = 0;
            for (Object result : results) {
                BigInteger dfIdBigInt = (BigInteger)result;
                recordId = dfIdBigInt.longValue();
                docFolderIds.add(recordId);
            }

            if (docFolderIds.size() > 0) {
                solrFileCatRepository.updateFieldByQuery(
                        CatFileFieldType.DELETED.getSolrName(), "true",
                        SolrFieldOp.SET,
                        SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.FOLDER_ID, SolrOperator.IN, docFolderIds)),
                        null, SolrCommitType.SOFT_NOWAIT);
            }

            opData.put(START_LIMIT, String.valueOf(recordId));

            if (docFolderIds.size() < pageSize) {
                logger.info("Files migration done for {} folders", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} folders", (from + pageSize));
            }
        } catch (Exception e) {
            logger.info("Deleted files migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }


    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
