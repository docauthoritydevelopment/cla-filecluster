package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 6/2/2019
 */
@Upgrade(targetVersion = 7, step = 6)
public class EmptyExtensionUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Value("${migration.empty-extension.enabled:true}")
    private boolean enabled;

    private boolean isJobFinished = false;

    @Override
    public void upgrade(TransactionStatus status) throws Exception {
        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 6 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();

        logger.info("Starting migration");

        try {
            List<String> filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.EXTENSION, SolrOperator.MISSING, null)));
            Integer cnged = solrFileCatRepository.updateFieldByQuery(
                    CatFileFieldType.EXTENSION.getSolrName(), FileNamingUtils.EMPTY_EXTENSION_VALUE, SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

            filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.EXTENSION, SolrOperator.EQ, "\"\"")));
            Integer cnged2 = solrFileCatRepository.updateFieldByQuery(
                    CatFileFieldType.EXTENSION.getSolrName(), FileNamingUtils.EMPTY_EXTENSION_VALUE, SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

            int total = (cnged == null ? 0 : cnged) + (cnged2 == null ? 0 : cnged2);

            opData.put(START_LIMIT, String.valueOf(total));
            logger.info("Files empty extension migration done for {} files", total);
            isJobFinished = true;

        } catch (Exception e) {
            logger.info("empty extension files migration issue", e);
            throw e;
        }
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
