package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.filecluster.service.operation.ApplyFolderAssociationOnFiles;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;

@Upgrade(targetVersion = 7, step = 2)
public class FolderAssociationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrCatFolderRepository solrCatFolderRepository;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private AssociationsService associationsService;

    @Value("${associations.apply-folder-associations-retry:5}")
    private int retryForApplyFolderAssociationToFiles;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Value("${migration.migrate-association-folders.enabled:true}")
    private boolean enabled;

    @Value("${migration.migrate-association-folders.page-size:5000}")
    private int pageSize;

    private boolean isJobFinished = false;

    private static String SQL_QUERY_PREFIX = "SELECT " +
            "a.id, " + // 00
            "a.association_time_stamp, " + // 01
            "a.deleted, " + // 02
            "a.files_should_be_dirty, " + // 03
            "a.implicit, " + // 04
            "a.solr_dirty, " + // 05
            "a.doc_type_id, " + // 06
            "a.file_tag_id, " + // 07
            "a.origin_folder_id, " + // 08
            "a.simple_biz_list_item_sid, " + // 09
            "a.doc_folder_id, " + // 10
            "a.functional_role_id, " + // 11
            "a.tag_association_scope_id, " + // 12
            "a.origin_depth_from_root " + // 13
            "from folder_tag_association a "
            ;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 2 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

       try {
           List<Object[]> tableExistsResult = em.createNativeQuery("show tables like 'folder_tag_association'").getResultList();
           if (tableExistsResult == null || tableExistsResult.isEmpty()) {
               isJobFinished = true;
               logger.info("exiting migration table folder_tag_association doesnt exist");
               return;
           }

           Query query = em.createNativeQuery("select max(id) from folder_tag_association");
           Object res = query.getSingleResult();
           BigInteger maxId = (BigInteger)res;

           query = em.createNativeQuery(SQL_QUERY_PREFIX +
                   "where a.id >= " + from + " and a.id < " + (from + pageSize));
           List<Object[]> results = query.getResultList();
           int rows = results.isEmpty() ? 0 : handleResults(results);

           opData.put(START_LIMIT, String.valueOf(from + pageSize));

           if (maxId == null || (from + pageSize) > maxId.longValue()) {
               isJobFinished = true;
           } else {
               logger.info("ended current migration cycle for {} records", (rows));
           }
       } catch (Exception e) {
            logger.info("folder association migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results) {
        int rows = 0;
        List<SolrInputDocument> foldersDataToUpdate = new LinkedList<>();
        logger.trace("Upgrade: page with {} results. First res. has {} columns",
                results.size(), results.isEmpty() ? 0 : results.get(0).length);

        List<Long> folderIds = new ArrayList<>();
        for (Object[] folderAssociationData : results) {
            BigInteger folderId = (BigInteger)folderAssociationData[10];
            folderIds.add(folderId.longValue());
        }

        com.cla.filecluster.repository.solr.query.Query query =
                com.cla.filecluster.repository.solr.query.Query.create()
                        .addField(CatFoldersFieldType.ID)
                        .addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                        .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID, SolrOperator.IN, folderIds))
                        .setRows(folderIds.size());

        List<SolrFolderEntity> folders = solrCatFolderRepository.find(query.build());
        Map<Long, SolrFolderEntity> foldersById = folders.stream()
                .collect(Collectors.toMap(SolrFolderEntity::getId, r -> r));

        for (Object[] folderAssociationData : results) {
            SolrInputDocument document = createDocument(folderAssociationData, foldersById);
            if (document != null) {
                foldersDataToUpdate.add(document);
            }
            rows++;
        }

        if (!foldersDataToUpdate.isEmpty()) {
            solrCatFolderRepository.saveAll(foldersDataToUpdate);
            solrCatFolderRepository.softCommitNoWaitFlush();
        }
        return rows;
    }

    private SolrInputDocument createDocument(Object[] folderAssociationData, Map<Long, SolrFolderEntity> foldersById) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger folderId = (BigInteger)folderAssociationData[10];

        SolrFolderEntity folderEntity = foldersById.get(folderId.longValue());
        if (folderEntity == null) {
            logger.debug("found association with no folder for id {}", folderId);
            return null;
        }

        solrInputDocument.setField(CatFoldersFieldType.ID.getSolrName(), folderId.longValue());

        AssociationEntity entity = new AssociationEntity();
        entity.setCreationDate(((BigInteger)folderAssociationData[1]).longValue());
        entity.setImplicit((boolean)folderAssociationData[4]);

        if (folderAssociationData[12] != null) {
            entity.setAssociationScopeId(((BigInteger)folderAssociationData[12]).longValue());
        }

        if (folderAssociationData[8] != null) {
            entity.setOriginFolderId(((BigInteger)folderAssociationData[8]).longValue());
        }

        if (folderAssociationData[13] != null) {
            entity.setOriginDepthFromRoot((Integer)folderAssociationData[13]);
        }

        if (folderAssociationData[6] != null) {
            entity.setDocTypeId(((BigInteger)folderAssociationData[6]).longValue());
        } else if (folderAssociationData[7] != null) {
            entity.setFileTagId(((BigInteger) folderAssociationData[7]).longValue());
        } else if (folderAssociationData[9] != null) {
            entity.setSimpleBizListItemSid((String) folderAssociationData[9]);
        } else if (folderAssociationData[11] != null) {
            entity.setFunctionalRoleId(((BigInteger) folderAssociationData[11]).longValue());
        }

        String json = AssociationsService.convertFromAssociationEntity(entity);

        // check if association entity was already added to file
        if (folderEntity.getAssociations() != null && folderEntity.getAssociations().contains(json)) {
            return null;
        }

        if (folderAssociationData[6] != null) {
            entity.setDocTypeId(((BigInteger)folderAssociationData[6]).longValue());
        } else if (folderAssociationData[7] != null) {
            entity.setFileTagId(((BigInteger)folderAssociationData[7]).longValue());
            FileTagDto tag = fileTagService.getFromCache(entity.getFileTagId());
            if (!tag.getType().isSingleValueTag() && !entity.getImplicit()) {
                // recreate identifier for files
                String solrKey = AssociationIdUtils.createScopedTagIdentifier(tag, null);
                String folderParentsInfo = entity.getOriginDepthFromRoot()+"."+entity.getOriginFolderId();
                updateAssociationByQuery(solrKey, folderParentsInfo, CatFileFieldType.SCOPED_TAGS, false);
                solrKey = AssociationIdUtils.createPriorityScopedTagIdentifier(tag, null, TagPriority.folder(entity.getOriginDepthFromRoot()));
                updateAssociationByQuery(solrKey, folderParentsInfo, CatFileFieldType.SCOPED_TAGS, true);
            }
        } else if (folderAssociationData[9] != null) {
            entity.setSimpleBizListItemSid((String)folderAssociationData[9]);
            // recreate identifier and populate field for files
            if (!entity.getImplicit()) {
                SimpleBizListItemDto bl = bizListItemService.getFromCache(entity.getSimpleBizListItemSid());
                String solrKey = AssociationIdUtils.createSimpleBizListItemIdentfier(bl, TagPriority.folder(entity.getOriginDepthFromRoot()));
                String folderParentsInfo = entity.getOriginDepthFromRoot() + "." + entity.getOriginFolderId();
                updateAssociationByQuery(solrKey, folderParentsInfo, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, true);
            }
        } else if (folderAssociationData[11] != null) {
            entity.setFunctionalRoleId(((BigInteger)folderAssociationData[11]).longValue());
            // recreate identifier and populate field for files
            if (!entity.getImplicit()) {
                FunctionalRoleDto fr = functionalRoleService.getFromCache(entity.getFunctionalRoleId());
                String solrKey = AssociationIdUtils.createFunctionalRoleIdentfier(fr, TagPriority.folder(entity.getOriginDepthFromRoot()));
                String folderParentsInfo = entity.getOriginDepthFromRoot() + "." + entity.getOriginFolderId();
                updateAssociationByQuery(solrKey, folderParentsInfo, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, true);
            }
        }

        // deleted association
        if (folderAssociationData[2] != null && ((boolean)folderAssociationData[2])) {
            if (!entity.getImplicit()) {
                createOperation(folderId.longValue(), json, SolrFieldOp.REMOVE);
            }
        } else { // undeleted association
            addField(solrInputDocument, CatFoldersFieldType.FOLDER_ASSOCIATIONS, json, SolrFieldOp.ADD);

            // association dirty
            if (((boolean)folderAssociationData[3]) || ((boolean)folderAssociationData[5])) {
                createOperation(folderId.longValue(), json, SolrFieldOp.ADD);
            }
        }

        return solrInputDocument;
    }

    private void updateAssociationByQuery(String solrKey, String folderParentsInfo, CatFileFieldType field, boolean addTag) {
        try {
            Criteria filterQuery = Criteria.create(FOLDER_IDS, SolrOperator.EQ, folderParentsInfo);
            associationsService.updateAssociationByQuery(filterQuery, addTag, solrKey, solrKey, field, false, SolrCommitType.SOFT_NOWAIT);
        } catch (Exception e) {
            logger.error("fail to remove solr key {} from {} for folder {}", solrKey, field, folderParentsInfo, e);
        }
    }

    private void addField(SolrInputDocument solrInputDocument, CatFoldersFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    private void createOperation(Long docFolderId, String association, SolrFieldOp operation) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_FOLDER_ASSOCIATIONS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyFolderAssociationOnFiles.DOC_FOLDER_ID_PARAM, String.valueOf(docFolderId));
        data.getOpData().put(ApplyFolderAssociationOnFiles.ASSOCIATION_PARAM, association);
        data.getOpData().put(ApplyFolderAssociationOnFiles.ACTION_PARAM, operation.name());
        data.setUserOperation(true);
        data.setRetryLeft(retryForApplyFolderAssociationToFiles);
        scheduledOperationService.createNewOperation(data);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }

}
