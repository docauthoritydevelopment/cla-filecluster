package com.cla.filecluster.upgrade.operations.draco;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.TransactionStatus;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 5/16/2019
 */
@Upgrade(targetVersion = 7, step = 10)
public class ContentBizListFindingsUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrContentMetadataRepository contentMetadataRepository;

    @Value("${migration.add-content-biz-list.enabled:true}")
    private boolean enabled;

    @Value("${migration.add-content-biz-list.page-size:10000}")
    private int pageSize;

    private boolean isJobFinished = false;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        if (!enabled) {
            isJobFinished = true;
            logger.info("Upgrade step 10 is disabled. Skipped");
            return;
        }

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("Starting migration");

        try {
            Query q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(
                    Criteria.create(CatFileFieldType.EXTRACTED_ENTITIES_IDS, SolrOperator.PRESENT, null)));
            q.addField(CatFileFieldType.ID);
            q.addField(CatFileFieldType.CONTENT_ID);
            q.addField(CatFileFieldType.EXTRACTED_ENTITIES_IDS);
            q.setRows(pageSize);
            q.setStart(from);
            q.addSort(CatFoldersFieldType.ID, Sort.Direction.ASC);
            List<SolrFileEntity> entities = solrFileCatRepository.find(q.build());

            Map<String, SolrInputDocument> contentIds = new HashMap<>();
            entities.forEach(file -> {
                try {
                    if (!contentIds.containsKey(file.getContentId())) {
                        SolrInputDocument document = new SolrInputDocument();
                        document.setField(ContentMetadataFieldType.ID.getSolrName(), file.getContentId());
                        addField(document, ContentMetadataFieldType.EXTRACTED_ENTITIES_IDS, file.getExtractedEntitiesIds());
                        contentIds.put(file.getContentId(), document);
                    }
                } catch (Exception e) {
                    logger.error("fail handling file {}", (file == null ? null : file.getId()), e);
                }
            });

            // get content objects from solr to check if exists
            q = Query.create();
            q.addFilterQuery(Query.create().addFilterWithCriteria(
                    Criteria.create(ContentMetadataFieldType.ID, SolrOperator.IN, contentIds.keySet())));
            q.addField(ContentMetadataFieldType.ID);
            q.setRows(contentIds.size());
            List<SolrContentMetadataEntity> existingContents = contentMetadataRepository.find(q.build());
            List<String> existingContentsIds = existingContents.stream().map(SolrContentMetadataEntity::getId).collect(Collectors.toList());

            // not all exists - remove missing from update
            if (existingContents.size() < contentIds.size()) {
                List<String> missingContents = new ArrayList<>();
                contentIds.keySet().forEach(contentId -> {
                    if (!existingContentsIds.contains(contentId)) {
                        missingContents.add(contentId);
                    }
                });
                missingContents.forEach(contentIds::remove);
                logger.warn("the following content ids came from files but the objects are missing {}", missingContents);
            }

            if (!contentIds.isEmpty()) {
                contentMetadataRepository.saveAll(contentIds.values());
                contentMetadataRepository.softCommitNoWaitFlush();
            }

            opData.put(START_LIMIT, String.valueOf(from+pageSize));

            if (entities.size() < pageSize) {
                logger.info("Files migration done for {} content biz list items", (from + pageSize));
                isJobFinished = true;
            } else {
                logger.info("Ended current migration cycle for {} content biz list items", (from + pageSize));
            }

        } catch (Exception e) {
            logger.info("content biz list items migration issue", e);
            throw e;
        }
    }

    private void addField(SolrInputDocument solrInputDocument, ContentMetadataFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return false;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }
}
