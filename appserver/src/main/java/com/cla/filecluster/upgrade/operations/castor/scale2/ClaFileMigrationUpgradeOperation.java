package com.cla.filecluster.upgrade.operations.scale2;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.upgrade.Upgrade;
import com.cla.filecluster.upgrade.UpgradeOperation;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.TransactionStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by: yael
 * Created on: 5/28/2018
 */
@Upgrade(targetVersion = 5, step = 1)
public class ClaFileMigrationUpgradeOperation extends UpgradeOperation {

    private static String START_LIMIT = "START_LIMIT";

    @Value("${migration.files.page-size:5000}")
    private int pageSize;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrContentMetadataRepository solrContentMetadataRepository;

    @Value("${migration.logging.catfile.limit:100}")   // 0 - no log; -1 - log all
    private int loggingItemsLimit;

    private boolean isJobFinished = false;

    private List<String> idsToDelete = new ArrayList<>();

    private int loggingItemsCounter = 0;

    private static String SQL_QUERY_PREFIX = "SELECT " +
            "f.id " +                               // 00
            ",f.acl_inheritance_type" +             // 01
            ",f.acl_signature" +                    // 02
            ",f.base_name" +                        // 03
            ",f.content_dirty" +                    // 04
            ",f.creation_date" +                    // 05
            ",f.deleted" +                          // 06
            ",f.deletion_date" +                    // 07
            ",f.dirty" +                            // 08
            ",f.dirty_wip" +                        // 09
            ",f.extension" +                        // 10
            ",f.file_name_hashed" +                 // 11
            ",f.fs_last_access" +                   // 12
            ",f.fs_last_modified" +                 // 13
            ",f.full_path" +                        // 14
            ",f.initial_scan_date" +                // 15
            ",f.last_metadata_change_date" +        // 16
            ",f.media_entity_id" +                  // 17
            ",f.mime" +                             // 18
            ",f.owner" +                            // 19
            ",f.root_folder_id" +                   // 20
            ",f.size_on_media" +                    // 21
            ",f.state" +                            // 22
            ",f.type" +                             // 23
            ",f.file_group_id" +                    // 24
            ",f.content_metadata_id" +              // 25
            ",f.doc_folder_id" +                    // 26
            ",f.user_group_id" +                    // 27
            ",c.analyze_hint" +                     // 28
            ",c.app_creation_date" +                // 29
            ",c.author" +                           // 30
            ",c.company" +                          // 31
            ",c.generating_app" +                   // 32
            ",c.items_countl1" +                    // 33
            ",c.items_countl2" +                    // 34
            ",c.items_countl3" +                    // 35
            ",c.keywords" +                         // 36
            ",c.subject" +                          // 37
            ",c.title" +                            // 38
            ",c.user_content_signature" +           // 39
            ",d.parents_info" +                     // 40
            ",rf.path as rf_path" +                 // 41
            ",rf.real_path as rf_real_path" +       // 42
            ",rf.media_type " +                     // 43
            ",d.deleted as folder_deleted "+        // 44
            ",f.package_top_file_id " +             // 45
            "FROM cla_files_md f " +
            "LEFT JOIN content_metadata c ON c.id = f.content_metadata_id " +
            "left join doc_folder d on d.id = f.doc_folder_id " +
            "left join root_folder rf on rf.id = f.root_folder_id " ;

    @SuppressWarnings("unchecked")
    @Override
    public void upgrade(TransactionStatus status) throws Exception {

        Map<String, String> opData = metadata.getOpData();
        Integer from = Integer.valueOf(opData.
                getOrDefault(START_LIMIT, "0"));

        logger.info("starting migration from {}", from);

        EntityManager em = emf.createEntityManager();

        try {
            List<Object[]> tableExistsResult = em.createNativeQuery("show tables like 'cla_files_md'").getResultList();
            if (tableExistsResult == null || tableExistsResult.isEmpty()) {
                isJobFinished = true;
                logger.info("exiting migration table cla_files_md doesnt exist");
                return;
            }

            Query query = em.createNativeQuery("select max(id) from cla_files_md");
            Object res = query.getSingleResult();
            BigInteger maxId = (BigInteger)res;

            query = em.createNativeQuery(SQL_QUERY_PREFIX +
                    "where f.id >= " + from + " and f.id < " + (from + pageSize));
            List<Object[]> results = query.getResultList();
            int rows = handleResults(results, false, null);

            opData.put(START_LIMIT, String.valueOf(from + pageSize));

            if (maxId == null || (from + pageSize) > maxId.longValue()) {
                logger.info("file migration done for {} records", (rows));
                com.cla.filecluster.repository.solr.query.Query querySolr =
                        com.cla.filecluster.repository.solr.query.Query.create()
                                .addFilterWithCriteria(Criteria.create(CatFileFieldType.MEDIA_TYPE, SolrOperator.MISSING, null))
                                .addField(CatFileFieldType.FILE_ID)
                                .addField(CatFileFieldType.ID)
                                .setRows(pageSize)
                                .setStart(0);
                                ;
                int sizeHandled;
                do {
                    QueryResponse queryResponse = solrFileCatRepository.runQuery(querySolr.build());
                    Map<Long, String> resSolr = new HashMap<>();
                    if (queryResponse != null && queryResponse.getResults() != null) {
                        logger.debug("look for strays returned {}", queryResponse);
                        sizeHandled = queryResponse.getResults().size();
                        queryResponse.getResults().forEach(r -> {
                            Object id = r.getFieldValue(CatFileFieldType.ID.getSolrName());
                            Object fileId = r.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());
                            if (fileId != null) {
                                logger.debug("file id to chk extra {}", fileId);
                                resSolr.put((Long) fileId, (String) id);
                            } else {
                                if (id != null) {
                                    String[] ids = ((String) id).split("!");
                                    logger.debug("file id to chk extra {}", ids[1]);
                                    resSolr.put(Long.parseLong(ids[1]), (String) id);
                                }
                            }
                        });
                    } else {
                        sizeHandled = 0;
                    }
                    if (!resSolr.isEmpty()) {
                        String idsCommaSeparated = resSolr.keySet().stream().map(Object::toString).collect(Collectors.joining(","));
                        query = em.createNativeQuery(SQL_QUERY_PREFIX +
                                "where f.id in (" + idsCommaSeparated + ")");
                        results = query.getResultList();
                        rows = handleResults(results, true, resSolr);
                        logger.info("file extra migration done for {} records", (rows));
                    }
                } while (sizeHandled == pageSize);
                isJobFinished = true;
            } else {
                logger.info("ended current migration cycle for {} records", (rows));
            }
        } catch (Exception e) {
            logger.info("file migration issue", e);
            throw e;
        } finally {
            em.close();
        }
    }

    private int handleResults(List<Object[]> results, boolean printDocs, Map<Long, String> actualIds) {
        int rows = 0;
        boolean isChange = false;
        List<SolrInputDocument> filesToUpdate = new LinkedList<>();
        logger.trace("Upgrade: page with {} results. First res. has {} columns",
                results.size(), results.isEmpty() ? 0 : results.get(0).length);

        for (Object[] fileData : results) {
            SolrInputDocument document = createDocument(fileData, actualIds);
            if (document != null) {
                filesToUpdate.add(document);

                if (logger.isTraceEnabled() && (loggingItemsLimit < 0 || loggingItemsCounter < loggingItemsLimit)) {
                    logger.trace("Upgrade CatFile: \n    row={}\n    doc={}",
                            IntStream.range(0, fileData.length)
                                    .mapToObj(i -> (i + ":" + (fileData[i] == null ? "null" : fileData[i].toString())))
                                    .collect(Collectors.joining(",")),
                            document.toString());
                    loggingItemsCounter++;
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Upgrade: got null document for row: {}",
                            IntStream.range(0, fileData.length)
                                    .mapToObj(i -> (i + ":" + (fileData[i] == null ? "null" : fileData[i].toString())))
                                    .collect(Collectors.joining(",")));
                }
            }
            rows++;
        }
        if (!filesToUpdate.isEmpty()) {
            if (printDocs) {
                logger.debug("Send to Solr the following docs: {}", filesToUpdate);
            }

            solrFileCatRepository.createCategoryFilesFromDocuments(filesToUpdate);
            isChange = true;
        }
        if (!idsToDelete.isEmpty()) {
            logger.debug("Upgrade: deleting files with ids {}", idsToDelete);
            solrFileCatRepository.deleteByCompositeIds(idsToDelete);
            isChange = true;
            idsToDelete = new ArrayList<>();
        }

        if (isChange) {
            solrFileCatRepository.softCommitNoWaitFlush();
        }
        return rows;
    }

    private SolrInputDocument createDocument(Object[] fileData, Map<Long, String> actualIds) {
        SolrInputDocument solrInputDocument = new SolrInputDocument();
        BigInteger fileId = (BigInteger)fileData[0];
        BigInteger contentId = (BigInteger)fileData[25];

        long cid;
        if (contentId == null) {
            cid = fileId.longValue();
        } else {
            cid = contentId.longValue();
        }

        String compositeId = FileCatCompositeId.of(cid, fileId.longValue()).toString();

        if (actualIds != null && actualIds.containsKey(fileId.longValue())) {
            String id = actualIds.get(fileId.longValue());
            if (!id.equals(compositeId)) {
                logger.warn("Calculated id {} different from solr id {} use solr", compositeId, id);
                idsToDelete.add(id);
                return null;
            }
        }

        solrInputDocument.setField(CatFileFieldType.ID.getSolrName(), compositeId);


        addField(solrInputDocument, CatFileFieldType.FILE_ID, fileId.longValue());

        if (contentId != null) {
            addField(solrInputDocument, CatFileFieldType.CONTENT_ID, contentId.longValue());
        }

        addField(solrInputDocument, CatFileFieldType.TYPE, fileData[23]);
        addField(solrInputDocument, CatFileFieldType.BASE_NAME, fileData[3]);

        if (fileData[43] != null) {
            addField(solrInputDocument, CatFileFieldType.MEDIA_TYPE, fileData[43]);
        } else {
            addField(solrInputDocument, CatFileFieldType.MEDIA_TYPE, 0);
        }

        String rfPath = (String)fileData[41];
        String rfRealPath = (String)fileData[42];

        String fullPath = (String)fileData[14];
        fullPath = FileNamingUtils.convertFromUnixToWindows(fullPath);
        fullPath = fullPath.startsWith(rfRealPath) ? fullPath.substring(rfRealPath.length()) : fullPath;
        fullPath = FileNamingUtils.convertPathToUniversalString(fullPath);
        addField(solrInputDocument, CatFileFieldType.FULLPATH, fullPath);
        String fullName = fullPath+(String)fileData[3];
        fullName = FileNamingUtils.convertFromUnixToWindows(fullName);
        //fullName = !fullName.contains("\\\\") ? fullName.replace("\\","\\\\") : fullName;
        addField(solrInputDocument, CatFileFieldType.FULL_NAME, fullName);
        
        if (fileData[21] != null) {
            BigInteger size = (BigInteger) fileData[21];
            addField(solrInputDocument, CatFileFieldType.SIZE, size.longValue());
        }
        addField(solrInputDocument, CatFileFieldType.MIME, fileData[18]);

        if (fileData[22] != null) {
            ClaFileState state = ClaFileState.valueOf((int)fileData[22]);
            addField(solrInputDocument, CatFileFieldType.PROCESSING_STATE, state.name());
        }

        addField(solrInputDocument, CatFileFieldType.LAST_MODIFIED, fileData[13]);
        addField(solrInputDocument, CatFileFieldType.LAST_ACCESS, fileData[12]);
        addField(solrInputDocument, CatFileFieldType.CREATION_DATE, fileData[5]);

        if (fileData[24] != null) {
            addField(solrInputDocument, CatFileFieldType.ANALYSIS_GROUP_ID, fileData[24]);
        }

        if (fileData[27] != null) {
            addField(solrInputDocument, CatFileFieldType.USER_GROUP_ID, fileData[27]);
        }

        if (fileData[26] != null) {
            BigInteger folderId = (BigInteger) fileData[26];
            addField(solrInputDocument, CatFileFieldType.FOLDER_ID, folderId.longValue());
        }

        addField(solrInputDocument, CatFileFieldType.OWNER, fileData[19]);

        String extension = (String)fileData[10];
        addField(solrInputDocument, CatFileFieldType.EXTENSION, StringUtils.lowerCase(extension));

        if (fileData[45] != null) {
            BigInteger packageTopFileId = (BigInteger) fileData[45];
            addField(solrInputDocument, CatFileFieldType.PACKAGE_TOP_FILE_ID, packageTopFileId.longValue());
        }

        String hash = ClaFile.getHashedFileName(fullName);
        addField(solrInputDocument, CatFileFieldType.FILE_NAME_HASHED, hash);

        BigInteger rfid = (BigInteger)fileData[20];
        addField(solrInputDocument, CatFileFieldType.ROOT_FOLDER_ID, rfid.longValue());

        if (fileData[17] != null) {
            String medEntId = (String) fileData[17];
            medEntId = medEntId.startsWith(rfPath) ? medEntId.substring(rfPath.length()) : medEntId;
            medEntId = medEntId.startsWith(rfRealPath) ? medEntId.substring(rfRealPath.length()) : medEntId;
            addField(solrInputDocument, CatFileFieldType.MEDIA_ENTITY_ID, medEntId);
        }

        if (fileData[7] != null) {
            addField(solrInputDocument, CatFileFieldType.DELETION_DATE, fileData[7]);
        }

        addField(solrInputDocument, CatFileFieldType.DELETED, fileData[6]);

        if (fileData[6] == null || !((boolean)fileData[6])) {
            if (fileData[44] != null && (boolean)fileData[44]) {
                logger.warn("Found an non-deleted file to a deleted folder - need to fix manually. Folder={}, File={} {}",
                        ((BigInteger) fileData[26]).longValue(), fileId, fullName);
            }
        }

        if (fileData[2] != null) {
            addField(solrInputDocument, CatFileFieldType.ACL_SIGNATURE, fileData[2]);
        }

        if (fileData[1] != null) {
            addField(solrInputDocument, CatFileFieldType.ACL_INHERITANCE_TYPE, fileData[1]);
        }

        if (fileData[15] != null) {
            addField(solrInputDocument, CatFileFieldType.INITIAL_SCAN_DATE, fileData[15]);
        }

        if (fileData[16] != null) {
            BigInteger metaDataCngDate = (BigInteger)fileData[16];
            addField(solrInputDocument, CatFileFieldType.LAST_METADATA_CNG_DATE, metaDataCngDate.longValue());
        }

        if (fileData[30] != null) {
            addField(solrInputDocument, CatFileFieldType.AUTHOR, fileData[30]);
        }
        if (fileData[31] != null) {
            addField(solrInputDocument, CatFileFieldType.COMPANY, fileData[31]);
        }
        if (fileData[37] != null) {
            addField(solrInputDocument, CatFileFieldType.SUBJECT, fileData[37]);
        }
        if (fileData[38] != null) {
            addField(solrInputDocument, CatFileFieldType.TITLE, fileData[38]);
        }
        if (fileData[36] != null) {
            addField(solrInputDocument, CatFileFieldType.KEYWORDS, fileData[36]);
        }
        if (fileData[32] != null) {
            addField(solrInputDocument, CatFileFieldType.GENERATING_APP, fileData[32]);
        }
        if (fileData[39] != null) {
            addField(solrInputDocument, CatFileFieldType.USER_CONTENT_SIGNATURE, fileData[39]);
        }
        if (fileData[28] != null) {
            AnalyzeHint hint = AnalyzeHint.valueOf((Integer)fileData[28]);
            addField(solrInputDocument, CatFileFieldType.ANALYSIS_HINT, hint.name());
        }
        if (fileData[29] != null) {
            addField(solrInputDocument, CatFileFieldType.APP_CREATION_DATE, fileData[29]);
        }
        if (fileData[33] != null) {
            BigInteger ic1 = (BigInteger)fileData[33];
            addField(solrInputDocument, CatFileFieldType.ITEM_COUNT_1, ic1.longValue());
        }
        if (fileData[34] != null) {
            BigInteger ic2 = (BigInteger)fileData[34];
            addField(solrInputDocument, CatFileFieldType.ITEM_COUNT_2, ic2.longValue());
        }
        if (fileData[35] != null) {
            BigInteger ic3 = (BigInteger)fileData[35];
            addField(solrInputDocument, CatFileFieldType.ITEM_COUNT_3, ic3.longValue());
        }

        if (fileData[40] != null) {
            String data = (String)fileData[40];
            String[] parents = data.split("x");
            addField(solrInputDocument, CatFileFieldType.FOLDER_IDS, parents);
        }

        if (solrInputDocument.getField(CatFileFieldType.MEDIA_TYPE.getSolrName()) == null) {
            logger.warn("Something is wrong with this record {} for fileData {}", solrInputDocument, fileData);
        } else if (solrInputDocument.getField(CatFileFieldType.ID.getSolrName()) == null) {
            logger.warn("Something is wrong with this record {} for fileData {}", solrInputDocument, fileData);
        } else if (solrInputDocument.getField(CatFileFieldType.FILE_ID.getSolrName()) == null) {
            logger.warn("Something is wrong with this record {} for fileData {}", solrInputDocument, fileData);
        }

        return solrInputDocument;
    }

    private void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(SolrFieldOp.SET.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public boolean isIterative() {
        return true;
    }

    @Override
    public boolean isJobFinished() {
        return isJobFinished;
    }

    @Override
    public boolean operatesOnSolr() {
        return true;
    }


}
