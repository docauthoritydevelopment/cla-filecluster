package com.cla.filecluster.upgrade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.TransactionStatus;

/**
 * Upgrade operation. Will be automatically loaded and called by the UpgradeService.
 */
public abstract class UpgradeOperation {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected ApplicationContext applicationContext;

    protected UpgradeOperationMetadata metadata;

    /**
     * Whether the current operation is iterative.
     * If yes, the infrastructure will call this operation time after time until isJobFinished methods returns true.
     *
     * @return true if the current operation is iterative.
     */
    public boolean isIterative(){
        return false;
    }

    /**
     * Whether iterative operation has finished its job.
     *
     * @return true if the job is finished
     */
    public boolean isJobFinished(){
        return true;
    }

    /**
     * Whether this upgrade operation do modifications on solr
     * @return true in case it does
     */
    public boolean operatesOnSolr(){
        return false;
    }

    /**
     * Upgrade operation logic
     *
     * @param status        transaction status, may be used to perform flush inside the method
     * @throws Exception    in case of error
     */
    public abstract void upgrade(TransactionStatus status) throws Exception;

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public void setMetadata(UpgradeOperationMetadata metadata) {
        this.metadata = metadata;
    }

    protected void beforeIterationStart() {
        // Do nothing, meant for extension to show info before it starts
    }
}
