package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.domain.dto.security.UserType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.service.security.UserService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AuthenticationHelper {

    private final static Logger logger = LoggerFactory.getLogger(AuthenticationHelper.class);

    public static Authentication getCurrentAuthentication(){
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static void setAuthentication(Authentication auth){
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    public static void authenticateAs(UserDto userDto){
        setAuthentication(new UsernamePasswordAuthenticationToken(new ClaUserDetails(userDto), null));
    }

    public static void doWithAuthentication(String principle, boolean replace, Runnable operation, UserService userService){
        boolean authenticationWasReplaced = false;
        Authentication previousAuth = null;
        try {
            Pair<Boolean, Authentication> prepareResult = prepareRun(principle, replace, userService);
            authenticationWasReplaced = prepareResult.getKey();
            previousAuth = prepareResult.getValue();

            // proceed with the call of the original method
            operation.run();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected Exception:", e); // TODO Itai: we may want to enable this method to throw checked exceptions
        } finally {
            // return the authentication to the previous state
            if (authenticationWasReplaced) {
                logger.trace("Auto authentication returns to {}",
                        previousAuth == null ? "unauthenticated state" : previousAuth.getName());
                AuthenticationHelper.setAuthentication(previousAuth);
            }
        }
    }

    public static <T> T doWithAuthentication(String principle, boolean replace, Callable<T> operation, UserService userService){
        boolean authenticationWasReplaced = false;
        Authentication previousAuth = null;
        T operationResult;

        try {
            Pair<Boolean, Authentication> prepareResult = prepareRun(principle, replace, userService);
            authenticationWasReplaced = prepareResult.getKey();
            previousAuth = prepareResult.getValue();

            // proceed with the call of the original method
            operationResult = operation.call();
        } catch (Exception e) {
            throw new RuntimeException("Unexpected Exception:", e); // TODO Itai: we may want to enable this method to throw checked exceptions
        } finally {
            // return the authentication to the previous state
            if (authenticationWasReplaced) {
                logger.trace("Auto authentication returns to {}",
                        previousAuth == null ? "unauthenticated state" : previousAuth.getName());
                AuthenticationHelper.setAuthentication(previousAuth);
            }
        }

        return operationResult;
    }

    private static Pair<Boolean, Authentication> prepareRun(String principle, boolean replace, UserService userService) {
        boolean authenticationWasReplaced = false;
        Authentication previousAuth = AuthenticationHelper.getCurrentAuthentication();

        // if there is already an authenticated user and the annotation
        // doesn't call for replacement of the existing authentication - skip
        if (previousAuth == null || Strings.isNullOrEmpty(previousAuth.getName()) || replace) {
            if (logger.isWarnEnabled() && Thread.currentThread().getName().startsWith("http")) {
                logger.warn("Unexpected authentication replacement for a REST worker thread. Call stack:\n{}", getStackTraceDump());
            }

            UserDto userDto;
            if (AutoAuthenticate.ADMIN.equals(principle)) {
                userDto = new UserDto();
                userDto.setUserType(UserType.SYSTEM);
                userDto.setUsername("system-auto");
                if (logger.isTraceEnabled()) {
                    logger.trace("Replacing auto authentication: prevAuth={} replace={} trace:\n{}",
                            (previousAuth == null ? "null" : previousAuth.getName()),
                            replace,
                            getStackTraceDump());
                }
            }
            else
            if (AutoAuthenticate.SCHEDULE.equals(principle)) {
                userDto = userService.getScheduleUserDto();
            }
            else {
                userDto = userService.getUserDtoByNameWithTokens(principle);
            }

            // create authentication on the current thread
            if (userDto != null) {
                logger.trace("Auto authentication with username {}.", principle);
                authenticationWasReplaced = true;
                AuthenticationHelper.authenticateAs(userDto);
            }
            else {
                logger.warn("Auto authentication with username {} failed, could not find user with this name.",
                        principle);
            }
        }
        else{
            logger.trace("Proceeding with existing authentication as {}.", previousAuth.getName());
        }

        return Pair.of(authenticationWasReplaced, previousAuth);
    }

    private static String getStackTraceDump() {
        return Stream.of(Thread.currentThread().getStackTrace())
                .filter(ste-> ste.getClassName().startsWith("com.cla."))    // Display only relevant trace items
                .map(ste -> ste.getClassName() + "(" + ste.getMethodName() + ") " + ste.getFileName() + ":" + ste.getLineNumber())
                .collect(Collectors.joining("\n\t"));
    }
}
