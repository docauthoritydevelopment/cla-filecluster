package com.cla.filecluster.config;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import com.cla.common.media.files.ooxml.ClaOOXMLParser;
import com.cla.common.media.files.pdf.ClaNativePdfExtractor;
import com.cla.common.media.files.pdf.ClaPdfExtractor;
import com.cla.common.media.files.word.ClaWord6Extractor;
import com.cla.common.media.files.word.ClaWordExtractor;
import com.cla.common.media.files.word.ClaWordXmlExtractor;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.filecluster.batch.writer.analyze.*;
import com.cla.filecluster.batch.writer.ingest.*;
import com.cla.filecluster.batch.writer.map.*;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.jobmanager.service.TaskManagerService;
import com.cla.filecluster.pvs.PvsOneWriteManyReadsScaler;
import com.cla.filecluster.pvs.PvsScaler;
import com.cla.filecluster.pvs.PvsShifter;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.repository.jpa.pvs.PvsTrackRepository;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.scripts.SolrCloudConfigurer;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.analyze.NewFileGroupsCache;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.filecluster.util.executors.ClientThreadBlockingExecutor;
import com.cla.filecluster.util.executors.ThreadPoolBlockingExecutor;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MediaType;
import org.apache.tika.parser.CompositeParser;
import org.apache.tika.parser.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.*;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

@Configuration
public class AppConfig {
    private static final Logger logger = LoggerFactory.getLogger(AppConfig.class);

    @Value("${utilBlockingExecutor.poolSize:10}")
    private int utilBlockingExecutorPoolSize;

    @Value("${utilBlockingExecutor.QueueSize:15}")
    private int utilBlockingExecutorQueueSize;

    @Value("${scanBlockingExecutor.poolSize:10}")
    private int scanBlockingExecutorPoolSize;

    @Value("${scanBlockingExecutor.QueueSize:15}")
    private int scanBlockingExecutorQueueSize;

    @Value("${startScanBlockingExecutor.poolSize:10}")
    private int startScanBlockingExecutorPoolSize;

    @Value("${startScanBlockingExecutor.QueueSize:15}")
    private int startScanBlockingExecutorQueueSize;

    @Value("${consumeScanBlockingExecutor.poolSize:10}")
    private int consumeScanBlockingExecutorPoolSize;

    @Value("${consumeScanBlockingExecutor.QueueSize:15}")
    private int consumeScanBlockingExecutorQueueSize;

    @Value("${ingestBlockingExecutor.poolSize:10}")
    private int ingestBlockingExecutorPoolSize;

    @Value("${ingestBlockingExecutor.QueueSize:15}")
    private int ingestBlockingExecutorQueueSize;

    @Value("${zipBlockingExecutor.QueueSize:15}")
    private int packageIngestBlockingExecutorQueueSize;

    @Value("${analyzeBlockingExecutor.poolSize:10}")
    private int packageIngestBlockingExecutorPoolSize;

    @Value("${analyzeBlockingExecutor.poolSize:10}")
    private int analyzeBlockingExecutorPoolSize;

    @Value("${analyzeBlockingExecutor.queueSize:15}")
    private int analyzeBlockingExecutorQueueSize;

    @Value("${finalizeBlockingExecutor.poolSize:10}")
    private int finalizeBlockingExecutorPoolSize;

    @Value("${finalizeBlockingExecutor.queueSize:15}")
    private int finalizeBlockingExecutorQueueSize;

    @Value("${ingest.others.tikaConfig:}")
    private String tikeConfigFilename;

    @Value("${threadPool.idleTimeBeforeShutdown:1}")
    private long idleTimeBeforeShutdown;

    @Value("${maxStringLengthToExtract:50000}")
    private int maxStringLength;

    @Value("${pv.dbOperationsRetryCount:5}")
    private int dbOperationsRetryCount;

    @Value("${pv.dbOperationsSleepSec:2}")
    private int dbOperationsSleepSec;

    @Value("${useThreadPoolExecutors:true}")
    private boolean useThreadPoolExecutors;

    @Value("${alternative.name.override:false}")
    private boolean overrideWithAlternativeName;

    @Value("${ingester.excel.xmlFilteringSizeThreshold:1000000}")
    private int xmlFilteringSizeThreshold;

    @Value("${ingester.excel.xmlFilteringMaxObjects:1000000}")
    private int xmlFilteringMaxObjects;

    @Value(("${file.support-additional-xls:true}"))
    private boolean supportAdditionalExcelFiles;

    @Value("${task-scheduler.thread-pool.core-size:10}")
    private int taskSchedulerThreadPoolCoreSize;

    @Value("${inget.batch-content.size:100}")
    private int ingestContentBatchSize;

    @Value("${ingest.batch.task-results.flush-size:1000}")
    private int flushSize;

    @Value("${ingest.commit-extraction-content-within-ms:60000}")
    private int commitExtractionContentWithinMs;

    @Value("${ingest.commit-histogram-within-ms:60000}")
    private int commitHistogramWithinMs;

    @Value("${ingest.batch.solr-flush-partition-size:100}")
    private int solrFlushInnerPartitionSize;

    @Value("${ingest.batch.flush-partition-concurrency:50}")
    private int flushPartitionConcurrency;

    @Value("${ingest.batch.db-flush-partition-size:1000}")
    private int dbFlushPartitionSize;

    @Value("${ingest.batch-store-populator.pool-size:10}")
    private int ingestBatchStorePopulatorExecutorPoolSize;

    @Value("${ingest.batch-store-populator.queue-size:10}")
    private int ingestBatchStorePopulatorExecutorQueueSize;

    @Value("${solr.pvs-scaler.count-threshold:2000000}")
    private int pvsScalerCountThreshold;

    @Value("${solr.pvs-scaler.num-of-shards:1}")
    private int pvsScalerNumOfShards;

    @Value("${solr.pvs-scaler.perform-collection-optimize:true}")
    private boolean performCollectionOptimize;

    @Value("${solr.pvs-scaler.allow-read-write:true}")
    private boolean allowReadAndWrite;

    @PostConstruct
    private void init() {
        if (!useThreadPoolExecutors) {
            logger.warn("==============================================================================");
            logger.warn("| THE APPLICATION IS SINGLE THREADED. THIS SHOULD BE USED ONLY FOR DEBUG !!! |");
            logger.warn("==============================================================================");
        }

        if (supportAdditionalExcelFiles) {
            FileTypeUtils.addExtension("xlt", FileType.EXCEL);
            FileTypeUtils.addExtension("xlsm", FileType.EXCEL);
            FileTypeUtils.addExtension("xltx", FileType.EXCEL);
        }
    }


    @Bean
    @Profile("!test-rest")
    public RestTemplate fileClusterRestTemplate() {
        return new RestTemplate();
    }


    @Bean
    public BlockingExecutor ingestBatchStorePopulatorExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("ingest-batch-loader", ingestBatchStorePopulatorExecutorPoolSize, ingestBatchStorePopulatorExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor scanBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("scan", scanBlockingExecutorPoolSize, scanBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingQueue<DAEvent> eventQueue() {
        return new LinkedBlockingQueue<>();
    }

    @Bean
    public BlockingQueue<ActionPluginRequest> actionPluginRequestQueue() {
        return new LinkedBlockingQueue<>();
    }


    @Bean
    public BlockingExecutor scanResponsesBlockingExecutor() {
        //To enable parallel scan consumption, the first step is to increase the pool size from 1
        return new ThreadPoolBlockingExecutor("scan", consumeScanBlockingExecutorPoolSize, consumeScanBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
    }

    @Bean
    public BlockingExecutor startScanBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("Start scan", startScanBlockingExecutorPoolSize, startScanBlockingExecutorQueueSize, idleTimeBeforeShutdown, false);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public ExecutorService messagingBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("Messaging", 50, 50, idleTimeBeforeShutdown, false);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor ingestBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("ingest", ingestBlockingExecutorPoolSize, ingestBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor packageIngestBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("packageIngest", packageIngestBlockingExecutorPoolSize, packageIngestBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor analyzeBlockingExecutor() {
        if (useThreadPoolExecutors) {
            logger.info("Create Analyze Blocking Executor");
            return new ThreadPoolBlockingExecutor("analyze", analyzeBlockingExecutorPoolSize, analyzeBlockingExecutorQueueSize, idleTimeBeforeShutdown, false);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor finalizeBlockingExecutor() {
        if (useThreadPoolExecutors) {
            logger.info("Create Finalize Blocking Executor");
            return new ThreadPoolBlockingExecutor("finalize", finalizeBlockingExecutorPoolSize, finalizeBlockingExecutorQueueSize, idleTimeBeforeShutdown, false);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor utilBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("util", utilBlockingExecutorPoolSize, utilBlockingExecutorQueueSize, idleTimeBeforeShutdown, true);
        }
        return new ClientThreadBlockingExecutor();
    }

    @Bean
    public BlockingExecutor extractionBlockingExecutor() {
        if (useThreadPoolExecutors) {
            return new ThreadPoolBlockingExecutor("extraction", utilBlockingExecutorPoolSize, utilBlockingExecutorQueueSize, idleTimeBeforeShutdown, false);
        }
        return new ClientThreadBlockingExecutor();
    }

    @SuppressWarnings("unused") // this Bean is found by ScheduledAnnotationBeanPostProcessor.resolveSchedulerBean()
    @Bean
    public TaskScheduler defaultScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setDaemon(true);
        taskScheduler.setThreadNamePrefix("da-scheduler");
        taskScheduler.setPoolSize(taskSchedulerThreadPoolCoreSize);
        return taskScheduler;
    }

    @Bean
    public Tika tika() {
        TikaConfig tikaConfig = TikaConfig.getDefaultConfig();
        if (tikeConfigFilename != null && !tikeConfigFilename.isEmpty()) {
            try {
                tikaConfig = new TikaConfig(tikeConfigFilename);
                logger.info("TikaConfig overridden with elements in file {}", tikeConfigFilename);
            } catch (Exception e) {
                logger.warn("Exception while creating custom TikaConfig from file {}. {}", tikeConfigFilename, e.getMessage());
            }
        }

        CompositeParser parser = (CompositeParser) tikaConfig.getParser();
        Map<MediaType, Parser> parsers = parser.getParsers();

        Set<MediaType> supportedTypes = ClaOOXMLParser.getSupportedTypes();
        ClaOOXMLParser customOOXMLParser = new ClaOOXMLParser();
        customOOXMLParser.setTotalObjectsLimit(xmlFilteringMaxObjects);
        customOOXMLParser.setSizeThreshold(xmlFilteringSizeThreshold);

        supportedTypes.forEach(type -> parsers.put(type, customOOXMLParser));

        parser.setParsers(parsers);

        final Tika tika = new Tika(tikaConfig);
        tika.setMaxStringLength(maxStringLength);
        return tika;
    }


    @Bean
    public RetryTemplate dbRetryTemplate() {
        final RetryTemplate rt = new RetryTemplate();
        final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(dbOperationsRetryCount);
        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(1000L * dbOperationsSleepSec);
        backOffPolicy.setMaxInterval(600000L);
        rt.setRetryPolicy(retryPolicy);
        rt.setBackOffPolicy(backOffPolicy);
        return rt;
    }


//    @Bean
//    @Lazy(false)
//    public AnnotationMBeanExporter exporter() {
//        return new AnnotationMBeanExporter();
//    }


    ////////////////////////////////////////////////////////////////
    @Bean
    @Scope("prototype")
    public ClaWord6Extractor claWord6Extractor() {
        return new ClaWord6Extractor();
    }

    @Bean
    @Scope("prototype")
    public ClaWordXmlExtractor claWordXmlExtractor() {
        return new ClaWordXmlExtractor();
    }

    @Bean
    public ClaWordExtractor claWordExtractor() {
        return new ClaWordExtractor() {
            @Override
            protected ClaWord6Extractor createClaWord6Extractor() {
                return claWord6Extractor();
            }

            @Override
            protected ClaWordXmlExtractor createClaWordXmlExtractor() {
                return claWordXmlExtractor();
            }

        };
    }

    @Bean
    @Scope("prototype")
    public ClaNativePdfExtractor claNativePdfExtractor() {
        return new ClaNativePdfExtractor();
    }

    @Bean
    public ClaPdfExtractor claPdfExtractor() {
        return new ClaPdfExtractor() {
            @Override
            protected ClaNativePdfExtractor createClaPdfExtractor() {
                return claNativePdfExtractor();
            }

        };
    }

    @Bean
    public ScriptEngine javaScriptEngine() {
        return new ScriptEngineManager().getEngineByName("nashorn");
    }

    @Bean
    public RestTemplate fileProcessorRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Map<MapBatchTaskType, MapBatchTaskExecutor> mapBatchExecutorsRegistry(TaskManagerService taskManagerService,
                                                                                 KeyValueDatabaseRepository kvdbRepo,
                                                                                 FileCatService fileCatService,
                                                                                 FileCrawlerExecutionDetailsService runService,
                                                                                 DocFolderService docFolderService,
                                                                                 FileGroupService fileGroupService,
                                                                                 SolrContentMetadataRepository contentMetadataRepository) {
        Map<MapBatchTaskType, MapBatchTaskExecutor> batchExecutorsRegistry = Maps.newConcurrentMap();
        batchExecutorsRegistry.put(MapBatchTaskType.FILE_FOLDER, new MapFileFolderBatchExecutor(kvdbRepo, fileCatService, docFolderService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(MapBatchTaskType.MARK_FILES_DELETED, new MapMarkFilesDeletedBatchExecutor(fileCatService, runService, fileGroupService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(MapBatchTaskType.CONTENT_METADATA_FILE_COUNT_UPDATE, new ContentMetadataFileCountUpdater(contentMetadataRepository, fileCatService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        return batchExecutorsRegistry;
    }

    @Bean
    public Map<IngestBatchTaskType, IngestBatchTaskExecutor> ingestBatchExecutorsRegistry(SequenceIdGenerator sequenceIdGenerator,
                                                                                          DBTemplateUtils dbTemplateUtils,
                                                                                          FileDTOSearchRepository fileDTOSearchRepository,
                                                                                          ContentMetadataService contentMetadataService,
                                                                                          SolrGrpHelperRepository solrGrpHelperRepository,
                                                                                          SolrPvEntriesIngesterRepository solrPvEntriesIngesterRepository,
                                                                                          SolrAnalysisDataRepository solrAnalysisDataRepository,
                                                                                          FileGroupService fileGroupService,
                                                                                          FileCatService fileCatService) {
        Map<IngestBatchTaskType, IngestBatchTaskExecutor> batchExecutorsRegistry = Maps.newConcurrentMap();
        batchExecutorsRegistry.put(IngestBatchTaskType.DELETED_FILES_BY_ID, new DeletedFileCatBatchExecutor(fileCatService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.MARK_DIRTY_POPULATION_FLAG, new MarkDirtyPopulationFlagBatchExecutor(contentMetadataService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.CLEAR_SAMPLE_FILE_ID, new ClearSampleFileBatchExecutor(contentMetadataService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.MARK_DIRTY_GROUP, new MarkDirtyGroupBatchExecutor(fileGroupService, dbFlushPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.FILE_ENTITY, new FileEntitiesBatchExecutor(fileCatService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.CAT_FILE, new CatFileBatchExecutor(fileCatService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.CONTENT_ENTITY, new SolrContentEntityBatchExecutor(fileDTOSearchRepository, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.CONTENT_METADATA, new ContentMetadataBatchTaskExecutor(contentMetadataService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.GRP_HELPER, new GrpHelperBatchExecutor(solrGrpHelperRepository, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.HISTOGRAM, new HistogramBatchExecutor(solrPvEntriesIngesterRepository, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.INGESTION_ERROR, new ErrorProcessedFileBatchExecutor(sequenceIdGenerator, dbTemplateUtils, dbFlushPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.PV_ANALYSIS_DATA, new PvAnalysisDataBatchExecutor(solrAnalysisDataRepository, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(IngestBatchTaskType.EMPTY_FILES, new EmptyFileBatchExecutor(fileCatService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        return batchExecutorsRegistry;
    }

    @Bean
    public Map<AnalyzeBatchTaskType, AnalyzeBatchTaskExecutor> analyzeBatchExecutorsRegistry(JobManagerService jobManagerService,
                                                                                             SolrFileGroupRepository fileGroupRepository,
                                                                                             SolrTaskService solrTaskService,
                                                                                             FilesDataPersistenceService filesDataPersistenceService,
                                                                                             SolrContentMetadataRepository solrContentMetadataRepository,
                                                                                             NewFileGroupsCache newFileGroupsCache) {
        Map<AnalyzeBatchTaskType, AnalyzeBatchTaskExecutor> batchExecutorsRegistry = Maps.newConcurrentMap();
        batchExecutorsRegistry.put(AnalyzeBatchTaskType.FAILED_TASKS, new AnalyzeFailedTasksBatchExecutor(solrTaskService, dbFlushPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(AnalyzeBatchTaskType.FILE_CONTENT_TO_ANALYZE, new ContentFilesToAnalyzeBatchExecutor(filesDataPersistenceService, solrFlushInnerPartitionSize, flushPartitionConcurrency));
        batchExecutorsRegistry.put(AnalyzeBatchTaskType.FAILED_TASKS_COUNTER, new AnalyzeJobFailCounterBatchExecutor(dbFlushPartitionSize, flushPartitionConcurrency, jobManagerService));
        batchExecutorsRegistry.put(AnalyzeBatchTaskType.SUCCESS_TASKS_COUNTER, new AnalyzeJobSuccessCounterBatchExecutor(dbFlushPartitionSize, flushPartitionConcurrency, jobManagerService));
        batchExecutorsRegistry.put(AnalyzeBatchTaskType.NEW_FILE_GROUP, new GroupCreationExecutor(dbFlushPartitionSize, flushPartitionConcurrency, newFileGroupsCache, fileGroupRepository));
        batchExecutorsRegistry.put(AnalyzeBatchTaskType.CONTENT_GROUP_ASSIGNMENT, new ContentGroupUpdateExecutor(solrFlushInnerPartitionSize, flushPartitionConcurrency, solrContentMetadataRepository));
        return batchExecutorsRegistry;
    }

    @Bean(destroyMethod = "close")
    public SolrCloudConfigurer solrCloudConfigurer(SolrClientFactory solrClientFactory) {
        return new SolrCloudConfigurer(solrClientFactory.generalSolrClient().getUnderyingClient());
    }

    @Bean
    @ConditionalOnProperty(value = "solr.pvs-scaler.enabled", havingValue = "true", matchIfMissing = true)
    public PvsShifter pvsShifter(SolrCloudConfigurer solrCloudConfigurer) {
        return new PvsShifter(solrCloudConfigurer, performCollectionOptimize, allowReadAndWrite);
    }

    @Bean
    @ConditionalOnProperty(value = "solr.pvs-scaler.enabled", havingValue = "true", matchIfMissing = true)
    public PvsScaler pvsOneWriteManyReadsScaler(SolrCloudConfigurer solrCloudConfigurer, PvsShifter pvsShifter,
                                                PvsTrackRepository pvsTrackRepository) {
        return new PvsOneWriteManyReadsScaler(solrCloudConfigurer, pvsShifter, pvsTrackRepository, pvsScalerCountThreshold, pvsScalerNumOfShards);
    }

    @Bean
    @ConditionalOnProperty(value = "solr.pvs-scaler.enabled", havingValue = "false", matchIfMissing = false)
    public PvsScaler noPvsScaler() {
        return new PvsScaler() {
            @Override
            protected void initialize() {
                // Do nothing
            }

            @Override
            protected void scalePvs() {
                // do nothing
            }

            @Override
            protected boolean shouldScale() {
                return false;
            }
        };
    }
}