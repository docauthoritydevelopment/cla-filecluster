package com.cla.filecluster.config.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.stereotype.Service;

import javax.naming.NamingEnumeration;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Created by Itai Marko.
 */
@Service
public class LdapUtils {

    private static final Logger logger = LoggerFactory.getLogger(LdapUtils.class);

    @Value("${ldap.rootdse.attributes.names:}")
    private String ldapRootDSEAttributesNames;

    @Value("${ldap.ad.rootdse.attributes.names:}")
    private String adRootDSEAttributesNames;

    // TODO Itai: return some RootDSE object (need to create once we get more knowledge from Jerusalem)
    public void logRootDSE(LdapContextSource contextSource) {

        try {
            // get a union of the attributes lists of both flavours (standard LDAP and Active Directory)
            String[] ldapRootDSEAttNames = ldapRootDSEAttributesNames.split(",");
            String[] adRootDSEAttNames = adRootDSEAttributesNames.split(",");
            String[] allRootDSEAttNames =
                    Stream.concat(Arrays.stream(ldapRootDSEAttNames), Arrays.stream(adRootDSEAttNames)).
                            distinct().toArray(String[]::new);

            SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            SpringSecurityLdapTemplate template = new SpringSecurityLdapTemplate(contextSource);
            template.setSearchControls(searchControls);
            template.setIgnorePartialResultException(true);

            DirContextOperations dirContextOperations = template.retrieveEntry("", allRootDSEAttNames);

            logger.trace("Retrieved RootDSE: {}", dirContextOperations);
            if (logger.isDebugEnabled()) {
                Attributes rootDSEAttributes = dirContextOperations.getAttributes();
                NamingEnumeration<? extends Attribute> attEnumeration = rootDSEAttributes.getAll();
                StringBuilder sb = new StringBuilder();
                while (attEnumeration.hasMore()) {
                    Attribute attribute = attEnumeration.next();
                    String attributeID = attribute.getID();
                    sb.append("    ").append(attributeID).append(String.format(" (%d)", attribute.size()));
                    if (attribute.size() > 0) {
                        NamingEnumeration<?> attValuesEnumeration = attribute.getAll();
                        sb.append("\n        ").append(attValuesEnumeration.next());
                        while (attValuesEnumeration.hasMore()) {
                            sb.append("; ").append(attValuesEnumeration.next());
                        }
                    }
                    sb.append("\n");
                }
                logger.debug("Retrieved RootDSE: (custom dump)\n{}", sb.toString());
            }
        } catch (Exception e) {
            logger.error("Failed to retrieve/log RootDSE: ", e);
        }
    }
}
