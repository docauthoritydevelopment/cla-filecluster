package com.cla.filecluster.config.security;

import com.cla.filecluster.domain.entity.security.BizRole;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.domain.entity.security.CompositeRole;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.service.security.BizRoleService;
import com.cla.filecluster.service.security.DomainAccessHandler;
import com.cla.filecluster.service.security.LdapGroupMappingService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import java.util.*;
import java.util.stream.Collectors;

public class CustomUserDetailsContextMapper implements UserDetailsContextMapper {

    private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsContextMapper.class);

    @Autowired
    private UserService userService;

    @Autowired
    private LdapGroupMappingService ldapGroupMappingService;

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private DomainAccessHandler domainAccessHandler;

    @Value("${ldap.no-role-user.allow-login:false}")
    private boolean allowNoRoleUser;

    @Value("${ldap.default.role-name:viewer}")
    private String defaultRoleName;

    @Value("${ldap.user-tokens.add-domain-prefix.active:true}")
    private boolean addDomainNameToAccessTokens;

    @Value("${ldap.user-tokens.save-type:both}") // possible values are both/email/nt
    private String accessTokenTypeToSave;

    @Override
    @Transactional
    public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {

        Set<String> accessTokens = Sets.newHashSet();
        Attribute cn = ctx.getAttributes().get("cn");
        logger.trace("start user {} cn {}, authorities {}", username, cn, authorities);
        try {
            String name = (String) cn.get();
            Set<CompositeRole> daRoles = new HashSet<>(authorities.size());
            for (GrantedAuthority authority : authorities) {
                String groupName = authority.getAuthority();
                accessTokens.add(groupName);
                Set<CompositeRole> groupDaRoles = ldapGroupMappingService.getGroupCompositeRoles(groupName);
                if (groupDaRoles != null) {
                    daRoles.addAll(groupDaRoles);
                }
            }
            logger.trace("user {} tokens {} roles {}", username, accessTokens, daRoles);

            if (daRoles.size() == 0) {
                handleUserWithNoBizRoles(username, daRoles);
                logger.trace("user {} tokens {} roles {}", username, accessTokens, daRoles);
            }

            accessTokens.add(username); // this will have the domain prefix concatenated in next if statement

            if (addDomainNameToAccessTokens) {
                String currentUserDomain = userService.getCurrentUserDomain();
                accessTokens = accessTokens.stream().
                        map(token -> concatDomainPrefix(token, currentUserDomain))
                        .flatMap(Collection::stream).distinct().
                        collect(Collectors.toSet());
                logger.trace("user {} domain {} tokens {}", username, currentUserDomain, accessTokens);
            }

            logger.trace("create ldap user name {} user {} roles {} tokens {}", name, username, daRoles, accessTokens);
            User user = userService.createLdapUserIfAbsentInCurrentDomain(name, username, daRoles, accessTokens);
            return new ClaUserDetails(UserService.convertUser(user, false, true));
        } catch (NamingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {

    }

    Set<String> concatDomainPrefix(String token, String domainName) {
        Set<String> result = Sets.newHashSet();
        if (!accessTokenTypeToSave.equals("email")) {
            result.add(domainAccessHandler.getFileShareToken(token, domainName));
        }
        if (!accessTokenTypeToSave.equals("nt")) {
            result.add(domainAccessHandler.getEmailToken(token, domainName));
        }
        return result;
    }

    void handleUserWithNoBizRoles(String username, Set<CompositeRole> userDaRoles) {
        Objects.requireNonNull(userDaRoles);
        if (allowNoRoleUser){
            logger.info("Allowing user {} to login with default role {}", username, defaultRoleName);
            BizRole defaultBizRole = bizRoleService.getBizRoleByName(defaultRoleName);
            if (defaultBizRole == null) {
                logger.warn("Default BizRole {} does not exist. Allowing no-role user {} to login", defaultRoleName, username);
            } else {
                userDaRoles.add(defaultBizRole);
            }
        } else {
            logger.info("Denying access from user {} that has no roles", username);
            throw new BadCredentialsException("User " + username + " has no roles. Access is Denied");
        }
    }
}
