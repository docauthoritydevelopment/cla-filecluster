package com.cla.filecluster.config;

import com.cla.common.jms.ErrorLoggingPolicy;
import com.cla.config.jms.JmsErrorHandler;
import com.cla.config.jms.JmsErrorHandlingTemplate;
import com.cla.config.jms.JmsErrorRetryPolicy;
import com.cla.filecluster.utils.EncUtils;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.transport.DefaultTransportListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.ResourceAllocationException;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.util.backoff.BackOffExecution;

import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by uri on 22-Jun-17.
 */
@Configuration
@Profile({"prod", "win", "dev", "default", "test"})
public class AppServerJmsConfig {

    @Value("${spring.activemq.broker-url}")
    private String activeMqBrokerUrl;

    @Value("${da.activemq.maxAllowedMessageFrameSize:268435456}")
    private long maxAllowedMessageFrameSize;

    @Value("${da.activemq.template-ttl-min:1440}")
    private int templateTTlMin;

    @Value("${da.activemq.broadcast.template-ttl-min:10}")
    private int templateBroadcastTTlMin;

    @Value("${da.activemq.timeout.millis:2000}")
    private int timeout;

    @Value("${scan.requests.retryPolicy:-1;1}")
    private String scanRequestsRetryPolicyConfig;

    @Value("${scan.requests.topic:}")
    private String scanRequestsTopic;

    @Value("${ingest.requests.retryPolicy:-1;1}")
    private String ingestRequestsRetryPolicyConfig;

    @Value("${ingest.requests.topic:}")
    private String ingestRequestsTopic;

    @Value("${control.requests.retryPolicy:5;1}")
    private String controlRequestsRetryPolicyConfig;

    @Value("${control.requests.broadcast.topic:}")
    private String controlRequestsTopic;

    @Value("${realtime.requests.broadcast.topic:realtime.requests.broadcast}")
    private String realtimeBroadcastRequestsTopic;

    @Value("${realtime.requests.retryPolicy:5;1}")
    private String realtimeRequestsRetryPolicyConfig;

    @Value("${realtime.requests.topic:}")
    private String realtimeRequestsTopic;

    @Value("${labeling.requests.retryPolicy:-1;1}")
    private String labelingRequestsRetryPolicyConfig;

    @Value("${labeling.requests.topic:}")
    private String labelingRequestsTopic;

    @Value("${activemq.monitor-queue-user:admin}")
    private String activemqUser;

    @Value("${activemq.monitor-queue-pass:admin}")
    private String activemqPass;

    @Value("${use-enc-pass:false}")
    private boolean useEncPassword;

    private final static Logger logger = LoggerFactory.getLogger(AppServerJmsConfig.class);

    /**
     * We provide the active mq connection factory inorder to track the transport errors and provide
     * stats about them
     *
     * @return active mq connection factory
     */
    @ConditionalOnExpression("!'${spring.activemq.broker-url}'.contains('vm://')")
    @Bean
    public ConnectionFactory connectionFactory() {
        logger.info("Creating active appserver mq config: {}", activeMqBrokerUrl);
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(activeMqBrokerUrl);
        activeMQConnectionFactory.setCloseTimeout(timeout);
        activeMQConnectionFactory.setOptimizeAcknowledgeTimeOut(timeout);
        activeMQConnectionFactory.setSendTimeout(timeout);
        activeMQConnectionFactory.setUserName(activemqUser);
        activeMQConnectionFactory.setPassword(useEncPassword ? EncUtils.decrypt(activemqPass) : activemqPass);
        activeMQConnectionFactory.setUseAsyncSend(true);

        // Setting transportErrorLoggingPolicy listener to track active mq failover errors
        ErrorLoggingPolicy transportErrorLoggingPolicy = new ErrorLoggingPolicy();
        activeMQConnectionFactory.setTransportListener(new DefaultTransportListener() {
            @Override
            public void onException(IOException error) {
                transportErrorLoggingPolicy.onException(logger, error);
            }
        });
        return new PooledConnectionFactory(activeMQConnectionFactory);
    }

    /**
     * DefaultJmsListenerContainerFactory for the listeners of scan,  ingest result messages from media proc.
     * We disable here backoff policy, cause we use the one in active mq.
     * Also we configure to convert message via a custom {@link AppServerJmsConfig#jacksonJmsMessageConverter()}
     *
     * @param connectionFactory JMS connection factory
     * @param configurer        DefaultJmsListenerContainerFactoryConfigurer
     * @return DefaultJmsListenerContainerFactory
     */
    @Bean
    public JmsListenerContainerFactory<?> appServerJmsFactory(ConnectionFactory connectionFactory,
                                                              DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

        factory.setBackOff(() -> () -> BackOffExecution.STOP);
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
//        logger.debug("ActiveMQ broker URL: {}",activeMqBrokerUrl);
        return factory;
    }

    /**
     * Also possible to replace with SimpleMessageConverter
     *
     * @return Serialize message content to json using TextMessage
     */
    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

    /**
     * Provide the jms template to send messages over the active mq.
     * The jms template will throw an error in case message length is greater than what is configured for
     * da.activemq.maxAllowedMessageFrameSize
     *
     * @param connectionFactory JMS connection factory
     * @param messageConverter  JMS message converter
     * @return JMS template
     */
    @Bean
    public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory, MessageConverter messageConverter, JmsErrorHandler jmsErrorHandler) {
        // Configure the jms template to throw an error
        // if message length is greater than maxAllowedMessageFrameSize
        JmsTemplate jmsTemplate = new JmsErrorHandlingTemplate(connectionFactory, jmsErrorHandler, maxAllowedMessageFrameSize);
        jmsTemplate.setTimeToLive(TimeUnit.MINUTES.toMillis(templateTTlMin));
        jmsTemplate.setExplicitQosEnabled(true);
        jmsTemplate.setMessageConverter(messageConverter);
        jmsTemplate.setDeliveryMode(DeliveryMode.PERSISTENT);

        return jmsTemplate;
    }

    /**
     * Provide the jms template to send messages over the active mq.
     * The jms template will throw an error in case message length is greater than what is configured for
     * da.activemq.maxAllowedMessageFrameSize
     * and will apply policies configured by the jmsErrorHandler
     *
     * @param connectionFactory JMS connection factory
     * @param messageConverter  JMS message converter
     * @param jmsErrorHandler   JMS error handler
     * @return JMS template
     */
    @Bean
    public JmsTemplate broadcastJmsTemplate(ConnectionFactory connectionFactory, MessageConverter messageConverter, JmsErrorHandler jmsErrorHandler) {
        JmsTemplate jmsTemplate = new JmsErrorHandlingTemplate(connectionFactory, jmsErrorHandler, maxAllowedMessageFrameSize);
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.setTimeToLive(TimeUnit.MINUTES.toMillis(templateBroadcastTTlMin));
        jmsTemplate.setExplicitQosEnabled(true);
        jmsTemplate.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        jmsTemplate.setMessageConverter(messageConverter);
        return jmsTemplate;
    }

    @Bean
    public JmsErrorHandler jmsErrorHandler() {

        return JmsErrorHandler.builder()
                // Scan requests error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(scanRequestsRetryPolicyConfig))
                .onDestinationPrefix(scanRequestsTopic)
                .against(ResourceAllocationException.class)

                // Ingest requests error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(ingestRequestsRetryPolicyConfig))
                .onDestinationPrefix(ingestRequestsTopic)
                .against(ResourceAllocationException.class)

                // Control requests error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(controlRequestsRetryPolicyConfig))
                .onDestinationPrefix(controlRequestsTopic)
                .against(ResourceAllocationException.class)

                // Labeling requests error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(labelingRequestsRetryPolicyConfig))
                .onDestinationPrefix(labelingRequestsTopic)
                .against(ResourceAllocationException.class)

                // Realtime broadcast requests error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(realtimeRequestsRetryPolicyConfig))
                .onDestinationPrefix(realtimeBroadcastRequestsTopic)
                .against(ResourceAllocationException.class)

                // Realtime requests error policy
                .withErrorPolicy(new JmsErrorRetryPolicy(realtimeRequestsRetryPolicyConfig))
                .onDestinationPrefix(realtimeRequestsTopic)
                .against(ResourceAllocationException.class)

                .build();
    }

}
