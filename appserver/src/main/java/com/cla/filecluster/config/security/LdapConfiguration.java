package com.cla.filecluster.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

/**
 * Created by uri on 12/11/2015.
 */
@Configuration
public class LdapConfiguration {

    @Autowired
    Environment env;

    @Value("${ldap.enable:false}")
    private boolean enableLdap;

    @Bean
    public LdapContextSource contextSource () {
        LdapContextSource contextSource= new LdapContextSource();
        contextSource.setUrl(env.getProperty("ldap.url", "127.0.0.1"));
        contextSource.setBase(env.getProperty("ldap.base", "OU=Employees,OU=Users,DC=domain,DC=com"));
        contextSource.setUserDn(env.getProperty("ldap.user", "CN=myuserid,OU=employees,OU=Users,DC=domain,DC=com"));
        contextSource.setPassword(env.getProperty("ldap.password","secret"));
        return contextSource;
    }

    @Bean
    public LdapTemplate ldapTemplate() {
        return new LdapTemplate(contextSource());
    }
}
