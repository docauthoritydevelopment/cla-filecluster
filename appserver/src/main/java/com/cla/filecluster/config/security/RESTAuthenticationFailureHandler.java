package com.cla.filecluster.config.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

@Component
public class RESTAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response,
			final AuthenticationException exception) throws IOException, ServletException {
        if (exception instanceof AccountStatusException) {
            //Send 403 instead
            response.sendError(HttpServletResponse.SC_FORBIDDEN, "User Forbidden: " + exception.getMessage());
        }
        else {
            super.onAuthenticationFailure(request, response, exception);
        }
	}
}
