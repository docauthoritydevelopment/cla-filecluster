package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.Authority;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for specifying RBAC permissions (authorities) to be enforced by SecurityService to access/deny usage
 * of an API controller method.
 * Roles are a set of authority constants, usually specified as static constants at {@link Authority Authority}.
 * The set of permissions are matched against the roles of the active user of the session making the API call.
 *
 * Logic allows 'or' of specified permissions in addition to 'and' with mandatory permissions marked with a '+' prefix.
 * All mandatory permissions must exist. If non-mandatory permissions are specified then at least one of them should exist.
 *
 * For example:
 *      @Permissions(roles={"Perm1,Perm2"})        - either perm1 or perm2 is required
 *      @Permissions(roles={"+Perm1,Perm2,Perm3"}) - perm1 is required + perm2 or perm3 (one of the non-mandatory permissions is required)
 *      @Permissions(roles={"+Perm1,+Perm2"})      - both perm1 and perm2 is required
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Permissions {
    String[] roles();
}
