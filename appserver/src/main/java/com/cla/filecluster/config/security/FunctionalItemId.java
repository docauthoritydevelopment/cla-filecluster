package com.cla.filecluster.config.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface FunctionalItemId {
    // item type: folder, group, file, tag, doc-type etc.
    String value();
}
