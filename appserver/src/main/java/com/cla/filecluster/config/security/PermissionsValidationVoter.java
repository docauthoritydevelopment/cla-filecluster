package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.Authority;
import com.cla.common.domain.dto.security.FunctionalItemDto;
import com.cla.filecluster.domain.entity.security.FunctionalItem;
import com.cla.filecluster.service.security.PermissionsService;
import com.google.common.collect.Sets;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Set;

/**
 * Permissions voter, uses PermissionsService to grant or deny access to service method
 * Created by vladiu on 4/12/2015.
 */
public class PermissionsValidationVoter implements AccessDecisionVoter<MethodInvocation> {

    private PermissionsService permissionsService;

    public PermissionsValidationVoter(PermissionsService permissionsService) {
        this.permissionsService = permissionsService;
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    public int vote(Authentication authentication, MethodInvocation mi, Collection<ConfigAttribute> attributes) {
        Method method = mi.getMethod();
        Permissions permissionAnnotation = method.getAnnotation(Permissions.class);

        // if no permission annotation, do nothing
        if (permissionAnnotation == null){
            return ACCESS_ABSTAIN;
        }

        String[] requiredRoles = permissionAnnotation.roles();
        Class<?>[] parameterTypes = method.getParameterTypes();
        Annotation[][] allParameterAnnotations = method.getParameterAnnotations();
        Object[] arguments = mi.getArguments();

        return vote(requiredRoles, arguments, parameterTypes, allParameterAnnotations);
    }

    int vote(String[] requiredRoles, Object[] arguments,
                    Class<?>[] parameterTypes, Annotation[][] allParameterAnnotations) {


        Integer accessDecision = null;

        Set<String> requiredRolesSet = Sets.newHashSet(requiredRoles);

        // GeneralAdmin is allowed to view/modify everything
        requiredRolesSet.add(Authority.GeneralAdmin.getName());

        // examine method parameters
        // first - parameters of FunctionalItem type
        // check that user has permission to use specific item
        int index = 0;
        for (Class<?> type : parameterTypes){
            if (FunctionalItem.class.isAssignableFrom(type)){
                FunctionalItem item = (FunctionalItem)arguments[index];
                accessDecision = permissionsService.getAccess(requiredRolesSet, item);
            }
            else if (FunctionalItemDto.class.isAssignableFrom(type)){
                FunctionalItemDto item = (FunctionalItemDto)arguments[index];
                accessDecision = permissionsService.getAccess(requiredRolesSet, item);
            }

            if (accessDecision != null && accessDecision == ACCESS_DENIED){
                return ACCESS_DENIED;
            }
            index++;
        }

        // second - parameters annotated with @FunctionalItemId annotation
        // check that user has permission to use specific item by its id and type
        index = 0;
        for (Annotation[] singleParamAnnotations : allParameterAnnotations){
            for (Annotation annotation : singleParamAnnotations) {
                if (FunctionalItemId.class.isAssignableFrom(annotation.annotationType())) {
                    String functionItemType = ((FunctionalItemId) annotation).value();
                    Object functionalItemId = arguments[index];
                    accessDecision = permissionsService.getAccess(requiredRolesSet, functionalItemId, functionItemType);
                    if (accessDecision == ACCESS_DENIED){
                        return ACCESS_DENIED;
                    }
                }
            }
            index++;
        }

        return ACCESS_GRANTED;
    }
}

