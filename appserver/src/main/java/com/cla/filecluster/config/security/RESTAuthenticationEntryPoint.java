package com.cla.filecluster.config.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@Component
public class RESTAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private static final Logger logger = LoggerFactory.getLogger(RESTAuthenticationEntryPoint.class);

	@Override
	public void commence(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException authException)
			throws IOException, ServletException {

		if (logger.isTraceEnabled()) {
			logger.trace("Sending 401 response on authentication exception. Reason: {}. URL={}", authException.getMessage(), request.getRequestURI(), authException);
			StringBuilder hSb = new StringBuilder();
			Enumeration<String> headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String hname = headerNames.nextElement();
				hSb.append(hname).append(":");
				Enumeration<String> hvalues = request.getHeaders(hname);
				boolean first = true;
				while (hvalues.hasMoreElements()) {
					hSb.append(hvalues.nextElement());
					if (first) {
						first = false;
					} else {
						hSb.append(",");
					}
				}
				hSb.append(";");
			}
			StringBuilder aSb = new StringBuilder();
			Enumeration<String> attributes = request.getAttributeNames();
			while (attributes.hasMoreElements()) {
				String attr = attributes.nextElement();
				aSb.append(attr).append(":").append(request.getAttribute(attr).toString()).append(";");
			}
			logger.trace("Details: reqAuth={} reqHeaders={} reqAttr={} ", request.getAuthType(), hSb.toString(), aSb.toString());
			logger.trace("Request={} Response={}", request, response==null?"null":response.toString());
		} else {
			logger.debug("Sending 401 response on authentication exception. Reason: {}. URL={}", authException.getMessage(), request.getRequestURI());
		}
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}
}