package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.LdapServerDialect;
import com.cla.filecluster.service.convertion.DataEncryptionService;
import com.cla.filecluster.service.security.LdapConnectionDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.security.web.session.HttpSessionEventPublisher;

import java.security.SecureRandom;

import static com.cla.common.domain.dto.security.Authority.Const.*;

@Order(SecurityProperties.BASIC_AUTH_ORDER - 2)
@Configuration
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RESTAuthenticationEntryPoint authenticationEntryPoint;

    @Autowired
    private RESTAuthenticationFailureHandler authenticationFailureHandler;

    @Autowired
    private RESTAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private RESTLogoutSuccessHandler logoutSuccessHandler;

    @Autowired
    private LdapConnectionDetailsService ldapConnectionDetailsService;

    @Autowired
    private DataEncryptionService dataEncryptionService;

    @Value("${password.strength:10}")
    private int passwordStrength;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {

        http.csrf().disable()
                .formLogin()
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .logout()
                .logoutSuccessHandler(logoutSuccessHandler);


        authorizeRequests(http);

    }

    private void authorizeRequests(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/login.html", "/**/*.js").permitAll()
                .antMatchers("/api/logs/**").hasAuthority(GENERAL_ADMIN)
                .antMatchers("/swagger-ui.html").hasAuthority(GENERAL_ADMIN)
                .antMatchers(HttpMethod.GET, "/api/healthCheck").access(
                "hasIpAddress('0:0:0:0:0:0:0:1') or hasIpAddress('127.0.0.1/32')")
                .antMatchers(HttpMethod.POST, "/api/run/logmon/**").access(
                "hasIpAddress('0:0:0:0:0:0:0:1') or hasIpAddress('127.0.0.1/32')")
                .antMatchers(HttpMethod.GET, "/api/license/**").authenticated()
                .antMatchers("/api/license/**").hasAnyAuthority(GENERAL_ADMIN, TECH_SUPPORT, MNG_LICENSE)

                .antMatchers("/api/communication/email/notification/**", "/scan/email-report").hasAnyAuthority(RUN_SCANS, TECH_SUPPORT)

                .antMatchers(HttpMethod.GET, "/api/user/current").authenticated()
                .antMatchers(HttpMethod.GET, "/api/user/current/with-access-tokens").authenticated()
                .antMatchers(HttpMethod.PUT, "/api/user/current/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/user/current/**").authenticated()
                .antMatchers("/api/user/saveddata").authenticated()
                .antMatchers("/api/user/saveddata/**").authenticated()

                .antMatchers(HttpMethod.GET, "/api/user/bizroles", "/api/user/roles").authenticated()

                .antMatchers(HttpMethod.GET, "/api/content/checkSignature").permitAll()        // .authenticated() - for now require not authentication

                .antMatchers(HttpMethod.POST, "/api/user/bizroles/**").hasAnyAuthority(MNG_ROLES)
                .antMatchers(HttpMethod.PUT, "/api/user/bizroles/**").hasAnyAuthority(MNG_ROLES)
                .antMatchers(HttpMethod.DELETE, "/api/user/bizroles/**").hasAnyAuthority(MNG_ROLES)

                .antMatchers(HttpMethod.POST, "/api/user/**", "/api/user", "/authorities").hasAnyAuthority(MNG_USERS)
                .antMatchers(HttpMethod.PUT, "/api/user/**", "/api/user", "/authorities").hasAnyAuthority(MNG_USERS)
                .antMatchers(HttpMethod.DELETE, "/api/user/**", "/api/user", "/authorities").hasAnyAuthority(MNG_USERS)
                .antMatchers(HttpMethod.GET, "/api/user/**", "/api/user", "/authorities").hasAnyAuthority(VIEW_USERS, MNG_USERS)

                .antMatchers("/api/rerun").hasAnyAuthority(RUN_SCANS)
                .antMatchers("/api/scan/**").hasAnyAuthority(RUN_SCANS)

                .antMatchers(HttpMethod.GET, "/api/run/logmon").authenticated()
                .antMatchers(HttpMethod.GET, "/api/run/logmon/**").authenticated()
                .antMatchers(HttpMethod.GET, "/api/scan/process/**").authenticated()

                .antMatchers(HttpMethod.GET, "/api/sys/customerDataCenter/**").hasAnyAuthority(RUN_SCANS, MNG_SCAN_CONFIG)
                .antMatchers(HttpMethod.POST, "/api/sys/customerDataCenter/**").hasAnyAuthority(MNG_SCAN_CONFIG)
                .antMatchers(HttpMethod.PUT, "/api/sys/customerDataCenter/**").hasAnyAuthority(MNG_SCAN_CONFIG)
                .antMatchers(HttpMethod.DELETE, "/api/sys/customerDataCenter/**").hasAnyAuthority(MNG_SCAN_CONFIG)

                .antMatchers(HttpMethod.GET, "/api/filetree/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/filetree/rootfolders/**").hasAnyAuthority(MNG_SCAN_CONFIG)
                .antMatchers(HttpMethod.PUT, "/api/filetree/rootfolders/**").hasAnyAuthority(MNG_SCAN_CONFIG)
                .antMatchers(HttpMethod.DELETE, "/api/filetree/rootfolders/**").hasAnyAuthority(MNG_SCAN_CONFIG)

                .antMatchers("/scan", "/ingest", "/analyze", "/finalizeAnalysis", "/run").hasAnyAuthority(RUN_SCANS)
                .antMatchers(HttpMethod.POST, "/api/groupsnaming/updatename").hasAnyAuthority(RUN_SCANS, MNG_GROUPS)

                .antMatchers("/api/media/box/response").anonymous()

                .antMatchers("/actuator/**").access(
                "hasIpAddress('0:0:0:0:0:0:0:1') or hasIpAddress('127.0.0.1/32')")

				.antMatchers(HttpMethod.POST, "/api/health/drive/**").permitAll()

                .antMatchers("/**").authenticated();
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsServiceBean() throws Exception {
        return super.userDetailsServiceBean();
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return new ClaUserDetailsService();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(passwordStrength, new SecureRandom());
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new CompositeAuthenticationProvider(
                userDetailsService(),
                passwordEncoder(),
                ldapConnectionDetailsService,
                ldapServerDialectConfigurer(),
                dataEncryptionService,
                userDetailsContextMapper(),
                ldapUtils());
    }

    @Bean
    public UserDetailsContextMapper userDetailsContextMapper() {
        return new CustomUserDetailsContextMapper();
    }

    @Bean
    public LdapServerDialectConfigurer ldapServerDialectConfigurer() {
        LdapServerDialectConfigurer ldapConfigurer = new LdapServerDialectConfigurer();
        ldapConfigurer.setDialect(LdapServerDialect.ACTIVE_DIRECTORY_LEGACY); // use AD-legacy as default
        return ldapConfigurer;
    }

    @Bean
    public LdapUtils ldapUtils() {
        return new LdapUtils();
    }

    @Bean
    @SuppressWarnings("unused")
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}
