package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.LdapConnectionDetailsDto;
import com.cla.common.domain.dto.security.LdapServerDialect;
import com.cla.common.domain.dto.security.UserState;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.service.convertion.DataEncryptionService;
import com.cla.filecluster.service.security.LdapConnectionDetailsService;
import com.cla.filecluster.service.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.List;

import static com.cla.filecluster.service.security.UserService.LdapDomainDelim;

/**
 * Created by Itai Marko.
 */
public class CompositeAuthenticationProvider implements AuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(CompositeAuthenticationProvider.class);

    @Value("${ldap.domain.dn.format:}")
    private String domainDnFormat;

    @Value("${ldap.referral-mode:}")
    private String referralMode;

    // Look for nested AD groups up to this depth (to avoid endless recursion)
    @Value("${ldap.groups.max-level:9}")
    private int maxGroupRecursionLevel;

    @Value("${ldap.attribute.group-role:cn}")
    private String groupRoleAttribute;

    @Value("${ldap.attribute.parent-group-role:dn}")
    private String parentGroupRoleAttribute;

    @Autowired
    private UserService userService;

    private LdapUtils ldapUtils;
    private DaoAuthenticationProvider daoAuthenticationProvider;

    private LdapConnectionDetailsService ldapConnectionDetailsService;
    private DataEncryptionService dataEncryptionService;
    private UserDetailsContextMapper userDetailsContextMapper;
    private LdapContextSource contextSource;
    private LdapServerDialectConfigurer ldapConfigurer;
    private BindAuthenticator ldapAuthenticator;
    private CustomLdapAuthoritiesPopulator ldapAuthoritiesPopulator;
    private LdapAuthenticationProvider ldapAuthenticationProvider;

    CompositeAuthenticationProvider(UserDetailsService userDetailsService,
                                    PasswordEncoder passwordEncoder,
                                    LdapConnectionDetailsService ldapConnectionDetailsService,
                                    LdapServerDialectConfigurer ldapConfigurer,
                                    DataEncryptionService dataEncryptionService,
                                    UserDetailsContextMapper userDetailsContextMapper,
                                    LdapUtils ldapUtils) {

        daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder);

        this.ldapConnectionDetailsService = ldapConnectionDetailsService;
        this.ldapConfigurer = ldapConfigurer;
        this.dataEncryptionService = dataEncryptionService;
        this.userDetailsContextMapper = userDetailsContextMapper;
        this.ldapUtils = ldapUtils;
        LdapConnectionDetailsDto ldapConnectionDetailsDto = ldapConnectionDetailsService.getAnyEnabledDto();
        if (ldapConnectionDetailsDto != null) {
            configLdapConnection(ldapConnectionDetailsDto);
        }
    }

    private String configLdapConnection(LdapConnectionDetailsDto ldapConnectionDetailsDto) {

        String ldapUrl = ldapConnectionDetailsService.getConnectionUrl(ldapConnectionDetailsDto);
        contextSource = new DefaultSpringSecurityContextSource(ldapUrl);
        if (ReferralMode.isValidReferralString(referralMode)) {
            contextSource.setReferral(referralMode.toLowerCase());
        }
        LdapServerDialect ldapServerDialect = ldapConnectionDetailsDto.getServerDialect();
        logger.debug("Configuring LDAP connection to url: {} using dialect: {}. Group max heirarchy search={}", ldapUrl, ldapServerDialect, maxGroupRecursionLevel);
        ldapConfigurer.setDialect(ldapServerDialect);
        contextSource.setUserDn(ldapConnectionDetailsDto.getManageDN());
        contextSource.setPassword(dataEncryptionService.decrypt(ldapConnectionDetailsDto.getManagerPass()));
        contextSource.afterPropertiesSet();
        ldapAuthenticator = new BindAuthenticator(contextSource);
        ldapAuthenticator.setUserDnPatterns(ldapConfigurer.getUserDnPatterns());
        ldapAuthoritiesPopulator = new CustomLdapAuthoritiesPopulator(contextSource);
        ldapAuthoritiesPopulator.setGroupSearchFilter(ldapConfigurer.getGroupSearchFilter());
        ldapAuthoritiesPopulator.setMaxLevel(maxGroupRecursionLevel);
        ldapAuthoritiesPopulator.setGroupRoleAttribute(groupRoleAttribute);
        ldapAuthoritiesPopulator.setParentGroupRoleAttribute(parentGroupRoleAttribute);
        if (ldapConfigurer.isActiveDirectoryDialect()) {
            ldapAuthoritiesPopulator.setIgnorePartialResultException(true);
        }
        ldapAuthenticationProvider = new LdapAuthenticationProvider(ldapAuthenticator, ldapAuthoritiesPopulator);
        ldapAuthenticationProvider.setUserDetailsContextMapper(userDetailsContextMapper);

        ldapUtils.logRootDSE(contextSource);
        return ldapUrl;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(UsernamePasswordAuthenticationToken.class, authentication,
                "CompositeAuthenticationProvider only supports UsernamePasswordAuthenticationToken");

        String username = authentication.getName();
        logger.debug("Authenticating username: {}", username);
        int domainDelimIndex = validateAndGetDelimIndex(username);

        if (domainDelimIndex == -1) {
            // no domain specified => this is (supposedly) a local user
            return daoAuthenticationProvider.authenticate(authentication);
        } else {
            User user = validateUserNotDisabled(username);
            LdapUsernameDetails ldapUsernameDetails = new LdapUsernameDetails(
                    username, domainDelimIndex, user == null ? null : user.getLastLdapFound());
            ldapUsernameDetails.validate();
            Authentication result = authenticateAgainstLdap(authentication, ldapUsernameDetails);
            saveLastLdapForUser(user, ldapUsernameDetails.lastLdapFound);
            return result;
        }
    }

    private void saveLastLdapForUser(User user, Long lastLdapFound) {
        if (user != null && lastLdapFound != null && !lastLdapFound.equals(user.getLastLdapFound())) {
            userService.saveLastLdapForUser(user.getId(), lastLdapFound);
        }
    }

    private Authentication authenticateAgainstLdap(Authentication authentication, LdapUsernameDetails usernameDetails)
            throws AuthenticationException {

        List<LdapConnectionDetailsDto> ldapConnectionDtos =
                ldapConnectionDetailsService.getEnabledLdapConnectionDtos(true);
        if (ldapConnectionDtos.isEmpty()) {
            throw new BadCredentialsException("Username " + usernameDetails.originalUsername + " contains " + usernameDetails.domainDelim +
                    " which indicates an LDAP user but there's no (enabled) LDAP connection configured");
        }

        fixLdapOrderForUserLogin(usernameDetails, ldapConnectionDtos);

        // test all ldap until found (timeout can be long id ldap not responsive)
        String ldapUrl = null;
        for (int i = 0; i < ldapConnectionDtos.size(); i++) {
            try {
                LdapConnectionDetailsDto ldapConnectionDto = ldapConnectionDtos.get(i);
                ldapUrl = configLdapConnection(ldapConnectionDto);
                usernameDetails.lastLdapFound = ldapConnectionDto.getId();

                userService.setCurrentUserDomain(usernameDetails.userDomain);
                FilterBasedLdapUserSearch userSearch =
                        new FilterBasedLdapUserSearch(usernameDetails.domainDN, ldapConfigurer.getUserSearchFilter(), contextSource);
                ldapAuthenticator.setUserSearch(userSearch);
                String groupSearchBase = ldapConfigurer.getGroupSearchBase(usernameDetails.domainDN);
                logger.debug("Setting groupSearchBase to {}", groupSearchBase);
                ldapAuthoritiesPopulator.setGroupSearchBase(groupSearchBase);
                String password = (String) authentication.getCredentials();
                authentication = new UsernamePasswordAuthenticationToken(usernameDetails.username, password);

                // failure to authenticate throws exception
                return ldapAuthenticationProvider.authenticate(authentication);
            }
            catch (AuthenticationException | org.springframework.ldap.AuthenticationException e) {
                logger.debug("Failed to login LDAP user {} against {}", usernameDetails.originalUsername, ldapUrl, e);
                if (i+1 < ldapConnectionDtos.size()) {
                    logger.debug("Trying to login against another LDAP server");
                }
            }
        }
        logger.debug("Failed to login against {} LDAP servers.", ldapConnectionDtos.size());
        throw new BadCredentialsException("Bad Credentials");
    }

    private void fixLdapOrderForUserLogin(LdapUsernameDetails usernameDetails, List<LdapConnectionDetailsDto> ldapConnectionDtos) {
        if (usernameDetails.lastLdapFound != null) {
            int i = 0;
            Integer foundIndex = null;
            for (LdapConnectionDetailsDto ldap : ldapConnectionDtos) {
                if (ldap.getId().equals(usernameDetails.lastLdapFound)) {
                    foundIndex = i;
                    break;
                }
                i++;
            }

            if (foundIndex != null) {
                LdapConnectionDetailsDto ldap = ldapConnectionDtos.get(foundIndex);
                ldapConnectionDtos.remove(ldap);
                ldapConnectionDtos.add(0, ldap);
            }
        }
    }

    private User validateUserNotDisabled(String username) {
        if (username == null || username.trim().isEmpty()) {
            throw new IllegalArgumentException("Username can't be empty. Got: [" + username + "]");
        }
        User user = userService.getUserByName(username);
        if (user != null && (user.isDeleted() || user.getUserState() != UserState.ACTIVE)) {
            throw new DisabledException("User " + username + " is disabled");
        }
        return user;
    }

    /**
     * if domain user return last index of domain delimiter <br/>
     * otherwise return -1
     * @param usernameWithDomain username
     * @return last index of domain delimiter or -1 if not found
     */
    private int validateAndGetDelimIndex(String usernameWithDomain) {
        if (!StringUtils.hasText(usernameWithDomain)) {
            throw new BadCredentialsException("Username must contain some text");
        }

        String atDelim = LdapDomainDelim.AT.toString();
        String backslashDelim = UserService.LdapDomainDelim.BACKSLASH.toString();
        boolean gotAtDelim = usernameWithDomain.contains(atDelim);
        boolean gotBackslashDelim = usernameWithDomain.contains(backslashDelim);

        if (gotAtDelim && gotBackslashDelim) {
            throw new BadCredentialsException(
                    "Username-with-Domain contains both " + atDelim + " and " + backslashDelim + " delimiters");
        }

        if (gotAtDelim) {
            return validateAndGetDelimIndex(usernameWithDomain, LdapDomainDelim.AT);
        } else if (gotBackslashDelim) {
            return validateAndGetDelimIndex(usernameWithDomain, UserService.LdapDomainDelim.BACKSLASH);
        } else {
            return -1;
        }
    }

    private int validateAndGetDelimIndex(String usernameWithDomain, LdapDomainDelim domainDelim) {
        // The type of the domainDelim determines the form (i.e. what's the first token and what is the last token)
        String firstToken;
        String lastToken;
        switch (domainDelim) {
            case AT:
                firstToken = "username";
                lastToken = "domain";
                break;
            case BACKSLASH:
                firstToken = "domain";
                lastToken = "username";
                break;
            default:
                throw new AssertionError("Unexpected LdapDomainDelim: " + domainDelim);
        }

        int lastIndexOfDomainDelim = usernameWithDomain.lastIndexOf(domainDelim.toString());
        if (lastIndexOfDomainDelim == 0) {
            // this means that the domainDelim appears as the first character which specifies an empty firstToken
            String errMsg = "Got empty " + firstToken + ": " + usernameWithDomain;
            throw new BadCredentialsException(errMsg);
        }
        if (lastIndexOfDomainDelim+1 == usernameWithDomain.length()) {
            // this means that the domainDelim appears as the last character which specifies an empty lastToken
            String errMsg = "Got empty " + lastToken + ": " + usernameWithDomain;
            throw new BadCredentialsException(errMsg);
        }
        return lastIndexOfDomainDelim;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return daoAuthenticationProvider.supports(authentication) || ldapAuthenticationProvider.supports(authentication);
    }

    public void removeUserFromAuthenticationCache(String username) {
        UserCache userCache = daoAuthenticationProvider.getUserCache();
        if (userCache != null) {
            userCache.removeUserFromCache(username);
        }
    }

    private final class LdapUsernameDetails {

        private final String originalUsername;      // the username with the domain (e.g. rogerRabbit@acme.com or dc=acme,dc=com\rogerRabbit)
        private final String username;              // the extracted username (e.g. rogerRabbit)
        private final String userDomain;            // the extracted userDomain (e.g. acme.com or dc=acme,dc=com)
        private final String domainDN;              // the extracted domainDN (e.g. dc=acme,dc=com)
        private final LdapDomainDelim domainDelim;  // the extracted domainDelim (either '@' or '\')
        private Long lastLdapFound;

        private LdapUsernameDetails(String username, int domainDelimIndex, Long lastLdapFound) {
            this.originalUsername = username;
            this.domainDelim =
                    LdapDomainDelim.of(username.substring(domainDelimIndex, domainDelimIndex+1));
            // TODO Itai: find a way to map domain short name and domain dot notation to domain DN
            String userDomain;
            String domainDN;
            if (domainDelim == LdapDomainDelim.AT) {
                String dotDelimitedDomain = username.substring(domainDelimIndex + 1);
                userDomain = dotDelimitedDomain;
                domainDN = domainDNFromDotNotation(dotDelimitedDomain);
                username = username.substring(0, domainDelimIndex);
            } else {
                userDomain = domainDN = username.substring(0, domainDelimIndex);
                username = username.substring(domainDelimIndex + 1);
            }

            logger.debug("Parsed ldap user: {}, user domain: {}, domainDN: {}, delim: {}",
                    username, userDomain, domainDN, domainDelim);

            this.username = username;
            this.userDomain = userDomain;
            this.domainDN = domainDN;
            this.lastLdapFound = lastLdapFound;
        }

        private String domainDNFromDotNotation(String dotDelimitedDomain) {
            String[] domainTokens = StringUtils.tokenizeToStringArray(dotDelimitedDomain, ".");
            logger.debug("domainDnFormat: {}", domainDnFormat);
            if (domainDnFormat == null || domainDnFormat.equals("")) {
                StringBuilder sb = new StringBuilder(dotDelimitedDomain.length());
                String comma = ""; // empty just for first iteration
                for (String domainToken : domainTokens) {
                    sb.append(comma).append("dc=").append(domainToken);
                    comma = ",";
                }
                return sb.toString();
            } else {
                String domainDN = MessageFormat.format(domainDnFormat, (Object[]) domainTokens);
                logger.debug("Formatted DomainDN: {}", domainDN);
                return domainDN;
            }
        }

        public void validate() {
            if (!StringUtils.hasText(domainDN)) {
                throw new BadCredentialsException("Ldap domain must contain some text");
            }
            if (!StringUtils.hasText(username)) {
                throw new BadCredentialsException("Ldap username must contain some text");
            }
        }
    }

    private enum ReferralMode {
        FOLLOW
        ,IGNORE
        ,THROW;

        private static boolean isValidReferralString(String ref) {
            for (ReferralMode referralMode : values()) {
                if (referralMode.name().equalsIgnoreCase(ref)) {
                    return true;
                }
            }
            return false;
        }
    }
}
