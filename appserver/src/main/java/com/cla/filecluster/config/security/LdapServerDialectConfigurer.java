package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.LdapServerDialect;
import org.springframework.beans.factory.annotation.Value;

import static com.cla.common.domain.dto.security.LdapServerDialect.ACTIVE_DIRECTORY;
import static com.cla.common.domain.dto.security.LdapServerDialect.ACTIVE_DIRECTORY_LEGACY;

/**
 * Provides LDAP connection configuration properties depending on the {@link LdapServerDialect}.
 *
 * Created by Itai Marko.
 */
public class LdapServerDialectConfigurer {

    private LdapServerDialect dialect;

    private LdapServerDialectConfigurer delegate;

    @Value("${ldap.dialect.user.dn.patterns:}")
    private String userDnPatterns;

    @Value("${ldap.dialect.user.search.filter:}")
    private String userSearchFilter;

    @Value("${ldap.dialect.group.search.filter:}")
    private String groupSearchFilter;

    @Value("${ldap.dialect.group.search.base:}")
    private String groupSearchBase;

    @Value("${ldap.dialect.is-ad:false}")
    private Boolean isAD;

    public String[] getUserDnPatterns() {
        return delegate.getUserDnPatterns();
    }

    public String getUserSearchFilter() {
        return delegate.getUserSearchFilter();
    }

    public String getGroupSearchFilter() {
        return delegate.getGroupSearchFilter();
    }

    public String getGroupSearchBase(String baseDN) {
        return delegate.getGroupSearchBase(baseDN);
    }

    public boolean isActiveDirectoryDialect() {
        return dialect == ACTIVE_DIRECTORY || dialect == ACTIVE_DIRECTORY_LEGACY;
    }

    public void setDialect(LdapServerDialect dialect) {

        this.dialect = dialect;

        switch (dialect) {
            case ACTIVE_DIRECTORY:
                this.delegate = new ActiveDirectoryConfigurer();
                break;
            case ACTIVE_DIRECTORY_LEGACY:
                this.delegate = new ActiveDirectoryLegacyConfigurer();
                break;
            case ApacheDS:
                this.delegate = new ApacheDSConfigurer();
                break;
            case PROPERTY_BASED:
                this.delegate = new PropertyBasedConfigurer();
                break;
            default:
                throw new IllegalArgumentException("Unsupported LdapServerDialect " + dialect);
        }
    }

    private class ActiveDirectoryConfigurer extends LdapServerDialectConfigurer {

        @Override
        public String[] getUserDnPatterns() {
            return new String[] {"cn={0},cn=users"};
        }

        @Override
        public String getUserSearchFilter() {
            return "(userPrincipalName={0})";
        }

        @Override
        public String getGroupSearchFilter() {
            return "(member={0})";
        }

        @Override
        public String getGroupSearchBase(String baseDN) {
            return baseDN;
        }
    }

    private class ActiveDirectoryLegacyConfigurer extends ActiveDirectoryConfigurer {

        @Override
        public String getUserSearchFilter() {
            return "(sAMAccountName={0})";
        }
    }

    private class ApacheDSConfigurer extends LdapServerDialectConfigurer {

        @Override
        public String[] getUserDnPatterns() {
            return new String[] {"cn={0},ou=people","cn={0},cn=users"};
        }

        @Override
        public String getUserSearchFilter() {
            return "(|(uid={0})(sAMAccountName={0})(userPrincipalName={0}))";
        }

        @Override
        public String getGroupSearchFilter() {
            return "(|(member={0})(uniqueMember={0}))";
        }

        @Override
        public String getGroupSearchBase(String baseDN) {
            return "ou=groups,"+baseDN;
        }
    }

    private class PropertyBasedConfigurer extends LdapServerDialectConfigurer {

        @Override
        public String[] getUserDnPatterns() {
            return new String[] { userDnPatterns };
        }

        @Override
        public String getUserSearchFilter() {
            return userSearchFilter;
        }

        @Override
        public String getGroupSearchFilter() {
            return groupSearchFilter;
        }

        @Override
        public String getGroupSearchBase(String baseDN) {
            return groupSearchBase;
        }

        @Override
        public boolean isActiveDirectoryDialect() {
            return isAD;
        }
    }
}
