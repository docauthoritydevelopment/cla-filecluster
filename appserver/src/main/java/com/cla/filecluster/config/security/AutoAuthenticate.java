package com.cla.filecluster.config.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to mark methods that should create authentication as specific user before running
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AutoAuthenticate {

    String ADMIN = "admin";
    String SCHEDULE = "scheduleInternalUser";

    String runAs() default ADMIN;
    boolean replace() default true;

}
