package com.cla.filecluster.config.security;

import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.repository.jpa.security.UserRepository;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.service.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.transaction.Transactional;


@Transactional
public class ClaUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private LicenseService licenseService;

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		User userByUsername = userRepository.findUserByUsername(username);
		if (userByUsername == null) {
			throw new UsernameNotFoundException(username);
		}
		UserDto userDto = UserService.convertUser(userByUsername,true, true);
		userDto.setLoginLicenseExpired(licenseService.isLoginExpired());
		return new ClaUserDetails(userDto);
	}

}
