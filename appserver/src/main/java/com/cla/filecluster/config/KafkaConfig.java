package com.cla.filecluster.config;

/**
 * Apache Kafka configuration
 * Created by vladi on 1/31/2017.
 */
//@Configuration
//@EnableKafka
public class KafkaConfig {

//    @Autowired
//    private Environment env;
//
//    @Value("${kafka.server-address}")
//    private String kafkaServerAddress;
//
//    @Value("${kafka.consumer-threads-num}")
//    private int consumerThreadsNum;
//
//    @Value("${kafka.groups.appserver.worker}")
//    private String workerGroup;
//
//    @Value("${kafka.groups.appserver.control}")
//    private String controlGroup;
//
//    @Value("${kafka.config.max.message.size:33554432}")
//    private int maxMessageSize;
//
//    @Value("${kafka.config.poll.timeout:15000}")
//    private int pollTimeout;
//
//    @Value("${kafka.config.template.flush:true}")
//    private boolean flushTemplate;
//
//    private static final Logger logger = LoggerFactory.getLogger(KafkaConfig.class);
//    //----------- Kafka Producer Configuration -------------------------------------------------------
//
//    @Bean
//    public ProducerFactory<Long, String> producerFactory() {
//        logger.info("Creating Kafka Producer factory");
//        Map<String, Object> configs = producerConfigs();
//        for (Map.Entry<String, Object> stringObjectEntry : configs.entrySet()) {
//            logger.debug("Kafka config: {}:{}", stringObjectEntry.getKey(), stringObjectEntry.getValue());
//        }
//
//        return new DefaultKafkaProducerFactory<>(configs);
//    }
//
//    @Bean
//    public Map<String, Object> producerConfigs() {
//        Map<String, Object> props = Maps.newHashMap();
//        String embeddedKafkaAddress = System.getProperty("spring.embedded.kafka.brokers");
//        if (StringUtils.isNotEmpty(embeddedKafkaAddress)) {
//            logger.debug("Using Embedded Kafka Server {}", embeddedKafkaAddress);
//            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, embeddedKafkaAddress);
//        } else {
//            logger.debug("Using Kafka Server {}", kafkaServerAddress);
//            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
//        }
//        props.put(ProducerConfig.RETRIES_CONFIG, 0);
//        props.put(ProducerConfig.BATCH_SIZE_CONFIG, 16384);
//        props.put(ProducerConfig.LINGER_MS_CONFIG, 2);
//        props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, 33554432);
//        props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, maxMessageSize);
//
//        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
//        return props;
//    }
//
//    @Bean
//    public KafkaTemplate<Long, String> kafkaTemplate() {
//        logger.debug("Creating Kafka Template");
//        return new KafkaTemplate<>(producerFactory(),flushTemplate);
//    }
//
//    //----------- Kafka Consumer Configuration -------------------------------------------------------
//
//    /**
//     * Consumer factory for task processing listeners.
//     * Multi-threaded, with group-id common to all media processor instances
//     *
//     * @return KafkaListenerContainerFactory
//     */
//    @Bean
//    @Named("kafkaListenerContainerFactory")
//    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaListenerContainerFactory() {
//        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
//        factory.setConsumerFactory(consumerFactory());
//        factory.setConcurrency(consumerThreadsNum);
//        factory.getContainerProperties().setPollTimeout(pollTimeout);
//        return factory;
//    }
//
//    /**
//     * Consumer factory for task processing batch listeners.
//     * Multi-threaded, with group-id common to all media processor instances
//     *
//     * @return KafkaListenerContainerFactory
//     */
//    @Bean
//    @Named("kafkaListenerBatchFactory")
//    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaListenerBatchFactory() {
//        ConcurrentKafkaListenerContainerFactory<Integer, String> factory =
//                new ConcurrentKafkaListenerContainerFactory<>();
//        factory.setConsumerFactory(consumerFactory());
//        factory.getContainerProperties().setPollTimeout(pollTimeout);
//        factory.setBatchListener(true);  // <<<<<<<<<<<<<<<<<<<<<<<<<
////        factory.setConcurrency(4);
////        factory.getContainerProperties().setListenerTaskExecutor(execC());
//        return factory;
//    }
//
////    @Bean
////    public AsyncListenableTaskExecutor execC() {
////        ThreadPoolTaskExecutor tpte = new ThreadPoolTaskExecutor();
////        tpte.setCorePoolSize(15);
////        tpte.setThreadGroupName("appServer-Kafka-listen");
////        return tpte;
////    }
//
////    @Bean
////    public AsyncListenableTaskExecutor execL() {
////        ThreadPoolTaskExecutor tpte = new ThreadPoolTaskExecutor();
////        tpte.setCorePoolSize(15);
////        return tpte;
////    }
//
//    /**
//     * Consumer factory for control messages processing listener.
//     * Single-threaded, with unique group-id
//     *
//     * @return KafkaListenerContainerFactory
//     */
//    @Bean
//    KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<Integer, String>> kafkaControlListenerContainerFactory() {
//        ConcurrentKafkaListenerContainerFactory<Integer, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
//        Map<String, Object> configs = consumerConfigs();
//        configs.put(ConsumerConfig.GROUP_ID_CONFIG, controlGroup);
//
//        DefaultKafkaConsumerFactory<Integer, String> controlConsumerFactory = new DefaultKafkaConsumerFactory<>(configs);
//
//        factory.setConsumerFactory(controlConsumerFactory);
//        factory.setConcurrency(1);
//        factory.getContainerProperties().setPollTimeout(pollTimeout);
//        return factory;
//    }
//
//    /**
//     * Default consumer factory for task processing consumers
//     *
//     * @return ConsumerFactory
//     */
//    @Bean
//    public ConsumerFactory<Integer, String> consumerFactory() {
//        return new DefaultKafkaConsumerFactory<>(consumerConfigs());
//    }
//
//    /**
//     * Consumer configuration for task processing consumers
//     *
//     * @return properties
//     */
//    @Bean
//    public Map<String, Object> consumerConfigs() {
//        Map<String, Object> props = Maps.newHashMap();
//        String embeddedKafkaAddress = System.getProperty("spring.embedded.kafka.brokers");
//        if (StringUtils.isNotEmpty(embeddedKafkaAddress)) {
//            logger.debug("Using Embedded Kafka Server {}", embeddedKafkaAddress);
//            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, embeddedKafkaAddress);
//        } else {
//            props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServerAddress);
//        }
//
//        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
//        props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "100");
//        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "15000");
//        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        props.put(ConsumerConfig.GROUP_ID_CONFIG, workerGroup);
//        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
//        return props;
//    }

}
