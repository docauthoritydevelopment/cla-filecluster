package com.cla.filecluster.config;

import com.cla.common.constants.CatFileFieldType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by uri on 13/10/2015.
 */
@Configuration
public class SummaryReportConfig {

    public static final String SOLR_FIELD = "SOLR_FIELD";

    @Value("${dashboard.properties.file:./config/dashboard.properties}")
    private String dashboardPropertiesFile;

    @Value("${subprocess:false}")
    private boolean subProcess;

    private Properties dashboardProperties;

    private Logger logger = LoggerFactory.getLogger(SummaryReportConfig.class);

    @PostConstruct
    private void init() throws IOException {
        dashboardProperties = new Properties();
        if (subProcess) {
            logger.debug("Skip dashboard properties");
            return;
        }
        try {
            if (Files.exists(Paths.get(dashboardPropertiesFile))) {
                InputStream is = null;
                try {
                    is = new FileInputStream(dashboardPropertiesFile);
                    dashboardProperties.load(is);
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Failed to load Summary Report configuration",e);
        }
    }

    @Bean
    public Map<String,Object> fileAccessHistogramConfig() {
        Map<String,Object> result = new HashMap<>();
        String field= dashboardProperties.getProperty("fileAccessHistogramConfig.solr.field","LAST_MODIFIED");
        result.put(SOLR_FIELD, CatFileFieldType.valueOf(field.toUpperCase()));
        return result;
    }
}
