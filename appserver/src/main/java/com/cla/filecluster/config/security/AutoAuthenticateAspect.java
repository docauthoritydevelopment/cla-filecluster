package com.cla.filecluster.config.security;

import com.cla.filecluster.service.security.UserService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;

/**
 * Performs automatic authentication on current thread according to the AutoAuthenticate annotation placed on a method
 * @see AutoAuthenticate
 */
@Aspect
@Component
public class AutoAuthenticateAspect {

    private static final Logger logger = LoggerFactory.getLogger(AutoAuthenticateAspect.class);

    @Autowired
    private UserService userService;

    @Around("execution(@com.cla.filecluster.config.security.AutoAuthenticate * *(..)) && @annotation(autoAuthenticate)")
    public Object handle(final ProceedingJoinPoint caller, AutoAuthenticate autoAuthenticate) throws Throwable{
        Object[] result = new Object[1];
        AuthenticationHelper.doWithAuthentication(autoAuthenticate.runAs(), autoAuthenticate.replace(),
                Executors.callable(() -> {
                    try {
                        result[0] = caller.proceed();
                    } catch (Exception e) {
                        logger.error("operation failed", e);
                        throw new RuntimeException(e);
                    } catch (Throwable throwable) {
                        logger.error("operation failed {} {}", throwable.getMessage(), throwable.getStackTrace());
                        throw new RuntimeException(throwable);
                    }
                }),
                userService);
        return result[0];
    }

}
