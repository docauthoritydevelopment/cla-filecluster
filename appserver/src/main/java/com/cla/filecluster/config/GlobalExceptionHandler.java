package com.cla.filecluster.config;

import com.cla.common.domain.dto.ErrorInfo;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;

/**
 * Created by uri on 10/05/2016.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody ErrorInfo handleBadRequest(HttpServletRequest req, Exception ex) {
        if (logger.isTraceEnabled()) {
            logger.trace("Bad request exception.", ex);
        }
        else{
            logger.info("Bad request exception {}, enable TRACE for more info.", ex.getMessage());
        }
        ErrorInfo errorInfo = new ErrorInfo(req.getRequestURL().toString(), ex);
        if (ex instanceof BadRequestException) {
            errorInfo.setType(((BadRequestException)ex).getType());
        }
        return errorInfo;
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(org.springframework.web.multipart.MultipartException.class)
    public ResponseEntity<?> handle(org.springframework.web.multipart.MultipartException exception) {
        logger.error("handle->MultipartException" + exception.getMessage(), exception);
        String lookFor = "The temporary upload location";
        String message = null;
        if (exception.getMessage() != null && exception.getMessage().startsWith(lookFor)) {
            message = exception.getMessage();
        } else if (exception.getCause() != null && exception.getCause() instanceof IOException && exception.getCause().getMessage().startsWith(lookFor)) {
            message = exception.getCause().getMessage();
        }

        if (message != null) {
            String pathToRecreate = message.substring(message.indexOf("[")+1,message.indexOf("]"));
            try {
                Files.createDirectories(FileSystems.getDefault().getPath(pathToRecreate));
            } catch (IOException e) {
                logger.error(e.getMessage(),e);
                return ResponseEntity.unprocessableEntity().body("Unable to recreate deleted temp directories. Please check  "+ pathToRecreate);
            }
            return ResponseEntity.unprocessableEntity().body("Recovered from temporary error by recreating temporary directory. Please try to upload again.");
        }
        return ResponseEntity.unprocessableEntity().body("Unable to process this request.");
    }
}
