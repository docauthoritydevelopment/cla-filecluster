package com.cla.filecluster.config.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;

import javax.naming.directory.SearchControls;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Itai Marko.
 */
public class CustomLdapAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    private static final Logger logger = LoggerFactory.getLogger(CustomLdapAuthoritiesPopulator.class);

    private final SpringSecurityLdapTemplate ldapTemplate;
    private String groupSearchBase;
    private String groupSearchFilter;
    private String groupRoleAttribute;
    private String parentGroupRoleAttribute;
    private int maxLevel = 0;

    public CustomLdapAuthoritiesPopulator(ContextSource contextSource) {
        Objects.requireNonNull(contextSource, "contextSource can't be null");
        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        this.ldapTemplate = new SpringSecurityLdapTemplate(contextSource);
        this.ldapTemplate.setSearchControls(searchControls);
        this.groupRoleAttribute = "cn";
        this.parentGroupRoleAttribute = "dn";
    }

    @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData, String username) {
        String userDn = userData.getNameInNamespace();

        logger.info("Searching for groups of user {} (username={}), with filter {} in search base {}", userDn, username, groupSearchFilter, groupSearchBase);
        Set<String> resolvedGroupsHistory = new HashSet<>();
        Set<String> userGroupsNames = getGrantedAuthorities(userDn, username, 0, resolvedGroupsHistory);
        logger.info("Retrieved user groups: {}", userGroupsNames);

        //noinspection UnnecessaryLocalVariable
        List<SimpleGrantedAuthority> userGroups = userGroupsNames.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        return userGroups;
    }

    public Set<String> getGrantedAuthorities(String userDn, String username, int level, Set<String> resolvedGroupsHistory) {

        if (groupSearchBase == null) {
            logger.debug("groupSearchBase is null. Skipping group search of user {}", userDn);
            return Collections.emptySet();
        }
        if (resolvedGroupsHistory != null) {
            if (resolvedGroupsHistory.contains(userDn)) {
                if (username != null) {
                    logger.warn("Unexpected non-null value for username={}", username);
                }
                logger.debug("Circular group definition found for {}. Aborting repeated resolution.", userDn);
                return Collections.emptySet();
            } else {
                resolvedGroupsHistory.add(userDn);
            }
        }

        if (groupSearchBase.trim().length() == 0) {
            logger.debug("groupSearchBase is empty. Search will be performed from the context source base");
        }

        Set<String> userGroupsNames = ldapTemplate.searchForSingleAttributeValues(
                groupSearchBase,
                groupSearchFilter,
                (username!=null)?(new String[]{userDn, username}):(new String[]{userDn}),
                groupRoleAttribute);
        logger.debug("Retrieved groups for ({} {}): {}", userDn, Optional.ofNullable(username).orElse("-"), userGroupsNames);

        if (!userGroupsNames.isEmpty() && level < maxLevel) {
            Set<Map<String, List<String>>> userGrpAttrib = ldapTemplate.searchForMultipleAttributeValues(
                    groupSearchBase,
                    groupSearchFilter,
                    new String[]{userDn},
                    new String[]{parentGroupRoleAttribute});
            // Returned key is wrongly set to 'spring.security.ldap.dn' so searchForSingleAttributeValues() don't get it right
            Set<String> userGroupsDn = userGrpAttrib.stream()
                    .map(me -> me.entrySet().stream().findFirst().map(e -> e.getValue().get(0)).orElse(""))
                    .collect(Collectors.toSet());

            if (logger.isTraceEnabled()) {
                logger.trace("Retrieved groups (dn): {}",
                        userGrpAttrib.stream().map(me -> me.entrySet()
                                .stream().map(e -> e.getKey() + ":" + e.getValue().stream().collect(Collectors.joining("||")))
                                .collect(Collectors.joining("|", "[", "]"))).collect(Collectors.joining(";\n")));
                logger.trace("Retrieved groups (userGroupsDn) level:{}: {}", level, userGroupsDn.stream().collect(Collectors.joining("|")));
            }
            // Recursively get parents groups to get full list of user's access-tokens
            for (String gdn : userGroupsDn) {
                Set<String> parentGrps = getGrantedAuthorities(gdn, null, level + 1, resolvedGroupsHistory);
                if (!parentGrps.isEmpty()) {
                    logger.debug("Adding parent groups (level:{}): {}", level, parentGrps.stream().collect(Collectors.joining("|")));
                    userGroupsNames.addAll(parentGrps);
                }
            }
        } else if (!userGroupsNames.isEmpty()) {
            logger.warn("Max level of parents AD groups reached. level={}, current level dn={} with parent groupNames= {}",
                    level, userDn, userGroupsNames.stream().collect(Collectors.joining("|")));
        }
        return userGroupsNames;
    }

    public void setIgnorePartialResultException(boolean ignore) {
        ldapTemplate.setIgnorePartialResultException(ignore);
    }

    public String getGroupSearchBase() {
        return groupSearchBase;
    }

    public void setGroupSearchBase(String groupSearchBase) {
        this.groupSearchBase = groupSearchBase;
    }

    public String getGroupSearchFilter() {
        return groupSearchFilter;
    }

    public void setGroupSearchFilter(String groupSearchFilter) {
        this.groupSearchFilter = groupSearchFilter;
    }

    public String getGroupRoleAttribute() {
        return groupRoleAttribute;
    }

    public void setGroupRoleAttribute(String groupRoleAttribute) {
        this.groupRoleAttribute = groupRoleAttribute;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public String getParentGroupRoleAttribute() {
        return parentGroupRoleAttribute;
    }

    public void setParentGroupRoleAttribute(String parentGroupRoleAttribute) {
        this.parentGroupRoleAttribute = parentGroupRoleAttribute;
    }
}
