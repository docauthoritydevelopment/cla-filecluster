package com.cla.filecluster.config;

import com.google.common.collect.Lists;
import org.hibernate.jpa.boot.internal.EntityManagerFactoryBuilderImpl;
import org.hibernate.jpa.boot.spi.IntegratorProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EntityScan(basePackages = {
        "${entity.package.scan:com.cla.filecluster.domain.entity}",
        "${entity.package.scan:com.cla.filecluster.jobmanager.domain.entity}",
        "${entity.package.scan:com.cla.filecluster.policy.domain.entity}",
        "${entity.package.scan:com.cla.filecluster.processes.domain}"
})
@EnableJpaRepositories(basePackages = {
        "${repository.package.scan:com.cla.filecluster.repository.jpa}",
        "${repository.package.scan:com.cla.filecluster.jobmanager.repository.jpa}",
        "${repository.package.scan:com.cla.filecluster.policy.repository.jpa}",
        "${repository.package.scan:com.cla.filecluster.upgrade.operations.v_2_0.tagenhancments.repository.jpa}",
        "${repository.package.scan:com.cla.filecluster.processes.repository}"
})
@EnableTransactionManagement
public class JpaConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JpaMetadataIntegrator metadataIntegrator;


    /**
     * Customizing id integrator, which is used to store the Database object, which is used by
     * TableGenerator for generating sequences of IDs
     *
     * @return hibernatePropertiesCustomizer
     */
    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer() {
        return hibernateProperties -> hibernateProperties.put(EntityManagerFactoryBuilderImpl.INTEGRATOR_PROVIDER,
                (IntegratorProvider) () -> Lists.newArrayList(metadataIntegrator));
    }

    /*@Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder factoryBuilder) {
        LocalContainerEntityManagerFactoryBean em = factoryBuilder.dataSource(dataSource).build();
        //LocalContainerEntityManagerFactoryBean em = autoConfig.entityManagerFactory(factoryBuilder);
        Properties props = new Properties();
        props.put(EntityManagerFactoryBuilderImpl.INTEGRATOR_PROVIDER,
                (IntegratorProvider) () -> Lists.newArrayList(metadataIntegrator));
        em.setJpaProperties(props);
        return em;
    }*/

}