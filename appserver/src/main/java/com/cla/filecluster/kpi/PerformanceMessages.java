package com.cla.filecluster.kpi;

public final class PerformanceMessages {

    public static final String PERFORMANCE_MEASURE_PATTERN = ">>>> KPI(method: {}, operation: {}, line: {} ,time: {} ms)";
}
