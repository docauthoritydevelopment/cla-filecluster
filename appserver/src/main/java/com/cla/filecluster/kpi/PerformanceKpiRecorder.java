package com.cla.filecluster.kpi;

import com.cla.connector.utils.Pair;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class PerformanceKpiRecorder {

    private final static Logger logger = LoggerFactory.getLogger(PerformanceKpiRecorder.class);

    private Map<KpiRecording, Set<Pair<Long, Long>>> kpiMap = new HashMap<>();

    private ExecutorService executorService = Executors.newFixedThreadPool(50, new ThreadFactoryBuilder().setNameFormat("kpi-recorder-%d").build());

    @Value("${kpi-performance.active:false}")
    private boolean active;

    public KpiRecording startRecording(String method, String operation, int line) {
        return new KpiRecording(method, operation, line, System.currentTimeMillis(), this, Thread.currentThread().getName());
    }


    void endRecording(KpiRecording kpiRecording, long time) {
        if (!active) return;
        long took = time - kpiRecording.startTime;
        if (took > 5) {
            logger.warn(PerformanceMessages.PERFORMANCE_MEASURE_PATTERN, kpiRecording.method, kpiRecording.operation, kpiRecording.line, took);
        }
        executorService.execute(() -> {
            Set<Pair<Long, Long>> count = kpiMap.get(kpiRecording);
            if (count == null) {
                synchronized (kpiMap) {
                    count = kpiMap.get(kpiRecording);
                    if (count == null) {
                        count = Collections.synchronizedSet(new TreeSet<>(Comparator.comparing(Pair::getKey)));
                        kpiMap.put(kpiRecording, count);
                    }
                }
            }
            count.add(Pair.of(kpiRecording.startTime, time));
        });
    }

    @Scheduled(initialDelayString = "0", fixedRateString = "1000")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void printStats() {
        if (!active) return;
        synchronized (kpiMap) {
            kpiMap.entrySet().stream().filter(r -> !r.getValue().isEmpty()).sorted(Comparator.comparingLong(r -> r.getKey().startTime)).forEach(kpiRecordingPairEntry -> {
                KpiRecording kpiRecording = kpiRecordingPairEntry.getKey();
                ArrayList<Pair<Long, Long>> sortedTimeSegments = new ArrayList<>(kpiRecordingPairEntry.getValue());
                long totalTime = sortedTimeSegments.stream().mapToLong(p -> p.getValue() - p.getKey()).sum();
                logger.info(PerformanceMessages.PERFORMANCE_MEASURE_PATTERN + ", avg of {}",
                        kpiRecording.method, kpiRecording.operation, kpiRecording.line,
                        totalTime / sortedTimeSegments.size(), sortedTimeSegments.size());
            });
            kpiMap.clear();
        }
    }

    @PreDestroy
    public void destroy() {
        active = false;
        executorService.shutdown();
    }
}
