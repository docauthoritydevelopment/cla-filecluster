package com.cla.filecluster.kpi;

import java.util.Objects;

public class KpiRecording {

    final String method;
    final String operation;
    final int line;
    final long startTime;
    final String tName;
    private final PerformanceKpiRecorder performanceKpiRecorder;

    public KpiRecording(String method, String operation, int line, long startTime, PerformanceKpiRecorder performanceKpiRecorder,String tName) {
        this.method = method;
        this.operation = operation;
        this.line = line;
        this.startTime = startTime;
        this.performanceKpiRecorder = performanceKpiRecorder;
        this.tName = tName;
    }

    public void end() {
        performanceKpiRecorder.endRecording(this, System.currentTimeMillis());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KpiRecording that = (KpiRecording) o;
        return line == that.line &&
                Objects.equals(method, that.method) &&
                Objects.equals(operation, that.operation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(method, operation, line);
    }

    @Override
    public String toString() {
        return "KpiRecording{" +
                "method='" + method + '\'' +
                ", operation='" + operation + '\'' +
                ", line=" + line +
                ", startTime=" + startTime +
                '}';
    }
}
