package com.cla.filecluster.batch.writer.map;

import com.cla.connector.utils.Pair;

import java.util.Collection;
import java.util.Set;

/**
 * executor to save data to be persisted to solr and db and flush in chunks
 *
 * Created by: yael
 * Created on: 9/20/2018
 */
public interface MapBatchTaskExecutor<T> {
    boolean hasItem(T t);

    void add(Pair<Long, Long> batchTaskId, T t);

    void flush(Collection<Pair<Long, Long>> batchTaskIds);

    void clear(Collection<Pair<Long, Long>> batchTaskIds);

    void clearByTask(Collection<Long> taskIds);

    Set<Pair<Long, Long>> getPendingBatchTaskIds();
}
