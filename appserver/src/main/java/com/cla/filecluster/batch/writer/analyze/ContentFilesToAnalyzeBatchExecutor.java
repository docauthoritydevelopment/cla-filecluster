package com.cla.filecluster.batch.writer.analyze;

import com.cla.common.constants.SkipAnalysisReasonType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.files.FilesDataPersistenceService;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.*;

/**
 * executor to change file state to analyzed
 *
 * Created by: yael
 * Created on: 10/4/2018
 */
public class ContentFilesToAnalyzeBatchExecutor extends AbstractAnalyzeBatchExecutor<Pair<Long, SkipAnalysisReasonType>> {

    private FilesDataPersistenceService filesDataPersistenceService;

    public ContentFilesToAnalyzeBatchExecutor(FilesDataPersistenceService filesDataPersistenceService, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.filesDataPersistenceService = filesDataPersistenceService;
    }

    @Override
    protected void flushBatch(Collection<Pair<Long, SkipAnalysisReasonType>> batch) {
        Map<SkipAnalysisReasonType, List<Long>> map = batch.stream().
                collect(groupingBy(Pair::getValue, mapping(Pair::getKey, toList())));

        map.forEach((type, list) -> filesDataPersistenceService.updateStateToAnalyzed(list, type));
    }
}
