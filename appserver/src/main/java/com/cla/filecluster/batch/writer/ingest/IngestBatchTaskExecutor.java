package com.cla.filecluster.batch.writer.ingest;

import java.util.Collection;
import java.util.List;

public interface IngestBatchTaskExecutor<T> {

    void add(Long taskId, T t);

    void flush(Collection<Long> taskIds);

    void clear(Collection<Long> taskIds);

    Collection<Long> getTasks();
}
