package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.service.categories.FileCatService;
import org.apache.solr.common.SolrInputDocument;

import java.util.List;

public class CatFileBatchExecutor extends AbstractIngestBatchExecutor<SolrInputDocument> {

    private FileCatService fileCatService;

    public CatFileBatchExecutor(FileCatService fileCatService, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.fileCatService = fileCatService;
    }

    @Override
    protected void flushBatch(List<SolrInputDocument> batch) {
        fileCatService.saveAll(batch);
    }
}
