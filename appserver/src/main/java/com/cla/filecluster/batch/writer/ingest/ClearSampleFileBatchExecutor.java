package com.cla.filecluster.batch.writer.ingest;

import com.cla.common.constants.SolrCommitType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.files.ContentMetadataService;

import java.util.List;

public class ClearSampleFileBatchExecutor extends AbstractIngestBatchExecutor<Pair<Long, Long>> {

    private final ContentMetadataService contentMetadataService;

    public ClearSampleFileBatchExecutor(ContentMetadataService contentMetadataService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.contentMetadataService = contentMetadataService;
    }

    @Override
    protected void flushBatch(List<Pair<Long, Long>> contentIdAndFileIdPairs) {
        contentMetadataService.clearSampleFileIdIfNeededByIdAndClaFileIdPairs(contentIdAndFileIdPairs, SolrCommitType.NONE);
    }
}
