package com.cla.filecluster.batch.writer.analyze;

import com.cla.filecluster.domain.entity.pv.MissedFileMatch;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.service.crawler.analyze.AnalyzerDataService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.cla.common.constants.DbDatePattern.CREATED_ON_DB_PATTERN;

/**
 * executor to persist missed matches on DB
 * <p>
 * Created by: Nadav
 * Created on: 16/12/2018
 */
@Component
public class FileMissedMatchBatchExecutor extends AbstractAnalyzeBatchExecutor<MissedFileMatch> {

    public static final String MISSED_FILE_MATCH_BATCH_INSERT_STATMENT = "INSERT INTO `missed_file_match` (`id`, `file1_id`, `file2_id`, `created_ondb`, `reason`,`processed`) VALUES %s";
    public static final String MISSED_FILES_MATCH_BATCH_VALUES_PATTERN = "(%d,%d,%d,'%s',%d,%b)";

    private AnalyzerDataService analyzerDataService;
    private DBTemplateUtils dbTemplateUtils;
    private SequenceIdGenerator sequenceIdGenerator;


    @Autowired
    public FileMissedMatchBatchExecutor(AnalyzerDataService analyzerDataService,
                                        SequenceIdGenerator sequenceIdGenerator,
                                        DBTemplateUtils dbTemplateUtils,
                                        @Value("${ingest.batch.db-flush-partition-size:1000}")
                                                int innerPartitionSize,
                                        @Value("${ingest.batch.flush-partition-concurrency:50}")
                                                int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.analyzerDataService = analyzerDataService;
        this.sequenceIdGenerator = sequenceIdGenerator;
        this.dbTemplateUtils = dbTemplateUtils;
    }

    @Override
    protected void flushBatch(Collection<MissedFileMatch> batch) {
        dbTemplateUtils.doInTransaction(jdbcTemplate ->
                jdbcTemplate.execute(batchedFiledMissedMatchStatement(batch)));
        batch.forEach(ms -> analyzerDataService.incMissedMatchesCounter());
    }

    private String batchedFiledMissedMatchStatement(Collection<MissedFileMatch> batch) {
        String createdOnDbStr = CREATED_ON_DB_PATTERN.format(new Date());
        String batchedValues = batch.stream()
                .map(msFile -> String.format(
                        MISSED_FILES_MATCH_BATCH_VALUES_PATTERN,
                        sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.MISSED_FILE_MATCH),
                        msFile.getFile1Id(),
                        msFile.getFile2Id(),
                        createdOnDbStr,
                        msFile.getReason().ordinal(),
                        false))
                .collect(Collectors.joining(","));
        return String.format(MISSED_FILE_MATCH_BATCH_INSERT_STATMENT, batchedValues);
    }


}
