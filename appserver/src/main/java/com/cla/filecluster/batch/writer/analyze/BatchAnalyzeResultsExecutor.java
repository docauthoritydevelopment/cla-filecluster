package com.cla.filecluster.batch.writer.analyze;

import com.cla.common.utils.ConcurrencyUtils;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.service.crawler.analyze.ContentGroupsCache;
import com.cla.filecluster.service.files.managers.FileGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * executor to save data to be persisted to solr and db and flush in chunks
 * <p>
 * Created by: yael
 * Created on: 10/4/2018
 */
@Component
public class BatchAnalyzeResultsExecutor {

    @Autowired
    private Map<AnalyzeBatchTaskType, AnalyzeBatchTaskExecutor> analyzeBatchExecutorsRegistry;

    private ExecutorService batchRepoExecutor;

    @Autowired
    private ContentGroupsCache contentGroupsCache;

    @Autowired
    private MissedMatchHandler missedMatchHandler;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private FileGroupService fileGroupService;

    private boolean active = true;

    private Map<Long, Set<String>> dirtyFileGroups = new HashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(BatchAnalyzeResultsExecutor.class);

    public void collectDirtyFileGroups(Long taskId, String groupId) {
        Set<String> dfg = dirtyFileGroups.getOrDefault(taskId, new HashSet<>());
        dfg.add(groupId);
        dirtyFileGroups.put(taskId, dfg);
    }

    private void flushDirtyFileGroups(Collection<Long> taskIds) {
        Set<String> dfg = new HashSet<>();
        taskIds.forEach(taskId -> {
            if (dirtyFileGroups.containsKey(taskId)) {
                Set<String> taskDfg = dirtyFileGroups.remove(taskId);
                dfg.addAll(taskDfg);
            }
        });
        if (!dfg.isEmpty()) {
            fileGroupService.markGroupAsDirtyByIds(dfg);
        }
    }

    @PostConstruct
    private void init() {
        batchRepoExecutor = Executors.newFixedThreadPool(analyzeBatchExecutorsRegistry.keySet().size());
    }

    public <T> void collect(AnalyzeBatchTaskType batchTaskType, Long taskId, T t) {
        analyzeBatchExecutorsRegistry.get(batchTaskType).add(taskId, t);
    }

    public synchronized void flush(Collection<Long> taskIdsDone, Collection<Long> taskIdsFailed) {
        if (!active) return;
        List<Long> allTasks = new ArrayList<>();
        allTasks.addAll(taskIdsDone);
        allTasks.addAll(taskIdsFailed);
        missedMatchHandler.acquireLock();
        try {
            ConcurrencyUtils.executeTasks(analyzeBatchExecutorsRegistry.entrySet(),
                    e -> e.getValue().flush(allTasks),
                    batchRepoExecutor
            );
            flushDirtyFileGroups(allTasks);

            if (taskIdsDone.size() > 0) {
                long start = System.currentTimeMillis();
                logger.debug("Start: update {} tasks to done", taskIdsDone.size());
                solrTaskService.endTaskForContents(taskIdsDone);
                logger.debug("Done: update {} tasks to done in {} ms", taskIdsDone.size(), System.currentTimeMillis() - start);
            }

            // clear content cache for task
            contentGroupsCache.handleTasksDone(allTasks);


        } catch (Throwable t) {
            logger.error(String.format("Failed to flush results of %d tasks", allTasks.size()), t);
        } finally {
            clearTaskData(allTasks);
            missedMatchHandler.releaseLock();
        }
    }

    public synchronized void clearTaskData(Collection<Long> taskIds) {
        if (taskIds == null || taskIds.isEmpty()) return;

        batchRepoExecutor.submit(() -> {
            analyzeBatchExecutorsRegistry.values().forEach(collector -> {
                collector.clear(taskIds);
            });
        });
    }

    @PreDestroy
    public void destroy() {
        active = false;
        batchRepoExecutor.shutdown();
    }
}
