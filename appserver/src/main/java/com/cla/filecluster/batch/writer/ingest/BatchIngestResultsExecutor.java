package com.cla.filecluster.batch.writer.ingest;

import com.cla.common.utils.ConcurrencyUtils;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class BatchIngestResultsExecutor {

    @Autowired
    private Map<IngestBatchTaskType, IngestBatchTaskExecutor> ingestBatchExecutorsRegistry;

    private ExecutorService batchRepoExecutor;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private SolrTaskService solrTaskService;

    private static final Logger logger = LoggerFactory.getLogger(BatchIngestResultsExecutor.class);

    private boolean active = true;

    @PostConstruct
    private void init() {
        batchRepoExecutor = Executors.newFixedThreadPool(ingestBatchExecutorsRegistry.keySet().size());
    }

    public <T> void collect(IngestBatchTaskType batchTaskType, Long taskId, T t) {
        ingestBatchExecutorsRegistry.get(batchTaskType).add(taskId, t);
    }

    private Collection<Long> getTaskIds(IngestBatchTaskType batchTaskType) {
        return ingestBatchExecutorsRegistry.get(batchTaskType).getTasks();
    }

    public synchronized void flush(Collection<Long> taskIds) {
        if (!active) return;
        try {
            ConcurrencyUtils.executeTasks(ingestBatchExecutorsRegistry.entrySet(),
                    e -> e.getValue().flush(taskIds),
                    batchRepoExecutor
            );

            List<Long> doneTasks = new ArrayList<>(taskIds);
            if (!doneTasks.isEmpty()) {
                long start = System.currentTimeMillis();
                filesDataPersistenceService.softCommitSolrCF();
                logger.debug("Start: update {} tasks to done", doneTasks.size());
                solrTaskService.endTaskForFiles(doneTasks);
                logger.debug("Done: update {} tasks to done in {} ms", doneTasks.size(), System.currentTimeMillis() - start);
            }
        } catch (Throwable t) {
            logger.error(String.format("Failed to flush results of %d tasks", taskIds.size()), t);
        } finally {
            clearTaskData(taskIds);
            long start = System.currentTimeMillis();
            filesDataPersistenceService.softCommitSolr();
            long end = System.currentTimeMillis();
            logger.info("ingest - soft commit to solr: took {} ms", end - start);
        }
    }

    public synchronized void clearTaskData(Collection<Long> taskIds) {
        if (taskIds == null || taskIds.isEmpty()) return;

        batchRepoExecutor.submit(() -> {
            ingestBatchExecutorsRegistry.values().forEach(collector -> {
                collector.clear(taskIds);
            });
        });
    }

    @PreDestroy
    public void destroy() {
        active = false;
        batchRepoExecutor.shutdown();
    }
}
