package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.repository.solr.SolrGrpHelperRepository;
import org.apache.solr.common.SolrInputDocument;

import java.util.List;

public class GrpHelperBatchExecutor extends AbstractIngestBatchExecutor<SolrInputDocument> {

    private SolrGrpHelperRepository solrGrpHelperRepository;

    public GrpHelperBatchExecutor(SolrGrpHelperRepository solrGrpHelperRepository, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.solrGrpHelperRepository = solrGrpHelperRepository;
    }

    @Override
    protected void flushBatch(List<SolrInputDocument> batch) {
        solrGrpHelperRepository.saveAll(batch);
    }
}
