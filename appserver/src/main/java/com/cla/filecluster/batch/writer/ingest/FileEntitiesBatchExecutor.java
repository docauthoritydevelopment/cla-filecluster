package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.service.categories.FileCatService;

import java.util.List;

public class FileEntitiesBatchExecutor extends AbstractIngestBatchExecutor<SolrFileEntity> {

    private FileCatService fileCatService;

    public FileEntitiesBatchExecutor(FileCatService fileCatService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.fileCatService = fileCatService;
    }

    @Override
    protected void flushBatch(List<SolrFileEntity> batch) {
        fileCatService.saveEntities(batch);
    }
}
