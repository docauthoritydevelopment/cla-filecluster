package com.cla.filecluster.batch.writer.analyze;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.jobmanager.service.JobManagerService;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * executor to update job success task counter
 *
 * Created by: yael
 * Created on: 10/16/2018
 */
public class AnalyzeJobSuccessCounterBatchExecutor extends AbstractAnalyzeBatchExecutor<Pair<Long, Integer>> {

    private JobManagerService jobManagerService;

    public AnalyzeJobSuccessCounterBatchExecutor(int innerPartitionSize, int partitionUpdateConcurrency, JobManagerService jobManagerService) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.jobManagerService = jobManagerService;
    }

    @Override
    protected void flushBatch(Collection<Pair<Long, Integer>> batch) {
        Map<Long, Integer> perJob = new HashMap();

        batch.forEach(pair -> {
            Integer val = perJob.getOrDefault(pair.getKey(), 0);
            val += pair.getValue();
            perJob.put(pair.getKey(), val);
        });

        logger.trace("increment success counters for jobs {}", perJob);

        perJob.forEach((jobId, counter) -> {
            jobManagerService.incJobCounters(jobId, counter, 0);
        });
    }
}
