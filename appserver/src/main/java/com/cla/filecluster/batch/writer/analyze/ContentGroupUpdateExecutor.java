package com.cla.filecluster.batch.writer.analyze;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;

import java.util.Collection;
import java.util.List;

/**
 * Created by: yael
 * Created on: 1/10/2019
 */
public class ContentGroupUpdateExecutor extends AbstractAnalyzeBatchExecutor<Pair<Long, String>> {

    private SolrContentMetadataRepository solrContentMetadataRepository;

    public ContentGroupUpdateExecutor(int innerPartitionSize, int partitionUpdateConcurrency, SolrContentMetadataRepository solrContentMetadataRepository) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.solrContentMetadataRepository = solrContentMetadataRepository;
    }

    @Override
    protected void flushBatch(Collection<Pair<Long, String>> batch) {
        solrContentMetadataRepository.updateGroupsForContents(batch, true);
    }
}
