package com.cla.filecluster.batch.writer.analyze;

import com.cla.common.utils.ConcurrencyUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * executor to save data to be persisted to solr and db and flush in chunks
 *
 * Created by: yael
 * Created on: 10/4/2018
 */
public abstract class AbstractAnalyzeBatchExecutor <T> implements AnalyzeBatchTaskExecutor<T> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    private Map<Long, List<T>> taskIdToBatchItemMap = Maps.newHashMap();

    private Object mutex = new Object();

    private final int innerPartitionSize;

    private final ExecutorService innerPartitionBatchExecutor;

    private boolean active = true;

    public AbstractAnalyzeBatchExecutor(int innerPartitionSize, int partitionUpdateConcurrency) {
        this.innerPartitionSize = innerPartitionSize;
        innerPartitionBatchExecutor = Executors.newFixedThreadPool(partitionUpdateConcurrency,
                new ThreadFactoryBuilder().setNameFormat("inner-batch-partition-updater-%d").build());
    }

    @Override
    public void add(Long taskId, T t) {
        List<T> items = taskIdToBatchItemMap.get(taskId);
        if (items == null) {
            synchronized (mutex) {
                items = taskIdToBatchItemMap.get(taskId);
                if (items == null) {
                    items = new LinkedList<>();
                    taskIdToBatchItemMap.put(taskId, items);
                }
            }
        }

        synchronized (mutex) {
            items.add(t);
        }
    }

    @Override
    public void flush(Collection<Long> taskIds) {
        List<T> batch;
        synchronized (mutex) {
            batch = taskIdToBatchItemMap.entrySet().stream()
                    .filter(e -> taskIds.contains(e.getKey()))
                    .flatMap(e -> e.getValue().stream())
                    .collect(Collectors.toList());
        }
        if (!batch.isEmpty()) {
            try {
                long start = System.currentTimeMillis();
                logger.debug("Start: Flushing batch of {}", batch.size());
                flushByPartitions(batch);
                logger.debug("Done: Flushing batch of {} in {} ms", batch.size(), System.currentTimeMillis() - start);
            } catch (InterruptedException e) {
                logger.info("Flush operation interrupted", e.getMessage());
            }
        }
    }

    private void flushByPartitions(List<T> batch) throws InterruptedException {
        if (!active) return;
        List<List<T>> partitionList = Lists.partition(batch, innerPartitionSize);
        ConcurrencyUtils.executeTasks(partitionList, ts -> {
                    long innerStart = System.currentTimeMillis();
                    logger.debug("Start: Flushing by inner partition of {}", ts.size());
                    flushBatch(ts);
                    logger.debug("Done: Flushing by inner partition of {} in {} ms", ts.size(), System.currentTimeMillis() - innerStart);
                },
                innerPartitionBatchExecutor
        );
    }

    @Override
    public void clear(Collection<Long> taskIds) {
        taskIds.forEach(taskIdToBatchItemMap::remove);
    }

    protected abstract void flushBatch(Collection<T> batch);

    @PreDestroy
    public void destroy() {
        active = false;
        innerPartitionBatchExecutor.shutdown();
    }
}
