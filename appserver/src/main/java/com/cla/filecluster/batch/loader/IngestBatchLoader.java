package com.cla.filecluster.batch.loader;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.messages.ProcessingPhaseMessage;
import com.cla.common.utils.ConcurrencyUtils;
import com.cla.common.utils.DaMessageConversionUtils;
import com.cla.common.utils.FileSizeUnits;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import com.cla.filecluster.pvs.PvsScaler;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.filecluster.util.executors.ThreadPoolBlockingExecutor;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

@Service
public class IngestBatchLoader implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(IngestBatchLoader.class);

    private final List<IngestResultMessage> resultMessagesSubmitted = new ArrayList<>(10000);

    @Value("${ingest.batch.results-messages.flush-interval-in-millis:15000}")
    private long flushInterval;

    @Value("${ingest.batch.results-messages.flush-size-in-kb:20000}")
    private int flushSize;

    @Value("${ingest.batch.worker-partition-size:100}")
    private int innerPartitionSize;

    @Value("${ingest.batch.queue.bulk-limit:5}")
    private int batchQueueLimit;

    private LinkedBlockingQueue<List<IngestResultMessage>> batchQeueue;

    private ExecutorService ingestBatchLoaderQueueWorker;

    private boolean active = true;

    @Autowired
    private BlockingExecutor ingestBlockingExecutor;

    @Autowired
    private IngestBatchStorePopulator ingestBatchStorePopulator;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Autowired
    private IngestBatchResultsProcessor ingestBatchResultsProcessor;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private PvsScaler pvsScaler;

    private BlockingExecutor ingestBatchExecutor;

    private boolean flushed = false;

    private double currentSubmittedSize;

    private boolean isSchedulingActive = false;

    private TimeSource timeSource = new TimeSource();

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @PostConstruct
    private void init() {
        ingestBatchExecutor = new ThreadPoolBlockingExecutor("ingest-batch", batchQueueLimit, 0, 1, true);
        batchQeueue = new LinkedBlockingQueue<>(batchQueueLimit);
        ingestBatchLoaderQueueWorker = Executors.newSingleThreadExecutor(
                new ThreadFactoryBuilder().setNameFormat("ingest-batch-executor-%d").build());
        ingestBatchLoaderQueueWorker.execute(() -> {
            while (active) {
                try {
                    List<IngestResultMessage> ingestResultsToProcess = batchQeueue.poll(1, TimeUnit.SECONDS);
                    if (ingestResultsToProcess == null) {
                        continue;
                    }

                    pvsScaler.scale();

                    logger.debug("Batch loading Queue tail: " + batchQeueue.size());
                    if (!ingestResultsToProcess.isEmpty()) {
                        executeBatch(ingestResultsToProcess);
                    }
                } catch (InterruptedException e) {
                    logger.error("Batch queue poller interrupted, probably stopped...");
                }
            }
        });
    }

    private void executeBatch(List<IngestResultMessage> ingestResultsToProcess) {
        try {
            if (!ingestResultsToProcess.isEmpty()) {
                long start = timeSource.currentTimeMillis();
                logger.info("Start: populate ingest batch store for {} ingest results", ingestResultsToProcess.size());
                IngestBatchStore ingestBatchStore = ingestBatchStorePopulator.populateStore(ingestResultsToProcess);
                logger.info("Done: populate ingest batch store for {} ingest results in {} ms",
                        ingestResultsToProcess.size(), timeSource.millisSince(start));

                start = timeSource.currentTimeMillis();
                logger.info("Start: Processing ingest batch results for {} ingest results", ingestResultsToProcess.size());
                ConcurrencyUtils.executeTasks(Lists.partition(ingestResultsToProcess, innerPartitionSize),
                        messages ->
                                messages.forEach(ir -> remoteMediaProcessingService.consumeIngestResult(ir, ingestBatchStore)),
                        ingestBlockingExecutor
                );
                logger.info("Done: Processing ingest batch results for {} ingest results in {} ms",
                        ingestResultsToProcess.size(), timeSource.millisSince(start));

                if (ingestBatchStore.getDoneTasks().size() != ingestResultsToProcess.size()) {
                    logger.warn("{} Messages lost... ", ingestResultsToProcess.size() - ingestBatchStore.getDoneTasks().size());
                    if (logger.isDebugEnabled()) {
                        logger.debug("ingestResultsToProcess: {}", ingestResultsToProcess.stream()
                                .map(ProcessingPhaseMessage::getTaskId).map(l->l.toString()).collect(Collectors.joining(",")));
                        logger.debug("ingestBatchStore.getDoneTasks {}", ingestBatchStore.getDoneTasks());
                    }
                }
                ingestBatchResultsProcessor.flush(ingestBatchStore.getDoneTasks(),
                        ingestBatchStore.getDoneTasksJobCounter(), ingestBatchStore.getFailTasksJobCounter());
            }
        } catch (Throwable t) {
            logger.error(String.format("Failed to process batch of %d result messages", ingestResultsToProcess.size()), t);
        }
    }


    public void submit(String data) {
        synchronized (resultMessagesSubmitted) {
            try {
                double dataKbSize = FileSizeUnits.BYTE.toKilobytes(data.getBytes().length);
                if (currentSubmittedSize + dataKbSize > flushSize) {
                    logger.info("Reached submitted {} kb, flushing {} tasks", currentSubmittedSize, resultMessagesSubmitted.size());
                    flushToBatchQueue();
                    flushed = true;
                    currentSubmittedSize = dataKbSize;
                } else {
                    currentSubmittedSize += dataKbSize;
                }
                KpiRecording kpiRecording = performanceKpiRecorder.startRecording("ingestResultListener",
                        "DaMessageConversionUtils.Ingest.deserializeResultMessage(data)", 111);
                IngestResultMessage ingestResultMessage = DaMessageConversionUtils.Ingest.deserializeResultMessage(data);
                kpiRecording.end();
                if (ingestResultMessage.getRequestId() != null) {
                    remoteMediaProcessingService.consumeIngestResult(ingestResultMessage);
                } else {
                    resultMessagesSubmitted.add(ingestResultMessage);
                }
            } catch (Throwable t) {
                logger.error(String.format("Failed deserialize result message %s", data), t);
            }
        }
    }

    @Scheduled(initialDelayString = "0", fixedRateString = "${ingest.batch.results-messages.flush-interval-in-millis:10000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    private void loadResultMessageData() {
        if (!isSchedulingActive) {
            return;
        }
        synchronized (resultMessagesSubmitted) {
            if (resultMessagesSubmitted.isEmpty() || flushed) {
                flushed = false;
                return;
            }
            logger.info("Interval passed, submitting {} tasks", resultMessagesSubmitted.size());
            flushToBatchQueue();
            currentSubmittedSize = 0;
        }
    }

    private void flushToBatchQueue() {
        ArrayList<IngestResultMessage> batch = new ArrayList<>(resultMessagesSubmitted);
        resultMessagesSubmitted.clear();
        try {
            batchQeueue.put(batch);
        } catch (InterruptedException e) {
            logger.error("Ingest batch loader interrupted", e);
        }
    }

    @PreDestroy
    private void destroy() throws InterruptedException {
        logger.info("Shutting down...");
        active = false;
        ingestBatchLoaderQueueWorker.shutdown();
        ingestBatchLoaderQueueWorker.awaitTermination(10, TimeUnit.SECONDS);
        logger.info("Terminated");
    }

    @Component
    static class IngestBatchStorePopulator {

        @Autowired
        private BlockingExecutor ingestBatchStorePopulatorExecutor;

        @Autowired
        private JobManagerService jobManager;

        @Autowired
        private FileCatService fileCatService;

        @Autowired
        private ContentMetadataService contentMetadataService;

        @Autowired
        private DocFolderService docFolderService;

        @Autowired
        private GroupsService groupsService;

        @AutoAuthenticate
        public IngestBatchStore populateStore(Collection<IngestResultMessage> ingestResultsToProcess) throws ExecutionException, InterruptedException {
            IngestBatchStore ingestBatchStore = new IngestBatchStore();

            List<Long> claFileIds = ingestResultsToProcess.stream()
                    .filter(ir -> ir.getClaFileId() != null)
                    .map(IngestResultMessage::getClaFileId)
                    .collect(Collectors.toList());
            List<SolrFileEntity> entities = fileCatService.findByFileIds(claFileIds);
            List<ClaFile> claFiles = fileCatService.fromEntities(entities);
            ingestBatchStore.addClaFiles(claFiles);
            ingestBatchStore.addFileEntities(entities);

            CompletableFuture.allOf(
                    CompletableFuture.runAsync(() -> fillTasks(ingestBatchStore, ingestResultsToProcess), ingestBatchStorePopulatorExecutor),
                    CompletableFuture.runAsync(() -> fillContentMetadatas(ingestBatchStore, entities), ingestBatchStorePopulatorExecutor),
                    CompletableFuture.runAsync(() -> fillDocFolderByPaths(ingestBatchStore, entities), ingestBatchStorePopulatorExecutor),
                    CompletableFuture.runAsync(() -> fillFileGroups(ingestBatchStore, entities), ingestBatchStorePopulatorExecutor),
                    CompletableFuture.runAsync(() -> fillIdenticalContents(ingestBatchStore, ingestResultsToProcess), ingestBatchStorePopulatorExecutor))
                    .get();

            return ingestBatchStore;
        }

        private void fillIdenticalContents(IngestBatchStore ingestBatchStore, Collection<IngestResultMessage> ingestResultsToProcess) {
            // Take messages that has sig and fileSize
            List<IngestResultMessage> potentialDuplicateMessages = ingestResultsToProcess.stream()
                    .filter(ir -> ir.getFileProperties() != null && ir.getFileProperties().getContentSignature() != null
                            && ir.getFileProperties().getFileSize() != null && ir.getFileProperties().getFileSize() > 0)
                    .collect(Collectors.toList());

            // Map them to signature and size pairs and verify if duplicate already exists in ContentMetadataRepo
            List<Pair<String, Long>> sigAndSizePairs = potentialDuplicateMessages.stream()
                    .map(ir -> Pair.of(ir.getFileProperties().getContentSignature(), ir.getFileProperties().getFileSize()))
                    .collect(Collectors.toList());
            if (logger.isTraceEnabled()) {
                logSubmitDupRequestInRepo(potentialDuplicateMessages);
            }
            Map<Pair<String, Long>, List<SolrContentMetadataEntity>> identicalContentMap = sigAndSizePairs.size() > 0 ?
                    contentMetadataService.findIdenticals(sigAndSizePairs) : new HashMap<>();
            if (logger.isTraceEnabled()) {
                logDupResultsInRepo(identicalContentMap);
            }

            // Take the results that did not return from repo and verify if in batch itself there are duplicates
            List<IngestResultMessage> potentialDuplicateMessagesInBatch = potentialDuplicateMessages.stream()
                    .filter(ir -> identicalContentMap.containsKey(Pair.of(ir.getFileProperties().getContentSignature(), ir.getFileProperties().getFileSize())))
                    .filter(ir -> identicalContentMap.get(Pair.of(ir.getFileProperties().getContentSignature(), ir.getFileProperties().getFileSize())).isEmpty())
                    .collect(Collectors.toList());
            if (logger.isTraceEnabled()) {
                logSubmitDupRequestsInBatch(potentialDuplicateMessages);
            }
            Map<Pair<String, Long>, Long> identicalContentMapInBatch = findIdenticalInBatch(ingestBatchStore, potentialDuplicateMessagesInBatch);
            if (logger.isTraceEnabled()) {
                logDupResultsInBatch(identicalContentMapInBatch);
            }

            // Update ingest batch store with repo duplicates and batch duplicates
            ingestBatchStore.addIdenticals(identicalContentMap, identicalContentMapInBatch);
        }

        private void logDupResultsInBatch(Map<Pair<String, Long>, Long> identicalContentMapInBatch) {
            identicalContentMapInBatch.forEach((k, v) ->
                    logger.trace("Result in batch for: sig={}, fileSize={}, cm=(id:{})", k.getKey(), k.getValue(), v));
        }

        private void logSubmitDupRequestsInBatch(List<IngestResultMessage> potentialDuplicateMessages) {
            potentialDuplicateMessages.forEach(ir ->
                    logger.trace("Checking duplicate messages in batch for: fileId={}, cmdId={}, sig={}, fileSize={}",
                            ir.getClaFileId(), ir.getContentMetadataId(), ir.getFileProperties().getContentSignature(),
                            ir.getFileProperties().getFileSize()));
        }

        private void logDupResultsInRepo(Map<Pair<String, Long>, List<SolrContentMetadataEntity>> identicalContentMap) {
            identicalContentMap.forEach((k, v) ->
                    v.forEach(cm -> {
                        logger.trace("Result for sig={}, fileSize={}, cm=(id:{}, sig={}, fileSize={})",
                                k.getKey(), k.getValue(), cm.getId(), cm.getContentSignature(), cm.getSize());
                    }));
        }

        private void logSubmitDupRequestInRepo(List<IngestResultMessage> potentialDuplicateMessages) {
            potentialDuplicateMessages.forEach(ir ->
                    logger.trace("Submitting request to repo: fileId={}, sig={}, fileSize={}",
                            ir.getClaFileId(), ir.getFileProperties().getContentSignature(),
                            ir.getFileProperties().getFileSize()));
        }

        private Map<Pair<String, Long>, Long> findIdenticalInBatch(IngestBatchStore ingestBatchStore,
                                                                   Collection<IngestResultMessage> ingestResultsToProcess) {
            Map<Pair<String, Long>, Long> result = Maps.newHashMap();
            ingestResultsToProcess.stream()
                    .filter(ir -> ir.getContentMetadataId() == null)
                    .forEach(ir -> {
                        Pair<String, Long> pair = Pair.of(ir.getFileProperties().getContentSignature(), ir.getFileProperties().getFileSize());
                        SolrFileEntity file = ingestBatchStore.getFileEntity(ir.getClaFileId());
                        Long id = result.get(pair);
                        if (id == null) { // Place dummy id to remove it later when done filtering
                            result.put(pair, -1L);
                        }
                        // Meaning there is a duplicate, place the first cla file contentMetadataId as the duplicate reference
                        else if (id.equals(-1L)) {
                            if (file.getContentId() == null) {   // if no content id for file then generate it
                                file.setContentId(String.valueOf(contentMetadataService.getNextAvailableId()));
                            }
                            result.put(pair, Long.valueOf(file.getContentId()));
                        }
                    });

            return result.entrySet().stream()
                    .filter(e -> e.getValue() != -1) // Remove -1 ids
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        }

        private void fillTasks(IngestBatchStore ingestBatchStore, Collection<IngestResultMessage> resultMessages) {
            List<Long> taskIds = resultMessages.stream()
                    .map(ProcessingPhaseMessage::getTaskId)
                    .collect(Collectors.toList());
            Lists.partition(taskIds, 1000)
                    .forEach(p -> {
                        List<SimpleTask> tasksByIds = jobManager.getTasksByIds(p);
                        ingestBatchStore.addTasks(tasksByIds);
                    });
        }

        private void fillFileGroups(IngestBatchStore ingestBatchStore, List<SolrFileEntity> entities) {
            List<String> groupIds = Lists.newLinkedList();
            entities.forEach(file -> {
                Optional.ofNullable(file.getAnalysisGroupId()).ifPresent(groupIds::add);
                Optional.ofNullable(file.getUserGroupId()).ifPresent(groupIds::add);
            });

            if (!groupIds.isEmpty()) {
                List<FileGroup> fileGroups = groupsService.findByIds(groupIds);
                ingestBatchStore.addFileGroups(fileGroups);
            }
        }

        private void fillDocFolderByPaths(IngestBatchStore ingestBatchStore, List<SolrFileEntity> entities) {
            if (!entities.isEmpty()) {
                List<SolrFolderEntity> folders = docFolderService.getFoldersByEntities(entities);
                List<DocFolder> docFolders = folders.stream().map(DocFolderService::getFromEntity).collect(Collectors.toList());
                ingestBatchStore.addDocFolders(docFolders);
                ingestBatchStore.addFolderEntities(folders);
            }
        }

        private void fillContentMetadatas(IngestBatchStore ingestBatchStore, List<SolrFileEntity> entity) {
            List<Long> contentMetadataIds = entity.stream()
                    .filter(file -> !Strings.isNullOrEmpty(file.getContentId()))
                    .map(file -> Long.parseLong(file.getContentId()))
                    .collect(Collectors.toList());

            if (!contentMetadataIds.isEmpty()) {
                List<SolrContentMetadataEntity> solrContentMetadataEntities = contentMetadataService.findByIds(contentMetadataIds);
                ingestBatchStore.addContentMetadataEntities(solrContentMetadataEntities);
                List<ContentMetadata> contentMetadatas = SolrContentMetadataRepository.fromEntity(solrContentMetadataEntities);
                ingestBatchStore.addContentMetadatas(contentMetadatas);
            }
        }
    }
}
