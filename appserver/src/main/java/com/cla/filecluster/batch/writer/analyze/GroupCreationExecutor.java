package com.cla.filecluster.batch.writer.analyze;

import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.crawler.analyze.NewFileGroupsCache;

import java.util.ArrayList;
import java.util.Collection;

public class GroupCreationExecutor extends AbstractAnalyzeBatchExecutor<SolrFileGroupEntity> {

    private NewFileGroupsCache newFileGroupsCache;
    private SolrFileGroupRepository fileGroupRepository;

    public GroupCreationExecutor(int innerPartitionSize, int partitionUpdateConcurrency,
                                 NewFileGroupsCache newFileGroupsCache, SolrFileGroupRepository fileGroupRepository) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.newFileGroupsCache = newFileGroupsCache;
        this.fileGroupRepository = fileGroupRepository;
    }

    @Override
    public void add(Long taskId, SolrFileGroupEntity fileGroup) {
        super.add(taskId, fileGroup);
        newFileGroupsCache.add(fileGroup);
    }

    @Override
    protected void flushBatch(Collection<SolrFileGroupEntity> batch) {
        try {
            AuthenticationHelper.doWithAuthentication(
                    AutoAuthenticate.ADMIN, false, () -> {
                        fileGroupRepository.saveEntities(new ArrayList(batch));
                        fileGroupRepository.softCommitNoWaitFlush();
                    },
                    null);

        } finally {
            newFileGroupsCache.removeAll(batch);
        }

    }
}
