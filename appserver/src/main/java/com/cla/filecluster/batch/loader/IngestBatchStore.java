package com.cla.filecluster.batch.loader;

import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import io.vertx.core.impl.ConcurrentHashSet;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class IngestBatchStore {

    private Map<Long, ClaFile> claFilesMap = new HashMap<>();
    private Map<Long, ContentMetadata> contentMetadataMap = new HashMap<>();
    private Table<Long, String, SolrFolderEntity> foldersTable = HashBasedTable.create();
    private Map<String, FileGroup> fileGroupsMap = new HashMap<>();
    private Map<Long, SimpleTask> taskIdsMap = new HashMap<>();
    private Map<Long, DocFolder> docFoldersByIdMap = new HashMap<>();
    private Map<Long, SolrFolderEntity> docFoldersEntityByIdMap = new HashMap<>();
    private Map<Pair<String, Long>, List<SolrContentMetadataEntity>> identicalContentMap = new HashMap<>();
    private Map<Long, SolrFileEntity> entitiesMap = new HashMap<>();
    private Map<Long, SolrContentMetadataEntity> contentMetadataEntitiesMap = new HashMap<>();
    private Map<Pair<String, Long>, Long> identicalContentMapInBatch = new HashMap<>();
    private List<Long> doneTasks = Collections.synchronizedList(new LinkedList<>());
    private Set<Long> fileIdsWithDuplicateContents = new ConcurrentHashSet<>();
    private Map<Long, Integer> doneTasksJobCounter = new ConcurrentHashMap<>();
    private Map<Long, Integer> failTasksJobCounter = new ConcurrentHashMap<>();

    void addClaFiles(List<ClaFile> claFiles) {
        claFiles.forEach(claFile -> claFilesMap.put(claFile.getId(), claFile));
    }

    public void addContentMetadatas(List<ContentMetadata> contentMetadatas) {
        contentMetadatas.forEach(contentMetadata -> contentMetadataMap.put(contentMetadata.getId(), contentMetadata));
    }

    void addFolderEntities(List<SolrFolderEntity> folders) {
        folders.forEach(solrFolderEntity -> {
                foldersTable.put(solrFolderEntity.getRootFolderId(), solrFolderEntity.getPath(), solrFolderEntity);
                docFoldersEntityByIdMap.put(solrFolderEntity.getId(), solrFolderEntity);
            }
        );
    }

    void addFileGroups(List<FileGroup> fileGroups) {
        fileGroups.forEach(fileGroup -> fileGroupsMap.put(fileGroup.getId(), fileGroup));
    }

    void addIdenticals(Map<Pair<String, Long>, List<SolrContentMetadataEntity>> identicalContentMap,
                       Map<Pair<String, Long>,Long> identicalContentMapInBatch) {
        this.identicalContentMap = identicalContentMap;
        this.identicalContentMapInBatch = identicalContentMapInBatch;
        identicalContentMap.values().stream()
                .flatMap(Collection::stream)
                .forEach(e -> contentMetadataEntitiesMap.put(Long.valueOf(e.getId()), e));
    }

    void addFileEntities(List<SolrFileEntity> entities) {
        entities.forEach(e -> entitiesMap.put(e.getFileId(), e));
    }

    void addContentMetadataEntities(List<SolrContentMetadataEntity> solrContentMetadataEntities) {
        solrContentMetadataEntities.forEach(e -> contentMetadataEntitiesMap.put(Long.valueOf(e.getId()), e));
    }

    void addDocFolders(List<DocFolder> docFolders) {
        docFolders.forEach(df -> docFoldersByIdMap.put(df.getId(), df));
    }

    void addTasks(List<SimpleTask> tasks) {
        tasks.forEach(task -> taskIdsMap.put(task.getId(), task));
    }

    public SimpleTask getTaskById(Long taskId) {
        return taskIdsMap.get(taskId);
    }

    public ClaFile getFileObject(Long claFileId) {
        return claFilesMap.get(claFileId);
    }

    public ContentMetadata getContentMetadata(Long contentMetadataId) {
        return contentMetadataMap.get(contentMetadataId);
    }

    public SolrFolderEntity getFolderByPath(Long rootFolderId, String fullPath) {
        String normalizedPath = FileNamingUtils.convertPathToUniversalString(fullPath);
        return foldersTable.get(rootFolderId, normalizedPath);
    }

    public FileGroup getFileGroup(String groupId) {
        return fileGroupsMap.get(groupId);
    }

    public void addFileGroup(String groupId, FileGroup group) {
        fileGroupsMap.put(groupId, group);
    }

    public DocFolder getDocFolderById(Long docFolderId) {
        return docFoldersByIdMap.get(docFolderId);
    }

    public SolrFolderEntity getDocFolderEntityById(Long docFolderId) {
        return docFoldersEntityByIdMap.get(docFolderId);
    }

    public SolrFileEntity getFileEntity(Long fileId) {
        return entitiesMap.get(fileId);
    }

    public SolrContentMetadataEntity getContentMetadataEntity(Long contentMetadataId) {
        return contentMetadataEntitiesMap.get(contentMetadataId);
    }

    public Long findIdenticalInBatch(String contentSignature, Long fileSize) {
        return identicalContentMapInBatch.get(Pair.of(contentSignature, fileSize));
    }


    public List<SolrContentMetadataEntity> findIdentical(String contentSignature, Long fileSize) {
        return identicalContentMap.get(Pair.of(contentSignature, fileSize));
    }


    public synchronized void markTaskAsDone(Long taskId, Long jobId, TaskState state) {
        doneTasks.add(taskId);
        if (state.equals(TaskState.DONE)) {
            incrementCounterInMap(doneTasksJobCounter, jobId);
        } else {
            incrementCounterInMap(failTasksJobCounter, jobId);
        }
    }

    private static void incrementCounterInMap(Map<Long, Integer> taskCounterByJob, Long jobId) {
        Integer counter = taskCounterByJob.get(jobId);
        if (counter == null) {
            counter = 0;
        }
        taskCounterByJob.put(jobId, counter + 1);
    }

    List<Long> getDoneTasks() {
        return doneTasks;
    }

    Map<Long, Integer> getDoneTasksJobCounter() {
        return doneTasksJobCounter;
    }

    Map<Long, Integer> getFailTasksJobCounter() {
        return failTasksJobCounter;
    }

    public Boolean isFileWithDuplicateContent(Long fileId) {
        return fileIdsWithDuplicateContents.contains(fileId);
    }

    public void addToFileWithDuplicateContent(Long fileId) {
        fileIdsWithDuplicateContents.add(fileId);
    }
}
