package com.cla.filecluster.batch.writer.ingest;

public enum IngestBatchTaskType {
    FILE_ENTITY,
    INGESTION_ERROR,
    CAT_FILE,
    CONTENT_ENTITY,
    GRP_HELPER,
    PV_ANALYSIS_DATA,
    HISTOGRAM,
    DELETED_FILES_BY_ID,
    MARK_DIRTY_POPULATION_FLAG,
    CLEAR_SAMPLE_FILE_ID,
    MARK_DIRTY_GROUP,
    CONTENT_METADATA,
    EMPTY_FILES,
}
