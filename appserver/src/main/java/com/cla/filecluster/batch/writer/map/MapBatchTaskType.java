package com.cla.filecluster.batch.writer.map;

/**
 * Created by: yael
 * Created on: 9/20/2018
 */
public enum MapBatchTaskType {
    FILE_FOLDER,
    CONTENT_METADATA_FILE_COUNT_UPDATE,
    MARK_FILES_DELETED
}
