package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.domain.dto.SolrContentEntity;
import com.cla.filecluster.repository.solr.FileDTOSearchRepository;

import java.util.List;

public class SolrContentEntityBatchExecutor extends AbstractIngestBatchExecutor<SolrContentEntity> {

    private FileDTOSearchRepository fileDTOSearchRepository;


    public SolrContentEntityBatchExecutor(FileDTOSearchRepository fileDTOSearchRepository, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize,partitionUpdateConcurrency );
        this.fileDTOSearchRepository = fileDTOSearchRepository;
    }

    @Override
    protected void flushBatch(List<SolrContentEntity> batch) {
        long start = System.currentTimeMillis();
        int totalSize = batch.stream().mapToInt(c -> c.getContent().getBytes().length).sum();
        logger.debug("Start: Persisting total of {} content ({} bytes)", batch.size(), totalSize, System.currentTimeMillis() - start);
        fileDTOSearchRepository.saveAllEntities(batch);
        logger.debug("Done: Persisting total of {} content ({} bytes) in {} ms", batch.size(), totalSize, System.currentTimeMillis() - start);
    }
}

