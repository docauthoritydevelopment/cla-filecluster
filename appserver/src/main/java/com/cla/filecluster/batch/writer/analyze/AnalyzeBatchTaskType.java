package com.cla.filecluster.batch.writer.analyze;

/**
 * Created by: yael
 * Created on: 10/4/2018
 */
public enum AnalyzeBatchTaskType {
    FILE_CONTENT_TO_ANALYZE,
    FAILED_TASKS,
    FAILED_TASKS_COUNTER,
    SUCCESS_TASKS_COUNTER,
    NEW_FILE_GROUP,
    CONTENT_GROUP_ASSIGNMENT
}
