package com.cla.filecluster.batch.writer.map;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.FileCatService;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.data.domain.PageRequest;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by: Nadav
 * Responsible for updating content metadata file count upon deletion
 * TODO: in future when undelete will be in batch as well, use this class to update fileCount
 */
public class ContentMetadataFileCountUpdater extends AbstractMapBatchExecutor<Long> {

    private final SolrContentMetadataRepository contentMetadataRepository;
    private final FileCatService fileCatService;

    public ContentMetadataFileCountUpdater(SolrContentMetadataRepository contentMetadataRepository,
                                           FileCatService fileCatService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.contentMetadataRepository = contentMetadataRepository;
        this.fileCatService = fileCatService;
    }


    @Override
    protected void flushBatch(List<Long> batch) {
        long start = System.currentTimeMillis();
        Collection<Long> contentIds = fileCatService.findContentsByFileIds(PageRequest.of(0, batch.size()), batch);
        logger.debug("Time to fetch content ids for {} files from solr took: {} ms", batch.size(), System.currentTimeMillis() - start);
        if (contentIds == null || contentIds.isEmpty()) {
            logger.warn("no content ids found for files {}",batch);
        } else {
            start = System.currentTimeMillis();
            List<SolrContentMetadataEntity> contents = contentMetadataRepository.find(contentIds);
            List<Long> existingContentIds = contents.stream().map(e -> Long.parseLong(e.getId())).collect(Collectors.toList());
            List<SolrInputDocument> fileCountUpdates = contentIds.stream()
                    .filter(existingContentIds::contains)
                    .map(cId ->
                            AtomicUpdateBuilder.create()
                                    .setId(ContentMetadataFieldType.ID, cId)
                                    .addModifier(ContentMetadataFieldType.FILE_COUNT, SolrOperator.INC, -1)
                                    .addModifier(ContentMetadataFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                                    .build()
                    )
                    .collect(Collectors.toList());
            if (!fileCountUpdates.isEmpty()) {
                contentMetadataRepository.saveAll(fileCountUpdates);
            }
            logger.debug("Update file count in solr for {} contents took: {} ms", System.currentTimeMillis() - start);
        }
    }
}
