package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.service.files.managers.FileGroupService;

import java.util.List;

public class MarkDirtyGroupBatchExecutor extends AbstractIngestBatchExecutor<String> {

    private FileGroupService fileGroupService;

    public MarkDirtyGroupBatchExecutor(FileGroupService fileGroupService, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.fileGroupService = fileGroupService;
    }

    @Override
    protected void flushBatch(List<String> ids) {
        fileGroupService.markGroupAsDirtyByIds(ids);
    }
}
