package com.cla.filecluster.batch.writer.map;

import com.cla.common.utils.ConcurrencyUtils;
import com.cla.connector.utils.Pair;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * executor to save data to be persisted to solr and db and flush in chunks
 *
 * Created by: yael
 * Created on: 9/20/2018
 */
public abstract class AbstractMapBatchExecutor<T> implements MapBatchTaskExecutor<T> {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    private Map<Pair<Long, Long>, List<T>> taskIdToBatchItemMap = Maps.newHashMap();

    private final Object mutex = new Object();

    private final int innerPartitionSize;

    private final ExecutorService innerPartitionBatchExecutor;

    private boolean active = true;

    public AbstractMapBatchExecutor(int innerPartitionSize, int partitionUpdateConcurrency) {
        this.innerPartitionSize = innerPartitionSize;
        innerPartitionBatchExecutor = Executors.newFixedThreadPool(partitionUpdateConcurrency,
                new ThreadFactoryBuilder().setNameFormat("inner-batch-partition-updater-%d").build());
    }

    public Set<Pair<Long, Long>> getPendingBatchTaskIds() {
        return new HashSet<>(taskIdToBatchItemMap.keySet());
    }

    @Override
    public void add(Pair<Long, Long> batchTaskId, T t) {
        List<T> items = taskIdToBatchItemMap.get(batchTaskId);
        if (items == null) {
            synchronized (mutex) {
                items = taskIdToBatchItemMap.computeIfAbsent(batchTaskId, k -> new LinkedList<>());
            }
        }

        synchronized (mutex) {
            items.add(t);
        }
    }

    @Override
    public boolean hasItem(T t) {
        synchronized (mutex) {
            for (List<T> list : taskIdToBatchItemMap.values()) {
                if (list.contains(t)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void flush(Collection<Pair<Long, Long>> batchTaskIds) {
        List<T> batch;
        synchronized (mutex) {
            batch = taskIdToBatchItemMap.entrySet().stream()
                    .filter(e -> batchTaskIds.contains(e.getKey()))
                    .flatMap(e -> e.getValue().stream())
                    .collect(Collectors.toList());
        }
        if (!batch.isEmpty()) {
            //logger.debug("Pre: Flushing batch {}", batch);
            try {
                long start = System.currentTimeMillis();
                logger.debug("Start: Flushing batch of {}", batch.size());
                flushByPartitions(batch);
                logger.debug("Done: Flushing batch of {} in {} ms", batch.size(), System.currentTimeMillis() - start);
            } catch (InterruptedException e) {
                logger.info("Flush operation interrupted", e.getMessage());
            }
        }
    }

    private void flushByPartitions(List<T> batch) throws InterruptedException {
        if (!active) return;
        List<List<T>> partitionList = Lists.partition(batch, innerPartitionSize);
        ConcurrencyUtils.executeTasks(partitionList, ts -> {
                    long innerStart = System.currentTimeMillis();
                    logger.debug("Start: Flushing by inner partition of {}", ts.size());
                    flushBatch(ts);
                    logger.debug("Done: Flushing by inner partition of {} in {} ms", ts.size(), System.currentTimeMillis() - innerStart);
                },
                innerPartitionBatchExecutor
        );
    }

    @Override
    public void clear(Collection<Pair<Long, Long>> batchTaskIds) {
        synchronized (mutex) {
            batchTaskIds.forEach(taskIdToBatchItemMap::remove);
        }
    }

    @Override
    public void clearByTask(Collection<Long> taskIds) {
        List<Pair<Long, Long>> batchTaskIdsToRem = new ArrayList<>();
        taskIdToBatchItemMap.keySet().forEach(keyPair -> {
            if (taskIds.contains(keyPair.getValue())) {
                batchTaskIdsToRem.add(keyPair);
            }
        });

        if (!batchTaskIdsToRem.isEmpty()) {
            clear(batchTaskIdsToRem);
        }
    }

    protected abstract void flushBatch(List<T> batch);

    @PreDestroy
    public void destroy() {
        active = false;
        innerPartitionBatchExecutor.shutdown();
    }
}
