package com.cla.filecluster.batch.writer.map;

import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Tables;
import org.apache.solr.common.SolrInputDocument;

import java.util.*;
import java.util.stream.Collectors;

/**
 * executor to save files and foldersto solr and kvdb
 *
 * Created by: yael
 * Created on: 9/20/2018
 */
public class MapFileFolderBatchExecutor extends AbstractMapBatchExecutor<FileToAdd> {

    private KeyValueDatabaseRepository kvdbRepo;
    private FileCatService fileCatService;
    private DocFolderService docFolderService;

    public MapFileFolderBatchExecutor(KeyValueDatabaseRepository kvdbRepo, FileCatService fileCatService,
                                      DocFolderService docFolderService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.kvdbRepo = kvdbRepo;
        this.fileCatService = fileCatService;
        this.docFolderService = docFolderService;
    }

    @Override
    protected void flushBatch(List<FileToAdd> batch) {
        docFolderService.createFoldersInSolr();

        if (batch == null || batch.isEmpty()) return;

        // create new files if needed
        createNewFiles(batch);

        // update records so will be picked for ingest if needed
        handleFilesToBeIngested(batch);

        updateKvdb(batch);
    }

    private void createNewFiles(List<FileToAdd> batch) {
        List<SolrInputDocument> docs = batch.stream()
                .map(FileToAdd::getDoc)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        if (docs != null && !docs.isEmpty()) {
            logger.trace("save docs {}", docs);
            fileCatService.createCategoryFilesFromDocuments(docs);
            fileCatService.softCommitNoWaitFlush();
        }
    }

    private void handleFilesToBeIngested(List<FileToAdd> batch) {
        List<Long> fileIdsToIngest = batch.stream()
                .map(FileToAdd::getFileIdToIngest)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        if (fileIdsToIngest != null && !fileIdsToIngest.isEmpty()) {

            Map<Long, String> fileIdsMap = AuthenticationHelper.doWithAuthentication(
                    AutoAuthenticate.ADMIN, false,
                    () -> fileCatService.getIdsForFileIds(fileIdsToIngest)
                    , null);

            List<SolrInputDocument> docsToIngest = new ArrayList<>();
            batch.stream().filter(b -> b.getFileIdToIngest() != null && b.getFileIdToIngest() > 0).forEach(
                    fileToAdd -> {
                        if (fileIdsMap.containsKey(fileToAdd.getFileIdToIngest())) {
                            logger.trace("create task ingest mark for file {} id {}", fileToAdd.getFileIdToIngest(), fileIdsMap.get(fileToAdd.getFileIdToIngest()));
                            docsToIngest.add(SolrTaskService.createNewTask(
                                    fileIdsMap.get(fileToAdd.getFileIdToIngest()), fileToAdd.getRunId(), fileToAdd.getParamNewIngestTask()
                            ));
                        } else {
                            logger.warn("file to ingest {} not found, cant update", fileToAdd.getFileIdToIngest());
                        }
                    }
            );

            if (!docsToIngest.isEmpty()) {
                logger.trace("save docs ingest mark {}", docsToIngest);
                fileCatService.saveAll(docsToIngest);
                fileCatService.softCommitNoWaitFlush();
            }
        }
    }

    private void updateKvdb(List<FileToAdd> batch) {
        // Split the given batch to the actual batches (one per transaction)
        // <RF-ID, RunId, actual batch>
        Table<Long, Long, List<FileToAdd>> batches = batch
                .stream()
                .filter(fta -> fta.getMediaItemId() != null)
                .collect(Tables.toTable(
                        FileToAdd::getRootFolderId,
                        FileToAdd::getRunId,
                        Collections::singletonList,
                        (filesToAdd, filesToAdd2) -> {
                            List<FileToAdd> mergedList = new ArrayList<>(filesToAdd.size() + filesToAdd2.size());
                            mergedList.addAll(filesToAdd);
                            mergedList.addAll(filesToAdd2);
                            return mergedList;
                        },
                        HashBasedTable::create));

        if (batches.isEmpty()) return;

        for (Long rfId : batches.rowKeySet()) {
            Map<Long, List<FileToAdd>> filesToAddByRunId = batches.row(rfId);
            String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, rfId);
            if (filesToAddByRunId.size() > 1) {
                logger.error("Unexpected multiple runIds for root folder id {}. RunIds: {}", rfId, filesToAddByRunId.keySet());
            }
            filesToAddByRunId.forEach((runId, fileToAdds) ->
                    kvdbRepo.getFileEntityDao().executeInTransaction(runId, storeName, fileEntityStore -> {
                        for (FileToAdd p : fileToAdds) {
                            fileEntityStore.put(p.getMediaItemId(), p.getFileEntity());
                        }
                        return true;
                    }));
        }
    }
}
