package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.repository.solr.SolrPvEntriesIngesterRepository;
import org.apache.solr.common.SolrInputDocument;

import java.util.List;

public class HistogramBatchExecutor extends AbstractIngestBatchExecutor<SolrInputDocument> {

    private SolrPvEntriesIngesterRepository solrPvEntriesIngesterRepository;

    public HistogramBatchExecutor(SolrPvEntriesIngesterRepository solrPvEntriesIngesterRepository, int innerPartitionSize,int concurrency) {
        super(innerPartitionSize, concurrency);
        this.solrPvEntriesIngesterRepository = solrPvEntriesIngesterRepository;

    }


    @Override
    protected void flushBatch(List<SolrInputDocument> batch) {
        solrPvEntriesIngesterRepository.saveAll(batch);
}
}
