package com.cla.filecluster.batch.writer.ingest;

import com.cla.common.constants.SolrCommitType;
import com.cla.filecluster.service.files.ContentMetadataService;

import java.util.List;

public class MarkDirtyPopulationFlagBatchExecutor extends AbstractIngestBatchExecutor<Long> {

    private final ContentMetadataService contentMetadataService;

    public MarkDirtyPopulationFlagBatchExecutor(ContentMetadataService contentMetadataService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.contentMetadataService = contentMetadataService;
    }

    @Override
    protected void flushBatch(List<Long> batch) {
        try {
            logger.trace("Before flushing batch of {} dirty contents, trying to acquire population dirty lock", batch.size());
            contentMetadataService.acquirePopulationDirtyReadLock();
            logger.trace("Successfully acquired population dirty lock for batch of {} contents", batch.size());
            contentMetadataService.markDirtyPopulationFlagById(batch, SolrCommitType.SOFT_NOWAIT);
        } finally {
            logger.trace("Trying to release population dirty lock for batch of {} contents", batch.size());
            contentMetadataService.releasePopulationDirtyReadLock();
            logger.trace("Successfully released population dirty lock for batch of {} contents", batch.size());
        }
    }
}
