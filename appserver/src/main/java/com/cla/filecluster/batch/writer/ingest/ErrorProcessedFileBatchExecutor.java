package com.cla.filecluster.batch.writer.ingest;

import com.cla.common.utils.DbUtils;
import com.cla.filecluster.domain.entity.pv.ErrProcessedFile;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.util.query.DBTemplateUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.cla.common.constants.DbDatePattern.CREATED_ON_DB_PATTERN;
import static com.cla.common.utils.DbUtils.getStrOrNull;


public class ErrorProcessedFileBatchExecutor extends AbstractIngestBatchExecutor<ErrProcessedFile> {

    private static final String ERROR_BATCH_INSERT_STATMENT = "INSERT INTO `err_processed_files` " +
            "(`id`, `cla_file_id`, `content_id`, `created_ondb`, `exception_text`, `file_type`, `folder_id`, `full_path`, " +
            "`root_folder_id`, `run_id`, `text`, `type`,`date_created`, `media_type`, `extension`) VALUES %s";
    private static final String ERROR_BATCH_VALUES_PATTERN = "(%d,%d,%d,'%s',%s,%d,%d,%s,%d,%d,%s,%d,%d,%d,%s)";

    private DBTemplateUtils dbTemplateUtils;

    private SequenceIdGenerator sequenceIdGenerator;

    public ErrorProcessedFileBatchExecutor(SequenceIdGenerator sequenceIdGenerator, DBTemplateUtils dbTemplateUtils, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.sequenceIdGenerator = sequenceIdGenerator;
        this.dbTemplateUtils = dbTemplateUtils;
    }

    @Override
    protected void flushBatch(List<ErrProcessedFile> batch) {
        dbTemplateUtils.doInTransaction(jdbcTemplate -> jdbcTemplate.execute(batchedErrorStatement(batch)));
    }

    private String batchedErrorStatement(List<ErrProcessedFile> batch) {
        String createdOnDbStr = CREATED_ON_DB_PATTERN.format(new Date());
        String batchedValues = batch.stream()
                .map(err -> String.format(
                        ERROR_BATCH_VALUES_PATTERN,
                        sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.INGESTION_ERROR),
                        err.getClaFile(),
                        err.getContentId(),
                        createdOnDbStr,
                        DbUtils.escapeBackwardSeparator(getStrOrNull(err.getExceptionText())),
                        err.getFileType(),
                        err.getFolderId(),
                        DbUtils.escapeBackwardSeparator(getStrOrNull(err.getFullPath())),
                        err.getRootFolderId(),
                        err.getRunId(),
                        DbUtils.escapeBackwardSeparator(getStrOrNull(err.getText())),
                        err.getType().ordinal(),
                        err.getDateCreated(),
                        err.getMediaType() == null ? null : err.getMediaType().getValue(),
                        getStrOrNull(err.getExtension())))
                .collect(Collectors.joining(","));
        String sqlStmt = String.format(ERROR_BATCH_INSERT_STATMENT, batchedValues);
        return sqlStmt;
    }
}
