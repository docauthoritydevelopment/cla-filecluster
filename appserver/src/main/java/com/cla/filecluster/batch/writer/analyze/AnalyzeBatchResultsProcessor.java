package com.cla.filecluster.batch.writer.analyze;

import com.cla.common.constants.SkipAnalysisReasonType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.MissedFileMatch;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * save data to be persisted to solr and db and flush in chunks
 * <p>
 * Created by: yael
 * Created on: 10/4/2018
 */
@Component
public class AnalyzeBatchResultsProcessor implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(AnalyzeBatchResultsProcessor.class);

    private List<Long> doneTasks = new ArrayList<>();
    private List<Long> failedTasks = new ArrayList<>();

    @Autowired
    private BatchAnalyzeResultsExecutor batchAnalyzeResultsExecutor;

    @Autowired
    private MissedMatchHandler missedMatchHandler;

    @Value("${analyze.min-millis-between-batch-execute:30000}")
    private long minMillisBetweenBatchExecute;

    @Value("${analyze.min-objects-to-batch-execute:1000}")
    private int minObjectsToBatchExecute;

    private long timeOfLastBatchFlush = 0;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    public void collectFailedTasks(Long taskId, Long jobId) {
        batchAnalyzeResultsExecutor.collect(AnalyzeBatchTaskType.FAILED_TASKS, taskId, taskId);
        batchAnalyzeResultsExecutor.collect(AnalyzeBatchTaskType.FAILED_TASKS_COUNTER, taskId, Pair.of(jobId, 1));
        synchronized (this) {
            failedTasks.add(taskId);
        }
    }

    public void collectContentFilesToAnalyze(Long taskId, Long contentId, SkipAnalysisReasonType skipAnalysisReasonType) {
        batchAnalyzeResultsExecutor.collect(AnalyzeBatchTaskType.FILE_CONTENT_TO_ANALYZE, taskId, Pair.of(contentId, skipAnalysisReasonType));
    }

    public void collectDirtyFileGroups(Long taskId, String groupId) {
        batchAnalyzeResultsExecutor.collectDirtyFileGroups(taskId, groupId);
    }

    public void collectMissedFileMatches(MissedFileMatch missedFileMatch) {
        missedMatchHandler.handle(missedFileMatch);
    }

    public synchronized void collectDoneTasks(Long taskId, Long jobId) {
        batchAnalyzeResultsExecutor.collect(AnalyzeBatchTaskType.SUCCESS_TASKS_COUNTER, taskId, Pair.of(jobId, 1));
        doneTasks.add(taskId);
    }

    public void collectNewFileGroups(Long taskId, SolrFileGroupEntity fileGroup) {
        logger.info("Collecting new group {}", fileGroup.getId());
        batchAnalyzeResultsExecutor.collect(AnalyzeBatchTaskType.NEW_FILE_GROUP, taskId, fileGroup);
    }

    public void collectContentGroupChange(Long taskId, Long contentId, String groupId) {
        batchAnalyzeResultsExecutor.collect(AnalyzeBatchTaskType.CONTENT_GROUP_ASSIGNMENT, taskId, Pair.of(contentId, groupId));
    }

    public void flush() {
        if ((doneTasks == null || doneTasks.isEmpty()) && (failedTasks == null || failedTasks.isEmpty())) { // Can happen in the case of use pause
            logger.warn("Called flush when no done & failed tasks, skipping flush (Probably user paused)");
            return;
        }

        List<Long> tasks;
        List<Long> failed;
        synchronized (this) {
            tasks = doneTasks;
            doneTasks = new ArrayList<>();
            failed = failedTasks;
            failedTasks = new ArrayList<>();
        }

        long start = System.currentTimeMillis();
        logger.info("Start: flushing of {} tasks results", tasks.size());
        batchAnalyzeResultsExecutor.flush(tasks, failed);
        logger.info("Done: flushing of {} tasks results in {} ms", tasks.size(), System.currentTimeMillis() - start);
    }

    @Scheduled(initialDelayString = "${analyze.batch-execute.initial-delay.millis:10000}",
            fixedRateString = "${analyze.batch-execute.rate.millis:5000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void executeAnalyzeBatchIfNeeded() {
        if (!isSchedulingActive) {
            return;
        }

        long timePassed;
        boolean flush = false;
        synchronized (this) {
            timePassed = System.currentTimeMillis() - timeOfLastBatchFlush;
            int size = doneTasks.size() + failedTasks.size();
            if (size > 0 && (size > minObjectsToBatchExecute || timePassed > minMillisBetweenBatchExecute)) {
                timeOfLastBatchFlush = System.currentTimeMillis();
                flush = true;
            }
        }

        if (flush) {
            flush();
        } else {
            logger.trace("Called flush when no done tasks or time not passed, skipping flush");
        }
    }

    public synchronized boolean isDoneTask(Long taskId) {
        return doneTasks.contains(taskId) || failedTasks.contains(taskId);
    }

}
