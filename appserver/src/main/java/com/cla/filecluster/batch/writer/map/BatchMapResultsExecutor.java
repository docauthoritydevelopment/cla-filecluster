package com.cla.filecluster.batch.writer.map;

import com.cla.common.utils.ConcurrencyUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.mediaproc.ScanDoneConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * executor to save data to be persisted to solr and db and flush in chunks
 *
 * Created by: yael
 * Created on: 9/20/2018
 */
@Component
public class BatchMapResultsExecutor {

    @Autowired
    private Map<MapBatchTaskType, MapBatchTaskExecutor> mapBatchExecutorsRegistry;

    @Autowired
    private ScanDoneConsumer scanDoneConsumer;

    private ExecutorService batchRepoExecutor;

    private static final Logger logger = LoggerFactory.getLogger(BatchMapResultsExecutor.class);

    private boolean active = true;

    @PostConstruct
    private void init() {
        batchRepoExecutor = Executors.newFixedThreadPool(mapBatchExecutorsRegistry.keySet().size());
    }

    public <T> void collect(MapBatchTaskType batchTaskType, Pair<Long, Long> batchTaskId, T t) {
        mapBatchExecutorsRegistry.get(batchTaskType).add(batchTaskId, t);
    }

    public <T> boolean isAlreadySet(MapBatchTaskType batchTaskType, T t) {
        return mapBatchExecutorsRegistry.get(batchTaskType).hasItem(t);
    }

    public Set<Pair<Long, Long>> getPendingBatchTaskIds() {
        Set<Pair<Long, Long>> result = new HashSet<>();
        mapBatchExecutorsRegistry.values().forEach(executor -> {
            result.addAll(executor.getPendingBatchTaskIds());
        });
        return result;
    }

    public synchronized void flush(Collection<Pair<Long, Long>> batchTaskIds) {
        if (!active) return;
        try {
            ConcurrencyUtils.executeTasks(mapBatchExecutorsRegistry.entrySet(),
                    e -> e.getValue().flush(batchTaskIds),
                    batchRepoExecutor
            );
            scanDoneConsumer.updateTasksRunningCounter(batchTaskIds, false);

        } catch (Throwable t) {
            logger.error(String.format("Failed to flush results of %d tasks", batchTaskIds.size()), t);
        } finally {
            mapBatchExecutorsRegistry.values().forEach(batchTaskExecutor -> batchTaskExecutor.clear(batchTaskIds));
        }
    }

    public synchronized void clearTaskData(List<Long> taskIds) {
        if (taskIds == null || taskIds.isEmpty()) return;

        batchRepoExecutor.submit(() -> {
            mapBatchExecutorsRegistry.values().forEach(collector ->
                    collector.clearByTask(taskIds)
            );
        });
    }

    @PreDestroy
    public void destroy() {
        active = false;
        batchRepoExecutor.shutdown();
    }
}
