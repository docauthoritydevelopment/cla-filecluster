package com.cla.filecluster.batch.writer.analyze;

import java.util.Collection;

/**
 * executor to save data to be persisted to solr and db and flush in chunks
 *
 * Created by: yael
 * Created on: 10/4/2018
 */
public interface AnalyzeBatchTaskExecutor<T> {

    void add(Long taskId, T t);

    void flush(Collection<Long> taskIds);

    void clear(Collection<Long> taskIds);
}
