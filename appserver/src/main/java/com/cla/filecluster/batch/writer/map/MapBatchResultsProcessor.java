package com.cla.filecluster.batch.writer.map;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.mediaproc.map.FileToDel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

/**
 * processor to save data to be persisted to solr and db and flush in chunks
 *
 * Created by: yael
 * Created on: 9/20/2018
 */
@Component
public class MapBatchResultsProcessor {

    private static final Logger logger = LoggerFactory.getLogger(MapBatchResultsProcessor.class);

    @Autowired
    private BatchMapResultsExecutor batchMapResultsExecutor;

    public void collectCatFiles(Pair<Long, Long> batchTaskId, FileToAdd file) {
        batchMapResultsExecutor.collect(MapBatchTaskType.FILE_FOLDER, batchTaskId, file);
    }

    public void collectDeletedFiles(Pair<Long, Long> batchTaskId, FileToDel file) {
        batchMapResultsExecutor.collect(MapBatchTaskType.MARK_FILES_DELETED, batchTaskId, file);
    }

    public void collectContentMetadataFileCountToUpdate(Pair<Long, Long> batchTaskId, Long contentMdId) {
        batchMapResultsExecutor.collect(MapBatchTaskType.CONTENT_METADATA_FILE_COUNT_UPDATE, batchTaskId, contentMdId);
    }

    public boolean fileAlreadySetForDelete(FileToDel file) {
        return batchMapResultsExecutor.isAlreadySet(MapBatchTaskType.MARK_FILES_DELETED, file);
    }

    public boolean fileAlreadyHandled(FileToAdd file) {
        return batchMapResultsExecutor.isAlreadySet(MapBatchTaskType.FILE_FOLDER, file);
    }

    public Set<Pair<Long, Long>> getPendingBatchTaskIds() {
        return batchMapResultsExecutor.getPendingBatchTaskIds();
    }

    public void flush(Collection<Pair<Long, Long>> batchTaskIds) {
        if (batchTaskIds == null || batchTaskIds.isEmpty()) { // Can happen in the case of use pause
            logger.warn("Called flush when no done tasks, skipping flush (Probably user paused)");
            return;
        }
        long start = System.currentTimeMillis();
        logger.info("Start: flushing of {} tasks results", batchTaskIds.size());
        batchMapResultsExecutor.flush(batchTaskIds);
        logger.info("Done: flushing of {} tasks results in {} ms", batchTaskIds.size(), System.currentTimeMillis() - start);
    }
}
