package com.cla.filecluster.batch.writer.map;

import com.cla.filecluster.batch.writer.ingest.AbstractIngestBatchExecutor;
import com.cla.filecluster.service.categories.FileCatService;

import java.util.List;

public class DeletedFileCatBatchExecutor extends AbstractIngestBatchExecutor<String> {

    private final FileCatService fileCatService;

    public DeletedFileCatBatchExecutor(FileCatService fileCatService, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.fileCatService = fileCatService;
    }

    @Override
    protected void flushBatch(List<String> batch) {
        try {
            fileCatService.acquireFileDeleteWriteLock();
            fileCatService.deleteByIds(batch);
        } finally {
            fileCatService.releaseFileDeleteWriteLock();
        }
    }
}
