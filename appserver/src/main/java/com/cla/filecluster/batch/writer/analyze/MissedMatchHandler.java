package com.cla.filecluster.batch.writer.analyze;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.pv.GroupUnificationHelper;
import com.cla.filecluster.domain.entity.pv.MissedFileMatch;
import com.cla.filecluster.jobmanager.domain.entity.Job;
import com.cla.filecluster.jobmanager.service.CoordinationLockDeniedException;
import com.cla.filecluster.jobmanager.service.JobCoordinator;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.jpa.MissedFileMatchRepository;
import com.cla.filecluster.service.crawler.analyze.AnalyzerDataService;
import com.cla.filecluster.service.crawler.analyze.ContentGroupsCache;
import com.cla.filecluster.service.crawler.analyze.NewFileGroupsCache;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Responsible for handling analyze missed matches, once it reaches a given threshold all accumulated missed match
 * are processed.
 * This is an analyze-stop-the-world process, meaning that analysis cannot proceed till all accumulated missed matches
 * are handled
 */
@Component
public class MissedMatchHandler {

    private static final Logger logger = LoggerFactory.getLogger(MissedMatchHandler.class);

    @Autowired
    private FileMissedMatchBatchExecutor fileMissedMatchBatchExecutor;

    @Autowired
    private AnalyzerDataService analyzerDataService;

    @Autowired
    private JobCoordinator jobCoordinator;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private MissedFileMatchRepository missedFileMatchRepository;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private ContentGroupsCache contentGroupsCache;

    @Autowired
    private NewFileGroupsCache newFileGroupsCache;

    @Value("${analyze.missed-matches.handle-threshold:1000}")
    private int missedMatchHandleThreshold;

    @Value("${analyze.missed-matches.flush-to-db-threshold:100}")
    private int missedMatchesFlushToDbThreshold;

    @Value("${analyze.missed-matches.startup.handle-page-size:5000}")
    private int missedMatchesPageSize;

    private List<MissedFileMatch> missedMatchesToHandle = Collections.synchronizedList(new LinkedList<>());

    private List<MissedFileMatch> collectedMissedMatches = Collections.synchronizedList(new ArrayList<>(1000000));

    private ReentrantReadWriteLock missedMatchLock = new ReentrantReadWriteLock();

    private Function<List<String>, Collection<SolrFileGroupEntity>> fileGroupsProvider;

    private int handleGroupMatchCounter = 0;

    private List<MissedFileMatch> missedMatchesToFlush = new LinkedList<>();

    @PostConstruct
    private void init() {
        // if the strategy requires full groups with all the references - use the full query
        // otherwise use simple query
        if (!analyzerDataService.getElectionStrategy().requiresFullGroupObjects()) {
            logger.debug("Election strategy, requiresFullGroupObjects: setting file groups with association provider");
            fileGroupsProvider = wrapWithFallbackFromCache(fileGroupService::findByIds);
        } else {
            logger.debug("Election strategy, not requiresFullGroupObjects: setting simple file groups provider");
            fileGroupsProvider = wrapWithFallbackFromCache(fileGroupService::findByIds);
        }
    }

    private Function<List<String>, Collection<SolrFileGroupEntity>> wrapWithFallbackFromCache(
            Function<List<String>, Collection<SolrFileGroupEntity>> fileGroupsProvider) {
        return groupIds -> {
            Collection<SolrFileGroupEntity> fileGroups = fileGroupsProvider.apply(groupIds);

            List<SolrFileGroupEntity> result = new LinkedList<>();
            logger.debug("Returned {} file groups out of {} group ids", fileGroups.size(), groupIds.size());
            result.addAll(fileGroups);

            List<String> existingIds = fileGroups.stream()
                    .map(SolrFileGroupEntity::getId)
                    .collect(Collectors.toList());
            if (existingIds.size() == groupIds.size()) {
                return fileGroups;
            }

            logger.debug("Missing results for {} group ids, retrieving from cache", groupIds.size() - fileGroups.size());
            groupIds.stream()
                    .filter(groupId -> notInExistingIdsButInCache(existingIds, groupId))
                    .map(groupId -> newFileGroupsCache.get(groupId))
                    .forEach(result::add);

            logger.debug("After cache retrieval, total of {} file groups found", result.size());
            return result;

        };
    }

    private boolean notInExistingIdsButInCache(List<String> existingIds, String groupId) {
        if (!existingIds.contains(groupId)) {
            logger.debug("missing group id {} trying to get from cache", groupId);
            if (!newFileGroupsCache.contains(groupId)) {
                logger.warn("Missing group id {} from cache!!", groupId);
                return false;
            }
            return true;
        }
        return false;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void handleMissedMatchesOnStartup() {
        missedMatchLock.writeLock().lock();
        logger.info("Locking for handling missed file matches on startup");
        try {
            updateRunningJobsStates(JobType.ANALYZE, "Handling group missed matches...");
            acquireCoordinationLock();
            resolveMissedMatchGroupReunions()
                    .ifPresent(this::joinGroupsAndTruncateMissedFileMatches);
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            logger.error("Interrupted", ie);
        } catch (Exception e) {
            logger.error("Failed processing missed grouping matches", e);
        } finally {
            missedMatchLock.writeLock().unlock();
            releaseCoordinationLock();
            updateRunningJobsStates(JobType.ANALYZE, "Finished handling group missed matches");
            logger.info("Done handling missed file matches on startup, releasing lock");
        }
    }

    @NotNull
    private Optional<MissedMatchHandler.MissedMatchGroupReunions> resolveMissedMatchGroupReunions() {
        int page = 0;
        Page<MissedFileMatch> missedFileMatchPage = missedFileMatchRepository
                .findAllUnprocessedPage(PageRequest.of(page++, missedMatchesPageSize));

        if (!missedFileMatchPage.hasContent()) {
            return Optional.empty();
        }

        MissedMatchGroupReunions missedMatchGroupReunions = new MissedMatchGroupReunions();
        while (missedFileMatchPage.hasContent()) {
            logger.debug("Processing {} missed matches, page: {}", missedFileMatchPage.getSize(), page - 1);
            final List<MissedFileMatch> content = missedFileMatchPage.getContent();
            fillGroupReunions(content, missedMatchGroupReunions);
            missedFileMatchPage = missedFileMatchRepository
                    .findAllUnprocessedPage(PageRequest.of(page++, missedMatchesPageSize));
        }

        return Optional.of(missedMatchGroupReunions);
    }

    /**
     * Adding missed file match for handling, once reaches the defined threshold (see analyze.missed-matches-threshold)
     * It will resolve all accumulated missed matches and do necessary group joins.
     * <p>
     *
     * @param missedFileMatch - a new missed match to handle
     */
    public void handle(MissedFileMatch missedFileMatch) {
        logger.trace("Waiting for missedMatch write lock");
        missedMatchLock.writeLock().lock();
        collectedMissedMatches.add(missedFileMatch);
        missedMatchesToFlush.add(missedFileMatch);
        logger.trace("Acquired missedMatch write lock");
        logger.trace("Size: {}, Added: {}", missedMatchesToHandle.size(), missedFileMatch);
        try {
            if (missedMatchesToFlush.size() >= missedMatchesFlushToDbThreshold) {
                logger.debug("Flushing {} missed matches to DB, total: {}", missedMatchesToFlush.size(),
                        collectedMissedMatches.size());
                try {
                    fileMissedMatchBatchExecutor.flushBatch(missedMatchesToFlush);
                } finally {
                    missedMatchesToFlush.clear();
                }
            }

            if (collectedMissedMatches.size() >= missedMatchHandleThreshold) {
                updateRunningJobsStates(JobType.ANALYZE, "Handling group missed matches...");
                try {
                    logger.debug("Reached {} cycles , handling {} missed matches", missedMatchHandleThreshold, missedMatchesToHandle);
                    handleByBatch(collectedMissedMatches);
                } finally {
                    collectedMissedMatches.clear();
                    updateRunningJobsStates(JobType.ANALYZE, "Finished handling group missed matches");
                }
            }
        } catch (InterruptedException exp) {
            logger.info("Interrupted {}", exp.getMessage());
        } catch (Exception e) {
            logger.error("Failed processing missed matches", e);
        } finally {
            missedMatchLock.writeLock().unlock();
            logger.trace("Released missedMatch write lock");
        }
    }

    @SuppressWarnings("SameParameterValue")
    private void updateRunningJobsStates(JobType jobType, String message) {
        List<Long> runIds = jobManager.getRunsByJobAndState(jobType, JobState.IN_PROGRESS);
        runIds.forEach(runId -> {
            Long jobId = jobManager.getJobIdCached(runId, jobType);
            if (jobId != null) {
                JobState state = jobManager.getCachedJobState(jobId);
                if (state == null || state.isActive()) { // change status only if still active
                    jobManager.updateJobState(jobId, JobState.IN_PROGRESS, null, message, false);
                }
            }
        });
    }

    public void handleAll(long... jobIds) {
        missedMatchLock.writeLock().lock();
        try {
            updateRunningJobsStates(JobType.ANALYZE, "Handling all remained group missed matches...");
            if (!missedMatchesToFlush.isEmpty()) {
                logger.debug("Flushing {} missed matches to DB, total: {}", missedMatchesToFlush.size(),
                        collectedMissedMatches.size());
                fileMissedMatchBatchExecutor.flushBatch(missedMatchesToFlush);
            }

            logger.debug("Handling all {} remained missed matches", missedMatchesToHandle.size());
            if (!collectedMissedMatches.isEmpty()) {

                logger.debug("Creating check point for last {} remained missed matches");
                fileMissedMatchBatchExecutor.flushBatch(collectedMissedMatches);
            }
            handleByBatch(collectedMissedMatches, jobIds);
        } catch (InterruptedException exp) {
            logger.info("Interrupted {}", exp.getMessage());
        } catch (Exception e) {
            logger.error("Failed processing missed matches", e);
        } finally {
            missedMatchesToFlush.clear();
            collectedMissedMatches.clear();
            updateRunningJobsStates(JobType.ANALYZE, "Finished handling all remained group missed matches");
            missedMatchLock.writeLock().unlock();
        }

    }

    /**
     * Flush explicitly what's accumulated in the missed match handler.
     * This is an analyze-stop-the-world process, meaning that no analysis cannot proceed
     * till all accumulated missed matches are handled.
     *
     * @param jobIds - relevant job ids that acquire the coordination lock
     * @throws InterruptedException            - in case application stopped
     * @throws CoordinationLockDeniedException - in case some other analyze-finalize or ingest-finalize caught the lock
     */
    private void handleByBatch(Collection<MissedFileMatch> batchToHandle, long... jobIds) throws InterruptedException, CoordinationLockDeniedException {
        try {
            long start = System.currentTimeMillis();
            acquireCoordinationLock(jobIds);
            logger.debug("Start processing {} missed matches...", batchToHandle.size());

            MissedMatchGroupReunions missedMatchGroupReunions = new MissedMatchGroupReunions();
            fillGroupReunions(batchToHandle, missedMatchGroupReunions);
            joinGroupsAndTruncateMissedFileMatches(missedMatchGroupReunions);
            logger.debug("Done processing {} missed matches in {} ms",
                    batchToHandle.size(), System.currentTimeMillis() - start);
        } finally {
            releaseCoordinationLock();
        }
    }

    private void fillGroupReunions(Collection<MissedFileMatch> missedFileMatches, MissedMatchGroupReunions missedMatchGroupReunions) {
        long start = System.currentTimeMillis();
        Set<Long> contentIds = missedFileMatches.stream()
                .flatMap(this::collectContentIds)
                .collect(Collectors.toSet());
        Map<Long, String> groupForContents = contentGroupsCache.getContentToGroup(contentIds);
        missedFileMatches
                .stream()
                .map(missedFileMatch -> convertToGroupConflict(missedFileMatch, groupForContents))
                .filter(Objects::nonNull)
                .forEach(groupConflict -> handleGroupConflict(groupConflict, missedMatchGroupReunions));
        logger.debug("{} missed matches resulted in {} groups merged into {} reunions in {} ms",
                missedFileMatches.size(), missedMatchGroupReunions.getGroups().size(),
                missedMatchGroupReunions.getReunions().size(), System.currentTimeMillis() - start);
    }

    private void handleGroupConflict(Pair<String, String> groupConflict, MissedMatchGroupReunions missedMatchGroupReunions) {
        String gid1 = groupConflict.getKey();
        String gid2 = groupConflict.getValue();
        try {
            final Long unifiedGroupsId1 = missedMatchGroupReunions.getUnifiedGroupsId(gid1);
            final Long unifiedGroupsId2 = missedMatchGroupReunions.getUnifiedGroupsId(gid2);
            if (unifiedGroupsId1 == null && unifiedGroupsId2 == null) {        // new match
                missedMatchGroupReunions.addNewReunion(gid1, gid2);
            } else if (unifiedGroupsId1 == null || unifiedGroupsId2 == null) {
                // Already know that they are not both null
                String newGid = (unifiedGroupsId1 == null) ? gid1 : gid2;
                Long existingUnifiedGroup = (unifiedGroupsId1 == null) ? unifiedGroupsId2 : unifiedGroupsId1;
                missedMatchGroupReunions.addToExistingReunion(existingUnifiedGroup, newGid);
            } else if (!unifiedGroupsId1.equals(unifiedGroupsId2)) {    // If they are the same, do nothing. If different then...
                // Move one unifiedGroup into the other
                missedMatchGroupReunions.moveUnifiedGroupToReunion(unifiedGroupsId2, unifiedGroupsId1);
            }
            logGroupsReunions(missedMatchGroupReunions);
        } catch (Exception e) {
            logger.error("failed handleGroupMatch for {} and {}", gid1, gid2, e);
            throw e;
        }
    }

    private Pair<String, String> convertToGroupConflict(MissedFileMatch missedFileMatch, Map<Long, String> groupForContents) {
        if (missedFileMatch.getFile1Id() == null || missedFileMatch.getFile2Id() == null) {
            logger.error("something wrong record of mismatch has file id null {}", missedFileMatch);
            return null;
        }

        if (missedFileMatch.getFile1Id().equals(missedFileMatch.getFile2Id())) {
            return null;
        }

        String gid1 = groupForContents.get(missedFileMatch.getFile1Id());
        String gid2 = groupForContents.get(missedFileMatch.getFile2Id());

        if (gid1 == null || gid2 == null || gid1.equals(gid2)) {
            return null;
        }
        return Pair.of(gid1, gid2);
    }

    private void joinGroupsAndTruncateMissedFileMatches(MissedMatchGroupReunions missedMatchGroupReunions) {
        dbTemplateUtils.doInTransaction(() -> joinGroups(missedMatchGroupReunions.getReunions(), new MissedMatchesProgressTracker()));
        logger.debug("Finished missed match handling on start up");
        missedFileMatchRepository.truncateMissedFileMatchTable();
        logger.debug("Truncated missed_file_match");
    }


    private void acquireCoordinationLock(long... jobIds) throws InterruptedException, CoordinationLockDeniedException {
        logger.debug("Requesting job coordination lock for Analyze-Fin processing miss-matches");
        jobCoordinator.requestLock(JobType.ANALYZE_FINALIZE, jobIds);
        logger.debug("Acquired job coordination lock for Analyze-Fin processing miss-matches");
    }

    private void releaseCoordinationLock() {
        logger.debug("Releasing job coordination lock for Analyze-Fin processing miss-matches");
        jobCoordinator.release();
        logger.debug("Released job coordination lock for Analyze-Fin processing miss-matches");
    }

    private Stream<Long> collectContentIds(MissedFileMatch missedFileMatch) {
        ArrayList<Long> contentIds = Lists.newArrayList();
        if (missedFileMatch.getFile1Id() != null) {
            contentIds.add(missedFileMatch.getFile1Id());
        }
        if (missedFileMatch.getFile2Id() != null) {
            contentIds.add(missedFileMatch.getFile2Id());
        }
        return contentIds.stream();
    }


    private void logGroupsReunions(MissedMatchGroupReunions missedMatchGroupReunions) {
        if (logger.isDebugEnabled() && (++handleGroupMatchCounter) % 1000 == 0) {
            logger.debug("handleGroupMatch {} processed ({} groups, {} reunions)", handleGroupMatchCounter,
                    missedMatchGroupReunions.getGroups().size(), missedMatchGroupReunions.getReunions().size());
            handleGroupMatchCounter = 0;
        }
    }


    private void joinGroups(final Multimap<Long, String> new2GidMap, ProgressTracker progressTracker) {
        progressTracker.startStageTracking(510);
        Collection<Collection<String>> new2GidValues = new2GidMap.asMap().values();
        progressTracker.setCurStageEstimatedTotal(new2GidValues.size());
        // Get all groups
        Collection<Collection<SolrFileGroupEntity>> groupsToJoinCollection = new2GidValues.stream()
                .map(this::findFileGroups)
                .filter(fileGroups -> !fileGroups.isEmpty())
                .collect(Collectors.toList());
        logger.debug("Found {} groupsToJoin", groupsToJoinCollection.size());

        Set<GroupUnificationHelper> unificationToProcess = Sets.newHashSet();

        groupsToJoinCollection.forEach(groupsToJoin -> {
            unificationToProcess.addAll(analyzerDataService.replaceGroups(groupsToJoin));
            progressTracker.incrementProgress(1L);

        });
        logger.debug("Done: replace {} groups joins", unificationToProcess.size());

        Set<String> groupIdsToMarkAsDirty = unificationToProcess.stream()
                .map(GroupUnificationHelper::getNewGroupId).collect(Collectors.toSet());
        fileGroupService.markGroupAsDirtyByIds(groupIdsToMarkAsDirty);
        logger.debug("Done: markGroupAsDirtyByIds for {} new group ids", unificationToProcess.size());

        progressTracker.startStageTracking(4590);


        analyzerDataService.processGroupSwitches(unificationToProcess, progressTracker);
        logger.debug("Done: processGroupSwitches for {} unification's", unificationToProcess.size());


        analyzerDataService.handleRawAnalysisGroups();
        logger.debug("Done: handleRawAnalysisGroups");

    }


    private List<SolrFileGroupEntity> findFileGroups(Collection<String> groupIds) {
        List<SolrFileGroupEntity> fileGroupList = Lists.partition(Lists.newArrayList(groupIds), 500).
                stream().flatMap(partition -> fileGroupsProvider.apply(partition).stream())
                .collect(Collectors.toList());
        if (fileGroupList.size() < groupIds.size()) {
            logger.warn("{} groups does not exists in DB, ignoring join", groupIds.size() - fileGroupList.size());
            return Lists.newArrayList();
        }
        return fileGroupList;
    }

    public void acquireLock() {
        logger.trace("Acquired missedMatch read lock");
        missedMatchLock.readLock().lock();
    }

    public void releaseLock() {
        missedMatchLock.readLock().unlock();
        logger.trace("Released missedMatch read lock");
    }

    private class MissedMatchesProgressTracker implements ProgressTracker {

        private Set<Long> jobIds;

        MissedMatchesProgressTracker() {
            jobIds = jobManager.getJobs(JobType.ANALYZE, JobState.IN_PROGRESS).stream()
                    .map(Job::getId)
                    .collect(Collectors.toSet());
        }

        @Override
        public void startStageTracking(int stageWeight) {
        }

        @Override
        public void setCurStageEstimatedTotal(long stageTotalEstimate) {
        }

        @Override
        public boolean incrementProgress(long amount) {
            jobIds.forEach(jobId -> jobManager.updateJobCounters(jobId, -1, 0));
            return true;
        }

        @Override
        public void endCurStage() {
        }

        @Override
        public void endTracking() {
        }
    }

    private class MissedMatchGroupReunions {

        private static final String NEW_MATCH_ID_KEY_STRING = "newMatchId";

        private final Map<String, Long> gid2newMap = new HashMap<>();
        private final LinkedListMultimap<Long, String> new2GidMap = LinkedListMultimap.create();

        MissedMatchGroupReunions() {
            gid2newMap.put(NEW_MATCH_ID_KEY_STRING, 1L);
        }

        Long getUnifiedGroupsId(String gid) {
            return gid2newMap.get(gid);
        }

        Long generateNewMatchId() {
            Long newMatchId = gid2newMap.get(NEW_MATCH_ID_KEY_STRING);
            gid2newMap.put(NEW_MATCH_ID_KEY_STRING, newMatchId + 1);
            return newMatchId;
        }

        public Map<String, Long> getGroups() {
            return gid2newMap;
        }

        LinkedListMultimap<Long, String> getReunions() {
            return new2GidMap;
        }

        void addNewReunion(String gid1, String gid2) {
            Long newMatchId = generateNewMatchId();
            gid2newMap.put(gid1, newMatchId);
            gid2newMap.put(gid2, newMatchId);
            new2GidMap.put(newMatchId, gid1);
            new2GidMap.put(newMatchId, gid2);
        }

        void addToExistingReunion(Long newMatchId, String newGid) {
            new2GidMap.put(newMatchId, newGid);
            gid2newMap.put(newGid, newMatchId);
        }

        void moveUnifiedGroupToReunion(Long unifiedGroupsId2, Long unifiedGroupsId1) {
            final Collection<String> deletedList = new2GidMap.get(unifiedGroupsId2);
            new2GidMap.putAll(unifiedGroupsId1, deletedList);
            deletedList.forEach(id -> gid2newMap.put(id, unifiedGroupsId1));        // change new matchId for the forced items
            new2GidMap.removeAll(unifiedGroupsId2);                                // remove forced list from the map
        }

        public boolean isEmpty() {
            return new2GidMap.isEmpty();
        }
    }
}
