package com.cla.filecluster.batch.writer.analyze;

import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.util.query.DBTemplateUtils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * executor to persist failed tasks to db
 *
 * Created by: yael
 * Created on: 10/4/2018
 */
public class AnalyzeFailedTasksBatchExecutor extends AbstractAnalyzeBatchExecutor<Long> {
    private final SolrTaskService solrTaskService;

    public AnalyzeFailedTasksBatchExecutor(SolrTaskService solrTaskService, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.solrTaskService = solrTaskService;
    }

    @Override
    protected void flushBatch(Collection<Long> batch) {
        solrTaskService.endTaskForContents(batch);
    }
}
