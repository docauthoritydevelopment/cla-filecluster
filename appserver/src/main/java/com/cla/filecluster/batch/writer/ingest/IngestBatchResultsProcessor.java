package com.cla.filecluster.batch.writer.ingest;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrContentEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ErrProcessedFile;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class IngestBatchResultsProcessor {

    private static final Logger logger = LoggerFactory.getLogger(IngestBatchResultsProcessor.class);

    @Autowired
    private BatchIngestResultsExecutor batchIngestResultsExecutor;

    @Autowired
    private JobManagerService jobManagerService;

    public void collectMetadata(Long taskId, SolrInputDocument contentMetadata) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.CONTENT_METADATA, taskId, contentMetadata);
    }

    public void collectFileEntity(Long taskId, SolrFileEntity fileEntity) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.FILE_ENTITY, taskId, fileEntity);
    }

    public void collectIngestionError(Long taskId, ErrProcessedFile errProcessedFile) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.INGESTION_ERROR, taskId, errProcessedFile);
    }

    public void collectFileCatDocument(Long taskId, SolrInputDocument catFile) {
        // TODO remove after X500 debug
        if (logger.isTraceEnabled()) {
            SolrInputField id = catFile.getField("id");
            SolrInputField rAddr = Optional.ofNullable(catFile.getField("recipientsAddresses")).orElse(id);
            SolrInputField rNames = Optional.ofNullable(catFile.getField("recipientsNames")).orElse(id);
            SolrInputField rFAddr = Optional.ofNullable(catFile.getField("recipientsFullAddresses")).orElse(id);
            SolrInputField rDom = Optional.ofNullable(catFile.getField("recipientsDomains")).orElse(id);
            logger.trace("collectFileCatDocument: id={} rec-addr={} rec-names={} rec-full-add={} rec-dom={}",
                    id.getValue().toString(),
                    rAddr.getValue().toString(),
                    rNames.getValue().toString(),
                    rFAddr.getValue().toString(),
                    rDom.getValue().toString());
        }
        batchIngestResultsExecutor.collect(IngestBatchTaskType.CAT_FILE, taskId, catFile);
    }

    public void collectContentEntity(Long taskId, SolrContentEntity solrContentEntity) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.CONTENT_ENTITY, taskId, solrContentEntity);
    }

    public void collectGrpHelperContentMetadata(Long taskId, SolrInputDocument contentMetadata) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.GRP_HELPER, taskId, contentMetadata);
    }

    public void collectPvAnalysisData(Long taskId, SolrInputDocument pvAnalysisData) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.PV_ANALYSIS_DATA, taskId, pvAnalysisData);
    }

    public void collectHistogram(Long taskId, SolrInputDocument doc) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.HISTOGRAM, taskId, doc);
    }

    public void collectDeletedFilesById(Long taskId, String id) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.DELETED_FILES_BY_ID, taskId, id);
    }

    public void collectDirtyPopulationFlagById(Long taskId, Long selectedContentId) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.MARK_DIRTY_POPULATION_FLAG, taskId, selectedContentId);
    }

    public void collectClearSampleFileIdIfNeededByIdAndClaFileId(Long taskId, Long contentId, Long fileId) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.CLEAR_SAMPLE_FILE_ID, taskId, Pair.of(contentId, fileId));
    }

    public void collectMarkGroupAsDirty(Long taskId, String groupId) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.MARK_DIRTY_GROUP, taskId, groupId);
    }

    public void collectEmptyFiles(Long taskId, Pair<String, SolrFileEntity> emptyFile) {
        batchIngestResultsExecutor.collect(IngestBatchTaskType.EMPTY_FILES, taskId, emptyFile);
    }

    public void flush(List<Long> doneTasks, Map<Long, Integer> doneTasksJobCounter, Map<Long, Integer> failTasksJobCounter) {

        if (doneTasks == null || doneTasks.isEmpty()) { // Can happen in the case of use pause
            logger.warn("Called flush when no done tasks, skipping flush (Probably user paused)");
            return;
        }
        long start = System.currentTimeMillis();
        logger.info("Start: flushing of {} tasks results", doneTasks.size());

        batchIngestResultsExecutor.flush(doneTasks);
        failTasksJobCounter.forEach((jobId, counter) -> jobManagerService.incJobFailedCounters(jobId, counter));
        doneTasksJobCounter.forEach((jobId, counter) -> jobManagerService.incJobCounters(jobId, counter, 0));

        logger.info("Done: flushing of {} tasks results in {} ms", doneTasks.size(), System.currentTimeMillis() - start);
    }
}
