package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.service.files.ContentMetadataService;
import org.apache.solr.common.SolrInputDocument;

import java.util.List;

public class ContentMetadataBatchTaskExecutor extends AbstractIngestBatchExecutor<SolrInputDocument> {


    private ContentMetadataService contentMetadataService;

    public ContentMetadataBatchTaskExecutor(ContentMetadataService contentMetadataService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.contentMetadataService = contentMetadataService;
    }


    @Override
    protected void flushBatch(List<SolrInputDocument> batch) {
        contentMetadataService.saveAll(batch);
    }

}