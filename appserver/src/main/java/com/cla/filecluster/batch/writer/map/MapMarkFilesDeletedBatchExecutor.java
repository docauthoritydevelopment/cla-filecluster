package com.cla.filecluster.batch.writer.map;

import com.cla.filecluster.mediaproc.map.FileToDel;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.google.common.base.Strings;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 12/3/2018
 */
public class MapMarkFilesDeletedBatchExecutor extends AbstractMapBatchExecutor<FileToDel> {

    private FileCatService fileCatService;
    private FileCrawlerExecutionDetailsService runService;
    private FileGroupService fileGroupService;

    public MapMarkFilesDeletedBatchExecutor(FileCatService fileCatService,
                                            FileCrawlerExecutionDetailsService runService,
                                            FileGroupService fileGroupService,
                                            int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.fileCatService = fileCatService;
        this.runService = runService;
        this.fileGroupService = fileGroupService;
    }

    @Override
    protected void flushBatch(List<FileToDel> batch) {
        if (logger.isTraceEnabled()) {
            logger.trace("Files to delete {}", batch.stream().map(FileToDel::toString).collect(Collectors.joining(",")));
        }
        Map<Long, Long> runFiles = new HashMap<>();
        Set<Long> filesToDelete = new HashSet<>();
        Set<String> groupMarkDirty = new HashSet<>();

        batch.forEach(f -> {
            Long val = runFiles.getOrDefault(f.getRunId(), 0L);
            runFiles.put(f.getRunId(), val + 1);
            filesToDelete.add(f.getFileId());
            if (!Strings.isNullOrEmpty(f.getAnalysisGroupId())) {
                groupMarkDirty.add(f.getAnalysisGroupId());
            }
            if (!Strings.isNullOrEmpty(f.getUserGroupId())) {
                groupMarkDirty.add(f.getUserGroupId());
            }
        });

        fileCatService.processFilesDeletion(filesToDelete);
        runFiles.forEach((runId, total) ->
                runService.incDeletedFiles(runId, total)
        );

        if (!groupMarkDirty.isEmpty()) {
            fileGroupService.markGroupAsDirtyByIds(groupMarkDirty);
        }
    }
}
