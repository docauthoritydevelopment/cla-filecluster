package com.cla.filecluster.batch.writer.ingest;

import com.cla.filecluster.repository.solr.SolrAnalysisDataRepository;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PvAnalysisDataBatchExecutor extends AbstractIngestBatchExecutor<SolrInputDocument> {

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    public PvAnalysisDataBatchExecutor(SolrAnalysisDataRepository solrAnalysisDataRepository, int innerPartitionSize,int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.solrAnalysisDataRepository = solrAnalysisDataRepository;
    }

    @Override
    protected void flushBatch(List<SolrInputDocument> batch) {
        solrAnalysisDataRepository.saveAll(batch);
    }
}
