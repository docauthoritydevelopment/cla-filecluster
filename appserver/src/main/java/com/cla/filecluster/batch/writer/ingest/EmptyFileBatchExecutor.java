package com.cla.filecluster.batch.writer.ingest;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.service.categories.FileCatService;

import java.util.List;
import java.util.stream.Collectors;

/**
 * executor to handle  persisted to solr of empty files
 *
 * Created by: yael
 * Created on: 9/16/2018
 */
public class EmptyFileBatchExecutor extends AbstractIngestBatchExecutor<Pair<String, SolrFileEntity>> {

    private FileCatService fileCatService;

    public EmptyFileBatchExecutor(FileCatService fileCatService, int innerPartitionSize, int partitionUpdateConcurrency) {
        super(innerPartitionSize, partitionUpdateConcurrency);
        this.fileCatService = fileCatService;
    }

    @Override
    protected void flushBatch(List<Pair<String, SolrFileEntity>> batch) {
        long start = System.currentTimeMillis();

        List<String> idsToDelete = batch.stream().map(Pair::getKey).collect(Collectors.toList());
        List<SolrFileEntity> filesToAdd = batch.stream().map(Pair::getValue).collect(Collectors.toList());

        logger.debug("Start: Persisting total of {} empty files", batch.size());

        try {
            fileCatService.acquireFileDeleteWriteLock();
            fileCatService.deleteByCompositeIds(idsToDelete);
        } finally {
            fileCatService.releaseFileDeleteWriteLock();
        }

        fileCatService.saveEntities(filesToAdd);

        logger.debug("Done: Persisting total of {} empty files in {} ms", batch.size(), System.currentTimeMillis() - start);
    }
}
