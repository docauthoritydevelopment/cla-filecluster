package com.cla.filecluster.batch.writer.utils;

import com.cla.common.utils.UniqueIdGenerator;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.service.TaskManagerService;
import com.cla.filecluster.repository.SequenceIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.cla.common.constants.DbDatePattern.CREATED_ON_DB_PATTERN;
import static com.cla.common.utils.DbUtils.getStrOrNull;

/**
 * create tasks in batch sql builder
 *
 * Created by: yael
 * Created on: 9/20/2018
 */
public class TaskCreation {

    private static final Logger logger = LoggerFactory.getLogger(TaskCreation.class);

    private static final String TASK_BATCH_INSERT_STATMENT = "INSERT INTO `jm_tasks` (`id`, `created_ondb`, `deleted`, `retries`, `run_context`, `state`, `state_update_time`, `type`, `item_id`, `job_id`, `json_data`, `name`, `part_id`, `version`, path) VALUES %s";
    private static final String TASK_BATCH_VALUES_PATTERN = "('%d', '%s',%b, '%d', '%d', '%d', '%d', '%d', '%d', '%d', %s, %s, '%d', '%d', %s)";

    private static TimeSource timeSource = TimeSource.create();

    public static Optional<String> batchedTaskStatement(SequenceIdGenerator sequenceIdGenerator, List<SimpleTask> batch) {
        String createdOnDbStr = CREATED_ON_DB_PATTERN.format(new Date(timeSource.currentTimeMillis()));
        String batchedValues = batch.stream()
                .filter(TaskCreation::validateTask)
                .map(simpleTask -> formatTask(simpleTask, createdOnDbStr, sequenceIdGenerator))
                .collect(Collectors.joining(","));
        return Optional.of(batchedValues)
                .filter(StringUtils::isNotEmpty)
                .map(s -> String.format(TASK_BATCH_INSERT_STATMENT, s));
    }

    private static boolean validateTask(SimpleTask simpleTask){
        return
            validateNotNull(simpleTask, simpleTask.getRunContext(), "run context") &&
            validateNotNull(simpleTask, simpleTask.getState(), "state") &&
            validateNotNull(simpleTask, simpleTask.getStateUpdateTime(), "state update time") &&
            validateNotNull(simpleTask, simpleTask.getItemId(), "item ID");
    }

    private static boolean validateNotNull(SimpleTask simpleTask, Object obj, String fieldName){
        if (obj == null){
            logger.warn("Batch contains task without " + fieldName + ": " + simpleTask.toString());
            return false;
        }
        return true;
    }

    private static String formatTask(SimpleTask simpleTask, String createdOnDbStr, SequenceIdGenerator sequenceIdGenerator){
        return String.format(
                TASK_BATCH_VALUES_PATTERN,
                sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.JM_TASKS),
                createdOnDbStr,
                simpleTask.isDeleted(),
                simpleTask.getRetries(),
                simpleTask.getRunContext(),
                simpleTask.getState().ordinal(),
                simpleTask.getStateUpdateTime(),
                simpleTask.getType(),
                simpleTask.getItemId(),
                simpleTask.getJobId(),
                getStrOrNull(simpleTask.getJsonData()),
                getStrOrNull(simpleTask.getName()),
                simpleTask.getPartitionId(),
                simpleTask.getVersion(),
                getStrOrNull(simpleTask.getPath()));
    }
}
