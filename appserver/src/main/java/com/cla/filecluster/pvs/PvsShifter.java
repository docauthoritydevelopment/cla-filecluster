package com.cla.filecluster.pvs;

import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.scripts.SolrCloudConfigurer;
import com.google.common.collect.Sets;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.cla.filecluster.pvs.PvsAdminConstants.*;
import static com.cla.filecluster.scripts.SolrCloudConfigurer.getArg;
import static com.cla.filecluster.scripts.SolrCloudConfigurer.throwIfFail;

/**
 * A task that shift PVS collection:
 * 1. Create new PVS collection with increasing index (i.e. PVS1, PVS2 ...)
 * 2. Route write alias to new PVS collection.
 * 3. Add new PVS collection to read alias
 * 4. Optimize previous PVS collection
 */
public class PvsShifter {

    private static final Logger logger = LoggerFactory.getLogger(PvsShifter.class);


    private static final String PVS_TRACK_FILE = "pvs-track";


    // Defaults
    private static String ZK_HOST_DEFAULT = "localhost:2181/solr";
    private static final String COLLECTION_THRESHOLD_DEFAULT = "100";
    private static final String NUM_OF_SHARDS_DEFAULT = "1";
    private static final String PERFORM_COLLECTION_OPTIMIZE_DEFAULT = "false";
    private static final String ALLOW_READ_AND_WRITE = "true";

    // Command params index
    private static final int ZK_HOST_INDEX = 0;
    private static final int COLLECTION_THRESHOLD_INDEX = 1;
    private static final int NUM_OF_SHARDS_INDEX = 2;
    private static final int PERFORM_COLLECTION_OPTIMIZE_INDEX = 3;
    private static final int ALLOW_READ_AND_WRITE_INDEX = 4;

    private final SolrCloudConfigurer solrCloudConfigurer;
    private final ExecutorService optimizeExecutor;

    private boolean performCollectionOptimize;
    private boolean allowReadAndWrite;


    public static void main1(String[] args) {
        try {
            String zkHost = getArg(args, ZK_HOST_INDEX, ZK_HOST_DEFAULT);
            int collectionThreshold = Integer.valueOf(getArg(args, COLLECTION_THRESHOLD_INDEX,
                    COLLECTION_THRESHOLD_DEFAULT));
            int numOfShards = Integer.valueOf(getArg(args, NUM_OF_SHARDS_INDEX,
                    NUM_OF_SHARDS_DEFAULT));

            boolean performCollectionOptimize = Boolean.valueOf(getArg(args, PERFORM_COLLECTION_OPTIMIZE_INDEX,
                    PERFORM_COLLECTION_OPTIMIZE_DEFAULT));

            boolean allowReadAndWrite = Boolean.valueOf(getArg(args, ALLOW_READ_AND_WRITE_INDEX,
                    ALLOW_READ_AND_WRITE));

            SolrCloudConfigurer solrCloudConfigurer = new SolrCloudConfigurer(
                    new CloudSolrClient.Builder().withZkHost(zkHost).build());
            checkPreConditions(solrCloudConfigurer);
            initPvsTrackIfNeeded();
            String lastPVSCollection = extractLastPVSCollection();
            if (shouldShiftPVSCollection(solrCloudConfigurer, lastPVSCollection, collectionThreshold)) {
                PvsShifter pvsShifter = new PvsShifter(solrCloudConfigurer, performCollectionOptimize, allowReadAndWrite);
                String newPVSCollection = pvsShifter.shift(lastPVSCollection, numOfShards);
                trackNewPVSCollection(newPVSCollection);
            }

        } catch (Throwable t) {
            logger.error("Failed to perform PVS shift, reason: " + t.getMessage(), t);
        }
    }


    public PvsShifter(SolrCloudConfigurer solrCloudConfigurer, boolean performCollectionOptimize, boolean allowReadAndWrite) {
        this.solrCloudConfigurer = solrCloudConfigurer;
        this.performCollectionOptimize = performCollectionOptimize;
        optimizeExecutor = Executors.newFixedThreadPool(1);
        this.allowReadAndWrite = allowReadAndWrite;
    }

    public String shift(String lastPVSCollection, int numOfShards) {
        String newWritePvsCollection = createNewWritePvsCollection(lastPVSCollection, numOfShards);
        logger.info("New {} collection created", newWritePvsCollection);

        routePvsCollectionToWriteAlias(newWritePvsCollection);
        logger.info("Routed {} alias to new {} collection", PVS_WRITE_ALIAS, newWritePvsCollection);

        addPvsCollectionToReadAlias(lastPVSCollection);
        logger.info("Previous {} collection added to {} alias", lastPVSCollection, PVS_READ_ALIAS);

        if (performCollectionOptimize) {
            optimizeExecutor.submit(() -> {
                logger.info("{}: Async Optimize Started", lastPVSCollection);
                solrCloudConfigurer.optimizeNoWaitNoFlush(lastPVSCollection);
                logger.info("{}: Async Optimize Done", lastPVSCollection);
            });
        }

        if (allowReadAndWrite) {
            addPvsCollectionToReadAlias(newWritePvsCollection);
            logger.info("New {} collection added to {} alias", newWritePvsCollection, PVS_READ_ALIAS);
        }

        return newWritePvsCollection;
    }

    private static void initPvsTrackIfNeeded() throws IOException {
        if (!Files.exists(Paths.get(PVS_TRACK_FILE))) {
            Files.write(Paths.get(PVS_TRACK_FILE),
                    (PVS_COLLECTION_BASE_NAME + 1 + "\n").getBytes());
        }
    }

    private static void checkPreConditions(SolrCloudConfigurer solrCloudConfigurer) {
        Map<String, String> aliasesMap = solrCloudConfigurer.listAliases();
        if (aliasesMap == null || aliasesMap.isEmpty()) {
            throw new RuntimeException(String.format("Pre-Condition not met: %s and %s aliases are not configured",
                    PVS_READ_ALIAS, PVS_WRITE_ALIAS));
        }

        boolean pvsReadAliasExists = aliasesMap.keySet().stream().anyMatch(alias -> alias.equals(PVS_READ_ALIAS));
        boolean pvsWriteAliasExists = aliasesMap.keySet().stream().anyMatch(alias -> alias.equals(PVS_WRITE_ALIAS));
        if (!pvsWriteAliasExists && !pvsReadAliasExists) {
            throw new RuntimeException(String.format("Pre-Condition not met: %s and %s aliases are not configured",
                    PVS_READ_ALIAS, PVS_WRITE_ALIAS));
        } else if (!pvsReadAliasExists) {
            throw new RuntimeException(String.format("Pre-Condition not met: %s alias is not configured",
                    PVS_READ_ALIAS));
        } else if (!pvsWriteAliasExists) {
            throw new RuntimeException(String.format("Pre-Condition not met: %s alias is not configured",
                    PVS_WRITE_ALIAS));
        }

        List<String> collections = solrCloudConfigurer.listCollections();
        boolean firstPvsCollection = collections.stream().anyMatch(coll -> coll.equals(PVS_COLLECTION_BASE_NAME + 1));
        if (!firstPvsCollection) {
            throw new RuntimeException(String.format("Pre-Condition not met: first pvs collection %s was not created",
                    PVS_COLLECTION_BASE_NAME + 1));
        }
    }

    private static void trackNewPVSCollection(String newWritePVSCollection) {
        try {
            Files.write(
                    Paths.get(PVS_TRACK_FILE),
                    (newWritePVSCollection + "\n").getBytes(),
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to update track config with new collection %s",
                    newWritePVSCollection), e);
        }
    }

    private static boolean shouldShiftPVSCollection(SolrCloudConfigurer solrCloudConfigurer, String lastPVSCollection, int collectionThreshold) throws IOException, SolrServerException {
        SolrClient cloudSolrClient = solrCloudConfigurer.getSolrClient();
        Query countQuery = Query.create().setRows(0).setStart(0);
        QueryResponse response = cloudSolrClient.query(lastPVSCollection, countQuery.build());
        boolean result = response.getResults().getNumFound() > collectionThreshold;
        logger.debug("Threshold check: pvs count on {} is {}, above threshold %d? %b",
                lastPVSCollection, response.getResults().getNumFound(), result);
        return result;
    }

    private String createNewWritePvsCollection(String lastPvCollection, int numOfShards) {
        String newWritePVSCollection;
        if (lastPvCollection.equals(PVS_COLLECTION_BASE_NAME)) { // first time case
            newWritePVSCollection = PVS_COLLECTION_BASE_NAME + PVS_COLLECTION_SEPARATOR + 1;
        } else {
            int pvsSeparatorIndex = lastPvCollection.indexOf(PVS_COLLECTION_SEPARATOR);
            Integer lastIndex = Integer.valueOf(lastPvCollection.substring(pvsSeparatorIndex + 1));
            Integer newIndex = lastIndex + 1;
            newWritePVSCollection = PVS_COLLECTION_BASE_NAME + PVS_COLLECTION_SEPARATOR + newIndex;
        }
        if (!solrCloudConfigurer.listCollections().stream().anyMatch(coll -> coll.equals(newWritePVSCollection))) {
            throwIfFail(() -> solrCloudConfigurer.createCollection(newWritePVSCollection, numOfShards,
                    PVS_COLLECTION_BASE_NAME, SIMILARITY_COLLECTION_CREATION_RULE),
                    String.format("Failed to create new collection %s", newWritePVSCollection));
        }
        return newWritePVSCollection;
    }

    private static String extractLastPVSCollection() throws IOException {
        List<String> pvsCollectionsCreated = Files.readAllLines(Paths.get(PVS_TRACK_FILE),
                Charset.forName("utf-8"));
        return pvsCollectionsCreated.get(pvsCollectionsCreated.size() - 1);
    }


    private void routePvsCollectionToWriteAlias(String pvsCollection) {
        throwIfFail(() -> solrCloudConfigurer.createAlias(PVS_WRITE_ALIAS, pvsCollection),
                String.format("Failed to route %s to collection %s", PVS_WRITE_ALIAS, pvsCollection));
    }

    public void addPvsCollectionToReadAlias(String pvsCollection) {
        Map<String, String> aliasesMap = solrCloudConfigurer.listAliases();
        String updatedPvsCollectionsStr = aliasesMap.get(PVS_READ_ALIAS) + "," + pvsCollection;
        String[] pvsCollectionArr = Sets.newHashSet(updatedPvsCollectionsStr.split(",")).toArray(new String[]{});
        throwIfFail(() -> solrCloudConfigurer.createAlias(PVS_READ_ALIAS, pvsCollectionArr),
                String.format("Failed to add %s to %s alias", PVS_READ_ALIAS, pvsCollection));
    }


    public boolean isAllowReadAndWrite() {
        return allowReadAndWrite;
    }
}
