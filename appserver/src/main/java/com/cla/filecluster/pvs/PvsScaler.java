package com.cla.filecluster.pvs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public abstract class PvsScaler {

    private static final Logger logger = LoggerFactory.getLogger(PvsScaler.class);

    private boolean initialized = false;

    @Transactional
    public void scale() {
        if (!confirmInitialization()) {
            return;
        }

        try {
            if (shouldScale()) {
                scalePvs();
            }
        } catch (Throwable t) {
            logger.error("Failed to apply scale operation", t);
        }
    }

    @Transactional
    public boolean confirmInitialization() {
        try {
            if (!initialized) {
                initialize();
                initialized = true;
                logger.debug("PVS scaler initialized");
            }
        } catch (Throwable t) {
            logger.error("Failed to initialize PVS scaler, will ignore scale request, reason: {}", t.getMessage());
        }
        return initialized;

    }

    protected abstract void initialize();

    protected abstract void scalePvs();

    protected abstract boolean shouldScale();


}
