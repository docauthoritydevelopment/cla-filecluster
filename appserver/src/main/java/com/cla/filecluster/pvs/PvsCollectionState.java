package com.cla.filecluster.pvs;

public enum PvsCollectionState {
    READ, WRITE, READ_WRITE
}
