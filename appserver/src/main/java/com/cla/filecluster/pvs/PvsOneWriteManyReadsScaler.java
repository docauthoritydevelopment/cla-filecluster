package com.cla.filecluster.pvs;

import com.cla.filecluster.domain.entity.pv.PvsTrackRecord;
import com.cla.filecluster.repository.jpa.pvs.PvsTrackRepository;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.scripts.SolrCloudConfigurer;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;

import static com.cla.filecluster.pvs.PvsAdminConstants.PVS_READ_ALIAS;
import static com.cla.filecluster.scripts.SolrCloudConfigurer.throwIfFail;

public class PvsOneWriteManyReadsScaler extends PvsScaler {

    private static final Logger logger = LoggerFactory.getLogger(PvsOneWriteManyReadsScaler.class);

    private final SolrCloudConfigurer solrCloudConfigurer;
    private final PvsShifter pvsShifter;
    private final PvsTrackRepository pvsTrackRepository;
    private final int pvsCountThreshold;
    private final int numOfShards;

    public PvsOneWriteManyReadsScaler(SolrCloudConfigurer solrCloudConfigurer,
                                      PvsShifter pvsShifter,
                                      PvsTrackRepository pvsTrackRepository,
                                      int pvsCountThreshold,
                                      int numOfShards) {
        this.solrCloudConfigurer = solrCloudConfigurer;
        this.pvsTrackRepository = pvsTrackRepository;
        this.pvsShifter = pvsShifter;
        this.pvsCountThreshold = pvsCountThreshold;
        this.numOfShards = numOfShards;
    }

    /**
     * Recreate the PVS_Read alias without the writePvsCollection
     *
     * @param writePvsCollection
     */
    private void excludeWriteFromRead(String writePvsCollection) {
        Map<String, String> aliases = solrCloudConfigurer.listAliases();
        String collections = aliases.get(PvsAdminConstants.PVS_READ_ALIAS);
        ArrayList<String> readCollections = Lists.newArrayList(collections.split(","));
        readCollections.remove(writePvsCollection);
        throwIfFail(() -> solrCloudConfigurer.createAlias(PVS_READ_ALIAS, readCollections.toArray(new String[]{})),
                String.format("Failed to point %s to collections %s", PVS_READ_ALIAS, readCollections));
    }

    @Override
    protected void scalePvs() {
        pvsTrackRepository.findTopRecordById()
                .ifPresent(lastPvsTrackRecord -> {
                    logger.info("Start: PVS shift process");
                    String newPVSCollection = pvsShifter.shift(lastPvsTrackRecord.getName(), numOfShards);
                    trackNewPvsStates(lastPvsTrackRecord, newPVSCollection);
                    logger.info("Done: PVS shift process, new collection is {}", newPVSCollection);
                });
    }

    @Override
    protected boolean shouldScale() {
        return pvsTrackRepository.findTopRecordById()
                .map(this::shouldShiftPVSCollection)
                .orElseGet(() -> {
                    logger.warn("Failed to find last pvs track record, please verify pvs_track_record was initialized");
                    return false;
                });
    }

    @Override
    protected void initialize() {
        if (pvsTrackRepository.count() == 0) {
            logger.info("Setup initial PVS Distribution");

            PvsTrackRecord firstPvsTrackRecord = PvsTrackRecord.builder()
                    .name(PvsAdminConstants.PVS_COLLECTION_BASE_NAME)
                    .collectionState(PvsCollectionState.READ_WRITE)
                    .build();

            pvsTrackRepository.save(firstPvsTrackRecord);
            logger.info("Done: Collection {} is now tracked", firstPvsTrackRecord.getName());
        } else {
            PvsTrackRecord lastPvsTrackRecord = pvsTrackRepository.findTopRecordById()
                    .orElseThrow(() -> new RuntimeException("failed getting last pvs track record " +
                            "even though count is not zero, please check"));

            if (lastPvsTrackRecord.getName().equals(PvsAdminConstants.PVS_COLLECTION_BASE_NAME)) {
                logger.info("Only {} collection exists, solr.pvs-scaler.allow-read-write is ignored in such case",
                        PvsAdminConstants.PVS_COLLECTION_BASE_NAME);
                return;
            }
            logger.info("Current write pvs record: {}", lastPvsTrackRecord);
            // We need to tune the PVS_Read to exclude write collection
            if (!pvsShifter.isAllowReadAndWrite() &&
                    lastPvsTrackRecord.getCollectionState() == PvsCollectionState.READ_WRITE) {
                lastPvsTrackRecord.setCollectionState(PvsCollectionState.WRITE);
                logger.info("Set policy of exclude write from read for {}", lastPvsTrackRecord.getName());
                excludeWriteFromRead(lastPvsTrackRecord.getName());
            }
            // We need to tune the PVS_Read to include the write collection
            else if (pvsShifter.isAllowReadAndWrite() &&
                    lastPvsTrackRecord.getCollectionState() == PvsCollectionState.WRITE) {
                lastPvsTrackRecord.setCollectionState(PvsCollectionState.READ_WRITE);
                logger.info("Set policy of allow write and read");
                pvsShifter.addPvsCollectionToReadAlias(lastPvsTrackRecord.getName());
            }
            pvsTrackRepository.save(lastPvsTrackRecord);
        }
    }

    private void trackNewPvsStates(PvsTrackRecord lastPvsTrackRecord, String newPVSCollection) {
        pvsTrackRepository.save(PvsTrackRecord.builder()
                .name(newPVSCollection)
                .collectionState(pvsShifter.isAllowReadAndWrite() ? PvsCollectionState.READ_WRITE : PvsCollectionState.WRITE)
                .build());
        lastPvsTrackRecord.setCollectionState(PvsCollectionState.READ);
        pvsTrackRepository.save(lastPvsTrackRecord);
    }

    private boolean shouldShiftPVSCollection(PvsTrackRecord lastPvsTrackRecord) {
        try {
            SolrClient solrClient = solrCloudConfigurer.getSolrClient();
            Query countQuery = Query.create().setRows(0).setStart(0);
            QueryResponse response = solrClient.query(lastPvsTrackRecord.getName(), countQuery.build());
            boolean result = response.getResults().getNumFound() > pvsCountThreshold;
            logger.debug("Threshold check: pvs count on {} is {}, above threshold {}: {}",
                    lastPvsTrackRecord.getName(), response.getResults().getNumFound(), pvsCountThreshold, result);
            return result;
        } catch (Throwable t) {
            throw new RuntimeException("Failed to verify is need to scale PVS", t);
        }
    }
}
