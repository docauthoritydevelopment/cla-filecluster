package com.cla.filecluster.pvs;

public final class PvsAdminConstants {

    // PVS Collection & Aliases
    static final String PVS_COLLECTION_SEPARATOR= "_";
    static final String PVS_COLLECTION_BASE_NAME = "PVS";
    static final String PVS_WRITE_ALIAS = PVS_COLLECTION_BASE_NAME + PVS_COLLECTION_SEPARATOR + "Write";
    static final String PVS_READ_ALIAS = PVS_COLLECTION_BASE_NAME + PVS_COLLECTION_SEPARATOR + "Read";

    static final String SIMILARITY_COLLECTION_CREATION_RULE = "sysprop.SIM_node:!0";
}
