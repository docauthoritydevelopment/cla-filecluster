package com.cla.filecluster.actions.base;


import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;

/**
 * Created by shtand on 15/02/2016.
 */
public interface DocAuthorityActionClassExecutor {

    public static final String TARGET = "target";

    /**
     * Init is called once - right after the executor is created
     */
    void init(ActionHelper actionHelper);

    /**
     * Apply is called after a set of actions was sent to the executor.
     * Apply is only called after prepare was called (once for each prepare).
     */
    void apply();

    /**
     * Prepare is called before a set of actions is sent to the executor.
     * @param actionExecutionTaskStatusDto
     */
    void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto);
}
