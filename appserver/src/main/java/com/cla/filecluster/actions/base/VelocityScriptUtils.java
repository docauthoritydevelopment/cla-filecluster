package com.cla.filecluster.actions.base;

import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.File;

/**
 * Created by uri on 13/03/2016.
 */
public class VelocityScriptUtils {

    public static String getVelocityScriptTemplateName(String defaultScriptName) {
        String userScript = defaultScriptName + ".custom";
        File f = new File(userScript);
        if (f.exists()) {
            return userScript;
        }
        return defaultScriptName;
    }

    public static void addGenericVelocityParameters(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto, int page, VelocityContext context) {
        context.put(DocAuthorityAction.LOG_PATH, actionExecutionTaskStatusDto.getLogPath());
        context.put(DocAuthorityAction.EXECUTION_ID, actionExecutionTaskStatusDto.getActionTriggerId());
        context.put(DocAuthorityAction.PAGE, page);
    }

    public static Template createVelocityTemplate(String defaultTemplateFileName, String velocityRuntimeLog) {
        final VelocityEngine ve = new VelocityEngine();
        ve.setProperty("runtime.log", velocityRuntimeLog);
        ve.init();
        String templateFileName = getVelocityScriptTemplateName(defaultTemplateFileName);
        return ve.getTemplate(templateFileName);
    }

    public static String getSanitizedValue(String value) {
        return value.replaceAll("\\W", "");
    }
}
