package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.actions.base.*;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 09/03/2016.
 */
public class StoragePolicyScriptGenerator implements DocAuthorityActionClassExecutor {

    private static final int ACTIONS_PER_FILE = 10000;
    private ActionHelper actionHelper;

    private ActionExecutionTaskStatusDto actionExecutionTaskStatusDto;

    private String velocityRuntimeLog = "./logs/velocity.log";

    private String scriptTemplateFileName = "./config/actions/storagePolicyScriptGenerator.vm";
    private String listTemplateFileName = "./config/actions/storagePolicyListGenerator.vm";

    private Logger logger = LoggerFactory.getLogger(StoragePolicyScriptGenerator.class);

    private List<FileOperationRequest> fileOperationRequests;

    @Override
    public void init(ActionHelper actionHelper) {
        this.actionHelper = actionHelper;
    }

    public static final String COPY_OPERATION = "COPY";
    public static final String MOVE_OPERATION = "MOVE";
    public static final String UPLOAD_OPERATION = "UPLOAD";
    public static final String DOWNLOAD_OPERATION = "DOWNLOAD";
    public static final String MOVE_UPLOAD_OPERATION = "MOVE_TO_REMOTE";
    public static final String MOVE_DOWNLOAD_OPERATION = "MOVE_FROM_REMOTE";

    public static final String TARGET = "target";

    @Override
    public void apply() {
        logger.debug("Creating access management script");
        File actionScriptFolder = actionHelper.getActionClassScriptsFolder(actionExecutionTaskStatusDto, ActionClass.ACCESS_MANAGEMENT);
        int page = actionExecutionTaskStatusDto.getPage();
        Iterable<List<FileOperationRequest>> partition = Iterables.partition(fileOperationRequests, ACTIONS_PER_FILE);
        for (List<FileOperationRequest> operationRequests : partition) {
            page++;
//            String scriptName = actionScriptFolder.getAbsolutePath() + File.separator + "storage_policy_script"+page+".bat";
            String scriptName = generateScriptName("", actionExecutionTaskStatusDto.getActionTriggerId(), actionScriptFolder, page, "bat");
            String listScriptName = generateScriptName("list_", actionExecutionTaskStatusDto.getActionTriggerId(), actionScriptFolder, page, "txt");
            try (FileWriter writer = new FileWriter(scriptName); FileWriter listWriter = new FileWriter(listScriptName)) {

                final Template scriptTemplate = VelocityScriptUtils.createVelocityTemplate(scriptTemplateFileName, velocityRuntimeLog);
                final Template listTemplate = VelocityScriptUtils.createVelocityTemplate(listTemplateFileName, velocityRuntimeLog);

                final VelocityContext context = new VelocityContext();

                List<Map> copyFileRequests = new ArrayList<>();
                List<Map> moveFileRequests = new ArrayList<>();
                // <ExternalMediaType, <Target, List<ReqParams>>>
                Map<String, List<Map<String, String>>> externalCopyFileRequests = Maps.newHashMap();

                for (FileOperationRequest fileOperationRequest : operationRequests) {
                    processFileOperationRequest(copyFileRequests, moveFileRequests, externalCopyFileRequests, fileOperationRequest);
                }
                context.put("copyFileRequests", copyFileRequests);
                context.put("moveFileRequests", moveFileRequests);
                context.put("externalFileOperationRequests", externalCopyFileRequests);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d HH:mm");
                context.put("nowStr", sdf.format(new Date()));
//                Set<String> relativeFolders = operationRequests.stream().map(r -> (String)r.getActionParams().get(DocAuthorityAction.RELATIVE_PATH)).collect(Collectors.toSet());
//                context.put("relFolders",Lists.newArrayList(relativeFolders));
                VelocityScriptUtils.addGenericVelocityParameters(actionExecutionTaskStatusDto, page, context);
                scriptTemplate.merge(context, writer);
                listTemplate.merge(context, listWriter);
            } catch (IOException e) {
                logger.error("IO Exception while creating script {}", e.getMessage(), e);
                throw new RuntimeException("IO Exception while creating script " + e.getMessage(), e);
            }
            logger.info("Finished creating script {}", scriptName);
        }
        actionExecutionTaskStatusDto.setPage(page);
        fileOperationRequests.clear();
    }

    private void processFileOperationRequest(List<Map> copyFileRequests,
                                             List<Map> moveFileRequests,
                                             Map<String, List<Map<String, String>>> externalCopyFileRequests,
                                             FileOperationRequest fileOperationRequest) {

        Map<String, String> request = new HashMap<>();
        Properties actionParams = fileOperationRequest.getActionParams();
        request.put("rootPath", (String) actionParams.get(DocAuthorityAction.ROOT_PATH));
        request.put("rootAltBase", DocFolderUtils.pathToSimpleString((String) actionParams.get(DocAuthorityAction.ROOT_PATH)));
        request.put("basename", (String) actionParams.get(DocAuthorityAction.BASE_NAME));
        String target = fileOperationRequest.getTarget();
        String relPath = (String) actionParams.get(DocAuthorityAction.RELATIVE_PATH);
        request.put("isContainer",Boolean.valueOf(actionParams.get(DocAuthorityAction.IS_CONTAINER) != null).toString());
        if (actionParams.containsKey(DocAuthorityAction.EXTERNAL_MEDIA_URL)) {
            String url = getDomainFromUrl(actionParams.getProperty(DocAuthorityAction.EXTERNAL_MEDIA_URL));
            String relTarget = getTargetRelPath(target, url);
            request.put("relPath", relPath);
            request.put("url", url);
            request.put("media_type", actionParams.getProperty(DocAuthorityAction.EXTERNAL_MEDIA_TYPE));
            request.put(TARGET, relTarget);
            addExternalMediaOperationRequest(fileOperationRequest, request, relTarget, externalCopyFileRequests);
        } else { // file share operation
            request.put("relPath", addLastSlashIfMissing(target, relPath));
            request.put(TARGET, target);
            if (fileOperationRequest.getOperation().equals(COPY_OPERATION)) {
                copyFileRequests.add(request);
            } else if (fileOperationRequest.getOperation().equals(MOVE_OPERATION) ||
                    fileOperationRequest.getOperation().equals(MOVE_DOWNLOAD_OPERATION) ||
                    fileOperationRequest.getOperation().equals(MOVE_UPLOAD_OPERATION)) {
                moveFileRequests.add(request);
            }
        }
    }

    static String addLastSlashIfMissing(String example, String path) {
        if (Strings.isNullOrEmpty(path)) {
            return path;
        }
        String slash = "\\";
        if (example.contains("/")) {
            slash = "/";
        }
        if (!path.endsWith(slash)) {
            return path.concat(slash);
        }
        return path;
    }

    private String getDomainFromUrl(String url) {
        if (!(url.startsWith("http://") || url.startsWith("https://"))) {
            return url;
        }

        int dblslsh = url.indexOf("://");
        int lastslash = url.indexOf("/", dblslsh + 3);
        if (lastslash > -1) {
            url = url.substring(0, lastslash);
        }

        return url;
    }

    private String getTargetRelPath(String path, String url) {
        if (url == null || !path.startsWith(url)) {
            return path;
        }
        return path.substring(url.length());
    }


    // <Operation, <ExternalMediaUrl, <Target, List<ReqParams>>>>
    private void addExternalMediaOperationRequest(FileOperationRequest fileOperationRequest,
                                                  Map<String, String> requestParams,
                                                  String target,
                                                  Map<String, List<Map<String, String>>> externalOperationMap) {

        Properties props = fileOperationRequest.getActionParams();
        //fileOperationRequest.getOperation() ::: props.getProperty(DocAuthorityAction.EXTERNAL_MEDIA_TYPE) ::: target
        String key = createExternalOperationMapKey(fileOperationRequest, props, target);
        List<Map<String, String>> targetOperationList = externalOperationMap.computeIfAbsent(key, (k) -> new ArrayList<>());
        targetOperationList.add(requestParams);
    }

    private String createExternalOperationMapKey(FileOperationRequest fileOperationRequest, Properties props, String target) {
        return fileOperationRequest.getOperation() +
                "::" +
                props.getProperty(DocAuthorityAction.EXTERNAL_MEDIA_TYPE) +
                "::" +
                props.getProperty(DocAuthorityAction.ROOT_PATH) +
                "::" +
                target;
    }

    public static String generateScriptName(long actionTriggerId, File actionScriptFolder, int page) {
        return generateScriptName("", actionTriggerId, actionScriptFolder, page, "bat");
    }

    public static String generateScriptName(String prefix, long actionTriggerId, File actionScriptFolder, int page, String extension) {
        if (prefix == null) {
            prefix = "";
        }
        return String.format("%s%s%s%s_%04d_%04d.%s",
                actionScriptFolder.getAbsolutePath(), File.separator, prefix, "storagePolicyScript", actionTriggerId, page, extension);
    }

    @Override
    public void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        this.actionExecutionTaskStatusDto = actionExecutionTaskStatusDto;
        fileOperationRequests = new ArrayList<>();
    }

    private void addRequest(BaseActionParams baseActionParams, Properties actionParameters, String actionId, String operation) {
        String path = baseActionParams.getFileUrl();
        String target = actionHelper.extractSingleParameter("target", actionParameters, actionId, path);
        FileOperationRequest fileOperationRequest = FileOperationRequest.createSourceTargetOperation(path, target, actionParameters, operation);
        fileOperationRequests.add(fileOperationRequest);
    }

    public void copyFile(BaseActionParams baseActionParams, Properties actionParameters) {
        String operation = COPY_OPERATION;
        // Case share point convert to UPLOAD/DOWNLOAD
        if (Objects.equals(actionParameters.getProperty(DocAuthorityAction.EXTERNAL_MEDIA_TYPE),
                MediaType.SHARE_POINT.name())) {
            boolean isSrcLocal = isFileSharePath(actionParameters.getProperty(DocAuthorityAction.ROOT_PATH));
            boolean isTargetLocal = isFileSharePath(actionParameters.getProperty(TARGET));
            if (isSrcLocal ^ isTargetLocal) {
                operation = isSrcLocal ? UPLOAD_OPERATION : DOWNLOAD_OPERATION;
            }
        }
        addRequest(baseActionParams, actionParameters, "copyFile", operation);
    }

    public void moveFile(BaseActionParams baseActionParams, Properties actionParameters) {
        String operation = MOVE_OPERATION;
        if (Objects.equals(actionParameters.getProperty(DocAuthorityAction.EXTERNAL_MEDIA_TYPE),
                MediaType.SHARE_POINT.name())) {
            boolean isSrcLocal = isFileSharePath(actionParameters.getProperty(DocAuthorityAction.ROOT_PATH));
            boolean isTargetLocal = isFileSharePath(actionParameters.getProperty(TARGET));
            if (isSrcLocal ^ isTargetLocal) {
                operation = isSrcLocal ? MOVE_UPLOAD_OPERATION : MOVE_DOWNLOAD_OPERATION;
            }
        }
        addRequest(baseActionParams, actionParameters, "moveFile", operation);
    }

    private boolean isFileSharePath(String path) {
        try {
            Paths.get(path);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
