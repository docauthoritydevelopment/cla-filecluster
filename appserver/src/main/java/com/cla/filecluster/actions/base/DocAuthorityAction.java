package com.cla.filecluster.actions.base;

/**
 * Created by uri on 02/02/2016.
 */
public interface DocAuthorityAction {
    String FULL_PATH = "fullPath";
    String BASE_NAME = "baseName";
    String ROOT_PATH = "rootPath";
    String DOC_TYPE = "docType";
    String TAGS = "tags";
    String RELATIVE_PATH = "relPath";
    String LOG_PATH = "logPath";
    String EXECUTION_ID = "executionId";
    String PAGE = "page";
    String EXTERNAL_MEDIA_URL = "externalMediaUrl";
    String EXTERNAL_MEDIA_TYPE = "externalMediaType";
    String IS_PACKAGE = "isPackage";
    String IS_CONTAINER = "isContainer";
}
