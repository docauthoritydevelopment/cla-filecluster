package com.cla.filecluster.actions.executors;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.*;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.google.common.collect.Iterables;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by uri on 09/03/2016.
 */
public class IonicEncryptionScriptGenerator implements DocAuthorityActionClassExecutor {

    private static final int ACTIONS_PER_FILE = 1000;
    public static final String ACTION_EXECUTOR_NAME = "IONIC_ENCRYPTION_SCRIPT_GENERATOR";
    private ActionHelper actionHelper;

    private ActionExecutionTaskStatusDto actionExecutionTaskStatusDto;

    private String velocityRuntimeLog = "./logs/velocity.log";

    private String defaultTemplateFileName = "./config/actions/ionicEncryptionScriptGenerator.vm";
    private Logger logger = LoggerFactory.getLogger(IonicEncryptionScriptGenerator.class);

    private List<FileOperationRequest> fileOperationRequests;

    TaggingValueBuilder taggingValueBuilder;

    @Override
    public void init(ActionHelper actionHelper) {
        this.actionHelper = actionHelper;
        Map<String, Object> actionProperties = actionHelper.getActionExecutorProperties(ACTION_EXECUTOR_NAME);
        for (Map.Entry<String, Object> stringObjectEntry : actionProperties.entrySet()) {
            logger.debug("{} property {}={}",ACTION_EXECUTOR_NAME,stringObjectEntry.getKey(),stringObjectEntry.getValue());
        }
        taggingValueBuilder = new TaggingValueBuilder(actionProperties);
    }


    private static final String ENCRYPT_OPERATION = "encrypt";

    private static final String DECRYPT_OPERATION = "decrypt";

    @Override
    public void apply() {
        logger.debug("Creating access management script");
        File actionScriptFolder = actionHelper.getActionClassScriptsFolder(actionExecutionTaskStatusDto, ActionClass.ACCESS_MANAGEMENT);
        int page = actionExecutionTaskStatusDto.getPage();
        Iterable<List<FileOperationRequest>> partition = Iterables.partition(fileOperationRequests, ACTIONS_PER_FILE);
        String scriptBaseName = "ionicEncryptionScript";
        if (fileOperationRequests.size() > 0
                && fileOperationRequests.get(0).getOperation().equals(DECRYPT_OPERATION)) {
            scriptBaseName = "ionicDecryptionScript";
        }
        for (List<FileOperationRequest> operationRequests : partition) {
            page++;
            String scriptName = String.format("%s%s%s_%04d_%04d.bat",
                    actionScriptFolder.getAbsolutePath(), File.separator, scriptBaseName, actionExecutionTaskStatusDto.getActionTriggerId(), page);
            try (FileWriter writer = new FileWriter(scriptName)) {
                final Template t = VelocityScriptUtils.createVelocityTemplate(defaultTemplateFileName, velocityRuntimeLog);

                final VelocityContext context = new VelocityContext();

                List<Map> fileRequests = new ArrayList<>();
                for (FileOperationRequest fileOperationRequest : operationRequests) {
                    Map<String,String> request = new HashMap<>();
                    Properties actionParams = fileOperationRequest.getActionParams();
                    String path = fileOperationRequest.getPath();
                    request.put("operation",fileOperationRequest.getOperation());
                    request.put("fullPath",fileOperationRequest.getPath());
                    request.put("rootPath",(String)actionParams.get(DocAuthorityAction.ROOT_PATH));
                    request.put("relPath",(String)actionParams.get(DocAuthorityAction.RELATIVE_PATH));
                    request.put("isContainer",Boolean.valueOf(actionParams.get(DocAuthorityAction.IS_CONTAINER) != null).toString());
                    request.put("basename",(String)actionParams.get(DocAuthorityAction.BASE_NAME));
                    request.put("rootAltBase", DocFolderUtils.pathToSimpleString((String)actionParams.get(DocAuthorityAction.ROOT_PATH)));
                    request.put("target",fileOperationRequest.getTarget());
//                    String target = fileOperationRequest.getTarget();
//                    request.put(TARGET, target);
                    if (fileOperationRequest.getOperation().equals(ENCRYPT_OPERATION)) {
                        StringBuilder markings = new StringBuilder();
                        if (actionParams.containsKey("dataMarkings")) {
                            markings.append(actionParams.getProperty("dataMarkings"));
                            logger.debug("Request includes dataMarkings: {}",markings);
                        };
                        if (actionParams.containsKey(DocAuthorityAction.DOC_TYPE)) {
                            String property = actionParams.getProperty(DocAuthorityAction.DOC_TYPE);
                            markings.append(" docType:").append(VelocityScriptUtils.getSanitizedValue(property));
                        }
                        if (actionParams.containsKey(DocAuthorityAction.TAGS)) {
                            String tags = actionParams.getProperty(DocAuthorityAction.TAGS);
                            taggingValueBuilder.addTagMarkings(tags,markings);
                        }
                        if (markings.length() > 0) {
                            if (markings.charAt(0) == ' ') {
                                markings.deleteCharAt(0);
                            }
                            request.put("dataMarkings",markings.toString());
                        }
                    }
                    fileRequests.add(request);
                }
                context.put("fileRequests", fileRequests);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d HH:mm");
                context.put("nowStr", sdf.format(new Date()));
//                Set<String> relativeFolders = operationRequests.stream().map(r -> (String)r.getActionParams().get(DocAuthorityAction.RELATIVE_PATH)).collect(Collectors.toSet());
//                context.put("relFolders",Lists.newArrayList(relativeFolders));
                VelocityScriptUtils.addGenericVelocityParameters(actionExecutionTaskStatusDto,page,context);
                t.merge(context, writer);
            }
            catch (IOException e) {
                logger.error("IO Exception while creating script {}",e.getMessage(),e);
                throw new RuntimeException("IO Exception while creating script "+e.getMessage(),e);
            }
            logger.info("Finished creating script {}",scriptName);
        }
        actionExecutionTaskStatusDto.setPage(page);
        fileOperationRequests.clear();
    }


    @Override
    public void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        this.actionExecutionTaskStatusDto = actionExecutionTaskStatusDto;
        fileOperationRequests = new ArrayList<>();
    }

    public void encryptFile(BaseActionParams baseActionParams, Properties actionParameters) {
        String path = baseActionParams.getFileUrl();
        String target = actionHelper.extractSingleParameter("target",actionParameters,"encryptFile", path);; // This is the target dir
        FileOperationRequest fileOperationRequest = FileOperationRequest.createSourceTargetOperation(path,target,actionParameters,ENCRYPT_OPERATION);
        fileOperationRequests.add(fileOperationRequest);
    }

    public void decryptFile(BaseActionParams baseActionParams, Properties actionParameters) {
        String path = baseActionParams.getFileUrl();
        String target = actionHelper.extractSingleParameter("target",actionParameters,"decryptFile", path);; // This is the target dir
        FileOperationRequest fileOperationRequest = FileOperationRequest.createSourceTargetOperation(path,target,actionParameters,DECRYPT_OPERATION);
        fileOperationRequests.add(fileOperationRequest);
    }

}
