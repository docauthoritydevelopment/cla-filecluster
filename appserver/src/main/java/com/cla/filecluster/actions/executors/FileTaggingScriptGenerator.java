package com.cla.filecluster.actions.executors;

import com.cla.filecluster.actions.base.*;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by uri on 09/03/2016.
 */
public class FileTaggingScriptGenerator implements DocAuthorityActionClassExecutor {

    private static final int ACTIONS_PER_FILE = 10000;
    private ActionHelper actionHelper;

    private ActionExecutionTaskStatusDto actionExecutionTaskStatusDto;

    private String velocityRuntimeLog = "./logs/velocity.log";

    private String defaultTemplateFileName = "./config/actions/fileTaggingScriptGenerator3.vm";
    private Logger logger = LoggerFactory.getLogger(FileTaggingScriptGenerator.class);

    private List<FileOperationRequest> fileOperationRequests;


    private static final String SET_TAGS_OPERATION = "set";
    private static final String ADD_TAGS_OPERATION = "add";
    private static final String REM_TAGS_OPERATION = "rem";

    public static final String ACTION_EXECUTOR_NAME = "FILE_TAGGING_SCRIPT_GENERATOR";


    public static final String PROPERTY = "property";

    TaggingValueBuilder taggingValueBuilder;

    String docTypePrefix;
    @Override
    public void init(ActionHelper actionHelper) {
        this.actionHelper = actionHelper;
        Map<String, Object> actionProperties = actionHelper.getActionExecutorProperties(ACTION_EXECUTOR_NAME);
        for (Map.Entry<String, Object> stringObjectEntry : actionProperties.entrySet()) {
            logger.debug("{} property {}={}",ACTION_EXECUTOR_NAME,stringObjectEntry.getKey(),stringObjectEntry.getValue());
        }
        docTypePrefix = (String) actionProperties.getOrDefault("docTypePrefix", "docType:");
        taggingValueBuilder = new TaggingValueBuilder(actionProperties);
    }

    @Override
    public void apply() {
        logger.debug("Creating file tagging script");
        File actionScriptFolder = actionHelper.getActionClassScriptsFolder(actionExecutionTaskStatusDto, ActionClass.TAGGING);
        int page = actionExecutionTaskStatusDto.getPage();
        Iterable<List<FileOperationRequest>> partition = Iterables.partition(fileOperationRequests, ACTIONS_PER_FILE);
        for (List<FileOperationRequest> operationRequests : partition) {
            page++;
            String scriptName = generateScriptName(actionExecutionTaskStatusDto.getActionTriggerId(), actionScriptFolder, page);
            try (FileWriter writer = new FileWriter(scriptName)) {
                final Template t = VelocityScriptUtils.createVelocityTemplate(defaultTemplateFileName, velocityRuntimeLog);

                final VelocityContext context = new VelocityContext();

                List<Map> fileRequests = new ArrayList<>();
                for (FileOperationRequest fileOperationRequest : operationRequests) {
                    Map<String,String> request = new HashMap<>();
                    Properties actionParams = fileOperationRequest.getActionParams();
                    request.put("rootPath",(String)actionParams.get(DocAuthorityAction.ROOT_PATH));
                    request.put("rootAltBase", DocFolderUtils.pathToSimpleString((String)actionParams.get(DocAuthorityAction.ROOT_PATH)));
                    request.put("relPath",(String)actionParams.get(DocAuthorityAction.RELATIVE_PATH));
                    request.put("basename",(String)actionParams.get(DocAuthorityAction.BASE_NAME));
                    request.put("isContainer",Boolean.valueOf(actionParams.get(DocAuthorityAction.IS_CONTAINER) != null).toString());

//                    request.put("backupFolder",actionHelper.getBackupFolder());
                    request.put("fullPath",fileOperationRequest.getPath());
                    request.put("target",fileOperationRequest.getTarget());
                    String value = calculateTagValue(fileOperationRequest);
                    request.put("value", value);
                    request.put("property",fileOperationRequest.getProperty());
                    request.put("operation",fileOperationRequest.getOperation());
                    fileRequests.add(request);
                }
                context.put("fileRequests", fileRequests);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d HH:mm");
                context.put("nowStr", sdf.format(new Date()));
//                Set<String> relativeFolders = operationRequests.stream().map(r -> (String)r.getActionParams().get(DocAuthorityAction.RELATIVE_PATH)).collect(Collectors.toSet());
//                context.put("relFolders",Lists.newArrayList(relativeFolders));
                VelocityScriptUtils.addGenericVelocityParameters(actionExecutionTaskStatusDto,page,context);
                t.merge(context, writer);
            }
            catch (IOException e) {
                logger.error("IO Exception while creating script {}",e.getMessage(),e);
                throw new RuntimeException("IO Exception while creating script "+e.getMessage(),e);
            }
            logger.info("Finished creating script {}",scriptName);
        }
        actionExecutionTaskStatusDto.setPage(page);
        fileOperationRequests.clear();
    }

    private String calculateTagValue(FileOperationRequest fileOperationRequest) {
        String baseValue = fileOperationRequest.getValue();
        if (StringUtils.isNotEmpty(baseValue) && !baseValue.equals("-")) {
            return baseValue;
        }
        else {
            //Calculate the tag value automatically from docType and tags
            Properties actionParams = fileOperationRequest.getActionParams();
            StringBuilder value = new StringBuilder();
            if (actionParams.containsKey(DocAuthorityAction.DOC_TYPE)) {
                String property = actionParams.getProperty(DocAuthorityAction.DOC_TYPE);
                value.append(" ").append(docTypePrefix).append(VelocityScriptUtils.getSanitizedValue(property));
            }
            if (actionParams.containsKey(DocAuthorityAction.TAGS)) {
                String tags = actionParams.getProperty(DocAuthorityAction.TAGS);
                taggingValueBuilder.addTagMarkings(tags,value);
            }
            if (value.length() > 0 && value.charAt(0) == ' ') {
                value.deleteCharAt(0);
            }
            return value.toString();
        }
    }

    public static String generateScriptName(long actionTriggerId, File actionScriptFolder, int page) {
        return String.format("%s%s%s_%04d_%04d.bat",
                actionScriptFolder.getAbsolutePath(), File.separator, "taggingPolicyScript", actionTriggerId, page);
    }

    @Override
    public void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        this.actionExecutionTaskStatusDto = actionExecutionTaskStatusDto;
        fileOperationRequests = new ArrayList<>();
    }

    public void setPropertyValues(BaseActionParams baseActionParams, Properties actionParameters) {
        String path = baseActionParams.getFileUrl();
        String value = actionHelper.extractSingleParameterIfExists("value",actionParameters,"setPropertyValues", path);
        String property = actionHelper.extractSingleParameter("property",actionParameters,"setPropertyValues", path);;
        String target = actionHelper.extractSingleParameter("tempFolder",actionParameters,"setPropertyValues", path);;
        FileOperationRequest fileOperationRequest = FileOperationRequest.createTaggingRequest(path,value,property,target,actionParameters,SET_TAGS_OPERATION);
        fileOperationRequests.add(fileOperationRequest);
    }

    public void addPropertyValue(BaseActionParams baseActionParams, Properties actionParameters) {
        String path = baseActionParams.getFileUrl();
        String value = actionHelper.extractSingleParameterIfExists("value",actionParameters,"addPropertyValue", path);
        String property = actionHelper.extractSingleParameter("property",actionParameters,"addPropertyValue", path);;
        String target = actionHelper.extractSingleParameter("tempFolder",actionParameters,"addPropertyValue", path);;
        FileOperationRequest fileOperationRequest = FileOperationRequest.createTaggingRequest(path,value,property,target,actionParameters,ADD_TAGS_OPERATION);
        fileOperationRequests.add(fileOperationRequest);
    }

    public void remPropertyValue(BaseActionParams baseActionParams, Properties actionParameters) {
        String path = baseActionParams.getFileUrl();
        String value = actionHelper.extractSingleParameterIfExists("value",actionParameters,"remPropertyValue", path);
        String property = actionHelper.extractSingleParameter("property",actionParameters,"remPropertyValue", path);;
        String target = actionHelper.extractSingleParameter("tempFolder",actionParameters,"remPropertyValue", path);;
        FileOperationRequest fileOperationRequest = FileOperationRequest.createTaggingRequest(path,value,property,target,actionParameters,REM_TAGS_OPERATION);
        fileOperationRequests.add(fileOperationRequest);
    }

}
