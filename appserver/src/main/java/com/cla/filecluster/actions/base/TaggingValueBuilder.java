package com.cla.filecluster.actions.base;

import com.cla.common.domain.dto.filetag.FileTagMinimalDto;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by uri on 19-Apr-17.
 */
public class TaggingValueBuilder {

    private static final Logger logger = LoggerFactory.getLogger(TaggingValueBuilder.class);

    Set<String> tagTypesToMark = null;

    boolean sensitiveTagTypes;

    private String valueSeparator;

    public static final String TAG_TYPES_PROPERTY = "tagTypes";

    public TaggingValueBuilder(Map<String, Object> actionProperties) {
        sensitiveTagTypes = Boolean.valueOf((String)actionProperties.getOrDefault("sensitiveTagTypes", "true"));
        String tagTypes = (String) actionProperties.get(TAG_TYPES_PROPERTY);
        valueSeparator = (String) actionProperties.getOrDefault("tagValueSeparator"," ");
        if (tagTypes != null) {
            tagTypesToMark = new HashSet<>();
            String[] split = StringUtils.split(tagTypes, ",");
            for (String tagType : split) {
                logger.debug("action will use tagType {}",tagType);
                tagTypesToMark.add(tagType);
            }
        }
    }

    public void addTagMarkings(String tags, StringBuilder markings) {
        try {
            FileTagMinimalDto[] fileTagMinimalDtos = ModelDtoConversionUtils.getMapper().readValue(tags, FileTagMinimalDto[].class);

            for (FileTagMinimalDto fileTagMinimalDto : fileTagMinimalDtos) {
                if (sensitiveTagTypes && fileTagMinimalDto.isSensitive()) {
                    appendMarking(markings, fileTagMinimalDto);
                }
                else if (tagTypesToMark != null && tagTypesToMark.contains(fileTagMinimalDto.getTypeName())) {
                    appendMarking(markings, fileTagMinimalDto);
                }
            }

        } catch (IOException e) {
            logger.error("Failed to deserialize FileTags from action properties ({})",tags,e);
        }

    }

    private void appendMarking(StringBuilder markings, FileTagMinimalDto fileTagMinimalDto) {
        String name = VelocityScriptUtils.getSanitizedValue(fileTagMinimalDto.getName());
        String typeName = VelocityScriptUtils.getSanitizedValue(fileTagMinimalDto.getTypeName());

        markings.append(valueSeparator).append(typeName).append(":").append(name);
    }

}
