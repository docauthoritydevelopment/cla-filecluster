package com.cla.filecluster.actions.executors;

import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.filecluster.actions.base.DocAuthorityActionClassExecutor;
import com.cla.filecluster.actions.base.VelocityScriptUtils;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.google.common.collect.Iterables;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by shtand on 15/02/2016.
 * Uses CACLS. TODO - support ICACLS as well
 */
public class AccessManagementScriptGenerator implements DocAuthorityActionClassExecutor {

    public static final String ADD = "add";
    public static final String REMOVE = "remove";   // TODO: ...
    public static final String SET = "set";
    public static final int ACTIONS_PER_FILE = 10000;
    private ActionHelper actionHelper;

    private ActionExecutionTaskStatusDto actionExecutionTaskStatusDto;

    private String velocityRuntimeLog = "./logs/velocity.log";

    private String defaultTemplateFileName = "./config/actions/accessManagementScriptGenerator.vm";
    private Logger logger = LoggerFactory.getLogger(AccessManagementScriptGenerator.class);

    private List<FileOperationRequest> fileOperationRequests;

    public static String generateScriptName(long actionTriggerId, File actionScriptFolder, int page) {
        return String.format("%s%s%s_%04d_%04d.bat",
                actionScriptFolder.getAbsolutePath(), File.separator, "access_management_script", actionTriggerId, page);
    }

    @Override
    public void init(ActionHelper actionHelper) {
        this.actionHelper = actionHelper;
    }


    @Override
    public void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        this.actionExecutionTaskStatusDto = actionExecutionTaskStatusDto;
        fileOperationRequests = new ArrayList<>();
    }

    public void addReadPermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "addReadPermission", "R", ADD);
    }

    public void addWritePermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "addWritePermission", "W", ADD);
    }

    public void addFullControlPermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "addFullControlPermission", "F", ADD);
    }

    public void setReadPermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "setReadPermission", "R", SET);
    }

    public void setWritePermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "setWritePermission", "W", SET);
    }

    public void setFullControlPermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "setFullControlPermission", "F", SET);
    }

    public void addPermission(BaseActionParams baseActionParams, Properties actionParameters) {
        handlePermission(baseActionParams, actionParameters, "addPermission", "W", ADD);
    }

    public void removePermission(BaseActionParams baseActionParams, Properties actionParameters) {
        // TODO: implement ...
        handlePermission(baseActionParams, actionParameters, "removePermission", "W", REMOVE);
    }

    private void handlePermission(BaseActionParams baseActionParams, Properties actionParameters, String actionId, String permission, String todo) {
        String path = baseActionParams.getFileUrl();
        List<String> users = actionHelper.extractParameterValues("users",actionParameters,actionId, path);
        for (String user : users) {
            FileOperationRequest filePermissionRequest = FileOperationRequest.createPermissionRequest(path, user, permission, todo);
            fileOperationRequests.add(filePermissionRequest);
        }
    }

    @Override
    public void apply() {
        logger.debug("Creating access management script");
        File reportsFolder = actionHelper.getActionClassReportsFolder(actionExecutionTaskStatusDto.getActionTriggerId(),ActionClass.ACCESS_MANAGEMENT);
        int page = actionExecutionTaskStatusDto.getPage();
        Iterable<List<FileOperationRequest>> partition = Iterables.partition(fileOperationRequests, ACTIONS_PER_FILE);

        for (List<FileOperationRequest> operationRequests : partition) {
            page++;
//            String scriptName = reportsFolder.getAbsolutePath() + File.separator + "access_management_script"+page+".bat";
            String scriptName = generateScriptName(actionExecutionTaskStatusDto.getActionTriggerId(), reportsFolder, page);
            try (FileWriter writer = new FileWriter(scriptName)) {

                final Template t = VelocityScriptUtils.createVelocityTemplate(defaultTemplateFileName, velocityRuntimeLog);
                final VelocityContext context = new VelocityContext();

                List<Map> permissionAddRequests = new ArrayList<>();
                List<Map> permissionSetRequests = new ArrayList<>();
                List<Map> permissionRemoveRequests = new ArrayList<>();
                for (FileOperationRequest filePermissionRequest : operationRequests) {
                    Map<String,String> request = new HashMap<>();
                    request.put("path",filePermissionRequest.getPath());
                    request.put("user",filePermissionRequest.getUser());
                    request.put("permission",filePermissionRequest.getPermission());
                    if (filePermissionRequest.getOperation().equals(SET)) {
                        permissionSetRequests.add(request);
                    } else if (filePermissionRequest.getOperation().equals(REMOVE)) {
                        permissionRemoveRequests.add(request);
                    } else {
                        permissionAddRequests.add(request);
                    }
                }

                context.put("permissionAddRequests", permissionAddRequests);
                context.put("permissionSetRequests", permissionSetRequests);
                context.put("permissionRemoveRequests", permissionRemoveRequests);
                VelocityScriptUtils.addGenericVelocityParameters(actionExecutionTaskStatusDto, page, context);
                t.merge(context, writer);
            }
            catch (IOException e) {
                logger.error("IO Exception while creating access management script "+e.getMessage(),e);
                throw new RuntimeException("IO Exception while creating access management script "+e.getMessage(),e);
            }
            logger.info("Finished creating script {}",scriptName);
        }
        actionExecutionTaskStatusDto.setPage(page);
        fileOperationRequests.clear();
    }

}
