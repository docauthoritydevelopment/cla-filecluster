package com.cla.filecluster.actions.executors;

import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.filecluster.actions.base.DocAuthorityActionClassExecutor;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by uri on 16/02/2016.
 */
public class ReportingCsvGenerator implements DocAuthorityActionClassExecutor {

    private List<ReportingCsvLine> csvLines;

    private String reportFileName;

    private ActionHelper actionHelper;

    private ActionExecutionTaskStatusDto actionExecutionTaskStatusDto;

    private File reportsFolder;

    @Override
    public void init(ActionHelper actionHelper) {
        this.actionHelper = actionHelper;
    }

    @Override
    public void apply() {
        reportFileName = "file_report.csv";
        File f = new File(reportsFolder,reportFileName);
        try {
            FileWriter fw = new FileWriter(f);
            if (csvLines != null) {
                for (ReportingCsvLine csvLine : csvLines) {
                    fw.write(csvLine.toString());
                    fw.write(System.lineSeparator());
                }
            }
            fw.flush();
            fw.close();
        } catch (IOException e) {
            throw new RuntimeException("IO Exception while writing CSV report file",e);
        }
        finally {
            csvLines = null;
        }
    }

    @Override
    public void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        this.actionExecutionTaskStatusDto = actionExecutionTaskStatusDto;
        reportsFolder = actionHelper.getActionClassReportsFolder(actionExecutionTaskStatusDto.getActionTriggerId(),ActionClass.REPORTING);
        csvLines = new ArrayList<>();
    }

    public void reportFile(BaseActionParams baseActionParams, Properties actionParameters) {
        ReportingCsvLine e = new ReportingCsvLine(baseActionParams);
        csvLines.add(e);
    }

    private class ReportingCsvLine {
        Long claFileId;
        String url;

        public ReportingCsvLine(BaseActionParams baseActionParams) {
            claFileId = baseActionParams.getClaFileId();
            url = baseActionParams.getFileUrl();
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(claFileId).append(",")
                    .append(url);
            return stringBuilder.toString();
        }
    }
}
