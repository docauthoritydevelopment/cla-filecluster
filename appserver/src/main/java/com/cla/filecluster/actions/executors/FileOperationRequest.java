package com.cla.filecluster.actions.executors;

import java.util.Properties;

/**
 * Created by uri on 09/03/2016.
 */
public class FileOperationRequest {

    String path;
    String user;
    String permission;

    String operation;

    String target;

    String backupFolder;

    private Properties actionParams;
    private String tags;
    private String property;
    private String value;

    public static FileOperationRequest createPermissionRequest(String path, String user, String permission, String operation) {
        FileOperationRequest fileOperationRequest = new FileOperationRequest();
        fileOperationRequest.setPath(path);
        fileOperationRequest.setPermission(permission);
        fileOperationRequest.setUser(user);
        fileOperationRequest.setOperation(operation);
        return fileOperationRequest;
    }

    public static FileOperationRequest createTaggingRequest(String path, String value, String property, String target, Properties actionParams, String operation) {
        FileOperationRequest fileOperationRequest = new FileOperationRequest();
        fileOperationRequest.setPath(path);
        fileOperationRequest.setValue(value);
        fileOperationRequest.setProperty(property);
        fileOperationRequest.setTarget(target);
        fileOperationRequest.setOperation(operation);
        fileOperationRequest.setActionParams(actionParams);
        return fileOperationRequest;
    }

    public static FileOperationRequest createSourceTargetOperation(String path, String target, Properties actionParams, String operation) {
        FileOperationRequest fileOperationRequest = new FileOperationRequest();
        fileOperationRequest.setPath(path);
        fileOperationRequest.setTarget(target);
        fileOperationRequest.setActionParams(actionParams);
        fileOperationRequest.setOperation(operation);
        return fileOperationRequest;
    }

    public static FileOperationRequest createBasicOperation(String path, Properties actionParams, String operation) {
        FileOperationRequest fileOperationRequest = new FileOperationRequest();
        fileOperationRequest.setPath(path);
        fileOperationRequest.setActionParams(actionParams);
        fileOperationRequest.setOperation(operation);
        return fileOperationRequest;
    }

    public Properties getActionParams() {
        return actionParams;
    }

    public void setActionParams(Properties actionParams) {
        this.actionParams = actionParams;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getProperty() {
        return property;
    }

    public String getBackupFolder() {
        return backupFolder;
    }

    public void setBackupFolder(String backupFolder) {
        this.backupFolder = backupFolder;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
