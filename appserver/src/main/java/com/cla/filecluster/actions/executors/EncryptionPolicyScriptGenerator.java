package com.cla.filecluster.actions.executors;

import com.cla.filecluster.actions.base.*;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.google.common.collect.Iterables;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by uri on 09/03/2016.
 */
public class EncryptionPolicyScriptGenerator implements DocAuthorityActionClassExecutor {

    private static final int ACTIONS_PER_FILE = 10000;
    private ActionHelper actionHelper;

    private ActionExecutionTaskStatusDto actionExecutionTaskStatusDto;

    private String velocityRuntimeLog = "./logs/velocity.log";

    private String defaultTemplateFileName = "./config/actions/encryptPolicyScriptGenerator.vm";
    private Logger logger = LoggerFactory.getLogger(EncryptionPolicyScriptGenerator.class);

    private List<FileOperationRequest> fileOperationRequests;

    @Override
    public void init(ActionHelper actionHelper) {
        this.actionHelper = actionHelper;
    }

    private static final String ENCRYPT_OPERATION = "encrypt";

    public static final String TARGET = "target";

    @Override
    public void apply() {
        logger.debug("Creating access management script");
        File actionScriptFolder = actionHelper.getActionClassScriptsFolder(actionExecutionTaskStatusDto, ActionClass.ACCESS_MANAGEMENT);
        int page = actionExecutionTaskStatusDto.getPage();
        Iterable<List<FileOperationRequest>> partition = Iterables.partition(fileOperationRequests, ACTIONS_PER_FILE);
        for (List<FileOperationRequest> operationRequests : partition) {
            page++;
//            String scriptName = actionScriptFolder.getAbsolutePath() + File.separator + "storage_policy_script"+page+".bat";
            String scriptName = String.format("%s%s%s_%04d_%04d.bat",
                    actionScriptFolder.getAbsolutePath(), File.separator, "storagePolicyScript", actionExecutionTaskStatusDto.getActionTriggerId(), page);
            try (FileWriter writer = new FileWriter(scriptName)) {
                final Template t = VelocityScriptUtils.createVelocityTemplate(defaultTemplateFileName, velocityRuntimeLog);

                final VelocityContext context = new VelocityContext();

                List<Map> encryptFileRequests = new ArrayList<>();
                for (FileOperationRequest fileOperationRequest : operationRequests) {
                    Map<String,String> request = new HashMap<>();
                    Properties actionParams = fileOperationRequest.getActionParams();
                    request.put("rootPath",(String)actionParams.get(DocAuthorityAction.ROOT_PATH));
                    request.put("relPath",(String)actionParams.get(DocAuthorityAction.RELATIVE_PATH));
                    request.put("basename",(String)actionParams.get(DocAuthorityAction.BASE_NAME));
                    request.put("isContainer",Boolean.valueOf(actionParams.get(DocAuthorityAction.IS_CONTAINER) != null).toString());
//                    String target = fileOperationRequest.getTarget();
//                    request.put(TARGET, target);
                    if (fileOperationRequest.getOperation().equals(ENCRYPT_OPERATION)) {
                        encryptFileRequests.add(request);
                    }
                }
                context.put("encryptFileRequests", encryptFileRequests);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d HH:mm");
                context.put("nowStr", sdf.format(new Date()));
//                Set<String> relativeFolders = operationRequests.stream().map(r -> (String)r.getActionParams().get(DocAuthorityAction.RELATIVE_PATH)).collect(Collectors.toSet());
//                context.put("relFolders",Lists.newArrayList(relativeFolders));
                VelocityScriptUtils.addGenericVelocityParameters(actionExecutionTaskStatusDto,page,context);
                t.merge(context, writer);
            }
            catch (IOException e) {
                logger.error("IO Exception while creating script {}",e.getMessage(),e);
                throw new RuntimeException("IO Exception while creating script "+e.getMessage(),e);
            }
            logger.info("Finished creating script {}",scriptName);
        }
        actionExecutionTaskStatusDto.setPage(page);
        fileOperationRequests.clear();
    }

    @Override
    public void prepare(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        this.actionExecutionTaskStatusDto = actionExecutionTaskStatusDto;
        fileOperationRequests = new ArrayList<>();
    }

    public void encryptFile(BaseActionParams baseActionParams, Properties actionParameters) {
        String path = baseActionParams.getFileUrl();
        String target = actionHelper.extractSingleParameter("target",actionParameters,"encryptFile", path);
        FileOperationRequest fileOperationRequest = FileOperationRequest.createSourceTargetOperation(path,target,actionParameters,ENCRYPT_OPERATION);
        fileOperationRequests.add(fileOperationRequest);
    }
}
