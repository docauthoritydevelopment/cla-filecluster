package com.cla.filecluster.actions.base;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * Created by uri on 17/02/2016.
 */
public interface ActionHelper {

    public static String PARAMETER_1 = "param1";
    public static String PARAMETER_2 = "param2";

    File getActionClassReportsFolder(long actionTriggerId, ActionClass actionClass);

    File getActionClassScriptsFolder(ActionExecutionTaskStatusDto actionTrigger, ActionClass actionClass);

    Stream<Path> getAllActionClassFolders(long actionTriggerId);

    String extractSingleParameter(String parameterName, Properties actionParameters, String actionId, String path);

    String extractSingleParameterIfExists(String parameterName, Properties actionParameters, String actionId, String path);

    List<String> extractParameterValues(String parameterName, Properties actionParameters, String actionId, String path);

    String getBackupFolder();

    Map<String, Object> getActionExecutorProperties(String actionExecutorName);
}
