package com.cla.filecluster.actions.base;

/**
 * Created by uri on 16/02/2016.
 */
public class BaseActionParams {

    private Long claFileId;

    private String fileUrl;

    private Long actionTriggerId;

    private String actionId;

    public BaseActionParams() {
    }

    public BaseActionParams(Long claFileId, String fileUrl, Long actionTriggerId, String actionId) {
        this.claFileId = claFileId;
        this.fileUrl = fileUrl;
        this.actionTriggerId = actionTriggerId;
        this.actionId = actionId;
    }

    public Long getClaFileId() {
        return claFileId;
    }

    public void setClaFileId(Long claFileId) {
        this.claFileId = claFileId;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getActionTriggerId() {
        return actionTriggerId;
    }

    public void setActionTriggerId(Long actionTriggerId) {
        this.actionTriggerId = actionTriggerId;
    }

}
