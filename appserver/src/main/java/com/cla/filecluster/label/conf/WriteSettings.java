package com.cla.filecluster.label.conf;

import java.util.List;

/**
 * Created by: yael
 * Created on: 7/11/2019
 */
public class WriteSettings {
    private List<LabelMappings> labelMappings;

    public WriteSettings() {
    }

    public List<LabelMappings> getLabelMappings() {
        return labelMappings;
    }

    public void setLabelMappings(List<LabelMappings> labelMappings) {
        this.labelMappings = labelMappings;
    }
}
