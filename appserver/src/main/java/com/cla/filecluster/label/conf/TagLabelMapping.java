package com.cla.filecluster.label.conf;

/**
 * Created by: yael
 * Created on: 7/17/2019
 */
public class TagLabelMapping {

    private String tagType;
    private String tagName;
    private String daLabel;

    public TagLabelMapping() {
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getDaLabel() {
        return daLabel;
    }

    public void setDaLabel(String daLabel) {
        this.daLabel = daLabel;
    }
}
