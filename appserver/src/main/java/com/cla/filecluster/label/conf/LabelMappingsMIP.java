package com.cla.filecluster.label.conf;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
public class LabelMappingsMIP implements LabelMappings {
    private String label;
    private String daLabel;

    public LabelMappingsMIP() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDaLabel() {
        return daLabel;
    }

    public void setDaLabel(String daLabel) {
        this.daLabel = daLabel;
    }
}
