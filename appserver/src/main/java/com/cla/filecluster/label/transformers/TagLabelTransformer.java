package com.cla.filecluster.label.transformers;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 7/17/2019
 */
public class TagLabelTransformer {

    private Map<Long, Long> tagIdToDaLabelId;

    public Map<Long, Long> getTagIdToDaLabelId() {
        return tagIdToDaLabelId;
    }

    public void setTagIdToDaLabelId(Map<Long, Long> tagIdToDaLabelId) {
        this.tagIdToDaLabelId = tagIdToDaLabelId;
    }
}
