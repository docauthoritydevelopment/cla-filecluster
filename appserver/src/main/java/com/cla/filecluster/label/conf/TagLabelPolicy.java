package com.cla.filecluster.label.conf;

import java.util.List;

/**
 * Created by: yael
 * Created on: 7/17/2019
 */
public class TagLabelPolicy {

    private List<TagLabelMapping> tagLabelMappings;

    public TagLabelPolicy() {
    }

    public List<TagLabelMapping> getTagLabelMappings() {
        return tagLabelMappings;
    }

    public void setTagLabelMappings(List<TagLabelMapping> tagLabelMappings) {
        this.tagLabelMappings = tagLabelMappings;
    }
}
