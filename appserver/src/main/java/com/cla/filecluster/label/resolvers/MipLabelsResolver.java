package com.cla.filecluster.label.resolvers;

import com.cla.connector.domain.dto.file.label.LabelData;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.connector.domain.dto.file.label.NativeLabel;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 7/8/2019
 */
public class MipLabelsResolver {
    private final static Pattern mipLabelPattern = Pattern.compile("(MSIP_Label_)([^_]*)_(\\w*)");
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ");

    /**
     see {@see https://docs.microsoft.com/en-us/information-protection/develop/concept-mip-metadata}
     key structure: DefinedPrefix_ElementType_GlobalIdentifier_AttributeName.
     I.e. MSIP_Label_f048e7b8-f3aa-4857-bf32-a317f4bc3f29_GeneratedBy = HRReportingSystem
     * **/
    public static List<LabelData> resolveMipLabels(Map<String, String> metadata) {
        Map<String,Map<String,String>> mipProperties = new HashMap<>();//a map grouped by GUID
        //Map<String,Map<String,String>> rawLabels = new HashMap<>();//a map grouped by GUID
        Table<String,String,String> rawLabels = HashBasedTable.create();
        for (Map.Entry<String, String> entry : metadata.entrySet()) {
            Matcher matcher = mipLabelPattern.matcher(entry.getKey());
            if(matcher.matches()){//extracting the guid and the property name
                String guid = matcher.group(2);
                String propertyName = matcher.group(3);
                mipProperties.putIfAbsent(guid, new HashMap<>());
                Map<String, String> currentDaLabelData = mipProperties.get(guid);
                currentDaLabelData.put(propertyName, entry.getValue());
                //Map<String, String> currentRawLabelData = mipProperties.putIfAbsent(guid, new HashMap<>());
                currentDaLabelData.put(entry.getKey(), entry.getValue());
            }
        }
        //resolving DA label
        List<NativeLabel> labels = Lists.newArrayList();
        mipProperties.forEach((key, labelMap) -> {
            String setDateString = labelMap.get("SetDate");
            LocalDateTime setDate = mipDateToDate(setDateString);
            String labelName = labelMap.get("Name");
            labels.add(new NativeLabel(key, labelName, setDate, LabelingVendor.MicrosoftInformationProtection));
        });

        //return labels;
        return labels.stream()
                .map(nativeLabel -> new LabelData(rawLabels.row(nativeLabel.getNativeId()), nativeLabel))
                .collect(Collectors.toList());
    }

    /**
     * The mip date is in format of mm:dd:yyyyThh:mm:ss+zzzz, however the standard zone should be zz:zz,
     * I am adding the missing semicolon to standardize the format before converting to date.
     * */
    private static LocalDateTime mipDateToDate(String setDateString) {
        if (Strings.isNullOrEmpty(setDateString)) {
            return null;
        }
        try {
            String toParseDate = setDateString.contains(":") ?
                    setDateString
                    : new StringBuilder(setDateString).insert(setDateString.length() - 2, ":").toString();
            ZonedDateTime z = ZonedDateTime.parse(toParseDate);
            return z.toLocalDateTime();
        } catch (Exception e) {
            // do nothing
        }

        // if we get format mm:dd:yyyyThh:mm:ssZ
        try {
            return LocalDateTime.parse(setDateString, formatter);
        } catch (Exception er) {
            return null;
        }
    }
}
