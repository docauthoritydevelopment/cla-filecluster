package com.cla.filecluster.label;

import com.cla.connector.domain.dto.file.label.LabelData;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface LabelsResolver extends Function<Map<String,String>, List<LabelData>> {
}
