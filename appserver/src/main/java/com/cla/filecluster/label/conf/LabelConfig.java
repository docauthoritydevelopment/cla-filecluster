package com.cla.filecluster.label.conf;

import java.util.List;

/**
 * Created by: yael
 * Created on: 7/9/2019
 */
public class LabelConfig {

    private List<Transformation> transformations;

    public List<Transformation> getTransformations() {
        return transformations;
    }

    public void setTransformations(List<Transformation> transformations) {
        this.transformations = transformations;
    }
}
