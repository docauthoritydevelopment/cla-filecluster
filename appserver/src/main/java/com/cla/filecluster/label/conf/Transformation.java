package com.cla.filecluster.label.conf;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
public class Transformation {
    private String labelingVendor;
    private ReadSettings readSettings;
    private WriteSettings writeSettings;

    public Transformation() {
    }

    public String getLabelingVendor() {
        return labelingVendor;
    }

    public void setLabelingVendor(String labelingVendor) {
        this.labelingVendor = labelingVendor;
    }

    public ReadSettings getReadSettings() {
        return readSettings;
    }

    public void setReadSettings(ReadSettings readSettings) {
        this.readSettings = readSettings;
    }

    public WriteSettings getWriteSettings() {
        return writeSettings;
    }

    public void setWriteSettings(WriteSettings writeSettings) {
        this.writeSettings = writeSettings;
    }
}
