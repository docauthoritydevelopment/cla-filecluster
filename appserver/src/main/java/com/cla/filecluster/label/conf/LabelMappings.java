package com.cla.filecluster.label.conf;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@type")
public interface LabelMappings {
}
