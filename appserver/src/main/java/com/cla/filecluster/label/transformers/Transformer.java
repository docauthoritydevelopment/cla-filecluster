package com.cla.filecluster.label.transformers;

import com.cla.connector.domain.dto.file.label.LabelData;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.filecluster.label.conf.Transformation;

import java.util.Set;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
public interface Transformer {
    String transformRead(LabelData data);
    String transformWrite(LabelDto data);
    LabelingVendor getVendor();
    Set<String> init(Transformation transformation);
}
