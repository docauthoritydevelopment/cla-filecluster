package com.cla.filecluster.label.transformers;

import com.cla.connector.domain.dto.file.label.LabelData;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.filecluster.label.conf.LabelMappingsMIP;
import com.cla.filecluster.label.conf.Transformation;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
@Service
public class TransformerMIP implements Transformer {

    private Map<String, String> transformationReadData;
    private Map<String, String> transformationWriteData;

    public LabelingVendor getVendor() {
        return LabelingVendor.MicrosoftInformationProtection;
    }

    public Set<String> init(Transformation transformation) {
        Set<String> daLabels = new HashSet<>();

        transformationReadData = new HashMap<>();
        if (transformation.getReadSettings() != null && transformation.getReadSettings().getLabelMappings() != null) {
            transformation.getReadSettings().getLabelMappings().forEach(t -> {
                if (t instanceof LabelMappingsMIP) {
                    LabelMappingsMIP data = (LabelMappingsMIP) t;
                    transformationReadData.put(data.getLabel(), data.getDaLabel());
                    daLabels.add(data.getDaLabel());
                }
            });
        }

        transformationWriteData = new HashMap<>();
        if (transformation.getWriteSettings() != null && transformation.getWriteSettings().getLabelMappings() != null) {
            transformation.getWriteSettings().getLabelMappings().forEach(t -> {
                if (t instanceof LabelMappingsMIP) {
                    LabelMappingsMIP data = (LabelMappingsMIP) t;
                    transformationWriteData.put(data.getDaLabel(), data.getLabel());
                    daLabels.add(data.getDaLabel());
                }
            });
        }
        return daLabels;
    }

    @Override
    public String transformRead(LabelData data) {
        if (transformationReadData == null) {
            return null;
        }
        return transformationReadData.get(data.getNativeLabel().getName());
    }

    @Override
    public String transformWrite(LabelDto data) {
        if (transformationWriteData == null) {
            return null;
        }
        return transformationWriteData.get(data.getName());
    }
}
