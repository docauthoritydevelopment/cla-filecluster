package com.cla.filecluster.label.conf;

import java.util.List;

/**
 * Created by: yael
 * Created on: 7/10/2019
 */
public class ReadSettings {
    private List<LabelMappings> labelMappings;

    public ReadSettings() {
    }

    public List<LabelMappings> getLabelMappings() {
        return labelMappings;
    }

    public void setLabelMappings(List<LabelMappings> labelMappings) {
        this.labelMappings = labelMappings;
    }
}
