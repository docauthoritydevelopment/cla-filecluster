package com.cla.filecluster.metadata;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.metadata.conf.MetadataConfig;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.label.LabelingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * translates raw metadata from file to properties using regex according to file config
 */
@Service
public class MetadataTranslationService {

    private final static Logger logger = LoggerFactory.getLogger(MetadataTranslationService.class);

    @Value("${metadata.transformer.file:}")
    private String metadataTransformerFilePath;

    @Value("${metadata.separator:}")
    private String metadataSeparator;

    private MetadataConfig metadataConfig;

    @SuppressWarnings("unused")
    @PostConstruct
    void init() {
        loadFromFile();
    }

    private void loadFromFile() {
        if (metadataTransformerFilePath == null || metadataTransformerFilePath.isEmpty()) {
            logger.info("No metadata transformer file defined - ignored");
        } else {
            logger.info("Load metadata transformer from file {}", metadataTransformerFilePath);
            loadFromFile(metadataTransformerFilePath);
        }
    }

    private void loadFromFile(String metadataTransformerFilePath) {
        try {
            File configFile = new File(metadataTransformerFilePath);
            metadataConfig = ModelDtoConversionUtils.getMapper().readValue(configFile, MetadataConfig.class);
        } catch (Exception e) {
            logger.error("failed loading metadata transformer from file", e);
        }
    }

    public Map<String, String> translateMetadata(Map<String, String> rawMetadata) {
        return MetadataTranslator.translateMetadata(metadataConfig, rawMetadata);
    }

    public List<String> mapToListWithSeparator(Map<String, String> metadata) {
        List<String> result = new ArrayList<>();

        if (metadata != null) {
            metadata.forEach((type, value) ->
                result.add(type + metadataSeparator + value)
            );
        }

        return result;
    }

    public void setMetadata(FileCatBuilder builder, ClaFilePropertiesDto props) {
        calculateMetadataValues(props).ifPresent(result -> {
            builder.setRawMetadata(result.rawMetadata);
            builder.setGeneralMetadata(result.generalMetadata);
            builder.setActionExpected(result.actionsExpected);
            builder.setDaMetadataType(result.daMetadataType);
            builder.setDaMetadata(result.daMetadata);
        });
    }

    public void setMetadata(SolrFileEntity file, ClaFilePropertiesDto props) {
        calculateMetadataValues(props).ifPresent(result -> {
            file.setRawMetadata(result.rawMetadata);
            file.setGeneralMetadata(result.generalMetadata);
            file.setActionsExpected(result.actionsExpected);
            file.setDaMetadataType(result.daMetadataType);
            file.setDaMetadata(result.daMetadata);
        });
    }

    private Optional<MetadataValues> calculateMetadataValues(ClaFilePropertiesDto props) {
        MetadataValues result = null;
        Map<String, String> metadata = props == null ? null : props.getMetadata();
        if (metadata != null && !metadata.isEmpty()) {
            try {
                result = new MetadataValues();
                Map<String, String> metadataSeparated = LabelingService.getNonLabelData(metadata);
                result.setRawMetadata(LabelingService.metadataToJson(metadata));
                result.setGeneralMetadata(mapToListWithSeparator(metadataSeparated));
                if (props.getLabelData() != null) {
                    List<String> actionExpected = LabelingService.createActionExpected(props.getLabelData());
                    result.setActionsExpected(actionExpected);
                }
                Map<String, String> daMetadata = translateMetadata(metadata);
                if (!daMetadata.isEmpty()) {
                    result.setDaMetadataType(new ArrayList<>(daMetadata.keySet()));
                    result.setDaMetadata(mapToListWithSeparator(daMetadata));
                }
            } catch (Exception e) {
                logger.error("problem setting metadata for file {}", props.getClaFileId(), e);
            }
        }
        return Optional.ofNullable(result);
    }

    private class MetadataValues {
        public List<String> getGeneralMetadata() {
            return generalMetadata;
        }

        void setGeneralMetadata(List<String> generalMetadata) {
            this.generalMetadata = generalMetadata;
        }

        public String getRawMetadata() {
            return rawMetadata;
        }

        void setRawMetadata(String rawMetadata) {
            this.rawMetadata = rawMetadata;
        }

        public List<String> getActionsExpected() {
            return actionsExpected;
        }

        void setActionsExpected(List<String> actionsExpected) {
            this.actionsExpected = actionsExpected;
        }

        public List<String> getDaMetadata() {
            return daMetadata;
        }

        void setDaMetadata(List<String> daMetadata) {
            this.daMetadata = daMetadata;
        }

        public List<String> getDaMetadataType() {
            return daMetadataType;
        }

        void setDaMetadataType(List<String> daMetadataType) {
            this.daMetadataType = daMetadataType;
        }

        private List<String> generalMetadata = null;
        private String rawMetadata = null;
        private List<String> actionsExpected = null;
        private List<String> daMetadata = null;
        private List<String> daMetadataType = null;
    }

    void setMetadataSeparator(String metadataSeparator) {
        this.metadataSeparator = metadataSeparator;
    }

    void setMetadataTransformerFilePath(String metadataTransformerFilePath) {
        this.metadataTransformerFilePath = metadataTransformerFilePath;
    }
}
