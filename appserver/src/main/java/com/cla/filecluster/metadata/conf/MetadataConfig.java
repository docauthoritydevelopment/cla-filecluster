package com.cla.filecluster.metadata.conf;

import java.util.List;

/**
 * class used to read metadata config from json file
 */
public class MetadataConfig {

    // separator to be used in code
    // we create input string of type + separator + value
    // and run the regex of the matchers of both type and value on the result
    private String typeValueSeparator;

    // different matchers to translate
    private List<MetadataMatcher> metadataMatchers;

    public MetadataConfig() {
    }

    public String getTypeValueSeparator() {
        return typeValueSeparator;
    }

    public void setTypeValueSeparator(String typeValueSeparator) {
        this.typeValueSeparator = typeValueSeparator;
    }

    public List<MetadataMatcher> getMetadataMatchers() {
        return metadataMatchers;
    }

    public void setMetadataMatchers(List<MetadataMatcher> metadataMatchers) {
        this.metadataMatchers = metadataMatchers;
    }

    public boolean hasConfig() {
        return metadataMatchers != null && !metadataMatchers.isEmpty();
    }
}
