package com.cla.filecluster.metadata;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.metadata.conf.MetadataConfig;
import com.cla.filecluster.metadata.conf.MetadataMatcher;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetadataTranslator {

    // translate all raw metadata by config and return translated values if relevant
    static Map<String, String> translateMetadata(MetadataConfig metadataConfig, Map<String, String> rawMetadata) {
        Map<String, String> result = new HashMap<>();

        if (metadataConfig == null || !metadataConfig.hasConfig() || rawMetadata == null || rawMetadata.isEmpty()) {
            return result;
        }

        for (Map.Entry<String, String> entry : rawMetadata.entrySet()) {
            for (MetadataMatcher matcher : metadataConfig.getMetadataMatchers()) {
                Pair<String, String> translatedValue = translate(matcher, entry.getKey(), entry.getValue(), metadataConfig.getTypeValueSeparator());
                if (translatedValue != null) {
                    result.put(translatedValue.getKey(), translatedValue.getValue());
                    break;
                }
            }
        }
        return result;
    }

    // try translating a value with a specific matcher, return null if no match for either type or value
    static Pair<String, String> translate(MetadataMatcher matcher, String type, String value, String separator) {
        Pattern typePattern = Pattern.compile(matcher.getTypeMatcher());
        String input = type + separator + value;
        Matcher typeMatcher = typePattern.matcher(input);
        String outputType;
        if (typeMatcher.find()) {
            outputType = typeMatcher.replaceFirst(matcher.getTypeReplacement());
        } else {
            return null;
        }

        String outputVal;
        Pattern valPattern = Pattern.compile(matcher.getValueMatcher());
        Matcher valMatcher = valPattern.matcher(input);
        if (valMatcher.find()) {
            outputVal = valMatcher.replaceFirst(matcher.getValueReplacement());
        } else {
            return null;
        }

        return Pair.of(outputType, outputVal);
    }
}
