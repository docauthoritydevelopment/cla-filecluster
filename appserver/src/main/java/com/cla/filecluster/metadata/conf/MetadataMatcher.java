package com.cla.filecluster.metadata.conf;

/**
 * class used to read metadata config from json file
 */
public class MetadataMatcher {

    // we dont use these, for customer convenience
    // unique id might be used in the future if moved to db table
    private String uniqueId;
    private String name;

    // regex to find relevant type (also mark groups for regex if needed)
    private String typeMatcher;

    // regex to find relevant value (also mark groups for regex if needed)
    private String valueMatcher;

    // replacement string that will be used on the find result, including groups
    private String typeReplacement;
    private String valueReplacement;

    public MetadataMatcher() {
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeMatcher() {
        return typeMatcher;
    }

    public void setTypeMatcher(String typeMatcher) {
        this.typeMatcher = typeMatcher;
    }

    public String getValueMatcher() {
        return valueMatcher;
    }

    public void setValueMatcher(String valueMatcher) {
        this.valueMatcher = valueMatcher;
    }

    public String getTypeReplacement() {
        return typeReplacement;
    }

    public void setTypeReplacement(String typeReplacement) {
        this.typeReplacement = typeReplacement;
    }

    public String getValueReplacement() {
        return valueReplacement;
    }

    public void setValueReplacement(String valueReplacement) {
        this.valueReplacement = valueReplacement;
    }
}
