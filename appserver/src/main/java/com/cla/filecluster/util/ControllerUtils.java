package com.cla.filecluster.util;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.security.Authority;
import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.domain.dto.security.UserType;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.security.UserService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by: yael
 * Created on: 12/11/2017
 */
@Service
public class ControllerUtils {

    private Logger logger = LoggerFactory.getLogger(ControllerUtils.class);

    public static int FACET_MAX_PAGE_SIZE_FILECOUNT = 10000;    // Was 10M

    @Autowired
    private UserService userService;

    @Value("${facet.max-size.file-count:10000}")
    private int facetMaxPageSizeFilecountParam;

    @Value("${report.api.time-out-sec:30}")
    private int reportTimeOutSec;

    private Map<String, Object> queryCache = new HashMap<>();

    private final ExecutorService pool = Executors.newCachedThreadPool();

    @PostConstruct
    private void init(){
        FACET_MAX_PAGE_SIZE_FILECOUNT = facetMaxPageSizeFilecountParam;
        logger.debug("Setting FACET_MAX_PAGE_SIZE_FILECOUNT = {}", FACET_MAX_PAGE_SIZE_FILECOUNT);
    }

    @Scheduled(initialDelayString = "${controller-cache.invalidate.initial.ms:300000}", fixedRateString = "${controller-cache.invalidate.cycle.ms:300000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public synchronized void invalidateCache() {
        queryCache = new HashMap<>();
    }

    public synchronized void putInCache(String key, Object value) {
        queryCache.put(key, value);
    }

    public Object getFromCache(String key) {
        return queryCache.get(key);
    }

    public synchronized void removeFromCache(String key) {
        try {
            queryCache.remove(key);
        } catch (Exception e) {
            logger.warn("problem accessing cache for removal of key {} {}", key, e.getMessage());
        }
    }

    public static List<RunStatus> getStatesList(Map<String, String> params) {
        if (params != null && !Strings.isNullOrEmpty(params.get("states"))) {
            String[] split = params.get("states").split(",");
            List<RunStatus> filterByRunStatus = Arrays.stream(split)
                    .map(RunStatus::valueOf)
                    .collect(Collectors.toList());
            return filterByRunStatus;
        }
        return null;
    }

    public static String getParamByName(Map<String, String> params, String name) {
        if (params != null && !Strings.isNullOrEmpty(params.get(name))) {
            return params.get(name);
        }
        return "";
    }

    public static int roundUpDivision(long num, long divisor) {
        return (int) ((num + divisor - 1) / divisor);
    }

    // for DAMAIN-3898 we cant use @AutoAuthenticate on a method called from ui
    // as it runs over the user in the session and ruins other requests
    // so we opened a separate thread
    // we should consider changing the way @AutoAuthenticate works and not use
    // the spring context
    public <R> R runInOtherThreadAsAdmin(Callable<R> runnable) {
        Future<R> f = pool.submit(runnable);
        try {
            return f.get(reportTimeOutSec, TimeUnit.SECONDS);
        } catch (Exception ex) {
            logger.error("error running task", ex);
            throw new RuntimeException("error running task", ex);
        }
    }

    public void runInOtherThreadAsAdmin(Runnable runnable) {
        try {
            pool.submit(runnable);
        } catch (Exception ex) {
            logger.error("error running task", ex);
            throw new RuntimeException("error running task", ex);
        }
    }

    public boolean isAdmin() {
        UserDto user = userService.getCurrentUser();

        if (user == null) return false;
        if (user.getUserType().equals(UserType.SYSTEM)) return true;

        Set<SystemRoleDto> systemRoleDtos = user.getBizRoleDtos().stream()
                .flatMap(br -> br.getTemplate().getSystemRoleDtos().stream())
                .collect(Collectors.toSet());

        return systemRoleDtos.stream()
                .anyMatch(role ->
                        // general admin can see all the files
                        Authority.GeneralAdmin.nameEq(role.getName()) ||
                                // user with global ViewFiles permission can see all files
                                Authority.ViewFiles.nameEq(role.getName())
                );
    }

    /*
    This function gets the entire content and returns only the page requested by ui
    if selected index is send we search for its page
    otherwise we use the page number requested
     */
    public static <T> FacetPage<AggregationCountItemDTO<T>> getRelevantResultPage(List<AggregationCountItemDTO<T>> content,
                                                                                  PageRequest pageRequest, String selectItemId,
                                                                                  Function<T, String> idExtractor) {
        return getRelevantResultPage(content, pageRequest, selectItemId, idExtractor, null, null);
    }

    public static <T> FacetPage<AggregationCountItemDTO<T>> getRelevantResultPage(List<AggregationCountItemDTO<T>> content,
                                                                                  PageRequest pageRequest, String selectItemId,
                                                                                  Function<T, String> idExtractor,
                                                                                  Long pageAggregatedCount,
                                                                                  Long totalAggregatedCount) {
        Integer totalElements = content.size();
        if (selectItemId != null) { // use selected item id
            OptionalInt indexOpt = IntStream.range(0, content.size())
                    .filter(i -> selectItemId.equals(idExtractor.apply(content.get(i).getItem())))
                    .findFirst();

            if (indexOpt.isPresent()) {
                //calculate page - to find fromIndex and toIndex
                pageRequest = PageRequest.of(indexOpt.getAsInt() / pageRequest.getPageSize(),
                        pageRequest.getPageSize());
            }
        }

        int toIndex = Math.min(content.size(), (int) pageRequest.getOffset() + pageRequest.getPageSize());
        if (pageRequest.getOffset() > toIndex) {
            return new FacetPage<>(Lists.newArrayList(), pageRequest);
        }

        List<AggregationCountItemDTO<T>> resultList =
                Lists.newArrayList(content.subList((int) pageRequest.getOffset(), toIndex));

        FacetPage<AggregationCountItemDTO<T>> facetPage = new FacetPage<>(resultList, pageRequest);
        if (totalAggregatedCount != null && pageAggregatedCount != null) {
            facetPage.setPageAggregatedCount(pageAggregatedCount);
            facetPage.setTotalAggregatedCount(totalAggregatedCount);
        }
        facetPage.setTotalElements(totalElements);
        return facetPage;
    }

    @PreDestroy
    public void destroy() {
        pool.shutdown();
    }
}
