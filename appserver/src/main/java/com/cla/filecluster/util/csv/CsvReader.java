package com.cla.filecluster.util.csv;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.cla.common.domain.dto.RowDTO;
import org.apache.commons.io.input.CharSequenceReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.util.CsvContext;

import javax.validation.constraints.NotNull;

@Component
public class CsvReader {	
	private static final Logger logger = LoggerFactory.getLogger(CsvReader.class);

	private class ErrorTracker {
		public boolean isPreviousLineFailure;
		public int consequtiveFailures;
		public int failureCounter;
		public int goodCounter;
	}

	@Value("${consequtiveFailuresLimit:10}")
	private int consequtiveFailuresLimit;
	
	@Value("${numOfFailuresToLog:0}")
	private int numOfFailuresToLog;

    public int readAndProcess(String fileName, final Consumer<RowDTO> rowProcessor,
                              final Predicate<RowDTO> rowPredicate, Map<String,CellProcessor> cellValidationProcessorsMap) throws Exception {
        return readAndProcess(new FileReader(fileName),rowProcessor,rowPredicate,cellValidationProcessorsMap);
    }

	public int readAndProcess(byte[] rawData, final Consumer<RowDTO> rowProcessor,
							  final Predicate<RowDTO> rowPredicate, Map<String,CellProcessor> cellValidationProcessorsMap) throws Exception {
		return readAndProcess(new CharSequenceReader(new String(rawData,"UTF-8")),rowProcessor,rowPredicate,cellValidationProcessorsMap);
	}

	public int readAndProcess(Reader reader, final Consumer<RowDTO> rowProcessor,
                              final Predicate<RowDTO> rowPredicate, Map<String,CellProcessor> cellValidationProcessorsMap) throws Exception {
		if (cellValidationProcessorsMap == null)
			cellValidationProcessorsMap = new HashMap<>();
		final ErrorTracker errorTracker = new ErrorTracker();
		
		try(ICsvListReader listReader = new CsvListReader(reader, CsvPreference.STANDARD_PREFERENCE)) {
			final String[] headers = listReader.getHeader(true);
			List<Object> row;
			final CellProcessor[] cellValidationProcessors = createCellProcessors(cellValidationProcessorsMap, headers);
			
			while ((row = readSafety(listReader, cellValidationProcessors, headers, errorTracker)) != null) {
				final RowDTO rowDTO = new RowDTO(row.toArray(new Object[]{}), listReader.getLineNumber());
				if(rowPredicate == null || rowPredicate.test(rowDTO))
					rowProcessor.accept(rowDTO);
			}
		}
		return errorTracker.failureCounter;
	}


	private CellProcessor[] createCellProcessors(final Map<String, CellProcessor> cellValidationProcessorsMap, final String[] headers) {
//		final Set<String> keySet = new HashSet<String>(cellValidationProcessorsMap.keySet();
//		keySet.removeAll(Arrays.asList(headers));	
		final Set<String> headersSet = Stream.of(headers).collect(toSet());
		final List<String> undefinedHeaders = cellValidationProcessorsMap.keySet().stream().filter(s->!(headersSet.contains(s))).collect(toList());		
		if(!undefinedHeaders.isEmpty()) {
			System.err.println(String.format("Error: input file is missing one of the following headers: %s", cellValidationProcessorsMap.keySet().toString()));
			throw new RuntimeException("The following defined validation processors do not exist in headers of the the input file: {}"+undefinedHeaders);
		}
		final List<CellProcessor> cellProcessors = new ArrayList<>(headers.length);
		for (final String header : headers) {
			final CellProcessor cellProcessor = cellValidationProcessorsMap.get(header);
			cellProcessors.add(cellProcessor==null?new Optional():cellProcessor);
		}
		return cellProcessors.toArray(new CellProcessor[]{});
	}


	private List<Object> readSafety(final ICsvListReader listReader, final CellProcessor[] processors, 
			final String[] headers, final ErrorTracker errorTracker) throws IOException {
		List<Object> line = null;
		boolean readLineOK = false;
		do {
			try {
				line = listReader.read(processors);
				readLineOK = true;
				errorTracker.isPreviousLineFailure = false;
				errorTracker.goodCounter++;
			} catch (SuperCsvCellProcessorException e) {
				handleViolation(e, headers,errorTracker);
			}
		} while (!readLineOK);
		return line;
	}

	private void handleViolation(final SuperCsvCellProcessorException e, final String[] headers, final ErrorTracker errorTracker) {
		final CsvContext ctx = e.getCsvContext();
		final int line = ctx.getLineNumber();
		final int fldInx = ctx.getColumnNumber();
		if (errorTracker.failureCounter < numOfFailuresToLog) {
			logger.warn("Validation failure. Problem: {}. Validation failure. Problem: {}", e.getLocalizedMessage(), e.getCsvContext());
		}
		else {
			logger.warn("Warning: illegal value '{}' in line {}, field number {} '{}'. Line ignored. {}",ctx.getRowSource().get(fldInx-1),line,fldInx,headers[fldInx-1],e.getLocalizedMessage());
		}
		if (ctx.getRowSource().get(fldInx-1)==null) {
			System.out.println(String.format("Warning: empty value in line %d, field number %d '%s'. Line ignored.", 
					line,fldInx,headers[fldInx-1]));
		}
		else if (e.getProcessor() instanceof org.supercsv.cellprocessor.constraint.Unique || 
				e.getProcessor() instanceof org.supercsv.cellprocessor.constraint.UniqueHashCode) {
			System.out.println(String.format("Warning: duplicate value was found '%s' in line %d, for field number %d '%s'. Line ignored.", 
					ctx.getRowSource().get(fldInx-1),line,fldInx,headers[fldInx-1]));
		}
		else {
			System.out.println(String.format("Warning: illegal value '%s' in line %d, field number %d '%s'. Line ignored.", 
					ctx.getRowSource().get(fldInx-1),line,fldInx,headers[fldInx-1]));
		}
		errorTracker.failureCounter++;
		if (errorTracker.isPreviousLineFailure) {
			errorTracker.consequtiveFailures++;
			if (errorTracker.consequtiveFailures > consequtiveFailuresLimit) {
				throw new RuntimeException("Too many consequtive read failures.");
			}
		}
		errorTracker.isPreviousLineFailure = true;
	}
	

	public String[] getHeaders(@NotNull  final String fileName) throws Exception {
		if (fileName == null) {
            throw new RuntimeException("No fileName given to getHeaders.");
        }
        try(ICsvListReader listReader = new CsvListReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE)) {
			return listReader.getHeader(true); 
		} 
	}

    public String[] getHeaders(byte[] rawData) throws Exception {
        CharSequenceReader charSequenceReader = new CharSequenceReader(new String(rawData, "UTF-8"));
        try(ICsvListReader listReader = new CsvListReader(charSequenceReader, CsvPreference.STANDARD_PREFERENCE)) {
            return listReader.getHeader(true);
        }
    }

}
