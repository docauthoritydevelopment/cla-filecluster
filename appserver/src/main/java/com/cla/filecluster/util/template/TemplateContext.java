package com.cla.filecluster.util.template;

import java.util.Map;

/**
 * Generic template context, wrapping specific template engine implementation
 *
 * Created by vladi on 3/27/2017.
 */
public interface TemplateContext {

    void put(String key, Object value);

    void putAll(Map<String, Object> props);

    String process();

}
