package com.cla.filecluster.util.query;

import org.springframework.data.domain.PageRequest;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * Created by uri on 03/09/2015.
 */
public class CriteriaQueryUtils {

    public static List<Tuple> executeTupleQuery(EntityManager entityManager, PageRequest pageRequest, CriteriaQuery<Tuple> query) {
        TypedQuery<Tuple> resultQuery = entityManager.createQuery(query);
        resultQuery.setFirstResult((int) pageRequest.getOffset()).setMaxResults(pageRequest.getPageSize());
        return resultQuery.getResultList();
    }

    public static String convertSlashesForQuerySearch(String path) {

        if (path.endsWith("/") || path.endsWith("\\") && path.length() > 1) {
            path = path.substring(0, path.length()-1);
        }

        CharSequence from = "\\";
        CharSequence to = "\\\\";
        return path.replace(from, to); // escape slashes correctly for sql query
    }
}
