package com.cla.filecluster.util.template;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

/**
 * Specific implementation of template engine using velocity as underlying engine.
 *
 * Created by vladi on 3/27/2017.
 */
public class VelocityTemplateEngine extends TemplateEngine{

    private VelocityEngine engine;

    public VelocityTemplateEngine() {
        engine = new VelocityEngine();
    }

    @Override
    protected TemplateContext createTemplateContext(String templateFilename) {
        Template template = engine.getTemplate(templateFilename);
        return new VelocityTemplateContext(template);
    }
}
