package com.cla.filecluster.util.categories;

public class FileCatCompositeId {

    private static final String DELIMITER = "!";

    private Long contentId;
    private Long fileId;

    private FileCatCompositeId(Long contentId, Long fileId) {
        this.contentId = contentId;
        this.fileId = fileId;
    }

    public static FileCatCompositeId of(Long contentId, Long fileId){
        return new FileCatCompositeId(contentId, fileId);
    }

    public static FileCatCompositeId of(String contentId, Long fileId){
        return new FileCatCompositeId(Long.valueOf(contentId), fileId);
    }

    public static FileCatCompositeId of(String compositeIdStr){
        String[] split = compositeIdStr.split(DELIMITER);
        if (split.length < 2){
            throw new IllegalArgumentException("Illegal composite id " + compositeIdStr);
        }
        return new FileCatCompositeId(Long.valueOf(split[0]), Long.valueOf(split[1]));
    }

    public Long getFileId() {
        return fileId;
    }

    public Long getContentId() {
        return contentId;
    }

    @Override
    public String toString() {
        Long firstSegment = (contentId == null) ? fileId : contentId;
        return firstSegment + DELIMITER + fileId;
    }
}
