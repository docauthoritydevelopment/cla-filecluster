package com.cla.filecluster.util.categories;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.FileAclDataDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.mediaconnector.mail.MailRelated;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.SearchPatternCount;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Created by uri on 14/12/2015.
 */
public class FileCatUtils {

    public static final String CATEGORY_SEPARATOR = ".";

    public static final String GLOBAL_SCOPE_IDENTIFIER = "g";

    private final static Logger logger = LoggerFactory.getLogger(FileCatUtils.class);

    public static final String EMPTY_VALUE = "_empty_";

    public static String getEmptyDefault(String value) {
        return Strings.isNullOrEmpty(value) ? EMPTY_VALUE : value;
    }

    @SuppressWarnings("unchecked")
    public static FileAclDataDto extractAclData(SolrFileEntity categoryFile) {
        List<String> aclReadData = categoryFile.getAclRead();
        List<String> aclDeniedReadData = categoryFile.getAclDenyRead();
        List<String> aclWriteData = categoryFile.getAclWrite();
        List<String> aclDeniedWriteData = categoryFile.getAclDenyWrite();

        Long fileId = categoryFile.getFileId();
        return createAclDataDto(fileId, aclReadData, aclDeniedReadData, aclWriteData, aclDeniedWriteData);
    }

    public static FileAclDataDto createAclDataDto(long fileId, List<String> aclReadData, List<String> aclDeniedReadData, List<String> aclWriteData, List<String> aclDeniedWriteData) {
        FileAclDataDto aclDataDto = new FileAclDataDto();
        aclDataDto.setAclReadData(aclReadData);
        aclDataDto.setAclWriteData(aclWriteData);
        aclDataDto.setAclDeniedReadData(aclDeniedReadData);
        aclDataDto.setAclDeniedWriteData(aclDeniedWriteData);
        aclDataDto.setFileId(fileId);
        return aclDataDto;
    }

    public static List<String> getRoles(SolrFileEntity categoryFile) {
        return categoryFile.getTags2();
    }

    public static String createCategoryIdentifier(FileCategory fileCategory) {
        return fileCategory.getType().getSolrName() + CATEGORY_SEPARATOR + fileCategory.getId();
    }

    public static Long getFileId(SolrDocument sd) {
        return (Long) sd.get(CatFileFieldType.FILE_ID.getSolrName());
    }

    @Deprecated
    public static ClaFileDto convertCatFileToDto(SolrDocument solrDocument) {
        Long fileId = getFileId(solrDocument);
        String fullPath = (String) solrDocument.get("fullPath");
        int type = (Integer) solrDocument.get("type");
        String baseName = (String) solrDocument.get("baseName");
        Long fsFileSizeInt = (Long) solrDocument.get("size");
        String analysisGroupId = (String) solrDocument.get("groupId");
        String userGroupId = (String) solrDocument.get("userGroupId");
        String groupName = (String) solrDocument.get("groupName");
        String owner = (String) solrDocument.get("owner");
        String contentIdString = (String) solrDocument.get("contentId");
        Long contentId = contentIdString == null ? null : Long.valueOf(contentIdString);
        Long rootFolderId = (Long) solrDocument.get("rootFolderId");
        Date fsLastAccessDate = null; //TODO
        Date fsLastModified = null; //TODO
        Date creationDate = null; //TODO
        Date initialScanDate = null; //TODO
        Date deletionDate = null; //TODO
        ClaFileDto claFileDto = new ClaFileDto(fileId, fullPath, type, baseName, fsFileSizeInt,
                fsLastModified, creationDate, fsLastAccessDate,
                initialScanDate, deletionDate,
                analysisGroupId, userGroupId, groupName, contentId, rootFolderId);
        claFileDto.setOwner(owner);
        claFileDto.setSortName((String) solrDocument.get("sortName"));
        return claFileDto;
    }

    public static String calcSortName(String baseName) {
        return calcSortName(baseName, null, null, null);
    }

    public static String calcSortName(String baseName, ItemType itemType, String containerFile, Long containerFileId) {
        String value = baseName;
        if (value.length() > 1024) {
            value = value.substring(0, 1024);
        }
        if (itemType != null && itemType.equals(ItemType.ATTACHMENT)) {
            String containerFileName = containerFile == null ? MailRelated.EMPTY_SUBJECT : containerFile;
            if (containerFileName.length() > 1024) {
                containerFileName = containerFileName.substring(0, 1024);
            }
            value = containerFileName + "-" + (containerFileId == null ? 0 : containerFileId) + "-" + value;
        }
        return value;
    }

    public static ClaFileDto convertClaFile(ClaFile claFile) {

        if (claFile == null) {
            throw new RuntimeException("Got a request to convert null claFile");
        }
        String analysisGroupId = claFile.getAnalysisGroupId();
        String userGroupId = claFile.getUserGroupId();
        String groupName = null;

        Long contentId = claFile.getContentMetadataId();
        ClaFileDto claFileDto = new ClaFileDto(claFile.getId(), claFile.getFullPath(), claFile.getTypeOrdinal(),
                claFile.getBaseName(), null,
                claFile.getFsLastModified(), null,
                claFile.getFsLastAccess(),
                claFile.getInitialScanDate(), claFile.getDeletionDate(),
                analysisGroupId, userGroupId, groupName, contentId, claFile.getRootFolderId());

        claFileDto.setCreationDate(claFile.getCreationDate());
        claFileDto.setExtension(claFile.getExtension());
        claFileDto.setState(claFile.getState());
        claFileDto.setItemType(claFile.getItemType());
        claFileDto.setNumOfAttachments(claFile.getNumOfAttachments());
        claFileDto.setContainerIdsByList(claFile.getContainerIds());
        claFileDto.setMailboxUpn(claFile.getMailboxUpn());
        claFileDto.setFileName(claFile.getFileName());
        claFileDto.setItemSubject(claFile.getItemSubject());
        claFileDto.setFsFileSize(claFile.getSizeOnMedia());
        claFileDto.setFileNameHashed(claFile.getFileNameHashed());
        claFileDto.setSortName(claFile.getSortName());

        claFileDto.setDocFolder(new DocFolderDto());
        claFileDto.getDocFolder().setId(claFile.getDocFolderId());

        claFileDto.setOwner(claFile.getOwner());
        if (claFile.getDocFolderId() != null) {
            claFileDto.setDocFolderId(claFile.getDocFolderId());
        }

        claFileDto.setSearchPatternsCounting(claFile.getSearchPatternsCounting());
        claFileDto.setLastMetadataChangeDate(claFile.getLastMetadataChangeDate());
        claFileDto.setMediaEntityId(claFile.getMediaEntityId());
        claFileDto.setSenderName(claFile.getSenderName());
        claFileDto.setSenderAddress(claFile.getSenderAddress());
        claFileDto.setSenderDomain(claFile.getSenderDomain());
        claFileDto.setSenderFullAddress(claFile.getSenderFullAddress());
        claFileDto.setRecipientsNames(claFile.getRecipientsNames());
        claFileDto.setRecipientsAddresses(claFile.getRecipientsAddresses());
        claFileDto.setRecipientsDomains(claFile.getRecipientsDomains());
        claFileDto.setRecipientsFullAddresses(claFile.getRecipientsFullAddresses());
        claFileDto.setSentDate(claFile.getSentDate());

        claFileDto.setDaLabels(claFile.getDaLabels());
        claFileDto.setExternalMetadata(claFile.getExternalMetadata());
        claFileDto.setActionsExpected(claFile.getActionsExpected());
        claFileDto.setRawMetadata(claFile.getRawMetadata());
        claFileDto.setDaMetadata(claFile.getDaMetadata());
        claFileDto.setDaMetadataType(claFile.getDaMetadataType());
        claFileDto.setGeneralMetadata(claFile.getGeneralMetadata());
        claFileDto.setActionConfirmation(claFile.getActionConfirmation());
        claFileDto.setLabelProcessDate(claFile.getLabelProcessDate());

        return claFileDto;
    }

    public static Set<SearchPatternCountDto> convertSearchPatternCounting(Collection<SearchPatternCount> searchPatternsCounting) {
        Set<SearchPatternCountDto> result = new HashSet<>();
        if (searchPatternsCounting == null) {
            return result;
        }
        for (SearchPatternCount searchPatternCount : searchPatternsCounting) {
            SearchPatternCountDto e = new SearchPatternCountDto();
            e.setPatternCount(searchPatternCount.getPatternCount());
            e.setPatternId(searchPatternCount.getPatternId());
            e.setPatternName(searchPatternCount.getPatternName());
            result.add(e);
        }
        return result;
    }

    public static List<SearchPatternCount> convertSearchPatternCountingFromDto(Collection<SearchPatternCountDto> searchPatternsCounting) {
        List<SearchPatternCount> result = Lists.newLinkedList();
        if (searchPatternsCounting == null) {
            return result;
        }
        for (SearchPatternCountDto searchPatternCount : searchPatternsCounting) {
            SearchPatternCount patternCount = new SearchPatternCount();
            patternCount.setPatternCount(searchPatternCount.getPatternCount());
            patternCount.setPatternId(searchPatternCount.getPatternId());
            patternCount.setPatternName(searchPatternCount.getPatternName());
            result.add(patternCount);
        }
        return result;
    }

    public static String convertSearchPatternCountingToJson(Set<SearchPatternCountDto> searchPatternsCounting) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(searchPatternsCounting);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert search patterns counting to JSON", e);
            throw new RuntimeException(e);
        }
    }

    public static String convertSearchPatternCountingSetToJson(Collection<SearchPatternCount> searchPatternsCounting) {
        Set<SearchPatternCountDto> searchPatternCountDtos = convertSearchPatternCounting(searchPatternsCounting);
        return convertSearchPatternCountingToJson(searchPatternCountDtos);
    }

    public static Set<SearchPatternCountDto> convertFromJsonSearchPatterns(String json) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            SearchPatternCountDto[] patterns = ModelDtoConversionUtils.getMapper().readValue(json, SearchPatternCountDto[].class);
            return patterns == null ? new HashSet<>() : new HashSet<>(Arrays.asList(patterns));
        } catch (IOException e) {
            logger.error("Failed to convert SearchPatterns string [" + json + "] to object", e);
            throw new RuntimeException("Failed to convert SearchPatterns string [" + json + "] to object", e);
        }
    }

    public static boolean hasId(ClaFile claFile) {
        return claFile != null && claFile.getId() != null && claFile.getId() > 0;
    }
}
