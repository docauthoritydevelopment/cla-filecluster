package com.cla.filecluster.util.categories;

import com.cla.filecluster.domain.entity.categorization.CategoryFilter;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import com.cla.filecluster.domain.entity.categorization.GroupCategoryFilter;
import com.cla.filecluster.domain.entity.categorization.MissingValueCategoryFilter;

/**
 * Created by uri on 26/07/2015.
 */
public class FileCategoryFilterUtils {

    public static boolean isGroupFilterCategory(FileCategory fileCategory) {
        return doesCategoryHaveFirstFilterType(fileCategory,GroupCategoryFilter.class);
    }

    public static boolean isMissingValueCategory(FileCategory fileCategory) {
        return doesCategoryHaveFirstFilterType(fileCategory,MissingValueCategoryFilter.class);
    }

    public static boolean doesCategoryHaveFirstFilterType(FileCategory fileCategory,Class<? extends CategoryFilter> categoryFilterClass) {
        if (fileCategory.getFilters() != null || fileCategory.getFilters().size() >0) {
            CategoryFilter categoryFilter = fileCategory.getFilters().get(0);
            if (categoryFilterClass.isInstance(categoryFilter)) {
                return true;
            }
        }
        return false;
    }
}
