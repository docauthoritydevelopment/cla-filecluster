package com.cla.filecluster.util.template;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;

import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

/**
 * Velocity specific implementation of template context
 * Created by vladi on 3/27/2017.
 */
public class VelocityTemplateContext implements TemplateContext {

    private Template template;
    private VelocityContext context;

    public VelocityTemplateContext(Template template) {
        this.template = template;
        this.context = new VelocityContext();
    }

    @Override
    public void put(String key, Object value) {
        context.put(key, value);
    }

    @Override
    public void putAll(Map<String, Object> props) {
        if (props != null) {
            props.entrySet().forEach(entry -> context.put(entry.getKey(), entry.getValue()));
        }
    }

    @Override
    public String process() {
        Writer writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
