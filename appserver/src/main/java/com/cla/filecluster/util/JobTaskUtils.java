package com.cla.filecluster.util;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.jobmanager.domain.entity.Job;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * Created by uri on 22-Mar-17.
 */
public class JobTaskUtils {

    private static final Logger logger = LoggerFactory.getLogger(JobTaskUtils.class);

    public static Collection<Long> getFinishedRuns(Table<Long, TaskState, Number> taskStatesByType) {
        return getFinishedRuns(null, taskStatesByType);
    }

    /**
     * Get collection of run context IDs, for which there are only DONE/FAILED tasks and there are no NEW or ENQUEUED tasks
     *
     * @param taskStatesByType table of task states by context from the DB
     * @return collection of run context IDs for which all tasks are finished
     */
    public static Collection<Long> getFinishedRuns(List<SimpleJob> jobs, Table<Long, TaskState, Number> taskStatesByType) {
        Set<Long> runIds = new HashSet<>(taskStatesByType.rowKeySet());
        if (jobs != null && !jobs.isEmpty()) {
            runIds.addAll(jobs.stream()
                    .filter(j -> j.getEstimatedTaskCount() == 0 ||
                            j.getCurrentTaskCount() > 0 ||
                            j.getFailedCount() > 0 ||
                            j.getType().equals(JobType.INGEST) ||
                            j.getType().equals(JobType.ANALYZE))
                    .map(Job::getRunContext).collect(Collectors.toSet()));
        }
        return runIds.stream()
                .filter(runId -> filterFinishedRun(runId, taskStatesByType))
                .collect(Collectors.toSet());
    }

    /**
     *
     * @param runId                 runId to check
     * @param taskStatesByType      table of task states by task type
     * @return true if the run's tasks are finished
     */
    private static boolean filterFinishedRun(long runId, Table<Long, TaskState, Number> taskStatesByType) {
        Map<TaskState, Number> taskStateMap = taskStatesByType.row(runId);
        Number newTaskCount = taskStateMap.get(TaskState.NEW);
        Number enqueuedTaskCount = taskStateMap.get(TaskState.ENQUEUED);
        Number doneTaskCount = taskStateMap.get(TaskState.DONE);
        Number failedTaskCount = taskStateMap.get(TaskState.FAILED);
        Number consumingTaskCount = taskStateMap.get(TaskState.CONSUMING);
        if (newTaskCount == null) {newTaskCount = 0;}
        if (enqueuedTaskCount== null) {enqueuedTaskCount= 0;}
        if (doneTaskCount == null) {doneTaskCount = 0;}
        if (failedTaskCount == null) {failedTaskCount = 0;}
        if (consumingTaskCount == null) {consumingTaskCount = 0;}

        boolean finished = (doneTaskCount.longValue() >= 0)
                && (failedTaskCount.longValue() >= 0)
                && (newTaskCount.longValue() == 0)
                && (consumingTaskCount.longValue() == 0)
                && (enqueuedTaskCount.longValue() == 0);
        if (finished) {
            logger.info("Run {} finished processing tasks (n:{},e:{},d:{})",runId,newTaskCount,enqueuedTaskCount,doneTaskCount);
        }
        return finished;
    }

    public static Collection<Long> getRunsWithDoneTasks(Table<Long, TaskState, Number> taskStatesByType) {
        Set<Long> runIds = taskStatesByType.rowKeySet();
        return runIds.stream()
                .filter(runId -> filterRunWithDoneTasks(runId, taskStatesByType))
                .collect(Collectors.toSet());
    }

    // TODO Itai: use pre-defined predicates to de-dup this code (and also wrap the log with a consumer - the problem is it needs to be a varargs consumer)
    private static boolean filterRunWithDoneTasks(Long runId, Table<Long, TaskState, Number> taskStatesByType) {
        Map<TaskState, Number> taskStateMap = taskStatesByType.row(runId);
        Number doneTaskCount = taskStateMap.get(TaskState.DONE);
        if (doneTaskCount == null) {doneTaskCount = 0;}
        boolean done = doneTaskCount.longValue() > 0;
        if (done) {
            logger.info("Run {} has {} done tasks", runId, doneTaskCount);
        }
        return done;
    }
}
