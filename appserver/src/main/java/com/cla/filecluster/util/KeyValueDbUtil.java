package com.cla.filecluster.util;

import com.cla.kvdb.KeyValueDatabaseRepository;

/**
 * Created by: yael
 * Created on: 12/27/2018
 */
public class KeyValueDbUtil {

    public static String getServerStoreName(KeyValueDatabaseRepository kvdbRepo, Long rootFolderId) {
        return kvdbRepo.getRepoName(rootFolderId, "appserver");
    }
}
