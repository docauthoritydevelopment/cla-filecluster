package com.cla.filecluster.util.csv;

import com.cla.common.domain.dto.RowDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 29/09/2015.
 */
public class CsvUtils {

    static Logger logger = LoggerFactory.getLogger(CsvUtils.class);

    public static Map<String, Integer> handleCsvHeaders(CsvReader csvReader, @NotNull String templateFileName, String[] requiredHeaders) {
        Map<String,Integer> headersLocationMap = new HashMap<>();
        String[] headers;
        try {
            headers = csvReader.getHeaders(templateFileName);
            for (int i=0; i< headers.length; i++) {
                headers[i] = headers[i].trim();
            }
        } catch (final Exception e) {
            logger.error("Error: Could not read csv input file " + templateFileName, e);
            throw new RuntimeException("Error: Could not read csv input file "+ templateFileName,e);
        }

        if (!headersValid(headers,requiredHeaders)) {
            logger.error("Error: unexpected input file format: " + templateFileName);
            throw new RuntimeException("Error: unexpected input file format: " + templateFileName+". Expected headers: [" + Arrays.toString(requiredHeaders)+"] But got headers: ["+Arrays.toString(headers)+"]");
        }
        for (int i=0; i < headers.length; i++) {
            headersLocationMap.put(headers[i],i);
        }
        return headersLocationMap;
    }
    public static Map<String, Integer> handleCsvHeaders(CsvReader csvReader, @NotNull byte[] bytes, String[] requiredHeaders) {
        try {
               String[] csvHeaders = csvReader.getHeaders(bytes);
            return handleCsvHeaders(csvHeaders,requiredHeaders);
        } catch (final Exception e) {
            logger.error("Error: Could not read csv from bytes", e);
            throw new RuntimeException("Error: Could not read csv from bytes", e);
        }

    }
    public static Map<String, Integer> handleCsvHeaders(@NotNull String[] csvHeaders, String[] requiredHeaders) {
        Map<String,Integer> headersLocationMap = new HashMap<>();

        for (int i=0; i< csvHeaders.length; i++) {
            csvHeaders[i] = csvHeaders[i].trim();
        }
        if (!headersValid(csvHeaders,requiredHeaders)) {
                return null;
        }
        for (int i=0; i < csvHeaders.length; i++) {
            headersLocationMap.put(csvHeaders[i],i);
        }
        return headersLocationMap;
    }

    private static boolean headersValid(String[] headers, String[] requiredHeaders) {
        List<String> headersList = Arrays.asList(headers);
        if (!headersList.containsAll(Arrays.asList(requiredHeaders))) {
            logger.info("Expected headers: {}",Arrays.toString(requiredHeaders));
            return false;
        }
        return true;
    }

    public static String getOptionalFieldValue(RowDTO row, Map<String, Integer> headersLocationMap, String headerName) {
        Integer headerId = headersLocationMap.get(headerName);
        if (headerId != null && headerId >= 0) {
            return row.getFieldsValues()[headerId];
        }
        return null;
    }


}
