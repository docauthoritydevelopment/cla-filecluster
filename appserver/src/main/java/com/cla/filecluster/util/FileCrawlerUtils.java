package com.cla.filecluster.util;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by uri on 31/05/2016.
 */
public class FileCrawlerUtils {

    public static final String ANALYZE = "analyze";
    public static final String SCAN = "scan";
    public static final String INGEST = "ingest";
    public static final String EXTRACT = "extract";

    public static final List<String> allOperations = Lists.newArrayList(FileCrawlerUtils.SCAN,
            FileCrawlerUtils.INGEST,FileCrawlerUtils.ANALYZE, FileCrawlerUtils.EXTRACT);

}
