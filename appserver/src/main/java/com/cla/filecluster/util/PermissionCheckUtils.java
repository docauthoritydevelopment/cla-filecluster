package com.cla.filecluster.util;

import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.common.domain.dto.security.*;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.dto.AccessState;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.service.security.UserService;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.attribute.AclEntryType;
import java.util.*;
import java.util.stream.Collectors;

public class PermissionCheckUtils {

    private static Logger logger = LoggerFactory.getLogger(PermissionCheckUtils.class);

    public static AccessState getCurrentFileAccessState(Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions,
                                                        List<String> genericTokens, boolean caseSensitiveAcls, long readPermissionMask) {
        return getCurrentAccessState(rootFolderSharePermissions, genericTokens,
                Authority.ViewFiles, Authority.ViewPermittedFiles, caseSensitiveAcls, readPermissionMask);
    }

    public static AccessState getCurrentContentAccessState(Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions,
                                                           List<String> genericTokens, boolean caseSensitiveAcls, long readPermissionMask) {
        return getCurrentAccessState(rootFolderSharePermissions, genericTokens,
                Authority.ViewContent, Authority.ViewPermittedContent, caseSensitiveAcls, readPermissionMask);
    }

    /**
     * Analyze the currently logged in user and his roles, summarize his access information
     * @return access state object - summary of the user access information
     */
    private static AccessState getCurrentAccessState(Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions,
                                                     List<String> genericTokens, Authority regular, Authority permitted,
                                                    boolean caseSensitiveAcls, long readPermissionMask) {
        AccessState accessState = new AccessState();
        Optional<ClaUserDetails> currentUserDetailsOp = UserService.getCurrentUserDetails();
        ClaUserDetails userDetails = currentUserDetailsOp.orElse(null);
        if (userDetails != null){
            UserDto user = userDetails.getUser();
            // allow full access to system users
            if (UserType.SYSTEM.equals(user.getUserType())){
                logger.trace("The user {} is a system user.", user.getName());
                accessState.setAccessDenied(false);
                accessState.setAllowAllFiles(true);
            }

            // get functional roles from the user, in order to modify SOLR query to filter out non-permitted files
            if (user.getFuncRoleDtos().size() > 0){
                Set<String> allowAll = user.getFuncRoleDtos().stream()
                        .filter(funcRole -> containsOneOfAuthorities(funcRole, regular))
                        .map(fr -> AssociationIdUtils.createFunctionalRoleIdentfier(fr.getId()))
                        .collect(Collectors.toSet());
                Set<String> allowPermitted = user.getFuncRoleDtos().stream()
                        .filter(funcRole -> containsOneOfAuthorities(funcRole, permitted))
                        .map(fr -> AssociationIdUtils.createFunctionalRoleIdentfier(fr.getId()))
                        .collect(Collectors.toSet());
                accessState.setAccessDenied(false);
                accessState.getAllowAllFuncRoles().addAll(allowAll);
                accessState.getAllowPermittedFuncRoles().addAll(allowPermitted);
            }
            if (genericTokens != null && genericTokens.size() > 0) {
                accessState.getAccessTokens().addAll(genericTokens.stream()
                        .map(token -> conditionalToLowerCase(token, caseSensitiveAcls))
                        .collect(Collectors.toList()));
            }
            // get users ACLs
            if (user.getAccessTokens().size() > 0) {
                accessState.setAccessDenied(false);
                accessState.getAccessTokens().addAll(user.getAccessTokens().stream()
                        .map(token -> conditionalToLowerCase(token, caseSensitiveAcls))
                                                     .collect(Collectors.toList()));
                if (logger.isTraceEnabled()) {
                    logger.trace("The user has the following access tokens: [{}]", user.getAccessTokens().stream()
                            .collect(Collectors.joining(",")));
                }
            }

            if (!UserType.SYSTEM.equals(user.getUserType())) {
                Set<Long> deniedRootFolders = getRootFolderAccessDenied(
                        rootFolderSharePermissions,
                        accessState.getAccessTokens(), genericTokens, readPermissionMask, caseSensitiveAcls);
                accessState.setDeniedRootFolders(deniedRootFolders);
            }

            // analyze user business roles
            Set<SystemRoleDto> systemRoleDtos = user.getBizRoleDtos().stream()
                    .flatMap(br -> br.getTemplate().getSystemRoleDtos().stream())
                    .collect(Collectors.toSet());
            if (logger.isTraceEnabled()){
                String sysRolesStr = systemRoleDtos.stream()
                    .map(SystemRoleDto::getName)
                    .collect(Collectors.joining(","));
                logger.trace("The user {} has acquired the following system roles from his biz-roles: [{}]",
                        user.getName(), sysRolesStr);
            }
            // see if any of the user business roles grant him a full access to any files
            boolean hasAllowAllFiles = systemRoleDtos.stream()
                    .anyMatch(role ->
                            // general admin can see all the files
                            Authority.GeneralAdmin.nameEq(role.getName()) ||
                                    // user with global ViewFiles permission can see all files
                                    regular.nameEq(role.getName())
                    );
            if (hasAllowAllFiles) {
                logger.trace("The user {} has permission to view all files.", user.getName());
                accessState.setAccessDenied(false);
                accessState.setAllowAllFiles(true);
            }
            // see if any of the user business roles grant him an access to files allowed to him by ACLs
            boolean hasAllowPermittedFiles = systemRoleDtos.stream()
                    .anyMatch(role ->
                            permitted.nameEq(role.getName())
                    );
            if (hasAllowPermittedFiles){
                logger.trace("The user {} has permission to view files with matching ACLs.", user.getName());
                accessState.setAccessDenied(false);
                accessState.setAllowPermittedFiles(true);
            }
        }
        return accessState;
    }

    public static List<String> getGenericTokensAsList(String[] genericTokens, boolean caseSensitiveAcls) {
        List<String> genericTokensList = null;
        if (genericTokens != null && genericTokens.length > 0) {
            // should be in lower-case, since the security mechanism compares the ACLs to their lower-case version
            genericTokensList = Arrays.stream(genericTokens)
                    .map(token -> conditionalToLowerCase(token, caseSensitiveAcls))
                    .collect(Collectors.toList());
        }
        return genericTokensList;
    }

    private static boolean containsOneOfAuthorities(CompositeRoleDto compositeRole, Authority ... authorities){
        Set<String> compositeRoleAuthorities = compositeRole.getTemplate().getSystemRoleDtos().stream()
                .map(SystemRoleDto::getName)
                .collect(Collectors.toSet());
        Set<String> requiredAuthorities = Arrays.stream(authorities)
                .map(Authority::getName)
                .collect(Collectors.toSet());
        return !Sets.intersection(compositeRoleAuthorities, requiredAuthorities).isEmpty();
    }

    private static String conditionalToLowerCase(String upper, boolean caseSensitive){
        return caseSensitive ? upper : upper.toLowerCase();
    }

    public static boolean hasPermission(long permissionValue, long mask) {
        return (permissionValue & mask) > 0;
    }

    /**
     * returns root folder ids that user is not allowed to access
     * @param rootFolderSharePermissions - share permissions of all root folders
     * @param userAccessTokens - user access tokens
     * @param genericTokens - e.g everyone
     * @param readPermissionMask - read mask permission
     * @return returns root folder ids that user is not allowed to access
     */
    public static Set<Long> getRootFolderAccessDenied(Map<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions,
            Set<String> userAccessTokens, List<String> genericTokens, long readPermissionMask, boolean caseSensitiveAcls) {

        Set<Long> disAllowedRootFoldersForUser = Sets.newHashSet();

        if (rootFolderSharePermissions != null && !rootFolderSharePermissions.isEmpty()) {

            List<String> genericTokensClean = genericTokens == null ? new ArrayList<>() :
                    genericTokens.stream().map(a -> {
                        if (a.startsWith("//")) {
                            a = a.replace("//", "");
                        } else if (a.startsWith("\\\\")) {
                            a = a.replace("\\\\", "");
                        }
                        return a;
                    }).collect(Collectors.toList());

            Set<String> userTokens = userAccessTokens == null ? Sets.newHashSet() : userAccessTokens;

            for (Long rootFolderId : rootFolderSharePermissions.keySet()) {
                List<RootFolderSharePermissionDto> sharePermissions = rootFolderSharePermissions.get(rootFolderId);
                boolean allowed = false;
                boolean deny = false;

                if (logger.isTraceEnabled()) {
                    logger.trace("user tokens {} perm {} generic {} caseSensitiveAcls {}", userTokens, sharePermissions, genericTokensClean, caseSensitiveAcls);
                }

                for (RootFolderSharePermissionDto perm : sharePermissions) {
                    if (hasPermission(perm.getMask(), readPermissionMask)) {
                        if (userTokens.contains(conditionalToLowerCase(perm.getUser(), caseSensitiveAcls)) ||
                                userTokens.contains(conditionalToLowerCase(perm.getUser().replaceAll("\\\\", "\\\\\\\\"), caseSensitiveAcls)) ||
                                genericTokensClean.contains(conditionalToLowerCase(perm.getUser(), caseSensitiveAcls))) {
                           if (AclEntryType.ALLOW.equals(perm.getAclEntryType())) {
                               allowed = true;
                           } else {
                               deny = true;
                           }
                        }
                    }
                }

                // currently 1 deny negates any allow as we dont have priorities for the different access tokens
                // same behavior is consistent with regular acls
                if (!allowed || deny) {
                    disAllowedRootFoldersForUser.add(rootFolderId);
                }
            }
        }
        if (logger.isTraceEnabled()) {
            logger.trace("root folder perm {} disallow {} for user tokens {} caseSensitiveAcls {}",
                    rootFolderSharePermissions, disAllowedRootFoldersForUser, userAccessTokens, caseSensitiveAcls);
        }
        return disAllowedRootFoldersForUser;
    }
}
