package com.cla.filecluster.util.query;

import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.jobmanager.domain.entity.IdentifiableByLong;
import com.cla.filecluster.repository.SequenceIdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Consumer;

/**
 * Generic utility service for db transaction actions
 * Created by: yael
 */
@Service
public class DBTemplateUtils {

    private final static Logger logger = LoggerFactory.getLogger(DBTemplateUtils.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private SequenceIdGenerator sequenceIdGenerator;

    @Value("${hibernate.jdbc.batch_size:10}")
    private int batchSize;



    private TimeSource timeSource = new TimeSource();

    /**
     * Creates or updates objects in a batch in the db
     * <p>
     *
     * Note: the {@link IdentifiableByLong#getId()} method of each entity is used
     * to determine whether it is transient or not.
     * <br>
     * Entities with null id are considered transient and are passed to {@link EntityManager#persist(Object)}.
     * Other entities are passed to {@link EntityManager#merge(Object)}.
     * <br>
     * It's the caller's responsibility to make sure that each entity passed to this method
     * has a null id iff it is transient.
     *
     * @param entities - entities to persist
     *
     * @see <a href="http://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#pc">
     *          Persistence Contexts chapter in Hibernate user guide
     *     </a>
     */
    public void persistEntitiesBatchInTx(Collection<? extends IdentifiableByLong> entities) {
        long start = timeSource.currentTimeMillis();
        Runnable r = () -> {
            int i = 0;
            for (IdentifiableByLong entity : entities) {
                if (entity.getId() == null) {
                    entityManager.persist(entity);
                } else {
                    entityManager.merge(entity);
                }
                i++;
                if (i % batchSize == 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
            }
        };
        doInTransaction(r);
        long duration = timeSource.millisSince(start);
        logger.debug("persistEntitiesBatchInTx {} took {} ms", entities.size(), duration);
    }

    public void bulkSqlBatchInTx(List<String> queries) {
        long start = timeSource.currentTimeMillis();
        Runnable r = () -> {
            int i = 0;
            for (String q : queries) {
                if (q != null) {
                    entityManager.createQuery(q).executeUpdate();
                }
                i++;
                if (i % batchSize == 0) {
                    entityManager.flush();
                    entityManager.clear();
                }
            }
        };
        doInTransaction(r);
        long duration = timeSource.millisSince(start);
        logger.debug("bulkSqlBatchInTx {} took {} ms", queries.size(), duration);
    }

    /**
     * Execute runnable in separate transaction
     * @param runnable runnable to execute
     */
    public void doInTransaction(Runnable runnable){
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        doInTransaction(template, runnable);
    }


    /**
     * Execute jdbcOperation in separate transaction
     * @param jdbcOperation to execute
     */

    /**
     * Execute jdbc operaion in separate transaction via jdbcTemplate
     * @param jdbcOperation runnable to execute
     */
    public void doInTransaction(Consumer<JdbcTemplate> jdbcOperation){
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        doInTransaction(template, jdbcOperation);
    }

    private void doInTransaction(TransactionTemplate template, Consumer<JdbcTemplate> jdbcTemplateConsumer) {
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                long start = timeSource.currentTimeMillis();
                jdbcTemplateConsumer.accept(jdbcTemplate);
                long duration = timeSource.millisSince(start);
                logger.trace("doInTransaction took {} ms", duration);
            }
        });
    }

    /**
     * Execute runnable in separate transaction
     * @param runnable  runnable to execute
     * @param propagationBehavior   propagation (REQUIRED, REQUIRED_NEW etc.)
     */
    public void doInTransaction(Runnable runnable, int propagationBehavior){
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.setPropagationBehavior(propagationBehavior);
        doInTransaction(template, runnable);
    }

    public <R> R  doInTransaction(Callable<R> runnable){
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        return doInTransaction(template, runnable);
    }

    public <R> R  doInTransaction(Callable<R> runnable, int propagationBehavior){
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.setPropagationBehavior(propagationBehavior);
        return doInTransaction(template, runnable);
    }

    /**
     * Execute runnable in separate read only transaction
     * @param runnable runnable to execute
     */
    public void doInTransactionReadOnly(Runnable runnable){
        TransactionTemplate template = new TransactionTemplate(transactionManager);
        template.setReadOnly(true);
        doInTransaction(template, runnable);
    }

    private void doInTransaction(TransactionTemplate template, Runnable runnable) {
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                long start = timeSource.currentTimeMillis();
                runnable.run();
                long duration = timeSource.millisSince(start);
                logger.trace("doInTransaction took {} ms", duration);
            }
        });
    }

    private <R> R doInTransaction(TransactionTemplate template, Callable<R> runnable) {
        return template.execute(status -> {
            R result;
            long start = timeSource.currentTimeMillis();
            try {
                result = runnable.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            long duration = timeSource.millisSince(start);
            logger.trace("doInTransaction took {} ms", duration);
            return result;
        });
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public SequenceIdGenerator getSequenceIdGenerator() {
        return sequenceIdGenerator;
    }
}
