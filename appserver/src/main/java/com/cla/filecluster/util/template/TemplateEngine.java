package com.cla.filecluster.util.template;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.TimeUnit;

/**
 *
 * Created by vladi on 3/27/2017.
 */
public abstract class TemplateEngine {

    private LoadingCache<String, TemplateContext> templateLoadingCache = CacheBuilder.newBuilder()
            .maximumSize(10)
            .expireAfterWrite(5, TimeUnit.MINUTES)
            .build(
                    new CacheLoader<String, TemplateContext>() {
                        public TemplateContext load(String key) {
                            return createTemplateContext(key);
                        }
                    });

    public TemplateContext getTemplate(String templateName){
        return templateLoadingCache.getUnchecked(templateName);
    }

    protected abstract TemplateContext createTemplateContext(String templateFilename);



}
