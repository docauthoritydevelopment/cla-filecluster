package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

public class StateMachineTracker<S, E> extends StateMachineListenerAdapter<S, E> {

    private static final Logger logger = LoggerFactory.getLogger(StateMachineTracker.class);

    private final EventBus eventBus;
    private final String machineId;

    public StateMachineTracker(EventBus eventBus, String machineId) {
        this.eventBus = eventBus;
        this.machineId = machineId;
    }

    @Override
    public void stateChanged(State<S, E> from, State<S, E> to) {
        S fromState = from == null ? null : from.getId();
        S toState = to == null ? null : to.getId();
        System.out.println(String.format("Machine (id:%s} transition from %s to %s", machineId, fromState, toState));
        logger.info("Machine (id:{}} transition from {} to {}", machineId, fromState, toState);
    }

}



