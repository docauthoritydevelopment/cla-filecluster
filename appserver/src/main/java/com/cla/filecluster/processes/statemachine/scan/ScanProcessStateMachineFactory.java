package com.cla.filecluster.processes.statemachine.scan;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.scan.ScanBehaviorType;
import com.cla.filecluster.processes.domain.scan.ScanProcess;
import com.cla.filecluster.processes.domain.scan.ScanProcessDefinition;
import com.cla.filecluster.processes.statemachine.ProcessStateMachineFactory;
import com.cla.filecluster.processes.statemachine.scan.builders.FullScanProcessStateMachineBuilder;
import com.cla.filecluster.processes.statemachine.scan.builders.OnlyMapProcessStateMachineBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;

@Component
public class ScanProcessStateMachineFactory implements ProcessStateMachineFactory<ScanProcess> {


    public static final String NO_STATE_MACHINE_DEFINED_FOR_SCAN_BEHAVIOR_ERR_MSG = "No state machine exists for scan config %s";

    @Autowired
    private FullScanProcessStateMachineBuilder fullScanProcessStateMachineBuilder;

    @Autowired
    private OnlyMapProcessStateMachineBuilder onlyMapProcessStateMachineBuilder;

    @Override
    public StateMachine<Phase, EventType> newStateMachine(ScanProcess scanProcess) {
        ScanProcessDefinition processDefinition = scanProcess.getProcessDefinition();
        ScanBehaviorType scanBehaviorType = processDefinition.getBehavior().getScanBehaviorType();
        switch (scanBehaviorType) {
            case ONLY_MAP:
                return onlyMapProcessStateMachineBuilder.build(scanProcess);
            case FULL_SCAN:
                return fullScanProcessStateMachineBuilder.build(scanProcess);
        }
        throw new RuntimeException(String.format(NO_STATE_MACHINE_DEFINED_FOR_SCAN_BEHAVIOR_ERR_MSG, scanBehaviorType));
    }
}



