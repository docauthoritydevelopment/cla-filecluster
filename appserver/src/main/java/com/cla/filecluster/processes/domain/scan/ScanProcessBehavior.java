package com.cla.filecluster.processes.domain.scan;

import com.cla.filecluster.processes.domain.ProcessBehavior;

public class ScanProcessBehavior implements ProcessBehavior {

    private ScanBehaviorType scanBehaviorType;

    public ScanBehaviorType getScanBehaviorType() {
        return scanBehaviorType;
    }

    public void setScanBehaviorType(ScanBehaviorType scanBehaviorType) {
        this.scanBehaviorType = scanBehaviorType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScanProcessBehavior that = (ScanProcessBehavior) o;

        return scanBehaviorType == that.scanBehaviorType;
    }

    @Override
    public int hashCode() {
        return scanBehaviorType != null ? scanBehaviorType.hashCode() : 0;
    }
}
