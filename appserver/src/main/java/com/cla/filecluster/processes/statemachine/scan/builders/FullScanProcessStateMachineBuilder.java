package com.cla.filecluster.processes.statemachine.scan.builders;

import com.beust.jcommander.internal.Lists;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.scan.ScanPhase;
import com.cla.filecluster.processes.domain.scan.ScanProcess;
import org.springframework.statemachine.config.configurers.ExternalTransitionConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FullScanProcessStateMachineBuilder extends AbstractScanProcessStateMachineBuilder {

    @Override
    public Phase initialPhase() {
        return ScanPhase.MAP;
    }

    @Override
    public List<ScanPhase> processPhases() {
        return Lists.newArrayList(
                ScanPhase.MAP,
                ScanPhase.MAP_FINALIZE,
                ScanPhase.INGEST,
                ScanPhase.INGEST_FINALIZE,
                ScanPhase.ANALYZE,
                ScanPhase.ANALYZE_FINALIZE
        );
    }

    @Override
    protected void configureTransitions(
            ExternalTransitionConfigurer<Phase, EventType> transitionConfigurer,
            ScanProcess scanProcessConfig) throws Exception {
        transitionConfigurer
                .source(ScanPhase.MAP).target(ScanPhase.MAP_FINALIZE)
                .event(EventType.SCAN_DONE_MAP)
                .and()
                .withExternal()
                .source(ScanPhase.MAP_FINALIZE).target(ScanPhase.INGEST)
                .event(EventType.SCAN_DONE_MAP_FINALIZE)
                .and()
                .withExternal()
                .source(ScanPhase.INGEST).target(ScanPhase.INGEST_FINALIZE)
                .event(EventType.SCAN_DONE_INGEST)
                .and()
                .withExternal()
                .source(ScanPhase.INGEST_FINALIZE).target(ScanPhase.ANALYZE)
                .event(EventType.SCAN_DONE_INGEST_FINALIZE)
                .and()
                .withExternal()
                .source(ScanPhase.ANALYZE).target(ScanPhase.ANALYZE_FINALIZE)
                .event(EventType.SCAN_DONE_ANALYZE)
                .and()
                .withExternal()
                .source(ScanPhase.ANALYZE_FINALIZE).target(Phase.FINISHED)
                .event(EventType.SCAN_DONE_ANALYZE_FINALIZE);
    }
}
