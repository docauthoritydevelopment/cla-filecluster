package com.cla.filecluster.processes.statemachine.scan.builders;


import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.scan.ScanPhase;
import com.cla.filecluster.processes.domain.scan.ScanProcess;
import com.google.common.collect.Lists;
import org.springframework.statemachine.config.configurers.ExternalTransitionConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class OnlyMapProcessStateMachineBuilder extends AbstractScanProcessStateMachineBuilder {

    @Override
    protected ScanPhase initialPhase() {
        return ScanPhase.MAP;
    }


    @Override
    protected List<ScanPhase> processPhases() {
        return Lists.newArrayList(ScanPhase.MAP, ScanPhase.MAP_FINALIZE);
    }

    @Override
    protected void configureTransitions(
            ExternalTransitionConfigurer<Phase, EventType> transitionConfigurer,
            ScanProcess scanProcess) throws Exception {
        transitionConfigurer
                .source(ScanPhase.MAP).target(ScanPhase.MAP_FINALIZE)
                .event(EventType.SCAN_DONE_MAP)
                .and()
                .withExternal()
                .source(ScanPhase.MAP_FINALIZE).target(ScanPhase.FINISHED)
                .event(EventType.SCAN_DONE_MAP_FINALIZE);
    }
}
