package com.cla.filecluster.processes.statemachine;

import com.cla.filecluster.processes.domain.Process;
import com.cla.filecluster.processes.domain.ProcessDefinition;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

public class ProcessStateMachineFactoryProvider {

    private Map<Class<? extends Process>, ProcessStateMachineFactory> factoriesMap = new HashMap<>();

    public ProcessStateMachineFactoryProvider(Map<Class<? extends Process>, ProcessStateMachineFactory> factoriesMap) {
        this.factoriesMap = factoriesMap;
    }

    public <P extends Process> ProcessStateMachineFactory getFactory(P process) {
        Class<? extends Process> pdClass = Objects.requireNonNull(process).getClass();
        return Optional.ofNullable(factoriesMap.get(pdClass))
                .orElseThrow(() -> new RuntimeException(String.format("No state machine factory for %s", process)));
    }
}
