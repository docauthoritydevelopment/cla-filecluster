package com.cla.filecluster.processes.domain;

public final class ProcessKeys {

    public static String scanKey(long rootFolderId) {
        return "scan-" + rootFolderId;
    }
}
