package com.cla.filecluster.processes.domain.scan;

import com.cla.filecluster.processes.domain.AbstractProcessDefinition;


public class ScanProcessDefinition extends AbstractProcessDefinition<ScanProcessBehavior, ScanProcessConfig> {
}
