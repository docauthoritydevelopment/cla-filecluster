package com.cla.filecluster.processes.domain.scan;

import com.cla.filecluster.processes.domain.Phase;

/**
 * Possible phases for scan process
 */
public enum ScanPhase implements Phase {
    MAP,
    MAP_FINALIZE,
    INGEST,
    DECIDE_INGEST_INTERIM,
    INGEST_INTERIM,
    INGEST_FINALIZE,
    ANALYZE,
    DECIDE_ANALYZE_INTERIM,
    ANALYZE_INTERIM,
    ANALYZE_FINALIZE;


}
