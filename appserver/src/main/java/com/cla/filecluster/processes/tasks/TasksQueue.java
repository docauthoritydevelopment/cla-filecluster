package com.cla.filecluster.processes.tasks;


import com.cla.filecluster.processes.domain.Phase;

import java.util.List;

/**
 * Data structure for adding tasks, enqueuing, marking as done and cleaning them.
 *
 * @author nadav
 */
public interface TasksQueue {

    <T extends Task> void addTask(T task);

    <T extends Task> List<T> enqueueTasks(String processId, Phase phase, int batchSize);

    <T extends Task> void done(T task);
}
