package com.cla.filecluster.processes.domain;

public interface Phase {

    Phase PROCESSING = new DefaultProcessPhase("PROCESSING");

    Phase PAUSED = new DefaultProcessPhase("PAUSED");

    Phase FINISHED = new DefaultProcessPhase("FINISHED");

    Phase STOPPED = new DefaultProcessPhase("STOPPED");

    Phase HISTORY = new DefaultProcessPhase("HISTORY");

    class DefaultProcessPhase implements Phase {

        private String phaseId;

        public DefaultProcessPhase() {
        }

        public DefaultProcessPhase(String phaseId) {
            this.phaseId = phaseId;
        }



        @Override
        public String toString() {
            return phaseId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DefaultProcessPhase that = (DefaultProcessPhase) o;

            return phaseId != null ? phaseId.equals(that.phaseId) : that.phaseId == null;
        }

        @Override
        public int hashCode() {
            return phaseId != null ? phaseId.hashCode() : 0;
        }
    }
}
