package com.cla.filecluster.processes.domain.scan;

public enum ScanBehaviorType {

    FULL_SCAN, ONLY_MAP
}
