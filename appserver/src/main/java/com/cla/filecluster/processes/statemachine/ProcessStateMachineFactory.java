package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import org.springframework.statemachine.StateMachine;

/**
 * According to process config, allocate a proper state machine.
 * config includes: scan phases, timeout, retry policy...
 *
 * @author nadav
 */
public interface ProcessStateMachineFactory<P extends Process> {

    StateMachine<Phase,EventType> newStateMachine(P process);
}
