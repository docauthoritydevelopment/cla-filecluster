package com.cla.filecluster.processes.domain.scan;

import com.cla.filecluster.processes.domain.Process;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


@Entity
@DiscriminatorValue(value = "SCAN")
public class ScanProcess extends Process<ScanProcessDefinition> {


}
