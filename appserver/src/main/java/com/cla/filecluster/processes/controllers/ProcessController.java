package com.cla.filecluster.processes.controllers;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.ProcessDefinition;
import com.cla.filecluster.processes.domain.ProcessKeys;
import com.cla.filecluster.processes.domain.scan.ScanProcess;
import com.cla.filecluster.processes.domain.scan.ScanProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * APIs for managing DA processes.
 * <ul>
 * <li>Receives a process config, creates & starts a proper state-machine (SM).
 * <br/><pre>{@link ProcessDefinition} describes States, transitions, onError, onTimeout handing.</pre>
 * </li>
 * <li>Process lifecycle operations (pause/resume/stop) on one or more processes</li>
 * <li>Querying specific process state</li>
 * <li>Querying all processes state</li>
 * </ul>
 *
 * @author nadav
 */
@RestController
@RequestMapping("/api/processes")
public class ProcessController {

    @Autowired
    private EventBus eventBus;

    @RequestMapping(value = "/scan/{rfId}/start", method = RequestMethod.POST)
    public void startScan(@PathVariable("rfId") long rootFolderId, @RequestBody ScanProcessDefinition scanProcessDefinition) {
        ScanProcess scanProcess = new ScanProcess();
        scanProcess.setProcessKey(ProcessKeys.scanKey(rootFolderId));
        scanProcess.setProcessDefinition(scanProcessDefinition);
        eventBus.broadcast(Topics.PROCESSES_STATE_MACHINES, Event.builder()
                .withEventType(EventType.SCAN_START)
                .withEntry(EventKeys.PROCESS, scanProcess)
                .build());
    }

    @RequestMapping(value = "/scan/{rfId}/stop", method = RequestMethod.POST)
    public void stopScan(@PathVariable("rfId") long rootFolderId) {
        eventBus.broadcast(Topics.PROCESSES_STATE_MACHINES, Event.builder()
                .withEventType(EventType.SCAN_STOP)
                .withEntry(EventKeys.PROCESS_KEY, ProcessKeys.scanKey(rootFolderId))
                .build());
    }

    @RequestMapping(value = "/scan/{rfId}/pause", method = RequestMethod.POST)
    public void pauseScan(@PathVariable("rfId") long rootFolderId) {
        eventBus.broadcast(Topics.PROCESSES_STATE_MACHINES, Event.builder()
                .withEventType(EventType.SCAN_PAUSE)
                .withEntry(EventKeys.PROCESS_KEY, ProcessKeys.scanKey(rootFolderId))
                .build());
    }

    @RequestMapping(value = "/scan/{rfId}/resume", method = RequestMethod.POST)
    public void resumeScan(@PathVariable("rfId") long rootFolderId) {
        eventBus.broadcast(Topics.PROCESSES_STATE_MACHINES, Event.builder()
                .withEventType(EventType.SCAN_RESUME)
                .withEntry(EventKeys.PROCESS_KEY, ProcessKeys.scanKey(rootFolderId))
                .build());
    }
}
