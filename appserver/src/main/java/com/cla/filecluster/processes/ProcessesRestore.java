package com.cla.filecluster.processes;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.processes.repository.ProcessRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProcessesRestore {

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private EventBus eventBus;

    private static final Logger logger = LoggerFactory.getLogger(ProcessesRestore.class);

    @Autowired
    private ApplicationContext ctx;

    @EventListener
    @AutoAuthenticate
    public void restoreScanProcesses(ContextRefreshedEvent event) {
        Optional.ofNullable(processRepository.findAllProcessKeys())
                .ifPresent(this::restoreProcesses);
    }

    private void restoreProcesses(List<String> processKeys) {
        logger.info("Restoring {} processes", processKeys.size());
        processKeys.forEach(this::restoreProcess);
    }

    private void restoreProcess(Object processKey) {
        logger.debug("Restoring process (key: {})", processKey);
        eventBus.broadcast(Topics.PROCESSES_STATE_MACHINES,
                Event.builder()
                        .withEventType(EventType.SCAN_RESTORE)
                        .withEntry(EventKeys.PROCESS_KEY, processKey)
                        .build());
    }
}
