package com.cla.filecluster.processes;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.vertx.VertxEventBus;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A configuration class to event bus, jobs thread pools
 *
 * @author nadav
 */
@Configuration
public class ProcessesAppConfig {

    @Value("${event-bus.consumers.concurrency:15}")
    private int consumersConcurrency;

    @Value("${event-bus.sendTimeout:10000}")
    private long sendTimeout;

    @Bean
    public EventBus eventBus() {
        final ExecutorService consumersExecutor = Executors.newFixedThreadPool(
                consumersConcurrency,
                new ThreadFactoryBuilder().setNameFormat("eb-consumer-%d").build());
        return new VertxEventBus(sendTimeout, consumersExecutor);
    }
}
