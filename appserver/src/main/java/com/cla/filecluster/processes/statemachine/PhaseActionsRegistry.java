package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.ProcessConfig;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.statemachine.action.Action;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PhaseActionsRegistry<PH extends Phase> {

    private final BeanFactory beanFactory;

    private Map<PH, Class<? extends Action<PH, EventType>>> typeRegistry = new HashMap<>();

    private Map<String, Map<PH, Action<PH, EventType>>> registry = new HashMap<>();

    public PhaseActionsRegistry(BeanFactory beanFactory, Map<PH, Class<? extends Action<PH, EventType>>> typeRegistry) {
        this.beanFactory = beanFactory;
        this.typeRegistry = typeRegistry;
    }

    public Action<PH, EventType> registerAction(String machineId, PH phase, ProcessConfig processConfig) {
        Map<PH, Action<PH, EventType>> scanPhaseActionMap = registry.get(machineId);
        if (scanPhaseActionMap == null) {
            scanPhaseActionMap = new HashMap<>();
            registry.put(machineId, scanPhaseActionMap);
        }
        Action<PH, EventType> action = beanFactory.getBean(typeRegistry.get(phase), machineId, processConfig);
        scanPhaseActionMap.put(phase, action);
        return action;
    }


    public Action<PH, EventType> getAction(String machineId, PH phase) {
        return Optional.ofNullable(registry.get(machineId))
                .map(scanPhaseActionMap -> scanPhaseActionMap.get(phase))
                .orElse(null);
    }

}
