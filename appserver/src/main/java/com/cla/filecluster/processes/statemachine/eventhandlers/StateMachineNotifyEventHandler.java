package com.cla.filecluster.processes.statemachine.eventhandlers;

import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.eventbus.events.handler.AbstractEventHandler;
import com.cla.eventbus.events.handler.ErrorType;
import com.cla.eventbus.events.handler.ValidationResult;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.repository.ProcessRepository;
import com.cla.filecluster.processes.statemachine.ProcessStateMachineRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;

@Component
public class StateMachineNotifyEventHandler extends AbstractEventHandler {

    @Autowired
    private ProcessStateMachineRegistry processStateMachineRegistry;

    @Autowired
    private ProcessRepository processRepository;

    @Override
    protected ValidationResult validateEvent(Event event) {
        final Context context = event.getContext();
        return context.containsKey(EventKeys.PROCESS_KEY) ?
                ValidationResult.Ok() : ValidationResult.error(ErrorType.NO_PROCESS_KEY);
    }

    @Override
    protected void handleEvent(Event event) {
        final String processKey = event.getContext().get(EventKeys.PROCESS_KEY, String.class);
        StateMachine<? extends Phase, EventType> sm = processStateMachineRegistry.getStateMachine(processKey);
        if (sm == null) {
            broadcastError(event, ErrorType.NO_MATCHING_PROCESS_STATE_MACHINE);
            return;
        }
        sm.sendEvent(event.getEventType());
    }
}
