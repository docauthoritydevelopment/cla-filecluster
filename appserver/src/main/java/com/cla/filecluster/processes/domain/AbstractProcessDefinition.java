package com.cla.filecluster.processes.domain;


public abstract class AbstractProcessDefinition<B extends ProcessBehavior,C extends ProcessConfig> implements ProcessDefinition<B,C> {

    private B behavior;

    private C config;

    public AbstractProcessDefinition() {
    }


    @Override
    public B getBehavior() {
        return behavior;
    }

    public void setBehavior(B behavior) {
        this.behavior = behavior;
    }

    @Override
    public C getConfig() {
        return config;
    }

    public void setConfig(C config) {
        this.config = config;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProcessDefinition<?, ?> that = (AbstractProcessDefinition<?, ?>) o;

        if (behavior != null ? !behavior.equals(that.behavior) : that.behavior != null) return false;
        return config != null ? config.equals(that.config) : that.config == null;
    }

    @Override
    public int hashCode() {
        int result = behavior != null ? behavior.hashCode() : 0;
        result = 31 * result + (config != null ? config.hashCode() : 0);
        return result;
    }
}
