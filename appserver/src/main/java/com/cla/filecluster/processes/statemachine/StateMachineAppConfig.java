package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import com.cla.filecluster.processes.domain.scan.ScanPhase;
import com.cla.filecluster.processes.domain.scan.ScanProcess;
import com.cla.filecluster.processes.statemachine.scan.ScanProcessStateMachineFactory;
import com.cla.filecluster.processes.statemachine.scan.actions.*;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.data.jpa.JpaPersistingStateMachineInterceptor;
import org.springframework.statemachine.data.jpa.JpaRepositoryStateMachinePersist;
import org.springframework.statemachine.data.jpa.JpaStateMachineRepository;
import org.springframework.statemachine.kryo.KryoStateMachineSerialisationService;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

@Configuration
public class StateMachineAppConfig {

    @Bean
    public JpaRepositoryStateMachinePersist<Phase, EventType> stateMachinePersist(
            JpaStateMachineRepository jpaStateMachineRepository) {
        return new JpaRepositoryStateMachinePersist(jpaStateMachineRepository, new KryoStateMachineSerialisationService<ScanPhase, EventType>());
    }

    @Bean
    public StateMachineRuntimePersister<Phase, EventType, String> stateMachineRuntimePersister(
            JpaRepositoryStateMachinePersist<Phase, EventType> stateMachinePersist) {
        return new JpaPersistingStateMachineInterceptor(stateMachinePersist);
    }

    @Bean
    public StateMachinePersister<Phase, EventType, String> stateMachinePersister(StateMachineRuntimePersister<Phase, EventType, String> stateMachineRuntimePersister) {
        return new DefaultStateMachinePersister<Phase, EventType, String>(stateMachineRuntimePersister);
    }

    @Bean
    public PhaseActionsRegistry phaseActionsRegistry(BeanFactory beanFactory) {
        Map<Phase, Class<? extends Action<? extends Phase, EventType>>> phaseToActionTypeMap = new HashMap<>();
        phaseToActionTypeMap.put(ScanPhase.MAP, MapAction.class);
        phaseToActionTypeMap.put(ScanPhase.MAP_FINALIZE, MapFinalizeAction.class);
        phaseToActionTypeMap.put(ScanPhase.INGEST, IngestAction.class);
        phaseToActionTypeMap.put(ScanPhase.INGEST_FINALIZE, IngestFinalizeAction.class);
        phaseToActionTypeMap.put(ScanPhase.ANALYZE, AnalyzeAction.class);
        phaseToActionTypeMap.put(ScanPhase.ANALYZE_FINALIZE, AnalyzeFinalizeAction.class);
        return new PhaseActionsRegistry(beanFactory, phaseToActionTypeMap);
    }

    @Bean
    public ProcessStateMachineFactoryProvider processStateMachineFactoryProvider(ScanProcessStateMachineFactory scanProcessStateMachineFactory) {
        Map<Class<? extends Process>, ProcessStateMachineFactory> factoriesMap = new HashMap();
        factoriesMap.put(ScanProcess.class, scanProcessStateMachineFactory);
        return new ProcessStateMachineFactoryProvider(factoriesMap);
    }

    @Bean
    public TaskExecutor stateMachineTaskExecutor() {
        return new ConcurrentTaskExecutor(
                Executors.newFixedThreadPool(15, new ThreadFactoryBuilder().setNameFormat("sm-%d").build()));
    }
}
