package com.cla.filecluster.processes.feeds;

import org.springframework.stereotype.Component;

/**
 * Stream media processor messages (mapping, ingest...) to registered subscribers.
 * Provide timeout, buffer, retry & overall backpressure policy
 *
 * @author nadav
 */
@Component
public class MediaProcessorFeed {
}
