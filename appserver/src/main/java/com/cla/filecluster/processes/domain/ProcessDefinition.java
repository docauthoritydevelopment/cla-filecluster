package com.cla.filecluster.processes.domain;

import com.cla.filecluster.processes.domain.scan.ScanProcessDefinition;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@type")
@JsonSubTypes({@JsonSubTypes.Type(value = ScanProcessDefinition.class, name = "scan")})
public interface ProcessDefinition<B extends ProcessBehavior, C extends ProcessConfig> {


    B getBehavior();

    C getConfig();

}
