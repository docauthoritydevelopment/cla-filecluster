package com.cla.filecluster.processes.statemachine.scan.builders;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.ProcessConfig;
import com.cla.filecluster.processes.domain.scan.ScanProcess;
import com.cla.filecluster.processes.statemachine.AbstractProcessStateMachineBuilder;
import com.cla.filecluster.processes.statemachine.scan.actions.SafeExitAction;
import org.springframework.statemachine.action.Action;

public abstract class AbstractScanProcessStateMachineBuilder extends AbstractProcessStateMachineBuilder<ScanProcess> {

    @Override
    protected Action<Phase, EventType> getSafeExitAction(String processKey, ProcessConfig config) {
        return beanFactory.getBean(SafeExitAction.class, processKey, config);
    }

}
