package com.cla.filecluster.processes.feeds;

import org.springframework.stereotype.Component;

/**
 * Stream tasks to registered subscribers.
 * Provide timeout, buffer, retry & overall backpressure policy
 *
 * @author nadav
 */
@Component
public class TasksFeed {
}
