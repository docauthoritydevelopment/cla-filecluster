package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import com.cla.filecluster.processes.domain.ProcessConfig;
import com.cla.filecluster.processes.domain.ProcessDefinition;
import com.cla.filecluster.processes.domain.scan.ScanPhase;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.configurers.ExternalTransitionConfigurer;
import org.springframework.statemachine.config.configurers.StateConfigurer;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;
import org.springframework.statemachine.state.HistoryPseudoState;
import org.springframework.statemachine.state.PseudoState;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public abstract class AbstractProcessStateMachineBuilder<P extends Process> implements ProcessStateMachineBuilder<P> {


    @Autowired
    protected TaskExecutor stateMachineTaskExecutor;

    @Autowired
    protected EventBus eventBus;

    @Autowired
    private StateMachineRuntimePersister<Phase, EventType, String> stateMachineRuntimePersister;

    @Autowired
    protected BeanFactory beanFactory;

    @Autowired
    protected PhaseActionsRegistry phaseActionsRegistry;

    @Override
    public StateMachine<Phase, EventType> build(P process) {
        String processKey = process.getProcessKey();
        ProcessDefinition processDefinition = process.getProcessDefinition();
        StateMachineBuilder.Builder<Phase, EventType> smBuilder = org.springframework.statemachine.config.StateMachineBuilder.builder();
        try {
            // Configure defaults:  machineId, task executor, persistency tracking states
            smBuilder.configureConfiguration()
                    .withConfiguration()
                    .machineId(processKey)
                    .taskExecutor(stateMachineTaskExecutor)
                    .listener(new StateMachineTracker<>(eventBus, processKey))
                    .and()
                    .withPersistence()
                    .runtimePersister(stateMachineRuntimePersister);


            configureStates(processKey, smBuilder, processDefinition);

            // Call specific scan implementation to configure internal transitions
            configureTransitions(prepareBaseTransitionConfigurer(smBuilder), process);

            return smBuilder.build();
        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed to build state machine for scan %s", process), e);
        }
    }

    private StateConfigurer<Phase, EventType> configureStates(String processKey,
                                                              StateMachineBuilder.Builder<Phase, EventType> smBuilder,
                                                              ProcessDefinition processDefinition) throws Exception {
        StateConfigurer<Phase, EventType> stateConfigurer = smBuilder.configureStates()
                .withStates()
                .initial(Phase.PROCESSING)
                .state(Phase.PAUSED)
                .stateExit(Phase.PROCESSING,
                        getSafeExitAction(processKey,processDefinition.getConfig()))
                .end(Phase.FINISHED)
                .end(Phase.STOPPED)
                .and()
                .withStates()
                .parent(Phase.PROCESSING)
                .initial(initialPhase());

        processPhases().forEach(phase -> stateConfigurer.stateDo(
                phase,
                phaseActionsRegistry.registerAction(processKey, phase, processDefinition.getConfig())));
        stateConfigurer.state(Phase.HISTORY);
        stateConfigurer.history(Phase.HISTORY, StateConfigurer.History.SHALLOW);
        return stateConfigurer;


    }

    protected abstract Action<Phase, EventType> getSafeExitAction(String processKey,ProcessConfig config);

    protected abstract List<? extends Phase> processPhases();

    protected abstract Phase initialPhase();

    protected abstract void configureTransitions(
            ExternalTransitionConfigurer<Phase, EventType> transitionConfigurer,
            P process) throws Exception;

    private ExternalTransitionConfigurer<Phase, EventType> prepareBaseTransitionConfigurer(StateMachineBuilder.Builder<Phase, EventType> smBuilder) throws Exception {
        return smBuilder.configureTransitions().withExternal()
                .source(Phase.PROCESSING).target(Phase.PAUSED)
                .event(EventType.SCAN_PAUSE)
                .and()
                .withExternal()
                .source(Phase.PAUSED).target(Phase.HISTORY)
                .event(EventType.SCAN_RESUME)
                .action(this::transitionToDirectState)
                .and()
                .withExternal()
                .source(Phase.PAUSED).target(ScanPhase.STOPPED)
                .event(EventType.SCAN_STOP)
                .and()
                .withExternal()
                .source(Phase.PROCESSING).target(Phase.STOPPED)
                .event(EventType.SCAN_STOP)
                .and()
                .withExternal();

    }

    private void transitionToDirectState(StateContext<Phase, EventType> context) {
        PseudoState<Phase, EventType> pseudoState = context.getTransition().getTarget().getPseudoState();
        if (pseudoState instanceof HistoryPseudoState) {
            if (((HistoryPseudoState) pseudoState).getState() != null) {
                context.getStateMachine().getStateMachineAccessor().doWithAllRegions(function -> function.setInitialEnabled(false));
            }
        } else {
            context.getStateMachine().getStateMachineAccessor().doWithAllRegions(function -> function.setInitialEnabled(true));
        }
    }


}
