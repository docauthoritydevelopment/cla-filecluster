package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import org.springframework.statemachine.StateMachine;

public interface ProcessStateMachineBuilder<P extends Process> {

    StateMachine<Phase, EventType> build(P process);
}
