package com.cla.filecluster.processes.domain.scan;

import com.cla.filecluster.processes.domain.ProcessConfig;

public class ScanProcessConfig implements ProcessConfig {

    private String rootFolderPath;

    public void setRootFolderPath(String rootFolderPath) {
        this.rootFolderPath = rootFolderPath;
    }

    public String getRootFolderPath() {
        return rootFolderPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ScanProcessConfig that = (ScanProcessConfig) o;

        return rootFolderPath != null ? rootFolderPath.equals(that.rootFolderPath) : that.rootFolderPath == null;
    }

    @Override
    public int hashCode() {
        return rootFolderPath != null ? rootFolderPath.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "ScanProcessConfig{" +
                "rootFolderPath='" + rootFolderPath + '\'' +
                '}';
    }
}
