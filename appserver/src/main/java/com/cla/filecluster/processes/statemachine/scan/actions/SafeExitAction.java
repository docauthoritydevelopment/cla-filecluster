package com.cla.filecluster.processes.statemachine.scan.actions;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.scan.ScanProcessConfig;
import com.cla.filecluster.processes.statemachine.ProcessAction;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.statemachine.StateContext;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Lazy
public class SafeExitAction extends ProcessAction<Phase, ScanProcessConfig> {

    public SafeExitAction() {
    }


    public SafeExitAction(String processKey, ScanProcessConfig scanProcessConfig) {
        super(processKey, scanProcessConfig);
    }

    @Override
    public void execute(StateContext<Phase, EventType> context) {

    }
}