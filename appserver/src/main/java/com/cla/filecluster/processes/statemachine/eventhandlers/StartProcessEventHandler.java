package com.cla.filecluster.processes.statemachine.eventhandlers;

import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.eventbus.events.handler.AbstractEventHandler;
import com.cla.eventbus.events.handler.ErrorType;
import com.cla.eventbus.events.handler.ErrorUtils;
import com.cla.eventbus.events.handler.ValidationResult;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import com.cla.filecluster.processes.repository.ProcessRepository;
import com.cla.filecluster.processes.statemachine.ProcessStateMachineRegistration;
import com.cla.filecluster.processes.statemachine.ProcessStateMachineRegistry;
import com.cla.filecluster.processes.statemachine.scan.ScanProcessStateMachineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class StartProcessEventHandler extends AbstractEventHandler {

    public static final String FAILED_TO_START_SCAN_MSG_TEMPLATE = "Failed start scan process for %s";
    public static final String FAILED_TO_START_SCAN_PROCESS_BROADCAST_ERROR_MSG = FAILED_TO_START_SCAN_MSG_TEMPLATE + ", reason: %s";

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private ScanProcessStateMachineFactory scanStateMachineBuilderFactory;

    @Autowired
    private ProcessStateMachineRegistry processStateMachineRegistry;

    @Override
    protected ValidationResult validateEvent(Event event) {
        return event.getContext().containsKey(EventKeys.PROCESS) ?
                ValidationResult.Ok() : ValidationResult.error(ErrorType.NO_PROCESS);
    }

    @Override
    @Transactional
    protected void handleEvent(Event event) {
        Process process = event.getContext().get(EventKeys.PROCESS, Process.class);
        try {
            boolean reset = false;
            ProcessStateMachineRegistration processStateMachineRegistration = processStateMachineRegistry.registerStateMachine(process);
            synchronized (processStateMachineRegistration) { // Not to allow multiple event try to start the state machine at once
                StateMachine<? extends Phase, EventType> stateMachine = processStateMachineRegistration.getStateMachine();
                if (stateMachine.getState() == null) { // Not yet started
                    stateMachine.start();
                } else if (stateMachine.isComplete()) { // Means we can restart it
                    stateMachine.stop();
                    // We set the reset flag to true in case of a different process definition
                    if (!process.equals(processStateMachineRegistration.getProcess())) {
                        reset = true;
                    } else {
                        stateMachine.start();
                    }
                } else {
                    broadcastError(event, ErrorType.PROCESS_STATE_MACHINE_ALREADY_STARTED);
                }
            }
            if (reset) { // Means we need to replace process with new one
                processStateMachineRegistration = processStateMachineRegistry
                        .resetRegistration(process);
                synchronized (processStateMachineRegistration) {
                    StateMachine<Phase, EventType> stateMachine = processStateMachineRegistration.getStateMachine();
                    stateMachine.start();
                }

            }
        } catch (Exception e) {
            logger.error(String.format(FAILED_TO_START_SCAN_MSG_TEMPLATE, event), e);
            ErrorUtils.broadcastError(eventBus,
                    ErrorType.START_PROCESS_ERROR,
                    String.format(FAILED_TO_START_SCAN_PROCESS_BROADCAST_ERROR_MSG, event, e.getMessage()));
        }
    }

}
