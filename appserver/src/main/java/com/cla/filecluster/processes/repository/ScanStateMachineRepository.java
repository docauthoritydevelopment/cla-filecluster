package com.cla.filecluster.processes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.statemachine.data.jpa.JpaRepositoryStateMachine;

import java.util.List;

public interface ScanStateMachineRepository extends JpaRepository<JpaRepositoryStateMachine, String> {

    @Query(nativeQuery = true,
            value = "select sm.machine_id From state_machine sm")
    List<String> getAllStateMachineIds();
}