package com.cla.filecluster.processes.repository;

import com.cla.filecluster.processes.domain.Process;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProcessRepository extends JpaRepository<Process, Long> {

    @Query("From Process pd where pd.processKey = :processKey")
    Process findByProcessKey(@Param("processKey") String processKey);

    @Query("select pd.processKey From Process pd")
    List<String> findAllProcessKeys();

    /*@Query("delete from Process pd where pd.processKey = :processKey")
    @Modifying
    <K> void deleteByProcessKey(@Param("processKey") K processKey);*/
}