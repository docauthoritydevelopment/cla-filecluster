package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import org.springframework.statemachine.StateMachine;

public class ProcessStateMachineRegistration {

    private final Process process;
    private final StateMachine<Phase, EventType> stateMachine;

    public ProcessStateMachineRegistration(Process process, StateMachine<Phase, EventType> stateMachine) {
        this.process = process;
        this.stateMachine = stateMachine;
    }

    public Process getProcess() {
        return process;
    }

    public StateMachine<Phase, EventType> getStateMachine() {
        return stateMachine;
    }

    @Override
    public String toString() {
        return "ProcessStateMachineRegistration{" +
                "process=" + process +
                ", stateMachine=" + stateMachine +
                '}';
    }
}
