package com.cla.filecluster.processes.statemachine.eventhandlers;

import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.eventbus.events.handler.AbstractEventHandler;
import com.cla.eventbus.events.handler.ErrorType;
import com.cla.eventbus.events.handler.ErrorUtils;
import com.cla.eventbus.events.handler.ValidationResult;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import com.cla.filecluster.processes.repository.ProcessRepository;
import com.cla.filecluster.processes.statemachine.PhaseActionsRegistry;
import com.cla.filecluster.processes.statemachine.ProcessStateMachineRegistration;
import com.cla.filecluster.processes.statemachine.ProcessStateMachineRegistry;
import com.cla.filecluster.processes.statemachine.scan.ScanProcessStateMachineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class RestoreProcessEventHandler extends AbstractEventHandler {

    private static final String FAILED_TO_RESTORE_SCAN_ERROR_MSG_TEMPLATE = "Failed to restore scan for %s";
    private static final String FAILED_TO_RESTORE_PROCESS_BROADCAST_ERROR_MSG = FAILED_TO_RESTORE_SCAN_ERROR_MSG_TEMPLATE + ", reason: %s";

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private ScanProcessStateMachineFactory scanStateMachineBuilderFactory;

    @Autowired
    private StateMachinePersist<Phase, EventType, String> stateMachinePersist;

    @Autowired
    private ProcessStateMachineRegistry processStateMachineRegistry;

    @Autowired
    private TaskExecutor stateMachineTaskExecutor;

    @Autowired
    protected PhaseActionsRegistry phaseActionsRegistry;

    @Override
    protected ValidationResult validateEvent(Event event) {
        final Context context = event.getContext();
        return context.containsKey(EventKeys.PROCESS_KEY) ?
                ValidationResult.Ok() : ValidationResult.error(ErrorType.NO_PROCESS_KEY);
    }

    @Override
    protected void handleEvent(Event event) {
        final String processKey = event.getContext().get(EventKeys.PROCESS_KEY, String.class);
        final Process process = processRepository.findByProcessKey(processKey);
        if (process == null) {
            broadcastError(event, ErrorType.NO_EXISTING_PROCESS);
        }

        restoreScanStateMachine(process, event);
    }

    private void restoreScanStateMachine(Process process, Event event) {
        try {
            ProcessStateMachineRegistration processStateMachineRegistration = processStateMachineRegistry.registerStateMachine(process);
            synchronized (processStateMachineRegistration) {
                StateMachine<Phase, EventType> stateMachine = processStateMachineRegistration.getStateMachine();
                // Read context from db
                StateMachineContext<Phase, EventType> stateMachineContext = stateMachinePersist.read(stateMachine.getId());

                if (stateMachineContext != null) { // case already stored in db
                    logger.trace("Restore process {} from db, state {}", process, stateMachineContext);
                    restoreFromContext(stateMachine, stateMachineContext);
                }

                // Only if state machine is not completed or never started then start it
                if (!stateMachine.isComplete() || stateMachine.getState() == null) {
                    logger.debug("Starting process {}", process);
                    stateMachine.start();
                }
            }
        } catch (Exception e) {
            logger.error(String.format(FAILED_TO_RESTORE_SCAN_ERROR_MSG_TEMPLATE, event), e);
            ErrorUtils.broadcastError(eventBus,
                    ErrorType.RESTORE_PROCESS_ERROR,
                    String.format(FAILED_TO_RESTORE_PROCESS_BROADCAST_ERROR_MSG, event, e.getMessage()));
        }
    }

    private void restoreFromContext(StateMachine<Phase, EventType> stateMachine, StateMachineContext<Phase, EventType> stateMachineContext) {
        Optional
                // Get current phase
                .ofNullable(stateMachineContext.getState())
                // get matching action for this state machine
                .map(scanPhase -> phaseActionsRegistry.getAction(stateMachine.getId(), scanPhase))
                // prepare new resume listener with action
                .map(action -> new ScanStateMachineResume(action))
                // add it to the state machine
                .ifPresent(stateMachine::addStateListener);

        // Reset state machine with context from db and start it
        stateMachine.stop();
        stateMachine.getStateMachineAccessor()
                .doWithAllRegions(access -> access.resetStateMachine(stateMachineContext));
    }


    private class ScanStateMachineResume<PH> extends StateMachineListenerAdapter<PH, EventType> {

        private final Action<PH, EventType> action;

        public ScanStateMachineResume(Action<PH, EventType> action) {
            this.action = action;
        }

        @Override
        public void stateContext(StateContext<PH, EventType> stateContext) {
            StateMachine<PH, EventType> stateMachine = stateContext.getStateMachine();
            stateMachine.removeStateListener(this);
            stateMachineTaskExecutor.execute(() -> action.execute(stateContext));
        }
    }
}
