package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.Process;
import com.cla.filecluster.processes.repository.ProcessRepository;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

@Component
public class ProcessStateMachineRegistry {

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    ProcessStateMachineFactoryProvider processStateMachineFactoryProvider;

    private static final Logger logger = LoggerFactory.getLogger(ProcessStateMachineRegistry.class);

    private final LoadingCache<ProcessWrapperKey, ProcessStateMachineRegistration> internalCache = CacheBuilder
            .newBuilder()
            .build(new ScanStateMachineCacheLoader());

    public StateMachine<? extends Phase, EventType> getStateMachine(String processKey) {
        return Optional.ofNullable(internalCache.getUnchecked(ProcessWrapperKey.idKey(processKey)))
                .map(r -> r.getStateMachine())
                .orElse(null);
    }

    @Transactional
    public ProcessStateMachineRegistration resetRegistration(Process process) throws ExecutionException {
        ProcessStateMachineRegistration processStateMachineRegistration = internalCache.get(ProcessWrapperKey.idKey(process.getProcessKey()));
        Long processId = processStateMachineRegistration.getProcess().getId();
        process.setId(processId);
        processRepository.save(process);
        String processKey = process.getProcessKey();
        internalCache.invalidate(ProcessWrapperKey.idKey(processKey));
        return registerStateMachine(process);
    }

    @Transactional
    public ProcessStateMachineRegistration registerStateMachine(Process processDefinition) throws ExecutionException {
        return internalCache.get(ProcessWrapperKey.deifnitionKey(processDefinition));
    }

    private static class ProcessWrapperKey {

        private final Process process;
        private final String processKey;

        public static ProcessWrapperKey idKey(String processKey) {
            return new ProcessWrapperKey(null, processKey);
        }

        public static ProcessWrapperKey deifnitionKey(Process process) {
            return new ProcessWrapperKey(process, process.getProcessKey());
        }

        private ProcessWrapperKey(Process process, String processKey) {
            this.process = process;
            this.processKey = processKey;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProcessWrapperKey that = (ProcessWrapperKey) o;

            return processKey != null ? processKey.equals(that.processKey) : that.processKey == null;
        }

        @Override
        public int hashCode() {
            return processKey != null ? processKey.hashCode() : 0;
        }
    }

    private final class ScanStateMachineCacheLoader extends CacheLoader<ProcessWrapperKey, ProcessStateMachineRegistration> {

        @Override
        public ProcessStateMachineRegistration load(ProcessWrapperKey key) throws Exception {
            logger.debug("Registering state machine for process ({})", key.processKey);
            Process processForMachine = key.process;
            Process process = processRepository.findByProcessKey(key.processKey);
            if (process != null) {
                logger.trace("Process ({}) already exists in db, allocating state machine for it");
                processForMachine = process;
            }
            StateMachine stateMachine = processStateMachineFactoryProvider
                    .getFactory(processForMachine)
                    .newStateMachine(processForMachine);
            if (process == null) {
                logger.debug("New process ({}), allocating state machine for it", key.processKey);
                long now = DateTime.now().getMillis();
                key.process.setDateCreated(now);
                key.process.setDateModified(now);
                processForMachine = processRepository.save(key.process);
            }

            return new ProcessStateMachineRegistration(processForMachine, stateMachine);
        }
    }


}
