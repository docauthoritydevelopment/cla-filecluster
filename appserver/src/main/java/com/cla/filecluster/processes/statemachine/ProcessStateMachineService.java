package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.eventbus.events.handler.EventHandler;
import com.cla.filecluster.processes.statemachine.eventhandlers.RestoreProcessEventHandler;
import com.cla.filecluster.processes.statemachine.eventhandlers.StartProcessEventHandler;
import com.cla.filecluster.processes.statemachine.eventhandlers.StateMachineNotifyEventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.function.Consumer;

/**
 * Responsible for allocate, control and query state machines
 */
@Service
public class ProcessStateMachineService implements Consumer<Event> {

    private static final Logger logger = LoggerFactory.getLogger(ProcessStateMachineService.class);

    @Autowired
    private StartProcessEventHandler startProcessEventHandler;

    @Autowired
    private StateMachineNotifyEventHandler stateMachineNotifyEventHandler;

    @Autowired
    private RestoreProcessEventHandler restoreProcessEventHandler;

    @Autowired
    private EventBus eventBus;

    @PostConstruct
    private void init() {
        eventBus.subscribe(Topics.PROCESSES_STATE_MACHINES, this);
    }

    @Override
    public void accept(Event event) {
        EventHandler stateMachineEventHandler = getMatchingScanEventHandler(event.getEventType());
        if (stateMachineEventHandler == null) {
            logger.debug("No handler registered for event {}", event);
            return;
        }
        logger.trace("Activating handler {} for event {}", stateMachineEventHandler, event);
        stateMachineEventHandler.handle(event);
    }

    private EventHandler getMatchingScanEventHandler(EventType eventType) {
        switch (eventType) {
            case SCAN_START:
                return startProcessEventHandler;
            case SCAN_RESTORE:
                return restoreProcessEventHandler;
            default:
                return stateMachineNotifyEventHandler;
        }

    }
}

