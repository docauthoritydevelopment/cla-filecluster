package com.cla.filecluster.processes.domain;

import com.cla.filecluster.domain.entity.BasicEntity;
import org.hibernate.annotations.Type;

import javax.persistence.*;


@Entity
@Table(name = "processes",
        indexes = {
                @Index(unique = true, name = "process_key_idx", columnList = "process_key")
        })
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class Process<D extends ProcessDefinition> extends BasicEntity {

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private D processDefinition;

    @Column(name = "process_key")
    private String processKey;

    public D getProcessDefinition() {
        return processDefinition;
    }

    public void setProcessDefinition(D processDefinition) {
        this.processDefinition = processDefinition;
    }

    public String getProcessKey() {
        return processKey;
    }

    public void setProcessKey(String processKey) {
        this.processKey = processKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Process<?> process = (Process<?>) o;

        if (processDefinition != null ? !processDefinition.equals(process.processDefinition) : process.processDefinition != null)
            return false;
        return processKey != null ? processKey.equals(process.processKey) : process.processKey == null;
    }

    @Override
    public int hashCode() {
        int result = processDefinition != null ? processDefinition.hashCode() : 0;
        result = 31 * result + (processKey != null ? processKey.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Process{" +
                "processKey='" + processKey + '\'' +
                "} " + super.toString();
    }
}
