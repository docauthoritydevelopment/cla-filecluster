package com.cla.filecluster.processes.statemachine;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.processes.domain.Phase;
import com.cla.filecluster.processes.domain.ProcessConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.action.Action;

public abstract class ProcessAction<P extends Phase,C extends ProcessConfig> implements Action<P, EventType> {

    protected String processKey;

    protected final C processConfig;

    @Autowired
    protected EventBus eventBus;

    public ProcessAction() {
        this(null, null);
    }

    public ProcessAction(String processKey, C processConfig) {
        this.processConfig = processConfig;
        this.processKey = processKey;

    }

    protected Event scanProcessEvent(EventType eventType) {
        return Event.builder()
                .withEventType(eventType)
                .withEntry(EventKeys.PROCESS_KEY, processKey)
                .build();
    }

    protected void broadCastScanProcessEvent(EventType eventType) {
        eventBus.broadcast(Topics.PROCESSES_STATE_MACHINES, scanProcessEvent(eventType));
    }
}
