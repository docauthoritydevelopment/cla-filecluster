package com.cla.filecluster.mediaproc.map;

import java.util.Objects;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
public class FileToDel {

    private Long fileId;
    private Long runId;
    private String analysisGroupId;
    private String userGroupId;

    public FileToDel(Long fileId, Long runId, String analysisGroupId, String userGroupId) {
        this.fileId = fileId;
        this.runId = runId;
        this.analysisGroupId = analysisGroupId;
        this.userGroupId = userGroupId;
    }

    public Long getFileId() {
        return fileId;
    }

    public Long getRunId() {
        return runId;
    }

    public String getAnalysisGroupId() {
        return analysisGroupId;
    }

    public String getUserGroupId() {
        return userGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileToDel fileToDel = (FileToDel) o;
        return Objects.equals(fileId, fileToDel.fileId) &&
                Objects.equals(runId, fileToDel.runId);
    }

    @Override
    public String toString() {
        return "FileToDel{" +
                "fileId=" + fileId +
                ", runId=" + runId +
                ", analysisGroupId='" + Optional.ofNullable(analysisGroupId).orElse("null") + '\'' +
                ", userGroupId='" + Optional.ofNullable(userGroupId).orElse("null") + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileId, runId);
    }
}
