package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Handle ingestion result that contains duplicate (already existing content)
 * This handler stops the chain if activated
 */
@Component
public class DuplicateContentHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(DuplicateContentHandler.class);

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return context.getIngestResultMessage().getDuplicateContentFound();
    }

    @Override
    public boolean handle(IngestResultContext context) {
        IngestResultMessage<?> ingestResult = context.getIngestResultMessage();
        if (ingestResult.getContentMetadataId() == null) {
            throw new RuntimeException("Illegal IngestResult message. Null contentMetadataId on duplicateContent");
        }

        logger.trace("Duplicate content found for fileId {} on content {}", ingestResult.getClaFileId(),
                ingestResult.getContentMetadataId());

        filesDataPersistenceService.saveDuplicateContentFile(ingestResult, context.getIngestBatchStore());
        return false;
    }
}
