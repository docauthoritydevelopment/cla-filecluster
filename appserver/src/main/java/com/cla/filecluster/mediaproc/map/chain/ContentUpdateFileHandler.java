package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.cla.common.domain.dto.messages.MessagePayloadType.*;
import static com.cla.connector.domain.dto.file.DiffType.CONTENT_UPDATED;

/**
 * Handle content update
 * <p>
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class ContentUpdateFileHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(ContentUpdateFileHandler.class);

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private ScanDiffEventProvider scanDiffEventProvider;

    @Autowired
    private EventBus eventBus;

    @Autowired
    private DocStoreService docStoreService;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        ClaFilePropertiesDto payload = (ClaFilePropertiesDto) scanResultMessage.getPayload();
        RootFolder rootFolderCached = docStoreService.getRootFolderCached(scanResultMessage.getRootFolderId());
        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());

        if (!scanResultMessage.isPackagedScan() && oldFileEntity.getFileId() != null) {
            IngestTaskJsonProperties ingestTaskJsonProps = IngestTaskJsonProperties.forDiffType(CONTENT_UPDATED);
            mapServiceUtils.createIngestTask(null, payload.getFileName(), scanResultMessage.getRootFolderId(), payload.getType(),
                    mapContext.getBatchIdentifierNumber(), scanResultMessage.getRunContext(), scanResultMessage.getTaskId(),
                    oldFileEntity.getFileId(), ingestTaskJsonProps);
        }

        // update kvdb
        Long prevSize = oldFileEntity.getSize(); // take previous file size
        oldFileEntity.setSize(payload.getFileSize());
        oldFileEntity.setLastModified(payload.getModTimeMilli());
        oldFileEntity.setAclSignature(payload.getAclSignature());
        mapContext.setFileEntity(oldFileEntity);

        scanDiffEventProvider.generateScanDiffEvent(EventType.SCAN_DIFF_UPDATED, oldFileEntity.getFileId(), payload.getFileName());
        eventBus.broadcast(Topics.MAP, Event.builder()
                .withEventType(com.cla.eventbus.events.EventType.RUN_PROGRESS)
                .withEntry(EventKeys.RUN_ID, scanResultMessage.getRunContext())
                .withEntry(EventKeys.IS_A_FILE, oldFileEntity.isFile())
                .withEntry(EventKeys.FILE_CONTENT_UPDATE, 1)
                .withEntry(EventKeys.ROOT_FOLDER_ID, scanResultMessage.getRootFolderId())
                .withEntry(EventKeys.FILE_MEDIA_TYPE, rootFolderCached.getMediaType())
                .withEntry(EventKeys.FILE_DIFF_PAYLOAD, Pair.of(prevSize, payload.getFileSize()))
                .build());

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        MessagePayloadType payloadType = scanResultMessage.getPayloadType();
        List<MessagePayloadType> types = Lists.newArrayList(SCAN_CHANGES, SCAN_PST_ENTRY, SCAN, EXCHANGE365_ITEM_CHANGE);
        if (!types.contains(payloadType)) {
            logger.trace("payload type irrelevant {}, skip", mapContext);
            return false;
        }

        if (mapContext.getFstScan()) {
            logger.trace("first scan for file {}, skip", mapContext);
            return false;
        }

        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());
        boolean shouldHandle = (mapContext.getDiffType().equals(CONTENT_UPDATED) && oldFileEntity != null);
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
