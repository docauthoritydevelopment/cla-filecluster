package com.cla.filecluster.mediaproc;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.filetree.EntityCredentialsDto;
import com.cla.common.domain.dto.messages.LabelRequestType;
import com.cla.common.domain.dto.messages.LabelTaskParameters;
import com.cla.common.domain.dto.messages.LabelRequestMessage;
import com.cla.common.utils.CollectionsUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.filetree.EntityCredentialsService;
import com.cla.filecluster.service.label.LabelingService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.util.LocalFileUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;
import static java.util.stream.Collectors.toList;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
@Service
public class AsyncLabelingProducerService implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(AsyncLabelingProducerService.class);

    @Autowired
    private LabelingService labelingService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Autowired
    private EntityCredentialsService entityCredentialsService;

    @Value("${labeling.is-active:false}")
    private boolean shouldHandleLabelingRequests;

    @Value("${labeling.request-batch-size:1000}")
    private int batchSize;

    @Value("${job-manager.tasks.max-retries:3}")
    private int maxTaskRetries;

    @Value("${labeling.tasks.enqueued-timeout-sec:1200}")
    private int labelTaskTimeoutSec;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @Scheduled(initialDelayString = "${labeling.tasks-polling-init-delay-millis}", fixedRateString = "${labeling.tasks-polling-rate-millis}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @AutoAuthenticate
    public void processNextTasksGroup() {

        if (!isSchedulingActive) {
            return;
        }

        if (!shouldHandleLabelingRequests) {
            return;
        }

        int from = 0;
        int size = 0;

        long threshold = System.currentTimeMillis() - TimeUnit.SECONDS.toMillis(labelTaskTimeoutSec);

        List<MediaConnectionDetailsDto> labelingConns = mediaConnectionDetailsService.getMediaConnectionDetailsDtoByType(MediaType.MIP, false);
        Long labelingConnectionId = labelingConns == null || labelingConns.isEmpty() ? null : labelingConns.get(0).getId();
        MediaConnectionDetailsDto labelingConnection = labelingConnectionId == null ? null :
            mediaConnectionDetailsService.getMediaConnectionDetailsByConnId(
                    labelingConnectionId, true).orElse(null);

        do {
            try {
                Query query = Query.create().setRows(batchSize).setStart(from)
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.ACTIONS_EXPECTED, SolrOperator.PRESENT, null))
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.LABEL_PROCESS_DATE, SolrOperator.LT, threshold))
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"));
                List<SolrFileEntity> files = fileCatService.find(query.build());
                size = (files == null ? 0 : files.size());
                logger.trace("handle label file batch from {} size {}", from, size);
                if (size > 0) {
                    from += batchSize;
                    handleFilesBatch(files, labelingConnection);
                }
            } catch (Exception e) {
                logger.error("failed during labeling process for batch from {} size {}", from, size, e);
            }
        } while (size > 0);
    }

    private void handleFilesBatch(List<SolrFileEntity> files, MediaConnectionDetailsDto labelingConnection) {

        if (labelingConnection == null) {
            logger.trace("no media connections exists for microsoft labeling, do not try handling labeling requests");
            return;
        }

        List<Long> rootFolders = files.stream()
                .map(SolrFileEntity::getRootFolderId)
                .filter(Objects::nonNull)
                .distinct().collect(toList());
        List<RootFolder> rfs = docStoreService.getRootFoldersCached(rootFolders);
        Map<Long, RootFolder> rootFolderMap = rfs.stream()
                .collect(Collectors.toMap(RootFolder::getId, r -> r, CollectionsUtils::keyCollisionResolver));

        Map<Long, MediaConnectionDetailsDto> connectionDetails = Maps.newHashMap();
        rootFolders.forEach(rootFolder -> {
            Optional<MediaConnectionDetailsDto> connection = mediaConnectionDetailsService
                    .getMediaConnectionDetailsByRootFolderId(rootFolder, true);
            connection.ifPresent(detailsDto -> connectionDetails.put(rootFolder, detailsDto));
        });

        Map<Long, List<EntityCredentialsDto>> credentials = rootFolders.stream()
                .collect(Collectors.toMap(r -> r, r -> {
                    List<EntityCredentialsDto> cs = entityCredentialsService.getEntityCredentialsForRootFolderCached(r);
                    return (cs == null ? Lists.newArrayList() : cs);
                }, CollectionsUtils::keyCollisionResolver));

        List<SolrInputDocument> filesToUpdate = new ArrayList<>();
        files.forEach(file -> {
            SolrInputDocument doc = handleFileLabeling(file, rootFolderMap, connectionDetails, credentials, labelingConnection);
            if (doc != null) {
                filesToUpdate.add(doc);
            }
        });

        if (!filesToUpdate.isEmpty()) {
            fileCatService.saveAll(filesToUpdate);
            fileCatService.softCommitNoWaitFlush();
        }
    }

    private SolrInputDocument handleFileLabeling(SolrFileEntity file, Map<Long, RootFolder> rootFolderMap,
                                                 Map<Long, MediaConnectionDetailsDto> connectionDetails,
                                                 Map<Long, List<EntityCredentialsDto>> credentials,
                                                 MediaConnectionDetailsDto labelingConnection) {
        try {
            List<String> actions = new ArrayList<>();
            Set<String> handledActions = new HashSet<>(); // for handling duplicate actions expected

            file.getActionsExpected().forEach(actionStr -> {

                String[] actionParts = actionStr.split("["+LabelingService.SEPARATOR+"]");

                Integer retries = Integer.parseInt(actionParts[2]);

                if (retries + 1 < maxTaskRetries) { // if has more retries and run is done
                    String origin = actionParts[0];
                    String nativeId = actionParts[1];

                    String actionId = LabelingService.createActionExpected(origin, nativeId, "");
                    if (!handledActions.contains(actionId)) { // skip duplicate actions for file
                        handledActions.add(actionId);
                        Long rfId = file.getRootFolderId();
                        if (rfId == null || rootFolderMap.get(rfId) == null) {
                            logger.error("labeling failed - cannot find root folder for file {}", file.getFileId());
                        } else {
                            RootFolder rootFolder = rootFolderMap.get(rfId);
                            MediaConnectionDetailsDto conn = connectionDetails.get(rfId);
                            List<EntityCredentialsDto> creds = credentials.get(rfId);
                            LabelingVendor vendor = LabelingVendor.getByName(origin);
                            generateLabelRequestMessage(
                                    file, rootFolder, conn, creds, vendor, nativeId, labelingConnection);
                        }

                        // advance retries
                        actions.add(LabelingService.createActionExpected(origin, nativeId, String.valueOf(retries + 1)));
                    }
                }
            });

            AtomicUpdateBuilder builder = AtomicUpdateBuilder.create()
                    .setId(ID, file.getId())
                    .addModifier(LABEL_PROCESS_DATE, SolrOperator.SET, System.currentTimeMillis())
                    .addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                    ;

            if (actions.isEmpty()) {
                builder.addModifier(ACTIONS_EXPECTED, SolrOperator.SET, null);
            } else {
                builder.addModifier(ACTIONS_EXPECTED, SolrOperator.SET, actions);
            }
            return builder.build();
        } catch (Exception e) {
            logger.error("problem sending labeling request for file {}", file.getFileId(), e);
        }
        return null;
    }

    private void generateLabelRequestMessage(SolrFileEntity file, RootFolder rootFolder,
                                                            MediaConnectionDetailsDto conn, List<EntityCredentialsDto> creds,
                                                            LabelingVendor vendor, String nativeId,
                                                            MediaConnectionDetailsDto labelingConnection) {
        LabelTaskParameters labelMessagePayload = LabelTaskParameters.create()
            .setMediaConnectionDetailsDto(conn)
                .setFileId(file.getFileId())
                .setFilePath(LocalFileUtils.createFullPath(
                        rootFolder.getMediaType(), rootFolder.getRealPath(), file.getFullName()))
                .setFileType(FileType.valueOf(file.getType()))
                .setMediaEntityId(LocalFileUtils.createFullPath(
                        rootFolder.getMediaType(), rootFolder.getRealPath(), file.getMediaEntityId()))
                .setRootFolderId(file.getRootFolderId())
                .setLabelingConnection(labelingConnection)
                ;

        if (creds != null && !creds.isEmpty()) {
            EntityCredentialsDto dto = creds.iterator().next();
            labelMessagePayload
                    .setUsername(dto.getUsername())
                    .setPassword(dto.getPassword());
        }

        long customerDataCenterId = rootFolder.getCustomerDataCenter().getId();
        if (LabelingVendor.DocAuthority.equals(vendor)) { // write request
            LabelDto labelDto = labelingService.getLabelByIdCached(Long.parseLong(nativeId)).orElse(null);
            labelMessagePayload
                    .setRequestType(LabelRequestType.WRITE_LABEL)
                    .setLabelId(labelDto.getId());
            Map<LabelingVendor, String> nativeIds = labelingService.getNativeIdsForLabel(labelDto);
            nativeIds.forEach((writeVendor, labelNativeId) -> {
                labelMessagePayload
                        .setNativeId(labelNativeId)
                        .setLabelingVendor(writeVendor);
                LabelRequestMessage labelRequestMessage = new LabelRequestMessage(labelMessagePayload);
                remoteMediaProcessingService.issueLabelingRequest(customerDataCenterId, labelRequestMessage);
                logger.debug("send label write request for file {} vendor {} label {}", file.getFileId(), writeVendor, labelNativeId);
            });
        } else { // read request
            labelMessagePayload
                    .setRequestType(LabelRequestType.READ_LABELS)
                    .setNativeId(nativeId)
                    .setLabelingVendor(vendor);
            LabelRequestMessage labelRequestMessage = new LabelRequestMessage(labelMessagePayload);
            remoteMediaProcessingService.issueLabelingRequest(customerDataCenterId, labelRequestMessage);
            logger.debug("send label read request for file {} vendor {} label {}", file.getFileId(), vendor, nativeId);
        }
    }
}
