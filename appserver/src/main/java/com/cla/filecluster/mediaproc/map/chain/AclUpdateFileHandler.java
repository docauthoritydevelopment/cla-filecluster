package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.file.AclItemDto;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.cla.common.domain.dto.messages.MessagePayloadType.*;
import static com.cla.connector.domain.dto.file.DiffType.ACL_UPDATED;

/**
 * If acl changed and not send in map - set file to be ingested
 *
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class AclUpdateFileHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(AclUpdateFileHandler.class);

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private ScanDiffEventProvider scanDiffEventProvider;

    @Autowired
    private EventBus eventBus;


    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        ClaFilePropertiesDto payload = (ClaFilePropertiesDto)scanResultMessage.getPayload();
        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());
        if (!scanResultMessage.isPackagedScan()) {
            IngestTaskJsonProperties ingestTaskJsonProps = IngestTaskJsonProperties.forDiffType(ACL_UPDATED);
            mapServiceUtils.createIngestTask(null, payload.getFileName(), scanResultMessage.getRootFolderId(), payload.getType(),
                    mapContext.getBatchIdentifierNumber(), scanResultMessage.getRunContext(), scanResultMessage.getTaskId(),
                    oldFileEntity.getFileId(), ingestTaskJsonProps);
        }

        oldFileEntity.setAclSignature(payload.getAclSignature());
        mapContext.setFileEntity(oldFileEntity);

        scanDiffEventProvider.generateScanDiffEvent(EventType.SCAN_DIFF_ACL_UPDATED, oldFileEntity.getFileId(), payload.getFileName());

        eventBus.broadcast(Topics.MAP, Event.builder()
                .withEventType(com.cla.eventbus.events.EventType.RUN_PROGRESS)
                .withEntry(EventKeys.RUN_ID, scanResultMessage.getRunContext())
                .withEntry(EventKeys.FILE_METADATA_UPDATE, 1).build());

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        MessagePayloadType payloadType = scanResultMessage.getPayloadType();
        List<MessagePayloadType> types = Lists.newArrayList(SCAN_CHANGES, SCAN_PST_ENTRY, SCAN, EXCHANGE365_ITEM_CHANGE);
        if (!types.contains(payloadType)) {
            logger.trace("payload type irrelevant {}, skip", mapContext);
            return false;
        }

        if (mapContext.getFstScan()) {
            logger.trace("first scan for file {}, skip", mapContext);
            return false;
        }

        ClaFilePropertiesDto payload = (ClaFilePropertiesDto)scanResultMessage.getPayload();

        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());

        List<AclItemDto> aclItems = ModelIngestUtils.getAclItems(mapContext.getFileId(), payload);
        boolean shouldHandle = (mapContext.getDiffType().equals(ACL_UPDATED) && oldFileEntity != null && aclItems.isEmpty());
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
