package com.cla.filecluster.mediaproc;

/**
 * Object for ui use to know the current biz list run state
 *
 * Created by: yael
 * Created on: 11/5/2018
 */
public class BizListScheduleRunStatus {

    private Long lastRunTime;

    private Long nextRunTime;

    private Long everyInMillis;

    private Boolean schedulingFlagOn;

    public Long getLastRunTime() {
        return lastRunTime;
    }

    public void setLastRunTime(Long lastRunTime) {
        this.lastRunTime = lastRunTime;
    }

    public Long getNextRunTime() {
        return nextRunTime;
    }

    public void setNextRunTime(Long nextRunTime) {
        this.nextRunTime = nextRunTime;
    }

    public Long getEveryInMillis() {
        return everyInMillis;
    }

    public void setEveryInMillis(Long everyInMillis) {
        this.everyInMillis = everyInMillis;
    }

    public Boolean getSchedulingFlagOn() {
        return schedulingFlagOn;
    }

    public void setSchedulingFlagOn(Boolean schedulingFlagOn) {
        this.schedulingFlagOn = schedulingFlagOn;
    }
}
