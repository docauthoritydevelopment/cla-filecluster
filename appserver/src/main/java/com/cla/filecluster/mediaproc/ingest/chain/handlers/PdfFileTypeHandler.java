package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * PDF-only handler
 * Applies the correct type to the file object
 * TODO: see if this can be replaced by correct handling of the PDF file types
 */
@Component
public class PdfFileTypeHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(PdfFileTypeHandler.class);

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        SolrFileEntity file = context.getFile();
        return FileType.PDF.equals(file.getType()) || FileType.SCANNED_PDF.equals(file.getType());
    }

    @Override
    public boolean handle(IngestResultContext context) {
        SolrFileEntity file = context.getFile();
        IngestResultMessage<WordIngestResult> wordResultMessage = context.getWordResultMessage();
        WordIngestResult payload = wordResultMessage.getPayload();
        ClaFilePropertiesDto fileProperties = wordResultMessage.getFileProperties();

        if (payload.isScanned() && FileType.PDF.equals(file.getType())) {
            file.setType(FileType.SCANNED_PDF.toInt());
            fileProperties.setType(FileType.SCANNED_PDF);   // file type is later overridden by fileProperties' type.
            logger.debug("Set PDF file as SCANNED_PDF for {} {}", file.getId(), file.getFullName());
        } else if (!payload.isScanned() && FileType.SCANNED_PDF.equals(file.getType())) {
            file.setType(FileType.PDF.toInt());
            fileProperties.setType(FileType.PDF);
            logger.debug("Set SCANNED_PDF file as PDF for {} {}", file.getId(), file.getFullName());
        }
        return true;
    }

}
