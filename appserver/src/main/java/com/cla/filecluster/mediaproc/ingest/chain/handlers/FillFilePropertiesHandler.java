package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

/**
 * Copy properties from claFileProperties to file entity
 */
@Component
public class FillFilePropertiesHandler implements ChainLink<IngestResultContext> {
    private final static Logger logger = LoggerFactory.getLogger(FillFilePropertiesHandler.class);

    @Autowired
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return true;
    }

    @Value("${pst.email.parse-x500-addresses:true}")
    private boolean parseX500Addresses;

    @Value("${pst.email.x500.extract-non-domain-addresses:true}")
    private boolean extractNonDomainX500CN;

    @Override
    public boolean handle(IngestResultContext context) {
        SolrFileEntity file = context.getFile();
        ClaFilePropertiesDto claFileProperties = context.getIngestResultMessage().getFileProperties();

        filesDataPersistenceService.fillFileProperties(claFileProperties, file);
        if (claFileProperties instanceof PstEntryPropertiesDto) {
            ModelIngestUtils.copyPstFields(((PstEntryPropertiesDto) claFileProperties), file, parseX500Addresses, extractNonDomainX500CN);
            if (logger.isTraceEnabled()) {
                logger.trace("copyPstFields: sender address {} converted into {}. (parseX500Addresses={}, extractNonDomainX500CN={})",
                        ((PstEntryPropertiesDto) claFileProperties).getSender(), file.getSenderAddress(),
                        parseX500Addresses?"true":"false", extractNonDomainX500CN?"true":"false");
                if (((PstEntryPropertiesDto) claFileProperties).getRecipients() != null) {
                    logger.trace("copyPstFields: recipient addresses {} converted into {}",
                            Arrays.toString(((PstEntryPropertiesDto) claFileProperties).getRecipients().toArray()),
                            Arrays.toString(Optional.ofNullable(file.getRecipientsAddresses()).orElse(Collections.<String>emptyList()).toArray()));
                }
            }
        }
        return true;
    }

}
