package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.beust.jcommander.internal.Sets;
import com.cla.common.domain.dto.SearchPatternCounting;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.ExcelSheetIngestResult;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.SearchPatternCount;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.util.categories.FileCatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/**
 * Handle search pattern counting
 */
@Component
public class PatternSearchHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(PatternSearchHandler.class);

    @Value("${ingester.patternSearchActive}")
    private boolean patternSearchActive;

    @Autowired
    private ClaFileSearchPatternService searchPatternService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return patternSearchActive;
    }

    @Override
    public boolean handle(IngestResultContext context) {

        final Set<SearchPatternCountDto> searchPatternsCounting = Sets.newHashSet();

        // Word / PDF
        if (context.isWordOrPDF()){
            WordIngestResult payload = context.getWordResultMessage().getPayload();
            fillSearchPatternCounting(searchPatternsCounting, payload);
        }
        // Other
        else if (context.isOther()){
            OtherFile payload = context.getOtherResultMessage().getPayload();
            fillSearchPatternCounting(searchPatternsCounting, payload);
        }
        // Excel
        else if (context.isExcel()){
            ExcelIngestResult payload = context.getExcelResultMessage().getPayload();
            SolrFileEntity file = context.getFile();
            if (payload.getSheets() == null) {
                logger.error("Workbook has no sheets, file ID: {}, path {}", file.getId(), file.getFullPath());
            } else {
                for (ExcelSheetIngestResult sheet : payload.getSheets()) {
                    fillSearchPatternCounting(searchPatternsCounting, sheet);
                }
            }
        }

        SolrFileEntity file = context.getFile();
        if (!searchPatternsCounting.isEmpty()){
            List<SearchPatternCount> spcDtos = FileCatUtils.convertSearchPatternCountingFromDto(searchPatternsCounting);
            file.setSearchPatternsCounting(spcDtos);
        }

        context.setSearchPatternsCounting(searchPatternsCounting);

        return true;
    }

    private void fillSearchPatternCounting(Set<SearchPatternCountDto> searchPatternsCounting, SearchPatternCounting payload){
        if (payload.getSearchPatternsCounting() != null) {
            searchPatternsCounting.addAll(payload.getSearchPatternsCounting());
        } else if (payload.getOriginalContent() != null) { // for backward compatibility, remove in the future
            searchPatternsCounting.addAll(searchPatternService.getSearchPatternsCounting(payload.getOriginalContent()));
        }
    }

}
