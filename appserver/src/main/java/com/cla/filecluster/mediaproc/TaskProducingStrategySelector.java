package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.producers.*;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Component that manages the task producing strategies for ingest and analysis scheduled producers
 */
@Component
public class TaskProducingStrategySelector {

    private final static Logger logger = LoggerFactory.getLogger(TaskProducingStrategySelector.class);
    private  static final String TASK_PROD_STRATEGY_CONFIG = "taskProdStrategyConfig";

    public static final String EXCLUSIVE_MODE = "exclusiveMode";
    private static final String TIMEOUT_GRACE_SEC = "timeoutGraceSec";
    private static final String INGEST_TASKS_QUEUE_LIMIT = "ingestTasksQueueLimit";
    private static final String ANALYZE_TASKS_QUEUE_LIMIT = "analyzeTasksQueueLimit";
    private static final String COOLDOWN_TASKS_THRESHOLD = "cooldownThreshold";
    private static final String AUTO_EXCLUSIVE_HIGH_THRESHOLD = "autoExclusiveHighThreshold";
    private static final String AUTO_EXCLUSIVE_LOW_THRESHOLD = "autoExclusiveLowThreshold";
    private static final String AUTO_EXCLUSIVE_PERCENTAGE_THRESHOLD = "autoExclusivePercentageThreshold";

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private IngestFinalizationService ingestFinalizationService;

    @Autowired
    private SystemSettingsService systemSettingsService;

    @Autowired
    private DBTemplateUtils dBTemplateUtils;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private UserService userService;

    @Value("${ingest.tasksQueueLimit:1000}")
    private int ingestTasksQueueLimit;

    @Value("${analyze.tasksQueueLimit:100}")
    private int analyzeTasksQueueLimit;

    @Value("${task-producers.exclusive.cooldown-threshold:0}")
    private int cooldownThreshold;

    @Value("${auto-exclusive-strategy.high-threshold:10000}")
    private long highThreshold;

    @Value("${auto-exclusive-strategy.low-threshold:2000}")
    private long lowThreshold;

    @Value("${auto-exclusive-strategy.percentage-threshold:20}")
    private int percentageThreshold;

    private TaskProducingStrategyConfig config;

    private ObjectMapper objectMapper;

    private StrategyType selectedStrategy = StrategyType.FAIR;

    private Map<StrategyType, TaskProducingStrategy> instances;

    private TimeSource timeSource = new TimeSource();

    @PostConstruct
    public void init(){
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);     // Allow null fields to be sent (and ignored during serialization)

        instances = Maps.newHashMap();

        // load settings from DB
        dBTemplateUtils.doInTransaction(() -> {
            List<SystemSettingsDto> savedData = systemSettingsService.getSystemSettings(TASK_PROD_STRATEGY_CONFIG);
            String data = savedData == null || savedData.size() == 0 ? null : savedData.get(0).getValue();
            if (Strings.isNullOrEmpty(data)){
                config = createDefaultConfig();
            }
            else{
                try {
                    config = objectMapper.readValue(data, TaskProducingStrategyConfig.class);
                    setDefaults(config);    // Force defaults until we have user control of this configuration
                    selectedStrategy = config.getSelectedStrategy();
                } catch (IOException e) {
                    logger.error("Failed to read configuration.");
                }
            }
        });

        instances.put(StrategyType.SIMPLE, new SimpleTasksStrategy(jobManagerService, solrTaskService, config));
        instances.put(StrategyType.FAIR, new FairTaskProducingStrategy(jobManagerService, solrTaskService, config));
        instances.put(StrategyType.MANUAL_EXCLUSIVE, new ManualExclusiveFairStrategy(jobManagerService, solrTaskService, config));
        instances.put(StrategyType.AUTO_EXCLUSIVE, new AutoExclusiveFairStrategy(userService, ingestFinalizationService, jobManagerService, solrTaskService, config));

        try {
            logger.info("Task producing strategy configuration: {}.", objectMapper.writeValueAsString(config));
        } catch (JsonProcessingException e) {
            logger.error("Failed to serialize configuration.", e);
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends TaskProducingStrategy> T getSelectedStrategy(){
        return (T)instances.get(selectedStrategy);
    }

    public StrategyType getSelectedStrategyType(){
        return selectedStrategy;
    }

    public void setSelectedStrategy(StrategyType type){
        if (this.selectedStrategy != type) {
            this.selectedStrategy = type;
            config.setSelectedStrategy(type);
            saveConfig(config);
        }
        logger.info("{} task producing strategy was selected.", type);
    }

    public void setParameter(String paramName, String value) {
        if (Strings.isNullOrEmpty(paramName) || Strings.isNullOrEmpty(value)){
            logger.warn("Invalid parameters {} : {}", paramName, value);
        }
        logger.info("Setting task producing strategy parameter {} to value {}", paramName, value);
        switch(paramName){
            case INGEST_TASKS_QUEUE_LIMIT:
                config.setIngestTasksQueueLimit(Integer.valueOf(value));
                break;
            case ANALYZE_TASKS_QUEUE_LIMIT:
                config.setAnalyzeTasksQueueLimit(Integer.valueOf(value));
                break;
            case EXCLUSIVE_MODE:
                JobType jobType = JobType.valueOf(value);
                if (config.getExclusiveMode() != jobType) {
                    config.setExclusiveMode(jobType);
                    ((ManualExclusiveFairStrategy)instances.get(StrategyType.MANUAL_EXCLUSIVE))
                            .setModeChangeTime(timeSource.currentTimeMillis());
                }
                break;
            case TIMEOUT_GRACE_SEC:
                Long timeoutGraceSec = Long.valueOf(value);
                config.setTimeoutGraceSec(timeoutGraceSec);
                break;
            case COOLDOWN_TASKS_THRESHOLD:
                Integer cooldownThreshold = Integer.valueOf(value);
                config.setCooldownTasksThreshold(cooldownThreshold);
                break;
            case AUTO_EXCLUSIVE_HIGH_THRESHOLD:
                Long highThreshold = Long.valueOf(value);
                config.setHighThreshold(highThreshold);
                break;
            case AUTO_EXCLUSIVE_LOW_THRESHOLD:
                Long lowThreshold = Long.valueOf(value);
                config.setLowThreshold(lowThreshold);
                break;
            case AUTO_EXCLUSIVE_PERCENTAGE_THRESHOLD:
                Integer pThresold = Integer.valueOf(value);
                config.setPercentageThreshold(pThresold);
                break;
            default:
                logger.warn("Unknown parameter {}.", paramName);
        }
        saveConfig(config);
    }

    public TaskProducingStrategyConfig getConfig() {
        return config;
    }

    private TaskProducingStrategyConfig createDefaultConfig(){
        TaskProducingStrategyConfig config = new TaskProducingStrategyConfig();
        config.setSelectedStrategy(StrategyType.FAIR);
        config.setExclusiveMode(JobType.INGEST);
        config.setTimeoutGraceSec(TimeUnit.HOURS.toSeconds(1));
        setDefaults(config);
        return config;
    }

    /*
     * Force strategy config from application.properties settings
     */
    private void setDefaults(TaskProducingStrategyConfig config) {
        config.setIngestTasksQueueLimit(ingestTasksQueueLimit);
        config.setAnalyzeTasksQueueLimit(analyzeTasksQueueLimit);
        config.setHighThreshold(highThreshold);
        config.setLowThreshold(lowThreshold);
        config.setPercentageThreshold(percentageThreshold);
        config.setCooldownTasksThreshold(cooldownThreshold);
    }

    /*
     * Conditional use of application.properties settings if defaults are not set
     */
    private void setDefaultsIfNeeded(TaskProducingStrategyConfig config) {
        if (config.getIngestTasksQueueLimit() < 0) {
            config.setIngestTasksQueueLimit(ingestTasksQueueLimit);
        }

        if (config.getAnalyzeTasksQueueLimit() < 0) {
            config.setAnalyzeTasksQueueLimit(analyzeTasksQueueLimit);
        }

        if (config.getHighThreshold() < 0) {
            config.setHighThreshold(highThreshold);
        }

        if (config.getLowThreshold() < 0) {
            config.setLowThreshold(lowThreshold);
        }

        if (config.getPercentageThreshold() < 0) {
            config.setPercentageThreshold(percentageThreshold);
        }

        if (config.getCooldownTasksThreshold() < 0) {
            config.setCooldownTasksThreshold(cooldownThreshold);
        }
    }

    private void saveConfig(TaskProducingStrategyConfig config){
        String data;
        try {
            data = objectMapper.writeValueAsString(config);
            systemSettingsService.updateOrCreateSystemSettings(TASK_PROD_STRATEGY_CONFIG, data);
        } catch (Exception e) {
            logger.error("Failed to save config data", e);
        }
    }
}
