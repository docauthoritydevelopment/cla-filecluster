package com.cla.filecluster.mediaproc.map;

import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.Exchange365FoldersChangesDto;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.service.files.filetree.DocFolderService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
public class MailFolderFullPathResolver {
    private static final String PATH_SEPARATOR = "/";

    private final DocFolderService docFolderService; // TODO Itai: autowire when moving to a specific ScanConsumer
    private final Map<String, MailFolderNode> folderChangesByMid;
    private final String mailboxUPN;
    private final boolean isFirstScan;
    private final boolean deltaScan;
    private final Long rootFolderId;
    private final Long runContext;
    private final Long jobId;
    private final Long taskId;

    private MailFolderFullPathResolver(DocFolderService docFolderService, ScanResultMessage scanResultMessage) {

        Exchange365FoldersChangesDto foldersChangesDto =
                (Exchange365FoldersChangesDto) scanResultMessage.getPayload();

        this.docFolderService = docFolderService;
        this.mailboxUPN = foldersChangesDto.getMailboxUPN();
        this.isFirstScan = scanResultMessage.isFirstScan();
        this.deltaScan = scanResultMessage.isDeltaScan();
        this.rootFolderId = scanResultMessage.getRootFolderId();
        this.runContext = scanResultMessage.getRunContext();
        this.jobId = scanResultMessage.getJobId();
        this.taskId = scanResultMessage.getTaskId();

        folderChangesByMid =
                foldersChangesDto.getFolderChangesDtos()
                        .stream()
                        .collect(Collectors.toMap(
                                ClaFilePropertiesDto::getMediaItemId,
                                MailFolderNode::new));
    }

    public static MailFolderFullPathResolver forScanResultMsg(
            ScanResultMessage scanResultMessage, DocFolderService docFolderService) {

        return new MailFolderFullPathResolver(docFolderService, scanResultMessage);
    }

    public Stream<ScanResultMessage> streamResolvedFolders() {

        return folderChangesByMid.values()
                .stream()
                .map(this::resolveFullPathOfFolderDynasty)
                .map(this::wrapInScanResultMsg);
    }

    private MailFolderNode resolveFullPathOfFolderDynasty(MailFolderNode mailFolderNode) {

        // Step 1: Traverse over the folder and its ancestors and build its dynasty up to the top-folder ancestor
        // or up to a folder that was already resolved as part of a previously resolved other dynasty
        LinkedList<MailFolderNode> folderDynasty = new LinkedList<>();
        Optional<MailFolderNode> optionalFolder = Optional.of(mailFolderNode);
        while (optionalFolder.isPresent()) {
            mailFolderNode = optionalFolder.get();
            folderDynasty.addFirst(mailFolderNode);
            if (mailFolderNode.isFullPathResolved()) {
                // Stop building the dynasty when reaching an ancestor that was already resolved in another dynasty
                // Even if it is not a top-folder
                break;
            }
            optionalFolder = getParentFolder(mailFolderNode); // returns Optional.empty() after returning the top-folder
        }

        // Step 2: Iterate over the unresolved folders (that is, either the whole dynasty or all of it except the
        // top folder which already had its full path built) and build their full paths
        StringBuilder pathBuilder;
        List<MailFolderNode> unresolvedFolderDynasty;
        MailFolderNode topFolder = folderDynasty.getFirst(); // Actually, this may be either a top-folder or an already-resolved-folder
        if (topFolder.isFullPathResolved()) {
            // The topFolder's folderFullPath includes the leading slash and mailboxUpn
            pathBuilder = new StringBuilder(topFolder.getFolderFullPath());
            // skip topFolder in iteration below
            unresolvedFolderDynasty = folderDynasty.subList(1, folderDynasty.size());
        } else {
            // Start with the leading slash and mailboxUpn
            pathBuilder = new StringBuilder(PATH_SEPARATOR).append(mailboxUPN);
            // Iterate over all the dynasty
            unresolvedFolderDynasty = folderDynasty;
        }
        for (MailFolderNode folderNode : unresolvedFolderDynasty) {
            pathBuilder.append(PATH_SEPARATOR).append(Objects.requireNonNull(folderNode.getDisplayName())); // Note, that the display name may be null if this folder node came from solr, in which case we should have skipped it in the above if, hence the null check here
            folderNode.setFolderFullPath(pathBuilder.toString());
        }
        return folderDynasty.getLast();
    }

    private Optional<MailFolderNode> getParentFolder(MailFolderNode mailFolderNode) {
        String parentMid = getParentMid(mailFolderNode);
        if (folderChangesByMid.containsKey(parentMid)){
            return Optional.of(folderChangesByMid.get(parentMid));
        }
        if (isFirstScan) {
            // No dto for the parent folder -> This is a top-folder -> stop iteration
            return Optional.empty();
        }

        // Got here -> We're in rescan and there's no dto for the parent folder -> There are two options:
        // 1. This folder is a new top-level folder (sibling of Inbox) and its parent in Solr is the mailbox folder
        //    which doesn't have the IPM-ROOT MID set, thus the below call will return null.
        // 2. This folder is a new sub-folder (not top-level) and its parent should be in Solr from a previous scan
        // In other words, if the below call return null, we assume this is a new top-level folder and return empty optional to stop iteration
        // TODO Itai: here, we're just resolving the full path and for this end, fetching the parent folder from solr is enough. When moving to a specific ScanConsumer, we'll need to fetch from solr every non-new folder (non-new can be determined by kvdb existence) and determine whether it was moved or renamed (or both) by comparing parent id and displayName of the dto to those of the solr entity. This should change the current code that checks whether the folder was moved by comparing the full path (which we're resolving here)
        SolrFolderEntity parentFolderFromSolr =
                docFolderService.getFolderByMediaEntityId(rootFolderId, parentMid);
        if (parentFolderFromSolr == null) {
            return Optional.empty();
        }

        MailFolderNode parentFolderNode = new MailFolderNode(parentFolderFromSolr); // Note this folder node is already resolved -> the while loop will exit in the next iteration break statement
        return Optional.of(parentFolderNode);
    }

    private ScanResultMessage wrapInScanResultMsg(MailFolderNode mailFolderNode) {
        Exchange365ItemDto changedFolderDto = mailFolderNode.getUnderlyingDto();
        changedFolderDto.setFileName(mailFolderNode.getFolderFullPath());
        ScanResultMessage scanResultMessage = // TODO Itai: This is ugly.. Remove this when creating a scanConsumer dedicated for Exchange365
                new ScanResultMessage(changedFolderDto, rootFolderId, isFirstScan, runContext, taskId, jobId);
        scanResultMessage.setDeltaScan(deltaScan);
        return scanResultMessage;
    }

    private String getParentMid(MailFolderNode mailFolderNode) {
        String parentMid = mailFolderNode.getParentMid();
        if (parentMid == null) {
            throw new IllegalStateException("Can't get the parent folder if the parentMid is null. This may indicate trying to get a parent of a parent folder from solr. MailFolder: " + mailFolderNode + ", mailboxUPN" + mailboxUPN + ", rootFolderId" + rootFolderId + ", runContext" + runContext + ", jobId" + jobId + ", taskId" + taskId + ", isFirstScan" + isFirstScan + ", deltaScan" + deltaScan);
        }
        return parentMid;
    }
}
