package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.managers.FileClusterAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.util.FileCrawlerUtils;
import com.cla.filecluster.util.executors.BlockingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class StartScanProducer {

    private final static Logger logger = LoggerFactory.getLogger(StartScanProducer.class);

    public static final String SCAN_INGEST_ANALYZE = "scan,ingest,analyze";

    @Autowired
    private FileClusterAppService fileClusterAppService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private BlockingExecutor startScanBlockingExecutor;

    public List<String> startScanRootFolder(List<Long> rootFolderIds, Boolean isWait, List<String> operationList, Boolean forceReingest)
    {
        logger.info("Start scan on {} root Folders", rootFolderIds.size());
        List<RootFolderDto> rootFoldersDto = docStoreService.getRootFoldersDto(rootFolderIds);
        List<String> result = new ArrayList<>();
        rootFoldersDto.forEach(rootFolderDto ->
                result.add(startScanRootFolder(isWait, operationList, rootFolderDto, null, forceReingest)));
        jobManagerAppService.markStatusChange();
        return result;
    }

    public String startScanRootFolder(long rootFolderId, Boolean isWait, List<String> operationList, Boolean forceReingest) {
        try {
            RootFolderDto rootFolderDto = docStoreService.getRootFolderDto(rootFolderId);
            String result = startScanRootFolder(isWait, operationList, rootFolderDto, null, forceReingest);
            jobManagerAppService.markStatusChange();
            return result;
        } catch (RuntimeException e) {
            logger.warn("RootFolder does not exist id {} {}", rootFolderId, e);
            return "RootFolder does not exist. ID: "+rootFolderId;
        }

    }

    public List<String> startScanScheduleGroups(List<Long> scheduleGroupIds, Boolean isWait, List<String> operationList, Boolean manual) {
        logger.info("Scan schedule groups {}", scheduleGroupIds);
        List<String> result = new ArrayList<>();
        scheduleGroupIds.forEach(scheduleGroupId ->
                result.addAll(startScanScheduleGroup(scheduleGroupId, isWait, operationList, manual)));
        return result;
    }

    public List<String> startScanScheduleGroup(long scheduleGroupId, Boolean isWait, List<String> operationList, Boolean manual) {
        List<String> result = new ArrayList<>();
        try {
            ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(scheduleGroupId);
            if(scheduleGroup == null) {
                result.add("ScheduleGroup does not exist. ID: "+scheduleGroupId);
                return result;
            }
            if (!operationList.contains(FileCrawlerUtils.SCAN)) {
                result.add( "TODO - Run on a rootFolder without scan is still unsupported.");
                return result;
            }
            List<RootFolderDto> rootFolderDtos = scheduleGroupService.listScheduleGroupRootFolderDtos(scheduleGroupId);
            rootFolderDtos = rootFolderDtos.stream().filter(rf -> rf.isRescanActive() || rf.getLastRunId() == null).collect(toList());
            String error = fileCrawlerExecutionDetailsService.validateStartScheduleGroupCrawlRun(scheduleGroup, rootFolderDtos);
            if (error != null) {
                result.add( error);
                return result;
            }
            CrawlRun scheduleGroupCrawlRun = fileCrawlerExecutionDetailsService.createScheduleGroupCrawlRun(scheduleGroup, rootFolderDtos,manual, operationList);

            rootFolderDtos.forEach(rootFolderDto ->
                    result.add(startScanRootFolder(isWait, null, rootFolderDto, scheduleGroupCrawlRun, rootFolderDto.isReingest())));
            jobManagerAppService.markStatusChange();
        } catch (RuntimeException e) {
            logger.info("ScheduleGroup does not exist id {} {}", scheduleGroupId,e );
            result.add("ScheduleGroup does not exist. ID: "+scheduleGroupId);
        }
        return result;
    }

    public List<String> createIngestRunForRootFolder(List<Long> rootFolderIds) {
        logger.info("Start ingest on {} root Folders", rootFolderIds.size());
        List<RootFolderDto> rootFoldersDto = docStoreService.getRootFoldersDto(rootFolderIds);
        List<String> result = new ArrayList<>();
        rootFoldersDto.forEach(rootFolderDto ->
                result.add(createIngestRunForRootFolder(rootFolderDto)));
        jobManagerAppService.markStatusChange();
        return result;
    }

    private String createIngestRunForRootFolder(RootFolderDto rootFolderDto) {
        List<String> operationList = new ArrayList<>();
        try {
            String error = fileCrawlerExecutionDetailsService.validateStartRootFolderCrawlRun(rootFolderDto);
            if (error != null) {
                return error;
            }
            operationList.add(FileCrawlerUtils.INGEST);
            operationList.add(FileCrawlerUtils.ANALYZE);
            CrawlRun crawlRun = createRootFolderCrawlRun(rootFolderDto, operationList);
            startScanBlockingExecutor.execute(() -> {
                try {
                    jobManagerAppService.createIngestRunForRootFolder(rootFolderDto, crawlRun.getId());
                } catch (Exception e) {
                    logger.error("failed creating ingest tasks for run {} and root folder {}", crawlRun.getId(), rootFolderDto.getId(), e);
                }
            });
            return "Start ingest for " + rootFolderDto.getRealPath();
        } catch (Exception e) {
            logger.error("Failed to start root folder ingest {} {}", rootFolderDto, e);
            return "Failed to start root folder ingest for " + rootFolderDto.getRealPath();
        }
    }

    public List<String> createAnalysisRunForRootFolder(List<Long> rootFolderIds) {
        logger.info("Start analysis on {} root Folders", rootFolderIds.size());
        List<RootFolderDto> rootFoldersDto = docStoreService.getRootFoldersDto(rootFolderIds);
        List<String> result = new ArrayList<>();
        rootFoldersDto.forEach(rootFolderDto ->
                result.add(createAnalysisRunForRootFolder(rootFolderDto)));
        jobManagerAppService.markStatusChange();
        return result;
    }

    private String createAnalysisRunForRootFolder(RootFolderDto rootFolderDto) {
        List<String> operationList = new ArrayList<>();
        try {
            String error = fileCrawlerExecutionDetailsService.validateStartRootFolderCrawlRun(rootFolderDto);
            if (error != null) {
                return error;
            }
            operationList.add(FileCrawlerUtils.ANALYZE);
            CrawlRun crawlRun = createRootFolderCrawlRun(rootFolderDto, operationList);
            startScanBlockingExecutor.execute(() -> {
                try {
                    jobManagerAppService.createAnalysisRunForRootFolder(rootFolderDto, crawlRun.getId());
                } catch (Exception e) {
                    logger.error("failed creating analysis tasks for run {} and root folder {}", crawlRun.getId(), rootFolderDto.getId(), e);
                }
            });
            return "Start analysis for " + rootFolderDto.getRealPath();
        } catch (Exception e) {
            logger.error("Failed to start root folder analysis {} {}", rootFolderDto, e);
            return "Failed to start root folder analysis for " + rootFolderDto.getRealPath();
        }
    }

    private String startScanRootFolder(Boolean isWait, List<String> operationList, RootFolderDto rootFolderDto, CrawlRun scheduleGroupCrawlRun, Boolean forceReingest) {
        try {
            CrawlRun crawlRun;

            if (forceReingest && !rootFolderDto.isReingest()) {
                docStoreService.updateRootFolderReingest(rootFolderDto.getId(), true);
            }
            if(scheduleGroupCrawlRun != null) {
                String error = fileCrawlerExecutionDetailsService.validateStartScheduleGroupRootFolderCrawlRun(rootFolderDto);
                if (error != null) {
                    return error;
                }

                List<String> opList = fileCrawlerExecutionDetailsService.generateOpList(scheduleGroupCrawlRun.getId());
                CrawlRun rootFolderCrawlRun = createRootFolderCrawlRun(rootFolderDto, opList);
                crawlRun = fileCrawlerExecutionDetailsService.updateScheduleGroupRootFolderCrawlRun(scheduleGroupCrawlRun, rootFolderCrawlRun);
            } else {
                String error = fileCrawlerExecutionDetailsService.validateStartRootFolderCrawlRun(rootFolderDto);
                if (error != null) {
                    return error;
                }
                crawlRun = createRootFolderCrawlRun(rootFolderDto, operationList);
            }

            if (isWait) {
                logger.debug("Rerun (run id = {}) on root folder {} and wait for run to end", crawlRun.getId(), rootFolderDto.getId());
                fileClusterAppService.doStartScanSync(crawlRun, rootFolderDto);
                jobManagerAppService.waitForRunToFinish(crawlRun.getId());
            } else {
                startScanBlockingExecutor.execute(() -> {
                    try {
                        fileClusterAppService.doStartScanSync(crawlRun, rootFolderDto);
                    } catch (RuntimeException e) {
                        logger.warn("Failed to start scan request " + e.getMessage());
                    }
                });
            }
            return "Start scan for " + rootFolderDto.getIdentifier();
        } catch (RuntimeException e) {
            logger.error("Failed to start root folder scan {}", rootFolderDto, e);
            return "Failed to start root folder scan for " + rootFolderDto.getRealPath();
        }
    }

    private CrawlRun createRootFolderCrawlRun(RootFolderDto rootFolderDto, List<String> opList) {
        CrawlRun crawlRun = fileCrawlerExecutionDetailsService.createRootFolderCrawlRun(rootFolderDto, opList);
        jobManagerAppService.createFinalizeTasksIfNeeded(crawlRun.getId());
        return crawlRun;
    }


    public List<String> getOperationList(Map<String, String> params) {
        String[] ops = params.getOrDefault("op", SCAN_INGEST_ANALYZE).split(",");
        return Arrays.stream(ops).collect(toList());
    }

}
