package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.FileToDel;
import com.cla.filecluster.mediaproc.map.MapDeleteHandler;
import com.cla.filecluster.mediaproc.map.MapExchangeServices;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Create and delete attachments if exchange
 *
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class ExchangeAttachmentHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(ExchangeAttachmentHandler.class);

    @Autowired
    private MapExchangeServices mapExchangeServices;

    @Autowired
    private MapDeleteHandler mapDeleteHandler;

    @Autowired
    private DocStoreService docStoreService;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ClaFilePropertiesDto propertiesDto = (ClaFilePropertiesDto) mapContext.getScanResultMessage().getPayload();
        Map<String, FileCatBuilder> attachmentsToCreate = mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToCreate();

        if (propertiesDto.isFile()) {
            List<Long> attachmentsFileIDs = attachmentsToCreate
                    .values()
                    .stream()
                    .map(FileCatBuilder::getFileId)
                    .distinct()
                    .collect(Collectors.toList());
            mapContext.setAttachmentsFileIDs(attachmentsFileIDs);
        }

        Pair<Long, Long> batchTaskId = MapServiceUtils.createBatchTaskId(mapContext.getBatchIdentifierNumber(), mapContext.getScanResultMessage().getTaskId());
        Long rootFolderId = mapContext.getScanResultMessage().getRootFolderId();
        Long runContext = mapContext.getScanResultMessage().getRunContext();
        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);

        // get email association data to be applied to attachments as well
        SolrInputDocument doc = mapContext.getSolrInputDocumentFileAndFolderId().getDocFileToCreate();
        Map<CatFileFieldType, Object> associationData = AssociationsService.getDocAssociationData(doc);

        // handle attachment file creation
        mapExchangeServices.handleAttachmentCreation(attachmentsToCreate, batchTaskId,
                rf, runContext, mapContext.getFolderId(), mapContext.getTime(), associationData);

        // handle attachment file deletion
        List<Triple<FileToDel, FileEntity, String>> attToDel = mapExchangeServices.handleAttachmentDeletion(
                mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToDelete(), rf,
                runContext, mapContext.getFolderId(), mapContext.getTime());
        attToDel.forEach(att ->
                mapDeleteHandler.handleFileToDelete(batchTaskId, runContext, rootFolderId, att.getLeft(), att.getMiddle(), att.getRight())
        );
        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        boolean shouldHandle = mapContext.getSolrInputDocumentFileAndFolderId() != null && // new file
                (mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToCreate() != null ||
                        mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToDelete() != null);
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
