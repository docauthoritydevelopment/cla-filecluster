package com.cla.filecluster.mediaproc.map;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Scan result message consumer on the AppServer side
 */
@Service
public class MapBatchTaskHandler implements ControlledScheduling {

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Value("${scan.min-millis-between-batch-execute:10000}")
    private long minMillisBetweenBatchExecute;

    @Value("${scan.min-objects-to-batch-execute:1000}")
    private int minObjectsToBatchExecute;

    private long timeOfLastBatchFlush = 0;

    private Set<Pair<Long, Long>> batchTasksToProcess = new HashSet<>();

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    public synchronized void addToBatch(Pair<Long, Long> batchTaskId) {
        batchTasksToProcess.add(batchTaskId);
    }

    @Scheduled(initialDelayString = "${scan.batch-execute.initial-delay.millis:10000}",
            fixedRateString = "${scan.batch-execute.rate.millis:5000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void executeScanBatchIfNeeded() {
        if (!isSchedulingActive) {
            return;
        }

        Set<Pair<Long, Long>> batch = getBatch();

        if (batch != null && !batch.isEmpty()) {
            //logger.debug("handle map batch for {}",batch);
            mapBatchResultsProcessor.flush(batch);
        }
    }

    private synchronized Set<Pair<Long, Long>> getBatch() {
        long timePassed;
        Set<Pair<Long, Long>> batch = null;
        synchronized (this) {
            timePassed = System.currentTimeMillis() - timeOfLastBatchFlush;
            if (batchTasksToProcess.size() > minObjectsToBatchExecute || timePassed > minMillisBetweenBatchExecute) {
                batch = batchTasksToProcess;
                batchTasksToProcess = new HashSet<>();
                timeOfLastBatchFlush = System.currentTimeMillis();
            }
        }
        return batch;
    }
}