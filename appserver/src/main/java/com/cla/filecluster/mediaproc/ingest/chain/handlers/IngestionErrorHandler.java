package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.beust.jcommander.internal.Sets;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.messages.ProcessingError;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.cla.connector.domain.dto.error.ProcessingErrorType.*;

/**
 * Handle ingestion errors if present
 * This handler stops the chain if activated
 */
@Component
public class IngestionErrorHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(IngestionErrorHandler.class);

    @Autowired
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        Optional<ProcessingErrorType> processingErrorType = detectError(context);
        processingErrorType.ifPresent(context::setErrorType);
        return processingErrorType.isPresent();
    }

    @Override
    public boolean handle(IngestResultContext context) {
        Long fileId = context.getResultMessage().getClaFileId();
        logger.debug("Ingestion errors found at file {} type {}", fileId, context.getErrorType());

        switch(context.getErrorType()) {
            case ERR_FILE_NOT_FOUND:
            case ERR_NETWORK:
                String message = messageHandler.getMessage(FilesDataPersistenceService.ERROR_MSG_PREFIX + context.getErrorType().getText());
                ingestErrorService.saveError(context.getResultMessage().getTaskId(),
                        context.getResultMessage().getRunContext(), context.getFile(),
                        context.getErrorType(), message,
                        context.getPayload() == null ? null : context.getPayload().getExceptionText());
                context.setFailedTask(true);
                if (context.getErrorType().equals(ERR_NETWORK) && jobManagerService.isJobStarted(context.getResultMessage().getJobId())) {
                    logger.info("Network error for ingest file {} fail job {}", fileId, context.getResultMessage().getJobId());
                    jobManagerService.handleFailedFinishJob( context.getResultMessage().getJobId(), "Ingest failed");
                    jobManagerAppService.endRun(context.getResultMessage().getRunContext(), PauseReason.SYSTEM_ERROR);
                }
                break;
            case ERR_LIMIT_EXCEEDED:
            case ERR_OFFLINE_SKIPPED:
            default:
                filesDataPersistenceService.addIngestionErrorRecord(context.getIngestResultMessage(), context.getErrorType(),
                        ClaFileState.SCANNED_AND_STOPPED, context.getIngestBatchStore());
                break;
        }
        return false;
    }

    private Optional<ProcessingErrorType> detectError(IngestResultContext context){

        IngestResultMessage ingestResult = context.getResultMessage();
        if (!ingestResult.hasErrors()){
            return Optional.empty();
        }

        Set<ProcessingErrorType> errorTypesSet = Sets.newHashSet();
        List errors = ingestResult.getErrors();
        for (Object error : errors){
            ProcessingError err = (ProcessingError)error;
            errorTypesSet.add(err.getErrorType());
        }

        if (errorTypesSet.contains(ERR_FILE_NOT_FOUND)){
            return Optional.of(ERR_FILE_NOT_FOUND);
        }

        if (errorTypesSet.contains(ERR_NETWORK)){
            return Optional.of(ERR_NETWORK);
        }

        if (errorTypesSet.contains(ERR_LIMIT_EXCEEDED)) {
            return Optional.of(ERR_LIMIT_EXCEEDED);
        }

        if (errorTypesSet.contains(ERR_OFFLINE_SKIPPED)) {
            return Optional.of(ERR_OFFLINE_SKIPPED);
        }

        if (!ingestResult.hasPayload()){
            // in this case the system will attempt to save the file properties without the content
            return Optional.empty();
        }
        else if (context.getPayload().hasError()){
            return Optional.of(context.getPayload().getErrorType());
        }

        return errorTypesSet.stream().findFirst();
    }

}
