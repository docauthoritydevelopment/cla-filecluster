package com.cla.filecluster.mediaproc.ingest.chain;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.TimeSource;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.mediaproc.ingest.chain.handlers.*;
import com.cla.filecluster.service.chain.Chain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class IngestItemConsumerChain {

    private final static Logger logger = LoggerFactory.getLogger(IngestItemConsumerChain.class);

    @Autowired
    private MissingLabelsHandler missingLabelsHandler;

    @Autowired
    private IngestionErrorHandler ingestionErrorHandler;

    @Autowired
    private EmptyFileHandler emptyFileHandler;

    @Autowired
    private DuplicateContentHandler duplicateContentHandler;

    @Autowired
    private MetadataOnlyHandler metadataOnlyHandler;

    @Autowired
    private PdfFileTypeHandler pdfFileTypeHandler;

    @Autowired
    private PatternSearchHandler patternSearchHandler;

    @Autowired
    private OtherExtractedContentHandler otherExtractedContentHandler;

    @Autowired
    private FillFilePropertiesHandler filePropertiesHandler;

    @Autowired
    private ContentMetadataHandler contentMetadataHandler;

    @Autowired
    private FileRecordHandler fileRecordHandler;

    @Autowired
    private PVAnalysisDataHandler pvAnalysisDataHandler;

    @Autowired
    private ExtractionDataHandler extractionDataHandler;

    @Autowired
    private HistogramInfoHandler histogramInfoHandler;

    @Autowired
    private EventBus eventBus;

    private Chain<IngestResultContext> chain = null;

    private TimeSource timeSource = new TimeSource();


    @PostConstruct
    public void init() {
        chain = Chain.create("IngestChain");
        chain
            .next(missingLabelsHandler)
            .next(ingestionErrorHandler)
            .next(emptyFileHandler)
            .next(duplicateContentHandler)
            .next(metadataOnlyHandler)
            .next(pdfFileTypeHandler)
            .next(patternSearchHandler)
            .next(otherExtractedContentHandler)
            .next(filePropertiesHandler)
            .next(contentMetadataHandler)
            .next(fileRecordHandler)
            .next(pvAnalysisDataHandler)
            .next(extractionDataHandler)
            .next(histogramInfoHandler);

    }

    @AutoAuthenticate
    public <S extends IngestMessagePayload> void processIngestionResult(IngestResultMessage<S> result, IngestBatchStore ingestBatchStore) {
        logger.trace("Handle ingestion result {} on task {}", result, result.getTaskId());

        IngestResultContext context = IngestResultContext.create(result)
                .setIngestBatchStore(ingestBatchStore);

        ClaFilePropertiesDto fileProperties = result.getFileProperties();
        FileType fileType = fileProperties.getType();

        eventBus.broadcast(Topics.INGEST, Event.builder()
                .withEventType(EventType.OPERATION_MESSAGE)
                .withEntry(EventKeys.JOB_ID, result.getJobId())
                .withEntry(EventKeys.OPERATION_MESSAGE,
                        "Ingesting " + (fileType == null ? "" : fileType + " ")  + "file: " + fileProperties.getFileName())
                .build());

        SolrFileEntity file = context.getIngestBatchStore().getFileEntity(
                context.getIngestResultMessage().getClaFileId());

        if (ClaFileState.ERROR.equals(file.getState())) {
            logger.warn("Handling ingestion result of file that is already in ERROR state ({})", file);
        }

        long start = timeSource.currentTimeMillis();
        chain.handle(context);
        ingestBatchStore.markTaskAsDone(result.getTaskId(), result.getJobId(),
                context.isFailedTask() ? TaskState.FAILED : TaskState.DONE);
        logger.trace("Handle ingestion result {} on task {} took {} ms.",
                result, result.getTaskId(), timeSource.millisSince(start));
    }
}
