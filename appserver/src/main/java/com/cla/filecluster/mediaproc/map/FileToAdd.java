package com.cla.filecluster.mediaproc.map;

import com.cla.common.domain.dto.crawler.FileEntity;
import org.apache.solr.common.SolrInputDocument;

import java.util.Objects;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
public class FileToAdd {

    private SolrInputDocument doc;
    private FileEntity fileEntity;
    private String mediaItemId;
    private Long rootFolderId;
    private final Long runId;
    private Long fileIdToIngest;
    private String paramNewIngestTask;

    public FileToAdd(Long fileIdToIngest, String paramNewIngestTask, Long runId) {
        this.fileIdToIngest = fileIdToIngest;
        this.runId = runId;
        this.paramNewIngestTask = paramNewIngestTask;
    }

    public FileToAdd(SolrInputDocument doc, FileEntity fileEntity, String mediaItemId, Long rootFolderId, Long runId) {
        this.doc = doc;
        this.fileEntity = fileEntity;
        this.mediaItemId = mediaItemId;
        this.rootFolderId = rootFolderId;
        this.runId = runId;
        this.fileIdToIngest = null;
        this.paramNewIngestTask = null;
    }

    public SolrInputDocument getDoc() {
        return doc;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public String getMediaItemId() {
        return mediaItemId;
    }

    public Long getRootFolderId() {
        return rootFolderId;
    }

    public Long getRunId() {
        return runId;
    }

    public Long getFileIdToIngest() {
        return fileIdToIngest;
    }

    public String getParamNewIngestTask() {
        return paramNewIngestTask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileToAdd fileToAdd = (FileToAdd) o;
        return Objects.equals(mediaItemId, fileToAdd.getMediaItemId()) &&
                Objects.equals(rootFolderId, fileToAdd.getRootFolderId()) &&
                Objects.equals(fileIdToIngest, fileToAdd.getFileIdToIngest()) &&
                Objects.equals(paramNewIngestTask, fileToAdd.getParamNewIngestTask()) &&
                Objects.equals(runId, fileToAdd.getRunId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(mediaItemId, rootFolderId, runId, fileIdToIngest, paramNewIngestTask);
    }
}
