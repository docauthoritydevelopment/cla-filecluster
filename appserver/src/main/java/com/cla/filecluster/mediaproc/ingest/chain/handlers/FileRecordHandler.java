package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.AclItemDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.crawler.analyze.MaturityLevelService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Populate and save/update the file record in Solr (CatFile)
 */
@Component
public class FileRecordHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(FileRecordHandler.class);

    @Autowired
    private MaturityLevelService maturityLevelService;

    @Autowired
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private IngestBatchResultsProcessor ingestResultCollector;

    @Autowired
    private EventBus eventBus;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean handle(IngestResultContext context) {
        SolrFileEntity file = context.getFile();
        Long runId = context.getIngestResultMessage().getRunContext();
        Long taskId = context.getIngestResultMessage().getTaskId();
        ContentMetadata contentMetadata = context.getContentMetadata();

        List<AclItemDto> aclItems = getAclItems(context);
        int maturityLevel = calcMaturityLevel(context);
        context.setMaturityLevel(maturityLevel);

        logger.trace("Saving file, name:{}, ID {}, maturityLevel: {}.",
                file.getFullName(), file.getId(), maturityLevel);

        if (ModelIngestUtils.isNonIngestedExcelState(context)) {
            logger.debug("File was not ingested due to its size.");
            file.setState(ClaFileState.SCANNED_AND_STOPPED.toString());
        }
        // SCANNED indicates a new content was created, otherwise its something else
        else if (!context.getOriginalState().equals(ClaFileState.SCANNED)) {
            logger.debug("Found existing content {} of state {} for file {} {}. Analysis group ID: {}",
                    contentMetadata.getId(), context.getOriginalState(), file.getId(),
                    file.getFullName(), contentMetadata.getAnalysisGroupId());

            file.setState(contentMetadata.getState().name());
            if (contentMetadata.getAnalysisGroupId() != null) {
                file.setAnalysisGroupId(contentMetadata.getAnalysisGroupId());
                ingestResultCollector.collectMarkGroupAsDirty(taskId, contentMetadata.getAnalysisGroupId());
            }
            if (contentMetadata.getUserGroupId() != null) {
                file.setUserGroupId(contentMetadata.getUserGroupId());
                ingestResultCollector.collectMarkGroupAsDirty(taskId, contentMetadata.getUserGroupId());
            }

            if (contentMetadata.getAnalysisGroupId() != null || contentMetadata.getUserGroupId() != null) {
                eventBus.broadcast(Topics.FILE_GROUP_ATTACH, Event.builder()
                        .withEventType(EventType.SINGLE_FILE_GROUP_ATTACH)
                        .withEntry(EventKeys.ATTACHED_GROUPED_FILE_ID, file.getId())
                        .withEntry(EventKeys.ROOT_FOLDER_ID, file.getRootFolderId())
                        .withEntry(EventKeys.FILE_MEDIA_TYPE, MediaType.valueOf(file.getMediaType()))
                        .build());
            }
        } else {
            // update file state
            file.setState(ClaFileState.INGESTED.name());

            fillPvAnalysisData(context, contentMetadata);
        }

        SolrFileEntity savedFile;
        if (context.isOther()) {
            savedFile = filesDataPersistenceService.saveOtherFileRecordInSolr(taskId, file,
                    context.getPayload().getClaFileProperties(), context.getSearchPatternsCounting(), aclItems,
                    contentMetadata, context.getIngestBatchStore());
        } else {
            savedFile = filesDataPersistenceService.saveFileRecordInSolr(taskId, file,
                    context.getPvAnalysisData(), aclItems, runId, context.getPayload().getCombinedProposedTitles(),
                    contentMetadata, maturityLevel, context.getIngestBatchStore());
        }

        Long contentId = savedFile.getContentId() != null ? Long.valueOf(savedFile.getContentId()) : null;
        context.setSavedFile(savedFile);
        context.setSavedContentId(contentId);
        logger.trace("Saved file {} with ID {} and content ID {} to Solr",
                file.getFullName(), file.getId(), contentId);

        return true;
    }

    private List<AclItemDto> getAclItems(IngestResultContext context) {
        List<AclItemDto> aclItems;
        SolrFileEntity file = context.getFile();
        if (context.isOther()) {
            aclItems = filesDataPersistenceService.getAclItems(file.getFileId(),
                    context.getIngestResultMessage().getFileProperties());
        } else {
            aclItems = ModelIngestUtils.getAclItems(
                    file.getFileId(), context.getPayload().getMetadata());
        }
        return aclItems;
    }

    private int calcMaturityLevel(IngestResultContext context) {
        int maturityFileLevel = 0;
        if (context.isWordOrPDF()) {
            ClaSuperCollection<Long> longHistograms = context.getWordResultMessage().getPayload().getLongHistograms();
            maturityFileLevel = maturityLevelService.getPartMaturityLevel(longHistograms);
        } else if (context.isExcel()) {
            PvAnalysisData pvAnalysisData = context.getPvAnalysisData();
            if (pvAnalysisData == null || pvAnalysisData.getPvCollections() == null ||
                    pvAnalysisData.getPvCollections().getCollections() == null) {
                maturityFileLevel = 0;
                logger.info("File name={} is missing PV collections", context.getFile().getFullName());
            } else {
                maturityFileLevel = maturityLevelService.workbookMaturityLevel(pvAnalysisData);
                logger.trace("File name={} maturity file Level ={}", context.getFile().getFullName(), maturityFileLevel);
            }
        }
        return maturityFileLevel;
    }

    /**
     * Word/PDF only
     *
     * @param context         ingest chain context
     * @param contentMetadata content metadata
     */
    private void fillPvAnalysisData(IngestResultContext context, ContentMetadata contentMetadata) {
        if (context.isWordOrPDF()) {
            WordIngestResult payload = context.getWordResultMessage().getPayload();
            context.setPvAnalysisData(new PvAnalysisData(contentMetadata.getId(),
                    payload.getLongHistograms(), null));
        } else if (context.isExcel()) {
            ExcelIngestResult payload = context.getExcelResultMessage().getPayload();
            context.setPvAnalysisData(new PvAnalysisData(contentMetadata.getId(),
                    payload.getFileCollections(), payload.getSheetsMetadata()));
        }
    }

}
