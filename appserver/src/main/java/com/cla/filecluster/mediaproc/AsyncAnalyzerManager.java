package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobStateUpdateInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.utils.LoggingUtils;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.batch.writer.analyze.AnalyzeBatchResultsProcessor;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.pvs.PvsScaler;
import com.cla.filecluster.service.crawler.analyze.*;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.service.syscomponent.AppServerStateService;
import com.cla.filecluster.util.JobTaskUtils;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Created by uri on 19-Mar-17.
 */
@Service
public class AsyncAnalyzerManager implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(AsyncAnalyzerManager.class);

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private FileGroupingService fileGroupingService;

    @Autowired
    private BlockingExecutor analyzeBlockingExecutor;

    @Autowired
    private AnalyzeAppService analyzeAppService;

    @Autowired
    private AppServerStateService appServerStateService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    private TimeSource timeSource = new TimeSource();

    private AtomicLong analysisCounter = new AtomicLong(0L);

    //------------------------- variables ---------------------------------------------

    @Value("${analyze.delete.before.proposal:false}")
    private boolean deleteBeforeProposalOnFinalize;

    @Value("${analyze.missed-matches-threshold:1000}")
    private int missedMatchesThreshold;

    @Autowired
    private TaskProducingStrategySelector taskProducingStrategySelector;

    @Autowired
    private AnalyzerDataService analyzerDataService;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private ContentGroupsCache contentGroupsCache;

    @Autowired
    private AnalyzeBatchResultsProcessor analyzeBatchResultsProcessor;

    @Autowired
    private PvsScaler pvsScaler;

    @Value("${analyze.soft-commit-solr-rate:600}")
    private long analyzeSoftCommitSolrRate;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @AutoAuthenticate
    @Scheduled(fixedRateString = "${analyzer.tasks-polling-rate-millis}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void processNextTasksGroup() {
        if (!isSchedulingActive) {
            return;
        }

        // Lazy initialization to initialize the aliases of read write
        if (!pvsScaler.confirmInitialization()) {
            logger.error("Cannot process analyze job, PVS scaler failed to initialize");
            return;
        }

        List<SimpleJob> inProgressJobs = jobManager.getJobs(JobType.ANALYZE, JobState.IN_PROGRESS);
        if (inProgressJobs.isEmpty()) {
            logger.trace("No in progress analyze jobs found, skipping");
            // avoid calling getTaskStatesByType (which involves a pricey query) if there are no in-progress jobs
            return;
        }

        Table<Long, TaskState, Number> taskStatesByType = solrTaskService.getAnalysisTaskByRunAndStateNum(
                inProgressJobs.stream().map(Job::getRunContext).collect(Collectors.toList())
        );

        logJobCounters(taskStatesByType);

        // acquire the next group of tasks
        Collection<SimpleTask> tasks = taskProducingStrategySelector.getSelectedStrategy()
                .nextTaskGroup(TaskType.ANALYZE_CONTENT, taskStatesByType, null);

        if (tasks.size() > 0) {
            List<Long> taskIds = tasks.stream().map(Task::getId).collect(Collectors.toList());
            try {
                final long start = timeSource.currentTimeMillis();
                solrTaskService.updateTaskStateByContentId(TaskState.ENQUEUED, taskIds);
                KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AsyncAnalyzerManager.processNextTasksGroup", "analyzeTasks", 155);
                analyzeTasks(tasks);
                kpiRecording.end();
                long duration = timeSource.millisSince(start);
                LoggingUtils.log(logger, (logger.isTraceEnabled()) ? Level.TRACE : Level.DEBUG,
                        "Handled {} analyze tasks in {} ms", tasks.size(), duration);
            } catch (Exception e) {
                logger.error("Failed to handle analyze tasks", e);
                tasks.forEach(t ->
                        analyzeBatchResultsProcessor.collectFailedTasks(t.getId(), t.getJobId()));
            }
        }
        handleFinishedJobs(taskStatesByType, inProgressJobs);
    }


    private void handleFinishedJobs(Table<Long, TaskState, Number> taskStatesByType, List<SimpleJob> inProgressJobs) {
        // for all the contexts that have finished ingestion - create new "finalize ingest" task
        Collection<Long> finishedRuns = JobTaskUtils.getFinishedRuns(inProgressJobs, taskStatesByType);
        if (finishedRuns != null && !finishedRuns.isEmpty()) {
            filesDataPersistenceService.softCommitSolrCF();
        }
        finishedRuns.forEach(this::handleAnalyzeFinalization);
    }


    private void handleAnalyzeFinalization(long runContext) {
        // mark analyze job as DONE
        try {
            boolean result = makeJobsStateTransition(runContext);
        } catch (RuntimeException e) {
            logger.error("Failed to handle analyze Finalization", e);
            //     throw e;
        }
    }

    /**
     * Change the states of ANALYZE to DONE, ANALYZE-FINALIZE to IN_PROGRESS and EXTRACT to READY in one transaction.
     *
     * @param runContext the run id for which to update these job states
     */
    private boolean makeJobsStateTransition(long runContext) {
        logger.debug("Transitioning job states of run {} to start Analyze-Finalize", runContext);

        Long analyzeJobId = jobManager.getJobIdCached(runContext, JobType.ANALYZE);

        Callable<Boolean> task = () -> {
            SimpleJob job = jobManager.getJobById(analyzeJobId);
            return job.isAcceptingTasks();
        };
        Boolean isAcceptingTasks = DBTemplateUtils.doInTransaction(task);

        if (isAcceptingTasks) {
            logger.info("accepting task flag is turned on for analysis job {} will not be stopped", analyzeJobId);
            return false;
        }

        // Update the state of the Analyze, Extract and Analyze-Finalize jobs in one transaction
        JobStateUpdateInfo updateAnalyzeStateInfo =
                new JobStateUpdateInfo(analyzeJobId, JobState.DONE, null, "Analysis completed", true);

        List<JobStateUpdateInfo> jobStateUpdateInfos = new ArrayList<>(2);

        jobStateUpdateInfos.add(updateAnalyzeStateInfo);

        Long analyzeFinJobId = jobManager.getJobIdCached(runContext, JobType.ANALYZE_FINALIZE);
        JobStateUpdateInfo updateAnalyzeFinStateInfo =
                new JobStateUpdateInfo(analyzeFinJobId, JobState.WAITING, null, "Starting analysis finalization", false);
        jobStateUpdateInfos.add(updateAnalyzeFinStateInfo);

        jobManager.updateJobStates(jobStateUpdateInfos);
        return true;
    }

    private void analyzeTasks(Collection<SimpleTask> tasks) {
        final Map<Long, List<SimpleTask>> tasksByJob = tasks.stream().collect(Collectors.groupingBy(SimpleTask::getJobId));
        tasksByJob.forEach((jobId, value) -> jobManager.setOpMsg(jobId, "Analyzing batch of " + value.size() + " items"));

        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AsyncAnalyzerManager.analyzeTasks", "findFilesContentByIds", 276);
        Set<Long> contentIds = tasks.stream().map(SimpleTask::getItemId).collect(Collectors.toSet());
        Map<Long, ContentMetadataDto> filesContentByIds = fileGroupingService.findFilesContentByIds(contentIds);
        kpiRecording.end();

        Map<Long, Boolean> isJobActive = new HashMap<>();
        tasksByJob.forEach((jobId, value) -> {
            jobManager.setOpMsg(jobId,
                    "Analyzing batch of " + value.size() + " (" +
                            // Calculate num of parts (doc/pdf=1, excel=num-of-sheets)
                            value.stream().mapToLong(t -> {
                                ContentMetadataDto cmd = filesContentByIds.get(t.getItemId());
                                if (cmd == null) {
                                    logger.warn("calc part num - task with content that is missing id {}", t.getItemId());
                                    return 0;
                                }
                                return cmd.getType() == FileType.EXCEL ? (cmd.getItemsCountL3() == null ? 0 : cmd.getItemsCountL3()) : 1;
                            }).sum() + ") items");
            isJobActive.put(jobId, jobManager.isJobActive(jobId));
        });

        // populate cache for contents
        Map<Long, Long> contentTaskList = new HashMap<>();
        tasks.forEach(task -> contentTaskList.put(task.getItemId(), task.getId()));
        contentGroupsCache.addContentsToCache(contentTaskList);

        long counter = 0L;
        for (SimpleTask task : tasks) {
            counter = analyzeSoftCommitSolrRate > 0 ? analysisCounter.incrementAndGet() : 0L;
            appServerStateService.incAnalyzeTaskRequested();
            if (!isJobActive.get(task.getJobId())) {
                break;
            }
            ContentMetadataDto contentMetadataDto = filesContentByIds.get(task.getItemId());
            if (contentMetadataDto == null) {
                logger.warn("execute task with content that is missing id {}, skip", task.getItemId());
                continue;
            }
            kpiRecording = performanceKpiRecorder.startRecording("AsyncAnalyzerManager.analyzeTasks", "analyzeContent", 300);
            analyzeBlockingExecutor.execute(() -> {
                if (!jobManager.isJobActive(task.getJobId())) {
                    return;
                }
                analyzeAppService.analyzeContent(contentMetadataDto, task, filesContentByIds);
            });
            kpiRecording.end();
        }
        logger.debug("Finished submitting page of analyze tasks ({}).", tasks.size());

        if (counter % analyzeSoftCommitSolrRate == 0) {
            logger.debug("analysis - soft commit to solr");
            filesDataPersistenceService.softCommitSolrCF();
        }
    }


    private void logJobCounters(Table<Long, TaskState, Number> taskStatesByType) {
        Set<Long> runIds = taskStatesByType.rowKeySet();
        for (Long runId : runIds) {
            Map<TaskState, Number> row = taskStatesByType.row(runId);
            if (logger.isDebugEnabled()) {
                StringBuilder sb = new StringBuilder("Analyze Job with run ID ").append(runId).append(":");
                for (Map.Entry<TaskState, Number> taskStateNumberEntry : row.entrySet()) {

                    sb.append(" ").append(taskStateNumberEntry.getKey()).append(" - ").append(taskStateNumberEntry.getValue());
                }
                logger.debug(sb.toString());
            }
        }
    }
}
