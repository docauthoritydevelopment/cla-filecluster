package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Populate and save/update content metadata object
 */
@Component
public class ContentMetadataHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(ContentMetadataHandler.class);

    @Autowired
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return true;
    }

    @Override
    public boolean handle(IngestResultContext context) {

        SolrFileEntity file = context.getFile();
        Long runId = context.getIngestResultMessage().getRunContext();
        Long taskId = context.getIngestResultMessage().getTaskId();
        ClaFilePropertiesDto fileProperties = context.getIngestResultMessage().getFileProperties();

        // create/update content metadata
        //--------------------------------------------------------------------
        ContentMetadata contentMetadata = filesDataPersistenceService.resolveContentMetadata(taskId, file,
                fileProperties, context.getIngestBatchStore(), null, null);
        logger.trace("Got for file id: {}, content: {}", file.getId(), contentMetadata);
        context.setOriginalState(contentMetadata.getState());

        if (ModelIngestUtils.isNonIngestedExcelState(context)) {
            logger.debug("File {} was not ingested due to its size.", file.getId());
        }
        // SCANNED indicates a new content was created, otherwise its something else
        else if (contentMetadata.getState().equals(ClaFileState.SCANNED)) {
            fillContentMetaData(context, contentMetadata);
            contentMetadata = filesDataPersistenceService.updateContentMetadata(contentMetadata, file, fileProperties);
            contentMetadata.setState(ClaFileState.INGESTED);
        }

        boolean wasDuplicateContent = context.getIngestBatchStore().isFileWithDuplicateContent(file.getFileId());
        boolean shouldAnalyze = !wasDuplicateContent && !FileType.SCANNED_PDF.equals(file.getType()) && !context.isOther();
        if (wasDuplicateContent) {
            contentMetadata.setPopulationDirty(true);
        }
        filesDataPersistenceService.collectMetadataToBatch(taskId, contentMetadata, fileProperties, shouldAnalyze, runId);

        context.setContentMetadata(contentMetadata);

        return true;
    }

    private void fillContentMetaData(IngestResultContext context, ContentMetadata contentMetadata) {
        if (context.isWordOrPDF()) {
            WordfileMetaData metadata = context.getWordResultMessage().getPayload().getMetadata();
            // can be null if content had already existed
            if (metadata != null) {
                ModelIngestUtils.fillContentMetaData(contentMetadata, metadata);
            }
        } else if (context.isExcel()) {
            ExcelWorkbookMetaData metadata = context.getExcelResultMessage().getPayload().getMetadata();
            // can be null if content had already existed
            if (metadata != null) {
                ModelIngestUtils.fillContentMetaData(contentMetadata, metadata);
            }
        } else if (context.isOther()) {
            OtherFile payload = context.getOtherResultMessage().getPayload();
            if (payload != null) {
                ModelIngestUtils.fillContentMetaData(contentMetadata, payload);
            }
        }
    }

}

