package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.chain.ChainLink;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.cla.common.domain.dto.messages.MessagePayloadType.SCAN_DELETION;

/**
 * Finish file create and update kvdb
 *
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class EndMapForFile implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(EndMapForFile.class);

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private EventBus eventBus;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();

        if (scanResultMessage.getPayload() instanceof ClaFilePropertiesDto) {
            ClaFilePropertiesDto propertiesDto = (ClaFilePropertiesDto) scanResultMessage.getPayload();
            String mediaItemId = Optional.ofNullable(propertiesDto.getMediaItemId()).orElse(propertiesDto.getFileName());
            MessagePayloadType payloadType = scanResultMessage.getPayloadType();
            FileEntity oldFileEntity = null;

            if (mapContext.getSolrInputDocumentFileAndFolderId() != null) { // new file
                SolrInputDocument doc = mapContext.getSolrInputDocumentFileAndFolderId().getDocFileToCreate();
                FileType fileType = propertiesDto.getType();
                if (fileType == null) {
                    fileType = FileTypeUtils.getFileType(propertiesDto.getFileName());
                }
                mapServiceUtils.handleFinishMapForNewFile(doc, propertiesDto.getClaFileId(), fileType,
                        propertiesDto.getFileName(), scanResultMessage.getRootFolderId(),
                        scanResultMessage.getRunContext(), scanResultMessage.getJobId(),
                        scanResultMessage.getTaskId(), mapContext.getBatchIdentifierNumber(), mapContext.getAttachmentsFileIDs());


                Pair<Long, Long> batchTaskId = MapServiceUtils.createBatchTaskId(mapContext.getBatchIdentifierNumber(), scanResultMessage.getTaskId());
                mapBatchResultsProcessor.collectCatFiles(batchTaskId, new FileToAdd(doc, mapContext.getFileEntity(), mediaItemId,
                        scanResultMessage.getRootFolderId(), scanResultMessage.getRunContext()));
            } else if (mapContext.getFileEntity() != null) {
                oldFileEntity = mapContext.getFileEntity();
            } else if (!payloadType.equals(SCAN_DELETION)) {
                oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mediaItemId);
            }

            if (oldFileEntity != null) {
                oldFileEntity.setLastScanned(mapContext.getTime());
                oldFileEntity.setDeleted(false);
                mapBatchResultsProcessor.collectCatFiles(MapServiceUtils.createBatchTaskId(mapContext.getBatchIdentifierNumber(), scanResultMessage.getTaskId()),
                        new FileToAdd(null, oldFileEntity, mediaItemId, scanResultMessage.getRootFolderId(), scanResultMessage.getRunContext()));

                if (propertiesDto.isFile()) {
                    if (propertiesDto.getType() == null) {
                        propertiesDto.setType(FileTypeUtils.getFileType(propertiesDto.getFileName()));
                    }
                    int meaningfulCounter = propertiesDto.getType().isMeaningfulFile() ? 1 : 0;
                    eventBus.broadcast(Topics.MAP, Event.builder()
                            .withEventType(EventType.JOB_PROGRESS)
                            .withEntry(EventKeys.JOB_ID, mapContext.getScanResultMessage().getJobId())
                            .withEntry(EventKeys.MEANINGFUL_ITEM_COUNT, meaningfulCounter)
                            .withEntry(EventKeys.ITEM_COUNT, 1).build());
                }
            }
        }

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        return true;
    }
}
