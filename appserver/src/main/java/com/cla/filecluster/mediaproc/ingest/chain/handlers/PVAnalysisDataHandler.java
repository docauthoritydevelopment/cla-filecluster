package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.pv.PropertyVector;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.repository.PvEntriesIngesterRepository;
import com.cla.filecluster.service.chain.ChainLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Save PV analysis data in SOLR
 */
@Component
public class PVAnalysisDataHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(PVAnalysisDataHandler.class);

    @Autowired
    protected PvEntriesIngesterRepository pvEntriesIngester;

    @Value("${ingest.save-pvs-directly:false}")
    private boolean savePvs;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return savePvs && !context.isOther();
    }

    @Override
    public boolean handle(IngestResultContext context) {

        SolrFileEntity file = context.getFile();
        Long taskId = context.getIngestResultMessage().getTaskId();

        // save PV analysis data in SOLR
        //--------------------------------------------------------------------
        PvAnalysisData pvAnalysisData = context.getPvAnalysisData();
        if (pvAnalysisData != null) {
            Long savedContentId = context.getSavedContentId();
            if (context.isWordOrPDF()) {
                pvEntriesIngester.saveHistograms(taskId, file.getFullName(), savedContentId,
                        PropertyVector.DEF_SYS_PART, pvAnalysisData.getDefaultPvCollection(), context.getMaturityLevel());
            }
            else if (context.isExcel()) {
                pvAnalysisData.getPvCollections().getCollections()
                        .forEach((key, value) ->
                                pvEntriesIngester.saveHistograms(taskId, file.getFullName(), savedContentId,
                                        key, value, context.getMaturityLevel()));
            }
            logger.trace("Saved file with name: {} and ID {} to PVs repository.", file.getFullName(), file.getId());
        }


        return true;
    }

}
