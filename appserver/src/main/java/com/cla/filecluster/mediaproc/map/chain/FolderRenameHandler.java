package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.MapRenameHandler;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Responsible for folder rename (executing rename both fileshare and changed based rescans).
 */
@Component
public class FolderRenameHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(FolderRenameHandler.class);

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MapRenameHandler mapRenameHandler;

    @Autowired
    private EventBus eventBus;

    @Override
    public boolean handle(MapResultContext context) {
        boolean shouldHandleFurther = true;
        ScanResultMessage scanResultMessage = context.getScanResultMessage();
        PhaseMessagePayload payload = scanResultMessage.getPayload();
        Long runContext = scanResultMessage.getRunContext();
        Long rootFolderId = scanResultMessage.getRootFolderId();
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
        if (isRemoteFolderChange(rootFolder, payload)) {
            logger.debug("{} rename for scanned folder {}", rootFolder.getMediaType(), payload);
            mapRenameHandler.handleFolderMove((MediaChangeLogDto) payload, runContext, rootFolder.getId(), System.currentTimeMillis());
            shouldHandleFurther = false;
        } else if (payload instanceof ClaFilePropertiesDto && rootFolder.getMediaType() != MediaType.EXCHANGE_365) {
            ClaFilePropertiesDto scanPayload = (ClaFilePropertiesDto) payload;
            String path = rootFolder.getRealPath() != null && scanPayload.getFileName().startsWith(rootFolder.getRealPath()) ?
                    scanPayload.getFileName().substring(rootFolder.getRealPath().length()) : scanPayload.getFileName();
            if (!path.endsWith("/")) {
                path += "/";
                scanPayload.setFileName(path);
            }
            docFolderService.createDocFolderIfNeededWithCache(rootFolder.getId(), path);
            logger.debug("{} rename for scanned folder {}", rootFolder.getMediaType(), payload);
            shouldHandleFurther = false;
        }

        if (!shouldHandleFurther) {
            eventBus.broadcast(Topics.MAP, Event.builder()
                    .withEventType(com.cla.eventbus.events.EventType.RUN_PROGRESS)
                    .withEntry(EventKeys.RUN_ID, runContext)
                    .withEntry(EventKeys.FILE_METADATA_UPDATE, 1).build());
        }
        return shouldHandleFurther;
    }

    private boolean isRemoteFolderChange(RootFolder rootFolder, PhaseMessagePayload payload) {
        // Not relevant cause it's handled in the PreProcessHandler
        if (rootFolder.getMediaType() == MediaType.EXCHANGE_365) {
            return false;
        }
        return rootFolder.getMediaType().isSupportsNativeChangeLog() && payload instanceof MediaChangeLogDto
                && ((MediaChangeLogDto) payload).isFolder();
    }

    @Override
    public boolean shouldHandle(MapResultContext context) {
        final PhaseMessagePayload payload = context.getScanResultMessage().getPayload();
        return (payload instanceof ClaFilePropertiesDto) &&
                ((ClaFilePropertiesDto) payload).isFolder() &&
                context.getDiffType() == DiffType.RENAMED;
    }
}
