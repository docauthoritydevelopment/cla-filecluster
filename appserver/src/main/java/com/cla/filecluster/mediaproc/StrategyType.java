package com.cla.filecluster.mediaproc;

import com.cla.filecluster.service.producers.*;

public enum StrategyType {
    SIMPLE(SimpleTasksStrategy.class),
    FAIR(FairTaskProducingStrategy.class),
    MANUAL_EXCLUSIVE(ManualExclusiveFairStrategy.class),
    AUTO_EXCLUSIVE(AutoExclusiveFairStrategy.class);

    private Class<? extends TaskProducingStrategy> strategyClass;

    StrategyType(Class<? extends TaskProducingStrategy> strategyClass) {
        this.strategyClass = strategyClass;
    }

    public Class<? extends TaskProducingStrategy> getStrategyClass() {
        return strategyClass;
    }
}
