package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.*;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.kvdb.domain.FileEntityUtils;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.cla.common.domain.dto.messages.MessagePayloadType.*;

/**
 * Handle map of a new file
 * <p>
 * Created by: yael
 * Created on: 8/5/2019
 */
@Service
public class NewFileHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(NewFileHandler.class);

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MapAddHandler mapAddHandler;

    @Autowired
    private EventBus eventBus;

    @Override
    public boolean handle(MapResultContext mapContext) {
        processFirstScan(mapContext);
        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        MessagePayloadType payloadType = mapContext.getScanResultMessage().getPayloadType();
        List<MessagePayloadType> types = Lists.newArrayList(SCAN_CHANGES, SCAN_PST_ENTRY, SCAN, EXCHANGE365_ITEM_CHANGE);
        if (!types.contains(payloadType)) {
            logger.trace("payload type irrelevant {}, skip", mapContext);
            return false;
        }

        if (mapContext.getFstScan()) {
            logger.trace("first scan for file {}, handle", mapContext);
            return true;
        }

        RootFolder rootFolder = docStoreService.getRootFolderCached(mapContext.getScanResultMessage().getRootFolderId());
        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(rootFolder.getId(), mapContext.getKvdbKey());


        switch (mapContext.getDiffType()) {
            case NEW:
            case CREATED_UPDATED:
                logger.trace("first scan for file {}, handle", mapContext);
                return true;
            case ACL_UPDATED:
                logger.trace("diff type irrelevant {}, skip", mapContext);
                return false;
            case RENAMED:
                if (mapContext.getNewFile()) {
                    boolean shouldHandle = mapContext.getBelongToRF() && !mapContext.getAlreadyHandled();
                    logger.trace("diff type for file {}, should handle {}", mapContext, shouldHandle);
                    return shouldHandle;
                }
                logger.trace("diff type irrelevant {}, skip", mapContext);
                return false;
            case CONTENT_UPDATED:
                logger.trace("diff type irrelevant {}, skip", mapContext);
                return false;
            case UNDELETED:
                return oldFileEntity == null;
            case SIMILAR:
            default:
                logger.trace("diff type irrelevant {}, skip", mapContext);
                return false;
        }
    }

    /**
     * Process scan result message on the first scan
     */
    private void processFirstScan(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        long rootFolderId = scanResultMessage.getRootFolderId();
        long runContext = scanResultMessage.getRunContext();

        ClaFilePropertiesDto propertiesDto = (ClaFilePropertiesDto) scanResultMessage.getPayload();
        ItemToCreate solrInputDocumentFileAndFolderId = mapAddHandler.addScannedItem(rootFolderId, runContext, propertiesDto);

        SolrInputDocument doc = solrInputDocumentFileAndFolderId.getDocFileToCreate();
        Long folderId = solrInputDocumentFileAndFolderId.getFolderId();
        if (doc == null && folderId == null) {
            return; //Skipped item, or scan error
        }

        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);
        String pathForKvdb = null;
        if (!rf.getMediaType().isPathAsMediaEntity()) {
            pathForKvdb = propertiesDto.getFileName();
        }

        FileEntity fileEntity = FileEntityUtils.createFileEntity(propertiesDto.getClaFileId(), folderId, mapContext.getTime(), propertiesDto, pathForKvdb);
        mapContext.setSolrInputDocumentFileAndFolderId(solrInputDocumentFileAndFolderId);
        mapContext.setFileEntity(fileEntity);
        mapContext.setFileId(propertiesDto.getClaFileId());

        if (folderId == null) {
            folderId = (Long) MapServiceUtils.getFieldValueFromSolrInputDoc(doc, CatFileFieldType.FOLDER_ID);
        }
        mapContext.setFolderId(folderId);

        if (mapContext.getDiffType() == DiffType.NEW || mapContext.getDiffType() == DiffType.CREATED_UPDATED) {
            broadcastNewMappedItems(runContext, propertiesDto, rf, fileEntity,
                    solrInputDocumentFileAndFolderId.getAttachmentsToCreate());
        }
    }

    private void broadcastNewMappedItems(long runContext, ClaFilePropertiesDto propertiesDto,
                                         RootFolder rf, FileEntity fileEntity,
                                         Map<String, FileCatBuilder> attachmentsToCreate) {
        logger.trace("Broadcasting new file event for {}", propertiesDto.getFileName());
        eventBus.broadcast(Topics.MAP, Event.builder()
                .withEventType(EventType.RUN_PROGRESS)
                .withEntry(EventKeys.RUN_ID, runContext)
                .withEntry(EventKeys.ROOT_FOLDER_ID, rf.getId())
                .withEntry(EventKeys.FILE_MEDIA_TYPE, rf.getMediaType())
                .withEntry(EventKeys.NEW_FILE, 1)
                .withEntry(EventKeys.IS_A_FILE, fileEntity.isFile())
                .withEntry(EventKeys.NEW_FILE_PAYLOAD, propertiesDto)
                .build());
        if (attachmentsToCreate == null) {
            logger.trace("No attachments associated with the new file {}, not broadcasting count event",
                    propertiesDto.getFileName());
            return;
        }

        // Not making a single broadcast because only few attachments are expected on a single mail.
        logger.trace("Broadcasting {} new attachments event for mail item {}",
                attachmentsToCreate.values().size(), propertiesDto.getFileName());
        attachmentsToCreate.values().forEach(attachment -> eventBus.broadcast(Topics.MAP, Event.builder()
                .withEventType(EventType.RUN_PROGRESS)
                .withEntry(EventKeys.RUN_ID, runContext)
                .withEntry(EventKeys.ROOT_FOLDER_ID, rf.getId())
                .withEntry(EventKeys.FILE_MEDIA_TYPE, rf.getMediaType())
                .withEntry(EventKeys.NEW_FILE, 1)
                .withEntry(EventKeys.IS_A_FILE, true)
                .withEntry(EventKeys.NEW_FILE_PAYLOAD, attachment.getProps())
                .build()));
    }
}
