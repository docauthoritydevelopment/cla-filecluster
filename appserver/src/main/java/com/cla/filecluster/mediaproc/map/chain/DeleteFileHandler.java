package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.filecluster.mediaproc.map.MapDeleteHandler;
import com.cla.filecluster.service.chain.ChainLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.cla.common.domain.dto.messages.MessagePayloadType.SCAN_DELETION;

/**
 * Handle file deletion
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class DeleteFileHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(DeleteFileHandler.class);

    @Autowired
    private MapDeleteHandler mapDeleteHandler;

    @Override
    public boolean handle(MapResultContext mapContext) {

        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        ItemDeletionPayload payload = ((ItemDeletionPayload) scanResultMessage.getPayload());
        logger.trace("Handle deleted file {} for task {}", payload.getFilename(),
                scanResultMessage.getTaskId());
        mapDeleteHandler.handleDeletedFiles(scanResultMessage.getRunContext(), scanResultMessage.getTaskId(),
                scanResultMessage.getRootFolderId(), mapContext.getBatchIdentifierNumber(), payload.getFilename());
        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        MessagePayloadType payloadType = mapContext.getScanResultMessage().getPayloadType();
        boolean shouldHandle = payloadType.equals(SCAN_DELETION);
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
