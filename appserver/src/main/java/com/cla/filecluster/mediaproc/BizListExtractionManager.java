package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.crawler.RunType;
import com.cla.common.domain.dto.jobmanager.*;
import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.extraction.ExtractionRule;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.repository.jpa.entitylist.SimpleBizListItemRepository;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.entitylist.BizListExtractionService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.util.JobTaskUtils;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Created by vladi on 5/4/2017.
 */
@Service
public class BizListExtractionManager implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(BizListExtractionManager.class);

    private final static String RUN_EXTRACT_SCHEDULE_KEY = "runScheduledExtract";

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private BizListService bizListService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private BizListAppService bizListAppService;

    @Autowired
    private BizListExtractionService bizListExtractionService;

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private BlockingExecutor extractionBlockingExecutor;

    @Autowired
    private SystemSettingsService systemSettingsService;

    @Autowired
    private SimpleBizListItemRepository simpleBizListItemRepository;

    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Value("${extraction.bizlist.run:true}")
    private boolean runBizListExtract;

    @Value("${create-run-extract.initial-delay-millis:600000}")
    private long runBizListExtractInitDelay;

    @Value("${create-run-extract.frequency-millis:86400000}")
    private long runBizListExtractFrequency;

    private TimeSource timeSource;

    private AtomicBoolean finalizationInProgress = new AtomicBoolean(false);

    private LoadingCache<Long, List<ExtractionRule>> rulesCache;

    private long nextExtractRunTime = 0;
    private int tasksQueueLimit = 200;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @PostConstruct
    public void init() {
        timeSource = new TimeSource();

        rulesCache = CacheBuilder.newBuilder()
                .maximumSize(500)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, List<ExtractionRule>>() {
                    public List<ExtractionRule> load(@NotNull Long bizListId) {
                        BizList bizList = bizListService.getBizListById(bizListId, true);
                        return bizListExtractionService.extractRules(bizList);
                    }
                });

        nextExtractRunTime = System.currentTimeMillis() + runBizListExtractInitDelay;
    }

    public void setShouldRunNextExtractFlag(boolean flag) {
        systemSettingsService.updateOrCreateSystemSettings(RUN_EXTRACT_SCHEDULE_KEY, String.valueOf(flag));
    }

    public BizListScheduleRunStatus getBizListSchedulingState() {
        BizListScheduleRunStatus state = new BizListScheduleRunStatus();

        CrawlRun run = crawlRunRepository.getLatestExtractRun();

        state.setEveryInMillis(runBizListExtractFrequency);
        state.setLastRunTime(run == null ? null : run.getStartTimeAsLong());
        state.setNextRunTime(nextExtractRunTime);
        state.setSchedulingFlagOn(runBizListExtract && shouldRunNextExtractFlag());

        return state;
    }

    private boolean shouldRunNextExtractFlag() {
        List<SystemSettingsDto> result = systemSettingsService.getSystemSettings(RUN_EXTRACT_SCHEDULE_KEY);
        if (result == null || result.isEmpty()) {
            return true;
        }
        String flagValue = result.get(0).getValue();
        return Strings.isNullOrEmpty(flagValue) || Boolean.parseBoolean(flagValue);
    }

    @Scheduled(initialDelayString = "${create-run-extract.initial-delay-millis:600000}",
            fixedDelayString = "${create-run-extract.frequency-millis:86400000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @Transactional
    public void createExtractRunScheduled() {
        nextExtractRunTime = System.currentTimeMillis() + runBizListExtractFrequency;
        if (runBizListExtract && shouldRunNextExtractFlag()) {
            if (!componentsAppService.isSolrUp()) {
                logger.info("solr seems to be down, try again later");
                return;
            }
            createExtractRun(false);
        } else {
            logger.info("extract is off, will not schedule a new run for active biz lists");
        }
    }

    @Transactional
    public boolean createExtractRun(boolean isManual) {
        if (!extractRunAlreadyExists()) {
            createRunAndJobsForAllBizLists(isManual);
            logger.info("created new extract run for all active biz lists");
            return true;
        } else {
            logger.info("did not create new extract run - one already exists");
            return false;
        }
    }

    @AutoAuthenticate
    @Scheduled(initialDelayString =  "${bizlist.tasks-polling-initial-delay-millis:5000}", fixedRateString = "${bizlist.tasks-polling-rate-millis}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void processNextTasksGroup() {
        if (!isSchedulingActive) {
            return;
        }
        try {
            boolean newJobsCreated = processNewJobs();

            if (newJobsCreated) {
                logger.debug("New extraction jobs were found, tasks were created.");
            }

            List<SimpleJob> inProgressJobs = jobManager.getJobs(JobType.EXTRACT, JobState.IN_PROGRESS);
            if (inProgressJobs.isEmpty()) {
                // avoid calling getTaskStatesByType (which involves a pricey query) if there are no in-progress jobs
                return;
            }
            Table<Long, TaskState, Number> taskStatesByTypePerInProgressJob = jobManager.getTaskStatesByTypePerJob(TaskType.EXTRACT_BIZ_LIST_ITEM);
            updateJobCounters(taskStatesByTypePerInProgressJob);

            Table<Long, TaskState, Number> taskStatesByTypePerRun = jobManager.getTaskStatesByType(TaskType.EXTRACT_BIZ_LIST_ITEM);
            Collection<SimpleTask> tasks = nextTaskGroup(taskStatesByTypePerRun);

            if (tasks.size() > 0) {
                List<Long> taskIds = tasks.stream().map(Task::getId).collect(Collectors.toList());
                try {
                    jobManager.updateTasksStatus(taskIds, TaskState.ENQUEUED);
                    extractTasks(tasks);
                } catch (Exception e) {
                    logger.error("Failed to handle analyze tasks", e);
                    jobManager.updateTasksStatus(taskIds, TaskState.FAILED);
                }
            }


            handleFinishedJobs(taskStatesByTypePerInProgressJob);


        } catch (Exception e) {
            logger.error("Failed to process extraction jobs.", e);
        }
    }

    private void handleFinishedJobs(Table<Long, TaskState, Number> taskStatesByTypePerJob) {
        Collection<Long> finishedJobIds = JobTaskUtils.getFinishedRuns(taskStatesByTypePerJob);
        if (finishedJobIds.size() > 0) {
            if (finalizationInProgress.get()) {
                logger.info("Finalize analysis currently in progress");
            } else {
                finalizationInProgress.set(true);
                try {
                    logger.debug("Handling extract finalize for the following jobs: {}", finishedJobIds);
                    finishedJobIds.forEach(this::handleExtractFinalization);
                } finally {
                    finalizationInProgress.set(false);
                }
            }
        }
    }

    private void handleExtractFinalization(long finishedExtractJobId) {

        SimpleJobDto extractionJobDto = jobManager.getJobAsDto(finishedExtractJobId);
        Long runContext = extractionJobDto.getRunContext();
        long bizListId = extractionJobDto.getItemId();
        try {
            logger.debug("Handle Extract Finalization on job {}", finishedExtractJobId);

            List<JobStateUpdateInfo> jobStateUpdateInfos = new ArrayList<>(2);

            JobStateUpdateInfo updateExtractStateInfo =
                    new JobStateUpdateInfo(finishedExtractJobId, JobState.DONE, null, "Extract completed", true);
            jobStateUpdateInfos.add(updateExtractStateInfo);

            SimpleJob extractFinJob = jobManager.getJobs(runContext, JobType.EXTRACT_FINALIZE, bizListId).get(0);
            JobStateUpdateInfo updateExtractFinStateInfo =
                    new JobStateUpdateInfo(extractFinJob.getId(), JobState.IN_PROGRESS, null, "Starting Extract finalization", false);

            jobStateUpdateInfos.add(updateExtractFinStateInfo);

            Map<TaskState, Number> existingExtFinTasks = jobManager.getTaskStatesByJobId(
                    extractFinJob.getId(), extractFinJob.getTasksPartition());
            if (existingExtFinTasks == null || existingExtFinTasks.isEmpty()) {
                jobManager.createTask(JobManagerService.simpleJobToDto(extractFinJob), TaskType.EXTRACT_FINALIZE);
            }

            jobManager.updateJobStates(jobStateUpdateInfos);
        } catch (RuntimeException e) {
            logger.error("Failed to start extract job {} done ", finishedExtractJobId, e);
        }
    }

    private void updateJobCounters(Table<Long, TaskState, Number> taskStatesByTypePerInProgressJob) {
        Set<Long> jobIds = taskStatesByTypePerInProgressJob.rowKeySet();
        for (Long jobId : jobIds) {
            Map<TaskState, Number> row = taskStatesByTypePerInProgressJob.row(jobId);
            jobManager.updateJobCounters(jobId, row);
        }
    }

    private String validateStartExtractRunForBizList(long bizListId) {
        licenseService.validateCrawlerExpiry();

        BizList list = bizListService.findById(bizListId);
        if (list == null) {
            return "Request start extract for business list that does not exists. Id: " + bizListId;
        }
        if (list.getExtractionSessionId() != 0) {
            CrawlRun crawlRun = fileCrawlerExecutionDetailsService.getCrawlRun(list.getExtractionSessionId());
            if (RunStatus.RUNNING.equals(crawlRun.getRunStatus())) {
                return "Request start extract for business list that is currently RUNNING " + list.getName();

            }
        }

        return null;
    }

    @Transactional
    public void startExtractRunForBizListAsync(long bizListId) {
        String error = validateStartExtractRunForBizList(bizListId);
        if (error != null) {
            logger.info(error);
            return;
        }
        BizList list = bizListService.findById(bizListId);
        Long runId = createExtractRun(list.getName(), true);
        createJobsForList(runId, list);
    }

    private Long createExtractRun(String name, boolean isManual) {
        CrawlRun crawlRun = new CrawlRun();
        crawlRun.setRunStatus(RunStatus.RUNNING);
        crawlRun.setStartTime(new Date(timeSource.currentTimeMillis()));
        crawlRun.setManual(isManual);
        crawlRun.setExtractFiles(true);
        crawlRun.setHasChildRuns(false);
        crawlRun.setRunType(RunType.BIZ_LIST);
        crawlRun.setStateString(name);
        crawlRun.setRootFolderAccessible(true);
        CrawlRun savedRun = crawlRunRepository.save(crawlRun);
        return savedRun.getId();
    }

    private void createJobsForList(Long runId, BizList list) {
        jobManager.createGenericJob(runId, JobType.EXTRACT, "Extract business list " + list.getName(),
                JobState.READY, list.getId());
        jobManager.createGenericJob(runId, JobType.EXTRACT_FINALIZE, "Extract finalize business list " + list.getName(),
                JobState.NEW, list.getId()); //Job itemId holds the bizListId to extract
    }

    private boolean extractRunAlreadyExists() {
        List<CrawlRun> runs = crawlRunRepository.findExtractByStates(Arrays.asList(RunStatus.RUNNING, RunStatus.PAUSED));
        return (runs != null && runs.size() > 0);
    }

    private void createRunAndJobsForAllBizLists(boolean isManual){
        List<BizList> bizLists = bizListService.findActiveBizLists();

        if (bizLists != null && bizLists.size() > 0) {
            Long runId = createExtractRun("Extract business list", isManual);
            for (BizList list : bizLists) {
                createJobsForList(runId, list);
            }
        } else {
            logger.info("no active biz list - do not create extract run");
        }
    }

    /**
     * Find all the NEW/READY extraction jobs, de-duplicate, create tasks
     *
     * @return true if new jobs were created
     */
    private boolean processNewJobs() {
        // de-duplicate ready jobs in case there are existing in-progress jobs for the same BizLists
        List<SimpleJob> readyJobs = jobManager.getJobs(JobType.EXTRACT, JobState.READY);
        if (readyJobs.size() > 0) {
            List<SimpleJob> inProgressJobs = jobManager.getJobs(JobType.EXTRACT, JobState.IN_PROGRESS);
            Set<Long> inProgressBizListIds = inProgressJobs.stream().map(SimpleJob::getItemId).collect(Collectors.toSet());

            Map<Long, SimpleJob> bizListIdToJobsToProcess = new HashMap<>();
            List<SimpleJob> redundantJobsToSkip = new LinkedList<>();
            List<SimpleJob> noDataToExtractJobs = new LinkedList<>();

            for (SimpleJob readyJob : readyJobs) {
                try {
                    long bizListId = readyJob.getItemId();
                    if (inProgressBizListIds.contains(bizListId) || bizListIdToJobsToProcess.containsKey(bizListId)) {
                        redundantJobsToSkip.add(readyJob);
                    } else if (extractJobHasDataToExtract(readyJob)) {
                        bizListIdToJobsToProcess.put(bizListId, readyJob);
                    } else {
                        noDataToExtractJobs.add(readyJob);
                    }
                } catch (Exception e) {
                    logger.error("failed testing extract ready job {}", readyJob.getId(), e);
                }
            }

            Map<Long, Number> tasksByJob = jobManager.getTaskStatesByTypePerJob(TaskType.EXTRACT_BIZ_LIST_ITEM,
                    bizListIdToJobsToProcess.values().stream().map(Job::getId).collect(Collectors.toList()));
            bizListIdToJobsToProcess.values().forEach(job -> {
                if (jobManager.isJobActive(job.getId())) { // In case job was PAUSED
                    try {
                        BizList bizList = bizListService.getBizListById(job.getItemId(), true);
                        bizListService.updateBizListExtractionSession(bizList.getId(), job.getRunContext());
                        if (!tasksByJob.containsKey(job.getId()) || tasksByJob.get(job.getId()).intValue() == 0) {
                            createBizListTasks(job.getRunContext(), job, bizList); //TODO: create this ASYNC
                        }
                        jobManager.updateJobState(job.getId(), JobState.IN_PROGRESS, null, "Initializing process", false);
                    } catch (Exception e) {
                        logger.error("failed handling start extract for job {}", job.getId(), e);
                    }
                }
            });
            redundantJobsToSkip.forEach(job ->
                    setExtractionAsSkipped(job, "Joining with extraction for this business list that is already in progress"));
            noDataToExtractJobs.forEach(job ->
                    setExtractionAsSkipped(job, "No data to extract"));
            return bizListIdToJobsToProcess.size() > 0;
        }
        return false;

    }

    private boolean extractJobHasDataToExtract(SimpleJob extractJob) {
        Long runId = extractJob.getRunContext();
        CrawlRun run = fileCrawlerExecutionDetailsService.getCrawlRun(runId);
        Long rootFolderId = run.getRootFolderId();
        // Allowing EXTRACT job for null root folder id for the case of 'global' EXTRACT
        return rootFolderId == null || jobManager.rootFolderJobWasDone(rootFolderId, JobType.INGEST);
    }

    private void setExtractionAsSkipped(SimpleJob job, String opMsg) {
        logger.debug("set extraction as skipped for job {} and run {}", job.getId(), job.getRunContext());
        jobManager.updateJobState(job.getId(), JobState.DONE, JobCompletionStatus.SKIPPED, opMsg, true);
        SimpleJob extractFinJob = jobManager.getJobs(job.getRunContext(), JobType.EXTRACT_FINALIZE, job.getItemId()).get(0);
        jobManager.updateJobState(extractFinJob.getId(), JobState.DONE, JobCompletionStatus.CANCELLED, "", true);
        fileCrawlerExecutionDetailsService.handleFinishedRun(job.getRunContext(), PauseReason.SYSTEM_INITIATED);
    }


    private void createBizListTasks(long runId, SimpleJob job, BizList bizList) {
        try {
            // if job partition does not exist for some reason - fix
            jobManager.fixPartitionIfNeeded(job);

            final long bizListId = bizList.getId();
            logger.debug("Clear previous extraction on bizList {}", bizListId);
            bizListAppService.clearPreviousExtraction(bizListId, null);

            List<String> itemIds = bizListItemService.getActiveSimpleBizListItemIds(bizListId);
            jobManager.setOpMsg(job.getId(), "Creating tasks");
            logger.debug("Creating tasks for bizList jobId {}", job.getId());

            if (itemIds.size() == 0) {
                setExtractionAsSkipped(job, "No active items found in this list");
                return;
            }

            List<String> itemIdJsons = itemIds.stream().map(this::itemIdToJson).collect(Collectors.toList());
            jobManager.createTasksBatch(job, bizListId, TaskType.EXTRACT_BIZ_LIST_ITEM, itemIdJsons);

        } catch (RuntimeException e) {
            logger.error("Failed to create extract job tasks jobId {} Exception {}", job, e);
            jobManager.handleFailedFinishJob(job.getId(), "Extract failed");
            fileCrawlerExecutionDetailsService.handleFinishedRun(runId, PauseReason.SYSTEM_ERROR);
            throw e;
        }
    }


    private void extractTasks(Collection<SimpleTask> tasks) {
        // Reflect extract jobs started in the UI via jobManager.setOpMsg
        tasks.stream()
                .mapToLong(SimpleTask::getJobId)
                .distinct()
                .forEach(jobId -> jobManager.setOpMsg(jobId, "Fetching business list data"));

        //   final Map<Long, List<SimpleTask>> tasksByJob = tasks.stream().collect(Collectors.groupingBy(SimpleTask::getJobId));
        List<String> bizListItemIds = tasks.stream().map(t -> jsonToItemId(t.getJsonData())).collect(Collectors.toList());
        //TODO Liora: improve query performance and building map
        List<SimpleBizListItem> simpleBizListItems = simpleBizListItemRepository.findItemsWithAliases(bizListItemIds);
        HashMap<Long, SimpleBizListItem> taskIdToSimpleBizListItemMap = new HashMap<>();
        simpleBizListItems.forEach(item ->
        {
            SimpleTask task = tasks.stream().filter(t -> jsonToItemId(t.getJsonData()).equals(item.getSid())).findFirst().get();
            taskIdToSimpleBizListItemMap.put(task.getId(), item);
        });

        for (SimpleTask task : tasks) {
            if (!jobManager.isJobActive(task.getJobId())) {
                break;
            }
            extractionBlockingExecutor.execute(() -> {
                try {
                    Long bizListId = task.getItemId();
                    List<ExtractionRule> extractionRules = rulesCache.getUnchecked(bizListId);
                    BizList bizList = bizListService.getBizListById(bizListId, true);
                    SimpleBizListItem simpleBizListItem = taskIdToSimpleBizListItemMap.get(task.getId());
                    Long bizListUpdateDate = bizList.getLastUpdateTimeStamp() != null ? bizList.getLastUpdateTimeStamp() : bizList.getCreationTimeStamp();
                    boolean isBizListNewerThenLastScan = bizList.getLastUpdateTimeStamp() != null && bizList.getLastSuccessfulRunDate() != null &&
                            bizListUpdateDate > bizList.getLastSuccessfulRunDate();
                    Date contentModifiedFilterDate = bizList.getLastSuccessfulRunDate() == null ? null : isBizListNewerThenLastScan ? null : (new Date(bizList.getLastSuccessfulRunDate()));
                    //TODO 2.0: Query only changed bizList items
                    jobManager.updateTaskStatus(task.getId(), TaskState.CONSUMING);
                    jobManager.setOpMsg(task.getJobId(), "Processing item: " + simpleBizListItem.getName());
                    bizListAppService.processExtractionsForSingleBizListItem(bizList, extractionRules,
                            contentModifiedFilterDate, contentModifiedFilterDate != null, simpleBizListItem,
                            () -> jobManager.updateTaskStatus(task.getId(), TaskState.DONE));
                } catch (Exception e) {
                    logger.error("Failed to extract bizList task {}", task, e);
                    jobManager.updateTaskStatus(task.getId(), TaskState.FAILED);
                }
            });
        }
    }


    private String itemIdToJson(String itemId) {
        return "{\"item-id\":\"" + itemId + "\"}";
    }

    private String jsonToItemId(String itemIdJson) {
        String itemId = itemIdJson.substring(0, itemIdJson.length() - 1).split(":")[1].trim();
        if (itemId.startsWith("\"")) {
            itemId = itemId.substring(1);
        }
        if (itemId.endsWith("\"")) {
            itemId = itemId.substring(0, itemId.length() - 1);
        }
        return itemId;
    }

    private Collection<SimpleTask> nextTaskGroup(Table<Long, TaskState, Number> taskStatesByType) {
        Map<Long, Number> newTasksCount = taskStatesByType.column(TaskState.NEW);
        List<SimpleTask> tasks = Lists.newLinkedList();
        if (newTasksCount.size() == 0) {
            //Nothing to do.
            return tasks;
        }
        Map<Long, Number> enqueuedTasksCount = taskStatesByType.column(TaskState.ENQUEUED);
        int totalInProgress = enqueuedTasksCount.values().stream().mapToInt(Number::intValue).sum();

        int queueLimit = tasksQueueLimit - totalInProgress;
        if (queueLimit <= 0) {
            //Queue is currently full
            logger.debug("Extract Queue is currently full ({} in progress)", totalInProgress);
            return tasks;
        }
        for (Long contextId : newTasksCount.keySet()) {
            List<SimpleTask> tasksByContext = jobManager.getTasksByContext(TaskType.EXTRACT_BIZ_LIST_ITEM, TaskState.NEW, contextId, queueLimit);
            tasks.addAll(tasksByContext);
            queueLimit -= tasksByContext.size();
            if (queueLimit <= 0) {
                break;
            }
        }
        logger.debug("Fetched {} tasks for extract", tasks.size());
        return tasks;
    }
}
