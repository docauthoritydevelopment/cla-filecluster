package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.service.chain.ChainLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Error handler for map chain
 *
 * Created by: yael
 * Created on: 8/8/2019
 */
@Service
public class MapErrorHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(MapErrorHandler.class);

    @Autowired
    private EventBus eventBus;

    @Override
    public boolean handle(MapResultContext mapContext) {
        try {
            ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();

            eventBus.broadcast(Topics.MAP, Event.builder()
                    .withEventType(EventType.JOB_PROGRESS)
                    .withEntry(EventKeys.JOB_ID, scanResultMessage.getJobId())
                    .withEntry(EventKeys.FILE_MAP_FAIL, 1).build());
        } catch (Exception e) {
            logger.error("error handle failed with exception for {}", mapContext, e);
        }
        return false;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        return true;
    }
}
