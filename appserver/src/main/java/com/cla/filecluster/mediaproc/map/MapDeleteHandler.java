package com.cla.filecluster.mediaproc.map;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.kvdb.FileEntityStore;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
@Service
public class MapDeleteHandler {

    private static final Logger logger = LoggerFactory.getLogger(MapDeleteHandler.class);

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private ScanDiffEventProvider scanDiffEventProvider;

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private MapExchangeServices mapExchangeServices;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    public void handleDeletedFiles(long runId, Long taskId, long rootFolderId, Long batchIdentifierNumber, String filename) {
        String storeName = mapServiceUtils.getStoreName(rootFolderId);
        Table<Long, String, FileEntity> fileEntitiesToDeleteTable = HashBasedTable.create();
        kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {
            FileEntity fileEntity = fileEntityStore.get(filename);
            logger.trace("Handle deleted file id: {}, path: {} for task {}", fileEntity.getFileId(),
                    fileEntity.getPath(), taskId);
            // Drop file entity last separator (i.e. d:/something.zip/ will be d:/something.zip)
            String filenamePath = FileNamingUtils.getFilenamePath(filename);
            if (FileNamingUtils.isPackage(filenamePath)) { // Case of packaged files collect all its entries for deletion too.
                collectFilesToDeleteFromPackage(fileEntityStore, filenamePath, fileEntitiesToDeleteTable);
                logger.debug("**** Deleted file {} is packaged, going also to delete its {} entries", filenamePath, fileEntitiesToDeleteTable.columnKeySet().size());
            } else if (fileEntity == null) { // Just collect the file itself
                logger.warn("Received deletion request on file with path {}, " +
                        "but could not find such file in KVDB on AppServer side.", filename);
            } else {
                if (fileEntity.isDeleted()) {
                    logger.trace("handle file already marked for deletion id {} name {}", fileEntity.getFileId(), filename);
                    return true;
                }
                if (fileEntity.isFolder()) {
                    logger.trace("handle folder deletion id {} name {}", fileEntity.getFolderId(), filename);
                    removeFilesFromFolder(batchIdentifierNumber, runId, taskId, rootFolderId, fileEntity.getFolderId());
                    collectDeletedFolder(runId, taskId, rootFolderId, batchIdentifierNumber, filename, fileEntity);
                }
                if (fileEntity.isFile()) {
                    logger.trace("handle file deletion id {} name {}", fileEntity.getFileId(), filename);
                    fileEntitiesToDeleteTable.put(fileEntity.getFileId(), filename, fileEntity);
                }
            }
            return true;
        });
        handleDeletedFiles(runId, taskId, rootFolderId, batchIdentifierNumber, fileEntitiesToDeleteTable);
    }

    public void removeFilesFromFolder(Long batchIdentifierNumber, long runId, Long taskId, Long rootFolderId, long docFolderId) {
        DocFolder df = docFolderService.findById(docFolderId);

        if (df.isDeleted()) {
            logger.trace("folder {} already marked as deleted", docFolderId);
            return;
        }

        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);
        List<DocFolder> folders = docFolderService.getChildrenDocFolders(df.getParentsInfo());
        List<Long> childrenDocFolderIds = folders.stream()
                .map(DocFolder::getId)
                .collect(Collectors.toList());
        childrenDocFolderIds.add(docFolderId);

        logger.debug("going to delete folders {} and their files", childrenDocFolderIds);

        List<SolrFileEntity> descendantDocFolderFiles = fileCatService.findAllForDocFolders(childrenDocFolderIds);
        Table<Long, String, FileEntity> itemsToDelete = HashBasedTable.create();

        String storeName = mapServiceUtils.getStoreName(rootFolderId);
        kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {
            descendantDocFolderFiles.stream()
                    .map(SolrFileEntity::getMediaEntityId)
                    .forEach(mediaItemId -> {
                        String fileName = rf.getMediaType().isPathAsMediaEntity() ? rf.getRealPath() + mediaItemId : mediaItemId;
                        FileEntity fileEntity = fileEntityStore.get(fileName);
                        itemsToDelete.put(fileEntity.getFileId(), fileName, fileEntity);
                    });
            return true;
        });

        handleDeletedFiles(runId, taskId, rootFolderId, batchIdentifierNumber, itemsToDelete);
        childrenDocFolderIds.forEach(docFolderService::markDocFolderAsDeleted);
    }

    private void collectDeletedFolder(long runId, Long taskId, long rootFolderId, Long batchIdentifierNumber, String filename, FileEntity fileEntity) {
        Pair<Long, Long> batchTaskId = MapServiceUtils.createBatchTaskId(batchIdentifierNumber, taskId);
        fileEntity.setDeleted(true);
        mapBatchResultsProcessor.collectCatFiles(batchTaskId, new FileToAdd(null, fileEntity, filename, rootFolderId, runId));
    }

    private void handleDeletedFiles(long runId, Long taskId, Long rootFolderId, Long batchIdentifierNumber,
                                    Table<Long, String, FileEntity> fileEntitiesToDeleteTable) {

        Pair<Long, Long> batchTaskId = MapServiceUtils.createBatchTaskId(batchIdentifierNumber, taskId);
        String storeName = mapServiceUtils.getStoreName(rootFolderId);
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
        Set<Long> fileIds = fileEntitiesToDeleteTable.rowKeySet();
        List<SolrFileEntity> fileEntities = fileIds.isEmpty() ? new ArrayList<>() : fileCatService.findByFileIds(fileIds);
        Map<Long, SolrFileEntity> fileEntityById = fileEntities.stream().collect(Collectors.toMap(SolrFileEntity::getFileId, entity -> entity));

        for (Table.Cell<Long, String, FileEntity> cell : fileEntitiesToDeleteTable.cellSet()) {
            SolrFileEntity entity = fileEntityById.get(cell.getRowKey());
            handleFileDeletion(entity, runId, rootFolder, storeName, batchTaskId, cell.getColumnKey(), cell.getValue());
        }
    }

    private void handleFileDeletion(SolrFileEntity entity, long runId, RootFolder rootFolder, String storeName,
                                    Pair<Long, Long> batchTaskId, String mediaEntityId, FileEntity fileEntity) {

        if (fileEntity != null) {
            fileEntity.setDeleted(true);
        }

        if (entity == null) {
            logger.warn("cannot find entity file object for {}, delete will not proceed", fileEntity);
            return;
        }

        FileToDel ftd = new FileToDel(entity.getFileId(), runId, entity.getAnalysisGroupId(), entity.getUserGroupId());

        if (mapBatchResultsProcessor.fileAlreadySetForDelete(ftd)) {
            logger.trace("handle file already marked for deletion id {}", ftd.getFileId());
            return;
        }

        // email with attachments deleted - delete the attachments as well
        Map<SolrFileEntity, FileEntity> unDeletedAtt = mapExchangeServices.findAttachmentsToDelete(rootFolder, entity, storeName);
        if (unDeletedAtt != null) {
            // handle attachment delete
            unDeletedAtt.forEach((att, attEntity) ->
                    handleFileDeletion(att, runId, rootFolder, storeName, batchTaskId,
                            att.getMediaEntityId(), attEntity));
        }

        handleFileToDelete(batchTaskId, runId, rootFolder.getId(), ftd, fileEntity, mediaEntityId);

        mapBatchResultsProcessor.collectContentMetadataFileCountToUpdate(batchTaskId, entity.getFileId());
    }

    public void handleFileToDelete(Pair<Long, Long> batchTaskId, long runId, Long rootFolderId, FileToDel ftd, FileEntity fileEntity, String mediaEntityId) {
        // this marks the file as deleted in kvdb
        mapBatchResultsProcessor.collectCatFiles(batchTaskId, new FileToAdd(null, fileEntity, mediaEntityId, rootFolderId, runId));

        scanDiffEventProvider.generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, ftd.getFileId(), mediaEntityId);

        mapBatchResultsProcessor.collectDeletedFiles(batchTaskId, ftd);
    }

    private void collectFilesToDeleteFromPackage(FileEntityStore fileEntityStore, String filename,
                                                 Table<Long, String, FileEntity> fileEntitiesToDeleteTable) {
        final int[] count = {0};
        fileEntityStore.forEach((s, fileEntity) -> {
            // TODO Itai: [DAMAIN-3075] the call below to isFolder() will need demutexification once we support PST files within package files
            if (!fileEntity.isFolder() && s.startsWith(filename)) {
                count[0]++;
                fileEntitiesToDeleteTable.put(fileEntity.getFileId(), s, fileEntity);
            }
        });
    }
}
