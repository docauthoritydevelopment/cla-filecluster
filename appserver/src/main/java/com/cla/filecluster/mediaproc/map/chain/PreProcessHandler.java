package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.filecluster.mediaproc.map.MailFolderFullPathResolver;
import com.cla.filecluster.mediaproc.map.MapExchangeServices;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Pre process map response, currently only for exchange
 *
 * Created by: yael
 * Created on: 8/4/2019
 */
@Service
public class PreProcessHandler {

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MapExchangeServices mapExchangeServices;

    public Stream<ScanResultMessage> handle(ScanResultMessage scanResultMessage, long time) {
        List<ScanResultMessage> msgs = preProcessSingleScanResult(scanResultMessage).collect(Collectors.toList());
        List<ScanResultMessage> result = new ArrayList<>();
        for (ScanResultMessage msg : msgs) {
            boolean res = preProcessExchange(msg, time);
            if (res) {
                result.add(msg);
            }
        }

        return result.stream();
    }

    private boolean preProcessExchange(ScanResultMessage scanResultMessage, long time) {
        return !scanResultMessage.getPayloadType().equals(MessagePayloadType.EXCHANGE365_ITEM_CHANGE) ||
                !mapExchangeServices.handleExchange(scanResultMessage, time);

    }

    private Stream<ScanResultMessage> preProcessSingleScanResult(ScanResultMessage scanResultMessage) {
        switch (scanResultMessage.getPayloadType()) {
            case EXCHANGE365_FOLDER_CHANGES:
                return MailFolderFullPathResolver
                        .forScanResultMsg(scanResultMessage, docFolderService)
                        .streamResolvedFolders();
            default:
                return Stream.of(scanResultMessage);
        }
    }
}
