package com.cla.filecluster.mediaproc.connectors;

import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.ControlResultMessage;
import com.cla.common.utils.DaMessageConversionUtils;
import com.cla.common.utils.thread.ExecutorUtil;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import com.cla.filecluster.batch.loader.IngestBatchLoader;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.util.executors.BlockingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Kafka listeners configuration for AppServer
 * Created by vladi on 2/1/2017.
 */
@Service
@Profile({"prod", "win", "dev", "default", "test"})
public class MediaProcessorCommListeners {

    private static final Logger logger = LoggerFactory.getLogger(MediaProcessorCommListeners.class);

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Autowired
    private BlockingExecutor ingestBlockingExecutor;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Autowired
    private BlockingExecutor scanResponsesBlockingExecutor;

    @Value("${content-check-executor.pool-size:5}")
    private int contentCheckExecutorPoolSize;

    @Value("${content-check-executor.queue-size:50}")
    private int contentCheckExecutorQueueSize;

    @Value("${control-executor.pool-size:2}")
    private int controlExecutorPoolSize;

    @Value("${control-executor.queue-size:50}")
    private int controlExecutorQueueSize;

	@Autowired
	private SystemSettingsService systemSettingsService;

    private ExecutorService contentCheckBlockingExecutor;

    private ExecutorService controlBlockingExecutor;

    private Map<Long, Long> ingestRate = new HashMap<>();

    private long counter = 0;

    // todo yael remove performance chk map
    private AtomicBoolean acceptScanResponses = new AtomicBoolean(true);

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private IngestBatchLoader ingestBatchLoader;

    @PostConstruct
    void init() {
        contentCheckBlockingExecutor = ExecutorUtil.newFixedThreadPoolWithQueueSize(contentCheckExecutorPoolSize, contentCheckExecutorQueueSize);
        controlBlockingExecutor = ExecutorUtil.newFixedThreadPoolWithQueueSize(controlExecutorPoolSize, controlExecutorQueueSize);
    }

    @PreDestroy
    public void destroy() {
        contentCheckBlockingExecutor.shutdown();
        controlBlockingExecutor.shutdown();
    }

    public void setAcceptScanResponses(boolean accept) {
        acceptScanResponses.set(accept);
    }

    @JmsListener(destination = "${scan.responses.topic:scan.responses}",
            concurrency = "${scan.responses.concurrency:1}", containerFactory = "appServerJmsFactory")
    public void scanResultListener(String data, @Header("timestamp") String timestamp) {
        while (!acceptScanResponses.get()) {
            logger.debug("waiting for map release");
            try {
                Thread.sleep(10000);
            } catch (Exception e) {

            }
        }
        logger.trace("Received scan result message with timestamp {}", timestamp);
        scanResponsesBlockingExecutor.execute(() -> {
            try {
                KpiRecording kpiRecording = performanceKpiRecorder.startRecording("deserializeResultMessage", "MediaProcessorCommListeners.scanResultListener", 82);
                ScanResultMessage scanResultMessage = DaMessageConversionUtils.Scan.deserializeResultMessage(data);
                kpiRecording.end();

                if (scanResultMessage != null && scanResultMessage.getPayloadType() == MessagePayloadType.SCAN_CREATE_TASK) {
                    kpiRecording = performanceKpiRecorder.startRecording("consumeCreateScanTaskMessage", "MediaProcessorCommListeners.scanResultListener", 87);
                    remoteMediaProcessingService.consumeCreateScanTaskMessage(scanResultMessage);
                    kpiRecording.end();
                } else if (scanResultMessage != null && scanResultMessage.getPayloadType() == MessagePayloadType.SCAN_SHARE_PERMISSIONS) {
                    remoteMediaProcessingService.consumeSharePermissionsMessage(scanResultMessage);
                } else if (scanResultMessage != null && scanResultMessage.getContainedMessages() != null &&
                        scanResultMessage.getContainedMessages().size() > 0) {
                    kpiRecording = performanceKpiRecorder.startRecording("consumeScanResult", "MediaProcessorCommListeners.scanResultListener", 92);
                    remoteMediaProcessingService.consumeScanResult(scanResultMessage, timestamp);
                    kpiRecording.end();
                } else {
                    kpiRecording = performanceKpiRecorder.startRecording("consumeScanResult", "MediaProcessorCommListeners.scanResultListener", 96);
                    remoteMediaProcessingService.consumeScanResult(scanResultMessage, timestamp);
                    kpiRecording.end();
                }
            } catch (Exception e) {
                logger.error("failed handling scan result {}", data, e);
            }
        });
    }

    @JmsListener(destination = "${ingest.responses.topic:ingest.responses}",
            concurrency = "${ingest.responses.concurrency:1}", containerFactory = "appServerJmsFactory")
    public void ingestResultListener(String data, @Header("timestamp") String timestamp) {
        logger.trace("Ingesting item with timestamp {}", timestamp);
        /*        try {*/
        ingestBatchLoader.submit(data);
        /*} catch (IOException | ClassNotFoundException ex) {
            logger.error("Failed to convert message.", ex);
        }*/
        /*ingestBlockingExecutor.execute(() ->
        {
            try {
                KpiRecording startKpiRecoding = performanceKpiRecorder.startRecording("ingestResultListener", "ingest result listener", 110);
                KpiRecording kpiRecording = performanceKpiRecorder.startRecording("ingestResultListener", "DaMessageConversionUtils.Ingest.deserializeResultMessage(data)", 111);
                IngestResultMessage ingestResultMessage = DaMessageConversionUtils.Ingest.deserializeResultMessage(data);
                logger.trace("Consume ingest result message on file id: {} task: {}", ingestResultMessage.getClaFileId(), ingestResultMessage.getTaskId());
                kpiRecording.end();

                kpiRecording = performanceKpiRecorder.startRecording("ingestResultListener", "remoteMediaProcessingService.consumeIngestResult(ingestResultMessage)", 116);
                remoteMediaProcessingService.consumeIngestResult(ingestResultMessage);
                kpiRecording.end();
                startKpiRecoding.end();
            } catch (IOException | ClassNotFoundException ex) {
                logger.error("Failed to convert message.", ex);
            } catch (Exception e) {
                logger.error("Failed to consume ingest message {}", data, e);
            }
        });*/
    }


    @JmsListener(destination = "${control.responses.topic:control.responses}",
            concurrency = "${control.responses.concurrency:1}", containerFactory = "appServerJmsFactory")
    public void controlListener(String data) {
        logger.trace("received control msg");
        controlBlockingExecutor.execute(() -> {
            logger.trace("start handle control msg");
            try {
                ControlResultMessage controlResultMessage = DaMessageConversionUtils.Control.deserializeResultMessage(data);
                remoteMediaProcessingService.consumeControlResult(controlResultMessage);
            } catch (IOException | ClassNotFoundException ex) {
                logger.error("Failed to convert message.", ex);
            } catch (Throwable t) {
                logger.error("Failed to handle control message {}", data, t);
            }
            logger.trace("done handle control msg");
        });
    }

    @JmsListener(destination = "${realtime.responses.topic:realtime.responses}",
            concurrency = "${realtime.listener.concurrency:5}", containerFactory = "appServerJmsFactory")
    public void fileContentResultListener(String data) {
        try {
            FileContentResultMessage fileContentResultMessage = DaMessageConversionUtils.FileContent.deserializeResultMessage(data);
			switch (fileContentResultMessage.getPayloadType()) {
				case LIST_FOLDERS_RESPONSE:
					logger.debug("received a folders list message");
					FoldersListResultMessage foldersListResultMessage = DaMessageConversionUtils.FileContent.deserializeFoldersListResultMessage(data);
					remoteMediaProcessingService.consumeListFolderContentResult(foldersListResultMessage);
				break;
				case KEY_VALUE_RESPONSE:
					//TODO: here save / handle all the incoming key-values
					KeyValuePayloadResp keyValuePayloadResp = DaMessageConversionUtils.FileContent.deserializeKeyValueResultMessage(data);
					String collect = keyValuePayloadResp.getMapPayload().entrySet().stream()
							.map(e -> "[" + e.getKey() + "," + e.getValue() + "]")
							.collect(Collectors.joining(","));
					logger.info("received key-value pairs from MP: " + collect);
					keyValuePayloadResp.getMapPayload().entrySet()
							.forEach(kv->systemSettingsService.updateOrCreateSystemSettings(kv.getKey(), kv.getValue()));
					break;
				default:
				remoteMediaProcessingService.consumeFileContentResult(fileContentResultMessage);
			}
		} catch (IOException | ClassNotFoundException ex) {
            logger.error("Failed to convert message.", ex);
        } catch (Exception e) {
            logger.error("Failed to consume fileContentResult Messages", e);
        }
    }

    @JmsListener(destination = "${contentcheck.requests.topic:contentcheck.requests}",
            concurrency = "${contentcheck.listener.concurrency:5}", containerFactory = "appServerJmsFactory")
    public void contentCheckRequestListener(String data) {
        contentCheckBlockingExecutor.execute(() -> {
            try {
                ContentMetadataRequestMessage req = DaMessageConversionUtils.ContentCheck.deserializeRequestMessage(data);
                remoteMediaProcessingService.consumeContentCheckRequest(req);
            } catch (Exception e) {
                logger.error("Failed to consume ContentMetadataRequest Messages {}", data, e);
            }
        });
    }

    @JmsListener(destination = "${labeling.responses.topic}",
            concurrency = "${labeling.listener.concurrency:5}", containerFactory = "appServerJmsFactory")
    public void labelingResponseListener(String data) {
        contentCheckBlockingExecutor.execute(() -> {
            try {
                LabelingResponseMessage resp = DaMessageConversionUtils.Labeling.deserializeResultMessage(data);
                remoteMediaProcessingService.consumeLabelingResponse(resp);
            } catch (Exception e) {
                logger.error("Failed to consume LabelingResponseMessage Messages {}", data, e);
            }
        });
    }
}
