package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Handle ingestion result of empty files
 * This handler stops the chain if activated
 */
@Component
public class EmptyFileHandler implements ChainLink<IngestResultContext> {

    @Autowired
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return (context.getResultMessage().getFileProperties().getFileSize() == 0);
    }

    @Override
    public boolean handle(IngestResultContext context) {
        IngestResultMessage<?> ingestResult = context.getIngestResultMessage();
        filesDataPersistenceService.saveEmptyIngestionFile(ingestResult.getTaskId(), ingestResult.getClaFileId(),
                    ingestResult.getFileProperties(), context.getIngestBatchStore());
        return false;
    }

}
