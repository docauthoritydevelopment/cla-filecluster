package com.cla.filecluster.mediaproc.map;

import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import org.apache.solr.common.SolrInputDocument;

import java.util.Collection;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
public class ItemToCreate {
    private SolrInputDocument docFileToCreate;
    private Long folderId;
    private Map<String, FileCatBuilder> attachmentsToCreate;
    private Collection<SolrFileEntity> attachmentsToDelete;

    public ItemToCreate(SolrInputDocument docFileToCreate, Long folderId,
                         Map<String, FileCatBuilder> attachmentsToCreate, Collection<SolrFileEntity> attachmentsToDelete) {
        this.docFileToCreate = docFileToCreate;
        this.folderId = folderId;
        this.attachmentsToCreate = attachmentsToCreate;
        this.attachmentsToDelete = attachmentsToDelete;
    }

    public SolrInputDocument getDocFileToCreate() {
        return docFileToCreate;
    }

    public Long getFolderId() {
        return folderId;
    }

    public Map<String, FileCatBuilder> getAttachmentsToCreate() {
        return attachmentsToCreate;
    }

    public Collection<SolrFileEntity> getAttachmentsToDelete() {
        return attachmentsToDelete;
    }
}
