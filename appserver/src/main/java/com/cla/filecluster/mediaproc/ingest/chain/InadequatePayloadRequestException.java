package com.cla.filecluster.mediaproc.ingest.chain;

import com.cla.common.domain.dto.messages.ProcessingPhaseMessage;

public class InadequatePayloadRequestException extends RuntimeException{

    private ProcessingPhaseMessage payload;
    private Class expectedType;

    public InadequatePayloadRequestException(ProcessingPhaseMessage payload, Class expectedType) {
        super("Requested type " + expectedType.getSimpleName() +
                " does not match the actual payload type " + payload.getPayload().getClass());
        this.payload = payload;
        this.expectedType = expectedType;
    }

    public ProcessingPhaseMessage getPayload() {
        return payload;
    }

    public Class getExpectedType() {
        return expectedType;
    }
}
