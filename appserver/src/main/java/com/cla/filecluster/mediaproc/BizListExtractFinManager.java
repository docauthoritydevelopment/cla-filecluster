package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.crawler.FileCrawlPhase;
import com.cla.common.domain.dto.jobmanager.*;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.files.managers.CatFileExtractionAppService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.util.executors.BlockingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by: yael
 * Created on: 7/17/2019
 */
@Service
public class BizListExtractFinManager implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(BizListExtractFinManager.class);

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private BlockingExecutor extractionBlockingExecutor;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private CatFileExtractionAppService catFileExtractionAppService;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private BizListService bizListService;

    @Value("${entities.update.catfile:true}")
    private boolean updateEntitiesInCatFile;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @AutoAuthenticate
    @Scheduled(initialDelayString =  "${bizlist-fin.tasks-polling-initial-delay-millis:5000}",
            fixedRateString = "${bizlist-fin.tasks-polling-rate-millis:20000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void processNextTasksGroup() {
        if (!isSchedulingActive) {
            return;
        }

        try {
            List<SimpleJob> readyJobs = jobManager.getJobs(JobType.EXTRACT_FINALIZE, JobState.IN_PROGRESS);
            if (readyJobs.isEmpty()) {
                return;
            }
            readyJobs.forEach(job -> {
                SimpleJobDto jobDto = JobManagerService.simpleJobToDto(job);
                    try {
                        List<SimpleTask> tasks = jobManager.getTaskByJobIdAndTaskTypeAndStates(
                                job.getId(), TaskType.EXTRACT_FINALIZE, TaskState.NEW);
                        if (tasks != null && !tasks.isEmpty()) {
                            enqueueExtractFinTask(jobDto, tasks.get(0).getId());
                        }
                    } catch (Exception e) {
                        jobManager.handleFailedFinishJob(job.getId(),"Extract finalize failed");
                        fileCrawlerExecutionDetailsService.handleFinishedRun(job.getRunContext(), PauseReason.SYSTEM_ERROR);
                    }
            });
        } catch (Exception e) {
            logger.error("Failed to process extraction finalize jobs.", e);
        }
    }

    private void enqueueExtractFinTask(SimpleJobDto extractFinJob, Long taskId) {
        logger.debug("enqueue extract finalize task {} of job {}", taskId, extractFinJob.getId());
        jobManager.updateTaskStatus(taskId, TaskState.ENQUEUED);
        jobManager.updateJobCounters(extractFinJob.getId(), 0, 100, 0);
        long bizListId = extractFinJob.getItemId();
        Long runContext = extractFinJob.getRunContext();
        final Authentication currentAuthentication = AuthenticationHelper.getCurrentAuthentication();
        executeExtractFinTask(bizListId, runContext, extractFinJob.getId(), taskId, currentAuthentication);
    }

    private void executeExtractFinTask(long bizListId, Long runId, Long extractFinJobId,
                                       Long taskId, Authentication auth) {
        extractionBlockingExecutor.execute(() -> {
            // copy the authentication of the main thread to the executor thread
            AuthenticationHelper.setAuthentication(auth);
            try {
                logger.debug("consume extract finalize task {} of job {}", taskId, extractFinJobId);
                jobManager.updateTaskStatus(taskId, TaskState.CONSUMING);


                if (updateEntitiesInCatFile) {
                    jobManager.setOpMsg(extractFinJobId, "Update extraction indexes");

                    updateCatFilesForBizList(bizListId);
                }

                bizListService.updateLastSuccessfulRunDate(bizListId);

                logger.debug("end extract finalize task {} of job {}", taskId, extractFinJobId);
                jobManager.updateTaskStatus(taskId, TaskState.DONE);
                jobManager.updateJobCounters(extractFinJobId, 100, 0);
                if (jobManager.isJobActive(extractFinJobId)) {
                    jobManager.handleFinishJob(extractFinJobId, "Finished extract finalize");
                    fileCrawlerExecutionDetailsService.handleFinishedRun(runId, PauseReason.SYSTEM_INITIATED);
                }
            } catch (Exception e) {
                jobManager.updateTaskStatus(taskId, TaskState.FAILED);
                logger.error("Failed to run finalize extract", e);
                if (jobManager.isJobActive(extractFinJobId)) {
                    jobManager.handleFailedFinishJob(extractFinJobId, "Extract finalize failed");
                    fileCrawlerExecutionDetailsService.handleFinishedRun(extractFinJobId, PauseReason.SYSTEM_ERROR);
                }
            }

        });
    }

    private void updateCatFilesForBizList(long bizListId) {
        opStateService.setCurrentPhase(FileCrawlPhase.SOLR_EXTRACT_UPDATE);
        catFileExtractionAppService.updateCatFilesBizListExtractions(bizListId,null);
    }
}
