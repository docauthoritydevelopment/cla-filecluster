package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.AclItemDto;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.google.common.collect.Sets;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static com.cla.common.constants.CatFileFieldType.ID;

/**
 * Update acl if received from map
 *
 * Created by: yael
 * Created on: 8/12/2019
 */
@Service
public class MapAclHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(MapAclHandler.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        ClaFilePropertiesDto payload = (ClaFilePropertiesDto)scanResultMessage.getPayload();
        List<AclItemDto> aclItems = ModelIngestUtils.getAclItems(mapContext.getFileId(), payload);

        if (mapContext.getSolrInputDocumentFileAndFolderId() != null) { // new file
            if (mapContext.getSolrInputDocumentFileAndFolderId().getDocFileToCreate() != null) {
                SolrInputDocument doc = mapContext.getSolrInputDocumentFileAndFolderId().getDocFileToCreate();
                fileCatService.updateAcls(doc, Sets.newHashSet(aclItems));

                // handle attachments
                Map<String, FileCatBuilder> attachmentsToCreate =
                        mapContext.getSolrInputDocumentFileAndFolderId().getAttachmentsToCreate();
                if (attachmentsToCreate != null && attachmentsToCreate.size() > 0) {
                    attachmentsToCreate.values().forEach(builder ->
                            fileCatService.updateAcls(builder, Sets.newHashSet(aclItems)));
                }
            }
        } else { // existing file
            SolrFileEntity file = fileCatService.getFileEntity(mapContext.getFileId());
            SolrInputDocument doc = new SolrInputDocument();
            doc.setField(ID.getSolrName(), file.getId());
            fileCatService.updateAcls(doc, Sets.newHashSet(aclItems));
            SolrFileCatRepository.setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
            mapBatchResultsProcessor.collectCatFiles(MapServiceUtils.createBatchTaskId(mapContext.getBatchIdentifierNumber(), scanResultMessage.getTaskId()),
                    new FileToAdd(doc, null, null, scanResultMessage.getRootFolderId(), scanResultMessage.getRunContext()));
        }

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        if (mapContext.getSolrInputDocumentFileAndFolderId() == null && mapContext.getFileId() == null) {
            logger.trace("no file data {}, skip", mapContext);
            return false;
        }
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        if (scanResultMessage.getPayload() instanceof ClaFilePropertiesDto) {
            ClaFilePropertiesDto payload = (ClaFilePropertiesDto)scanResultMessage.getPayload();
            List<AclItemDto> aclItems = ModelIngestUtils.getAclItems(mapContext.getFileId(), payload);
            if (!aclItems.isEmpty()) {
                logger.trace("acl received for file {}, handle", mapContext);
                return true;
            }
        }
        logger.trace("no acl received for file {}, skip", mapContext);
        return false;
    }
}
