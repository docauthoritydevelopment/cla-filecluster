package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * If there are no errors and no payload then need to update properties only
 * This handler stops the chain if activated
 */
@Component
public class MetadataOnlyHandler implements ChainLink<IngestResultContext> {

    private final static Logger logger = LoggerFactory.getLogger(MetadataOnlyHandler.class);

    @Autowired
    protected FilesDataPersistenceService filesDataPersistenceService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        IngestResultMessage<?> ingestResult = context.getIngestResultMessage();
        return (!ingestResult.hasPayload() || ingestResult.getPayload().isMetadataOnly());
    }

    @Override
    public boolean handle(IngestResultContext context) {
        IngestResultMessage<?> ingestResult = context.getIngestResultMessage();
        Long fileId = ingestResult.getClaFileId();
        logger.debug("Ingest result on {} contains only metadata without content. Updating file properties.", fileId);
            filesDataPersistenceService.updateFileProperties(ingestResult.getTaskId(), ingestResult.getRunContext(),
                    fileId, ingestResult.getFileProperties(), context.getIngestBatchStore());
        return false;
    }
}
