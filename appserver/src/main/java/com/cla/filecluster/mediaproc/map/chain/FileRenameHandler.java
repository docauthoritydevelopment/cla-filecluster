package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.MapRenameHandler;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.cla.common.domain.dto.messages.MessagePayloadType.*;
import static com.cla.connector.domain.dto.file.DiffType.RENAMED;

/**
 * Handle file rename
 * <p>
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class FileRenameHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(FileRenameHandler.class);

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private MapRenameHandler mapRenameHandler;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        ClaFilePropertiesDto payload = (ClaFilePropertiesDto) scanResultMessage.getPayload();
        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());
        logger.trace("Handle file rename for id: {}, path: {} for task {}", oldFileEntity.getFileId(), oldFileEntity.getPath(), scanResultMessage.getTaskId());
        RootFolder rootFolder = docStoreService.getRootFolderCached(scanResultMessage.getRootFolderId());

        boolean updateOldFileEntity = mapRenameHandler.handleFileOrDeletedFolderRename(mapContext.getBatchIdentifierNumber(), rootFolder,
                scanResultMessage.getRunContext(), scanResultMessage.getTaskId(), payload, oldFileEntity, mapContext.getBelongToRF());

        if (rootFolder.getReingestTimeStampMs() > 0 && !mapContext.getFileShare() && !mapContext.getAlreadyHandled()) {// full rescan
            mapRenameHandler.createIngestTaskForRenamed(mapContext.getBatchIdentifierNumber(), rootFolder,
                    scanResultMessage.getRunContext(), scanResultMessage, payload, mapContext.getKvdbKey(), oldFileEntity);
        }

        if (updateOldFileEntity && oldFileEntity != null) {
            mapContext.setFileEntity(oldFileEntity);
        }
        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        MessagePayloadType payloadType = scanResultMessage.getPayloadType();
        List<MessagePayloadType> types = Lists.newArrayList(SCAN_CHANGES, SCAN_PST_ENTRY, SCAN, EXCHANGE365_ITEM_CHANGE);
        if (!types.contains(payloadType)) {
            logger.trace("payload type irrelevant {}, skip", mapContext);
            return false;
        }

        if (mapContext.getFstScan()) {
            logger.trace("first scan for file {}, skip", mapContext);
            return false;
        }

        boolean shouldHandle = mapContext.getDiffType().equals(RENAMED) && !mapContext.getNewFile();
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
