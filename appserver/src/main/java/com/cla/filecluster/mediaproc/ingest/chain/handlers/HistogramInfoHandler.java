package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.DebFileHistogramInfo;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.repository.jpa.DebFileHistogramInfoRepository;
import com.cla.filecluster.service.chain.ChainLink;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Save histogram data into the debug histogram info repository.
 * Off by default.
 */
@Component
public class HistogramInfoHandler implements ChainLink<IngestResultContext> {

    @Value("${debug.writeFileHistogramInfo:false}")
    private boolean writeFileHistogramInfo;

    @Autowired
    private DebFileHistogramInfoRepository debFileHistogramInfoRepository;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return writeFileHistogramInfo && !context.isOther();
    }

    @Override
    public boolean handle(IngestResultContext context) {
        SolrFileEntity file = context.getFile();
        debFileHistogramInfoRepository.save(new DebFileHistogramInfo(
                context.getSavedContentId(), String.valueOf(file.getFileId()), Strings.EMPTY,
                FileType.valueOf(file.getType()), context.getPayload().getCsvValue()));
        return true;
    }

}
