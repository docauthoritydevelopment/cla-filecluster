package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.filecluster.mediaproc.map.ItemToCreate;
import com.cla.filecluster.service.chain.ChainContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by: yael
 * Created on: 8/4/2019
 */
public class MapResultContext implements ChainContext {
    private ScanResultMessage scanResultMessage;
    private long time;
    private Long batchIdentifierNumber;
    private Long folderId;
    private Long fileId;
    private ItemToCreate solrInputDocumentFileAndFolderId;
    private FileEntity fileEntity;
    private List<Long> attachmentsFileIDs = new ArrayList<>();
    private boolean errorOccurred = false;
    private String kvdbKey;
    private Boolean isFileShare;
    private DiffType diffType;
    private Boolean isAlreadyHandled;
    private Boolean isBelongToRF;
    private Boolean isNewFile;
    private Boolean fstScan;

    public ScanResultMessage getScanResultMessage() {
        return scanResultMessage;
    }

    public void setScanResultMessage(ScanResultMessage scanResultMessage) {
        this.scanResultMessage = scanResultMessage;
    }

    public boolean errorOccurred() {
        return errorOccurred;
    }

    public void setErrorOccurred(boolean errorOccurred) {
        this.errorOccurred = errorOccurred;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public Long getBatchIdentifierNumber() {
        return batchIdentifierNumber;
    }

    public void setBatchIdentifierNumber(Long batchIdentifierNumber) {
        this.batchIdentifierNumber = batchIdentifierNumber;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public ItemToCreate getSolrInputDocumentFileAndFolderId() {
        return solrInputDocumentFileAndFolderId;
    }

    public void setSolrInputDocumentFileAndFolderId(ItemToCreate solrInputDocumentFileAndFolderId) {
        this.solrInputDocumentFileAndFolderId = solrInputDocumentFileAndFolderId;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public List<Long> getAttachmentsFileIDs() {
        return attachmentsFileIDs;
    }

    public void setAttachmentsFileIDs(List<Long> attachmentsFileIDs) {
        this.attachmentsFileIDs = attachmentsFileIDs;
    }

    public String getKvdbKey() {
        return kvdbKey;
    }

    public void setKvdbKey(String kvdbKey) {
        this.kvdbKey = kvdbKey;
    }

    public Boolean getFileShare() {
        return isFileShare;
    }

    public void setFileShare(Boolean fileShare) {
        isFileShare = fileShare;
    }

    public DiffType getDiffType() {
        return diffType;
    }

    public void setDiffType(DiffType diffType) {
        this.diffType = diffType;
    }

    public Boolean getAlreadyHandled() {
        return isAlreadyHandled;
    }

    public void setAlreadyHandled(Boolean alreadyHandled) {
        isAlreadyHandled = alreadyHandled;
    }

    public Boolean getBelongToRF() {
        return isBelongToRF;
    }

    public void setBelongToRF(Boolean belongToRF) {
        isBelongToRF = belongToRF;
    }

    public Boolean getNewFile() {
        return isNewFile;
    }

    public void setNewFile(Boolean newFile) {
        isNewFile = newFile;
    }

    public Boolean getFstScan() {
        return fstScan;
    }

    public void setFstScan(Boolean fstScan) {
        this.fstScan = fstScan;
    }

    @Override
    public String toString() {
        return "MapResultContext{" +
                "scanResultMessage=" + scanResultMessage +
                '}';
    }
}
