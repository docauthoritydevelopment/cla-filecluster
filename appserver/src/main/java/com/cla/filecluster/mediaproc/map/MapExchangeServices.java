package com.cla.filecluster.mediaproc.map;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.cla.kvdb.domain.FileEntityUtils;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
@Service
public class MapExchangeServices {

    private static final Logger logger = LoggerFactory.getLogger(MapExchangeServices.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private MapRenameHandler mapRenameHandler;

    public Collection<SolrFileEntity> handleEmailAttachments(Map<String, FileCatBuilder> attachments, Long existingEmailFileId) {
        if (attachments == null || attachments.size() == 0) {
            return new ArrayList<>();
        }

        // get existing email attachments
        List<SolrFileEntity> attachmentEntities = existingEmailFileId != null ?
                fileCatService.findEmailAttachments(existingEmailFileId) : Lists.newArrayList();
        Map<String, SolrFileEntity> attachmentEntitiesByMEI = attachmentEntities.stream()
                .collect(Collectors.toMap(SolrFileEntity::getMediaEntityId, Function.identity()));

        attachments.forEach((id, attachment) -> {
            Long attFileId;
            Long attContentId;
            FileType fileType;
            String mediaItemId = attachment.getProps().getMediaItemId();
            SolrFileEntity attByHash = attachmentEntitiesByMEI.get(mediaItemId);

            if (attByHash == null) { // new attachment
                fileType = FileTypeUtils.getFileType(attachment.getProps().getFileName());
                attFileId = fileCatService.getNextAvailableFileId();
                attContentId = fileCatService.getNextAvailableContentId();
            } else { // attachment removed
                fileType = FileType.valueOf(attByHash.getType());
                attFileId = attByHash.getFileId();
                attContentId = FileCatService.getContentId(attByHash);
                attachmentEntitiesByMEI.remove(attByHash.getMediaEntityId());
            }
            attachment.setId(FileCatCompositeId.of(attContentId, attFileId).toString())
                    .setFileId(attFileId)
                    .setFileType(fileType)
                    .setContentId(attContentId);
        });

        // some attachments were deleted
        if (!attachmentEntitiesByMEI.isEmpty()) {
            return attachmentEntitiesByMEI.values();
        }
        return new ArrayList<>();
    }

    private void fixPathForExchangeFile(Exchange365ItemDto exchange365ItemDto, Long rootFolderId) {
        String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
        String folderMediaEntityId = exchange365ItemDto.getParentFolderId();
        // get folder id from kvdb
        boolean fixed = false;
        if (kvdbRepo.getFileEntityDao().storeExists(storeName)) {
            FileEntity fileEntity = kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName,
                    fileEntityStore -> fileEntityStore.get(folderMediaEntityId));
            if (fileEntity != null) {
                exchange365ItemDto.setFileName(fileEntity.getPath() + "/" + exchange365ItemDto.getFileName());
                fixed = true;
            }
        }
        if (!fixed) { // try solr instead
            SolrFolderEntity folder = docFolderService.getFolderByMediaEntityId(rootFolderId, folderMediaEntityId);
            if (folder != null) {
                exchange365ItemDto.setFileName(folder.getPath() + exchange365ItemDto.getFileName());
            } else {
                logger.warn("exchange cant find folder {} for file {}", folderMediaEntityId, exchange365ItemDto.getFileName());
            }
        }
    }


    public boolean handleExchange(ScanResultMessage scanResultMessage, long time) {
        if (scanResultMessage.getPayload() instanceof Exchange365ItemDto) {
            Exchange365ItemDto exchange365ItemDto = (Exchange365ItemDto) scanResultMessage.getPayload();
            logger.trace("processing scan result {} msg for Exchange365ItemDto {}", exchange365ItemDto.getEventType(),
                    exchange365ItemDto.getFileName());
            if (exchange365ItemDto.isFile()) {
                fixPathForExchangeFile(exchange365ItemDto, scanResultMessage.getRootFolderId());
            } else {// folder
                return mapRenameHandler.handleFolderMove(exchange365ItemDto, scanResultMessage.getRunContext(),
                        scanResultMessage.getRootFolderId(), time);
            }
        }
        return false;
    }

    public Map<SolrFileEntity, FileEntity> findAttachmentsToDelete(RootFolder rootFolder, SolrFileEntity entity, String storeName) {
        if (rootFolder.getMediaType().equals(MediaType.EXCHANGE_365) &&
                entity.getItemType() != null && entity.getItemType().equals(ItemType.MESSAGE.toInt()) &&
                entity.getNumOfAttachments() != null && entity.getNumOfAttachments() > 0) {
            List<SolrFileEntity> attachments = fileCatService.findEmailAttachments(entity.getFileId());
            if (attachments != null && attachments.size() > 0) {
                List<SolrFileEntity> unDeletedAtt = attachments.stream().filter(att -> !att.isDeleted()).collect(Collectors.toList());
                Map<Long, FileEntity> attEntities = new HashMap<>();

                // get attachments file entities from kvdb
                kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {
                    unDeletedAtt.forEach(att -> {
                        FileEntity e = fileEntityStore.get(att.getMediaEntityId());
                        if (e != null) {
                            attEntities.put(att.getFileId(), e);
                        }
                    });
                    return true;
                });

                Map<SolrFileEntity, FileEntity> result = new HashMap<>();
                unDeletedAtt.forEach(del -> result.put(del, attEntities.get(del.getFileId())));
                return result;
            }
        }
        return null;
    }

    public void handleAttachmentCreation(Map<String, FileCatBuilder> attachmentsToCreate, Pair<Long, Long> batchTaskId,
                                         RootFolder rf, long runContext, Long folderId, long time, Map<CatFileFieldType, Object> associationData) {
        if (attachmentsToCreate != null && attachmentsToCreate.size() > 0) {

            attachmentsToCreate.forEach((id, attachment) -> {
                String attPathForKvdb = null;
                if (!rf.getMediaType().isPathAsMediaEntity()) {
                    attPathForKvdb = attachment.getProps().getFileName();
                }
                String attMediaItemId = Optional.ofNullable(attachment.getProps().getMediaItemId())
                        .orElse(attachment.getProps().getFileName());
                FileEntity attFileEntity = FileEntityUtils.createFileEntity(attachment.getFileId(), folderId, time, attachment.getProps(), attPathForKvdb);

                SolrInputDocument doc = attachment.buildAtomicUpdate();
                associationData.forEach((field, value) -> doc.setField(field.getSolrName(), value));

                mapBatchResultsProcessor.collectCatFiles(batchTaskId, new FileToAdd(doc,
                        attFileEntity, attMediaItemId, rf.getId(), runContext));
            });
        }
    }

    public List<Triple<FileToDel, FileEntity, String>> handleAttachmentDeletion(Collection<SolrFileEntity> attachmentsToDelete, RootFolder rf,
                                                                                long runContext, Long folderId, long time) {
        List<Triple<FileToDel, FileEntity, String>> result = new ArrayList<>();
        if (attachmentsToDelete != null && attachmentsToDelete.size() > 0) {
            attachmentsToDelete.forEach(attachment -> {
                String attPathForKvdb = null;
                if (!rf.getMediaType().isPathAsMediaEntity()) {
                    attPathForKvdb = attachment.getFullName();
                }
                ClaFilePropertiesDto cfp = ClaFilePropertiesDto.create();
                FileEntity attFileEntity = FileEntityUtils.createFileEntity(attachment.getFileId(), folderId, time, cfp, attPathForKvdb);
                FileToDel ftd = new FileToDel(attachment.getFileId(), runContext, attachment.getAnalysisGroupId(), attachment.getUserGroupId());
                result.add(Triple.of(ftd, attFileEntity, attachment.getMediaEntityId()));
            });
        }
        return result;
    }
}
