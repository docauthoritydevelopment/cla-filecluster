package com.cla.filecluster.mediaproc.ingest.chain;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.chain.ChainContext;

import java.util.Set;

public class IngestResultContext implements ChainContext {

    // ingestion result message as received from the MP
    private IngestResultMessage resultMessage;
    // ingest batch store
    private IngestBatchStore ingestBatchStore;
    // content metadata
    private ContentMetadata contentMetadata;
    // file/content state before the current scan has begun
    private ClaFileState originalState;
    // whether there was an ingestion error
    private boolean errorOccurred = false;
    // file type
    private FileType fileType;
    // type of the processing error, if relevant
    private ProcessingErrorType errorType;
    // PVS data
    private PvAnalysisData pvAnalysisData;
    // search pattern data
    private Set<SearchPatternCountDto> searchPatternsCounting;
    // Solr file object for persistence
    private SolrFileEntity savedFile;
    // content ID received from the repository during the persistence
    private Long savedContentId;
    // file maturity level
    private int maturityLevel;

    private boolean failedTask = false;

    public static IngestResultContext create(IngestResultMessage resultMessage){
        IngestResultContext context = new IngestResultContext();
        context.setResultMessage(resultMessage);
        return context;
    }

    //region Convenience methods
    public SolrFileEntity getFile(){
        return getIngestBatchStore().getFileEntity(getIngestResultMessage().getClaFileId());
    }

    @SuppressWarnings("unchecked")
    public IngestResultMessage<WordIngestResult> getWordResultMessage() {
        if (isWordOrPDF()){
            return (IngestResultMessage<WordIngestResult>)resultMessage;
        }
        throw new InadequatePayloadRequestException(resultMessage, WordIngestResult.class);
    }

    @SuppressWarnings("unchecked")
    public IngestResultMessage<ExcelIngestResult> getExcelResultMessage() {
        if (isExcel()){
            return (IngestResultMessage<ExcelIngestResult>)resultMessage;
        }
        throw new InadequatePayloadRequestException(resultMessage, ExcelIngestResult.class);
    }

    @SuppressWarnings("unchecked")
    public IngestResultMessage<OtherFile> getOtherResultMessage() {
        if (isOther()){
            return (IngestResultMessage<OtherFile>)resultMessage;
        }
        throw new InadequatePayloadRequestException(resultMessage, OtherFile.class);
    }

    public boolean isWordOrPDF(){
        return WordIngestResult.class.isAssignableFrom(getPayloadType());
    }

    public boolean isExcel(){
        return ExcelIngestResult.class.isAssignableFrom(getPayloadType());
    }

    public boolean isOther(){
        return OtherFile.class.isAssignableFrom(getPayloadType());
    }
    //endregion

    public ContentMetadata getContentMetadata() {
        return contentMetadata;
    }

    public int getMaturityLevel() {
        return maturityLevel;
    }

    @Override
    public boolean errorOccurred() {
        return errorOccurred;
    }

    @Override
    public void setErrorOccurred(boolean errorOccurred) {
        this.errorOccurred = errorOccurred;
    }

    public IngestResultMessage getResultMessage() {
        return resultMessage;
    }

    public Class<? extends PhaseMessagePayload> getPayloadType(){
        return resultMessage.getPayload().getClass();
    }

    public IngestMessagePayload getPayload() {
        return (IngestMessagePayload)resultMessage.getPayload();
    }

    public IngestResultMessage<?> getIngestResultMessage() {
        return (IngestResultMessage)resultMessage;
    }

    public IngestBatchStore getIngestBatchStore() {
        return ingestBatchStore;
    }

    public FileType getFileType() {
        return fileType;
    }

    public ProcessingErrorType getErrorType() {
        return errorType;
    }

    public PvAnalysisData getPvAnalysisData() {
        return pvAnalysisData;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting() {
        return searchPatternsCounting;
    }

    public SolrFileEntity getSavedFile() {
        return savedFile;
    }

    public Long getSavedContentId() {
        return savedContentId;
    }

    public ClaFileState getOriginalState() {
        return originalState;
    }

    IngestResultContext setIngestBatchStore(IngestBatchStore ingestBatchStore){
        this.ingestBatchStore = ingestBatchStore;
        return this;
    }

    private void setResultMessage(IngestResultMessage resultMessage) {
        this.resultMessage = resultMessage;
        this.fileType = resultMessage.getFileProperties().getType();
    }

    public void setContentMetadata(ContentMetadata contentMetadata) {
        this.contentMetadata = contentMetadata;
    }

    public void setErrorType(ProcessingErrorType errorType) {
        this.errorType = errorType;
    }

    public void setPvAnalysisData(PvAnalysisData pvAnalysisData) {
        this.pvAnalysisData = pvAnalysisData;
    }

    public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
        this.searchPatternsCounting = searchPatternsCounting;
    }

    public void setSavedFile(SolrFileEntity savedFile) {
        this.savedFile = savedFile;
    }

    public void setSavedContentId(Long savedContentId) {
        this.savedContentId = savedContentId;
    }

    public void setMaturityLevel(int maturityLevel) {
        this.maturityLevel = maturityLevel;
    }

    public void setOriginalState(ClaFileState originalState) {
        this.originalState = originalState;
    }

    public boolean isFailedTask() {
        return failedTask;
    }

    public void setFailedTask(boolean failedTask) {
        this.failedTask = failedTask;
    }
}
