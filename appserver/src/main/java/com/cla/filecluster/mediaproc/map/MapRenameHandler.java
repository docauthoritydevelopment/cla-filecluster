package com.cla.filecluster.mediaproc.map;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.MediaChangeLogDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.cla.connector.domain.dto.file.DiffType.RENAMED;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
@Service
public class MapRenameHandler {

    private static final Logger logger = LoggerFactory.getLogger(MapRenameHandler.class);

    @Autowired
    private FileCatService fileCatService;


    @Autowired
    private MapDeleteHandler mapDeleteHandler;

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private EventBus eventBus;

    /**
     * @return false if shouldn't update FileEntity store.
     */
    public boolean handleFileOrDeletedFolderRename(Long batchIdentifierNumber, RootFolder rootFolder, long runContext, Long taskId,
                                                   ClaFilePropertiesDto payload, FileEntity oldFileEntity, boolean isBelongToRF) {
        boolean updateOldFileEntity = true;
        Long fileId = oldFileEntity.getFileId();
        Long folderId = oldFileEntity.getFolderId();

        if (!isBelongToRF) { // Item moved out.
            if (payload.isFolder()) {
                mapDeleteHandler.removeFilesFromFolder(batchIdentifierNumber, runContext, taskId, rootFolder.getId(), folderId);
            }
            if (payload.isFile()) {
                mapDeleteHandler.handleDeletedFiles(runContext, taskId, rootFolder.getId(), batchIdentifierNumber, payload.getMediaItemId());
            }
            updateOldFileEntity = false;
        } else if (payload.isFile()) {
            fileCatService.processFileRename(rootFolder, payload, fileId);
            eventBus.broadcast(Topics.MAP, Event.builder()
                    .withEventType(com.cla.eventbus.events.EventType.RUN_PROGRESS)
                    .withEntry(EventKeys.RUN_ID, runContext)
                    .withEntry(EventKeys.FILE_METADATA_UPDATE, 1).build());
        }


        return updateOldFileEntity;
    }

    public void createIngestTaskForRenamed(Long batchIdentifierNumber, RootFolder rootFolder, long runContext,
                                           ScanResultMessage scanResultMessage, ClaFilePropertiesDto payload, String kvdbKey,
                                           FileEntity oldFileEntity) {
        IngestTaskJsonProperties ingestTaskJsonProps = IngestTaskJsonProperties.forDiffType(RENAMED);
        Long fileId = oldFileEntity != null ? oldFileEntity.getFileId() :
                mapServiceUtils.getFileEntity(rootFolder.getId(), kvdbKey).getFileId();
        mapServiceUtils.createIngestTask(null, payload.getFileName(), rootFolder.getId(), payload.getType(),
                batchIdentifierNumber, runContext, scanResultMessage.getTaskId(), fileId, ingestTaskJsonProps);
    }

    public boolean handleFolderMove(MediaChangeLogDto mediaChangeLogDto,
                                    Long runContext, Long rootFolderId, long time) {
        try {
            rootFolderService.acquireWriteLock(rootFolderId);
            String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
            if (kvdbRepo.getFileEntityDao().storeExists(storeName)) {
                // get folder id from kvdb
                FileEntity fileEntity = kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName,
                        fileEntityStore -> fileEntityStore.get(mediaChangeLogDto.getMediaItemId()));
                if (fileEntity != null && !mediaChangeLogDto.getFileName().equals(fileEntity.getPath())) { // folder move
                    logger.info("Processing change for folder move from {} to {}", fileEntity.getPath(), mediaChangeLogDto.getFileName());
                    fileCatService.handleFolderMove(fileEntity.getFolderId(), fileEntity.getPath(), mediaChangeLogDto.getFileName(), rootFolderId);
                    kvdbRepo.getFileEntityDao().executeInTransaction(runContext, storeName, fileEntityStore -> {
                        fileEntity.setPath(mediaChangeLogDto.getFileName());
                        fileEntity.setLastScanned(time);
                        fileEntityStore.put(mediaChangeLogDto.getMediaItemId(), fileEntity);
                        return true;
                    });
                    return true;
                }
            } else {
                logger.warn("Did not find storeName {}, not handling folder move for {}", storeName, mediaChangeLogDto);
            }
        } finally {
            rootFolderService.releaseWriteLock(rootFolderId);
        }
        return false;
    }
}
