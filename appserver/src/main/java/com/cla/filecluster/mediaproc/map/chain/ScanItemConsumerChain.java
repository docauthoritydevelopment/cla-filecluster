package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.map.MapBatchTaskHandler;
import com.cla.filecluster.service.chain.Chain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;

/**
 * Handle scan responses via chain processing
 * <p>
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class ScanItemConsumerChain {

    private static final Logger logger = LoggerFactory.getLogger(ScanItemConsumerChain.class);

    @Autowired
    private PreProcessHandler preProcessHandler;

    @Autowired
    private FolderHandler folderHandler;

    @Autowired
    private FolderRenameHandler folderRenameHandler;

    @Autowired
    private NewFileHandler newFileHandler;

    @Autowired
    private AclUpdateFileHandler aclUpdateFileHandler;

    @Autowired
    private UnDeleteFileHandler unDeleteFileHandler;

    @Autowired
    private ContentUpdateFileHandler contentUpdateFileHandler;

    @Autowired
    private DeleteFileHandler deleteFileHandler;

    @Autowired
    private EndMapForFile endMapForFile;

    @Autowired
    private FileRenameHandler fileRenameHandler;

    @Autowired
    private MapErrorHandler mapErrorHandler;

    @Autowired
    private ExchangeAttachmentHandler exchangeAttachmentHandler;

    @Autowired
    private SpecialResponseHandler specialResponseHandler;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private MapAclHandler mapAclHandler;

    @Autowired
    private ApplyFolderAssociationHandler applyFolderAssociationHandler;

    @Autowired
    private CalcGeneralDataHandler calcGeneralDataHandler;

    @Autowired
    private MapBatchTaskHandler mapBatchTaskHandler;

    private Chain<MapResultContext> mapChain = null;
    private TimeSource timeSource = new TimeSource();

    @PostConstruct
    public void init() {
        mapChain = new Chain<MapResultContext>("mapChain")
                .next(specialResponseHandler)
                .next(calcGeneralDataHandler)
                .next(folderHandler)
                .next(folderRenameHandler)
                .next(newFileHandler)
                .next(deleteFileHandler)
                .next(aclUpdateFileHandler)
                .next(contentUpdateFileHandler)
                .next(fileRenameHandler)
                .next(unDeleteFileHandler)
                .next(mapAclHandler)
                .next(applyFolderAssociationHandler)
                .next(exchangeAttachmentHandler)
                .next(endMapForFile)
                .setErrorHandler(mapErrorHandler);
    }

    public void processScanResultMessages(Long batchIdentifierNumber, Collection<ScanResultMessage> scanResultMessages) {
        long start = timeSource.currentTimeMillis();
        Optional<ScanResultMessage> firstOptional = scanResultMessages.stream().findFirst();
        if (!firstOptional.isPresent()) {
            logger.warn("No messages to consume.");
            return;
        }

        Set<Pair<Long, Long>> batchTaskToMark = new HashSet<>();
        scanResultMessages
                .stream()
                .filter(this::validateResponse)
                .flatMap(a -> preProcessHandler.handle(a, start))
                .forEach(scanResultMessage -> {
                    try {
                        MapResultContext context = new MapResultContext();
                        context.setScanResultMessage(scanResultMessage);
                        context.setTime(start);
                        context.setBatchIdentifierNumber(batchIdentifierNumber);
                        AuthenticationHelper.doWithAuthentication(
                                AutoAuthenticate.ADMIN, true,
                                () -> mapChain.handle(context)
                                , null);
                    } catch (Exception e) {
                        logger.error(String.format("Failed to consume scan result message %s", scanResultMessage), e);
                    }
                    Pair<Long, Long> batchTaskId = Pair.of(batchIdentifierNumber, scanResultMessage.getTaskId());
                    batchTaskToMark.add(batchTaskId);
                });

        batchTaskToMark.forEach(batchTaskId -> mapBatchTaskHandler.addToBatch(batchTaskId));
    }

    private boolean validateResponse(ScanResultMessage scanResultMessage) {
        PhaseMessagePayload payload = scanResultMessage.getPayload();
        if (payload == null) {
            logger.error("Invalid scanResultMessage - missing payload {}", scanResultMessage);
            return false;
        }

        Long runContext = scanResultMessage.getRunContext();
        Long jobId = jobManagerService.getJobIdCached(runContext, JobType.SCAN);
        if (jobId == null) {
            jobId = scanResultMessage.getJobId();
        }

        if (jobId == null) {
            long rootFolderId = scanResultMessage.getRootFolderId();
            logger.error("Scan Result with invalid run context {} (root folder {}) arrived. Dropping message.", runContext, rootFolderId);
            return false;
        }

        return true;
    }
}
