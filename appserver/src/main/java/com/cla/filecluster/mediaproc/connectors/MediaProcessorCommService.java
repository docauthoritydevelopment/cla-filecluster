package com.cla.filecluster.mediaproc.connectors;

import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.*;
import com.cla.common.utils.DaMessageConversionUtils;
import com.cla.config.jms.JmsBatchSupport;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import static com.cla.filecluster.kpi.PerformanceMessages.PERFORMANCE_MEASURE_PATTERN;


/**
 * Service that wraps the communication with media processors
 * Created by vladi on 6/8/2017.
 */
@Service
public class MediaProcessorCommService {

    //<editor-fold private members>
    private final static Logger logger = LoggerFactory.getLogger(MediaProcessorCommService.class);

    private final static Logger kpiLogger = LoggerFactory.getLogger(PerformanceKpiRecorder.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private JmsTemplate broadcastJmsTemplate;

    @Value("${control.requests.broadcast.topic:control.requests.broadcast}")
    private String controlRequestsBroadcast;

    @Value("${scan.requests.topic:scan.requests}")
    private String scanRequestTopic;

    @Value("${ingest.requests.topic:ingest.requests}")
    private String ingestRequestsTopic;

    @Value("${labeling.requests.topic:labeling.requests}")
    private String labelingRequestsTopic;

    @Value("${realtime.requests.topic:realtime.requests}")
    private String realtimeRequestsTopic;

    @Value("${realtime.requests.broadcast.topic:realtime.requests.broadcast}")
    private String realtimeRequestsBroadcast;

    @Value("${contentcheck.responses.broadcast.topic:contentcheck.responses.broadcast}")
    private String contentCheckResponseTopic;

    @Value("${search-patterns.max-single-matches:}")
    private Integer maxSingleSearchPatternMatches;

    @Value("${search-patterns.stop-after-multi-reached:false}")
    private boolean searchPatternStopSearchAfterMulti;

    @Value("${regulations.match.threshold.multi:10}")
    private int searchPatternCountThresholdMulti;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private ExecutorService messagingBlockingExecutor;

    //</editor-fold>

    //<editor-fold public>
    public void sendStartScanRequest(long customerDataCenterId, ScanRequestMessage scanRequestMessage) {
        try {
            String serializedRequestMessage = DaMessageConversionUtils.Scan.serializeRequestMessage(scanRequestMessage);
            logger.debug("Send scan request job ID: {} to topic {}", scanRequestMessage.getJobId(), scanRequestTopic);

            doSend(serializedRequestMessage, scanRequestTopic, customerDataCenterId);

            logger.info("Media Processor scan request sent");

            logger.info("Scan request received by mediaProcessor ");
            jobManagerService.setOpMsg(scanRequestMessage.getJobId(), "Scan request sent to media processor");
        } catch (Exception e) {
            logger.error("Failed to convert and send message {}. {}", scanRequestMessage, e, e);
        }
    }

    public boolean sendIngestRequest(long customerDataCenterId, IngestRequestMessage request, ConnectorCallback connectorCallback) {
        boolean success = false;
        try {
            String message = DaMessageConversionUtils.Ingest.serializeRequestMessage(request);
            doSend(message, ingestRequestsTopic, customerDataCenterId);
            logger.trace("Sent ingest request message task Id: {}", request.getTaskId());
            if (connectorCallback != null) {
                connectorCallback.onSuccess(request);
            }
            success = true;
        } catch (Exception e) {
            logger.error("Failed to convert and send message for {}-{} ({}). Error={}",
                    request.getJobId(), request.getTaskId(), request.getPayload().getFileId(),
                    e, e);
            if (connectorCallback != null) {
                connectorCallback.onFailure(request, e);
            }
        }
        return success;
    }

    public boolean sendIngestRequest(long customerDataCenterId, List<IngestRequestMessage> requests, ConnectorCallback connectorCallback) {
        try {
            long start = System.currentTimeMillis();
            List<String> messages = requests.stream()
                    .parallel()
                    .map(r -> {
                        try {
                            return DaMessageConversionUtils.Ingest.serializeRequestMessage(r);
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .collect(Collectors.toList());
            long end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "sendIngestRequest", "parallel serialize for " + requests.size() + " requests", 122, end - start);

            start = System.currentTimeMillis();
            messages.forEach(s -> doSend(s,ingestRequestsTopic,customerDataCenterId));
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "sendIngestRequest", "sequential send for " + requests.size() + " requests", 122, end - start);

            start = System.currentTimeMillis();
            logger.trace("Sent batch ingest requests message of {} tasks", requests.size());
            if (connectorCallback != null) {
                connectorCallback.onSuccess(requests.get(0));
                end = System.currentTimeMillis();
                kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "sendIngestRequest", "connectorCallback.onSuccess(requests.get(0)) for " + requests.size() + " requests", 130, end - start);
            }
            return true;
        } catch (Throwable e) {
            logger.error("Failed to convert and send message for dest {} {}-{} ({}). Error={}",
                    customerDataCenterId, requests.get(0).getJobId(), requests.size(), requests.get(0).getPayload().getFileId(),
                    e, e);
            if (connectorCallback != null) {
                connectorCallback.onFailure(requests.get(0), e);
            }
            return false;
        }
    }

    public void sendLabelingRequest(long customerDataCenterId, LabelRequestMessage labelRequestMessage) {
        try {
            String serializedRequestMessage = DaMessageConversionUtils.Labeling.serializeRequestMessage(labelRequestMessage);
            logger.debug("Send labeling request to topic {}", labelRequestMessage, labelingRequestsTopic);

            doSend(serializedRequestMessage, labelingRequestsTopic, customerDataCenterId);

            logger.info("Media Processor labeling request sent");
        } catch (Exception e) {
            logger.error("Failed to convert and send message {}. {}", labelRequestMessage, e, e);
        }
    }


    public void sendMPLogRequest(FileContentRequestMessage fileContentRequestMessage,
                                       ConnectorCallback connectorCallback) {
        try {
            String message = DaMessageConversionUtils.FileContent.serializeRequestMessage(fileContentRequestMessage);
            doBroadcast(realtimeRequestsBroadcast, message);
            if (connectorCallback != null) {
                connectorCallback.onSuccess(fileContentRequestMessage);
            }
            logger.debug("Sent file content message: {}", fileContentRequestMessage);
        } catch (Exception e) {
            logger.error("Failed to convert and send message: {}. {}", fileContentRequestMessage, e, e);
            if (connectorCallback != null) {
                connectorCallback.onFailure(fileContentRequestMessage, e);
            }
        }
    }

    public void sendFileContentRequest(long customerDataCenterId, FileContentRequestMessage fileContentRequestMessage,
                                       ConnectorCallback connectorCallback) {
        try {
            String message = DaMessageConversionUtils.FileContent.serializeRequestMessage(fileContentRequestMessage);
            doSend(message, realtimeRequestsTopic, customerDataCenterId);
            if (connectorCallback != null) {
                connectorCallback.onSuccess(fileContentRequestMessage);
            }
            logger.debug("Sent file content message: {}", fileContentRequestMessage);
        } catch (Exception e) {
            logger.error("Failed to convert and send message: {}. {}", fileContentRequestMessage, e, e);
            if (connectorCallback != null) {
                connectorCallback.onFailure(fileContentRequestMessage, e);
            }
        }
    }

    public void sendContentCheckResponse(ContentMetadataResponseMessage response) {
        try {
            String message = DaMessageConversionUtils.ContentCheck.serializeResultMessage(response);
            doBroadcast(contentCheckResponseTopic, message);
            logger.debug("Sent content check message: {}", response);
        } catch (Exception e) {
            logger.error("Failed to convert and send message: {}. {}", response, e, e);
        }
    }

    public void sendRefreshMediaConnectorDetails(MediaConnectionDetailsDto connectionDetails) {
        try {
            ControlRequestMessage message = new ControlRequestMessage();
            message.setType(ControlRequestType.REFRESH_CONNECTOR);
            message.setPayload(new RefreshConnectionDetailsPayload(connectionDetails));
            String messageStr = DaMessageConversionUtils.Control.serializeRequestMessage(message);
            doBroadcast(controlRequestsBroadcast, messageStr);
        } catch (JsonProcessingException | RuntimeException e) {
            logger.error("Failed to serialize invalidate media connection details control message for connection {}", connectionDetails.getId(), e);
        }
    }

    public void sendLimitReachedMessage(Long jobId) {
        try {
            ControlRequestMessage message = new ControlRequestMessage();
            message.setType(ControlRequestType.LIMIT_REACHED);
            message.setJobId(jobId);
            String messageStr = DaMessageConversionUtils.Control.serializeRequestMessage(message);
            doBroadcast(controlRequestsBroadcast, messageStr);
        } catch (JsonProcessingException | RuntimeException e) {
            logger.error("Failed to serialize limit reached control message for job {} {}", jobId, e);
        }
    }

    public void sendPatternControlRequest(Collection<TextSearchPatternImpl> searchPatterns) {
        try {
            ControlPatternRequestPayload payload = new ControlPatternRequestPayload();
            payload.setMaxSingleSearchPatternMatches(maxSingleSearchPatternMatches);
            payload.setSearchPatternStopSearchAfterMulti(searchPatternStopSearchAfterMulti);
            payload.setSearchPatternCountThresholdMulti(searchPatternCountThresholdMulti);
            payload.setSearchPatterns(searchPatterns);
            ControlRequestMessage message = new ControlRequestMessage(payload, ControlRequestType.SEARCH_PATTERNS);
            if (logger.isDebugEnabled()) {
                logger.debug("Sending control message: {} ...", message);
            }
            String messageStr = DaMessageConversionUtils.Control.serializeRequestMessage(message);
            doBroadcast(controlRequestsBroadcast, messageStr);
        } catch (JsonProcessingException e) {
            logger.error("Failed to serialize search patterns control message for {} {}",searchPatterns, e);
        } catch (RuntimeException e) {
            logger.error("Failed to send search patterns control message {} exception {} ",searchPatterns, e);
        }
    }

    public void sendJobsStatesControlRequest(List<JobStateByDate> jobStates){
        try {
            if (jobStates.size() > 0) {
                ControlStateRequestPayload payload = new ControlStateRequestPayload();
                payload.setJobStates(jobStates);
                ControlRequestMessage message = new ControlRequestMessage(payload, ControlRequestType.JOBS_STATES);
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending control message: {} ...", message);
                }
                String messageStr = DaMessageConversionUtils.Control.serializeRequestMessage(message);
                doBroadcast(controlRequestsBroadcast, messageStr);
            }
        } catch (JsonProcessingException e) {
            logger.error("Failed to serialize jobs status control message for {} {}", jobStates, e);
        } catch (RuntimeException e) {
            logger.error("Failed to send jobs status control message {} exception {} ", jobStates, e);
        }
    }

    public void broadcastControlRequest(ControlRequestMessage controlRequestMessage) {
        try {
            String messageStr = DaMessageConversionUtils.Control.serializeRequestMessage(controlRequestMessage);
            doBroadcast(controlRequestsBroadcast, messageStr);
            logger.debug("Broadcast control request for: {}", controlRequestMessage);
        } catch (Exception e) {
            logger.error("Failed to broadcast control request message for: {}. {}", controlRequestMessage, e, e);
        }
    }

    public void updateMediaProcessorCommunicationChannel(ControlRequestMessage controlRequestMessage) {
        try {
            String messageStr = DaMessageConversionUtils.Control.serializeRequestMessage(controlRequestMessage);
            doBroadcast(controlRequestsBroadcast, messageStr);
            logger.debug("Broadcast mediaprocessor with changing endpoints or active state for: {}", controlRequestMessage);
        } catch (Exception e) {
            logger.error("Failed to broadcast mediaprocessor with changing endpoints or active state for: {}. {}", controlRequestMessage, e, e);
        }

    }
    //</editor-fold>

    //<editor-fold private>
    private void doBroadcast(String topic, String messageStr) {
        broadcastJmsTemplate.convertAndSend(topic, messageStr);
    }

    private void doSend(String serializedRequestMessage, String topic, Long customerDataCenterId) {
        String topicToSend = topic;

        if (customerDataCenterId == null) {
            throw new RuntimeException("Missing customerDataCenterId, message not sent to queue. Topic " + topic);
        }
        topicToSend = customerDataCenterId.toString() + "_" + topic;
        jmsTemplate.convertAndSend(topicToSend, serializedRequestMessage);
    }

    private void doSendInBatch(List<? extends Object> serializedRequestMessages, String topic, Long customerDataCenterId) {
        String topicToSend = topic;

        if (customerDataCenterId == null) {
            throw new RuntimeException("Missing customerDataCenterId, message not sent to queue. Topic " + topic);
        }
        topicToSend = customerDataCenterId.toString() + "_" + topic;
        ((JmsBatchSupport) jmsTemplate).sendInBatch(topicToSend, serializedRequestMessages);
    }

    //</editor-fold>

}
