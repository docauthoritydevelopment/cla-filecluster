package com.cla.filecluster.mediaproc.connectors;

import com.cla.common.domain.dto.messages.ProcessingPhaseMessage;

/**
 * When message is being sent to remote device (such as MP), the connector
 * calls the onSuccess method when the message is accepted by the broker
 * and onFailure method when the broker fails to accept the message
 *
 * Created by uri on 24-May-17.
 */
public interface ConnectorCallback {

    void onSuccess(ProcessingPhaseMessage message);

    void onFailure(ProcessingPhaseMessage message, Throwable ex);
}
