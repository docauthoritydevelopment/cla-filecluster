package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.ExcelSheetIngestResult;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.service.files.ingest.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.function.Function;

/**
 * Receives and handles expression lookup results
 */
@Component
public class ExpressionLookupConsumerService {

    private final static Logger logger = LoggerFactory.getLogger(ExpressionLookupConsumerService.class);

    @Autowired
    protected ClaFileSearchPatternService claFileSearchPatternService;

	//----------------------- public methods -----------------------------------------------

    public ExpressionLookupResult expressionLookup(IngestResultMessage ingestResult) {
        logger.trace("Processing expressiion lookup response {}", ingestResult);
        Function<IngestResultMessage, ExpressionLookupResult> handler =
                getIngestResultHandler(ingestResult.getFileProperties().getType());
        return handler.apply(ingestResult);
    }

    // --------------------------- private methods -------------------------------------------------------------------
    /**
     * Get relevant handler for ingestion result processing
     *
     * @param fileType type of the ingested file
     * @return relevant ingestion result handler
     */
    private Function<IngestResultMessage, ExpressionLookupResult> getIngestResultHandler(FileType fileType) {
        if (fileType == null) {
            return this::expressionLookupOther;
        }
        switch (fileType) {
            case WORD:
            case PDF:
                return this::expressionLookupWord;
            case EXCEL:
                return this::expressionLookupExcel;
            case PST:
                throw new IllegalArgumentException("PST file type is not supported");
            case OTHER:
            case MAIL:
            default:
                return this::expressionLookupOther;
        }
    }

    private ExpressionLookupResult expressionLookupOther(IngestResultMessage<OtherFile> ingestResult) {
        Set<SearchPatternCountDto> searchPatternsCounting = null;
        OtherFile otherFile = ingestResult.getPayload();
        if (otherFile.getExtractedContent() != null) {
            if (otherFile.getExtractedContent() != null) {
                searchPatternsCounting = claFileSearchPatternService.getSearchPatternsCounting(otherFile.getExtractedContent(), true);
            }
        }
        return new ExpressionLookupResult(otherFile.getExtractedContent(), searchPatternsCounting);
    }

    private ExpressionLookupResult expressionLookupWord(IngestResultMessage<WordIngestResult> ingestResult) {
        Set<SearchPatternCountDto> searchPatternsCounting;
        final String origContent = ingestResult.getPayload().getOriginalContent();
        searchPatternsCounting = claFileSearchPatternService.getSearchPatternsCounting(origContent, true);
        return new ExpressionLookupResult(origContent, searchPatternsCounting);
    }

    private ExpressionLookupResult expressionLookupExcel(IngestResultMessage<ExcelIngestResult> ingestResult) {
        Set<SearchPatternCountDto> searchPatternsCounting;
        ExcelIngestResult excelWorkbook = ingestResult.getPayload();
        StringBuilder sb = new StringBuilder();
        for (ExcelSheetIngestResult sheet : excelWorkbook.getSheets()) {
            sb.append(sheet.getOrigContentTokens());
        }
        searchPatternsCounting = claFileSearchPatternService.getSearchPatternsCounting(sb.toString(), true);
        return new ExpressionLookupResult(sb.toString(), searchPatternsCounting);
    }
}
