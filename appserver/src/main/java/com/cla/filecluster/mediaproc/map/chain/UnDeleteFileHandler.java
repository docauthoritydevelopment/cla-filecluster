package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.scan.DeletedItemsProcessor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.chain.ChainLink;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.cla.common.domain.dto.messages.MessagePayloadType.*;
import static com.cla.connector.domain.dto.file.DiffType.UNDELETED;

/**
 * Undelete file handler
 *
 * Created by: yael
 * Created on: 8/6/2019
 */
@Service
public class UnDeleteFileHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(UnDeleteFileHandler.class);

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private FileCatService fileCatService;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        ClaFilePropertiesDto payload = (ClaFilePropertiesDto)scanResultMessage.getPayload();
        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());

        if (!scanResultMessage.isPackagedScan()) {
            mapServiceUtils.createIngestTask(null, payload.getFileName(), scanResultMessage.getRootFolderId(), payload.getType(),
                    mapContext.getBatchIdentifierNumber(), scanResultMessage.getRunContext(), scanResultMessage.getTaskId(), oldFileEntity.getFileId(), null);
        }
        fileCatService.undeleteRecord(oldFileEntity, payload);
        mapContext.setFileEntity(oldFileEntity);

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        MessagePayloadType payloadType = scanResultMessage.getPayloadType();
        List<MessagePayloadType> types = Lists.newArrayList(SCAN_CHANGES, SCAN_PST_ENTRY, SCAN, EXCHANGE365_ITEM_CHANGE);
        if (!types.contains(payloadType)) {
            logger.trace("payload type irrelevant {}, skip", mapContext);
            return false;
        }

        if (mapContext.getFstScan()) {
            logger.trace("first scan for file {}, skip", mapContext);
            return false;
        }

        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(scanResultMessage.getRootFolderId(), mapContext.getKvdbKey());

        boolean shouldHandle = (mapContext.getDiffType().equals(UNDELETED) && DeletedItemsProcessor.isValidFileEntity(oldFileEntity));
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
