package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.label.LabelingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Process labeling information on ingest result
 * This handler always runs
 */
@Component
public class MissingLabelsHandler implements ChainLink<IngestResultContext> {

    @Autowired
    private LabelingService labelingService;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return true;
    }

    @Override
    public boolean handle(IngestResultContext context) {
        SolrFileEntity fileEntity = context.getIngestBatchStore().getFileEntity(context.getResultMessage().getClaFileId());
        ClaFilePropertiesDto fileProperties = context.getResultMessage().getFileProperties();
        labelingService.enrichEntitiesForIngest(fileEntity, fileProperties);
        return true;
    }

}
