package com.cla.filecluster.mediaproc.map;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.util.categories.FileCatUtils;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
@Service
public class MapAddHandler {

    private static final Logger logger = LoggerFactory.getLogger(MapAddHandler.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MapExchangeServices mapExchangeServices;

    @SuppressWarnings("SameParameterValue")
    public ItemToCreate addScannedItem(Long rootFolderId, Long runId, ClaFilePropertiesDto claFileProp) {
        if (claFileProp == null) {
            logger.error("Invalid scanned item on run {}. No claFileProperties", runId);
            throw new RuntimeException("Invalid scanned item on run " + runId + ". No ClaFileProperties");
        }
        Map<String, FileCatBuilder> attachments = null;
        Collection<SolrFileEntity> attachmentsToDelete = null;
        Long folderId = null;
        SolrInputDocument result = null;
        try {
            String fileUrl = claFileProp.getFileName();

            if (claFileProp.isFolder()) {
                Pair<Long, SolrInputDocument> newFolderData = handleFolderAddItem(fileUrl, rootFolderId, runId, claFileProp);
                folderId = newFolderData.getKey();
                result = newFolderData.getValue();
            }

            if (claFileProp.isFile()) {
                Triple<SolrInputDocument, Map<String, FileCatBuilder>, Collection<SolrFileEntity>> newFileData = handleFileAddItem(
                        fileUrl, rootFolderId, claFileProp);
                result = newFileData.getLeft();
                attachments = newFileData.getMiddle();
                attachmentsToDelete = newFileData.getRight();
            }
        } catch (Throwable t) {
            logger.error("Failed to add scanned file for {}", claFileProp, t);
            throw t;
        }
        return new ItemToCreate(result, folderId, attachments, attachmentsToDelete);
    }

    public Pair<Long, SolrInputDocument> handleFolderAddItem(
            String fileUrl, Long rootFolderId, Long runId, ClaFilePropertiesDto claFileProp) {
        logger.debug("Handling folder scan for {}", fileUrl);
        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("createDocFolderIfNeededWithCache", "ScanItemConsumer.addScannedItem", 646);
        DocFolderService.DocFolderCacheValue docFolderCached = docFolderService.createDocFolderIfNeededWithCache(rootFolderId, fileUrl);
        kpiRecording.end();
        Long folderId = docFolderCached == null ? null : docFolderCached.getDocFolderId();
        SolrInputDocument result = null;
        if (folderId != null) {
            kpiRecording = performanceKpiRecorder.startRecording("updateDocFolderProperties", "ScanItemConsumer.addScannedItem", 651);
            docFolderService.updateDocFolderProperties(folderId, claFileProp, runId, true, true, rootFolderId);
            kpiRecording.end();
            logger.debug("Added scanned folder {}", claFileProp);
            result = new SolrInputDocument();
        } else {
            logger.warn("Skip scanned folder {}", claFileProp);
        }
        return Pair.of(folderId, result);
    }

    private Triple<SolrInputDocument, Map<String, FileCatBuilder>, Collection<SolrFileEntity>> handleFileAddItem(
            String fileUrl, Long rootFolderId, ClaFilePropertiesDto claFileProp) {
        logger.trace("Adding ClaFile for: {} props: {}", claFileProp.getFileName(), claFileProp);
        //TODO - look if URL exists first
        String folderUrl = DocFolderUtils.extractFolderFromPath(fileUrl);
        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("createDocFolderIfNeededWithCache", "ScanItemConsumer.addScannedItem", 664);
        DocFolderService.DocFolderCacheValue docFolderCached = docFolderService.createDocFolderIfNeededWithCache(rootFolderId, folderUrl);
        kpiRecording.end();

        claFileProp.setType(Optional.ofNullable(claFileProp.getType())
                .orElse(FileTypeUtils.getFileType(fileUrl)));

        kpiRecording = performanceKpiRecorder.startRecording("getByFileHashAll",
                "ScanItemConsumer.addScannedItem", 671);
        ClaFile claFileByHash = fileCatService.getByFileHashAll(rootFolderId, fileUrl);
        kpiRecording.end();

        ClaFile claFile = fileCatService.useExistingOrCreateRecord(claFileByHash,
                docFolderCached.getDocFolderId(), fileUrl,
                claFileProp.getType(), claFileProp, rootFolderId);

        ClaFile entity = null;
        if (FileCatUtils.hasId(claFile)) {
            kpiRecording = performanceKpiRecorder.startRecording("getFileObject", "ScanItemConsumer.addScannedItem", 680);
            entity = fileCatService.getFileObject(claFile.getId());
            kpiRecording.end();
        }

        if (entity != null) {
            claFile = entity;
        }

        logger.trace("Add scanned file {} before doc creation", claFile);

        // create SOLR record
        FileCatBuilder fileCatBuilder =
                fileCatService.getBuilderForCreateUpdateRecord(claFileByHash, rootFolderId, docFolderCached.getDocFolderId(),
                        docFolderCached.getParentsInfo(), claFileProp, claFile == null ? null : claFile.getContentMetadataId());
        SolrInputDocument result = fileCatBuilder.buildAtomicUpdate();
        Long fileId = (Long)MapServiceUtils.getFieldValueFromSolrInputDoc(result, CatFileFieldType.FILE_ID);
        Map<String, FileCatBuilder> attachments = fileCatBuilder.buildAtomicUpdateAttachments();

        // set ids for attachment files
        Collection<SolrFileEntity> attachmentsToDelete = mapExchangeServices.handleEmailAttachments(attachments, (claFileByHash == null ? null : claFileByHash.getId()));

        logger.trace("Add scanned file {} after doc creation", claFile);
        claFileProp.setClaFileId(fileId);
        return Triple.of(result, attachments, attachmentsToDelete);
    }
}
