package com.cla.filecluster.mediaproc.map.chain;

import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * apply folder association to new file
 *
 * Created by: yael
 * Created on: 8/12/2019
 */
@Service
public class ApplyFolderAssociationHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(ApplyFolderAssociationHandler.class);

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private AssociationsService associationsService;

    @Override
    public boolean handle(MapResultContext mapContext) {
        Long folderId = mapContext.getFolderId();
        SolrFolderEntity folder = docFolderService.getOneById(folderId)
                .orElse(docFolderService.getOneByIdFromCache(folderId)
                        .orElse(null));

        if (folder != null && folder.getAssociations() != null && !folder.getAssociations().isEmpty()) {
            logger.trace("has folder {} associations - apply to file {} assoc {}",
                    folderId, mapContext.getFileId(), folder.getAssociations());
            SolrInputDocument doc = mapContext.getSolrInputDocumentFileAndFolderId().getDocFileToCreate();
            associationsService.addTagDataToDocument(doc, mapContext.getFileId(), null,
                    null, null, false,  folder);
        }
        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        boolean shouldHandle = mapContext.getSolrInputDocumentFileAndFolderId() != null; // new file
        logger.trace("file {}, should handle {}", mapContext, shouldHandle);
        return shouldHandle;
    }
}
