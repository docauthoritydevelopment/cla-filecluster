package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobStateUpdateInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.producers.TaskProducingStrategy;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.util.JobTaskUtils;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.filecluster.kpi.PerformanceMessages.PERFORMANCE_MEASURE_PATTERN;


/**
 * Ingest manager that checks for new ingestion tasks and enqueues them to the workers queue
 * <p>
 * Created by uri on 03-May-17.
 */
@Service
public class AsyncIngestProducerService implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(AsyncIngestProducerService.class);

    private final static Logger kpiLogger = LoggerFactory.getLogger(PerformanceKpiRecorder.class);

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Autowired
    private IngestFinalizationService ingestFinalizationService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private DocStoreService docStoreService;

    @Value("${ingest-finalize.interim.threshold:50000}")
    private int interimIngestFinThreshold;

    @Value("${ingest-finalize.interim.enable:true}")
    private boolean enableInterimIngestFin;

    @Autowired
    private TaskProducingStrategySelector taskProducingStrategySelector;

    @Autowired
    private SolrTaskService solrTaskService;

    private Map<Long, Long> processNextRateCounter = new HashMap<>();

    private long counter = 0;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @Scheduled(initialDelayString = "${ingester.tasks-polling-init-delay-millis}", fixedRateString = "${ingester.tasks-polling-rate-millis}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @AutoAuthenticate
    public void processNextTasksGroup() {
        if (!isSchedulingActive) {
            return;
        }
        try {
            if (counter == 0) {
                processNextRateCounter.put(counter, System.currentTimeMillis());
                counter++;
            } else {
                kpiLogger.info(">>>> processNextTasksGroup, tick: {}, idleTime: {}", counter, System.currentTimeMillis() - processNextRateCounter.get(counter - 1));
                processNextRateCounter.put(counter, System.currentTimeMillis());
                counter++;
            }

            long overallStart = System.currentTimeMillis();

            long start = System.currentTimeMillis();
            List<SimpleJob> ingestJobs = jobManagerService.gotJobs(Collections.singletonList(JobType.INGEST), Arrays.asList(JobState.IN_PROGRESS, JobState.PENDING));
            long end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "jobManagerService.gotJobs", 80, end - start);
            if (ingestJobs.isEmpty()) {
                // avoid calling getTaskStatesByType (which involves a pricey query) if there are no in-progress/pending jobs
                logger.trace("found no started ingest jobs, skip");
                return;
            }

            // run context, state, tasks count
            start = System.currentTimeMillis();
            Table<Long, TaskState, Number> taskStatesByType = solrTaskService.getIngestTaskByRunAndStateNum(
                    ingestJobs.stream().map(Job::getRunContext).collect(Collectors.toList())
            );
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", " jobManagerService.getTaskStatesByType(TaskType.INGEST_FILE)", 91, end - start);
            if (!taskStatesByType.isEmpty()) {
                printJobCounters(taskStatesByType);
            } else {
                logger.trace("found no tasks for started ingest jobs");
            }

            Map<CustomerDataCenter, Table<Long, TaskState, Number>> taskStatesByTypeForDC = new HashMap<>();

            if (!taskStatesByType.isEmpty()) {
                start = System.currentTimeMillis();
                Set<Long> runIds = taskStatesByType.rowKeySet();
                Map<Long, Long> runToRf = fileCrawlerExecutionDetailsService.getRootFoldersForRun(runIds);
                Map<Long, CustomerDataCenter> dcForRfs = docStoreService.getDCForRootFolders(runToRf.values());

                Set<Table.Cell<Long, TaskState, Number>> cells = taskStatesByType.cellSet();
                for (Table.Cell<Long, TaskState, Number> task : cells) {
                    Long run = task.getRowKey();
                    Long rf = runToRf.get(run);
                    CustomerDataCenter dc = dcForRfs.get(rf);
                    Table<Long, TaskState, Number> tasks = taskStatesByTypeForDC.computeIfAbsent(dc, k -> HashBasedTable.create());
                    tasks.put(task.getRowKey(), task.getColumnKey(), task.getValue());
                }
                end = System.currentTimeMillis();
                kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "taskStatesByTypeForDC for " + cells.size() + " tasks and " + runIds.size() + " runs", 91, end - start);
            }


            int ingestedNonFinalizedCount = ingestFinalizationService.getIngestedNonFinalizedCount();
            if (enableInterimIngestFin && ingestedNonFinalizedCount > interimIngestFinThreshold) {

                logger.info("Running interim Ingest-Finalize after reaching {} ingested files, passing threshold: {}",
                        ingestedNonFinalizedCount, interimIngestFinThreshold);

                Collection<Long> runsWithDoneTasks = ingestJobs.stream().filter(j -> j.getCurrentTaskCount() > 0 &&
                        (j.getFailedCount() != null && j.getCurrentTaskCount() > j.getFailedCount())).map(Job::getRunContext).collect(Collectors.toList());
                ingestFinalizationService.runInterimIngestFinalize(runsWithDoneTasks);
                logger.info("Finished interim Ingest-Finalize");
            }

            // for all the contexts that have finished ingestion - create new "finalize ingest" task
            start = System.currentTimeMillis();
            Collection<Long> finishedRuns = JobTaskUtils.getFinishedRuns(ingestJobs, taskStatesByType);
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "JobTaskUtils.getFinishedRuns(taskStatesByType) for " + taskStatesByType.rowKeySet().size() + " tasks", 138, end - start);

            start = System.currentTimeMillis();
            finishedRuns.forEach(runContext -> handleIngestFinalization(runContext, true));
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "handleIngestFinalization for " + finishedRuns.size() + " runs", 143, end - start);

            // get next group of tasks to send to the queue
            // get cla file objects by the IDs from the tasks (bring all the data in one query)
            if (!taskStatesByTypeForDC.isEmpty()) {
                long totalTasksForQueue = 0;
                start = System.currentTimeMillis();
                TaskProducingStrategy strategy = taskProducingStrategySelector.getSelectedStrategy();
                int ingestLimitGlobal = strategy.getConfig().getIngestTasksQueueLimit() / taskStatesByTypeForDC.keySet().size();
                for (CustomerDataCenter dc : taskStatesByTypeForDC.keySet()) {
                    int ingestLimit = dc.getIngestTasksQueueLimit() == null ? ingestLimitGlobal : dc.getIngestTasksQueueLimit();
                    long innerStart = System.currentTimeMillis();
                    Table<Long, TaskState, Number> taskStatesTable = taskStatesByTypeForDC.get(dc);
                    Collection<SimpleTask> newTasks = strategy.nextTaskGroup(TaskType.INGEST_FILE, taskStatesTable, ingestLimit);
                    if (newTasks.isEmpty()) {
                        logger.trace("no ingest tasks to enqueue from strategy for dc {}", dc.getId());
                    }
                    long innerEnd = System.currentTimeMillis();
                    kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "strategy.nextTaskGroup(TaskType.INGEST_FILE total of " + taskStatesTable.size() + " tasks with limit " + ingestLimit, 159, innerEnd - innerStart);

                    totalTasksForQueue += newTasks.size();

                    innerStart = System.currentTimeMillis();
                    addIngestTasksToQueue(newTasks, dc.getIngestTaskExpireTime());
                    innerEnd = System.currentTimeMillis();
                    kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "addIngestTasksToQueue for " + newTasks.size() + " tasks", 166, innerEnd - innerStart);
                }
                end = System.currentTimeMillis();
                kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "addIngestTasksToQueue total of " + totalTasksForQueue + " tasks", 150, end - start);
                logger.info("Enqueued {} tasks", totalTasksForQueue);
            } else {
                logger.trace("no ingest tasks to enqueue");
            }
            end = System.currentTimeMillis();
            kpiLogger.info(">>>> Overall processNextTasksGroup in {} ms", end - overallStart);
        } catch (Exception e) {
            logger.error("Failed to process next task group for ingestion", e);
        }
    }

    private void addIngestTasksToQueue(Collection<SimpleTask> ingestTasks, Integer timeout) {
        if (ingestTasks.size() > 0) {
            Collection<Long> taskIds = ingestTasks.stream().map(Task::getId).collect(Collectors.toList());
            List<Long> fileIds = ingestTasks.stream().map(SimpleTask::getItemId).collect(Collectors.toList());
            if (logger.isTraceEnabled()) {
                logger.trace("Processing {} ingest tasks to the queue, ids {} ", taskIds.size(), taskIds.stream().map(Object::toString).collect(Collectors.joining(",")));
            } else {
                logger.debug("Processing {} ingest tasks to the queue", taskIds.size());
            }


            long start = System.currentTimeMillis();
            // Change tasks state in a single query
            solrTaskService.updateTaskStateByFileId(TaskState.ENQUEUED, fileIds);
            long end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "addIngestTasksToQueue", "taskManagerService.updateTaskState(taskIds, TaskState.ENQUEUED) for " + taskIds.size() + " tasks", 194, end - start);

            start = System.currentTimeMillis();
            RemoteMediaProcessingService.IngestStartTaskHandling taskHandling = remoteMediaProcessingService.issueStartIngestRequest(ingestTasks, timeout);
            Collection<Long> failedFileTaskIds = taskHandling.failedTaskIds;
            Collection<Long> skippedFileTaskIds = taskHandling.skippedTaskIds;
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "addIngestTasksToQueue", "remoteMediaProcessingService.issueStartIngestRequest(ingestTasks, timeout) for " + ingestTasks.size() + " tasks", 210, end - start);
            int sentTasks = (taskIds.size() - failedFileTaskIds.size());

            logger.debug("Ingest processing summary: total of {} ingest tasks were sent to MP, {} tasks failed enqueue.",
                    sentTasks, failedFileTaskIds.size());

            if (failedFileTaskIds.size() > 0) {
                logger.warn("Error: {} ingest tasks could not be enqueued. Setting state back to NEW. Ids: {} ",
                        failedFileTaskIds.size(),
                        failedFileTaskIds.stream().map(Object::toString).collect(Collectors.joining(",")));
                // No max-retry on enqueue failure - for now try forever until broker works
                // do not count failing enqueues as job retries.
                long innerStart = System.currentTimeMillis();
                // Change tasks state in a single query
                solrTaskService.updateTaskStateByFileId(TaskState.NEW, failedFileTaskIds);
                long innerEnd = System.currentTimeMillis();
                kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "addIngestTasksToQueue", "taskManagerService.updateTaskState(failedTaskIds, TaskState.NEW) for " + failedFileTaskIds.size() + " tasks", 218, innerEnd - innerStart);
            }

            if (skippedFileTaskIds.size() > 0) {
                logger.info("ingest tasks skipped, {} files already handled, set task to done", skippedFileTaskIds.size());
                solrTaskService.endTaskForFiles(skippedFileTaskIds);
            }
        }
    }

    private void printJobCounters(Table<Long, TaskState, Number> taskStatesByType) {
        Set<Long> runIds = taskStatesByType.rowKeySet();
        long start = System.currentTimeMillis();
        for (Long runId : runIds) {
            Map<TaskState, Number> row = taskStatesByType.row(runId);
            if (logger.isDebugEnabled()) {
                Long jobIdCached = jobManagerService.getJobIdCached(runId, JobType.INGEST);
                SimpleJobDto jobDto = jobManagerService.getJobAsDto(jobIdCached);
                StringBuilder sb = new StringBuilder("Ingest Job ").append(jobIdCached)
                        .append(" with run ID ").append(runId)
                        .append(" at state ").append(jobDto.getState())
                        .append(":");
                for (Map.Entry<TaskState, Number> taskStateNumberEntry : row.entrySet()) {
                    sb.append(" ")
                            .append(taskStateNumberEntry.getKey())
                            .append(" - ")
                            .append(taskStateNumberEntry.getValue());
                }
                logger.debug(sb.toString());
            }
        }
        long end = System.currentTimeMillis();
        kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "processNextTasksGroup", "updateJobCounters for " + runIds.size() + " runIds", 182, end - start);
    }

    /**
     * Create ingest finalize task
     *
     * @param runContext         run ID
     * @param expectIngestActive indicates whether to run only if the INGEST job is active
     */
    public void handleIngestFinalization(Long runContext, boolean expectIngestActive) {
        logger.debug("Handle Ingest Finalization on run {}", runContext);

        try {
            Long ingestJobId = jobManagerService.getJobIdCached(runContext, JobType.INGEST);
            SimpleJobDto jobAsDto = jobManagerService.getJobAsDto(ingestJobId);

            // if the job is no longer active - no treatment is needed, skip
            if (expectIngestActive && !jobManagerService.isJobActive(ingestJobId)) {
                logger.debug("Job {} is not active", jobAsDto);
                return;
            }
            logger.debug("Ingest Job Done status: {}", jobAsDto);

            makeJobsStateTransition(runContext);

        } catch (Exception e) {
            logger.error("Failed to finalize ingestion on run {} - unexpected error {}", runContext, e.getMessage());
        }
    }

    private void makeJobsStateTransition(Long runContext) {
        Long ingestJobId = jobManagerService.getJobIdCached(runContext, JobType.INGEST);
        Long ingestFinJobId = jobManagerService.getJobIdCached(runContext, JobType.INGEST_FINALIZE);
        // Update the state of ingest and ingest-finalize in a transaction
        JobStateUpdateInfo updateIngestStateInfo =
                new JobStateUpdateInfo(ingestJobId, JobState.DONE, null, "Ingestion completed", true);
        JobStateUpdateInfo updateIngestFinStateInfo =
                new JobStateUpdateInfo(ingestFinJobId, JobState.IN_PROGRESS, null, "Starting ingestion finalization", false);
        jobManagerService.updateJobStates(Arrays.asList(updateIngestStateInfo, updateIngestFinStateInfo));
    }
}
