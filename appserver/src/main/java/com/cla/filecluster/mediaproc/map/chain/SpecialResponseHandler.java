package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.file.ScanProgressPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Handles special map responses (dir listing and progress)
 *
 * Created by: yael
 * Created on: 8/8/2019
 */
@Service
public class SpecialResponseHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(SpecialResponseHandler.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private DocStoreService docStoreService;

    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();

        if (scanResultMessage.getPayloadType() == MessagePayloadType.SCAN_DIR_LISTING) {
            logger.trace("processing scan result msg for type SCAN_DIR_LISTING NOT SUPPORTED");
            return false;
        }

        if (scanResultMessage.getPayloadType() == MessagePayloadType.SCAN_PROGRESS) {
            ScanProgressPayload payload = (ScanProgressPayload)scanResultMessage.getPayload();
            long itemsScanned = payload.getItemsScanned();
            logger.trace("processing scan result msg for items {} and type SCAN_PROGRESS", itemsScanned);
            eventBus.broadcast(Topics.MAP, Event.builder()
                    .withEventType(EventType.JOB_PROGRESS)
                    .withEntry(EventKeys.JOB_ID, scanResultMessage.getJobId())
                    .withEntry(EventKeys.TASK_ID, scanResultMessage.getJobId())
                    .withEntry(EventKeys.ITEM_COUNT, itemsScanned).build());
            return false;
        }

        if ((scanResultMessage.getPayload() instanceof ClaFilePropertiesDto)) {
            ClaFilePropertiesDto prop = (ClaFilePropertiesDto)scanResultMessage.getPayload();
            if (CollectionUtils.isNotEmpty(prop.getErrors()) && prop.getFileName() == null) {
                logger.info("Scan errors found at {}. Not ingesting.", prop);

                RootFolder rootFolder = docStoreService.getRootFolderCached(scanResultMessage.getRootFolderId());

                for (ScanErrorDto scanErrorDto : prop.getErrors()) {
                    scanErrorsService.addScanError(scanErrorDto.getText(), scanErrorDto.getExceptionText(),
                            null, scanErrorDto.getPath(), scanResultMessage.getRunContext(),
                            scanResultMessage.getRootFolderId(), rootFolder.getMediaType());
                    eventBus.broadcast(Topics.MAP, Event.builder()
                            .withEventType(EventType.JOB_PROGRESS)
                            .withEntry(EventKeys.JOB_ID, mapContext.getScanResultMessage().getJobId())
                            .withEntry(EventKeys.FILE_MAP_FAIL, 1).build());
                }
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        return true;
    }
}
