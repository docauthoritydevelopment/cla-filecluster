package com.cla.filecluster.mediaproc;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.messages.LabelRequestType;
import com.cla.common.domain.dto.messages.LabelTaskParameters;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.connector.mediaconnector.labeling.LabelResult;
import com.cla.connector.mediaconnector.labeling.mip.MipLabelResultTyped;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.metadata.MetadataTranslationService;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.label.LabelingService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.filecluster.repository.solr.query.SolrOperator.*;

@Service
public class AsyncLabelingConsumerService implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(AsyncLabelingConsumerService.class);

    @Autowired
    private LabelingService labelingService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private MetadataTranslationService metadataTranslationService;

    @Value("${metadata.mip.type:}")
    private String mipMetadataType;

    private Map<Long, FileLabelingData> fileData = new HashMap<>();
    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    public synchronized void handleLabeling(Long fileId, List<LabelResult> externalMetadata, LabelTaskParameters params) {
        fileData.put(fileId, new FileLabelingData(externalMetadata, params.getLabelId(), params.getLabelingVendor(),
                params.getNativeId(), params.getRequestType()));
        logger.debug("received label {} response for file {} vendor {} label {} will be handled in batch", params.getRequestType(),
                fileId, params.getLabelingVendor(), params.getLabelId());
    }

    @Scheduled(initialDelayString = "${labeling.tasks-polling-init-delay-millis}", fixedRateString = "${labeling.tasks-polling-rate-millis}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @AutoAuthenticate
    public void processNextTasksGroup() {
        if (!isSchedulingActive) {
            return;
        }
        try {
            Map<Long, FileLabelingData> fileDataToSave;
            synchronized (this) {
                fileDataToSave = fileData;
                fileData = new HashMap<>();
            }

            List<SolrInputDocument> docs = new ArrayList<>();
            if (fileDataToSave != null && !fileDataToSave.isEmpty()) {
                List<SolrFileEntity> fileEntities = fileCatService.findByFileIds(fileDataToSave.keySet(),
                        Lists.newArrayList(CatFileFieldType.FILE_ID, CatFileFieldType.ID,
                                CatFileFieldType.ACTIONS_EXPECTED, CatFileFieldType.ACTION_CONFIRMATION));
                Map<Long, SolrFileEntity> filesMap = fileEntities.stream().
                        collect(Collectors.toMap(SolrFileEntity::getFileId, Function.identity()));
                fileDataToSave.forEach((fileId, fileLabelingData) -> {
                    try {
                        SolrInputDocument result;
                        if (fileLabelingData.requestType.equals(LabelRequestType.WRITE_LABEL)) {
                            result = handleWriteResponse(fileId, fileLabelingData, filesMap);
                        } else {
                            result = handleReadResponse(fileId, fileLabelingData, filesMap);
                        }

                        if (result != null) {
                            docs.add(result);
                        }

                    } catch (Exception e) {
                        logger.error("problem setting labels on file {}", fileId, e);
                    }
                });
            }

            if (!docs.isEmpty()) {
                fileCatService.saveAll(docs);
                fileCatService.softCommitNoWaitFlush();
            }
        } catch (Exception e) {
            logger.error("problem setting labels on files", e);
        }
    }

    private SolrInputDocument handleWriteResponse(Long fileId, FileLabelingData fileLabelingData,
                                                  Map<Long, SolrFileEntity> filesMap) {
        SolrFileEntity fileEntity = filesMap.get(fileId);
        if (fileEntity != null) {

            logger.debug("handle label write response for file {} vendor {} label {}",
                    fileId, fileLabelingData.vendor, fileLabelingData.nativeId);

            AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
            builder.setId(ID, fileEntity.getId())
                    .addModifier(UPDATE_DATE, SET, System.currentTimeMillis());

            String actionExpectedPrefix = LabelingService.createActionExpected(
                    LabelingVendor.DocAuthority.getName(), String.valueOf(fileLabelingData.labelId), "");

            if (fileEntity.getActionsExpected() != null) {
                List<String> toRemove = fileEntity.getActionsExpected().stream()
                        .filter(a -> a.startsWith(actionExpectedPrefix)).collect(Collectors.toList());
                toRemove.forEach(rem -> builder.addModifierNotNull(ACTIONS_EXPECTED, REMOVE, rem));
            }

            builder.addModifierNotNull(ACTION_CONFIRMATION, ADD,
                    actionExpectedPrefix.substring(0, actionExpectedPrefix.length()-1));
            return builder.build();
        }
        return null;
    }


    private SolrInputDocument handleReadResponse(Long fileId, FileLabelingData fileLabelingData,
                                                 Map<Long, SolrFileEntity> filesMap) {

        if (fileLabelingData.externalMetadata == null) {
            logger.warn("got no label data for file {} skip labeling read response", fileId);
            return null;
        }

        SolrFileEntity fileEntity = filesMap.get(fileId);
        if (fileEntity == null) {
            logger.warn("found no file entity file {} skip labeling read response", fileId);
            return null;
        }

        logger.debug("handle label read response for file {} vendor {} label {}",
                fileId, fileLabelingData.vendor, fileLabelingData.nativeId);

        List<Long> labelIds = new ArrayList<>();
        List<String> jsonExternalMetadata = new ArrayList<>();
        List<String> expectedActionRemoval = new ArrayList<>();
        Set<String> actionConfirmationRemoval = new HashSet<>();
        List<String> daMetadata = new ArrayList<>();
        List<String> daMetadataType = new ArrayList<>();
        fileLabelingData.externalMetadata.forEach(labelResult -> {
            if (labelResult instanceof MipLabelResultTyped) {
                MipLabelResultTyped labelResultMip = (MipLabelResultTyped)labelResult;
                LabelDto labelDto = labelingService.transform(fileLabelingData.vendor, labelResultMip.getName());
                if (labelDto != null) {
                    labelIds.add(labelDto.getId());
                    String actionConfirmation = LabelingService.createActionExpected(
                            fileLabelingData.vendor.getName(), String.valueOf(labelDto.getId()), "");
                    actionConfirmationRemoval.add(actionConfirmation.substring(0, actionConfirmation.length()-1));
                }
                expectedActionRemoval.add(LabelingService.createActionExpected(
                        fileLabelingData.vendor.getName(), labelResultMip.getId(), ""));

                Map<String, String> metadata = ImmutableMap.of(mipMetadataType, labelResultMip.getName());
                Map<String, String> daMetadataMap = metadataTranslationService.translateMetadata(metadata);
                daMetadata.addAll(metadataTranslationService.mapToListWithSeparator(daMetadataMap));
                daMetadataType.addAll(daMetadataMap.keySet());
            }
            addJsonMetadata(labelResult, jsonExternalMetadata);
        });

        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
        builder.setId(ID, fileEntity.getId())
                .addModifier(UPDATE_DATE, SET, System.currentTimeMillis())
                .addModifierNotNull(DA_LABELS, SET, labelIds);

        daMetadata.forEach(dm -> {
            builder.addModifierNotNull(DA_METADATA, ADD, dm);
        });

        daMetadataType.forEach(dm -> {
            builder.addModifierNotNull(DA_METADATA_TYPE, ADD, dm);
        });

        expectedActionRemoval.forEach(action -> {
            if (fileEntity.getActionsExpected() != null) {
                List<String> toRemove = fileEntity.getActionsExpected().stream()
                        .filter(a -> a.startsWith(action)).collect(Collectors.toList());
                toRemove.forEach(rem -> builder.addModifierNotNull(ACTIONS_EXPECTED, REMOVE, rem));
            }
        });

        actionConfirmationRemoval.forEach(action -> {
            builder.addModifierNotNull(ACTION_CONFIRMATION, REMOVE, action);
        });

        jsonExternalMetadata.forEach(json -> builder.addModifierNotNull(EXTERNAL_METADATA, ADD, json));
        return builder.build();
    }

    private void addJsonMetadata(LabelResult labelResult, List<String> jsonExternalMetadata) {
        try {
            String json = ModelDtoConversionUtils.getMapper().writeValueAsString(labelResult);
            jsonExternalMetadata.add(json);
        } catch (Exception e) {
            logger.error("problem adding label result to external metadata {}", labelResult, e);
        }
    }

    private class FileLabelingData {
        private List<LabelResult> externalMetadata;
        private LabelingVendor vendor;
        private LabelRequestType requestType;
        private String nativeId;
        private Long labelId;

        FileLabelingData(List<LabelResult> externalMetadata, Long labelId,
                         LabelingVendor vendor, String nativeId, LabelRequestType requestType) {
            this.externalMetadata = externalMetadata;
            this.vendor = vendor;
            this.labelId = labelId;
            this.nativeId = nativeId;
            this.requestType = requestType;
        }
    }
}
