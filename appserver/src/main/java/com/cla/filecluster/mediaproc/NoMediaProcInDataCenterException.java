package com.cla.filecluster.mediaproc;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public class NoMediaProcInDataCenterException extends RuntimeException {

    public NoMediaProcInDataCenterException(String reason) {
        super(reason);
    }

}
