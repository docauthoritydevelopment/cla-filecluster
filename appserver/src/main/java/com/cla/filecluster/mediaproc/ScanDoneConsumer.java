package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.jobmanager.*;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.Task;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.connectors.ScanResultMessageConsumer;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.files.MailboxService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.managers.FileClusterAppService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 *
 * Created by uri on 15-Mar-17.
 */
@Service
public class ScanDoneConsumer implements ScanResultMessageConsumer {

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MailboxService mailboxService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private FileClusterAppService fileClusterAppService;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private EventBus eventBus;

    @Value("${scanner.scanTaskDepth:0}")
    private int scanTaskDepth;

    private static final Logger logger = LoggerFactory.getLogger(ScanDoneConsumer.class);

    private Table<Long, Long, AtomicInteger> tasksRunningCounter = HashBasedTable.create();
    private List<ScanResultMessage> msgAwaitProcess = new ArrayList<>();

    public void pausedTasks(Collection<Long> tasks) {
        tasks.forEach(t -> {
            Map<Long, AtomicInteger> values = tasksRunningCounter.column(t);
            synchronized (this) {
                values.keySet().forEach(k -> tasksRunningCounter.remove(k, t));
            }
        });

    }

    public void updateTasksRunningCounter(Collection<Pair<Long, Long>> batchTaskIds, boolean inc) {
        Map<Long, List<Long>> batch = new HashMap<>();
        batchTaskIds.forEach(p -> {
            List<Long> tasks = batch.computeIfAbsent(p.getKey(), m -> new ArrayList<>());
            tasks.add(p.getValue());
        });
        batch.forEach((b, t) -> updateTasksRunningCounter(b, t, inc));
    }

    public void updateTasksRunningCounter(Long batchIdentifierNumber, Collection<Long> taskIds, boolean inc) {
        if (taskIds == null || taskIds.size() == 0) return;

        synchronized (this) {
            taskIds.forEach(t -> {
                AtomicInteger counter = tasksRunningCounter.get(t, batchIdentifierNumber);
                if (counter == null) {
                    counter = new AtomicInteger();
                }

                if (inc) {
                    counter.incrementAndGet();
                    tasksRunningCounter.put(t, batchIdentifierNumber, counter);
                } else if (counter.get() > 0) {
                    int res = counter.decrementAndGet();
                    if (res == 0) {
                        tasksRunningCounter.remove(t, batchIdentifierNumber);
                    } else {
                        tasksRunningCounter.put(t, batchIdentifierNumber, counter);
                    }
                }
            });
        }
        if (!inc && msgAwaitProcess.size() > 0) {
            List<ScanResultMessage> msg;
            synchronized (this) {
                msg = msgAwaitProcess;
                msgAwaitProcess = new ArrayList<>();
            }
            msg.forEach(this::accept);
        }
    }

    @Override
    public void accept(ScanResultMessage scanResultMessage) {
        synchronized (this) {
            if (tasksRunningCounter.containsRow(scanResultMessage.getTaskId())) {
                msgAwaitProcess.add(scanResultMessage);
                logger.debug("counter exists wait with scan done for task {}", scanResultMessage.getTaskId());
                return;
            }
        }

        Long runContext = scanResultMessage.getRunContext();
        MessagePayloadType payloadType = scanResultMessage.getPayloadType();
        try {
            if (MessagePayloadType.SCAN_FAILED.equals(payloadType)) {
                scanRequestFailed(scanResultMessage, runContext);
            } else if (MessagePayloadType.SCAN_DONE.equals(payloadType)) {
                scanDone(scanResultMessage, runContext);
            } else {
                throw new RuntimeException("UNSUPPORTED Message Payload type passed to SCAN DONE CONSUMER (" + scanResultMessage + ")");
            }
            logger.debug("Scan Done consumer finished for task {}", scanResultMessage.getTaskId());
        } catch (RuntimeException e) {
            CrawlRun crawlRun = fileCrawlerExecutionDetailsService.getCrawlRun(runContext);
            if (crawlRun != null) {
                logger.error("Failed to finish scan and start ingest. Failing run {} ({})", crawlRun, e.getMessage(), e);
                //      jobManagerService.handleFailedFinishJob(scanResultMessage.getJobId(),"Failed to finish map ");
                jobManagerService.handleFailedFinishJob(scanResultMessage.getJobId(), "Failed to finish map ");
                jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_ERROR);
            }
            throw e;
        }
    }

    private void scanRequestFailed(ScanResultMessage scanResultMessage, Long runContext) {
        logger.error("SCAN FAILED {}.{} ", runContext, scanResultMessage);
        Long scanJobId = jobManagerService.getJobIdCached(runContext, JobType.SCAN);
        if (scanJobId == null) {
            logger.error("Got Scan Done message on an invalid run context {}", runContext);
            return;
        }

        updateJobTasksState(scanJobId, TaskState.FAILED);

        if (!Strings.isNullOrEmpty(scanResultMessage.getErrorMessage())) {
            RootFolder rootFolder = docStoreService.getRootFolderCached(scanResultMessage.getRootFolderId());
            scanErrorsService.addScanError(scanResultMessage.getErrorMessage(), (String)null,
                    null, scanResultMessage.getPath(), scanResultMessage.getRunContext(),
                    scanResultMessage.getRootFolderId(), rootFolder.getMediaType());
        }

        jobManagerService.scanTasksEnded(scanJobId);
        jobManagerService.handleFailedFinishJob(scanResultMessage.getJobId(), "Failed to finish map ");
        jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_ERROR);
    }

    private void scanDone(ScanResultMessage scanResultMessage, Long runContext) {

        logger.debug("got scan done for task {} - change status", scanResultMessage.getTaskId());
        Long rootFolderId = scanResultMessage.getRootFolderId();
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
        int scanTaskDepthForRootFolder = rootFolder.getScanTaskDepth() == null ? scanTaskDepth : rootFolder.getScanTaskDepth();
        boolean isDeltaScan = scanResultMessage.isDeltaScan();
        String json = null;
        if (!isDeltaScan && scanTaskDepthForRootFolder > 0) { // if multi task and deletion should be done on server side
            int depth = scanResultMessage.getPath() == null ? 0 :
                    DocFolderUtils.calculateDepth(FileNamingUtils.convertPathToUniversalString(scanResultMessage.getPath())) - rootFolder.getBaseDepth();
            boolean fstLevelOnly = depth < scanTaskDepthForRootFolder;

            Map<String, String> scanTaskParams = new HashMap<>();
            scanTaskParams.put("runDeletePerPath", "true");
            scanTaskParams.put("fstLevelOnly", String.valueOf(fstLevelOnly));
            json = ModelDtoConversionUtils.mapToJson(scanTaskParams);
        }

        // for exchange mailbox task in rescan (not full) create tasks for the folders
        if (!rootFolder.getMediaType().hasPath() && scanResultMessage.getPath() == null) {
            fileClusterAppService.createExchangeRelatedMapTasksForMailbox(rootFolderId,
                    runContext, scanResultMessage.getJobId(),
                    rootFolder.getFirstScan(), rootFolder.getReingestTimeStampMs() > 0, scanResultMessage.getMailboxUPN());
        }

        // for exchange update sync state
        if (requiresSyncStateUpdate(scanResultMessage, rootFolder)) {
            if (scanResultMessage.getPath() == null) { // for mailbox
                mailboxService.updateMailboxDtoSyncState(rootFolderId, scanResultMessage.getMailboxUPN(), scanResultMessage.getSyncState());
            } else { // for folder
                String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
                Long folderId = null;
                if (kvdbRepo.getFileEntityDao().storeExists(storeName)) { // search folder in kvdb
                    folderId = kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {
                        FileEntity fileEntity = fileEntityStore.get(scanResultMessage.getPath()); // assume populated by media entity id
                        return (fileEntity == null ? null : fileEntity.getFolderId());
                    });
                }
                docFolderService.updateFolderSyncState(folderId, rootFolderId, scanResultMessage.getPath(), scanResultMessage.getSyncState());
            }
        }
        jobManagerService.updateTaskStatus(scanResultMessage.getTaskId(), TaskState.DONE, json);
        if (scanResultMessage.getProgressUnreportedFiles() != null) {
            jobManagerService.updateScanTaskJobProgress(scanResultMessage.getJobId(), scanResultMessage.getTaskId(), scanResultMessage.getProgressUnreportedFiles());
        }
        jobManagerService.scanTaskEnded(scanResultMessage.getJobId(), scanResultMessage.getTaskId());

        // get active tasks for job
        int tasksRunningCount = jobManagerService.getNotDoneTasksForJobByType(scanResultMessage.getJobId(), TaskType.SCAN_ROOT_FOLDER);

        // finish with root folder if needed
        if (tasksRunningCount == 0) {
            boolean firstScan = scanResultMessage.isFirstScan();
            logger.info("Scan done on rootFolder {} at run {} (firstScan={}). Run Scan finalize", rootFolderId, runContext, firstScan);
            CrawlRun crawlRun = fileCrawlerExecutionDetailsService.getCrawlRun(runContext);
            if (crawlRun == null) {
                logger.warn("SCAN DONE message arrived on an invalid run id " + runContext);
                return;
                //  throw new RuntimeException("SCAN DONE message arrived on an invalid run id " + runContext);
            }
            crawlRun.setRootFolderAccessible(scanResultMessage.isRootFolderAccessible());

            // if first scan, no new files and folder inaccessible - fail scan
            if (rootFolder.getFirstScan() != null && rootFolder.getFirstScan() && !scanResultMessage.isRootFolderAccessible()) {
                Integer taskCounter = jobManagerService.getCachedJobTaskCounter(scanResultMessage.getJobId());
                if (taskCounter == null || taskCounter == 0) {
                    jobManagerService.updateJobState(scanResultMessage.getJobId(), JobState.DONE, JobCompletionStatus.ERROR, "Folder inaccessible", true);
                    jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_INITIATED);
                    return;
                }
            }

            finishScan(runContext, rootFolderId, !isDeltaScan);
        }
    }

    private boolean requiresSyncStateUpdate(ScanResultMessage scanResultMessage, RootFolder rootFolder) {
        return (rootFolder.getMediaType().equals(MediaType.EXCHANGE_365) ||
                rootFolder.getMediaType().equals(MediaType.ONE_DRIVE)) && scanResultMessage.getSyncState() != null;
    }

    public void finishScan(Long runContext, Long rootFolderId, boolean scannedFromScratch) {
        logger.debug("Finish scan on run context {}", runContext);
        Long scanJobId = jobManagerService.getJobIdCached(runContext, JobType.SCAN);
        if (scanJobId == null) {
            logger.error("Got Scan Done message on an invalid run context {}", runContext);
            return;
        }
        updateJobTasksState(scanJobId, TaskState.DONE);

        Long scanFinalizeJobId = jobManagerService.getJobIdCached(runContext, JobType.SCAN_FINALIZE);
        updateScanFinInitDetails(scanFinalizeJobId, scannedFromScratch);

        JobStateUpdateInfo updateScanStateInfo =
                new JobStateUpdateInfo(scanJobId, JobState.DONE, null, "Map completed", true);
        if (scanFinalizeJobId != null) {
            JobStateUpdateInfo updateScanFinStateInfo =
                    new JobStateUpdateInfo(scanFinalizeJobId, JobState.IN_PROGRESS, null, "Start map finalization", false);
            jobManagerService.updateJobStates(Arrays.asList(updateScanStateInfo, updateScanFinStateInfo));
            Map<String, String> params = new HashMap<>();
            params.put("scannedFromScratch", String.valueOf(scannedFromScratch));
            String json = ModelDtoConversionUtils.mapToJson(params);
            jobManagerService.createTaskIfAbsent(rootFolderId, scanFinalizeJobId, TaskType.SCAN_FINALIZE, json);
        } else {
            logger.error("Got Scan Done message but cant find map fin job for run context {}", runContext);
        }
        eventBus.broadcast(Topics.RUN, Event.builder()
                .withEventType(EventType.SCAN_DONE_MAP)
                .withEntry(EventKeys.RUN_ID, runContext)
                .build());
    }

    private void updateScanFinInitDetails(Long scanFinalizeJobId, boolean scannedFromScratch) {
        JobInitiationDetails initiationDetails = new JobInitiationDetails();
        initiationDetails.setScannedFromScratch(scannedFromScratch);
        jobManagerService.updateInitiationDetails(scanFinalizeJobId,
                JsonDtoConversionUtils.jobInitiationDetailsToString(initiationDetails));
    }

    private void updateJobTasksState(Long scanJobId, TaskState newTaskState) {
        List<SimpleTask> scanTasks =
                jobManagerService.getTaskByJobIdAndTaskTypeAndStates(
                        scanJobId, TaskType.SCAN_ROOT_FOLDER, TaskState.ENQUEUED, TaskState.CONSUMING);
        List<Long> taskIds = scanTasks.stream().map(Task::getId).collect(Collectors.toList());
        jobManagerService.updateTasksStatus(taskIds, newTaskState);
    }

}
