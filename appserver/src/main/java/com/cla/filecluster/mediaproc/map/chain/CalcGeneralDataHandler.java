package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.FileToAdd;
import com.cla.filecluster.mediaproc.map.MapServiceUtils;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.files.MailboxCache;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.kvdb.domain.FileEntityUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

/**
 * Calculate fields to be used by other chain links
 * <p>
 * Created by: yael
 * Created on: 8/12/2019
 */
@Service
public class CalcGeneralDataHandler implements ChainLink<MapResultContext> {

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private MailboxCache mailboxCache;

    private static final Set<MediaType> UPN_BASED_MEDIA_TYPES = Sets.newHashSet(
            MediaType.ONE_DRIVE);


    @Override
    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        long rootFolderId = scanResultMessage.getRootFolderId();
        Long runContext = scanResultMessage.getRunContext();

        ClaFilePropertiesDto prop = ((ClaFilePropertiesDto) scanResultMessage.getPayload());
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
        final String fileName = prop.getFileName();
        String kvdbKey = Optional.ofNullable(prop.getMediaItemId()).orElse(fileName);

        boolean isFileShare = Optional.ofNullable(rootFolder.getMediaConnectionDetails())
                .map(conn -> conn.getMediaType() == MediaType.FILE_SHARE)
                .orElse(true);

        boolean fstScan = scanResultMessage.isFirstScan() ||
                scanResultMessage.getPayloadType().equals(MessagePayloadType.EXCHANGE365_ITEM_CHANGE);

        FileEntity oldFileEntity = scanResultMessage.isFirstScan() ? null :
                mapServiceUtils.getFileEntity(rootFolder.getId(), kvdbKey);
        DiffType diffType = FileEntityUtils.detectChanges(!scanResultMessage.isDeltaScan(), oldFileEntity, prop, isFileShare);

        boolean isAlreadyHandled = false;
        if (oldFileEntity == null && !diffType.equals(DiffType.NEW)) {
            FileToAdd file = new FileToAdd(null, null, kvdbKey, rootFolder.getId(), runContext);
            if (mapBatchResultsProcessor.fileAlreadyHandled(file)) {
                isAlreadyHandled = true;
            }
        }

        boolean isBelongToRF;

        if (UPN_BASED_MEDIA_TYPES.contains(rootFolder.getMediaType())) { // Currently only one drive
            if (fileName == null) {
                isBelongToRF = true;
            } else {
                String fileNameToCheck = fileName.startsWith("/") ? fileName.substring(1) : fileName;
                fileNameToCheck = StringUtils.replace(fileNameToCheck, "_", "@", 1);
                fileNameToCheck = StringUtils.replace(fileNameToCheck, "_", ".", 2);
                isBelongToRF = mailboxCache.getMailBoxes(rootFolderId).stream().anyMatch(fileNameToCheck::startsWith);
            }
        } else {
            //avoiding the path to be recognized as un-equal based on inconsistent path separator (/ or \)
            isBelongToRF = rootFolder.getPath() == null || fileName.startsWith(rootFolder.getPath())
                    || (!Strings.isNullOrEmpty(rootFolder.getRealPath()) && fileName.startsWith(rootFolder.getRealPath()));
        }

        boolean isNewFile = oldFileEntity == null || oldFileEntity.isDeleted();

        mapContext.setKvdbKey(kvdbKey);
        mapContext.setFileShare(isFileShare);
        mapContext.setDiffType(diffType);
        mapContext.setAlreadyHandled(isAlreadyHandled);
        mapContext.setBelongToRF(isBelongToRF);
        mapContext.setNewFile(isNewFile);
        mapContext.setFstScan(fstScan);

        return true;
    }

    @Override
    public boolean shouldHandle(MapResultContext mapContext) {
        return (mapContext.getScanResultMessage().getPayload() instanceof ClaFilePropertiesDto);
    }
}
