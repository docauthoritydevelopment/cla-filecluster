package com.cla.filecluster.mediaproc.map;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.connector.domain.dto.file.DiffType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.jobmanager.domain.entity.IngestTaskJsonProperties;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
@Service
public class MapServiceUtils {

    private static final Logger logger = LoggerFactory.getLogger(MapServiceUtils.class);

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private FileCrawlerExecutionDetailsService runService;

    @Autowired
    private EventBus eventBus;

    public String getStoreName(Long rootFolderId) {
        return KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
    }

    public static Pair<Long, Long> createBatchTaskId(Long batchIdentifierNumber, Long taskId) {
        return Pair.of(batchIdentifierNumber, taskId);
    }

    public FileEntity getFileEntity(long rootFolderId, String filename) {
        String storeName = getStoreName(rootFolderId);
        return kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> fileEntityStore.get(filename));
    }

    public void deleteFromKvdb(long rootFolderId, Long runId, String filename) {
        String storeName = getStoreName(rootFolderId);
        kvdbRepo.getFileEntityDao().executeInTransaction(runId, storeName, fileEntityStore -> {
            fileEntityStore.delete(filename);
            return true;
        });
    }

    public void handleFinishMapForNewFile(SolrInputDocument doc, long fileId, FileType type, String fileFullPath, Long rootFolderId,
                                            long runId, long jobId, long taskId, long batchIdentifierNumber,
                                            List<Long> containedFileIDs){
        //increment counters
        int meaningfulCounter = type.isMeaningfulFile() ? 1 : 0;
        int fileNum = 1 + (containedFileIDs == null ? 0 : containedFileIDs.size());
        eventBus.broadcast(Topics.MAP, Event.builder()
                .withEventType(EventType.JOB_PROGRESS)
                .withEntry(EventKeys.JOB_ID, jobId)
                .withEntry(EventKeys.MEANINGFUL_ITEM_COUNT, meaningfulCounter)
                .withEntry(EventKeys.ITEM_COUNT, fileNum).build());

        // if has content create ingest task
        if (type.hasContent()) {
            IngestTaskJsonProperties ingestTaskJsonProps = IngestTaskJsonProperties.forDiffType(DiffType.NEW);
            ingestTaskJsonProps.setContainedFileIDs(containedFileIDs);
            createIngestTask(doc, fileFullPath, rootFolderId, type, batchIdentifierNumber, runId, taskId, fileId, ingestTaskJsonProps);
        }
    }

    public void createIngestTask(SolrInputDocument doc, String fileFullPath, Long rootFolderId, FileType type,
                                  Long batchIdentifierNumber, Long runContext, Long taskId, Long itemId, IngestTaskJsonProperties jsonProps) {

        if (!docStoreService.getRootFolderIngestTypePredicate(rootFolderId).test(fileFullPath)) {
            return;
        }

        if (type == null) {
            type = FileTypeUtils.getFileType(fileFullPath);
        }

        if (type != null && !type.hasContent()) {
            return;
        }

        Long ingestJobId = jobManagerService.getJobIdCached(runContext, JobType.INGEST);
        if (ingestJobId != null) {
            logger.trace("Create ingest task for run:{} item {}", runContext, itemId);

            String params = jsonProps == null ? null : jsonProps.toJsonString();

            if (doc == null) { // mostly for update existing, document not available so we need to create
                FileToAdd fa = new FileToAdd(itemId, params, runContext);
                mapBatchResultsProcessor.collectCatFiles(MapServiceUtils.createBatchTaskId(batchIdentifierNumber, taskId), fa);
                logger.trace("add file {} to be set to ingest", itemId);
            } else { // add new file - we can change the existing document
                SolrTaskService.setNewTask(doc, runContext, params);
                logger.trace("add ingest task params to file {}", itemId);
            }

        } else if (runService.getCrawlRunFromCache(runContext).isIngestFiles()) {
            logger.warn("cant find ingest job for run {}, so cannot create ingest task for item {} ", runContext, itemId);
        }
    }

    @SuppressWarnings("SameParameterValue")
    public static Object getFieldValueFromSolrInputDoc(SolrInputDocument doc, CatFileFieldType field) {
        if (doc == null) return null;
        Object obj = doc.getFieldValue(field.getSolrName());
        if (obj == null) return null;
        HashMap map = (HashMap)obj;
        return map.get(SolrOperator.SET.getOpName());
    }
}
