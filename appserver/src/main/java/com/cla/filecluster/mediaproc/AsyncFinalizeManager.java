package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.crawler.scan.ScanFinalizeService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 10/8/2018
 */
@Service
public class AsyncFinalizeManager implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(AsyncFinalizeManager.class);

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private BlockingExecutor finalizeBlockingExecutor;

    @Autowired
    private IngestFinalizationService ingestFinalizationService;

    @Autowired
    private ScanFinalizeService scanFinalizeService;

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @AutoAuthenticate
    @Scheduled(fixedRateString = "${finalizer.tasks-polling-rate-millis:5000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void processNextScanFinTasksGroup() {

        if (!isSchedulingActive) {
            return;
        }

        List<SimpleJob> inProgressJobs = jobManager.getJobs(JobType.SCAN_FINALIZE, JobState.IN_PROGRESS);
		if (inProgressJobs.isEmpty()) {
			return;
		}
		List<SimpleTask> tasks = jobManager.getTasksByTypeAndState(TaskType.SCAN_FINALIZE, TaskState.NEW, JobState.IN_PROGRESS);
		tasks.forEach(task -> finalizeBlockingExecutor.execute(() -> {
			boolean scannedFromScratch = false;
			if (!Strings.isNullOrEmpty(task.getJsonData())) {
				Map<String, Object> params = ModelDtoConversionUtils.jsonToMap(task.getJsonData());
				scannedFromScratch = Boolean.parseBoolean((String) params.getOrDefault("scannedFromScratch", "false"));
			}
			jobManager.updateTaskStatus(task.getId(), TaskState.CONSUMING);
			logger.debug("run scan finalize job {} task {}", task.getJobId(), task.getId());
			scanFinalizeService.runScanFinalize(task.getRunContext(), task.getItemId(), task.getJobId(), scannedFromScratch, task.getId());
		}));
	}

    @AutoAuthenticate
    @Scheduled(fixedRateString = "${finalizer.tasks-polling-rate-millis:5000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void processNextOptimizeTasksGroup() {

        if (!isSchedulingActive) {
            return;
        }

        List<SimpleJob> inProgressJobs = jobManager.getJobs(JobType.OPTIMIZE_INDEXES, JobState.IN_PROGRESS);
        if (inProgressJobs != null && !inProgressJobs.isEmpty()) {
            List<SimpleTask> tasks = jobManager.getTasksByTypeAndState(TaskType.OPTIMIZE_INDEXES, TaskState.NEW, JobState.IN_PROGRESS);
            if (tasks != null && !tasks.isEmpty()) {
                tasks.forEach(t -> {
                    finalizeBlockingExecutor.execute(() -> {
                        jobManager.updateTaskStatus(t.getId(), TaskState.CONSUMING);
                        logger.debug("run optimize indexes job {} task {}", t.getJobId(), t.getId());
                        ingestFinalizationService.runOptimizeIndexes(t.getRunContext(), t.getJobId(), t.getId(), true);
                    });
                });
            }
        }
    }
}
