package com.cla.filecluster.mediaproc.map;

import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.filecluster.domain.dto.SolrFolderEntity;

import java.util.Objects;

/**
 * Created by: yael
 * Created on: 6/23/2019
 */
public class MailFolderNode {
    private final String mid; // mediaItemId
    private final String parentMid;
    private final String displayName;
    private String folderFullPath = null;
    private Exchange365ItemDto underlyingDto; // may be null if created from a SolrFolderEntity

    public MailFolderNode(Exchange365ItemDto changedFolderDto) {
        this.mid = changedFolderDto.getMediaItemId();
        this.parentMid = changedFolderDto.getParentFolderId();
        this.displayName = changedFolderDto.getFileName();
        this.underlyingDto = changedFolderDto;
    }

    MailFolderNode(SolrFolderEntity parentFolderFromSolr) {
        this.parentMid = null;   // This ctor is not supposed to be called for a parent of a parent folder from solr
        this.mid = null;         // The mid         should not be queried in the case of parent folder from solr. Just its folderFullPath
        this.displayName = null; // The displayName should not be queried in the case of parent folder from solr. Just its folderFullPath
        this.folderFullPath = Objects.requireNonNull(parentFolderFromSolr.getRealPath());
    }

    public boolean isFullPathResolved() {
        return folderFullPath != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailFolderNode that = (MailFolderNode) o;
        return Objects.equals(mid, that.mid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mid);
    }

    public String getMid() {
        return mid;
    }

    public String getParentMid() {
        return parentMid;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getFolderFullPath() {
        return folderFullPath;
    }

    public Exchange365ItemDto getUnderlyingDto() {
        return underlyingDto;
    }

    public void setFolderFullPath(String folderFullPath) {
        this.folderFullPath = folderFullPath;
    }
}
