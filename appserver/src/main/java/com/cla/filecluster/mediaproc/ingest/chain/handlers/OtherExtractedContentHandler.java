package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Other files only handler.
 * TODO: Probably exists for backward compatibility and should be removed in the future
 */
@Component
public class OtherExtractedContentHandler implements ChainLink<IngestResultContext> {

    @Value("${tokenMatcherActive}")
    boolean isTokenMatcherActive;

    @Autowired
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return context.isOther();
    }

    @Override
    public boolean handle(IngestResultContext context) {
        OtherFile payload = context.getOtherResultMessage().getPayload();
        if (payload.getExtractedContent() != null) { // for backward compatibility, remove in the future
            payload.setTextLength(payload.getExtractedContent().length());
            if (isTokenMatcherActive) {
                payload.setHashedContent(nGramTokenizerHashed.getHash(payload.getExtractedContent(), null));
            }
        }
        return true;
    }
}
