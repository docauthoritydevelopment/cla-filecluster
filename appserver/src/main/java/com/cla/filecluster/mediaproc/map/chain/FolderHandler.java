package com.cla.filecluster.mediaproc.map.chain;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.file.ItemDeletionPayload;
import com.cla.common.domain.dto.messages.MessagePayloadType;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.common.scan.DeletedItemsProcessor;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.messages.PhaseMessagePayload;
import com.cla.connector.domain.dto.messages.ScanErrorDto;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.mediaproc.map.*;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.kvdb.domain.FileEntityUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.cla.common.domain.dto.messages.MessagePayloadType.*;

/**
 * Handle folder responses and handle folders for file responses
 * <p>
 * Created by: yael
 * Created on: 8/4/2019
 */
@Service
public class FolderHandler implements ChainLink<MapResultContext> {

    private static final Logger logger = LoggerFactory.getLogger(FolderHandler.class);

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private MapDeleteHandler mapDeleteHandler;

    @Autowired
    private MapServiceUtils mapServiceUtils;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MapRenameHandler mapRenameHandler;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private MapAddHandler mapAddHandler;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private EventBus eventBus;

    public boolean shouldHandle(MapResultContext mapContext) {
        return true;
    }

    public boolean handle(MapResultContext mapContext) {
        ScanResultMessage scanResultMessage = mapContext.getScanResultMessage();
        MessagePayloadType payloadType = scanResultMessage.getPayloadType();

        boolean shouldHandleMessageFurther;
        PhaseMessagePayload payload = scanResultMessage.getPayload();
        long rootFolderId = scanResultMessage.getRootFolderId();
        Long runContext = scanResultMessage.getRunContext();
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);

        try {
            rootFolderService.acquireReadLock(rootFolderId);
            switch (payloadType) {
                case SCAN_CHANGES:
                case SCAN_PST_ENTRY:
                case SCAN:
                case EXCHANGE365_ITEM_CHANGE:
                    shouldHandleMessageFurther = handleScanChange(mapContext, scanResultMessage,
                            payloadType, (ClaFilePropertiesDto) payload, runContext, rootFolder);
                    break;
                case SCAN_DELETION:
                    shouldHandleMessageFurther = handleDelete(mapContext, scanResultMessage,
                            (ItemDeletionPayload) payload, rootFolderId, runContext);
                    break;
                default:
                    logger.error("Invalid scan result message payload type: {}", payloadType);
                    shouldHandleMessageFurther = false;
            }
        } catch (Exception e) {
            logger.error("handle failed with exception for {}", scanResultMessage, e);
            shouldHandleMessageFurther = false;
            mapContext.setErrorOccurred(true);
        } catch (Throwable t) {
            logger.error("handle failed with throwable for {}", scanResultMessage, t);
            shouldHandleMessageFurther = false;
            mapContext.setErrorOccurred(true);
        } finally {
            rootFolderService.releaseReadLock(rootFolderId);
        }

        logger.trace("file {}, process continue {}", mapContext, shouldHandleMessageFurther);
        return shouldHandleMessageFurther;
    }

    private boolean handleDelete(MapResultContext mapContext, ScanResultMessage scanResultMessage,
                                 ItemDeletionPayload payload, long rootFolderId, Long runContext) {
        boolean shouldHandleMessageFurther = true;
        String fileNameDel = payload.getFilename();
        FileEntity fileEntity = mapServiceUtils.getFileEntity(rootFolderId, fileNameDel);
        if (fileEntity == null) {
            logger.warn("file: {} is associated to root folder {}, discarding this message, verify you don't have" +
                            "overlapping root folder configurations (can be in the case of one drive media)",
                    fileNameDel, rootFolderId);
            shouldHandleMessageFurther = false;
        } else if (fileEntity.isFolder()) {
            mapDeleteHandler.handleDeletedFiles(runContext, scanResultMessage.getTaskId(), rootFolderId,
                    mapContext.getBatchIdentifierNumber(), payload.getFilename());
            shouldHandleMessageFurther = false;
        } else {
            mapContext.setFileId(fileEntity.getFileId());
        }
        return shouldHandleMessageFurther;
    }

    private boolean handleScanChange(MapResultContext mapContext, ScanResultMessage scanResultMessage,
                                     MessagePayloadType payloadType, ClaFilePropertiesDto prop,
                                     Long runContext, RootFolder rootFolder) {
        boolean shouldHandleMessageFurther;
        eventBus.broadcast(Topics.MAP, Event.builder()
                .withEventType(EventType.OPERATION_MESSAGE)
                .withEntry(EventKeys.JOB_ID, scanResultMessage.getJobId())
                .withEntry(EventKeys.OPERATION_MESSAGE, "Scanning: " + prop.getFileName()).build());
        if (scanResultMessage.isFirstScan() || payloadType.equals(EXCHANGE365_ITEM_CHANGE)) {
            shouldHandleMessageFurther = callFirstScanHandleResult(scanResultMessage, prop,
                    mapContext.getBatchIdentifierNumber(), rootFolder, runContext, mapContext);
        } else {
            shouldHandleMessageFurther = handleByDiffType(scanResultMessage, prop,
                    mapContext.getBatchIdentifierNumber(), rootFolder, runContext, mapContext);
        }
        return shouldHandleMessageFurther;
    }

    private boolean handleByDiffType(ScanResultMessage scanResultMessage, ClaFilePropertiesDto prop, Long batchIdentifierNumber,
                                     RootFolder rootFolder, Long runContext, MapResultContext mapContext) {
        boolean shouldHandleMessageFurther = true;
        FileEntity oldFileEntity = mapServiceUtils.getFileEntity(rootFolder.getId(), mapContext.getKvdbKey());
        boolean updateOldFileEntity = true;
        boolean isFirst = false;
        boolean callFirstScan = false;

        switch (mapContext.getDiffType()) {
            case NEW:
            case CREATED_UPDATED:
                isFirst = true;
                callFirstScan = true;
                break;

            case ACL_UPDATED:
                shouldHandleMessageFurther = handleAclUpdate(scanResultMessage, prop,
                        mapContext, oldFileEntity);
                break;

            case RENAMED:
                if (mapContext.getNewFile()) {
                    if (mapContext.getBelongToRF()) {
                        callFirstScan = true;
                    }
                } else {
                    if (oldFileEntity.isFolder() && !mapContext.getBelongToRF()) {
                        mapRenameHandler.handleFileOrDeletedFolderRename(batchIdentifierNumber, rootFolder, runContext,
                                scanResultMessage.getTaskId(), prop, oldFileEntity, mapContext.getBelongToRF());
                        shouldHandleMessageFurther = false;
                    } else if (oldFileEntity.isFile()) {
                        fileCatService.handleFolderForFileRename(rootFolder, prop, oldFileEntity.getFileId());
                    }
                }
                break;

            case UNDELETED:
                if (oldFileEntity == null) {// Deleted before RF was registered - treat as new file
                    callFirstScan = true;
                } else if (DeletedItemsProcessor.isValidFileEntity(oldFileEntity)) {
                    if (oldFileEntity.isFolder()) {
                        docFolderService.updateFolderAcls(oldFileEntity.getFolderId(), prop, false, runContext);
                        docFolderService.markDocFolderAsNotDeleted(oldFileEntity.getFolderId());
                        shouldHandleMessageFurther = false;
                    }
                } else {
                    logger.warn("deleting kvdb entity without id {}", oldFileEntity);
                    mapServiceUtils.deleteFromKvdb(rootFolder.getId(), runContext, mapContext.getKvdbKey());
                    callFirstScan = true;
                }
                break;

            case CONTENT_UPDATED:
                shouldHandleMessageFurther = handleContentUpdate(prop, mapContext, oldFileEntity);
                break;

            default:
                // do nothing
        }

        if (callFirstScan) {
            shouldHandleMessageFurther = callFirstScanHandleResult(scanResultMessage, prop,
                    batchIdentifierNumber, rootFolder, runContext, mapContext);
        }

        // if was not a new file - update last scanned time in KVDB
        if (prop.isFolder() && !isFirst && oldFileEntity != null && updateOldFileEntity) {
            logger.trace("File {} is not new updating last scanned time to {}", prop.getFileName(), mapContext.getTime());
            oldFileEntity.setLastScanned(mapContext.getTime());
            oldFileEntity.setDeleted(false);
            mapBatchResultsProcessor.collectCatFiles(MapServiceUtils.createBatchTaskId(batchIdentifierNumber, scanResultMessage.getTaskId()),
                    new FileToAdd(null, oldFileEntity, mapContext.getKvdbKey(), rootFolder.getId(), runContext));
        }

        if (prop.isFile() && oldFileEntity != null) {
            mapContext.setFileId(oldFileEntity.getFileId());
        }

        return shouldHandleMessageFurther;
    }

    private boolean handleContentUpdate(ClaFilePropertiesDto prop, MapResultContext mapContext, FileEntity oldFileEntity) {
        boolean shouldHandleMessageFurther = true;
        if (oldFileEntity != null) {
            // update kvdb
            oldFileEntity.setSize(prop.getFileSize());
            oldFileEntity.setLastModified(prop.getModTimeMilli());
            oldFileEntity.setAclSignature(prop.getAclSignature());
        } else if (mapContext.getAlreadyHandled()) {
            logger.debug("file {} already processed and waiting for batch handling - ignore {} action", prop.getFileName(), mapContext.getDiffType());
            shouldHandleMessageFurther = false;
        } else {
            logger.error("problem updating content for key {} not in kvdb payload {}", mapContext.getKvdbKey(), prop);
            shouldHandleMessageFurther = false;
        }
        return shouldHandleMessageFurther;
    }

    private boolean handleAclUpdate(ScanResultMessage scanResultMessage, ClaFilePropertiesDto prop,
                                    MapResultContext mapContext, FileEntity oldFileEntity) {
        boolean shouldHandleMessageFurther = true;
        if (oldFileEntity != null) { // if ACLs were found to be updated, oldFileEntity can't be null
            if (oldFileEntity.isFolder()) {
                oldFileEntity.setInaccessible(prop.isInaccessible());
                // update folder ACLs from the properties received during scan
                if (oldFileEntity.getFolderId() == null) {
                    logger.warn("folder without folder id, cannot update acls {} {}", oldFileEntity, scanResultMessage.getPayload());
                } else {
                    docFolderService.updateFolderAcls(oldFileEntity.getFolderId(), prop, false, scanResultMessage.getRunContext());
                }
                shouldHandleMessageFurther = false;
            }
        } else if (mapContext.getAlreadyHandled()) {
            logger.debug("file {} already processed and waiting for batch handling - ignore {} action", prop.getFileName(), mapContext.getDiffType());
            shouldHandleMessageFurther = false;
        } else {
            logger.error("problem updating acl for key {} not in kvdb payload {}", mapContext.getKvdbKey(), prop);
            shouldHandleMessageFurther = false;
        }
        return shouldHandleMessageFurther;
    }

    private boolean callFirstScanHandleResult(ScanResultMessage scanResultMessage, ClaFilePropertiesDto prop,
                                              Long batchIdentifierNumber, RootFolder rootFolder, Long runContext, MapResultContext mapContext) {
        Pair<Boolean, FileEntity> result = processFirstScan(prop, rootFolder, runContext, mapContext);
        boolean shouldHandleMessageFurther = result.getKey();
        if (result.getValue() != null) {
            String kvdbKey = Optional.ofNullable(prop.getMediaItemId()).orElse(prop.getFileName());
            mapBatchResultsProcessor.collectCatFiles(MapServiceUtils.createBatchTaskId(batchIdentifierNumber, scanResultMessage.getTaskId()),
                    new FileToAdd(null, result.getValue(), kvdbKey, rootFolder.getId(), runContext));
        }
        return shouldHandleMessageFurther;
    }

    private Pair<Boolean, FileEntity> processFirstScan(ClaFilePropertiesDto prop, RootFolder rootFolder, Long runId, MapResultContext mapContext) {
        if (CollectionUtils.isNotEmpty(prop.getErrors())) {
            logger.info("Scan errors found at {}. Not ingesting.", prop);
            for (ScanErrorDto scanErrorDto : prop.getErrors()) {
                scanErrorsService.addScanError(scanErrorDto.getText(), scanErrorDto.getExceptionText(),
                        null, scanErrorDto.getPath(), runId, rootFolder.getId(), rootFolder.getMediaType());
                eventBus.broadcast(Topics.MAP, Event.builder()
                        .withEventType(EventType.JOB_PROGRESS)
                        .withEntry(EventKeys.JOB_ID, mapContext.getScanResultMessage().getJobId())
                        .withEntry(EventKeys.FILE_MAP_FAIL, 1).build());
            }
            return Pair.of(false, null);
        }

        if (prop.isFolder()) {
            logger.info("handling add folder {}", prop.getFileName());
            Pair<Long, SolrInputDocument> newFolderData = mapAddHandler.handleFolderAddItem(prop.getFileName(), rootFolder.getId(), runId, prop);
            Long folderId = newFolderData.getKey();
            FileEntity fileEntity = null;
            if (folderId != null) {
                mapContext.setFolderId(folderId);
                String pathForKvdb = null;
                if (!rootFolder.getMediaType().isPathAsMediaEntity()) {
                    pathForKvdb = prop.getFileName();
                }
                fileEntity = FileEntityUtils.createFileEntity(null, folderId, mapContext.getTime(), prop, pathForKvdb);
            }
            return Pair.of(false, fileEntity);
        } else {
            String folderUrl = DocFolderUtils.extractFolderFromPath(prop.getFileName());
            DocFolderService.DocFolderCacheValue docVal = docFolderService.createDocFolderIfNeededWithCache(rootFolder.getId(), folderUrl);
            mapContext.setFolderId(docVal.getDocFolderId());
            return Pair.of(true, null);
        }
    }
}
