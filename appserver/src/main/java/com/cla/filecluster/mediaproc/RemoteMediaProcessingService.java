package com.cla.filecluster.mediaproc;

import com.cla.common.domain.dto.components.ClaComponentDto;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.crawler.JobStateByDate;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.common.domain.dto.filetree.EntityCredentialsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobStateUpdateInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.messages.*;
import com.cla.common.domain.dto.messages.control.*;
import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.common.domain.pst.PstPath;
import com.cla.common.jobs.ErrorMonitor;
import com.cla.common.utils.CollectionsUtils;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.mediaconnector.labeling.LabelResult;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.jobmanager.domain.entity.*;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobConsistencyManager;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.jobmanager.service.TaskManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.mediaproc.ingest.chain.IngestItemConsumerChain;
import com.cla.filecluster.mediaproc.map.chain.ScanItemConsumerChain;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.filetree.*;
import com.cla.filecluster.service.files.managers.FileContentService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.media.MediaSpecificAppService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.service.syscomponent.AppServerStateService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.filecluster.util.LocalFileUtils;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.*;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.common.domain.dto.jobmanager.JobState.IN_PROGRESS;
import static com.cla.common.domain.dto.jobmanager.JobState.PENDING;
import static com.cla.filecluster.jobmanager.domain.entity.TaskState.*;
import static com.cla.filecluster.jobmanager.domain.entity.TaskType.SCAN_ROOT_FOLDER;
import static com.cla.filecluster.kpi.PerformanceMessages.PERFORMANCE_MEASURE_PATTERN;
import static java.util.stream.Collectors.toList;

/**
 * Sends requests to the media processor to perform tasks
 * Created by uri on 14-Mar-17.
 */
@Service
public class RemoteMediaProcessingService implements ControlledScheduling {

    private static Logger logger = LoggerFactory.getLogger(RemoteMediaProcessingService.class);

    private static Logger kpiLogger = LoggerFactory.getLogger(PerformanceKpiRecorder.class);

    @Value("${component.media-processor.max-allowed-time-shift-min:8}")
    private int maxAllowedMpTimeShift;

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Autowired
    private TaskManagerService taskManagerService;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private IngestItemConsumerChain ingestItemConsumerChain;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private ScanItemConsumerChain scanItemConsumer;

    @Autowired
    private ScanDoneConsumer scanDoneConsumer;

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private JobConsistencyManager jobConsistencyManager;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private EntityCredentialsService entityCredentialsService;

    @Autowired
    private FileContentService fileContentService;

    @Autowired
    private IngestFinalizationService ingestFinalizationService;

    @Autowired
    private AppServerStateService appServerStateService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private AsyncLabelingConsumerService asyncLabelingConsumerService;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Value("${component.unknown.media-processor.send-state-change:true}")
    private boolean sendMediaProcessorStateUpdateIfUnknown;

    @Value("${scanner.global.folder.skip.basenames:}")
    private String[] globalDirnames2SkipStr;

    @Value("${enqueued.scan-tasks.per-media-processor:2}")
    private int enqueuedScanTasksPerMediaProcessor;

    @Value("${scanner.scanTaskDepth:0}")
    private int scanTaskDepth;

    @Value("${scanner.ignoreAccessErrors:false}")
    private boolean ignoreAccessErrors;

    @Value("${ingester.send-requests-in-batches:false}")
    private boolean sendJmsIngestRequestsInBatch;

    @Value("${job-manager.tasks.enqueued-timeout-sec:600}")
    private int enqueuedTaskTimeoutSec;

    @Value("${media-processor.error-monitor.trigger-interval:1}")
    private int errorMonitorTriggerInterval;

    @Value("${media-processor.error-monitor.trigger-interval-unit:MINUTES}")
    private TimeUnit errorMonitorTriggerIntervalUnit;

    @Value("${media-processor.error-monitor.trigger-threshold:5}")
    private int errorMonitorTriggerThreshold;

    @Autowired
    private SystemSettingsService systemSettingsService;

    private LoadingCache<Long, Set<String>> rootFolderFileTypes;

    private LoadingCache<Long, ErrorMonitor> mediaConnectorErrorMonitors;

    private TimeSource timeSource = new TimeSource();

    private List<String> globalDirnames2Skip = null;

    private AtomicLong batchIdentifier = new AtomicLong(0L);

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @SuppressWarnings("unused")
    @PostConstruct
    private void init() {
        rootFolderFileTypes = CacheBuilder.newBuilder()
                .maximumSize(50)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, Set<String>>() {
                    public Set<String> load(@NotNull Long rootFolderId) {
                        return getFileTypesFromDB(rootFolderId);
                    }
                });

        //noinspection NullableProblems
        mediaConnectorErrorMonitors = CacheBuilder.newBuilder()
                .expireAfterAccess(5, TimeUnit.MINUTES)
                .maximumSize(50)
                .build(new CacheLoader<Long, ErrorMonitor>() {
                    @Override
                    public ErrorMonitor load(Long runId) {
                        return new ErrorMonitor(errorMonitorTriggerInterval, errorMonitorTriggerIntervalUnit, errorMonitorTriggerThreshold);
                    }
                });

        if (globalDirnames2SkipStr != null && globalDirnames2SkipStr.length > 0) {
            globalDirnames2Skip = Stream.of(globalDirnames2SkipStr).collect(toList());
            logger.info("Folders basenames to skip set to: {}", String.join(",", globalDirnames2Skip));
        }
    }

    @Scheduled(initialDelayString = "${distribute.start-scan-requests.initial-delay.millis:120000}",
            fixedRateString = "${distribute.start-scan-requests.rate.millis:30000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void distributePendingStartScanRequests() {

        if (!isSchedulingActive) {
            return;
        }

        List<SimpleTask> newScanTasks =
                jobManager.getTasksByTypeAndState(SCAN_ROOT_FOLDER, TaskState.NEW, PENDING, IN_PROGRESS);
        ListMultimap<Long, SimpleTask> newScanTasksPerDataCenterId =
                MultimapBuilder.treeKeys().arrayListValues().build();

        for (SimpleTask task : newScanTasks) {
            try {
                Long dataCenterId = getCustomerDataCenterId(task.getItemId());
                newScanTasksPerDataCenterId.put(dataCenterId, task);
            } catch (NoMediaProcInDataCenterException e) {
                logger.warn("Problem finding data center for task {} - pause run", task, e);
                jobConsistencyManager.pauseRun(task.getRunContext(), PauseReason.NO_AVAILABLE_MEDIA_PROCESSOR);
            } catch (Exception e) {
                logger.warn("Problem getting data center for task {}", task, e);
            }
        }

        Set<Long> dataCenterIds = newScanTasksPerDataCenterId.keySet();
        if (dataCenterIds.size() == 0) {
            return;
        }
        Map<Long, Integer> sentTaskCountPerDataCenterId =
                jobManager.getTaskCountPerDataCenterId(dataCenterIds, SCAN_ROOT_FOLDER, ENQUEUED, CONSUMING);

        List<Long> tasksToEnqueue = new ArrayList<>();
        for (Long dataCenterId : dataCenterIds) {
            List<SimpleTask> dataCenterNewScanTasks = newScanTasksPerDataCenterId.get(dataCenterId);
            if (dataCenterNewScanTasks == null || dataCenterNewScanTasks.size() == 0) continue;
            Integer sentToDataCenterTaskCount = sentTaskCountPerDataCenterId.getOrDefault(dataCenterId, 0);
            int mediaProcessorsCount = sysComponentService.getActiveMediaProcessors(dataCenterId, true).size(); // TODO Itai: make sure that this is up-to-date (e.g. if a media processor had crashed)
            int numOfTasksToEnqueue =
                    (mediaProcessorsCount * enqueuedScanTasksPerMediaProcessor) - sentToDataCenterTaskCount;
            if (dataCenterNewScanTasks.size() < numOfTasksToEnqueue) {
                numOfTasksToEnqueue = dataCenterNewScanTasks.size();
            }
            for (int i = 0; i < numOfTasksToEnqueue; i++) {
                SimpleTask newScanTask = dataCenterNewScanTasks.get(i);
                boolean jobActive = jobManager.isJobActive(newScanTask.getJobId());
                if (jobActive) {
                    long rootFolderId = newScanTask.getItemId();
                    RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
                    boolean isFirstScan = resolveFirstScan(rootFolder, newScanTask);
                    ScanRequestMessage scanRequestMessage =
                            createScanStartRequestMessage(newScanTask.getRunContext(), rootFolder, newScanTask, isFirstScan);
                    if (!rootFolder.getMediaType().hasPath()) {
                        Map<String, Object> properties = ModelDtoConversionUtils.jsonToMap(newScanTask.getJsonData());
                        // Case scan is of mailboxes then populate sync state and update firstScan indicator per mailbox
                        if (properties.get(ScanTaskParameters.MAILBOX_UPN) != null) {
                            scanRequestMessage.getPayload().setMailboxUPN((String) properties.get(ScanTaskParameters.MAILBOX_UPN));
                            Object syncState = properties.get(ScanTaskParameters.SYNC_STATE);
                                if (syncState != null && !scanRequestMessage.isForceScanFromScratch()) {
                                scanRequestMessage.getPayload().setSyncState((String) syncState);
                            }
                        }
                    }

                    logger.debug("Sending Start scan request. startScanTask: {}", newScanTask);
                    mediaProcessorCommService.sendStartScanRequest(dataCenterId, scanRequestMessage);
                    tasksToEnqueue.add(newScanTask.getId());
                }
            }
        }
        if (!tasksToEnqueue.isEmpty()) {
            taskManagerService.updateTaskState(tasksToEnqueue, ENQUEUED);
        }
    }

    private boolean resolveFirstScan(RootFolder rootFolder, SimpleTask newScanTask) {
        if (rootFolder.getMediaType().hasPath()) {
            return rootFolder.getFoldersCount() == null || rootFolder.getFoldersCount() == 0;
        } else {
            Map<String, Object> properties = ModelDtoConversionUtils.jsonToMap(newScanTask.getJsonData());
            // Case scan is of mailboxes then populate sync state and update firstScan indicator per mailbox
            return properties.get(ScanTaskParameters.MAILBOX_UPN) != null &&
                    properties.get(ScanTaskParameters.SYNC_STATE) == null;
        }
    }

    private boolean skipFileIngestion(SimpleTask task, ClaFile file, Map<Long, RootFolder> rootFolderMap) {
        Long fileId = task.getItemId();
        if (file == null) {
            logger.error("no file found for itemId: {}", fileId);
            return true;
        }
        RootFolder rootFolder = rootFolderMap.get(file.getRootFolderId());
        CrawlRun run = fileCrawlerExecutionDetailsService.getCrawlRunFromCache(task.getRunContext());
        if (file.getState().alreadyIngested() && rootFolder.getReingestTimeStampMs() <= 0 &&
                file.getIngestDate() != null && file.getIngestDate() > run.getStartTime().getTime()) {
            logger.info("file {} set to be ingested twice, skip", fileId);
            return true;
        }
        return false;
    }

    public void issueLabelingRequest(long customerDataCenterId, LabelRequestMessage labelRequestMessage) {
        mediaProcessorCommService.sendLabelingRequest(customerDataCenterId, labelRequestMessage);
    }

    /**
     * send ingest requests to the queue, return a list of taskIds which failed to be enqueued
     *
     * @param ingestTasks tasks to send
     * @return list of taskIds which failed to be enqueued
     */
    @AutoAuthenticate
    IngestStartTaskHandling issueStartIngestRequest(Collection<SimpleTask> ingestTasks, Integer timeout) {
        long start = System.currentTimeMillis();
        List<Long> fileIdsToIngest = ingestTasks
                .stream()
                .flatMap(t -> {
                    Long containerFileId = t.getItemId();
                    IngestTaskJsonProperties jsonProps = IngestTaskJsonProperties.fromJsonString(t.getJsonData());
                    List<Long> containedFileIDs = jsonProps.getContainedFileIDs();
                    return Stream.concat(Stream.of(containerFileId), containedFileIDs.stream());
                })
                .collect(Collectors.toList());
        List<ClaFile> claFilesToIngest = Lists.partition(fileIdsToIngest, 5000).stream()
                .flatMap(ls -> fileCatService.findByFilesIds(ls).stream())
                .collect(Collectors.toList());
        long end = System.currentTimeMillis();
        kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "issueStartIngestRequest",
                "fileCatService.findByFilesIds(fileIdsToIngest) for " + fileIdsToIngest.size() + " files", 317, end - start);

        Map<Long, ClaFile> claFilesById = claFilesToIngest.stream()
                .collect(Collectors.toMap(ClaFile::getId, f -> f, this::claFileKeyCollisionResolver));
        Set<Long> failedFileTaskIds = Sets.newHashSet();
        Set<Long> skippedFileTaskIds = Sets.newHashSet();

        start = System.currentTimeMillis();
        List<Long> rootFolders = claFilesToIngest.stream()
                .map(ClaFile::getRootFolderId)
                .filter(Objects::nonNull)
                .distinct().collect(toList());
        List<RootFolder> rfs = docStoreService.getRootFoldersCached(rootFolders);
        end = System.currentTimeMillis();
        kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "issueStartIngestRequest", "docStoreService.getRootFolders(rootFolders) for " + rfs.size() + " root folders", 330, end - start);

        start = System.currentTimeMillis();
        Map<Long, RootFolder> rootFolderMap = rfs.stream()
                .collect(Collectors.toMap(RootFolder::getId, r -> r, CollectionsUtils::keyCollisionResolver));

        Map<Long, Long> rootFoldersDataCenter = rootFolders.stream()
                .collect(Collectors.toMap(r -> r, this::getCustomerDataCenterId, CollectionsUtils::keyCollisionResolver));

        Map<Long, MediaConnectionDetailsDto> connectionDetails = Maps.newHashMap();
        rootFolders.forEach(rootFolder -> {
            Optional<MediaConnectionDetailsDto> connection = mediaConnectionDetailsService
                    .getMediaConnectionDetailsByRootFolderId(rootFolder, true);
            connection.ifPresent(detailsDto -> connectionDetails.put(rootFolder, detailsDto));
        });

        Map<Long, List<EntityCredentialsDto>> credentials = rootFolders.stream()
                .collect(Collectors.toMap(r -> r, r -> {
                    List<EntityCredentialsDto> cs = entityCredentialsService.getEntityCredentialsForRootFolderCached(r);
                    return (cs == null ? Lists.newArrayList() : cs);
                }, CollectionsUtils::keyCollisionResolver));

        kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "issueStartIngestRequest", "get media connection details for " + rfs.size() + " root folders", 335, end - start);

        if (sendJmsIngestRequestsInBatch) {
            // Convert tasks to ingest request messages and group them by destination id
            start = System.currentTimeMillis();
            Map<Long, List<IngestRequestMessage>> ingestMessagesMap =
                    ingestTasks.stream().map(t -> {
                        ClaFile file = claFilesById.get(t.getItemId());
                        if (skipFileIngestion(t, file, rootFolderMap)) {
                            skippedFileTaskIds.add(t.getItemId());
                            return null;
                        }
                        Long rfId = file.getRootFolderId();
                        MediaConnectionDetailsDto conn = rfId == null ? null : connectionDetails.get(rfId);
                        List<EntityCredentialsDto> creds = rfId == null ? null : credentials.get(rfId);
                        return createIngestRequestMessage(t, rootFolderMap.get(rfId), file, claFilesById, conn, creds, timeout);
                    }).filter(Objects::nonNull)
                            .collect(
                                    Collectors.groupingBy(
                                            m -> (rootFoldersDataCenter.get(m.getPayload().getRootFolderId())),
                                            Collectors.toList()));
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "issueStartIngestRequest", "ingestMessagesMap(per data center), size: " + ingestMessagesMap.size(), 345, end - start);

            start = System.currentTimeMillis();
            ingestMessagesMap.forEach((customerDataCenterId, ingestRequestMessages) -> {
                long innerStart = System.currentTimeMillis();
                boolean success = mediaProcessorCommService.sendIngestRequest(customerDataCenterId,
                        ingestRequestMessages, null);
                long innerEnd = System.currentTimeMillis();
                kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "issueStartIngestRequest", "mediaProcessorCommService.sendIngestRequest(centerId: " + customerDataCenterId + ", size: " + ingestRequestMessages.size(), 363, innerEnd - innerStart);
                if (!success) {
                    ingestRequestMessages.forEach(t -> failedFileTaskIds.add(t.getPayload().getFileId()));
                }
            });
            end = System.currentTimeMillis();
            kpiLogger.info(PERFORMANCE_MEASURE_PATTERN, "issueStartIngestRequest", "mediaProcessorCommService.sendIngestRequest(in batch), total: " + ingestMessagesMap.size(), 361, end - start);
        } else {
            for (SimpleTask ingestTask : ingestTasks) {
                ClaFile claFile = claFilesById.get(ingestTask.getItemId());
                if (skipFileIngestion(ingestTask, claFile, rootFolderMap)) {
                    skippedFileTaskIds.add(ingestTask.getItemId());
                    continue;
                }
                Long rfId = claFile.getRootFolderId();
                MediaConnectionDetailsDto conn = rfId == null ? null : connectionDetails.get(rfId);
                List<EntityCredentialsDto> creds = rfId == null ? null : credentials.get(rfId);
                IngestRequestMessage ingestRequestMessage =
                        createIngestRequestMessage(
                                ingestTask, rootFolderMap.get(rfId), claFile, claFilesById, conn, creds, timeout);
                long customerDataCenterId = rootFoldersDataCenter.get(claFile.getRootFolderId());
                if (ingestRequestMessage != null) {
                    boolean success = mediaProcessorCommService.sendIngestRequest(customerDataCenterId,
                            ingestRequestMessage, null);
                    if (!success) {
                        failedFileTaskIds.add(ingestTask.getItemId());
                    }
                }
            }
        }
        return new IngestStartTaskHandling(failedFileTaskIds, skippedFileTaskIds);
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean issueFileIngestRequest(String requestId, RootFolder rf, ClaFile claFile, boolean getReverseDictionary) {
        Optional<MediaConnectionDetailsDto> connectionDetails = //TODO Vladi: should it be taken from cache????
                mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(claFile.getRootFolderId(), true);

        IngestTaskParameters parameters = IngestTaskParameters.create()
                .setIngestContents(true)
                .setGetReverseDictionary(getReverseDictionary)
                .setMediaConnectionDetailsDto(connectionDetails.orElse(null))
                .setMediaEntityId(LocalFileUtils.createFullPath(rf.getMediaType(), rf.getRealPath(), claFile.getMediaEntityId()));

        parameters.setRootFolderId(claFile.getRootFolderId())
                .setFilePath(LocalFileUtils.createFullPath(rf.getMediaType(), rf.getRealPath(), claFile.getFileName()))
                .setFileType(claFile.getType())
                .setFileId(claFile.getId());

        IngestRequestMessage message = new IngestRequestMessage(parameters);
        message.setRequestId(requestId);
        message.setExpirationTime(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(enqueuedTaskTimeoutSec));
        long customerDataCenterId = getCustomerDataCenterId(claFile.getRootFolderId());
        return mediaProcessorCommService.sendIngestRequest(customerDataCenterId, message, null);
    }

    public void issueFileContentRequest(String requestId, FileContentType taskType, RootFolder rf,
                                        ClaFile claFile, MediaConnectionDetailsDto mediaConnectionDetailsDto,
                                        Set<String> textTokens) {
        FileContentTaskParameters fileContentTaskParameters = FileContentTaskParameters.create();
        fileContentTaskParameters.setRequestId(requestId);
        fileContentTaskParameters.setFileContentType(taskType);
        fileContentTaskParameters.setFileId(claFile.getId());
        fileContentTaskParameters.setFilePath(LocalFileUtils.createFullPath(rf.getMediaType(), rf.getRealPath(), claFile.getFullPath()));
        fileContentTaskParameters.setFileType(claFile.getType());
        fileContentTaskParameters.setRootFolderId(claFile.getRootFolderId());
        fileContentTaskParameters.setMediaConnectionDetailsDto(mediaConnectionDetailsDto);
        fileContentTaskParameters.setFileName(LocalFileUtils.createFullPath(rf.getMediaType(), rf.getRealPath(), claFile.getFileName()));
        fileContentTaskParameters.setTextTokens(textTokens);
        fileContentTaskParameters.setBaseName(claFile.getBaseName());
        fileContentTaskParameters.setMediaEntityId(LocalFileUtils.createFullPath(rf.getMediaType(), rf.getRealPath(), claFile.getMediaEntityId()));
        FileContentRequestMessage fileContentRequestMessage = new FileContentRequestMessage(fileContentTaskParameters);

        long customerDataCenterId = getCustomerDataCenterId(claFile.getRootFolderId());

        mediaProcessorCommService.sendFileContentRequest(customerDataCenterId, fileContentRequestMessage, null);
    }

    public void issueListFoldersRequest(String requestId, String mediaEntityId, long customerDataCenterId,
                                        MediaConnectionDetailsDto mediaConnectionDetailsDto, boolean isTest) {
        FileContentTaskParameters fileContentTaskParameters = FileContentTaskParameters.create();
        fileContentTaskParameters.setRequestId(requestId);
        fileContentTaskParameters.setMediaConnectionDetailsDto(mediaConnectionDetailsDto);
        fileContentTaskParameters.setMediaEntityId(mediaEntityId);
        fileContentTaskParameters.setTest(isTest);
        FileContentRequestMessage fileContentRequestMessage = new FileContentRequestMessage(fileContentTaskParameters);
        fileContentRequestMessage.setPayloadType(MessagePayloadType.LIST_FOLDERS_REQUEST);
        mediaProcessorCommService.sendFileContentRequest(customerDataCenterId, fileContentRequestMessage, null);
    }

    public void issueFileContentRequest(String requestId, FileContentType taskType,
                                        RootFolderDto rootFolderDto, MediaConnectionDetailsDto mediaConnectionDetailsDto,
                                        Set<String> textTokens, boolean forceAccessibilityCheck) {
        FileContentTaskParameters fileContentTaskParameters = FileContentTaskParameters.create();
        fileContentTaskParameters.setRequestId(requestId);
        fileContentTaskParameters.setFileContentType(taskType);
        fileContentTaskParameters.setMediaConnectionDetailsDto(mediaConnectionDetailsDto);
        fileContentTaskParameters.setTextTokens(textTokens);
        fileContentTaskParameters.setRootFolderDto(rootFolderDto);
        fileContentTaskParameters.setForceAccessibilityCheck(forceAccessibilityCheck);
        FileContentRequestMessage fileContentRequestMessage = new FileContentRequestMessage(fileContentTaskParameters);

        long customerDataCenterId = getCustomerDataCenterId(rootFolderDto.getId());

        mediaProcessorCommService.sendFileContentRequest(customerDataCenterId, fileContentRequestMessage, null);
    }

    private long getCustomerDataCenterId(Long rootFolderId) {
        long customerDataCenterId = docStoreService.getRootFolderCustomerDataCenter(rootFolderId, true);
        List<Long> activeMediaProcessors = sysComponentService.getActiveMediaProcessors(customerDataCenterId, true);
        if (activeMediaProcessors.isEmpty()) {
            throw new NoMediaProcInDataCenterException("No Media Processor is configured to receive requests for rootFolderID: " + rootFolderId + " at data center: " + customerDataCenterId);
        }
        return customerDataCenterId;
    }

    public void issueMediaProcessorLogsRequest(String requestId, Long instanceId) {
        FileContentTaskParameters fileContentTaskParameters = FileContentTaskParameters.create();
        fileContentTaskParameters.setFileContentType(FileContentType.GET_LOGS);
        fileContentTaskParameters.setRequestId(requestId);
        fileContentTaskParameters.setInstanceId(instanceId);
        FileContentRequestMessage fileContentRequestMessage = new FileContentRequestMessage(fileContentTaskParameters);
        mediaProcessorCommService.sendMPLogRequest(fileContentRequestMessage, null);
    }

    public void consumeIngestResult(IngestResultMessage ingestResultMessage) {
        if (ingestResultMessage.getRequestId() != null) {
            CompletableFuture<IngestResultMessage> completableFuture = fileContentService
                    .getCompletableFutureLockByIngestRequestId(ingestResultMessage.getRequestId());
            fileContentService.removeRequestFromLockMap(ingestResultMessage.getRequestId());
            completableFuture.complete(ingestResultMessage);
        }
    }

    private Long getNextMapBatchId() {
        return batchIdentifier.incrementAndGet();
    }

    private void handleIngestResult(IngestResultMessage ingestResultMessage, IngestBatchStore ingestBatchStore) {
        try {
            appServerStateService.incIngestTaskRequested();
            //noinspection unchecked
            ingestItemConsumerChain.processIngestionResult(ingestResultMessage, ingestBatchStore);
            logger.trace("Done consume ingest result {} on task {}", ingestResultMessage, ingestResultMessage.getTaskId());
            ingestFinalizationService.incrementIngestedNonFinalizedCount();
            appServerStateService.incIngestTaskFinished();
        } catch (Exception e) {
            appServerStateService.incIngestTaskRequestFailure();
            logger.error("Failed to consume ingest result {} on task {}", ingestResultMessage, ingestResultMessage.getTaskId(), e);
            solrTaskService.endTaskForContents(Lists.newArrayList(ingestResultMessage.getTaskId()));
        }
    }

    private boolean verifyResultValid(IngestResultMessage result, IngestBatchStore ingestBatchStore) {
        Long taskId = result.getTaskId();
        if (result.getRunContext() == null) {
            throw new RuntimeException("IngestionResult is missing runContext (" + result + ") on task " + taskId);
        }

        Long jobId = jobManager.getJobIdCached(result.getRunContext(), JobType.INGEST);
        if (jobId == null) {
            logger.warn("Failed to consume ingest result [{}] task [{}], the Job does not exist.", result, taskId);
            solrTaskService.endTaskForContents(Lists.newArrayList(taskId));
            return false;
        } else {
            if (JobState.PENDING.equals(jobManager.getCachedJobState(jobId))) {
                logger.debug("consumed ingest task for pending job, changed job state to in-progress {}", jobId);
                jobManager.updateJobState(jobId, JobState.IN_PROGRESS, null, "Continue ingesting", false);
            }
        }
        try {
            boolean valid = true;
            if (result.getErrors() != null) {
                //noinspection unchecked
                valid = result.getErrors().stream()
                        .filter(err -> ProcessingErrorType.ERR_TOO_MANY_REQUESTS.equals(((IngestError) err).getErrorType()))
                        .peek(err -> {
                            ErrorMonitor errorMonitor = mediaConnectorErrorMonitors.getUnchecked(result.getRunContext());
                            if (errorMonitor.addErrorAndGetIsValveTriggered()) {
                                logger.warn("Received too many errors ({} over {} {} period) - pausing run {}.", errorMonitorTriggerThreshold, errorMonitorTriggerInterval, errorMonitorTriggerIntervalUnit, result.getRunContext());
                                jobConsistencyManager.handleTooManyThrottlingErrors(result);
                            }
                            jobManager.updateJobTaskStateIncrementRetryForEver(result.getRunContext(), jobId, JobType.INGEST, Lists.newArrayList(taskId), null);
                        }).noneMatch(err -> true);
            }
            if (MessagePayloadType.INGEST_ERROR.equals(result.getPayloadType())) {
                logger.error("Remote Ingestion error:{}", result);
                ingestBatchStore.markTaskAsDone(taskId, jobId, TaskState.FAILED);
                valid = false;
            }

            return valid;
        } catch (Exception e) {
            logger.error("Failed to consume ingest result {} on task {}", result, taskId, e);
            ingestBatchStore.markTaskAsDone(taskId, jobId, TaskState.FAILED);
            return false;
        }
    }

    @Transactional
    public void consumeControlResult(ControlResultMessage controlResultMessage) {
        logger.trace("Handling control message: {}", controlResultMessage);

        String mediaProcessorId = controlResultMessage.getMediaProcessorId();

        Long offsetMillis = controlResultMessage.getSendTime() - System.currentTimeMillis();
        boolean isMpOutOfSync = Math.abs(offsetMillis) > TimeUnit.MINUTES.toMillis(maxAllowedMpTimeShift);
        if (isMpOutOfSync) {
            logger.warn("MediaProcessor id {} time is out of sync by {} minutes. May result message loss.", mediaProcessorId, TimeUnit.MILLISECONDS.toMinutes(offsetMillis));
        }

        switch (controlResultMessage.getPayloadType()) {
            case HANDSHAKE_RESPONSE:
                ServerHandShakeResponsePayload handShakePayload = (ServerHandShakeResponsePayload) controlResultMessage.getPayload();
                ClaComponentDto claComponentDto = componentsAppService.registerMediaProcessor(mediaProcessorId, handShakePayload, controlResultMessage.getSendTime());
                logger.info("Add or update handshake component {}", claComponentDto);

                CustomerDataCenterDto customerDataCenterDto = claComponentDto.getCustomerDataCenterDto();
                long dataCenterId = customerDataCenterDto.getId();
                sendHandShakeRequestMessage(mediaProcessorId, dataCenterId, claComponentDto.getActive());

                if (controlResultMessage.isFirstTimeSinceGoingUp()) {
                    // send jobs status to mp
                    List<JobStateByDate> states = jobManager.getCachedJobStates();
                    mediaProcessorCommService.sendJobsStatesControlRequest(states);
                }

                if (handShakePayload.isNeedSearchPatterns()) {
                    // send search patterns to mp
                    Collection<TextSearchPatternImpl> allFileSearchPatternImpls = claFileSearchPatternService.getAllFileSearchPatternImpls();
                    mediaProcessorCommService.sendPatternControlRequest(allFileSearchPatternImpls);
                }

                break;
            case STATUS_REPORT:
                logger.trace("start handle control msg STATUS_REPORT for mp {}", mediaProcessorId);
                ClaComponentDto componentDto = componentsAppService.findRegisteredMediaProcessor(mediaProcessorId);
                if (componentDto == null) {
                    if (sendMediaProcessorStateUpdateIfUnknown) {
                        logger.warn("Component sent status but is not registered. Sending stateChange msg. {}", controlResultMessage);
                        sendStateChangeRequestMessage(mediaProcessorId, null, false);
                    } else {
                        logger.warn("Component sent status but is not registered. {}", controlResultMessage);
                    }
                    return;
                }
                ControlStatusResponsePayload statusPayload = (ControlStatusResponsePayload) controlResultMessage.getPayload();
                componentsAppService.updateMediaProcessorAlive(mediaProcessorId, statusPayload, controlResultMessage.getSendTime());

                if (statusPayload.isNeedSearchPatterns()) {
                    // send search patterns to mp
                    Collection<TextSearchPatternImpl> allFileSearchPatternImpls = claFileSearchPatternService.getAllFileSearchPatternImpls();
                    mediaProcessorCommService.sendPatternControlRequest(allFileSearchPatternImpls);
                }

                break;
            case SCAN_STARTED:
                logger.trace("start handle control msg SCAN_STARTED for mp {}", mediaProcessorId);
                ScanStartedPayload scanStartedPayload = (ScanStartedPayload) controlResultMessage.getPayload();
                boolean jobActive = jobManager.isJobActive(scanStartedPayload.getJobId());
                if (jobActive) {
                    String stateOpMsg = scanStartedPayload.isFirstScan() ? "Start mapping root folder" : "Start remapping root folder";
                    JobStateUpdateInfo jobUpdateInfo = new JobStateUpdateInfo(scanStartedPayload.getJobId(), IN_PROGRESS, stateOpMsg, false);
                    jobManager.updateJobAndTaskStates(jobUpdateInfo, scanStartedPayload.getTaskId(), CONSUMING, false);
                } else {
                    jobManager.updateTaskStatus(scanStartedPayload.getTaskId(), NEW);
                }
                break;
            default:
                logger.warn("Unknown ControlResultMessage type {}", controlResultMessage.getPayloadType());
        }
        logger.trace("Done handling control message: {}", controlResultMessage);
    }

    private void sendHandShakeRequestMessage(String mediaProcessorId, Long dataCenterId, boolean active) {
        ControlRequestMessage controlRequestMessage = createHandShakeRequestMessage(mediaProcessorId, dataCenterId, active);
        mediaProcessorCommService.broadcastControlRequest(controlRequestMessage);
    }

    @SuppressWarnings("SameParameterValue")
    private void sendStateChangeRequestMessage(String mediaProcessorId, Long dataCenterId, boolean active) {
        ControlRequestMessage controlRequestMessage = createStateChangeRequestMessage(mediaProcessorId, dataCenterId, active);
        logger.info("Sending MediaProcessor {} state change {}, dataCenterId {}", mediaProcessorId, active,
                dataCenterId == null ? "null" : dataCenterId);
        mediaProcessorCommService.broadcastControlRequest(controlRequestMessage);
    }

    public void consumeFileContentResult(FileContentResultMessage fileContentResultMessage) {
        FileContentType fileContentType = fileContentResultMessage.getRequestType();
        if (FileContentType.IS_DIRECTORY_EXISTS.equals(fileContentType)) {
            fileContentService.setIsDirectoryExistStatus(fileContentResultMessage.getRootFolderId(), fileContentResultMessage.getPayload().getDirectoryExistsStatus());
        } else if (FileContentType.ERROR.equals(fileContentType) && fileContentResultMessage.getPayload().getRequestId() == null) {
            logger.warn("got error response with no request id {}", fileContentResultMessage);
        } else {
            CompletableFuture<FileContentResultMessage> completableFuture = fileContentService
                    .getCompletableFutureLockByRequestId(fileContentResultMessage.getPayload().getRequestId());
            fileContentService.removeRequestFromLockMap(fileContentResultMessage.getPayload().getRequestId());
            completableFuture.complete(fileContentResultMessage);
        }
    }

    public void consumeContentCheckRequest(ContentMetadataRequestMessage req) {
        ContentMetadataResponseMessage resp = filesDataPersistenceService.processRequest(req);
        resp.setRequestId(req.getRequestId());
        resp.setMediaProcessorId(req.getMediaProcessorId());
        mediaProcessorCommService.sendContentCheckResponse(resp);
    }

    public void consumeLabelingResponse(LabelingResponseMessage resp) {
        LabelTaskParameters params = resp.getPayload();
        Long fileId = params.getFileId();
        if (resp.getErrors() != null && !resp.getErrors().isEmpty()) {
            logger.error("Labeling error: {}, file ID: {}", resp.getErrors(), fileId);
            return;
        }
        List<LabelResult> externalMetadata = resp.getExternalMetadata();
        // handle response and remove expected
        asyncLabelingConsumerService.handleLabeling(fileId, externalMetadata, params);
    }

    private ControlRequestMessage createHandShakeRequestMessage(String mediaProcessorId, Long dataCenterId, boolean active) {
        ServerHandShakeRequestPayload serverHandShakeResponsePayload = new ServerHandShakeRequestPayload();
        serverHandShakeResponsePayload.setDataCenterId(dataCenterId);
        serverHandShakeResponsePayload.setDataCenterRequiresPrefix(true);
        serverHandShakeResponsePayload.setActive(active);

        ControlRequestMessage controlRequestMessage = new ControlRequestMessage(serverHandShakeResponsePayload, ControlRequestType.HANDSHAKE);
        controlRequestMessage.setMediaProcessorId(mediaProcessorId);
        logger.info("Handling control handshake from media processor {}, about to return handshake request {}", mediaProcessorId, controlRequestMessage);

        return controlRequestMessage;
    }

    private ControlRequestMessage createStateChangeRequestMessage(String mediaProcessorId, Long dataCenterId, boolean active) {
        ServerStateChangeRequestPayload serverHandShakeResponsePayload = new ServerStateChangeRequestPayload();
        serverHandShakeResponsePayload.setDataCenterId(dataCenterId);
        serverHandShakeResponsePayload.setDataCenterRequiresPrefix(true);
        serverHandShakeResponsePayload.setActive(active);

        ControlRequestMessage controlRequestMessage = new ControlRequestMessage(serverHandShakeResponsePayload, ControlRequestType.STATE_CHANGE);
        controlRequestMessage.setMediaProcessorId(mediaProcessorId);

        return controlRequestMessage;
    }

    public void consumeSharePermissionsMessage(ScanResultMessage scanTaskMessage) {
        sharePermissionService.consumeSharePermissionsMessage(
                docStoreService.getRootFolderCached(scanTaskMessage.getRootFolderId()), scanTaskMessage);
        docStoreService.evictFromCache(scanTaskMessage.getRootFolderId());
    }

    public void consumeCreateScanTaskMessage(ScanResultMessage scanTaskMessage) {
        Long itemId = null;
        String mailboxUpn = null;
        String params = null;
        if (scanTaskMessage.getPayload() != null) {
            ScanTaskParameters p = (ScanTaskParameters) scanTaskMessage.getPayload();
            itemId = p.getRootFolderId();
            mailboxUpn = p.getMailboxUPN();
        }

        if (mailboxUpn != null && itemId != null) {
            Map<String, String> scanTaskParams = new HashMap<>();
            scanTaskParams.put(ScanTaskParameters.MAILBOX_UPN, mailboxUpn);

            // get folder id from kvdb
            String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, itemId);
            String syncState = null;
            Long folderId;
            if (kvdbRepo.getFileEntityDao().storeExists(storeName)) {
                folderId = kvdbRepo.getFileEntityDao().executeInReadonlyTransaction(storeName, fileEntityStore -> {
                    FileEntity fileEntity = fileEntityStore.get(scanTaskMessage.getPath()); // assume populated by media entity id
                    return (fileEntity == null ? null : fileEntity.getFolderId());
                });

                // get sync state for folder
                if (folderId != null) {
                    DocFolder folder = docFolderService.getById(folderId);
                    syncState = (folder == null ? null : folder.getSyncState());
                } else {
                    logger.debug("cant find folder in kvdb by path {} for root folder {} to get sync state", scanTaskMessage.getPath(), itemId);
                }
                if (syncState != null) {
                    scanTaskParams.put(ScanTaskParameters.SYNC_STATE, syncState);
                }
            }
            params = ModelDtoConversionUtils.mapToJson(scanTaskParams);
        }

        if (!jobManager.chkIfScanTaskAlreadyExists(scanTaskMessage.getJobId(), scanTaskMessage.getPath())) {

            SimpleTask taskCreated = jobManager.createTask(itemId, scanTaskMessage.getJobId(), TaskType.SCAN_ROOT_FOLDER, params, scanTaskMessage.getPath());
            logger.debug("created scan task for path {} with parent task {} and id {}", scanTaskMessage.getPath(), scanTaskMessage.getTaskId(), taskCreated.getId());
        } else {
            logger.debug("task for this path {} and job {} already exists - skip", scanTaskMessage.getPath(), scanTaskMessage.getJobId());
        }
    }

    public void consumeScanResult(ScanResultMessage scanResultMessage, String offsetsString) {

        Set<Long> locallyStoppedJobIds = Sets.newHashSet();
        Long batchIdentifierNumber = getNextMapBatchId();

        try {
            scanDoneConsumer.updateTasksRunningCounter(batchIdentifierNumber, scanResultMessage.getTaskIds(), true);
            List<ScanResultMessage> scanResultMessages = scanResultMessage.getContainedMessages() == null ?
                    Lists.newArrayList(scanResultMessage) : scanResultMessage.getContainedMessages();
            List<ScanResultMessage> scanDoneMessages = Lists.newArrayList();
            List<ScanResultMessage> newScanMessages = Lists.newArrayList();

            if (scanResultMessages == null) {
                logger.error("Received an empty scan result: {}", scanResultMessage.toString());
                return;
            }

            Map<Long, Boolean> isRunPaused = Maps.newHashMap();
            Set<Long> tasksWithNewMsgs = Sets.newHashSet();

            scanResultMessages.forEach(msg -> {
                try {
                    if (msg == null) {
                        Long jid = -1L;
                        Long rc = -1L;
                        if (scanResultMessages.size() > 0 && scanResultMessages.get(0) != null) {
                            rc = scanResultMessages.get(0).getRunContext();
                            jid = jobManager.getJobIdCached(rc, JobType.SCAN);
                        }
                        logger.warn("Got null scanResultMessage. Total in response {}. RunId {}, scanJob {}", scanResultMessages.size(), rc, jid);
                    } else if (msg.getRunContext() == null) {
                        logger.warn("Got scanResultMessage with no run context will not be processed {}", msg);
                    } else {
                        // chk if job is paused - save in map for the entire bulk so we dont do this per msg
                        Boolean isPaused = isRunPaused.get(msg.getRunContext());
                        if (isPaused == null) {
                            isPaused = jobConsistencyManager.isJobPaused(msg.getRunContext(), JobType.SCAN);
                            isRunPaused.put(msg.getRunContext(), isPaused);
                        }

                        // if job is paused - do not consume the results.
                        if (!isPaused) {
                            MessagePayloadType payloadType = msg.getPayloadType();
                            if (payloadType.in(MessagePayloadType.SCAN_DONE, MessagePayloadType.SCAN_FAILED)) {
                                scanDoneMessages.add(msg);
                            } else {
                                newScanMessages.add(msg);
                                tasksWithNewMsgs.add(msg.getTaskId());
                            }
                        } else {
                            Long jobId = jobManager.getJobIdCached(msg.getRunContext(), JobType.SCAN);
                            logger.debug("Got consumeScanResult while jobType SCAN is paused. RunId {}, scanJob {}",
                                    msg.getRunContext(), jobId);
                            // TODO: avoid sending multiple messages in case of a queue filled with messages.
                            // Note last msg sent and re-send only after a threshold has passed.
                            if (jobId != null && !locallyStoppedJobIds.contains(jobId)) {
                                List<JobStateByDate> jobStates = new ArrayList<>();
                                jobStates.add(jobManager.createStateByDateForJob(jobId, JobState.PAUSED, msg.getRunContext()));
                                mediaProcessorCommService.sendJobsStatesControlRequest(jobStates);
                                locallyStoppedJobIds.add(jobId);
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("problem processing msg {}", msg, e);
                    throw e;
                }
            });

            if (scanDoneMessages.size() > 0) {
                logger.info("{} scan done messages with offset {} found", scanDoneMessages.size(), offsetsString);
            }

            if (scanResultMessage.getTaskIds() != null && !scanResultMessage.getTaskIds().isEmpty()) {
                Set<Long> tasksWithoutNewMsgs = new HashSet<>(scanResultMessage.getTaskIds());
                tasksWithoutNewMsgs.removeAll(tasksWithNewMsgs);
                scanDoneConsumer.updateTasksRunningCounter(batchIdentifierNumber, tasksWithoutNewMsgs, false);
            }

            if (newScanMessages.size() > 0) {
                opStateService.incrementMessageCounter(newScanMessages.size());
                //We separate to get different transactions
                KpiRecording kpiRecording = performanceKpiRecorder.startRecording("processScanResultMessages", "RemoteMediaProcessingService.consumeScanResult", 852);
                scanItemConsumer.processScanResultMessages(batchIdentifierNumber, newScanMessages);
                kpiRecording.end();
            }

            if (scanDoneMessages.size() > 0) {
                KpiRecording kpiRecording = performanceKpiRecorder.startRecording("processScanDoneMessages", "RemoteMediaProcessingService.consumeScanResult", 859);
                processScanDoneMessages(scanDoneMessages);
                kpiRecording.end();
            }

        } catch (Exception e) {
            logger.error("Failed to consume scanResult Messages", e);
        }
    }


    public void updateMediaProcessorCommunicationChannel(ClaComponent sysComponent, CustomerDataCenter customerDataCenter) {
        ControlRequestMessage controlRequestMessage = createHandShakeRequestMessage(sysComponent.getInstanceId(), customerDataCenter.getId(), sysComponent.getActive());
        mediaProcessorCommService.updateMediaProcessorCommunicationChannel(controlRequestMessage);
    }

    public void consumeListFolderContentResult(FoldersListResultMessage foldersListResultMessage) {
        Optional.ofNullable(MediaSpecificAppService.requestLockMap.pop(foldersListResultMessage.getRequestId()))
                .map(fut -> fut.complete(foldersListResultMessage)).orElseGet(() -> {
            logger.warn("No request with id {} was found in requestLockMap", foldersListResultMessage.getRequestId());
            return null;
        });
    }

    public void consumeIngestResult(IngestResultMessage ingestResultMessage, IngestBatchStore ingestBatchStore) {
        // if job is paused - do not consume the results
        boolean paused = jobConsistencyManager.isJobPaused(ingestResultMessage.getRunContext(), JobType.INGEST);
        if (!paused) {
            if (verifyResultValid(ingestResultMessage, ingestBatchStore)) {
                logger.trace("***** Handling ingest result {}", ingestResultMessage);
                handleIngestResult(ingestResultMessage, ingestBatchStore);
            }
        } else {
            Long jobId = jobManager.getJobIdCached(ingestResultMessage.getRunContext(), JobType.INGEST);
            logger.debug("Got consumeIngestResult while jobType INGEST is paused. RunId {}, ingestJob {}",
                    ingestResultMessage.getRunContext(), jobId);
            List<JobStateByDate> jobStates = Lists.newArrayList();
            jobStates.add(jobManager.createStateByDateForJob(jobId, JobState.PAUSED, ingestResultMessage.getRunContext()));
            mediaProcessorCommService.sendJobsStatesControlRequest(jobStates);
        }
    }

    private ClaFile claFileKeyCollisionResolver(ClaFile file1, ClaFile file2) {
        int rank1 = fileCatService.rankClaFileRecord(file1);
        int rank2 = fileCatService.rankClaFileRecord(file2);
        logger.warn("Collision between similar ClaFiles during collection of stream into map: {} (rank {}) vs. {} (rank {})",
                file1, rank1, file2, rank2);

        return rank1 > rank2 ? file1 : file2;
    }

    /**
     * Convert ingestion task to ingestion request message
     *
     * @param task         ingestion task
     * @param claFilesById
     * @return ingestion request message
     */
    private IngestRequestMessage createIngestRequestMessage(
            SimpleTask task, RootFolder rf, ClaFile file, Map<Long, ClaFile> claFilesById,
            MediaConnectionDetailsDto connectionDetails, List<EntityCredentialsDto> credentials, Integer ingestTimeoutSec) {

        if (task == null || file == null) {
            logger.debug("Ingest Task or file are null - unable to convert to message");
            return null;
        }
        if (task.getRunContext() == null) {
            throw new RuntimeException("Missing runContext on ingest task " + task);
        }

        long jobId = task.getJobId();
        IngestTaskJsonProperties jsonProps = IngestTaskJsonProperties.fromJsonString(task.getJsonData());
        IngestTaskParameters mainFileParameters = createIngestTaskParameters(
                task.getRunContext(), jobId, rf, file, connectionDetails, credentials, jsonProps);

        List<IngestTaskParameters> containedFilesIngestParams = jsonProps.getContainedFileIDs()
                .stream()
                .map(claFilesById::get)
                .map(claFile -> createIngestTaskParameters(task.getRunContext(), jobId, rf, claFile, connectionDetails, credentials, jsonProps))
                .collect(toList());


        IngestRequestMessage message = new IngestRequestMessage(mainFileParameters);
        message.setContainedFilesPayloads(containedFilesIngestParams);
        message.setRunContext(task.getRunContext());
        message.setTaskId(task.getId());
        message.setJobId(jobId);
        int timeout = (ingestTimeoutSec == null ? enqueuedTaskTimeoutSec : ingestTimeoutSec);
        message.setExpirationTime(System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(timeout));
        return message;
    }

    private IngestTaskParameters createIngestTaskParameters(long runId,
                                                            long jobId, RootFolder rootFolder, ClaFile file, MediaConnectionDetailsDto connectionDetails,
                                                            List<EntityCredentialsDto> credentials, IngestTaskJsonProperties jsonProps) {

        boolean metadataOnly = jsonProps.isMetadataOnly();
        boolean firstScan = jsonProps.isFirstScan();
        boolean stopAfterScanning = fileCatService.stopAfterScanning(file);
        Long rootFolderId = file.getRootFolderId();
        boolean ingestContent = shouldIngestContents(file);

        IngestTaskParameters parameters = IngestTaskParameters.create()
                .setIngestContents(ingestContent && !metadataOnly && !stopAfterScanning)
                .setJobId(jobId)
                .setPackageScanTypeSpecification(buildPackageScanTypeSpecification(rootFolder))
                .setMediaConnectionDetailsDto(connectionDetails)
                .setFirstScan(firstScan)
                .setJobStateIdentifier(fileCrawlerExecutionDetailsService.getRunStopTime(runId))
                .setMediaEntityId(LocalFileUtils.createFullPath(
                        rootFolder.getMediaType(), rootFolder.getRealPath(), file.getMediaEntityId()));

        parameters.setRootFolderId(rootFolderId)
                .setFilePath(LocalFileUtils.createFullPath(
                        rootFolder.getMediaType(), rootFolder.getRealPath(), file.getFileName()))
                .setFileType(file.getType())
                .setFileId(file.getId());

        if (credentials != null && !credentials.isEmpty()) {
            EntityCredentialsDto dto = credentials.iterator().next();
            parameters.setUsername(dto.getUsername());
            parameters.setPassword(dto.getPassword());
        }
        return parameters;
    }

    private boolean shouldIngestContents(ClaFile file) {
        FileType fileType = file.getType();
        Long rootFolderId = file.getRootFolderId();
        Set<String> fileTypes = getFileTypes(rootFolderId);
        //noinspection SimplifiableIfStatement
        if (fileTypes.contains(fileType.toString().toLowerCase())) {
            return true;
        }

        return fileTypes.contains(FileType.PST.toString().toLowerCase())
                && PstPath.isPstEntryPath(file.getMediaEntityId());
    }

    @NotNull
    private ScanTypeSpecification buildPackageScanTypeSpecification(RootFolder rootFolder) {
        final Map<String, FileType> concreteScanFileTypesMap = Maps.newHashMap();
        List<FileType> typesToScan = DocStoreService.getRootFolderFileTypes(rootFolder.getMapFileTypes(), concreteScanFileTypesMap);

        // TODO: NADAV - START: remove this when supported
        List<FileType> supportedFileTypes = typesToScan.stream()
                .filter(f -> f != FileType.PACKAGE)
                .collect(Collectors.toList());
        Set<String> extensionsToScan = concreteScanFileTypesMap.entrySet().stream()
                .filter(e -> e.getValue() != FileType.PACKAGE)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());

        // TODO: NADAV - END: remove this when supported
        Set<String> extensionsToIgnore = FileTypeUtils.getScanFileTypesMap().entrySet().stream()
                .filter(e -> e.getValue() == FileType.PACKAGE)
                .map(Map.Entry::getKey)
                .collect(Collectors.toSet());
        extensionsToIgnore.addAll(docStoreService.getScanOtherIgnoreExtSet());
        return new ScanTypeSpecification(supportedFileTypes, extensionsToScan, extensionsToIgnore);
    }


    private void processScanDoneMessages(List<ScanResultMessage> scanDoneMessages) {
        try {
            logger.debug("Process {} scanDone messages", scanDoneMessages.size());
            long start = timeSource.currentTimeMillis();
            logger.debug("Consume {} scan done messages", scanDoneMessages.size());
            for (ScanResultMessage scanResultMessage : scanDoneMessages) {
                try {
                    scanDoneConsumer.accept(scanResultMessage);
                } catch (Exception e) {
                    logger.error("Failed to consume scan done message {}", scanResultMessage, e);
                }
            }
            long duration = timeSource.millisSince(start);
            logger.debug("Consumed {} scan result messages in {}ms", scanDoneMessages.size(), duration);
        } catch (Exception e) {
            logger.error("Failed to process scanDone message {}", scanDoneMessages.get(0), e);
        }
    }

    private ScanRequestMessage createScanStartRequestMessage(Long runId, RootFolder rootFolder, SimpleTask scanTask, boolean isFirstScan) {
        Long rootFolderId = rootFolder.getId();
        List<FolderExcludeRuleDto> folderExcludeRulesDto = docStoreService.getFolderExcludeRulesDto(rootFolderId);

        Optional<MediaConnectionDetailsDto> connectionDetails =
                mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(rootFolder.getId(), true);

        long lastRunDate = fileCrawlerExecutionDetailsService.getLastSuccessfulCrawlRun(rootFolder.getId());

        int scanTaskDepthForRootFolder = rootFolder.getScanTaskDepth() == null ? scanTaskDepth : rootFolder.getScanTaskDepth();
        int depth = scanTask.getPath() == null ? 0 :
                DocFolderUtils.calculateDepth(FileNamingUtils.convertPathToUniversalString(scanTask.getPath())) - rootFolder.getBaseDepth();
        String path = scanTask.getPath() != null ? scanTask.getPath() : rootFolder.getRealPath(); // TODO does this need to happen here or in scan task creation? DAMAIN-2784

        logger.trace("send scan start req to mp for folder {} under root folder {} when task depth is {} and depth is {}", path, rootFolderId, scanTaskDepthForRootFolder, depth);

        CrawlRun run = fileCrawlerExecutionDetailsService.getCrawlRun(runId);
        long startRunTime = run.getStartTime().getTime();

        boolean ftsLevelOnly = (rootFolder.getMediaType().equals(MediaType.EXCHANGE_365) || depth < scanTaskDepthForRootFolder);

        Map<String, String> keyValuePairs = retrieveKeyValuePairsForMP(rootFolder);

        ScanTaskParameters scanTaskParameters = ScanTaskParameters.create()
                .setRunId(runId)
                .setRootFolderId(rootFolderId)
                .setFirstScan(isFirstScan)
                .setLastScannedOnTimestamp(lastRunDate)
                .setPath(path)
                .setExcludedRules(folderExcludeRulesDto)
                .setSkipDirnames(globalDirnames2Skip)
                .setScanTypeSpecification(docStoreService.getRootFolderScanTypeSpecification(rootFolderId))
                .setConnectionDetails(connectionDetails.orElse(null))
                .setJobId(scanTask.getJobId())
                .setFstLevelOnly(ftsLevelOnly)
                .setStartScanTime(startRunTime)
                .setRootFolderMediaEntityId(rootFolder.getMediaEntityId())
                .setKeyValuePairs(keyValuePairs)
                .setGetSharePermissions(shouldGetSharePermissions(rootFolder.getMediaType(), rootFolder.getRealPath(), depth))
                .setIgnoreAccessErrors(rootFolder.getIgnoreAccessErrors() == null ? ignoreAccessErrors : rootFolder.getIgnoreAccessErrors())
                .setJobStateIdentifier(run.getStopTimeAsLong());

        if (rootFolder.getMediaType().equals(MediaType.EXCHANGE_365)) {
            scanTaskParameters.setFromDateFilter(rootFolder.getFromDateScanFilter());
        }

        List<EntityCredentialsDto> credentials = entityCredentialsService.getEntityCredentialsForRootFolderCached(rootFolderId);

        if (credentials != null && !credentials.isEmpty()) {
            EntityCredentialsDto credentialsDto = credentials.iterator().next();
            scanTaskParameters.setUsername(credentialsDto.getUsername());
            scanTaskParameters.setPassword(credentialsDto.getPassword());
        }

        ScanRequestMessage scanRequestMessage = new ScanRequestMessage(scanTaskParameters);
        scanRequestMessage.setTaskId(scanTask.getId());
        scanRequestMessage.setJobId(scanTask.getJobId());

        if (rootFolder.getReingestTimeStampMs() > 0) {
            scanRequestMessage.setForceScanFromScratch(true);
        }

        return scanRequestMessage;
    }

    boolean shouldGetSharePermissions(MediaType mediaType, String realPath, int depth) {
        if (!sharePermissionService.isSharePermissionFeatureEnabled()) {
            return false;
        }

        if (!mediaType.equals(MediaType.FILE_SHARE)) {
            return false;
        }

        if (depth != 0) {
            return false;
        }

        if (realPath.matches("^[a-zA-Z]{1}:.*")) {
            return false;
        }

        String path = FilenameUtils.separatorsToWindows(realPath);
        if (!path.startsWith("\\\\")) {
            return false;
        }
        path = path.replace("\\\\", "\\");
        if (path.startsWith("\\")) {
            path = path.substring(1);
        }
        String[] parts = path.split("\\\\");
        String share;
        if (parts.length > 1) {
            share = parts[1];
            if (share.matches("^[a-zA-Z]{1}[$]{1}$")) {
                return false;
            }
        }

        return true;
    }

    private Map<String, String> retrieveKeyValuePairsForMP(RootFolder rootFolder) {
        //TODO: retrieve (from DB) all the additional  key value pairs needed for the scan (like the stream position an such)
        final String prefix = "box-delta-position.";
        return systemSettingsService.getSystemSettingsByPrefix(prefix).stream()
                .filter(s -> s.getName().equals(prefix + "lastScanDate.RF" + rootFolder.getId()) ||
                        s.getName().equals(prefix + "position.RF" + rootFolder.getId()))
                .collect(Collectors.toMap(SystemSettingsDto::getName, SystemSettingsDto::getValue));
        //NOTE : see example values bellow
		/*//example
		return new HashMap<String,String>(){{
			put("box-delta-position.lastScanDate.RF" + rootFolder.getId().toString(), String.valueOf( System.currentTimeMillis() - (1000 * 60 * 10)));
			put("box-delta-position.position.RF" + rootFolder.getId().toString(), "0");
		}};*/
    }


    /**
     * Get file types (from cache)
     *
     * @param rootFolderId root folder ID
     * @return set of file types to ingest
     */
    private Set<String> getFileTypes(Long rootFolderId) {
        return rootFolderFileTypes.getUnchecked(rootFolderId);
    }

    /**
     * Get file types (from DB)
     *
     * @param rootFolderId root folder ID
     * @return set of file types to ingest
     */
    private Set<String> getFileTypesFromDB(Long rootFolderId) {
        String[] fileTypes = docStoreService.getRootFolderMapFileTypesAsStringArray(rootFolderId);
        Set<String> result = Stream.of(fileTypes).map(String::toLowerCase).collect(Collectors.toSet());
        if (result.contains(FileType.OTHER.name().toLowerCase())) {
            result.add(FileType.CSV.name().toLowerCase());
            result.add(FileType.TEXT.name().toLowerCase());
        }
        return result;
    }

    // ---------------------------------------------------------------------------------------------

    class IngestStartTaskHandling {
        IngestStartTaskHandling(Collection<Long> failedTaskIds, Collection<Long> skippedTaskIds) {
            this.failedTaskIds = failedTaskIds;
            this.skippedTaskIds = skippedTaskIds;
        }

        Collection<Long> failedTaskIds;
        Collection<Long> skippedTaskIds;
    }
}
