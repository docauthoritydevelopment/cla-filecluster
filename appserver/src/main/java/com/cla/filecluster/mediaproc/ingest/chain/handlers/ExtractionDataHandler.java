package com.cla.filecluster.mediaproc.ingest.chain.handlers;

import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.ExcelIngestResult;
import com.cla.common.domain.dto.messages.ExcelSheetIngestResult;
import com.cla.common.domain.dto.messages.WordIngestResult;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.chain.ChainLink;
import com.cla.filecluster.service.extractor.entity.ContentSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Save extraction data in the Extraction Solr core
 * @see com.cla.filecluster.repository.solr.FileDTOSearchRepository
 */
@Component
public class ExtractionDataHandler implements ChainLink<IngestResultContext> {

    private static final String SHEET_NAMES_SEPARATOR = ", ";

    @Autowired
    protected ContentSearchService contentSearchService;

    @Value("${tokenMatcherActive}")
    private boolean isTokenMatcherActive;

    @Override
    public boolean shouldHandle(IngestResultContext context) {
        return isTokenMatcherActive;
    }

    @Override
    public boolean handle(IngestResultContext context) {
        SolrFileEntity file = context.getFile();
        Long taskId = context.getIngestResultMessage().getTaskId();
        ContentMetadata contentMetadata = context.getContentMetadata();

        if (context.isWordOrPDF()) {
            WordIngestResult payload = context.getWordResultMessage().getPayload();
            contentSearchService.saveContentInSolr(taskId, file, contentMetadata, payload.getContent());
        }
        else if (context.isOther()){
            OtherFile otherFile = context.getOtherResultMessage().getPayload();
            if (otherFile.getHashedContent() != null) {
                contentSearchService.saveContentInSolr(taskId, file, contentMetadata, otherFile.getHashedContent());
            }
        }
        else if (context.isExcel()){
            ExcelIngestResult payload = context.getExcelResultMessage().getPayload();
            StringBuilder sbTokens = new StringBuilder();
            StringBuilder sbParts = new StringBuilder();
            for (ExcelSheetIngestResult excelSheet : payload.getSheets()) {
                sbTokens.append(excelSheet.getContentTokens());
                sbParts.append(excelSheet.getFullSheetName());
                sbParts.append(SHEET_NAMES_SEPARATOR);
            }
            if (payload.getSheets().size() > 0){
                sbParts.setLength(sbParts.length() - SHEET_NAMES_SEPARATOR.length());
            }
            contentSearchService.saveContentInSolr(taskId, context.getSavedFile(), contentMetadata,
                    sbTokens.toString(), sbParts.toString());
        }

        return true;
    }


}
