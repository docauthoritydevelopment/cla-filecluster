package com.cla.filecluster.repository.solr;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 *
 * Created by uri on 31/01/2017.
 */
@Repository
public class SolrGrpHelperRepository extends SolrRepository {

    @Value("${solr.contentMetadata.group.commit.ms:1}")
    private int commitGroupWithinMs;

    @Value("${grpHelper.solr.core:GrpHelper}")
    private String grpHelperSolrCore;

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected String coreName() {
        return grpHelperSolrCore;
    }

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    public void updateAnalysisGroup(long contentId, String groupId) {
        SolrInputDocument updateGroupDocument = SolrRepositoryUtils.updateFieldInputDocument(""+contentId, "groupId", groupId);
        updateDocument(contentId, groupId, updateGroupDocument);
    }

    private void updateDocument(long contentId, String groupId, SolrInputDocument updateGroupDocument) {
        try {
            UpdateResponse updateResponse = client.add(updateGroupDocument, commitGroupWithinMs);
            logger.debug("Update analysis group {} for contentId {} in Solr: {}",groupId, contentId, updateResponse.toString());
        } catch (Exception e) {
            throw new RuntimeException("Failed to update analysis group to content id "+contentId,e);
        }
    }

    public void excludeFileFromGrouping(Long contentId) {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", ""+contentId);
        Map<String, Object> unsetFieldValueMap = atomicSetValue(null);
        doc.setField("groupId", unsetFieldValueMap);
        doc.setField("analysisHint", atomicSetValue(AnalyzeHint.EXCLUDED));
        updateDocument(contentId, null, doc);
    }

    public void updateAnalyzeHint(AnalyzeHint analyzeHint, Long contentId, boolean commit) {
        try {
            SolrInputDocument doc = new SolrInputDocument();
            doc.addField("id", contentId);
            doc.addField("analysisHint", atomicSetValue(analyzeHint));
            UpdateResponse updateResponse = getSolrClient().add(doc);
            if (updateResponse == null) {
                throw new RuntimeException("Failed to analyze hint in Solr. Content id:" + contentId);
            }
            if (commit) {
                commit();
            }
        } catch (Exception e) {
            logger.error("Failed to analyze hint in Solr", e);
            throw new RuntimeException("Failed to analyze hint in Solr. Content id:" + contentId, e);
        }
    }

}
