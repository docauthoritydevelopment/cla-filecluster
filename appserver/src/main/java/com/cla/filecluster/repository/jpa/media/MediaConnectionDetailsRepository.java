package com.cla.filecluster.repository.jpa.media;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 08/08/2016.
 */
public interface MediaConnectionDetailsRepository extends JpaRepository<MediaConnectionDetails,Long> {

    @Query("select mcd from MediaConnectionDetails mcd where mcd.name = :name")
    MediaConnectionDetails findByName(@Param("name") String name);

    @Query("select mcd from MediaConnectionDetails mcd where mcd.mediaType = :mediaType")
    List<MediaConnectionDetails> listMediaConnectionDetails(@Param("mediaType") MediaType mediaType);
}
