package com.cla.filecluster.repository.jpa.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cla.filecluster.domain.entity.security.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	@Query("select distinct r from Role r "
			+ " left join fetch r.authorities ")
	List<Role> findWithAuthorities();

	@Query("select r from Role r where r.name = :roleName")
	Role findRoleByName(@Param("roleName") String roleName);

    @Query("select r from Role r where r.name in :roleNames")
    List<Role> findRolesByName(@Param("roleNames") List<String> rolesList);
}
