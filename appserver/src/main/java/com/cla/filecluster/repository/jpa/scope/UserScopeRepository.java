package com.cla.filecluster.repository.jpa.scope;

import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.scope.UserScope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserScopeRepository extends JpaRepository<UserScope, Long> {

    @Query("select usc from UserScope usc where usc.user.id = :userId")
    UserScope findByUserId(@Param("userId") Long userId);
}
