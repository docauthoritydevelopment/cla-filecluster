package com.cla.filecluster.repository.solr;

import com.cla.common.constants.SolrCoreSchema;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SolrFacetQueryJson {

    private String name;
    private FacetType type;
    private SolrCoreSchema field;
    private FacetFunction func;
    private Long offset;
    private boolean numBuckets = false;
    private Integer mincount;
    private Integer limit;
    private ArrayList<SolrFacetQueryJson> facetList;
    private String sortField;
    private Sort.Direction sortDirection;
    private String prefix;
    private String start;
    private String end;
    private String gap;
    private List<String> filterList;
    private List<String> queryValues;
    private String queryValueString;
    private boolean searchOthers = false;
    private boolean showTotalNumberOfBuckets;

    ////////////// ctor

    public SolrFacetQueryJson() {
    }

    public SolrFacetQueryJson(String name) {
        this.name = name;
    }

    /////////////// geter & setter

    public boolean isSearchOthers() {
        return searchOthers;
    }

    public void setSearchOthers(boolean searchOthers) {
        this.searchOthers = searchOthers;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SolrCoreSchema getField() {
        return field;
    }

    public void setField(SolrCoreSchema field) {
        this.field = field;
    }

    public Long getOffset() {
        return offset;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Integer getMincount() {
        return mincount;
    }

    public void setMincount(Integer mincount) {
        this.mincount = mincount;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public ArrayList<SolrFacetQueryJson> getFacetList() {
        return this.facetList;
    }

    public FacetType getType() {
        return type;
    }

    public void setType(FacetType type) {
        this.type = type;
    }

    public FacetFunction getFunc() {
        return func;
    }

    public void setFunc(FacetFunction func) {
        this.func = func;
    }

    public void addFacet(SolrFacetQueryJson newFacet) {
        if (this.facetList == null) {
            this.facetList = new ArrayList<>();
        }
        this.facetList.add(newFacet);
    }

    public List<String> getQueryValues() {
        return queryValues;
    }

    public void setQueryValues(List<String> queryValues) {
        this.queryValues = queryValues;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getQueryValueString() {
        return queryValueString;
    }

    public void setQueryValueString(String queryValueString) {
        this.queryValueString = queryValueString;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    private String getSimpleQuery() {
        return "\"" + this.getName() + "\"" + ":\"" + this.getFunc().getName() + "(" + this.getField() + ")\"";
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getGap() {
        return gap;
    }

    public void setGap(String gap) {
        this.gap = gap;
    }

    public void setFacetList(ArrayList<SolrFacetQueryJson> facetList) {
        this.facetList = facetList;
    }

    public List<String> getFilterList() {
        return filterList;
    }

    public void setFilterList(List<String> filterList) {
        this.filterList = filterList;
    }

    public Sort.Direction getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(Sort.Direction sortDirection) {
        this.sortDirection = sortDirection;
    }

    public boolean isNumBuckets() {
        return numBuckets;
    }

    public void setNumBuckets(boolean numBuckets) {
        this.numBuckets = numBuckets;
    }

    //// private utils

    private String getRangeTypeQuery() {
        String ans = "";
        if (this.field != null) {
            ans = ans + "field: " + this.field + ",";
        }
        if (this.start != null) {
            ans = ans + "start: \"" + this.start + "\",";
        }
        if (this.end != null) {
            ans = ans + "end: \"" + this.end + "\",";
        }
        if (this.gap != null) {
            ans = ans + "gap: \"" + this.gap + "\",";
        }
        return ans;
    }

    private String getQueryTypeQuery() {
        if (this.field != null && this.queryValues != null && this.queryValues.size() > 0) {
            Stream<String> myStream = this.queryValues.stream();
            List<String> newFieldsList = myStream.map(s -> "\"" + s + "\"").collect(Collectors.toList());
            return "q : '" + (this.searchOthers ? "!" : "") + this.field + ":(" + String.join(" ", newFieldsList) + ")',";
        } else if (this.field != null && this.queryValueString != null) {
            return "q : '" + (this.searchOthers ? "!" : "") + this.field + ":" + queryValueString + "',";
        }
        return "";
    }

    //// calc query string

    public String toQueryString() {
        String ans;
        if (this.type == FacetType.SIMPLE) {
            ans = getSimpleQuery();
        } else {
            ans = "\"" + this.name + "\"" + ":{";
            if (this.type != null) {
                ans = ans + "type : " + this.type.getName() + ",";
            }
            if (this.type == FacetType.TERMS) {
                if (this.field != null) {
                    ans = ans + "field : " + this.field + ",";
                }
            } else if (this.type == FacetType.QUERY) {
                ans = ans + getQueryTypeQuery();
            } else if (this.type == FacetType.RANGE) {
                ans = ans + getRangeTypeQuery();
            }

            if (this.prefix != null) {
                ans = ans + "prefix : \"" + this.prefix + "\",";
            }

            if (this.sortField != null) {
                ans = ans + "sort : \"" + this.sortField + " " + (this.sortDirection != null ? this.sortDirection.name().toLowerCase() : Sort.Direction.DESC.name().toLowerCase()) + "\",";
            }

            if (this.offset != null) {
                ans = ans + "offset : " + this.offset + ",";
            }

            if (this.numBuckets ) {
                ans = ans + "numBuckets : true ,";
            }

            if (this.mincount != null) {
                ans = ans + "mincount : " + this.mincount + ",";
            }

            if (this.limit != null) {
                ans = ans + "limit : " + this.limit + ",";
            }

            if (this.filterList != null && !this.filterList.isEmpty()) {
                ans = ans + "domain:{filter:[" + this.filterList.stream().map(filterStr -> "\"" + filterStr + "\"").collect(Collectors.joining(",")) + "]},";
            }

            if (this.showTotalNumberOfBuckets) {
                ans = ans + "numBuckets : " + this.showTotalNumberOfBuckets + ",";
            }

            if (this.facetList != null) {
                StringBuilder facetString = new StringBuilder();
                for (SolrFacetQueryJson facet : facetList) {
                    if (facetString.length() > 0) {
                        facetString.append(",");
                    }
                    facetString.append(facet.toQueryString());
                }
                ans = ans + "facet : {" + facetString + "},";
            }
            if (ans.endsWith(",")) {
                ans = ans.substring(0, ans.length() - 1);
            }
            ans = ans + "}";
        }
        return ans;
    }

    // TODO
    // we added this as to not break current functionality
    // should be merged and ut added
    public String toFuncQueryString() {
        String ans;
        if (this.type == FacetType.SIMPLE) {
            ans = getSimpleQuery();
        } else {
            if (name != null) {
                ans = "\"" + this.name + "\"" + ":{";
            } else {
                ans = "{";
            }

            if (this.type != null) {
                ans = ans + "type : " + this.type.getName() + ",";
            }
            if (this.type == FacetType.TERMS) {
                if (this.field != null) {
                    ans = ans + "field : " + this.field + ",";
                }
            } else if (this.type == FacetType.QUERY) {
                ans = ans + getQueryTypeQuery();
            } else if (this.type == FacetType.RANGE) {
                ans = ans + getRangeTypeQuery();
            }

            if (this.func != null) {
                ans = ans + "facet : {" + this.getField() + "_" + this.getFunc().getName() + ":\"" + this.getFunc().getName() + "(" + this.getField() + ")\"}";
            }

            if (this.prefix != null) {
                ans = ans + "prefix : \"" + this.prefix + "\",";
            }

            if (this.sortField != null) {
                ans = ans + "sort : \"" + this.sortField + " " + (this.sortDirection != null ? this.sortDirection.name().toLowerCase() : Sort.Direction.DESC.name().toLowerCase()) + "\",";
            }

            if (this.offset != null) {
                ans = ans + "offset : " + this.offset + ",";
            }
            if (this.mincount != null) {
                ans = ans + "mincount : " + this.mincount + ",";
            }

            if (this.limit != null) {
                ans = ans + "limit : " + this.limit + ",";
            }

            if (this.filterList != null && !this.filterList.isEmpty()) {
                ans = ans + "domain:{filter:[" + this.filterList.stream().map(filterStr -> "\"" + filterStr + "\"").collect(Collectors.joining(",")) + "]},";
            }

            if (this.showTotalNumberOfBuckets) {
                ans = ans + "numBuckets : " + this.showTotalNumberOfBuckets + ",";
            }

            if (this.facetList != null) {
                StringBuilder facetString = new StringBuilder();
                for (SolrFacetQueryJson facet : facetList) {
                    if (facetString.length() > 0) {
                        facetString.append(",");
                    }
                    facetString.append(facet.toFuncQueryString());
                }
                ans = ans + "facet : {" + facetString + "},";
            }
            if (ans.endsWith(",")) {
                ans = ans.substring(0, ans.length() - 1);
            }
            ans = ans + "}";
        }
        return ans;
    }

    public void setShowTotalNumberOfBuckets(boolean showTotalNumberOfBuckets) {
        this.showTotalNumberOfBuckets = showTotalNumberOfBuckets;
    }

    public boolean getShowTotalNumberOfBuckets() {
        return showTotalNumberOfBuckets;
    }
}
