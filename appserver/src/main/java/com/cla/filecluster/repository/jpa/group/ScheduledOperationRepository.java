package com.cla.filecluster.repository.jpa.group;


import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ScheduledOperationRepository extends JpaRepository<ScheduledOperationMetadata, Long> {

    @NotNull
    @Override
    @Query(value = "select go from ScheduledOperationMetadata go where state <> 'FAILED' ORDER BY priority ASC")
    Page<ScheduledOperationMetadata> findAll(@NotNull Pageable pageable);

    @NotNull
    @Override
    @Query(value = "select go from ScheduledOperationMetadata go where state <> 'FAILED' ORDER BY priority ASC")
    List<ScheduledOperationMetadata> findAll();

    @Query(value = "select count(go) from ScheduledOperationMetadata go where state <> 'FAILED'")
    long countPending();

    @Query(value = "select go from ScheduledOperationMetadata go where isUserOperation = true " +
            "and (state <> 'FAILED' or updateTime is null or updateTime > :fromTime) and coalesce(shouldDisplay, true) = true " +
            "ORDER BY priority ASC")
    List<ScheduledOperationMetadata> findAllUserOperations(@Param("fromTime") long fromTime);

    @Query(value = "select go from ScheduledOperationMetadata go " +
            "where isUserOperation = true and ownerId = :owner " +
            "and (state <> 'FAILED' or updateTime is null or updateTime > :fromTime) and coalesce(shouldDisplay, true) = true " +
            "ORDER BY priority ASC")
    List<ScheduledOperationMetadata> findAllUserOperations(@Param("owner") Long ownerId, @Param("fromTime") long fromTime);

    @Query(value = "select go from ScheduledOperationMetadata go where operationType = :operationType and state <> 'FAILED'")
    List<ScheduledOperationMetadata> findAllByType(@Param("operationType") ScheduledOperationType operationType);

    @Query(value = "select go from ScheduledOperationMetadata go where operationType in (:operationTypes) and state <> 'FAILED'")
    List<ScheduledOperationMetadata> findAllByTypes(@Param("operationTypes") Iterable<ScheduledOperationType> operationType);

    @Transactional
    @Modifying
    @Query("delete from ScheduledOperationMetadata go where state = 'FAILED' and create_time < :olderThan")
    int deleteByTime(@Param("olderThan") long olderThan);
}
