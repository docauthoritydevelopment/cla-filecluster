package com.cla.filecluster.repository.solr;

import com.cla.common.constants.*;
import com.cla.common.domain.dto.category.DeepPageImpl;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.converters.SolrEntityBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.repository.solr.infra.SolrClientAdapter;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;
import static java.util.stream.Collectors.toList;

/**
 * Repository for operations on cat file core in Solr
 * Created by uri on 23/07/2015.
 */
@Repository
public class SolrFileCatRepository extends SolrRepository<CatFileFieldType, SolrFileEntity> {
    private final static Logger logger = LoggerFactory.getLogger(SolrFileCatRepository.class);

    public static final Pattern EXPIRED_USER_MATCH_PATTERN = Pattern.compile("^S-[\\-0-9]+$");

    @Autowired
    private SequenceIdGenerator sequenceIdGenerator;

    @Value("${solr.catFile.expiredUserKey:ExpiredUser}")
    private String expiredUserKey;

    @Value("${categoryFiles.solr.core:CatFiles}")
    private String catFilesSolrCore;

    private TimeSource timeSource = new TimeSource();

    private ReadWriteLock fileDeleteLock = new ReentrantReadWriteLock();

    @Override
    protected Class<CatFileFieldType> getSchemaType() {
        return CatFileFieldType.class;
    }

    @Override
    protected Class<SolrFileEntity> getEntityType() {
        return SolrFileEntity.class;
    }

    @Override
    protected SolrCoreSchema getIdField() {
        return CatFileFieldType.ID;
    }

    @Override
    protected SolrClientAdapter getSolrClient() {
        return client;
    }

    @Override
    protected String coreName() {
        return catFilesSolrCore;
    }

    //----------------------------- new methods ----------------------------------------------------------------

    public Long getNextAvailableFileId() {
        return sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.CAT_FILE);
    }

    @Override
    public SolrOpResult save(SolrFileEntity entity) {
        SolrOpResult result = SolrOpResult.create();
        try {
            long fileId;
            long contentId;
            if (entity.getId() == null) {
                fileId = getNextAvailableFileId();
                // pre-allocate content metadata ID in order to generate composite ID
                contentId = sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.CONTENT_METADATA);
                entity.setId(FileCatCompositeId.of(contentId, fileId).toString());
                entity.setFileId(fileId);
                entity.setInitialScanDate(new Date());
            } else {
                fileId = entity.getFileId();
                contentId = FileCatCompositeId.of(entity.getId()).getContentId();
            }
            client.addBean(entity);
            result.setId(fileId).setContentId(contentId).setSuccessful(true);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
        return result;
    }

    public void saveAllEntities(List<SolrFileEntity> entities) {
        try {
            entities.forEach(entity -> {
                if (entity.getId() == null) {
                    Long fileId = getNextAvailableFileId();
                    // pre-allocate content metadata ID in order to generate composite ID
                    long contentId = sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.CONTENT_METADATA);
                    entity.setId(FileCatCompositeId.of(contentId, fileId).toString());
                    entity.setFileId(fileId);
                    entity.setInitialScanDate(new Date());
                }
            });
            client.addBeans(entities);

        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed to save %d entities", entities.size()), e);
        }
    }

    /**
     * Get content ID by file ID
     *
     * @param fileId file ID
     * @return content ID
     */
    public Long getContentId(long fileId) {
        SolrQuery query = Query.create()
                .addField(CONTENT_ID)
                .addFilterWithCriteria(FILE_ID, SolrOperator.EQ, fileId)
                .build();

        try {
            QueryResponse response = client.query(query);
            return getSingleFieldValue(response, Long.class, CONTENT_ID);
        } catch (Exception e) {
            logger.error("Failed to get content ID by file ID {}.", fileId);
            return null;
        }
    }


    //-----------------------------end of new methods ----------------------------------------------------------

    public SolrInputDocument toSolrDocument(RootFolder rf, SolrFileEntity file, ContentMetadata contentMetadata) {
        final SolrInputDocument doc = new SolrInputDocument();
        String fullPath = FileNamingUtils.convertPathToUniversalString(file.getFullPath());

        setField(doc, ID, FileCatCompositeId.of(file.getContentId(), file.getFileId()).toString());
        setField(doc, FILE_ID, file.getFileId());
        setField(doc, FULLPATH, fullPath);
        setField(doc, BASE_NAME, file.getBaseName());
        setField(doc, FULL_NAME, file.getFullName());
        setField(doc, FULL_NAME_SEARCH, file.getFullName().toLowerCase());
        setField(doc, FOLDER_NAME, FileNamingUtils.getParentNameOnly(file.getFullName().toLowerCase()));

        if (file.getSortName() != null) {
            setField(doc, SORT_NAME, file.getSortName());
        } else {
            logger.warn("did not set sort name for {} check solr to see issue", file.getId());
        }
        setField(doc, ROOT_FOLDER_ID, file.getRootFolderId());
        Long fsFileSize = contentMetadata == null ? file.getSizeOnMedia() : contentMetadata.getFsFileSize();
        setField(doc, SIZE, fsFileSize);
        setField(doc, TYPE, file.getType());
        if (file.getItemType() != null) {
            setField(doc, ITEM_TYPE, file.getItemType());
        }
        setField(doc, MIME, file.getMime());
        Date fsLastModified = (contentMetadata == null || file.getModificationDate() != null ?
                file.getModificationDate() : contentMetadata.getAppCreationDate());
        setField(doc, LAST_MODIFIED, fsLastModified);
        setField(doc, LAST_ACCESS, file.getAccessDate());
        setField(doc, PROCESSING_STATE, file.getState());
        setField(doc, LAST_RECORD_UPDATE, new Date());
        setField(doc, DELETED, file.isDeleted());
        setFieldIfNotNull(doc, CREATION_DATE, file.getCreationDate());
        setFieldIfNotNull(doc, DELETION_DATE, file.getDeletionDate());
        setFieldIfNotNull(doc, MIME, file.getMime());
        setFieldIfNotNull(doc, INGESTION_STATE, file.getIngestionState());
        setFieldIfNotNull(doc, EXTENSION, StringUtils.lowerCase(file.getExtension())); // TODO Itai: [DAMAIN-5453] pass a proper Locale everywhere StringUtils.lowerCase or (String.toLowerCase()) is used
        setFieldIfNotNull(doc, PACKAGE_TOP_FILE_ID, file.getPackageTopFileId());
        setFieldIfNotNull(doc, FILE_NAME_HASHED, file.getFileNameHashed());
        setFieldIfNotNull(doc, MEDIA_ENTITY_ID, file.getMediaEntityId());
        setFieldIfNotNull(doc, APP_CREATION_DATE, file.getAppCreationDate());
        setFieldIfNotNull(doc, INITIAL_SCAN_DATE, file.getInitialScanDate());
        setFieldIfNotNull(doc, CREATED_ON_DB, file.getCreatedOnDB());
        setFieldIfNotNull(doc, UPDATED_ON_DB, file.getUpdatedOnDB());
        setFieldIfNotNull(doc, SENDER_NAME, file.getSenderName());
        setFieldIfNotNull(doc, SENDER_ADDRESS, StringUtils.lowerCase(file.getSenderAddress()));
        setFieldIfNotNull(doc, SENDER_DOMAIN, StringUtils.lowerCase(file.getSenderDomain()));
        setFieldIfNotNull(doc, RECIPIENTS_NAMES, file.getRecipientsNames());
        setFieldIfNotNull(doc, RECIPIENTS_FULL_ADDRESSES, file.getRecipientsFullAddresses());
        if (file.getRecipientsAddresses() != null) {
            setField(doc, RECIPIENTS_ADDRESSES, file.getRecipientsAddresses().stream().map(StringUtils::lowerCase).collect(toList()));
            if (logger.isTraceEnabled()) {
                logger.trace("toSolrDocument: getRecipientsAddresses()={} doc.getField({})={}",
                        file.getRecipientsAddresses().stream().map(String::valueOf).collect(Collectors.joining(",")),
                        RECIPIENTS_ADDRESSES.getSolrName(),
                        doc.getFieldValues(RECIPIENTS_ADDRESSES.getSolrName()).stream().map(s -> s == null ? "null" : s.toString()).collect(Collectors.joining(",")));
            }
        }
        if (file.getRecipientsDomains() != null)
            setField(doc, RECIPIENTS_DOMAINS, file.getRecipientsDomains().stream().map(StringUtils::lowerCase).collect(toList()));
        setFieldIfNotNull(doc, SENT_DATE, file.getSentDate());

        if (rf != null) {
            setField(doc, MEDIA_TYPE, rf.getMediaType().getValue());
        }

        if (file.getSearchPatternsCounting() != null && !file.getSearchPatternsCounting().isEmpty()) {
            String searchPatternsJson =
                    FileCatUtils.convertSearchPatternCountingSetToJson(file.getSearchPatternsCounting());
            setFieldIfNotNull(doc, EXTRACTED_PATTERNS_IDS, searchPatternsJson);
        }

        setFieldIfNotNull(doc, CONTAINER_IDS, file.getContainerIds());

        setContentMetadataInfo(doc, contentMetadata);

        setField(doc, FOLDER_ID, file.getFolderId());
        setFieldIfNotNull(doc, DOC_STORE_ID, file.getDocStoreId());
        setOwnerInfo(doc, file.getOwner());

        setFieldIfNotNull(doc, EXTERNAL_METADATA, file.getExternalMetadata());
        setFieldIfNotNull(doc, DA_LABELS, file.getDaLabels());
        setFieldIfNotNull(doc, ACTIONS_EXPECTED, file.getActionsExpected());
        setFieldIfNotNull(doc, RAW_METADATA, file.getRawMetadata());
        setFieldIfNotNull(doc, DA_METADATA, file.getDaMetadata());
        setFieldIfNotNull(doc, DA_METADATA_TYPE, file.getDaMetadataType());
        setFieldIfNotNull(doc, GENERAL_METADATA, file.getGeneralMetadata());
        setFieldIfNotNull(doc, ACTION_CONFIRMATION, file.getActionConfirmation());

        if (file.getActionsExpected() != null && file.getLabelProcessDate() == null) {
            setFieldIfNotNull(doc, LABEL_PROCESS_DATE, 0);
        }

        return doc;
    }

    public void deleteByFileIdContentId(Long fileId, Long contentId) {
        if (contentId == null) {
            //Nothing to do
            return;
        }
        deleteById(FileCatCompositeId.of(contentId, fileId).toString());
    }

    public void deleteByCompositeIds(List<String> compositeIds) {
        if (compositeIds == null || compositeIds.isEmpty()) {
            return;
        }
        StringJoiner deleteQueryStringJoiner = new StringJoiner(" ", "id:(", ")");    // query: id:(id1 id2 id3 ...)
        compositeIds.forEach(cid -> deleteQueryStringJoiner.add(String.valueOf(cid)));
        try {
            logger.trace("Delete CatFiles: {}", deleteQueryStringJoiner);
            client.deleteByQuery(deleteQueryStringJoiner.toString());
        } catch (Exception e) {
            throw new RuntimeException("Could not delete CatFiles (" + deleteQueryStringJoiner.toString() + ")", e);
        }
    }

    public void deleteByRootFolderId(Long rootFolderId) {
        try {
            String deleteQueryString = ROOT_FOLDER_ID.filter(rootFolderId);
            logger.trace("Delete by root folder id CatFiles: {}", deleteQueryString);
            client.deleteByQuery(deleteQueryString);
        } catch (Exception e) {
            throw new RuntimeException("Could not delete CatFiles for root folder (" + rootFolderId + ")", e);
        }
    }

    @Deprecated
    public void deleteByFileIds(Collection<Long> fileIds) {
        if (fileIds == null || fileIds.isEmpty()) {
            return;
        }
        if (fileIds.stream().anyMatch(id -> id < 0)) {
            throw new IllegalArgumentException("Got negative file id to delete in: {}" +
                    Arrays.toString(fileIds.toArray()));
        }
        StringJoiner deleteQueryStringJoiner = new StringJoiner(" ", "fileId:(", ")");    // query: id:(id1 id2 id3 ...)
        fileIds.forEach(cid -> deleteQueryStringJoiner.add(String.valueOf(cid)));
        try {
            logger.trace("Delete by FileIds CatFiles: {}", deleteQueryStringJoiner);
            client.deleteByQuery(deleteQueryStringJoiner.toString()); //TODO: scale2 mark as deleted instead of deleting
        } catch (Exception e) {
            throw new RuntimeException("Could not delete CatFiles (" + deleteQueryStringJoiner.toString() + ")", e);
        }
    }

    public void markAsDeleted(Long fileId, boolean commit) {
        if (fileId == null) {
            return;
        }
        String stringQuery = SolrQueryGenerator.generateFilter(Criteria.create(FILE_ID, SolrOperator.EQ, fileId));
        updateByQuery(SolrQueryGenerator.ALL_QUERY, Lists.newArrayList(stringQuery), CatFileFieldType.DELETED, true,
                commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    public void updateUserGroupId(Long fileId, Long contentId, String newGroupId, boolean commit) {
        try {
            SolrInputDocument doc = new SolrInputDocument();
            Map<String, Object> fieldModifier = atomicSetValue(newGroupId);
            String compositeId = FileCatCompositeId.of(contentId, fileId).toString();
            doc.addField(ID.getSolrName(), compositeId);
            doc.addField(USER_GROUP_ID.getSolrName(), fieldModifier);  // add the map as the field value
            setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
            client.add(doc);
            if (commit) {
                commit();
            }
        } catch (Exception e) {
            logger.error("Failed to update category file in Solr", e);
            throw new RuntimeException("Failed to update category file in Solr. File-ID:" + fileId, e);
        }
    }

    public void updateGroupName(List<SolrFileEntity> claFiles, String newGroupName) {
        logger.debug("Update Group Name for {} claFiles to {}", claFiles.size(), newGroupName);
        if (claFiles.size() == 0) {
            return;
        }
        Collection<SolrInputDocument> docs = new ArrayList<>();
        Map<String, Object> fieldModifier = atomicSetValue(newGroupName);
        for (SolrFileEntity claFile : claFiles) {
            SolrInputDocument doc = new SolrInputDocument();
            doc.setField(ID.getSolrName(), claFile.getId());
            doc.addField(GROUP_NAME.getSolrName(), fieldModifier);  // add the map as the field value
            setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
            docs.add(doc);
        }
        try {
            long start = timeSource.currentTimeMillis();
            UpdateResponse updateResponse = client.add(docs, 3000);
            if (updateResponse == null) {
                throw new RuntimeException("Failed to update group name in Solr.");
            }
            long duration = timeSource.millisSince(start);
            logger.debug("Updated Group Name for {} claFiles to {}. It took {} ms. Update Response: {}", claFiles.size(),
                    newGroupName, duration, updateResponse.toString());
        } catch (Exception e) {
            logger.error("Failed to update category files in Solr", e);
            if (e.getCause() != null && e.getCause().getMessage() != null) {
                String message = e.getCause().getMessage();
                if (message.contains("missing required field")) {
                    throw new RuntimeException("Failed to update group name in Solr. " +
                            "At least one file in the group is not in Solr. (" + message + ")", e);
                }
            }
            if (e.getMessage() != null && e.getMessage().contains("missing required field")) {
                String message = e.getCause().getMessage();
                reportOnProblematicClaFile(claFiles, message);
                throw new RuntimeException("Failed to update group name in Solr. " +
                        "At least one file in the group is not in Solr. (" + message + ")", e);
            }
            throw new RuntimeException("Failed to update category files in Solr. " + e.getMessage(), e);
        }
    }

    public List<SolrFileEntity> findByFileIds(Collection<Long> fileIds) {
        return findByFileIds(fileIds, null);
    }

    public List<SolrFileEntity> findByFileIds(Collection<Long> fileIds, List<CatFileFieldType> fields) {
        if (fileIds == null || fileIds.isEmpty()) {
            return new ArrayList<>();
        }

        Query query = Query.create().addFilterWithCriteria(CatFileFieldType.FILE_ID, SolrOperator.IN, fileIds);

        if (fields != null && !fields.isEmpty()) {
            fields.forEach(query::addField);
        }

        return find(query.build());
    }

    /**
     * Parse the message that came back from SOLR and find the problematic docId.
     * Look at the list of claFiles and find that particular document.
     * Print the details of the document.
     *
     * @param claFiles list of cla files
     * @param message  SOLR message
     */
    private void reportOnProblematicClaFile(List<SolrFileEntity> claFiles, String message) {
        //noinspection RegExpRedundantEscape
        Matcher matcher = Pattern.compile(".+\\[doc=(\\d+)\\].+").matcher(message);
        if (matcher.matches()) {
            String docId = matcher.group(1);
            Optional<SolrFileEntity> first = claFiles.stream()
                    .filter(f -> f.getId().equalsIgnoreCase(docId)).findFirst();
            first.ifPresent(claFile -> logger.error("Problem in claFile: {}", claFile));
        }
    }

    public SolrQuery createUpdateTagsQuery(Criteria filterQuery, String objectName, boolean add, String solrKey, CatFileFieldType filterField, boolean filterByField, SolrCommitType commitType) {
        Query query = Query.create();
        query.setRequestHandler(UPDATE_TAGS_REQUEST_HANDLER);
        query.addQuery(filterQuery);
        String operation = add ? "Attach " : "Remove ";
        query.addParam(SolrRequestParams.TAG_OP, add ? SolrFieldModifiers.ADD : SolrFieldModifiers.REMOVE);
        if (filterByField) {
            Criteria additionalFilter = Criteria.create(filterField,
                    (add ? SolrOperator.NE : SolrOperator.EQ), "\"" + solrKey + "\"");

                    //add ? "-" + filterField + ":\"" + solrKey + "\"" : filterField + ":\"" + solrKey + "\"";
            query.addFilterWithCriteria(additionalFilter);
        }
        query.addParam(SolrRequestParams.TAG, solrKey);
        query.addParam(SolrRequestParams.COMMIT_TYPE, commitType.name());

        logger.trace("{} object {} in Solr CatFiles by {} using {} identifier: {}", operation, objectName, filterQuery, filterField, solrKey);
        return query.build();
    }

    public void create(SolrInputDocument doc, String comment, Long fileId) {
        try {
            UpdateResponse add = client.add(doc);
            if (add == null) {
                throw new RuntimeException("Failed to create category file in Solr.");
            }
            softCommitNoWaitFlush();
            logger.trace("Create category file {} ({}) response: {}", fileId, comment, add);
        } catch (Exception e) {
            logger.error("Failed to create category file in Solr", e);
            throw new RuntimeException("Failed to create category file in Solr. File-ID:" + fileId, e);
        }
    }

    public void updateErrorType(SolrInputDocument doc, FileTag fileTag) {
        if (fileTag == null) {
            return;
        }
        Collection<Object> fieldValues = doc.getFieldValues(SCOPED_TAGS.getSolrName());
        Set<Object> updatedValues = fieldValues == null ? Sets.newHashSet() : Sets.newHashSet(fieldValues);
        TagPriority priority = TagPriority.FILE;
        updatedValues.add(AssociationIdUtils.createPriorityScopedTagIdentifier(fileTag, null, priority));
        setField(doc, SCOPED_TAGS, updatedValues);
    }

    public void createCategoryFilesFromDocuments(Collection<SolrInputDocument> documents) {
        if (documents == null || documents.size() == 0) {
            return;
        }
        try {
            logger.debug("Send {} documents to Solr", documents.size());
            UpdateResponse response = client.add(documents);
            if (response == null) {
                throw new RuntimeException("Failed to create files in Solr.");
            }
            logger.debug("Got response from solr: {}", response);
        } catch (Exception e) {
            logger.error("Failed to create category files in Solr", e);
            String listStr = documents.stream().map(d -> d.getField(ID.getSolrName()).toString())
                    .collect(Collectors.joining(","));
            logger.error("Failed file ids: {}", listStr);
            throw new RuntimeException("Failed to create category files in Solr.", e);
        }

    }

    public SolrInputDocument createClearFieldDocument(String compositeId, CatFileFieldType catFileFieldType) {
        Map<String, Object> fieldModifier = atomicSetValue(null);
        return createModifyFieldDocument(compositeId, catFileFieldType, fieldModifier);
    }

    public SolrInputDocument createModifyFieldDocument(String compositeId, CatFileFieldType catFileFieldType, Map<String, Object> fieldModifier) {
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField(ID.getSolrName(), compositeId);
        doc.setField(catFileFieldType.getSolrName(), fieldModifier);
        setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
        return doc;
    }

    public static Object atomicConditionalSetValue(boolean atomicUpdate, Object value) {
        if (atomicUpdate) {
            return atomicSetValue(value);
        }
        return value;
    }

    public static Object atomicConditionalSetValue(boolean atomicUpdate, Object value, SolrFieldOp operation) {
        if (atomicUpdate) {
            return atomicChangeValue(value, operation);
        }
        return value;
    }

    public void updateFilePath(ClaFile claFile, DocFolder folder) {
        String compositeId = FileCatCompositeId.of(claFile.getContentMetadataId(), claFile.getId()).toString();
        updateFilePath(claFile.getFullPath(), claFile.getBaseName(), claFile.getFileName(), claFile.getType().toInt(), folder, compositeId);
    }

    private void updateFilePath(String fullPath, String baseName, String filename, int fileTypeId, DocFolder folder, String compositeId) {
        SolrInputDocument doc = new SolrInputDocument();

        doc.setField(ID.getSolrName(), compositeId);
        doc.setField(FULLPATH.getSolrName(), atomicSetValue(fullPath));
        doc.setField(BASE_NAME.getSolrName(), atomicSetValue(baseName));
        doc.setField(FILE_NAME_HASHED.getSolrName(), atomicSetValue(SolrEntityBuilder.getHashedString(filename)));
        doc.setField(SORT_NAME.getSolrName(), atomicSetValue(FileCatUtils.calcSortName(baseName)));
        doc.setField(FULL_NAME.getSolrName(), atomicSetValue(filename));
        doc.setField(FULL_NAME_SEARCH.getSolrName(), atomicSetValue(filename.toLowerCase()));
        doc.setField(FOLDER_NAME.getSolrName(), atomicSetValue(FileNamingUtils.getParentNameOnly(filename.toLowerCase())));
        doc.setField(TYPE.getSolrName(), atomicSetValue(fileTypeId));
        doc.setField(FOLDER_ID.getSolrName(), atomicSetValue(folder.getId()));
        doc.setField(FOLDER_IDS.getSolrName(), atomicSetValue(folder.getParentsInfo()));
        doc.setField(ROOT_FOLDER_ID.getSolrName(), atomicSetValue(folder.getRootFolderId()));
        doc.setField(UPDATE_DATE.getSolrName(), atomicSetValue(System.currentTimeMillis()));

        try {
            client.add(doc);
            client.commit();
        } catch (Exception e) {
            logger.error("Failed to update CatFile document. file {} id {} type-id {}", fullPath, compositeId, fileTypeId, e);
            throw new RuntimeException(e);
        }
    }

    public SolrInputDocument createUpdateGroupDocument(SolrFileEntity file, FileGroup analysisGroup, FileGroup userGroup, ContentMetadata contentMetadata) {
        final SolrInputDocument doc = new SolrInputDocument();

        String compositeId = FileCatCompositeId.of(file.getContentId(), file.getFileId()).toString();
        doc.setField(ID.getSolrName(), compositeId);

        if (file.getContentId() != null && contentMetadata != null) {

            AnalyzeHint analyzeHint = contentMetadata.getAnalyzeHint();
            if (analyzeHint != null) {
                doc.setField(ANALYSIS_HINT.getSolrName(), atomicSetValue(analyzeHint.toString()));
            }
        }
        if (analysisGroup == null) {
            Map<String, Object> unsetFeildValueMap = atomicSetValue(null);
            doc.setField(CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName(), unsetFeildValueMap);
        } else {
            doc.setField(CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName(), atomicSetValue(analysisGroup.getId()));
        }
        if (userGroup == null) {
            Map<String, Object> unsetFeildValueMap = atomicSetValue(null);
            doc.setField(CatFileFieldType.USER_GROUP_ID.getSolrName(), unsetFeildValueMap);
            doc.setField(CatFileFieldType.GROUP_NAME.getSolrName(), unsetFeildValueMap);
        } else {
            doc.setField(USER_GROUP_ID.getSolrName(), atomicSetValue(userGroup.getId()));
            doc.setField(GROUP_NAME.getSolrName(), atomicSetValue(userGroup.getNameForReport()));
        }
        doc.setField(UPDATE_DATE.getSolrName(), atomicSetValue(System.currentTimeMillis()));
        return doc;
    }

    private void setOwnerInfo(SolrInputDocument doc, final String owner) {
        if (owner != null) {
            String ownerToSet = FileCatUtils.getEmptyDefault(owner);
            setFieldIfNotNull(doc, OWNER_F, ownerToSet);
            String ownerVal = ownerToSet;
            if (EXPIRED_USER_MATCH_PATTERN.matcher(ownerToSet).matches()) {
                ownerVal = expiredUserKey;
            }
            // Aggregate expired users as a single key
            setFieldIfNotNull(doc, OWNER, ownerVal);
            setFieldOnce(doc, LAST_METADATA_CNG_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
            setFieldOnce(doc, UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
        }
    }

    public void setParentFoldersInfo(SolrInputDocument doc, List<String> parentFoldersInfo) {
        if (parentFoldersInfo != null) {
            setFieldIfNotNull(doc, FOLDER_IDS, parentFoldersInfo);
        }
    }

    private void setContentMetadataInfo(SolrInputDocument doc, ContentMetadata contentMetadata) {
        if (contentMetadata != null) {
            setFieldIfNotNull(doc, AUTHOR, contentMetadata.getAuthor());
            setFieldIfNotNull(doc, COMPANY, contentMetadata.getCompany());
            setFieldIfNotNull(doc, SUBJECT, contentMetadata.getSubject());
            setFieldIfNotNull(doc, TITLE, contentMetadata.getTitle());

            if (contentMetadata.getId() != null) {
                setField(doc, CONTENT_ID, contentMetadata.getId());
                setField(doc, CONTENT_CHANGE_DATE, System.currentTimeMillis());
            }

            setFieldIfNotNull(doc, USER_CONTENT_SIGNATURE, contentMetadata.getUserContentSignature());
            setFieldIfNotNull(doc, APP_CREATION_DATE, contentMetadata.getAppCreationDate());
            setFieldIfNotNull(doc, ITEM_COUNT_1, contentMetadata.getItemsCountL1());
            setFieldIfNotNull(doc, ITEM_COUNT_2, contentMetadata.getItemsCountL2());
            setFieldIfNotNull(doc, ITEM_COUNT_3, contentMetadata.getItemsCountL3());
            setFieldIfNotNull(doc, GENERATING_APP, contentMetadata.getGeneratingApp());

            setFieldIfNotNull(doc, KEYWORDS, contentMetadata.getKeywords());
            AnalyzeHint analyzeHint = contentMetadata.getAnalyzeHint();
            if (analyzeHint != null) {
                setField(doc, ANALYSIS_HINT, analyzeHint.toString());
            }
        }
    }

    public void addGroupDetails(SolrInputDocument doc, FileGroup analysisGroup, FileGroup userGroup) {
        String groupName = null;
        if (analysisGroup != null) {
            setField(doc, ANALYSIS_GROUP_ID, analysisGroup.getId());
            groupName = analysisGroup.getNameForReport();
        }
        if (userGroup != null) {
            setField(doc, USER_GROUP_ID, userGroup.getId());
            groupName = userGroup.getNameForReport();
        }
        setFieldIfNotNull(doc, GROUP_NAME, groupName);
    }

    @SuppressWarnings("unchecked")
    public boolean checkIfTagIdentifierExistsForFile(long fileId, String tagIdentifier) {
        SolrDocument categoryFile = findCategoryFile(fileId);
        if (categoryFile == null) {
            throw new RuntimeException("Get File Tags for file " + fileId + ". Unable to find file by fileId in fileCat Solr core");
        }
        List<String> solrTagIds = (List<String>) categoryFile.get("tags");
        if (solrTagIds == null) {
            return false;
        }
        for (String solrTagId : solrTagIds) {
            if (tagIdentifier.equals(solrTagId)) {
                return true;
            }
        }
        return false;
    }

    public SolrDocument findCategoryFile(long fileId) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(FILE_ID, SolrOperator.EQ, fileId));
        try {
            QueryResponse queryResponse = client.query(query.build());
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                return null;
            }
            return queryResponse.getResults().get(0);
        } catch (Exception e) {
            logger.error("Failed to find category file by id - solr exception", e);
            throw new RuntimeException("Failed to find category file by id - solr exception", e);
        }

    }

    public DeepPageImpl<String> listCompositeIdsWithFilter(Criteria filterQuery, int pageSize, String cursorMark) {
        Query query = Query.create();
        query.addSort(ID, Sort.Direction.ASC);
        if (filterQuery != null) {
            query.addFilterWithCriteria(filterQuery);
        }
        if (cursorMark == null) {
            cursorMark = "*";
        }
        query.addField(ID);
        updateQueryWithPageData(cursorMark, pageSize, query);
        try {
            QueryResponse queryResponse = client.query(query.build(), SolrRequest.METHOD.POST);
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                logger.debug("Got 0 results for list of files in category. Solr Query: {}", query);
            }
            List<String> content = results.stream().map(doc -> (String) doc.get(ID.getSolrName())).collect(toList());
            long numFound = queryResponse.getResults().getNumFound();
            String nextCursorMark = queryResponse.getNextCursorMark();
            return new DeepPageImpl<>(nextCursorMark, pageSize, content, numFound);
        } catch (Exception e) {
            logger.error("Failed to list files of category - solr exception", e);
            throw new RuntimeException("Failed to list files of category - solr exception", e);
        }
    }

    public Map<Long, Long> getFoldersAllFileNumber(Collection<DocFolder> folders) {
        Map<String, Long> foldersIdentifiers = folders.stream().collect(Collectors.toMap(f -> f.getDepthFromRoot() + "." + f.getId(), DocFolder::getId));

        Query query = Query.create()
                .addFacetField(FOLDER_IDS)
                .setFacetMinCount(1)
                .setRows(0);

        foldersIdentifiers.keySet().forEach(k -> query.addFacetQuery(Criteria.create(FOLDER_IDS, SolrOperator.EQ, k)));

        query.addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, "false"));

        try {
            Map<Long, Long> result = new HashMap<>();
            QueryResponse queryResponse = client.query(query.build(), SolrRequest.METHOD.POST);

            if (queryResponse.getFacetQuery() != null && !queryResponse.getFacetQuery().isEmpty()) {
                queryResponse.getFacetQuery().forEach((k,v) -> {
                    String identifier = k.substring(k.indexOf(":") +1);
                    Long id = foldersIdentifiers.get(identifier);
                    result.put(id, new Long(v));
                });
            }

            return result;
        } catch (Exception e) {
            logger.error("Failed to count catFiles by folder - solr exception (" + query + ")", e);
            throw new RuntimeException("IO Failure to count cat files by folder - solr exception", e);
        }
    }

    public Map<Long, Long> getFoldersDirectFileNumber(Collection<Long> folderIds) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(FOLDER_ID, SolrOperator.IN, folderIds))
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, "false"))
                .addFacetField(FOLDER_ID)
                .setFacetMinCount(1)
                .setFacetLimit(folderIds.size() + 1)
                .setRows(0);

        try {
            Map<Long, Long> result = new HashMap<>();
            QueryResponse queryResponse = client.query(query.build(), SolrRequest.METHOD.POST);
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    String folderId = c.getName();
                    long count = c.getCount();
                    result.put(Long.parseLong(folderId), count);
                }
            }

            return result;
        } catch (Exception e) {
            logger.error("Failed to count catFiles by folder - solr exception (" + query + ")", e);
            throw new RuntimeException("IO Failure to count cat files by folder - solr exception", e);
        }
    }

    public DeepPageImpl<SolrDocument> listFilesFieldWithFilter(Criteria filterQuery, CatFileFieldType catFileFieldType, int pageSize, String cursorMark) {
        Query query = Query.create();
        query.addSort(ID, Sort.Direction.ASC);
        if (filterQuery != null) {
            query.addFilterWithCriteria(filterQuery);
        }
        if (cursorMark == null) {
            cursorMark = "*";
        }
        query.addField(ID).addField(FILE_ID).addField(catFileFieldType);
        updateQueryWithPageData(cursorMark, pageSize, query);
        try {
            QueryResponse queryResponse = client.query(query.build(), SolrRequest.METHOD.POST);
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                logger.debug("Got 0 results for list of files in category. Solr Query: {}", query);
            }
            List<SolrDocument> content = new ArrayList<>(results);
            long numFound = queryResponse.getResults().getNumFound();
            String nextCursorMark = queryResponse.getNextCursorMark();
            return new DeepPageImpl<>(nextCursorMark, pageSize, content, numFound);
        } catch (Exception e) {
            logger.error("Failed to list files of category - solr exception", e);
            throw new RuntimeException("Failed to list files of category - solr exception", e);
        }
    }

    public Page<SolrDocument> listFilesWithFilter(PageRequest pageRequest, Criteria filterQuery) {
        Query query = Query.create();

        if (filterQuery != null) {
            query.addFilterWithCriteria(filterQuery);
        }
        updateQueryWithPageData(pageRequest, query);
        try {
            QueryResponse queryResponse = client.query(query.build(), SolrRequest.METHOD.POST);
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                logger.debug("Got 0 results for list of files in category. Solr Query: {}", query.build().getQuery());
            }
            return new PageImpl<>(results, pageRequest, queryResponse.getResults().getNumFound());
        } catch (Exception e) {
            logger.error("Failed to list files of category - solr exception", e);
            throw new RuntimeException("Failed to list files of category - solr exception", e);
        }
    }

    private void updateQueryWithPageData(PageRequest pageRequest, Query query) {
        query.setRows(pageRequest.getPageSize());
        //TODO - support deep paging
        if (pageRequest.getPageNumber() > 0) {
            query.setStart(pageRequest.getPageNumber() * pageRequest.getPageSize());
        } else if (pageRequest.getOffset() > 0) {
            query.setStart((int) pageRequest.getOffset());
        }
    }

    private void updateQueryWithPageData(String cursorMark, int pageSize, Query query) {
        query.setRows(pageSize);
        query.setCursorMark(cursorMark);
    }

    /**
     * Update the catFiles with the list of contentIds to the first content id instead.
     * This rewrites all those catFiles
     *
     * @param contentMetadata first content
     * @param contentIds      target content IDs
     */
    public void mergeFilesToSingleContentId(ContentMetadata contentMetadata, List<Long> contentIds) {
        for (Long contentId : contentIds) {
            moveFilesWithContentId(contentMetadata, contentId);
        }
    }

    private void moveFilesWithContentId(ContentMetadata contentMetadata, Long contentId) {
        logger.debug("Move files with contentId {} in catFile to contentId {}", contentId, contentMetadata.getId());
        try {
            Query solrQuery = Query.create().addFilterWithCriteria(Criteria.create(CONTENT_ID, SolrOperator.EQ, contentId));
            QueryResponse queryResponse = client.query(solrQuery.build(), SolrRequest.METHOD.POST);
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                logger.debug("No documents to move");
                return;
            }
            List<String> compositeIds = results.stream().map(f -> (String) f.get(ID.getSolrName())).collect(toList());
            logger.debug("Deleting {} fileCat documents", compositeIds.size());
            deleteByCompositeIds(compositeIds);
            logger.debug("Recreating records");
            List<SolrInputDocument> newDocuments = Lists.newArrayList();
            for (SolrDocument document : results) {
                long fileId = (Long) document.get(FILE_ID.getSolrName());
                SolrInputDocument newDocument = new SolrInputDocument();
                for (Map.Entry<String, Object> entry : document.entrySet()) {
                    if (entry.getKey().startsWith("_")) {
                        continue; // Internal Solr field
                    }
                    newDocument.setField(entry.getKey(), entry.getValue());
                }
                newDocument.setField(ID.getSolrName(), FileCatCompositeId.of(contentMetadata.getId(), fileId).toString());
                newDocument.setField(CONTENT_ID.getSolrName(), contentMetadata.getId());
                newDocument.setField(ANALYSIS_GROUP_ID.getSolrName(), contentMetadata.getAnalysisGroupId());
                newDocument.setField(USER_GROUP_ID.getSolrName(), contentMetadata.getUserGroupId());
                newDocument.setField(PROCESSING_STATE.getSolrName(), contentMetadata.getState().name());
                newDocument.setField(CONTENT_CHANGE_DATE.getSolrName(), System.currentTimeMillis());
                newDocument.setField(UPDATE_DATE.getSolrName(), System.currentTimeMillis());

                if (contentMetadata.getAppCreationDate() != null) {
                    newDocument.setField(APP_CREATION_DATE.getSolrName(), contentMetadata.getAppCreationDate());
                }

                newDocuments.add(newDocument);
            }
            UpdateResponse add = client.add(newDocuments, COMMIT_WITHIN_MS);
            if (add == null) {
                throw new RuntimeException("Failed to move files with contentId in Solr.");
            }
            logger.debug("Move files in catFile to new ContentId Got response {}", add);
        } catch (Exception e) {
            logger.error("Failed to move files with contentId {} to contentId {}", contentId, contentMetadata.getId(), e);
            throw new RuntimeException("Failed to move files with contentId " + contentId
                    + " to contentId " + contentMetadata.getId(), e);
        }
    }

    public boolean docTypeFileExists(long fileId, DocTypeDto docType, Long scopeId) {
        try {
            String docTypeIdentifier = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(docType.getDocTypeIdentifier(), scopeId, TagPriority.FILE) + "*";

            Query solrQuery = Query.create()
                    .addFilterWithCriteria(Criteria.create(FILE_ID, SolrOperator.EQ, fileId))
                    .addFilterWithCriteria(Criteria.create(SCOPED_TAGS, SolrOperator.EQ, docTypeIdentifier));

            QueryResponse queryResponse = client.query(solrQuery.build(), SolrRequest.METHOD.POST);
            return queryResponse.getResults().size() > 0;
        } catch (Exception e) {
            logger.error("Failed to check whether doc type " + docType.getId() + " exists", e);
            throw new RuntimeException("Failed to find category file by id - solr exception", e);
        }
    }

    public SolrFileEntity getById(Long fileId) {
        Optional<SolrFileEntity> entity = getOneByFilterCriterias(Criteria.create(FILE_ID, SolrOperator.EQ, fileId));
        return entity.orElse(null);
    }

    public void updateByQuery(String query, CatFileFieldType field, Object value, SolrCommitType commitType) {
        updateByQuery(query, null, field, value, commitType);
    }

    public void updateByQuery(String query, List<String> filters, CatFileFieldType field, Object value, SolrCommitType commitType) {
        updateByQuery(query, filters, field, value, commitType, SolrFieldOp.SET);
    }

    private void updateByQuery(String query, List<String> filters, CatFileFieldType field, Object value, SolrCommitType commitType, SolrFieldOp op) {
        updateFieldByQueryMultiFields(new String[]{field.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{value == null ? null : String.valueOf(value), String.valueOf(System.currentTimeMillis())}, op.getOpName(),
                query, filters, commitType == null ? SolrCommitType.SOFT_NOWAIT : commitType);
    }

    public Integer updateFieldBySolrSpecification(SolrSpecification solrSpecification, CatFileFieldType field,
                                                  SolrFieldOp operation, String value, SolrCommitType commitType) {
        try {
            SolrQuery solrQuery = solrSpecQueryBuilder.buildSolrQuery(solrSpecification, null);
            return updateFieldByQueryMultiFields(new String[]{field.getSolrName(), UPDATE_DATE.getSolrName()},
                    new String[]{value == null ? null : String.valueOf(value), String.valueOf(System.currentTimeMillis())}, operation.getOpName(),
                    solrQuery, null, commitType == null ? SolrCommitType.SOFT_NOWAIT : commitType);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update field " + field + " in category files in Solr to value " + value, e);
        }
    }

    public void acquireFileDeleteWriteLock() {
        fileDeleteLock.writeLock().lock();
    }

    public void releaseFileDeleteWriteLock() {
        fileDeleteLock.writeLock().unlock();
    }

    public void acquireFileDeleteReadLock() {
        fileDeleteLock.readLock().lock();
    }

    public void releaseFileDeleteReadLock() {
        fileDeleteLock.readLock().unlock();
    }
}