package com.cla.filecluster.repository.jpa;

import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.repository.jpa.security.RoleRepository;
import com.cla.filecluster.service.actions.ActionsAppService;
import com.cla.filecluster.service.categories.FileTypeCategoryService;
import com.cla.filecluster.service.communication.EmailConfigurationService;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.filecluster.service.date.DateRangeAppService;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.extractor.pattern.SearchPatternAppService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.cla.filecluster.service.filter.SavedFilterService;
import com.cla.filecluster.service.license.LicenseAppService;
import com.cla.filecluster.service.license.TrackerAppService;
import com.cla.filecluster.service.report.TrendChartAppService;
import com.cla.filecluster.service.report.UserViewAndFilterService;
import com.cla.filecluster.service.report.UserViewDataService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.security.UserAppService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.security.WorkGroupService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.service.upgrade.UpgradeSchemaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

@Component
@Transactional
public class DataPopulator {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserAppService userAppService;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String hibernateDdlAuto;

    @Value("${defaultAccountName:default-account}")
    private String defaultAccountName;

    @Value("${subprocess:false}")
    private boolean subProcess;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private BizListAppService bizListAppService;

    @Autowired
    private SavedFilterService savedFilterService;

    @Autowired
    private ActionsAppService actionsAppService;

  @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private DateRangeAppService dateRangeAppService;

    @Autowired
    private SearchPatternAppService searchPatternAppService;

    @Autowired
    private TrendChartAppService trendChartAppService;

    private final static Logger logger = LoggerFactory.getLogger(DataPopulator.class);

    @Autowired
    UpgradeSchemaService upgradeSchemaService;

    @Autowired
    private LicenseAppService licenseAppService;

    @Autowired
    private TrackerAppService trackerAppService;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private EmailConfigurationService emailConfigurationService;

    @Autowired
    private WorkGroupService workGroupService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserViewAndFilterService userViewAndFilterService;

    @Autowired
    private FileTypeCategoryService fileTypeCategoryService;

    @PostConstruct
    private void init() {
        
        if (hibernateDdlAuto.contains("create")) {
            logger.info("Fresh database - populating base data in database...");
            userAppService.populateDefaultRoles();
            userAppService.populateDefaultBizRoles();
            User accountsAdmin = userAppService.populateDefaultAccountUsers();

            workGroupService.populateDefaultWorkGroups();

            licenseAppService.importDefaultLicense();

            logger.info("Populate default schedule groups");
            scheduleAppService.createDefaultScheduleData();

            docStoreService.createDefaultDocStore();
            logger.info("Finished populating base data in database.");

            logger.info("Populate EntityLists.");
            bizListAppService.createDefaultBizList();

            logger.info("Populate File Tag Types");
            fileTagTypeService.loadDefaultTagTypes(accountsAdmin);

            logger.info("Populate Default Doc Types");
            docTypeService.loadDefaultDocTypeTemplate();

            logger.info("Populate Default Department");
            departmentService.loadDefaultDepartmentTemplate();

            actionsAppService.loadDefaultActionConfiguration();

            logger.info("Populate Default saved filters");
            savedFilterService.loadDefaultSavedFiltersFile();

            dateRangeAppService.populateDefaultRangePartitions();

            searchPatternAppService.populateDefaultSearchPatterns();

            trendChartAppService.populateDefaultTrendChartsAndScheduler(accountsAdmin);

            emailConfigurationService.initDefaultSettings();

            componentsAppService.createDefaultComponents();

            customerDataCenterService.createDefaultComponents();

            userViewAndFilterService.populateDefaultUserViewData();

            //userViewAndFilterService.createDefaultAnalyticsDashboard();

            logger.info("Populate extensions");
            fileTypeCategoryService.loadDefaultExtensions();

        } else {

            if (subProcess) {
                logger.debug("Skip dataPopulator upgrade scripts");
                return;
            }
            //Check for upgrade scripts
            checkForUpgradeScripts();
            //Reload all actions
//            try {
//                actionsAppService.loadDefaultActionConfiguration();
//            } catch (Exception e) {
//                logger.error("Failed to reload default action configurations",e);
//            }
        }
    }

    private void checkForUpgradeScripts() {
        logger.info("checkForUpgradeScripts");
//        try {
//            SchemaUpgradeState upgradeSchemaStatus = upgradeSchemaService.getUpgradeSchemaStatus(40);
//            if (SchemaUpgradeState.NOT_FOUND.equals(upgradeSchemaStatus)) {
//                executionManagerService.startAsyncExecution(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            logger.info("Execute upgrade script 40");
//                            long executionTime = upgradeSchemaService.executeScript40();
//                            upgradeSchemaService.markUpgradeSchema(40, SchemaUpgradeState.SUCCESS, "superGroups", executionTime);
//                        } catch (Exception e) {
//                            logger.error("Failed to execute Upgade Script 40 (java): " + e.getMessage(), e);
//                        }
//                    }
//                });
//
//            }
//        } catch (Exception e) {
//            logger.error("FATAL ERROR - Failed to check for upgrade scripts", e);
//        }

    }

}
