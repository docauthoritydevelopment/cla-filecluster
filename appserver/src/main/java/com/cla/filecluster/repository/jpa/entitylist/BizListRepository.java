package com.cla.filecluster.repository.jpa.entitylist;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

/**
 * Created by uri on 20/08/2015.
 */
public interface BizListRepository extends JpaRepository<BizList, Long>, JpaSpecificationExecutor<BizList> {

    @Query(value = "select el from BizList el where el.name = :name")
    BizList findByName(@Param("name") String name);

    @Query(value = "select el from BizList el where el.active = :active")
    List<BizList> findActiveBizLists(@Param("active") Boolean active);


    @Query(value = "select el from BizList el")
    List<BizList> find();

    @Query(value = "select el from BizList el where el.id in (:ids)")
    List<BizList> findIds(@Param("ids") List<Long> ids);

    @Query(value = "select el from BizList el where el.bizListItemType = :bizListItemType")
    List<BizList> findByType(@Param("bizListItemType") BizListItemType bizListItemType);

    @Modifying
    @Query( "update BizList el" +
            " set el.lastSuccessfulRunDate = :lastSuccessfulRunDate" +
            " where  el.id = :bizListId")
    int updateLastSuccessfulRunDate(@Param("bizListId") long bizListId, @Param("lastSuccessfulRunDate") long lastSuccessfulRunDate);
}
