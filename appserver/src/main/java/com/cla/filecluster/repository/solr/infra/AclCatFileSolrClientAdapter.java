package com.cla.filecluster.repository.solr.infra;

import com.cla.filecluster.domain.dto.AccessState;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.util.PermissionCheckUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;

import java.util.*;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.filecluster.repository.solr.infra.SolrRepositoryUtils.createInClause;

/**
 * Cat File core SOLR client adapter, which enforces ACLs, i.e. adds ACL-based filters to SOLR queries
 */
public class AclCatFileSolrClientAdapter implements SolrClientAdapter {
    private static Logger logger = LoggerFactory.getLogger(AclCatFileSolrClientAdapter.class);

    private final SolrClientAdapter solrClientDelegate;

    private SharePermissionService sharePermissionService;

    // permission tokens that allow access to any user
    // such as \Everybody
    private List<String> genericTokens;

    private boolean caseSensitiveAcls;

    private long readMaskSharePermission;

    AclCatFileSolrClientAdapter(SolrClientAdapter solrClientDelegate, SharePermissionService sharePermissionService,
                                List<String> genericTokens, boolean caseSensitiveAcls, long readMaskSharePermission) {
        this.solrClientDelegate = solrClientDelegate;
        this.genericTokens = genericTokens;
        this.caseSensitiveAcls = caseSensitiveAcls;
        this.readMaskSharePermission = readMaskSharePermission;
        this.sharePermissionService = sharePermissionService;
    }

    @Override
    public SolrClient getUnderyingClient() {
        return solrClientDelegate.getUnderyingClient();
    }

    @Override
    public boolean isSolrCloudClient() {
        return solrClientDelegate.isSolrCloudClient();
    }

    /**
     * Modify query according to acces state
     * @param query - query
     * @param <T>   - type of the input (SolrQuery or String)
     * @return modified query
     */
    private <T> T modifyQuery(T query) {
        AccessState accessState = PermissionCheckUtils.getCurrentFileAccessState(
                sharePermissionService.getAllSharePermissions(),
                genericTokens, caseSensitiveAcls, readMaskSharePermission);

        if (accessState.isAccessDenied()){
            throw new RuntimeException("Access denied.");
        }
        // for universal access - no need to modify the query
        if (accessState.isAllowAllFiles()){
            return query;
        }
        if (logger.isTraceEnabled()) {
            logger.trace("adding tokens to filterQuery: {}", accessState.getAccessTokens());
        }

        if (query instanceof String){
            //noinspection unchecked
            return (T)getModifiedReadQuery((String)query, accessState);
        }
        else{
            modifyReadQuery((SolrQuery)query, accessState);
            return query;
        }
    }

    private void modifyReadQuery(SolrQuery query, AccessState accessState) {

        String aclReadAccessTokensClause = createInClause(aclReadFieldName(), accessState.getAccessTokens());
        String aclDenyReadAccessTokensClause = createInClause(aclDeniedReadFieldName(), accessState.getAccessTokens());

        boolean bizRolesAllowPermitted = accessState.isAllowPermittedFiles();
        Set<String> allowAllFuncRoles = accessState.getAllowAllFuncRoles();
        Set<String> allowPermittedFuncRoles = accessState.getAllowPermittedFuncRoles();

        String rootFolderDeniedQuery = null;
        if (!accessState.getDeniedRootFolders().isEmpty()) {
            rootFolderDeniedQuery = ROOT_FOLDER_ID.notInQuery(accessState.getDeniedRootFolders());
        }

        // if BizRoles allow ALL permitted files, and there are no functional roles that allow ALL files
        // modify the query and return, because BizRole allow ALL permitted wins over FuncRole allow ALL permitted
        if (bizRolesAllowPermitted && allowAllFuncRoles.isEmpty()){
            // add "allow" clause
            query.addFilterQuery(aclReadAccessTokensClause);
            // add "deny" clause
            query.addFilterQuery("-" + aclDenyReadAccessTokensClause);

            if (rootFolderDeniedQuery != null) {
                query.addFilterQuery(rootFolderDeniedQuery);
            }

            return;
        }

        if (allowAllFuncRoles.isEmpty() && allowPermittedFuncRoles.isEmpty()){
            // no business roles and no functional roles to allow viewing any files
            throw new AccessDeniedException("Access denied.");
        }

        // if BizRoles allow access to permitted files and there are FuncRoles that allow access to ALL files
        // then build query combined of allow permitted on all files and allow all files with FuncRoles
        if (bizRolesAllowPermitted){
            // where ((ACL_READ in accessTokens) AND (ACL_DENIED_READ not in accessTokens))
            //    OR (tag2 contains one of allowAllFuncRoles)
            query.addFilterQuery("(" +
                    aclReadAccessTokensClause +
                    " AND -" + aclDenyReadAccessTokensClause +
                            (rootFolderDeniedQuery != null ?
                    " AND " + rootFolderDeniedQuery : "") + ")" +
                    " OR (" + createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowAllFuncRoles, true) + ")");
            return;
        }

        // if both kinds of functional roles are present
        if (!allowAllFuncRoles.isEmpty() && !allowPermittedFuncRoles.isEmpty()) {
            String filterQuery = createFuncRoleFilters(accessState);
            query.addFilterQuery(filterQuery);
            logger.trace("Modified SOLR query: {}", query.toString());
            return;
        }

        if (!allowAllFuncRoles.isEmpty()) {
            //where (tag2 contains one of allowAllFuncRoles)
            query.addFilterQuery(
                    createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowAllFuncRoles, true)
            );
        }
        else {
            // where (tag2 contains one of  allowPermittedFuncRoles) AND
            //       (ACL_READ in accessTokens) AND
            //       (ACL_DENIED_READ not in accessTokens))
            query.addFilterQuery(createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowPermittedFuncRoles, true));
            query.addFilterQuery(aclReadAccessTokensClause);
            query.addFilterQuery("-" + aclDenyReadAccessTokensClause);
            if (rootFolderDeniedQuery != null) {
                query.addFilterQuery(rootFolderDeniedQuery);
            }
        }

        logger.trace("Modified SOLR query: {}", query.toString());
    }

    private String getModifiedReadQuery(final String query, AccessState accessState) {

        // for global ViewPermittedFiles access - modify the query to consider ACLs
        String newQuery = query;

        String aclReadAccessTokensClause = createInClause(aclReadFieldName(), accessState.getAccessTokens());
        String aclDenyReadAccessTokensClause = createInClause(aclDeniedReadFieldName(), accessState.getAccessTokens());

        boolean bizRolesAllowPermitted = accessState.isAllowPermittedFiles();
        Set<String> allowAllFuncRoles = accessState.getAllowAllFuncRoles();
        Set<String> allowPermittedFuncRoles = accessState.getAllowPermittedFuncRoles();

        String rootFolderDeniedQuery = null;
        if (!accessState.getDeniedRootFolders().isEmpty()) {
            rootFolderDeniedQuery = ROOT_FOLDER_ID.notInQuery(accessState.getDeniedRootFolders());
        }

        // if BizRoles allow ALL permitted files, and there are no functional roles that allow ALL files
        // modify the query and return, because BizRole allow ALL permitted wins over FuncRole allow ALL permitted
        if (bizRolesAllowPermitted && allowAllFuncRoles.isEmpty()) {
            // add "allow" clause
            newQuery += "&fq=" + aclReadAccessTokensClause;
            // add "deny" clause
            newQuery += "&fq=-" + aclDenyReadAccessTokensClause;

            if (rootFolderDeniedQuery != null) {
                newQuery += "&fq=" + rootFolderDeniedQuery;
            }

            return newQuery;
        }

        if (allowAllFuncRoles.isEmpty() && allowPermittedFuncRoles.isEmpty()) {
            // no business roles and no functional roles to allow viewing any files
            throw new AccessDeniedException("Access denied.");
        }

        // if BizRoles allow access to permitted files and there are FuncRoles that allow access to ALL files
        // then build query combined of allow permitted on all files and allow all files with FuncRoles
        if (bizRolesAllowPermitted){
            // where ((ACL_READ in accessTokens) AND (ACL_DENIED_READ not in accessTokens))
            //    OR (tag2 contains one of allowAllFuncRoles)
            newQuery += "&fq=(" +
                    aclReadAccessTokensClause +
                    " AND -" + aclDenyReadAccessTokensClause +
                    (rootFolderDeniedQuery != null ?
                    " AND " + rootFolderDeniedQuery : "") + ")" +
                    " OR (" + createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowAllFuncRoles, true) + ")";
            return newQuery;
        }

        // if both kinds of functional roles are present, but no biz roles
        if (!allowAllFuncRoles.isEmpty() && !allowPermittedFuncRoles.isEmpty()) {
            newQuery += "&fq=" +  createFuncRoleFilters(accessState);
            return newQuery;
        }

        if (!allowAllFuncRoles.isEmpty()) {
            //where (tag2 contains one of allowAllFuncRoles)
            newQuery += "&fq=" + createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowAllFuncRoles, true);
            return newQuery;
        }
        else{
            // where (tag2 contains one of  allowPermittedFuncRoles) AND
            //       (ACL_READ in accessTokens) AND
            //       (ACL_DENIED_READ not in accessTokens))
            newQuery += "&fq=" + createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowPermittedFuncRoles, true);
            newQuery += "&fq=" + aclReadAccessTokensClause;
            newQuery += "&fq=-" + aclDenyReadAccessTokensClause;

            if (rootFolderDeniedQuery != null) {
                newQuery += "&fq=" + rootFolderDeniedQuery;
            }
            return newQuery;
        }
    }

    /**
     * where (tag2 contains one of allowAllFuncRoles) OR
     *              ((tag2 contains one of  allowPermittedFuncRoles)
     *               AND (ACL_READ in accessTokens)
     *               AND (ACL_DENIED_READ not in accessTokens))
     * @param accessState summary of access parameters
     * @return filter query string
     */
    private String createFuncRoleFilters(AccessState accessState) {
        Set<String> allowAllFuncRoles = accessState.getAllowAllFuncRoles();
        Set<String> allowPermittedFuncRoles = accessState.getAllowPermittedFuncRoles();
        Set<String> accessTokens = accessState.getAccessTokens();

        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder
                .append(createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowAllFuncRoles, true))
                .append(" OR (")
                .append(createInClause(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), allowPermittedFuncRoles, true));


        if (accessTokens.size() > 0) {
            String aclReadAccessTokensClause = createInClause(aclReadFieldName(), accessTokens);
            String aclDenyReadAccessTokensClause = createInClause(aclDeniedReadFieldName(), accessTokens);
            resultBuilder.append(" ) OR ( ").append(aclReadAccessTokensClause)
                    .append(" AND -").append(aclDenyReadAccessTokensClause);
        }

        if (!accessState.getDeniedRootFolders().isEmpty()) {
            resultBuilder.append(" AND ")
                    .append(ROOT_FOLDER_ID.notInQuery(accessState.getDeniedRootFolders()));
        }

        resultBuilder.append(")");
        return resultBuilder.toString();
    }

    private String aclReadFieldName() {
        return caseSensitiveAcls ? ACL_READ.getSolrName() : SEARCH_ACL_READ.getSolrName();
    }

    private String aclDeniedReadFieldName() {
        return caseSensitiveAcls ? ACL_DENIED_READ.getSolrName() : SEARCH_ACL_DENIED_READ.getSolrName();
    }

    //------------------------------------------------------------------------------
    // SOLR Interface methods
    //------------------------------------------------------------------------------

    @Override
    public QueryResponse query(SolrQuery query, SolrRequest.METHOD method) throws Exception {
        modifyQuery(query);
        if (logger.isTraceEnabled()) {
            logger.trace("query({}, {})", query, method);
        }
        return solrClientDelegate.query(query, method);
    }

    @Override
    public QueryResponse query(SolrQuery query) throws Exception {
        modifyQuery(query);
        if (logger.isTraceEnabled()) {
            logger.trace("query({})", query);
        }
        return solrClientDelegate.query(query);
    }

    @Override
    public QueryResponse query(SolrQuery query, SolrRequest.METHOD method, int retryCount) throws Exception {
        modifyQuery(query);
        if (logger.isTraceEnabled()) {
            logger.trace("query({}, {}, retry: {})", query, method, retryCount);
        }
        return solrClientDelegate.query(query, method, retryCount);
    }

    @Override
    public QueryResponse query(SolrQuery query, int retryCount) throws Exception {
        modifyQuery(query);
        if (logger.isTraceEnabled()) {
            logger.trace("query({}, retry: {})", query, retryCount);
        }
        return solrClientDelegate.query(query, retryCount);
    }

    @Override
    public UpdateResponse deleteByQuery(String query) throws Exception {
        String newQuery = modifyQuery(query);
        if (logger.isTraceEnabled()) {
            logger.trace("deleteByQuery({})", query);
        }
        return solrClientDelegate.deleteByQuery(newQuery);
    }

    @Override
    public UpdateResponse deleteById(String id) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("deleteById({})", id);
        }
        return solrClientDelegate.deleteById(id);
    }

    @Override
    public UpdateResponse deleteByIds(List<String> ids) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("deleteByIds({})", ids);
        }
        return solrClientDelegate.deleteByIds(ids);
    }

    @Override
    public UpdateResponse commit() throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("commit()");
        }
        return solrClientDelegate.commit();
    }

    @Override
    public UpdateResponse commit(String collection, boolean waitFlush, boolean waitSearcher) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("commit({}, {}, {})", collection, waitFlush, waitSearcher);
        }
        return solrClientDelegate.commit(collection, waitFlush, waitSearcher);
    }

    @Override
    public UpdateResponse commit(boolean waitFlush, boolean waitSearcher, boolean softCommit) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("commit({}, {}, {})", waitFlush, waitSearcher, softCommit);
        }
        return solrClientDelegate.commit(waitFlush, waitSearcher, softCommit);
    }

    @Override
    public UpdateResponse optimize(boolean waitFlush, boolean waitSearcher, int maxSegments) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("optimize({}, {}, {})", waitFlush, waitSearcher, maxSegments);
        }
        return solrClientDelegate.optimize(waitFlush, waitSearcher, maxSegments);
    }

    @Override
    public UpdateResponse optimize() throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("optimize()");
        }
        return solrClientDelegate.optimize();
    }

    @Override
    public SolrPingResponse ping() throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("ping()");
        }
        return solrClientDelegate.ping();
    }

    @Override
    public UpdateResponse add(SolrInputDocument doc) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("add({})", doc);
        }
        return solrClientDelegate.add(doc);
    }

    @Override
    public UpdateResponse add(SolrInputDocument doc, boolean shouldRetry) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("add({})", doc);
        }
        return solrClientDelegate.add(doc, shouldRetry);
    }

    @Override
    public UpdateResponse add(SolrInputDocument doc, int commitWithinMs) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("add({}, {})", doc, commitWithinMs);
        }
        return solrClientDelegate.add(doc, commitWithinMs);
    }

    @Override
    public UpdateResponse add(Collection<SolrInputDocument> docs, int commitWithinMs) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("add({}, {})", docs, commitWithinMs);
        }
        return solrClientDelegate.add(docs, commitWithinMs);
    }

    @Override
    public UpdateResponse add(Collection<SolrInputDocument> docs) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("add({})", docs);
        }
        return solrClientDelegate.add(docs);
    }

    @Override
    public UpdateResponse addBean(Object obj) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("addBean({})", obj.getClass());
        }
        return solrClientDelegate.addBean(obj);
    }

    @Override
    public UpdateResponse addBean(Object obj, int commitWithinMs) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("addBean({}, {})", obj.getClass(), commitWithinMs);
        }
        return solrClientDelegate.addBean(obj, commitWithinMs);
    }

    public QueryResponse queryNonSecure(SolrQuery query, SolrRequest.METHOD method) throws Exception{
        return solrClientDelegate.query(query, method);
    }

    @Override
    public UpdateResponse addBeans(List<?> entities) throws Exception {
        return solrClientDelegate.addBeans(entities);
    }

    @Override
    public UpdateResponse addBeans(List<?> entities, int commitWithinMs) throws Exception {
        return solrClientDelegate.addBeans(entities,commitWithinMs);
    }
}
