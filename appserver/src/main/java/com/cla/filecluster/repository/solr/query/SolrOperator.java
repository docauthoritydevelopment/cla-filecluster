package com.cla.filecluster.repository.solr.query;

public enum SolrOperator {
    EQ(""),
    NE(""),
    GT(""),
    GTE(""),
    LT(""),
    LTE(""),
    IN(""),
    NOT_IN(""),
    IN_RANGE(""),
    PRESENT(""),
    MISSING(""),
    EXISTS(""),
    // logical operators
    AND("AND"),
    OR("OR"),
    NOT("NOT"),
    // Set or replace the field value(s) with the specified value(s),
    // or remove the values if 'null' or empty list is specified as the new value.
    // May be specified as a single value, or as a list for multiValued fields
    SET("set"),

    // Adds the specified values to a multiValued field.
    // May be specified as a single value, or as a list.
    ADD("add"),

    // Removes (all occurrences of) the specified values from a multiValued field.
    // May be specified as a single value, or as a list.
    REMOVE("remove"),

    // Removes all occurrences of the specified regex from a multiValued field.
    // May be specified as a single value, or as a list.
    REMOVE_REGEX("removeregex"),

    // Increments a numeric value by a specific amount.
    // Must be specified as a single numeric value.
    INC("inc");

    private String opName;

    SolrOperator(String opName) {
        this.opName = opName;
    }

    public String getOpName() {
        return opName;
    }

    public boolean isLogicalOperator(){
        return AND.equals(this) || OR.equals(this) || NOT.equals(this);
    }
}
