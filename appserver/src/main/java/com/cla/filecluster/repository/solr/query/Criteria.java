package com.cla.filecluster.repository.solr.query;

import com.cla.common.constants.SolrCoreSchema;
import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

/**
 * Single SOLR query unit, as in my_field:myValue <br/><br/>
 * Supports various operators, resulting in corresponding SOLR query syntax, such as: <br/>
 * <b>IN</b> - my_field:(val1 val2 ... valn)<br/>
 * <b>IN_RANGE</b> - my_field:(0 TO 10)<br/>
 * etc.<br/><br/>
 * Also supports atomic update operators for modifiers creation, such as <b>ADD</b>, <b>SET</b> etc.<br/>
 * In this case field should be null, since it is specified outside the modifier.
 */
public class Criteria {
    private SolrCoreSchema field;
    private SolrOperator op;
    private Object value;
    private List<Criteria> subCriteria = Lists.newLinkedList();

    private Criteria(SolrCoreSchema field, SolrOperator op, Object value) {
        this.field = field;
        this.op = op;
        this.value = value;
    }

    public static Criteria create(SolrCoreSchema field, SolrOperator op, Object value){
        return new Criteria(field, op, value);
    }

    public static Criteria and(Criteria... criterias){
        Criteria criteria = Criteria.create(null, SolrOperator.AND, null);
        criteria.getSubCriteria().addAll(Arrays.asList(criterias));
        return criteria;
    }

    public static Criteria or(Criteria... criterias){
        Criteria criteria = Criteria.create(null, SolrOperator.OR, null);
        criteria.getSubCriteria().addAll(Arrays.asList(criterias));
        return criteria;
    }

    public static Criteria and(List<Criteria> criterias){
        Criteria criteria = Criteria.create(null, SolrOperator.AND, null);
        criteria.getSubCriteria().addAll(criterias);
        return criteria;
    }

    public static Criteria or(List<Criteria> criterias){
        Criteria criteria = Criteria.create(null, SolrOperator.OR, null);
        criteria.getSubCriteria().addAll(criterias);
        return criteria;
    }

    public List<Criteria> getSubCriteria() {
        return subCriteria;
    }

    public SolrCoreSchema getField() {
        return field;
    }

    public SolrOperator getOp() {
        return op;
    }

    public Object getValue() {
        return value;
    }
}
