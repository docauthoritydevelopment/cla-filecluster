package com.cla.filecluster.repository.jpa.actions;

import com.cla.filecluster.domain.entity.actions.ActionInstance;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uri on 14/02/2016.
 */
public interface ActionInstanceRepository extends JpaRepository<ActionInstance,Long> {

}
