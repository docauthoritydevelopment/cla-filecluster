package com.cla.filecluster.repository.jpa.license;

import com.cla.filecluster.domain.entity.license.LicenseResource;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uri on 27/06/2016.
 */
public interface LicenseResourceRepository extends JpaRepository<LicenseResource,String>{
}
