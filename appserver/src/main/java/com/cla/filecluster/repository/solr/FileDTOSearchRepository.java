package com.cla.filecluster.repository.solr;

import com.cla.common.constants.ExtractionFieldType;
import com.cla.common.domain.dto.FacetFieldEntry;
import com.cla.common.domain.dto.PivotFacetFieldEntry;
import com.cla.filecluster.domain.dto.FileDTOSearchResult;
import com.cla.filecluster.domain.dto.SolrContentEntity;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.dto.SolrExtractionEntity;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.PivotField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.CursorMarkParams;
import org.apache.solr.common.util.NamedList;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Repository
public class FileDTOSearchRepository extends SolrRepository {

	@Value("${tokenMatcherActive}")
	private boolean isTokenMatcherActive;

    @Value("${subprocess:false}")
    private boolean subProcess;

	@Value("${fileDTO.solr.core:Extraction}")
	private String extractionSolrCore;


    @Override
	@PostConstruct
	protected void init() {
		if (!isTokenMatcherActive) {
			logger.info("isTokenMatcherActive is false. Will not try to connect to Solr, and token matching will not be applied");
			return;
		}
		super.init();
	}


	@Override
	protected Class getSchemaType() {
		return null;
	}

	@Override
	protected String coreName() {
		return extractionSolrCore;
	}

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    public FileDTOSearchResult find(final String[] filterQueries, final String[] fields) {
        return find(SolrQueryGenerator.ALL_QUERY, null, null, filterQueries, null, null, fields);
    }

    public FileDTOSearchResult find(final String q, final String[] facetFields,
                                    final String pivotFacetField, final String[] filterQueries,
                                    final Integer rows, final String cursorMark) {
        return find(q, facetFields, pivotFacetField, filterQueries, rows, cursorMark, SolrExtractionEntity.getFieldNamesNoContent());
    }

	public FileDTOSearchResult find(final String q, final String[] facetFields,
                                    final String pivotFacetField, final String[] filterQueries,
                                    final Integer rows, final String cursorMark, final String[] fields) {
		try {
			final SolrQuery query = createQuery(q, facetFields, pivotFacetField, filterQueries, rows, cursorMark, fields);
			final QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
			
			final List<SolrExtractionEntity> extractionSolrEntities = queryResponse.getBeans(SolrExtractionEntity.class);
			final List<FacetFieldEntry> facetFieldEntries = getFacets(queryResponse);
			final String nextCursorMark = queryResponse.getNextCursorMark();
			final List<PivotFacetFieldEntry> pivotFacetFieldEntries = getPivotFacet(queryResponse.getFacetPivot());

			return new FileDTOSearchResult(extractionSolrEntities, facetFieldEntries,pivotFacetFieldEntries, nextCursorMark);
        }
        catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private SolrQuery createQuery(final String q, final String[] facetFields, final String pivotFacetField,
			final String[] filterQueries, final Integer rows, String cursorMark, final String[] fields) {
		final SolrQuery query = new SolrQuery();
		
		if(filterQueries!=null) {
			query.addFilterQuery(filterQueries);
		}
		
		query.setQuery(q);
		if(facetFields != null && facetFields.length>0) {
			query.addFacetField(facetFields).setFacetMinCount(1);
		}
		if(pivotFacetField != null) {
			query.addFacetPivotField(pivotFacetField).setFacetMinCount(1);
		}
		if (rows!=null && rows>=0) {
			query.setRows(rows);
		}
		if (cursorMark!=null) {
			if (cursorMark.isEmpty()) {
				cursorMark = "*";
			}
			query.addSort(ExtractionFieldType.ID.getSolrName(), ORDER.asc);
			query.set(CursorMarkParams.CURSOR_MARK_PARAM, cursorMark);
		}

		query.setFields(fields);
		query.setRequestHandler("/query");
		return query;
	}

	private List<PivotFacetFieldEntry> getPivotFacet(final NamedList<List<PivotField>> solrPivots) {
		if(solrPivots==null) {
			return new LinkedList<>();
		}
		return solrPivots.getVal(0).stream().map(this::getPivotFacetFieldEntry).collect(toList());
	}
	
	private PivotFacetFieldEntry getPivotFacetFieldEntry(final PivotField solrPivotField){
		final PivotFacetFieldEntry pivotFacetFieldEntry = new PivotFacetFieldEntry(solrPivotField.getField(),String.valueOf(solrPivotField.getValue()),solrPivotField.getCount());
		if(solrPivotField.getPivot() != null) {
			pivotFacetFieldEntry.setPivotFacetFieldEntries(solrPivotField.getPivot().stream()
					.map(this::getPivotFacetFieldEntry)
					.collect(toList()));
		}
		return pivotFacetFieldEntry;
		
	}

	private List<FacetFieldEntry> getFacets(final QueryResponse queryResponse) {
		final List<FacetFieldEntry> facetFieldEntries = new LinkedList<>();
		final List<FacetField> solrFacetFields = queryResponse.getFacetFields();
		if(solrFacetFields!=null){
			for (final FacetField solrFacetField : solrFacetFields) {
				facetFieldEntries.addAll(solrFacetField.getValues().stream().map(c->new FacetFieldEntry(solrFacetField.getName(),c.getName(), c.getCount())).collect(toList()));
			}
		}
		return facetFieldEntries;
	}


    public void save(final SolrContentEntity contentEntity){
        try {
            getSolrClient().addBean(contentEntity, 1000);
		} catch (Exception e) {
			throw new RuntimeException("Could not save bean", e);
        }
    }


    public void saveAllEntities(List<SolrContentEntity> entities){
    	if (entities == null || entities.isEmpty()){
    		logger.warn("Entities is empty, ignoring request");
    		return;
		}
		try {
			getSolrClient().addBeans(entities);
		} catch (Exception e) {
			throw new RuntimeException(String.format("Could not save %d beans",entities.size()), e);
		}
	}

	public void commit() {
		try {
			client.commit();
		} catch (Exception e) {
			logger.error("Error during solr core commit. {}", e.getMessage(), e);
		}
	}
}
