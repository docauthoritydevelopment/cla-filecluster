package com.cla.filecluster.repository.solr.query;

import com.cla.common.constants.SolrCoreSchema;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrQuery;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Object representing SOLR query.
 * After being complete with all the relevant settings should be converted into a string query using SolrQueryGenerator
 *
 * @see SolrQueryGenerator
 */
public class Query {
    private String requestHandler;
    private List<Criteria> criterias = Lists.newLinkedList();    // translates to q
    private List<Query> filterQueries = Lists.newLinkedList();   // translates to fq
    private List<SolrCoreSchema> fields = Lists.newLinkedList(); // projection fields
    private Map<String, String> otherParams = Maps.newHashMap(); // custom parameters
    private List<SolrCoreSchema> facetFields = Lists.newLinkedList(); // facet fields
    private List<SolrCoreSchema> facetPivot = Lists.newLinkedList(); // facet pivot
    private List<Query> facetQuery = Lists.newLinkedList(); // facet query

    private Integer facetMinCount;
    private Integer facetPivotMinCount;
    private boolean facetPivotAsOne = false;
    private boolean facetFieldsAsOne = false;
    private Integer facetLimit;
    private Integer facetOffset;

    private Integer groupLimit;
    private SolrCoreSchema groupField;
    private boolean ngroupsOn = false;
    private List<Query> groupQueries = Lists.newLinkedList();

    private SolrCoreSchema statsField;

    private Boolean terms;

    private SolrFacetQueryJson jsonFacet;
    private List<SolrFacetQueryJson> facetJsonObjectList =  new ArrayList<>();

    // paging
    private Integer start;
    private Integer rows;

    private String cursorMark;

    private String jsonQuery;

    // sorting
    private Map<SolrCoreSchema, Sort.Direction> sortFields = new LinkedHashMap<>();

    // create

    public static Query create(){
        return new Query();
    }

    // getter & setters

    public List<SolrCoreSchema> getFields() {
        return fields;
    }

    public Query addQuery(Criteria criteria) {
        this.criterias.add(criteria);
        return this;
    }

    public List<Criteria> getCriteria() {
        return criterias;
    }

    private Query addCriteria(Criteria criteria){
        this.criterias.add(criteria);
        return this;
    }

    public Query addField(SolrCoreSchema field){
        fields.add(field);
        return this;
    }

    public Query addFilterQuery(Query filterQuery){
        filterQueries.add(filterQuery);
        return this;
    }

    public Query addFilterQuery(List<Query> filterQuery){
        filterQueries.addAll(filterQuery);
        return this;
    }

    /**
     * Syntactic sugar for adding a filter query with single criteria
     * @param criteria filter criteria
     * @return fluent notation - return this object
     */
    public Query addFilterWithCriteria(Criteria criteria){
        Query filterQuery = Query.create().addCriteria(criteria);
        this.filterQueries.add(filterQuery);
        return this;
    }

    public Query addParam(String name, String value) {
        otherParams.put(name, value);
        return this;
    }

    public Map<String, String> getOtherParams() {
        return otherParams;
    }

    /**
     * Even further syntactic sugar for adding a filter query with single criteria,
     * also skips Criteria.create() part
     *
     * @param field     criteria schema field
     * @param op        criteria operator
     * @param value     criteria value
     *
     * @return fluent notation - return this object
     */
    public Query addFilterWithCriteria(SolrCoreSchema field, SolrOperator op, Object value){
        addFilterWithCriteria(Criteria.create(field, op, value));
        return this;
    }

    public List<Query> getFilterQueries() {
        return filterQueries;
    }

    public Integer getRows() {
        return rows;
    }

    public Query setRows(Integer rows) {
        this.rows = rows;
        return this;
    }

    public Integer getStart() {
        return start;
    }

    public Query setStart(Integer start) {
        this.start = start;
        return this;
    }

    public List<SolrCoreSchema> getFacetFields() {
        return facetFields;
    }

    public Query addFacetField(SolrCoreSchema facetField) {
        this.facetFields.add(facetField);
        return this;
    }

    public List<Query> getFacetQuery() {
        return facetQuery;
    }

    public Query addFacetQuery(Criteria criteria) {
        Query facetQ = Query.create().addCriteria(criteria);
        this.facetQuery.add(facetQ);
        return this;
    }

    public List<SolrCoreSchema> getFacetPivot() {
        return facetPivot;
    }

    public Query addFacetPivot(SolrCoreSchema facetPivot) {
        this.facetPivot.add(facetPivot);
        return this;
    }

    public String getRequestHandler() {
        return requestHandler;
    }

    public Query setRequestHandler(String requestHandler) {
        this.requestHandler = requestHandler;
        return this;
    }

    public Map<SolrCoreSchema, Sort.Direction> getSortFields() {
        return sortFields;
    }

    public Query addSort(SolrCoreSchema sortField, Sort.Direction sortDir) {
        this.sortFields.put(sortField, sortDir);
        return this;
    }

    public Integer getFacetMinCount() {
        return facetMinCount;
    }

    public Query setFacetMinCount(Integer facetMinCount) {
        this.facetMinCount = facetMinCount;
        return this;
    }

    public boolean isFacetPivotAsOne() {
        return facetPivotAsOne;
    }

    public Query setFacetPivotAsOne(boolean facetPivotAsOne) {
        this.facetPivotAsOne = facetPivotAsOne;
        return this;
    }

    public boolean isFacetFieldsAsOne() {
        return facetFieldsAsOne;
    }

    public Query setFacetFieldsAsOne(boolean facetFieldsAsOne) {
        this.facetFieldsAsOne = facetFieldsAsOne;
        return this;
    }

    public SolrCoreSchema getGroupField() {
        return groupField;
    }

    public Query setGroupField(SolrCoreSchema groupField) {
        this.groupField = groupField;
        return this;
    }

    public Integer getGroupLimit() {
        return groupLimit;
    }

    public Query setGroupLimit(Integer groupLimit) {
        this.groupLimit = groupLimit;
        return this;
    }

    public Integer getFacetPivotMinCount() {
        return facetPivotMinCount;
    }

    public Query setFacetPivotMinCount(Integer facetPivotMinCount) {
        this.facetPivotMinCount = facetPivotMinCount;
        return this;
    }

    public boolean isNgroupsOn() {
        return ngroupsOn;
    }

    public Query setNgroupsOn(boolean ngroupsOn) {
        this.ngroupsOn = ngroupsOn;
        return this;
    }

    public Integer getFacetLimit() {
        return facetLimit;
    }

    public Query setFacetLimit(Integer facetLimit) {
        this.facetLimit = facetLimit;
        return this;
    }

    public Integer getFacetOffset() {
        return facetOffset;
    }

    public Query setFacetOffset(Integer facetOffset) {
        this.facetOffset = facetOffset;
        return this;
    }

    public String getCursorMark() {
        return cursorMark;
    }

    public Query setCursorMark(String cursorMark) {
        this.cursorMark = cursorMark;
        return this;
    }

    public SolrCoreSchema getStatsField() {
        return statsField;
    }

    public Query setStatsField(SolrCoreSchema statsField) {
        this.statsField = statsField;
        return this;
    }

    public Boolean getTerms() {
        return terms;
    }

    public void setTerms(Boolean terms) {
        this.terms = terms;
    }

    public Query addGroupQueryWithCriteria(Criteria criteria){
        Query filterQuery = Query.create().addCriteria(criteria);
        this.groupQueries.add(filterQuery);
        return this;
    }

    public List<Query> getGroupQueries() {
        return groupQueries;
    }

    public String getJsonQuery() {
        return jsonQuery;
    }

    public Query setJsonQuery(String jsonQuery) {
        this.jsonQuery = jsonQuery;
        return this;
    }

    public SolrFacetQueryJson getJsonFacet() {
        return jsonFacet;
    }

    public Query setJsonFacet(SolrFacetQueryJson jsonFacet) {
        this.jsonFacet = jsonFacet;
        return this;
    }

    public List<SolrFacetQueryJson> getFacetJsonObjectList() {
        return facetJsonObjectList;
    }

    public Query setFacetJsonObjectList(List<SolrFacetQueryJson> facetJsonObjectList) {
        this.facetJsonObjectList = facetJsonObjectList;
        return this;
    }

    // build
    public SolrQuery build(){
        return SolrQueryGenerator.generate(this);
    }
}
