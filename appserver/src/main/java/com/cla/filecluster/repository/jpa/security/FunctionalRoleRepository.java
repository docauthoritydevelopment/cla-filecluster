package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.FunctionalRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IR on 15/10/2017.
 */
public interface FunctionalRoleRepository extends JpaRepository<FunctionalRole, Long> {

    @Query("SELECT fr FROM FunctionalRole fr WHERE fr.deleted = false")
    List<FunctionalRole> findNotDeleted();

    @Query("SELECT fr FROM FunctionalRole fr WHERE fr.deleted = false")
    Page<FunctionalRole> findNotDeleted(Pageable pageRequest);

    @Query("SELECT fr FROM FunctionalRole fr LEFT JOIN FETCH fr.template t " +
            "WHERE fr.deleted = FALSE and t.id = :roleTemplateId")
    List<FunctionalRole> findByRoleTemplate(@Param("roleTemplateId") Long roleTemplateId);

    @Query("SELECT fr FROM FunctionalRole fr WHERE fr.name = :name"+
            " AND fr.deleted=false")
    FunctionalRole findByName(@Param("name") String name);

    @Query("select ft from FunctionalRole ft where ft.name like %:name% and ft.deleted = false")
    List<FunctionalRole> findFuncRoleNameLike(@Param("name") String name);

    @Query("SELECT fr FROM FunctionalRole fr WHERE fr.name = :name")
    FunctionalRole findByNameAll(@Param("name") String name);

    @Query("SELECT fr FROM FunctionalRole fr " +
            "WHERE fr.displayName = :displayName")
    FunctionalRole getFunctionalRoleByDisplayNameAll(@Param("displayName") String displayName);

    @Query("SELECT fr FROM FunctionalRole fr " +
            " WHERE fr.id IN (:funRoleIds)" +
            " AND fr.deleted=false")
    List<FunctionalRole> findByIds(@Param("funRoleIds") Iterable<Long> funRoleIds);

    @Query("SELECT br FROM FunctionalRole br LEFT JOIN FETCH br.template t LEFT JOIN FETCH t.roles r left join fetch r.authorities WHERE br.id = :id AND br.deleted = FALSE")
    FunctionalRole getOneWithRoles(@Param("id") Long funcRoleId);

    @Query("SELECT br FROM FunctionalRole br LEFT JOIN FETCH br.template t LEFT JOIN FETCH t.roles r left join fetch r.authorities WHERE br.deleted = FALSE")
    List<FunctionalRole> findAllWithRoles();
}
