package com.cla.filecluster.repository.jpa.doctype;

import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by uri on 17/08/2015.
 */

public interface DocTypeRepository extends JpaRepository<DocType, Long>, JpaSpecificationExecutor<DocType> {

    @Query(value = "select dt from DocType dt where dt.template.id = :templateId and dt.name = :name")
    DocType findByTemplateIdAndName(@Param("templateId") long templateId, @Param("name") String name);

    @Query(value = "select dt from DocType dt where dt.template.id = :templateId and dt.uniqueName = :name")
    DocType findByTemplateIdAndUniqueName(@Param("templateId") long templateId, @Param("name") String name);

    @Query(value = "select dt from DocType dt where dt.deleted = false")
    Page<DocType> findAllByAccount(Pageable pageRequest);

    @Query(value = "select dt from DocType dt where dt.name = :name")
    DocType findByName(@Param("name") String docTypeName);

    @Query(value = "select dt from DocType dt where dt.name like %:name% and dt.deleted = false")
    List<DocType> findByNameLike(@Param("name") String docTypeName);

    @Query(value = "select dt from DocType dt where dt.docTypeIdentifier in (:identifiers)")
    Set<DocType> findBySolrIdentifiers(@Param("identifiers") List<String> docTypeIdentifiers);

    @Query(value = "select dt from DocType dt where dt.deleted = false order by dt.name")
    List<DocType> findAllByAccount();

    @Query(value = "select dt from DocType dt where dt.deleted = true")
    List<DocType> findDeleted();

    @Query(value = "select dt from DocType dt where dt.parentDocType.id = :parentId and dt.deleted = false")
    List<DocType> findByParent(@Param("parentId") long parentId);

    @Query(value = "select dt from DocType dt where dt.parentDocType.id = :parentId and dt.name = :name and dt.deleted = false")
    List<DocType> findByParentAndName(@Param("parentId") long parentId, @Param("name") String name);

    @Query(value = "select dt from DocType dt where dt.parentDocType is null and dt.name = :name and dt.deleted = false")
    List<DocType> findByNullParentAndName( @Param("name") String name);

    @Query(value = "select dt from DocType dt where dt.parentDocType.id = :parentId")
    List<DocType> findByParentAll(@Param("parentId") long parentId);

    @Query(value = "select fts from FileTagSetting fts where fts.dirty = true")
    Page<FileTagSetting> findDirtyFileTagSettings(Pageable page);

    @Query(value = "select dt from DocType dt left join fetch dt.fileTagSettings where dt.id = :id")
    DocType getByIdWithTagSettings(@Param("id") long id);

    @Query(value = "select dt from DocType dt where dt.id in :ids")
    List<DocType> findByIds(@Param("ids") Collection<Long> ids);

}