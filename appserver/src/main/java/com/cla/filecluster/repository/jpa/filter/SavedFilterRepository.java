package com.cla.filecluster.repository.jpa.filter;

import com.cla.filecluster.domain.entity.filter.SavedFilter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by uri on 08/11/2015.
 */
public interface SavedFilterRepository extends JpaRepository<SavedFilter,Long> {

    @Query("SELECT br FROM SavedFilter br WHERE br.name = :name")
    SavedFilter findByName(@Param("name") String name);
}
