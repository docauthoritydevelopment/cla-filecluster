package com.cla.filecluster.repository.solr;

/**
 * Result of Solr operation to bew returned to the caller.
 * For example, may contain ID of newly created object.
 */
public class SolrOpResult {
    private boolean successful = true;
    private Object id;
    private Long contentId;

    public static SolrOpResult create(){
        return new SolrOpResult();
    }

    public static SolrOpResult create(Object id){
        SolrOpResult solrOpResult = new SolrOpResult();
        return solrOpResult.setId(id);
    }

    public SolrOpResult setContentId(Long contentId) {
        this.contentId = contentId;
        return this;
    }

    public Object getId() {
        return id;
    }

    public String getIdAsString() {
        return (String)id;
    }

    public Long getIdAsLong() {
        return (Long)id;
    }

    public Long getContentId() {
        return contentId;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public SolrOpResult setSuccessful(boolean successful) {
        this.successful = successful;
        return this;
    }

    public SolrOpResult setId(Object id) {
        this.id = id;
        return this;
    }
}
