package com.cla.filecluster.repository.jpa.filetree;

import com.cla.filecluster.domain.entity.filetree.DocStore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Set;

/**
 * Created by uri on 11/08/2015.
 */
public interface DocStoreRepository extends JpaRepository<DocStore, Long>, JpaSpecificationExecutor<DocStore> {

    @Query("select ds from DocStore ds left join ds.rootFolders where ds.description = :description")
    Collection<DocStore> findDocStoreByDescription(@Param("description") String description);
}
