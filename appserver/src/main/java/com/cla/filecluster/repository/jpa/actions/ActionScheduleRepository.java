package com.cla.filecluster.repository.jpa.actions;

import com.cla.filecluster.domain.entity.actions.ActionSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by: yael
 * Created on: 3/4/2018
 */
public interface ActionScheduleRepository extends JpaRepository<ActionSchedule,Long> {

    @Query("SELECT br FROM ActionSchedule br WHERE br.name = :name")
    ActionSchedule findByName(@Param("name") String name);
}
