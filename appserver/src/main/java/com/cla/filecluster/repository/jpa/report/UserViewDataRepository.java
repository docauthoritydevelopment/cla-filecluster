package com.cla.filecluster.repository.jpa.report;

import com.cla.common.domain.dto.report.DisplayType;
import com.cla.filecluster.domain.entity.report.UserViewData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 3/8/2018
 */
public interface UserViewDataRepository extends JpaRepository<UserViewData,Long> {

    @Query(value = "select uvd from UserViewData uvd" +
            " left join uvd.filter" +
            " where uvd.id = :id")
    UserViewData findOneJoinFilter(@Param("id") Long id);

    @Query("select uvd from UserViewData uvd where uvd.name = :name and uvd.displayType = :displayType and uvd.owner.id = :ownerId")
    List<UserViewData> findByNameUserAndType(@Param("name") String name, @Param("displayType") DisplayType displayType, @Param("ownerId") Long ownerId);

    @Query("select uvd from UserViewData uvd where uvd.displayType = :displayType")
    Page<UserViewData> findByType(@Param("displayType") DisplayType displayType, Pageable pageable);

    @Query("select uvd from UserViewData uvd where lower(uvd.name) like %:namingSearchTerm% and uvd.displayType = :displayType")
    Page<UserViewData> findByTypeFilterByTerm(@Param("displayType") DisplayType displayType, @Param("namingSearchTerm") String namingSearchTerm, Pageable pageable);

    @Query("select uvd from UserViewData uvd where uvd.displayType = :displayType and (uvd.globalFilter = true or uvd.owner.id = :ownerId)")
    Page<UserViewData> findByTypeAndUser(@Param("displayType") DisplayType displayType, @Param("ownerId") Long ownerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where lower(uvd.name) like %:namingSearchTerm% and uvd.displayType = :displayType and (uvd.globalFilter = true or uvd.owner.id = :ownerId)")
    Page<UserViewData> findByTypeAndUserFilterByTerm(@Param("displayType") DisplayType displayType, @Param("ownerId") Long ownerId, @Param("namingSearchTerm") String namingSearchTerm, Pageable pageable);

    @Query("select uvd from UserViewData uvd where uvd.globalFilter = true or uvd.owner.id = :ownerId")
    Page<UserViewData> findByUser(@Param("ownerId") long ownerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where lower(uvd.name) like %:namingSearchTerm% and (uvd.globalFilter = true or uvd.owner.id = :ownerId)")
    Page<UserViewData> findByUserFilterByTerm(@Param("ownerId") long ownerId, @Param("namingSearchTerm") String namingSearchTerm, Pageable pageable);

    @Query("select uvd from UserViewData uvd where uvd.filter.id = :filterId")
    List<UserViewData> findByFilterId(@Param("filterId") Long filterId);

    @Query("select uvd from UserViewData uvd where uvd.displayType = :displayType and uvd.container.id = :containerId")
    Page<UserViewData> findByTypeAndContainerId(@Param("displayType") DisplayType displayType, @Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where lower(uvd.name) like %:namingSearchTerm% and uvd.displayType = :displayType and uvd.container.id = :containerId")
    Page<UserViewData> findByTypeFilterByTermAndContainerId(@Param("displayType") DisplayType displayType, @Param("namingSearchTerm") String namingSearchTerm, @Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where uvd.displayType = :displayType and (uvd.globalFilter = true or uvd.owner.id = :ownerId) and uvd.container.id = :containerId")
    Page<UserViewData> findByTypeAndUserAndContainerId(@Param("displayType") DisplayType displayType, @Param("ownerId") Long ownerId, @Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where lower(uvd.name) like %:namingSearchTerm% and uvd.displayType = :displayType and (uvd.globalFilter = true or uvd.owner.id = :ownerId) and uvd.container.id = :containerId")
    Page<UserViewData> findByTypeAndUserFilterByTermAndContainerId(@Param("displayType") DisplayType displayType, @Param("ownerId") Long ownerId, @Param("namingSearchTerm") String namingSearchTerm, @Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where (uvd.globalFilter = true or uvd.owner.id = :ownerId) and uvd.container.id = :containerId")
    Page<UserViewData> findByUserAndContainerId(@Param("ownerId") long ownerId, @Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where lower(uvd.name) like %:namingSearchTerm% and (uvd.globalFilter = true or uvd.owner.id = :ownerId) and uvd.container.id = :containerId")
    Page<UserViewData> findByUserFilterByTermAndContainerId(@Param("ownerId") long ownerId, @Param("namingSearchTerm") String namingSearchTerm, @Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where uvd.container.id = :containerId")
    Page<UserViewData> findByContainerId(@Param("containerId") long containerId, Pageable pageable);

    @Query("select uvd from UserViewData uvd where ( uvd.displayType = :displayType and  uvd.container is null and ( uvd.owner.id = :ownerId or uvd.globalFilter = true) )")
    Page<UserViewData> findByUserAndTypeWithoutContainer(@Param("ownerId") long ownerId, @Param("displayType") DisplayType displayType, Pageable pageable);

    @Modifying
    @Query("delete from UserViewData uvd where uvd.container.id = :containerId")
    void deleteByContainerId(@Param("containerId") long containerId);

    @Query("select uvd from UserViewData uvd where uvd.id = :userViewDataId and (uvd.owner.id = :ownerId  or uvd.globalFilter = true )")
    Optional<UserViewData> findEligibaleById(@Param("userViewDataId") Long userViewDataId, @Param("ownerId") long ownerId);
}
