package com.cla.filecluster.repository.jpa.searchpattern;

import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by ophir on 10/04/2019.
 */
public interface RegulationRepository extends JpaRepository<Regulation,Integer> {
    @Query("SELECT rg FROM Regulation rg WHERE rg.name = :name")
    Regulation findByName(@Param("name") String name);
}
