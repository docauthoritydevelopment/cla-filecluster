package com.cla.filecluster.repository.jpa;

/**
 * Created by: yael
 * Created on: 7/21/2019
 */
public interface PartitionManagement {

    String getLastPartitionName();

    Long getLastPartitionDate();

    String getFirstPartitionName();

    Long getFirstPartitionDate();

    void createPartition(long partitionId, long partitionDate);

    void truncatePartitionForPartitionId(long partitionId);

    void deletePartitionForPartitionId(long partitionId);
}
