package com.cla.filecluster.repository.file;

import com.google.common.collect.Lists;
import com.independentsoft.share.File;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by uri on 08/08/2016.
 */
public class SharePointMediaFolderWithFiles extends MediaFolderWithFiles {

    private Map<String,File> filesMap;

    public SharePointMediaFolderWithFiles(String folderId) {
        super(folderId);
    }

    public void setFiles(List<File> files) {
        this.filesMap = files.stream().collect(Collectors.toMap(this::extractKey, Function.identity()));
        this.fileIds = Lists.newArrayList(filesMap.keySet());
    }

    private String extractKey(File file){
        return folderId + "/" + file.getName();
    }

}
