package com.cla.filecluster.repository.file;

import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TextSearchPatternImplementationRepository {

    @Value("${search-patterns.load-all:false}")
    private Boolean loadAllPatterns;

    private final Map<Integer, TextSearchPatternImpl> textSearchPatternMap = new ConcurrentHashMap<>();

    private static final Logger logger = LoggerFactory.getLogger(TextSearchPatternImplementationRepository.class);

    public Collection<TextSearchPatternImpl> findAll() {
        return textSearchPatternMap.values();
    }

    public TextSearchPatternImpl findById(final int id) {
        return textSearchPatternMap.get(id);
    }

    public void init(List<TextSearchPatternDto> textSearchPatternDtos) {
        textSearchPatternMap.clear();
        for (TextSearchPatternDto textSearchPatternDto : textSearchPatternDtos) {
            if (textSearchPatternDto.isActive() || loadAllPatterns) {
                addTextSearchPatternToCache(textSearchPatternDto);
            } else {
                logger.info("Skipping inactive patterns: {}", textSearchPatternDto);
            }
        }
    }

    public void addTextSearchPatternToCache(TextSearchPatternDto textSearchPatternDto) {
        int id = textSearchPatternDto.getId();
        String name = textSearchPatternDto.getName();
        boolean active = textSearchPatternDto.isActive();
        if (textSearchPatternDto.getValidatorClass() != null) {
            try {
                String validatorClass = textSearchPatternDto.getValidatorClass();
                TextSearchPatternImpl textSearchPatternImpl = getTextSearchPatternImpl(validatorClass, id, name, active, textSearchPatternDto.getPattern(), textSearchPatternDto.isSubCategoryActive());
                textSearchPatternMap.put(id, textSearchPatternImpl);
            } catch (Exception e) {
                logger.error("Failed to create text search pattern class implementation ({})", textSearchPatternDto.getValidatorClass(), e);
            }
        } else {
            TextSearchPatternImpl textSearchPatternImpl = new TextSearchPatternImpl(id, name, textSearchPatternDto.getPattern(), active, textSearchPatternDto.isSubCategoryActive());
            textSearchPatternMap.put(id, textSearchPatternImpl);
        }
    }

    public void deleteSearchPatternStateFromCache(Integer id) {
        textSearchPatternMap.remove(id);
    }

    public static TextSearchPatternImpl getTextSearchPatternImpl(String validatorClass, int id, String name, Boolean active, String pattern, Boolean subCategoryActive)
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, java.lang.reflect.InvocationTargetException, NoSuchMethodException {
        Class<?> aClass = Class.forName(validatorClass);
        TextSearchPatternImpl res = (TextSearchPatternImpl) aClass
                .getConstructor(Integer.class, String.class).newInstance(id, name);
        res.setActive(active);
        res.setSubCategoryActive(subCategoryActive);
        if (!Strings.isNullOrEmpty(pattern)) {
            res.updatePattern(pattern);
        }
        return res;
    }

//	private TextSearchPatternDto createFromPatternClaFileSearchPattern(final Object key) {
//		final String[] split = key.toString().split("\\.");
//		return new TextSearchPatternDto(Integer.valueOf(split[0]), split[1], props.getProperty(key.toString()));
//	}
//
//	private TextSearchPatternDto createFromClassClaFileSearchPattern(final Object key) {
//		try {
//			final String[] split = key.toString().split("\\.");
//			final String packageName = TextSearchPatternDto.class.getPackage().getName();
//			return (TextSearchPatternDto) Class.forName(packageName + "." + props.getProperty(key.toString()))
//					.getConstructor(Integer.class, String.class).newInstance(Integer.valueOf(split[0]), split[1]);
//		} catch (final Exception e) {
//			throw new RuntimeException("Could not create ClaFilePattern class " + key.toString(), e);
//		}
//	}

}
