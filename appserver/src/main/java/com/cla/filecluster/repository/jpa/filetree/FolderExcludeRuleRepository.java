package com.cla.filecluster.repository.jpa.filetree;

import com.cla.filecluster.domain.entity.filetree.FolderExcludeRule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 12/04/2016.
 */
public interface FolderExcludeRuleRepository  extends JpaRepository<FolderExcludeRule,Long> {

    @Query("select fer from FolderExcludeRule fer where fer.rootFolder.id = :rootFolderId")
    Page<FolderExcludeRule> findByRootFolderId(@Param("rootFolderId") Long rootFolderId, Pageable pageRequest);

    @Query("select fer from FolderExcludeRule fer" +
            " where fer.rootFolder.id = :rootFolderId or fer.rootFolder is null")
    List<FolderExcludeRule> getRootFolderRelevantExcludeRules(@Param("rootFolderId") Long rootFolderId);

    @Query("select fer from FolderExcludeRule fer" +
            " where fer.rootFolder.id = :rootFolderId and fer.matchString = :matchString and fer.operator = :operator")
    FolderExcludeRule findByRootFolderIdMatchStringOperator(@Param("rootFolderId") long rootFolderId,
                                                            @Param("matchString") String matchString,@Param("operator") String operator);

    @Query("delete from FolderExcludeRule dta where dta.rootFolder.id = :rootFolderId")
    @Modifying
    int deleteByRootFolderId(@Param("rootFolderId") Long rootFolderId);
}
