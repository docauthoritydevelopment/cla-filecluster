package com.cla.filecluster.repository.solr;

import com.cla.common.constants.DefaultSolrSchema;
import com.cla.common.domain.dto.pv.TitleToken;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.crawler.pv.SolrDocHelper;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Repository
public class GroupSearchRepository extends SolrRepository {

    private static final String PROPOSED_TITLE_KEY_SEPARATOR = ":";
    private static final Pattern PATH_SPLIT_PATTERN = Pattern.compile("[\\\\/]");

    @Value("${groupNaming.maxPathSplits:50}")
    private int groupNamingMaxPathSplits;

    @Value("${groupNaming.groups.autoCommitMs:600000}")
    private int groupNamingAutoCommitMs;

    @Value("${groupNaming.minTitleTokenLength:4}")
    private int minTitleTokenLength;

    @Value("${groupsNaming.solr.core:Groups}")
    private String groupsSolrCore;

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected String coreName() {
        return groupsSolrCore;
    }

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    public void deleteAllForAccount() {
        try {
            logger.info("Delete Groups index documents ...");
            client.deleteByQuery(SolrQueryGenerator.ALL_QUERY);
            client.commit();
        } catch (Exception e) {
            throw new RuntimeException("Could not delete Groups", e);
        }
    }


    public List<TitleToken> getTermVector(final String fieldName, final String id, final double skipTermsThreshold) {
        try {
            // Adding terms for 2 word phrases
            Query qterms = Query.create();
            qterms.setTerms(true);
            qterms.addParam("tv.fl", fieldName);
            qterms.setRequestHandler("/tvrh");
            if (client.isSolrCloudClient()) {
                qterms.addParam("shards.qt", "/tvrh");
            }
            // qterms.setTermsLimit(20);
            // qterms.addTermsField("titles");
            // qterms.setTermsMinCount(20);
            qterms.addQuery(Criteria.create(DefaultSolrSchema.ID, SolrOperator.EQ, id));

            final List<TitleToken> tokens = new LinkedList<>();
            final QueryResponse query = client.query(qterms.build(), SolrRequest.METHOD.POST);
            final NamedList<Object> queryResponse = query.getResponse();
            int numOfFiles;
            int minTfThreshold;
            final SolrDocumentList results = query.getResults();
            if (results.isEmpty() || results.get(0) == null || results.get(0).getFirstValue("numOfFiles") == null) {
                logger.warn("Failed to get numOfFiles for group fieldName:{}, id:{}, qterms:{}, queryResponse:{}", fieldName, id, qterms, query);
                return tokens;
            }
            numOfFiles = ((Integer) results.get(0).getFirstValue("numOfFiles"));
            minTfThreshold = (int) (numOfFiles * skipTermsThreshold);
            final NamedList namedList = (NamedList) queryResponse.get("termVectors");
            final Iterator<Entry> termVectors = namedList.iterator();
            while (termVectors.hasNext()) {
                processTermVector(fieldName, id, tokens, numOfFiles, minTfThreshold, termVectors);
            }

//			final Comparator<TitleToken> comparator = Collections.reverseOrder();
//			Collections.sort(tokens, comparator);
            return tokens;

        } catch (final SolrServerException e) {
            throw new RuntimeException("Failed on search with searchQuery:" + id, e);
        } catch (IOException e) {
            throw new RuntimeException("IO issue on search with searchQuery:" + id, e);
        } catch (Exception e) {
            throw new RuntimeException("Unexpected issue on search with searchQuery:" + id, e);
        }
    }


    private void processTermVector(final String fieldName, final String id, final List<TitleToken> tokens, final int numOfFiles,
                                   final int minTfThreshold, final Iterator<Entry> termVectors) {
        final Entry entry = termVectors.next();
        if (!(entry.getValue() instanceof NamedList)) {
            return;
        }
        for (final Iterator fi = ((NamedList) entry.getValue()).iterator(); fi.hasNext(); ) {
            final Entry fieldEntry = (Entry) fi.next();
            if (fieldEntry.getKey().equals(fieldName)) {
                for (final Iterator tvInfoIt = ((NamedList) fieldEntry.getValue()).iterator(); tvInfoIt.hasNext(); ) {
                    processTvInfo(id, tokens, numOfFiles, minTfThreshold, (Entry) tvInfoIt.next());
                }
            }
        }
    }


    private void processTvInfo(final String id, final List<TitleToken> tokens, final int numOfFiles,
                               final int minTfThreshold, final Entry tvInfo) {
        final NamedList tv = (NamedList) tvInfo.getValue();
        final TitleToken titleToken = new TitleToken((String) tvInfo.getKey(), (Double) tv.get("tf-idf"), (Integer) tv.get("tf"));
        if (minTfThreshold == 0 || titleToken.getTf() >= minTfThreshold) {
            if (numOfFiles > 0 && titleToken.getTf() > numOfFiles) {
                logger.trace("Adjust {} for groupId {} as tf> {}", titleToken, id, numOfFiles);
                final double fixFactor = ((double) numOfFiles) / titleToken.getTf();
                titleToken.setTfidf(titleToken.getTfidf() / (fixFactor));
                titleToken.setTf(numOfFiles);
            } 
            if (titleToken.getToken().length() >= minTitleTokenLength) {
                tokens.add(titleToken);
            }
        } else {
            logger.trace("Skip {} for groupId {} as getTf()={}<{}", titleToken, id, titleToken.getTf(), minTfThreshold);
        }
    }

    public SolrInputDocument createFileToGroupDocument(final FileGroup group, final List<String> titlesCollection, final String baseName, final String fullPath, int maxGroupIndexedFiles) {
    	return createFileToGroupDocument(group, titlesCollection, baseName, fullPath, maxGroupIndexedFiles, null);
    }

	public SolrInputDocument createFileToGroupDocument(final FileGroup group, final List<String> titlesCollection, final String baseName, final String fullPath, int maxGroupIndexedFiles, SolrDocHelper doc) {
        boolean returnValue = false;
		if (doc == null) {
        	doc = new SolrDocHelper();
        	returnValue = true;
        }

        try {
        	if (doc.getValue("id") == null) {
                String groupId = group.getId();
                if (groupId == null){
                    logger.error("Group ID was null, cannot continue.");
                    return null;
                }

        		doc.setValue("id", groupId);
    			long numOfFilesForTermsSearch = group.getNumOfFiles();
    			// In case of limit to the number of indexed files per group, use that limit as the number used for minCount limit calc.
    			if (maxGroupIndexedFiles > 0 && numOfFilesForTermsSearch >= maxGroupIndexedFiles) {
    				logger.debug("Setting numOfFilesForTermsSearch to {} (was {})", maxGroupIndexedFiles, numOfFilesForTermsSearch);
    				numOfFilesForTermsSearch = maxGroupIndexedFiles; 
    			}
        		doc.setValue("numOfFiles", numOfFilesForTermsSearch);
        	}

//			addMultiValueField("fileNames", Arrays.asList(baseName), doc);
        	doc.addValues("fileNamesShingle", Lists.newArrayList(baseName));

//         final String[] sp = fullPath.split("[\\\\/]");
            final String[] sp = PATH_SPLIT_PATTERN.split(fullPath,groupNamingMaxPathSplits);
            int pathSplitLength = sp.length;
            if (pathSplitLength > 2 && pathSplitLength < groupNamingMaxPathSplits) {
            	doc.addValues("parentDirShingle", Lists.newArrayList(sp[pathSplitLength - 2]));
            }

            if (pathSplitLength > 3 && pathSplitLength < groupNamingMaxPathSplits) {
            	doc.addValues("grandParentDirShingle", Lists.newArrayList(sp[pathSplitLength - 3]));
            }

            final List<String> strippedTitlesCollection = titlesCollection.stream()
                    .map(this::removeTitleKey)
                    .collect(Collectors.toList());
            final List<String> titlesNoKeys = removeTitleKey(titlesCollection);
            doc.addListValues("titles", titlesNoKeys);
            doc.addListValues("titlesShingle", titlesNoKeys);
            if (titlesCollection.size() > 0) {
            	doc.addListValues("firstTitlesShingle", titlesNoKeys.subList(0, 1));
            }
            if (titlesCollection.size() > 1) {
            	doc.addListValues("secondTitlesShingle", titlesNoKeys.subList(1, 2));
            }
            if (titlesCollection.size() > 2) {
            	doc.addListValues("thirdTitlesShingle", titlesNoKeys.subList(2, 3));
            }
        } catch (final Exception e) {
            throw new RuntimeException("Error during group idexing. {}", e);
        }
        return returnValue ? doc.toSolrDoc() : null;		// Convert to SolrDoc if doc was not passed as parameter
    }
    
    public void writeDocuments(Collection<SolrInputDocument> documents) {
        if (documents == null || documents.size() == 0) {
            return;
        }
        try {
            logger.debug("Send {} documents to Solr",documents.size());
            UpdateResponse response = client.add(documents, groupNamingAutoCommitMs);
            if (response == null) {
                throw new RuntimeException("Failed to create group search docs in Solr.");
            }
            logger.trace("Got response from solr: {}",response);
        } catch (Exception e) {
            logger.error("Failed to create group search docs in Solr", e);
            StringJoiner stringJoiner = new StringJoiner(",");
            documents.forEach(f -> stringJoiner.add(f.getField("id") != null ? f.getField("id").toString() : "null ID"));
            logger.error("Failed file ids: {}", stringJoiner.toString());
            throw new RuntimeException("Failed to create group search docs in Solr.", e);
        }
    }

    private List<String> removeTitleKey(final List<String> titlesCollection) {
        return titlesCollection.stream().map(this::removeTitleKey).collect(Collectors.toList());
    }

    private String removeTitleKey(final String t) {
        return t.substring(t.indexOf(PROPOSED_TITLE_KEY_SEPARATOR) + PROPOSED_TITLE_KEY_SEPARATOR.length());
    }


    @SuppressWarnings("unchecked")
	private void addMultiValueField(final String fieldName, final Collection<String> collection, final SolrInputDocument doc) {
    	Map<String, Object> mapVal = (Map<String, Object>) doc.getFieldValue(fieldName);
    	if (mapVal == null ) {
    		mapVal = new HashMap<>();
            mapVal.put("add", new ArrayList<>(collection));
    	}
    	else {
    		Collection<String> prevColl = (Collection<String>) mapVal.get("add");
    		prevColl.addAll(collection);
    	}
        doc.setField(fieldName, mapVal);
    }

	public void deleteByGroupId(String group) {
        try {
            logger.debug("Delete group index for {}", group);
            client.deleteByQuery(DefaultSolrSchema.ID.filter(group));
        } catch (Exception e) {
            throw new RuntimeException("Could not delete Groups", e);
        }
	}

    public void deleteByGroupIds(Collection<String> groupIds) {
        if (groupIds == null || groupIds.isEmpty()) {
            return;
        }

        //StringJoiner deleteQueryStringJoiner = new StringJoiner(" ", "id:(", ")");    // query: id:(id1 id2 id3 ...)
        //groupIds.forEach(cid -> deleteQueryStringJoiner.add(cid));
        try {
            logger.trace("Delete title indexes for groupIds: {}", groupIds);
            client.deleteByQuery(DefaultSolrSchema.ID.inQuery(groupIds));
        } catch (Exception e) {
            throw new RuntimeException("Could not delete title indexes for groupIds (" + groupIds + ")", e);
        }
    }

	public void add(List<SolrInputDocument> groupSolrDocs) {
        try {
            client.add(groupSolrDocs);
        } catch (Exception e) {
            throw new RuntimeException("Could not add documents", e);
        }
	}

}
