package com.cla.filecluster.repository.jpa.report;

import com.cla.filecluster.domain.entity.report.TrendChart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 27/07/2016.
 */

public interface TrendChartRepository extends JpaRepository<TrendChart,Long>{

    @Query("select tc from TrendChart tc where tc.deleted=false")
    List<TrendChart> findNonDeleted();

    @Query("select tc from TrendChart tc where tc.name=:name")
    List<TrendChart> findByName(@Param("name") String name);
}
