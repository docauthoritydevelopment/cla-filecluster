package com.cla.filecluster.repository.jpa.entitylist;

import com.cla.filecluster.domain.entity.bizlist.BizListItemAlias;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by uri on 31/12/2015.
 */
public interface BizListItemAliasRepository extends JpaRepository<BizListItemAlias, Long>, JpaSpecificationExecutor<BizListItemAlias> {

    @Modifying
    @Query("delete from BizListItemAlias bla where bla.id in :ids")
    void deleteByIds(@Param("ids") List<Long> ids);

    @Query("select bla from BizListItemAlias bla where bla.entity.importId = :importId")
    Set<BizListItemAlias> findAllByImportId(@Param("importId") long importId);

    @Query("select bla.id from BizListItemAlias bla where bla.entity.bizList.id = :bizListId") //left join fetch bla.SimpleBizListItem bli
    List<Long> getByBizListId(@Param("bizListId") long bizListId);
}
