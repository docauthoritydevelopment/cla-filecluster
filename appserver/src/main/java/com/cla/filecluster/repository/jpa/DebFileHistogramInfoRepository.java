package com.cla.filecluster.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cla.filecluster.domain.entity.pv.DebFileHistogramInfo;

public interface DebFileHistogramInfoRepository extends JpaRepository<DebFileHistogramInfo, String> {

}
