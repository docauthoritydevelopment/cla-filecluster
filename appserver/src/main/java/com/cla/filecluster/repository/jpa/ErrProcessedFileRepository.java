package com.cla.filecluster.repository.jpa;

import com.cla.filecluster.domain.entity.pv.ErrProcessedFile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ErrProcessedFileRepository extends JpaRepository<ErrProcessedFile, Long> , JpaSpecificationExecutor<ErrProcessedFile> {
	

    @Query(value = "select ef from ErrProcessedFile ef where ef.claFile = :fileId")
    public List<ErrProcessedFile> getFileErrors(@Param("fileId") Long claFileId);

    @Modifying
    @Query("delete from ErrProcessedFile ef" +
            " where ef.rootFolderId = :rootFolderId")
    int deleteByRootFolder(@Param("rootFolderId") Long rootFolderId);

    @Query("select count(se) from ErrProcessedFile se where se.runId = :runId")
    long countByRunId(@Param("runId") Long crawlRunId);

}
