package com.cla.filecluster.repository.jpa.scope;

import com.cla.filecluster.domain.entity.scope.WorkGroupsScope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.List;

public interface WorkGroupsScopeRepository extends JpaRepository<WorkGroupsScope, Long> {

    @Query("select wgs from WorkGroupsScope wgs where name = :name")
    WorkGroupsScope findByName(@Param("name") String name);

    @Query(nativeQuery = true,
            value = "select wgswg.work_groups_scope_id from work_groups_scope_work_groups wgswg \n" +
                    "join users_work_groups uwg on uwg.work_group_id = wgswg.work_group_id\n" +
                    "where uwg.user_id = :userId")
    List<BigInteger> findWorkGroupScopeIds(@Param("userId") Long userId);
}
