package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.LdapConnectionDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Itai Marko.
 */
public interface LdapConnectionDetailsRepository extends JpaRepository<LdapConnectionDetails, Long> {

    boolean existsByName(String name);

    LdapConnectionDetails findByName(String name);

    Page<LdapConnectionDetails> findAllByEnabledTrue(Pageable pageable);
}
