package com.cla.filecluster.repository.jpa.filetree;

import com.cla.filecluster.domain.entity.filetree.RootFolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

/**
 * Repository for handling root folders
 * Created by uri on 11/08/2015.
 */
public interface RootFolderRepository extends JpaRepository<RootFolder, Long>, JpaSpecificationExecutor<RootFolder> {

    @Query(value = "select rf from RootFolder rf" +
            " left join rf.docStoreInclude" +
            " left join rf.folderExcludeRules where rf.id = :id and rf.deleted = false")
    RootFolder findOneJoinDocStore(@Param("id") Long id);

    @Query(value = "select rf from RootFolder rf " +
            " left join rf.docStoreInclude " +
            " left join fetch rf.mediaConnectionDetails " +
            " left join rf.folderExcludeRules where rf.id = :id and rf.deleted = false")
    RootFolder findOneJoinDocStoreMediaConnection(@Param("id") Long id);

    @Query(value = "select rf from RootFolder rf" +
            " left join rf.docStoreInclude" +
            " left join rf.folderExcludeRules where rf.path = :path and rf.deleted = false")
    RootFolder findOneByPathJoinDocStore(@Param("path") String rootFolderPath);

    @Query(value = "select rf.id from RootFolder rf" +
            " where (lower(rf.realPath) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
            " or lower(rf.nickName) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%" +
            " or lower(rf.mailboxGroup) like %:namingSearchTerm%) and rf.deleted = false")
    List<Long> findLikeNaming(@Param("namingSearchTerm") String namingSearchTerm);

    @Query(value = "select rf from RootFolder rf" +
            " where (lower(rf.realPath) like %:namingSearchTerm% " +
            "or lower(rf.path) like %:namingSearchTerm%) " +
            "and rf.deleted = false")
    List<RootFolder> findLikePath(@Param("namingSearchTerm") String namingSearchTerm);

    @Query(value = "select rf from RootFolder rf" +
            " left join rf.docStoreInclude" +
            " left join rf.folderExcludeRules where rf.deleted = false")
    List<RootFolder> findAllJoinDocStore();

    @Query(value = "select rf from RootFolder rf" +
            " left join rf.docStoreInclude" +
            " left join rf.folderExcludeRules where rf.id in :ids and rf.deleted = false")
    List<RootFolder> findByIdsJoinDocStore(@Param("ids") Collection<Long> rootIds);

    @Query(value = "select rf from RootFolder rf where rf.id in :ids and rf.deleted = false")
    List<RootFolder> findByIds(@Param("ids") Collection<Long> rootIds);

    @Query("Select rf.id from RootFolder rf where rf.deleted = false")
    List<Long> getAllRootFolderIds();

    @Query("SELECT rf FROM RootFolder rf WHERE rf.id in :ids and rf.deleted = false ORDER BY " +
            "ifnull(rf.nickName, ifnull(ifnull(substring_index(rf.realPath, '/',-1), substring_index(rf.realPath, '\\\\',-1)), rf.mailboxGroup))")
    Page<RootFolder> getAllRootFolderIdsSorted(@Param("ids") Collection<Long> rootIds, Pageable pageRequest);

    @Query("Select rf.id from RootFolder rf where rf.mediaConnectionDetails.id = :mediaConnectionDetailsId and rf.deleted = false")
    List<Long> getRootFolderIdsByMediaConnectionId(@Param("mediaConnectionDetailsId") Long mediaConnectionId);

    @Query("Select rf.id from RootFolder rf where rf.deleted = true")
    List<Long> getDeletedRootFolderIds();

    @Query("Select count(rf) from RootFolder rf where rf.customerDataCenter.id = :customerDataCenterId and rf.deleted = false")
    long countRootfoldersUnderCustomerDataCenter(@Param("customerDataCenterId") Long customerDataCenterId);

    @Query("Select rf from RootFolder rf where rf.customerDataCenter.id = :customerDataCenterId and rf.deleted = false")
    List<RootFolder> getRootfoldersUnderCustomerDataCenter(@Param("customerDataCenterId") Long customerDataCenterId);

    @Query(nativeQuery = true,
            value =  "SELECT cr.run_status, cr.pause_reason, count(*) AS cnt " +
            " FROM root_folder rf " +
            " LEFT JOIN crawl_runs AS cr ON (rf.last_run_id = cr.id)" +
            " WHERE rf.deleted = false" +
            " GROUP BY cr.run_status, cr.pause_reason")
    List<Object[]> countRootFoldersPerRunStatus();

    @Query(nativeQuery = true,
            value =  "SELECT cr.run_status, cr.pause_reason, count(*) AS cnt " +
                    " FROM root_folder rf " +
                    " LEFT JOIN crawl_runs AS cr ON (rf.last_run_id = cr.id)" +
                    " WHERE (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
                    " or lower(rf.nick_name) like %:namingSearchTerm% or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)" +
                    " and rf.deleted = false" +
                    " GROUP BY cr.run_status, cr.pause_reason")
    List<Object[]> countRootFoldersPerRunStatus(@Param("namingSearchTerm") String namingSearchTerm);

    @Query(nativeQuery = true, value = "select rf.* from root_folder rf where rf.real_path like :path% ESCAPE '|' and rf.deleted = false")
    List<RootFolder> findByPathPrefix(@Param("path") String path);

    @Query("SELECT rf FROM RootFolder rf WHERE realPath in :paths and rf.deleted = false")
    List<RootFolder> findByPaths(@Param("paths") List<String> paths);

    @Query("SELECT rf FROM RootFolder rf WHERE mailboxGroup = :mailboxGroup and rf.deleted = false")
    List<RootFolder> findByMailboxGroup(@Param("mailboxGroup") String mailboxGroup);

	//using  this because of race condition discovered in bug 5882
	@Modifying
	@Transactional
	@Query(value="UPDATE root_folder SET is_accessible=:accessible, accessability_last_cng_date = :lastChangeDate WHERE id = :id", nativeQuery=true)
	void updateAccessability(@Param("accessible")int accessible,  @Param("lastChangeDate")long lastChangeDate, @Param("id") long id);

    @Modifying
    @Transactional
    @Query(value="UPDATE RootFolder rf SET rf.deleted = true WHERE rf.id = :id")
    void setDeleted(@Param("id") long id);

    @Modifying
    @Query(value="UPDATE RootFolder rf SET rf.failureGettingSharePermission = :sharePermissionFailure WHERE rf.id = :id")
    void setSharePermissionFailure(@Param("id") long id, @Param("sharePermissionFailure") boolean sharePermissionFailure);

    @Query("select count(rf) from RootFolder rf where rf.departmentId = :departmentId and rf.deleted = false")
    long countUnDeletedDepartmentAssociations(@Param("departmentId") Long departmentId);
}
