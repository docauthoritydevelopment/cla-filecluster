package com.cla.filecluster.repository.solr.infra;

import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.common.SolrInputDocument;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Util methods for building Solr requests
 * Created by vladi on 7/4/2017.
 */
@SuppressWarnings("WeakerAccess")
public class SolrRepositoryUtils {

    public static final String SOLR_UPDATE_FIELD_OP = "set";

    /**
     * Create SolrInputDocument object which, when applied, will perform partial update of a document field.
     * Equivalent of SQL: update [some_table] set updateKey = updateVal where some_table.id = id
     *
     * <br/><br/>
     *
     * @param id value of the ID field to match
     * @param updateKey field to be updated
     * @param updateVal value to be set to the updated field
     *
     * @return Solr input document object
     */
    public static SolrInputDocument updateFieldInputDocument(Object id, String updateKey, String updateVal) {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", id);
        doc.setField(updateKey, setValueMap(updateVal));
        return doc;
    }

    public static Map<String, Object> setValueMap(Object value) {
        HashMap<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put(SOLR_UPDATE_FIELD_OP, value);
        return fieldModifier;
    }

    public static String createInClause(String field, final Collection<String> values) {
        return createInClause(field, values, false);
    }

    public static String createInClause(String field, final Collection<String> values, boolean isPrefixSearch) {
        if (values.size() > 1) {
            ArrayList<String> ordered = Lists.newArrayList(values);
            Collections.sort(ordered); // order consistency needed for tests
            if (isPrefixSearch) {
                return ordered.stream().collect(Collectors.joining(
                        FileCatUtils.CATEGORY_SEPARATOR+"* ",
                        field + ":(",
                        FileCatUtils.CATEGORY_SEPARATOR+"*)"));
            }
            return ordered.stream().collect(Collectors.joining("\" \"",field + ":(\"","\")"));
//            return field + ":(" +
//                    Joiner.on(" ").join(ordered) +
//                    ")";
        } else {
            Optional<String> first = values.stream().findFirst();
            if (isPrefixSearch) {
                return first.isPresent() ? (field + ":" + first.get()+FileCatUtils.CATEGORY_SEPARATOR+"*") : (field + ":*");
            }
            return first.isPresent() ? (field + ":\"" + first.get() + "\"") : (field + ":*");
//            return first.isPresent() ? (field + ":" + first.get()) : (field + ":*");
        }
    }

}
