package com.cla.filecluster.repository.jpa.filetree;

import com.cla.filecluster.domain.entity.filetree.EntityCredentials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * database access to the entity credentials table
 * Created by yael on 11/9/2017.
 */
public interface EntityCredentialsRepository extends JpaRepository<EntityCredentials, Long>, JpaSpecificationExecutor<EntityCredentials> {

    @Query(value = "select df from EntityCredentials df where df.entityId = :entityId and df.entityType = :entityType")
    List<EntityCredentials> findForEntity(@Param("entityId") Long entityId, @Param("entityType") String entityType);

    @Query(value = "select df from EntityCredentials df where df.id in :entityIds and df.entityType = :entityType")
    List<EntityCredentials> findForEntities(@Param("entityIds") List<Long> entityIds, @Param("entityType") String entityType);

    @Modifying
    @Query(value = "delete from EntityCredentials df where df.entityId in :entityIds and df.entityType = :entityType")
    int deleteForEntityIds(@Param("entityIds") List<Long> entityIds, @Param("entityType") String entityType);
}
