package com.cla.filecluster.repository.jpa.security;

import com.cla.common.domain.dto.security.WorkGroupDto;
import com.cla.filecluster.domain.entity.security.WorkGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface WorkGroupRepository extends JpaRepository<WorkGroup, Long> {

    @Query("select wg from WorkGroup wg where wg.name = :name")
    WorkGroup findWorkGroupByName(@Param("name") String name);
}
