package com.cla.filecluster.repository.jpa.actions;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.filecluster.domain.entity.actions.ActionClassExecutorDefinition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by shtand on 15/02/2016.
 */
public interface ActionClassExecutorDefinitionRepository extends JpaRepository<ActionClassExecutorDefinition,String> {

    @Query("select aced from ActionClassExecutorDefinition aced where aced.actionClass = :actionClass")
    List<ActionClassExecutorDefinition> findByActionClass(@Param("actionClass") ActionClass actionClass);
}
