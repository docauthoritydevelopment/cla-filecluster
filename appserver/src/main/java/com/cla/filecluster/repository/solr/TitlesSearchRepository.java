package com.cla.filecluster.repository.solr;

import com.cla.common.domain.dto.pv.TitleToken;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.NamedList;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

@Repository
public class TitlesSearchRepository extends SolrRepository {

    @Value("${groupNaming.minTitleTokenLength:4}")
    private int minTitleTokenLength;

    @Value("${groupNaming.titles.autoCommitMs:600000}")
    private int groupNamingAutoCommitMs;

    @Value("${titlesNaming.solr.core:Titles}")
    private String titlesSolrCore;

    private static final String TITLE_SHIGLE_FIELD = "textShingle";

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    protected String coreName() {
        return titlesSolrCore;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    public void deleteAllForAccount() {
        try {
            logger.info("Delete Titles index documents ..." );
            client.deleteByQuery(SolrQueryGenerator.ALL_QUERY);
            client.commit();
        } catch (Exception e) {
            throw new RuntimeException("Could not delete Titles", e);
        }
    }

    public List<TitleToken> getTermVector(final String groupId, final int numOfFiles, final double skipTermsThreshold, final Map<String, TitleToken> groupTokensMap) {
        if (numOfFiles <= 0) {
            logger.debug("Missing number of files in group {}", groupId);
            return null;
        }
        try {
            // Adding terms for 2 word phrases
            final SolrQuery qterms = new SolrQuery();
            qterms.setTerms(true);
            qterms.set("tv.fl", TITLE_SHIGLE_FIELD);
            qterms.setRequestHandler("/tvrh");
            // qterms.setTermsLimit(20);
            // qterms.addTermsField("titles");
            qterms.setQuery("+groupId:" + groupId);
            final List<TitleToken> tokens = new LinkedList<>();
            if (client.isSolrCloudClient()) {
                qterms.add("shards.qt", "/tvrh");
            }
            final QueryResponse query = client.query(qterms, SolrRequest.METHOD.POST);
            final NamedList<Object> queryResponse = query.getResponse();
            int minTfThreshold = 0;
            SolrDocumentList results = query.getResults();
            if (results.isEmpty() || results.get(0) == null) {
                logger.debug("No titles found for group {}", groupId);
                // The file name/path processing will be done with the Groups Solr core.
                return tokens;
            }
            // Convert results to be id accessible collection
            final Map<String, SolrDocument> resultsMap = new HashMap<>(results.size());
            results.forEach(t -> resultsMap.put((String) t.get("id"), t));
            results = null;    // free resources

            minTfThreshold = (int) (numOfFiles * skipTermsThreshold);
            final NamedList<Object> namedList = (NamedList<Object>) queryResponse.get("termVectors");
            final Iterator<Entry<String, Object>> termVectorsIter = namedList.iterator();
//			final Entry<String,Object> firstEntry = termVectorsIter.next();
//			if (!firstEntry.getKey().equals("uniqueKeyFieldName") || !firstEntry.getValue().equals("id")) {
//				logger.error("Solr TV schama change expecting uniqueKeyFieldName=id. Got {}", firstEntry.toString());
//				throw new RuntimeException("Solr TV schama change expecting uniqueKeyFieldName=id. Got " + firstEntry.toString());
//			}
            while (termVectorsIter.hasNext()) {
                processTermVector(groupId, tokens, numOfFiles, minTfThreshold, termVectorsIter.next(), resultsMap, groupTokensMap);
            }

//			final Comparator<TitleToken> comparator = Collections.reverseOrder();
//			Collections.sort(tokens, comparator);
            return tokens;

        } catch (final SolrServerException e) {
            throw new RuntimeException("Failed on search with searchQuery:" + groupId, e);
        } catch (IOException e) {
            throw new RuntimeException("IO failure on search with searchQuery:" + groupId, e);
        } catch (Exception e) {
            throw new RuntimeException("Unexpected failure on search with searchQuery:" + groupId, e);
        }
    }

    private void processTermVector(final String groupId, final List<TitleToken> tokens, final int numOfFiles,
                                   final int minTfThreshold, final Entry<String, Object> termVectorsEntry,
                                   final Map<String, SolrDocument> resultsMap, final Map<String, TitleToken> groupTokensMap) {

        if (!(termVectorsEntry.getValue() instanceof NamedList)) {
            logger.error("Unexpected termVectorEntry type {}", termVectorsEntry.getValue().getClass().toGenericString());
            return;
        }
        final NamedList<Object> nl = (NamedList) termVectorsEntry.getValue();
        final String titleId = termVectorsEntry.getKey();
//		final String titleId = (String)nl.get("uniqueKey");
        final SolrDocument titleRecord = resultsMap.get(titleId);
        if (titleRecord == null) {
            logger.error("Could not find title id={} tv(0)={}", titleId, ((NamedList) nl.get(TITLE_SHIGLE_FIELD)).getVal(0));
            return;
        }
        Object o = titleRecord.getFieldValue("titleRate");
        if (o == null || !(o instanceof Integer)) {
            logger.error("missing or wrong titleRate value {}", titleRecord.toString());
            throw new RuntimeException("missing or wrong titleRate value");
        }
        final int titleRate = (Integer) o;
        o = titleRecord.getFieldValue("titleLen");
        if (o == null || !(o instanceof Integer)) {
            logger.error("missing or wrong titleLen value {}", titleRecord.toString());
            throw new RuntimeException("missing or wrong titleRate value");
        }
        final int titleLen = (Integer) o;
        o = titleRecord.getFieldValue("docOrder");
        if (o == null || !(o instanceof Integer)) {
            logger.error("missing or wrong docOrder value {}", titleRecord.toString());
            throw new RuntimeException("missing or wrong titleRate value");
        }
        final int docOrder = (Integer) o;

        // TODO: add titles solrDoc collection as a parameter + pass entry down to processing
        // Get all TV items and calc best candidate term for each titleId based on TF value and term length.
        // Consider linguistic score analysis in the future.
        final NamedList tv = (NamedList) nl.get(TITLE_SHIGLE_FIELD);
        if (titleId != null && tv != null) {
            for (final Iterator tvInfoIt = tv.iterator(); tvInfoIt.hasNext(); ) {

                processTvInfo(titleId, groupId, tokens, numOfFiles, minTfThreshold, (Entry) tvInfoIt.next(), titleRate, titleLen, docOrder, groupTokensMap);
            }
        }
    }


    private void processTvInfo(final String titleId, final String groupId, final List<TitleToken> tokens, final int numOfFiles,
                               final int minTfThreshold, final Entry tvInfo, final int titleRate, final int titleLen, final int docOrder, final Map<String, TitleToken> groupTokensMap) {
        if (!(tvInfo.getValue() instanceof NamedList)) {
            logger.error("Unexpected tvInfo type {}", tvInfo.getValue().getClass().toGenericString());
            return;
        }
        final NamedList<Object> tv = (NamedList) tvInfo.getValue();
//		final Object tfIdfObj = tv.get("tf-idf");
//		final Object tfObj = tv.get("tf");
//		if (tfIdfObj==null || tfObj==null || !(tfIdfObj instanceof Double) || !(tfObj instanceof Integer)) {
//			logger.error("Unexpected tv record key={} val={}", tvInfo.getKey(), tv.toString());
//			return;
//		}
//		int tf = (Integer)tfObj;
//		double tfIdf = (Double)tfIdfObj;
        final String term = (String) tvInfo.getKey();
        final TitleToken tokenGroupInfo = groupTokensMap.get(term);
        if (tokenGroupInfo == null) {
            logger.trace("Could not find tokenGroupInfo for {}", term);
            return;
        }
        final int tf = tokenGroupInfo.getTf();
        final double tfIdf = tokenGroupInfo.getTfidf();
        logger.trace("Term info {}: {} {}", term, tf, tfIdf);
        // TODO !!!! : TF and tf-IDF values are wrong (!) - need to get them from the Groups core or use faceted search to count them within the scope of the group
        final TitleToken titleToken = new TitleToken(term, tfIdf, tf, titleRate, titleLen, docOrder);
        if (minTfThreshold == 0 || titleToken.getTf() >= minTfThreshold) {
            if (numOfFiles > 0 && titleToken.getTf() > numOfFiles) {
                logger.trace("Adjust {} for groupId {} as tf> {}", titleToken, groupId, numOfFiles);
                final double fixFactor = ((double) numOfFiles) / titleToken.getTf();
                titleToken.setTfidf(titleToken.getTfidf() / (fixFactor));
                titleToken.setTf(numOfFiles);
            } else if (titleToken.getToken().length() >= minTitleTokenLength) {
                tokens.add(titleToken);
            }
        } else {
            logger.trace("Skip {} for groupId {} as getTf()={}<{}", titleToken, groupId, titleToken.getTf(), minTfThreshold);
        }
    }

    private static final String proposedTitleKeySeparator = ":";
    private static final String titleKeyInternalSeparator = "=";

    public List<SolrInputDocument> addFileToGroupDocument(final FileGroup group, final List<String> titlesCollection) {
        List<SolrInputDocument> tDocs = new ArrayList<>(titlesCollection.size());
        try {
            for (int i = 0; i < titlesCollection.size(); ++i) {
                final String t = titlesCollection.get(i);
                final String[] sp1 = t.split(proposedTitleKeySeparator, 2);
                if (sp1.length < 2) {
                    logger.error("Unexpected split length {} of {} with {}", sp1.length, t, proposedTitleKeySeparator);
                    continue;
                }
                final String key = sp1[0];
                final String value = sp1[1];
                final String[] sp2 = key.split(titleKeyInternalSeparator, 3);

                if (logger.isTraceEnabled()) {
                    logger.trace("Title token details: {},{},{},{},{},{}", group.getId(), sp2[0], (sp2.length < 2) ? "" : sp2[1], (sp2.length < 3) ? "" : sp2[2], key, value);
                }
                if (sp2.length < 2 || sp2[1].isEmpty() || sp2[2].isEmpty()) {
                    logger.error("Unexpected title key. Text={} key={}", value, key);
                    continue;
                }
                int titleRate;
                int docOrder;
                try {
                    titleRate = (sp2.length < 2) ? 7 : Integer.parseInt(sp2[1]);
                    docOrder = (sp2.length < 3) ? i : Integer.parseInt(sp2[2]);
                } catch (final Exception e) {
                    logger.error("Couldnot parse key. Text={} key={}. {}", value, key, e);
                    continue;
                }

                final SolrInputDocument doc = new SolrInputDocument();
                doc.setField("id", UUID.randomUUID().toString());
                doc.setField("groupId", group.getId());
//				doc.setField("text", value);
                doc.setField("text", key);    // debug
                doc.setField("textShingle", value);
                doc.setField("docOrder", docOrder);
                doc.setField("titleRate", titleRate);
                doc.setField("titleLen", value.length());
//				titlesSolrClient.add(doc);
                tDocs.add(doc);
            }
        } catch (final Exception e) {
            throw new RuntimeException("Could not save Title", e);
        }
        return tDocs;
    }

    public void writeDocuments(Collection<SolrInputDocument> documents) {
        if (documents == null || documents.size() == 0) {
            return;
        }
        try {
            logger.debug("Send {} documents to Solr", documents.size());
            UpdateResponse response = client.add(documents, groupNamingAutoCommitMs);
            if (response == null) {
                throw new RuntimeException("Failed to create titles in Solr.");
            }
            logger.trace("Got response from solr: {}", response);
        } catch (Exception e) {
            logger.error("Failed to create titles in Solr", e);
            StringJoiner stringJoiner = new StringJoiner(",");
            documents.forEach(f -> stringJoiner.add((f.getField("id").toString())));
            logger.error("Failed file ids: {}", stringJoiner.toString());
            throw new RuntimeException("Failed to create titles in Solr.", e);
        }
    }

    public void deleteByGroupId(String group) {
        try {
            logger.debug("Delete title indexes for {}", group);
            client.deleteByQuery("groupId:" + group);
        } catch (Exception e) {
            throw new RuntimeException("Could not delete Groups", e);
        }
    }

    public void deleteByGroupIds(Collection<String> groupIds) {
        if (groupIds == null || groupIds.isEmpty()) {
            return;
        }

        StringJoiner deleteQueryStringJoiner = new StringJoiner(" ", "groupId:(", ")");    // query: id:(id1 id2 id3 ...)
        groupIds.forEach(cid -> deleteQueryStringJoiner.add(cid));
        try {
            logger.trace("Delete title indexes for groupIds: {}", deleteQueryStringJoiner);
            client.deleteByQuery(deleteQueryStringJoiner.toString());
        } catch (Exception e) {
            throw new RuntimeException("Could not delete title indexes for groupIds (" + deleteQueryStringJoiner.toString() + ")", e);
        }
    }

    public void add(List<SolrInputDocument> titleSolrDocs) {
        try {
            client.add(titleSolrDocs);
        } catch (Exception e) {
            throw new RuntimeException("Could not add documents", e);
        }
    }

}
