package com.cla.filecluster.repository.jpa.alert;

import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.filecluster.domain.entity.alert.Alert;
import com.cla.filecluster.repository.jpa.PartitionManagement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
public interface AlertRepository extends JpaRepository<Alert, Long>, PartitionManagement {

    @Query(value = "select a from Alert a" +
            " where a.id = :id")
    Alert getById(@Param("id") Long id);

    @Query(value = "select a from Alert a" +
            " where a.acknowledged = false")
    Page<Alert> findAllNotAcknowledged(Pageable pageable);

    @Query(value = "select a from Alert a" +
            " where a.acknowledged = false and a.severity = :severity")
    Page<Alert> findAllNotAcknowledgedBySeverity(
            @Param("severity") AlertSeverity severity, Pageable pageable);

    @Query(value = "select a from Alert a" +
            " where a.acknowledged = false and a.eventType = :eventType")
    Page<Alert> findAllNotAcknowledgedByType(
            @Param("eventType") EventType type, Pageable pageable);

    @Query(value = "select a from Alert a" +
            " where a.acknowledged = false and a.severity = :severity and a.eventType = :eventType")
    Page<Alert> findAllNotAcknowledgedBySeverityAndType(
            @Param("severity") AlertSeverity severity, @Param("eventType") EventType type, Pageable pageable);

    @Query(nativeQuery=true, value="select get_last_partition_name('alerts')")
    String getLastPartitionName();

    @Query(nativeQuery=true, value="select get_last_partition_date('alerts')")
    Long getLastPartitionDate();

    @Query(nativeQuery=true, value="select get_first_partition_name('alerts')")
    String getFirstPartitionName();

    @Query(nativeQuery=true, value="select get_first_partition_date('alerts')")
    Long getFirstPartitionDate();

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE alerts ADD PARTITION (PARTITION p:partitionId VALUES LESS THAN (:partitionDate))")
    void createPartition(@Param("partitionId") long partitionId, @Param("partitionDate") long partitionDate);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE alerts TRUNCATE PARTITION p:partitionId")
    void truncatePartitionForPartitionId(@Param("partitionId") long partitionId);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE alerts DROP PARTITION p:partitionId")
    void deletePartitionForPartitionId(@Param("partitionId") long partitionId);
}
