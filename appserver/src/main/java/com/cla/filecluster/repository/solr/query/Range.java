package com.cla.filecluster.repository.solr.query;

public class Range {
    private Object fromValue;
    private Object toValue;
    private SolrOperator fromOp = SolrOperator.GT;
    private SolrOperator toOp = SolrOperator.LT;

    private Range(Object fromValue, Object toValue) {
        this.fromValue = fromValue;
        this.toValue = toValue;
    }

    public static Range create(Object fromValue, Object toValue){
        return new Range(fromValue, toValue);
    }

    public Range fromOp(SolrOperator fromOp) {
        this.fromOp = fromOp;
        return this;
    }

    public Range toOp(SolrOperator toOp) {
        this.toOp = toOp;
        return this;
    }

    public Object getFromValue() {
        return fromValue;
    }

    public Object getToValue() {
        return toValue;
    }

    public SolrOperator getFromOp() {
        return fromOp;
    }

    public SolrOperator getToOp() {
        return toOp;
    }

    @Override
    public String toString() {
        String range = (fromOp == SolrOperator.GT) ? "[" : "{";
        range += fromValue == null ? "*" : fromValue;
        range += " TO ";
        range += toValue == null ? "*" : toValue;
        range += (toOp == SolrOperator.LT) ? "]" : "}";
        return range;
    }
}
