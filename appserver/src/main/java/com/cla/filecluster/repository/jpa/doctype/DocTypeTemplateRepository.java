package com.cla.filecluster.repository.jpa.doctype;

import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.doctype.DocTypeTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by uri on 17/08/2015.
 */

public interface DocTypeTemplateRepository extends JpaRepository<DocTypeTemplate, Long>, JpaSpecificationExecutor<DocTypeTemplate> {

    @Query("select dtt from DocTypeTemplate dtt where name=:name")
    DocTypeTemplate findByName(@Param("name") String name);
}
