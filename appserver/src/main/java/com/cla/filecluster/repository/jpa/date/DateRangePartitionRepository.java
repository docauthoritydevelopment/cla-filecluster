package com.cla.filecluster.repository.jpa.date;

import com.cla.filecluster.domain.entity.date.DateRangePartition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by uri on 05/06/2016.
 */
public interface DateRangePartitionRepository extends JpaRepository<DateRangePartition, Long> {
    @Query("select drp from DateRangePartition drp" +
            " left join drp.simpleDateRangeItems sdr where drp.id = :id order by sdr.orderValue")
    DateRangePartition findOneWithItems(@Param("id") long partitionId);
}
