package com.cla.filecluster.repository.jpa.actions;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 25/01/2016.
 */
public interface ActionDefinitionRepository extends JpaRepository<ActionDefinition,String> {

    @Query("select ad from ActionDefinition ad where name like :name")
    ActionDefinition findByName(@Param("name") String name);

    @Query("select ad from ActionDefinition ad where ad.actionClass in :actionClassList")
    Page<ActionDefinition> findAllByActionClassList(@Param("actionClassList") List<ActionClass> actionClasses, Pageable pageRequest);

}
