package com.cla.filecluster.repository.file;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by shtand on 16/08/2015.
 */
public class FolderWithFiles extends MediaFolderWithFiles {
    public static final String ATTRIBUTE_SIZE = "size";
    public static final String ATTRIBUTE_LAST_MODIFIED = "lastModifiedTime";
    public static final String ATTRIBUTE_LAST_ACCESS = "lastAccessTime";

    private final List<Path> files = new ArrayList<>();
    private final Path path;

    private int childFolderCount;

    public FolderWithFiles(Path path) {
        super(path.toString());
        this.path = path;
    }

    public void addFile(Path p) {
        files.add(p);
    }

    public List<Path> getFiles() {
        return files;
    }

    public Path getPath() {
        return path;
    }

    @Override
	public String toString() {
		return "{path=" + path + ", files("+files.size()+")=" + files + "}";
	}

    public int getChildFolderCount() {
        return childFolderCount;
    }

    public void increaseChildFolderCount() {
        childFolderCount++;
    };

}
