package com.cla.filecluster.repository.solr.query;

import com.cla.common.constants.SolrCoreSchema;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.common.SolrInputDocument;

import java.util.List;
import java.util.Map;

public class AtomicUpdateBuilder {
    private String idName;
    private String idValue;
    private List<Criteria> modifiers = Lists.newLinkedList();

    public static AtomicUpdateBuilder create(){
        return new AtomicUpdateBuilder();
    }

    public AtomicUpdateBuilder setId(String idName, String idValue){
        this.idName = idName;
        this.idValue = idValue;
        return this;
    }

    public AtomicUpdateBuilder setId(SolrCoreSchema idField, Object idValue){
        this.idName = idField.getSolrName();
        this.idValue = idValue.toString();
        return this;
    }

    public AtomicUpdateBuilder addModifier(SolrCoreSchema field, SolrOperator op, Object value){
        modifiers.add(Criteria.create(field, op, value));
        return this;
    }

    public AtomicUpdateBuilder addModifierNotNull(SolrCoreSchema field, SolrOperator op, Object value){
        if (value != null) {
            modifiers.add(Criteria.create(field, op, value));
        }
        return this;
    }

    public AtomicUpdateBuilder addModifier(Criteria mod){
        modifiers.add(mod);
        return this;
    }

    public SolrInputDocument build(){
        if (idName == null || idValue == null){
            throw new IllegalArgumentException("Missing mandatory ID parameter.");
        }
        SolrInputDocument doc = new SolrInputDocument();
        doc.addField(idName, idValue);
        modifiers.forEach(field -> doc.addField(field.getField().getSolrName(), modifier(field)));
        return doc;
    }

    private Map<String,Object> modifier(Criteria criteria) {
        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put(criteria.getOp().getOpName(), criteria.getValue());
        return fieldModifier;
    }
}
