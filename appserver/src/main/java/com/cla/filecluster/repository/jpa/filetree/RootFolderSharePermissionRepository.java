package com.cla.filecluster.repository.jpa.filetree;

import com.cla.filecluster.domain.entity.filetree.RootFolderSharePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by: yael
 * Created on: 9/16/2019
 */
public interface RootFolderSharePermissionRepository extends JpaRepository<RootFolderSharePermission, Long>, JpaSpecificationExecutor<RootFolderSharePermission> {

    @Query("Select rfsp from RootFolderSharePermission rfsp where rfsp.rootFolderId = :rootFolderId")
    List<RootFolderSharePermission> getForRootFolder(@Param("rootFolderId") Long rootFolderId);

    @Modifying
    @Query(value = "delete from RootFolderSharePermission rfsp where rfsp.rootFolderId = :rootFolderId")
    int deleteForRootFolder(@Param("rootFolderId") Long rootFolderId);
}
