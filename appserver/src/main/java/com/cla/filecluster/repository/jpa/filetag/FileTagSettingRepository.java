package com.cla.filecluster.repository.jpa.filetag;

import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by: yael
 * Created on: 9/5/2018
 */
public interface FileTagSettingRepository extends JpaRepository<FileTagSetting, Long>, JpaSpecificationExecutor<FileTagSetting> {

    @Query(value = "select dt from FileTagSetting dt where dt.fileTag.id = :fileTagId")
    List<FileTagSetting> findByFileTag(@Param("fileTagId") long fileTagId);
}
