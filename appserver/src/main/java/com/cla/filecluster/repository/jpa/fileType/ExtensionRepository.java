package com.cla.filecluster.repository.jpa.fileType;

import com.cla.filecluster.domain.entity.filetype.Extension;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ophir on 25/03/2019.
 */
public interface ExtensionRepository extends JpaRepository<Extension,Long> {
    @Query("select ex from Extension ex where name = :name")
    List<Extension> findByName(@Param("name") String name);
}
