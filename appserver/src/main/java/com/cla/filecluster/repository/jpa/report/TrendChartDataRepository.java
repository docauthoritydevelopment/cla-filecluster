package com.cla.filecluster.repository.jpa.report;

import com.cla.filecluster.domain.entity.report.TrendChartData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by uri on 27/07/2016.
 */
public interface TrendChartDataRepository extends JpaRepository<TrendChartData,Long> , JpaSpecificationExecutor<TrendChartData> {

    @Modifying
    @Query("delete from TrendChartData tcd where tcd.trendChart.id = :trendChartId")
    int deleteByChartId(@Param("trendChartId") long trendChartId);
}
