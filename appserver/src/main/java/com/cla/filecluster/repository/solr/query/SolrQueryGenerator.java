package com.cla.filecluster.repository.solr.query;

import com.cla.common.constants.SolrCoreSchema;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.common.params.FacetParams;
import org.springframework.data.domain.Sort;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.cla.filecluster.repository.solr.query.SolrOperator.IN_RANGE;

public class SolrQueryGenerator {

    public static final String ALL = "*";
    public static final String ALL_QUERY = "*:*";
    private static final String SEPARATOR = ":";
    private static final String RANGE_OP = " TO ";
    private static final String RANGE_FROM = "[";
    private static final String RANGE_FROM_OPEN = "{";
    private static final String RANGE_TO = "]";
    private static final String RANGE_TO_OPEN = "}";
    private static final String NEGATE = "-";
    public static final String ALL_RANGE = "[* TO *]";
    private static final DateTimeFormatter SOLR_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    public static SolrQuery generate(Query query){
        SolrQuery solrQuery = new SolrQuery();

        String[] fields = query.getFields().stream()
                .map(SolrCoreSchema::getSolrName)
                .collect(Collectors.toList())
                .toArray(new String[]{});
        solrQuery.setFields(fields);

        solrQuery.setQuery(generateString(query));

        query.getFilterQueries().forEach(qu -> solrQuery.addFilterQuery(generateFilterQueries(qu)));

        if (query.getRows() != null){
            solrQuery.setRows(query.getRows());
        }

        if (query.getStart() != null){
            solrQuery.setStart(query.getStart());
        }

        if (query.getCursorMark() != null) {
            solrQuery.set("cursorMark", query.getCursorMark());
        }

        if (query.getRequestHandler() != null){
            solrQuery.setRequestHandler(query.getRequestHandler());
        }

        if (!query.getOtherParams().isEmpty()) {
            for (Map.Entry<String, String> e : query.getOtherParams().entrySet()) {
                solrQuery.set(e.getKey(), e.getValue());
            }
        }

        if (query.getFacetFields() != null && !query.getFacetFields().isEmpty()){
            solrQuery.setFacet(true);
            List<String> facetFields = query.getFacetFields().stream().map(SolrCoreSchema::getSolrName).collect(Collectors.toList());
            if (query.isFacetFieldsAsOne()) {
                solrQuery.addFacetField(StringUtils.join(facetFields, ','));
            } else {
                solrQuery.addFacetField(facetFields.toArray(new String[0]));
            }
        }

        if (query.getFacetQuery() != null && !query.getFacetQuery().isEmpty()) {
            solrQuery.setFacet(true);
            query.getFacetQuery().forEach(qu -> solrQuery.addFacetQuery(generateFilterQueries(qu)));
        }

        if (query.getJsonFacet() != null) {
            solrQuery.setFacet(true);
            String jsonFacet = query.getJsonFacet().toFuncQueryString();
            if (!jsonFacet.startsWith("{")) {
                jsonFacet = "{" + jsonFacet + "}";
            }
            solrQuery.setParam("json.facet", jsonFacet);
        } else if (!query.getFacetJsonObjectList().isEmpty()) {
            String jsonFacet = query.getFacetJsonObjectList().stream().map(SolrFacetQueryJson::toFuncQueryString)
                    .collect(Collectors.joining(", ","{", "}"));
            solrQuery.setParam("json.facet", jsonFacet);
        }

        if (query.getSortFields() != null && !query.getSortFields().isEmpty()) {
            for (Map.Entry<SolrCoreSchema, Sort.Direction> entry : query.getSortFields().entrySet()) {
                SolrQuery.ORDER order = entry.getValue() == Sort.Direction.ASC ? SolrQuery.ORDER.asc : SolrQuery.ORDER.desc;
                solrQuery.addSort(entry.getKey().getSolrName(), order);
            }
        }

        if (query.getFacetPivot() != null && !query.getFacetPivot().isEmpty()) {
            solrQuery.setFacet(true);
            List<String> pivots = query.getFacetPivot().stream().map(SolrCoreSchema::getSolrName).collect(Collectors.toList());
            if (query.isFacetPivotAsOne()) {
                solrQuery.addFacetPivotField(StringUtils.join(pivots, ','));
            } else {
                solrQuery.addFacetPivotField(pivots.toArray(new String[0]));
            }
        }

        if (query.getFacetPivotMinCount() != null) {
            solrQuery.setParam(FacetParams.FACET_PIVOT_MINCOUNT, query.getFacetPivotMinCount().toString());
        }

        if (query.getFacetMinCount() != null) {
            solrQuery.setFacetMinCount(query.getFacetMinCount());
        }

        if (query.getFacetLimit() != null) {
            solrQuery.setFacetLimit(query.getFacetLimit());
        }

        if (query.getFacetOffset() != null) {
            solrQuery.setParam("facet.offset", String.valueOf(query.getFacetOffset()));
        }

        if (query.getGroupField() != null) {
            solrQuery.setParam("group", "true");
            solrQuery.setParam("group.field", query.getGroupField().getSolrName());
            if (query.getGroupLimit() != null) {
                solrQuery.setParam("group.limit", query.getGroupLimit().toString());
            }
            if (query.isNgroupsOn()) {
                solrQuery.setParam("group.ngroups", "true");
            }
            if (!query.getGroupQueries().isEmpty()) {
                query.getGroupQueries().forEach(q -> {
                    solrQuery.setParam("group.query", generateFilterQueries(q));
                });
            }
        }

        if (query.getStatsField() != null) {
            solrQuery.setParam("stats", "true");
            solrQuery.setParam("stats.field", query.getStatsField().getSolrName());
        }

        if (query.getTerms() != null && query.getTerms()) {
            solrQuery.setTerms(true);
        }

        return solrQuery;
    }

    private static String generateFilterQueries(Query query) {
        StringBuilder queryStr = new StringBuilder();
        if (query.getFilterQueries().size() > 0) {
            for (Query q : query.getFilterQueries()) {
                queryStr.append(generateString(q));
            }
        } else {
            queryStr.append(generateString(query));
        }
        return queryStr.toString();
    }

    private static String generateString(Query query){
        StringBuilder queryStr;
        if (query.getCriteria().size() == 0) {
            queryStr = new StringBuilder(ALL_QUERY);
        }
        else {
            //TODO: currently only supports simple queries
            queryStr = new StringBuilder(generateFilter(query.getCriteria().get(0)));
        }
        return queryStr.toString();
    }

    public static String generateFilter(Criteria criteria){

        String filterStr = "";
        if (!criteria.getOp().isLogicalOperator()) {
            filterStr = criteria.getField().getSolrName() + SEPARATOR;
        }
        Object value = criteria.getValue();
        Range range;

        switch (criteria.getOp()){
        	case EQ:
                filterStr += transformValue(criteria.getField(), value);
                break;
            case IN_RANGE:
                if (!(value instanceof Range)){
                    throw new IllegalArgumentException("Operator " + IN_RANGE + " does not match the value type.");
                }
                filterStr += generateRange(criteria.getField(), (Range)value);
                break;
            case GT:
            case GTE:
                range = Range.create(value, null)
						.fromOp(criteria.getOp());
                filterStr += generateRange(criteria.getField(), range);
                break;
            case LT:
            case LTE:
                range = Range.create(null, value)
						.toOp(criteria.getOp());
                filterStr += generateRange(criteria.getField(), range);
                break;
            case NOT_IN:
                filterStr = NEGATE + filterStr;
                //fallthrough to IN...
            case IN:
                if (value instanceof Collection){
                    @SuppressWarnings("unchecked")
                    Collection<Object> values = (Collection<Object>) value;
                    filterStr += values.stream()
                            .map(val -> transformValue(criteria.getField(), val))
                            .collect(Collectors.joining(" ", "(", ")"));
                }
                else{
                    throw new IllegalArgumentException("The value must be a collection for IN operator.");
                }
                break;
            case EXISTS:
            case PRESENT:
                filterStr = filterStr + ALL_RANGE;
                break;
            case MISSING:
                filterStr = "(" + NEGATE + filterStr + ALL_RANGE + ")";
                break;
            case NE:
                filterStr = NEGATE +  filterStr + transformValue(criteria.getField(), value);
                break;
			case OR:
			case AND:
                StringBuilder logicalOpBuilder = new StringBuilder();
                logicalOpBuilder.append("(");
                int subCounter = 0;
                int subSize = criteria.getSubCriteria().size();
                for (Criteria sub : criteria.getSubCriteria()){
                    logicalOpBuilder.append(generateFilter(sub));
                    if (++subCounter < subSize){
                        logicalOpBuilder.append(") ")
                                .append(criteria.getOp().getOpName())
                                .append(" (");
                    }
                }
                logicalOpBuilder.append(")");
                filterStr = logicalOpBuilder.toString();
                break;
            default:
                throw new IllegalArgumentException("Unsupported operation " + criteria.getOp());
        }

        return filterStr;
    }

    public static String transformValue(SolrCoreSchema field, Object value){
        String trans;
        if (value == null) {
            return null;
        } else if (field.getType().equals(Date.class) || field.getType().equals(LocalDateTime.class)){
            trans = transformDate(value);
        }
        else {
            trans = String.valueOf(value);
        }

        return escape(trans);
    }

    public static String escape(String trans) {
        return escape(trans, true, false);
    }

    public static String escape(String trans, boolean escapeSpace, boolean escapeSlash) {
        if (escapeSlash) {
            trans = trans.replace("\\", "\\\\");
        }

        trans = trans.replace(":", "\\:");
        trans = trans.replace("~", "\\~");
        trans = trans.replace("=", "\\=");
        trans = trans.replace("/", "\\/");
        trans = trans.replace("+", "\\+");
        trans = trans.replace("-", "\\-");
        trans = trans.replace("[", "\\[");
        trans = trans.replace("]", "\\]");
        trans = trans.replace(";", "\\;");

        if (escapeSpace) {
            trans = trans.replace(" ", "\\ ");
        }

        return trans;
    }

    public static <T> T retrieveValue(Class<T> type, String value){
        Object result;
        if (Strings.isNullOrEmpty(value)){
            return null;
        }
        else
        if (type.equals(String.class)){
            result = value;
        }
        else
        if (type.equals(Date.class) || type.equals(LocalDateTime.class)){
            return retrieveDate(type, value);
        }
        else
        if (type.equals(Long.class)){
            result = Long.parseLong(value);
        }
        else
        if (type.equals(Integer.class)){
            result = Integer.parseInt(value);
        }
        else
        if (type.equals(Boolean.class)){
            result = Boolean.getBoolean(value);
        }
        else
        if (Enum.class.isAssignableFrom(type)){
            //noinspection unchecked
            result = Enum.valueOf((Class<? extends Enum>)type, value);
        }
        else {
            throw new IllegalArgumentException("Unsupported type: " + type.getCanonicalName());
        }
        return type.cast(result);
    }

    public static String now(){
        return transformDate(LocalDateTime.now());
    }

    private static String transformDate(Object value){
        String dateTimeStr;
        if (value instanceof Long){
            LocalDateTime dateTime = LocalDateTime
                    .ofEpochSecond(TimeUnit.MILLISECONDS.toSeconds((Long) value), 0, ZoneOffset.UTC);
            dateTimeStr = SOLR_DATE_FORMAT.format(dateTime);
        }
        else
        if (value instanceof LocalDateTime){
            dateTimeStr = SOLR_DATE_FORMAT.format((LocalDateTime)value);
        }
        else
        if (value instanceof Date){
            LocalDateTime dateTime = LocalDateTime.ofInstant(((Date)value).toInstant(), ZoneId.of("UTC"));
            dateTimeStr = SOLR_DATE_FORMAT.format(dateTime);
        }
        else{
            throw new IllegalArgumentException("Illegal value type for date field: " + value.getClass().getCanonicalName());
        }
        return dateTimeStr;
    }

    private static <T> T retrieveDate(Class<T> dateType, String value){

        LocalDateTime dateTime = LocalDateTime.parse(value, SOLR_DATE_FORMAT);

        if (Long.class.isAssignableFrom(dateType)){
            return dateType.cast(TimeUnit.SECONDS.toMillis(dateTime.toEpochSecond(ZoneOffset.UTC)));
        }
        else
        if (LocalDateTime.class.isAssignableFrom(dateType)){
            return dateType.cast(dateTime);
        }
        else
        if (Date.class.isAssignableFrom(dateType)){
            Date in = new Date();
            LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
            return dateType.cast(Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant()));
        }
        else{
            throw new IllegalArgumentException("Illegal type for date field: " + value.getClass().getCanonicalName());
        }
    }

    private static String generateRange(SolrCoreSchema field, Range range){
        String rangeStr = (range.getFromOp() == SolrOperator.GT) ? RANGE_FROM_OPEN : RANGE_FROM;
        rangeStr += range.getFromValue() == null ? ALL : transformValue(field, range.getFromValue());
        rangeStr += RANGE_OP;
        rangeStr += range.getToValue() == null ? ALL : transformValue(field, range.getToValue());
        rangeStr += (range.getToOp() == SolrOperator.LT) ? RANGE_TO_OPEN : RANGE_TO;
        return rangeStr;
    }
}
