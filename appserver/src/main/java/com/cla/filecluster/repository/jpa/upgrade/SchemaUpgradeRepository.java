package com.cla.filecluster.repository.jpa.upgrade;

import com.cla.filecluster.domain.entity.security.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shtand on 22/06/2016.
 */
public interface SchemaUpgradeRepository extends JpaRepository<User, Integer> {

//    sql = "CREATE TABLE `schema_upgrade_scripts` (" +
//            "`id` bigint(20) NOT NULL," +
//            "`description` varchar(255) DEFAULT NULL," +
//            "`execution_time` bigint(20) DEFAULT NULL," +
//            "`installed_on` bigint(20) DEFAULT NULL," +
//            "`type` varchar(255) NOT NULL," +
//            "`upgrade_state` varchar(255) DEFAULT NULL," +
//            "PRIMARY KEY (`id`)" +
//            ") ENGINE=InnoDB DEFAULT CHARSET=utf8";

    @Transactional
    @Query( nativeQuery = true,
            value = "select id,type,upgrade_state from schema_upgrade_scripts")
    List<Object[]> showAllScripts();

    @Transactional
    @Query( nativeQuery = true,
            value = "select upgrade_state from schema_upgrade_scripts where id = :id")
    List<Object> showScriptUpgradeState(@Param("id") long id);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "INSERT INTO schema_upgrade_scripts" +
                    " (`id`, `description`, `execution_time`, `installed_on`, `type`, `upgrade_state`)" +
                    " VALUES (:id, :description, :executionTime, :installedOn, 'JAVA', :upgradeState)")
    void markUpgradeSchema(@Param("id") long version,
                           @Param("description") String description,
                           @Param("executionTime") long executionTime,
                           @Param("installedOn") long installedOn,
                           @Param("upgradeState") String upgradeState);
}
