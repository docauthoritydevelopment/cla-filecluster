package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u "
			+ " left join fetch u.roles r "
			+ " left join fetch r.authorities "
            + " left join fetch u.compositeRoles br "
			+ " left join fetch br.template t "
            + " left join fetch t.roles brr "
            + " left join fetch brr.authorities brru "
			+ " where u.username = :username")
	User findUserByUsername(@Param("username") String username);

	@Query("select u from User u "
			+ " left join fetch u.roles r "
            + " left join fetch u.compositeRoles r "
			+ " where u.id = :userId and u.deleted = false")
	User findByIdWithRoles(@Param("userId") long userId);

    @Query("select u from User u "
            + " left join fetch u.roles r "
            + " left join fetch u.compositeRoles r "
            + " where u.id = :userId")
    User findByIdWithRolesWithDeleted(@Param("userId") long userId);

    @Modifying
	@Query("update User u" +
            " set u.passwd = :encodedNewPassword , u.lastPasswordChange = current_date " +
            " where u.id = :userId ")
	void changePassword(@Param("userId") long userId, @Param("encodedNewPassword") String encodedNewPassword);

	@Query( "select u.passwd from User u " +
			"where u.id = :userId")
	String getPasswordById(@Param("userId") long userId);

	@Modifying
	@Query("delete User u")
	void deleteAll();

    @Query( value = "select u from User u " +
            "left join fetch u.roles r " +
            "where u.userType <> com.cla.common.domain.dto.security.UserType.SYSTEM" +
            " and u.deleted = false",
    countQuery = "select count(u) from User u " +
            "where u.userType <> com.cla.common.domain.dto.security.UserType.SYSTEM" +
            " and u.deleted = false")
    Page<User> findAllNonSystem(Pageable pageRequest);

    @Query( "select u from User u " +
            "where u.deleted = false")
    List<User> findAllNotDeleted();

    @Query( value = "select u from User u" +
            " join u.compositeRoles br" +
            " where u.deleted = false" +
            " and br.id = :compRoleId",
    countQuery = "select count(u) from User u join u.compositeRoles br where u.deleted=false and br.id = :compRoleId")
    Page<User> getCompRoleAssignedUsers(@Param("compRoleId") Long compRoleId, Pageable pageRequest);

}
