package com.cla.filecluster.repository.jpa.searchpattern;

import com.cla.filecluster.domain.entity.searchpatterns.PatternCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ophir on 10/04/2019.
 */
public interface PatternCategoryRepository extends JpaRepository<PatternCategory,Integer> {
    @Query("SELECT pc FROM PatternCategory pc WHERE pc.name = :name")
    PatternCategory findByName(@Param("name") String name);

    @Query("SELECT pc FROM PatternCategory pc WHERE pc.name = :name and pc.parent.id = :parentId ")
    PatternCategory findByNameAndParentId(@Param("name") String name, @Param("parentId") Integer parentId);

    @Query("SELECT pc FROM PatternCategory pc WHERE pc.name = :name and pc.parent is NULL")
    PatternCategory findByNameAndNullParentId(@Param("name") String name);

    @Query("SELECT pc FROM PatternCategory pc WHERE pc.parent.id = :parentId ")
    List<PatternCategory> findByParentId(@Param("parentId") Integer parentId);
}
