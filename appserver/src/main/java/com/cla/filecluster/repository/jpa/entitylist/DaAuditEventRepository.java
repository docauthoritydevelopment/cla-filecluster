package com.cla.filecluster.repository.jpa.entitylist;

import com.cla.filecluster.domain.entity.audit.DaAuditEvent;
import com.cla.filecluster.repository.jpa.PartitionManagement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

/**
 * Created by Oren, Itay on 2017-10-10.
 *
 * DaAuditEvent class is for READ-ONLY of the audit table events.
 * Events are created by the EventAuditService using logger DB appender.
 * This entity is created so the table is created and for retrieving items for reporting and BI purposes.
 *
 * Changes in fields/columns should be in line with the columns definition at com.cla.filecluster.service.audit.AuditColumns
 *      and com.cla.filecluster.service.audit.AuditDBAppender
 *
 */
public interface DaAuditEventRepository extends JpaRepository<DaAuditEvent, Long>, PartitionManagement {


    @Query("SELECT ae FROM DaAuditEvent ae ")
    Page<DaAuditEvent> getAll(Pageable pageRequest);

    @Query("SELECT ae FROM DaAuditEvent ae where timeStampMs >= :fromTime and timeStampMs <= :toTime " +
        "and username like :username and subSystem in (:auditType)")
    Page<DaAuditEvent> getAllWithType(@Param("username") String username,
                              @Param("auditType") Collection<String> auditType, @Param("fromTime") Long fromTime,
                              @Param("toTime") Long toTime, Pageable pageRequest);

    @Query("SELECT ae FROM DaAuditEvent ae where timeStampMs >= :fromTime and timeStampMs <= :toTime " +
            "and username like :username and action in (:crudAction)")
    Page<DaAuditEvent> getAllWithAction(@Param("username") String username,
                              @Param("crudAction") Collection<String> crudAction, @Param("fromTime") Long fromTime,
                              @Param("toTime") Long toTime, Pageable pageRequest);

    @Query("SELECT ae FROM DaAuditEvent ae where timeStampMs >= :fromTime and timeStampMs <= :toTime " +
            "and username like :username and action in (:crudAction) and subSystem in (:auditType)")
    Page<DaAuditEvent> getAllWithActionAndType(@Param("username") String username, @Param("auditType") Collection<String> auditType,
                                        @Param("crudAction") Collection<String> crudAction, @Param("fromTime") Long fromTime,
                                        @Param("toTime") Long toTime, Pageable pageRequest);

    @Query("SELECT ae FROM DaAuditEvent ae where timeStampMs >= :fromTime and timeStampMs <= :toTime " +
            "and username like :username")
    Page<DaAuditEvent> getAll(@Param("username") String username,
                              @Param("fromTime") Long fromTime,
                              @Param("toTime") Long toTime, Pageable pageRequest);

    @Query(nativeQuery=true, value="select get_last_partition_name('audit')")
    String getLastPartitionName();

    @Query(nativeQuery=true, value="select get_last_partition_date('audit')")
    Long getLastPartitionDate();

    @Query(nativeQuery=true, value="select get_first_partition_name('audit')")
    String getFirstPartitionName();

    @Query(nativeQuery=true, value="select get_first_partition_date('audit')")
    Long getFirstPartitionDate();

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE audit ADD PARTITION (PARTITION p:partitionId VALUES LESS THAN (:partitionDate))")
    void createPartition(@Param("partitionId") long partitionId, @Param("partitionDate") long partitionDate);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE audit TRUNCATE PARTITION p:partitionId")
    void truncatePartitionForPartitionId(@Param("partitionId") long partitionId);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE audit DROP PARTITION p:partitionId")
    void deletePartitionForPartitionId(@Param("partitionId") long partitionId);
}
