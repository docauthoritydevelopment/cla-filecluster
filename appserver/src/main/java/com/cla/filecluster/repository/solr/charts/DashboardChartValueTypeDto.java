package com.cla.filecluster.repository.solr.charts;

import com.cla.common.constants.SolrCoreSchema;
import com.cla.filecluster.repository.solr.FacetFunction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DashboardChartValueTypeDto {

    private SolrCoreSchema solrField;
    private FacetFunction solrFunction;
    private Sort.Direction sortDirection;

}

