package com.cla.filecluster.repository.jpa.board;

import com.cla.filecluster.domain.entity.board.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BoardRepository extends JpaRepository<Board, Long> {

    @Query("select br from Board br where br.ownerId = :ownerId")
    Page<Board> findByOwnerPageRequest(@Param("ownerId") long ownerId, Pageable pageRequest);

    @Query("select br from Board br where br.ownerId = :ownerId or br.isPublic = true")
    Page<Board> findAllOwnerPageRequest(@Param("ownerId") long ownerId, Pageable pageRequest);
}
