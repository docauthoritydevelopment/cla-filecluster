package com.cla.filecluster.repository.jpa.infra;

import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
@Component
public class JpaRetryAspect {
	private static final Logger logger = LoggerFactory.getLogger(JpaRetryAspect.class);
	
	@Autowired
	private RetryTemplate retryTemplate;

	@Value("${applyRetryStrategy.jpa:true}")
	private boolean applyRetryStrategy;

	@Value("${applyRetryStrategy.get-db-staus-on-lock:true}")
	private boolean getDbStatusOnLock;

	@Around("execution(* org.springframework.data.repository.Repository+.*(..))")
	public Object retryOnRepository(final ProceedingJoinPoint call) throws Throwable {
		if(!applyRetryStrategy) {
			return call.proceed();
		}

		return retryTemplate.execute(retryContext -> {
			if (isDoNotRetryException(retryContext.getLastThrowable())) {
				retryContext.setExhaustedOnly();
			}
			if (retryContext.getRetryCount() > 0) {
				logger.warn("Retry #{} for {} err={}",retryContext.getRetryCount(), call.toString(), retryContext.getLastThrowable());
//				if (getDbStatusOnLock && retryContext.getRetryCount() == 3 &&
//						(retryContext.getLastThrowable() instanceof CannotAcquireLockException)) {
//					String status = getDbStatus();
//					if (status != null) {
//						logger.info("Show db innodb status:\n" + status);
//					}
//				}
			}
			return call.proceed();
		},
		rc -> {
			logger.error("Failed all retries for {}. Exception: {}",call.toString(), rc.getLastThrowable());
			return null;
		});
	
	}

	private boolean isDoNotRetryException(final Throwable lastThrowable) {
		if (lastThrowable instanceof NonTransientDataAccessException) {
			// Recommended by Spring for no-retry. Covers the following exceptions:
			// 		    CleanupFailureDataAccessException, DataIntegrityViolationException, DataRetrievalFailureException, 
			//			DataSourceLookupFailureException, InvalidDataAccessApiUsageException, InvalidDataAccessResourceUsageException, 
			//			NonTransientDataAccessResourceException, PermissionDeniedDataAccessException, UncategorizedDataAccessException
			return true;
		}
		else if (lastThrowable instanceof SQLException) {
			return true;
		}
		return false;
	}

	// Temp location for the show-db-status-query
	// Consider moving this elsewhere
	@Autowired
	private SimpleTaskRepository simpleTaskRepository;

	private String getDbStatus() {
		List<Object[]> res = simpleTaskRepository.showEngineInnoDbStatus();
		if (!res.isEmpty() && res.get(0).length == 3 && (res.get(0)[2] instanceof String)) {
			String st = new String((String) res.get(0)[2]);
			return st;
		}
		logger.warn("Unexpected response from showEngineInnoDbStatus. {}, {}, {}",
				res.size(), res.isEmpty()?"":res.get(0).length,
				res.isEmpty()?"": Arrays.asList(res.get(0)).stream()
						.map(o->o.getClass().toString()+":"+o.toString())
						.collect(Collectors.joining(",")));
		return null;
	}

}
