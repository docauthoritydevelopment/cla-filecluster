package com.cla.filecluster.repository.jpa.filetag;

import com.cla.filecluster.domain.entity.filetag.FileTagType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 29/09/2015.
 */
public interface FileTagTypeRepository extends JpaRepository<FileTagType, Long>, JpaSpecificationExecutor<FileTagType> {

    @Query("select tt from FileTagType tt where name=:name and tt.deleted = false")
    FileTagType findTagTypeByName(@Param("name") String name);

    @Query("select tt from FileTagType tt where name like %:name% and tt.deleted = false")
    List<FileTagType> findTagTypeNameLike(@Param("name") String name);

    @Query("select tt from FileTagType tt where tt.deleted = false")
    List<FileTagType> findTagTypeNotDeleted();

    @Query("select tt from FileTagType tt where id in :ids")
    List<FileTagType> findTagTypesByIds(@Param("ids") List<Long> ids);

    @Query("select tt.id, count(ft.id) from FileTag ft left join ft.type tt where tt.deleted = false and ft.deleted = false group by tt.id")
    List<Object[]> findFileTagCounts();

    @Query(nativeQuery = true,
            value = "select tt.* from file_tag_type tt \n" +
                    "left join scope s on s.id = tt.tag_type_scope_id\n" +
                    "where tt.deleted = 0 and tt.tag_type_scope_id is null or s.user_id = :userId \n" +
                    "union \n" +
                    "select tt.* from file_tag_type tt \n" +
                    "left join scope s on s.id = tt.tag_type_scope_id\n" +
                    "left join work_groups_scope_work_groups wgswg on wgswg.work_groups_scope_id = s.id\n" +
                    "left join work_groups wg on wg.id = wgswg.work_group_id\n" +
                    "left join users_work_groups uwg on uwg.work_group_id = wg.id\n" +
                    "where tt.deleted = 0 and uwg.user_id = :userId")
    List<FileTagType> findAllTagsByUser(@Param("userId") Long userId);
}
