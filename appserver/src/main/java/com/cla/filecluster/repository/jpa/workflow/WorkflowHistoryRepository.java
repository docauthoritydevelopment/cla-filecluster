package com.cla.filecluster.repository.jpa.workflow;

import com.cla.filecluster.domain.entity.workflow.WorkflowHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public interface WorkflowHistoryRepository extends JpaRepository<WorkflowHistory, Long> {

    @Query( value = "select u from WorkflowHistory u where u.workflowId = :workflowId",
            countQuery = "select count(u) from WorkflowHistory u where u.workflowId = :workflowId")
    Page<WorkflowHistory> getByWorkflow(@Param("workflowId") Long workflowId, Pageable pageRequest);

    @Modifying
    @Transactional
    @Query( "DELETE FROM WorkflowHistory u WHERE u.workflowId=:workflowId")
    void deleteByWorkflow(@Param("workflowId") Long workflowId);
}
