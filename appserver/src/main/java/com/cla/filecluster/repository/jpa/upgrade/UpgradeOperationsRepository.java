package com.cla.filecluster.repository.jpa.upgrade;

import com.cla.filecluster.domain.entity.upgrade.UpgradeOperationStep;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UpgradeOperationsRepository extends JpaRepository<UpgradeOperationStep, Integer>{

    @Transactional
//    @Query("select st FROM UpgradeOperationStep st " +
//            "INNER JOIN " +
//            "(SELECT op.version, MAX(op.step) as step FROM UpgradeOperationStep op " +
//                "INNER JOIN " +
//                "(SELECT MAX(inst.version) AS mx FROM UpgradeOperationStep inst) op_max " +
//                "WHERE op.version = op_max.mx) lt \n"+
//            "WHERE st.version = lt.version AND st.step = lt.step")

    @Query(nativeQuery = true, value = "select * from upgrade_op_step st inner join\n" +
            "(SELECT upgrade_version, max(upgrade_step) as step  \n" +
            "FROM upgrade_op_step op inner join (select max(upgrade_version) as mx from upgrade_op_step) op_max\n" +
            "where op.upgrade_version = op_max.mx) lt \n" +
            "where st.upgrade_version = lt.upgrade_version and st.upgrade_step = lt.step")
    UpgradeOperationStep getLatestOperation();

    @Transactional
    @Query("SELECT op \n"+
            "FROM UpgradeOperationStep op \n"+
            "where op.upgradeVersion = :version AND op.upgradeStep = :step")
    UpgradeOperationStep getByVersion(@Param("version") int version, @Param("step") int step);
}
