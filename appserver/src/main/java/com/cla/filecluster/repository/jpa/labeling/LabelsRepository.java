package com.cla.filecluster.repository.jpa.labeling;

import com.cla.filecluster.domain.entity.label.Label;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;

import java.util.Optional;

public interface LabelsRepository extends JpaRepository<Label,Long> {
	Optional<Label> findByName(@NonNull String name);
}
