package com.cla.filecluster.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cla.filecluster.domain.entity.pv.DebSimilarityInfo;

public interface DebSimilarityInfoRepository extends JpaRepository<DebSimilarityInfo, Long> {

}
