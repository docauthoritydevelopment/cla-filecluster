package com.cla.filecluster.repository.jpa.license;

import com.cla.filecluster.domain.entity.license.DocAuthorityLicense;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by uri on 27/06/2016.
 */
public interface DocAuthorityLicenseRepository extends JpaRepository<DocAuthorityLicense,Long> {
    @Query("select dal from DocAuthorityLicense dal order by applyDate desc")
    List<DocAuthorityLicense> findByApplyDate(Pageable pageable);
}
