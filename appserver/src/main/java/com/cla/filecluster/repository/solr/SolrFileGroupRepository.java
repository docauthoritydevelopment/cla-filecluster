package com.cla.filecluster.repository.solr;

import com.cla.common.constants.FileGroupsFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.converters.SolrFileGroupBuilder;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class SolrFileGroupRepository  extends SolrRepository<FileGroupsFieldType, SolrFileGroupEntity> {

    public static int MAX_ROWS = 10000000;

    @Value("${fileGroups.solr.core:FileGroups}")
    private String fileGroupsSolrCore;

    @Override
    protected Class<FileGroupsFieldType> getSchemaType() {
        return FileGroupsFieldType.class;
    }

    @Override
    protected Class<SolrFileGroupEntity> getEntityType() {
        return SolrFileGroupEntity.class;
    }

    @Override
    protected String coreName() {
        return fileGroupsSolrCore;
    }

    @Override
    protected SolrCoreSchema getIdField() {
        return FileGroupsFieldType.ID;
    }

    public String getNextAvailableId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public SolrOpResult save(SolrFileGroupEntity entity) {
        SolrOpResult result = SolrOpResult.create();
        try {
            String groupId = entity.getId();
            if (groupId == null) {
                groupId = getNextAvailableId();
                entity.setId(groupId);
            }
            client.addBean(entity);
            result.setId(groupId).setSuccessful(true);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
        return result;
    }

    public void saveEntities(List<SolrFileGroupEntity> entities) {
        try {
            client.addBeans(entities);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entities", e);
        }
    }

    public Optional<FileGroup> getById(String groupId) {
        Optional<SolrFileGroupEntity> entity = getOneById(groupId);
        FileGroup result = null;
        if (entity.isPresent()) {
            result = getFromEntity(entity.get());
        }
        return Optional.ofNullable(result);
    }

    public List<FileGroup> getByIds(Collection<String> groupIds) {
        SolrQuery query = Query.create()
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.ID, SolrOperator.IN, groupIds))
                .setRows(groupIds.size())
                .build();
        return getGroupsFromResult(query).getValue();
    }

    public List<SolrFileGroupEntity> findByIds(Collection<String> groupIds) {
        SolrQuery query = Query.create()
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.ID, SolrOperator.IN, groupIds))
                .setRows(groupIds.size())
                .build();
        return getGroupsEntitiesFromResult(query);
    }

    public List<FileGroup> getByQuery(Query query) {
        return getGroupsFromResult(query.build()).getValue();
    }

    public Pair<Long, List<FileGroup>> getByQueryWithTotal(Query query) {
        return getGroupsFromResult(query.build());
    }

    private Pair<Long, List<FileGroup>> getGroupsFromResult(SolrQuery query) {
        try {
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            List<SolrFileGroupEntity> res = queryResponse.getBeans(SolrFileGroupEntity.class);
            List<FileGroup> groups = new ArrayList<>();
            if (res != null && !res.isEmpty()) {
                groups = res.stream().map(SolrFileGroupRepository::getFromEntity).collect(Collectors.toList());
            }
            return Pair.of(queryResponse.getResults().getNumFound(), groups);
        } catch (Exception e) {
            logger.error("Failed to get file group from Solr", e);
            throw new RuntimeException("Failed to get file group from Solr. query:" + query, e);
        }
    }

    private List<SolrFileGroupEntity> getGroupsEntitiesFromResult(SolrQuery query) {
        try {
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            List<SolrFileGroupEntity> res = queryResponse.getBeans(SolrFileGroupEntity.class);
            if (res != null && !res.isEmpty()) {
                return res;
            }
            return new ArrayList<>();
        } catch (Exception e) {
            logger.error("Failed to get file group from Solr", e);
            throw new RuntimeException("Failed to get file group from Solr. query:" + query, e);
        }
    }

    public void saveGroupChanges(SolrFileGroupBuilder builder) {
        SolrInputDocument doc = builder.buildAtomicUpdate();
        atomicUpdate(doc);
        softCommitNoWaitFlush();
    }

    public FileGroup createGroup(SolrFileGroupBuilder builder, String id, GroupType type) {
        builder.setFieldsForNewGroup(id, type);
        SolrFileGroupEntity entity = builder.build();
        save(entity);
        softCommitNoWaitFlush();
        return getFromEntity(entity);
    }

    public static FileGroup getFromEntity(SolrFileGroupEntity entity) {
        FileGroup fg = new FileGroup();
        fg.setId(entity.getId());
        fg.setName(entity.getName());
        fg.setGeneratedName(entity.getGeneratedName());
        fg.setFirstMemberId(entity.getFirstMemberId());
        fg.setNumOfFiles(entity.getNumOfFiles());
        fg.setDeleted(entity.getDeleted());
        fg.setDirty(entity.getDirty());
        fg.setCountOnLastNaming(entity.getCountOnLastNaming());
        fg.setProposedNames(entity.getProposedNames());
        fg.setProposedNewNames(entity.getProposedNewNames());
        fg.setFirstProposedNames(entity.getFirstProposedNames());
        fg.setSecondProposedNames(entity.getSecondProposedNames());
        fg.setThirdProposedNames(entity.getThirdProposedNames());
        fg.setFileNamesTV(entity.getFileNamestv());
        fg.setParentDirTV(entity.getParentDirtv());
        fg.setGrandParentDirTV(entity.getGrandParentDirtv());
        fg.setFirstMemberContentId(entity.getFirstMemberContentId());
        fg.setParentGroupId(entity.getParentGroupId());
        fg.setRawAnalysisGroupId(entity.getRawAnalysisGroupId());
        fg.setHasSubgroups(entity.getHasSubgroups());
        fg.setAssociations(entity.getAssociations());

        if (entity.getType() != null) {
            fg.setGroupType(GroupType.valueOf(entity.getType()));
        }

        return fg;
    }

    public static void addField(SolrInputDocument solrInputDocument, FileGroupsFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }
}
