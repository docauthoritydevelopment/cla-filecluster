package com.cla.filecluster.repository.jpa.monitor;

import com.cla.common.domain.dto.components.ClaComponentType;
import com.cla.common.domain.dto.components.ComponentState;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * Created by itay on 22/02/2017.
 */
public interface ClaComponentRepository extends JpaRepository<ClaComponent,Long> {

    @Query("select cc from ClaComponent cc where cc.instanceId = :instanceId and cc.claComponentType = :componentType")
    ClaComponent findComponentByInstanceId(@Param("componentType") ClaComponentType componentType, @Param("instanceId") String instanceId);

    @Query("select cc from ClaComponent cc" +
            " where cc.claComponentType = :componentType" +
            " and cc.active = true" +
            " and cc.lastResponseDate < :minLastResponseTime")
    List<ClaComponent> findActiveComponentsNotResponsiveSince(@Param("componentType") ClaComponentType componentType,
                                                             @Param("minLastResponseTime") Long minLastResponseTime);

    @Query("select cc from ClaComponent cc where cc.customerDataCenter.id = :customerDataCenterId and cc.claComponentType = :claComponentType")
    List<ClaComponent> findByCustomerDataCenterId(@Param("customerDataCenterId") long customerDataCenterId, @Param("claComponentType") ClaComponentType claComponentType);

    @Query("select cc from ClaComponent cc where cc.customerDataCenter.id = :customerDataCenterId and cc.claComponentType = :claComponentType")
    Page<ClaComponent> findByCustomerDataCenterId(@Param("customerDataCenterId") long customerDataCenterId, @Param("claComponentType") ClaComponentType claComponentType, Pageable pageRequest);

    @Query("select count(cc) from ClaComponent cc where cc.customerDataCenter.id = :customerDataCenterId and cc.claComponentType = :claComponentType")
    int countByCustomerDataCenterId(@Param("customerDataCenterId") long customerDataCenterId, @Param("claComponentType") ClaComponentType claComponentType);

    @Query("select count(cc) from ClaComponent cc where cc.customerDataCenter.id = :customerDataCenterId and " +
                        "cc.claComponentType = :claComponentType and cc.active = true and cc.state = :state")
    int countActiveByCustomerDataCenterId(@Param("customerDataCenterId") long customerDataCenterId, @Param("claComponentType") ClaComponentType claComponentType, @Param("state") ComponentState state);

    @Query("select cc from ClaComponent cc where cc.claComponentType = :claComponentType")
    List<ClaComponent> findByComponentType(@Param("claComponentType") ClaComponentType claComponentType);

    @Query(nativeQuery = true, value = "select m.`id`, m.`active`, m.`cla_component_type` , m.`component_type` ,m.`external_net_address` ,m.`instance_id`, m.`last_response_date` , m.`local_net_address` ,\n" +
            "        m.`location` , m.`state` , m.`state_change_date`, m.`customer_data_center_id`, s.extra_metrics, s.timestamp, s.component_state, c.name customer_data_center_name, s.drive_data " +
            " from mon_components m " +
            " left join customer_data_center c on m.customer_data_center_id = c.id " +
            " left join  mon_component_status s on m.component_type=s.component_type and m.id=s.cla_component_id " +
            " and s.timestamp < :toTimestamp and s.timestamp >= :fromTimestamp " +
            "  order by m.id, s.timestamp")
    List<Object[]> findAllWithStatus(@Param("fromTimestamp") long fromTimestamp, @Param("toTimestamp") long toTimestamp);

    @Query(nativeQuery = true, value = "select m.`id`, s.drive_data, s.timestamp " +
            " from mon_components m, mon_component_status s " +
            " where m.component_type = s.component_type and m.id = s.cla_component_id and s.drive_data is not null" +
            " and s.timestamp >= :fromTimestamp and m.`cla_component_type` = :claComponentType " +
            " order by m.id, s.timestamp")
    List<Object[]> findDriveDataForTypeAndTime(@Param("claComponentType") int claComponentType,
                                        @Param("fromTimestamp") long fromTimestamp);

    @Query(nativeQuery = true, value = "select m.`id`, m.`active`, m.`cla_component_type` , m.`component_type` ,m.`external_net_address` ,m.`instance_id`, m.`last_response_date` , m.`local_net_address` ,\n" +
            "        m.`location` , m.`state` , m.`state_change_date`, m.`customer_data_center_id`, s.extra_metrics, s.timestamp, s.component_state, c.name customer_data_center_name, s.drive_data " +
            " from mon_components m " +
            " left join customer_data_center c on m.customer_data_center_id = c.id " +
            " left join  mon_component_status s on m.component_type=s.component_type and m.id=s.cla_component_id " +
            " and s.id = m.last_status_id " +
            "  order by m.id, s.timestamp")
    List<Object[]> findAllWithLatestStatus();

    @Query(nativeQuery = true, value = "select m.`id`, m.`active`, m.`cla_component_type` , m.`component_type` ,m.`external_net_address` ,m.`instance_id`, m.`last_response_date` , m.`local_net_address` ,\n" +
            "        m.`location` , m.`state` , m.`state_change_date`, m.`customer_data_center_id`, s.extra_metrics, s.timestamp, s.component_state, c.name customer_data_center_name, s.drive_data " +
            " from mon_components m " +
            " left join customer_data_center c on m.customer_data_center_id = c.id " +
            " left join  mon_component_status s on m.component_type=s.component_type and m.id=s.cla_component_id " +
            " and s.timestamp < :toTimestamp and s.timestamp >= :fromTimestamp " +
            " where " +
            "  m.component_type = :claComponentType and m.customer_data_center_id = :customerDataCenterId " +
            "  order by m.id, s.timestamp")
    List<Object[]> findByDataCenterIdWithStatus(@Param("customerDataCenterId") long customerDataCenterId, @Param("claComponentType") int claComponentType,
                                                @Param("fromTimestamp") long fromTimestamp, @Param("toTimestamp") long toTimestamp);
}
