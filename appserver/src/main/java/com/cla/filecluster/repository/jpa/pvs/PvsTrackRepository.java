package com.cla.filecluster.repository.jpa.pvs;

import com.cla.filecluster.domain.entity.pv.PvsTrackRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PvsTrackRepository extends JpaRepository<PvsTrackRecord, Long> {


    @Query(nativeQuery = true, value = "select * from pvs_track_record order by id desc limit 1")
    Optional<PvsTrackRecord> findTopRecordById();
}
