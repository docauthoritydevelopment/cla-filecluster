package com.cla.filecluster.repository.jpa.date;

import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uri on 05/06/2016.
 */
public interface SimpleDateRangeItemRepository extends JpaRepository<SimpleDateRangeItem, Long> {
}
