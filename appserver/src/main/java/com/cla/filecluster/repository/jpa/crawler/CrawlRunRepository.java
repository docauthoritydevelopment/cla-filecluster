package com.cla.filecluster.repository.jpa.crawler;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by uri on 01/05/2016.
 */
public interface CrawlRunRepository extends JpaRepository<CrawlRun, Long> {

    @Query("select cr from CrawlRun cr where cr.id in :ids")
    Page<CrawlRun> findByIdsWithPageRequest(@Param("ids") Collection<Long> ids, Pageable pageRequest);

    @Query("select cr from CrawlRun cr where cr.id in :ids")
    Set<CrawlRun> findByIds(@Param("ids") Collection<Long> ids);

    @Query("select cr from CrawlRun cr" +
            " where cr.parentRunId = :parentRunId")
    List<CrawlRun> findCrawlRunsByParentRunId(@Param("parentRunId") Long parentRunId);

    @Query("select cr from CrawlRun cr" +
            " where cr.runStatus in (:states) ")
    Page<CrawlRun> findByStates(@Param("states") Iterable<RunStatus> states, Pageable pageRequest);

    @Query("select cr from CrawlRun cr" +
            " where cr.runStatus in (:states) ")
    List<CrawlRun> findByStates(@Param("states") Iterable<RunStatus> states);

    @Query("select cr from CrawlRun cr" +
            " where (cr.runType is null or cr.runType = 'BIZ_LIST') and cr.extractFiles = true and cr.runStatus in (:states) ")
    List<CrawlRun> findExtractByStates(@Param("states") Iterable<RunStatus> states);

    @Query(nativeQuery = true, value = "select * from crawl_runs cr inner join " +
            "(select max(id) as id from crawl_runs where root_Folder_Id is null and schedule_group_id is null and extract_Files = true) m " +
            "on m.id = cr.id")
    CrawlRun getLatestExtractRun();

    @Query("select cr from CrawlRun cr" +
            " where cr.extractFiles = true and cr.runStatus in (:states) ")
    Page<CrawlRun> findAllExtractByStates(@Param("states") Iterable<RunStatus> states, Pageable pageRequest);

    @Query("select cr from CrawlRun cr" +
            " where cr.extractFiles = true ")
    Page<CrawlRun> findAllExtract(Pageable pageRequest);

    @Query(nativeQuery = true,
            value = "SELECT cr.id " +
                    " FROM crawl_runs cr " +
                    " left join root_folder rf on cr.root_folder_id = rf.id and rf.deleted = 0" +
                    " WHERE cr.run_status in (:states) and " +
                    " (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
                    " or lower(rf.nick_name) like %:namingSearchTerm%  or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)" +
                    " LIMIT :offset, :count")
    List<BigInteger> findByStates(@Param("states") Iterable<String> states, @Param("namingSearchTerm") String namingSearchTerm, @Param("offset") long offset, @Param("count") int count);

    @Query(nativeQuery = true,
            value = "SELECT count(cr.id) " +
                    " FROM crawl_runs cr " +
                    " left join root_folder rf on cr.root_folder_id = rf.id and rf.deleted = 0" +
                    " WHERE cr.run_status in (:states) and " +
                    " (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
                    " or lower(rf.nick_name) like %:namingSearchTerm%  or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)")
    int countByStates(@Param("states") Iterable<String> states, @Param("namingSearchTerm") String namingSearchTerm);

    @Query(nativeQuery = true,
            value = "SELECT cr.id " +
                    " FROM crawl_runs cr " +
                    " left join root_folder rf on cr.root_folder_id = rf.id and rf.deleted = 0" +
                    " WHERE (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
                    " or lower(rf.nick_name) like %:namingSearchTerm% or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)" +
                    " LIMIT :offset, :count")
    List<BigInteger> findBySearchTerm(@Param("namingSearchTerm") String namingSearchTerm, @Param("offset") long offset, @Param("count") int count);

    @Query(nativeQuery = true,
            value = "SELECT count(cr.id) " +
                    " FROM crawl_runs cr " +
                    " left join root_folder rf on cr.root_folder_id = rf.id and rf.deleted = 0" +
                    " WHERE (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
                    " or lower(rf.nick_name) like %:namingSearchTerm% or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)")
    int countBySearchTerm(@Param("namingSearchTerm") String namingSearchTerm);

    @Query("select cr from CrawlRun cr" +
            " where cr.rootFolderId = :rootFolderId AND cr.runStatus in (:states)")
    Page<CrawlRun> findByStatesForRootFolder(@Param("rootFolderId") Long rootFolderId, @Param("states") Iterable<RunStatus> states, Pageable pageRequest);

    @Query("select cr from CrawlRun cr" +
            " where cr.rootFolderId = :rootFolderId")
    Page<CrawlRun> findByRootFolder(@Param("rootFolderId") Long rootFolderId, Pageable pageRequest);

    @Query("select cr from CrawlRun cr" +
            " where cr.runStatus = :runStatus")
    List<CrawlRun> findAllByStatus(@Param("runStatus") RunStatus runStatus);

    @Query("select cr from CrawlRun cr" +
            " where cr.runStatus = :runStatus" +
            " order by cr.stopTime desc")
    List<CrawlRun> getCrawlRunsByRunStatus(@Param("runStatus") RunStatus runStatus);

    CrawlRun findTop1ByRootFolderIdAndRunStatusOrderByStartTimeDesc(Long rootFolderId, RunStatus runStatus);

    @Query(nativeQuery = true,
            value = "SELECT cr.id " +
                    " FROM crawl_runs cr " +
                    " left join root_folder rf on cr.root_folder_id = rf.id and rf.deleted = 0" +
                    " WHERE cr.run_status = :runStatus and " +
                    "(lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
                    " or lower(rf.nick_name) like %:namingSearchTerm% or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)"
    )
    List<BigInteger> getCrawlRunsByRunStatus(@Param("runStatus") String runStatus, @Param("namingSearchTerm") String namingSearchTerm);
}
