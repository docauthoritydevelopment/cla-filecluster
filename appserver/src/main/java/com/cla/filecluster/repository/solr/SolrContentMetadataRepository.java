package com.cla.filecluster.repository.solr;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.GroupUnificationHelper;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.repository.solr.query.*;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.ANALYSIS_GROUP_ID;
import static com.cla.common.constants.ContentMetadataFieldType.*;

/**
 * Created by uri on 31/01/2017.
 */
@Repository
public class SolrContentMetadataRepository extends SolrRepository<ContentMetadataFieldType, SolrContentMetadataEntity> {

    @Autowired
    private SequenceIdGenerator sequenceIdGenerator;

    @Value("${solr.contentMetadata.group.commit.ms:1}")
    private int commitGroupWithinMs;

    @Value("${contentMetadata.solr.core:ContentMetadata}")
    private String contentMetadataSolrCore;

    @Value("${contentMetadata.solr.commit:SOFT_NOWAIT}")
    private SolrCommitType updateContentMetadataCommitType;


    @Override
    protected Class<ContentMetadataFieldType> getSchemaType() {
        return ContentMetadataFieldType.class;
    }

    @Override
    protected SolrCoreSchema getIdField() {
        return ContentMetadataFieldType.ID;
    }

    @Override
    protected Class<SolrContentMetadataEntity> getEntityType() {
        return SolrContentMetadataEntity.class;
    }

    @Override
    protected String coreName() {
        return contentMetadataSolrCore;
    }

    public Long getNextAvailableId() {
        return sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.CONTENT_METADATA);
    }

    public static void addField(SolrInputDocument solrInputDocument, ContentMetadataFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @Override
    public SolrOpResult save(SolrContentMetadataEntity entity) {
        SolrOpResult result = SolrOpResult.create();
        try {
            String contentId = entity.getId();
            if (contentId == null) {
                contentId = String.valueOf(getNextAvailableId());
                entity.setId(contentId);
            }
            client.addBean(entity);
            result.setId(contentId).setContentId(Long.valueOf(contentId)).setSuccessful(true);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
        return result;
    }

    @Deprecated
    public SolrInputDocument createUpdateGroupDocument(long id, String groupId) {
        return atomicUpdate(id, FieldMod.of(GROUP_ID, SolrFieldOp.SET, groupId));
    }

    @Deprecated
    public void updateAnalysisGroup(long contentId, String groupId) {
        SolrInputDocument updateGroupDocument = createUpdateGroupDocument(contentId, groupId);
        updateDocument(contentId, updateGroupDocument);
    }

    private void updateDocument(long contentId, SolrInputDocument updateDocument) {
        try {
            SolrContentMetadataRepository.addField(updateDocument, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
            UpdateResponse updateResponse = client.add(updateDocument, commitGroupWithinMs);
            if (updateResponse == null) {
                throw new RuntimeException("Failed to update content in Solr.");
            }
            logger.debug("Update analysis group for contentId {} in Solr: {}", contentId, updateResponse.toString());
        } catch (Exception e) {
            throw new RuntimeException("Failed to update analysis group to content id " + contentId, e);
        }
    }

    @Deprecated
    public void excludeFileFromGrouping(Long contentId) {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.setField(ID.getSolrName(), contentId);
        Map<String, Object> unsetFieldValueMap = atomicSetValue(null);
        doc.setField(GROUP_ID.getSolrName(), unsetFieldValueMap);
        doc.setField(ANALYSIS_HINT.getSolrName(), atomicSetValue(AnalyzeHint.EXCLUDED.name()));
        updateDocument(contentId, doc);
    }

    @Deprecated
    public void updateAnalyzeHint(AnalyzeHint analyzeHint, Long contentId, boolean commit) {
        try {
            SolrInputDocument doc = new SolrInputDocument();
            doc.addField(ID.getSolrName(), contentId);
            doc.addField(ANALYSIS_HINT.getSolrName(), atomicSetValue(analyzeHint.name()));
            updateDocument(contentId, doc);
            if (commit) {
                commit();
            }
        } catch (Exception e) {
            logger.error("Failed to analyze hint in Solr", e);
            throw new RuntimeException("Failed to analyze hint in Solr. Content id:" + contentId, e);
        }
    }

    public List<String> findProposedTitlesByContentId(long contentId) {
        Optional<SolrContentMetadataEntity> documentOp = getOneById(contentId);
        if (documentOp.isPresent() && documentOp.get().getProposedTitles() != null) {
            return Lists.newArrayList(documentOp.get().getProposedTitles());
        }
        return Lists.newArrayList();
    }

    public List<String> findProposedTitlesByContent(Optional<SolrContentMetadataEntity> documentOp) {
        if (documentOp.isPresent() && documentOp.get().getProposedTitles() != null) {
            return Lists.newArrayList(documentOp.get().getProposedTitles());
        }
        return Lists.newArrayList();
    }

    public ContentMetadata getById(Long contentId) {
        Optional<SolrContentMetadataEntity> entity = getOneById(contentId);
        if (entity.isPresent()) {
            return fromEntity(entity.get());
        }
        return null;
    }

    public SolrContentMetadataEntity findById(Long contentId) {
        Optional<SolrContentMetadataEntity> entity = getOneById(contentId);
        return entity.orElse(null);
    }

    public List<SolrContentMetadataEntity> findContentMetadataEntitiesByIds(Collection<Long> contentIds) {
        return find(Query.create()
                .addFilterWithCriteria(ContentMetadataFieldType.ID, SolrOperator.IN, contentIds)
                // Overrides Solr default page size - paging logic should be implemented at higher levels to maintain scalability
                .setRows(contentIds.size())
                .build());
    }

    public static List<ContentMetadata> fromEntity(List<SolrContentMetadataEntity> entities) {
        List<ContentMetadata> result = new ArrayList<>();
        if (entities != null) {
            entities.forEach(e -> result.add(fromEntity(e)));
        }
        return result;
    }

    public static ContentMetadata fromEntity(SolrContentMetadataEntity entity) {
        if (entity == null) return null;
        ContentMetadata cm = new ContentMetadata();
        cm.setId(Long.parseLong(entity.getId()));
        if (entity.getLastIngestTimestampMs() != null) {
            cm.setLastIngestTimestampMs(entity.getLastIngestTimestampMs());
        }
        cm.setAlreadyAccessed(entity.isAlreadyAccessed());

        if (entity.getItemsCountL1() != null) {
            cm.setItemsCountL1(entity.getItemsCountL1());
        } else {
            cm.setItemsCountL1(0L);
        }

        if (entity.getItemsCountL2() != null) {
            cm.setItemsCountL2(entity.getItemsCountL2());
        } else {
            cm.setItemsCountL2(0L);
        }

        if (entity.getItemsCountL3() != null) {
            cm.setItemsCountL3(entity.getItemsCountL3());
        } else {
            cm.setItemsCountL3(0L);
        }

        if (entity.getState() != null) {
            cm.setState(ClaFileState.valueOf(entity.getState()));
        }
        cm.setFsFileSize(entity.getSize());
        cm.setUserGroupId(entity.getUserGroupId());
        cm.setAnalysisGroupId(entity.getAnalysisGroupId());
        if (entity.getAnalysisHint() != null) {
            cm.setAnalyzeHint(AnalyzeHint.valueOf(entity.getAnalysisHint()));
        }
        cm.setAppCreationDate(entity.getCreationDate());
        cm.setAuthor(entity.getAuthor());
        cm.setCompany(entity.getCompany());
        cm.setTemplate(entity.getTemplate());
        cm.setSubject(entity.getSubject());
        cm.setTitle(entity.getTitle());
        cm.setFileCount(entity.getFileCount());
        cm.setType(entity.getType());
        cm.setContentSignature(entity.getContentSignature());
        cm.setGeneratingApp(entity.getGeneratingApp());
        cm.setSampleFileId(entity.getSampleFileId());
        cm.setUserContentSignature(entity.getUserContentSignature());
        cm.setKeywords(entity.getKeywords());
        cm.setPopulationDirty(entity.isPopulationDirty());
        cm.setGroupDirty(entity.isGroupDirty());
        cm.setExtractedEntitiesIds(entity.getExtractedEntitiesIds());
        return cm;
    }

    public static List<ContentMetadataDto> fromEntityToDto(List<SolrContentMetadataEntity> entities) {
        List<ContentMetadataDto> result = new ArrayList<>();
        if (entities != null) {
            entities.forEach(e -> result.add(fromEntityToDto(e)));
        }
        return result;
    }

    public static ContentMetadataDto fromEntityToDto(SolrContentMetadataEntity entity) {
        ContentMetadataDto dto = new ContentMetadataDto();
        dto.setContentId(Long.parseLong(entity.getId()));
        dto.setState(ClaFileState.valueOf(entity.getState()));
        dto.setFileCount(entity.getFileCount());
        dto.setContentSignature(entity.getContentSignature());
        dto.setAppCreationDate(entity.getCreationDate());
        dto.setSampleFileId(entity.getSampleFileId());
        dto.setAnalysisGroupId(entity.getAnalysisGroupId());
        dto.setAuthor(entity.getAuthor());
        dto.setCompany(entity.getCompany());
        dto.setFsFileSize(entity.getSize());
        dto.setKeywords(entity.getKeywords());
        dto.setTemplate(entity.getTemplate());
        dto.setTitle(entity.getTitle());
        dto.setUserContentSignature(entity.getUserContentSignature());
        if (entity.getType() > 0) {
            dto.setType(FileType.valueOf(entity.getType()));
        }
        dto.setUserGroupId(entity.getUserGroupId());
        if (entity.getAnalysisHint() != null) {
            dto.setAnalyzeHint(AnalyzeHint.valueOf(entity.getAnalysisHint()));
        }
        return dto;
    }

    public List<SolrContentMetadataEntity> findIdentical(String contentSignature, Long fileSize) {
        if (contentSignature == null) return new ArrayList<>();
        SolrQuery query = Query.create()
                .addFilterWithCriteria(CONTENT_SIGNATURE, SolrOperator.EQ, contentSignature)
                .addFilterWithCriteria(SIZE, SolrOperator.EQ, fileSize)
                .build();
        return find(query);
    }

    public Map<Pair<String, Long>, List<SolrContentMetadataEntity>> findIdenticals(List<Pair<String, Long>> sigAndSizePairList) {

        Query query = Query.create();
        Map<Pair<String, Long>, List<SolrContentMetadataEntity>> result = new HashMap<>();
        sigAndSizePairList.forEach(p -> result.put(p, new LinkedList<>()));

        Criteria[] sigAndSizeCriteriaArray = sigAndSizePairList.stream().map(p -> Criteria.and(
                Criteria.create(CONTENT_SIGNATURE, SolrOperator.EQ, p.getKey()),
                Criteria.create(SIZE, SolrOperator.EQ, p.getValue())))
                .toArray(Criteria[]::new);
        query.addFilterWithCriteria(Criteria.or(sigAndSizeCriteriaArray));
        List<SolrContentMetadataEntity> solrContentMetadataEntities = find(query.build());
        solrContentMetadataEntities.forEach(c -> {
            result.get(Pair.of(c.getContentSignature(), c.getSize())).add(c);
        });
        return result;
    }

    public String getAnalysisGroupId(String contentId) {
        SolrQuery query = Query.create()
                .setRows(1)
                .addField(GROUP_ID)
                .addFilterWithCriteria(ID, SolrOperator.EQ, contentId)
                .build();
        try {
            QueryResponse response = client.query(query);
            return getSingleFieldValue(response, String.class, GROUP_ID);
        } catch (Exception e) {
            logger.error("Failed to get analysis group ID by content ID {}.", contentId);
            return null;
        }
    }

    public Collection<String> filterNonEmptyByGroup(List<Long> subList, String analysisGroupId) {
        SolrQuery query = Query.create()
                .addField(ID)
                .addFilterWithCriteria(GROUP_ID, SolrOperator.NE, analysisGroupId)
                .addFilterWithCriteria(ID, SolrOperator.IN, subList)
                .addFilterWithCriteria(FILE_COUNT, SolrOperator.GT, 0)
                .build();
        try {
            QueryResponse response = client.query(query, SolrRequest.METHOD.POST);
            return getSingleFieldValues(response, String.class, ID);
        } catch (Exception e) {
            logger.error("Failed to filter contents by analysis group ID .", analysisGroupId, e);
            return null;
        }

    }

    public Pair<String, Collection<Long>> getContentsPopulationDirty(String solrPagingCursor, Integer count) {
        SolrQuery query = Query.create().addField(ID)
                .addFilterWithCriteria(POPULATION_DIRTY, SolrOperator.EQ, "true")
                .setRows(count)
                .addSort(ID, Sort.Direction.ASC)
                .build();

        query.set(CursorMarkParams.CURSOR_MARK_PARAM, solrPagingCursor);
        try {
            QueryResponse response = client.query(query);
            String nextCursorMark = response.getNextCursorMark();
            return Pair.of(nextCursorMark, getSingleFieldValues(response, Long.class, ID));
        } catch (Exception e) {
            logger.error("Failed to find analysis groups by population dirty.", e);
            return null;
        }
    }

    public Pair<String, Collection<Long>> getContentsPopulationDirtyNeedSample(String solrPagingCursor, Integer count) {
        SolrQuery query = Query.create().addField(ID)
                .addFilterWithCriteria(POPULATION_DIRTY, SolrOperator.EQ, "true")
                .addFilterWithCriteria(FILE_COUNT, SolrOperator.GT, 0)
                .addFilterWithCriteria(SAMPLE_FILE_ID, SolrOperator.MISSING, null)
                .setRows(count)
                .addSort(ID, Sort.Direction.ASC)
                .build();
        query.set(CursorMarkParams.CURSOR_MARK_PARAM, solrPagingCursor);
        try {
            QueryResponse response = client.query(query);
            return Pair.of(response.getNextCursorMark(),getSingleFieldValues(response, Long.class, ID));
        } catch (Exception e) {
            logger.error("Failed to find analysis groups by population dirty.", e);
            return null;
        }
    }

    public Collection<String> getGroupsByContentPopulationDirty() {
        SolrQuery query = Query.create().addField(GROUP_ID)
                .addFilterWithCriteria(GROUP_ID, SolrOperator.PRESENT, null)
                .addFilterWithCriteria(POPULATION_DIRTY, SolrOperator.EQ, "true")
                .addFilterWithCriteria(FILE_COUNT, SolrOperator.GT, 0)
                .build();

        try {
            QueryResponse response = client.query(query);
            return getSingleFieldValues(response, String.class, GROUP_ID);
        } catch (Exception e) {
            logger.error("Failed to find analysis groups by population dirty.", e);
            return null;
        }
    }

    /**
     * Find content ID, analysis group ID and analysis hint for each content ID in provided list
     *
     * @param contentIdsList list of content metadata IDs
     * @return list of field -> value maps
     */
    public List<Map<SolrCoreSchema, Object>> findAnalysisDataByContentIds(Collection<Long> contentIdsList) {
        SolrQuery query = Query.create()
                .addField(ID)
                .addField(GROUP_ID)
                .addField(ANALYSIS_HINT)
                .addFilterWithCriteria(ID, SolrOperator.IN, contentIdsList)
                .addFilterWithCriteria(GROUP_ID, SolrOperator.PRESENT, null)
                .setRows(contentIdsList.size())
                .build();
        try {
            QueryResponse response = client.query(query, SolrRequest.METHOD.POST);
            return getMultiFieldValues(response, ID, GROUP_ID, ANALYSIS_HINT);
        } catch (Exception e) {
            logger.error("Failed to find analysis groups by contentIDs.", e);
            return null;
        }
    }

    /**
     * Find content by ID, set its analysis group and user group to specified group ID,
     * but only if analysis group field is empty
     *
     * @param contentId content metadata ID
     * @param groupId   group ID to set
     */
    public Integer setGroupIfEmpty(String contentId, String groupId) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.EQ, contentId)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ANALYSIS_GROUP_ID, SolrOperator.MISSING, null)));
        return updateFieldByQueryMultiFields(new String[]{GROUP_ID.getSolrName(), USER_GROUP_ID.getSolrName(),
                        GROUP_DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), GROUP_DIRTY_DATE.getSolrName()},
                new String[]{groupId, groupId, "true", String.valueOf(System.currentTimeMillis()),
                        String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, updateContentMetadataCommitType);
    }

    public void updateGroupsForContents(Collection<Pair<Long, String>> contentGroups, boolean commit) {
        List<SolrInputDocument> docs = new ArrayList<>();

        contentGroups.forEach(pair -> {
            SolrInputDocument doc = AtomicUpdateBuilder.create().setId(ID, pair.getKey())
                    .addModifier(GROUP_ID, SolrOperator.SET, pair.getValue())
                    .addModifier(USER_GROUP_ID, SolrOperator.SET, pair.getValue())
                    .addModifier(GROUP_DIRTY, SolrOperator.SET, true)
                    .addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                    .addModifier(GROUP_DIRTY_DATE, SolrOperator.SET, System.currentTimeMillis())
                    .build();
            docs.add(doc);
        });

        if (!docs.isEmpty()) {
            saveAll(docs);

            if (commit) {
                softCommitNoWaitFlush();
            }
        }
    }

    public void updateAnalysisGroupIdByContentIds(Collection<Long> contentIdList, String newGid, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.IN, contentIdList)));
        //updateFieldByQuery(ContentMetadataFieldType.GROUP_ID.getSolrName(), newGid, SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{GROUP_ID.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{newGid, String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void updateUserGroupIdByContentIds(Collection<Long> contentIdList, String newGid, String oldGid, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.IN, contentIdList)));
        if (oldGid != null) {
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(USER_GROUP_ID, SolrOperator.EQ, oldGid)));
        }
        //updateFieldByQuery(ContentMetadataFieldType.USER_GROUP_ID.getSolrName(), newGid, SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{USER_GROUP_ID.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{newGid, String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void setGroup(String newGroupId, String oldGroupId, ContentMetadataFieldType groupField, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(groupField, SolrOperator.EQ, oldGroupId)));
        //updateFieldByQuery(groupField.getSolrName(), newGroupId, SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{groupField.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{newGroupId, String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void clearSampleFileIdIfNeededByClaFileId(long claFileId, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(SAMPLE_FILE_ID, SolrOperator.EQ, claFileId)));
        updateFieldByQuery(SAMPLE_FILE_ID, null, SolrFieldOp.REMOVE, null, filterQueries, commitType);
    }

    public void clearPopulationDirtyFlag(SolrCommitType commitType, long toDate) {
        String fq = SolrQueryGenerator.generateFilter(
                Criteria.or(
                        Criteria.create(POP_DIRTY_DATE, SolrOperator.MISSING, null),
                        Criteria.create(POP_DIRTY_DATE, SolrOperator.LT, toDate)));
        List<String> filterQueries = Lists.newArrayList(fq);
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(POPULATION_DIRTY, SolrOperator.EQ, "true")));
        updateFieldByQuery(ContentMetadataFieldType.POPULATION_DIRTY, "false", SolrFieldOp.SET, null, filterQueries, commitType);
    }

    public void clearGroupDirtyFlag(SolrCommitType commitType, long toDate) {
        String fq = SolrQueryGenerator.generateFilter(
                Criteria.or(
                        Criteria.create(GROUP_DIRTY_DATE, SolrOperator.MISSING, null),
                        Criteria.create(GROUP_DIRTY_DATE, SolrOperator.LT, toDate)));
        List<String> filterQueries = Lists.newArrayList(fq);
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(GROUP_DIRTY, SolrOperator.EQ, "true")));
        updateFieldByQuery(ContentMetadataFieldType.GROUP_DIRTY, "false", SolrFieldOp.SET, null, filterQueries, commitType);
    }

    public void markDirtyPopulationFlagById(long contentId, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.EQ, contentId)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(POPULATION_DIRTY, SolrOperator.EQ, "false")));
        //updateFieldByQuery(ContentMetadataFieldType.POPULATION_DIRTY, "true", SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{POPULATION_DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), POP_DIRTY_DATE.getSolrName()},
                new String[]{"true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void markDirtyPopulationFlagById(Collection<Long> contentIds, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.IN, contentIds)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(POPULATION_DIRTY, SolrOperator.EQ, "false")));
        //updateFieldByQuery(ContentMetadataFieldType.POPULATION_DIRTY, "true", SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{POPULATION_DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), POP_DIRTY_DATE.getSolrName()},
                new String[]{"true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void changeStateAll(ClaFileState state, ClaFileState oldState, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(PROCESSING_STATE, SolrOperator.EQ, oldState.name())));
        //updateFieldByQuery(ContentMetadataFieldType.PROCESSING_STATE, state.name(), SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{PROCESSING_STATE.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{state.name(), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void updateContentIdFileCount(long contentId, int fileCount, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.EQ, contentId)));
        //updateFieldByQuery(ContentMetadataFieldType.FILE_COUNT, fileCount, SolrFieldOp.SET, null, filterQueries, commitType);
        updateFieldByQueryMultiFields(new String[]{FILE_COUNT.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{String.valueOf(fileCount), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, commitType);
    }

    public void clearSampleFileIdIfNeededByIdAndClaFileIdPairs(List<Pair<Long, Long>> pairs, SolrCommitType commitType) {
        List<String> filterQueries = Lists.newLinkedList();
        pairs.forEach(p ->
                filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.and(Criteria.create(ID, SolrOperator.EQ, p.getKey()),
                        Criteria.create(SAMPLE_FILE_ID, SolrOperator.EQ, p.getValue())))));
        updateFieldByQuery(SAMPLE_FILE_ID, null, SolrFieldOp.REMOVE, null, filterQueries, commitType);
    }

    public void markAsDeletedBySignatureExceptId(List<Long> contentIds) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.IN, contentIds)));
        updateFieldByQueryMultiFields(new String[]{FILE_COUNT.getSolrName(), CONTENT_SIGNATURE.getSolrName()},
                new String[]{"0", null}, SolrFieldOp.REMOVE.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, updateContentMetadataCommitType);
    }

    public void setNewUserGroupForAnalysisGroup(String childrenGroupId, String newGroup) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ANALYSIS_GROUP_ID, SolrOperator.EQ, childrenGroupId)));
        updateFieldByQueryMultiFields(new String[]{USER_GROUP_ID.getSolrName(), GROUP_DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), GROUP_DIRTY_DATE.getSolrName()},
                new String[]{newGroup, "true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, updateContentMetadataCommitType);
    }

    public void setGroupAndHintForContentManual(String contentId, String groupId) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.EQ, contentId)));
        updateFieldByQueryMultiFields(new String[]{GROUP_ID.getSolrName(), USER_GROUP_ID.getSolrName(),
                        ANALYSIS_HINT.getSolrName(), GROUP_DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), GROUP_DIRTY_DATE.getSolrName()},
                new String[]{groupId, groupId, AnalyzeHint.MANUAL.name(), "true", String.valueOf(System.currentTimeMillis()),
                        String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, updateContentMetadataCommitType);
    }

    public void setGroupByUnification(Collection<GroupUnificationHelper> unprocessedUnifications, ContentMetadataFieldType groupField) {
        List<String> origGroup = unprocessedUnifications.stream().map(GroupUnificationHelper::getOrigGroupId).collect(Collectors.toList());
        Map<String, String> origToNew = unprocessedUnifications.stream().collect(
                Collectors.toMap(GroupUnificationHelper::getOrigGroupId, GroupUnificationHelper::getNewGroupId));

        Iterables.partition(origGroup, 5000).forEach(p -> {
            SolrQuery solrQuery = Query.create()
                    .addFilterWithCriteria(Criteria.create(groupField, SolrOperator.IN, p))
                    .addField(ID)
                    .addField(groupField)
                    .build();
            List<SolrDocument> documents = findAllByQuery(solrQuery, ID, Sort.Direction.ASC);
            if (documents.isEmpty()) {
                logger.debug("No existing content found for group ids {}", p);
                return;
            } else {
                logger.debug("Updating {} documents with group field {}", documents.size(), groupField);
            }
            List<SolrInputDocument> contentGroupUpdates = documents.stream().map(d -> {
                Long id = Long.valueOf((String) d.getFieldValue(ID.getSolrName()));
                String groupId = (String) d.getFieldValue(groupField.getSolrName());
                return AtomicUpdateBuilder.create().setId(ID, id)
                        .addModifier(groupField, SolrOperator.SET, origToNew.get(groupId))
                        .addModifier(GROUP_DIRTY, SolrOperator.SET, true)
                        .addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                        .addModifier(GROUP_DIRTY_DATE, SolrOperator.SET, System.currentTimeMillis())
                        .build();
            }).collect(Collectors.toList());
            saveAll(contentGroupUpdates);
        });
        if (!origGroup.isEmpty()) {
            softCommitNoWaitFlush();
        }
        /*origGroup.forEach(gid -> {
            List<String> filterQueries = Lists.newLinkedList();
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(groupField, SolrOperator.EQ, gid)));
            updateFieldByQueryMultiFields(new String[]{groupField.getSolrName(), GROUP_DIRTY.getSolrName()},
                    new String[]{origToNew.get(gid), "true"}, SolrFieldOp.SET.getOpName(),
                    SolrQueryGenerator.ALL_QUERY, filterQueries, updateContentMetadataCommitType);
        });
        commit();*/

    }

    public void updateContentsSampleFilesWithNoFiles(Collection<Long> fileIds) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(SAMPLE_FILE_ID, SolrOperator.IN, fileIds)));
        updateFieldByQueryMultiFields(new String[]{SAMPLE_FILE_ID.getSolrName(), POPULATION_DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), POP_DIRTY_DATE.getSolrName()},
                new String[]{null, "true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries, updateContentMetadataCommitType);
    }

    public long countPopulationDirty() {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.POPULATION_DIRTY, SolrOperator.EQ, "true"));
        return getTotalCount(query, ContentMetadataFieldType.POPULATION_DIRTY, "true");
    }

    public long countGroupDirty() {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.GROUP_DIRTY, SolrOperator.EQ, "true"));
        return getTotalCount(query, ContentMetadataFieldType.GROUP_DIRTY, "true");
    }

    public Map<String, Long> getDuplicateSignatures() {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.POPULATION_DIRTY, SolrOperator.EQ, "true"));
        query.addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.CONTENT_SIGNATURE, SolrOperator.PRESENT, null));
        query.addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.FILE_COUNT, SolrOperator.PRESENT, null));
        query.addFacetPivot(ContentMetadataFieldType.CONTENT_SIGNATURE);
        query.addFacetPivot(ContentMetadataFieldType.FILE_COUNT);
        query.setFacetPivotAsOne(true);
        query.setFacetPivotMinCount(2);
        query.setRows(0);
        QueryResponse queryResponse = runQuery(query.build());
        Map<String, Long> result = new HashMap<>();
        queryResponse.getFacetPivot().forEach((k, v) -> {
            v.forEach(f -> {
                try {
                    String contentSignature = ((String) f.getValue());
                    if (f.getPivot() == null) {
                        result.put(contentSignature, Long.valueOf(f.getCount()));
                    } else {
                        f.getPivot().forEach(pv -> {
                            Long fileCount = ((Integer) pv.getCount()).longValue();
                            result.put(contentSignature, fileCount);
                        });
                    }
                } catch (Exception e) {
                    logger.error("parsing pivot resp {}", queryResponse.getFacetPivot(), e);
                }
            });
        });
        return result;
    }

    public Long getTotalCount(Query query, ContentMetadataFieldType facetField, String facetValueToCount) {
        Long total = 0L;
        query.setRows(0);
        query.addFacetField(facetField);
        query.setFacetMinCount(1);
        try {
            QueryResponse queryResponse = runQuery(query.build());
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    if (facetValueToCount == null) {
                        total += c.getCount();
                    } else if (c.getName().equals(facetValueToCount)) {
                        return c.getCount();
                    }
                }
            }
            return total;
        } catch (Exception e) {
            logger.error("Failed to get file count from Solr", e);
            throw new RuntimeException("Failed to get file count from Solr. query:" + query, e);
        }
    }

    public void createContentsFromDocuments(Collection<SolrInputDocument> documents) {
        if (documents == null || documents.size() == 0) {
            return;
        }
        try {
            logger.debug("Send {} documents to Solr", documents.size());
            UpdateResponse response = client.add(documents);
            if (response == null) {
                throw new RuntimeException("Failed to create contents in Solr.");
            }
            logger.debug("Got response from solr: {}", response);
        } catch (Exception e) {
            logger.error("Failed to create contents in Solr", e);
            String listStr = documents.stream().map(d -> d.getField(ID.getSolrName()).toString())
                    .collect(Collectors.joining(","));
            logger.error("Failed content ids: {}", listStr);
            throw new RuntimeException("Failed to create contents in Solr.", e);
        }

    }
}
