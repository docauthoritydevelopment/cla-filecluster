
package com.cla.filecluster.repository.jpa;

import com.cla.filecluster.domain.entity.pv.GroupUnificationHelper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface GroupUnificationHelperRepository extends JpaRepository<GroupUnificationHelper, String> {

	@Query("select guh from GroupUnificationHelper guh where guh.origGroupId = :from and guh.processed=false and guh.newGroupId = :to")
	GroupUnificationHelper findOne(@Param("from") String from, @Param("to") String to);

	@Query("select guh from GroupUnificationHelper guh where guh.origGroupId = :group and guh.processed=false")
	List<GroupUnificationHelper> findByOrigGroup(@Param("group") String group);

	@Query("select guh from GroupUnificationHelper guh where guh.processed=false")
	List<GroupUnificationHelper> findAllUnprocessed();

	@Modifying
	@Transactional
	@Query("update GroupUnificationHelper guh set guh.processed=true where guh.origGroupId = :group and guh.processed=false")
	int markProcessedForOriginGroup(@Param("group") String group);

	@Modifying
	@Transactional
	@Query("update GroupUnificationHelper guh set guh.processed=true where guh.processed=false")
	int markAllProcessed();

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "TRUNCATE TABLE group_unification_helper")
	void truncateGroupUnificationHelper();
}

