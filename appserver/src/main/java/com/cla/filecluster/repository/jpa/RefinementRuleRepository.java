package com.cla.filecluster.repository.jpa;

import com.cla.filecluster.domain.entity.pv.RefinementRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * For rules to refine a file group
 * Created by: yael
 * Created on: 2/5/2018
 */
public interface RefinementRuleRepository extends JpaRepository<RefinementRule, Long>, JpaSpecificationExecutor<RefinementRule> {

    @Query("SELECT br FROM RefinementRule br WHERE br.rawAnalysisGroupId = :rawAnalysisGroupId order by br.priority")
    List<RefinementRule> getByRawAnalysisGroupId(@Param("rawAnalysisGroupId") String rawAnalysisGroupId);
}
