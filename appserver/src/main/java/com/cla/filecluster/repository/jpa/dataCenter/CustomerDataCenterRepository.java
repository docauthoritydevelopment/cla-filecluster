package com.cla.filecluster.repository.jpa.dataCenter;


import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by liora on 29/05/2017.
 */
public interface CustomerDataCenterRepository extends JpaRepository<CustomerDataCenter,Long> {

    CustomerDataCenter findByName(@Param("name") String name);
    CustomerDataCenter findById(@Param("id") long id);

    CustomerDataCenter findFirstByIsDefault(@Param("isDefault") boolean isDefault);

    @Query(nativeQuery = true, value = "select sg.id, sg.name from customer_data_center sg order by sg.name")
    List<Object[]> getDataCenterNames();

  //  Page<CustomerDataCenter> findAll(Pageable pageable);
}
