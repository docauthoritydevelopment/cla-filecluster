package com.cla.filecluster.repository.solr;

import com.cla.common.domain.dto.file.FileSizePartitionDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public final class SolrFileSizeQueryUtils {

    public static List<String> getFileSizeQueryValues(Collection<FileSizePartitionDto> fileSizePartitionDtos) {
        return fileSizePartitionDtos.stream().map(FileSizePartitionDto::getSolrQuery).collect(Collectors.toList());
    }

    public static String getFileSizeFilter(FileSizePartitionDto partition) {
        return "[" + partition.getFromType().toBytes(partition.getFromValue()) +
                " TO " +
                (partition.getToType() == null ? "*" : partition.getToType().toBytes(partition.getToValue()) - 1) + // make to range an 'open range' excluding the limit
                "]";
    }
}
