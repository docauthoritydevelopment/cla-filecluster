package com.cla.filecluster.repository.jpa.fileType;

import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by ophir on 25/03/2019.
 */
public interface FileTypeCategoryRepository extends JpaRepository<FileTypeCategory,Long> {

    @Query("select ftc from FileTypeCategory ftc where name = :name")
    List<FileTypeCategory> findByName(@Param("name") String name);

    @Query("select ftc from FileTypeCategory ftc where ftc.name like %:name%")
    List<FileTypeCategory> findByNameLike(@Param("name") String name);
}
