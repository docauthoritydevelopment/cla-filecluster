package com.cla.filecluster.repository.solr.infra;

import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

import java.util.Collection;
import java.util.List;

/**
 * Solr Client proxy interface.
 * Makes it possible to create nested SOLR client adapters
 */
public interface SolrClientAdapter {

    SolrClient getUnderyingClient();

    boolean isSolrCloudClient();

    UpdateResponse deleteByQuery(String query) throws Exception;

    UpdateResponse deleteById(String id) throws Exception;

    public UpdateResponse deleteByIds(List<String> ids) throws Exception;

    UpdateResponse commit() throws Exception;

    UpdateResponse commit(String collection, boolean waitFlush, boolean waitSearcher) throws Exception;

    UpdateResponse commit(boolean waitFlush, boolean waitSearcher, boolean softCommit) throws Exception;

    QueryResponse query(SolrQuery query, SolrRequest.METHOD method) throws Exception;

    default QueryResponse query(Query query, SolrRequest.METHOD method) throws Exception {
        SolrQuery solrQuery = SolrQueryGenerator.generate(query);
        return query(solrQuery, method);
    }

    QueryResponse query(SolrQuery query) throws Exception;

    QueryResponse query(SolrQuery query, SolrRequest.METHOD method, int retryCount) throws Exception;

    QueryResponse query(SolrQuery query, int retryCount) throws Exception;

    UpdateResponse optimize(boolean waitFlush, boolean waitSearcher, int maxSegments) throws Exception;

    UpdateResponse optimize() throws Exception;

    SolrPingResponse ping() throws Exception;

    UpdateResponse add(SolrInputDocument doc) throws Exception;

    UpdateResponse add(SolrInputDocument doc, boolean shouldRetry) throws Exception;

    UpdateResponse add(SolrInputDocument doc, int commitWithinMs) throws Exception;

    UpdateResponse add(Collection<SolrInputDocument> docs, int commitWithinMs) throws Exception;

    UpdateResponse add(Collection<SolrInputDocument> docs) throws Exception;

    UpdateResponse addBean(Object obj) throws Exception;

    UpdateResponse addBean(Object obj, int commitWithinMs) throws Exception;


    default QueryResponse queryNonSecure(SolrQuery query, SolrRequest.METHOD method) throws Exception {
        return query(query, method);
    }

    UpdateResponse addBeans(List<?> entities) throws Exception;

    UpdateResponse addBeans(List<?> entities, int commitWithinMs) throws Exception;
}
