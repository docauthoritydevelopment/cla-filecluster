package com.cla.filecluster.repository.solr.infra;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

import java.util.Collection;
import java.util.List;

/**
 * SOLR client proxy, implements SolrClientAdapter interface,
 * adds no functionality, i.e. is transparent. <br/>
 * Extending classes can implement only relevant methods, leaving the rest of them as is
 */
public class SolrClientAdapterSupport implements SolrClientAdapter{

    private SolrClient solrClient;

    public SolrClientAdapterSupport(SolrClient solrClient) {
        this.solrClient = solrClient;
    }

    @Override
    public SolrClient getUnderyingClient() {
        return solrClient;
    }

    @Override
    public boolean isSolrCloudClient() {
        return solrClient instanceof CloudSolrClient;
    }

    @Override
    public UpdateResponse deleteByQuery(String query) throws Exception {
        UpdateResponse resp = solrClient.deleteByQuery(query);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse deleteById(String id) throws Exception {
        UpdateResponse resp = solrClient.deleteById(id);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse deleteByIds(List<String> ids) throws Exception {
        UpdateResponse resp = solrClient.deleteById(ids);
        return handleBadResponse(resp);
    }

    @Override
    public QueryResponse query(SolrQuery query, SolrRequest.METHOD method) throws Exception {
        QueryResponse resp = solrClient.query(query, method);
        return handleBadResponse(resp);
    }

    @Override
    public QueryResponse query(SolrQuery query) throws Exception {
        QueryResponse resp = solrClient.query(query);
        return handleBadResponse(resp);
    }

    @Override
    public QueryResponse query(SolrQuery query, SolrRequest.METHOD method, int retryCount) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public QueryResponse query(SolrQuery query, int retryCount) throws Exception {
        throw new UnsupportedOperationException();
    }

    @Override
    public UpdateResponse commit() throws Exception {
        UpdateResponse resp = solrClient.commit();
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse commit(String collection, boolean waitFlush, boolean waitSearcher) throws Exception {
        UpdateResponse resp = solrClient.commit(collection, waitFlush, waitSearcher);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse commit(boolean waitFlush, boolean waitSearcher, boolean softCommit) throws Exception {
        UpdateResponse resp = solrClient.commit(waitFlush, waitSearcher, softCommit);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse optimize(boolean waitFlush, boolean waitSearcher, int maxSegments) throws Exception {
        UpdateResponse resp = solrClient.optimize(waitFlush, waitSearcher, maxSegments);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse optimize() throws Exception {
        UpdateResponse resp = solrClient.optimize();
        return handleBadResponse(resp);
    }

    @Override
    public SolrPingResponse ping() throws Exception {
        return solrClient.ping();
    }

    @Override
    public UpdateResponse add(SolrInputDocument doc) throws Exception {
        UpdateResponse resp = solrClient.add(doc);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse add(SolrInputDocument doc, boolean shouldRetry) throws Exception {
        UpdateResponse resp = solrClient.add(doc);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse add(SolrInputDocument doc, int commitWithinMs) throws Exception {
        UpdateResponse resp = solrClient.add(doc, commitWithinMs);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse add(Collection<SolrInputDocument> docs, int commitWithinMs) throws Exception {
        UpdateResponse resp = solrClient.add(docs, commitWithinMs);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse add(Collection<SolrInputDocument> docs) throws Exception {
        UpdateResponse resp = solrClient.add(docs);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse addBean(Object obj) throws Exception {
        UpdateResponse resp = solrClient.addBean(obj);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse addBean(Object obj, int commitWithinMs) throws Exception {
        UpdateResponse resp = solrClient.addBean(obj, commitWithinMs);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse addBeans(List<?> entities) throws Exception {
        UpdateResponse resp = solrClient.addBeans(entities);
        return handleBadResponse(resp);
    }

    @Override
    public UpdateResponse addBeans(List<?> entities, int commitWithin) throws Exception{
        UpdateResponse resp = solrClient.addBeans(entities,commitWithin);
        return handleBadResponse(resp);
    }

    private UpdateResponse handleBadResponse(UpdateResponse resp) {
        if (resp == null) {
            throw new RuntimeException("No response from Solr");
        }
        return resp;
    }

    private QueryResponse handleBadResponse(QueryResponse resp) {
        if (resp == null) {
            throw new RuntimeException("No response from Solr");
        }
        return resp;
    }
}
