package com.cla.filecluster.repository;

import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.SimilarityMap;
import com.cla.common.domain.dto.pv.SimilarityMapKey;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;

import java.util.Collection;

public interface PvAnalyzerRepository {
    public static final String SHEET_HANDLER = "/sheetsimilarity";
    public static final String DOC_HANDLER = "/docsimilarity";
    public static final String WORKBOOK_HANDLER = "/workbooksimilarity";


    SimilarityMap analyzePvForFile(final ContentMetadataDto claFileDto, final String pvName, final ClaHistogramCollection<Long> claHistogramCollection);
    SimilarityMap analyzePvForPart(final ContentMetadataDto claFileDto, final String part, final String pvName, final ClaHistogramCollection<Long> claHistogramCollection);

    Collection<Long> findWordGroupCandidates(ContentMetadataDto claFileDto, String customThresholds);

    Collection<SimilarityMapKey> findWordGroupCandidates(ContentMetadataDto claFileDto, PvAnalysisData pvAnalysisData, String customThresholds);

    Collection<SimilarityMapKey> analyzeAllPvsForSheet(ContentMetadataDto claFileDto, ClaSuperCollection<Long> partPvCollection);

    Collection<Long> analyzeAllPvsForWorkBook(ContentMetadataDto contentMetadataDto);
}
