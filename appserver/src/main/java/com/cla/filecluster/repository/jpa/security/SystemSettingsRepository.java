package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.SystemSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SystemSettingsRepository extends JpaRepository<SystemSettings,Long> {

    @Query("select usd from SystemSettings usd where usd.name = :name")
    List<SystemSettings> findByName(@Param("name") String name);

    @Modifying
    @Query(value = "delete from SystemSettings df where df.name = :name")
    int deleteByName(@Param("name") String name);

    @Query("select usd from SystemSettings usd where usd.name like :prefix%")
    List<SystemSettings> findByPrefix(@Param("prefix") String prefix);
}
