package com.cla.filecluster.repository.solr;

public enum FacetSortType {
    INDEX("index"),
    COUNT("count")
    ;

    private String name;

    FacetSortType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
