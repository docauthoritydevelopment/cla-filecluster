package com.cla.filecluster.repository.solr;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.util.NamedList;
import org.springframework.data.domain.PageRequest;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Created by uri on 24/01/2016.
 */
public class SolrQueryResponse<T> {

    QueryResponse queryResponse;

    private PageRequest pageRequest;
    private String cursorMark;
    private Class<T> beansType;

    public SolrQueryResponse(QueryResponse queryResponse, PageRequest pageRequest, Class<T> beansType) {
        this.queryResponse = queryResponse;
        this.pageRequest = pageRequest;
        this.beansType = beansType;
        cursorMark = (queryResponse==null)?null:queryResponse.getNextCursorMark();
    }

    public QueryResponse getQueryResponse() {
        return queryResponse;
    }

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }

    public String getCursorMark() {
        return cursorMark;
    }

    public List<String> getHighlighting(String fileId) {
        if (queryResponse.getHighlighting() != null && queryResponse.getHighlighting().get(fileId) != null) {
            return queryResponse.getHighlighting().get(fileId).get("content");
        }
        return Collections.EMPTY_LIST;
    }

    public void setCursorMark(String cursorMark) {
        this.cursorMark = cursorMark;
    }

    public List<T> getEntites() {
        return Optional.of(queryResponse).map(qr -> qr.getBeans(beansType)).orElse(Collections.emptyList());
    }

}
