package com.cla.filecluster.repository.jpa.schedule;

import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.schedule.RootFolderSchedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.List;

public interface RootFolderScheduleRepository extends JpaRepository<RootFolderSchedule,Long> {

    @Query("select rfs from RootFolderSchedule rfs left join rfs.scheduleGroup where rfs.rootFolder.id = :rootFolderId")
    List<RootFolderSchedule> findRootFolderSchedules(@Param("rootFolderId") Long rootFolderId);

    @Query("select rfs from RootFolderSchedule rfs left join rfs.scheduleGroup where rfs.scheduleGroup.id = :scheduleGroupId")
    List<RootFolderSchedule> findRootFolderSchedulesByScheduleGroup(@Param("scheduleGroupId") Long scheduleGroupId);

    @Query("select rf from RootFolderSchedule rfs " +
            "left join rfs.rootFolder rf " +
            "left join rfs.scheduleGroup " +
            "where rfs.scheduleGroup.id = :scheduleGroupId and rf.deleted = false")
    Page<RootFolder> findRootFoldersByScheduleGroup(@Param("scheduleGroupId") Long scheduleGroupId, Pageable pageable);

    @Query("select rf from RootFolderSchedule rfs " +
            "left join rfs.rootFolder rf " +
            "left join rfs.scheduleGroup " +
            "where rfs.scheduleGroup.id = :scheduleGroupId and rf.deleted = false")
    List<RootFolder> findRootFoldersByScheduleGroup(@Param("scheduleGroupId") Long scheduleGroupId);

    @Query("select rf from RootFolderSchedule rfs " +
            "left join rfs.rootFolder rf " +
            "left join rfs.scheduleGroup " +
            "where rfs.scheduleGroup.id = :scheduleGroupId and rf.deleted = false and (lower(rf.realPath) like %:namingSearchTerm% or " +
            "lower(rf.path) like %:namingSearchTerm% or lower(rf.nickName) like %:namingSearchTerm% or " +
            "lower(rf.description) like %:namingSearchTerm% or lower(rf.mailboxGroup) like %:namingSearchTerm%)")
    Page<RootFolder> findLikeNamingByScheduleGroup(@Param("scheduleGroupId") Long scheduleGroupId, @Param("namingSearchTerm") String namingSearchTerm, Pageable pageRequest);

    @Query(nativeQuery = true, value = "select rf.id " +
            "from root_folder_schedule sh " +
            "left join root_folder rf on sh.root_folder_id = rf.id " +
            "left join crawl_runs cr on cr.id = rf.last_run_id " +
            "where sh.schedule_group_id = :scheduleGroupId and rf.deleted = false " +
            "and (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
            " or lower(rf.nick_name) like %:namingSearchTerm% or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%) " +
            "and cr.run_status in (:states) LIMIT :start,:count")
    List<BigInteger> findLikeNamingByScheduleGroupWithStates(@Param("scheduleGroupId") Long scheduleGroupId, @Param("namingSearchTerm") String namingSearchTerm, @Param("states") Iterable<String> states, @Param("start") long offset, @Param("count") int count);

    @Query(nativeQuery = true, value = "select count(*) " +
            "from root_folder_schedule sh " +
            "left join root_folder rf on sh.root_folder_id = rf.id " +
            "left join crawl_runs cr on cr.id = rf.last_run_id " +
            "where sh.schedule_group_id = :scheduleGroupId and rf.deleted = false " +
            "and (lower(rf.real_path) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
            " or lower(rf.nick_name) like %:namingSearchTerm% or lower(rf.mailbox_group) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%) " +
            "and cr.run_status in (:states)")
    long findLikeNamingByScheduleGroupWithStatesNum(@Param("scheduleGroupId") Long scheduleGroupId, @Param("namingSearchTerm") String namingSearchTerm, @Param("states") Iterable<String> states);

}
