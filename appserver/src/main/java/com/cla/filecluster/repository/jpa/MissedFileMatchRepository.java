package com.cla.filecluster.repository.jpa;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cla.filecluster.domain.entity.pv.MissedFileMatch;

public interface MissedFileMatchRepository extends JpaRepository<MissedFileMatch, Long> {

	@Query("select mfm from MissedFileMatch mfm where (mfm.file1Id = :id and mfm.processed=false) or (mfm.file2Id = :id and mfm.processed=false)")
	Page<MissedFileMatch> findByFileIdPaged(@Param("id") Long id, Pageable pageable);

	@Query("select mfm from MissedFileMatch mfm where mfm.processed=false")
	Page<MissedFileMatch> findAllUnprocessedPage(Pageable pageRequest);

	@Modifying
	@Transactional
	@Query("update MissedFileMatch mfm set mfm.processed=true where mfm.processed=false")
	int markAllProcessed();

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "TRUNCATE TABLE missed_file_match")
	void truncateMissedFileMatchTable();
}
