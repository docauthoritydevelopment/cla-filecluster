package com.cla.filecluster.repository.jpa.doctype;

import com.cla.filecluster.domain.entity.doctype.DocTypeRiskInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by uri on 17/08/2015.
 */

public interface DocTypeRiskInformationRepository extends JpaRepository<DocTypeRiskInformation, Long>, JpaSpecificationExecutor<DocTypeRiskInformation> {

    @Query(value = "select dtr from DocTypeRiskInformation dtr where dtr.name = :name")
    DocTypeRiskInformation findByName(@Param("name") String name);

}
