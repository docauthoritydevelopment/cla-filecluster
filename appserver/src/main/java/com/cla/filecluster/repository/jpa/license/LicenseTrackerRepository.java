package com.cla.filecluster.repository.jpa.license;

import com.cla.filecluster.domain.entity.license.LicenseResource;
import com.cla.filecluster.domain.entity.license.LicenseTracker;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Itay on 06/10/2019.
 */
public interface LicenseTrackerRepository extends JpaRepository<LicenseTracker,String>{
}
