package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.entity.security.UserSavedData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 27/04/2016.
 */
public interface UserSavedDataRepository extends JpaRepository<UserSavedData,Long>{

    @Query("select usd from UserSavedData usd where usd.user = :userName and usd.key = :key")
    List<UserSavedData> findByUserAndKey(@Param("userName") String userName, @Param("key") String key);
}
