package com.cla.filecluster.repository.jpa;

import com.cla.filecluster.domain.entity.pv.ScanError;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ScanErrorsRepository extends JpaRepository<ScanError, Long> {

    @Query("select count(se) from ScanError se where se.runId = :runId")
    long countByRunId(@Param("runId") Long crawlRunId);

    @Modifying
    @Query("delete from ScanError ef where ef.rootFolderId = :rootFolderId")
    int deleteByRootFolder(@Param("rootFolderId") Long rootFolderId);
}
