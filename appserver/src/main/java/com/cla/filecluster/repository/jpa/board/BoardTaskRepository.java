package com.cla.filecluster.repository.jpa.board;

import com.cla.filecluster.domain.entity.board.BoardTask;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface BoardTaskRepository extends JpaRepository<BoardTask, Long> {

    @Query("select br from BoardTask br where br.ownerId = :ownerId and br.boardTaskState <> 'DONE'")
    Page<BoardTask> findByOwnerPageRequest(@Param("ownerId") long ownerId, Pageable pageRequest);

    @Query("select br from BoardTask br where br.assignee = :assignee and br.boardTaskState <> 'DONE'")
    Page<BoardTask> findByAssigneePageRequest(@Param("assignee") long assignee, Pageable pageRequest);

    @Query("select br from BoardTask br where br.boardId = :boardId and br.ownerId = :ownerId and br.boardTaskState <> 'DONE'")
    Page<BoardTask> findByOwnerAndBoard(@Param("boardId") long boardId, @Param("ownerId") long ownerId, Pageable pageRequest);

    @Query("select br from BoardTask br where br.boardId = :boardId and br.assignee = :assignee and br.boardTaskState <> 'DONE'")
    Page<BoardTask> findByAssigneeAndBoard(@Param("boardId") long boardId, @Param("assignee") long assignee, Pageable pageRequest);

    @Query("select br from BoardTask br where br.boardId = :boardId")
    Page<BoardTask> findByBoard(@Param("boardId") long boardId, Pageable pageRequest);

    @Modifying
    @Query("delete from BoardTask br where br.boardId = :boardId")
    int deleteByBoard(@Param("boardId") long boardId);
}
