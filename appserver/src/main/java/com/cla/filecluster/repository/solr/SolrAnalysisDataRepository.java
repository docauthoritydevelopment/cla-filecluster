package com.cla.filecluster.repository.solr;

import com.cla.common.constants.DefaultSolrSchema;
import com.cla.common.domain.dto.file.FileCollections;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.domain.entity.pv.PvAnalysisDataFormat;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.crawler.analyze.AnalyzeAppService;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Base64;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by uri on 31/01/2017.
 */
@Repository //TODO create custom DocumentObjectBinder for the client and switch to using beans
public class SolrAnalysisDataRepository extends SolrRepository {

    private static Base64.Encoder encoder = Base64.getEncoder();

    @Value("${solr.analysisData.compress:false}")
    private boolean compressAnalysisData;

    @Value("${solr.analysisData.arrays-conversion:true}")
    private boolean pvArraysConversion;

    @Value("${solr.analysisData.arrays-conversion-percentage:-1}")
    private int selectiveArraysConversionPercentage;

    @Value("${analysisData.solr.core:AnalysisData}")
    private String analysisDataSolrCore;

    @Value("#{${solr.analysisData.deserialize.compatibilityMap:" +
            "{'com.cla.common.domain.dto.FileType':'com.cla.connector.domain.dto.file.FileType'}}}")
    private Map<String, String> compatibilityMap;

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected String coreName() {
        return analysisDataSolrCore;
    }

    /*public void createDocument(PvAnalysisData pvAnalysisData) {
        SolrInputDocument inputDocument = createInputDocument(pvAnalysisData);
        try {
            client.add(inputDocument, COMMIT_WITHIN_MS);
        }
        catch (Exception e) {
            logger.error("Failed to create PvAnalysis data in Solr", e);
            throw new RuntimeException("Failed to create PvAnalysis data in Solr. content id:" + pvAnalysisData.getContentId(), e);
        }
    }
*/
    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    public SolrInputDocument createInputDocument(PvAnalysisData pvAnalysisData) {
        final SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", pvAnalysisData.getContentId());
        boolean doArraysConversion = (pvArraysConversion &&
                (selectiveArraysConversionPercentage < 0 || AnalyzeAppService.isAboveHashedPercentage(pvAnalysisData.getContentId(), selectiveArraysConversionPercentage)));
        FileCollections pvColls = doArraysConversion ?
                pvAnalysisData.getPvCollections().getArraysCollections() :
                pvAnalysisData.getPvCollections();
        byte[] bytes = PvAnalysisDataUtils.serializePvCollections(pvColls);
        doc.setField("maturity", pvAnalysisData.getMaturityFile());
        doc.setField("pvCollections", bytes);
        doc.setField("data_size", bytes.length);
        bytes = PvAnalysisDataUtils.serializeAnalysisMetadata(pvAnalysisData.getAnalysisMeadata());
        doc.setField("metadata", bytes);
        doc.setField("format",
                doArraysConversion ? PvAnalysisDataFormat.PV_ARRAYS.ordinal() : PvAnalysisDataFormat.PV_COLLECTIONS.ordinal());
        if (logger.isTraceEnabled() && doArraysConversion) {
            logger.trace("Write Arrays PV-Collections for content {}: {}", pvAnalysisData.getContentId(),
                    pvColls.getParts().stream().collect(Collectors.joining(",")));
        }
        return doc;
    }

    public PvAnalysisData findByContentId(long contentId) {
        SolrDocument document = getDocument(contentId);
        return PvAnalysisDataUtils.convertDocumentToPvAnalysisData(document, compressAnalysisData, compatibilityMap);
    }

    public List<PvAnalysisData> findByContentId(Collection<Long> contentIdList) {
        List<SolrDocument> documents = findDocuments(contentIdList);
        return documents.stream()
                .map(f -> PvAnalysisDataUtils.convertDocumentToPvAnalysisData(f, compressAnalysisData, compatibilityMap))
                .collect(Collectors.toList());
    }


    @Deprecated
    private SolrDocument getDocument(long id) {
        final Query query = Query.create().addFilterWithCriteria(Criteria.create(DefaultSolrSchema.ID, SolrOperator.EQ, id));
        try {
            QueryResponse queryResponse = getSolrClient().query(query.build());
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                return null;
            }
            return queryResponse.getResults().get(0);
        } catch (Exception e) {
            logger.error("Failed to find document  by id - solr exception", e);
            throw new RuntimeException("Failed to find document by id - solr exception", e);
        }

    }


}