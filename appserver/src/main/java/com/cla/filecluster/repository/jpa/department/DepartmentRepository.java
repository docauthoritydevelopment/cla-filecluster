package com.cla.filecluster.repository.jpa.department;

import com.cla.filecluster.domain.entity.department.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * Created by ginat on 12/03/2019.
 */
public interface DepartmentRepository extends JpaRepository<Department,Long> {

    @Query(value = "select dp from Department dp where dp.deleted = false")
    Page<Department> findAllNotDeleted(Pageable pageRequest);

    @Query("select dp from Department dp where dp.name = :name")
    List<Department> findByName(@Param("name") String name);

    @Query("select dp from Department dp where dp.name like %:name% and dp.deleted = false")
    List<Department> findByNameLike(@Param("name") String name);

    @Query("select dp from Department dp where dp.uniqueName = :name")
    List<Department> findByUniqueName(@Param("name") String name);

    @Query(value = "select dp from Department dp where dp.id in :ids")
    List<Department> findByIds(@Param("ids") Collection<Long> ids);

    @Query(value = "select dp from Department dp where dp.parent.id = :parentId and dp.deleted = false")
    List<Department> findByParentNotDeleted(@Param("parentId") Long parentId);

    @Query("select dp from Department dp where dp.name = :name and dp.parent.id = :parentId and dp.deleted = false")
    Department findByParentAndNameNotDeleted(@Param("parentId") Long parentId, @Param("name") String name);

    @Query("select dp from Department dp where dp.name = :name and dp.parent.id is null and dp.deleted = false")
    Department findByNullParentAndNameNotDeleted(@Param("name") String name);

    @Query(nativeQuery = true, value = "select dp.id, dp.name from Department dp order by dp.name")
    List<Object[]> getDepartmentsNames();
}
