package com.cla.filecluster.repository.file;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 29/05/2016.
 */
public class MediaFolderWithFiles {

    protected String folderId;
    protected List<String> fileIds = new ArrayList<>();

    public MediaFolderWithFiles(String folderId) {
        this.folderId = folderId;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public List<String> getFileIds() {
        return fileIds;
    }

    public void setFileIds(List<String> fileIds) {
        this.fileIds = fileIds;
    }

    @Override
    public String toString() {
        return "MediaFolderWithFiles{" +
                "folderId='" + folderId + '\'' +
                "files='" + fileIds.size() +'\''+
                '}';
    }
}
