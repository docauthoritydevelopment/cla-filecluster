package com.cla.filecluster.repository.jpa.searchpattern;

import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * Created by uri on 23/06/2016.
 */
public interface TextSearchPatternRepository extends JpaRepository<TextSearchPattern,Integer> {
    @Query(value = "select tsp from TextSearchPattern tsp join fetch tsp.subCategories sCat",countQuery = "select count(tsp) from TextSearchPattern tsp")
    Page<TextSearchPattern> findAllWithFilters(Pageable pageable);

    @Query(value = "select tsp from TextSearchPattern tsp join fetch tsp.subCategories sCat where (lower(tsp.name) like %:namingSearchTerm% or lower(sCat.name) like %:namingSearchTerm% or lower(sCat.parent.name) like %:namingSearchTerm%)",
            countQuery = "select count(tsp) from TextSearchPattern tsp inner join tsp.subCategories sCat where (lower(tsp.name) like %:namingSearchTerm% or lower(sCat.name) like %:namingSearchTerm% or lower(sCat.parent.name) like %:namingSearchTerm%)")
    Page<TextSearchPattern> findAllWithFiltersAndSearch(Pageable pageable,@Param("namingSearchTerm") String namingSearchTerm);

    @Query(value = "select tsp from TextSearchPattern tsp inner join tsp.regulations reg join fetch tsp.subCategories sCat where reg.id in (:regulationIds)",countQuery = "select count(tsp) from TextSearchPattern tsp inner join tsp.regulations reg where reg.id in (:regulationIds)")
    Page<TextSearchPattern> findAllWithFiltersAndRegulations (Pageable pageable, @Param("regulationIds") Collection<Integer> regulationIds);

    @Query(value = "select tsp from TextSearchPattern tsp inner join tsp.regulations reg join fetch tsp.subCategories sCat where (lower(tsp.name) like %:namingSearchTerm% or lower(sCat.name) like %:namingSearchTerm% or lower(sCat.parent.name) like %:namingSearchTerm%) and reg.id in (:regulationIds) ",
            countQuery = "select count(tsp) from TextSearchPattern tsp inner join tsp.regulations reg  inner join tsp.subCategories sCat where (lower(tsp.name) like %:namingSearchTerm% or lower(sCat.name) like %:namingSearchTerm% or lower(sCat.parent.name) like %:namingSearchTerm%) and reg.id in (:regulationIds) ")
    Page<TextSearchPattern> findAllWithFiltersAndSearchAndRegulations(Pageable pageable,@Param("namingSearchTerm") String namingSearchTerm, @Param("regulationIds") Collection<Integer> regulationIds);

    @Query("SELECT tsp FROM TextSearchPattern tsp WHERE tsp.name = :name")
    TextSearchPattern findByName(@Param("name") String name);

    @Query("SELECT tsp FROM TextSearchPattern tsp join fetch tsp.subCategories sCat WHERE sCat.id = :subCategoryId")
    List<TextSearchPattern> findBySubCategoryId(@Param("subCategoryId") Integer parentId);

    @Query("SELECT tsp FROM TextSearchPattern tsp inner join tsp.regulations reg WHERE reg.id = :regulationId")
    List<TextSearchPattern> findByRegulationId(@Param("regulationId") Integer regulationId);

    @Query(value = "select tsp FROM TextSearchPattern tsp where tsp.id in :ids")
    List<TextSearchPattern> findByIds(@Param("ids") Collection<Integer> patternIds);

    @Query("select tsp from TextSearchPattern tsp where tsp.name like %:name%")
    List<TextSearchPattern> findByNameLike(@Param("name") String name);
}
