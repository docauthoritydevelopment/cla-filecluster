package com.cla.filecluster.repository;

import com.cla.common.domain.dto.histogram.ClaSuperCollection;

public interface PvEntriesIngesterRepository {
	void saveHistograms(Long taskId,final String fileName, final Long docLid, final String part,final ClaSuperCollection<Long> longHistograms, Integer maturityFileLevel);

	void optimizePvIndex();
}
