package com.cla.filecluster.repository.jpa.actions;

import com.cla.filecluster.domain.entity.actions.ActionParameterDefinition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 25/01/2016.
 */
public interface ActionParameterDefinitionRepository extends JpaRepository<ActionParameterDefinition,String> {

    @Query("select apd from ActionParameterDefinition apd where apd.actionDefinition.id = :actionId")
    List<ActionParameterDefinition> findByActionId(@Param("actionId") String actionId);
}
