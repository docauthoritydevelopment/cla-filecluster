package com.cla.filecluster.repository.jpa.diagnostics;

import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DiagnosticsRepository extends JpaRepository<Diagnostic,Long> {

    @Query(value = "select go from Diagnostic go where go.startTime <= UNIX_TIMESTAMP(now())*1000 order by go.startTime")
    Page<Diagnostic> findAllPending(Pageable pageable);

    @Query(nativeQuery = true, value = "select distinct diagnostic_type from diagnostic")
    List<Object> getAllExistingTypes();
}
