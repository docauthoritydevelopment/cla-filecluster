package com.cla.filecluster.repository.solr;

import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.service.DeepPageRequest;
import com.cla.filecluster.service.extractor.entity.ContentSearchTextService;
import com.google.common.base.Strings;
import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.FILE_ID;

/**
 * This class will be phased out, SolrQueryGenerator will be used instead
 */
@Service
@Deprecated
public class SolrSpecQueryBuilder {

    private static final String QUERY_DA_SEARCH_COMPONENT_REQUEST_HANDLER = "/query/da";

    private final static Logger logger = LoggerFactory.getLogger(SolrSpecQueryBuilder.class);

    @Autowired
    private SolrQueryBuilder solrQueryBuilder;

    @Autowired
    private ContentSearchTextService contentSearchTextService;

    @Value("${content.filter.proximity.value:20}")
    private String contentFilterProximityValue;

    @Value("${catFile.facet.maxLimit:10000}")
    private int facetMaxLimit;

    @Value("${catFile.search.content.use-da-plugin:false}")
    private boolean useDaContentSearchPlugin;

    @Value("${fileDTO.solr.core:Extraction}")
    private String extractionSolrCore;


    public SolrQuery buildSolrQuery(SolrSpecification solrSpecification, PageRequest pageRequest) {
        final SolrQuery query = new SolrQuery();
        query.setQuery(solrSpecification.getBaseQuery());
        if (useDaContentSearchPlugin) {
            query.setRequestHandler(QUERY_DA_SEARCH_COMPONENT_REQUEST_HANDLER);
        }
        for (String filterQuery : solrQueryBuilder.getFilterQueries(solrSpecification)) {
            if (Strings.isNullOrEmpty(filterQuery)) {
                continue;
            }
            query.addFilterQuery(filterQuery);
        }
        if (solrSpecification.getContentFilters().size() > 0) {
            if (useDaContentSearchPlugin) {
                handleContentFilter(solrSpecification, query);
            } else {
                handleContentFilterLegacy(solrSpecification, query);
            }
        }
        if (solrSpecification.getFileID() > 0) {
            query.addFilterQuery(FILE_ID.filter(solrSpecification.getFileID()));
        }

        if (solrSpecification.isUsingJsonFacet()) {
            configureFacetJsonObjectQuery(solrSpecification, pageRequest, query);
            logger.debug("run Solr Query: {}", query.toString());
        }
        else if (solrSpecification.getFacetFields() != null) {
            configureFacetQuery(solrSpecification, pageRequest, query);
        } else if (solrSpecification.getFacetRangeField() != null) {
            configureFacetRangeQuery(solrSpecification, pageRequest, query);
        } else {
            if (solrSpecification.getGroupByField() != null) {
                configureGroupByQuery(solrSpecification, query);
            }
            if (pageRequest instanceof DeepPageRequest) {
                query.set("cursorMark", ((DeepPageRequest) pageRequest).getDeepPageMarker());
                query.setRows(pageRequest.getPageSize());
            } else if (pageRequest != null) {
                query.setStart((int) pageRequest.getOffset());
                query.setRows(pageRequest.getPageSize());
            } else {
                query.setRows(0);
            }
            if (solrSpecification.getSortField() != null) {
                query.setSort(solrSpecification.getSortField(), solrSpecification.getSortOrder());
            }
            logger.debug("run Solr Query: {}", query.toString());
        }
        return query;
    }


    private void configureGroupByQuery(SolrSpecification solrSpecification, SolrQuery query) {
        query.set("group", "true");
        query.set("group.field", solrSpecification.getGroupByField());
        if (solrSpecification.isGroupMain()) {
            query.set("group.main", "true");
        }
    }




    private void handleContentFilterLegacy(SolrSpecification solrSpecification, SolrQuery query) {
        List<String> contentFilters = solrSpecification.getContentFilters();

        String result = generateContentFilterQuery(contentFilters);
        if (result == null) {
            logger.info("content filters {} translated into empty search - ignore", contentFilters);
        } else {
            query.addFilterQuery(result);
            if (logger.isDebugEnabled() && contentFilters.size() > 0) {
                logger.debug("Query FQ={}", Arrays.toString(query.getFilterQueries()));
            }
        }
    }

    private String generateContentFilterQuery(List<String> contentFilters) {
        String hashedContentSearchText = convertContentfiltersSingleQuery(contentFilters);
        if (hashedContentSearchText == null) {
            return null;
        }
        return createLegacyFileCatContentJoinFilter(hashedContentSearchText, true);
    }

    private String createLegacyFileCatContentJoinFilter(String hashedContentSearchText, boolean addFieldName) {
        return "{!dajoin from=id to=contentId fromIndex=" + extractionSolrCore +
                (addFieldName?"}content:":"}") +
                hashedContentSearchText;
    }

    private void handleContentFilter(SolrSpecification solrSpecification, SolrQuery query) {
        List<String> contentFilters = solrSpecification.getContentFilters();
        if (contentFilters.size() == 0) {
            return;
        }

        // Convert all content filters into a single content query that will be translated into a single JOIN query
        String hashedContentSearchText = convertContentfiltersSingleQuery(contentFilters);
        if (hashedContentSearchText == null) {
            return;
        }

        query.setParam(ClaSearchParams.JOIN_COLLECTION, "");
        addContentJoinFilter(query, hashedContentSearchText);
        logger.debug("Created doc-authority join query on {}", hashedContentSearchText);
    }

    private void addContentJoinFilter(SolrQuery query, String hashedContentSearchText) {
        query.setParam(ClaSearchParams.JOIN_COLLECTION, extractionSolrCore);
        query.setParam(ClaSearchParams.JOIN_FROM_FIELD, "id");
        query.setParam(ClaSearchParams.JOIN_TO_FIELD, "contentId");
        query.setParam(ClaSearchParams.JOIN_QUERY_FIELD, "content");
        query.setParam(ClaSearchParams.JOIN_QUERY_VALUE, hashedContentSearchText);
    }

    private String convertContentfiltersSingleQuery(List<String> contentFilters) {
        int added = 0;
        String hashedContentSearchText = "";
        StringJoiner sj = new StringJoiner(" AND ", "(", ")");
        for (String contentSearchText : contentFilters) {
            hashedContentSearchText = contentSearchTextService.convertContentSearchText(contentSearchText, false);
            if (hashedContentSearchText != null) {
                sj.add(hashedContentSearchText);
                added++;
            }
        }
        if (contentFilters.size() > 1 && added > 0) {
            hashedContentSearchText = sj.toString();
        }
        return hashedContentSearchText;
    }

    private void configureFacetQuery(SolrSpecification solrSpecification, PageRequest pageRequest, SolrQuery query) {
        query.setFacet(true);
        if (solrSpecification.getFacetQueryValues() != null) {
            String facetField = solrSpecification.getFacetFields()[0];
            for (String queryValue : solrSpecification.getFacetQueryValues()) {
                query.addFacetQuery(facetField + ":" + queryValue);
            }
        } else {
            for (String facetField : solrSpecification.getFacetFields()) {
                query.addFacetField(facetField);
            }
        }

        if (solrSpecification.getFacetPrefix() != null) {
            query.setFacetPrefix(solrSpecification.getFacetPrefix());
        }
        if (pageRequest != null) {
            query.setFacetLimit(pageRequest.getPageSize());
            query.set("facet.offset", (int) pageRequest.getOffset());
        } else {
            query.setFacetLimit(facetMaxLimit);
        }
        query.setRows(0);
        if (solrSpecification.getMinCount().isPresent()) {
            query.setFacetMinCount(solrSpecification.getMinCount().get());
        }
        logger.debug("run Facet Query: {}", query.toString());
    }

    private void configureFacetRangeQuery(SolrSpecification solrSpecification, PageRequest pageRequest, SolrQuery query) {
        query.setFacet(true);
        if (pageRequest != null) {
            query.setFacetLimit(pageRequest.getPageSize());
            query.set("facet.offset", (int) pageRequest.getOffset());
        }
        query.setRows(0);

        if (solrSpecification.getMinCount().isPresent()) {
            query.setFacetMinCount(solrSpecification.getMinCount().get());
        }

        String facetRangeField = solrSpecification.getFacetRangeField().getSolrName();
        Date facetDateStart = convertToDate(solrSpecification.getFacetDateStart());
        Date facetDateEnd = convertToDate(solrSpecification.getFacetDateEnd());
        if (solrSpecification.getFacetRangeField().getType().equals(LocalDateTime.class)) {
            query.addDateRangeFacet(facetRangeField, facetDateStart, facetDateEnd, solrSpecification.getFacetRangeGap());
        } else {
            throw new RuntimeException("Facet range on numeric fields still not supported");
        }
        logger.debug("run Facet Range Query: {}", query.toString());

    }

    private void configureFacetJsonObjectQuery(SolrSpecification solrSpecification, PageRequest pageRequest, SolrQuery query) {
        query.setFacet(true);
            List<SolrFacetQueryJson> facetJsonObjectList =  solrSpecification.getFacetJsonObjectList();
        List<SolrFacetQueryJson> facetsToConfigure = facetJsonObjectList.stream()
                .filter(jsonFacet -> jsonFacet.getType() != FacetType.SIMPLE).collect(Collectors.toList());
        PageRequest concretePageRequest = pageRequest == null ? PageRequest.of(0, facetMaxLimit) : pageRequest;
                if (pageRequest != null) {
            facetsToConfigure.stream()
                    .forEach(jsonFacet -> configurePageRequestIfNeeded(concretePageRequest, jsonFacet));
            }
            query.setRows(0);
        solrSpecification.getMinCount().ifPresent(
                minCount -> configureMinCountIfNeeded(facetsToConfigure, minCount));
        logger.debug("Built json facet Query: {}", query.toString());
            query.add("json.facet", solrSpecification.buildJsonFacet());
        }

    private void configureMinCountIfNeeded(List<SolrFacetQueryJson> facetsToConfigure, Integer minCount) {
        facetsToConfigure.stream()
                .filter(j -> j.getMincount() == null)
                .forEach(jsonFacet -> jsonFacet.setMincount(minCount));
    }

    private void configurePageRequestIfNeeded(PageRequest pageRequest, SolrFacetQueryJson jsonFacet) {
        if (jsonFacet.getLimit() == null) {
            jsonFacet.setLimit(pageRequest.getPageSize());
        }
        if (jsonFacet.getOffset() == null) {
            jsonFacet.setOffset(pageRequest.getOffset());
        }
    }


    /*
    private void configureJsonFacetQuery(SolrSpecification solrSpecification, PageRequest pageRequest, SolrQuery query) {
        query.setFacet(true);
        // json.face may include %1, %2 etc which will be replaced by the ordered facet-fields set for the SolrSpec.
        //      e.g. {x:'sum(%1)'} with facet-fiels set to 'size' will result {x:'sum(size)'}
        if (solrSpecification.isUsingJsonFacet()) {
            String json = solrSpecification.buildJsonFacet();
            if (solrSpecification.getFacetFields() != null) {
                for (int i = 0; i < solrSpecification.getFacetFields().length; i++) {
                    json = json.replaceAll("%" + (i + 1), solrSpecification.getFacetFields()[i]);
                }
            }
            query.add("json.facet", json);
        }
        if (solrSpecification.getFacetPrefix() != null) {
            query.setFacetPrefix(solrSpecification.getFacetPrefix());
        }
        if (pageRequest != null) {
            query.setFacetLimit(pageRequest.getPageSize());
            query.set("facet.offset", (int) pageRequest.getOffset());
        } else {
            query.setFacetLimit(facetMaxLimit);
        }
        query.setRows(0);  //We are setting the rows to 0 becuase the row number  in the solr response is not relevant for facet queries
        if (solrSpecification.getMinCount().isPresent()) {
            query.setFacetMinCount(solrSpecification.getMinCount().get());
        }
        logger.debug("run Facet Query: {}", query.toString());
    }
    */

    private Date convertToDate(LocalDateTime facetDateStart) {
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
    }


}
