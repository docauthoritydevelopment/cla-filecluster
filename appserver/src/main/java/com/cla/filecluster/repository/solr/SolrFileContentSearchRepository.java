package com.cla.filecluster.repository.solr;

import com.cla.common.constants.ExtractionFieldType;
import com.cla.filecluster.domain.dto.FileContent;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SolrFileContentSearchRepository extends SolrRepository {

    @Value("${highlighting.solr.core:FileContent}")
    private String fileContenetSolrCore;

    @Value("${filesContentsTimeToLive:#{null}}")
    private String timeToLive;

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected String coreName() {
        return fileContenetSolrCore;
    }

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    public void save(final FileContent fileContent) {
        try {
            if (timeToLive != null) {
                fileContent.setTimeToLive(timeToLive);
            }
            client.addBean(fileContent);
            client.commit();
        } catch (Exception e) {
            throw new RuntimeException("Could not save fileContent", e);
        }
    }

    public List<String> highlightTokens(final String id, final String tokens) {
        try {
            final SolrQuery query = new SolrQuery();
            query.setQuery(ExtractionFieldType.CONTENT.filter(tokens.replaceAll(" ", "\" \"") + "\""));
            query.setRequestHandler("/query");
            query.addFilterQuery(ExtractionFieldType.ID.filter(id));
            final QueryResponse resp = client.query(query, SolrRequest.METHOD.POST);
            if (resp.getHighlighting().get(id) != null) {
                return resp.getHighlighting().get(id).get(ExtractionFieldType.CONTENT.getSolrName());
            }
            return Lists.newLinkedList();
        }
        catch (Exception e) {
            throw new RuntimeException("Could not save fileContent", e);
        }
    }

    public SolrQueryResponse runQuery(SolrSpecification solrSpecification, PageRequest pageRequest) {
        final SolrQuery query = new SolrQuery();
//		query.setQuery(solrSpecification.getBaseQuery());
        query.setRequestHandler("/query");
        query.addFilterQuery(ExtractionFieldType.ID.filter(solrSpecification.getId()));
        query.setHighlightSnippets(solrSpecification.getHighlightPageSize());
        query.setHighlightFragsize(solrSpecification.getHighlightItemFullLength());
        query.addHighlightField(ExtractionFieldType.CONTENT.getSolrName());
//		query.setHighlightSimplePre("<b class=\"ff\">");
//		query.setHighlightSimplePost("</b>");
        query.setHighlightRequireFieldMatch(true);

        //q=content:"the"&qt=/query&fq=id:911dc595-e750-4ee9-b6d8-74cc143b26f7

//		for (String filterQuery : solrSpecification.getFilterQueries()) { //content filters from filter are added to contentFilter array
//			if (Strings.isNullOrEmpty(filterQuery)) {
//				continue;
//			}
//			query.addFilterQuery(filterQuery);
//		}

        if (solrSpecification.getContentFilters().size() > 0) {

            handleContentFilter(solrSpecification, query);
        }

        //		if (solrSpecification.fileID>0) {
//			query.addFilterQuery("claFileId:" + solrSpecification.fileID);
//		}

        if (pageRequest != null) {
            query.setStart((int) pageRequest.getOffset());
            query.setRows(pageRequest.getPageSize());
        } else {
            query.setRows(0);
        }

//			if (solrSpecification.getSortField() != null) {
//				query.setSort(solrSpecification.getSortField(), solrSpecification.getSortOrder());
//			}

        logger.debug("run Solr Query: {}", query.toString());

        try {
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            return new SolrQueryResponse<FileContent>(queryResponse, pageRequest, getEntityType());
        } catch (Exception e) {
            logger.error("Failed to list catFiles by specification - solr exception (" + query + ")", e);
            throw new RuntimeException("Failed to list cat files by specification - solr exception", e);
        }
    }

    private void handleContentFilter(SolrSpecification solrSpecification, SolrQuery query) {
        List<String> contentFilters = solrSpecification.getContentFilters();

//		List<String> allProcessedTokens = new ArrayList<String>();
//		List<String> processedTokens = new ArrayList<String>();
//		for (String contentToken : contentFilters) {
//			processedTokens = extractTokenizer.fillTokens(extractTokenizer.filter(contentToken),null);
//			allProcessedTokens.addAll(processedTokens);
//		}

        String trimmedTokens = StringUtils.join(contentFilters, " ");
        trimmedTokens = ClientUtils.escapeQueryChars(trimmedTokens);
        query.setQuery(ExtractionFieldType.CONTENT.filter(trimmedTokens));

        logger.debug("Highlight Query for content: {}", trimmedTokens);
    }

}
