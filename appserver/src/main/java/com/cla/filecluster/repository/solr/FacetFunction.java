package com.cla.filecluster.repository.solr;
// this enum is used for define what is the SOLR function that can be use on the accumulated data
public enum FacetFunction {
    COUNT("count"),
    SUM("sum"),
    MIN("min"),
    MAX("max"),
    AVG("avg"),
    UNIQUE("unique"),
    HLL("hll");



    private String name;

    FacetFunction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
