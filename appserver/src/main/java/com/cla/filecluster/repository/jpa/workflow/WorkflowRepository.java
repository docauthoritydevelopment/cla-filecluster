package com.cla.filecluster.repository.jpa.workflow;

import com.cla.common.domain.dto.workflow.WorkflowState;
import com.cla.common.domain.dto.workflow.WorkflowType;
import com.cla.filecluster.domain.entity.workflow.Workflow;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by: yael
 * Created on: 2/21/2018
 */
public interface WorkflowRepository extends JpaRepository<Workflow, Long> {

    @Query(value = "select u from Workflow u where u.workflowType = :workflowType",
            countQuery = "select count(u) from Workflow u where u.workflowType = :workflowType")
    Page<Workflow> getByWorkflowType(@Param("workflowType") WorkflowType workflowType, Pageable pageRequest);

    @Query(value = "select u from Workflow u where u.state in (:states)",
            countQuery = "select count(u) from Workflow u where u.state in (:states)")
    Page<Workflow> getByWorkflowStates(@Param("states") List<WorkflowState> states, Pageable pageRequest);

    @Query(value = "select u from Workflow u where u.state in (:states) and u.workflowType = :workflowType",
            countQuery = "select count(u) from Workflow u where u.state in (:states) and u.workflowType = :workflowType")
    Page<Workflow> getByWorkflowTypeAndStates(@Param("workflowType") WorkflowType workflowType, @Param("states") List<WorkflowState> states, Pageable pageRequest);

    @Query(/*nativeQuery = true,*/
            value = "select wf.state as state,count(distinct wf.id) as count from Workflow wf\n" +
                    "group by wf.state\n" +
                    "order by wf.state asc")
    List<WorkflowStateCount> countWorkflowsByState();


    interface WorkflowStateCount {

        WorkflowState getState();

        int getCount();
    }

}
