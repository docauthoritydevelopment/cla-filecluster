package com.cla.filecluster.repository.jpa.monitor;

import com.cla.filecluster.domain.entity.monitor.ClaComponentStatus;
import com.cla.filecluster.repository.jpa.PartitionManagement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * Created by itay on 22/02/2017.
 */
public interface ClaComponentStatusRepository extends JpaRepository<ClaComponentStatus,Long>, PartitionManagement {

    @Query("select cs from ClaComponentStatus cs where cs.claComponentId = :claComponentId and cs.timestamp <= :timeTo")
    Page<ClaComponentStatus> findByComponentId(@Param("claComponentId") Long claComponentId, @Param("timeTo") Long timeTo, Pageable pageRequest);

    @Query("select cs from ClaComponentStatus cs where  cs.claComponentId = :claComponentId and cs.id = :recordId")
    List<ClaComponentStatus> findByIdAndComponent(@Param("claComponentId") Long claComponentId, @Param("recordId") Long id);

    @Query(nativeQuery=true, value="select get_last_partition_name('mon_component_status')")
    String getLastPartitionName();

    @Query(nativeQuery=true, value="select get_last_partition_date('mon_component_status')")
    Long getLastPartitionDate();

    @Query(nativeQuery=true, value="select get_first_partition_name('mon_component_status')")
    String getFirstPartitionName();

    @Query(nativeQuery=true, value="select get_first_partition_date('mon_component_status')")
    Long getFirstPartitionDate();

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE mon_component_status ADD PARTITION (PARTITION p:partitionId VALUES LESS THAN (:partitionDate))")
    void createPartition(@Param("partitionId") long partitionId, @Param("partitionDate") long partitionDate);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE mon_component_status TRUNCATE PARTITION p:partitionId")
    void truncatePartitionForPartitionId(@Param("partitionId") long partitionId);

    @Modifying
    @Transactional
    @Query( nativeQuery = true,
            value = "ALTER TABLE mon_component_status DROP PARTITION p:partitionId")
    void deletePartitionForPartitionId(@Param("partitionId") long partitionId);
}
