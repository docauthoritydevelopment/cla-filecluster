package com.cla.filecluster.repository.jpa.actions;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 16/02/2016.
 */
public interface ActionExecutionStatusRepository extends JpaRepository<ActionExecutionTaskStatus,Long> {
    @Query("select at from ActionExecutionTaskStatus at where at.status = :runStatus")
    List<ActionExecutionTaskStatus> findByRunStatus(@Param("runStatus") RunStatus runStatus);

    @Query("select at from ActionExecutionTaskStatus at")
    List<ActionExecutionTaskStatus> getActionExecution(Pageable pageable);
}
