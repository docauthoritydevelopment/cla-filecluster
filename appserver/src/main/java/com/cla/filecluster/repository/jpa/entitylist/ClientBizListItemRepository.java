package com.cla.filecluster.repository.jpa.entitylist;

import com.cla.filecluster.domain.entity.bizlist.ClientBizListItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by uri on 31/12/2015.
 */
public interface ClientBizListItemRepository extends JpaRepository<ClientBizListItem, String>, JpaSpecificationExecutor<ClientBizListItem> {

    @Query("select bli from ClientBizListItem bli" +
            " where bli.bizList.id = :bizListId and bli.state not in "+SimpleBizListItemRepository.FILTERED_ITEMS)
//    + " sort by name asc")
    Page<ClientBizListItem> findAllByBizListFiltered(@Param("bizListId") long bizListId, Pageable pageable);

    @Query("select bli from ClientBizListItem bli left join fetch bli.bizListItemAliases" +
            " where bli.sid = :bizListItemId")
    ClientBizListItem findWithAliases(@Param("bizListItemId") String bizListItemId);

    @Query("select bli from ClientBizListItem bli where bli.importId = :importId")
    List<ClientBizListItem> findByImportId(@Param("importId") long importId);
}
