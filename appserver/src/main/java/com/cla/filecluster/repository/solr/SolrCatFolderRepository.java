package com.cla.filecluster.repository.solr;

import com.cla.common.constants.*;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.google.common.base.Strings;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public class SolrCatFolderRepository extends SolrRepository<CatFoldersFieldType, SolrFolderEntity> {

    private static final String UPDATE_FIELDS_REQUEST_HANDLER = "/update/fields";

    @Autowired
    private SequenceIdGenerator sequenceIdGenerator;

    @Value("${catFolders.solr.core:CatFolders}")
    private String catFoldersSolrCore;

    @Override
    protected Class<CatFoldersFieldType> getSchemaType() {
        return CatFoldersFieldType.class;
    }

    @Override
    protected Class<SolrFolderEntity> getEntityType() {
        return SolrFolderEntity.class;
    }

    @Override
    protected String coreName() {
        return catFoldersSolrCore;
    }

    @Override
    protected SolrCoreSchema getIdField() {
        return CatFoldersFieldType.ID;
    }

    public Long getNextAvailableId() {
        return sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.DOC_FOLDER);
    }

    public List<DocFolder> findAll(Query query) {
        return getDocFoldersFromResult(query.build());
    }

    public List<DocFolder> getWithLimit(int offset, int pageSize, Query query) {
        query.setRows(pageSize);
        query.setStart(offset);
        return getDocFoldersFromResult(query.build());
    }

    public List<DocFolder> getWithLimit(int offset, int pageSize, Integer maxDepth) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));
        if (maxDepth != null) {
            Criteria c = Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.LTE, maxDepth);
            query.addFilterWithCriteria(c);
        }
        return getWithLimit(offset, pageSize, query);
    }

    public List<DocFolder> getWithLimit(int offset, int pageSize) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));
        return getWithLimit(offset, pageSize, query);
    }

    public Long getTotalCount(Query query, CatFoldersFieldType facetField, String facetValueToCount) {
        Long total = 0L;
        query.setRows(0);
        query.addFacetField(facetField);
        try {
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    if (facetValueToCount == null) {
                        total += c.getCount();
                    } else if (c.getName().equals(facetValueToCount)) {
                        return c.getCount();
                    }
                }
            }
            return total;
        } catch (Exception e) {
            logger.error("Failed to get folder count from Solr", e);
            throw new RuntimeException("Failed to get folder count from Solr. query:" + query, e);
        }
    }

    public Long getTotalCount(Integer maxDepth) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));

        if (maxDepth != null) {
            Criteria c = Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.LTE, maxDepth);
            query.addFilterWithCriteria(c);
        }

        return getTotalCount(query, CatFoldersFieldType.DELETED, "false");
    }

    public List<DocFolder> getByFolderHash(String hash) {
        Map<SolrCoreSchema, String> filters = new HashMap<>();
        filters.put(CatFoldersFieldType.FOLDER_HASH, hash);
        SolrQuery query = createQuery(filters, null);
        return getDocFoldersFromResult(query);
    }

    private List<DocFolder> getDocFoldersFromResult(SolrQuery query) {
        try {
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            List<SolrFolderEntity> res = queryResponse.getBeans(SolrFolderEntity.class);
            if (res != null && !res.isEmpty()) {
                return res.stream().map(SolrCatFolderRepository::getFromEntity).collect(Collectors.toList());
            }
            return new ArrayList<>();
        } catch (Exception e) {
            logger.error("Failed to get folder from Solr", e);
            throw new RuntimeException("Failed to get folder from Solr. query:" + query, e);
        }
    }

    public DocFolder getByPath(Long rootFolderId, String path) {
        Map<SolrCoreSchema, String> filters = new HashMap<>();
        filters.put(CatFoldersFieldType.PATH, path);
        filters.put(CatFoldersFieldType.DELETED, "false");
        filters.put(CatFoldersFieldType.ROOT_FOLDER_ID, rootFolderId.toString());
        SolrQuery query = createQuery(filters, null);
        return getDocFolderFromResult(query);
    }

    public DocFolder getById(Long docFolderId) {
        Optional<SolrFolderEntity> entity = getOneById(docFolderId);
        DocFolder result = null;
        if (entity.isPresent()) {
            result = getFromEntity(entity.get());
        }
        return result;
    }

    private DocFolder getDocFolderFromResult(SolrQuery query) {
        try {
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            DocFolder folder = null;
            List<SolrFolderEntity> res = queryResponse.getBeans(SolrFolderEntity.class);
            if (res != null && !res.isEmpty()) {
                folder = getFromEntity(res.get(0));
            }
            return folder;
        } catch (Exception e) {
            logger.error("Failed to get folder from Solr", e);
            throw new RuntimeException("Failed to get folder from Solr. query:" + query, e);
        }
    }

    public void updateFolderByIds(Collection<Long> folderIds, CatFoldersFieldType field, Object value, SolrCommitType commitType){
        String query = CatFoldersFieldType.ID.inQuery(folderIds);
        updateFieldByQueryMultiFields(new String[]{field.getSolrName(), CatFoldersFieldType.UPDATE_DATE.getSolrName()},
                new String[]{value == null ? null : String.valueOf(value), String.valueOf(System.currentTimeMillis())}, SolrFieldOp.SET.getOpName(),
                query, null, commitType == null ? SolrCommitType.SOFT_NOWAIT : commitType);
    }

    public static DocFolder getFromEntity(SolrFolderEntity entity) {
        DocFolder folder = new DocFolder();
        folder.setPath(entity.getPath());
        folder.setRealPath(entity.getRealPath());
        folder.setDepthFromRoot(entity.getDepthFromRoot());
        folder.setParentsInfo(entity.getParentsInfo());
        folder.setId(entity.getId());
        folder.setName(entity.getName());
        folder.setAclSignature(entity.getAclSignature());
        folder.setDeleted(entity.getDeleted());
        folder.setAbsoluteDepth(entity.getAbsoluteDepth());
        folder.setMediaEntityId(entity.getMediaEntityId());
        folder.setRootFolderId(entity.getRootFolderId());
        folder.setFolderType(FolderType.valueOf(entity.getType()));
        folder.setFolderHashed(entity.getFolderHash());
        if (entity.getParentFolderId() != null && entity.getParentFolderId() > 0) {
            folder.setParentFolderId(entity.getParentFolderId());
        }
        if (!Strings.isNullOrEmpty(entity.getPathDescriptorType())) {
            folder.setPathDescriptorType(PathDescriptorType.valueOf(entity.getPathDescriptorType()));
        }
        folder.setSyncState(entity.getSyncState());
        folder.setMailboxUpn(entity.getMailboxUpn());
        folder.setAssociations(entity.getAssociations());
        return folder;
    }

    public Map<Long, Long> getFoldersSubFoldersNumber(Collection<Long> folderIds) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.PARENT_FOLDER_ID, SolrOperator.IN, folderIds))
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"))
                .addFacetField(CatFoldersFieldType.PARENT_FOLDER_ID)
                .setFacetMinCount(1)
                .setFacetLimit(folderIds.size() + 1)
                .setRows(0);

        try {
            Map<Long, Long> result = new HashMap<>();
            QueryResponse queryResponse = client.query(query.build(), SolrRequest.METHOD.POST);
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    String folderId = c.getName();
                    long count = c.getCount();
                    result.put(Long.parseLong(folderId), count);
                }
            }
            for (Long folder : folderIds) {
                if (!result.containsKey(folder)) {
                    result.put(folder, 0L);
                }
            }
            return result;
        } catch (Exception e) {
            logger.error("Failed to count sub folders by folder - solr exception (" + query + ")", e);
            throw new RuntimeException("IO Failure to count sub folders by folder - solr exception", e);
        }
    }

    @Override
    public SolrOpResult save(SolrFolderEntity entity){
        SolrOpResult result = SolrOpResult.create();
        try {
            Long folderId = entity.getId();
            if (folderId == null) {
                folderId = getNextAvailableId();
                entity.setId(folderId);
            }
            client.addBean(entity);
            result.setId(folderId).setSuccessful(true);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
        return result;
    }

    public void saveEntities(List<SolrFolderEntity> entities) {
        try {
            client.addBeans(entities);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entities", e);
        }
    }

    public void updateFoldersParentsInformationNoParent(long rootFolderId) {
        Map<SolrCoreSchema, String> filters = new HashMap<>();
        filters.put(CatFoldersFieldType.PARENTS_INFO, "");
        filters.put(CatFoldersFieldType.PARENT_FOLDER_ID, "");
        filters.put(CatFoldersFieldType.DELETED, "false");
        filters.put(CatFoldersFieldType.ROOT_FOLDER_ID, String.valueOf(rootFolderId));

        try {
            SolrQuery query = createQuery(filters, CatFolderSolrActions.UPDATE_PARENT_INFO_NO_PARENT.getName());
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            logger.debug("queryResponse {}", queryResponse);
            softCommitNoWaitFlush();
        } catch (Exception e) {
            logger.error("Failed to update folders in Solr", e);
            throw new RuntimeException("Failed to update folders in Solr. root folder id:" + rootFolderId, e);
        }
    }

    private SolrQuery createQuery(Map<SolrCoreSchema, String> filters, String operation) {
        Query query = Query.create();

        if (operation != null) {
            query.setRequestHandler(UPDATE_FIELDS_REQUEST_HANDLER);
        }

        if (filters != null && !filters.isEmpty()) {
            for (Map.Entry<SolrCoreSchema, String> filter : filters.entrySet()) {
                if (!filter.getValue().isEmpty()) {
                    query.addFilterWithCriteria(filter.getKey(), SolrOperator.EQ, filter.getValue());
                } else {
                    query.addFilterWithCriteria(filter.getKey(), SolrOperator.MISSING, filter.getValue());
                }
            }
        }
        if (operation != null) {
            query.addParam(SolrRequestParams.OPERATION, operation);
        }

        logger.trace("{} object {} in Solr CatFiles by {} using {} identifier: {}", operation, filters);
        return query.build();
    }

    public void createFoldersFromDocuments(Collection<SolrInputDocument> documents) {
        if (documents == null || documents.size() == 0) {
            return;
        }
        try {
            logger.debug("Send {} documents to Solr", documents.size());
            UpdateResponse response = client.add(documents);
            if (response == null) {
                throw new RuntimeException("Failed to create folders in Solr.");
            }
            logger.debug("Got response from solr: {}", response);
        } catch (Exception e) {
            logger.error("Failed to create folders in Solr", e);
            String listStr = documents.stream().map(d -> d.getField(CatFoldersFieldType.ID.getSolrName()).toString())
                    .collect(Collectors.joining(","));
            logger.error("Failed folder ids: {}", listStr);
            throw new RuntimeException("Failed to create folders in Solr.", e);
        }

    }
}
