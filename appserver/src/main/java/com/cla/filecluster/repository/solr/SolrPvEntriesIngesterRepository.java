package com.cla.filecluster.repository.solr;

import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.MaturityLevel;
import com.cla.filecluster.domain.dto.PVsDTO;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.repository.PvEntriesIngesterRepository;
import com.cla.filecluster.service.crawler.analyze.MaturityLevelService;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Repository
@Profile("solr")
public class SolrPvEntriesIngesterRepository extends SolrRepository implements PvEntriesIngesterRepository {

    private static final String PVSolrGlobalCountFieldsSuffix = "_cnt";
    private static final String PVSolrHistogramSizeFieldsSuffix = "_size";

    @Value("${skipIngestSavePvsByPVName:}")
    private String skipIngestSavePvsByName;

    @Value("${pvsSolr.idSeparator:!}")
    private String pvsSolrIdSeparator;

    @Autowired
    private MaturityLevelService maturityLevelService;

    @Value("${pvs.solr.core.write:PVS_Write}")
    private String pvsSolrCore;

    private Set<String> skippedSavedPvsNames;

    @Autowired
    private IngestBatchResultsProcessor ingestBatchResultsProcessor;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Override
    @PostConstruct
    protected void init() {
        super.init();
        fillSkippedPvNames();
    }

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected String coreName() {
        return pvsSolrCore;
    }

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    private void fillSkippedPvNames() {
        if (skipIngestSavePvsByName.isEmpty()) {
            return;
        }
        skippedSavedPvsNames = Stream.of(skipIngestSavePvsByName.split(",")).collect(toSet());
        logger.trace("Set skippedSavedPvsNames to {}", skippedSavedPvsNames);
    }

    private boolean skipPv(final String pvName) {
        return skippedSavedPvsNames != null && skippedSavedPvsNames.contains(pvName);
    }

    @Override
    public void saveHistograms(Long taskId, final String fileName, final Long docContentId, final String part, final ClaSuperCollection<Long> superCollection, Integer fileMaturityLevel) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace("Processing pvEntries for part={} docId={} {}", part, docContentId.toString(), fileName);
            }
            KpiRecording kpiRecording = performanceKpiRecorder.startRecording("saveHistograms", "getSaveHistogramsDoc", 98);
            final SolrInputDocument doc = getSaveHistogramsDoc(fileName, docContentId, part, superCollection, fileMaturityLevel);
           kpiRecording.end();
            if (doc != null) {
              kpiRecording = performanceKpiRecorder.startRecording("saveHistograms", "ingestBatchResultsProcessor.collectHistogram", 104);
                ingestBatchResultsProcessor.collectHistogram(taskId, doc);
             kpiRecording.end();
                //client.add(doc, 1000);
                logger.trace("Finished doc: part {} for file {} with id {}.", part, fileName, docContentId);
            }
        } catch (final Exception e) {
            logger.error("Error: !!! Failed saving histograms for doc: part {} for file {} with id {}. Exception: {}", part, fileName, docContentId, e);
        }
    }

    private SolrInputDocument createSolrInputDoc(final String fileName, final Long docLid, final String part) {
        final SolrInputDocument doc = new SolrInputDocument();
//        doc.setField("id", part + pvsSolrIdSeparator + docLid);
        doc.setField("id", docLid + pvsSolrIdSeparator + part);
        doc.setField("fileName", fileName);
        doc.setField("fileId", docLid);            // Maintain fileId to support SolrCloud routing - have all parts in same shard.
//		doc.setField("part", part);
        return doc;
    }

    /**
     * for now cng maturity to -99
     * @param contentIds - to mark
     */
    public void markDeletedByContentId(List<Long> contentIds) {
        StringJoiner query = new StringJoiner(" ", "fileId:(", ")");    // query: id:(id1 id2 id3 ...)
        contentIds.forEach(cid -> query.add(String.valueOf(cid)));
        updateFieldByQueryMultiFields(
                new String[]{ClaGroupingParams.ML_field, ClaGroupingParams.ML_FILE_field},
                new String[]{String.valueOf(MaturityLevel.Deleted.toInt()), String.valueOf(MaturityLevel.Deleted.toInt())},
                SolrFieldOp.SET.getOpName(),
                query.toString(), null,
                SolrCommitType.NONE
        );
    }

    public SolrInputDocument getSaveHistogramsDoc(final String fileName, final Long docLid, final String part, final ClaSuperCollection<Long> superCollection, Integer maturityFileLevel) {
        try {
            if (logger.isTraceEnabled()) {
                logger.trace("Processing pvEntries for part={} docId={} {}", part, docLid.toString(), fileName);
            }
            final SolrInputDocument doc = createSolrInputDoc(fileName, docLid, part);
            doc.setField(ClaGroupingParams.ML_field, maturityLevelService.getPartMaturityLevel(superCollection));
            doc.setField(ClaGroupingParams.ML_FILE_field,maturityFileLevel == null ? 0 : maturityFileLevel);
            superCollection.getCollections().entrySet().stream()
                    .filter(e -> !skipPv(e.getKey()))
                    .forEach(e -> {
                        doc.setField(e.getKey(), e.getValue().getJoinedKeys());
                        doc.setField(e.getKey() + PVSolrGlobalCountFieldsSuffix, e.getValue().getGlobalCount());
                        doc.setField(e.getKey() + PVSolrHistogramSizeFieldsSuffix, e.getValue().getItemsCount());
                    });
            return doc;
        } catch (final Exception e) {
            logger.error("Error: !!! Failed creating histograms SolrDoc for doc: part {} for file {} with id {}. Exception: {}", part, fileName, docLid, e);
            return null;
        }
    }

    @Override
    public void optimizePvIndex() {
        try {
            logger.info("PV index optimization start...");
            final UpdateResponse resp = client.optimize(true, true, 1);
            logger.info("PV index optimization done. status={} qtime={} seconds", (resp.getStatus() == 0) ? "ok" : resp.getStatus(), ((int) (resp.getQTime() / 100f)) / 10f);
        } catch (final Exception e) {
            logger.error("Error during PV index optimization. {}", e.getMessage(), e);
        }
    }

    public void saveDocs(List<SolrInputDocument> docs) {
        try {
            client.add(docs, 1000);
        } catch (Exception e) {
            logger.error("Error during PV docs add. {}", e.getMessage(), e);
        }
    }

    public void save(final PVsDTO t) {
        try {
            getSolrClient().addBean(t, 1000);
        } catch (Exception e) {
            throw new RuntimeException("Could not save bean", e);
        }
    }
}

