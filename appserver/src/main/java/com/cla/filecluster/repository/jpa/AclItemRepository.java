/*package com.cla.filecluster.repository.jpa;

import java.util.List;

import com.cla.connector.domain.dto.acl.AclType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cla.common.domain.dto.file.AclItemDto;
import com.cla.filecluster.domain.entity.pv.AclItem;

public interface AclItemRepository extends JpaRepository<AclItem, String> {

	@Query("SELECT NEW com.cla.common.domain.dto.file.AclItemDto(a.id, a.claFileId, a.entity, a.type, a.level) "
			+ " FROM AclItem a "
			+ " WHERE a.claFileId=:fileId")
	Page<AclItemDto> getDtoByFileId(@Param("fileId") Long fileId, Pageable pageable); 

	@Query("SELECT NEW com.cla.common.domain.dto.file.AclItemDto(a.id, a.claFileId, a.entity, a.type, a.level) "
			+ " FROM AclItem a "
			+ " WHERE a.claFileId=:fileId")
	List<AclItemDto> getDtoByFileId(@Param("fileId") Long fileId); 

	@Query("SELECT NEW com.cla.common.domain.dto.file.AclItemDto(a.id, a.claFileId, a.entity, a.type, a.level) "
			+ " FROM AclItem a "
			+ " WHERE a.claFileId=:fileId AND a.type=:type")
	Page<AclItemDto> getDtoByFileIdAndType(@Param("fileId") Long fileId, @Param("type") AclType type, Pageable pageable);

	@Query("SELECT NEW com.cla.common.domain.dto.file.AclItemDto(a.id, a.claFileId, a.entity, a.type, a.level) "
			+ " FROM AclItem a "
			+ " WHERE a.claFileId=:fileId AND a.type=:type")
	List<AclItemDto> getDtoByFileIdAndType(@Param("fileId") Long fileId, @Param("type") AclType type);

	@Query("SELECT a FROM AclItem a WHERE a.claFileId IN :fileIds ")
	List<AclItem> findAllByFileIds(@Param("fileIds") List<Long> claFileIds);

	@Modifying
	@Query("DELETE FROM AclItem a WHERE a.claFileId = :fileId ")
	int removeAclItemsForFile(@Param("fileId") long id);

	@Query("SELECT a FROM AclItem a WHERE a.claFileId = :fileId ")
	List<AclItem> getByFileId(@Param("fileId") long id);

	@Modifying
	@Query("DELETE FROM AclItem a WHERE a.id IN :aclItemIds ")
	int deleteByIds(@Param("aclItemIds") List<Long> idsToRemove);
}*/
