package com.cla.filecluster.repository.jpa.entitylist;

import com.cla.filecluster.domain.entity.bizlist.ConsumerBizListItem;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Created by uri on 20/03/2016.
 */
public interface ConsumerBizListItemRepository extends JpaRepository<ConsumerBizListItem,String>, JpaSpecificationExecutor<ConsumerBizListItem> {
}
