package com.cla.filecluster.repository;

import com.cla.filecluster.config.JpaMetadataIntegrator;
import com.google.common.collect.Maps;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.enhanced.TableGenerator;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

/**
 * Generates sequence IDs to be used as item IDs in SOLR cores
 * Uses Hibernate TableGenerator, which allocates chunks of sequences in memory
 * in order to minimize number of DB accesses.
 *
 * @see TableGenerator
 */
@Service
public class SequenceIdGenerator {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private JpaMetadataIntegrator metadataIntegrator;

    @Value("${sequence-id-generator.allocation-size:500}")
    private int sequenceAllocationSize;

    private SessionFactory sessionFactory;

    private ServiceRegistryImplementor serviceRegistry;

    private Map<SequenceType, TableGenerator> generators;

    public enum SequenceType {
        CAT_FILE,
        CONTENT_METADATA,
        DOC_FOLDER,
        ALERTS,
        BIZ_LIST_ITEM_EXTRACT,
        JM_TASKS("jm_tasks"),
        INGESTION_ERROR,
        MISSED_FILE_MATCH;

        private final String seqName;

        SequenceType(String seqName) {
            this.seqName = seqName;
        }

        SequenceType() {
            this.seqName = name();
        }

        public String getSeqName() {
            return seqName;
        }
    }

    @PostConstruct
    public void init() {
        sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        generators = Maps.newHashMap();
        serviceRegistry = ((SessionFactoryImplementor) sessionFactory).getServiceRegistry();
        Arrays.stream(SequenceType.values()).forEach(seq -> generators.put(seq, createSequenceGenerator(seq)));
    }

    @Transactional
    public long nextSequence(SequenceType type) {
        return (Long) generators.get(type)
                .generate((SessionImplementor) this.sessionFactory.getCurrentSession(), null);
    }

    private TableGenerator createSequenceGenerator(SequenceType seq) {
        Properties parameters = new Properties();
        parameters.put(TableGenerator.SEGMENT_VALUE_PARAM, seq.getSeqName());
        parameters.put(TableGenerator.INCREMENT_PARAM, sequenceAllocationSize);
        TableGenerator tableGenerator = new TableGenerator();
        tableGenerator.configure(new LongType(), parameters, serviceRegistry);
        tableGenerator.registerExportables(metadataIntegrator.getDatabase());
        return tableGenerator;
    }
}
