package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.LdapGroupMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by Itai Marko.
 */
public interface LdapGroupMappingRepository extends JpaRepository<LdapGroupMapping, Long> {

    @Query(nativeQuery = true, value = "select * from ldap_group_mapping ldgm where BINARY ldgm.group_name = :groupName")
    LdapGroupMapping findByGroupName(@Param("groupName") String groupName);

    @Query( "SELECT ldgm FROM LdapGroupMapping ldgm " +
            " LEFT JOIN FETCH ldgm.daRoles br" +
            " WHERE ldgm.groupName IN (:groupNames)")
    Set<LdapGroupMapping> findByGroupNames(@Param("groupNames") Set<String> groupName);

    @Query( "SELECT ldgm from LdapGroupMapping ldgm " +
            " JOIN ldgm.daRoles br" +
            " WHERE br.id = :daRoleId")
    List<LdapGroupMapping> findByDaRoleId(@Param("daRoleId") Long daRoleId);
}
