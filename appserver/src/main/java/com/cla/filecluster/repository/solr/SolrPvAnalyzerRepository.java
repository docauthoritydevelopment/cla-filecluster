package com.cla.filecluster.repository.solr;

import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.*;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.repository.PvAnalyzerRepository;
import com.cla.filecluster.service.crawler.analyze.MaturityLevelService;
import com.cla.filecluster.service.files.pv.SimilarityCalculator;
import com.cla.filecluster.util.pv.PvAnalysisDataUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.CursorMarkParams;
import org.apache.solr.common.util.NamedList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Repository
@Profile("solr")
public class SolrPvAnalyzerRepository extends SolrRepository implements PvAnalyzerRepository {

    private enum AnalyzeStrategy {SEARCH, MLT}

    @Value("${skipAnalyzedPvsByPVName:}")
    private String skipAnalyzedPvsByName;

    @Value("${pvsSolr.idSeparator:!}")
    private String pvsSolrIdSeparator;

    @Autowired
    private MaturityLevelService maturityLevelService;

    @Value("${solr.pv.analyze.useMaturityLevel:true}")
    private boolean useMaturityLevel;

    @Value("${solr.pv.analyze.forceMaturityLevelSimilarity:false}")
    private boolean forceMaturityLevelSimilarity;

    @Value("${solr.pv.analyzeStrategy:SEARCH}")
    private String analyzeStrategyStr;
    private AnalyzeStrategy analyzeStrategy;

    @Value("${solr.pv.maxSimilarResults:10000}")
    private int maxSimilarResults;

    private Set<Integer> skippedAnalysisPvsNames = null;

    @Value("${solr.pv.minSimRateThreshold:0.01}")
    private float minSimRateThreshold;

    @Value("${solr.pv.maxTokensInQuery:63900}")
    private int maxTokensInQuery;

    @Value("${solr.pv.limitResultsByScoreThreshold:true}")
    private boolean limitResultsByScoreThreshold;

    private final static String limitScoreThresholdFqStringFormat = "{!frange l=%f}query($q)";

    @Value("${solr.pv.analysisBprFq:query({!func v=clabpr($q,'%s@%s',%d)})}")
    private String analysisBprFqStringFormat;

    @Value("${solr.pv.analysisExtraFq:}")
    private String analysisExtraFq;

    @Value("${solr.pv.useBoilerPlateRemoval:false}")
    private boolean useBoilerPlateRemoval;

    @Value("${solr.pv.BprDfAverageFactor:6}")
    private int analysisBprDfAverageFactor;

    @Value("${fileGrouping.word.similarityStyleLowThreshold:0.2}")
    private float wordSimilarityStyleLowThreshold;

    @Value("${pvs.solr.core.read:PVS_Read}")
    private String pvsSolrCore;

    @Value("${pvs.analysis-retry-counter:0}")
    private int analysisRetryCounter;

    @PostConstruct
    protected void init() {
        super.init();
        fillSkippedPvNames();
        logger.info("Dropping all responses with score below {}", minSimRateThreshold);
        analyzeStrategy = AnalyzeStrategy.valueOf(analyzeStrategyStr);
        if (autoDelete && hibernateDdlAuto.contains("create")) {
           /* logger.info("Evict cache when all data is deleted");
            evictCache();*/
        }
    }

    @Override
    protected Class getSchemaType() {
        return null;
    }

    @Override
    protected String coreName() {
        return pvsSolrCore;
    }

    @Override
    protected Class getEntityType() {
        return null;
    }

    @Override
    public SolrOpResult save(SolrEntity entity) {
        return null;
    }

    @Override
    public Collection<SimilarityMapKey> findWordGroupCandidates(ContentMetadataDto contentMetadataDto, PvAnalysisData pvAnalysisData, String customThresholds) {
        return analyzeAllPvsForPart(contentMetadataDto, pvAnalysisData.getDefaultPvCollection(), PvAnalyzerRepository.DOC_HANDLER, customThresholds);
    }

    @Override
    public Collection<Long> findWordGroupCandidates(final ContentMetadataDto contentMetadataDto, String customThresholds) {
        // return content IDs of similar contents
        return analyzeAllPvsForPart(contentMetadataDto, PvAnalyzerRepository.DOC_HANDLER, customThresholds);
    }

    @Override
    public Collection<SimilarityMapKey> analyzeAllPvsForSheet(final ContentMetadataDto contentMetadataDto, final ClaSuperCollection<Long> partPvCollection) {
        return analyzeAllPvsForPart(contentMetadataDto, partPvCollection, PvAnalyzerRepository.SHEET_HANDLER, null);
    }

    @Override
    public Collection<Long> analyzeAllPvsForWorkBook(final ContentMetadataDto contentMetadataDto) {
        long contentId = contentMetadataDto.getContentId();
        final SolrQuery query = createQuery(contentMetadataDto, PvAnalyzerRepository.WORKBOOK_HANDLER, null);
        //Execute the query
        QueryResponse queryResponse;
        try {
            queryResponse = client.query(query, METHOD.POST, analysisRetryCounter);
        } catch (final Exception e) {
            logger.error("Caught {} exception while trying to find potential group candidates for contentId: {}/hint: {} query={}",
                    e.getMessage(), contentId, contentMetadataDto.getHint(), query.toString(), e);
            throw new RuntimeException(e);
        }

        //Parse results

        return parseAnalysisResponse(contentId, queryResponse);
    }


    private Collection<Long> analyzeAllPvsForPart(final ContentMetadataDto contentMetadataDto, final String solrHandler, final String customThresholds) {

        long contentId = contentMetadataDto.getContentId();
        final SolrQuery query = createQuery(contentMetadataDto, solrHandler, customThresholds);

        //Execute the query
        QueryResponse queryResponse;
        try {
            queryResponse = client.query(query, METHOD.POST, analysisRetryCounter);
        } catch (final Exception e) {
            logger.error("Caught {} exception while trying to find potential group candidates for contentId: {}/hint: {} query={}",
                    e.getMessage(), contentId, contentMetadataDto.getHint(), query.toString(), e);
            throw new RuntimeException(e);
        }

        //Parse results
        return parseAnalysisResponse(contentId, queryResponse);
    }

    public boolean evictCache() {
        final SolrQuery query = new SolrQuery();
        query.setRequestHandler(PvAnalyzerRepository.DOC_HANDLER);
        query.add("evict", "true");
        if (client.isSolrCloudClient()) {
            query.add("shards.qt", PvAnalyzerRepository.DOC_HANDLER);
        }
        QueryResponse queryResponse;
        try {
            queryResponse = client.query(query, METHOD.POST);
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    private SolrQuery createQuery(ContentMetadataDto contentMetadataDto, String solrHandler, String customThresholds) {
        long contentId = contentMetadataDto.getContentId();
        final SolrQuery query = buildSimilaritySolrQuery(solrHandler);
        addFilterOnSelf(contentId, query);
        //Go over all PV-Types and build a query clause for each PV type
        query.add(ClaGroupingParams.OD, String.valueOf(contentId));
        query.add(ClaGroupingParams.SPLIT_ON_WHITESPACE, Boolean.TRUE.toString());
        if (customThresholds != null) {
            query.add(ClaGroupingParams.TR, customThresholds);
        }
        query.add(ClaGroupingParams.FT, contentMetadataDto.getType().toString());
        return query;
    }

    private Collection<SimilarityMapKey> analyzeAllPvsForPart(final ContentMetadataDto contentMetadataDto, final ClaSuperCollection<Long> partPvCollection, final String solrHandler, final String customThresholds) {

        final int queryClauseCount = partPvCollection.getCollections().entrySet().stream()
                .filter(e -> !skipPv(PVType.valueOf(e.getKey()).toInt()))
                .mapToInt(e -> e.getValue().getGlobalCount())
                .sum();
        long contentId = contentMetadataDto.getContentId();
        if (queryClauseCount > maxTokensInQuery) {
            logger.warn("Too many clauses expected in query {}>{}. Skipping similarity check from content id:{} {}",
                    queryClauseCount, maxTokensInQuery, contentId, contentMetadataDto.getHint());
            // Throwing an exception will do no good - it will keep the file in state 'ANALYZING'.
            // For now, fake as if file is analyzed with not matches.
            return new ArrayList<>();
        }

        final SolrQuery query = buildSimilaritySolrQuery(solrHandler);
        addFilterOnSelf(contentId, query);
        //Go over all PV-Types and build a query clause for each PV type
        int queryCounter = 0;
        String mlStr = null;
        if (forceMaturityLevelSimilarity || useMaturityLevel) {
            mlStr = String.valueOf(maturityLevelService.getPartMaturityLevel(partPvCollection));
        }
        if (forceMaturityLevelSimilarity) {
            query.add(ClaGroupingParams.FML, mlStr);        // TODO: change default after ML is tested and added to Solr during ingestion
        } else if (useMaturityLevel) {
            query.add(ClaGroupingParams.ML, mlStr);        // TODO: change default after ML is tested and added to Solr during ingestion
        }
        query.add(ClaGroupingParams.OD, String.valueOf(contentId));
        for (final Map.Entry<String, ClaHistogramCollection<Long>> e : partPvCollection.getCollections().entrySet()) {
            if (!createQueryForPv(contentId, query, queryCounter, e)) {
                continue;
            }
            queryCounter++;
        }
        if (customThresholds != null) {
            query.add(ClaGroupingParams.TR, customThresholds);
        }
        //Execute the query
        QueryResponse queryResponse;
        try {
            queryResponse = client.query(query, METHOD.POST, analysisRetryCounter);
        } catch (final Exception e) {
            logger.error("Caught {} exception while trying to find potential group candidates for contentId: {}/hint: {} query={}",
                    e.getMessage(), contentId, contentMetadataDto.getHint(), query.toString(), e);
            throw new RuntimeException(e);
        }
        //Parse results

        return parseFindGroupResponse(queryResponse);
    }

    private boolean createQueryForPv(long contentId, SolrQuery query, int queryCounter, Map.Entry<String, ClaHistogramCollection<Long>> e) {
        final PropertyVectorKey propertyVectorKey = new PropertyVectorKey(contentId, null, PVType.valueOf(e.getKey()).toInt());
        final PVType pvt = PVType.valueOf(propertyVectorKey.getPvType());
        final ClaHistogramCollection<Long> claHistogramCollection = e.getValue();
        if (skipPv(propertyVectorKey.getPvType())) {
            return false;
        }
        if (claHistogramCollection.getItemsCount() == 0) {
            return false;
        }
        final String queryString = PvAnalysisDataUtils.buildQueryClauseForPv(propertyVectorKey, claHistogramCollection);
        query.add(ClaGroupingParams.GF + queryCounter, queryString);
        query.add(ClaGroupingParams.OGC + queryCounter, String.valueOf(claHistogramCollection.getGlobalCount()));
        query.add(ClaGroupingParams.OIC + queryCounter, String.valueOf(claHistogramCollection.getItemsCount()));
        query.add(ClaGroupingParams.PT + queryCounter, pvt.name());
        return true;
    }

    private void addFilterOnSelf(long contentId, final SolrQuery query) {
        query.addFilterQuery("!fileId:" + contentId);
    }

    private Collection<Long> parseAnalysisResponse(long contentIdAnalyzed, QueryResponse queryResponse) {
        if (queryResponse == null) {
            logger.error("Got null response from findGroup query for content {}", contentIdAnalyzed);
//			throw new RuntimeException("Got null response from findGroup query");
            return Lists.newArrayList();
        }
        NamedList<Object> response = queryResponse.getResponse();
        final NamedList<Long> contentIds = (NamedList) response.get(SimilarityCalculator.CONTENT_IDS);

        if (contentIds == null) {
            logger.error("Got empty response from findGroup query: \n{} for content {}", queryResponse.toString(), contentIdAnalyzed);
//			throw new RuntimeException("Got empty response from findGroup query: \n"+ queryResponse.toString());
            return Lists.newArrayList();
        }
        logger.debug("Got {} documents from findGroup query for content {} results {}", contentIds.size(), contentIdAnalyzed, contentIds);
        List<Long> result = Lists.newArrayList();
        for (Map.Entry<String, Long> contentId : contentIds) {
            result.add(contentId.getValue());
        }
        return result;
    }

    private Collection<SimilarityMapKey> parseFindGroupResponse(final QueryResponse queryResponse) {
        if (queryResponse == null) {
            logger.error("Got null response from findGroup query");
//            throw new RuntimeException("Got null response from findGroup query");
            return Lists.newArrayList();
        }
        final SolrDocumentList resultDocs = queryResponse.getResults();
        if (resultDocs == null) {
            logger.error("Got empty response from findGroup query: \n{}", queryResponse.toString());
//            throw new RuntimeException("Got empty response from findGroup query: \n"+ queryResponse.toString());
            return Lists.newArrayList();
        }
        logger.debug("Got {} documents from findGroup query", resultDocs.size());
        return resultDocs.stream().map(this::createSimilarityMapKey).collect(Collectors.toSet());
    }

    private SolrQuery buildSimilaritySolrQuery(final String solrHandler) {
        final SolrQuery query = new SolrQuery();
        query.setRequestHandler(solrHandler);
        if (analysisExtraFq != null && !analysisExtraFq.isEmpty()) {
            query.addFilterQuery(analysisExtraFq);
        }
        if (client.isSolrCloudClient()) {
            query.add("shards.qt", solrHandler);
        }

        return query;
    }

    @Override
    public SimilarityMap analyzePvForFile(final ContentMetadataDto claFile, final String pvName, final ClaHistogramCollection<Long> claHistogramCollection) {
        if (skipPv(PVType.valueOf(pvName).toInt())) {
//			logger.debug("Skipping PVType {}", pvType.name());
            return new SimilarityMap();
        }
        return analyzePvForPart(claFile, null, pvName, claHistogramCollection);
    }

    @Override
    public SimilarityMap analyzePvForPart(final ContentMetadataDto contentMetadataDto, final String part, final String pvName, final ClaHistogramCollection<Long> claHistogramCollection) {
        final PropertyVector pv = new PropertyVector(contentMetadataDto.getContentId(), part, PVType.valueOf(pvName).toInt(), contentMetadataDto.getHint(), claHistogramCollection);
        if (claHistogramCollection.getCollection().isEmpty() || skipPv(pv.getPk().getPvType())) {
            return new SimilarityMap();
        }
        if (analyzeStrategy == AnalyzeStrategy.SEARCH) {
            return searchByKeys(pv);
        }
        return moreLikeThis(pv);
    }

    private void fillSkippedPvNames() {
        if (skipAnalyzedPvsByName.isEmpty()) {
            return;
        }
        skippedAnalysisPvsNames = Stream.of(skipAnalyzedPvsByName.split(",")).map(n -> PVType.valueOf(n).toInt()).collect(toSet());

        String collect = skippedAnalysisPvsNames.stream().map(i -> PVType.valueOf(i).name()).collect(Collectors.joining(","));
        logger.info("Set skippedAnalysisPvsNames to {}={}", skippedAnalysisPvsNames,
                collect);
    }

    private boolean skipPv(final int pvType) {
        return skippedAnalysisPvsNames != null && skippedAnalysisPvsNames.contains(pvType);
    }

    @Deprecated
    private SimilarityMap moreLikeThis(final PropertyVector pv) {
        try {
            final PVType pvt = PVType.valueOf(pv.getPk().getPvType());
            final SolrQuery query = new SolrQuery();
            query.setRequestHandler("/mlt");
            final String part = (pv.getPk().getPart() == null) ? "_" : pv.getPk().getPart();
            query.setQuery("id:" + pv.getPk().getDocId() + pvsSolrIdSeparator + part);
            query.setParam("mlt.fl", pvt.name());
            query.setRows(maxSimilarResults);

            final QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST, analysisRetryCounter);
            final SolrDocumentList resultDocs = queryResponse.getResults();
            final SimilarityMap analyzedPvSimilarities = new SimilarityMap();
            resultDocs.forEach(doc -> addSimilarity(pv, analyzedPvSimilarities, doc));
            if (logger.isTraceEnabled()) {
                logger.trace("Solr returned {} docs {} dropped {} passed. {} with {} items, qtime={}", resultDocs.size(), analyzedPvSimilarities.getCounter(), resultDocs.size() - analyzedPvSimilarities.getCounter(),
                        pvt.name(), pv.getClaHistogramCollection().getCollection().size(), queryResponse.getQTime());
            }
            analyzedPvSimilarities.resetCounter();

            return analyzedPvSimilarities;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private SimilarityMap searchByKeys(final PropertyVector pv) {
        if (pv.getClaHistogramCollection().getItemsCount() < maxTokensInQuery) {
            // unit testing code - remove extra lines ===>
//			final SimilarityMap sim1 = searchByKeysSingleQuery(pv, 1f);
//			final SimilarityMap sim2 = searchByKeysLargeHistogram(pv, 100);
//			final SimilarityMap sim3 = searchByKeysLargeHistogram(pv, 250);
//			final SimilarityMap sim4 = searchByKeysLargeHistogram(pv, 2500);
//			logger.trace("SIM1 got {} items: {}",sim1.size(), sim1.toString());
//			logger.trace("SIM2 got {} items: {}",sim2.size(), sim2.toString());
//			logger.trace("SIM3 got {} items: {}",sim3.size(), sim3.toString());
//			logger.trace("SIM4 got {} items: {}",sim4.size(), sim4.toString());
            // End of unit test code <===

            //			final SimilarityMap s2 = searchByKeysSingleQuery(pv, 1f, !useBoilerPlateRemoval);	// @@@ For debug / verification
            return searchByKeysSingleQuery(pv, 1f, useBoilerPlateRemoval);
        } else {
            return searchByKeysLargeHistogram(pv, maxTokensInQuery);
        }
    }

    private SimilarityMap searchByKeysSingleQuery(final PropertyVector pv, final float globalBoost, final boolean useBPR) {
        QueryResponse queryResponse = null;
        final SimilarityMap analyzedPvSimilarities = new SimilarityMap();
        final PVType pvt = PVType.valueOf(pv.getPk().getPvType());
        String part = pv.getPk().getPart();
        part = (part == null) ? PropertyVector.DEF_SYS_PART :
                part.replaceAll("['\\,]", "").replaceAll("\\s", "_");    // make sure part is properly parsed
        String q;
        try {
            q = pv.getClaHistogramCollection().getJoinedKeysWithBoost(pvt.name() + ":");
        } catch (final Exception e) {
            logger.error("Unexpected error while generating query string for {}/{} {}. {}", pv.getPk().getDocId(), pv.getPk().getPart(), pvt.name(), e, e);
            return analyzedPvSimilarities;
        }

        int page = 0;
        try {
            //Use Deep-Paging mechanism for queries.
            String cursorMark = CursorMarkParams.CURSOR_MARK_START;
            final SolrQuery query = buildSolrQuery(pv, globalBoost, useBPR, part, q);
            boolean done = false;
            while (!done) {
                query.set(CursorMarkParams.CURSOR_MARK_PARAM, cursorMark);
                queryResponse = client.query(query, METHOD.POST, analysisRetryCounter);
                final String nextCursorMark = queryResponse.getNextCursorMark();
                processQueryResults(queryResponse, pv, analyzedPvSimilarities, globalBoost, pvt, useBPR, page);
                if (cursorMark.equals(nextCursorMark) || queryResponse.getResults().size() < maxSimilarResults) {
                    done = true;
                }
                cursorMark = nextCursorMark;
                page++;
            }

        } catch (final Exception e) {
            logger.error("Unexpected error while searching keys for {}/{} query={} response={}. {}",
                    pv.getPk().getDocId(), pv.getPk().getPart(), q, (queryResponse == null) ? "null" : queryResponse.toString(), e, e);
        }
        return analyzedPvSimilarities;
    }

    private void processQueryResults(final QueryResponse queryResponse, final PropertyVector pv, final SimilarityMap analyzedPvSimilarities, final float globalBoost, final PVType pvt, final boolean useBPR, final int page) {
        final SolrDocumentList resultDocs = queryResponse.getResults();
        resultDocs.forEach(doc -> addSimilarity(pv, analyzedPvSimilarities, doc, globalBoost));
        if (logger.isTraceEnabled()) {
            logger.trace("Solr returned {} docs {} dropped {} passed. {} with {} items, qtime={}{}{}", resultDocs.size(), analyzedPvSimilarities.getCounter(), resultDocs.size() - analyzedPvSimilarities.getCounter(),
                    pvt.name(), pv.getClaHistogramCollection().getCollection().size(), queryResponse.getQTime(), useBPR ? " BPR" : "", (page == 0) ? "" : (" page=" + page));
        }
        if ((page > 0 || resultDocs.size() >= maxSimilarResults) && logger.isDebugEnabled()) {
            logger.debug("Solr multi-page response: page #{} got {} docs {} dropped {} passed. {} with {} items, qtime={}{}",
                    page, resultDocs.size(), analyzedPvSimilarities.getCounter(), resultDocs.size() - analyzedPvSimilarities.getCounter(),
                    pvt.name(), pv.getClaHistogramCollection().getCollection().size(), queryResponse.getQTime(), useBPR ? " BPR" : "");
        }
        analyzedPvSimilarities.resetCounter();
    }

    private SolrQuery buildSolrQuery(final PropertyVector pv, final float globalBoost, final boolean useBPR, final String part, final String q) {
        final SolrQuery query = new SolrQuery();
        query.setRequestHandler("/query");
        query.setQuery(q);
        query.setSort(SolrQuery.SortClause.asc("id"));
        if (useBPR && analysisBprFqStringFormat != null && !analysisBprFqStringFormat.isEmpty()) {
            query.addFilterQuery(String.format(analysisBprFqStringFormat, part, pv.getPk().getDocId(), analysisBprDfAverageFactor));    // force bprFactor of 6 (formatted here as int. parsed as float)
        }
        if (analysisExtraFq != null && !analysisExtraFq.isEmpty()) {
            query.addFilterQuery(analysisExtraFq);
        }
        if (limitResultsByScoreThreshold) {
            query.addFilterQuery(String.format(limitScoreThresholdFqStringFormat, minSimRateThreshold * globalBoost));
        }
        query.setRows(maxSimilarResults);
        return query;
    }

    private SimilarityMap searchByKeysLargeHistogram(final PropertyVector pv, final int maxListSize) {
        // TODO: sub-list factor&merge results seem close enough to the desired result but not fully-accurate. Test and fix when there is some time ...
        final float globalFactor = (float) Math.sqrt(pv.getClaHistogramCollection().getGlobalCount()); /* pv.getClaHistogramCollection().getItemsCount()*/
        final List<ClaHistogramCollection<Long>> subPvLists = pv.getClaHistogramCollection().split(maxListSize);
        logger.debug("Splitting {} histogram into {} sub-lists...", PVType.valueOf(pv.getPk().getPvType()).name(), subPvLists.size());
        final SimilarityMap result = new SimilarityMap();
        for (final ClaHistogramCollection<Long> sl : subPvLists) {
            final float subListBoost = (float) Math.sqrt(sl.getGlobalCount());
            final SimilarityMap sim =
                    searchByKeysSingleQuery(new PropertyVector(pv.getPk(), null, sl),
                            subListBoost / globalFactor, useBoilerPlateRemoval);
            result.merge(sim);
            if (logger.isTraceEnabled()) {
                logger.trace("Merged sim with {} items with factor of {}/{}. Got {} items after merge.", sim.size(), subListBoost, globalFactor, result.size());
            }
        }
        return result.filterMinRate(minSimRateThreshold);
    }

    @Deprecated
    private void addSimilarity(final PropertyVector pv, final SimilarityMap analyzedPvSimilarities, final SolrDocument doc) {
        addSimilarity(pv, analyzedPvSimilarities, doc, 1f);
    }

    private SimilarityMapKey createSimilarityMapKey(final SolrDocument doc) {
        final long fileId = (long) doc.get("fileId");
        final String part = (String) doc.get("part");
        if (part == null) {
            final Pair<String, Long> stringLongPair = parseDocId(doc);
            return new SimilarityMapKey(stringLongPair.getRight(), stringLongPair.getLeft());
        }
        return new SimilarityMapKey(fileId, part);
    }

    private void addSimilarity(final PropertyVector pv, final SimilarityMap analyzedPvSimilarities, final SolrDocument doc, final float globalBoost) {
//		final long fileId = (long)doc.get("fileId");
//		final String part = (String)doc.get("part");
        final String id = (String) doc.get("id");
        long fileId;
        String part;
        try {
            final Pair<String, Long> stringLongPair = parseDocId(doc);
            part = stringLongPair.getLeft();
            fileId = stringLongPair.getRight();
        } catch (final Exception e) {
            logger.error("Error while extracting parts from id {}. {}", id, e);
            return;
        }

        // Consider adding 'AND id != myId' to the query
        if (!sameDocPart(pv.getPk(), fileId, part)) {
            final float simRate = (float) doc.get("score");
            if (simRate < minSimRateThreshold * globalBoost) {
                analyzedPvSimilarities.incrementCounter();
            } else {
                analyzedPvSimilarities.setRates(new SimilarityMapKey(fileId, part), new SimilarityRates(simRate * globalBoost, 0f));
            }
        }
    }

    private Pair<String, Long> parseDocId(final SolrDocument doc) {
        final String id = (String) doc.get("id");
        long fileId;
        String part;
        final String[] sp = id.split(pvsSolrIdSeparator, 2);
        part = sp[1];
        fileId = Long.parseLong(sp[0]);
        return Pair.of(part, fileId);
    }


    private boolean sameDocPart(final PropertyVectorKey pk, final long fileId, final String part) {
        if (pk.getDocId() != fileId) {
            return false;
        }
        final String otherPart = pk.getPart();
        if ((otherPart == null || otherPart.isEmpty() || otherPart.equals(PropertyVector.DEF_SYS_PART)) &&
                (part == null || part.isEmpty() || part.equals(PropertyVector.DEF_SYS_PART))) {
            return true;
        }
        return part.equals(otherPart);
    }

}
