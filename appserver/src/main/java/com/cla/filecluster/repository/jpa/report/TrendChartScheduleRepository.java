package com.cla.filecluster.repository.jpa.report;

import com.cla.filecluster.domain.entity.report.TrendChartSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uri on 27/07/2016.
 */
public interface TrendChartScheduleRepository extends JpaRepository<TrendChartSchedule,Long> {
}
