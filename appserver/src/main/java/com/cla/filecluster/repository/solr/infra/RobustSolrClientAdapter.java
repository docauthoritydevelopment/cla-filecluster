package com.cla.filecluster.repository.solr.infra;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
 * Support custom RetryTemplate calls to improve robustness of SOLR access
 */
public class RobustSolrClientAdapter implements SolrClientAdapter {
    private static Logger logger = LoggerFactory.getLogger(RobustSolrClientAdapter.class);

    private final SolrClientAdapter solrClientDelegate;
    private final String collectionName;
    private int operationsRetryCount = 3;
    private long operationsSleepSec = 3;
    private boolean applyRetryStrategy = true;

    private RetryTemplate scRetryTemplate;

    public static RobustSolrClientAdapter create(SolrClientAdapterSupport solrClient, String collectionName) {
        return new RobustSolrClientAdapter(solrClient, collectionName);
    }

    private RobustSolrClientAdapter(SolrClientAdapterSupport solrClientDelegate, String collectionName) {
        this.solrClientDelegate = solrClientDelegate;
        this.collectionName = collectionName;
        this.scRetryTemplate = createRetryTemplate();
    }

    @Override
    public SolrClient getUnderyingClient() {
        return solrClientDelegate.getUnderyingClient();
    }

    private RetryTemplate createRetryTemplate() {
        final RetryTemplate rt = new RetryTemplate();
        final SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
        retryPolicy.setMaxAttempts(operationsRetryCount);
        final ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
        backOffPolicy.setInitialInterval(TimeUnit.SECONDS.toMillis(operationsSleepSec));
        backOffPolicy.setMaxInterval(600000L);
        rt.setRetryPolicy(retryPolicy);
        rt.setBackOffPolicy(backOffPolicy);
        return rt;
    }

    public boolean isSolrCloudClient() {
        return solrClientDelegate.isSolrCloudClient();
    }

    private boolean isDoNotRetryException(final Throwable lastThrowable) {
        if (lastThrowable == null) {
            return false;
        } else if (lastThrowable instanceof HttpSolrClient.RemoteSolrException) {
            // 	SolrException - is it too general?
            return true;
        } else if (lastThrowable instanceof NullPointerException) {
            // 	SolrException - is it too general?
            return true;
        } else if (lastThrowable.getMessage() != null && lastThrowable.getMessage().contains("org.apache.solr.search.SyntaxError")) {
            return true;
        }
        return false;
    }

    private <T> T retryWithTemplate(RetryCallback<T> callback, String callName) throws Exception {
        T response;
        if (applyRetryStrategy) {
            response = scRetryTemplate.execute(
                    retryContext -> doRetry(retryContext, callback, callName),
                    rc -> {
                        logger.error("Failed all retries for {}. Exception: {}", callName, rc.getLastThrowable());
                        throw new RuntimeException("Failed all retries for "+callName);
                    });
        } else {
            response = callback.doSolrCall();
        }
        return response;
    }

    private <T> T doRetry(RetryContext retryContext, RetryCallback<T> callback, String callName) throws Exception {
        if (isDoNotRetryException(retryContext.getLastThrowable())) {
            if (logger.isTraceEnabled()) {
                logger.trace("setExhaustedOnly  #{} for {} err={}", retryContext.getRetryCount(), callName,
                        retryContext.getLastThrowable().getClass().toString());
            }
            retryContext.setExhaustedOnly();
        } else {
            if (retryContext.getRetryCount() > 0) {
                Throwable lastThrowable = retryContext.getLastThrowable();
                if (lastThrowable != null && lastThrowable.getCause() != null) {
                    logger.warn("Retry #{} for {} err={} , {}", retryContext.getRetryCount(), callName,
                            lastThrowable, lastThrowable.getCause());
                } else {
                    logger.warn("Retry #{} for {} err={}", retryContext.getRetryCount(), callName, lastThrowable);
                }
            }
        }
        return callback.doSolrCall();
    }

    private <T> T retryWithTemplate(RetryCallback<T> callback, String callName, int retryCount) throws Exception {
        T response;
        if (applyRetryStrategy) {
            response = scRetryTemplate.execute(
                    retryContext -> doRetry(retryContext, callback, callName, retryCount),
                    rc -> {
                        logger.error("Failed all retries for {}. Exception: {}", callName, rc.getLastThrowable());
                        throw new RuntimeException("Failed all retries for "+callName);
                    });
        } else {
            response = callback.doSolrCall();
        }
        return response;
    }

    private <T> T doRetry(RetryContext retryContext, RetryCallback<T> callback, String callName, int retryCount) throws Exception {
        if (isDoNotRetryException(retryContext.getLastThrowable())) {
            if (logger.isTraceEnabled()) {
                logger.trace("setExhaustedOnly  #{} for {} err={}", retryCount, callName,
                        retryContext.getLastThrowable().getClass().toString());
            }
            retryContext.setExhaustedOnly();
        } else {
            if (retryCount > 0) {
                Throwable lastThrowable = retryContext.getLastThrowable();
                if (lastThrowable != null && lastThrowable.getCause() != null) {
                    logger.warn("Retry #{} for {} err={} , {}", retryCount, callName,
                            lastThrowable, lastThrowable.getCause());
                } else {
                    logger.warn("Retry #{} for {} err={}", retryCount, callName, lastThrowable);
                }
            }
        }
        return callback.doSolrCall();
    }

    private String getCallName(String methodName, Object... args) {
        StringBuilder callName = new StringBuilder(collectionName + "." + methodName + "(");
        if (args != null) {
            boolean first = true;
            for (Object arg : args) {
                if (!first) {
                    callName.append(", ");
                }
                callName.append(String.valueOf(arg));
                first = false;
            }
        }
        callName.append(")");
        return callName.toString();
    }

    public RobustSolrClientAdapter setOperationsRetryCount(int value) {
        operationsRetryCount = value;
        return this;
    }

    public RobustSolrClientAdapter setOperationsSleepSec(long value) {
        operationsSleepSec = value;
        return this;
    }

    public RobustSolrClientAdapter setApplyRetryStrategy(boolean value) {
        applyRetryStrategy = value;
        return this;
    }

    public interface RetryCallback<T> {
        T doSolrCall() throws Exception;
    }

    //-----------------------------------------------------------------------------
    //  Interface Methods
    //-----------------------------------------------------------------------------

    public QueryResponse query(SolrQuery query, SolrRequest.METHOD method, int retryCount) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.query(query, method),
                getCallName("query", query.toQueryString(), method), retryCount);
    }

    public QueryResponse query(SolrQuery query, int retryCount) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.query(query),
                getCallName("query", query.toQueryString()), retryCount);
    }

    public QueryResponse query(SolrQuery query, SolrRequest.METHOD method) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.query(query, method),
                getCallName("query", query.toQueryString(), method));
    }

    public QueryResponse query(SolrQuery query) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.query(query),
                getCallName("query", query.toQueryString()));
    }

    public UpdateResponse deleteByQuery(String query) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.deleteByQuery(query),
                getCallName("deleteByQuery", query));
    }

    public UpdateResponse deleteById(String id) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.deleteById(id),
                getCallName("deleteById", id));
    }

    public UpdateResponse deleteByIds(List<String> ids) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.deleteByIds(ids),
                getCallName("deleteByIds", ids));
    }

    public UpdateResponse commit() throws Exception {
        return retryWithTemplate(solrClientDelegate::commit, getCallName("commit"));
    }

    public UpdateResponse commit(String collection, boolean waitFlush, boolean waitSearcher) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.commit(collection, waitFlush, waitSearcher),
                getCallName("commit", collection, waitFlush, waitSearcher));
    }

    public UpdateResponse commit(boolean waitFlush, boolean waitSearcher, boolean softCommit) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.commit(waitFlush, waitSearcher, softCommit),
                getCallName("commit", waitFlush, waitSearcher, softCommit));
    }

    public UpdateResponse optimize(boolean waitFlush, boolean waitSearcher, int maxSegments) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.optimize(waitFlush, waitSearcher, maxSegments),
                getCallName("optimize", waitFlush, waitSearcher, maxSegments));
    }

    public UpdateResponse optimize() throws Exception {
        return retryWithTemplate(solrClientDelegate::optimize, getCallName("optimize"));
    }

    public SolrPingResponse ping() throws Exception {
        return retryWithTemplate(solrClientDelegate::ping, getCallName("ping"));
    }

    public UpdateResponse add(SolrInputDocument doc) throws Exception {
        return add(doc, true);
    }

    public UpdateResponse add(SolrInputDocument doc, boolean shouldRetry) throws Exception {
        if (shouldRetry) {
            return retryWithTemplate(() ->
                            solrClientDelegate.add(doc),
                    getCallName("add", "SolrInputDocument"));
        } else {
            return solrClientDelegate.add(doc);
        }
    }

    public UpdateResponse add(SolrInputDocument doc, int commitWithinMs) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.add(doc, commitWithinMs),
                getCallName("add", "SolrInputDocument", commitWithinMs));
    }

    public UpdateResponse add(Collection<SolrInputDocument> docs, int commitWithinMs) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.add(docs, commitWithinMs),
                getCallName("add", "docs(" + docs.size() + ")", commitWithinMs));
    }

    public UpdateResponse add(Collection<SolrInputDocument> docs) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.add(docs),
                getCallName("add", "Collection<SolrInputDocument>(" + docs.size() + ")"));
    }

    public UpdateResponse addBean(Object obj) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.addBean(obj),
                getCallName("addBean", obj.getClass()));
    }

    public UpdateResponse addBean(Object obj, int commitWithinMs) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.addBean(obj, commitWithinMs),
                getCallName("addBean", obj.getClass(), commitWithinMs));
    }

    @Override
    public UpdateResponse addBeans(List<?> entities) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.addBeans(entities),
                getCallName("addBeans", List.class));
    }

    @Override
    public UpdateResponse addBeans(List<?> entities, int commitWithinMs) throws Exception {
        return retryWithTemplate(() ->
                        solrClientDelegate.addBeans(entities, commitWithinMs),
                getCallName("addBeans", List.class, Integer.class));
    }

    // -----------------------------------------------------------------------------------------------------------------

}
