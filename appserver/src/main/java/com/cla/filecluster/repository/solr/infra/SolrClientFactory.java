package com.cla.filecluster.repository.solr.infra;

import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.util.PermissionCheckUtils;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.apache.http.client.HttpClient;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.Optional;

@Service
public class SolrClientFactory {

    private static final Logger logger = LoggerFactory.getLogger(SolrClientFactory.class);

    @Autowired
    private SharePermissionService sharePermissionService;

    @Value("${spring.data.solr.host}")
    private String solrServerURL;

    @Value("${spring.data.solr.type:http}")
    private String solrServerType;

    @Value("${categoryFiles.solr.core:CatFiles}")
    private String catFilesSolrCore;

    @Value("${solr.client.operations-retryCount:3}")
    private int operationsRetryCount;

    @Value("${solr.client.operations-sleep-sec:5}")
    private long operationsSleepSec;

    @Value("${solr.client.timeout-in-millis:600000}")
    private int timeoutInMillis;

    @Value("${applyRetryStrategy.solr:true}")
    private boolean applyRetryStrategy;

    @Value("${solr.client.enforce-acls:true}")
    private boolean enforceAcls;

    @Value("${security.acl.generic-tokens:\\\\Everyone}")
    private String[] genericTokens;

    @Value("${solr.client.case-sensitive-acls:false}")
    private boolean caseSensitiveAcls;

    @Value("${solr.client.max-connections:128}")
    private int maxConnections;

    @Value("${solr.client.max-connections-per-host:32}")
    private int maxConnectionsPerHost;

    @Value("${security.share-permission.read-mask:131241}")
    private long readMaskSharePermission;

    private Table<String, Boolean, SolrClientAdapter> coreCache = HashBasedTable.create();

    public SolrClientAdapter getSolrClient(final String coreName, boolean secure) {
        String trimmedCoreName = coreName.trim();
        SolrClientAdapter solrClient = coreCache.get(trimmedCoreName, secure);
        if (solrClient == null) {
            solrClient = createSolrClient(trimmedCoreName, this.solrServerType, solrServerURL, secure);
            coreCache.put(coreName, secure, solrClient);
        }
        return solrClient;
    }

    private SolrClientAdapter createSolrClient(String coreName, String solrServerType, String solrServerURL, boolean secure) {
        SolrClientAdapter solrClientAdapter = createRobustSolrClient(coreName, solrServerType, solrServerURL);
        // for cat files core - wrap with ACLs enforcing client
        if (enforceAcls && secure && catFilesSolrCore.equals(coreName)) {
            solrClientAdapter = new AclCatFileSolrClientAdapter(solrClientAdapter, sharePermissionService,
                    PermissionCheckUtils.getGenericTokensAsList(genericTokens, caseSensitiveAcls),
                    caseSensitiveAcls, readMaskSharePermission);
        }
        return solrClientAdapter;
    }

    private SolrClientAdapter createRobustSolrClient(String coreName, String solrServerType, String solrServerURL) {
        SolrClient solrClient;
        String finalUrl = solrServerURL;
        boolean specificCore = !Strings.isNullOrEmpty(coreName);
        String coreStr = specificCore ? coreName : "N/A";

        if (specificCore) {
            finalUrl += "/" + coreName + "/";
        }

        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set(HttpClientUtil.PROP_MAX_CONNECTIONS, maxConnections);
        params.set(HttpClientUtil.PROP_MAX_CONNECTIONS_PER_HOST, maxConnectionsPerHost);
        HttpClient httpClient = HttpClientUtil.createClient(params);

        if (isSolrCloud(solrServerType, solrServerURL)) {
            String[] split = solrServerURL.split("/", 2);
            String zkUrl = split[0];
            String zkRoot = (split.length == 1) ? null : ("/" + split[1]);
            logger.info("Create Cloud Solr Client {} URL={} zkRoot={}, timeout: {} ms",
                    (coreName != null) ? coreName : "-Generic-", zkUrl, zkRoot, timeoutInMillis);

            solrClient = new CloudSolrClient
                    .Builder(Lists.newArrayList(zkUrl), Optional.of(zkRoot))
                    .withSocketTimeout(timeoutInMillis)
                    .withHttpClient(httpClient)
                    .build();

            if (coreName != null) {
                ((CloudSolrClient) solrClient).setDefaultCollection(coreName);
            }
        } else {
            logger.info("Create Solr Client  {}", coreName);
            HttpSolrClient.Builder builder = new HttpSolrClient.Builder();

            solrClient = builder.withBaseSolrUrl(finalUrl)
                    .withHttpClient(httpClient)
                    .withSocketTimeout(timeoutInMillis)
                    .allowCompression(true)
                    .build();
        }
        return createRobustSolrClient(solrClient, coreStr);
    }

    public SolrClientAdapter generalSolrClient() {
        return createGenericSolrClient(solrServerType, solrServerURL);
    }

    private SolrClientAdapter createGenericSolrClient(String solrServerType, String solrServerURL) {
        return createRobustSolrClient(null, solrServerType, solrServerURL);
    }

    public boolean isSolrCloud() {
        return isSolrCloud(solrServerType, solrServerURL);
    }

    private boolean isSolrCloud(String solrServerType, String solrServerURL) {
        return solrServerType.equalsIgnoreCase("cloud")
                || !solrServerURL.contains("http");
    }

    private RobustSolrClientAdapter createRobustSolrClient(SolrClient solrClient, String collectionName) {
        return RobustSolrClientAdapter.create(new SolrClientAdapterSupport(solrClient), collectionName)
                .setApplyRetryStrategy(applyRetryStrategy)
                .setOperationsRetryCount(operationsRetryCount)
                .setOperationsSleepSec(operationsSleepSec);
    }

    @PreDestroy
    private void closeAllUnderlyingClients() {
        coreCache.cellSet().stream()
                .forEach(this::closeSolrClient);
    }

    private void closeSolrClient(Table.Cell<String, Boolean, SolrClientAdapter> cachedSolrClientCell) {
        try {
            cachedSolrClientCell.getValue().getUnderyingClient().close();
            logger.debug("Closed solr client {}", cachedSolrClientCell.getRowKey());
        } catch (IOException e) {
            logger.error(String.format("Failed to close solr client %s", cachedSolrClientCell.getRowKey()), e);
        }
    }
}
