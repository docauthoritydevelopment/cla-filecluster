package com.cla.filecluster.repository.jpa.security;

import com.cla.common.domain.dto.security.RoleTemplateType;
import com.cla.filecluster.domain.entity.security.RoleTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * Role template holds the individual roles connected to a composite role
 * Created by: yael
 * Created on: 11/30/2017
 */
public interface RoleTemplateRepository extends JpaRepository<RoleTemplate, Long> {

    @Query("SELECT br FROM RoleTemplate br WHERE br.deleted = false")
    List<RoleTemplate> findNotDeleted();

    @Query("SELECT br FROM RoleTemplate br WHERE br.deleted = false")
    Page<RoleTemplate> findNotDeleted(Pageable pageRequest);

    @Query("SELECT br FROM RoleTemplate br WHERE br.deleted = false and br.templateType != :neqType")
    Page<RoleTemplate> findNotDeletedByNeqType(Pageable pageRequest, @Param("neqType") RoleTemplateType neqType);

    @Query("SELECT br FROM RoleTemplate br LEFT JOIN FETCH br.roles WHERE br.id = :id AND br.deleted = FALSE")
    RoleTemplate getOneWithRoles(@Param("id") Long templateRoleId);

    @Query("SELECT br FROM RoleTemplate br LEFT JOIN FETCH br.roles WHERE br.name = :name AND br.deleted = FALSE")
    RoleTemplate findByName(@Param("name") String name);

    @Query("SELECT br FROM RoleTemplate br LEFT JOIN FETCH br.roles WHERE br.name = :name")
    RoleTemplate findByNameAll(@Param("name") String name);

    @Query("SELECT br FROM RoleTemplate br LEFT JOIN FETCH br.roles WHERE br.displayName = :displayName AND br.deleted = FALSE")
    RoleTemplate getRoleTemplateByDisplayName(@Param("displayName") String displayName);

    @Query("SELECT br FROM RoleTemplate br LEFT JOIN FETCH br.roles WHERE br.displayName = :displayName")
    RoleTemplate getRoleTemplateByDisplayNameAll(@Param("displayName") String displayName);

    @Query("SELECT br FROM RoleTemplate br " +
            "WHERE br.id IN (:templateRoleIds)"+
            " AND br.deleted = false")
    List<RoleTemplate> findByIds(@Param("templateRoleIds") Collection<Long> templateRoleIds);
}
