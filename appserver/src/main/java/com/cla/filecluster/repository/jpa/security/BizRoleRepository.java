package com.cla.filecluster.repository.jpa.security;

import com.cla.filecluster.domain.entity.security.BizRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by uri on 07/09/2016.
 */
public interface BizRoleRepository extends JpaRepository<BizRole, Long> {

    @Query("SELECT br FROM BizRole br WHERE br.deleted = false")
    Set<BizRole> findNotDeleted();

    @Query("SELECT br FROM BizRole br WHERE br.deleted = false")
    Page<BizRole> findNotDeleted(Pageable pageRequest);

    @Query("SELECT br FROM BizRole br LEFT JOIN FETCH br.template t WHERE br.deleted = FALSE and t.id = :roleTemplateId")
    List<BizRole> findByRoleTemplate(@Param("roleTemplateId") Long roleTemplateId);

    @Query("SELECT br FROM BizRole br LEFT JOIN FETCH br.template t LEFT JOIN FETCH t.roles WHERE br.id = :id AND br.deleted = FALSE")
    BizRole getOneWithRoles(@Param("id") Long bizRoleId);

    @Query("SELECT br FROM BizRole br left join fetch br.template t LEFT JOIN FETCH t.roles WHERE br.name = :name AND br.deleted = FALSE")
    BizRole findByName(@Param("name") String name);

    @Query("SELECT br FROM BizRole br left join fetch br.template t LEFT JOIN FETCH t.roles WHERE br.name = :name")
    BizRole findByNameAll(@Param("name") String name);

    @Query("SELECT br FROM BizRole br LEFT JOIN FETCH br.template t LEFT JOIN FETCH t.roles WHERE br.displayName = :displayName AND br.deleted = FALSE")
    BizRole getBizRoleByDisplayName(@Param("displayName") String displayName);

    @Query("SELECT br FROM BizRole br LEFT JOIN FETCH br.template t LEFT JOIN FETCH t.roles WHERE br.displayName = :displayName")
    BizRole getBizRoleByDisplayNameAll(@Param("displayName") String displayName);

    @Query("SELECT br FROM BizRole br " +
            "WHERE br.id IN (:bizRoleIds)"+
            " AND br.deleted = false")
    Set<BizRole> findByIds(@Param("bizRoleIds") Collection<Long> bizRoleIds);
}
