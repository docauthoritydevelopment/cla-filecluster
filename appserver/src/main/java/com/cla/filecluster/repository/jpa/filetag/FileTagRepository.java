package com.cla.filecluster.repository.jpa.filetag;

import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Repository for file tags
 * Created by uri on 24/08/2015.
 */
public interface FileTagRepository extends JpaRepository<FileTag, Long>, JpaSpecificationExecutor<FileTag> {

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.id = :tagId")
    FileTag findOneWithDocTypeContext(@Param("tagId") long tagId);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.name = :name and ft.deleted = false")
    FileTag findOneByName(@Param("name") String name);

    @Query("select ft from FileTag ft left join fetch ft.type where ft.name like %:name% and ft.deleted = false")
    List<FileTag> findTagNameLike(@Param("name") String name);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.name = :name and ft.type = :fileTagType and ft.deleted = false")
    FileTag findOneByNameAndType(@Param("name") String name, @Param("fileTagType") FileTagType fileTagType);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.name = :name and ft.type.id = :fileTagTypeId and ft.deleted = false")
    FileTag findOneByNameAndType(@Param("name") String name, @Param("fileTagTypeId") long fileTagTypeId);


    @Query(value = "select ft from FileTag ft" +
            " left join fetch ft.docTypeContext" +
            " left join fetch ft.type " +
            " where ft.type = :fileTagType" +
            " and ft.docTypeContext is null" +
            " and ft.entityIdContext = :entityId" +
            " and action = :action and ft.deleted = false")
    FileTag findOneByUniqueIdentifiers(@Param("fileTagType") FileTagType fileTagType, @Param("entityId") String entityId, @Param("action") FileTagAction action);

    @Query(value = "select ft from FileTag ft" +
            " left join fetch ft.docTypeContext" +
            " left join fetch ft.type" +
            " where ft.type = :fileTagType" +
            " and ft.docTypeContext = :docType" +
            " and ft.entityIdContext is null" +
            " and action = :action and ft.deleted = false")
    FileTag findOneByUniqueIdentifiers(@Param("fileTagType") FileTagType fileTagType, @Param("docType") DocType docType, @Param("action") FileTagAction action);

    @Query(value = "select ft from FileTag ft" +
            " left join fetch ft.docTypeContext" +
            " left join fetch ft.type" +
            " where ft.type = :fileTagType" +
            " and ft.docTypeContext = :docType" +
            " and ft.entityIdContext = :entityId" +
            " and action = :action and ft.deleted = false")
    FileTag findOneByUniqueIdentifiers(@Param("fileTagType") FileTagType fileTagType, @Param("docType") DocType docType, @Param("entityId") String entityId, @Param("action") FileTagAction action);

    @Query(value = "select ft from FileTag ft" +
            " left join fetch ft.docTypeContext" +
            " left join fetch ft.type " +
            " where ft.type = :fileTagType" +
            " and ft.docTypeContext is null" +
            " and ft.entityIdContext is null" +
            " and action = :action and ft.deleted = false")
    FileTag findOneByUniqueIdentifiers(@Param("fileTagType") FileTagType fileTagType, @Param("action") FileTagAction action);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.id in :tagIds")
    List<FileTag> findFileTagsById(@Param("tagIds") List<Long> tagIds);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.name in :names and ft.deleted = false")
    List<FileTag> findTagsByNames(@Param("names") List<String> name);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.deleted = false")
    List<FileTag> findAllFileTags();

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.type.id = :typeId and ft.deleted = false")
    List<FileTag> findFileTagsByType(@Param("typeId") Long typeId);

    @Query(value = "select ft from FileTag ft left join fetch ft.docTypeContext left join fetch ft.type where ft.type.sensitiveTag = true and ft.deleted = false")
    List<FileTag> findSensitiveFileTags();

    @Query(value = "select ft from FileTag ft where ft.parentFileTag.id = :parentId and ft.deleted = false")
    List<FileTag> findFileTagChildren(@Param("parentId") long fileTagId);

    @Query(value = "select ft from FileTag ft where ft.identifier in :tagIdentifiers and ft.deleted = false")
    List<FileTag> findFileTagsByIdentifiers(@Param(("tagIdentifiers")) List<String> tagIdentifiers);
}
