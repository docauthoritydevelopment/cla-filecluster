package com.cla.filecluster.repository.jpa.filetree;

import com.cla.filecluster.domain.entity.filetree.Mailbox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by: yael
 * Created on: 2/10/2019
 */
public interface MailboxRepository extends JpaRepository<Mailbox, Long>, JpaSpecificationExecutor<Mailbox> {

    @Query(value = "select mb from Mailbox mb where mb.rootFolderId = :rootFolderId")
    List<Mailbox> getByRootFolderId(@Param("rootFolderId") long rootFolderId);

    @Query(value = "select mb from Mailbox mb where mb.rootFolderId = :rootFolderId and mb.upn = :upn")
    Mailbox getByUpnAndRootFolderId(@Param("upn") String upn, @Param("rootFolderId") long rootFolderId);
}
