package com.cla.filecluster.repository.jpa.scope;

import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.scope.UserScope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BasicScopeRepository extends JpaRepository<BasicScope, Long> {
}
