package com.cla.filecluster.repository.jpa.schedule;

import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 * Created by uri on 02/05/2016.
 */
public interface ScheduleGroupRepository extends JpaRepository<ScheduleGroup,Long> {

    @Query("select sg from ScheduleGroup sg where sg.name = :name")
    ScheduleGroup findByName(@Param("name") String name);

    @Query("select sg from ScheduleGroup sg left join sg.rootFolderSchedules where sg.name = :name")
    ScheduleGroup findByNameWithRootFolders(@Param("name") String name);

    @Query(value = "select distinct sg from ScheduleGroup sg left join sg.rootFolderSchedules"
    , countQuery = "select count(sg) from ScheduleGroup sg")
    Page<ScheduleGroup> findAllJoinRootFolders(Pageable pageRequest);

    @Query(value = "select distinct sg from ScheduleGroup sg left join sg.rootFolderSchedules where lower(sg.name) like %:groupNameSearch%"
            , countQuery = "select count(sg) from ScheduleGroup sg where lower(sg.name) like %:groupNameSearch%")
    Page<ScheduleGroup> findAllJoinRootFolders(Pageable pageRequest, @Param("groupNameSearch") String groupNameSearch);

    @Query(value = "select distinct sg from ScheduleGroup sg left join sg.rootFolderSchedules where sg.id in :ids"
            , countQuery = "select count(sg) from ScheduleGroup sg where sg.id in :ids")
    Page<ScheduleGroup> findAllJoinRootFolders(Pageable pageRequest, @Param("ids") Iterable<Long> groupIds);

    @Query("select sg from ScheduleGroup sg left join sg.rootFolderSchedules where sg.id = :id")
    ScheduleGroup findWithRootFolders(@Param("id") Long id);

    @Query(value = "select distinct sg from ScheduleGroup sg left join sg.rootFolderSchedules where sg.active = true")
    List<ScheduleGroup> findActiveWithRootFolders();

    @Query("select distinct sg from RootFolderSchedule rfs left join rfs.scheduleGroup sg where rfs.rootFolder.id = :rootFolderId")
    Page<ScheduleGroup> findRootFolderScheduleGroups(@Param("rootFolderId") Long rootFolderId, Pageable pageRequest);

    @Query(value = "select sg from ScheduleGroup sg where sg.id in :ids")
    List<ScheduleGroup> findByIds(@Param("ids") Collection<Long> ids);

    @Query(value = "select distinct sg from ScheduleGroup sg" +
            " inner join sg.rootFolderSchedules "
            , countQuery = "select count(sg) from ScheduleGroup sg where sg.rootFolderSchedules is not empty")
    Page<ScheduleGroup> listScheduleGroupsWithRootFolders(Pageable pageRequest);

    @Query(value = "select distinct sg from ScheduleGroup sg" +
            " inner join sg.rootFolderSchedules rfs inner join rfs.rootFolder rf where (lower(rf.realPath) like %:namingSearchTerm%" +
            " or lower(rf.path) like %:namingSearchTerm% or lower(rf.nickName) like %:namingSearchTerm%" +
            " or lower(rf.mailboxGroup) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)"
            , countQuery = "select count(sg) from ScheduleGroup sg inner join sg.rootFolderSchedules rfs inner join rfs.rootFolder rf " +
            "where (lower(rf.realPath) like %:namingSearchTerm% or lower(rf.path) like %:namingSearchTerm%" +
            " or lower(rf.nickName) like %:namingSearchTerm% or lower(rf.mailboxGroup) like %:namingSearchTerm% or lower(rf.description) like %:namingSearchTerm%)")
    Page<ScheduleGroup> listScheduleGroupsWithRootFolders(Pageable pageRequest, @Param("namingSearchTerm") String namingSearchTerm);

    @Query(value = "select distinct sg from ScheduleGroup sg" +
            " inner join sg.rootFolderSchedules rfs inner join rfs.rootFolder rf where lower(sg.name) like %:groupNameSearch%"
            , countQuery = "select count(sg) from ScheduleGroup sg inner join sg.rootFolderSchedules rfs inner join rfs.rootFolder rf where lower(sg.name) like %:groupNameSearch%")
    Page<ScheduleGroup> listScheduleGroupsWithRootFoldersByName(Pageable pageRequest, @Param("groupNameSearch") String groupNameSearch);

    @Query(value = "select distinct sg from ScheduleGroup sg" +
            " inner join sg.rootFolderSchedules rfs inner join rfs.rootFolder rf where sg.id in :ids"
            , countQuery = "select count(sg) from ScheduleGroup sg inner join sg.rootFolderSchedules rfs inner join rfs.rootFolder rf where sg.id in :ids")
    Page<ScheduleGroup> listScheduleGroupsWithRootFolders(Pageable pageRequest, @Param("ids") Iterable<Long> groupIds);

    @Query(nativeQuery = true, value = "select sg.id, sg.name from schedule_group sg order by sg.name")
    List<Object[]> getScheduleGroupsNames();
}
