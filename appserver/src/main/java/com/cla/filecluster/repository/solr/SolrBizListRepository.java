package com.cla.filecluster.repository.solr;

import com.cla.common.constants.BizListFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.filecluster.domain.dto.SolrBizListEntity;
import com.cla.filecluster.domain.entity.extraction.BizListItemExtraction;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

/**
 * save results of biz list run temporarily to see which files fit rule criteria and should be tagged
 * Created by: yael
 * Created on: 7/1/2018
 */
@Repository
public class SolrBizListRepository extends SolrRepository<BizListFieldType, SolrBizListEntity> {

    private static final String DELIMITER = "!";
    private static final String INNER_DELIMITER = "_";

    @Value("${bizList.solr.core:BizList}")
    private String bizListSolrCore;

    @Override
    protected Class<BizListFieldType> getSchemaType() {
        return BizListFieldType.class;
    }

    @Override
    protected Class<SolrBizListEntity> getEntityType() {
        return SolrBizListEntity.class;
    }

    @Override
    protected String coreName() {
        return bizListSolrCore;
    }

    @Override
    public SolrOpResult save(SolrBizListEntity entity) {
        SolrOpResult result = SolrOpResult.create();
        try {
            client.addBean(entity);
            result.setSuccessful(true);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
        return result;
    }

    public String calcId(String contentId, Long fileId, long bizListId, String ruleName, String bizListItemSid, Integer bizListItemType) {
        String id = contentId + DELIMITER + fileId + INNER_DELIMITER + bizListId + INNER_DELIMITER + ruleName;
        if (!bizListItemType.equals(BizListItemType.CONSUMERS.ordinal())) {
            id += INNER_DELIMITER + bizListItemSid;
        }
        return id;
    }

    @Override
    protected SolrCoreSchema getIdField() {
        return BizListFieldType.ID;
    }

    public static BizListItemExtraction fromEntity(SolrBizListEntity entity) {
        BizListItemExtraction item = new BizListItemExtraction();
        item.setId(entity.getId());
        item.setBizListItemSid(entity.getBizListItemSid());
        if (entity.getBizListItemType() != null) {
            item.setBizListItemType(BizListItemType.valueOf(entity.getBizListItemType()));
        }
        item.setAggMinCount(entity.getAggMinCount());
        item.setBizListId(entity.getBizListId());
        item.setClaFileId(entity.getFileId());
        item.setRuleName(entity.getRuleName());
        item.setDirty(entity.getDirty());
        item.setCounter(entity.getCounter());
        return item;
    }

    public List<BizListItemExtraction> fromEntity(List<SolrBizListEntity> entities) {
        List<BizListItemExtraction> result = new ArrayList<>();
        if (entities != null) {
            entities.forEach(e -> result.add(fromEntity(e)));
        }
        return result;
    }

    public void createFromDocuments(Collection<SolrInputDocument> documents) {
        if (documents == null || documents.size() == 0) {
            return;
        }
        try {
            logger.debug("Send {} documents to Solr", documents.size());
            UpdateResponse response = client.add(documents);
            if (response == null) {
                throw new RuntimeException("Failed to create biz record in Solr.");
            }
            logger.debug("Got response from solr: {}", response);
        } catch (Exception e) {
            logger.error("Failed to create biz record in Solr", e);
            String listStr = documents.stream().map(d -> d.getField(BizListFieldType.ID.getSolrName()).toString())
                    .collect(Collectors.joining(","));
            logger.error("Failed file ids: {}", listStr);
            throw new RuntimeException("Failed to create biz record in Solr.", e);
        }
    }

    public void updateNotDirtyByFileIds(final Long bizListId, List<Long> claFileIds) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(BizListFieldType.FILE_ID, SolrOperator.IN, claFileIds)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(BizListFieldType.DIRTY, SolrOperator.EQ, true)));

        if (bizListId != null) {
            filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(BizListFieldType.BIZ_LIST_ID, SolrOperator.EQ, bizListId)));
        }

        updateFieldByQuery(BizListFieldType.DIRTY, false, SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
    }

    public void deleteForBizList(Long bizListId) {
        SolrQuery query = Query.create().addFilterWithCriteria(Criteria.create(BizListFieldType.BIZ_LIST_ID, SolrOperator.EQ, bizListId)).build();
        deleteByQuery(query.getQuery());
    }

    public Collection<String> findByIds(Collection<String> ids) {
        SolrQuery query = Query.create()
                .addField(BizListFieldType.ID)
                .addFilterWithCriteria(BizListFieldType.ID, SolrOperator.IN, ids)
                .build();
        try {
            QueryResponse response = client.query(query, SolrRequest.METHOD.POST);
            return getSingleFieldValues(response, String.class, BizListFieldType.ID);
        } catch (Exception e) {
            logger.error("Failed to find biz list item records", e);
            return null;
        }
    }

    public Set<SolrBizListEntity> getFilesDirtyData(Long bizListId, List<Long> claFileIds) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(BizListFieldType.DIRTY, SolrOperator.EQ, "true"))
                .addFilterWithCriteria(Criteria.create(BizListFieldType.FILE_ID, SolrOperator.IN, claFileIds))
                .addField(BizListFieldType.FILE_ID)
                .addField(BizListFieldType.BIZ_LIST_ITEM_TYPE)
                .addField(BizListFieldType.BIZ_LIST_ID)
                .addField(BizListFieldType.BIZ_LIST_ITEM_SID)
                .addField(BizListFieldType.RULE_NAME)
                .addField(BizListFieldType.AGG_MIN_COUNT)
                .addField(BizListFieldType.COUNTER)
                ;

        if (bizListId != null) {
            query.addFilterWithCriteria(Criteria.create(BizListFieldType.BIZ_LIST_ID, SolrOperator.EQ, bizListId));
        }

        List<SolrBizListEntity> res = find(query.build());
        return new HashSet<>(res);
    }

    public Set<Long> getFilesByDirtyAndList(Long bizListId, int limit, int offset) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(BizListFieldType.DIRTY, SolrOperator.EQ, "true"));
        if (bizListId != null) {
            query.addFilterWithCriteria(Criteria.create(BizListFieldType.BIZ_LIST_ID, SolrOperator.EQ, bizListId));
        }
        query.setRows(0);
        query.addFacetField(BizListFieldType.FILE_ID);
        query.setFacetLimit(limit);
        query.setFacetOffset(offset);
        Set<Long> result = new HashSet<>();
        try {
            QueryResponse queryResponse = runQuery(query.build());
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    result.add(Long.parseLong(c.getName()));
                }
            }
            return result;
        } catch (Exception e) {
            logger.error("Failed to get file list from Solr", e);
            throw new RuntimeException("Failed to get file list from Solr. query:" + query, e);
        }
    }
}
