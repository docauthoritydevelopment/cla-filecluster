package com.cla.filecluster.repository.jpa.schedule;

import com.cla.filecluster.domain.entity.schedule.ScheduleConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by uri on 03/05/2016.
 */
public interface SchedulePresetRepository extends JpaRepository<ScheduleConfiguration,Long> {
}
