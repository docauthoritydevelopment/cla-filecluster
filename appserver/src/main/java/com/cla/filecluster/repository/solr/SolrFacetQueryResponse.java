package com.cla.filecluster.repository.solr;

import org.apache.solr.client.solrj.response.FacetField;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.function.Function;

/**
 * Created by uri on 10/11/2015.
 */
public class SolrFacetQueryResponse {

    List<FacetField> facetFields;

    private long numFound;

    private PageRequest pageRequest;
    private Function<FacetField.Count, String> idExtractor;

    public SolrFacetQueryResponse() {
    }

    public SolrFacetQueryResponse(List<FacetField> facetFields, long numFound) {
        this.facetFields = facetFields;
        this.numFound = numFound;
    }


    public List<FacetField> getFacetFields() {
        return facetFields;
    }

    public void setFacetFields(List<FacetField> facetFields) {
        this.facetFields = facetFields;
    }

    public FacetField getFirstResult() {
        return facetFields == null ? null : facetFields.get(0);
    }

    public long getNumFound() {
        return numFound;
    }

    public void setNumFound(long numFound) {
        this.numFound = numFound;
    }

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }

    public void setIdExtractor(Function<FacetField.Count, String> idExtractor) {
        this.idExtractor = idExtractor;
    }

    public Function<FacetField.Count, String> getIdExtractor() {
        return idExtractor;
    }
}
