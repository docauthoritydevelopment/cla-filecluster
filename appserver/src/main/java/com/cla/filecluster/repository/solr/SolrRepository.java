package com.cla.filecluster.repository.solr;

import com.cla.common.constants.*;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.exceptions.BadSolrQuery;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.repository.solr.infra.SolrClientAdapter;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.repository.solr.query.*;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.FILE_ID;


public abstract class SolrRepository<T extends Enum, S extends SolrEntity> {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    static final String UPDATE_TAGS_REQUEST_HANDLER = "/update/tags";
    private static final String UPDATE_VALS_REQUEST_HANDLER = "/update/vals";
    static final int COMMIT_WITHIN_MS = 2000;

    @Autowired
    private SolrClientFactory solrClientFactory;

    @Autowired
    protected SolrSpecQueryBuilder solrSpecQueryBuilder;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    String hibernateDdlAuto;

    @Value("${solr.autoDelete:true}")
    boolean autoDelete;

    @Value("${catFile.facet.maxLimit:10000}")
    private int facetMaxLimit;

    @Value("${selectedItemId.max.pages:5}")
    private int maxPagesForSelectedItemIdLookAhead;

    private SolrCoreSchema schemaId;

    protected SolrClientAdapter client;

    @SuppressWarnings("unchecked")
    @PostConstruct
    protected void init() {
        client = solrClientFactory.getSolrClient(coreName(), true);
        if (getSchemaType() != null) {
            schemaId = (SolrCoreSchema) Enum.valueOf(getSchemaType(), "ID");
        } else {
            schemaId = DefaultSolrSchema.ID;
        }
        if (autoDelete && hibernateDdlAuto.contains("create")) {
            logger.info("Delete all Solr index as for bean as hibernateDdlAuto is on create, which causes DB tables recreate");
            deleteAll(solrClientFactory.getSolrClient(coreName(), false));
        }
    }

    public static String generateExtractedEntityId(String ee) {
        //TODO - fix
        return ee.replaceAll("[\\[\\]]", "");
    }

    /**
     * Get objects by filter query with specified criteria
     *
     * @param queryCriterias criteria
     * @return list of entity objects
     */
    public List<S> getByFilterCriterias(Criteria... queryCriterias) {
        Query query = Query.create();
        Arrays.stream(queryCriterias).forEach(query::addFilterWithCriteria);
        return getByQuery(query);
    }

    public Optional<S> getOneById(Object id) {
        if (id == null) {
            return Optional.empty();
        }
        return getOneByFilterCriterias(Criteria.create(getIdField(), SolrOperator.EQ, id));
    }

    /**
     * Get one object by filter query with specified criteria.<br/>
     * If the search returns more than one object, the first one will be picked.
     *
     * @param queryCriterias criterias
     * @return entity object
     */
    public Optional<S> getOneByFilterCriterias(Criteria... queryCriterias) {
        Query query = Query.create().setRows(1);
        Arrays.stream(queryCriterias).forEach(query::addFilterWithCriteria);
        return getOneByQuery(query);
    }

    private Optional<S> getOneByQuery(Query query) {
        try {
            QueryResponse response = client.query(query, SolrRequest.METHOD.POST);
            List<S> beans = response.getBeans(getEntityType());
            if (beans == null || beans.size() == 0) {
                return Optional.empty();
            }
            return Optional.of(beans.get(0));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<S> getByQuery(Query query) {
        try {
            QueryResponse response = client.query(query, SolrRequest.METHOD.POST);
            List<S> beans = response.getBeans(getEntityType());
            if (beans == null || beans.size() == 0) {
                return Lists.newLinkedList();
            }
            return beans;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected abstract Class<T> getSchemaType();

    protected abstract Class<S> getEntityType();

    protected abstract String coreName();

    public abstract SolrOpResult save(S entity);

    protected SolrClientAdapter getSolrClient() {
        return client;
    }

    protected SolrCoreSchema getIdField() {
        throw new RuntimeException("Not implemented for this repository type.");
    }


    SolrInputDocument atomicUpdate(Object id, FieldMod... fields) {
        SolrInputDocument doc = new SolrInputDocument();
        doc.addField(schemaId.getSolrName(), id);
        addUpdateFields(doc, fields);
        return doc;
    }

    public UpdateResponse atomicUpdate(SolrInputDocument entity) {
        try {
            return client.add(entity);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
    }

    public UpdateResponse atomicUpdate(SolrInputDocument entity, boolean shouldRetry) {
        try {
            return client.add(entity, shouldRetry);
        } catch (Exception e) {
            throw new RuntimeException("Failed to save new entity", e);
        }
    }

    public UpdateResponse atomicUpdateSingleField(Object id, SolrCoreSchema field, SolrOperator op, Object value) {
        return atomicUpdateSingleField(id, field, op, value, true);
    }

    public UpdateResponse atomicUpdateSingleField(Object id, SolrCoreSchema field, SolrOperator op, Object value, boolean shouldRetry) {
        return atomicUpdate(AtomicUpdateBuilder.create()
                .setId(field.idField(), id.toString())
                .addModifier(field, op, value)
                .build(), shouldRetry);
    }

    public static Map<String, Object> atomicSetValue(Object value) {
        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("set", value);
        return fieldModifier;
    }

    public static Map<String, Object> atomicAddValue(Object value) {
        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("add", value);
        return fieldModifier;
    }

    public static Map<String, Object> atomicRemoveValue(Object value) {
        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put("remove", value);
        return fieldModifier;
    }

    public static Map<String, Object> atomicChangeValue(Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
        fieldModifier.put(operation.getOpName(), value);
        return fieldModifier;
    }

    private void addUpdateFields(SolrInputDocument doc, FieldMod... fields) {
        Arrays.stream(fields).forEach(field -> field.addToDoc(doc));
    }

    public void commit() {
        try {
            client.commit();
        } catch (Exception e) {
            logger.error("Error during solr core commit. {}", e.getMessage(), e);
        }
    }

    public void softCommitNoWaitFlush() {
        try {
            client.commit(false, true, true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void optimize() {
        try {
            client.optimize();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<S> find(SolrQuery query) {
        try {
            QueryResponse queryResponse = getSolrClient().query(query, SolrRequest.METHOD.POST);
            return queryResponse.getBeans(getEntityType());
        } catch (Exception e) {
            throw new RuntimeException("Failed to find SOLR documents by query", e);
        }
    }

    public List<S> find(Collection<Long> ids) {
        Collection finalIds = ids;
        SolrCoreSchema idField = getIdField();
        if (!Long.class.equals(idField.getType())) {
            finalIds = ids.stream()
                    .map(id -> SolrQueryGenerator.transformValue(idField, id))
                    .collect(Collectors.toSet());
        }
        return find(Query.create()
                .addFilterWithCriteria(idField, SolrOperator.IN, finalIds)
                // Overrides Solr default page size - paging logic should be implemented at higher levels to maintain scalability
                .setRows(finalIds.size())
                .build());
    }

    List<SolrDocument> findDocuments(Collection<Long> contentIds) {
        if (contentIds.size() == 0) {
            return new ArrayList<>();
        }
        //id:1 OR id:2 OR id:3
        Query query = Query.create().setRows(contentIds.size())
                .addFilterWithCriteria(Criteria.create(DefaultSolrSchema.ID, SolrOperator.IN, contentIds));

        try {
            QueryResponse queryResponse = getSolrClient().query(query.build(), SolrRequest.METHOD.POST);
            SolrDocumentList results = queryResponse.getResults();
            if (results.size() == 0) {
                return null;
            }
            logger.debug("findDocuments: asked for {} items, {} returned", contentIds.size(), queryResponse.getResults().size());
            return queryResponse.getResults();
        } catch (Exception e) {
            logger.error("Failed to find Solr documents by ids - solr exception", e);
            throw new RuntimeException("Failed to find Solr documents by ids - solr exception", e);
        }
    }


    void updateFieldByQuery(SolrCoreSchema field, Object value, SolrFieldOp operation, String query,
                            List<String> filters, SolrCommitType commitType) {
        String transformedValue = SolrQueryGenerator.transformValue(field, value);
        updateFieldByQuery(field.getSolrName(), transformedValue, operation, query, filters, commitType);
    }

    /**
     * Updating field of cat file core in Solr to the specified value
     * The entries to be updated are determined by the query and the filters.
     *
     * @param field   field to be updated
     * @param value   new value to set to the field
     * @param query   query to find the entries that need to be updated
     * @param filters filters to augment the query
     */
    public Integer updateFieldByQuery(String field, String value, SolrFieldOp operation, String query, List<String> filters, SolrCommitType commitType) {
        try {
            SolrQuery solrQuery = new SolrQuery();
            if (query == null) {
                solrQuery.setQuery(SolrQueryGenerator.ALL_QUERY);
            } else {
                solrQuery.setQuery(query);
            }
            if (filters != null && filters.size() > 0) {
                filters.forEach(solrQuery::addFilterQuery);
            }
            return internalExecuteUpdateFieldByQuery(solrQuery, field, operation, value, commitType);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update field " + field + " in category files in Solr to value " + value, e);
        }
    }

    public Integer updateFieldBySolrSpecification(SolrSpecification solrSpecification, String field, SolrFieldOp operation, String value, SolrCommitType commitType) {
        try {
            SolrQuery solrQuery = solrSpecQueryBuilder.buildSolrQuery(solrSpecification, null);
            return internalExecuteUpdateFieldByQuery(solrQuery, field, operation, value, commitType);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update field " + field + " in category files in Solr to value " + value, e);
        }
    }

    public QueryResponse runQuery(SolrQuery query) {
        try {
            return client.query(query, SolrRequest.METHOD.POST);
        } catch (Exception e) {
            logger.error("Failed to run Query in Solr", e);
            throw new RuntimeException("Failed to run Query in Solr. query:" + query, e);
        }
    }

    public SolrQueryResponse<S> runQuery(SolrSpecification solrSpecification, PageRequest pageRequest, String selectedItemId) {
        SolrQueryResponse<S> solrQueryResponse;
        if (selectedItemId != null) {
            logger.debug("FileCount query with selectedItemId={}", selectedItemId);
            int index;
            int searchPage = -1;
            int searchPageSize = 3000;
            SolrDocumentList results;
            do {
                searchPage++;
                solrQueryResponse = runQuery(solrSpecification, PageRequest.of(searchPage, searchPageSize));
                results = solrQueryResponse.getQueryResponse().getResults();
                index = findSelectedItemIndex(selectedItemId, results);
                logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
                if (index > 0) {
                    //calculate page
                    pageRequest = PageRequest.of((index / pageRequest.getPageSize()), pageRequest.getPageSize());
                }
            }
            while (results.size() == searchPageSize && index < 0 && searchPage < maxPagesForSelectedItemIdLookAhead);
            int fromIndex = (int) pageRequest.getOffset();
            int toIndex = Math.min(results.size(), fromIndex + pageRequest.getPageSize());
            ArrayList<SolrDocument> values = new ArrayList<>(results);
            results.clear();
            results.addAll(values.subList(fromIndex, toIndex));
            if (searchPage > 0) { // We need to recalculate the page
                pageRequest = PageRequest.of(((index + (searchPage * searchPageSize)) / pageRequest.getPageSize()), pageRequest.getPageSize());
            }
            solrQueryResponse.setPageRequest(pageRequest);
        } else {
            solrQueryResponse = runQuery(solrSpecification, pageRequest);
        }
        return solrQueryResponse;
    }

    public SolrFacetQueryJsonResponse runFacetQueryJson(SolrSpecification solrFacetSpecification,
                                                        Function<SolrFacetJsonResponseBucket, String> idExtractor,
                                                        PageRequest pageRequest, String selectedItemId) {
        SolrFacetQueryJsonResponse response;
        if (selectedItemId != null && solrFacetSpecification.isUsingJsonFacet()) {
            String jsonFacetKey = solrFacetSpecification.getFacetJsonObjectList().get(0).getName();
            logger.debug("FileCount query with selectedItemId={}", selectedItemId);
            List<SolrFacetJsonResponseBucket> values;
            int index;
            int searchPage = -1;
            int searchPageSize = 3000;
            do {
                searchPage++;
                response = runFacetQueryJson(solrFacetSpecification, PageRequest.of(searchPage, searchPageSize));
                values = response.getResponseBucket(jsonFacetKey).getBuckets();
                index = -1;
                Integer goIndex = 0;
                for (SolrFacetJsonResponseBucket bucket : values) {
                    if (selectedItemId.equals(idExtractor.apply(bucket))) {
                        index = goIndex;
                        break;
                    }
                    goIndex++;
                }
                logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
            }
            while (values.size() == searchPageSize && index < 0 && searchPage < maxPagesForSelectedItemIdLookAhead);

            if (index > 0) {
                //calculate page - to find from and to
                pageRequest = PageRequest.of((index / pageRequest.getPageSize()), pageRequest.getPageSize());
                int from = (int) pageRequest.getOffset();
                int to = Math.min(values.size(), from + pageRequest.getPageSize());
                reduceToSubResponse(response, jsonFacetKey, from, to);
            }


            if (searchPage > 0) { // We need to recalculate the page
                if (index > 0) { // We need to recalculate the page (only if found)
                    int page = (index + (searchPage * searchPageSize)) / pageRequest.getPageSize();
                    pageRequest = PageRequest.of(page, pageRequest.getPageSize());
                } else {
                    logger.debug("Could not find selectedItemId={}. Re-run search without", selectedItemId);
                    response = runFacetQueryJson(solrFacetSpecification, pageRequest);
                }
            }
        } else {
            response = runFacetQueryJson(solrFacetSpecification, pageRequest);
        }
        response.setPageRequest(pageRequest);
        return response;
    }

    private void reduceToSubResponse(SolrFacetQueryJsonResponse response, String jsonFacetKey, int from, int to) {
        SimpleOrderedMap<Object> resultResponse = (SimpleOrderedMap<Object>) response.getRawResponse()
                .get(jsonFacetKey);
        int bucketsIndex = findBucketsIndex(resultResponse);
        List<SimpleOrderedMap<Object>> results = (List<SimpleOrderedMap<Object>>) resultResponse.get(
                SolrFacetJsonResponseBucket.BUCKETS_KEY);
        resultResponse.setVal(bucketsIndex, results.subList(from, to));
    }

    private int findBucketsIndex(SimpleOrderedMap<Object> resultResponse) {
        int bucketsIndex = 0;
        for (Map.Entry<String, Object> entry : resultResponse) {
            if (entry.getKey().equals(SolrFacetJsonResponseBucket.BUCKETS_KEY)) {
                break;
            }
            bucketsIndex++;
        }
        return bucketsIndex;
    }

    public long getFacetNumber(SolrSpecification solrSpecification, String field) {
        SolrQuery query = null;
        try {
            query = solrSpecQueryBuilder.buildSolrQuery(solrSpecification, null);
            query.setRows(0);
            query.setParam("group", "true");
            query.setParam("group.ngroups", "true");
            query.setParam("group.field", field);
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            return queryResponse.getGroupResponse().getValues().get(0).getNGroups();
        } catch (BadRequestException e) {
            logger.error("Failed to list catFiles by specification - solr exception (" + query + ")", e);
            throw e;
        } catch (Exception e) {
            logger.error("Failed to list catFiles by specification - solr exception (" + query + ")", e);
            throw new RuntimeException("IO Failure to list cat files by specification - solr exception", e);
        }
    }

    public SolrFacetQueryResponse runFacetQuery(SolrSpecification solrFacetSpecification, Function<FacetField.Count, String> idExtractor, PageRequest pageRequest, String selectedItemId) {
        SolrFacetQueryResponse solrFacetQueryResponse;
        if (selectedItemId != null) {
            logger.debug("FileCount query with selectedItemId={}", selectedItemId);
            List<FacetField.Count> values;
            int index;
            int searchPage = -1;
            int searchPageSize = 3000;
            do {
                searchPage++;
                solrFacetQueryResponse = runFacetQuery(solrFacetSpecification, PageRequest.of(searchPage, searchPageSize));
                values = solrFacetQueryResponse.getFacetFields().stream().flatMap(f -> f.getValues().stream()).collect(Collectors.toList());
                index = findSelectedItemIndex(idExtractor, selectedItemId, values);
                logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
            }
            while (values.size() == searchPageSize && index < 0 && searchPage < maxPagesForSelectedItemIdLookAhead);
            FacetField facetField = solrFacetQueryResponse.getFacetFields().get(0);
            facetField.getValues().clear();
            if (index > 0) {
                //calculate page - to find fromIndex and toIndex
                pageRequest = PageRequest.of((index / pageRequest.getPageSize()), pageRequest.getPageSize());
            }
            int fromIndex = (int) pageRequest.getOffset();
            int toIndex = Math.min(values.size(), fromIndex + pageRequest.getPageSize());
            facetField.getValues().addAll(values.subList(fromIndex, toIndex));
            if (searchPage > 0) { // We need to recalculate the page
                if (index > 0) { // We need to recalculate the page (only if found)
                    int page = (index + (searchPage * searchPageSize)) / pageRequest.getPageSize();
                    pageRequest = PageRequest.of(page, pageRequest.getPageSize());
                } else {
                    logger.debug("Could not find selectedItemId={}. Re-run search without", selectedItemId);
                    solrFacetQueryResponse = runFacetQuery(solrFacetSpecification, pageRequest);
                }
            }
        } else {
            //Standard paged behavior
            solrFacetQueryResponse = runFacetQuery(solrFacetSpecification, pageRequest);
        }
        solrFacetQueryResponse.setPageRequest(pageRequest);
        solrFacetQueryResponse.setIdExtractor(idExtractor);
        return solrFacetQueryResponse;
    }


    /**
     * Updating field of cat file core in Solr to the specified value
     * The entries to be updated are determined by the query and the filters.
     *
     * @param field     SOLR core field
     * @param value     value to update
     * @param operation operation
     * @param criteria  criteria to find the entries that need to be updated
     * @param filters   filters to augment the criteria
     */
    public void updateFieldByCriteria(SolrCoreSchema field, Object value, SolrFieldOp operation, Criteria criteria, List<String> filters, SolrCommitType commitType) {
        String stringQuery = SolrQueryGenerator.generateFilter(criteria);
        String transformedValue = SolrQueryGenerator.transformValue(field, value);
        List<String> allFilters = Lists.newArrayList();
        if (filters != null) {
            allFilters.addAll(filters);
        }
        allFilters.add(stringQuery);
        updateFieldByQuery(field.getSolrName(), transformedValue, operation, SolrQueryGenerator.ALL_QUERY, allFilters, commitType);
    }

    public Integer updateFieldByQueryMultiFields(String[] fields, String[] values, String operation, String query, List<String> filters, SolrCommitType multiFieldCommitType) {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(query);
        return updateFieldByQueryMultiFields(fields, values, operation, solrQuery, filters, multiFieldCommitType);
    }

    public Integer updateFieldByQueryMultiFields(String[] fields, String[] values, String operation, SolrQuery solrQuery, List<String> filters, SolrCommitType multiFieldCommitType) {
        if (fields.length < 1 || fields.length > 10) {
            throw new RuntimeException("Failed to update field: length should be between 1 and 10 ");
        }
        if (fields.length != values.length) {
            throw new RuntimeException("Failed to update field: fields and values length should match: " + fields.length + "<>" + values.length);
        }
        try {
            /*
             ** Create a 'tags update' Query with the parameters: tag.op cnt field1 tag1 field2 tag2 ...
             */

            solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);

            if (filters != null && filters.size() > 0) {
                filters.forEach(solrQuery::addFilterQuery);
            }
            //solrQuery.set(field, value);
            for (int i = 1; i <= fields.length; ++i) {
                solrQuery.set(SolrRequestParams.FIELD + i, fields[i - 1]);
                solrQuery.set(SolrRequestParams.VALUE + i, values[i - 1]);
            }
            solrQuery.set(SolrRequestParams.UPDATE_OP, operation);
            solrQuery.set(SolrRequestParams.COUNT, String.valueOf(fields.length));
            solrQuery.set(SolrRequestParams.COMMIT_TYPE, multiFieldCommitType.name());
            QueryResponse resp = client.query(solrQuery, SolrRequest.METHOD.POST);

            return resp.getResponse().get("docsChanged") == null ? null :
                    (Integer) resp.getResponse().get("docsChanged");

        } catch (Exception e) {
            throw new RuntimeException("Failed to update fields " + Arrays.asList(fields).toString()
                    + " in category files in Solr to value " + Arrays.asList(values).toString(), e);
        }
    }

    public SolrQueryResponse<S> runQuery(SolrSpecification solrSpecification, PageRequest pageRequest) {
        SolrQuery query = null;
        try {
            query = solrSpecQueryBuilder.buildSolrQuery(solrSpecification, pageRequest);
            if (logger.isTraceEnabled()) {
                logger.trace("Running by pages settings: {}, Query: {}");
            }
            QueryResponse queryResponse = client.query(query, SolrRequest.METHOD.POST);
            return new SolrQueryResponse<>(queryResponse, pageRequest, getEntityType());
        } catch (BadRequestException e) {
            logger.error("Failed to list catFiles by specification - solr exception (" + query + ")", e);
            throw e;
        } catch (Exception e) {
            logger.error("Failed to list catFiles by specification - solr exception (" + query + ")", e);
            throw new RuntimeException("IO Failure to list cat files by specification - solr exception", e);
        }
    }

    public SolrFacetQueryResponse runFacetQuery(SolrSpecification solrFacetSpecification, PageRequest pageRequest) {
        SolrQueryResponse queryResponse = getQueryResponse(solrFacetSpecification, pageRequest);
        if (queryResponse == null) {
            logger.warn("Error during runQuery.");
            return new SolrFacetQueryResponse(Lists.newArrayList(), 0);
        }
        List<FacetField> facetFields = queryResponse.getQueryResponse().getFacetFields();
        int facetCount = facetFields != null && facetFields.size() > 0 ? facetFields.get(0).getValueCount() : 0;
        long numFound = queryResponse.getQueryResponse().getResults().getNumFound();
        logger.debug("found {} elements on {} facets", numFound, facetCount);
        if (pageRequest == null && facetFields != null && facetFields.size() > 0) {
            FacetField facetField = facetFields.get(0);
            if (facetMaxLimit == facetField.getValueCount()) {
                logger.warn("Solr Facet query ({}) returned the maximum limit ({}) of results ", facetField.getName(), facetMaxLimit);
            }
        }
        return new SolrFacetQueryResponse(facetFields, numFound);
    }


    @SuppressWarnings("unchecked")
    private SolrFacetQueryJsonResponse runFacetQueryJson(SolrSpecification solrFacetSpecification, PageRequest pageRequest) {
        SolrQueryResponse solrQueryResponse = getQueryResponse(solrFacetSpecification, pageRequest);
        return solrQueryResponse == null ?
                SolrFacetQueryJsonResponse.empty() : buildSolrFacetQueryJsonResponse(solrQueryResponse);
    }

    private SolrFacetQueryJsonResponse buildSolrFacetQueryJsonResponse(SolrQueryResponse solrQueryResponse) {
        final QueryResponse queryResponse = solrQueryResponse.getQueryResponse();
        final NamedList<Object> solrResponse = queryResponse.getResponse();
        SimpleOrderedMap<Object> result = (SimpleOrderedMap<Object>) solrResponse.get(
                SolrFacetJsonResponseBucket.FACETS_KEY);
        return new SolrFacetQueryJsonResponse(result, queryResponse.getResults().getNumFound());
    }

    @Nullable
    private SolrQueryResponse getQueryResponse(SolrSpecification solrFacetSpecification, PageRequest pageRequest) {
        SolrQueryResponse solrQueryResponse;
        try {
            solrQueryResponse = runQuery(solrFacetSpecification, pageRequest);
        } catch (Exception e) {
            if ((e instanceof HttpSolrClient.RemoteSolrException ||
                    (e.getCause() != null && e.getCause() instanceof HttpSolrClient.RemoteSolrException)) &&
                    e.getMessage().contains("SyntaxError")) {
                logger.debug("Bad Solr query. {}", e.getMessage());
                throw new BadSolrQuery("Query syntax error", e);
            } else {
                logger.error("Error during runQuery. {}", e, e);
                throw new RuntimeException(e);
            }
        }
        if (solrQueryResponse == null) {
            logger.warn("Error during runQuery.");
            return null;
        }
        return solrQueryResponse;
    }

    private Integer internalExecuteUpdateFieldByQuery(SolrQuery solrQuery, String field, SolrFieldOp operation, String value, SolrCommitType commitType) throws Exception {
        solrQuery.setRequestHandler(UPDATE_VALS_REQUEST_HANDLER);
        solrQuery.set(SolrRequestParams.FIELD, field);
        if (value != null) {
            solrQuery.set(SolrRequestParams.VALUE, value);
        }
        solrQuery.set(SolrRequestParams.UPDATE_OP, operation.getOpName());
        solrQuery.set(SolrRequestParams.COMMIT_TYPE, commitType.name());
        QueryResponse resp = client.query(solrQuery, SolrRequest.METHOD.POST);

        return resp.getResponse().get("docsChanged") == null ? null :
                (Integer) resp.getResponse().get("docsChanged");
    }

    void setFieldIfNotNull(SolrInputDocument doc, SolrCoreSchema field, Object value) {
        if (value != null) {
            setField(doc, field, value);
        }
    }

    public static void setField(SolrInputDocument doc, SolrCoreSchema field, Object value) {
        doc.setField(field.getSolrName(), value);
    }

    public static void setFieldOnce(SolrInputDocument doc, SolrCoreSchema field, Object value) {
        if (doc.getField(field.getSolrName()) == null) {
            doc.setField(field.getSolrName(), value);
        }
    }

    public void optimizeIndex() {
        try {
            logger.info("index optimization start...");
            final UpdateResponse resp = getSolrClient().optimize(true, true, 1);
            logger.info("index optimization done. status={} qtime={} seconds",
                    (resp.getStatus() == 0) ? "ok" : resp.getStatus(), ((int) (resp.getQTime() / 100f)) / 10f);
        } catch (final Exception e) {
            logger.error("Error during index optimization. {}", e.getMessage(), e);
        }
    }

    public SolrPingResponse pingSolr() {
        try {
            return getSolrClient().ping();
        } catch (Exception e) {
            logger.warn("Exception while pinging solr", e);
        }
        return null;
    }

    private int findSelectedItemIndex(String selectedItemId, SolrDocumentList results) {
        int index = -1;
        long selectedItemIdLong = Long.valueOf(selectedItemId);
        for (SolrDocument result : results) {
            index++;
            Long id = (Long) result.get(FILE_ID.getSolrName());
            if (selectedItemIdLong == id) {
                //found it.
                return index;
            }
        }
        return -1;
    }

    private int findSelectedItemIndex(Function<FacetField.Count, String> idExtractor, String selectedItemId, List<FacetField.Count> values) {
        int index = -1;
        for (FacetField.Count value : values) {
            index++;
            if (selectedItemId.equals(idExtractor.apply(value))) {
                //found it.
                return index;
            }
        }
        return -1;
    }

    <E> E getSingleFieldValue(QueryResponse response, Class<E> type, SolrCoreSchema field) {
        SolrDocumentList results = response.getResults();
        if (results.getNumFound() < 1) {
            return null;
        }
        SolrDocument entry = results.get(0);
        return SolrQueryGenerator.retrieveValue(type, (String) entry.getFieldValue(field.getSolrName()));
    }

    <E> Collection<E> getSingleFieldValues(QueryResponse response, Class<E> type, SolrCoreSchema field) {
        SolrDocumentList results = response.getResults();
        if (results.getNumFound() < 1) {
            return null;
        }
        List<E> list = Lists.newLinkedList();
        results.forEach(entry -> list.add(
                SolrQueryGenerator.retrieveValue(type, (String) entry.getFieldValue(field.getSolrName()))));
        return list;
    }

    List<Map<SolrCoreSchema, Object>> getMultiFieldValues(QueryResponse response, SolrCoreSchema... fields) {
        SolrDocumentList results = response.getResults();
        if (results.getNumFound() < 1) {
            return null;
        }
        List<Map<SolrCoreSchema, Object>> resultsList = Lists.newArrayList();
        results.forEach(entry -> resultsList.add(extractFields(entry, fields)));
        return resultsList;
    }

    public void deleteById(String id) {
        logger.debug("Delete by id {}", id);
        try {
            client.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException("Could not delete (" + id + ")", e);
        }
    }

    public void deleteByIds(List<String> ids) {
        logger.debug("Delete by ids {}", ids);
        try {
            if (ids == null || ids.isEmpty()) {
                logger.warn("Ids to delete is empty, ignoring request deleteByIds");
                return;
            }
            client.deleteByIds(ids);
        } catch (Exception e) {
            throw new RuntimeException("Could not delete (" + ids + ")", e);
        }
    }

    public void deleteByQuery(String query) {
        try {
            client.deleteByQuery(query);
            client.commit();
        } catch (Exception e) {
            throw new RuntimeException("Could not delete beans", e);
        }
    }

    private void deleteAll(SolrClientAdapter client) {
        try {
            client.deleteByQuery(SolrQueryGenerator.ALL_QUERY);
            client.commit();
        } catch (Exception e) {
            throw new RuntimeException("Could not delete beans", e);
        }
    }

    private Map<SolrCoreSchema, Object> extractFields(SolrDocument entry, SolrCoreSchema... fields) {
        Map<SolrCoreSchema, Object> map = Maps.newHashMap();
        for (SolrCoreSchema field : fields) {
            Object fieldValue = entry.getFieldValue(field.getSolrName());
            if (fieldValue != null) {
                map.put(field, fieldValue);
            }
        }
        return map;
    }

    public UpdateResponse saveAll(Collection<SolrInputDocument> atomicUpdates) {
        try {
            if (atomicUpdates == null || atomicUpdates.isEmpty()) {
                logger.warn("Documents list, ignoring request saveAll");
                return null;
            }
            return client.add(atomicUpdates);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed to save %d atomic updates",
                    atomicUpdates != null ? atomicUpdates.size() : 0), e);
        }
    }

    public UpdateResponse saveAll(List<SolrInputDocument> atomicUpdates, int commitWithinMs) {
        try {
            if (atomicUpdates == null || atomicUpdates.isEmpty()) {
                logger.warn("Documents list is empty, ignoring request saveAll with commitWithinMs {}", commitWithinMs);
                return null;
            }
            return client.add(atomicUpdates, commitWithinMs);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Failed to save %d atomic updates",
                    atomicUpdates != null ? atomicUpdates.size() : 0), e);
        }
    }

    /**
     * Field modifier construct
     */
    public static class FieldMod {
        private SolrCoreSchema field;
        private SolrFieldOp op;
        private Object value;

        private FieldMod(SolrCoreSchema field, SolrFieldOp op, Object value) {
            this.field = field;
            this.op = op;
            this.value = value;
        }

        public static FieldMod of(SolrCoreSchema field, SolrFieldOp op, Object value) {
            return new FieldMod(field, op, value);
        }

        void addToDoc(final SolrInputDocument doc) {
            Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
            fieldModifier.put(op.toString(), value);
            doc.addField(field.getSolrName(), fieldModifier);
        }
    }


    public List<SolrDocument> findAllByQuery(SolrQuery solrQuery, SolrCoreSchema sortField, Sort.Direction direction) {
        SolrQuery.ORDER order = direction == Sort.Direction.ASC ? SolrQuery.ORDER.asc : SolrQuery.ORDER.desc;
        solrQuery.setRows(1000)
                .setSort(sortField.getSolrName(), order);
        String cursorMark = CursorMarkParams.CURSOR_MARK_START;
        boolean done = false;
        LinkedList<SolrDocument> result = Lists.newLinkedList();
        while (!done) {
            solrQuery.set(CursorMarkParams.CURSOR_MARK_PARAM, cursorMark);
            QueryResponse rsp = runQuery(solrQuery);
            result.addAll(rsp.getResults());
            String nextCursorMark = rsp.getNextCursorMark();

            if (cursorMark.equals(nextCursorMark)) {
                done = true;
            }
            cursorMark = nextCursorMark;
        }
        return result;
    }

    private String getSolrFacetJsonResponseId(SolrFacetJsonResponseBucket jsonFacetBucket) {
        return jsonFacetBucket.getStringVal();
    }

    public SolrFacetQueryJsonResponse runFacetQueryJson(SolrSpecification solrFacetSpecification) {
        PageRequest pageRequest = solrFacetSpecification.getPageRequest();
        String selectedItemId = solrFacetSpecification.getSelectedItemId();
        return runFacetQueryJson(solrFacetSpecification, this::getSolrFacetJsonResponseId, pageRequest,
                pageRequest.getPageSize() == Integer.MAX_VALUE ? null : selectedItemId);
    }

    @AutoAuthenticate(replace = false)
    public Map<String, Integer> runFacetRangeQuery(SolrSpecification solrSpecification) {
        SolrQueryResponse solrQueryResponse = runQuery(solrSpecification, null);
        Map<String, Integer> facetQuery = solrQueryResponse.getQueryResponse().getFacetQuery();
        return facetQuery;
    }
}
