package com.cla.filecluster.repository.solr;
// this enum is used for define the SOLR json facet query type


public enum FacetType {
    SIMPLE("simple"),
    TERMS("terms"),
    FUNC("func"),
    QUERY("query"),
    RANGE("range"),
    HEATMAP("heatmap");


    private String name;

    FacetType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
