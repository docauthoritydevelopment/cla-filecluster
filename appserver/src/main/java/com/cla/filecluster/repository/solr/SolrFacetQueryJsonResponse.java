package com.cla.filecluster.repository.solr;

import org.apache.solr.common.util.SimpleOrderedMap;
import org.springframework.data.domain.PageRequest;

public class SolrFacetQueryJsonResponse {

    private final SimpleOrderedMap<Object> rawResponse;


    private PageRequest pageRequest;

    private long numFound;

    public SolrFacetQueryJsonResponse(SimpleOrderedMap<Object> rawResponse, long numFound) {
        this.rawResponse = rawResponse;
        this.numFound = numFound;
    }

    public long getNumFound() {
        return numFound;
    }

    public PageRequest getPageRequest() {
        return pageRequest;
    }

    public void setPageRequest(PageRequest pageRequest) {
        this.pageRequest = pageRequest;
    }


    public SolrFacetJsonResponseBucket getResponseBucket(String key) {
        SimpleOrderedMap<Object> tmpBucket = (SimpleOrderedMap<Object>) rawResponse.get(key);
        if (tmpBucket == null) {
            return new SolrFacetJsonResponseBucket(new SimpleOrderedMap<>());
        }
        return new SolrFacetJsonResponseBucket(tmpBucket);
    }

    public static SolrFacetQueryJsonResponse empty() {
        return new SolrFacetQueryJsonResponse(new SimpleOrderedMap<>(), 0L);
    }

    public SimpleOrderedMap<Object> getRawResponse() {
        return rawResponse;
    }


    public SolrFacetJsonResponseBucket topLevelBucket() {
        return new SolrFacetJsonResponseBucket(rawResponse);
    }
}
