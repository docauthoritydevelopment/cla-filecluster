package com.cla.filecluster.repository.solr.infra;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest.METHOD;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.StreamingResponseCallback;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;

import java.io.IOException;

public class AccountFilteredCloudSolrClient extends CloudSolrClient {

	public AccountFilteredCloudSolrClient(final Builder builder) {
		super(builder);
    }

	@Override
	public QueryResponse query(final SolrParams params) throws SolrServerException, IOException {
		return super.query(params);
	}

	@Override
	public QueryResponse query(final SolrParams params, final METHOD method) throws SolrServerException, IOException {
		return super.query(params, method);
	}

	@Override
	public QueryResponse queryAndStreamResponse(final SolrParams params, final StreamingResponseCallback callback)
			throws SolrServerException, IOException {
		return super.queryAndStreamResponse(params, callback);
	}
	
}
