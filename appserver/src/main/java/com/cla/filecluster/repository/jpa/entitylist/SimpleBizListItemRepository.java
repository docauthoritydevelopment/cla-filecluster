package com.cla.filecluster.repository.jpa.entitylist;

import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

/**
 *
 * Created by uri on 31/12/2015.
 */
public interface SimpleBizListItemRepository extends JpaRepository<SimpleBizListItem, String>, JpaSpecificationExecutor<SimpleBizListItem> {

    String PENDING_IMPORT = "com.cla.common.domain.dto.bizlist.BizListItemState.PENDING_IMPORT";
    String DELETED = "com.cla.common.domain.dto.bizlist.BizListItemState.DELETED";
    String ACTIVE = "com.cla.common.domain.dto.bizlist.BizListItemState.ACTIVE";
    String FILTERED_ITEMS = "(" + PENDING_IMPORT + "," + DELETED + ")";

    @Query("select bli from SimpleBizListItem bli where bli.bizList.id = :bizListId and bli.state not in " + FILTERED_ITEMS)
    Page<SimpleBizListItem> findAllByBizListFiltered(@Param("bizListId") long bizListId, Pageable pageable);

    @Modifying
    @Query("delete from SimpleBizListItem bli where bli.importId = :importId")
    void deleteByImportId(@Param("importId") long importId);

    @Query("select bli from SimpleBizListItem bli left join fetch bli.bizListItemAliases" +
            " where bli.sid = :bizListItemId")
    SimpleBizListItem findWithAliases(@Param("bizListItemId") String bizListItemId);

    @Query("select distinct bli from SimpleBizListItem bli left join fetch bli.bizListItemAliases")
    List<SimpleBizListItem> findAllWithAliases();

    @Query("select distinct bli from SimpleBizListItem bli left join fetch bli.bizListItemAliases" +
            " where bli.sid in :bizListItemIds")
    List<SimpleBizListItem> findItemsWithAliases(@Param("bizListItemIds") Collection<String> bizListItemIds);

    @Query("select bli from SimpleBizListItem bli left join fetch bli.bizListItemAliases" +
            " where bli.sid in :sids")
    List<SimpleBizListItem> findBySIds(@Param("sids") Collection<String> sids);

    @Query("select count(bli) from SimpleBizListItem bli where bli.bizList.id = :bizListId and bli.state not in " + FILTERED_ITEMS)
    long countByBizListId(@Param("bizListId") long bizListId);

    @Modifying
    @Query("delete from SimpleBizListItem bli where bli.bizList.id = :bizListId")
    void deleteByBizListId(@Param("bizListId") long bizListId);

    @Query("select bli.sid from SimpleBizListItem bli where bli.bizList.id = :listId and bli.state = " + ACTIVE)
    List<String> getActiveSimpleBizListItemIds(@Param("listId") long listId);

    @Query("select count(bli) from SimpleBizListItem bli where bli.bizList.id = :bizListId and bli.state = " + ACTIVE)
    long countActiveByBizListId(@Param("bizListId") long bizListId);

}
