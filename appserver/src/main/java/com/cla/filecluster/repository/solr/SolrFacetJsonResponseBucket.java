package com.cla.filecluster.repository.solr;

import org.apache.solr.common.util.SimpleOrderedMap;

import java.util.*;
import java.util.stream.Collectors;

public class SolrFacetJsonResponseBucket {
    public static final String FACETS_KEY = "facets";
    public static final String BUCKETS_KEY = "buckets";
    public static final String BUCKET_COUNT_KEY = "count";
    public static final String BUCKET_VALUE_KEY = "val";
    public static final String TOTAL_NUM_OF_BUCKETS_KEY = "numBuckets";
    private List<SolrFacetJsonResponseBucket> buckets = new ArrayList<>();

    private SimpleOrderedMap<Object> bucket;

    public SolrFacetJsonResponseBucket(SimpleOrderedMap<Object> bucket) {
        this(bucket, 0, Integer.MAX_VALUE);
    }

    private SolrFacetJsonResponseBucket(SimpleOrderedMap<Object> bucket, int fromIndex, int toIndex) {
        this.bucket = bucket;
        List<SimpleOrderedMap<Object>> buckets = (List<SimpleOrderedMap<Object>>) bucket.get(BUCKETS_KEY);
        if (buckets != null) {
            toIndex = buckets.size() < toIndex ? buckets.size() : toIndex;
            this.buckets = buckets.subList(fromIndex, toIndex).stream()
                    .map(SolrFacetJsonResponseBucket::new)
                    .collect(Collectors.toList());
        }

    }

    public Boolean getBooleanVal() {
        return (Boolean) bucket.get(BUCKET_VALUE_KEY);
    }

    public Long getLongVal() {
        return (Long) bucket.get(BUCKET_VALUE_KEY);
    }

    public String getStringVal() {
        return String.valueOf(bucket.get(BUCKET_VALUE_KEY));
    }

    public Date getDateVal() {
        return (Date) bucket.get(BUCKET_VALUE_KEY);
    }


    public boolean containsKey(String key) {
        return bucket.get(key) != null;
    }

    public SolrFacetJsonResponseBucket getBucketBasedFacets(String key) {
        SimpleOrderedMap<Object> tmpBucket = (SimpleOrderedMap<Object>) bucket.get(key);
        if (tmpBucket == null) {
            return null;
        }
        return new SolrFacetJsonResponseBucket(tmpBucket);
    }


    public List<SolrFacetJsonResponseBucket> getBuckets() {
        return buckets;
    }

    public Double getDoubleCountNumber() {
        return Double.valueOf(String.valueOf(bucket.get(BUCKET_COUNT_KEY)));
    }

    public Long getLongCountNumber() {
        final Object obj = bucket.get(BUCKET_COUNT_KEY);
        return Long.valueOf(String.valueOf(obj == null ? 0L : obj));
    }

    public Object getObjectValueByKey(String key) {
        return bucket.get(key);
    }

    public Double getDoubleValueByKey(String key) {
        final Object obj = bucket.get(key);
        return Double.valueOf(obj == null ? String.valueOf(0D) : obj.toString());
    }

    public Number getNumberValueByKey(String key) {
        return (Number) bucket.get(key);
    }

    public long getTotalNumOfBuckets() {
        Object res = bucket.get(TOTAL_NUM_OF_BUCKETS_KEY);
        return res == null ? 0 : Long.valueOf(res.toString());
    }

    public boolean isEmpty() {
        return bucket.size() == 0;
    }
}
