package com.cla.filecluster.repository.jpa;

import com.cla.common.domain.dto.category.FileCategoryType;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by uri on 23/07/2015.
 */
public interface FileCategoryRepository extends JpaRepository<FileCategory, Long>, JpaSpecificationExecutor<FileCategory> {


    @Query(value = "select fc from FileCategory fc join fetch fc.filters",countQuery = "select count(fc) from FileCategory fc")
    Page<FileCategory> findAllWithFilters(Pageable pageable);

    @Query(value = "select fc from FileCategory fc join fetch fc.filters fcf where fc.type = :fileCatType and fcf.value = :value")
    FileCategory findCategoryByTypeAndFilterValue(@Param("fileCatType") FileCategoryType fileCategoryType,@Param("value") String value);

    @Query(value = "select fc from FileCategory fc where fc.type = :fileCatType and fc.name = :name")
    FileCategory findCategoryByTypeAndName(@Param("fileCatType") FileCategoryType fileCategoryType, @Param("name") String name);
}
