package com.cla.filecluster.service.export;

import lombok.Getter;
import lombok.val;

import java.util.Optional;

// we use the string types in audit messages, if the string changes also change in message.properties
public enum ExportType {
    ROOT_FOLDERS("root_folders"),
    SCAN_HISTORY("scan_history"),
    DISCOVER_RIGHT("files"),
    DISCOVER_RIGHT_AS_FILES("as_files"),
    SENDER_DOMAIN("sender_domain"),
    SENDER_ADDRESS("sender_address"),
    RECIPIENT_DOMAIN("recipient_domain"),
    RECIPIENT_ADDRESS("recipient_address"),
    EXTRACTION_RULE("bizlist_item_consumers_extraction_rules"),
    CLIENT_ASSOCIATION("bizlist_items_clients"),
    CLIENT_EXTRACTION("bizlist_item_clients_extractions"),
    DOC_TYPE("doc_types"),
    PATTERN_CATEGORY("pattern_category"),
    DEPARTMENTS("departments"),
    MATTERS("matters"),
    DEPARTMENTS_LIST("departments_flat"),
    MATTERS_LIST("matters_flat"),
    TEXT_SEARCH_PATTERNS_SETTINGS("text_search_patterns_settings"),
    PATTERNS("patterns"),
    PATTERNS_MULTI("patterns_multi"),
    REGULATION("regulations"),
    REGULATIONS_SETTINGS("regulations_settings"),
    TAG("tags"),
    ACL_READ("acl_reads"),
    ACL_WRITE("acl_writes"),
    OWNER("owners"),
    LABELING("labeling"),
    GENERAL_METADATA("general_metadata"),
    EXTENSIONS("extensions"),
    FILE_SIZE("file_sizes"),
    FILE_TYPE_CATEGORIES("file_type_categories"),
    CONTENTS("contents"),
    ALL_FILES("all_files"),
    FOLDER_LIST("folders_flat"),
    ACCESSED_DATE("file_accessed_dates"),
    CREATION_DATE("file_created_dates"),
    MODIFIED_DATE("file_modified_dates"),
    DATA_ROLE("functional_roles"),
    MAP_ERROR("map_error"),
    INGEST_ERROR("ingest_error"),
    AUDIT("audit"),
    FILE_GROUP("groups"),
    BIZ_LIST_MONITOR("biz_list_monitor"),
    BIZ_LIST_SETTINGS("biz_list_settings"),
    DOC_TYPE_SETTINGS("doc_types_settings"),
    DEPARTMENT_SETTINGS("departments_settings"),
    TAG_TYPE_SETTINGS("tag_types_settings"),
    LDAP_CONNECTION_SETTINGS("ldap_connections"),
    SYSTEM_COMPONENTS_SETTINGS("system_components_settings"),
    SCHEDULED_GROUPS_SETTINGS("scheduled_groups_settings"),
    ACTIVE_SCANS("active_scans"),
    JOB_LIST("job_list"),
    ACTIVE_SCHEDULED_GROUPS("active_scheduled_groups"),
    SYSTEM_COMPONENTS("system_components"),
    FC_HISTORY("filecluster_component_history"),
    MP_HISTORY("mp_component_history"),
    DB_HISTORY("db_component_history"),
    BROKER_HISTORY("broker_component_history"),
    SOLR_NODE_HISTORY("solr_node_component_history"),
    SOLR_COLLECTION_HISTORY("solr_collection_component_history"),
    ZOOKEEPER_HISTORY("zookeeper_component_history"),
    MEDIA_CONNECTIONS("media_connections"),
    DATA_CENTER("customer_data_center"),
    SEARCH_PATTERNS("search_patterns"),
    USERS("users"),
    DATA_ROLE_SETTINGS("data_roles"),
    OPERATIONAL_ROLE("operational_roles"),
    ROLE_TEMPLATE("role_templates"),
    USER_VIEW_DATA("user_view_data"),
    DASHBOARD_CHART("dashboard_chart"),
    SHARED_PERMISSION_ALLOW_READ("share_permissions_allow_read"),
    DA_METADATA("metadatas"),
    DA_METADATA_TYPE("da_metadata_type"),
    ;

    @Getter
    private final String apiName;

    ExportType(String apiName) {
        this.apiName  = apiName;
    }

    public static Optional<ExportType> convertApiName(String exportTypeStr) {
        for (val exportType : ExportType.values()) {
            if (exportType.getApiName().equals(exportTypeStr)) {
                return Optional.of(exportType);
            }
        }
        return Optional.empty();
    }
}
