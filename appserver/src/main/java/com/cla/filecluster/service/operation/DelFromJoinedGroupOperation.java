package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * removes a child group from a user group
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
@Service
public class DelFromJoinedGroupOperation implements ScheduledOperation {

    public static String USER_GROUP_ID_PARAM = "USER_GROUP_ID";
    public static String CHILD_GROUP_ID_PARAM = "CHILD_GROUP_ID";

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 3;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            groupsApplicationService.removeGroupFromJoinedGroupUpdFilesDB(opData);
        } else if (step == 1) {
            groupsApplicationService.removeGroupFromJoinedGroupUpdFilesSolr(opData);
        } else if (step == 2) {
            groupsApplicationService.removeGroupFromJoinedGroupUpdParent(opData);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public List<String> getRelevantGroupIds(Map<String, String> opData) {
        List<String> result = new ArrayList<>();
        String groupId = opData.get(USER_GROUP_ID_PARAM);
        result.add(groupId);
        groupId = opData.get(CHILD_GROUP_ID_PARAM);
        result.add(groupId);
        return result;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String groupId = opData.get(USER_GROUP_ID_PARAM);
        if (Strings.isNullOrEmpty(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.empty"), BadRequestType.MISSING_FIELD);
        }
        String childGroupId = opData.get(CHILD_GROUP_ID_PARAM);
        if (Strings.isNullOrEmpty(childGroupId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.child.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.DEL_FROM_JOINED_GROUP;
    }
}
