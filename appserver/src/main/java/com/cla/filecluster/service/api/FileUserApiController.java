package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.FileUserAppService;
import com.cla.filecluster.service.security.SharePermissionAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created by itay (based on uri's code) on 01/09/2015.
 */
@RestController
@RequestMapping("/api")
public class FileUserApiController {

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private FileUserAppService fileUserAppService;

    @Autowired
    private SharePermissionAppService sharePermissionAppService;

	@Value("${reportPageSize:20}")		// TODO: allow page size config, either in request or as a user config.
	private int pageSize;

    @RequestMapping("/owner/list")
    public Page<AggregationCountItemDTO<FileUserDto>> getAllOwners(@RequestParam(defaultValue="0") final int page) {
    	PageRequest pageRequest = new PageRequest(adjustPageNumebr(page), pageSize);
        return fileUserAppService.getAllOwners(pageRequest);
    }

    @RequestMapping("/owner/groups")		// owner contains '\' so it can not be a path variable
    public Page<AggregationCountItemDTO<GroupDto>> getGroupsByOwner(@RequestParam("owner") final String owner, @RequestParam(defaultValue="0") final int page) {
    	PageRequest pageRequest = new PageRequest(adjustPageNumebr(page), pageSize);
    	return fileUserAppService.getGroupsByOwner(owner, pageRequest);
    }

    @RequestMapping("/owner/filecount")
    public FacetPage<AggregationCountItemDTO<FileUserDto>> getOwnerFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover owners export", AuditAction.VIEW,  FileUserDto.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover owners", AuditAction.VIEW,  FileUserDto.class,null, dataSourceRequest);
        }
        return fileUserAppService.getOwnerFileCounts(dataSourceRequest);
    }

    @RequestMapping("/sharepermission/allow/read/filecount")
    public FacetPage<AggregationCountItemDTO<RootFolderSharePermissionDto>> getSharePermissionFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover share permission allow read export", AuditAction.VIEW,  RootFolderSharePermissionDto.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover share permission allow read", AuditAction.VIEW,  RootFolderSharePermissionDto.class,null, dataSourceRequest);
        }
        return sharePermissionAppService.listCountWithFileFilter(dataSourceRequest, AclType.READ_TYPE);
    }

    @RequestMapping("/acl/read/filecount")
    public FacetPage<AggregationCountItemDTO<FileUserDto>> getReadAclFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover aclReads export", AuditAction.VIEW, FileUserDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover aclReads", AuditAction.VIEW, FileUserDto.class, null, dataSourceRequest);
        }
        return fileUserAppService.getAclFileCounts(AclType.READ_TYPE, dataSourceRequest);
    }

    @RequestMapping("/acl/write/filecount")
    public FacetPage<AggregationCountItemDTO<FileUserDto>> getWriteAclFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover aclWrites export", AuditAction.VIEW, FileUserDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover aclWrites", AuditAction.VIEW, FileUserDto.class, null, dataSourceRequest);
        }
        return fileUserAppService.getAclFileCounts(AclType.WRITE_TYPE,dataSourceRequest);
    }

   /* @RequestMapping("/aclread/list")
    public Page<AggregationCountItemDTO<FileUserDto>> getAllAclReadItems(@RequestParam(defaultValue="0") final int page) {
    	return getAllAclItems(page, AclType.READ_TYPE);
    }

    @RequestMapping("/aclwrite/list")
    public Page<AggregationCountItemDTO<FileUserDto>> getAllAclWriteItems(@RequestParam(defaultValue="0") final int page) {
    	return getAllAclItems(page, AclType.WRITE_TYPE);
    }

    private Page<AggregationCountItemDTO<FileUserDto>> getAllAclItems(final int page, final AclType aclType) {
    	PageRequest pageRequest = new PageRequest(adjustPageNumebr(page), pageSize);
        return fileUserAppService.getAllAclItems(aclType,pageRequest);
    }

    @RequestMapping("/aclread/groups")		// owner contains '\' so it can not be a path variable
    public Page<AggregationCountItemDTO<GroupDto>> getGroupsByAclReadItem(@RequestParam("user") final String user, @RequestParam(defaultValue="0") final int page) {
    	return getGroupsByAclItem(user, page, AclType.READ_TYPE);
    }

    @RequestMapping("/aclwrite/groups")		// owner contains '\' so it can not be a path variable
    public Page<AggregationCountItemDTO<GroupDto>> getGroupsByAclWriteItem(@RequestParam("user") final String user, @RequestParam(defaultValue="0") final int page) {
    	return getGroupsByAclItem(user, page, AclType.WRITE_TYPE);
    }*/

    /*private Page<AggregationCountItemDTO<GroupDto>> getGroupsByAclItem(final String user, final int page, final AclType aclType) {
    	PageRequest pageRequest = new PageRequest(adjustPageNumebr(page), pageSize);
    	return fileUserAppService.getGroupsByAclItem(user,aclType,pageRequest);
    }*/

	private int adjustPageNumebr(final int page) {
		return (page>0)?(page-1):page;
	}
    
}
