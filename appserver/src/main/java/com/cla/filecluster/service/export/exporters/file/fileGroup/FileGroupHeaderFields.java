package com.cla.filecluster.service.export.exporters.file.fileGroup;

public interface FileGroupHeaderFields {
    String NAME = "Name";
    String NUM_OF_FILES = "Number of files";
    String NUM_OF_FILTERED = "Number of filtered files";
    String PERCENT_OF_FILTERED = "Percentage of filtered files";
    String ASSOCIATION = "Clients association";
    String DOC_TYPES = "DocTypes";
    String DATA_ROLES = "Data roles";
    String TAGS = "Tags";
    String SAMPLE_MEMBER = "Sample member";
    String GROUP_SIZE = "Group size";
}
