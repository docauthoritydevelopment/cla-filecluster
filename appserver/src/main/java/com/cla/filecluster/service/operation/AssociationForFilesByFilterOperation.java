package com.cla.filecluster.service.operation;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.SolrQueryResponse;
import com.cla.filecluster.service.DeepPageRequest;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filter.SavedFilterService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AssociationForFilesByFilterOperation implements ScheduledOperation {

    private final static Logger logger = LoggerFactory.getLogger(AssociationForFilesByFilterOperation.class);

    public static String ASSOCIATION_PARAM = "ASSOCIATION_ENTITY";
    public static String FILTER_PARAM = "FILTER";
    public static String ACTION_PARAM = "ACTION_PARAM";
    public static String USER_PARAM = "USER_PARAM";

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private UserService userService;

    @Value("${associations.apply-filter-associations.page-size:5000}")
    private int pageSize;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            String filter = opData.get(FILTER_PARAM);
            AssociationEntity entity = AssociationsService.convertFromAssociationEntity(0L, opData.get(ASSOCIATION_PARAM), AssociationType.FILE);
            String action = opData.get(ACTION_PARAM);
            boolean addAction = action.equalsIgnoreCase("ADD");

            FilterDescriptor filterDescriptor = SavedFilterService.convertFilterDescriptorJson(filter);

            String userName = opData.get(USER_PARAM);
            AuthenticationHelper.doWithAuthentication(
                    userName == null ? AutoAuthenticate.ADMIN : userName,
                    true,
                    () -> handleFilesByFilter(filterDescriptor, entity, addAction)
                    , userService);


        } else {
            return true;
        }
        return false;
    }

    private void handleFilesByFilter(FilterDescriptor filterDescriptor, AssociationEntity entity,
                                     boolean addAction) {
        String cursorMark;
        String nextCursorMark = "*";

        do {
            cursorMark = nextCursorMark;
            PageRequest pageRequest = new DeepPageRequest(pageSize, cursorMark);
            SolrSpecification solrSpecification = buildSolrSpecification(filterDescriptor);
            SolrQueryResponse<SolrFileEntity> solrQueryResponse = fileCatService.runQuery(solrSpecification, pageRequest);
            nextCursorMark = solrQueryResponse.getCursorMark();
            List<SolrFileEntity> entities = solrQueryResponse.getEntites();
            logger.debug("got {} results with cursorMark {}", entities.size(), nextCursorMark);

            List<SolrInputDocument> docs = new ArrayList<>();
            for (SolrFileEntity file : entities) {
                SolrInputDocument doc = handleFile(file, entity, addAction);
                if (doc != null) {
                    docs.add(doc);
                }
            }

            if (!docs.isEmpty()) {
                fileCatService.saveAll(docs);
                fileCatService.softCommitNoWaitFlush();
            }

            if (entities.size() == 0) {
                nextCursorMark = cursorMark;
            }
            if (nextCursorMark == null) {
                logger.error("got a null cursor mark from Solr");
            }
        }
        while (nextCursorMark != null && !nextCursorMark.equals(cursorMark));
    }

    private SolrInputDocument handleFile(SolrFileEntity file, AssociationEntity entity, boolean addAction) {
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(file.getFileId(), file.getAssociations(), AssociationType.FILE);

        List<AssociationEntity> exists = associations.stream().filter(assoc -> assoc.similarAssociation(entity)).collect(Collectors.toList());
        if (addAction && !exists.isEmpty()) {
            return null;
        } else if (!addAction && exists.isEmpty()) {
            return null;
        }

        SolrInputDocument doc = new SolrInputDocument();
        doc.setField("id", file.getId());

        SolrFieldOp operation = addAction ? SolrFieldOp.ADD : SolrFieldOp.REMOVE;

        if (addAction && entity.getFileTagId() != null) {
            handleSingleValueTagAdd(associations, entity.getFileTagId(), file);
        }

        String association = AssociationsService.convertFromAssociationEntity(addAction ? entity : exists.get(0));

        SolrFileCatRepository.setField(doc, CatFileFieldType.FILE_ASSOCIATIONS, SolrFileCatRepository.atomicConditionalSetValue(true, association, operation));
        SolrFileCatRepository.setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));

        SolrFileCatRepository.setField(doc, CatFileFieldType.TAGS_2, file.getTags2());
        SolrFileCatRepository.setField(doc, CatFileFieldType.TAGS, file.getTags());
        SolrFileCatRepository.setField(doc, CatFileFieldType.SCOPED_TAGS, file.getScopedTags());
        SolrFileCatRepository.setField(doc, CatFileFieldType.DOC_TYPES, file.getDocTypes());
        SolrFileCatRepository.setField(doc, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, file.getDataRoles());
        SolrFileCatRepository.setField(doc, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, file.getBizListItems());
        SolrFileCatRepository.setField(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS, file.getActiveAssociations());


        SolrFolderEntity folder = new SolrFolderEntity();
        folder.setId(file.getFolderId());
        associationsService.addTagDataToDocument(doc, file.getFileId(), Lists.newArrayList(association),
                null, null, true, folder, operation);

        return doc;
    }

    private void handleSingleValueTagAdd(List<AssociationEntity> associations, Long tagId, SolrFileEntity file) {
        FileTagDto tagDto = fileTagService.getFromCache(tagId);
        if (tagDto.getType().isSingleValueTag()) {
            for (AssociationEntity association : associations) {
                if (association.getFileTagId() != null) {
                    FileTagDto assocTagDto = fileTagService.getFromCache(association.getFileTagId());
                    if (assocTagDto.getType().getId() == tagDto.getType().getId()) {
                        associationsService.removeAssociationFromFile(
                                file, assocTagDto, association.getAssociationScopeId(), SolrCommitType.NONE);
                    }
                }
            }
        }
    }

    private SolrSpecification buildSolrSpecification(FilterDescriptor filterDescriptor) {
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(filterDescriptor);
        solrSpecification.setAdditionalFilter(CatFileFieldType.DELETED.filter("false"));
        solrSpecification.setSortField(CatFileFieldType.ID.getSolrName());
        solrSpecification.setSortOrder(SolrQuery.ORDER.asc);
        return solrSpecification;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String filter = opData.get(FILTER_PARAM);
        if (Strings.isNullOrEmpty(filter)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.filter.empty"), BadRequestType.MISSING_FIELD);
        }

        try {
            SavedFilterService.convertFilterDescriptorJson(filter);
        } catch (Exception e) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.filter.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        String json = opData.get(ASSOCIATION_PARAM);
        if (Strings.isNullOrEmpty(json)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.empty"), BadRequestType.MISSING_FIELD);
        }

        AssociationEntity entity = AssociationsService.convertFromAssociationEntity(0L, json, AssociationType.FILE);
        if (entity == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        List<AssociableEntityDto> entities = associationsService.getEntitiesForAssociations(Lists.newArrayList(entity));
        if (entities == null || entities.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        String action = opData.get(ACTION_PARAM);
        if (Strings.isNullOrEmpty(action)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.action.empty"), BadRequestType.MISSING_FIELD);
        }
        if (!action.equalsIgnoreCase("ADD") && !action.equalsIgnoreCase("REMOVE")) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.action.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.ASSOCIATION_FOR_FILES_BY_FILTER;
    }

    @Override
    public boolean shouldAllowCreateWhileScheduleDown() {
        return true;
    }
}
