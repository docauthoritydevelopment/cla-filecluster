package com.cla.filecluster.service.communication;

import com.cla.common.domain.dto.components.amq.QueueProperty;
import com.cla.common.utils.FileSizeUnits;
import com.cla.filecluster.utils.EncUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

/**
 * Created by uri on 27-Jun-17.
 */
@Service
public class ActiveMqControlService {

    @Autowired
    JmsTemplate jmsTemplate;

    @Value("${spring.activemq.broker-url}")
    private String activeMqBrokerUrl;

    @Value("${control.responses.topic:}")
    private String controlResponseTopic;

    @Value("${scan.responses.topic:}")
    private String scanResponsesTopic;

    @Value("${activemq.monitor-queue-status-url:read/org.apache.activemq:type=Broker,brokerName=localhost,destinationType=Queue,destinationName=}")
    private String activemqStatusUrl;

    @Value("${activemq.monitor-queue-url:http://localhost:8161}")
    private String activemqMonUrlHostPort;

    @Value("${activemq.monitor-queue-url-path:/api/jolokia/}")
    private String activemqMonUrlPathPrefix;

    private String activemqUrlPrefix;

    @Value("${activemq.monitor-queue-names-url:search/*:destinationType=Queue,*}")
    private String activemqQueueUrl;

    @Value("${activemq.monitor-queue-user:admin}")
    private String activemqUser;

    @Value("${activemq.monitor-queue-pass:admin}")
    private String activemqPass;

    @Value("${activemq.monitor-queue-attributes:/QueueSize,EnqueueCount,ConsumerCount,ExpiredCount,DequeueCount,AverageEnqueueTime,MinEnqueueTime,MaxEnqueueTime}")
    private String activemqAttributes;

    @Value("${activemq.monitor-queue-names-ignore:ActiveMQ.DLQ}")
    private String[] activemqQueueNamesToIgnore;

    @Value("${use-enc-pass:false}")
    private boolean useEncPassword;

    @Value("${activemq.monitor-queue-attr-max-size:2048}")
    private int maxAttrSize;

    private static final Boolean NON_TRANSACTED = false;

    private ObjectMapper mapper;

    private static final Logger logger = LoggerFactory.getLogger(ActiveMqControlService.class);

    @PostConstruct
    public void init() {
        mapper = new ObjectMapper();
        mapper.configure(com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        activemqUrlPrefix = activemqMonUrlHostPort + activemqMonUrlPathPrefix;
        logger.info("ActiveMQ monitor prefix: {}", activemqMonUrlPathPrefix);
    }

    public List<String> getQueueNames() {
        List<String> queueNamesToIgnore = Arrays.asList(activemqQueueNamesToIgnore);
        List<String> queueNames = new ArrayList<>();
        try {
            URL url = new URL(activemqUrlPrefix+activemqQueueUrl);
            URLConnection uc = url.openConnection();
            String userpass = activemqUser + ":" + (useEncPassword ? EncUtils.decrypt(activemqPass) : activemqPass);
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
            uc.setRequestProperty("Authorization", basicAuth);
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    uc.getInputStream()));
            String inputLine;
            StringBuilder result = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                result.append(inputLine);
            }
            in.close();
            QueueNameResponse resp = mapper.readValue(result.toString(), QueueNameResponse.class);
            if (resp != null && resp.getValue() != null && resp.getValue().length > 0) {
                for (String value : resp.getValue()) {
                    int start = value.indexOf("destinationName=") + "destinationName=".length();
                    int end = value.indexOf(",",start);
                    if (start > 0) {
                        String name = value.substring(start,end);
                        if (!queueNamesToIgnore.contains(name)) {
                            queueNames.add(name);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("failed getting active MQ queue names", e);
        }
        return queueNames;
    }

    private int calcStringSizeForQueue(int maxStringSize, List<String> attr) {
        int sizeForQueue = (15) + // 15 is avg queue name size
                (attr.size() * (5 + 6)); // 5 is avg attr value size and 6 is the attr name and extra fillings ("x19":value)
        return maxStringSize/sizeForQueue;
    }

    public Map<String, String> getActiveMQQueueStatus() throws Exception {
        Map<String, Map<String, Object>> queuesStatusByDc = new HashMap<>();
        List<String> attr = Arrays.asList(activemqAttributes.split(","));

        try {
            List<String> queueNames = getQueueNames();

            // get max queues to audit
            int maxQueuesPerStatus = calcStringSizeForQueue(maxAttrSize, attr);

            for (String name : queueNames) {
                URL url = new URL(activemqUrlPrefix + activemqStatusUrl + name + activemqAttributes);
                URLConnection uc = url.openConnection();
                String userpass = activemqUser + ":" + (useEncPassword ? EncUtils.decrypt(activemqPass) : activemqPass);
                String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userpass.getBytes()));
                uc.setRequestProperty("Authorization", basicAuth);
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        uc.getInputStream()));
                String inputLine;
                StringBuilder result = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    result.append(inputLine);
                }
                in.close();
                try {
                    QueueStatusResponse resp = mapper.readValue(result.toString(), QueueStatusResponse.class);
                    String queueName = name.replaceAll("\\.","_");
                    Map<String, Object> data = (Map<String, Object>)resp.getValue();
                    Map<String, Object> queueStatus = new HashMap<>();

                    for (Map.Entry<String, Object> entry : data.entrySet()) {
                        String key = entry.getKey();
                        Object value = entry.getValue();

                        try {
                            // get property so we can get the initials
                            QueueProperty prop = QueueProperty.valueOf(key);

                            // if value is double - trim
                            if (value instanceof Double) {
                                if (prop.isDivideForKilo()) {
                                    value = FileSizeUnits.BYTE.toKilobytes((Double) value, FileSizeUnits.ROUND,2);
                                } else {
                                    value = new BigDecimal((Double) value).setScale(2, BigDecimal.ROUND_HALF_UP);
                                }
                            }
                            else if (prop.isDivideForKilo()) {
                                if (value instanceof Long) {
                                    value = FileSizeUnits.BYTE.toKilobytes((Long) value, FileSizeUnits.ROUND,2);
                                } else if (value instanceof Integer) {
                                    value = FileSizeUnits.BYTE.toKilobytes((Integer) value, FileSizeUnits.ROUND,2);
                                }
                            }

                            queueStatus.put(prop.getInitials(), value);
                        } catch (Exception e) {
                            logger.info("problem setting queue property value entry {} queue {}",entry,queueName);
                        }
                    }

                    String dc = "0";
                    if (name.matches("[0-9]+_.*")) {
                        int end = name.indexOf("_");
                        dc = name.substring(0, end);
                        queueStatus.put(QueueProperty.DataCenterId.getInitials(), dc);
                    }

                    Map<String, Object> queues = null;
                    queues = queuesStatusByDc.get(dc);
                    if (queues == null) {
                        queues = new HashMap<>();
                    }

                    if (queues.size() >= maxQueuesPerStatus) {
                        logger.warn("removed queue {} from active mq status as result too long", name);
                    } else {
                        queues.put(queueName, queueStatus);
                        queuesStatusByDc.put(dc, queues);
                    }
                } catch (Exception e) {
                    logger.error("failed parsing active MQ status for queue {} result was {}", name, result.toString(), e);
                }
            }
        } catch (Exception e) {
            logger.error("failed getting active MQ status", e);
            throw new RuntimeException(e.getMessage(), e);
        }

        Map<String, String> result = queuesStatusByDc.isEmpty() ? null : new HashMap<>();
        queuesStatusByDc.forEach(
                (k, v) -> {
                    try {
                        result.put(k, mapper.writeValueAsString(v));
                    } catch (Exception e) {
                        logger.error("failed writing active MQ status for queue {} and val {}", k, v, e);
                    }
                }
        );
        logger.trace("get active mq status returned {}", result);
        return result;
    }

    public boolean getBrokerState() {
//        String queueName = "ActiveMQ.Statistics.Broker";
////        javax.jms.Message response = jmsTemplate.sendAndReceive(queueName, new MessageCreator() {
////            @Override
////            public javax.jms.Message createMessage(Session session) throws JMSException {
////                TextMessage message = session.createTextMessage("");
////                //set ReplyTo header of Message, pretty much like the concept of email.
////                message.setJMSReplyTo(new ActiveMQQueue(controlResponseTopic));
////                return message;
////            }
////        });
//        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("admin", "password", activeMqBrokerUrl);
//        Connection connection = null;
//
//        try {
//
//            connection = connectionFactory.createConnection();
//            connection.start();
//
//            Session session = connection.createSession(NON_TRANSACTED, Session.AUTO_ACKNOWLEDGE);
//            Destination destination = session.createQueue("test-queue");
//            TemporaryQueue replyTo = session.createTemporaryQueue();
//            MessageConsumer consumer = session.createConsumer(replyTo);
//            javax.jms.Queue testQueue = session.createQueue(queueName);
//            MessageProducer producer = session.createProducer(testQueue);
//            javax.jms.Message msg = session.createMessage();
//            msg.setJMSReplyTo(replyTo);
//            producer.send(msg);
//            MapMessage reply = (MapMessage) consumer.receive();
//            // assertNotNull(reply);
//
//            for (Enumeration e = reply.getMapNames(); e.hasMoreElements();) {
//                String name = e.nextElement().toString();
//                System.out.println(name + "=" + reply.getObject(name));
//            }
//
//            producer.close();
//            session.close();
//        } catch (JMSException e) {
//            e.printStackTrace();
//        }
//        finally {
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (JMSException e) {
//                    System.out.println("Could not close an open connection...");
//                }
//            }
//        }


   //     String queueName = "ActiveMQ.Statistics.Broker";



       // System.out.println(response);
         return true;
    }

    public void getMetrics() {
        //TODO 2.0
    }

    public String getBrokerAddress() {
        return activeMqBrokerUrl;
    }

    public void getTopicStats()
    {
//        String queueName = "ActiveMQ.Statistics.Destination." + scanResponsesTopic;
//        javax.jms.Message response = jmsTemplate.sendAndReceive(queueName, new MessageCreator() {
//            @Override
//            public javax.jms.Message createMessage(Session session) throws JMSException {
//                TextMessage message = session.createTextMessage("");
//                //set ReplyTo header of Message, pretty much like the concept of email.
//                message.setJMSReplyTo(new ActiveMQQueue(controlResponseTopic));
//                return message;
//            }
//        });
//        System.out.println(response);
//        Queue replyTo = session.createTemporaryQueue();
//        MessageConsumer consumer = session.createConsumer(replyTo);
//
//        Queue testQueue = session.createQueue("TEST.FOO");
//        MessageProducer producer = session.createProducer(null);
//
//        String queueName = "ActiveMQ.Statistics.Destination." + testQueue.getQueueName()
//        Queue query = session.createQueue(queueName);
//
//        Message msg = session.createMessage();
//
//        producer.send(testQueue, msg)
//        msg.setJMSReplyTo(replyTo);
//        producer.send(query, msg);
//        MapMessage reply = (MapMessage) consumer.receive();
//        assertNotNull(reply);
//        assertTrue(reply.getMapNames().hasMoreElements());

//        for (Enumeration e = reply.getMapNames();e.hasMoreElements();) {
//            String name = e.nextElement().toString();
//            System.err.println(name + "=" + reply.getObject(name));
//        }
    }
}
