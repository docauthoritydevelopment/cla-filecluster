package com.cla.filecluster.service.export.exporters.scan_history;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.scan_history.mixin.CrawlRunDetailsDtoMixin;
import com.cla.filecluster.service.export.exporters.scan_history.mixin.RootFolderDtoMixin;
import com.cla.filecluster.util.ControllerUtils;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.scan_history.ScanHistoryHeaderFields.*;

@Slf4j
@Component
public class ScanHistoryExporter extends PageCsvExcelExporter<CrawlRunDetailsDto> {

    @Autowired
    private JobManagerAppService jobManagerAppService;

    private final static String[] header = {ID, NICKNAME, PATH,
            STATE, TRIGGER, SCHEDULED_GROUP, START_TIME, END_TIME};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.SCAN_HISTORY);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(CrawlRunDetailsDto.class, CrawlRunDetailsDtoMixin.class)
                .addMixIn(RootFolderDto.class, RootFolderDtoMixin.class);
    }

    @Override
    protected Page<CrawlRunDetailsDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        List<RunStatus> filterByRunStatus = ControllerUtils.getStatesList(requestParams);

        if (filterByRunStatus != null) {
            return jobManagerAppService.listRuns(dataSourceRequest, filterByRunStatus);
        } else {
            return jobManagerAppService.listRuns(dataSourceRequest);
        }
    }

    @Override
    protected Map<String, Object> addExtraFields(CrawlRunDetailsDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        String trigger = base.isManual() ? "Manual" : "System";
        result.put(TRIGGER, trigger);

        if (base.getRunOutcomeState() != null) {
            String state = FileCrawlerExecutionDetailsService.simplifyRunStatus(
                    base.getRunOutcomeState(), base.getPauseReason(), base.getGracefulStop());
            state = WordUtils.capitalizeFully(state);
            result.put(STATE, state);
        }

        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
