package com.cla.filecluster.service.extractor.pattern;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.common.utils.PatternSearcher;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.searchpatterns.PatternCategory;
import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.repository.file.TextSearchPatternImplementationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.PatternCategoryRepository;
import com.cla.filecluster.repository.jpa.searchpattern.RegulationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Component
public class ClaFileSearchPatternService {

	private static final Logger logger = LoggerFactory.getLogger(ClaFileSearchPatternService.class);
	
	@Autowired
	private TextSearchPatternImplementationRepository textSearchPatternImplementationRepository;

    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Value("${no-database:false}")
    private boolean noDatabase;

    @Value("${ingester.patternSearchActive:true}")
    private boolean patternSearchActive;

    @Value("${ingester.pattern-search.ignored-values-list:}")
    private String[] ignoreValuesList;

    @Value("${ingester.pattern-search.count-distinct-matches:true}")
    private boolean countDistinctMatches;

    private boolean searchPatternsImplementationsRefreshed = false;
    private PatternSearcher patternSearcher;

    @Autowired
    private PatternCategoryRepository patternCategoryRepository;

    @Autowired
    private RegulationRepository regulationRepository;

    @Autowired
    private MessageHandler messageHandler;

    @PostConstruct
    public void init() {
        patternSearcher = new PatternSearcher(patternSearchActive, countDistinctMatches, ignoreValuesList);
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText){
        Collection<TextSearchPatternImpl> allFileSearchPatternImpls = getAllFileSearchPatternImpls();
        return patternSearcher.getSearchPatternsCounting(fileText, false, allFileSearchPatternImpls);
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting(String fileText, boolean withMatchingTerms){
        Collection<TextSearchPatternImpl> allFileSearchPatternImpls = getAllFileSearchPatternImpls();
        return patternSearcher.getSearchPatternsCounting(fileText, withMatchingTerms, allFileSearchPatternImpls);
	}

    public boolean isPatternSearchActive() {
        return patternSearchActive;
    }

    public Collection<TextSearchPatternImpl> getAllFileSearchPatternImpls() {
        return textSearchPatternImplementationRepository.findAll();
    }

    @PostConstruct
    public void reloadFileSearchPatternsImplementations() {
        reloadFileSearchPatternsImplementations(false);
    }

    void reloadFileSearchPatternsImplementations(boolean onlyIfNeeded) {
        if ((onlyIfNeeded && searchPatternsImplementationsRefreshed) || noDatabase) {
            return;
        }
        logger.debug("Reload file Search patterns");
        List<TextSearchPattern> textSearchPatterns = textSearchPatternRepository.findAll();
        List<TextSearchPatternDto> textSearchPatternDtos = convertTextSearchPatternsToDto(textSearchPatterns);
        textSearchPatternImplementationRepository.init(textSearchPatternDtos);
        searchPatternsImplementationsRefreshed = true;
        mediaProcessorCommService.sendPatternControlRequest(getAllFileSearchPatternImpls());
        logger.debug("File Search patterns loaded");
	}

	public List<TextSearchPattern> getAllFileSearchPatterns() {
		return textSearchPatternRepository.findAll();
	}

    public Set<String> getAllMatchingStrings(final String input,  final int limit){
        if (!patternSearchActive) {
            return new HashSet<>(0);
        }
        Collection<TextSearchPatternImpl> allFileSearchPatternImpls = getAllFileSearchPatternImpls();

        // use only active patterns
        allFileSearchPatternImpls = allFileSearchPatternImpls.stream()
                .filter(t -> t.isActive() && t.isSubCategoryActive())
                .collect(Collectors.toList());

        return patternSearcher.getAllMatchingStrings(input, limit, allFileSearchPatternImpls);
    }

    Page<TextSearchPattern> listSearchPatterns(PageRequest pageRequest, String namingSearchTerm, List<Integer> regulationIds) {
        logger.debug("List all search patterns");
        if (namingSearchTerm != null) {
            if (regulationIds!= null) {
                return textSearchPatternRepository.findAllWithFiltersAndSearchAndRegulations(pageRequest, namingSearchTerm.toLowerCase(),regulationIds);
            }
            else {
                return textSearchPatternRepository.findAllWithFiltersAndSearch(pageRequest, namingSearchTerm.toLowerCase());
            }
        }
        else {
            if (regulationIds!= null) {
                return textSearchPatternRepository.findAllWithFiltersAndRegulations(pageRequest,regulationIds);
            }
            else {
                return textSearchPatternRepository.findAllWithFilters(pageRequest);
            }
        }
    }

//    public TextSearchPatternDto createFromPatternClaFileSearchPattern(final Object key, String pattern) {
//        final String[] split = key.toString().split("\\.");
//        Integer id = Integer.valueOf(split[0]);
//        String name = split[1];
//        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto(id, name, pattern);
//        textSearchPatternDto.setPatternType(PatternType.REGEX);
//        return textSearchPatternDto;
//    }

//    public TextSearchPatternDto createFromClassClaFileSearchPattern(final Object key,String value) {
//        try {
//            final String[] split = key.toString().split("\\.");
//
//            final String packageName = TextSearchPatternDto.class.getPackage().getName();
//
//            Integer id = Integer.valueOf(split[0]);
//            String name = split[1];
//            TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto(id, name,"");
//            textSearchPatternDto.setPatternType(PatternType.PREDEFINED);
//            textSearchPatternDto.setValidatorClass( packageName + "."+ value);
//            textSearchPatternDto.setActive(true);
//            return textSearchPatternDto;
//
////            return (TextSearchPatternDto) Class.forName(packageName + "." + value)
////                    .getConstructor(Integer.class, String.class).newInstance(Integer.valueOf(split[0]), split[1]);
//        } catch (final Exception e) {
//            throw new RuntimeException("Could not create ClaFilePattern class " + key.toString(), e);
//        }
//    }


    @Transactional
    TextSearchPattern createSearchPattern(TextSearchPatternDto textSearchPatternDto) {
        validateSearchPattern(textSearchPatternDto);
        searchPatternsImplementationsRefreshed = false;
        logger.debug("Create Search pattern {}", textSearchPatternDto);
        TextSearchPattern textSearchPattern = new TextSearchPattern();
        convertTextSearchPatternDtoToTextSearchPattern(textSearchPattern,textSearchPatternDto);
        TextSearchPattern pattern = textSearchPatternRepository.save(textSearchPattern);
        textSearchPatternImplementationRepository.addTextSearchPatternToCache(convertTextSearchPatternToDto(pattern));
        updateMP(pattern.getId());
        return pattern;
    }

    private void updateMP(Integer id) {
        updateMP(textSearchPatternImplementationRepository.findById(id));
    }

    public void patternCategoryStateUpdate(Integer patternCategoryId, boolean state) {
        List<TextSearchPattern> relatedPatterns = textSearchPatternRepository.findBySubCategoryId(patternCategoryId);
        if (relatedPatterns != null && !relatedPatterns.isEmpty()) {
            relatedPatterns.forEach(pattern -> {
                TextSearchPatternImpl searchPatternCached = textSearchPatternImplementationRepository.findById(pattern.getId());
                if (searchPatternCached == null) {
                    TextSearchPatternDto textSearchPatternDto = convertTextSearchPatternToDto(pattern);
                    textSearchPatternDto.setSubCategoryActive(state);
                    textSearchPatternImplementationRepository.addTextSearchPatternToCache(textSearchPatternDto);
                } else {
                    searchPatternCached.setSubCategoryActive(state);
                }
                updateMP(pattern.getId());
            });
        }
    }

    private void updateMP(TextSearchPatternImpl newPattern) {
        List<TextSearchPatternImpl> newPatternList = Lists.newArrayList(newPattern);
        mediaProcessorCommService.sendPatternControlRequest(newPatternList);
    }

    private void validateSearchPattern(TextSearchPatternDto textSearchPatternDto) {
        if (textSearchPatternDto == null) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.empty-value"),BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(textSearchPatternDto.getName()) || textSearchPatternDto.getName().trim().isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.name.mandatory"),BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(textSearchPatternDto.getPattern()) || textSearchPatternDto.getPattern().trim().isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.pattern.mandatory"),BadRequestType.MISSING_FIELD);
        }

        if (textSearchPatternDto.getPatternType() == null) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.pattern-type.mandatory"),BadRequestType.MISSING_FIELD);
        }

        TextSearchPattern TextSearchPattern = textSearchPatternRepository.findByName(textSearchPatternDto.getName());
        if (TextSearchPattern != null && !TextSearchPattern.getId().equals(textSearchPatternDto.getId())) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.name.already-exist",
                    Collections.singletonList("'"+textSearchPatternDto.getName()+"'")),BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    private void convertTextSearchPatternDtoToTextSearchPattern(TextSearchPattern targetTextSearchPattern, TextSearchPatternDto textSearchPatternDto) {
        targetTextSearchPattern.setActive(textSearchPatternDto.isActive());
        targetTextSearchPattern.setDescription(textSearchPatternDto.getDescription());
        targetTextSearchPattern.setName(textSearchPatternDto.getName());
        targetTextSearchPattern.setPattern(textSearchPatternDto.getPattern());
        targetTextSearchPattern.setPatternType(textSearchPatternDto.getPatternType());
        targetTextSearchPattern.setValidatorClass(textSearchPatternDto.getValidatorClass());
        Set<PatternCategory> categories = new HashSet<>();
        categories.add(patternCategoryRepository.findById(textSearchPatternDto.getSubCategoryId()).orElse(null));
        targetTextSearchPattern.setSubCategories(categories);
        Set<Regulation> regulations = new HashSet<>();
        if (textSearchPatternDto.getRegulationIds() != null) {
            textSearchPatternDto.getRegulationIds().forEach((Integer regId) -> regulations.add(regulationRepository.findById(regId).orElse(null)));
        }
        targetTextSearchPattern.setRegulations(regulations);
    }

    @Transactional
    TextSearchPattern updateSearchPattern(TextSearchPatternDto textSearchPatternDto) {
        validateSearchPattern(textSearchPatternDto);
        logger.debug("Update search pattern to {}",textSearchPatternDto);
        TextSearchPattern textSearchPattern = textSearchPatternRepository.getOne(textSearchPatternDto.getId());
        convertTextSearchPatternDtoToTextSearchPattern(textSearchPattern,textSearchPatternDto);
        TextSearchPattern pattern = textSearchPatternRepository.save(textSearchPattern);
        textSearchPatternImplementationRepository.addTextSearchPatternToCache(convertTextSearchPatternToDto(pattern));
        updateMP(pattern.getId());
        return pattern;
    }

    @Transactional
    void deleteSearchPattern(int id) {
        textSearchPatternRepository.deleteById(id);

        TextSearchPatternImpl val = textSearchPatternImplementationRepository.findById(id);
        if (val != null) {
            if (val.isActive()) {
                val.setActive(false);
                updateMP(val);
            }
            textSearchPatternImplementationRepository.deleteSearchPatternStateFromCache(id);
        }
    }

    @Transactional
    TextSearchPattern updateSearchPatternState(int id, boolean state) {
        Optional<TextSearchPattern> textSearchPatternOptional = textSearchPatternRepository.findById(id);
        if (!textSearchPatternOptional.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        TextSearchPattern textSearchPattern = textSearchPatternOptional.get();
        textSearchPattern.setActive(state);
        TextSearchPattern pattern =  textSearchPatternRepository.save(textSearchPattern);
        textSearchPatternImplementationRepository.addTextSearchPatternToCache(convertTextSearchPatternToDto(textSearchPattern));

        updateMP(pattern.getId());

        return pattern;
    }

    @Transactional(readOnly = true)
    public List<TextSearchPattern> getSearchPatternByIds(Collection<Integer> patternsIds) {
        if (patternsIds != null && !patternsIds.isEmpty()) {
            return textSearchPatternRepository.findByIds(patternsIds);
        } else {
            return new ArrayList<>();
        }
    }

    public static List<TextSearchPatternDto> convertTextSearchPatternsToDto(List<TextSearchPattern> textSearchPatterns) {
        List<TextSearchPatternDto> result = new ArrayList<>();
        for (TextSearchPattern textSearchPattern : textSearchPatterns) {
            TextSearchPatternDto textSearchPatternDto = convertTextSearchPatternToDto(textSearchPattern);
            result.add(textSearchPatternDto);
        }
        return result;
    }

    public static TextSearchPatternDto convertTextSearchPatternToDto(TextSearchPattern textSearchPattern) {
        TextSearchPatternDto textSearchPatternDto =
                new TextSearchPatternDto(textSearchPattern.getId(), textSearchPattern.getName(), textSearchPattern.getPattern(), true);
        textSearchPatternDto.setPatternType(textSearchPattern.getPatternType());
        textSearchPatternDto.setActive(textSearchPattern.isActive());
        textSearchPatternDto.setDescription(textSearchPattern.getDescription());
        textSearchPatternDto.setValidatorClass(textSearchPattern.getValidatorClass());
        if (textSearchPattern.getSubCategories() != null && !textSearchPattern.getSubCategories().isEmpty()) {
            PatternCategory pSubCategoty = textSearchPattern.getSubCategories().iterator().next();
            if (pSubCategoty != null) {
                textSearchPatternDto.setSubCategoryId(pSubCategoty.getId());
                textSearchPatternDto.setSubCategoryName(pSubCategoty.getName());
                textSearchPatternDto.setSubCategoryActive(pSubCategoty.isActive());
                if (pSubCategoty.getParent() != null) {
                    PatternCategory pCategoty = pSubCategoty.getParent();
                    textSearchPatternDto.setCategoryId(pCategoty.getId());
                    textSearchPatternDto.setCategoryName(pCategoty.getName());
                }
            }
        }

        if (textSearchPattern.getRegulations() != null) {
            List<Regulation> regList = textSearchPattern.getRegulations().stream()
                    .sorted((r1, r2) -> {
                        String regName1 = r1.getName().toUpperCase();
                        String regName2 = r2.getName().toUpperCase();
                        return regName1.compareTo(regName2);
                    })
                    .collect(toList());
            textSearchPatternDto.setRegulationIds(regList.stream()
                    .map(Regulation::getId).collect(Collectors.toList()));
            textSearchPatternDto.setRegulationStr(StringUtils.join(regList.stream()
                    .map(Regulation::getName).collect(Collectors.toList()),","));
        }
        return textSearchPatternDto;
    }

    public static Page<TextSearchPatternDto> convertTextSearchPatternsToDto(Page<TextSearchPattern> searchPatterns) {
        List<TextSearchPatternDto> content = convertTextSearchPatternsToDto(searchPatterns.getContent());
        Pageable pageable = PageRequest.of(searchPatterns.getNumber(), searchPatterns.getSize());
        return new PageImpl<>(content, pageable, searchPatterns.getTotalElements());
    }

    public static SearchPatternCountDto convertTextSearchPatternToSearchPatternCountDto(TextSearchPattern textSearchPattern, Integer repeatCount,int multiThreshold) {
        TextSearchPatternDto textSearchPatternDto = convertTextSearchPatternToDto(textSearchPattern);
        SearchPatternCountDto searchPatternCountDto =
                new SearchPatternCountDto(textSearchPatternDto.getId(), textSearchPatternDto.getName(),repeatCount,null);
        searchPatternCountDto.setMulti(repeatCount>= multiThreshold);
        return searchPatternCountDto;
    }

    @Transactional(readOnly = true)
    public List<TextSearchPattern> getByPartialName(String patternNamePart) {
        return textSearchPatternRepository.findByNameLike(patternNamePart);
    }
}
