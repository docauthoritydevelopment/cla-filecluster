package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.google.common.collect.Table;

import java.util.Collection;

/**
 * Task producer for processing stage (e.g. scan, ingest) <br/>
 * Responsible for: <br/>
 * 1. finding pending tasks <br/>
 * 2. acquiring them in limited portions and with correct distribution (e.g. between jobs, in order to prevent starvation)<br/>
 * <br/>
 * Created by vladi on 3/9/2017.
 */
public abstract class TaskProducingStrategy {

    protected TaskProducingStrategyConfig config;
    protected JobManagerService jobManager;
    protected SolrTaskService solrTaskService;

    TaskProducingStrategy(JobManagerService jobManager, SolrTaskService solrTaskService, TaskProducingStrategyConfig config) {
        this.jobManager = jobManager;
        this.solrTaskService = solrTaskService;
        this.config = config;
    }

    public abstract Collection<SimpleTask> nextTaskGroup(TaskType taskType, Table<Long, TaskState, Number> taskStatesTable, Integer tasksQueueLimit);

    public void setConfig(TaskProducingStrategyConfig config) {
        this.config = config;
    }

    public TaskProducingStrategyConfig getConfig() {
        return config;
    }

    public boolean shouldJobTimeout(JobType type) {
        return true;
    }

    int getQueueLimit(TaskType taskType){
        int tasksQueueLimit = config.getIngestTasksQueueLimit();
        if (TaskType.ANALYZE_CONTENT == taskType){
            tasksQueueLimit = config.getAnalyzeTasksQueueLimit();
        }
        return tasksQueueLimit;
    }

}
