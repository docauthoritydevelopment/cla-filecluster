package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.common.domain.dto.action.DispatchActionRequestDto;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.DocAuthorityActionClassExecutor;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import com.cla.filecluster.domain.entity.actions.FileActionDispatch;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.repository.solr.SolrQueryResponse;
import com.cla.filecluster.service.DeepPageRequest;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.nio.file.InvalidPathException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ActionExecutorService {

    private Logger logger = LoggerFactory.getLogger(ActionExecutorService.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private UserService userService;

    @Autowired
    private ActionsAppService actionsAppService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private FileActionsEngineService fileActionsEngineService;

    @Autowired
    private ActionClassExecutorService actionClassExecutorService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${action.execution.defaultLogPath:./temp/execution/log}")
    private String defaultLogPath;

    private Future<Void> actionExecutionRunStatus = null;

    public void dispatchAndStartActionExecution(boolean wait, FilterDescriptor filter, Properties actionParams,
                                                ActionsAppService.ActionQueueingResult actionQueueingResult, Long userId) {
        logger.info("Start action execution on trigger {}", actionQueueingResult.actionTriggerId);

        if (actionExecutionRunStatus != null && !actionExecutionRunStatus.isDone()) {
            throw new RuntimeException(messageHandler.getMessage("action.single"));
        }
        if (Boolean.TRUE.equals(wait)) {
            logger.info("action execution engine is started in local thread");
            fileActionsEngineService.prepareEngine();
            dispatchAndStartActionExecution(filter, actionParams, actionQueueingResult, userId);
        } else {
            logger.info("action execution engine is started in a background process");
            actionExecutionRunStatus = Executors.newSingleThreadExecutor().submit(() -> {
                fileActionsEngineService.prepareEngine();
                dispatchAndStartActionExecution(filter, actionParams, actionQueueingResult, userId);
                return null;
            });
        }
    }

    private void dispatchAndStartActionExecution(FilterDescriptor filter, Properties actionParams,
                                                ActionsAppService.ActionQueueingResult actionQueueingResult, Long userId) {
        Long fromTime = ActionsAppService.getDifferentialScriptFromTime(actionParams);

        String cursorMark;
        String nextCursorMark = "*";

        User user = userId == null ? null : userService.getUserById(userId);
        String userName = user == null ? null : user.getUsername();
        int page = 0;

        do {
            cursorMark = nextCursorMark;
            PageRequest pageRequest = new DeepPageRequest(1000, cursorMark);
            SolrSpecification solrSpecification = actionsAppService.buildSolrSpecification(fromTime, filter);
            SolrQueryResponse<SolrFileEntity> solrQueryResponse =
                    AuthenticationHelper.doWithAuthentication(
                            userName != null ? userName : AutoAuthenticate.ADMIN, true,
                            () -> fileCatService.runQuery(solrSpecification, pageRequest), userService);
            nextCursorMark = solrQueryResponse.getCursorMark();
            List<SolrFileEntity> entities = solrQueryResponse.getEntites();
            logger.debug("Policy engine: got {} results with cursorMark {}", entities.size(), nextCursorMark);
            if (entities.size() == 0) {
                logger.warn("Got 0 results - stopping");
                nextCursorMark = cursorMark;
            } else {
                List<FileActionDispatch> newActions = dispatchActionsOnSolrDocumentList(
                        entities, actionQueueingResult.actionDefinition, actionParams, actionQueueingResult.actionTriggerId);
                page = handleActions(actionQueueingResult.actionTriggerId, newActions, page);
            }
            if (nextCursorMark == null) {
                logger.error("got a null cursor mark from Solr");
            }
        }
        while (nextCursorMark != null && !nextCursorMark.equals(cursorMark));
    }

    private int handleActions(long triggerId, List<FileActionDispatch> newActions, int page) {
        Map<ActionClass, List<FileActionDispatch>> map = newActions.stream().
                collect(Collectors.groupingBy(FileActionDispatch::getActionClass));
        ActionHelper actionHelper = new ActionHelperImplementation(actionsAppService);
        ActionExecutionTaskStatusDto actionExecutionTaskStatusDto = actionsAppService.setActionTriggerRunning(triggerId);
        actionExecutionTaskStatusDto.setLogPath(defaultLogPath);
        actionExecutionTaskStatusDto.setPage(page);
        try {
            for (ActionClass actionClass : map.keySet()) {
                DocAuthorityActionClassExecutor actionClassExecutorInstance
                        = actionClassExecutorService.createActionClassExecutorInstance(actionClass, actionHelper);
                if (actionClassExecutorInstance == null) {
                    logger.info("No executor for action class {}, skipping", actionClass);
                    continue;
                }
                List<FileActionDispatch> actions = map.get(actionClass);
                actionClassExecutorInstance.prepare(actionExecutionTaskStatusDto);
                fileActionsEngineService.runActionClassExecutor(actionExecutionTaskStatusDto,
                        actionClass,
                        actionClassExecutorInstance,
                        actions);
                actionsAppService.updateErrorCount(triggerId, actionExecutionTaskStatusDto);
                actionsAppService.increaseActionTriggerCounter(triggerId, actions.size(), actionClass);
            }
            actionsAppService.setActionTriggerState(triggerId, RunStatus.FINISHED_SUCCESSFULLY);
        } catch (Exception e) {
            logger.error("Failed to execute action trigger "+triggerId+". Exception: "+e.getMessage(),e);
            actionsAppService.setActionTriggerState(triggerId, RunStatus.FAILED);
        }
        return actionExecutionTaskStatusDto.getPage();
    }

    private List<FileActionDispatch> dispatchActionsOnSolrDocumentList(List<SolrFileEntity> solrFileEntities,
                                                   ActionDefinition actionDefinition,
                                                   Properties actionParams, long actionTriggerId) {
        logger.debug("Dispatch actions on {} files",solrFileEntities.size());
        List<FileActionDispatch> newActions = new ArrayList<>();

        Collection<ClaFile> files = fileCatService.fromEntities(solrFileEntities);

        Map<Long, DocFolder> foldersById = actionsAppService.getFoldersForFiles(files);
        Map<Long, RootFolder> rootFoldersById = actionsAppService.getRootFoldersForFiles(files);

        List<String> userGroupIds = files.stream().map(ClaFile::getUserGroupId).filter(Objects::nonNull).collect(Collectors.toList());
        Map<String, FileGroup> groups;
        AssociationRelatedData groupAssociationData = null;
        if (userGroupIds == null || userGroupIds.isEmpty()) {
            groups = new HashMap<>();
        } else {
            groups = groupsService.findByIds(userGroupIds).stream().collect(Collectors.toMap(FileGroup::getId, Function.identity()));
            groupAssociationData = associationsService.getAssociationRelatedDataGroups(groups.values());
        }

        AssociationRelatedData foldersAssociationData = associationsService.getAssociationRelatedData(foldersById.values());

        for (ClaFile file : files) {
            if (!file.isDeleted()) {
                try {
                    RootFolder rf = rootFoldersById.get(file.getRootFolderId());
                    long fileId = file.getId();
                    String baseName = file.getBaseName();
                    String fullPath = FileNamingUtils.convertFromUnixToWindows(file.getFullPath());
                    //TODO - run a preemptive filter
                    DispatchActionRequestDto request = new DispatchActionRequestDto();
                    request.setActionDefinitionId(actionDefinition.getId());
                    request.setFileId(fileId);
                    request.setBaseName(baseName);
                    request.setFullPath(rf.getRealPath() + fullPath);
                    Properties myActionParams = new Properties();
                    if (actionParams != null) {
                        myActionParams.putAll(actionParams);
                    }

                    AssociationRelatedData fileAssociationData = associationsService.getAssociationRelatedData(file);
                    List<AssociationEntity> fileDocTypeAssociationEntities = AssociationsService.getDocTypeAssociation(fileAssociationData.getAssociations());
                    List<AssociableEntityDto> entities = new ArrayList<>(fileAssociationData.getEntities());
                    Map<Long, BasicScope> scopes = new HashMap<>(fileAssociationData.getScopes());
                    if (foldersAssociationData != null && foldersAssociationData.getEntities() != null) {
                        entities.addAll(foldersAssociationData.getEntities());
                    }
                    if (foldersAssociationData != null && foldersAssociationData.getScopes() != null) {
                        scopes.putAll(foldersAssociationData.getScopes());
                    }

                    if (groupAssociationData != null && groupAssociationData.getScopes() != null) {
                        scopes.putAll(groupAssociationData.getScopes());
                    }
                    if (groupAssociationData != null && groupAssociationData.getEntities() != null) {
                        entities.addAll(groupAssociationData.getEntities());
                    }

                    List<AssociationEntity> userGroupAssociations = null;
                    if (groupAssociationData != null && !Strings.isNullOrEmpty(file.getUserGroupId())) {
                        userGroupAssociations = groupAssociationData.getPerGroupAssociations().get(file.getUserGroupId());
                    }

                    fileActionsEngineService.populateBaseParameterValues(rf, myActionParams, file,
                            foldersById.get(file.getDocFolderId()),
                            Strings.isNullOrEmpty(file.getUserGroupId()) ? null : groups.get(file.getUserGroupId()),
                            userGroupAssociations,
                            scopes, entities,
                            fileAssociationData.getAssociations(),
                            fileDocTypeAssociationEntities,
                            foldersAssociationData.getPerEntityAssociations().get(file.getDocFolderId()),
                            AssociationsService.getDocTypeAssociation(foldersAssociationData.getPerEntityAssociations().get(file.getDocFolderId())));
                    myActionParams.put("media_connector_user", actionsAppService.resolveMediaConnectorUserName(file));
                    request.setParameters(myActionParams);
                    newActions.add(fileActionsEngineService.createFileActionDispatch(request, actionTriggerId));
                } catch (InvalidPathException e) {
                    logger.info("invalid path for file {} for action trigger {} - skip" , file.getFileName(), actionTriggerId);
                } catch (Exception e) {
                    logger.error("failed to dispatch file {} for action trigger {} err {}" , file.getFileName(), actionTriggerId, e.getMessage(), e);
                }
            }
        }
        return newActions;
    }
}
