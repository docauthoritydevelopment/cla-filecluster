package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.FileSizePartitionDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.files.FileSizeRangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;


@Component
public class FileSizeEnricher implements CategoryTypeDataEnricher {



    @Autowired
    private FileSizeRangeService fileSizeRangeService;

    @Override
    public String getEnricherType() {
        return CatFileFieldType.SIZE_PARTITION.getSolrName();
    }

    @Override
    public boolean needToRemoveEmptyValues(){
        return false;
    }

    @Override
    public Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params) {
        HashMap<String, String> ans = new HashMap<>();
        Collection<FileSizePartitionDto> partitions =  fileSizeRangeService.getPartitions();
        for (FileSizePartitionDto partition : partitions) {
            ans.put(String.valueOf(partition.getId()),partition.getSolrQuery());
        }
        return ans;
    }



    @Override
    public String getFilterFieldName() {
        return FileDtoFieldConverter.DtoFields.fsFileSize.getAlternativeName();
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return fileSizeRangeService.getPartition(Long.parseLong(category)).getLabel();
        }).collect(Collectors.toList()));
    }


}
