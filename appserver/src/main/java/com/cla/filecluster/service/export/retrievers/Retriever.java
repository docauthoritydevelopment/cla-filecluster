package com.cla.filecluster.service.export.retrievers;

import reactor.core.publisher.Flux;

public interface Retriever<T> {
    Flux<T> createFlux ();
}
