package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.filter.SavedFilterAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by uri on 08/11/2015.
 */
@RestController
@RequestMapping("/api/filter")
public class FilterApiController {

    @Autowired
    private SavedFilterAppService savedFilterAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(method = RequestMethod.PUT)
    public SavedFilterDto createSavedFilter(@RequestBody SavedFilterDto savedFilterDto) {
        SavedFilterDto savedFilter = savedFilterAppService.createSavedFilter(
                savedFilterDto.getName(), savedFilterDto.getRawFilter(),
                savedFilterDto.getFilterDescriptor(),savedFilterDto.isGlobalFilter(), savedFilterDto.getScope());
        eventAuditService.audit(AuditType.FILTER,"Create filter ", AuditAction.CREATE, SavedFilterDto.class,savedFilter.getId());
        return savedFilter;
    }

    @RequestMapping(method = RequestMethod.GET )
    public Page<SavedFilterDto> listSavedFilters(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return savedFilterAppService.listSavedFiltes(dataSourceRequest);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public SavedFilterDto getById(@PathVariable long id) {
        SavedFilterDto savedFilterDto = savedFilterAppService.getById(id);
        if (savedFilterDto == null) {
            throw new BadRequestException(messageHandler.getMessage("saved-filter.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return savedFilterDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteSavedFilter(@RequestParam long id) {
        savedFilterAppService.deleteSavedFilter(id);
        eventAuditService.audit(AuditType.FILTER,"Delete filter ", AuditAction.DELETE, SavedFilterDto.class, id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public SavedFilterDto updateSavedFilter(@RequestBody SavedFilterDto savedFilterDto) {
        SavedFilterDto savedFilter = savedFilterAppService.updateSavedFilter(savedFilterDto);
        eventAuditService.audit(AuditType.FILTER,"Update filter ", AuditAction.UPDATE, SavedFilterDto.class,savedFilter.getId());
        return savedFilter;
    }

}
