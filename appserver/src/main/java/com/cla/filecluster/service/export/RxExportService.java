package com.cla.filecluster.service.export;

import com.cla.common.domain.dto.export.FileExportRequestDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.export.serialize.BizListExtractionSummarySerializer;
import com.cla.filecluster.service.progress.ProgressBean;
import com.cla.filecluster.service.progress.ProgressMissingException;
import com.cla.filecluster.service.progress.ProgressService;
import com.cla.filecluster.service.progress.ProgressType;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalNotification;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class RxExportService {

    @Value("${exporter.max-exports:1000}")
    private Integer maxExportsAmount;

    @Value("${exporter.export-expiration-time-minutes:1440}")
    private Integer exportExpireTimeMinutes;

    @Value("${exporter.export-concurrency:2}")
    private Integer exportConcurrency;

    @Value("${exporter.file-export.client-extract-example-max:20}")
    private int maxClientExtractExampleNumber;

    @Autowired
    private ProgressService progressService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private Set<RxExporter> exporters = new HashSet<>();

    private Cache<String, ExportResult> resultsCache;

    private Path tempPath = null;

    private static final String TEMP_FILE_PREFIX = "da_exported_artifact_";

    private ScheduledExecutorService scheduledExecutorService;

    private Scheduler exportScheduler;

    @PostConstruct
    public void init() {
        log.info("Initializing exporter Service...");
        this.resultsCache = CacheBuilder.newBuilder()
                .maximumSize(maxExportsAmount)
                .expireAfterAccess(exportExpireTimeMinutes, TimeUnit.MINUTES)
                .removalListener(this::onRemoval)
                .build();

        ThreadFactory namedThreadFactory =
                new ThreadFactoryBuilder()
                        .setNameFormat("da-export-%d")
                        .setDaemon(true)
                        .build();
        scheduledExecutorService = Executors.newScheduledThreadPool(exportConcurrency, namedThreadFactory);
        exportScheduler = Schedulers.fromExecutorService(scheduledExecutorService);

        BizListExtractionSummarySerializer.setMaxExtractExampleNumber(maxClientExtractExampleNumber);
    }

    @PreDestroy
    public void destroy() {
        exportScheduler.dispose();
        scheduledExecutorService.shutdown();
    }

    @Scheduled(cron = "* */30 * * * *")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    private void resultsCacheCleaner() {
        resultsCache.cleanUp();
    }

    @Scheduled(cron = "0 0/10 * * * *")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    private void resultsCacheRemover() {
        try {
            if (tempPath == null) {
                log.debug("temp file path not yet populated");
                return;
            }

            File tempPathDir = tempPath.toFile();
            if (!tempPathDir.exists() || !tempPathDir.isDirectory()) {
                log.debug("temp file path does not exists {}", tempPath);
                return;
            }

            String[] files = tempPathDir.list((dir, name) -> name.startsWith(TEMP_FILE_PREFIX));
            if (files == null || files.length == 0) {
                log.debug("no export files in temp directory {}", tempPath);
                return;
            }

            // go over all export files and check if should be deleted
            Map<String, ExportResult> exportsPending = resultsCache.asMap();
            List<String> exportIdToDel = new ArrayList<>();
            List<String> filesToDel = new ArrayList<>();
            for (String f : files) {
                checkSpecificFileAndDecideDelete(f, exportsPending,
                        exportIdToDel, filesToDel);
            }

            // handle exports to delete
            if (!exportIdToDel.isEmpty()) {
                exportIdToDel.forEach(exportId -> {
                    log.debug("delete export {}", exportId);
                    delete(exportId);
                });
            }

            // handle export old files to delete (export no longer in cache)
            if (!filesToDel.isEmpty()) {
                filesToDel.forEach(this::deleteFileInTempFolder);
            }

        } catch (Exception e) {
            log.error("fail looking for files and export to remove", e);
        }
    }

    private void checkSpecificFileAndDecideDelete(String fileName, Map<String, ExportResult> exportsPending,
                                                  List<String> exportIdToDel, List<String> filesToDel) {
        log.trace("go over file {} in {}", fileName, tempPath);
        String[] parts = fileName.split("_");
        if (parts.length > 3) {
            String exportId = parts[3];
            if (exportsPending.containsKey(exportId)) {
                ExportResult res = exportsPending.get(exportId);
                if (shouldDeleteExport(res.getDownloadTime())) {
                    exportIdToDel.add(exportId);
                }
            } else {
                filesToDel.add(fileName);
            }
        }
    }

    private boolean shouldDeleteExport(Long downloadTime) {
        return downloadTime != null && System.currentTimeMillis() - downloadTime > (10 * 60 * 1000);
    }

    private void deleteFileInTempFolder(String fileName) {
        try {
            File fileToDel = new File(tempPath.toString() + "\\" + fileName);
            if (fileToDel.exists()) {
                log.debug("delete temp file {}", fileToDel);
                fileToDel.delete();
            }
        } catch (Exception e) {
            log.error("cant delete file {} in temp path", fileName, e);
        }
    }

    public void markDownloaded(String exportId) {
        ExportResult result = resultsCache.getIfPresent(exportId);
        if (result != null) {
            result.setDownloadTime(System.currentTimeMillis());
        }
    }

    public ExportResult getResults(String exportId) {
        return Optional.ofNullable(resultsCache.getIfPresent(exportId)).orElseThrow(() -> {
            log.warn("could not resolve exporter with id: {}", exportId);
            return new RuntimeException(String.format("could not resolve exporter with id: %s", exportId));
        });
    }

    /**
     * start a background export process,
     * returns an id of the export process for later polling.
     * check progress with ProgressService / get the results with getResults.
     * @param exportType the type of the requested export
     * @param request the parameters for the export
     * @return an ID for this export process
     */
    public String background(ExportType exportType, FileExportRequestDto request) {
        // create a new progress
        String progressId = progressService.createNewProgress(ProgressType.EXPORT, "Exporting", 0,
                Pair.of(ProgressBean.REQUEST_PAYLOAD_KEY, request));
        ProgressService.Handler progress = progressService.createHandler(progressId);

        log.debug("export {} type {} is pending", progressId, exportType);

        // get exporter based on export-type and params
        RxExporter exporter = getExporterFor(exportType, request.getExportParams())
                .orElseThrow(() -> new RuntimeException(String.format("No available Exporter for export-type: %s, params: %s", exportType, request.getExportParams())));

        Flux<DataBuffer> flux = exporter.createExportFlux(request.getExportParams(), progressId);

        // subscribe...
        // on finish we write a result object to the results cache
        flux.map(results -> saveResults(progressId, exporter.getExportResultType(request.getFileNamePrefix()), results))
                .doOnSubscribe(a -> log.debug("started handling export {}", progressId))
                .subscriberContext(ReactiveSecurityContextHolder.withAuthentication(SecurityContextHolder.getContext().getAuthentication())) // needed to preserve the context of the subscriber
                .subscribeOn(exportScheduler)
                .subscribe(result -> { // on result
                    resultsCache.put(progressId, result);
                    progress.set(100, "Done");
                    progress.setDone();
                    log.debug("Export process finished for: {}", progressId);
                }, error -> { // on error
                    if (error instanceof ProgressMissingException) {
                        log.debug("progress {} no longer exists, download probably cancelled by user", progressId);
                    } else {
                        log.error("export error on export {}", progressId, error);
                        progress.fail("Error: " + error.getMessage(), error);
                        throw new RuntimeException(messageHandler.getMessage("export.error", Lists.newArrayList(progressId)), error);
                    }
                }, () -> { // on complete
                    if (progressService.isProgressExists(progressId)) {
                        // TODO move completion handler to here when implementing multi-buffer export.
                        if (!progress.isDone()) {
                            progress.fail("completed but not done", null);
                        }
                    }
                });

        // return export id
        return progressId;
    }

    private ExportResult saveResults(String exportId, ResultType resultType, DataBuffer exportOutput) {
        try {
            Resource persistedData = saveData(exportId, exportOutput);
            return ExportResult.builder()
                    .extension(resultType.getFileType())
                    .fileName(resultType.getFileName())
                    .contentType(resultType.getContentType())
                    .data(persistedData)
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(messageHandler.getMessage("export.write-error", Lists.newArrayList(exportId)), e);
        } finally {
            // release the DataBuffer... we don't need it anymore.
            DataBufferUtils.release(exportOutput);
        }
    }

    private Resource saveData(String exportId, DataBuffer exportOutput) throws IOException {
        Path tempFile = Files.createTempFile(TEMP_FILE_PREFIX + exportId + "_", ".tmp");

        if (tempPath == null) {
            synchronized (this) {
                tempPath = tempFile.getParent();
            }
        }

        try (OutputStream resultsOutputStream = Files.newOutputStream(tempFile)) {
            IOUtils.copy(exportOutput.asInputStream(), resultsOutputStream);
        }
        return new FileSystemResource(tempFile.toFile());
    }

    /**
     * start an export operation directly (not returning and id for later polling)
     * this method returns a flux object of the export operation which the callre of this method can subscribe
     * on to get results "reactively"
     * @param exportType the type of the requested export
     * @param params parameters for the export
     * @return a flux of the export operation.
     */
    public Flux<DataBuffer> direct(ExportType exportType, Map<String, String> params) {

        // create a new progress
        String progressId = progressService.createNewProgress(ProgressType.EXPORT, "Exporting", 0, null);

        // get exporter based on export-type and params
        RxExporter exporter = getExporterFor(exportType, params)
                .orElseThrow(() -> new RuntimeException(String.format("No available Exporter for export-type: %s, params: %s", exportType, params)));

        // create the flux based on type and params
        return exporter.createExportFlux(params, progressId);
    }

    private void delete(String exportId) {
        resultsCache.invalidate(exportId);
    }

    private Optional<RxExporter> getExporterFor(ExportType exportType, Map<String, String> params) {
        return exporters.stream().filter(exporter -> exporter.doesAccept(exportType, params)).findFirst();
    }

    private void onRemoval(RemovalNotification<String, ExportResult> removal) {
        try {
            ExportResult exportResult = removal.getValue();
            Resource data = exportResult.getData();
            if (data.isFile()) {
                boolean success = data.getFile().delete();
                if (!success) {
                    throw new IOException(messageHandler.getMessage("export.delete.failure"));
                }
            }
        } catch (IOException e) {
            log.error("error removing export artifact {}", removal.getKey(), e);
        }
    }
}
