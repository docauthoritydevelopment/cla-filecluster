package com.cla.filecluster.service.export.exporters.file;

import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;

public class CollectionResolverHelper {
	public static <T> String resolveCollectionValue(Collection<T> items, Function<T,String> getValue, Predicate<T> isImplicit) {
		return ofNullable(items)
				.map(
						dtos -> dtos.stream()
								.map(t -> getValue.apply(t)
										+ (isImplicit.test(t) ? " (implicit)" : ""))
								.collect(joining(";")))
				.orElse("");
	}
}
