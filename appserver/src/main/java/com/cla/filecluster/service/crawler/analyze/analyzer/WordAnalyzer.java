package com.cla.filecluster.service.crawler.analyze.analyzer;

import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.pv.*;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.PvAnalyzerRepository;
import com.cla.filecluster.repository.solr.SolrAnalysisDataRepository;
import com.cla.filecluster.service.crawler.pv.SimilarityMapper;
import com.cla.filecluster.service.files.ContentMetadataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;

@Component
public class WordAnalyzer implements ContentAnalyzer {

    private static final Logger logger = LoggerFactory.getLogger(WordAnalyzer.class);

    @Autowired
    private SimilarityMapper similarityMapper;

    @Autowired
    private PvAnalyzerRepository pvAnalyzerRepository;

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Value("${fileGrouping.word.single-step-analysis:true}")
    private boolean singleStepAnalysis;

    @Autowired
    private ContentMetadataService contentMetadataService;

    public Collection<Long> analyzeContent(Long taskId, final ContentMetadataDto contentMetadataDto) {
        return analyzeContent(taskId, contentMetadataDto, null);
    }

    public Collection<Long> analyzeContent(Long taskId, final ContentMetadataDto contentMetadataDto,
                                           final SimilaritySimulationContext simulationContext) {
        String hint = contentMetadataDto.getHint();
        logger.debug("Start handling content {} (hint={})", contentMetadataDto.getContentId(), (hint == null ? "" : hint));

        String customThresholds = null;
        Collection<Long> matchedContentIds = findGroupsSingleStep(taskId, contentMetadataDto, customThresholds,
                simulationContext);

        return matchedContentIds;
    }

    /**
     * @param contentMetadataDto
     * @param customThresholds
     * @param simulationContext
     * @return matched contentIds
     */
    private Collection<Long> findGroupsSingleStep(Long taskId, ContentMetadataDto contentMetadataDto, String customThresholds, final SimilaritySimulationContext simulationContext) {
        logger.debug("Find groups in a single step for content {} ", contentMetadataDto.getContentId());
        final long start = System.currentTimeMillis();
        Collection<Long> verifiedCandidates = pvAnalyzerRepository.findWordGroupCandidates(contentMetadataDto, customThresholds);
        logger.debug("Got {} verified candidates from solr on content: {}", verifiedCandidates.size(), contentMetadataDto);
        similarityMapper.assignGroupToMatchedCandidates(taskId, contentMetadataDto, verifiedCandidates, simulationContext);
        final long finish = System.currentTimeMillis();
        logger.debug("Finished handling contentId {} {} in {} ms. Got {} potential group candidates.",
                contentMetadataDto.getContentId(),
                ((contentMetadataDto.getHint() == null) ? "-" : contentMetadataDto.getHint()),
                (finish - start),
                verifiedCandidates.size());
        return verifiedCandidates;
    }


    private PartSimilarityMaps findSimilarityMaps(final ContentMetadataDto contentMetadataDto, final PvAnalysisData pvAnalysisData) {
        final long start = System.currentTimeMillis();
        final PartSimilarityMaps similarityMaps = new PartSimilarityMaps();
        for (final Map.Entry<String, ClaHistogramCollection<Long>> e : pvAnalysisData.getDefaultPvCollection().getCollections().entrySet()) {
            final SimilarityMap similarityMap = pvAnalyzerRepository.analyzePvForFile(contentMetadataDto, e.getKey(), e.getValue());
            similarityMaps.put(PVType.valueOf(e.getKey()), similarityMap, e.getValue().getGlobalCount());
        }
        logger.debug("Found similarity for contentId:{} within {} milis", contentMetadataDto.getContentId(), System.currentTimeMillis() - start);
        return similarityMaps;
    }


}
