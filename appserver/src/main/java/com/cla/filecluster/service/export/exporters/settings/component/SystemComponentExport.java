package com.cla.filecluster.service.export.exporters.settings.component;

import com.cla.common.domain.dto.components.ClaComponentDto;
import com.cla.common.domain.dto.components.ClaComponentType;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.components.mixin.CustomerDataCenterDtoMixin;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.component.SystemComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 5/27/2019
 */
@Slf4j
@Component
public class SystemComponentExport extends PageCsvExcelExporter<ClaComponentDto> {

    @Autowired
    private SysComponentService sysComponentService;

    private final static String[] header = {INSTANCE, STATE, DATA_CENTER, LOCATION, EXTERNAL_ADDR, LOCAL_ADDR};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.SYSTEM_COMPONENTS_SETTINGS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ClaComponentDto.class, ClaComponentDtoMixin.class)
                .addMixIn(CustomerDataCenterDto.class, CustomerDataCenterDtoMixin.class);
    }

    @Override
    protected Page<ClaComponentDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        String customerDataCenterId = requestParams.get("customerDataCenterId");
        if (customerDataCenterId != null) {
            return sysComponentService.findSysComponentByCustomerDataCenter(Long.valueOf(customerDataCenterId), ClaComponentType.MEDIA_PROCESSOR, dataSourceRequest);
        }
        return sysComponentService.listAllSysComponents(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(ClaComponentDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
