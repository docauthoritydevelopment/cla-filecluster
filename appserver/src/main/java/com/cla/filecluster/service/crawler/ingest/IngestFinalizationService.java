package com.cla.filecluster.service.crawler.ingest;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobStateUpdateInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.*;
import com.cla.filecluster.repository.PvEntriesIngesterRepository;
import com.cla.filecluster.repository.solr.SolrAnalysisDataRepository;
import com.cla.filecluster.repository.solr.SolrPvEntriesIngesterRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Responsible for different stages of ingest finalization.
 * Each stage will be invoked asynchronously.
 * <p>
 * Created by vladi on 3/21/2017.
 */
@Service
public class IngestFinalizationService implements ControlledScheduling {

    private final static Logger logger = LoggerFactory.getLogger(IngestFinalizationService.class);

    //------------------------------- autowired services ---------------------
    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private PvEntriesIngesterRepository ingesterRepository;

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrPvEntriesIngesterRepository solrPvEntriesIngesterRepository;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private FileCrawlerExecutionDetailsService crawlerService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private JobCoordinator jobCoordinator;

    @Autowired
    private SolrTaskService solrTaskService;

    //------------------------------- autowired configuration ---------------------
    @Value("${ingester.dedup.pageSize:1000}")
    private int dedupPageSize;

    @Value("${ingest-finalize.optimize-pv-index.enable:false}")
    private boolean enableOptimizePvIndex;

    @Value("${ingest-finalize.final.minimize.enable:true}")
    private boolean enableFinalIngestFinMinimization;

    @Value("${ingest-finalize.final.minimize.threshold:10000}")
    private int finalIngestFinMinimizeThreshold;

    @Value("${job-manager.jobs.in-progress-timeout-sec:3600}")
    private int inProgressJobTimeoutSec;

    @Value("${ingester.force.analyze.ingested:true}")
    private boolean forceAnalysisIngestOnlyContents;

    //------------------------------- other ---------------------------------------
    private TimeSource timeSource = new TimeSource();

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    private AtomicBoolean finalizationInProgress = new AtomicBoolean(false);

    private AtomicInteger ingestedNonFinalizedCount = new AtomicInteger();

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @AutoAuthenticate
    @Scheduled(initialDelayString = "${ingest-fin.tasks-polling-init-delay-millis:5000}", fixedRateString = "${ingest-fin.tasks-polling-rate-millis:10000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    void processNextTasksGroup() {

        if (!isSchedulingActive) {
            return;
        }

        List<SimpleJob> inProgressJobs = jobManagerService.getJobs(JobType.INGEST_FINALIZE, JobState.IN_PROGRESS);
        if (inProgressJobs.isEmpty()) {
            // avoid calling getTaskCountPerRun (which involves a pricey query) if there are no in-progress jobs
            return;
        }

        Map<Long, Long> newTasksCountPerRun =
                jobManagerService.getTaskCountPerRun(TaskType.INGEST_FINALIZE_CONTENT, JobState.IN_PROGRESS, TaskState.NEW);

        if (newTasksCountPerRun.size() == 0) {
            return;
        }

        runIngestFinalize(newTasksCountPerRun);
    }

    private void runIngestFinalize(Map<Long, Long> newTasksCountPerRun) {
        if (finalizationInProgress.compareAndSet(false, true)) {
            try {
                for (Long runId : newTasksCountPerRun.keySet()) {
                    Long taskCount = newTasksCountPerRun.get(runId);
                    if (taskCount > 0L) {
                        List<SimpleTask> newTasks =
                                jobManagerService.getTasksByContext(TaskType.INGEST_FINALIZE_CONTENT, TaskState.NEW,
                                        runId, taskCount.intValue());
                        if (newTasks.size() > 1) {
                            logger.warn("Got More than one INGEST_FINALIZE_CONTENT task for run {}. Running analysis finalization only for the first one", runId);
                        }
                        runIngestFinalizeForRun(runId);
                    }
                }
                resetIngestedNonFinalizedCount();
            } finally {
                finalizationInProgress.set(false);
            }
        } else {
            logger.info("Ingest finalization currently in progress.");
        }
    }

    @AutoAuthenticate
    public void runInterimIngestFinalize(Collection<Long> runIds) {

        if (finalizationInProgress.compareAndSet(false, true)) {
            try {
                for (Long runId : runIds) {
                    updateIngestOpMsg(runId, "Running interim optimization");
                    doIngestFinAndOptIdxs(runId, true, false);
                    updateIngestOpMsg(runId, "Finished running interim optimization");
                }
                resetIngestedNonFinalizedCount();
            } finally {
                finalizationInProgress.set(false);
            }
        } else {
            logger.info("Ingest finalization currently in progress.");
        }
    }

    private void runIngestFinalizeForRun(Long runId) {
        int ingestedNonFinalizedCount = getIngestedNonFinalizedCount();
        if (enableFinalIngestFinMinimization &&
                jobManagerService.gotActiveJobs(JobType.SCAN, JobType.SCAN_FINALIZE, JobType.INGEST) &&
                ingestedNonFinalizedCount < finalIngestFinMinimizeThreshold) {

            logger.info("Running only minimal Ingest-Finalize and Skipping Optimize indexes. Got only {} ingested files while threshold is {}",
                    ingestedNonFinalizedCount, finalIngestFinMinimizeThreshold);
            doIngestFinAndOptIdxs(runId, false, true);
        } else {
            doIngestFinAndOptIdxs(runId, false, false);
        }
    }

    private void doIngestFinAndOptIdxs(Long runContext, boolean interimMode, boolean minimizeIngestFinalization) {
        Long jobId = jobManagerService.getJobIdCached(runContext, JobType.INGEST_FINALIZE);

        // Running minimal ingest finalization is only relevant in final (i.e. not interim) ingest finalization
        if (minimizeIngestFinalization && interimMode) {
            throw new IllegalArgumentException("minimizeIngestFinalization shouldn't be set to true in interim mode");
        }

        try {
            ProgressTracker ingestFinProgressTracker =
                    interimMode ? new DummyProgressTracker() : new ProgressPercentageTracker(jobId, jobManagerService);
            ingestFinProgressTracker =
                    new ProgressTimestampTracker(ingestFinProgressTracker, inProgressJobTimeoutSec, 10, jobId, jobManagerService);

            // The coordination lock request below changes the job state to WAITING. Make sure we're changing the correct job's state
            long curRunningJobId = interimMode ? jobManagerService.getJobIdCached(runContext, JobType.INGEST) : jobId;
            logger.debug("Requesting job coordination lock for job: #{}", curRunningJobId);
            jobCoordinator.requestLock(JobType.INGEST_FINALIZE, curRunningJobId);
            logger.debug("Acquired job coordination lock");

            JobStateUpdateInfo updateIngestFinDoneStateInfo =
                    new JobStateUpdateInfo(jobId, JobState.DONE, null, "Ingestion finalization completed", true);

            if (minimizeIngestFinalization) {
                // Run only the vital parts of ingest finalization
                ingestFinProgressTracker.startStageTracking(100);
                finalizeIngestionMinimally(runContext, jobId, ingestFinProgressTracker);
                ingestFinProgressTracker.endCurStage();
                ingestFinProgressTracker.endTracking();

                // mark Optimize-Indexes as DONE
                skipOptimizeIndexes(runContext);

                // update the Ingest-Finalize and Analyze statuses in one transaction
                handleNextJob(runContext, updateIngestFinDoneStateInfo);
            } else {
                // Run full ingest finalization
                finalizeIngestion(runContext, jobId, ingestFinProgressTracker);

                if (jobManagerService.isJobActive(jobId)) {
                    ProgressTracker optIndexesProgressTracker;
                    if (interimMode) {
                        optIndexesProgressTracker =
                                new ProgressTimestampTracker(new DummyProgressTracker(), inProgressJobTimeoutSec, 10, jobId, jobManagerService);
                        // Run Optimize-Indexes logic without updating the Optimize-Indexes status changes
                        runOptimizeIndexes(runContext, jobId, null, false, optIndexesProgressTracker);
                    } else {
                        // Run Optimize-Indexes Job
                        handleOptimizeIndexes(runContext, updateIngestFinDoneStateInfo);
                    }
                }
            }
        } catch (InterruptedException ie) {
            Thread.currentThread().interrupt();
            logger.error("Interrupted", ie);
        } catch (CoordinationLockDeniedException e) {
            logger.info("Ingest finalize did not run due to job coordination lock denial:", e);
        } finally {
            logger.debug("releasing job coordination lock for Ingest-Fin");
            jobCoordinator.release();
            logger.debug("released job coordination lock for Ingest-Fin");
        }
    }

    private void skipOptimizeIndexes(Long runContext) {

        Long optimizeIndexesJobId = jobManagerService.getJobIdCached(runContext, JobType.OPTIMIZE_INDEXES);
        jobManagerService.updateJobState(optimizeIndexesJobId,
                JobState.DONE,
                JobCompletionStatus.SKIPPED,
                "Index optimization was not required", true);
    }

    public void handleOptimizeIndexes(Long runContext, JobStateUpdateInfo updateIngestFinDoneStateInfo) {

        Long optimizeIndexesJobId = jobManagerService.getJobIdCached(runContext, JobType.OPTIMIZE_INDEXES);

        if (jobManagerService.getCachedJobState(optimizeIndexesJobId).isActive()) {

            jobManagerService.createTaskIfAbsent(null, optimizeIndexesJobId, TaskType.OPTIMIZE_INDEXES);

            // Update the state of INGEST_FINALIZE job along with the OPTIMIZE_INDEXES job in one transaction
            JobStateUpdateInfo updateOptIdxStateInfo =
                    new JobStateUpdateInfo(optimizeIndexesJobId, JobState.IN_PROGRESS, null, "Optimizing indexes", false);
            jobManagerService.updateJobStates(Arrays.asList(updateIngestFinDoneStateInfo, updateOptIdxStateInfo));
        } else { // should skip opt indexes
            handleNextJob(runContext, updateIngestFinDoneStateInfo);
        }
    }

    public void runOptimizeIndexes(Long runContext, Long jobId, Long taskId, boolean updateJobsStatus) {
        ProgressTracker optIndexesProgressTracker =
                new ProgressPercentageTracker(jobId, jobManagerService);
        optIndexesProgressTracker =
                new ProgressTimestampTracker(optIndexesProgressTracker, inProgressJobTimeoutSec, 10, jobId, jobManagerService);
        runOptimizeIndexes(runContext, jobId, taskId, updateJobsStatus, optIndexesProgressTracker);
    }

    private void runOptimizeIndexes(Long runContext, Long jobId, Long taskId, boolean updateJobsStatus, ProgressTracker optIndexesProgressTracker) {
        try {
            Runnable callback; // called asynchronously after optimize indexes is done
            if (updateJobsStatus) {
                // wake up the analysis job when finished
                callback = () -> {
                    jobManagerService.updateTaskStatus(taskId, TaskState.DONE);
                    JobStateUpdateInfo updateJobStateDoneInfo =
                            new JobStateUpdateInfo(jobId, JobState.DONE, null, "Indexes optimization completed", true);
                    handleNextJob(runContext, updateJobStateDoneInfo);
                    optIndexesProgressTracker.endTracking();
                };
            } else {
                // no need to update job statuses after done optimizing indexes. Just end progress tracking
                callback = optIndexesProgressTracker::endTracking;
            }
            optimizeIndexes(callback, optIndexesProgressTracker);
        } catch (Exception e) { //TODO: move this catch is to the the optimizeIndexes thread
            if (taskId != null) {
                jobManagerService.updateTaskStatus(taskId, TaskState.FAILED);
            }
            jobManagerService.handleFailedFinishJob(jobId, "Optimize indexes failed");
            jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_ERROR);
        }
    }

    private void handleNextJob(Long runContext, JobStateUpdateInfo lastJobDoneInfo) {
        CrawlRun crawlRun = fileCrawlerExecutionDetailsService.getCrawlRun(runContext);
        Long analyzeJobId = jobManagerService.getJobIdCached(runContext, JobType.ANALYZE);
        boolean analyzeJobLastStateActive = false;

        if (analyzeJobId != null) {
            analyzeJobLastStateActive = jobManagerService.getCachedJobState(analyzeJobId).isActive();
        }

        final boolean gotDeletions = crawlRun.getDeletedFiles() > 0;

        if (crawlRun.isAnalyzeFiles() && analyzeJobLastStateActive) {
            JobStateUpdateInfo updateAnalyzeStateInfo =
                    new JobStateUpdateInfo(analyzeJobId, JobState.IN_PROGRESS, null,
                            "Start analyzing", false);

            // dispatch analysis tasks for ingest only contents
            if (forceAnalysisIngestOnlyContents) {
                jobManagerService.createAnalysisTasksForIngestedOnlyContents(crawlRun.getId(), crawlRun.getRootFolderId());
            }

            jobManagerService.updateJobStates(Arrays.asList(lastJobDoneInfo, updateAnalyzeStateInfo));
        } else if (gotDeletions) { // analyze shouldnt run but has deletions so run analyze finalize
            Long analyzeFinJobId = jobManagerService.getJobIdCached(runContext, JobType.ANALYZE_FINALIZE);
            List<JobStateUpdateInfo> jobsInfo = Lists.newArrayList(lastJobDoneInfo);
            if (analyzeJobLastStateActive) { // if analyze should have run but not marked - make sure job is skipped
                JobStateUpdateInfo updateAnalyzeStateInfo =
                        new JobStateUpdateInfo(analyzeJobId, JobState.DONE, JobCompletionStatus.SKIPPED, "Analysis skipped", true);
                jobsInfo.add(updateAnalyzeStateInfo);
            }
            JobStateUpdateInfo updateAnalyzeFinStateInfo =
                    new JobStateUpdateInfo(analyzeFinJobId, JobState.WAITING, null, "Starting analysis finalization", false);
            jobsInfo.add(updateAnalyzeFinStateInfo);
            jobManagerService.updateJobStates(jobsInfo);
            jobManagerService.createTaskIfAbsent(null, analyzeFinJobId, TaskType.ANALYZE_FINALIZE);
        } else {
            logger.debug("Crawl run is not marked for analyze");
            if (analyzeJobLastStateActive) {
                jobManagerService.pauseJob(analyzeJobId, runContext, "Not marked for analyze", true, lastJobDoneInfo);
            } else {
                List<JobStateUpdateInfo> jobsInfo = Lists.newArrayList(lastJobDoneInfo);
                Long analyzeFinJobId = jobManagerService.getJobIdCached(runContext, JobType.ANALYZE_FINALIZE);
                if (analyzeFinJobId != null) {
                    JobStateUpdateInfo updateAnalyzeFinStateInfo =
                            new JobStateUpdateInfo(analyzeFinJobId, JobState.DONE, JobCompletionStatus.SKIPPED, "Analysis finalization skipped", false);
                    jobsInfo.add(updateAnalyzeFinStateInfo);
                }
                jobManagerService.updateJobStates(jobsInfo);
            }
            jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_INITIATED);
        }

        if (analyzeJobLastStateActive) {
            long analyzeTasksCount = solrTaskService.getAnalysisTaskCount(runContext);
            jobManagerService.updateEstimatedTaskCount(analyzeJobId, analyzeTasksCount);
        }
        jobManagerService.setRootFolderWasIngestedFinalized(crawlRun.getRootFolderId());
    }

    private void updateIngestOpMsg(long runId, String msg) {
        Long jobId = jobManagerService.getJobIdCached(runId, JobType.INGEST);
        if (jobId != null) {
            jobManagerService.updateJobState(jobId, JobState.IN_PROGRESS, null, msg, false);
        }
    }

    //------------------------------- interface methods ---------------------------------------

    @AutoAuthenticate
    private void finalizeIngestion(Long runContext, long jobId, ProgressTracker progressTracker) {
        try {
            logger.debug("Finalize Ingestion on run {}", runContext);
            progressTracker.startStageTracking(13);
            filesDataPersistenceService.commitSolr();
            progressTracker.endCurStage();

            jobManagerService.setOpMsg(jobId, "Joining duplicate files");
            progressTracker.startStageTracking(17);
            joinMissedDuplicateFiles(progressTracker);
            progressTracker.endCurStage();

            jobManagerService.setOpMsg(jobId, "Start preparing analysis data");
            filesDataPersistenceService.prepareAnalysisData(progressTracker);

            contentMetadataFinalize(jobId, progressTracker);

            jobManagerService.setOpMsg(jobId, "Start committing and optimizing SOLR fileCat");

            progressTracker.startStageTracking(13);
            fileCatService.commitToSolr();
            progressTracker.endCurStage();

            progressTracker.startStageTracking(12);
            jobManagerService.setOpMsg(jobId, "");
            updateRootFolderIfNeeded(runContext);
            progressTracker.endCurStage();

        } catch (Exception e) {
            logger.error("Failed to run finalize ingest", e);
            if (jobManagerService.isJobStarted(jobId)) {
                jobManagerService.handleFailedFinishJob(jobId, "Ingest finalize failed");
                jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_ERROR);
            }
            throw e;
        }
    }

    @AutoAuthenticate
    private void finalizeIngestionMinimally(Long runContext, long jobId, ProgressTracker progressTracker) {
        try {
            contentMetadataIngestFinalize(jobId, progressTracker);
            jobManagerService.setOpMsg(jobId, "Start committing and optimizing SOLR fileCat");
            filesDataPersistenceService.commitSolr();
            updateRootFolderIfNeeded(runContext);
            jobManagerService.setOpMsg(jobId, "");
        } catch (Exception e) {
            logger.error("Failed to run minimal finalize ingest", e);
            jobManagerService.handleFailedFinishJob(jobId, "Minimal Ingest Finalize failed");
            jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_ERROR);
            throw e;
        }
    }

    @Async
    private void optimizeIndexes(Runnable callback, ProgressTracker optIndexesProgressTracker) {
        if (enableOptimizePvIndex) {
            logger.warn("enableOptimizePvIndex = true, calling optimize indexes, this can be a very long running operation");
            optIndexesProgressTracker.startStageTracking(50);
            ingesterRepository.optimizePvIndex();
            optIndexesProgressTracker.endCurStage();
            optIndexesProgressTracker.startStageTracking(50);
            solrAnalysisDataRepository.optimizeIndex();
            optIndexesProgressTracker.endCurStage();
        } else {
            logger.debug("enableOptimizePvIndex = false, not performing pv index optimization");
        }

        // run callback when finished
        callback.run();
    }

    public void optimizeIndexesSync() {
        ingesterRepository.optimizePvIndex();
        solrAnalysisDataRepository.optimizeIndex();
    }

    //------------------------------- private methods ---------------------------------------

    private void joinMissedDuplicateFiles(ProgressTracker progressTracker) {
//        opStateService.setOpMsg("Join Missed Duplicate files");
        logger.info("joinMissedDuplicateFiles - start");
        long start = timeSource.currentTimeMillis();
        logger.debug("Find the contentIds that are duplicate");
        int signaturesToScan = contentMetadataService.getDirtySignaturesCount();

        progressTracker.setCurStageEstimatedTotal(signaturesToScan);

        if (signaturesToScan <= 0) {
            logger.debug("No signatures to scan  - done");
            return;
        }
        logger.debug("Got {} signatures to scan", signaturesToScan);
        Map<String, Long> dupSigs = contentMetadataService.getDuplicateSignatures();
        if (dupSigs.isEmpty()) {
            logger.debug("No duplicate signatures to scan  - done");
            return;
        }

        List<String> duplicateSignatures = new ArrayList<>(dupSigs.keySet());

        int counter = 0;
        int cnged = 0;
        while (counter < duplicateSignatures.size()) {
            int toIndex = Math.min(dedupPageSize, duplicateSignatures.size());
            List<String> subList = duplicateSignatures.subList(counter, toIndex);
            Map<String, List<Long>> contentIdsForSig = contentMetadataService.getContentIdsForSignature(subList);
            List<Object[]> duplicateSignaturesData = new ArrayList<>();
            dupSigs.keySet().forEach(sig -> {
                Object[] arr = {sig, dupSigs.getOrDefault(sig, 0L), contentIdsForSig.get(sig)};
                duplicateSignaturesData.add(arr);
            });

            try {
                filesDataPersistenceService.processDuplicateSignaturesPage(duplicateSignaturesData);
                cnged++;
                logger.debug("processDuplicateSignaturesPage  of {} items found. Batch scan of {} {}/{}. Proceed to next page.",
                        subList.size(), dedupPageSize, counter, signaturesToScan);
            } catch (Exception e) {
                logger.error("Failed to processDuplicateSignaturesPage of {} items found. Batch scan of {} {}/{}. Error={}. Proceed to next page.",
                        subList.size(), dedupPageSize, counter, signaturesToScan, e.getMessage(), e);
            }

            int nextCounter = counter + dedupPageSize;
            long progressToReport = nextCounter < signaturesToScan ? dedupPageSize : signaturesToScan - counter;
            progressTracker.incrementProgress(progressToReport);
            counter = nextCounter;
        }

        if (cnged > 0) {
            solrPvEntriesIngesterRepository.softCommitNoWaitFlush();
        }

        long duration = timeSource.millisSince(start);
        logger.info("joinMissedDuplicateFiles - finish in {}ms", duration);
    }

    private void contentMetadataFinalize(Long jobId, ProgressTracker progressTracker) {
        try {
            logger.debug("Before content metadata finalization trying to acquire population dirty lock" +
                    "population dirty lock for job {}", jobId);
            contentMetadataService.acquirePopulationDirtyWriteLock();
            logger.debug("Successfully acquired population dirty lock for job {}", jobId);
            logger.info("Start updateContentMetadataStats processing...");
//        opStateService.setOpMsg("update content stats");
            progressTracker.startStageTracking(1);
            filesDataPersistenceService.updateContentMetadataStats(jobId, progressTracker);
            progressTracker.endCurStage();

            jobManagerService.checkIfJobPaused(jobId);

            progressTracker.startStageTracking(4);
            contentMetadataIngestFinalize(jobId, progressTracker);
            progressTracker.endCurStage();
        } finally {
            logger.debug("Ingest finalization done trying to release population dirty lock for job {}",
                    jobId);
            contentMetadataService.releasePopulationDirtyWriteLock();
            logger.debug("Successfully released population dirty lock for job {}",
                    jobId);
        }
    }

    private void contentMetadataIngestFinalize(Long jobId, ProgressTracker progressTracker) {
        logger.info("Start contentMetadataIngestFinalize processing...");
        filesDataPersistenceService.contentMetadataIngestFinalize(jobId, progressTracker);
        logger.info("Done contentMetadataIngestFinalize processing");
    }

    private void updateRootFolderIfNeeded(Long runContext) {
        Long rootFolderId = crawlerService.getCrawlRun(runContext).getRootFolderId();
        docStoreService.updateRootFolderReingest(rootFolderId, false);
    }

    public int getIngestedNonFinalizedCount() {
        return ingestedNonFinalizedCount.get();
    }

    private void resetIngestedNonFinalizedCount() {
        ingestedNonFinalizedCount.set(0);
    }

    @SuppressWarnings("UnusedReturnValue")
    public int incrementIngestedNonFinalizedCount() {
        return ingestedNonFinalizedCount.incrementAndGet();
    }
}
