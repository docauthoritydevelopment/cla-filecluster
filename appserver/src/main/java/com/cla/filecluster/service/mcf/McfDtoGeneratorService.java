package com.cla.filecluster.service.mcf;

import com.cla.common.domain.dto.mcf.mcf_api.McfConfigurationParameterDto;
import com.cla.common.domain.dto.mcf.mcf_api.McfRepositoryConnectionObjectDto;
import com.cla.common.domain.dto.mcf.mcf_api.McfTransformationConnectionDto;
import com.cla.common.domain.dto.mcf.mcf_api.OutputConnectionObjectDto;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfIncludeObjectDto;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfJobObjectDto;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfPipelineStageObjectDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 18/07/2016.
 */
@Service
public class McfDtoGeneratorService {

    public static final String OUTPUT_CONNECTION_NAME = "FileClusterOutput";
    public static final String FILESYSTEM_REPOSITORY_CONNECTION_NAME = "FileSystem";
    public static final String ROOT_FOLDER_PREFIX = "rootFolder_";
    private Logger logger = LoggerFactory.getLogger(McfDtoGeneratorService.class);

    /**
     * {"job":
     *  {"id":"1468830417773",
     *  "description":"temp"
     *  ,"repository_connection":"FileSystem"
     *  ,"document_specification":
     *      {"startpoint":{"_value_":""
     *      ,"_attribute_path":"E:\\Workspace\\Data\\Play"
     *      ,"_attribute_converttouri":"false","include":[{"_value_":""
     *      ,"_attribute_match":"*","_attribute_type":"file"}
     *      ,{"_value_":"","_attribute_match":"*","_attribute_type":"directory"}]}
     *      }
     *      ,"pipelinestage":[
     *          {"stage_id":"0",
     *          "stage_isoutput":"false"
     *          ,"stage_connectionname":"addRootFolderId"
     *          ,"stage_description":"addRootFolderId"
     *          ,"stage_specification":{"expression":{"_value_":""
     *          ,"_attribute_parameter":"rootFolderId","_attribute_value":"1"}
     *          ,"keepAllMetadata":{"_value_":"","_attribute_value":"true"}
     *          ,"filterEmpty":{"_value_":"","_attribute_value":"true"}}
     *          }
     *          ,{"stage_id":"1"
     *          ,"stage_prerequisite":"0"
     *          ,"stage_isoutput":"true"
     *          ,"stage_connectionname":"FileCluster output"
     *          ,"stage_description":"1"
     *          ,"stage_specification":{}}
     *      ]
     *      ,"start_mode":"manual","run_mode":"scan once"
     *      ,"hopcount_mode":"accurate"
     *      ,"priority":"5"
     *      ,"recrawl_interval":"86400000"
     *      ,"expiration_interval":"infinite"
     *      ,"reseed_interval":"3600000"}
     *      }
     *
     * @return
     * @param rootFolderId
     * @param repositoryConnectionName
     * @param repositorySpecification
     */
    McfJobObjectDto createRootFolderJobObjectDto(Long rootFolderId
            , String repositoryConnectionName
            , Map repositorySpecification) {
        McfJobObjectDto newJob = new McfJobObjectDto();
        newJob.setDescription(ROOT_FOLDER_PREFIX + rootFolderId);
        newJob.setRepository_connection(repositoryConnectionName);
        newJob.setStart_mode("manual");
        newJob.setRun_mode("scan once");
        newJob.setPriority(5);
        newJob.setHopcount_mode("accurate");
        newJob.setRecrawl_interval("86400000");
        newJob.setReseed_interval("3600000");
        newJob.setExpiration_interval("infinite");
        newJob.setDocument_specification(repositorySpecification);

        List<McfPipelineStageObjectDto> pipelineStages = new ArrayList<>();
        pipelineStages.add(createAddRootFolderPipelineStage(rootFolderId));
        pipelineStages.add(createFileClusterOutputPipelineStage());
        newJob.setPipelinestage(pipelineStages);
        return newJob;
    }

    /**
     *      *          ,{"stage_id":"1"
     *          ,"stage_prerequisite":"0"
     *          ,"stage_isoutput":"true"
     *          ,"stage_connectionname":"FileCluster output"
     *          ,"stage_description":"1"
     *          ,"stage_specification":{}}
     * @return
     */
    private McfPipelineStageObjectDto createFileClusterOutputPipelineStage() {
        McfPipelineStageObjectDto dto = new McfPipelineStageObjectDto();
        dto.setStage_id("1");
        dto.setStage_prerequisite("0");
        dto.setStage_isoutput(true);
        dto.setStage_connectionname(OUTPUT_CONNECTION_NAME);
        dto.setStage_description("1");
        dto.setStage_specification(new HashMap());
        return dto;
    }

    /**
     *          {"stage_id":"0",
     *          "stage_isoutput":"false"
     *          ,"stage_connectionname":"addRootFolderId"
     *          ,"stage_description":"addRootFolderId"
     *          ,"stage_specification":{"expression":{"_value_":""
     *          ,"_attribute_parameter":"rootFolderId","_attribute_value":"1"}
     *          ,"keepAllMetadata":{"_value_":"","_attribute_value":"true"}
     *          ,"filterEmpty":{"_value_":"","_attribute_value":"true"}}
     *          }
     *
     * @return
     * @param rootFolderId
     */
    McfPipelineStageObjectDto createAddRootFolderPipelineStage(Long rootFolderId) {
        McfPipelineStageObjectDto dto = new McfPipelineStageObjectDto();
        dto.setStage_id("0");
        dto.setStage_isoutput(false);
        dto.setStage_connectionname("addRootFolderId");
        dto.setStage_description("addRootFolderId");

        Map specification = new HashMap();
        Map specificationExpression = new HashMap();
        specificationExpression.put("_value_","");
        specificationExpression.put("_attribute_parameter","rootFolderId");
        specificationExpression.put("_attribute_value",String.valueOf(rootFolderId));
        specification.put("expression",specificationExpression);

        Map keepAllMetadata = new HashMap();
        keepAllMetadata.put("_value_","");
        keepAllMetadata.put("_attribute_value","true");
        specification.put("keepAllMetadata",keepAllMetadata);

        Map filterEmpty = new HashMap();
        filterEmpty.put("_value_","");
        filterEmpty.put("_attribute_value","true");
        specification.put("filterEmpty",filterEmpty);

        dto.setStage_specification(specification);
        return dto;
    }

    /**
     *      {"startpoint":{"_value_":""
     *      ,"_attribute_path":"E:\\Workspace\\Data\\Play"
     *      ,"_attribute_converttouri":"false","include":[{"_value_":""
     *      ,"_attribute_match":"*","_attribute_type":"file"}
     *      ,{"_value_":"","_attribute_match":"*","_attribute_type":"directory"}]}
     *      }
     * @return
     */
    Map createFileSystemJobSpecification(String filePath) {
        logger.debug("Create file system job specification with file path {}",filePath);
        Map result = new HashMap();
        Map startPoint = new HashMap();
        startPoint.put("_value_","");
        startPoint.put("_attribute_path",filePath);
        startPoint.put("_attribute_converttouri","false");

        McfIncludeObjectDto[] includeArray = new McfIncludeObjectDto[2];
        includeArray[0] = new McfIncludeObjectDto("","*","file");
        includeArray[1] = new McfIncludeObjectDto("","*","directory");
        startPoint.put("include",includeArray);

        result.put("startpoint",startPoint);
        return result;
    }

    public OutputConnectionObjectDto createOutputConnectionObjectDto(String outputConnectionName, String outputConnectionClass, Map<String, String> configuration, McfCommunicationService mcfCommunicationService) {
        OutputConnectionObjectDto outputConnectionObjectDto = new OutputConnectionObjectDto();
        outputConnectionObjectDto.setClass_name(outputConnectionClass);
        outputConnectionObjectDto.setName(outputConnectionName);
        outputConnectionObjectDto.setDescription("");
        outputConnectionObjectDto.setConfiguration(convertConfigurationToDto(configuration));
        outputConnectionObjectDto.setMax_connections(10);
        return outputConnectionObjectDto;
    }

    private Map<String, McfConfigurationParameterDto[]> convertConfigurationToDto(Map<String, String> configuration) {
        Map<String, McfConfigurationParameterDto[]> result = new HashMap<>();
        McfConfigurationParameterDto[] values = new McfConfigurationParameterDto[configuration.size()];
        int i = 0;
        for (Map.Entry<String, String> entry : configuration.entrySet()) {
            values[i] = new McfConfigurationParameterDto(entry.getKey(), entry.getValue());
            i++;
        }

        result.put("_PARAMETER_", values);
        return result;
    }

    /**
     * {"transformationconnection":
     * {"isnew":"false"
     * ,"name":"addRootFolderId"
     * ,"class_name":"org.apache.manifoldcf.agents.transformation.forcedmetadata.ForcedMetadataConnector"
     * ,"max_connections":"10","description":"","configuration":{}}}
     * @param name
     * @return
     */
    public McfTransformationConnectionDto createTransformationConnectionObjectDto(String name) {
        McfTransformationConnectionDto dto = new McfTransformationConnectionDto();
        dto.setName(name);
        dto.setIsnew(Boolean.TRUE);
        dto.setClass_name("org.apache.manifoldcf.agents.transformation.forcedmetadata.ForcedMetadataConnector");
        dto.setMax_connections(10);
        return dto;
    }

    /**
     * Created by uri on 20/07/2016.
     {"isnew":"false","name":"FileSystem","class_name":"org.apache.manifoldcf.crawler.connectors.filesystem.FileConnector","max_connections":"10","description":"","configuration":{}}
     */
    public McfRepositoryConnectionObjectDto creatRepository(String name,String className) {
        McfRepositoryConnectionObjectDto dto = new McfRepositoryConnectionObjectDto();
        dto.setClass_name(className);
        dto.setName(name);
        dto.setIsnew("true");
        dto.setMax_connections(10);
        return dto;
    }
}
