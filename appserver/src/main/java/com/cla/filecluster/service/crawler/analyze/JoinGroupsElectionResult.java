package com.cla.filecluster.service.crawler.analyze;

import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.google.common.collect.Sets;

import java.util.Collection;

/**
 * Join group election result
 * @see JoinGroupsElectionStrategy
 */
public class JoinGroupsElectionResult {

    private SolrFileGroupEntity electedGroup;
    private Collection<SolrFileGroupEntity> acceptedGroups = Sets.newHashSet();
    private Collection<SolrFileGroupEntity> rejectedGroups = Sets.newHashSet();

    public void setElectedGroup(SolrFileGroupEntity electedGroup) {
        this.electedGroup = electedGroup;
    }

    public SolrFileGroupEntity getElectedGroup() {
        return electedGroup;
    }

    public Collection<SolrFileGroupEntity> getAcceptedGroups() {
        return acceptedGroups;
    }

    public Collection<SolrFileGroupEntity> getRejectedGroups() {
        return rejectedGroups;
    }
}
