package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class DepartmentAggCountDtoMixin {

    @JsonUnwrapped
    DepartmentDto item;

    @JsonProperty(DocTypeHeaderFields.NUM_OF_DIRECT_FILERED)
    private Integer count;
}
