package com.cla.filecluster.service.schedule;

import com.cla.common.domain.dto.schedule.CrawlerPhaseBehavior;
import com.cla.common.domain.dto.schedule.ScheduleType;
import com.cla.filecluster.service.importEntities.ImportItemRow;

/**
 * Created by liora on 01/05/2017.
 */
public class ScheduleItemRow  extends ImportItemRow {

    private String name;
    private CrawlerPhaseBehavior analyzePhaseBehavior;
    private Boolean active;
    private String schedulingDescription;
    private ScheduleType scheduleType;
    private Integer hour;
    private Integer minutes;
    private Integer every;
    private String[] daysOfTheWeek;
    private Boolean isDefault;
    private String customCronConfig;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CrawlerPhaseBehavior getAnalyzePhaseBehavior() {
        return analyzePhaseBehavior;
    }

    public void setAnalyzePhaseBehavior(CrawlerPhaseBehavior analyzePhaseBehavior) {
        this.analyzePhaseBehavior = analyzePhaseBehavior;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getSchedulingDescription() {
        return schedulingDescription;
    }

    public void setSchedulingDescription(String schedulingDescription) {
        this.schedulingDescription = schedulingDescription;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public Integer getHour() {
        return hour;
    }

    public void setHour(Integer hour) {
        this.hour = hour;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    public Integer getEvery() {
        return every;
    }

    public void setEvery(Integer every) {
        this.every = every;
    }

    public String[] getDaysOfTheWeek() {
        return daysOfTheWeek;
    }

    public void setDaysOfTheWeek(String[] daysOfTheWeek) {
        this.daysOfTheWeek = daysOfTheWeek;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public String getCustomCronConfig() {
        return customCronConfig;
    }

    public void setCustomCronConfig(String customCronConfig) {
        this.customCronConfig = customCronConfig;
    }

    @Override
    public String toString() {
        return "ScheduleItemRow{" +
                "name='" + name + '\'' +
                ", active='" + active + '\'' +
                ", scheduleType='" + scheduleType + '\'' +
                ", schedulingDescription='" + schedulingDescription + '\'' +
                ", analyzePhaseBehavior='" + analyzePhaseBehavior + '\'' +
                ", hour='" + hour + '\'' +
                ", minutes='" + minutes + '\'' +
                ", every='" + every + '\'' +
                ", daysOfTheWeek='" + daysOfTheWeek + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", customCronConfig='" + customCronConfig + '\'' +
                super.toString()+
                '}';
    }
}
