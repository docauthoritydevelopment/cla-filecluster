package com.cla.filecluster.service.export.exporters.file.date;

import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class DateAggCountDtoMixin {

    @JsonUnwrapped
    DateRangeItemDto item;

    @JsonProperty(DateHeaderFields.NUM_OF_FILTERED)
    private Integer count;
}
