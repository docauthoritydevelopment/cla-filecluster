package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.metadata.MetadataAppService;
import com.cla.filecluster.service.metadata.MetadataDto;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.GENERAL_ADMIN;
import static com.cla.common.domain.dto.security.Authority.Const.TECH_SUPPORT;

@RestController
@RequestMapping("/api/metadata")
public class MetadataApiController {

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MetadataAppService metadataAppService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value="/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<MetadataDto>> getMetadataFileCount(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover metadata export", AuditAction.VIEW, String.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover metadata", AuditAction.VIEW, String.class, null, dataSourceRequest);
        }
        return metadataAppService.getFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/type/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<String>> getMetadataTypeFileCount(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover metadata type export", AuditAction.VIEW, String.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover metadata type", AuditAction.VIEW, String.class, null, dataSourceRequest);
        }
        return metadataAppService.getFileCountsType(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN, TECH_SUPPORT})
    @RequestMapping(value="/retranslate", method = {RequestMethod.GET, RequestMethod.POST})
    public void reTranslateFileMetadata() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.RE_TRANSLATE_FILE_METADATA);
        data.setOpData(new HashMap<>());
        data.setUserOperation(true);
        data.setDescription(messageHandler.getMessage("operation-description.retranslate-metadata"));
        scheduledOperationService.createNewOperation(data);
    }

}
