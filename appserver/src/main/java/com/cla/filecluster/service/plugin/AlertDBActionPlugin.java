package com.cla.filecluster.service.plugin;

import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import com.cla.filecluster.domain.entity.alert.Alert;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.repository.jpa.alert.AlertRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * an action plugin to write an alert to the db
 * Created by: yael
 * Created on: 1/11/2018
 */
@Component
public class AlertDBActionPlugin implements ActionPlugin {

    private Logger logger = LoggerFactory.getLogger(AlertDBActionPlugin.class);

    @Autowired
    private AlertRepository alertRepository;

    @Autowired
    private SequenceIdGenerator sequenceIdGenerator;

    @Override
    @Transactional
    public void handleAction(ActionPluginRequest request) {
        try {
            if (request.getInput() instanceof Alert) {
                Alert alert = (Alert) request.getInput();
                alert.setId(sequenceIdGenerator.nextSequence(SequenceIdGenerator.SequenceType.ALERTS));
                if (alert.getDateCreated() == null) {
                    alert.setDateCreated(System.currentTimeMillis());
                }
                alertRepository.save(alert);
            } else {
                logger.warn("got request of the wrong type {}", request);
            }
        } catch (Exception e) {
            logger.error("error trying to save alert to db", e);
        }
    }
}
