package com.cla.filecluster.service.schedule;

import com.cla.common.domain.dto.schedule.CrawlerPhaseBehavior;
import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleType;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.service.importEntities.SchemaData;
import com.cla.filecluster.service.importEntities.SchemaLoader;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by liora on 01/05/2017.
 */
@Service
public class ScheduleSchemaLoader  extends SchemaLoader<ScheduleItemRow,ScheduleGroupDto> {

    private static Logger logger = LoggerFactory.getLogger(ScheduleSchemaLoader.class);

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    private final static String CSV_HEADER_NAME = "NAME";
    private final static String CSV_HEADER_DESCRIPTION = "DESCRIPTION";
    private final static String CSV_HEADER_ANALYZE_PHASE_BEHAVIOR = "ANALYZE_PHASE_BEHAVIOR";
    private final static String CSV_HEADER_ACTIVE = "ACTIVE";
    private final static String CSV_HEADER_TYPE = "TYPE";
    private final static String CSV_HEADER_SCHEDULE_EVERY = "SCHEDULE_EVERY";
    private final static String CSV_HEADER_SCHEDULE_HOUR = "SCHEDULE_HOUR";
    private final static String CSV_HEADER_SCHEDULE_MINUTES = "SCHEDULE_MINUTES";
    private final static String CSV_HEADER_SCHEDULE_DAYS_OF_THE_WEEK = "SCHEDULE_DAYS_OF_THE_WEEK";
    private final static String CSV_HEADER_DEFAULT = "DEFAULT";
    private final static String CSV_HEADER_SCHEDULE_CRON = "SCHEDULE_CRON";

    private final static String[] REQUIRED_HEADERS = {CSV_HEADER_NAME,CSV_HEADER_TYPE,CSV_HEADER_SCHEDULE_EVERY};

    public ScheduleSchemaLoader() {
        super(REQUIRED_HEADERS);
    }

    @Override
    public SchemaData<ScheduleItemRow> createSchemaInstance() {
        return new SchemaData<ScheduleItemRow>(){
            @Override
            public ScheduleItemRow createImportItemRowInstance() {
                return new ScheduleItemRow();
            }
        };
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected ScheduleItemRow extractRowSpecific(SchemaData<ScheduleItemRow> schemaData, String[] fieldsValues,ArrayList<String> parsingWarningMessages) {
        ScheduleItemRow itemRow = new ScheduleItemRow();

        itemRow.setName(extractValue(schemaData, fieldsValues, CSV_HEADER_NAME, parsingWarningMessages));
        if (itemRow.getName().startsWith("#")) {
            return null;    // Ignore comment
        }
        itemRow.setSchedulingDescription(extractValue(schemaData, fieldsValues, CSV_HEADER_DESCRIPTION, parsingWarningMessages));
        itemRow.setActive(extractBooleanValue(schemaData, fieldsValues, CSV_HEADER_ACTIVE, parsingWarningMessages));
        itemRow.setDaysOfTheWeek(extractMultipleStrings(schemaData, fieldsValues, CSV_HEADER_SCHEDULE_DAYS_OF_THE_WEEK, parsingWarningMessages));
        itemRow.setScheduleType(extractValue(schemaData, fieldsValues, CSV_HEADER_TYPE, ScheduleType.class, parsingWarningMessages));
        itemRow.setAnalyzePhaseBehavior(extractValue(schemaData, fieldsValues, CSV_HEADER_ANALYZE_PHASE_BEHAVIOR, CrawlerPhaseBehavior.class, parsingWarningMessages));
        itemRow.setEvery(extractIntegerValue(schemaData, fieldsValues, CSV_HEADER_SCHEDULE_EVERY, parsingWarningMessages));
        itemRow.setHour(extractIntegerValue(schemaData, fieldsValues, CSV_HEADER_SCHEDULE_HOUR, parsingWarningMessages));
        itemRow.setMinutes(extractIntegerValue(schemaData, fieldsValues, CSV_HEADER_SCHEDULE_MINUTES, parsingWarningMessages));
        itemRow.setDefault(extractBooleanValue(schemaData, fieldsValues, CSV_HEADER_DEFAULT, parsingWarningMessages));
        itemRow.setCustomCronConfig(extractValue(schemaData, fieldsValues, CSV_HEADER_SCHEDULE_CRON, parsingWarningMessages));

        return itemRow;
    }


    @Override
    protected boolean validateItemRow(ScheduleGroupDto scheduleGroupDto, ScheduleItemRow itemRow,
                                      SchemaData<ScheduleItemRow> schemaData, boolean afterAddToDB, boolean createStubs) {

        if (Strings.isNullOrEmpty(scheduleGroupDto.getName())) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("scheduled-group.import.missing-name"));
            return false;
        }

        if (itemRow.getScheduleType() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("scheduled-group.import.missing-type"));
            return false;
        }

        if (itemRow.getEvery() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("scheduled-group.import.missing-every"));
            return false;
        }

        if (!afterAddToDB) { //validate if already exists only before added to db
            try {
                scheduleGroupService.isValidForScheduleGroupCreation(scheduleGroupDto.getName());
				scheduleGroupService.validateSchedulingArgs(scheduleGroupDto.getScheduleConfigDto());
            } catch(Exception e){
				schemaData.addErrorRowItem(itemRow, e.getMessage());
				return false;
			}

            try {
                scheduleGroupService.checkAlreadyExists(scheduleGroupDto.getName(), null);
            } catch (RuntimeException e) {
                schemaData.addDupRowItem(itemRow, messageHandler.getMessage("scheduled-group.import.duplicate-name", Lists.newArrayList(scheduleGroupDto.getName())) );
                return false;
            }
        }

        return true;
    }

    @Override
    protected Boolean loadItemRowToRepository(SchemaData<ScheduleItemRow> schemaData, ScheduleItemRow itemRow, boolean updateDuplicates, boolean createStubs) {
		ScheduleGroupDto scheduleGroupDto = createDtoFromRowItem(itemRow);
        ScheduleGroup scheduledGroupByName = scheduleGroupService.getScheduleGroupByName(scheduleGroupDto.getName());
        if (scheduledGroupByName != null){
            scheduleGroupDto.setId(scheduledGroupByName.getId());
        }
        ScheduleGroupDto resultScheduleGroupDto = updateDuplicates && scheduledGroupByName != null ?
				scheduleGroupService.updateScheduleGroup(scheduleGroupDto) :
				scheduleGroupService.createScheduleGroup(scheduleGroupDto);

		if (resultScheduleGroupDto == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("import.error-insert-row"));
            return false;
        } else {
            return validateItemRow(resultScheduleGroupDto, itemRow, schemaData, true, createStubs);
        }
    }



    @Override
    protected ScheduleGroupDto createDtoFromRowItem(ScheduleItemRow itemRow) {
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName(itemRow.getName());
        scheduleGroupDto.setActive(itemRow.getActive());
        scheduleGroupDto.setSchedulingDescription(itemRow.getSchedulingDescription());
        scheduleGroupDto.setAnalyzePhaseBehavior(itemRow.getAnalyzePhaseBehavior());
        ScheduleConfigDto scheduleConfigDto = new ScheduleConfigDto();
        scheduleConfigDto.setDaysOfTheWeek(itemRow.getDaysOfTheWeek());
        if(itemRow.getMinutes() != null) {
            scheduleConfigDto.setMinutes(itemRow.getMinutes());
        }
        if(itemRow.getEvery() != null) {
            scheduleConfigDto.setEvery(itemRow.getEvery());
        }
        if(itemRow.getHour() != null) {
            scheduleConfigDto.setHour(itemRow.getHour());
        }
        scheduleConfigDto.setScheduleType(itemRow.getScheduleType());
        scheduleConfigDto.setCustomCronConfig(itemRow.getCustomCronConfig());

        scheduleGroupDto.setScheduleConfigDto(scheduleConfigDto);

        return scheduleGroupDto;
    }

}
