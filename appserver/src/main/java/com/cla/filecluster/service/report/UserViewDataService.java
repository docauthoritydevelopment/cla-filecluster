package com.cla.filecluster.service.report;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.DisplayType;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.common.domain.dto.security.Authority;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.report.UserViewData;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.report.UserViewDataRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.filter.SavedFilterAppService;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.service.security.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.rometools.utils.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

/**
 * service for managment of user view data (CRUD)
 *
 * Created by: yael
 * Created on: 3/8/2018
 */
@Service
public class UserViewDataService {

    private Logger logger = LoggerFactory.getLogger(UserViewDataService.class);

    @Autowired
    private UserViewDataRepository userViewDataRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private UserService userService;

    @Autowired
    private SavedFilterAppService savedFilterAppService;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Transactional(readOnly = true)
    public Page<UserViewDataDto> listUserViewData(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.getSelectedItemId() != null ?
                PageRequest.of(0, Integer.MAX_VALUE) : dataSourceRequest.createPageRequestWithSort();
        Page<UserViewData> datas;
        String namingSearchTerm = dataSourceRequest.getNamingSearchTerm();
        if (dataSourceRequest.isByUser() || userViewNotAllowed()) {
            if (namingSearchTerm == null) {
                datas = userViewDataRepository.findByUser(userService.getCurrentUserEntity().getId(), pageRequest);
            }
            else {
                datas = userViewDataRepository.findByUserFilterByTerm(userService.getCurrentUserEntity().getId(), namingSearchTerm.toLowerCase(), pageRequest);
            }
        }
        else {
            datas = userViewDataRepository.findAll(pageRequest);
        }

        return preparePageResult(dataSourceRequest, pageRequest, datas);
    }

    private Page<UserViewDataDto> preparePageResult(DataSourceRequest dataSourceRequest, PageRequest pageRequest, Page<UserViewData> datas) {
        int total;
        List<UserViewDataDto> result;
        if (dataSourceRequest.getSelectedItemId() != null) {
            Long selectedItemId = Long.valueOf(dataSourceRequest.getSelectedItemId());

            int index = findSelectedItemIndex(selectedItemId, datas.getContent());

            logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
            if (index > 0) {
                //calculate page
                int fromPage = (index / dataSourceRequest.getPageSize());
                pageRequest = PageRequest.of(fromPage, dataSourceRequest.getPageSize());
            }
            int fromIndex = (int) pageRequest.getOffset();
            total = datas.getContent().size();
            int toIndex = Math.min(total, fromIndex + dataSourceRequest.getPageSize());
            result = convertUserViewDatas(new ArrayList<>(datas.getContent().subList(fromIndex, toIndex)));
        } else {
            result = convertUserViewDatas(datas.getContent());
            total = (int)datas.getTotalElements();
        }
        return new PageImpl<>(result, pageRequest, total);
    }

    @Transactional(readOnly = true)
    public Page<UserViewDataDto> listUserViewDataByType(DataSourceRequest dataSourceRequest, DisplayType type) {
        PageRequest pageRequest = dataSourceRequest.getSelectedItemId() != null ?
                PageRequest.of(0, Integer.MAX_VALUE) : dataSourceRequest.createPageRequestWithSort();
        Page<UserViewData> datas;
        String namingSearchTerm = dataSourceRequest.getNamingSearchTerm();
        if (dataSourceRequest.isByUser() || userViewNotAllowed()) {
            if (namingSearchTerm == null) {
                datas = userViewDataRepository.findByTypeAndUser(type, userService.getCurrentUserEntity().getId(), pageRequest);
            }
            else {
                datas = userViewDataRepository.findByTypeAndUserFilterByTerm(type, userService.getCurrentUserEntity().getId(), namingSearchTerm.toLowerCase(), pageRequest);
            }
        }
        else {
            if (namingSearchTerm == null) {
                datas = userViewDataRepository.findByType(type, pageRequest);
            }
            else {
                datas = userViewDataRepository.findByTypeFilterByTerm(type, namingSearchTerm.toLowerCase(), pageRequest);
            }
        }

        return preparePageResult(dataSourceRequest, pageRequest, datas);
    }

    private boolean userViewNotAllowed() {
        return  (!userService.isUserSupportAuthority(Authority.MngUserViewData) &&
                !userService.isUserSupportAuthority(Authority.AssignPublicUserViewData));
    }

    @Transactional(readOnly = true) public Page<UserViewDataDto> listUserViewDataWithChildDataByTypeForExport(DataSourceRequest dataSourceRequest, DisplayType type) {
        List<UserViewDataDto> parentList = listUserViewDataByType(dataSourceRequest, type).getContent();
        List<UserViewDataDto> ansList = new ArrayList<>();
        DisplayType childType = DisplayType.DASHBOARD_WIDGET;  // need to add condition if need to support addition types
        for (UserViewDataDto parentUserViewData : parentList) {
            DataSourceRequest childSourceRequest = new DataSourceRequest();
            childSourceRequest.setPageSize(Integer.MAX_VALUE);
            List<UserViewDataDto> childList = listUserViewDataByTypeAndContainerId(childSourceRequest, childType, parentUserViewData.getId()).getContent();
            if (childList.size()>0){
                ansList.addAll(childList);
            }
            else {
                ansList.add(parentUserViewData);
            }
        }
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        return new PageImpl<>(ansList, pageRequest, ansList.size());
    }

    @Transactional(readOnly = true)
    public List<UserViewDataDto> listUserViewDataByFilterId(Long filterId) {
        List<UserViewData> datas = userViewDataRepository.findByFilterId(filterId);
        return convertUserViewDatas(datas);
    }

    private int findSelectedItemIndex(Long selectedItemId, List<UserViewData> all) {
        int i = 0;
        for (UserViewData one : all) {
            if (one.getId().equals(selectedItemId)) {
                return i;
            }
            i++;
        }
        return i;
    }

    @Transactional(readOnly = true)
    public Page<UserViewDataDto> listUserViewDataWithoutContainerByType(DataSourceRequest dataSourceRequest, DisplayType type) {
        PageRequest pageRequest = dataSourceRequest.getSelectedItemId() != null ?
                PageRequest.of(0, Integer.MAX_VALUE) : dataSourceRequest.createPageRequestWithSort();
        Page<UserViewData> datas = userViewDataRepository.findByUserAndTypeWithoutContainer(userService.getCurrentUserEntity().getId(), type, pageRequest);
        return preparePageResult(dataSourceRequest, pageRequest, datas);
    }

    @Transactional(readOnly = true)
    public Page<UserViewDataDto> listUserViewDataByContainerId(DataSourceRequest dataSourceRequest, Long containerId) {
        PageRequest pageRequest = dataSourceRequest.getSelectedItemId() != null ?
                PageRequest.of(0, Integer.MAX_VALUE) : dataSourceRequest.createPageRequestWithSort();

        Page<UserViewData> datas;
        String namingSearchTerm = dataSourceRequest.getNamingSearchTerm();
        if (dataSourceRequest.isByUser() || (!userService.isUserSupportAuthority(Authority.MngUserViewData) && !userService.isUserSupportAuthority(Authority.AssignPublicUserViewData))) {
            if (namingSearchTerm == null) {
                datas = userViewDataRepository.findByUserAndContainerId(userService.getCurrentUserEntity().getId(), containerId, pageRequest);
            }
            else {
                datas = userViewDataRepository.findByUserFilterByTermAndContainerId(userService.getCurrentUserEntity().getId(), namingSearchTerm.toLowerCase(), containerId, pageRequest);
            }
        }
        else {
            datas = userViewDataRepository.findByContainerId(containerId, pageRequest);
        }

        return preparePageResult(dataSourceRequest, pageRequest, datas);
    }

    @Transactional(readOnly = true)
    public Page<UserViewDataDto> listUserViewDataByTypeAndContainerId(DataSourceRequest dataSourceRequest, DisplayType type, Long containerId) {
        PageRequest pageRequest = dataSourceRequest.getSelectedItemId() != null ?
                PageRequest.of(0, Integer.MAX_VALUE) : dataSourceRequest.createPageRequestWithSort();
        Page<UserViewData> datas;
        String namingSearchTerm = dataSourceRequest.getNamingSearchTerm();
        if (dataSourceRequest.isByUser() || (!userService.isUserSupportAuthority(Authority.MngUserViewData) && !userService.isUserSupportAuthority(Authority.AssignPublicUserViewData))) {
            if (namingSearchTerm == null) {
                datas = userViewDataRepository.findByTypeAndUserAndContainerId(type, userService.getCurrentUserEntity().getId(), containerId, pageRequest);
            }
            else {
                datas = userViewDataRepository.findByTypeAndUserFilterByTermAndContainerId(type, userService.getCurrentUserEntity().getId(), namingSearchTerm.toLowerCase(), containerId, pageRequest);
            }
        }
        else {
            if (namingSearchTerm == null) {
                datas = userViewDataRepository.findByTypeAndContainerId(type, containerId, pageRequest);
            }
            else {
                datas = userViewDataRepository.findByTypeFilterByTermAndContainerId(type, namingSearchTerm.toLowerCase(), containerId, pageRequest);
            }
        }

        return preparePageResult(dataSourceRequest, pageRequest, datas);
    }


    @Transactional(readOnly = true)
    public UserViewDataDto getById(Long id) {
        User owner = userService.getCurrentUserEntity();
        if (userService.isUserSupportAuthority(Authority.MngUserViewData)) {
            return userViewDataRepository.findById(id).map(UserViewDataService::convertUserViewData).orElse(null);
        } else if (owner != null) {
            return userViewDataRepository.findEligibaleById(id, owner.getId()).map(UserViewDataService::convertUserViewData).orElse(null);
        } else {
            return null;
        }
    }

    @Transactional(readOnly = true)
    public Optional<UserViewDataDto> getFirstByNameAndType(String name, DisplayType type) {
        User owner = userService.getCurrentUserEntity();
        List<UserViewData> userViewDataByNameList = userViewDataRepository.findByNameUserAndType(name, type, owner.getId());
        if (Lists.isEmpty(userViewDataByNameList)) {
            return Optional.empty();
        }
        return Optional.of(convertUserViewData(userViewDataByNameList.get(0)));
    }

    @Transactional
    public UserViewDataDto cloneById(Long id, String newName, boolean removeContainer) {
        UserViewDataDto userViewDto = getById(id);
        if (userViewDto == null) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        userViewDto.setName(newName);
        userViewDto.setId(null);
        userViewDto.setGlobalFilter(false);
        if (removeContainer) {
            userViewDto.setContainerId(null);
        }
        UserViewDataDto savedUserView = createUserViewData(userViewDto);
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setPage(1);
        dataSourceRequest.setPageSize(Integer.MAX_VALUE);
        List<UserViewDataDto> childUserViewDataList = listUserViewDataByContainerId(dataSourceRequest, id).getContent();
        childUserViewDataList.forEach(childUserViewData -> {
            childUserViewData.setContainerId(savedUserView.getId());
            childUserViewData.setId(null);
            createUserViewData(childUserViewData);
        });
        return savedUserView;
    }

    @Transactional
    public UserViewDataDto createUserViewData(UserViewDataDto newDto) {
        validateNameNotEmpty(newDto.getName());
        // validateNameNotExist(newDto.getId(), newDto.getName());
        User owner = userService.getCurrentUserEntity();

        UserViewData viewData = new UserViewData();
        if (newDto.getContainerId() != null) {
            viewData.setContainer(validateAndGetContainerId(newDto.getContainerId()));
        }

        viewData.setDisplayData(newDto.getDisplayData());
        viewData.setDisplayType(newDto.getDisplayType());
        viewData.setFilter(savedFilterAppService.convertSavedFilter(newDto.getFilter()));
        viewData.setName(newDto.getName());
        if (owner == null) {
            logger.debug("Create Global UserViewData by name {}",newDto.getName());
        }
        else {
            viewData.setOwner(owner);
            logger.debug("Create UserViewData by name {} for user {} ",newDto.getName(),owner.getName());
        }

        if (newDto.getScope() != null) {
            BasicScope basicScope = newDto.getScope() == null ? null : scopeService.getOrCreateScope(newDto.getScope());
            viewData.setScope(basicScope);
        }
        viewData.setGlobalFilter(newDto.isGlobalFilter());

        UserViewData save = userViewDataRepository.save(viewData);
        return convertUserViewData(save);
    }

    @Transactional
    public UserViewDataDto updateUserViewData(UserViewDataDto newDto) {
        validatePermissions(newDto.getOwner());
        validateNameNotEmpty(newDto.getName());
        //validateNameNotExist(newDto.getId(), newDto.getName());

        Optional<UserViewData> optionalUserViewData = userViewDataRepository.findById(newDto.getId());
        if (!optionalUserViewData.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        logger.debug("Update UserViewData by name {} ", newDto.getName());
        UserViewData viewData = optionalUserViewData.get();
        viewData.setDisplayData(newDto.getDisplayData());
        viewData.setDisplayType(newDto.getDisplayType());
        viewData.setFilter(savedFilterAppService.convertSavedFilter(newDto.getFilter()));
        viewData.setName(newDto.getName());
        viewData.setGlobalFilter(newDto.isGlobalFilter());

        if (newDto.getScope() != null) {
            BasicScope basicScope = newDto.getScope() == null ? null : scopeService.getOrCreateScope(newDto.getScope());
            viewData.setScope(basicScope);
        }
        viewData.setGlobalFilter(newDto.isGlobalFilter());

        UserViewData updatedChart = userViewDataRepository.save(viewData);
        return convertUserViewData(updatedChart);
    }

    @Transactional
    public void deleteUserViewData(long id) {
        UserViewDataDto current = getById(id);
        if (current == null) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        validatePermissions(current.getOwner());

        userViewDataRepository.deleteByContainerId(id);
        userViewDataRepository.deleteById(id);
    }


    private static List<UserViewDataDto> convertUserViewDatas(Collection<UserViewData> viewDatas) {
        List<UserViewDataDto> result = new ArrayList<>();
        for (UserViewData viewData : viewDatas) {
            result.add(convertUserViewData(viewData));
        }
        return result;
    }

    private static UserViewDataDto convertUserViewData(UserViewData viewData) {
        if (viewData == null) return null;
        UserViewDataDto viewDataDto = new UserViewDataDto();
        viewDataDto.setId(viewData.getId());
        viewDataDto.setName(viewData.getName());
        if (viewData.getContainer() != null) {
            viewDataDto.setContainerId(viewData.getContainer().getId());
        }
        viewDataDto.setDisplayType(viewData.getDisplayType());
        viewDataDto.setDisplayData(viewData.getDisplayData());
        viewDataDto.setFilter(SavedFilterAppService.convertSavedFilter(viewData.getFilter()));
        viewDataDto.setOwner(UserService.convertUser(viewData.getOwner(), false));
        viewDataDto.setScope(AssociationUtils.convertBasicScopeToDto(viewData.getScope()));
        //viewDataDto.setGlobalFilter(viewData.getOwner() == null);
        viewDataDto.setGlobalFilter(viewData.isGlobalFilter());
        return viewDataDto;
    }


    FilterDescriptor getFilterDescriptionByJsonString(String jsonString) {
        try {
            return objectMapper.readValue(jsonString, FilterDescriptor.class);
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private UserViewData validateAndGetContainerId(Long containerId) {
        return userViewDataRepository.findById(containerId).orElseThrow(()->
                new BadParameterException(messageHandler.getMessage("user-view-data.container.invalid"), BadRequestType.UNSUPPORTED_VALUE));
    }


    private void validateNameNotEmpty(String name) {
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            throw new BadParameterException(messageHandler.getMessage("user-view-data.name.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void validatePermissions(UserDto owner) {
        if (!userService.isUserSupportAuthority(Authority.MngUserViewData)) {
            UserDto currentUser = userService.getCurrentUser();
            if (currentUser == null || !owner.getId().equals(currentUser.getId())) {
                throw new BadRequestException(messageHandler.getMessage("operation.user-not-allowed"), BadRequestType.UNAUTHORIZED);
            }
        }
    }
}
