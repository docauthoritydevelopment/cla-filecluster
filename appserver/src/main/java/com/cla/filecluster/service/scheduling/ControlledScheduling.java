package com.cla.filecluster.service.scheduling;

/**
 * Created by: yael
 * Created on: 7/16/2019
 */
public interface ControlledScheduling {
    boolean isSchedulingActive();
    void setSchedulingActive(boolean schedulingActive);
}
