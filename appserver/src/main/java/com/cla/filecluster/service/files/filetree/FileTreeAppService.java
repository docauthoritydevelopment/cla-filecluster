package com.cla.filecluster.service.files.filetree;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.crawler.RootFoldersAggregatedSummaryInfo;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.file.ConstFileTypeCategory;
import com.cla.common.domain.dto.file.FileTypeCategoryDto;
import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.filecluster.domain.dto.ActiveScanQueryResult;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.filetype.Extension;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.service.TagRelatedBaseService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.*;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.media.MediaSpecificAppService;
import com.cla.filecluster.service.media.MicrosoftMediaAppService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.exec.*;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;

import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Service
public class FileTreeAppService extends TagRelatedBaseService {

    private static final Logger logger = LoggerFactory.getLogger(FileTreeAppService.class);
    private static final int REBUILD_PAGE_SIZE = 1000;

    @Value("${subprocess:false}")
    private boolean subProcess;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private MicrosoftMediaAppService microsoftMediaAppService;

    @Autowired
    private ActiveScanQuery activeScanQuery;

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileTypeCategoryService fileTypeCategoryService;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private FileCatAppService fileCatAppService;


    private Map<MediaType, MediaSpecificAppService> mediaAppServiceMap;

    private List<ServerResourceDto> baseServerRootListCache = Lists.newArrayList();

    private Future<Void> serversListTaskState;

    @PostConstruct
    public void initMediaAppServiceMap() {
        mediaAppServiceMap = Maps.newEnumMap(MediaType.class);
        mediaAppServiceMap.put(MediaType.SHARE_POINT, microsoftMediaAppService);
        mediaAppServiceMap.put(MediaType.ONE_DRIVE, microsoftMediaAppService);
    }

    @Transactional(readOnly = true)
    public Page<RootFolderDto> listAllRootFoldersInDefaultDocStore(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Long defaultDocStore = docStoreService.getDefaultDocStoreId();
        FilterDescriptor filter = dataSourceRequest.getFilter();
        Page<RootFolder> response = docStoreService.listRootFolders(defaultDocStore, pageRequest, filter);
        return DocStoreService.convertRootFoldersPage(pageRequest, defaultDocStore, response);
    }

    public List<ServerResourceDto> listServerFolders(String baseFolder, Long connectionId) {
        if (baseFolder == null || baseFolder.isEmpty()) {
            return listServerRoots();
        } else if (connectionId != null) {
            return listRemoteFolders(baseFolder, connectionId);
        } else {
            List<ServerResourceDto> result = new ArrayList<>();
            Matcher matcher = Pattern.compile("//([^/]+)/?").matcher(baseFolder.replace("\\", "/"));
            if (matcher.find()) {
                //remote server passed
                try {
                    logger.info("List servers folders for server: {}", matcher.group(1));
                    List<ServerResourceDto> subFolders = executeNetView(matcher.group(1));
                    result.addAll(subFolders);
                } catch (IOException e) {
                    logger.error("IO issue while trying to get server shared for URL : {}", baseFolder, e);
                    throw new BadRequestException(messageHandler.getMessage("list-folders.bad-url",
                            Lists.newArrayList(e.getMessage())), BadRequestType.UNSUPPORTED_VALUE);
                } catch (InterruptedException e) {
                    logger.error("Timed out while trying to get server shares for URL : {}", baseFolder, e);
                    throw new BadRequestException(messageHandler.getMessage("list-folders.timeout",
                            Lists.newArrayList(e.getMessage())), BadRequestType.OPERATION_TIMED_OUT);
                }
            } else {
                try (Stream<Path> list = Files.list(Paths.get(baseFolder))) {
                    result = list
                            .filter(Files::isDirectory)
                            .map(f -> new ServerResourceDto(f.toString(), f.getFileName().toString()))
                            .collect(Collectors.toList());
                } catch (FileSystemException e) {
                    logger.warn("FileSystemException while getting list of subFolders from folder {} ({})", baseFolder, e.getMessage());
                    throw new BadRequestException(messageHandler.getMessage("list-folders.sub-folders-failure",
                            Lists.newArrayList(baseFolder, e.getMessage())), BadRequestType.TEST_FAILED);
                } catch (IOException e) {
                    logger.warn("Failed to get list of subFolders from folder " + baseFolder, e);
                    throw new BadRequestException(messageHandler.getMessage("list-folders.sub-folders-failure",
                            Lists.newArrayList(baseFolder, e.getMessage())), BadRequestType.OPERATION_FAILED);
                }
            }
            return result;
        }
    }

    private List<ServerResourceDto> listRemoteFolders(String baseFolder, Long connectionId) {
        MediaConnectionDetailsDto connectionDetails = mediaConnectionDetailsService.getMediaConnectionDetailsDtoByConnectionId(connectionId);
        return mediaAppServiceMap.get(connectionDetails.getMediaType())
                .listFoldersUnderPath(connectionDetails, Optional.ofNullable(baseFolder), null, false);
    }

    private List<ServerResourceDto> listServerRoots() {
        List<ServerResourceDto> result = new ArrayList<>();
        //List drives
        File[] roots = File.listRoots();
        for (File root : roots) {
            result.add(new ServerResourceDto(root.getPath(), root.getPath()));
        }
        if (baseServerRootListCache != null) {
            result.addAll(baseServerRootListCache);
        }
        populateServersListCache();
        return result;
    }

    @PostConstruct
    public void populateServersListCache() {
        if (subProcess) {
            logger.debug("subprocess - Don't populate servers list cache");
            return;
        }
        synchronized (this) {
            //Check we are not in the middle of cache fetch.
            if (serversListTaskState == null || serversListTaskState.isDone()) {
                serversListTaskState = executionManagerService.startAsyncTask(() -> {
                    try {
                        baseServerRootListCache = executeNetView(null);
                    } catch (Exception e) {
                        logger.error("Failed to get servers from domain", e);
                    }
                    return null;
                });
            }
        }
    }

    private List<ServerResourceDto> executeNetView(String shareName) throws IOException, InterruptedException {
        List<ServerResourceDto> result = new ArrayList<>();
        String commandLine = "net view";
        if (shareName != null) {
            commandLine = "net view " + shareName;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        executeCommandLine(commandLine, outputStream);
        String output = new String(outputStream.toByteArray());
        boolean resultStart = false;
        for (String line : StringUtils.split(output, '\n')) {
            if (line.startsWith("The command completed successfully")) {
                break;
            }
            if (resultStart) {
                String folder = line.replaceFirst("\\s.+", "").replace("\r", "");
                result.add(new ServerResourceDto("\\\\" + shareName + "\\" + folder, folder));
            }
            if (line.startsWith("------------------")) {
                resultStart = true;
            }
        }
        if (logger.isDebugEnabled()) {
            if (result.isEmpty()) {
                logger.debug("Found no Net view shares");
            } else {
                logger.debug("Net view on {} results in: {}", shareName, result.stream()
                        .map(ServerResourceDto::getName).collect(Collectors.joining(",")));
            }
        }
        return result;
    }


    private void executeCommandLine(String line, ByteArrayOutputStream outputStream) throws IOException, InterruptedException {
        DefaultExecutor executor = new DefaultExecutor();
        CommandLine cmdLine = CommandLine.parse(line);
        ExecuteWatchdog watchdog = new ExecuteWatchdog(30000);
        executor.setWatchdog(watchdog);
        DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        executor.setStreamHandler(streamHandler);
        executor.execute(cmdLine, resultHandler);
        resultHandler.waitFor();
    }

    @Transactional(readOnly = true)
    public Page<DocFolderDto> listAllFoldersInDefaultDocStore(DataSourceRequest dataSourceRequest) {
        Long defaultDocStore = docStoreService.getDefaultDocStoreId();
        Page<DocFolderDto> result = docFolderService.listAllFolders(defaultDocStore, dataSourceRequest);
        logger.debug("List All folders in default doc Store - return {} files in page", result.getNumberOfElements());
        return result;
    }

    @Transactional(readOnly = true)
    public Page<DocFolderDto> listFoldersFromSubFolder(Long folderId, DataSourceRequest dataSourceRequest) {
        DocFolder baseFolder = docFolderService.findById(folderId);
        Page<DocFolderDto> result = docFolderService.listFolders(baseFolder, dataSourceRequest);
        logger.debug("List folders in from base Folder {} - return {} folders in page",
                baseFolder.getPath(), result.getNumberOfElements());
        return result;
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountFolderDto> findFoldersFileCount(DataSourceRequest dataSourceRequest) {
        logger.debug("list Folder File Counts With File Filters");
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(
                dataSourceRequest, CatFileFieldType.FOLDER_ID);
        solrFacetSpecification.setMinCount(1);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
        PageRequest pageRequest = solrFacetQueryResponse.getPageRequest();
        logger.debug("list file facets by Solr field Folder Id returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        final FacetPage<AggregationCountFolderDto> facetPage = convertFacetFieldResultToFolderDto(
                solrFacetQueryResponse, pageRequest, CatFileFieldType.FOLDER_ID);
        facetPage.setTotalElements(fileCatAppService.getTotalFacetCount(dataSourceRequest, CatFileFieldType.FOLDER_ID));
        return facetPage;
    }

    public SolrFacetQueryResponse runFacetQuery(DataSourceRequest dataSourceRequest,
                                                SolrSpecification solrFacetSpecification,
                                                Function<FacetField.Count, String> idExtractor) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        String selectedItemId = dataSourceRequest.getSelectedItemId();
        return fileCatService.runFacetQuery(solrFacetSpecification, idExtractor, pageRequest, selectedItemId);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountFolderDto> findFoldersFileCountTreeRoot(DataSourceRequest dataSourceRequest) {
        logger.debug("list Folder File Counts With File Filters - on the tree root node");
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(
                dataSourceRequest, CatFileFieldType.FOLDER_IDS);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix("0.");
        SolrFacetQueryResponse solrFacetQueryResponse = runFacetQuery(
                dataSourceRequest, solrFacetSpecification, c -> StringUtils.substringAfter(c.getName(), "."));
        logger.debug("list file facets by Solr field Folder Id returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        return convertFacetFieldResultToFolderDto(solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(),
                CatFileFieldType.FOLDER_IDS, c -> Long.valueOf(StringUtils.substringAfter(c.getName(), ".")));
    }

    /**
     * @param dataSourceRequest data source request
     * @param parentFolderId    parent folder ID
     * @return direct folder children of the parentFolderId
     */
    @Transactional(readOnly = true)
    public FacetPage<AggregationCountFolderDto> findFoldersFileCountTreeNodeChildren(DataSourceRequest dataSourceRequest, long parentFolderId) {
        DocFolder parentFolder = docFolderService.findById(parentFolderId);
        logger.debug("list Folder File Counts With File Filters - on a tree node direct children");
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(
                dataSourceRequest, CatFileFieldType.FOLDER_IDS);
        solrFacetSpecification.setMinCount(1);
        //Only fetch direct children - depth wize
        solrFacetSpecification.setFacetPrefix(parentFolder.getDepthFromRoot() + 1 + ".");
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.FOLDER_IDS.getSolrName() + ":" + convertFolderIdToSolrValue(parentFolder));
        SolrFacetQueryResponse solrFacetQueryResponse = runFacetQuery(
                dataSourceRequest, solrFacetSpecification, c -> StringUtils.substringAfter(c.getName(), "."));
        logger.debug("list file facets by Solr field Folder Id returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        return convertFacetFieldResultToFolderDto(solrFacetQueryResponse,
                solrFacetQueryResponse.getPageRequest(), CatFileFieldType.FOLDER_IDS,
                c -> Long.valueOf(StringUtils.substringAfter(c.getName(), ".")));
    }

    private String convertFolderIdToSolrValue(DocFolder parentFolder) {
        return parentFolder.getDepthFromRoot() + "." + parentFolder.getId();
    }

    private FacetPage<AggregationCountFolderDto> convertFacetFieldResultToFolderDto(SolrFacetQueryResponse solrFacetQueryResponse,
                                                                                    PageRequest pageRequest,
                                                                                    CatFileFieldType requestFacetType) {
        return convertFacetFieldResultToFolderDto(solrFacetQueryResponse, pageRequest,
                requestFacetType, c -> Long.valueOf(c.getName()));
    }

    private Map<Long, Long> getFoldersDirectFileNumber(Collection<Long> folderIds) {
        return fileCatService.getFoldersDirectFileNumber(folderIds);
    }

    private Map<Long, Long> getFoldersAllFileNumber(Collection<DocFolder> folders) {
        return fileCatService.getFoldersAllFileNumber(folders);
    }

    private FacetPage<AggregationCountFolderDto> convertFacetFieldResultToFolderDto(
            SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, CatFileFieldType requestFacetType,
            Function<FacetField.Count, Long> idExtractor) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set
        List<Long> ids = facetField.getValues().stream().map(idExtractor).collect(Collectors.toList());

        Map<Long, Long> facetCount = new LinkedHashMap<>();
        for (FacetField.Count count : facetField.getValues()) {
            Long docFolderId = idExtractor.apply(count);
            facetCount.put(docFolderId, count.getCount());
        }

        return convertFacetFieldResultToFolderDto(ids, facetCount, pageRequest, requestFacetType, solrFacetQueryResponse.getNumFound());
    }

    private FacetPage<AggregationCountFolderDto> convertFacetFieldResultToFolderDto(
            List<Long> ids, Map<Long, Long> facetCount, PageRequest pageRequest, CatFileFieldType requestFacetType, long numFound) {

        //Fetch all group ids from the database
        List<DocFolder> folders = docFolderService.findByIds(ids);

        Set<Long> rootFolderIds = folders.stream()
                .map(DocFolder::getRootFolderId)
                .collect(Collectors.toSet());

        Map<Long, RootFolder> rfs = docStoreService.getRootFoldersCached(rootFolderIds).stream()
                .collect(Collectors.toMap(RootFolder::getId, r -> r));

        return convertFacetFieldResultToFolderDto(folders, rfs,
                ids, facetCount, pageRequest, requestFacetType, numFound);
    }

    private FacetPage<AggregationCountFolderDto> convertFacetFieldResultToFolderDto(List<DocFolder> folders, Map<Long, RootFolder> rfs,
            List<Long> ids, Map<Long, Long> facetCount, PageRequest pageRequest, CatFileFieldType requestFacetType, long numFound) {

        Map<Long, Long> directFileNumsForFolder = getFoldersDirectFileNumber(ids);
        Map<Long, Long> subFoldersForFolder = docFolderService.getFoldersSubFoldersNumber(ids);
        Map<Long, Long> fileNumAll = getFoldersAllFileNumber(folders);

        //Convert database results to a map (we need that to keep consistent order in results)
        Map<Long, DocFolder> docFolderMap = folders.stream().collect(Collectors.toMap(DocFolder::getId, g -> g));
        List<AggregationCountFolderDto> content = new ArrayList<>();
        logger.debug("convert Facet Field result for {} elements to DocFolderDto. DB returned {} elements", ids.size(), folders.size());
        long pageAggCount = 0;

        AssociationRelatedData foldersAssociations = associationsService.getAssociationRelatedData(folders);

        for (Map.Entry<Long, Long> entry : facetCount.entrySet()) {
            try {
                Long docFolderId = entry.getKey();
                Long count = entry.getValue();
                DocFolder folder = docFolderMap.get(docFolderId);

                if (folder == null) {
                    logger.error("DocFolder id {} exists in Solr but is missing from the repository", docFolderId);
                    continue;
                }

                RootFolder rf = rfs.get(folder.getRootFolderId());
                DocFolderDto docFolderDto = DocFolderService.convertDocFolder(
                        folder, false, false, rf, null, null, null);
                if (docFolderDto == null) {
                    logger.error("DocFolder id {} exists in Solr but is missing from the repository", docFolderId);
                    continue;
                }

                if (folder.isDeleted()) {
                    logger.error("DocFolder id {} exists in Solr but is deleted in repository", docFolderId);
                }

                AggregationCountFolderDto item;
                switch (requestFacetType) {
                    case FOLDER_IDS:
                        item = new AggregationCountFolderDto(docFolderDto, AggregationCountFolderDto.UNKNOWN, count.intValue());
                        break;

                    case FOLDER_ID:
                    default:
                        item = new AggregationCountFolderDto(docFolderDto, count.intValue(), AggregationCountFolderDto.UNKNOWN);
                        break;
                }
                Long sub = subFoldersForFolder == null ? null : subFoldersForFolder.get(docFolderDto.getId());
                docFolderDto.setNumOfSubFolders(sub == null ? 0 : sub.intValue());
                Long allFiles = (fileNumAll == null ? null : fileNumAll.get(docFolderDto.getId()));
                docFolderDto.setNumOfAllFiles(allFiles == null ? 0 : allFiles.intValue());
                Long direct = directFileNumsForFolder == null ? null : directFileNumsForFolder.get(docFolderDto.getId());
                docFolderDto.setNumOfDirectFiles(direct == null ? 0 : direct.intValue());

                List<AssociationEntity> tagAssociations = foldersAssociations.getPerEntityAssociations().get(docFolderDto.getId());
                List<AssociationEntity> docTypeAssociations = AssociationsService.getDocTypeAssociation(tagAssociations);
                if (tagAssociations != null && !tagAssociations.isEmpty()) {
                    docFolderDto.setFileTagAssociations(
                            AssociationUtils.collectFileTagAssociations(foldersAssociations.getScopes(),
                                    foldersAssociations.getEntities(), tagAssociations, docTypeAssociations, true));
                    docFolderDto.setFileTagDtos(AssociationUtils.collectFileTags(tagAssociations,
                            foldersAssociations.getEntities(), docFolderDto.getFileTagAssociations(), true));
                    docFolderDto.setAssociatedBizListItems(AssociationUtils.collectAssociatedBizListItemsFromFolder(
                            tagAssociations, foldersAssociations.getEntities()));
                    docFolderDto.setFunctionalRoles(AssociationUtils.collectAssociatedFunctionalRolesFromFolder(
                            tagAssociations, foldersAssociations.getEntities()));
                    docFolderDto.setDepartmentAssociationDtos(AssociationUtils.collectDepartmentAssociations(
                            tagAssociations, foldersAssociations.getEntities()));
                    docFolderDto.setDepartmentDto(AssociationUtils.getDepartmentDto(docFolderDto.getDepartmentAssociationDtos()));
                }

                if (docTypeAssociations != null && !docTypeAssociations.isEmpty()) {
                    List<DocTypeAssociationDto> docTypeAssociationsItems = AssociationUtils.convertFromAssociationEntity(
                            foldersAssociations.getEntities(), foldersAssociations.getScopes(), FileTagSource.FOLDER_EXPLICIT, docTypeAssociations);
                    docFolderDto.setDocTypeDtos(AssociationUtils.collectDocTypesDto(docTypeAssociationsItems));
                    docFolderDto.setDocTypeAssociations(AssociationUtils.collectDocTypeAssociations(
                            docTypeAssociations, foldersAssociations.getScopes(), FileTagSource.FOLDER_EXPLICIT, foldersAssociations.getEntities()));
                    AssociationUtils.addFileTagAssociationsByDocTypeDto(
                            docTypeAssociationsItems, docFolderDto.getFileTagAssociations(), docFolderDto.getFileTagDtos());
                }
                refineAssociations(docFolderDto, null);

                content.add(item);
                pageAggCount += count;
            } catch (Exception e) {
                logger.warn("problem handling folder {} in facet", entry.getKey(), e);
            }
        }

        FacetPage<AggregationCountFolderDto> resultPage = new FacetPage<>(content, pageRequest, pageAggCount, numFound);
        resultPage.setTotalElements(numFound);
        return resultPage;
    }

    @Transactional(readOnly = true)
    public DocFolderDto findDocFolder(long id, boolean fillStats) {
        DocFolderDto folder = docFolderService.getByIdWithAssociations(id);

        if (fillStats) {
            List<Long> ids = Lists.newArrayList(id);
            Map<Long, Long> directFileNumsForFolder = getFoldersDirectFileNumber(ids);
            Map<Long, Long> subFoldersForFolder = docFolderService.getFoldersSubFoldersNumber(ids);
            DocFolder df = new DocFolder();
            df.setId(id);
            df.setDepthFromRoot(folder.getDepthFromRoot());
            Map<Long, Long> fileNumAll = getFoldersAllFileNumber(Lists.newArrayList(df));

            folder.setNumOfDirectFiles(directFileNumsForFolder.getOrDefault(id, 0L).intValue());
            folder.setNumOfSubFolders(subFoldersForFolder.getOrDefault(id, 0L).intValue());
            folder.setNumOfAllFiles(fileNumAll.getOrDefault(id, 0L).intValue());
        }

        folder.setRootFolderSharePermissions(sharePermissionService.getRootFolderReadAllowSharePermissions(folder.getRootFolderId(), folder.getMediaType()));

        return folder;
    }

    @Transactional(readOnly = true)
    public DocFolderDto findDocFolder(Long rootFolderId, String folderPath) {
        //    String candidatePath = FileNamingUtils.convertPathToUniversalString(folderPath);
        DocFolder docFolder = docFolderService.getDocFolderByFolderPath(rootFolderId, folderPath);
        logger.debug("Fetched docFolder {} by path {}", docFolder, folderPath);
        if (docFolder == null) {
            return null;
        }
        AssociationRelatedData folderAssociations = associationsService.getAssociationRelatedData(docFolder);
        return DocFolderService.convertDocFolder(
                docFolder, true, true, null, folderAssociations.getAssociations(),
                folderAssociations.getScopes(), folderAssociations.getEntities());
    }

    private int findSelectedItemIndexRF(Long selectedItemId, List<RootFolder> all) {
        int i = 0;
        for (RootFolder one : all) {
            if (one.getId().equals(selectedItemId)) {
                return i;
            }
            i++;
        }
        return i;
    }

    public ActiveScanQueryResult activeScanSummaryInfo(DataSourceRequest dataSourceRequest, Map<String, String> params) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        List<RootFolderRunSummaryInfo> res = activeScanQuery.run(params, pageRequest);

        Set<Long> dataCenters = res.stream()
                .map(r -> r.getRootFolderSummaryInfo().getRootFolderDto().getCustomerDataCenterDto().getId())
                .collect(Collectors.toSet());

        Map<Long, Boolean> dataCentersReady = Maps.newHashMap();
        dataCenters.forEach(customerDataCenterId -> {
            List<Long> activeMediaProcessors = sysComponentService.getActiveMediaProcessors(customerDataCenterId, true);
            dataCentersReady.put(customerDataCenterId, !activeMediaProcessors.isEmpty());
        });

        res.forEach(r -> {
            Long dc = r.getRootFolderSummaryInfo().getRootFolderDto().getCustomerDataCenterDto().getId();
            r.getRootFolderSummaryInfo().setCustomerDataCenterReady(dataCentersReady.getOrDefault(dc, false));
        });

        RootFoldersAggregatedSummaryInfo stateSummaryInfo = activeScanQuery.getByState(params);
        int count = stateSummaryInfo.getTotalRootFoldersCount().intValue();
        Page rfs = new PageImpl<>(res, pageRequest, count);

        //noinspection unchecked
        return new ActiveScanQueryResult(rfs, stateSummaryInfo);
    }

    @Transactional(readOnly = true)
    public Page<RootFolderSummaryInfo> listRootFoldersSummaryInfo(DataSourceRequest dataSourceRequest, boolean getRules, boolean hideDeleted) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Long defaultDocStoreAsDto = docStoreService.getDefaultDocStoreId();
        Page<RootFolder> rootFolders;

        if (dataSourceRequest.getSelectedItemId() != null) {
            List<RootFolder> rootFoldersAll = docStoreService.listRootFolders(
                    defaultDocStoreAsDto, dataSourceRequest.getFilter(), dataSourceRequest.getNamingSearchTerm(), hideDeleted);
            Long selectedItemId = Long.valueOf(dataSourceRequest.getSelectedItemId());
            int index = findSelectedItemIndexRF(selectedItemId, rootFoldersAll);
            if (index > 0) {
                int fromPage = (index / pageRequest.getPageSize());
                pageRequest = PageRequest.of(fromPage, pageRequest.getPageSize());
            }
            int fromIndex = (int) pageRequest.getOffset();
            int total = rootFoldersAll.size();
            int toIndex = Math.min(total, fromIndex + pageRequest.getPageSize());
            List<RootFolder> content = new ArrayList<>(rootFoldersAll.subList(fromIndex, toIndex));
            rootFolders = new PageImpl<>(content, pageRequest, total);
        } else {
            rootFolders = docStoreService.listRootFolders(defaultDocStoreAsDto, pageRequest, dataSourceRequest.getFilter(),
                    dataSourceRequest.getNamingSearchTerm(), hideDeleted);
        }

        Page<RootFolderSummaryInfo> result = fileCrawlerExecutionDetailsService.convertRootFoldersToSummaryInfo(pageRequest,
                rootFolders, getRules);

        if (!result.getContent().isEmpty()) {
            result.getContent().forEach(rootFolderData -> {
                RootFolderDto rfDto = rootFolderData.getRootFolderDto();
                if (rfDto.getDepartmentId() != null) {
                    DepartmentDto departmentDto = departmentService.getByIdCached(rfDto.getDepartmentId());
                    rfDto.setDepartmentName(departmentDto.getName());
                    rootFolderData.setDepartmentDto(departmentDto);
                }
                rootFolderData.getRootFolderDto().setRootFolderSharePermissions(
                        sharePermissionService.getRootFolderSharePermissions(rfDto.getId(), rfDto.getMediaType()));
            });
        }

        return result;
    }

    @Transactional(readOnly = true)
    public RootFolderSummaryInfo getRootFolderSummaryInfo(Long rootFolderId) {
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        if (rootFolder == null) {
            return null;
        }
        RootFolderSummaryInfo result = fileCrawlerExecutionDetailsService.convertRootFolderToSummaryInfo(rootFolder);
        if (result != null && result.getRootFolderDto() != null && result.getRootFolderDto().getDepartmentId() != null) {
            DepartmentDto dto = departmentService.getByIdCached(result.getRootFolderDto().getDepartmentId());
            result.getRootFolderDto().setDepartmentName(dto.getName());
            result.setDepartmentDto(dto);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public List<MediaConnectionDetailsDto> listMediaConnectionDetailsOptions(MediaType mediaType) {
        List<MediaConnectionDetails> list = mediaConnectionDetailsService.getMediaConnectionDetails(mediaType);
        return MediaConnectionDetailsService.convertMediaConnectionDetailsList(list);
    }

    @Deprecated
    public void rebuildFileTree() {
        List<Long> allRootFolderIds = docStoreService.getAllRootFolderIds();
        for (Long rootFolderId : allRootFolderIds) {
            logger.warn("Rebuild file tree {}", rootFolderId);
            rebuildFileTree(rootFolderId);
            logger.warn("Rebuild parents info");
            docFolderService.updateFoldersParentsInformation(rootFolderId, null, new DummyProgressTracker());
            logger.warn("Rebuild parents info Finished");
        }
    }

    @Deprecated
    private void rebuildFileTree(Long rootFolderId) {
        logger.warn("Rebuilding file tree on root folder {}", rootFolderId);
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        Integer foldersCount = rootFolder.getFoldersCount();
        int totalPages = foldersCount == null ? -1 : foldersCount / REBUILD_PAGE_SIZE;
        int size;
        int page = 0;
        do {
            size = docFolderService.rebuildDocFoldersPage(rootFolder, new PageRequest(page, REBUILD_PAGE_SIZE));
            page++;
            if (page % 10 == 0) {
                logger.warn("Updated {} docFolders under rootFolder {} on page {}/{}", size, rootFolderId, page, totalPages);
            }
        }
        while (size > 0);
        logger.warn("Finished rebuilding file tree on root folder {}", rootFolderId);
    }


    private SolrFacetQueryJsonResponse getTypeCategoryFileCountsResponse(
            DataSourceRequest dataSourceRequest, List<FileTypeCategory> fTypeCategories) {
        List<SolrFacetQueryJson> jsonFacetList = new LinkedList<>();
        List<String> allExtNames = new ArrayList<>();
        fTypeCategories.forEach((FileTypeCategory fTypeCategory) -> {
            SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(
                    CatFileFieldType.FILE_TYPE_CATEGORY_ID.getSolrName() + fTypeCategory.getId());
            innerFacet.setType(FacetType.QUERY);
            innerFacet.setField(CatFileFieldType.EXTENSION);
            List<String> extNames = fTypeCategory.getExtensions().stream()
                    .map(Extension::getName)
                    .collect(Collectors.toList());
            allExtNames.addAll(extNames);
            innerFacet.setQueryValues(extNames);
            jsonFacetList.add(innerFacet);
        });

        SolrFacetQueryJson otherFacet = new SolrFacetQueryJson(
                CatFileFieldType.FILE_TYPE_CATEGORY_ID.getSolrName() + ConstFileTypeCategory.OTHER.getId());
        otherFacet.setType(FacetType.QUERY);
        otherFacet.setField(CatFileFieldType.EXTENSION);
        otherFacet.setQueryValues(allExtNames);
        otherFacet.setSearchOthers(true);
        jsonFacetList.add(otherFacet);

        return fileCatService.runJsonFacetQuery(dataSourceRequest, jsonFacetList);
    }

    @SuppressWarnings("unchecked")
    public FacetPage<AggregationCountItemDTO<FileTypeCategoryDto>> getTypeCategoryFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("Get Type Category  File Counts With File Filters");
        PageRequest pageRequest = dataSourceRequest.createPageRequest();

        List<FileTypeCategory> fTypeCategories = fileTypeCategoryService.getAll();
        SolrFacetQueryJsonResponse queryResponse = getTypeCategoryFileCountsResponse(dataSourceRequest, fTypeCategories);

        SolrFacetQueryJsonResponse queryResponseNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            queryResponseNoFilter = getTypeCategoryFileCountsResponse(dataSourceRequest, fTypeCategories);
        }

        List<AggregationCountItemDTO<FileTypeCategoryDto>> ftCategories = new ArrayList<>();
        for (FileTypeCategory fTypeCategory : fTypeCategories) {
            fillFileTypeCategoryList(ftCategories, fTypeCategory.getId(), fTypeCategory.getName(), queryResponse, queryResponseNoFilter);
        }

        fillFileTypeCategoryList(ftCategories, ConstFileTypeCategory.OTHER.getId(),
                ConstFileTypeCategory.OTHER.getName(), queryResponse, queryResponseNoFilter);
        List<AggregationCountItemDTO<FileTypeCategoryDto>> sortedFtCategories = ftCategories.stream()
                .sorted((r1, r2) -> (int) (r2.getCount() - r1.getCount())).collect(toList());
        String selectItemId = dataSourceRequest.getSelectedItemId();
        return ControllerUtils.getRelevantResultPage(
                sortedFtCategories, pageRequest, selectItemId, this::getFileTypeCategoryId);
    }

    private void fillFileTypeCategoryList(List<AggregationCountItemDTO<FileTypeCategoryDto>> ftCategories,
                                          long fTypeCategoryId, String fTypeCategoryName,
                                          SolrFacetQueryJsonResponse results,
                                          SolrFacetQueryJsonResponse resultsNoFilter) {
        FileTypeCategoryDto dto = new FileTypeCategoryDto(fTypeCategoryId, fTypeCategoryName);
        String fileTypeCatKey = CatFileFieldType.FILE_TYPE_CATEGORY_ID.getSolrName() + fTypeCategoryId;
        SolrFacetJsonResponseBucket categoryResultObject = results.getResponseBucket(fileTypeCatKey);
        SolrFacetJsonResponseBucket categoryResultObjectNoFilter = resultsNoFilter == null ? null :
                resultsNoFilter.getResponseBucket(fileTypeCatKey);
        if (!categoryResultObject.isEmpty()) {
            Long count = categoryResultObject.getLongCountNumber();
            AggregationCountItemDTO<FileTypeCategoryDto> item = new AggregationCountItemDTO<>(dto, count);
            if (item.getCount() > 0) {
                ftCategories.add(item);
                if (categoryResultObjectNoFilter != null) {
                    item.setCount2(categoryResultObjectNoFilter.getLongCountNumber());
                }
            }
        }
    }

    private String getFileTypeCategoryId(FileTypeCategoryDto item) {
        return String.valueOf(item.getId());
    }

}