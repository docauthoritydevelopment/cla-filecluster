package com.cla.filecluster.service.export.serialize;

import com.cla.common.domain.dto.filetag.FileTagDto;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

import static com.cla.filecluster.service.export.exporters.file.CollectionResolverHelper.resolveCollectionValue;
import static java.lang.String.format;

public class TagsSerializer extends JsonSerializer<Set<FileTagDto>> {
    @Override
    public void serialize(Set<FileTagDto> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(resolveCollectionValue(value,
                t -> format("%s:%s", t.getTypeName(), t.getName()), FileTagDto::isImplicit));
    }
}
