package com.cla.filecluster.service.export.exporters.settings.media;

import com.cla.connector.domain.dto.media.*;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.settings.media.mixin.MediaConnectionDetailsDtoMixIn;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.media.MediaConnectionHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/11/2019
 */
@Slf4j
@Component
public class MediaConnectionExporter extends PageCsvExcelExporter<MediaConnectionDetailsDto> {

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    private final static String[] header = {NAME, MEDIA_TYPE, USERNAME, DOMAIN, URL, TENANT, APPLICATION, CLIENT, RESOURCE};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.MEDIA_CONNECTIONS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(MediaConnectionDetailsDto.class, MediaConnectionDetailsDtoMixIn.class);
    }

    @Override
    protected Page<MediaConnectionDetailsDto> getData(Map<String, String> requestParams) {
        List<MediaConnectionDetailsDto> conns = mediaConnectionDetailsService.listConnections();
        return new PageImpl<>(conns);
    }

    @Override
    protected Map<String, Object> addExtraFields(MediaConnectionDetailsDto base, Map<String, String> requestParams) {
        Map<String, Object> fromJson = ModelDtoConversionUtils.jsonToMap(base.getConnectionParametersJson());
        Map<String, Object> result = new HashMap<>();

        switch (base.getMediaType()) {
            case SHARE_POINT:
            case ONE_DRIVE:
                result.put(DOMAIN, fromJson.get("domain"));
                break;
            case EXCHANGE_365:
                result.put(APPLICATION,  fromJson.get("applicationId"));
                result.put(TENANT,  fromJson.get("tenantId"));
                break;
            case MIP:
                result.put(CLIENT,  fromJson.get("clientId"));
                result.put(RESOURCE,  fromJson.get("resource"));
            default:
        }
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }
}
