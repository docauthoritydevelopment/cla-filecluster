package com.cla.filecluster.service.export.exporters.user_view;

/**
 * Created by: ophir
 * Created on: 10/7/2019
 */
public interface UserViewDataHeaderFields {
    String NAME = "NAME";
    String DISPLAY_TYPE = "DISPLAY_TYPE";
    String DISPLAY_DATA = "DISPLAY_DATA";
    String FILTER_NAME = "FILTER_NAME";
    String FILTER_DESCRIPTOR = "FILTER_DESCRIPTOR";
    String FILTER_RAW = "FILTER_RAW";
    String IS_FILTER_GLOBAL = "IS_FILTER_GLOBAL";
    String CONTAINER_NAME = "CONTAINER_NAME";
}

