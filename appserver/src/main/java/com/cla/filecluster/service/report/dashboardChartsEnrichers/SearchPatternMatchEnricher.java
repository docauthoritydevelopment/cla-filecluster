package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class SearchPatternMatchEnricher implements DefaultTypeDataEnricher {
    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;


    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        List<Integer> ids = dashboardChartDataDto.getCategories().stream().map(Integer::parseInt).collect(Collectors.toList());
        List<TextSearchPattern> patterns = textSearchPatternRepository.findByIds(ids);
        Map<Integer, TextSearchPattern> searchPatternMap = patterns.stream().collect(
                Collectors.toMap(TextSearchPattern::getId, pattern -> pattern));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map(category -> searchPatternMap.get(Integer.parseInt(category)).getName()).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return CatFileFieldType.MATCHED_PATTERNS_MULTI.getSolrName();
    }

}
