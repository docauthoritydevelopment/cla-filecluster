package com.cla.filecluster.service.api.reports;

import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.report.DisplayType;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.report.UserViewDataSchemaLoader;
import com.cla.filecluster.service.report.UserViewDataService;
import com.google.common.base.Strings;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

/**
 * api for user view CRUD
 *
 * Created by: yael
 * Created on: 3/11/2018
 */
@RestController
@RequestMapping("/api/userview")
public class UserViewDataApiController {

    @Autowired
    private UserViewDataService userViewDataService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private UserViewDataSchemaLoader userViewDataSchemaLoader;

    private static final Logger logger = LoggerFactory.getLogger(UserViewDataApiController.class);


    @RequestMapping(method = RequestMethod.PUT)
    public UserViewDataDto createUserViewData(@RequestBody UserViewDataDto newUserView) {
        UserViewDataDto savedUserView = userViewDataService.createUserViewData(newUserView);
        eventAuditService.audit(getAuditType(savedUserView),"Create user view data ", AuditAction.CREATE, UserViewDataDto.class, savedUserView.getId());
        return savedUserView;
    }

    @RequestMapping(value = "/detached",method = RequestMethod.GET)
    public Page<UserViewDataDto> listUserViewDatasByTypeWithoutContainer(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        DisplayType type = DisplayType.valueOf(params.get("displayType"));
        return userViewDataService.listUserViewDataWithoutContainerByType(dataSourceRequest, type);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Page<UserViewDataDto> listUserViewDatas(@RequestParam final Map<String, String> params) {
        Long containerId = null;
        if (!Strings.isNullOrEmpty(params.get("containerId"))) {
            containerId = Long.parseLong(params.get("containerId"));
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (!Strings.isNullOrEmpty(params.get("displayType"))) {
            DisplayType type = DisplayType.valueOf(params.get("displayType"));
            if (containerId != null) {
                return userViewDataService.listUserViewDataByTypeAndContainerId(dataSourceRequest, type, containerId);
            }
            else {
                return userViewDataService.listUserViewDataByType(dataSourceRequest, type);
            }
        }
        if (containerId != null) {
            return userViewDataService.listUserViewDataByContainerId(dataSourceRequest, containerId);
        }
        else {
            return userViewDataService.listUserViewData(dataSourceRequest);
        }
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public UserViewDataDto getById(@PathVariable long id) {
        UserViewDataDto userViewDto = userViewDataService.getById(id);
        if (userViewDto == null) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(getAuditType(userViewDto),"Get user view data ", AuditAction.VIEW, UserViewDataDto.class, userViewDto.getId(), userViewDto);
        return userViewDto;
    }

    @RequestMapping(value = "/clone/{id}", method = RequestMethod.POST)
    public UserViewDataDto cloneUserViewData(@PathVariable("id") long originalUserViewDataId, @RequestBody String newName, @RequestParam final Map<String, String> params) {
        boolean removeContainer = false;
        if (!Strings.isNullOrEmpty(params.get("removeContainer"))) {
            removeContainer = Boolean.parseBoolean(params.get("removeContainer"));
        }
        UserViewDataDto savedUserView = userViewDataService.cloneById(originalUserViewDataId, newName, removeContainer);
        eventAuditService.audit(getAuditType(savedUserView),"Clone user view data ", AuditAction.CREATE, UserViewDataDto.class, savedUserView.getId(), savedUserView);
        return savedUserView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public UserViewDataDto updateUserViewData(@RequestBody UserViewDataDto userViewDto) {
        UserViewDataDto savedUserView = userViewDataService.updateUserViewData(userViewDto);
        eventAuditService.audit(getAuditType(savedUserView),"Update user view data ", AuditAction.UPDATE, UserViewDataDto.class, savedUserView.getId(), savedUserView);
        return savedUserView;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteUserViewData(@PathVariable long id) {
        UserViewDataDto savedUserView = userViewDataService.getById(id);
        if (savedUserView == null) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        userViewDataService.deleteUserViewData(id);
        eventAuditService.audit(getAuditType(savedUserView),"Delete user view data ", AuditAction.DELETE, UserViewDataDto.class,id, savedUserView);
    }

    private AuditType getAuditType(UserViewDataDto userViewDto) {
        switch (userViewDto.getDisplayType()) {
            case CHART:
                return AuditType.CHART;
            case DASHBOARD_WIDGET:
                return AuditType.REPORT_WIDGET;
            case DISCOVER_VIEW:
                return AuditType.DISCOVER_VIEW;
            case DASHBOARD:
                return AuditType.DASHBOARD;
            default:
                return AuditType.DISCOVER_FILTER;
        }
    }

    @RequestMapping(value="/import/csv", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto importCsvFile(@RequestParam("file") MultipartFile file,
                                         @RequestParam(value ="updateDuplicates",
                                                 required = false,
                                                 defaultValue = "false") boolean updateDuplicates){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import user view data . update duplicates set to {}",updateDuplicates);
            byte[] bytes = file.getBytes();
            ImportSummaryResultDto importSummaryResultDto = userViewDataSchemaLoader.
                    doImport(false, bytes, null, updateDuplicates, true);
            eventAuditService.audit(AuditType.FILTER,"Import user view data ",
                    AuditAction.IMPORT, MultipartFile.class,file.getName(), Pair.of("updateDuplicates",updateDuplicates));
            return importSummaryResultDto;
        } catch (Exception e) {
            logger.error("Import CSV error",e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @RequestMapping(value="/import/csv/validate", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto validateImportCsvFile(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("maxErrorRowsReport") Integer maxErrorRowsReport){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("validate import user view data CSV File");
            byte[] bytes = file.getBytes();
            return userViewDataSchemaLoader.doImport(true, bytes,
                    (maxErrorRowsReport == null? 200 : maxErrorRowsReport),false);
        } catch (Exception e) {
            logger.error("Validate import CSV error:",e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription("Validate import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }
}
