package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CreationDateEnricher implements PeriodTypeDataEnricher {

    @Override
    public String getEnricherType() {
        return CatFileFieldType.CREATION_DATE.getSolrName();
    }

}
