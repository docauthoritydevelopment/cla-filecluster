package com.cla.filecluster.service.crawler.scan;

import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.service.AbstractProgressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.function.Consumer;

/**
 * Created by: yael
 * Created on: 8/8/2019
 */
@Service
public class MapProgressService extends AbstractProgressService {

    private final static Logger logger = LoggerFactory.getLogger(MapProgressService.class);

    @Override
    protected String topic() {
        return Topics.MAP;
    }

    @Override
    protected Consumer<Event> eventConsumer() {
        return this::consumeEvent;
    }

    private void consumeEvent(Event event){
        Context context = event.getContext();
        switch (event.getEventType()) {
            case JOB_PROGRESS:
                handleJobProgress(context);
                break;
            case RUN_PROGRESS:
                handleRunProgress(context);
                break;
            case OPERATION_MESSAGE:
                setJobOpMsg(context);
                break;
            default:
                logger.warn("received unknown event type {} for map topic", event.getEventType());
        }
    }

    private void handleRunProgress(Context context) {
        Long runId = context.get(EventKeys.RUN_ID, Long.class);
        if (context.containsKey(EventKeys.FILE_METADATA_UPDATE)) {
            runService.incUpdatedMetadataFiles(runId);
        } else if (context.containsKey(EventKeys.FILE_CONTENT_UPDATE)) {
            runService.incUpdatedContentFiles(runId);
        } else if (context.containsKey(EventKeys.NEW_FILE)) {
            runService.incNewFiles(runId);
        }
    }

    private void handleJobProgress(Context context) {
        Long jobId = context.get(EventKeys.JOB_ID, Long.class);
        if (context.containsKey(EventKeys.TASK_ID)) {
            Long taskId = context.get(EventKeys.TASK_ID, Long.class);
            Long fileNum = context.get(EventKeys.ITEM_COUNT, Long.class);
            jobManagerService.updateScanTaskJobProgress(jobId, taskId, fileNum);
        } else if (context.containsKey(EventKeys.FILE_MAP_FAIL)) {
            jobManagerService.incJobFailedCounters(jobId);
        } else {
            Integer meaningfulCounter = 0;
            Integer fileNum = 0;
            if (context.containsKey(EventKeys.MEANINGFUL_ITEM_COUNT)) {
                meaningfulCounter = context.get(EventKeys.MEANINGFUL_ITEM_COUNT, Integer.class);
            }
            if (context.containsKey(EventKeys.ITEM_COUNT)) {
                fileNum = context.get(EventKeys.ITEM_COUNT, Integer.class);
            }
            jobManagerService.incJobCounters(jobId, fileNum, meaningfulCounter);
        }
    }
}
