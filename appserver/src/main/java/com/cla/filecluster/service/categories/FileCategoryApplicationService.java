package com.cla.filecluster.service.categories;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.category.*;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.FileCategorySpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.jpa.FileCategoryRepository;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.convertion.FileCategoryConversionService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.common.SolrDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class FileCategoryApplicationService {

    private static final Logger logger = LoggerFactory.getLogger(FileCategoryApplicationService.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private FileCategoryRepository fileCategoryRepository;

    @Autowired
    private FileCategoryConversionService fileCategoryConversionService;

    @Autowired
    private FileCategoryBuilder fileCategoryBuilder;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional(readOnly = true)
    public Page<FileCategoryDto> listAccountCategories(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = getPageRequest(dataSourceRequest);
        logger.debug("List account categories (mysql). Page: {}", pageRequest.getPageNumber());
        Specification<FileCategory> filterSpecification = new FileCategorySpecification(dataSourceRequest.getFilter(), FileCategoryScope.ACCOUNT);
        Page<FileCategory> fileCategories = fileCategoryRepository.findAll(filterSpecification, pageRequest);
        logger.debug("List account categories (mysql) - got {} total results", fileCategories.getTotalElements());
        return fileCategoryConversionService.convertToDto(fileCategories, pageRequest);
    }


    @Transactional(readOnly = true)
    public List<FileCategoryDto> listAllAccountCategories(DataSourceRequest dataSourceRequest) {
        logger.debug("List all account categories (mysql)");
        Sort sortRequest = getSortRequest(dataSourceRequest);
        List<FileCategory> fileCategories = fileCategoryRepository.findAll(sortRequest);
        return fileCategoryConversionService.convertToDto(fileCategories);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<String>> listFileFacetsByField(DataSourceRequest dataSourceRequest, String facetField) {
        logger.debug("list file facets by field {}", facetField);
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        CatFileFieldType catFileFieldType = FileDtoFieldConverter.convertToCatFileField(facetField);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, catFileFieldType);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrFacetSpecification, pageRequest);
        logger.debug("list file facets by Solr field {} returned {} results",
                catFileFieldType == null ? "N/A" : catFileFieldType.getSolrName(),
                solrFacetQueryResponse.getFirstResult().getValueCount());
        return convertFacetFieldResultToDto(solrFacetQueryResponse.getFirstResult(), pageRequest);
    }

    private FacetPage<AggregationCountItemDTO<String>> convertFacetFieldResultToDto(FacetField facetField, PageRequest pageRequest) {
        List<AggregationCountItemDTO<String>> content = Lists.newArrayList();
        for (FacetField.Count count : facetField.getValues()) {
            AggregationCountItemDTO<String> item = new AggregationCountItemDTO<>(count.getName(), count.getCount());
            content.add(item);
        }
        return new FacetPage<>(content, pageRequest);
    }


    private PageRequest getPageRequest(DataSourceRequest dataSourceRequest) {
        Sort sort = getSortRequest(dataSourceRequest);
        return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(), sort);
    }

    private Sort getSortRequest(DataSourceRequest dataSourceRequest) {
        Sort sort;
        if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
            //Order the records according to default order
            Sort.Order nameOrder = new Sort.Order(Sort.Direction.ASC, "name");
            sort = new Sort(nameOrder);
        } else {
            sort = DataSourceRequest.convertSort(dataSourceRequest, FileDtoFieldConverter::convertDtoField);
        }
        return sort;
    }

    @Transactional(readOnly = true)
    public Page<ClaFileDto> listCategoryFiles(long categoryId, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = getPageRequest(dataSourceRequest);
        logger.debug("List files for category id: {}", categoryId);
        FileCategory fileCategory = fileCategoryRepository.findById(categoryId).orElse(null);
        if (fileCategory == null) {
            throw new RuntimeException(messageHandler.getMessage("file-category.id.illegal", Lists.newArrayList(categoryId)));
        }
        Page<SolrDocument> solrDocuments = fileCatService.listCategoryFiles(fileCategory, pageRequest);
        logger.debug("got back {} documents from solr", solrDocuments.getNumberOfElements());
        List<ClaFileDto> content = new ArrayList<>(solrDocuments.getContent().size());
        for (SolrDocument solrDocument : solrDocuments) {
            ClaFileDto claFileDto = FileCatUtils.convertCatFileToDto(solrDocument);
            content.add(claFileDto);
        }
        return new PageImpl<>(content, pageRequest, solrDocuments.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<ClaFileDto> listFilesWithSolrFilter(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = getPageRequest(dataSourceRequest);
        logger.debug("List catFiles");
        Page<SolrDocument> solrDocuments = fileCatService.listFilesWithFilter(pageRequest, null);
        logger.debug("got back {} documents from solr", solrDocuments.getNumberOfElements());
        List<ClaFileDto> content = new ArrayList<>(solrDocuments.getContent().size());
        for (SolrDocument solrDocument : solrDocuments) {
            ClaFileDto claFileDto = FileCatUtils.convertCatFileToDto(solrDocument);
            content.add(claFileDto);
        }
        return new PageImpl<>(content, pageRequest, solrDocuments.getTotalElements());
    }

    @Transactional
    public void mergeCategories(MergeCategoryRequestDto mergeCategoryRequestDto) {
        List<Long> categoriesToMergeIds = mergeCategoryRequestDto.getCategoriesToMerge();
        if (categoriesToMergeIds == null || categoriesToMergeIds.size() == 0) {
            throw new BadRequestException(messageHandler.getMessage("file-category.merge.missing"), BadRequestType.MISSING_FIELD);
        }
        logger.debug("Request to merge Categories: {}", categoriesToMergeIds);
        logger.trace("Find all the relevant categories");
        List<FileCategory> fileCategoriesToMerge = categoriesToMergeIds.stream().map(this::getFileCategoryById).collect(Collectors.toList());
        logger.trace("Validate that the categories are of the same type");
        validateCategoriesAreOfSameType(fileCategoriesToMerge);
        logger.trace("Validate that the categories are of the same scope");
        validateCategoriesAreOfSameScope(fileCategoriesToMerge);
        logger.trace("Create a new category that contains the merged result");
        FileCategory newCategory = fileCategoryBuilder.buildFileCategoryFromFileCategories(mergeCategoryRequestDto.getNewCategoryName(), fileCategoriesToMerge);
        fileCategoryRepository.save(newCategory);
        logger.trace("Mark the old categories as merged");
        for (FileCategory fileCategory : fileCategoriesToMerge) {
            markCategoryAsMerged(fileCategory);
        }
    }

    private void markCategoryAsMerged(FileCategory fileCategory) {
        fileCategory.setState(FileCategoryState.MERGED);
    }


    private void validateCategoriesAreOfSameScope(List<FileCategory> fileCategoriesToMerge) {
        FileCategoryScope scope = fileCategoriesToMerge.get(0).getScope();
        fileCategoriesToMerge.forEach(f -> validateCategoryHasScope(scope, f));

    }

    private void validateCategoryHasScope(FileCategoryScope scope, FileCategory f) {
        if (!scope.equals(f.getScope())) {
            throw new BadRequestException(messageHandler.getMessage("file-category.merge.illegal-scope"), BadRequestType.UNSUPPORTED_VALUE);
        }

    }

    private void validateCategoriesAreOfSameType(List<FileCategory> fileCategoriesToMerge) {
        FileCategoryType type = fileCategoriesToMerge.get(0).getType();
        fileCategoriesToMerge.forEach(f -> validateCategoryHasType(type, f));
    }

    private void validateCategoryHasType(FileCategoryType type, FileCategory f) {
        if (!type.equals(f.getType())) {
            throw new BadRequestException(messageHandler.getMessage("file-category.merge.illegal-type"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }


    private FileCategory getFileCategoryById(long id) {
        return fileCategoryRepository.findById(id).orElse(null);
    }

}
