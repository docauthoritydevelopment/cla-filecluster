package com.cla.filecluster.service.export.exporters.root_folders.mix_in;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CrawlRunDetailsDtoMixIn {

    @JsonProperty("# processed files")
    private Long totalProcessedFiles;

    @JsonProperty("Pdf processed")
    private Long processedPdfFiles;

    @JsonProperty("MS-Word processed")
    private Long processedWordFiles;

    @JsonProperty("MS-Excel processed")
    private Long processedExcelFiles;

    @JsonProperty("Other processed")
    private Long processedOtherFiles;

    @JsonProperty("Scan errors")
    private Long scanErrors;

    @JsonProperty("Processing errors")
    private Long scanProcessingErrors;

    @JsonProperty("Accessibility")
    boolean accessible;
}
