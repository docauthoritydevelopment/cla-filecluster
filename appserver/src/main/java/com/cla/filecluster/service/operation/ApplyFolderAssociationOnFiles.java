package com.cla.filecluster.service.operation;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.predicate.CompareOperation;
import com.cla.common.predicate.KeyValuePredicate;
import com.cla.common.tag.priority.TagPriority;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.scope.ScopeService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class ApplyFolderAssociationOnFiles implements ScheduledOperation {

    private final static Logger logger = LoggerFactory.getLogger(ApplyFolderAssociationOnFiles.class);

    public static String ASSOCIATION_PARAM = "ASSOCIATION_ENTITY";
    public static String DOC_FOLDER_ID_PARAM = "DOC_FOLDER_ID";
    public static String ACTION_PARAM = "ACTION_PARAM";
    public static String START_OP_PARAM = "START_OP_PARAM";

    @Autowired
    protected DocFolderService docFolderService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileCrawlerExecutionDetailsService runService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            Long folderId = Long.parseLong(opData.get(DOC_FOLDER_ID_PARAM));
            AssociationEntity entity = AssociationsService.convertFromAssociationEntity(folderId, opData.get(ASSOCIATION_PARAM), AssociationType.FOLDER);
            String action = opData.get(ACTION_PARAM);
            boolean addAction = action.equalsIgnoreCase("ADD");

            Long folderIdForOperation = entity.getOriginFolderId() != null ? entity.getOriginFolderId() : folderId;
            DocFolder docFolder = docFolderService.getById(folderIdForOperation);
            if (docFolder == null) {
                logger.error("cant find folder {} for operation {} - skip", folderIdForOperation, operationTypeHandled());
                return true;
            }

            if (entity.getDocTypeId() != null) {
                List<String> decodeFolderParentsInfo = docFolder.getParentsInfo();
                String folderParentsInfo = decodeFolderParentsInfo.get(decodeFolderParentsInfo.size() - 1);
                BasicScope scope = entity.getAssociationScopeId() == null ? null : scopeService.getFromCache(entity.getAssociationScopeId());
                associationsService.updateDocTypeToCategoryFiles(folderParentsInfo, CatFileFieldType.FOLDER_IDS,
                        entity.getDocTypeId(), addAction, scope,
                        TagPriority.folder(docFolder.getDepthFromRoot()), SolrCommitType.SOFT_NOWAIT);
            } else {
                List<AssociableEntityDto> entities = associationsService.getEntitiesForAssociations(Lists.newArrayList(entity));
                associationsService.updateAssociationToCategoryFilesByFolderId(docFolder, entities.get(0),
                        entity.getAssociationScopeId(), addAction, SolrCommitType.SOFT_NOWAIT);
            }

            Long startOperation = Long.parseLong(opData.getOrDefault(START_OP_PARAM, "0"));
            if (startOperation > 0) {
                String pattern = entity.getOriginDepthFromRoot() + "." + entity.getOriginFolderId();
                List<SolrFolderEntity> newFolders = docFolderService.getFolders(
                        Query.create()
                                .addFilterWithCriteria(CatFoldersFieldType.CREATION_DATE, SolrOperator.GT, startOperation - TimeUnit.MINUTES.toMillis(5))
                                .addFilterWithCriteria(CatFoldersFieldType.PARENTS_INFO, SolrOperator.EQ, pattern));
                List<Long> folderIdsToFix = new ArrayList<>();
                for (SolrFolderEntity folder : newFolders) {
                    boolean found = false;
                    if (folder.getAssociations() != null && !folder.getAssociations().isEmpty()) {
                        for (String assoc : folder.getAssociations()) {
                            AssociationEntity assocToCompare = AssociationsService.convertFromAssociationEntity(folderId, assoc, AssociationType.FOLDER);
                            if (assocToCompare != null && assocToCompare.similarAssociation(entity) &&
                                    entity.getOriginFolderId().equals(assocToCompare.getOriginFolderId())) {
                                found = true;
                            }
                        }
                    }
                    if (addAction != found) {
                        folderIdsToFix.add(folder.getId());
                    }
                }
                if (!folderIdsToFix.isEmpty()) {
                    entity.setImplicit(true); // for sub folders
                    if (addAction) {
                        associationsService.addRemoveFolderAssociation(SolrFieldOp.ADD, CatFoldersFieldType.ID.inQuery(folderIdsToFix), entity, null);
                    } else {
                        associationsService.addRemoveFolderAssociation(SolrFieldOp.REMOVE_REGEX, CatFoldersFieldType.ID.inQuery(folderIdsToFix), entity, null);
                    }
                    docFolderService.softCommitNoWaitFlush();
                }
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public boolean operationAllowedToRun(ScheduledOperationMetadata operationMetadata) {
        Map<String,String> opData = operationMetadata.getOpData();
        String folderIdStr = opData.get(DOC_FOLDER_ID_PARAM);
        Long folderId = Long.parseLong(folderIdStr);
        DocFolder docFolder = docFolderService.getById(folderId);
        Long rootFolderId = docFolder.getRootFolderId();
        Long runId = runService.runActiveForRootFolder(rootFolderId);
        if (runId != null) { // active run for root folder
            Long jobId = jobManagerService.getJobIdCached(runId, JobType.SCAN);
            if (jobId != null && jobManagerService.getCachedJobState(jobId).equals(JobState.DONE)) {
                return true; // map job done - allow run
            } else if (operationMetadata.getOpCondition() != null && operationMetadata.getOpCondition().getDesiredValue() != null) {
                if (operationMetadata.getOpCondition().getDesiredValue() instanceof Collection) {
                    List<Map<String, Number>> pendingBatchTaskIds = (List<Map<String, Number>>) operationMetadata.getOpCondition().getDesiredValue();
                    Set<Pair<Long, Long>> pendingBatchTaskIdsCurrent = mapBatchResultsProcessor.getPendingBatchTaskIds();

                    for (Map<String, Number> mapEntry : pendingBatchTaskIds) {
                        if (pendingBatchTaskIdsCurrent.contains(Pair.of(mapEntry.get("key").longValue(), mapEntry.get("value").longValue()))) {
                            return false; // existing batches not yet flushed - wait
                        }
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String folderIdStr = opData.get(DOC_FOLDER_ID_PARAM);
        if (Strings.isNullOrEmpty(folderIdStr)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.folder.empty"), BadRequestType.MISSING_FIELD);
        }
        Long folderId = Long.parseLong(folderIdStr);

        String json = opData.get(ASSOCIATION_PARAM);
        if (Strings.isNullOrEmpty(json)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.empty"), BadRequestType.MISSING_FIELD);
        }
        if (AssociationsService.convertFromAssociationEntity(folderId, json, AssociationType.FOLDER) == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        String action = opData.get(ACTION_PARAM);
        if (Strings.isNullOrEmpty(action)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.action.empty"), BadRequestType.MISSING_FIELD);
        }
        if (!action.equalsIgnoreCase("ADD") && !action.equalsIgnoreCase("REMOVE")) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.action.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.APPLY_FOLDER_ASSOCIATIONS_TO_SOLR;
    }

    @Override
    public boolean shouldAllowCreateWhileScheduleDown() {
        return true;
    }

    public static KeyValuePredicate createCondition(Set<Pair<Long, Long>> pendingBatchTaskIds) {
        if (pendingBatchTaskIds != null) {
            return new KeyValuePredicate("pendingBatchTaskIds", CompareOperation.CONTAINS, pendingBatchTaskIds);
        }
        return null;
    }
}
