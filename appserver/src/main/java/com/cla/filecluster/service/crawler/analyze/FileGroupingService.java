package com.cla.filecluster.service.crawler.analyze;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.mediaproc.excel.ExcelParseParams;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.categories.FileCatService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FileGroupingService {
	private static final Logger logger = LoggerFactory.getLogger(FileGroupingService.class);

	@Autowired
	private GroupsService groupsService;

	@Autowired
	private ContentMetadataService contentMetadataService;

	@Autowired
	private FileCatService fileCatService; //TODO : vladi falcon use this

	@Value("${tokenMatcher.excel.CellMinStringLength:3}")
	private int tokenMatcherExcelCellMinStringLength;

	@Value("${tokenMatcher.excel.CellMinNumberLength:4}")
	private int tokenMatcherExcelCellMinNumberLength;

	@Value("${tokenMatcher.excel.IgnoreComplexNumbers:true}")
	private boolean tokenMatcherExcelIgnoreComplexNumbers;

	@Value("${filesFetchPageSize:30}")
	private int pageSize;

    @PostConstruct
	private void init() {
        // setup parameters for tokenMatcher extraction by
        // ExcelWorkbook/ExcelSheet.
        ExcelParseParams.setTokenMatchedParameters(tokenMatcherExcelCellMinStringLength,
                tokenMatcherExcelCellMinNumberLength,
                tokenMatcherExcelIgnoreComplexNumbers);
	}


	public FileGroup findAnalysisGroupForFile(final long fileId){
		final SolrFileEntity f = fileCatService.getFileEntity(fileId);
		if (logger.isTraceEnabled()) {
			logger.trace("Rest response: group for file {} = {}", fileId, (f==null)?"null":f.getAnalysisGroupId());
		}
		return (f==null || f.getAnalysisGroupId() == null)?null:groupsService.findById(f.getAnalysisGroupId());
	}

	@AutoAuthenticate
	public void changeState(final ClaFileState fromState, final ClaFileState toState) {
		final long filesToReset = fileCatService.getFileNumByStatus(fromState);
		if (filesToReset > 0) {
			logger.info("Found {} files in {} state. Updating them to state {}", filesToReset, fromState, toState);
			String stringQuery = SolrQueryGenerator.generateFilter(Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, fromState.name()));
			fileCatService.updateByQuery(Lists.newArrayList(stringQuery), CatFileFieldType.PROCESSING_STATE, toState.name(), true);
			logger.debug("Updating finished");
		} else {
			logger.debug("Found no files in {} state to update", fromState);
		}
	}

    public void changeContentState(final ClaFileState fromState, final ClaFileState toState) {
        final int filesToReset = contentMetadataService.countByState(fromState);
        if (filesToReset > 0) {
            logger.info("Found {} contents in {} state. Updating them to state {}", filesToReset, fromState, toState);
			contentMetadataService.changeStateAll(toState, fromState);
            logger.debug("Updating finished");
        } else {
            logger.debug("Found no files in {} state to update", fromState);
        }
    }

	public Map<Long,ContentMetadataDto> findFilesContentByIds(Collection<Long> ids) {
		if (ids.size() == 0) {
			return Maps.newHashMap();
		}
		List<ContentMetadataDto> dtoByIds = contentMetadataService.findDtoByIds(ids);
		if (dtoByIds == null || dtoByIds.isEmpty()) {
			return Maps.newHashMap();
		}
		return dtoByIds.stream().collect(Collectors.toMap(ContentMetadataDto::getContentId, f -> f));
	}

}
