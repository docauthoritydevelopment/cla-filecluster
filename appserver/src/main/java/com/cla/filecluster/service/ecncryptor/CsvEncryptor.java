package com.cla.filecluster.service.ecncryptor;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.extractor.entity.CsvParseDateFormatStemming;
import com.cla.filecluster.util.csv.CsvReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import javax.annotation.PostConstruct;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.*;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class CsvEncryptor {
	
	@Value("${nullValueString:nullToken}")
	private String nullValue;

	@Value("${customFieldName:custom}")
	private String customFieldName;
	
	private Integer customFieldParamIndex; 
	
	@Value("${customFieldParamName:#{null}}")
	private String customFieldParamName; 
	
	@Value("${dateFormatsVariation:MM/dd/yyyy}")
	private String dateFormatsVariation; 

	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CsvEncryptor.class);
	
	@Value("${clearedEntitiesFile:defaultFileInCode.txt}")
	private String clearedEntitiesFileName;

	@Value("${entitiesFile}")
	private String fileName;
	
	@Autowired
	private CsvReader csvReader;
	
	@Autowired
	private ExtractTokenizer extractTokenizer;
	
	@Autowired
	private NGramTokenizerHashed nGramTokenizerHashed;
	
	@Value("${wordHeadingsTextNgramSize:3}")
	private short headingsTextNgramSize;
	
//	@Value("${clearFields}")
	private String clearFieldsStr;
	
	private List<Integer> clearFieldsIndexes;
	
	private  Invocable  invocable;

	@Value("${customFuncName:customFunc}")
	private String customFuncName;

    @Value("${encryptor.config.folder:./config/extraction/}")
    private String configurationFolder;

    @Value("${customFuncFile:./config/extraction/customFunc.js}")
	private String customFuncFile;

	private int processedRows;
	private int failedRows;
	
//	@Resource
	private Map<String,CellProcessor> cellValidationProcessorsMap;

    @Autowired
    private ResourceLoader resourceLoader;

    boolean encryptorReady = false;

    @PostConstruct
	private void init() throws ScriptException, FileNotFoundException{
        logger.debug("Init CsvEncryptor");
		Executors.newSingleThreadExecutor().execute(new Runnable() {
			@Override
			public void run() {
				final ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
				try {
					engine.eval(new FileReader(customFuncFile));
				} catch (ScriptException e) {
					logger.error("Failed to load custom function file {}",customFuncFile,e);
				} catch (FileNotFoundException e) {
					logger.error("Failed to load custom function file {}",customFuncFile,e);
				}
				invocable = (Invocable) engine;

				CsvParseDateFormatStemming.setFormatVariations(Arrays.asList(dateFormatsVariation.split(",")));
				logger.debug("CsvEncryptor initialized");
				encryptorReady = true;
			}
		});
        //TODO - decide if to load the service at all
	}

    public void loadCsvCellValidationProcessorMap(String configurationFileName) {
        String location = "file:" + configurationFileName;
        System.out.println("Loading CSV cell validator from "+location);
        if (!new File(configurationFileName).exists()) {
            System.err.println("Illegal Cell validator parameter value. CSV Cell validator file not found ("+configurationFileName+")");
            throw new RuntimeException("Illegal Cell validator parameter value. CSV Cell validator file not found ("+configurationFileName+")");
        }
        org.springframework.core.io.Resource resource = resourceLoader.getResource(location);
        logger.debug("Loading XML Bean reader");
        DefaultListableBeanFactory xbf = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(xbf);
        logger.debug("Loading Bean definitions");
        reader.loadBeanDefinitions(resource);
        logger.debug("Loading Bean definitions");
        cellValidationProcessorsMap = (Map<String,CellProcessor>) xbf.getBean("cellValidationProcessorsMap");
        clearFieldsStr = "CNAME";
    }
	public void encrypt(final String[] args) {
        int argsInx = 0;
		if(args.length>0 && args[0].equals("encryptor")) {
			argsInx++;
		}
		if (args.length < argsInx+2) {
			System.err.println("Wrong number of arguments: "+args.toString());
			logger.error("Wrong number of arguments: "+args.toString());
			return;
		}
		try {
			System.out.println("Start processing...");
			String entityFileValidator = configurationFolder + "entities-file-validation.xml";
            if (args.length > argsInx+2) {
                String validator = args[argsInx+2];
                entityFileValidator = configurationFolder + "entities-file-validation-" +validator+".xml";
            }
            loadCsvCellValidationProcessorMap(entityFileValidator);
            encrypt(args[argsInx], args[argsInx+1]);
			System.out.println();
			System.out.println(String.format("Done. Total of %d records were found. %d were processed. %d were ignored.",
					getProcessedRows()+getFailedRows(),
					getProcessedRows(), getFailedRows()));
		} catch (final Exception e) {
			logger.error("Eror during csvEncryptor.encrypt(): ", e);
		}
	}

	private void verifyEncryptorReady() {
		int triesLeft = 10;
    	while (!encryptorReady && triesLeft > 0) {
			try {
				Thread.sleep(200);
				triesLeft--;
			} catch (InterruptedException e) {
				throw new RuntimeException("Interrupted");
			}
		}
		if (triesLeft == 0) {
    		logger.error("Encryptor not ready");
		}
		else {
			logger.debug("Encryptor ready after {} tries",10-triesLeft);
		}
	}

	public void encrypt(){
		encrypt(null,null);
	}
	
	public void encrypt(String inFile, String outFile){
		verifyEncryptorReady();
		if (inFile == null || inFile.isEmpty()) {
			inFile = clearedEntitiesFileName;

		}
		if (outFile == null || outFile.isEmpty()) {
			outFile = fileName;
		}
		// Remove " from paths to work around Windows batch issue
		inFile = inFile.replaceAll("\"", "");
		outFile = outFile.replaceAll("\"", "");
			
		logger.info("Encrypt file {} to file {}...",inFile,outFile);
		try {
			CsvListWriter listWriter;
			try {
				listWriter = new CsvListWriter(new FileWriter(outFile), CsvPreference.STANDARD_PREFERENCE);
			} catch (final Exception e) {
				System.err.println("Error creating output file "+outFile);
				throw new RuntimeException("Error creating output file "+outFile, e);
			}
			String[] headers;
			try {
				headers = csvReader.getHeaders(inFile);
			} catch (final Exception e) {
				System.err.println("Error: Could not read inout file "+ inFile);
				throw new RuntimeException("Could not read inout file. "+inFile);
			}
			if (!validHeaders(headers,clearFieldsStr)) {
				System.err.println("Error: unexpected input file format");
				throw new RuntimeException("Unexpected input file format. "+inFile);
			}
			final List<String> headersList = new LinkedList<>(Arrays.asList(headers));
			customFieldParamIndex = (customFieldParamName==null || customFieldParamName.isEmpty())?-1:
				headersList.indexOf(customFieldParamName);
			if (customFieldParamName!=null && customFieldParamIndex<0) {
				System.err.println("Warning: custom field header not found: " + customFieldParamName);
			}
			
			clearFieldsIndexes = Stream.of(clearFieldsStr.split(",")).map(cf->headersList.indexOf(cf)).filter(i->!(i.equals(-1))).collect(toList());
			clearFieldsIndexes.forEach(i->headersList.add(headers[i]+"_CLEARED"));
			if(customFieldParamName!=null && !customFieldParamName.isEmpty()) {
				headersList.add(customFieldName);
			}
			try {
				listWriter.writeHeader(headersList.toArray(new String[0]));
			} catch (final Exception e) {
				System.err.println("Error writing output file "+outFile);
				listWriter.close();
				throw new RuntimeException("Error writing output file "+outFile, e);
			}
			processedRows = 0;
			try {
				failedRows = csvReader.readAndProcess(inFile, r->writeEncryptedRow(r,listWriter),null,cellValidationProcessorsMap);
			} catch (final Exception e) {
				logger.info("Encryption stopped due to errors - {}. {} rows were processed", e, processedRows);
				listWriter.close();
				throw new RuntimeException(e);
			}
			logger.info("Finished encryption. {} rows were processed", processedRows);
			listWriter.close();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	private boolean validHeaders(final String[] headers, final String requiredFieldStr) {
		final List<String> headersList = Arrays.asList(headers);
		boolean fail = Stream.of(requiredFieldStr.split(",")).anyMatch(rf->!headersList.contains(rf));
		if (fail) {
			logger.debug("validHeaders failed: some of {} was not found in {}", requiredFieldStr, headersList.toString());
			return false;
		}
		final long goodHeaders = headersList.stream().filter(h->(h.length() >3 && h.length()<50)).count();
		if (goodHeaders > 100 || goodHeaders < 2) {
			return false;
		}
		
		return true;
	}

	private void writeEncryptedRow(final RowDTO row, final ICsvListWriter listWriter){
		try{
			final List<String> values = new ArrayList<String>(row.getFieldsValues().length);
			final List<String> clearValues = new LinkedList<>();
			for (int i = 0; i < row.getFieldsValues().length; i++) {
				addField(row, values, clearValues, i);
			}
			values.addAll(clearValues);
			addCustomField(row, values);
            listWriter.write(values);
			processedRows++;
		} catch (final IOException | NoSuchMethodException | ScriptException e) {
			throw new RuntimeException(e);
		} 
	}


	private void addField(final RowDTO row, final List<String> values, final List<String> clearValues, final int i) {
		final Object field = row.getFieldsValues()[i];
		if (field instanceof Date) {
			
		}
		final String value = row.getFieldsValues()[i];
		if(value==null) {
			values.add(nullValue);
		} else {
			final String tokens = extractTokenizer.getHash(value);
			
			// Validate
			final String tokens1 = nGramTokenizerHashed.getHash(value,null);
			if (tokens!=null && tokens1!=null) {
				final List<String> t = Arrays.asList(tokens.split(" "));
				final List<String> t1 = Arrays.asList(tokens1.split(" "));
				final long mismatches = t.stream().filter(ti->(!t1.contains(ti))).count();
				if (mismatches>0) {
					logger.debug("found {} mismatches. {} from <{}> not found in <{}>", mismatches, 
							t.stream().filter(ti->(!t1.contains(ti))).collect(Collectors.joining(" ")), tokens, tokens1);
				}
			}
			
			if (tokens == null || tokens.isEmpty()) {
				values.add(nullValue);
			} else {
				values.add(tokens);
			}
		}
		if(clearFieldsIndexes.contains(i)) {
			clearValues.add(value);
		}
	}


	private void addCustomField(final RowDTO row, final List<String> values) throws ScriptException,
			NoSuchMethodException {
		if(customFieldParamName!=null && customFieldParamIndex>=0){
			final String customFieldParam = row.getFieldsValues()[customFieldParamIndex];
			final Object result = invocable.invokeFunction(customFuncName, customFieldParam);
			values.add(String.valueOf(result));
//			final String hashReult = extractTokenizer.getHash((String)result);
//			values.add(String.valueOf(hashReult));
		}
	}

	public int getProcessedRows() {
		return processedRows;
	}
	public int getFailedRows() {
		return failedRows;
	}

	
}
