package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.associations.FileTagHeaderFields.*;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
@Slf4j
@Component
public class TagExporter extends PageCsvExcelExporter<AggregationCountItemDTO<FileTagDto>> {

    private final static String[] HEADER = {NAME, TYPE, NUM_OF_FILES, DESCRIPTION};

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, TagAggCountDtoMixin.class);
        mapper.addMixIn(FileTagDto.class, FileTagDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<FileTagDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);

        Long scopeId = null;
        if (requestParams.get("scopeId") != null) {
            scopeId = Long.parseLong(requestParams.get("scopeId"));
        }

        FacetPage<AggregationCountItemDTO<FileTagDto>> res = fileTaggingAppService.listTagsFileCountWithFileFilter(dataSourceRequest, scopeId);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<FileTagDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.TAG);
    }
}
