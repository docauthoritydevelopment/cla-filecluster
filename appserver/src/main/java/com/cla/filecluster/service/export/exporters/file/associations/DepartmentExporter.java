package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.department.DepartmentAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.associations.DepartmentHeaderFields.*;
import static com.cla.filecluster.service.export.exporters.file.associations.DocTypeHeaderFields.NUM_OF_ALL_FILTERED;
import static com.cla.filecluster.service.export.exporters.file.associations.DocTypeHeaderFields.NUM_OF_DIRECT_FILERED;

@Slf4j
@Component
public class DepartmentExporter extends PageCsvExcelExporter<AggregationCountItemDTO<DepartmentDto>> {

    private final static String[] HEADER = {NAME, NUM_OF_DIRECT_FILERED, NUM_OF_ALL_FILTERED, DESCRIPTION};

    @Autowired
    protected DepartmentAppService departmentAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, DepartmentAggCountDtoMixin.class);
        mapper.addMixIn(DepartmentDto.class, DepartmentDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<DepartmentDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<DepartmentDto>> res = departmentAppService.findDepartmentFileCount(dataSourceRequest, true);
        return new PageImpl<>(res.getContent());
    }

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<DepartmentDto> base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        result.put(NAME, base.getItem().getFullName());
        result.put(NUM_OF_ALL_FILTERED, base.getSubtreeCount());
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DEPARTMENTS) || exportType.equals(ExportType.MATTERS);
    }
}
