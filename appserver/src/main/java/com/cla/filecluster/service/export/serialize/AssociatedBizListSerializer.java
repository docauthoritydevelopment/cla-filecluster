package com.cla.filecluster.service.export.serialize;

import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.List;

import static com.cla.filecluster.service.export.exporters.file.CollectionResolverHelper.resolveCollectionValue;

public class AssociatedBizListSerializer extends JsonSerializer<List<SimpleBizListItemDto>> {
    @Override
    public void serialize(List<SimpleBizListItemDto> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(resolveCollectionValue(value,
                SimpleBizListItemDto::getName, SimpleBizListItemDto::isImplicit));
    }
}
