package com.cla.filecluster.service.export.exporters.file.bizlist;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.bizlist.BizListHeaderFileds.*;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
@Slf4j
@Component
public class ClientAssociationExporter extends PageCsvExcelExporter<AggregationCountItemDTO<SimpleBizListItemDto>> {

    final static String[] HEADER = {NAME, BUSINESS_ID, TYPE, LIST, NUM_OF_FILTERED_FILES};

    @Autowired
    private BizListAppService bizListAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, ClientAssociationAggCountDtoMixin.class);
        mapper.addMixIn(SimpleBizListItemDto.class, SimpleBizListItemDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<SimpleBizListItemDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> res = bizListAppService.findBizListItemsAssociationFileCounts(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<SimpleBizListItemDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.CLIENT_ASSOCIATION);
    }
}
