package com.cla.filecluster.service.metadata;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.mediaconnector.labeling.LabelResult;
import com.cla.connector.mediaconnector.labeling.mip.MipLabelResultTyped;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.metadata.MetadataTranslationService;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.util.ControllerUtils;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.cla.common.constants.CatFileFieldType.*;

@Service
public class MetadataAppService {

    private final static Logger logger = LoggerFactory.getLogger(MetadataAppService.class);

    @Autowired
    private MetadataTranslationService metadataTranslationService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Value("${metadata.re-transformer.page-size:50000}")
    private int metadataReTransformerPageSize;

    @Value("${metadata.mip.type:}")
    private String mipMetadataType;

    @Value("${metadata.separator:}")
    private String metadataSeparator;


    public FacetPage<AggregationCountItemDTO<String>> getFileCountsForExport(DataSourceRequest dataSourceRequest) {
        return getFileCounts(dataSourceRequest, CatFileFieldType.DA_METADATA);
    }

    public FacetPage<AggregationCountItemDTO<MetadataDto>> getFileCounts(DataSourceRequest dataSourceRequest) {
        FacetPage<AggregationCountItemDTO<String>> resAsString = getFileCounts(dataSourceRequest, CatFileFieldType.DA_METADATA);

        if (resAsString == null) {
            return null;
        }

        List<AggregationCountItemDTO<MetadataDto>> content = new ArrayList<>();
        for (AggregationCountItemDTO<String> item : resAsString.getContent()) {
            String[] data = item.getItem().split(metadataSeparator);
            if (data.length > 1) {
                MetadataDto dto = new MetadataDto(data[0], data[1]);
                content.add(new AggregationCountItemDTO(dto, item.getCount(), item.getCount2()));
            } else {
                logger.warn("failed parsing metadata result for file count {}", item.getItem());
            }
        }
        FacetPage<AggregationCountItemDTO<MetadataDto>> ans = new FacetPage<>(content, resAsString.getPageSize(),
                resAsString.getPageNumber(),resAsString.getTotalAggregatedCount(), resAsString.getPageAggregatedCount());
        ans.setTotalElements(resAsString.getTotalElements());
        return ans;
    }

    public FacetPage<AggregationCountItemDTO<String>> getFileCountsType(DataSourceRequest dataSourceRequest) {
        return getFileCounts(dataSourceRequest, CatFileFieldType.DA_METADATA_TYPE);
    }

    private FacetPage<AggregationCountItemDTO<String>> getFileCounts(DataSourceRequest dataSourceRequest, CatFileFieldType fieldType) {
        try {
            long totalMetadataFacetCount = fileCatAppService.getTotalFacetCount(dataSourceRequest, fieldType);
            PageRequest pageRequest = dataSourceRequest.createPageRequest();

            Pair<Map<String, Long>, Map<String, Long>> res = runQuery(dataSourceRequest, fieldType);

            if (res.getKey() == null || res.getKey().isEmpty()) {
                logger.debug("list file facets by Solr field {} returned no results", fieldType.getSolrName());
                return new FacetPage<>(new ArrayList<>(), pageRequest);
            }

            logger.debug("list file facets by Solr field {} returned {} results", fieldType.getSolrName(), res.getKey().size());

            FacetPage<AggregationCountItemDTO<String>> ans = convertFacetFieldResultToDto(res,
                    pageRequest,
                    dataSourceRequest.getSelectedItemId());
            ans.setTotalElements(totalMetadataFacetCount);
            return ans;

        } catch (RuntimeException e) {
            logger.error("Failed to list {} counts with filter {}", fieldType.getSolrName(), dataSourceRequest.getFilter(), e);
            throw e;
        }
    }

    private Pair<Map<String, Long>, Map<String, Long>> runQuery(DataSourceRequest dataSourceRequest, CatFileFieldType field) {
        int page = dataSourceRequest.getPage();
        int pageSize = dataSourceRequest.getPageSize();

        dataSourceRequest.setPage(1);
        dataSourceRequest.setPageSize(ControllerUtils.FACET_MAX_PAGE_SIZE_FILECOUNT);

        Map<String, Long> filteredMap = fileCatService.getFieldTermCountMap(dataSourceRequest, field);

        Map<String, Long> noFilterMap = new HashMap<>();
        if (dataSourceRequest.hasFilter()) {
            dataSourceRequest.clearFilters();
            noFilterMap = fileCatService.getFieldTermCountMap(dataSourceRequest, field);
        }

        dataSourceRequest.setPage(page);
        dataSourceRequest.setPageSize(pageSize);

        return Pair.of(filteredMap, noFilterMap);
    }

    private String getItemId(String item) {
        return item;
    }

    private FacetPage<AggregationCountItemDTO<String>> convertFacetFieldResultToDto(
            Pair<Map<String, Long>, Map<String, Long>> result, PageRequest pageRequest, String selectedItemId) {

        Map<String, AggregationCountItemDTO<String>> values = new HashMap<>();

        Map<String, Long> filteredResults = result.getKey();
        Map<String, Long> notFilteredResults = result.getValue();

        logger.debug("convert Facet Field result for {} elements. DB returned {} elements", filteredResults.size());
        long pageAggCount = 0;
        String selected = null;
        for (Map.Entry<String, Long> entry : filteredResults.entrySet()) {

            Long countFiltered = entry.getValue();
            Long countNotFiltered = (notFilteredResults == null ? null : notFilteredResults.get(entry.getKey()));
            AggregationCountItemDTO<String> item = values.get(entry.getKey());
            if (item == null) {
                item = new AggregationCountItemDTO<>(entry.getKey(), countFiltered);
                values.put(entry.getKey(), item);
                if (countNotFiltered != null) {
                    item.setCount2(countNotFiltered);
                }
                if (selectedItemId != null && selectedItemId.equals(String.valueOf(entry.getKey()))) {
                    selected = entry.getKey();
                }
            } else {
                item.incrementCount(countFiltered);
                if (countNotFiltered != null) {
                    item.incrementCount2(countNotFiltered);
                }
            }
            pageAggCount += countFiltered;
        }

        List<AggregationCountItemDTO<String>> content = new ArrayList<>(values.values());

        // even though we must go over all facet options to get correct values
        // we should return to ui only what was requested
        return ControllerUtils.getRelevantResultPage(content, pageRequest,
                selected,
                this::getItemId,
                pageAggCount, (long) filteredResults.size());
    }

    // go over all files with metadata and try to re-translate using current config file
    @AutoAuthenticate
    public void reTranslateFileMetadata() {
        int pageCount = 0;
        String cursor = "*";
        boolean lastPage;
        do {
            Query query = Query.create()
                    .addFilterWithCriteria(RAW_METADATA, SolrOperator.PRESENT, null)
                    .setRows(metadataReTransformerPageSize).setCursorMark(cursor)
                    .addField(ID)
                    .addField(RAW_METADATA)
                    .addField(EXTERNAL_METADATA)
                    .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

            solrSpecificationFactory.addFilterDeletedFiles(query);

            long start = System.currentTimeMillis();
            logger.debug("loading page {} of {} groups", pageCount, metadataReTransformerPageSize);

            QueryResponse resp = fileCatService.runQuery(query.build());

            logger.trace("get page {} of {} files took {} ms", pageCount, resp.getResults().size(), (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();

            cursor = resp.getNextCursorMark();
            List<SolrInputDocument> filesToCng = new ArrayList<>();
            if (resp.getResults().size() > 0) {
                resp.getResults().forEach(doc -> {
                    String id = (String) doc.getFieldValue(ID.getSolrName());
                    String rawMetadata = (String) doc.getFieldValue(RAW_METADATA.getSolrName());
                    List<String> externalMetadata = (List<String>) doc.getFieldValue(EXTERNAL_METADATA.getSolrName());
                    if (!rawMetadata.isEmpty()) {
                        try {
                            Map<String, String> metadata = ModelDtoConversionUtils.getMapper().readValue(rawMetadata, Map.class);
                            if (externalMetadata != null) {
                                externalMetadata.forEach(em -> {
                                    try {
                                        LabelResult labelResult = ModelDtoConversionUtils.getMapper().readValue(em, LabelResult.class);
                                        if (labelResult instanceof MipLabelResultTyped) {
                                            MipLabelResultTyped mipLabelResult = (MipLabelResultTyped) labelResult;
                                            metadata.put(mipMetadataType, mipLabelResult.getName());
                                        }
                                    } catch (Exception e) {
                                        logger.error("problem from json on external metadata for file {} metadata {}", id, em);
                                    }
                                });
                            }
                            FileCatBuilder builder = FileCatBuilder.create();
                            builder.setId(id);
                            builder.setState(null);
                            ClaFilePropertiesDto props = ClaFilePropertiesDto.create();
                            props.setMetadata(metadata);
                            metadataTranslationService.setMetadata(builder, props);
                            filesToCng.add(builder.buildAtomicUpdate());
                        } catch (Exception e) {
                            logger.error("failed processing file {}", id, e);
                        }
                    }
                });
                if (!filesToCng.isEmpty()) {
                    fileCatService.saveAll(filesToCng);
                    fileCatService.softCommitNoWaitFlush();
                }
            }
            logger.trace("update page {} of {} files took {} ms", pageCount, resp.getResults().size(), (System.currentTimeMillis() - start));
            pageCount++;
            lastPage = resp.getResults().size() < metadataReTransformerPageSize;
        } while (!lastPage);
    }
}
