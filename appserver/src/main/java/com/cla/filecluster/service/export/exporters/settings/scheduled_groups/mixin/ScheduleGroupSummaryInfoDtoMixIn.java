package com.cla.filecluster.service.export.exporters.settings.scheduled_groups.mixin;

import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class ScheduleGroupSummaryInfoDtoMixIn {

    @JsonUnwrapped
    private ScheduleGroupDto scheduleGroupDto;

}
