package com.cla.filecluster.service.mcf;

import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Deprecated
@Service
public class TransformationConnectionService {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
			.getLogger(TransformationConnectionService.class);

//	@Value("classpath:mcf/transformationConnection.json")
//	private Resource transformationConnection;

	private String transformationConnectionTemplate;

	private HttpHost httpHost;

	@Value("${transformationConnectionName:cla-metadata-transformer}")
	private String transformationConnectionName;

	private boolean clatTransformationCreated;

	public boolean isClatTransformationCreated() {
		return clatTransformationCreated;
	}
	
	

	public void setClatTransformationCreated(final boolean clatTransformationCreated) {
		this.clatTransformationCreated = clatTransformationCreated;
	}



	public String getTransformationConnectionName() {
		return transformationConnectionName;
	}

//	@PostConstruct
//	private void init() throws IOException {
//		httpHost = new HttpHost("localhost", 8345, "http");
//		try (InputStream is = transformationConnection.getInputStream()) {
//			transformationConnectionTemplate = IOUtils.toString(is);
//		}
//	}

	public String findAll() {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			final HttpGet getRequest = new HttpGet("/mcf-api-service/json/transformationconnections");
			final HttpResponse response = httpclient.execute(httpHost, getRequest);

			if (response.getStatusLine().getStatusCode() != 200)
				throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			final String transformationconnections = EntityUtils.toString(response.getEntity());
			return transformationconnections;
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void createClaTransformationConnection() {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			final HttpPut putRequest = new HttpPut("/mcf-api-service/json/transformationconnections/"
					+ transformationConnectionName);
			final String transformationConnection = String.format(transformationConnectionTemplate,
					transformationConnectionName);
			logger.trace("Creating the following transformationconnection:\n {}", transformationConnection);
			final StringEntity se = new StringEntity(transformationConnection);
			se.setContentType("application/json");
			putRequest.setEntity(se);
			final HttpResponse response = httpclient.execute(httpHost, putRequest);

			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode < 200 || statusCode >= 300)
				throw new IOException("Failed : HTTP error code : " + statusCode);
			clatTransformationCreated = true;
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
}
