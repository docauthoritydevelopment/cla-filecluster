package com.cla.filecluster.service.api.reports;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.FilterApiController;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.report.UserViewDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * api for user view with or without a filter CRUD
 *
 * Created by: yael
 * Created on: 6/11/2018
 */
@RestController
@RequestMapping("/api/userviewfilter")
public class UserViewAndFilterApiController {

    @Autowired
    private UserViewDataApiController userViewDataApiController;

    @Autowired
    private FilterApiController savedFilterApiController;

    @Autowired
    private UserViewDataService userViewDataService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(method = RequestMethod.PUT)
    public UserViewDataDto createUserViewData(@RequestBody UserViewDataDto newUserView) {
        if (newUserView.getFilter() != null) {
            if (newUserView.getFilter().getId() <= 0) {
                SavedFilterDto filter = savedFilterApiController.createSavedFilter(newUserView.getFilter());
                newUserView.setFilter(filter);
            } else {
                SavedFilterDto filter = savedFilterApiController.updateSavedFilter(newUserView.getFilter());
                newUserView.setFilter(filter);
            }
        }
        return userViewDataApiController.createUserViewData(newUserView);
    }

    @RequestMapping(method = RequestMethod.POST)
    public UserViewDataDto updateUserViewData(@RequestBody UserViewDataDto userViewDto) {
        if (userViewDto.getFilter() != null) {
            if (userViewDto.getFilter().getId() <= 0) {
                SavedFilterDto filter = savedFilterApiController.createSavedFilter(userViewDto.getFilter());
                userViewDto.setFilter(filter);
            } else {
                SavedFilterDto filter = savedFilterApiController.updateSavedFilter(userViewDto.getFilter());
                userViewDto.setFilter(filter);
            }
        }
        return userViewDataApiController.updateUserViewData(userViewDto);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteUserViewData(@PathVariable long id) {
        UserViewDataDto savedUserView = userViewDataApiController.getById(id);
        if (savedUserView == null) {
            throw new BadRequestException(messageHandler.getMessage("user-view-data.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        boolean deleteFilter = false;
        if (savedUserView.getFilter() != null) {
            List<UserViewDataDto> byFilter = userViewDataService.listUserViewDataByFilterId(savedUserView.getFilter().getId());
            if (byFilter.size() > 1) {
                deleteFilter = false;
            } else if (byFilter.size() == 1 && byFilter.get(0).getId() != id) {
                deleteFilter = false;
            } else {
                deleteFilter = true;
            }
        }

        userViewDataApiController.deleteUserViewData(id);

        if (deleteFilter) {
            savedFilterApiController.deleteSavedFilter(savedUserView.getFilter().getId());
        }
    }
}
