package com.cla.filecluster.service.export.exporters.settings.doctype;

import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.settings.doctype.mixin.DocTypeDtoMixIn;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.filecluster.service.export.exporters.settings.doctype.DocTypeSettingsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/11/2019
 */
@Slf4j
@Service
public class DocTypeSettingsExporter extends PageCsvExcelExporter<DocTypeDto> {

    @Autowired
    private DocTypeAppService docTypeAppService;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    private final static String[] constantHeader = {ID, NAME, UNIQUE, PARENT};

    private static String[] entireHeader;

    @PostConstruct
    public void init() {
        List<FileTagType> tagTypes = fileTagTypeService.getFileTagTypes();
        entireHeader = new String[tagTypes.size()+constantHeader.length];
        int i = 0;
        for (String h : constantHeader) {
            entireHeader[i] = h;
            ++i;
        }
        for (FileTagType tagType : tagTypes) {
            entireHeader[i] = tagType.getName();
            ++i;
        }
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DOC_TYPE_SETTINGS);
    }

    @Override
    protected String[] getHeader() {
        return entireHeader;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(DocTypeDto.class, DocTypeDtoMixIn.class);
    }

    @Override
    protected Page<DocTypeDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return docTypeAppService.getAllDocTypesAsDto(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(DocTypeDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();

        if (base.getFileTagTypeSettings() != null && !base.getFileTagTypeSettings().isEmpty()) {
            base.getFileTagTypeSettings().forEach(tagType -> {
                List<String> tagList = tagType.getFileTags().stream().map(FileTagDto::getName).collect(Collectors.toList());
                String tags = tagList.stream().map(String::valueOf).collect(Collectors.joining(" ", "(", ")"));
                result.put(tagType.getFileTagType().getName(), tags);
            });
        }

        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
