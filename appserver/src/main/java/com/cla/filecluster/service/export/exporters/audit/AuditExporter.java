package com.cla.filecluster.service.export.exporters.audit;

import com.cla.filecluster.domain.dto.AuditDto;
import com.cla.filecluster.service.api.AuditApiController;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import static com.cla.filecluster.service.export.exporters.audit.AuditHeaderFields.*;

import java.util.Map;

@Slf4j
@Component
public class AuditExporter  extends PageCsvExcelExporter<AuditDto> {

	final static String[] HEADER = {AUDIT, ACTION, DATE, DETAILS, USERNAME};

	@Autowired
	private AuditApiController auditApiController;

	@Override
	protected String[] getHeader() {
		return HEADER;
	}

	@Override
	protected void configureMapper(CsvMapper mapper) {
		mapper.addMixIn(AuditDto.class, AuditExportDtoMixin.class);
	}

	@Override
	protected Page<AuditDto> getData(Map<String, String> requestParams) {
		Page<AuditDto> auditTrail = auditApiController.getAuditTrail(requestParams);
		return auditTrail;
	}

	@Override
	protected Map<String, Object> addExtraFields(AuditDto base, Map<String, String> requestParams) {
		return null;
	}

	@Override
	public PagingMethod getPagingMethod() {
		return PagingMethod.MULTI;
	}

	@Override
	public boolean doesAccept(ExportType exportType, Map<String, String> params) {
		return exportType.equals(ExportType.AUDIT);
	}
}
