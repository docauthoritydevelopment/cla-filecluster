package com.cla.filecluster.service.export.exporters.file.acl;

import com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.nio.file.attribute.AclEntryType;

public class RootFolderSharePermissionDtoMixin {

    @JsonProperty(FileCountHeaderFields.USER)
    private String user;
}
