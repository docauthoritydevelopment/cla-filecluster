package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.progress.ProgressStatusDto;
import com.cla.filecluster.service.progress.ProgressBean;
import com.cla.filecluster.service.progress.ProgressService;
import com.cla.filecluster.service.progress.ProgressType;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/progress")
public class ProgressApiController {
    private static final Logger logger = LoggerFactory.getLogger(ProgressApiController.class);

    @Autowired
    private ProgressService progressService;

    @RequestMapping(value = "/{progressId}")
    public ProgressStatusDto progress(@PathVariable final String progressId) {
        logger.trace("progress request received for artifact: {}", progressId);
        return progressService.getProgress(progressId).toStatusDto();
    }

    @RequestMapping(value = "/list")
    public Set<ProgressStatusDto> list() {
        logger.trace("export progress request received.");
        return progressService.getProgresses()
                .stream()
                .map(ProgressBean::toStatusDto)
                .collect(Collectors.toSet());
    }

    @RequestMapping(value = "/listByType/{progressType}")
    public Set<ProgressStatusDto> list(@PathVariable final String progressTypeStr) {
        logger.trace("export progress request received.");
        val progressType = ProgressType.valueOf(progressTypeStr.toUpperCase());
        return progressService.getProgresses(progressType)
                .stream()
                .map(ProgressBean::toStatusDto)
                .collect(Collectors.toSet());
    }
}
