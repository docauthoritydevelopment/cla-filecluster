package com.cla.filecluster.service.files.managers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.extraction.FileHighlightsDto;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.file.FileAclDataDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.messages.FileContentResultMessage;
import com.cla.common.domain.dto.messages.FileContentType;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.AccessState;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.FileContent;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFileContentSearchRepository;
import com.cla.filecluster.repository.solr.SolrQueryResponse;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.util.PermissionCheckUtils;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeUtility;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.common.domain.dto.security.Authority.Const.*;

/**
 *
 * Created by liron on 4/25/2017.
 */
@Service
public class FileContentService {

    private static final Logger logger = LoggerFactory.getLogger(FileContentService.class);

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private ExtractTokenizer extractTokenizer;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private RemoteFileContentManager remoteFileContentManager;

    @Autowired
    private SolrQueryBuilder solrQueryBuilder;

    @Autowired
    private SolrFileContentSearchRepository solrFileContentSearchRepository;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${report.streamingBuffer:4096}")
    private int streamingBufferSize;

    @Value("${file-content-service.mp-response.timeout-ms:90000}")
    private long fileContentMediaProcessorResponseTimeoutMS;

    @Value("${file-content-service.root-folder-check.threshold:60000}")
    private long timeSyncCheckedThreshold;

    @Value("${solr.client.enforce-acls:true}")
    private boolean enforceAcls;

    @Value("${security.acl.generic-tokens:\\\\Everyone}")
    private String[] genericTokensArray;

    @Value("${solr.client.case-sensitive-acls:false}")
    private boolean caseSensitiveAcls;

    @Value("${security.share-permission.read-mask:131241}")
    private long readMaskSharePermission;

    private ConcurrentHashMap<String,CompletableFuture<FileContentResultMessage>> lockMap = new ConcurrentHashMap<>();

    private ConcurrentHashMap<String,CompletableFuture<IngestResultMessage>> lockMapIngest = new ConcurrentHashMap<>();

    private List<String> genericTokens;

    private class RootFolderAccessCheckDto {
        volatile long rootFolderId;
        volatile boolean isCheckActive;
        volatile long lastAccessCheckTimeMS;
        volatile long issueAccessCheckTimeMS;
        private volatile int statusOrdinal;

        void setStatus(DirectoryExistStatus status) {
            statusOrdinal = status.ordinal();
        }
        DirectoryExistStatus getStatus() {
            return DirectoryExistStatus.values()[statusOrdinal];
        }
    }

    private ConcurrentHashMap<Long,RootFolderAccessCheckDto> appServerRootFolderAccessibilityMap = new ConcurrentHashMap<>();

    static private final Pattern removeMultipleLineBreak = Pattern.compile("(\\n(\\n|\\s)+)");

    @PostConstruct
    public void init() {
        genericTokens = PermissionCheckUtils.getGenericTokensAsList(genericTokensArray, caseSensitiveAcls);
    }

    public boolean userAllowedViewFile(long fileId) {
        return userHasAccessToFile(fileId, false);
    }

    public boolean userAllowedDownloadFile(long fileId) {
        return userHasAccessToFile(fileId, true);
    }

    private boolean userHasAccessToFile(long fileId, boolean includingContent) {
        if (!enforceAcls) {
            return true;
        }
        AccessState accessState = includingContent ?
                PermissionCheckUtils.getCurrentContentAccessState(
                        sharePermissionService.getAllSharePermissions(), genericTokens, caseSensitiveAcls, readMaskSharePermission) :
                PermissionCheckUtils.getCurrentFileAccessState(
                        sharePermissionService.getAllSharePermissions(), genericTokens, caseSensitiveAcls, readMaskSharePermission);
        return checkAccessByState(accessState, fileId);
    }

    private boolean checkAccessByState(AccessState accessState, long fileId){
        if (accessState.isAccessDenied()){
            return false;
        }
        // for universal access - no need to modify the query
        if (accessState.isAllowAllFiles()){
            return true;
        }

        // need to be an admin in order to get the file data from SOLR,
        // so that we could check accessibility for the current user,
        // since the user might not have access even to view it
        List<SolrFileEntity> categoryFileWrapper = Lists.newArrayList();
        AuthenticationHelper.doWithAuthentication(AutoAuthenticate.ADMIN, true,
                () -> categoryFileWrapper.add(fileCatService.getFileEntity(fileId)),null);
        SolrFileEntity categoryFile = categoryFileWrapper.get(0);
        if (categoryFile == null) {
            logger.trace("Failed to find file record with ID {}, granting access by default.", fileId);
            return true;
        }

        FileAclDataDto aclDataDto = FileCatUtils.extractAclData(categoryFile);
        List<String> roles = FileCatUtils.getRoles(categoryFile);
        boolean bizRolesAllowPermitted = accessState.isAllowPermittedFiles();
        Set<String> allowAllFuncRoles = accessState.getAllowAllFuncRoles();
        Set<String> allowPermittedFuncRoles = accessState.getAllowPermittedFuncRoles();
        boolean rfAllowed = !accessState.getDeniedRootFolders().contains(categoryFile.getRootFolderId());

        boolean hasAllow = aclMatchFound(aclDataDto.getAclReadData(), accessState.getAccessTokens());
        logger.trace("Compared file 'allow read' ACLs to the user ACLs, {} a match.",
                hasAllow ? "found" : "didn't find");

        boolean hasDeny = aclMatchFound(aclDataDto.getAclDeniedReadData(), accessState.getAccessTokens());
        logger.trace("Compared file 'deny read' ACLs to the user ACLs, {} a match.",
                hasDeny ? "found" : "didn't find");

        // if BizRoles allow ALL permitted files, and there are no functional roles that allow ALL files
        // modify the query and return, because BizRole allow ALL permitted wins over FuncRole allow ALL permitted
        if (bizRolesAllowPermitted && allowAllFuncRoles.isEmpty()){
            logger.trace("Operational roles allow all permitted files, no relevant data roles were found, " +
                    "resulting access state: {}.", hasAllow && !hasDeny);
            return hasAllow && !hasDeny && rfAllowed;
        }
        if (allowAllFuncRoles.isEmpty() && allowPermittedFuncRoles.isEmpty()){
            // no business roles and no functional roles to allow viewing any files
            logger.trace("No operational roles and no data roles to allow viewing any files were found, denying access");
            return false;
        }

        boolean hasFuncRole = false;
        for(String r : roles) {
            if (allowAllFuncRoles.contains(r)) {
                hasFuncRole = true;
                break;
            }
        }
        // if BizRoles allow access to permitted files and there are FuncRoles that allow access to ALL files
        // then build query combined of allow permitted on all files and allow all files with FuncRoles
        if (bizRolesAllowPermitted){
            // where ((ACL_READ in accessTokens) AND (ACL_DENIED_READ not in accessTokens))
            //    OR (tag2 contains one of allowAllFuncRoles)
            logger.trace("Operational roles allow all permitted files, ACLs access state: {}.",
                    (hasAllow && !hasDeny && rfAllowed) ? "allow" : "deny");
            logger.trace("{} data roles that allow access to ALL files.", hasFuncRole ? "Found" : "Didn't find");
            return (hasAllow && !hasDeny && rfAllowed) || hasFuncRole;
        }

        boolean hasPermittedFuncRole = false;
        for(String r : roles) {
            if (allowPermittedFuncRoles.contains(r)) {
                hasPermittedFuncRole = true;
                break;
            }
        }

        // if both kinds of functional roles are present
        if (!allowAllFuncRoles.isEmpty() && !allowPermittedFuncRoles.isEmpty()){
            logger.trace("ACLs access state: {}.", (hasAllow && !hasDeny && rfAllowed) ? "allow" : "deny");
            logger.trace("{} data roles that allow access to relevant files.",
                    (hasFuncRole || hasPermittedFuncRole) ? "Found" : "Didn't find");
            return (hasFuncRole || hasPermittedFuncRole) && (hasAllow && !hasDeny && rfAllowed);
        }

        if (!allowAllFuncRoles.isEmpty()){
            //where (tag2 contains one of allowAllFuncRoles)
            logger.trace("At least one of the user data roles allows access to ALL files, " +
                    "according to the file data roles access is {}.", hasFuncRole ? "granted" : "denied");
            return hasFuncRole;
        }
        else{
            // where (tag2 contains one of  allowPermittedFuncRoles) AND
            //       (ACL_READ in accessTokens) AND
            //       (ACL_DENIED_READ not in accessTokens))
            logger.trace("Access is {} based on 'access permitted' data role {} and ACL state {}. ",
                    hasPermittedFuncRole && hasAllow && !hasDeny && rfAllowed ? "granted" : "denied",
                    hasPermittedFuncRole, hasAllow && !hasDeny && rfAllowed);
            return hasPermittedFuncRole && hasAllow && !hasDeny && rfAllowed;
        }
    }

    @Scheduled(initialDelayString = "${root-folders.accessible.fc.periodic.initial-ms:10000}",
            fixedRateString = "${root-folders.accessible.fc.schedule.period-ms:60000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void validateExpiredRequests() {

        appServerRootFolderAccessibilityMap.forEach((key, value) -> {
            long timeSyncChecked = System.currentTimeMillis() - value.issueAccessCheckTimeMS;
            if (value.isCheckActive && timeSyncChecked > fileContentMediaProcessorResponseTimeoutMS) {
                value.isCheckActive = false;
                value.setStatus(DirectoryExistStatus.UNKNOWN);
            }
        });
    }

    public String addRequestToLockMap(CompletableFuture<FileContentResultMessage> completableFuture) {
        String uniqueID = UUID.randomUUID().toString();
        lockMap.put(uniqueID, completableFuture);

        return uniqueID;
    }

    public CompletableFuture<FileContentResultMessage> getCompletableFutureLockByRequestId(String requestId) {
        return lockMap.get(requestId);
    }

    public void removeRequestFromLockMap(String requestId) {
        lockMap.remove(requestId);
    }

    public String addIngestRequestToLockMap(CompletableFuture<IngestResultMessage> completableFuture) {
        String uniqueID = UUID.randomUUID().toString();
        lockMapIngest.put(uniqueID, completableFuture);

        return uniqueID;
    }

    public CompletableFuture<IngestResultMessage> getCompletableFutureLockByIngestRequestId(String requestId) {
        return lockMapIngest.get(requestId);
    }

    public void removeIngestRequestFromLockMap(String requestId) {
        lockMapIngest.remove(requestId);
    }

    public SolrSpecification addFileContentWithSolrSpecification(@NotNull long fileId, int itemFullLength,
                                                                 @NotNull DataSourceRequest dataSourceRequest,
                                                                 String additionalTextTokens, String requestId) {
        final ClaFile fileMetData = getClaFile(fileId);
        PageRequest pageRequest = getPageRequest(dataSourceRequest);
        if (pageRequest == null){
            return null;
        }

        SolrSpecification solrSpecification = createSolrSpecification(fileId, itemFullLength, dataSourceRequest, pageRequest.getPageSize());
        if (additionalTextTokens != null) {
            solrSpecification.addToContentFilter(additionalTextTokens);
        }
        solrQueryBuilder.getFilterQueries(solrSpecification);  //content filters from filter are added to contentFilter array
        ProcessTokens(solrSpecification);

        if (fileMetData.getType() == FileType.EXCEL) { //read excel file and extract cell locations
            addExcelFileContent(fileId, itemFullLength, dataSourceRequest, additionalTextTokens, requestId, fileMetData,solrSpecification);
        }
        else {
            addOtherFileContent(fileId,  requestId, fileMetData);
        }
        return solrSpecification;

    }

    public void addFileContent(@NotNull long fileId, int itemFullLength, @NotNull DataSourceRequest dataSourceRequest,
                               String additionalTextTokens, String requestId) {
        final ClaFile fileMetData = getClaFile(fileId);
        if (fileMetData.getType() == FileType.EXCEL) { //read excel file and extract cell locations
            addExcelFileContent(fileId, itemFullLength, dataSourceRequest, additionalTextTokens, requestId, fileMetData,null);
        }
        else {
            addOtherFileContent(fileId,  requestId, fileMetData);
        }
    }


    public FileHighlightsDto getHighlightResult(FileContentResultMessage fileContentResultMessage, long fileId ,int itemFullLength,
                                                DataSourceRequest dataSourceRequest,String additionalTextTokens, SolrSpecification solrSpecification)
    {
        SolrQueryResponse solrQueryResponse;
        List<String> highlights;
        PageRequest pageRequest = getPageRequest(dataSourceRequest);
        FileContent fileContent = new FileContent(fileContentResultMessage.getClaFileId(),fileContentResultMessage.getPayload().getContent());
        logger.debug("save Filecontent {} to a temporary core for highlighting",fileContent.getId());
        solrFileContentSearchRepository.save(fileContent);
        solrSpecification.setId(fileContent.getId()); //uniqueID of the newly solr file record
        logger.trace("Perform highlighting search on core");
        solrQueryResponse = solrFileContentSearchRepository.runQuery(solrSpecification, pageRequest);

        highlights = solrQueryResponse.getHighlighting(solrSpecification.getId());
        long numFound = solrQueryResponse.getQueryResponse().getResults().getNumFound();
        logger.debug("list Files returned {} NumFound", numFound);


        highlights.forEach(h -> h=removeMultipleLineBreak.matcher(h).replaceAll("\\\n"));
        for (int i = 0; i < highlights.size(); i++) { //replace consequetive \n\n\n with one \n
            highlights.set(i,removeMultipleLineBreak.matcher(highlights.get(i)).replaceAll("\\\n"));
        }
        FileHighlightsDto result = new FileHighlightsDto();
        result.setLocations(fileContentResultMessage.getPayload().getLocations());
        result.setSnippets(highlights);

        result.setFileContentLastModifiedDate(fileContentResultMessage.getPayload().getModTimeMilli());

        return result;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
    public void extractFileContent(long fileId, String requestId) {
        logger.debug("stating extract file content from media processor", fileId);
        final ClaFile claFile = getClaFile(fileId);
        if (claFile == null) {
            throw new BadRequestException(messageHandler.getMessage("file.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        Optional<MediaConnectionDetailsDto> connectionDetailsDto =  mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(claFile.getRootFolderId(), true);
        RootFolder rf = docStoreService.getRootFolder(claFile.getRootFolderId());
        remoteFileContentManager.doFileContentOperationAsync(requestId, FileContentType.EXTRACT_DOWNLOAD_FILE, rf, claFile,
                connectionDetailsDto.orElseGet(null),null);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
    public void openFile(FileContentResultMessage fileContentResultMessage, HttpServletRequest request, HttpServletResponse response, long id) throws IOException {
        byte[] bytesToSend = fileContentResultMessage.getPayload().getFileContentHolder().getBytes();
        if (fileContentResultMessage.getPayload().getFileLength().intValue() != bytesToSend.length) {
            logger.info("Unexpected size mismatch: bufferLen={}, payload fileSize={}", bytesToSend.length, fileContentResultMessage.getPayload().getFileLength().intValue());
        }
        openFile(bytesToSend,
                fileContentResultMessage.getPayload().getFileName(),
                fileContentResultMessage.getPayload().getBaseName(),
                fileContentResultMessage.getPayload().getFileLength().intValue(), request, response, id,
                fileContentResultMessage.getPayload().getFileType()
        ) ;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT})
    public void sendRequestForLogsForMediaProcessor(String requestId, Long instanceId) {
        remoteFileContentManager.doGetMediaProcessorLogsAsync(requestId, instanceId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
    public void openFile(byte[] data, String fileName, String baseName,
                         Integer fileSize, HttpServletRequest request, HttpServletResponse response, long id, FileType fileType) throws IOException
    {
        logger.debug("Receive file {} for download data", id);
        InputStream inputStream = new ByteArrayInputStream(data);
        openFile(inputStream, fileName, baseName, fileSize, request, response, id, fileType);
    }

    public void openFile(InputStream inputStream, String fileName, String baseName, Integer fileSize,
                         HttpServletRequest request, HttpServletResponse response, long id, FileType fileType) throws IOException {
        final ServletContext context = request.getServletContext();
        OutputStream outStream = null;
        try{

            if (fileType != null && FileType.MAIL.equals(fileType)) {
                baseName = FilenameUtils.removeExtension(baseName) + ".eml";
            }

            response.setContentType(context.getMimeType(fileName));

            if (fileSize != null) {
                response.setContentLength(fileSize);
            }

            // set headers for the response
            final String headerKey = "Content-Disposition";
            final String headerValue = String.format("attachment; filename=\"%s\"",
                    MimeUtility.encodeWord(baseName, "UTF-8",null)
            );//fileContentResultMessage.getPayload().getFileName());
            response.setHeader(headerKey, headerValue);

            // get output stream of the response
            outStream = response.getOutputStream();

            final byte[] buffer = new byte[streamingBufferSize];
            int bytesRead;

            // write bytes read from the input stream into the output stream
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
        } catch (final Exception e) {
            logger.warn("Unexpected error id={}. {}", id, e.getMessage());
            response.setStatus(500);
            response.getWriter().append("Unexpected error: ").append(e.toString());
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outStream != null) {
                outStream.close();
            }
        }
    }

    public Set<String> getPatternsMatchesSet(FileContentResultMessage fileContentResultMessage) {
        FileContent fileContent = new FileContent(fileContentResultMessage.getClaFileId(),fileContentResultMessage.getPayload().getContent());
        Set<String> matches = claFileSearchPatternService.getAllMatchingStrings(fileContent.getContent(), 100);
        StringBuilder matchesFilter = new StringBuilder();
        matches.forEach(element -> matchesFilter.append(element).append(" "));

        return matches;
    }

    private void addOtherFileContent(@NotNull long fileId,String  requestId, ClaFile fileMetData) {
        logger.debug("addFileContentToSolr: Highlight text in file {}", fileId);
        Optional<MediaConnectionDetailsDto> detailsOp =
                mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(fileMetData.getRootFolderId(), true);
        RootFolder rf = docStoreService.getRootFolder(fileMetData.getRootFolderId());
        remoteFileContentManager.doFileContentOperationAsync(requestId, FileContentType.EXTRACT_HIGHLIGHT_FILE, rf, fileMetData, detailsOp.orElseGet(null),null);

    }

    private void addExcelFileContent(@NotNull long fileId, int itemFullLength, @NotNull DataSourceRequest dataSourceRequest, String additionalTextTokens,
                                     String requestId, ClaFile fileMetData, SolrSpecification solrSpecification)
    {
        logger.debug("Highlight text in excel file {}", fileId);
        Set<String> textTokens = null;

        Optional<MediaConnectionDetailsDto> detailsOp =
                mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(fileMetData.getRootFolderId(), true);
        if(fileMetData.getType() == FileType.EXCEL) {
            logger.debug("Highlight Excel file");
            if(solrSpecification != null) {
                List<String> tokens = solrSpecification.getContentFilters();
                textTokens = tokens.stream().flatMap(token -> Stream.of(token.split(" "))).collect(Collectors.toSet()); //split multiple words to tokens// hashedTokensConsumer = new DictionaryHashedTokensConsumerImpl(textTokens, true);
            }
        }
        RootFolder rf = docStoreService.getRootFolder(fileMetData.getRootFolderId());
        remoteFileContentManager.doFileContentOperationAsync(requestId, FileContentType.EXTRACT_HIGHLIGHT_FILE, rf, fileMetData, detailsOp.orElseGet(null),textTokens);
    }

    private SolrSpecification createSolrSpecification(@NotNull long fileId,int itemFullLength, @NotNull DataSourceRequest dataSourceRequest,int highlightPageSize) {

        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
        solrSpecification.setSortField(CatFileFieldType.SORT_NAME.getSolrName());
        solrSpecification.setFileID(fileId);
        solrSpecification.setHighlightItemFullLength(itemFullLength);
        solrSpecification.setHighlightPageSize(highlightPageSize);
        return solrSpecification;
    }

    private PageRequest getPageRequest(DataSourceRequest dataSourceRequest) {
        if (dataSourceRequest.getPageSize() == 0) {
            return null;
        }
        Sort sort;
        if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
            //Order the records according to default order TODO - field names should come from Hibernate/JPA and not hardcoded
            Sort.Order baseNameOrder = new Sort.Order(Sort.Direction.ASC,"baseName");
            Sort.Order fileNameOrder= new Sort.Order(Sort.Direction.ASC,"fullPath");
            Sort.Order idOrder = new Sort.Order(Sort.Direction.ASC,"id");
            sort = new Sort(baseNameOrder,fileNameOrder,idOrder);
        }
        else {
            sort = DataSourceRequest.convertSort(dataSourceRequest, FileDtoFieldConverter::convertDtoField);
        }
        return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(),sort);
    }

    private void ProcessTokens(SolrSpecification solrSpecification) {
        List<String> contentFilters = solrSpecification.getContentFilters();
        List<String> processedTokens = new ArrayList<>();
        for (String contentToken : contentFilters) {
            //Trying to immitate token analysis chain as defined in solr
            processedTokens.addAll(extractTokenizer.fillTokens(extractTokenizer.filter(contentToken),null));
        }
        solrSpecification.setContentFilters(processedTokens);
    }

    private ClaFile getClaFile(final Long fileId) {
        final SolrFileEntity f = fileCatService.getFileEntity(fileId);
        if (f == null) {
            throw new RuntimeException("File does not exist.claFileId:" + fileId);
        }
        return fileCatService.getFromEntity(f);
    }


    public void setIsDirectoryExistStatus(Long rootFolderId, DirectoryExistStatus isDirectoryExistStatus) {
        FileContentService.RootFolderAccessCheckDto accessCheckDto = appServerRootFolderAccessibilityMap.get(rootFolderId);
        if (accessCheckDto == null) {
            logger.warn("Could not find rootFolderId {} for setIsDirectoryExistStatus response handling", rootFolderId);
            return;
        }
        if( accessCheckDto.isCheckActive) { //TODO liora If request returned too late, ignore?
            logger.debug("setIsDirectoryExistStatus for folder {} status {} ", rootFolderId, isDirectoryExistStatus);
            accessCheckDto.setStatus(isDirectoryExistStatus);
            accessCheckDto.lastAccessCheckTimeMS = System.currentTimeMillis();
            accessCheckDto.isCheckActive = false;
            docStoreService.updateRootFolderAccessible(rootFolderId, isDirectoryExistStatus);
        }
    }

    public DirectoryExistStatus isDirectoryExists(RootFolderDto rootFolderDto) {
        // Look in cache
        Long rootFolderId = rootFolderDto.getId();

        // check if should perform check even if time hasnt passed
        boolean force = docStoreService.isRootFolderForceAccessibilityCheck(rootFolderId);

        FileContentService.RootFolderAccessCheckDto accessCheckDto = appServerRootFolderAccessibilityMap.get(rootFolderId);
        if (accessCheckDto == null) {
            accessCheckDto = new FileContentService.RootFolderAccessCheckDto();
            accessCheckDto.isCheckActive = false;
            accessCheckDto.lastAccessCheckTimeMS = 0;
            accessCheckDto.rootFolderId = rootFolderId;
            accessCheckDto.setStatus(DirectoryExistStatus.UNKNOWN);
            appServerRootFolderAccessibilityMap.put(rootFolderId, accessCheckDto);
        }

        if (!force) {
            long timeSyncChecked = System.currentTimeMillis() - accessCheckDto.lastAccessCheckTimeMS;
            if (accessCheckDto.isCheckActive) {
                logger.trace("RF #{} accessibility check is in progress sync {}. Return immediate: {}", rootFolderId, accessCheckDto.lastAccessCheckTimeMS, accessCheckDto.getStatus());
                return accessCheckDto.getStatus();
            }
            if (timeSyncChecked < timeSyncCheckedThreshold) {
                logger.trace("RF #{} accessibility checked {} seconds ago. Return immediate: {}", rootFolderId, (timeSyncChecked / 100) / 10f, accessCheckDto.getStatus());
                return accessCheckDto.getStatus();
            }
        }

        try {
            // Changes are seen by other threads
            accessCheckDto.issueAccessCheckTimeMS = System.currentTimeMillis();
            accessCheckDto.isCheckActive = true;
            logger.trace("Send RF #{} accessibility checked to MP", rootFolderId);

            Optional<MediaConnectionDetailsDto> connectionDetailsDto = mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(rootFolderId, true);
            if (!connectionDetailsDto.isPresent()) {
                logger.error("No connection details for RF #() ()", rootFolderId, rootFolderDto.getRealPath());
            } else {
                logger.debug("Check RF #{} accessibility check. {}", rootFolderId,rootFolderDto.getRealPath());
                remoteFileContentManager.checkIsDirectoryExistsAsync(rootFolderDto, connectionDetailsDto.get(), force);
            }

            if (force) {
                docStoreService.removeRootFolderForceAccessibilityCheck(rootFolderId);
            }

            return accessCheckDto.getStatus();
        }
        catch (RuntimeException e) {
            logger.info("Failed in 'getDirectoryExistsStatus' rootFolderId = {}. Error Message:{}", rootFolderId, e.getMessage());
            return  DirectoryExistStatus.UNKNOWN;
        }
        catch (Exception e) {
            logger.error("Failed in 'getDirectoryExistsStatus' rootFolderId = {}", rootFolderId, e);
            return  DirectoryExistStatus.UNKNOWN;
        }
    }

    public void doFileIngestOperationAsync(long fileId, String requestId, boolean getReverseDictionary) {
        final ClaFile claFile = getClaFile(fileId);
        if (claFile == null) {
            throw new BadRequestException(messageHandler.getMessage("file.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        RootFolder rf = docStoreService.getRootFolder(claFile.getRootFolderId());
        remoteFileContentManager.doFileIngestOperationAsync(requestId, rf, claFile, getReverseDictionary);
    }

    //<editor-fold desc=" ---------- Private Methods --------------------------------------------------------">

    private String normalizeAclToken(String token){
        String matchStr = token.toLowerCase().trim();
        matchStr = matchStr.replaceAll("\\\\*", "|");
        return matchStr;
    }

    private boolean matchAclTokens(String userAcl, String fileAcl){
        return normalizeAclToken(userAcl).contains(normalizeAclToken(fileAcl));
    }

    private boolean aclMatchFound(Collection<String> fileAcls, Collection<String> userAcls){
        if (fileAcls == null || fileAcls.isEmpty()){
            return false;
        }

        for (String fileAcl : fileAcls) {
            for (String userAcl : userAcls) {
                if (matchAclTokens(userAcl, fileAcl)) {
                    return true;
                }
            }
        }
        return false;
    }
    //</editor-fold>
}
