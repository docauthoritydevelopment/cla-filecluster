package com.cla.filecluster.service.export.exporters.file.searchPattern;

/**
 * Created by: ophir
 * Created on: 4/18/2019
 */
public interface TextSearchPatternHeaderFields {
    String NAME = "NAME";
    String DESCRIPTION = "DESCRIPTION";
    String ACTIVE = "ACTIVE";
    String PATTERN = "PATTERN";
    String PATTERN_TYPE = "PATTERN_TYPE";
    String CLASS = "CLASS";
    String CATEGORY = "CATEGORY";
    String SUB_CATEGORY = "SUB_CATEGORY";
    String REGULATIONS = "REGULATIONS";
}