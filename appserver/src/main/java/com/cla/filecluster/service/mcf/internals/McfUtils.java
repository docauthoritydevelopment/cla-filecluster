package com.cla.filecluster.service.mcf.internals;

import com.cla.common.domain.dto.mcf.mcf_api.job.McfJobStatusDto;

/**
 * Created by uri on 18/07/2016.
 */
public class McfUtils {


    public static boolean isMcfJobRunning(McfJobStatusDto jobStatus) {
        String status = jobStatus.getStatus();
        if ("running".equalsIgnoreCase(status)) {
            return true;
        }
        if ("running no connector".equalsIgnoreCase(status)) {
            return true;
        }
        if ("starting up".equalsIgnoreCase(status)) {
            return true;
        }
        return false;
    }

    /**
     * "not yet run", "running", "paused", "done", "waiting", "stopping", "resuming", "starting up", "cleaning up", "error", "aborting", "restarting", "running no connector", and "terminating"
     * @param jobStatus
     * @return
     */
    public static boolean isMcfJobFinished(McfJobStatusDto jobStatus) {
        String status = jobStatus.getStatus();
        return status == null || status.isEmpty() || "done".equalsIgnoreCase(status);
    }
}
