package com.cla.filecluster.service.export.exporters.security.bizrole;

import com.cla.common.domain.dto.security.BizRoleDto;
import com.cla.common.domain.dto.security.BizRoleSummaryInfoDto;
import com.cla.common.domain.dto.security.RoleTemplateDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.security.funcrole.RoleTemplateDtoMixin;
import com.cla.filecluster.service.security.BizRoleService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.security.bizrole.BizRoleHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
@Slf4j
@Component
public class BizRoleExporter extends PageCsvExcelExporter<BizRoleSummaryInfoDto> {

    @Autowired
    private BizRoleService bizRoleService;

    private final static String[] header = {NAME, DESCRIPTION, LDAP_GROUP, ROLE_TEMPLATE};

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(BizRoleSummaryInfoDto.class, BizRoleSummaryInfoDtoMixin.class)
                .addMixIn(BizRoleDto.class, BizRoleDtoMixin.class)
                .addMixIn(RoleTemplateDto.class, RoleTemplateDtoMixin.class);
    }

    @Override
    protected Iterable<BizRoleSummaryInfoDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return bizRoleService.listBizRoles(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(BizRoleSummaryInfoDto base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();

        if (base.getLdapGroupMappingDtos() != null && !base.getLdapGroupMappingDtos().isEmpty()) {
            List<String> names = new ArrayList<>();
            base.getLdapGroupMappingDtos().forEach(ldap -> {
                names.add(ldap.getGroupName());
            });
            String stringConcated = String.join("; ", names);
            res.put(LDAP_GROUP, stringConcated);
        }

        return res;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.OPERATIONAL_ROLE);
    }
}
