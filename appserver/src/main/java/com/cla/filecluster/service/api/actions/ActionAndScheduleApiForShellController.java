package com.cla.filecluster.service.api.actions;

import com.cla.common.domain.dto.action.ActionScheduleDto;
import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.actions.ActionSchedule;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.actions.ActionScheduleShellService;
import com.cla.filecluster.service.actions.ActionsAppService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.filter.SavedFilterAppService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.util.ValidationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Properties;

/**
 * api to work with shell script to CRUD scheduled actions until we have ui for it
 *
 * Created by: yael
 * Created on: 3/6/2018
 *
 * To be removed with shell script once ui is written!
 */
@RestController
@RequestMapping("/api/shell/actionschedule")
public class ActionAndScheduleApiForShellController {

    private Logger logger = LoggerFactory.getLogger(ActionAndScheduleApiForShellController.class);

    @Autowired
    private ActionScheduleShellService actionScheduleShellService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private ActionsAppService actionsAppService;

    @Autowired
    private SavedFilterAppService savedFilterAppService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/schedule", method = {RequestMethod.POST, RequestMethod.GET}, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ActionScheduleDto policyActionOnFilterSchedule(@RequestParam(name = "action") String actionId, @RequestParam(name = "name") String scheduleName,
                                     @RequestBody Map<String, String> requestParams) {
        Properties actionParams = extractActionParameters(requestParams);
        String filter = requestParams.get("filter");
        String resultPath = requestParams.get("path");
        String cronTriggerString = requestParams.get("cron");

        ValidationUtils.validateStringNotEmpty(filter, messageHandler.getMessage("action.filter.empty"));
        ValidationUtils.validateStringNotEmpty(resultPath, messageHandler.getMessage("action.path.empty"));
        ValidationUtils.validateStringNotEmpty(cronTriggerString, messageHandler.getMessage("action-schedule.cron.empty"));


        long taskId = actionsAppService.queueActionOnFilterAndDispatchPolicy(
                getFilter(filter), actionId, actionParams, getCurrentUser());
        ActionSchedule schedule = new ActionSchedule();
        schedule.setName(scheduleName);
        schedule.setActive(true);
        schedule.setResultPath(resultPath);
        schedule.setCronTriggerString(cronTriggerString);
        schedule.setActionExecutionTaskStatusId(taskId);
        ActionScheduleDto actionScheduleDto = actionScheduleShellService.createActionSchedule(schedule);
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Create actionSchedule", AuditAction.CREATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionScheduleDto;
    }

    private FilterDescriptor getFilter(String filterName) {
        SavedFilterDto filterDto = savedFilterAppService.getSavedFilterByName(filterName);
        if (filterDto == null) {
            throw new BadRequestException(messageHandler.getMessage("saved-filter.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return filterDto.getFilterDescriptor();
    }

    @RequestMapping(value = "/active", method = {RequestMethod.POST, RequestMethod.GET})
    public ActionScheduleDto updateScheduleActionActive(@RequestParam String name, @RequestParam Boolean state) {
        ActionScheduleDto actionScheduleDto = actionScheduleShellService.updateScheduleActionActive(name, state);
        if (actionScheduleDto == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Update actionSchedule", AuditAction.UPDATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionScheduleDto;
    }

    @RequestMapping(value = "/cron", method = {RequestMethod.POST, RequestMethod.GET})
    public ActionScheduleDto updateScheduleActionCron(@RequestParam String name, @RequestBody String cronTriggerString) {
        ValidationUtils.validateStringNotEmpty(cronTriggerString, messageHandler.getMessage("action-schedule.cron.empty"));
        ActionScheduleDto actionScheduleDto = actionScheduleShellService.updateScheduleActionCron(name, cronTriggerString);
        if (actionScheduleDto == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Update actionSchedule", AuditAction.UPDATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionScheduleDto;
    }

    @RequestMapping(value = "/path", method = {RequestMethod.POST, RequestMethod.GET})
    public ActionScheduleDto updateScheduleActionPath(@RequestParam String name, @RequestBody String resultPath) {
        ValidationUtils.validateStringNotEmpty(resultPath, messageHandler.getMessage("action.path.empty"));
        ActionScheduleDto actionScheduleDto = actionScheduleShellService.updateScheduleActionPath(name, resultPath);
        if (actionScheduleDto == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Update actionSchedule", AuditAction.UPDATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionScheduleDto;
    }

    @RequestMapping(value = "/filter", method = {RequestMethod.POST, RequestMethod.GET})
    public ActionScheduleDto updateScheduleActionFilter(@RequestParam String name, @RequestBody String filter) {

        FilterDescriptor fd = getFilter(filter);

        ActionScheduleDto actionScheduleDto = actionScheduleShellService.updateScheduleActionFilter(name, fd);
        if (actionScheduleDto == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Update actionSchedule", AuditAction.UPDATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionScheduleDto;
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.POST, RequestMethod.GET})
    public void deleteActionSchedule(@RequestParam String name) {
        ActionScheduleDto actionSchedule = actionScheduleShellService.deleteActionSchedule(name);
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Delete actionSchedule", AuditAction.DELETE, ActionScheduleDto.class, actionSchedule.getId(), actionSchedule);
    }

    ///////////////////

    private Long getCurrentUser() {
        return userService == null || userService.getCurrentUser() == null ? 0L : userService.getCurrentUser().getId();
    }

    private Properties extractActionParameters(Map<String, String> requestParams) {
        Properties actionParams = new Properties();
        if (requestParams != null) {
            for (Map.Entry<String, String> entry : requestParams.entrySet()) {
                actionParams.setProperty(entry.getKey(), entry.getValue());
            }
        }
        return actionParams;
    }
}
