package com.cla.filecluster.service.export.exporters.scan_history.mixin;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.scan_history.ScanHistoryHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class CrawlRunDetailsDtoMixin {

    @JsonProperty(ID)
    private Long id;

    @JsonProperty(START_TIME)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long scanStartTime;

    @JsonProperty(END_TIME)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long scanEndTime;

    @JsonProperty(SCHEDULED_GROUP)
    private String scheduleGroupName;

    @JsonUnwrapped
    private RootFolderDto rootFolderDto;
}
