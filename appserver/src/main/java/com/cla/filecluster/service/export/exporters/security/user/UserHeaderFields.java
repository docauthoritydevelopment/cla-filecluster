package com.cla.filecluster.service.export.exporters.security.user;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public interface UserHeaderFields {
    String NAME = "Name";
    String LOCK = "Lock";
    String USERNAME = "User Name";
    String OPER_ROLES = "Operational Roles";
    String DATA_ROLES = "Data Roles";
    String STATE = "State";
    String TYPE = "Type";
    String EMAIL = "E-mail";
    String CNG_PWD = "Must change Password";
    String LDAP_USER = "Ldap User";
}
