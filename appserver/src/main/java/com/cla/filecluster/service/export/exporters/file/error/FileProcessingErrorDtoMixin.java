package com.cla.filecluster.service.export.exporters.file.error;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/7/2019
 */
public abstract class FileProcessingErrorDtoMixin {

    @JsonProperty(ErrorHeaderFields.NAME)
    private String fileName;

    @JsonProperty(ErrorHeaderFields.FILE_TYPE)
    private FileType fileType;

    @JsonProperty(ErrorHeaderFields.ROOT_FOLDER)
    private String rootFolder;

    @JsonProperty(ErrorHeaderFields.FOLDER)
    private String folder;

    @JsonProperty(ErrorHeaderFields.TEXT)
    private String text;

    @JsonProperty(ErrorHeaderFields.DETAILS)
    private String detailText;

    @JsonProperty(ErrorHeaderFields.RUN_ID)
    private Long runId;

    @JsonProperty(ErrorHeaderFields.TYPE)
    private ProcessingErrorType type;

    @JsonProperty(ErrorHeaderFields.DATE)
    public abstract String getDateString();

    @JsonProperty(ErrorHeaderFields.MEDIA_TYPE)
    private MediaType mediaType;

    @JsonProperty(ErrorHeaderFields.FILE_EXTENSION)
    private String extension;
}
