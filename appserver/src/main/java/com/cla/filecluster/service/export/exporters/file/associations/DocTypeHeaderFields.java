package com.cla.filecluster.service.export.exporters.file.associations;

public interface DocTypeHeaderFields {
    String NAME = "Name";
    String NUM_OF_DIRECT_FILERED = "Number of direct filtered files";
    String NUM_OF_ALL_FILTERED = "Number of all filtered files";
    String DESCRIPTION = "Description";
}
