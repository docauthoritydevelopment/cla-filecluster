package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Analyses available scan results and produces groups of ingestion tasks to be enqueued
 * for processing by the workers.
 *
 * Created by uri on 19-Mar-17.
 */
public class SimpleTasksStrategy extends TaskProducingStrategy {

    private final static Logger logger = LoggerFactory.getLogger(SimpleTasksStrategy.class);

    public SimpleTasksStrategy(JobManagerService jobManager, SolrTaskService solrTaskService, TaskProducingStrategyConfig config) {
        super(jobManager, solrTaskService, config);
    }

    @Override
    public Collection<SimpleTask> nextTaskGroup(TaskType taskType, Table<Long, TaskState, Number> taskStatesByType, Integer tasksQueueLimit) {
        Map<Long, Number> newTasksCount = taskStatesByType.column(TaskState.NEW);
        List<SimpleTask> tasks = Lists.newLinkedList();
        if (newTasksCount.size() == 0) {
            //Nothing to do.
            return tasks;
        }

        if (tasksQueueLimit == null) {
            tasksQueueLimit = getQueueLimit(taskType);
        }

        Map<Long, Number> enqueuedTasksCount = taskStatesByType.column(TaskState.ENQUEUED);
        int totalInProgress =  enqueuedTasksCount.values().stream().mapToInt(Number::intValue).sum();

        int queueLimit = tasksQueueLimit - totalInProgress;
        if (queueLimit <= 0) {
            //Queue is currently full
            logger.debug("The queue for tasks of type {} is currently full ({} in progress)", taskType.name(), totalInProgress);
            return tasks;
        }
        for (Long contextId : newTasksCount.keySet()){
            if (taskType.equals(TaskType.INGEST_FILE)) {
                List<SolrFileEntity> files = solrTaskService.getIngestTasks(contextId, TaskState.NEW, queueLimit);
                files.forEach(file -> {
                    SimpleTask task = SolrTaskService.createForIngest(file, jobManager.getJobIdCached(contextId, JobType.INGEST));
                    tasks.add(task);
                });
                queueLimit -= files.size();
            } else {
                List<SolrContentMetadataEntity> contents = solrTaskService.getAnalysisTasks(contextId, TaskState.NEW, queueLimit);
                contents.forEach(content -> {
                    SimpleTask task = SolrTaskService.createForAnalysis(content, jobManager.getJobIdCached(contextId, JobType.ANALYZE));
                    tasks.add(task);
                });
                queueLimit -= contents.size();
            }
            if (queueLimit <= 0) {
                break;
            }
        }
        logger.debug("Fetched {} tasks of type {}.", tasks.size(), taskType.name());
        return tasks;
    }


}
