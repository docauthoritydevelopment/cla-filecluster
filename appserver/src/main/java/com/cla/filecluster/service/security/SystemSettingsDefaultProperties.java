package com.cla.filecluster.service.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "system-settings")
@PropertySource("file:./config/db-application.properties")
public class SystemSettingsDefaultProperties {

    private Map<String, String> def = new LinkedHashMap<>();

    public Map<String, String> getDefault() {
        return def;
    }
}
