package com.cla.filecluster.service.export.transformers;

import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.function.Function;

public interface DataBufferTransformer<V> {
    Function<Flux<V>, Publisher<DataBuffer>> asDataBuffer();
}
