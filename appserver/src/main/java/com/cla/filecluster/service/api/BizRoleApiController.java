package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.security.BizRoleSummaryInfoDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.BizRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Itai Marko.
 */
@RestController
@RequestMapping("/api/bizrole")
public class BizRoleApiController {

    private static final Logger logger = LoggerFactory.getLogger(BizRoleApiController.class);

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private EventAuditService eventAuditService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<BizRoleSummaryInfoDto> listBizRoleSummaryInfoDto(@RequestParam Map<String,String> params) {
        logger.debug("Getting BizRoleSummaryInfoDtos");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return bizRoleService.listBizRoles(dataSourceRequest);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public BizRoleSummaryInfoDto createBizRole(@RequestBody BizRoleSummaryInfoDto bizRoleSummaryInfoDto) {
        logger.debug("Creating BizRole: {}", bizRoleSummaryInfoDto);
        BizRoleSummaryInfoDto bizRole = bizRoleService.createBizRole(
                bizRoleSummaryInfoDto.getBizRoleDto(),
                bizRoleSummaryInfoDto.getLdapGroupMappingDtos());
        eventAuditService.audit(
                AuditType.OPERATIONAL_ROLE, "Create operational role", AuditAction.CREATE, BizRoleSummaryInfoDto.class,
                bizRole.getBizRoleDto().getId(), bizRoleSummaryInfoDto);
        return bizRole;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public BizRoleSummaryInfoDto updateBizRole(@PathVariable Long id,
                                               @RequestBody BizRoleSummaryInfoDto bizRoleSummaryInfoDto) {
        logger.debug("Updating BizRole: {}", bizRoleSummaryInfoDto);
        BizRoleSummaryInfoDto bizRole = bizRoleService.updateBizRole(
                bizRoleSummaryInfoDto.getBizRoleDto(),
                bizRoleSummaryInfoDto.getLdapGroupMappingDtos());
        eventAuditService.audit(
                AuditType.OPERATIONAL_ROLE, "Update operational role", AuditAction.UPDATE, BizRoleSummaryInfoDto.class,
                bizRole.getBizRoleDto().getId(), bizRoleSummaryInfoDto);
        return bizRole;
    }

}
