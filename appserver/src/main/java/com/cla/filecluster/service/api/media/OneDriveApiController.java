package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MicrosoftConnectionDetailsDtoBase;
import com.cla.connector.domain.dto.media.OneDriveConnectionDetailsDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.filecluster.service.media.MicrosoftMediaAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by uri on 07/08/2016.
 */
@RestController
@RequestMapping("/api/media/onedrive")
public class OneDriveApiController extends MicrosoftApiControllerBase {

    private final Function<Long, OneDriveConnectionDetailsDto> GET_CONNECTION_DETAILS_FUNC = (id) -> connectionDetailsService.getOneDriveConnectionDetailsById(id, false);

    @Autowired
    private MicrosoftMediaAppService microsoftMediaAppService;

    @RequestMapping(value = {"/connections/test/dataCenter/{customerDataCenterId}", "/connections/{connectionId}/test/dataCenter/{customerDataCenterId}"},
            method = RequestMethod.POST)
    public TestResultDto testOneDriveConnection(@RequestBody(required = false) SharePointConnectionParametersDto oneDriveConnectionParametersDto, @PathVariable(required = false) Optional<Long> connectionId, @PathVariable Long customerDataCenterId) {
        return testConnection(oneDriveConnectionParametersDto, customerDataCenterId, connectionId, new OneDriveConnectionDetailsDto(), GET_CONNECTION_DETAILS_FUNC);
    }

    @RequestMapping(value = "/connections/{id}/test/{customerDataCenterId}", method = RequestMethod.POST)
    public TestResultDto testOneDriveConnectionById(@PathVariable long id, @PathVariable Long customerDataCenterId) {
        OneDriveConnectionDetailsDto dto = connectionDetailsService.getOneDriveConnectionDetailsById(id, true);
        return microsoftMediaAppService.testConnection(dto, customerDataCenterId);
    }

    @RequestMapping(value = "/server/folders", method = RequestMethod.GET)
    public List<ServerResourceDto> listServerFolders(@RequestParam long connectionId, @RequestParam(required = false) String id, @RequestParam long customerDataCenterId) {
        OneDriveConnectionDetailsDto dto = connectionDetailsService.getOneDriveConnectionDetailsById(connectionId, true);
        return microsoftMediaAppService.listFoldersUnderPath(dto, Optional.ofNullable(id), customerDataCenterId, false);
    }

    @RequestMapping(value = "/connections/new", method = RequestMethod.POST)
    public MicrosoftConnectionDetailsDtoBase configureOneDriveConnectionDetails(@RequestBody OneDriveConnectionDetailsDto oneDriveConnectionDetailsDto) {
        return connectionDetailsService.configureMicrosoftConnectionDetails(oneDriveConnectionDetailsDto);
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.POST)
    public MicrosoftConnectionDetailsDtoBase updateOneDriveConnectionDetails(@PathVariable long id,
                                                                             @RequestBody OneDriveConnectionDetailsDto oneDriveConnectionDetailsDto) {
        return connectionDetailsService.updateOneDriveConnectionDetailsById(id, oneDriveConnectionDetailsDto);
    }

    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public List<MicrosoftConnectionDetailsDtoBase> getOneDriveConnectionDetails() {
        return connectionDetailsService.getOneDriveConnectionDetails(true);
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.GET)
    public MicrosoftConnectionDetailsDtoBase getOneDriveConnectionDetails(@PathVariable long id) {
        OneDriveConnectionDetailsDto dto = connectionDetailsService.getOneDriveConnectionDetailsById(id, true);
        dto.getSharePointConnectionParametersDto().setPassword(null);
        return dto;
    }
}
