package com.cla.filecluster.service.report;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.repository.solr.SolrFacetJsonResponseBucket;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import java.util.List;
import java.util.Map;

public interface DashboardChartQueryBuilder<T,U extends DashboardDataEnricher> {
    ChartQueryType getQueryBuilderType();

    T setInitialDataToDashboardChartDto(DashboardChartDataDto dashboardChartDataDto, CatFileFieldType groupedField,
                                             DataSourceRequest dataSourceRequest, FilterDescriptor filter,
                                             String queryPrefix, DashboardChartValueTypeDto valueField,
                                             Map<String, String> params, String additionalFilter,
                                             U dashboardChartEnricher);

    SolrFacetQueryJson buildQueryJsonFacetByFilter(T initialGetDataResult, SolrSpecification solrSpecification, FilterDescriptor filter,
                                                          SolrCoreSchema field, String queryPrefix, DashboardChartValueTypeDto valueField, Object[] neededValues,
                                                          String queryName, Map<String, String> params, boolean getAccurateNumOfBuckets, String additionalFilter);

    void parseResponseToDashboardChartDataDto(T initialGetDataResult, DashboardChartDataDto dashboardChartDataDto,
                                                    List<SolrFacetJsonResponseBucket> buckets,
                                                     String seriesName, DashboardChartValueTypeDto valueField,
                                                     Map<String, String> params, boolean getAccurateNumOfBuckets);

    void enrichChartData(T initialGetDataResult, DashboardChartDataDto dashboardChartDataDto,Map<String, String> params, U dashboardChartEnricher);

    String getOtherQuery(T initialGetDataResult,List<String> categories, Map<String, String> params);

    String getAdditionalFilterByCategories(T initialGetDataResult, List<String> categories, CatFileFieldType groupedField, U dashboardChartEnricher, Map<String, String> params);

    public FilterDescriptor[] getSeriesQueryResults(CatFileFieldType seriesField,
                                              String seriesFieldPrefix,
                                              DashboardChartValueTypeDto valueField,
                                              DataSourceRequest dataSourceRequest,
                                              Map<String, String> params,
                                              String additionalFilter,
                                              U dashboardChartEnricher);
}