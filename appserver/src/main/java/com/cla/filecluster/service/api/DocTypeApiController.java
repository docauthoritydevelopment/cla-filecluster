package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.doctype.DocTypeDetailsDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagTypeSettingsDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by uri on 19/08/2015.
 */
@RestController
@RequestMapping("/api/doctype")
public class DocTypeApiController {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DocTypeApiController.class);

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private DocTypeAppService docTypeAppService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private DocFolderService folderService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${docType.defaultPageSize:1000}")
    private int docTypeDefaultPageSize;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<DocTypeDto> getAllDocTypes(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(docTypeDefaultPageSize);
        return docTypeAppService.getAllDocTypesAsDto(dataSourceRequest);
    }

    @RequestMapping(value = "/{docTypeId}", method = RequestMethod.GET)
    public DocTypeDetailsDto getDocTypeDetails(@PathVariable long docTypeId,
                                               @RequestParam(required = false, defaultValue = "false") boolean recursive) {
        return docTypeAppService.getDocTypeDetails(docTypeId, recursive);
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    public DocTypeDto getDocType(@PathVariable long id) {
        DocTypeDto docType = docTypeAppService.getDocTypeById(id);
        if (docType == null) {
            throw new BadParameterException(messageHandler.getMessage("doctype.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return docType;
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public DocTypeDetailsDto getDocTypeDetailsByName(@RequestParam String name, @RequestParam(required = false, defaultValue = "false") boolean recursive) {
        DocTypeDto docTypeByName = docTypeAppService.getDocTypeByName(name);
        return docTypeAppService.getDocTypeDetails(docTypeByName.getId(), recursive);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public DocTypeDto addDocType(@RequestBody DocTypeDto docTypeDto) {

        if (docTypeDto.getName().startsWith("file://") || docTypeDto.getName().startsWith("file //")) {    // currently UI replaces : with space
            String fileName = docTypeDto.getName().substring("file://".length());
            docTypeService.createDocTypeTemplateFromFile(fileName);
            eventAuditService.audit(AuditType.DOC_TYPE, "Create DocTypes from file", AuditAction.IMPORT, DocTypeDto.class, fileName);
            return null;
        } else {
            DocTypeDto docType = docTypeAppService.createDocType(docTypeDto);
            eventAuditService.audit(AuditType.DOC_TYPE, "Create DocType", AuditAction.CREATE, DocTypeDto.class, docType.getId(), docType);
            return docType;
        }
    }

    @RequestMapping(value = "/load", method = {RequestMethod.GET, RequestMethod.PUT})
    public String loadDocTypes(@RequestParam(value = "path", required = true) final String path) {
        logger.info("Loading docType template file: {}", path);
        if (path != null && !path.isEmpty()) {
            docTypeService.createDocTypeTemplateFromFile(path);
        }
        return "Loaded: " + path;
    }


    @RequestMapping(value = "/associateTags", method = RequestMethod.PUT)
    public List<DocTypeDto> associateTags(@RequestBody Map<Long, List<FileTagTypeSettingsDto>> fileTagTypeSettingsByDocTypeIds) {
        try {
            Table<Long, Long, List<Long>> docTypeTagSettingsTable = prepareDocTypeTagSettingsTable(fileTagTypeSettingsByDocTypeIds);
            List<DocTypeDto> docTypeDtos = docTypeAppService.assignDocTypeTagsByTagType(docTypeTagSettingsTable);
            fileTagTypeSettingsByDocTypeIds.forEach((docTypeId, ftsList) ->
                    ftsList.forEach(fts ->
                            eventAuditService.auditDual(AuditType.DOC_TYPE, "Replace tag type settings for DocType", AuditAction.ATTACH, FileTagSetting.class, fts.getFileTagType().getId(), DocTypeDto.class, docTypeId)
                    ));
            return docTypeDtos;
        } catch (Throwable t) {
            logger.error("Failed to replace tag settings", t);
            throw new BadRequestException(messageHandler.getMessage("doctype.associate-tag-error", Lists.newArrayList(t)), BadRequestType.OTHER);
        }
    }

    private Table<Long, Long, List<Long>> prepareDocTypeTagSettingsTable(Map<Long, List<FileTagTypeSettingsDto>> fileTagTypeSettingsByDocTypeIds) {
        Table<Long, Long, List<Long>> result = HashBasedTable.create();
        fileTagTypeSettingsByDocTypeIds.forEach((docTypeId, fileTagTypeSettingsDtoList) -> {
            fileTagTypeSettingsDtoList.forEach(fts -> {
                List<Long> tagIds = fts.getFileTags().stream()
                        .map(ft -> ft.getId())
                        .collect(Collectors.toList());
                result.put(docTypeId, fts.getFileTagType().getId(), tagIds);
            });
        });
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public DocTypeDto updateDocType(@PathVariable long id, @RequestBody DocTypeDto docTypeDto) {
        DocTypeDto docTypeDto1 = docTypeAppService.updateDocType(id, docTypeDto);
        eventAuditService.audit(AuditType.DOC_TYPE, "Update DocType", AuditAction.UPDATE, DocTypeDto.class, docTypeDto1.getId(), docTypeDto1);
        return docTypeDto1;
    }

    @RequestMapping(value = "/{id}/move", method = RequestMethod.POST)
    public DocTypeDto moveDocType(@PathVariable long id, @RequestParam(required = false) Long newParentId) {
        DocTypeDto docTypeDto = docTypeAppService.moveDocType(id, newParentId);
        eventAuditService.audit(AuditType.DOC_TYPE, "Move DocType under new parent", AuditAction.UPDATE, DocTypeDto.class, docTypeDto.getId(), docTypeDto);
        return docTypeDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteDocType(@PathVariable long id
            , @RequestParam(required = false, defaultValue = "false") boolean recursive) {
        DocTypeDto docType = getDocType(id);
        docTypeAppService.deleteDocType(id, recursive);
        eventAuditService.audit(AuditType.DOC_TYPE, "Delete DocType ", AuditAction.DELETE, DocTypeDto.class, docType.getId(), Pair.of("recursive", recursive), docType);
    }

    @RequestMapping(value = "/{id}/mark", method = RequestMethod.DELETE)
    public void markDeletedDocType(@PathVariable long id
            , @RequestParam(required = false, defaultValue = "false") boolean recursive) {
        DocTypeDto docType = getDocType(id);
        logger.info("Mark docType deleted {}. Remove docType", id);
        docTypeAppService.markDeletedDocType(id, recursive);
        eventAuditService.audit(AuditType.DOC_TYPE, "Mark deleted DocType ", AuditAction.DELETE, DocTypeDto.class, docType.getId(), Pair.of("recursive", recursive), docType);
    }

    @RequestMapping(value = "/{id}/force", method = RequestMethod.DELETE)
    public void deleteDocTypeForce(@PathVariable long id) {
        DocTypeDto docType = getDocType(id);
        logger.info("Delete docType {} - force. Remove associations", id);
        docTypeAppService.removeAllDocTypeAssociations(id, true, false);
        logger.info("Delete docType {} - force. Remove docType", id);
        docTypeAppService.markDeletedDocType(id, true);
        eventAuditService.audit(AuditType.DOC_TYPE, "Delete DocType and associations ", AuditAction.DELETE, DocTypeDto.class, docType.getId(), docType);
    }

    @RequestMapping(value = "/{docTypeId}/file/{fileId}", method = RequestMethod.PUT)
    public ClaFileDto assignDocTypeToFile(@PathVariable long docTypeId, @PathVariable long fileId) {
        DocTypeDto docType = getDocType(docTypeId);
        BasicScope basicScope = docType.getScope() == null ? null : scopeService.getOrCreateScope(docType.getScope());
        ClaFileDto claFileDto = docTypeAppService.assignDocTypeToFile(docTypeId, fileId, basicScope);
        if (claFileDto != null) {
            eventAuditService.auditDual(AuditType.DOC_TYPE, "Assign DocType to file ", AuditAction.ASSIGN, DocTypeDto.class, docTypeId, ClaFileDto.class, fileId, claFileDto);
        } else {
            claFileDto = FileCatUtils.convertClaFile(fileCatService.getFileObject(fileId));
        }
        return claFileDto;
    }

    @RequestMapping(value = "/{docTypeId}/files", method = RequestMethod.PUT)
    public void assignDocTypeToFiles(@PathVariable long docTypeId, @RequestParam String fileIds) {
        String[] fileIdArray = StringUtils.split(fileIds, ',');
        for (String fileId : fileIdArray) {
            assignDocTypeToFile(docTypeId, Long.getLong(fileId));
        }
    }

    @RequestMapping(value = {"/{docTypeId}/group/{groupId}", "/{docTypeId}/bid/{groupId}"}, method = RequestMethod.PUT)
    public GroupDto assignDocTypeToGroup(@PathVariable String groupId, @PathVariable long docTypeId) {
        DocTypeDto docType = getDocType(docTypeId);
        BasicScope basicScope = docType.getScope() == null ? null : scopeService.getOrCreateScope(docType.getScope());
        GroupDto groupDto = docTypeAppService.assignDocTypeToGroup(groupId, docTypeId, basicScope);
        if (groupDto != null) {
            eventAuditService.auditDual(AuditType.DOC_TYPE, "Assign DocType to group ", AuditAction.ASSIGN, DocTypeDto.class, docTypeId, GroupDto.class, groupId, groupDto);
        } else {
            groupDto = groupsService.findDtoById(groupId);
        }
        return groupDto;
    }

    @RequestMapping(value = "/{docTypeId}/groups", method = RequestMethod.PUT)
    public void assignDocTypeToGroups(@PathVariable long docTypeId, @RequestParam String groupIds) {
        String[] groupIdArray = StringUtils.split(groupIds, ',');
        for (String groupId : groupIdArray) {
            assignDocTypeToGroup(groupId, docTypeId);
        }
    }

    @RequestMapping(value = "/{docTypeId}/folder/{folderId}", method = RequestMethod.PUT)
    public DocFolderDto assignDocTypeToFolder(@PathVariable Long folderId, @PathVariable long docTypeId) {
        DocTypeDto docType = getDocType(docTypeId);
        BasicScope basicScope = docType.getScope() == null ? null : scopeService.getOrCreateScope(docType.getScope());
        DocFolderDto docFolderDto = docTypeAppService.assignDocTypeToFolder(folderId, docTypeId, basicScope);
        if (docFolderDto != null) {
            eventAuditService.audit(AuditType.DOC_TYPE, "Assign DocType to folder ", AuditAction.ASSIGN, DocTypeDto.class, docTypeId, Pair.of("folderId", folderId));
        } else {
            docFolderDto = folderService.getDtoById(folderId);
        }
        return docFolderDto;
    }

    @RequestMapping(value = "/{docTypeId}/folders", method = RequestMethod.PUT)
    public void assignDocTypeToFolders(@RequestParam String folderIds, @PathVariable long docTypeId) {
        String[] folderIdArray = StringUtils.split(folderIds, ',');
        for (String folderId : folderIdArray) {
            assignDocTypeToFolder(Long.valueOf(folderId), docTypeId);
        }
    }

    @RequestMapping(value = "/{docTypeId}/file/{fileId}", method = RequestMethod.DELETE)
    public ClaFileDto removeDocTypeFromFile(@PathVariable long docTypeId, @PathVariable long fileId) {
        DocTypeDto docType = getDocType(docTypeId);
        BasicScope basicScope = docType.getScope() == null ? null : scopeService.getOrCreateScope(docType.getScope());
        ClaFileDto claFileDto = docTypeAppService.removeDocTypeFromFile(docTypeId, fileId, basicScope == null ? null : basicScope.getId());
        if (claFileDto != null) {
            eventAuditService.auditDual(AuditType.DOC_TYPE, "Unassign DocType to file ", AuditAction.UNASSIGN, DocTypeDto.class, docTypeId, ClaFileDto.class, fileId, claFileDto);
        } else {
            claFileDto = FileCatUtils.convertClaFile(fileCatService.getFileObject(fileId));
        }
        return claFileDto;
    }

    @RequestMapping(value = "/{docTypeId}/files", method = RequestMethod.DELETE)
    public void removeDocTypeFromFiles(@PathVariable long docTypeId, @RequestParam String fileIds) {
        String[] fileIdArray = StringUtils.split(fileIds, ',');
        for (String fileId : fileIdArray) {
            removeDocTypeFromFile(docTypeId, Long.valueOf(fileId));
        }
    }

    @RequestMapping(value = {"/{docTypeId}/group/{groupId}", "/{docTypeId}/bid/{groupId}"}, method = RequestMethod.DELETE)
    public GroupDto removeDocTypeFromGroup(@PathVariable String groupId, @PathVariable long docTypeId) {
        DocTypeDto docType = getDocType(docTypeId);
        BasicScope basicScope = docType.getScope() == null ? null : scopeService.getOrCreateScope(docType.getScope());
        GroupDto groupDto = docTypeAppService.removeDocTypeFromGroup(groupId, docTypeId, basicScope);
        if (groupDto != null) {
            eventAuditService.auditDual(AuditType.DOC_TYPE, "Unassign DocType to group ", AuditAction.UNASSIGN, DocTypeDto.class, docTypeId, GroupDto.class, groupId, groupDto);
        } else {
            groupDto = groupsService.findDtoById(groupId);
        }
        return groupDto;

    }

    @RequestMapping(value = "/{docTypeId}/groups", method = RequestMethod.DELETE)
    public void removeDocTypeFromGroups(@PathVariable long docTypeId, @RequestParam String groupIds) {
        String[] groupIdArray = StringUtils.split(groupIds, ',');
        for (String groupId : groupIdArray) {
            removeDocTypeFromGroup(groupId, docTypeId);
        }
    }

    @RequestMapping(value = "/{docTypeId}/folder/{folderId}", method = RequestMethod.DELETE)
    public DocFolderDto removeDocTypeFromFolder(@PathVariable Long folderId, @PathVariable long docTypeId) {
        DocTypeDto docType = getDocType(docTypeId);
        BasicScope basicScope = docType.getScope() == null ? null : scopeService.getOrCreateScope(docType.getScope());
        DocFolderDto docFolderDto = docTypeAppService.removeDocTypeFromFolder(folderId, docTypeId, basicScope);
        if (docFolderDto != null) {
            docFolderDto = docTypeAppService.clearCacheReturnFolder(folderId);
            eventAuditService.auditDual(AuditType.DOC_TYPE, "Unassign DocType from folder ", AuditAction.UNASSIGN, DocTypeDto.class, docTypeId, DocFolderDto.class, docFolderDto.getId(), docFolderDto);
        } else {
            docFolderDto = folderService.getDtoById(folderId);
        }
        return docFolderDto;
    }

    @RequestMapping(value = "/{docTypeId}/folders", method = RequestMethod.DELETE)
    public void removeDocTypeFromFolders(@RequestParam String folderIds, @PathVariable long docTypeId) {
        String[] folderIdArray = StringUtils.split(folderIds, ',');
        for (String folderId : folderIdArray) {
            removeDocTypeFromFolder(Long.valueOf(folderId), docTypeId);
        }
    }

    @RequestMapping(value = "/{docTypeId}/associations/all", method = RequestMethod.DELETE)
    public void removeAllDocTypeAssociations(@PathVariable long docTypeId, @RequestParam(required = false, defaultValue = "false") boolean recursive) {
        docTypeAppService.removeAllDocTypeAssociations(docTypeId, recursive, false);
        eventAuditService.audit(AuditType.DOC_TYPE, "Unassign DocType from all ", AuditAction.UNASSIGN, DocTypeDto.class, docTypeId, Pair.of("recursive", recursive));
    }

    @RequestMapping(value = "/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<DocTypeDto>> findDocTypesFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(docTypeDefaultPageSize);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover DocTypes export", AuditAction.VIEW, DocTypeDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover DocTypes", AuditAction.VIEW, DocTypeDto.class, null, dataSourceRequest);
        }
        FacetPage<AggregationCountItemDTO<DocTypeDto>> res = docTypeAppService.findDocTypesFileCount(dataSourceRequest, true, null);
        res.setPageNumber(1);
        return res;
    }

    @RequestMapping(value = "/assigned/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<DocTypeDto>> findAssignedDocTypesFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY, "Discover DocTypes", AuditAction.VIEW, DocTypeDto.class, null, dataSourceRequest);
        return docTypeAppService.findDocTypesFileCount(dataSourceRequest, false, null);
    }
}
