package com.cla.filecluster.service.export.exporters.security.funcrole;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.security.funcrole.FunctionalRoleHeaderFields.DESCRIPTION;
import static com.cla.filecluster.service.export.exporters.security.funcrole.FunctionalRoleHeaderFields.ROLE_TEMPLATE;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class RoleTemplateDtoMixin {

    @JsonProperty(ROLE_TEMPLATE)
    private String displayName;
}
