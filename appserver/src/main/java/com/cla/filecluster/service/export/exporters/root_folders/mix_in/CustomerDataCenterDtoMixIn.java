package com.cla.filecluster.service.export.exporters.root_folders.mix_in;

import com.fasterxml.jackson.annotation.JsonValue;

public class CustomerDataCenterDtoMixIn {

    @JsonValue
    String name;
}
