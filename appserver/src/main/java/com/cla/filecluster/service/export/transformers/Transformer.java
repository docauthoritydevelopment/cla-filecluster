package com.cla.filecluster.service.export.transformers;

import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;

import java.util.function.Function;

public interface Transformer<V, T> extends Function<Flux<V>, Publisher<T>>, DataBufferTransformer<V>  {


}
