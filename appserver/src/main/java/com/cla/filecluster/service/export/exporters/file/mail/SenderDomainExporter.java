package com.cla.filecluster.service.export.exporters.file.mail;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.email.MailAddressDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.files.FileMailAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.mail.MailHeaderFields.*;

/**
 * Created by: yael
 * Created on: 2/5/2019
 */
@Slf4j
@Component
public class SenderDomainExporter extends PageCsvExcelExporter<AggregationCountItemDTO<MailAddressDto>> {

    final static String[] HEADER = {DOMAIN, NUM_OF_FILES};

    @Autowired
    private FileMailAppService fileMailAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, MailAggCountDtoMixin.class);
        mapper.addMixIn(MailAddressDto.class, MailAddressDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<MailAddressDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<MailAddressDto>> res = fileMailAppService.getSenderDomainFileCounts(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<MailAddressDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.SENDER_DOMAIN);
    }
}
