package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.stream.Collectors;


@Component

public class FunctionRolesEnricher implements DefaultTypeDataEnricher {
    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String convertSolrValueToId(String value) {
        return String.valueOf(AssociationIdUtils.extractFunctionalRoleIdFromSolrIdentifier(value));
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return functionalRoleService.getFromCache(Long.parseLong(convertSolrValueToId(category))).getName();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.TAGS_2, FunctionalRoleDto.ID_PREFIX);
    }

}
