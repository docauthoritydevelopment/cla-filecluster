package com.cla.filecluster.service.crawler.analyze;

import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@Component
public class NewFileGroupsCache {

    private Map<String, SolrFileGroupEntity> groupIdToFileGroup = Collections.synchronizedMap(Maps.newHashMap());

    public void add(SolrFileGroupEntity fileGroup) {
        groupIdToFileGroup.put(fileGroup.getId(), fileGroup);
    }

    public SolrFileGroupEntity get(String groupId) {
        return groupIdToFileGroup.get(groupId);
    }

    public void clear() {
        groupIdToFileGroup.clear();
    }

    public void removeAll(Collection<SolrFileGroupEntity> fileGroups) {
        fileGroups.stream().forEach(this::remove);

    }

    public SolrFileGroupEntity remove(SolrFileGroupEntity fileGroup) {
        return groupIdToFileGroup.remove(fileGroup);
    }

    public boolean contains(String groupId) {
        return groupIdToFileGroup.containsKey(groupId);
    }
}
