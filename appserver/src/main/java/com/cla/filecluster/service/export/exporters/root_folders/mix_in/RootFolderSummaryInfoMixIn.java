package com.cla.filecluster.service.export.exporters.root_folders.mix_in;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import static com.cla.filecluster.service.export.exporters.root_folders.RootFoldersHeaderFields.*;

public abstract class RootFolderSummaryInfoMixIn {

    @JsonUnwrapped
    private CrawlRunDetailsDto crawlRunDetailsDto;

    @JsonUnwrapped
    private RootFolderDto rootFolderDto;

    @JsonProperty(STATUS)
    private String runStatus;

    @JsonUnwrapped
    private DepartmentDto departmentDto;

    @JsonUnwrapped
    public abstract ScheduleGroupDto getScheduleGroupDto();

}
