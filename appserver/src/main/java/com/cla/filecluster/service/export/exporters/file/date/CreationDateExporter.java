package com.cla.filecluster.service.export.exporters.file.date;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.service.export.ExportType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class CreationDateExporter extends DateExporter {

    @Override
    protected CatFileFieldType getField() {
        return CatFileFieldType.CREATION_DATE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.CREATION_DATE);
    }
}
