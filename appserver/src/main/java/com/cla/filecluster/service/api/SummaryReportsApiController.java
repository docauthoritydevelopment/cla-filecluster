package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.report.CounterReportDto;
import com.cla.common.domain.dto.report.MultiValueListDto;
import com.cla.common.domain.dto.report.SummaryReportDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.util.ControllerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * Created by uri on 07/10/2015.
 */
@RestController()
@RequestMapping("/api/reports")
public class SummaryReportsApiController {

    private final static Logger logger = LoggerFactory.getLogger(SummaryReportsApiController.class);

    @Autowired
    private SummaryReportsApplicationService summaryReportsApplicationService;

    @Autowired
    private ControllerUtils controllerUtils;

    @Value("${histogram.defaultPageSize:100}")
    private int histogramDefaultPageSize;

    @RequestMapping(value="/scan/statistics",method={RequestMethod.GET})
    public SummaryReportDto getScanStatistics() throws Exception {
        Callable<SummaryReportDto> task = () ->
                summaryReportsApplicationService.fetchScanStatistics();
        return controllerUtils.runInOtherThreadAsAdmin(task);
    }

    @RequestMapping(value="/admin/documents/statistics",method={RequestMethod.GET})
    public SummaryReportDto getDocumentStatistics() throws Exception {
        return summaryReportsApplicationService.fetchDocumentStatistics();
    }

    @RequestMapping(value="/user/documents/statistics",method={RequestMethod.GET})
    public SummaryReportDto getUserDocumentStatistics() throws Exception {
        return summaryReportsApplicationService.fetchUserDocumentStatistics();
    }

    @RequestMapping(value="/scan/configuration",method={RequestMethod.GET})
    public SummaryReportDto getScanConfiguration() {
        return summaryReportsApplicationService.fetchScanConfiguration();
    }

    @RequestMapping(value="/scan/criteria",method={RequestMethod.GET})
    public SummaryReportDto getScanCriteria() {
        return summaryReportsApplicationService.fetchScanCriteria();
    }

    @RequestMapping(value="/group/size/histogram",method={RequestMethod.GET})
    public MultiValueListDto getGroupSizeHistogram(@RequestParam(required = false) final Boolean files) {
        return summaryReportsApplicationService.createGroupSizeHistogram(files!=null && files);
    }

    @RequestMapping(value="/file/extensions/histogram",method={RequestMethod.GET})
    public MultiValueListDto getFileExtensionsHistogram(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(histogramDefaultPageSize);
        return summaryReportsApplicationService.createFileExtensionsHistogram(dataSourceRequest);
    }

    @RequestMapping(value="/file/types/histogram",method={RequestMethod.GET})
    public MultiValueListDto getFileTypesHistogram(@RequestParam final Map params) throws Exception {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(histogramDefaultPageSize);
        Callable<MultiValueListDto> task = () ->
            summaryReportsApplicationService.createFileTypesHistogram(dataSourceRequest);
        return controllerUtils.runInOtherThreadAsAdmin(task);
    }

    @RequestMapping(value="/file/types/histogram/role",method={RequestMethod.GET})
    public MultiValueListDto getFileTypesHistogramRole(@RequestParam final Map params) throws Exception {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(histogramDefaultPageSize);
        return summaryReportsApplicationService.createFileTypesHistogram(dataSourceRequest);
    }

    @RequestMapping(value="/file/access/histogram",method={RequestMethod.GET})
    public MultiValueListDto getFileAccessDateHistogram(@RequestParam final Map params) throws Exception {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(histogramDefaultPageSize);
        Callable<MultiValueListDto> task = () ->
             summaryReportsApplicationService.createFileAccessDateHistogram(dataSourceRequest);
        return controllerUtils.runInOtherThreadAsAdmin(task);
    }

    @RequestMapping(value="/file/access/histogram/role",method={RequestMethod.GET})
    public MultiValueListDto getFileAccessDateHistogramRole(@RequestParam final Map params) throws Exception {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(histogramDefaultPageSize);
        return summaryReportsApplicationService.createFileAccessDateHistogram(dataSourceRequest);
    }

    public Pair<Double, Long> getAggregatedFileSize() {
        return summaryReportsApplicationService.createAggregatedFileSize();
    }

    public List<CounterReportDto> getExcelStats() {
        return summaryReportsApplicationService.createExcelStats();
    }

}
