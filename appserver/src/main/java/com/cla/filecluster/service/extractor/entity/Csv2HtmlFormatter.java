package com.cla.filecluster.service.extractor.entity;

import java.io.File;
import java.io.FileWriter;

import javax.annotation.PostConstruct;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Csv2HtmlFormatter {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Csv2HtmlFormatter.class);
	
	@Value("${reportFile:./logs/reportFile.csv}")
	private String reportFileName;

	@Value("${reportTemplateFile:./config/reportTemplated.vm}")
	private String reportTemplateFileName;

	@Value("${htmlReportFile:./logs/reportFile.html}")
	private String htmlReportFileName;
	
	@Value("${entitiesFile}")
	private String fileName;
	
	@Value("${velocity.runtime.log:./logs/velocity.log}")
	private String velocityRuntimeLog;
	
	@Value("${htmlReport.resolveAbsolutPath:false}")
	private boolean resolveAbsolutPath;
	private static boolean resolveAbsolutPathStatic;

	
	@PostConstruct
	private void init(){
		resolveAbsolutPathStatic = resolveAbsolutPath;
	}

	// Legacy code replaced by the DB-based HTML report service.
	public void csv2Html() {		
		try(FileWriter writer = new FileWriter(htmlReportFileName);CsvReaderIterable csvRows = new CsvReaderIterable(reportFileName, true)){
			logger.info("Creating html report {} out of {}",htmlReportFileName,reportFileName);
			final VelocityEngine ve = new VelocityEngine();
			ve.setProperty("runtime.log", velocityRuntimeLog);
			ve.init();		
			final Template t = ve.getTemplate( reportTemplateFileName );
			final VelocityContext context = new VelocityContext();
			context.put("csvRows", csvRows);
			context.put("htmlFormatter", this);
			t.merge( context, writer );
			logger.info("Finished html report {}",htmlReportFileName);
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String filename2Hyperlink(final String filename) {
		return filename2Hyperlink(filename, null, null);
	}
	// Allow path prefix replacement from Template
	public static String filename2Hyperlink(final String filename, final String findStr, final String replaceStr) {
		String link = null; 
		try {
			File f = new File(filename);
			if (resolveAbsolutPathStatic) {
				f = f.getAbsoluteFile();
			}
			final String absFilename = f.toString();
			link = f.toURI().toASCIIString();
			// Fix specific Windows paths
			if(absFilename.startsWith("\\")) {
				link = link.replace("file:", "file:/");
			}
		}catch(final Exception e){
			logger.debug("Error parsing URI {} filename",e);
		}
		if (link == null) {
			link = filename.replace('\\','/');
			// Fix specific Windows paths
			if(filename.startsWith("\\") || filename.charAt(1)==':') {
				link = "/"+link;
			}
			link = "file://"+link.replace("#", "%23");
		}
		if (findStr != null && replaceStr != null) {
			link = link.replace(findStr, replaceStr);
		}
		return link;
	}
	
}
