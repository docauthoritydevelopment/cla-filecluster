package com.cla.filecluster.service.export;

import lombok.Data;

@Data(staticConstructor = "of")
public class ResultType {
    private final String fileName;
    private final String fileType;
    private final String contentType;
}
