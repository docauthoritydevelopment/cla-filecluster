package com.cla.filecluster.service.extractor.entity;

import com.cla.common.domain.dto.extraction.ExtractedEntityDto;
import com.cla.filecluster.domain.entity.extraction.ExtractionRule;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

/**
 * Created by uri on 13/01/2016.
 */
public class ExtractionRulesUtils {

    static final Logger logger = LoggerFactory.getLogger(ExtractionRulesUtils.class);

    public static List<ExtractionRule> parseRules(final List<String> rulesLines) {
        return
                rulesLines.stream().map(line->{
                    final ExtractionRule r = new ExtractionRule();
                    r.setType(ExtractionRule.RuleType.SIMPLE_RULE);
                    final String[] split1 = line.split("=",2);
                    if (split1.length!=2) {
                        logger.warn("Bad rule line {}. missing =",line);
                        return null;
                    }
                    r.setQuery(split1[1]);
                    final String[] split2 = split1[0].split("/",2);
                    r.setName(split2[0]);
                    if (split2.length==2) {
                        final String[] split3 = split2[1].split(":");
                        if (split3[0].equals("count")) {
                            if (split3.length!=2) {
                                logger.warn("Bad rule line {}. missing :",line);
                                return null;
                            }
                            r.setType(ExtractionRule.RuleType.AGGREGATED_RULE);
                            r.setParam((short)Integer.parseInt(split3[1]));
                        }
                        else {
                            logger.warn("Bad rule line {}. unknown type {}",line, split2[1]);
                            return null;
                        }
                    }
                    return r;
                }).filter(r->r!=null).collect(toList());
    }


    public static String replaceCombination(String templateQuery, final String firstFieldHeader,
                                            final String firstFieldValue,
                                            final ExtractedEntityDto extractedEntityDto,String combinationProximity) {
        if(!(templateQuery.contains("$["))) {
            return templateQuery;
        }
        for (Map.Entry<String, Object> field : extractedEntityDto.getFields().entrySet()) {
            final String secondFieldHeader = field.getKey();
            if(firstFieldHeader.equals(secondFieldHeader)) {
                continue;
            }
            final String secondFieldValue = getFieldStringValue(field);
            if(secondFieldValue!=null){
                final String combinations = getCombinations(firstFieldValue,secondFieldValue,combinationProximity);
                templateQuery = templateQuery.replaceAll("\\$\\["+firstFieldHeader+","+secondFieldHeader+"\\]",combinations);
                templateQuery = templateQuery.replaceAll("\\$\\["+secondFieldHeader+","+firstFieldHeader+"\\]",combinations);
            }
        }

        return templateQuery;
    }

    static String getAndOrCombinations(final String firstFieldValue, final String secondFieldValue, String combinationProximity) {
        final List<String> combinations = new LinkedList<>();
        final String[] secondSet = secondFieldValue.split("\\s");
        for (final String s : secondSet) {
            combinations.add("\""+firstFieldValue+" "+s+"\"~"+combinationProximity);
        }

        return combinations.stream().collect(joining(" OR ","(",")"));
    }

    private static String getCombinations(final String firstFieldValue, final String secondFieldValue, String combinationProximity) {
        final List<String> combinations = new LinkedList<>();
        final String[] firstSet = firstFieldValue.split("\\s");
        final String[] secondSet = secondFieldValue.split("\\s");
        for (final String f : firstSet) {
            for (final String s : secondSet) {
                combinations.add("\""+f+" "+s+"\"~"+combinationProximity);
            }
        }

        return combinations.stream().collect(joining(" OR ","(",")"));
    }

    static String replaceORMacro(final String templateQuery, final String fieldHeader, final String fieldValue) {
		return templateQuery.replaceAll("\\$"+fieldHeader, fieldValue);
	}
    public static String replaceArrayMacro(String query, String fieldHeader, ArrayList value) {
        String token = "${"+fieldHeader+"}";
        if (!query.contains(token)) {
            return query;
        }
        StringJoiner stringJoiner = new StringJoiner(" OR ");
        for (Object o : value) {
            String filled = query.replace(token, o.toString());
            stringJoiner.add(filled);
        }
        return stringJoiner.toString();
    }

//    static String replaceORArrayMacro(final String templateQuery, final String fieldHeader, final String[] fieldValue) {
//        return templateQuery.replaceAll("\\$\\[\\["+fieldHeader+"\\]\\]", fieldValue);
//    }

    static String replaceANDMacro(String templateQuery, final String fieldHeader, final String fieldValue) {
        final String AndFieldValue = fieldValue.replaceAll("\\s", " AND ");
        templateQuery = templateQuery.replaceAll("\\$\\$"+fieldHeader, AndFieldValue);
        return templateQuery;
    }

    static String replaceAndOrCombination(String templateQuery, final String firstFieldHeader, final String firstFieldValue,
                                          ExtractedEntityDto extractedEntityDto, String combinationProximity) {
        if(!(templateQuery.contains("$[!"))) {
            return templateQuery;
        }
        for (Map.Entry<String, Object> field : extractedEntityDto.getFields().entrySet()) {
            final String secondFieldHeader = field.getKey();
            if(firstFieldHeader.equals(secondFieldHeader)) {
                continue;
            }
            final String secondFieldValue = getFieldStringValue(field);
            if(secondFieldValue!=null){
                final String combinations = getAndOrCombinations(firstFieldValue,secondFieldValue,combinationProximity);
                templateQuery = templateQuery.replaceAll("\\$\\[!"+firstFieldHeader+","+secondFieldHeader+"\\]",combinations);
            }
        }
        return templateQuery;
    }

    public static String getFieldStringValue(Map.Entry<String, Object> field) {
        if (field.getValue() == null) {
            return null;
        }
        if (field.getValue() instanceof String[]) {
            //For now - return an OR
            String[] value = (String[]) field.getValue();
            return StringUtils.join(value," OR ");
        }
        if (field.getValue() instanceof ArrayList) {
            ArrayList value = (ArrayList) field.getValue();
            StringJoiner stringJoiner = new StringJoiner(" ");
            for (Object o : value) {
                stringJoiner.add(o.toString());
            }
            return stringJoiner.toString();
        }
        return field.getValue().toString();
    }

}
