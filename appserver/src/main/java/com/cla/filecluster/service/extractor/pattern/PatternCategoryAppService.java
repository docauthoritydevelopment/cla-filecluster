package com.cla.filecluster.service.extractor.pattern;

import com.cla.common.domain.dto.filesearchpatterns.PatternCategoryDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.searchpatterns.PatternCategory;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.searchpattern.PatternCategoryRepository;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ophir on 10/04/2019.
 */
@Service
public class PatternCategoryAppService {

    private static final Logger logger = LoggerFactory.getLogger(PatternCategoryAppService.class);

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private PatternCategoryRepository patternCategoryRepository;

    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Transactional
    public void deletePatternCategory(int id) {
        PatternCategoryDto patternCategoryDto = getPatternCategoryById(id);
        if (patternCategoryDto.getParentId() == null) {
            List<PatternCategory> childCats =  patternCategoryRepository.findByParentId(patternCategoryDto.getId());
            boolean findRelatedPattern = childCats.stream()
                    .anyMatch(c -> c.getPatterns() != null && c.getPatterns().size() > 0);

            if (findRelatedPattern){
                throw new BadRequestException(messageHandler.getMessage("pattern-categories.delete.contains-subcategories",
                        Collections.singletonList("'"+patternCategoryDto.getName()+"'")), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
            }
            else {
                for (PatternCategory child : childCats ){
                    patternCategoryRepository.deleteById(child.getId());
                };
            }
        }
        else {
            List<TextSearchPattern> relatedPatterns = textSearchPatternRepository.findBySubCategoryId(patternCategoryDto.getId());
            if (relatedPatterns!= null && relatedPatterns.size()>0){
                throw new BadRequestException(messageHandler.getMessage(
                        "pattern-categories.delete.patternCategories.delete.used-by-pattern"),BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
            }
        }
        patternCategoryRepository.deleteById(id);
    }

    @Transactional
    public Page<PatternCategoryDto> listPatternsCategories(DataSourceRequest dataSourceRequest) {
        return listPatternsCategories(dataSourceRequest,false);
    }

    @Transactional
    public Page<PatternCategoryDto> listPatternsCategories(DataSourceRequest dataSourceRequest, Boolean getFullPatternCategory) {
        Page<PatternCategory> patternCategories = patternCategoryRepository.findAll(dataSourceRequest.createPageRequestWithSort());
        return convertPatternCategoriesToDto(patternCategories, getFullPatternCategory);
    }

    @Transactional
    public PatternCategoryDto updatePatternCategory(PatternCategoryDto patternCategoryDto) {
        validatePatternCategory(patternCategoryDto);
        logger.debug("Update pattern category to {}",patternCategoryDto);
        PatternCategory patternCategory = patternCategoryRepository.getOne(patternCategoryDto.getId());
        boolean stateChanged = false;
        if (patternCategory.isActive() != patternCategoryDto.isActive()) {
            stateChanged = true;
        }
        convertPatternCategoryDtoToPatternCategory(patternCategory, patternCategoryDto);
        PatternCategory ansPatternCategory = patternCategoryRepository.save(patternCategory);

        if (stateChanged) {
            claFileSearchPatternService.patternCategoryStateUpdate(patternCategoryDto.getId(), patternCategoryDto.isActive());
        }

        return convertPatternCategoryToDto(ansPatternCategory,false);
    }


    private void convertPatternCategoryDtoToPatternCategory(PatternCategory targetPatternCategory,PatternCategoryDto patternCategoryDto) {
        targetPatternCategory.setName(patternCategoryDto.getName());
        targetPatternCategory.setActive(patternCategoryDto.isActive());
        if (patternCategoryDto.getParentId()!=null) {
            PatternCategory parentPattern = patternCategoryRepository.findById(patternCategoryDto.getParentId()).orElse(null);
            targetPatternCategory.setParent(parentPattern);
        }
    }

    @Transactional
    public PatternCategoryDto createPatternCategory(PatternCategoryDto patternCategoryDto) {
        validatePatternCategory(patternCategoryDto);
        logger.debug("Create Pattern category {}",patternCategoryDto);
        PatternCategory patternCategory = new PatternCategory();
        convertPatternCategoryDtoToPatternCategory(patternCategory,patternCategoryDto);
        PatternCategory ansPatternCategory = patternCategoryRepository.save(patternCategory);
        return convertPatternCategoryToDto(ansPatternCategory,false);
    }

    @Transactional(readOnly = true)
    public PatternCategoryDto getPatternCategoryById(Integer id) {
        PatternCategory  patternCategory =patternCategoryRepository.findById(id).orElse(null);
        if (patternCategory == null){
            throw new BadRequestException(messageHandler.getMessage("pattern-categories.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return convertPatternCategoryToDto(patternCategory,true);
    }

    private void validatePatternCategory(PatternCategoryDto patternCategoryDto) {
        if (patternCategoryDto == null) {
            throw new BadRequestException(messageHandler.getMessage("pattern-categories.empty-value"),BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(patternCategoryDto.getName()) || patternCategoryDto.getName().trim().isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("pattern-categories.name.missing"),BadRequestType.MISSING_FIELD);
        }

        PatternCategory patternCategory = getPatternCategoryByNameAndParentId(patternCategoryDto.getName(),patternCategoryDto.getParentId());
        if (patternCategory != null && !patternCategory.getId().equals(patternCategoryDto.getId())) {
            throw new BadRequestException(messageHandler.getMessage("pattern-categories.name.exist",
                    Collections.singletonList("'" + patternCategoryDto.getName() + "'")),BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    @Transactional
    public PatternCategoryDto updatePatternCategoryState(int id, boolean state) {
        java.util.Optional<PatternCategory> patternCategoryOption = patternCategoryRepository.findById(id);
        if (!patternCategoryOption.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("pattern-categories.not-found"), BadRequestType.ITEM_NOT_FOUND);
        }
        PatternCategory patternCategory = patternCategoryOption.get();
        patternCategory.setActive(state);
        PatternCategory ansCategory =  patternCategoryRepository.save(patternCategory);
        claFileSearchPatternService.patternCategoryStateUpdate(id, state);
        return convertPatternCategoryToDto(ansCategory,false);
    }


    @Transactional
    PatternCategory getPatternCategoryByNameAndParentId(String name, Integer parentId) {
        if (parentId == null) {
            return patternCategoryRepository.findByNameAndNullParentId(name);
        }
        else {
            return patternCategoryRepository.findByNameAndParentId(name, parentId);
        }
    }

    private static List<PatternCategoryDto> convertPatternCategoriesToDto(List<PatternCategory> categories,Boolean getFullPatternCategory) {
        List<PatternCategoryDto> result = new ArrayList<>();
        for (PatternCategory category : categories) {
            PatternCategoryDto patternCategoryDto = convertPatternCategoryToDto(category,getFullPatternCategory);
            result.add(patternCategoryDto);
        }
        return result;
    }

    public static Page<PatternCategoryDto> convertPatternCategoriesToDto(Page<PatternCategory> categories, Boolean getFullPatternCategory) {
        List<PatternCategoryDto> content = convertPatternCategoriesToDto(categories.getContent(),getFullPatternCategory);
        Pageable pageable = PageRequest.of(categories.getNumber(), categories.getSize());
        return new PageImpl<>(content, pageable, categories.getTotalElements());
    }

    public static PatternCategoryDto convertPatternCategoryToDto(PatternCategory category,Boolean getFullPatternCategory) {
        PatternCategoryDto patternCategoryDto = new PatternCategoryDto();
        patternCategoryDto.setId(category.getId());
        patternCategoryDto.setName(category.getName());
        patternCategoryDto.setActive(category.isActive());
        if (category.getParent() != null) {
            patternCategoryDto.setParentId(category.getParent().getId());
        }
        if (getFullPatternCategory) {
            patternCategoryDto.setPatterns(ClaFileSearchPatternService.convertTextSearchPatternsToDto(new ArrayList<>(category.getPatterns())));
        }
        return patternCategoryDto;
    }
}
