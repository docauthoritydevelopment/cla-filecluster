package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The interface each group operation should implement to be ran by ScheduledOperationService
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
public interface ScheduledOperation {
    int getTotalStepNum();
    boolean runStep(int step, Map<String,String> opData);
    void validate(Map<String,String> opData);
    ScheduledOperationType operationTypeHandled();
    default void preOperation(Map<String, String> opData) {}
    default void postOperation(Map<String, String> opData) {}
    default List<String> getRelevantGroupIds(Map<String, String> opData) {return new ArrayList<>();}
    default boolean shouldAllowCreateWhileScheduleDown() {return false;}
    default boolean operationTypeUnique() {return false;}
    default boolean operationAllowedToRun(ScheduledOperationMetadata operationMetadata) {return true;}
}
