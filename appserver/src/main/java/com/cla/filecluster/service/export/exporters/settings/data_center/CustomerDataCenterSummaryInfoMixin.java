package com.cla.filecluster.service.export.exporters.settings.data_center;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import static com.cla.filecluster.service.export.exporters.settings.data_center.CustomerDataCenterHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class CustomerDataCenterSummaryInfoMixin {

    @JsonUnwrapped
    private CustomerDataCenterDto customerDataCenterDto;

    @JsonProperty(ROOT_FOLDERS)
    private Long rootFolderCount;

    @JsonProperty(MPS)
    private Integer mediaProcessorCount;
}
