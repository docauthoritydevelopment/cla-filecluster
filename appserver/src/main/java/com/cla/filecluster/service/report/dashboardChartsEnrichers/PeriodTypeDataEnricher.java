package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.filecluster.service.report.DashboardDataEnricher;
import org.springframework.stereotype.Component;


@Component
public interface PeriodTypeDataEnricher extends DashboardDataEnricher {

    default ChartQueryType getQueryType() {
        return ChartQueryType.PERIOD;
    }

}