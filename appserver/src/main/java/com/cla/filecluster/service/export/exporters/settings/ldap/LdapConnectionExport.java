package com.cla.filecluster.service.export.exporters.settings.ldap;

import com.cla.common.domain.dto.security.LdapConnectionDetailsDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.security.LdapConnectionDetailsService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.ldap.LdapConnectionHeaderFields.*;

/**
 * Created by: yael
 * Created on: 5/27/2019
 */
@Slf4j
@Component
public class LdapConnectionExport extends PageCsvExcelExporter<LdapConnectionDetailsDto> {

    @Autowired
    private LdapConnectionDetailsService ldapConnectionDetailsService;

    private final static String[] header = {NAME, HOST, ACTIVE, TYPE, MANAGE_DN, PORT, SSL};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.LDAP_CONNECTION_SETTINGS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(LdapConnectionDetailsDto.class, LdapConnectionDetailsDtoMixin.class);
    }

    @Override
    protected Page<LdapConnectionDetailsDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        return ldapConnectionDetailsService.getAllLdapConnectionDtos(pageRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(LdapConnectionDetailsDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        if (base.getServerDialect() != null) {
            result.put(TYPE, base.getServerDialect().getName());
        }
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
