package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.alert.AlertDto;
import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.service.alert.AlertService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * api for alerts management (CRUD)
 *
 * Created by: yael
 * Created on: 1/15/2018
 */
@RestController
@RequestMapping("/api/alert")
public class AlertApiController {

    private final static Logger logger = LoggerFactory.getLogger(AlertApiController.class);

    @Autowired
    private AlertService alertService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<AlertDto> listAlerts(@RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return alertService.getAllAlerts(dataSourceRequest);
    }

    @RequestMapping(value = "/unacknowledged", method = RequestMethod.GET)
    public Page<AlertDto> listNotAcknowledgedAlerts(@RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        EventType type = null;
        AlertSeverity severity = null;

        if (params.containsKey("severity")) {
            severity = AlertSeverity.valueOf(params.get("severity"));
        }

        if (params.containsKey("type")) {
            type = EventType.valueOf(params.get("type"));
        }

        if (params.containsKey("severity") && params.containsKey("type")) {
            return alertService.getNotAcknowledgedBySeverityAndType(dataSourceRequest, severity, type);
        } else if (params.containsKey("severity")) {
            return alertService.getNotAcknowledgedBySeverity(dataSourceRequest, severity);
        } else if (params.containsKey("type")) {
            return alertService.getNotAcknowledgedByType(dataSourceRequest, type);
        } else {
            return alertService.getNotAcknowledgedAlerts(dataSourceRequest);
        }
    }

    @RequestMapping(value = "/acknowledged/{id}", method = RequestMethod.POST)
    public void setAlertAcknowledged(@PathVariable Long id, @RequestParam Boolean acknowledged) {
        alertService.setAlertAcknowledged(id, acknowledged);
        eventAuditService.audit(AuditType.ALERT, "Alert update", AuditAction.UPDATE, AlertDto.class, id);
    }

    @RequestMapping(value = "/acknowledged", method = RequestMethod.POST)
    public void setAlertAcknowledged(@RequestParam String ids, @RequestParam Boolean acknowledged) {
        String[] idArray = StringUtils.split(ids, ',');
        for (String idStr : idArray) {
            Long id = Long.parseLong(idStr);
            setAlertAcknowledged(id, acknowledged);
        }
    }

    @RequestMapping(value = "/severity", method = RequestMethod.POST)
    public PolicyConfig changeAlertSeverity(@RequestParam EventType eventType, @RequestParam AlertSeverity alertSeverity) {
        PolicyConfig policyConfig = alertService.changeAlertSeverity(eventType, alertSeverity);
        if (policyConfig != null) {
            eventAuditService.audit(AuditType.ALERT, "Alert severity config update", AuditAction.UPDATE, PolicyConfig.class, eventType, alertSeverity);
            return policyConfig;
        }
        throw new BadRequestException(messageHandler.getMessage("alert.policy-definition.empty"), BadRequestType.ITEM_NOT_FOUND);
    }
}
