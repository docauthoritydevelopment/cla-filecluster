package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.functionalrole.FunctionalRoleAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.associations.DataRoleHeaderFields.*;

/**
 * Created by: yael
 * Created on: 2/5/2019
 */
@Slf4j
@Component
public class DataRoleExporter extends PageCsvExcelExporter<AggregationCountItemDTO<FunctionalRoleDto>> {

    final static String[] HEADER = {NAME, DESCRIPTION, NUM_OF_FILES};

    @Autowired
    private FunctionalRoleAppService functionalRoleAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, DataRoleAggCountDtoMixin.class);
        mapper.addMixIn(FunctionalRoleDto.class, FunctionalRoleDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<FunctionalRoleDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<FunctionalRoleDto>> res = functionalRoleAppService.listFuncRolesFileCountWithFileFilter(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<FunctionalRoleDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DATA_ROLE);
    }
}
