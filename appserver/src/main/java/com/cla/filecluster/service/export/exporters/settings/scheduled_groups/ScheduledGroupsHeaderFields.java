package com.cla.filecluster.service.export.exporters.settings.scheduled_groups;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public interface ScheduledGroupsHeaderFields {
    String NAME = "NAME";
    String SCHEDULE_DETAILS = "Schedule details";
    String ACTIVE = "ACTIVE";
    String TYPE = "TYPE";
    String DESCRIPTION = "DESCRIPTION";
    String SCHEDULE_EVERY = "SCHEDULE_EVERY";
    String SCHEDULE_HOUR = "SCHEDULE_HOUR";
    String SCHEDULE_MINUTES = "SCHEDULE_MINUTES";
    String SCHEDULE_CRON = "SCHEDULE_CRON";
    String SCHEDULE_DAYS_OF_THE_WEEK = "SCHEDULE_DAYS_OF_THE_WEEK";
    String ROOT_FOLDERS = "Root folders";
}
