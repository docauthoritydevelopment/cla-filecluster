package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternDto;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.extractor.pattern.SearchPatternAppService;
import com.cla.filecluster.service.extractor.pattern.SearchPatternSchemaLoader;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 23/06/2016.
 */
@RestController
@RequestMapping("/api/searchpattern")
public class SearchPatternApiController {

    @Autowired
    private SearchPatternAppService searchPatternAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private SearchPatternSchemaLoader searchPatternSchemaLoader;

    @Autowired
    private MessageHandler messageHandler;

    Logger logger = LoggerFactory.getLogger(SearchPatternApiController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<TextSearchPatternDto> listSearchPatterns(@RequestParam Map<String,String> params) {
        List<Integer> regulationIds = null;

        if (!Strings.isNullOrEmpty(params.get("regulationFilter"))) {
            String regulationFilterStr = params.get("regulationFilter").replace(" ", "");
            if (!regulationFilterStr.isEmpty()) {
                regulationIds = Stream.of(regulationFilterStr.split(","))
                        .map(Integer::parseInt)
                        .collect(Collectors.toList());
            }
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return searchPatternAppService.listSearchPatterns(dataSourceRequest,regulationIds);
    }

    @RequestMapping(value = "/load", method = {RequestMethod.GET, RequestMethod.PUT})
    public String loadSearchPatterns(@RequestParam(value = "path", required = true) final String path) {
        logger.info("Loading searchPatterns template file: {}", path);
        if (path != null && !path.isEmpty()) {
            searchPatternAppService.populateDefaultSearchPatterns(path);
        }
        return "Loaded: " + path;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public TextSearchPatternDto createSearchPattern(@RequestBody TextSearchPatternDto textSearchPatternDto) {
        TextSearchPatternDto searchPattern = searchPatternAppService.createSearchPattern(textSearchPatternDto);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Create search pattern", AuditAction.CREATE, TextSearchPatternDto.class,textSearchPatternDto.getId(),textSearchPatternDto);
        return searchPattern;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public TextSearchPatternDto updateSearchPattern(@PathVariable int id, @RequestBody TextSearchPatternDto textSearchPatternDto) {
        TextSearchPatternDto textSearchPatternDto1 = searchPatternAppService.updateSearchPattern(id, textSearchPatternDto);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Update search pattern", AuditAction.UPDATE, TextSearchPatternDto.class,textSearchPatternDto.getId(),textSearchPatternDto);
        return textSearchPatternDto1;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteSearchPattern(@PathVariable int id) {
        TextSearchPatternDto searchPattern = getSearchPattern(id);
        searchPatternAppService.deleteSearchPattern(id);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Delete search pattern", AuditAction.DELETE, TextSearchPatternDto.class,searchPattern.getId(),searchPattern);
    }

    @RequestMapping(value = "/{id}/enable", method = RequestMethod.POST)
    public TextSearchPatternDto enableSearchPattern(@PathVariable int id) {
        TextSearchPatternDto textSearchPatternDto = searchPatternAppService.updateSearchPatternState(id, true);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Update search pattern", AuditAction.UPDATE, TextSearchPatternDto.class,textSearchPatternDto.getId(),textSearchPatternDto);
        return textSearchPatternDto;
    }

    @RequestMapping(value = "/{id}/disable", method = RequestMethod.POST)
    public TextSearchPatternDto disableSearchPattern(@PathVariable int id) {
        TextSearchPatternDto textSearchPatternDto = searchPatternAppService.updateSearchPatternState(id, false);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Update search pattern", AuditAction.UPDATE, TextSearchPatternDto.class,textSearchPatternDto.getId(),textSearchPatternDto);
        return textSearchPatternDto;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public TextSearchPatternDto getSearchPattern(@PathVariable int id) {
        TextSearchPatternDto textSearchPatternDto = searchPatternAppService.getPattern(id);
        if (textSearchPatternDto == null) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return textSearchPatternDto;
    }

    @RequestMapping(value="/import/csv", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto importCsvFile(@RequestParam("file") MultipartFile file, @RequestParam(value ="updateDuplicates",required = false,defaultValue = "false") boolean updateDuplicates){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import search pattern CSV File. update duplicates set to {}",updateDuplicates);
            byte[] bytes = file.getBytes();
            ImportSummaryResultDto importSummaryResultDto = searchPatternSchemaLoader.doImport(false, bytes, null, updateDuplicates, true);
            eventAuditService.audit(AuditType.SEARCH_PATTERN,"Import search patterns", AuditAction.IMPORT, MultipartFile.class,file.getName(), Pair.of("updateDuplicates",updateDuplicates));
            return importSummaryResultDto;
        } catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(),e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @RequestMapping(value="/import/csv/validate", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto validateImportCsvFile(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("maxErrorRowsReport") Integer maxErrorRowsReport){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("search-patterns.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("validate import schedule CSV File");
            byte[] bytes = file.getBytes();
            return searchPatternSchemaLoader.doImport(true, bytes,(maxErrorRowsReport==null? 200: maxErrorRowsReport),false);
        } catch (Exception e) {
            logger.error("Validate import CSV error:" + e.getMessage(),e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription("Validate import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @RequestMapping("/filecount")
    public FacetPage<AggregationCountItemDTO<TextSearchPatternDto>> getPatternsFileCounts(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover search patterns export", AuditAction.VIEW, TextSearchPatternDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover search patterns", AuditAction.VIEW, TextSearchPatternDto.class, null, dataSourceRequest);
        }
        boolean withoutValidation = params.containsKey("withoutValidation") && params.get("withoutValidation").equals("true");
        return searchPatternAppService.getTextSearchPatternFileCounts(dataSourceRequest, false, withoutValidation);
    }

    @RequestMapping("/filecount/multi")
    public FacetPage<AggregationCountItemDTO<TextSearchPatternDto>> getPatternsMultiFileCounts(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(dataSourceRequest.isExport() ? AuditType.EXPORT : AuditType.QUERY , "Discover search patterns", AuditAction.VIEW, TextSearchPatternDto.class, null, dataSourceRequest);
        boolean withoutValidation = params.containsKey("withoutValidation") && params.get("withoutValidation").equals("true");
        return searchPatternAppService.getTextSearchPatternFileCounts(dataSourceRequest, true, withoutValidation);
    }


}
