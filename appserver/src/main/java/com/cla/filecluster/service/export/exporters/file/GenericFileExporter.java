package com.cla.filecluster.service.export.exporters.file;

import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 2/26/2019
 */
@Component
public class GenericFileExporter {

    @Autowired
    private FilesApplicationService filesApplicationService;

    public Page<ClaFileDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);

        if (Strings.isNullOrEmpty(dataSourceRequest.getSolrPagingCursor())) {
            dataSourceRequest.setSolrPagingCursor("*");
        }

        Page<ClaFileDto> result = filesApplicationService.listFilesDeNormalized(dataSourceRequest);

        if (!Strings.isNullOrEmpty(dataSourceRequest.getSolrPagingCursor())) {
            requestParams.put(DataSourceRequest.SOLR_PAGING_CURSOR, dataSourceRequest.getSolrPagingCursor());
        }

        return result;
    }
}
