package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.repository.jpa.actions.ActionExecutionStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ActionExecutionStatusService {

    @Autowired
    private ActionExecutionStatusRepository actionExecutionStatusRepository;

    private Logger logger = LoggerFactory.getLogger(ActionExecutionStatusService.class);

    public ActionExecutionTaskStatus getActionTrigger(long actionTriggerId) {
        return actionExecutionStatusRepository.findById(actionTriggerId).orElse(null);
    }

    @Transactional
    public ActionExecutionTaskStatus updateActionTriggerState(Long triggerId, RunStatus status) {
        return actionExecutionStatusRepository.findById(triggerId)
                .map(one -> {
                    one.setStatus(status);
                    return actionExecutionStatusRepository.save(one);
                })
                .orElse(null);
    }

    @Transactional
    public void increaseActionTriggerCounter(Long triggerId, int executedActions) {
        actionExecutionStatusRepository.findById(triggerId)
                .ifPresent(one -> {
                    one.setProgressMark(executedActions + one.getProgressMark());
                    actionExecutionStatusRepository.save(one);
                });
    }

    @Transactional(readOnly = true)
    public void updateErrorsCount(Long triggerId, long errorsCount, String firstError) {
        actionExecutionStatusRepository.findById(triggerId)
                .filter(one -> one.getErrorsCount() < errorsCount)
                .ifPresent(one -> {
                    one.setFirstError(firstError);
                    one.setErrorsCount(errorsCount);
                    actionExecutionStatusRepository.save(one);
                });
    }

    @Transactional(readOnly = true)
    public ActionExecutionTaskStatus getActionExecutionTaskStatus(Long id) {
        return actionExecutionStatusRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public Page<ActionExecutionTaskStatus> getAllActionTriggers(PageRequest pageRequest) {
        return actionExecutionStatusRepository.findAll(pageRequest);
    }

    @Transactional(readOnly = true)
    public List<ActionExecutionTaskStatus> getRunningActionTriggers() {
        return actionExecutionStatusRepository.findByRunStatus(RunStatus.RUNNING);
    }

    @Transactional(readOnly = true)
    public ActionExecutionTaskStatus getLastRunningActionExecution() {
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.DESC, "triggerTime"));
        Pageable pageable = PageRequest.of(0, 1, sort);
        List<ActionExecutionTaskStatus> actionExecution = actionExecutionStatusRepository.getActionExecution(pageable);
        return actionExecution.size() > 0 ? actionExecution.get(0) : null;
    }

}
