package com.cla.filecluster.service.export.exporters.file.prop;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

/**
 * Created by: ophir
 * Created on: 2/6/2019
 */
public class FileExtensionDtoMixin {

    @JsonProperty(NAME)
    private String extension;
}
