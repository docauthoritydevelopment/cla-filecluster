package com.cla.filecluster.service.api.groups;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.*;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.pv.PVType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.domain.dto.word.WordFilesSimilarity;
import com.cla.common.exceptions.NotSupportedException;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.excel.ExcelSheetSimilarity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.filetag.AssociableEntity;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.FileGroupSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrAnalysisDataRepository;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.analyze.MaturityLevelService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.operation.*;
import com.cla.filecluster.util.ControllerUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
public class GroupsApplicationService {

    private static final Logger logger = LoggerFactory.getLogger(GroupsApplicationService.class);

    @Value("${ingester.group.dump.pageSize:1000}")
    private int groupDumpPageSize;

    @Value("${ingester.dump.pageSize:100}")
    private int dumpIngestPageSize;

    @Value("${ingester.group.dump.pageSleepMS:100}")
    private long groupDumpPageSleepMS;

    @Value("${file-groups.update-files-page-size:5000}")
    private int updateGroupPageSize;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MaturityLevelService maturityLevelService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private GroupLockService groupLockService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private DBTemplateUtils templateUtils;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileCatAppService fileCatAppService;

    private final Set<String> joinInProgress = Sets.newConcurrentHashSet();

    public Page<GroupDto> listAllGroups(final DataSourceRequest dataSourceRequest) {
        Page<GroupDto> groupDtoPage;
        final PageRequest pageRequest = getPageRequest(dataSourceRequest);
        if (dataSourceRequest.getFilter() != null) {
            //TODO - add order by to specification
            final Specification<FileGroup> filterSpecification = convertFilterToSpecification(dataSourceRequest.getFilter());
            groupDtoPage = listAllGroups(filterSpecification, pageRequest);
            return groupDtoPage;
        }

        Pair<Long, List<GroupDto>> groupsResult;
        if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
            groupsResult = fileGroupService.listAllGroupsWithFirstFileDefaultSort((int)pageRequest.getOffset(), pageRequest.getPageSize());
        } else {
            groupsResult = fileGroupService.listAllGroupsWithFirstFile((int)pageRequest.getOffset(), pageRequest.getPageSize());
        }
        groupDtoPage = new PageImpl<>(groupsResult.getValue(), pageRequest, groupsResult.getKey());
        return groupDtoPage;
    }

    private PageRequest getPageRequest(final DataSourceRequest dataSourceRequest) {
        if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
            return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize());
        } else {
            //User asked us for a specific sort
            final Sort sort = DataSourceRequest.convertSort(dataSourceRequest, GroupDtoFieldConverter::convertDtoField);
            return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(), sort);
        }
    }

    private Page<GroupDto> listAllGroups(final Specification<FileGroup> specification, final PageRequest pageRequest) {
        Pair<Long, List<FileGroup>>fileGroupsRes = fileGroupService.findAll((int)pageRequest.getOffset(), pageRequest.getPageSize());
        final List<FileGroup> fileGroups = fileGroupsRes.getValue();
        AssociationRelatedData groupsAssociations = associationsService.getAssociationRelatedDataGroups(fileGroups);

        final List<GroupDto> content = new ArrayList<>(fileGroups.size());
        fileGroups.forEach(fileGroup -> {

            Collection< AssociationEntity > userGroupAssociations = groupsAssociations.getPerGroupAssociations().get(fileGroup.getId());
            Collection<AssociationEntity> userGroupDocTypeAssociations = AssociationsService.getDocTypeAssociation(userGroupAssociations);

            content.add(groupsService.convertGroupToDto(fileGroup, null, null,true,
                    userGroupDocTypeAssociations, userGroupAssociations));
        });
        return new PageImpl<>(content, pageRequest, fileGroupsRes.getKey());
    }

    private Specification<FileGroup> convertFilterToSpecification(final FilterDescriptor filter) {
        return new FileGroupSpecification(filter);
    }

    @Transactional(readOnly = true)
    public Page<AggregationCountItemDTO<GroupDto>> listGroupsByFolder(long folderId, DataSourceRequest dataSourceRequest) {
        DocFolder docFolder = docFolderService.findById(folderId);
        return groupsService.listUserGroupsWithCountByFolder(docFolder, dataSourceRequest);
    }

    @Transactional(readOnly = true)
    public Page<AggregationCountItemDTO<GroupDto>> listGroupsByFolderRecursive(Long folderId, DataSourceRequest dataSourceRequest) {
        DocFolder docFolder = docFolderService.findById(folderId);
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        return groupsService.listUserGroupsWithCountByFolderRecursive(docFolder, pageRequest, dataSourceRequest.getFilter(), null);
    }

    @Transactional(readOnly = true)
    public Page<AggregationCountItemDTO<RootFolderDto>> listGroupRootFolders(String groupId, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        return fileCatService.listGroupRootFolders(groupId, pageRequest);
    }

    /**
     * List groups with a file count for each group.
     * The Files in the groups can be filtered by various filters.
     * Only groups with files > 0 after filter are shown.
     * File counts are after filters of course...
     *
     * @return page of groups with a file count for each group
     */
    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<GroupDto>> listGroupsFileCountWithFileAndGroupFilter(DataSourceRequest dataSourceRequest) {
        if (dataSourceRequest.getGroupFilter() != null) {
            logger.debug("Use 2nd level group filter {}", dataSourceRequest.getGroupFilter());
            PageRequest originalPageRequest = dataSourceRequest.createPageRequest();
            dataSourceRequest.setPage(1);
            dataSourceRequest.setPageSize(10000);
            FacetPage<AggregationCountItemDTO<GroupDto>> unfilteredResults = listGroupsFileCountWithFileFilter(dataSourceRequest);
            logger.debug("Filter results ({}) by group filter", unfilteredResults.getNumberOfElements());

            Predicate<? super AggregationCountItemDTO<GroupDto>> filter = convertToFilter(dataSourceRequest.getGroupFilter());
            List<AggregationCountItemDTO<GroupDto>> filteredResults = unfilteredResults.getContent().stream().filter(filter).collect(Collectors.toList());
            logger.debug("Filtered results to ({}) items", filteredResults.size());
            int fromIndex = (int) originalPageRequest.getOffset();
            int toIndex = Math.min(filteredResults.size(), fromIndex + originalPageRequest.getPageSize());
            filteredResults = filteredResults.subList(fromIndex, toIndex);
            logger.debug("Filtered list to correct page");
            return new FacetPage<>(filteredResults, originalPageRequest, unfilteredResults.getPageAggregatedCount(), unfilteredResults.getTotalAggregatedCount());
        } else {
            return listGroupsFileCountWithFileFilter(dataSourceRequest);
        }
    }

    private Predicate<? super AggregationCountItemDTO<GroupDto>> convertToFilter(String groupFilter) {
        switch (groupFilter) {
            case "full_coverage":
                return (Predicate<AggregationCountItemDTO<GroupDto>>) groupDtoAggregationCountItemDTO ->
                        groupDtoAggregationCountItemDTO.getItem().getNumOfFiles() == groupDtoAggregationCountItemDTO.getCount();
            case "partial_coverage":
                return (Predicate<AggregationCountItemDTO<GroupDto>>) groupDtoAggregationCountItemDTO ->
                        groupDtoAggregationCountItemDTO.getItem().getNumOfFiles() == groupDtoAggregationCountItemDTO.getCount();
            default:
                throw new BadParameterException(messageHandler.getMessage("group.filter.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<GroupDto>> listGroupsFileCountWithFileFilter(DataSourceRequest dataSourceRequest) {
        logger.debug("list Groups File Count With File Filters");
        SolrSpecification solrFacetSpecification = getSolrSpecification(dataSourceRequest);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
        long total = fileCatAppService.getTotalFacetCount(dataSourceRequest, CatFileFieldType.USER_GROUP_ID);
        long totalFacet = fileCatService.getFacetNumber(solrFacetSpecification, CatFileFieldType.USER_GROUP_ID);

        SolrFacetQueryResponse solrFacetQueryResponseUnfiltered = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetSpecification = getSolrSpecification(dataSourceRequest);
            solrFacetSpecification.setAdditionalFilter(
                    CatFileFieldType.USER_GROUP_ID.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
            solrFacetQueryResponseUnfiltered = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
        }
        logger.debug("list file facets by Solr field Group_id returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        final FacetPage<AggregationCountItemDTO<GroupDto>> facetPage =
                convertFacetFieldResultToDto(solrFacetQueryResponse, solrFacetQueryResponseUnfiltered, solrFacetQueryResponse.getPageRequest(), totalFacet);
        facetPage.setTotalElements(total);
        return facetPage;
    }

    private SolrSpecification getSolrSpecification(DataSourceRequest dataSourceRequest) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.USER_GROUP_ID);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.USER_GROUP_ID.getSolrName() + ":*");
        return solrFacetSpecification;
    }


    private FacetPage<AggregationCountItemDTO<GroupDto>> convertFacetFieldResultToDto(
            SolrFacetQueryResponse solrFacetQueryResponse, SolrFacetQueryResponse solrFacetQueryResponseUnfiltered, PageRequest pageRequest, long totalFacet) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        FacetField facetFieldUnfiltered = solrFacetQueryResponseUnfiltered == null ? null : solrFacetQueryResponseUnfiltered.getFirstResult();
        List<AggregationCountItemDTO<GroupDto>> content = new ArrayList<>();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        long pageAggCount = groupsService.convertGroupFacetResults(facetField, facetFieldUnfiltered, content);
        enrichGroupsWithBizListItemAssociationDetails(content);
        FacetPage<AggregationCountItemDTO<GroupDto>> result = new FacetPage<>(content, pageRequest, pageAggCount, solrFacetQueryResponse.getNumFound());
        result.setLast(pageRequest.getOffset() + pageRequest.getPageSize() >= totalFacet);
        result.setTotalPageNumber(ControllerUtils.roundUpDivision(totalFacet, pageRequest.getPageSize()));
        return result;
    }

    private void enrichGroupsWithBizListItemAssociationDetails(List<AggregationCountItemDTO<GroupDto>> content) {
        logger.debug("enrichGroupsWithBizListItemAssociationDetails");
        Set<String> bizListItemIds = new HashSet<>();
        List<String> groupsInOps = scheduledOperationService.getGroupsWithOperations();

        for (AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO : content) {
            GroupDto group = groupDtoAggregationCountItemDTO.getItem();
            if (group.getAssociatedBizListItems() != null) {
                for (SimpleBizListItemDto simpleBizListItemDto : group.getAssociatedBizListItems()) {
                    bizListItemIds.add(simpleBizListItemDto.getId());
                }
            }

            group.setGroupProcessing(isGroupProcessing(group, groupsInOps));
        }
        if (bizListItemIds.size() > 0) {
            //Find values and enrich list
            Map<String, SimpleBizListItem> simpleBizListItemsAsMap = bizListItemService.getSimpleBizListItemsAsMap(bizListItemIds);
            for (AggregationCountItemDTO<GroupDto> groupDtoAggregationCountItemDTO : content) {
                if (groupDtoAggregationCountItemDTO.getItem().getAssociatedBizListItems() != null) {
                    for (SimpleBizListItemDto simpleBizListItemDto : groupDtoAggregationCountItemDTO.getItem().getAssociatedBizListItems()) {
                        SimpleBizListItem simpleBizListItem = simpleBizListItemsAsMap.get(simpleBizListItemDto.getId());
                        if (simpleBizListItem == null) {
                            logger.error("Failed to find bizList item id {} in database", simpleBizListItemDto.getId());
                            continue;
                        }
                        BizListItemService.fillBizListItemDtoDetails(simpleBizListItemDto, simpleBizListItem);
                    }
                }
            }
        }
    }

    private boolean isGroupProcessing(GroupDto group, List<String> groupsInOps) {
        boolean isGroupProcessing = false;
        if (group.isHasSubgroups()) {
            isGroupProcessing = groupLockService.isRawAnalysisGroupLockTaken(group.getId()) || groupsInOps.contains(group.getId());
        } else if (group.getGroupType().equals(GroupType.SUB_GROUP)) {
            isGroupProcessing =
                    groupLockService.isRawAnalysisGroupLockTaken(group.getRawAnalysisGroupId())
                            || groupsInOps.contains(group.getId())
                            || groupsInOps.contains(group.getRawAnalysisGroupId())
            ;
        } else {
            isGroupProcessing = groupsInOps.contains(group.getId());
        }
        return isGroupProcessing;
    }

    /**
     * @param groupId      group ID
     * @param newGroupName new group name
     * @param wait         - if false, do the action in the background
     */
    public void updateGroupName(String groupId, String newGroupName, boolean wait) {
        groupsService.updateGroupName(groupId, newGroupName);
        if (wait) {
            logger.info("Update group name on Solr Index");
            fileCatService.updateGroupNameOnFiles(groupId, newGroupName);
        } else {
            logger.info("Update group name on Solr Index in the background");
            executionManagerService.startAsyncTask(() -> {
                fileCatService.updateGroupNameOnFiles(groupId, newGroupName);
                return null;
            });
        }
    }


    @Transactional(readOnly = true)
    public void dumpIngestDataForGroup(String groupId, OutputStream outputStream, int offset, int limit, Integer maturityFilter) throws IOException {
        Writer writer = new PrintWriter(outputStream, false);
        //Add titles
        writer.write(getPvAnalysisTitles());
        int firstPage = offset / groupDumpPageSize;
        int pageOffset = offset % groupDumpPageSize;
        Pageable pageRequest = PageRequest.of(firstPage, groupDumpPageSize);
        int resultSize;
        do {
            Page<ClaFile> page = fileCatService.findByUserGroupIdFull(pageRequest, groupId);
            resultSize = page.getContent().size();
            logger.debug("dumpIngestDataForGroup - page {}/{}", page.getNumber(), page.getTotalPages());
            for (ClaFile claFile : page.getContent()) {
                if (pageOffset > 0) {    // for the fist page skip items if needed
                    pageOffset--;
                    continue;
                }
                PvAnalysisData pvAnalysisData = solrAnalysisDataRepository.findByContentId(claFile.getContentMetadataId());
                String details = getPvAnalysisSummary(claFile, pvAnalysisData, maturityFilter);

                if (details != null) {
                    writer.write(details);
                }
                limit--;
                if (limit == 0) {
                    break;
                }
            }
            pageRequest = pageRequest.next();
            writer.flush();
            try {
                Thread.sleep(groupDumpPageSleepMS);        // Yield for GC work
            } catch (InterruptedException e) {
                //continue;
            }
        } while (resultSize > 0 && limit > 0);
    }

    private static final PVType[] longHistogramNames = {
            PVType.PV_BodyNgrams
            , PVType.PV_SubBodyNgrams
            , PVType.PV_DocstartNgrams
            , PVType.PV_HeaderNgrams

            , PVType.PV_Headings
            , PVType.PV_StyleSignatures
            , PVType.PV_StyleSequence
            , PVType.PV_AllStyleSignatures
            , PVType.PV_AllStyleSequence
            , PVType.PV_AbsoluteStyleSignatures
            , PVType.PV_AbsoluteStyleSequence
            , PVType.PV_HeaderStyles
            , PVType.PV_ParagraphLabels
            , PVType.PV_LetterHeadLabels
            , PVType.PV_Pictures

            , PVType.PV_RowHeadings
            , PVType.PV_ExcelStyles
            , PVType.PV_CellValues
            , PVType.PV_Formulas
            , PVType.PV_ConcreteCellValues
            , PVType.PV_TextNgrams
            , PVType.PV_TitlesNgrams
            , PVType.PV_SheetLayoutSignatures
    };

    @Transactional(readOnly = true)
    public void dumpIngestMaturityStatsForGroup(String groupId, StringBuffer sb, int offset, int limit) {

        int firstPage = offset / groupDumpPageSize;
        int pageOffset = offset % groupDumpPageSize;
        Pageable pageRequest = PageRequest.of(firstPage, groupDumpPageSize);
        int resultSize;
        ClaSuperCollection<Long> histCollectionItems = new ClaSuperCollection<>();
        ClaSuperCollection<Long> histCollectionCount = new ClaSuperCollection<>();
        ClaSuperCollection<Long> histMl = new ClaSuperCollection<>();

        ClaHistogramCollection<Long> histMaturity = new ClaHistogramCollection<>();

        for (PVType longHistogramName : longHistogramNames) {
            histCollectionItems.getCollections().put(longHistogramName.name(), new ClaHistogramCollection<>());
            histCollectionCount.getCollections().put(longHistogramName.name(), new ClaHistogramCollection<>());
            histMl.getCollections().put(longHistogramName.name(), new ClaHistogramCollection<>());
        }

        do {

            Set<Long> contentIds = fileCatService.findContentsByAnalysisGroupId(pageRequest, groupId);
            resultSize = contentIds.size();

            for (Long contentId : contentIds) {
                if (pageOffset > 0) {    // for the fist page skip items if needed
                    pageOffset--;
                    continue;
                }
                PvAnalysisData pvAnalysisData = solrAnalysisDataRepository.findByContentId(contentId);
                processPvAnalysisStats(pvAnalysisData, histCollectionItems, histCollectionCount, histMl, histMaturity);
                limit--;
                if (limit == 0) {
                    break;
                }
            }
            pageRequest = pageRequest.next();
        } while (resultSize > 0 && limit > 0);

        sb.append("<h4>PV Stats for groupId ").append(groupId).append("</h4>");
        sb.append("Maturity: ").append(histMaturity.toStringOrderedKeyAsc(", "));
        sb.append("<br/>\n");
        for (int i = 0; i < longHistogramNames.length; ++i) {
            sb.append(longHistogramNames[i].name()).append(": (")
                    .append(histCollectionItems.getCollections().get(longHistogramNames[i].name()).getGlobalCount()).append(", ")
                    .append(histCollectionCount.getCollections().get(longHistogramNames[i].name()).getGlobalCount()).append(")")
                    .append(": <br/>\n");
            if (histMl.getCollections().get(longHistogramNames[i].name()).getItemsCount() > 0) {
                sb.append(" <span style='margin-left:2em;'>");
                sb.append(" ML: ").append(histMl.getCollections().get(longHistogramNames[i].name()).toStringOrderedKeyAsc(", "));
                sb.append("</span>").append("<br/>\n");
            }
            if (histCollectionItems.getCollections().get(longHistogramNames[i].name()).getItemsCount() > 0) {
                sb.append(" <span style='margin-left:2em;'>");
                sb.append(" items: ").append(histCollectionItems.getCollections().get(longHistogramNames[i].name()).toStringOrderedKeyAsc(", "));
                sb.append("</span>").append("<br/>\n");
                sb.append(" <span style='margin-left:2em;'>");
                sb.append(" totals: ").append(histCollectionCount.getCollections().get(longHistogramNames[i].name()).toStringOrderedKeyAsc(", "));
                sb.append("</span>").append("<br/>\n<br/>\n");
            }
        }
    }

    private void processPvAnalysisStats(final PvAnalysisData pvAnalysisData,
                                        final ClaSuperCollection<Long> histCollectionItems, final ClaSuperCollection<Long> histCollectionCount, ClaSuperCollection<Long> histMl, ClaHistogramCollection<Long> histMaturity) {
        Map<String, ClaSuperCollection<Long>> collections = pvAnalysisData.getPvCollections().getCollections();

        int maxMaturity = (-1);
        for (Map.Entry<String, ClaSuperCollection<Long>> stringClaSuperCollectionEntry : collections.entrySet()) {
            ClaSuperCollection<Long> value = stringClaSuperCollectionEntry.getValue();
            if (value == null) {
                value = new ClaSuperCollection<>();
            }
            int partMaturity = maturityLevelService.getPartMaturityLevel(value);
            if (partMaturity > maxMaturity) {
                maxMaturity = partMaturity;
            }
            for (PVType pvType : PVType.values()) {
                String name = pvType.name();
                ClaHistogramCollection<Long> longClaHistogramCollection = value.getCollections().get(name);
                if (longClaHistogramCollection != null) {
                    addHistogramStatsItem(name, longClaHistogramCollection.getItemsCount(), histCollectionItems);
                    addHistogramStatsItem(name, longClaHistogramCollection.getGlobalCount(), histCollectionCount);
                    if (maturityLevelService.isMaturityCriteria(name)) {
                        int ml = maturityLevelService.getPartMaturityLevel(name, longClaHistogramCollection);
                        histMl.getCollection(name).addItem((long) ml);
                    }
                }
            }
        }
        histMaturity.addItem((long) maxMaturity);
    }

    private void addHistogramStatsItem(String name, int value, ClaSuperCollection<Long> histCollection) {
        if (value < 10)
            value = ((value / 4)) * 4;
        else if (value < 100)
            value = ((value / 10)) * 10;
        else if (value < 1000)
            value = ((value / 100)) * 100;
        else if (value < 10000)
            value = ((value / 1000)) * 1000;
        else
            value = 10000;

        histCollection.getCollection(name).addItem((long) value);
    }


    public String dumpIngestSimpleDataForFiles(List<Long> claFileIds, Map<Long, Integer> countersMap1, Map<Long, Integer> countersMap2) {
        return dumpIngestSimpleDataForFiles(claFileIds, countersMap1, countersMap2, null, false);
    }

    public String dumpIngestSimpleDataForFiles(List<Long> claFileIds, Map<Long, Integer> countersMap1, Map<Long, Integer> countersMap2, boolean showSimValues) {
        return dumpIngestSimpleDataForFiles(claFileIds, countersMap1, countersMap2, null, showSimValues);
    }

    @Transactional(readOnly = true)
    public String dumpIngestSimpleDataForFiles(List<Long> claFileIds, Map<Long, Integer> countersMap1, Map<Long, Integer> countersMap2, Map<Long, Integer> countersMap3, boolean showSimValues) {
        List<List<Long>> subLists = Lists.partition(claFileIds, dumpIngestPageSize);
        StringBuilder sb = new StringBuilder();
        boolean showLegend = true;
        PvAnalysisData prevPvAnalysisData = (showSimValues && claFileIds.size() > 2) ?
                solrAnalysisDataRepository.findByContentId(claFileIds.get(claFileIds.size() - 1)) : null;    // Show similarity between last item and first one
        try {
            for (List<Long> sl : subLists) {
                List<SolrFileEntity> cfSet = fileCatService.findByFileIds(sl);
                Map<Long, SolrFileEntity> claFileMap = cfSet.stream().collect(Collectors.toMap(SolrFileEntity::getFileId, Function.identity()));
                List<Long> contentIds = cfSet.stream().map(SolrFileEntity::getContentId).filter(cid -> !Strings.isNullOrEmpty(cid))
                        .map(Long::parseLong).collect(Collectors.toList());
                List<PvAnalysisData> pvAnalysisDataList = solrAnalysisDataRepository.findByContentId(contentIds);
                Map<Long, PvAnalysisData> pvAnalysisDataMap = pvAnalysisDataList.stream()
                        .collect(Collectors.toMap(PvAnalysisData::getContentId, Function.identity()));

                for (Long id : sl) {
                    sb.append(id).append(" (").append(notNull(countersMap1.get(id)));
                    if (countersMap2 != null) {
                        sb.append(",").append(notNull(countersMap2.get(id)));
                    }
                    if (countersMap3 != null) {
                        sb.append(",").append(notNull(countersMap3.get(id)));
                    }
                    sb.append("): ");
                    SolrFileEntity cf = claFileMap.get(id);
                    Long contentId = FileCatService.getContentId(cf);
                    PvAnalysisData pvAnalysisData = pvAnalysisDataMap.get(contentId);
                    String details = getPvAnalysisSimpleSummary(cf, pvAnalysisData);
                    sb.append(details).append("<br/>\n");
                    if (prevPvAnalysisData != null && showSimValues) {
                        FileType fileType = FileType.valueOf(cf.getType());
                        if (fileType == FileType.PDF || fileType == FileType.WORD) {
                            // Compare current and prev and show sim vector
                            final WordFilesSimilarity wfs = new WordFilesSimilarity(
                                    prevPvAnalysisData.getDefaultPvCollection().getCollections(),
                                    pvAnalysisData.getDefaultPvCollection().getCollections());
                            // Dump similarities ...
                            if (showLegend) {
                                showLegend = false;
                                sb.append(" <span style='margin-left:2em;'>PVT Legend: ");
                                wfs.getSimilaritiesKeySet().forEach(pvName -> sb.append(" ").append(pvName.toString()));
                                sb.append("</span><br/>\n<br/>\n");
                            }
                            sb.append(" <span style='margin-left:2em;'>simRates: ");
                            wfs.getSimilaritiesKeySet().forEach(pvName -> {
                                float v = wfs.getSimilarityValue(pvName);
                                sb.append(" ").append(pvName.toString().substring(0, 4)).append("(");
                                if (v > 0.6f) {
                                    sb.append("<span style='color:red;'>");
                                }
                                sb.append(((int) (v * 100f)) / 100f);
                                if (v > 0.6f) {
                                    sb.append("</span>");
                                }
                                sb.append(")");
                            });
                            sb.append("</span><br/>\n");
                        } else if (fileType == FileType.EXCEL) {
                            Map<String, ClaSuperCollection<Long>> prevPartsCollections = prevPvAnalysisData.getPvCollections().getCollections();
                            Map<String, ClaSuperCollection<Long>> currPartsCollections = pvAnalysisData.getPvCollections().getCollections();
                            sb.append(" <span style=\"margin-left:2em;\">simRates: ").append(prevPartsCollections.keySet().size()).append(" x ").append(currPartsCollections.keySet().size()).append(" sheets</span><br/>\n");
                            for (String prevPart : prevPartsCollections.keySet()) {
                                for (String currPart : currPartsCollections.keySet()) {
                                    final ExcelSheetSimilarity ess = new ExcelSheetSimilarity(prevPartsCollections.get(prevPart).getCollections(),
                                            currPartsCollections.get(currPart).getCollections());
                                    if (showLegend) {
                                        showLegend = false;
                                        sb.append(" <span style='margin-left:2em;'>PVT Legend: ");
                                        ess.getSimilaritiesKeySet().forEach(pvName -> sb.append(" ").append(pvName.toString()));
                                        sb.append("</span><br/>\n<br/>\n");
                                    }
                                    sb.append(" <span style=\"margin-left:3em;\">").append(prevPart).append(" = ").append(currPart).append(":");
                                    // Dump similarities ...
                                    ess.getSimilaritiesKeySet().forEach(pvName -> {
                                        sb.append(" ").append(pvName.toString().substring(0, 4)).append("(");
                                        float v = ess.getSimilarityValue(pvName);
                                        if (v > 0.6f) {
                                            sb.append("<span style='color:red;'>");
                                        }
                                        sb.append(ess.getSimilarityVectorString(pvName));
                                        if (v > 0.6f) {
                                            sb.append("</span>");
                                        }
                                        sb.append(")");
                                    });
                                    sb.append("</span><br/>\n");
                                }
                            }
                        }
                    }
                    prevPvAnalysisData = pvAnalysisData;
                }
                pvAnalysisDataList.clear();
                pvAnalysisDataMap.clear();
            }
        } catch (Exception e) {
            logger.error("Error during dumpIngestDataForFile({}). {}", claFileIds.size(), e.getMessage());
            sb.append("<br/>\nException<br/>\n");
        }
        return sb.toString();
    }

    private int notNull(Integer val) {
        return val == null ? 0 : val;
    }

    private String getPvAnalysisTitles() {
        StringBuilder sb = new StringBuilder();
        sb.append("file_id");
        sb.append(",").append("part");
        for (PVType pvType : PVType.values()) {
            String name = pvType.name();
            sb.append(",").append(name).append("-gc");
            sb.append(",").append(name).append("-ic");
            sb.append(",").append(name).append("-tc");
            sb.append(",").append(name).append("-sc");
        }
        sb.append("file_path");
        sb.append("file_name");
        sb.append("\n");
        return sb.toString();
    }

    private String getPvAnalysisSimpleSummary(SolrFileEntity claFile, PvAnalysisData pvAnalysisData) {
        StringBuilder sb = new StringBuilder();
        sb.append(claFile.getId());
        if (pvAnalysisData == null) {
            sb.append(" -null-");
        } else {
            Map<String, ClaSuperCollection<Long>> collections = pvAnalysisData.getPvCollections().getCollections();
            for (Map.Entry<String, ClaSuperCollection<Long>> stringClaSuperCollectionEntry : collections.entrySet()) {
                String part = stringClaSuperCollectionEntry.getKey();
                ClaSuperCollection<Long> value = stringClaSuperCollectionEntry.getValue();
                sb.append(", ").append(part).append(":");
                if (value == null) {
                    value = new ClaSuperCollection<>();
                }
                for (PVType pvType : PVType.values()) {
                    String name = pvType.name();
                    ClaHistogramCollection<Long> longClaHistogramCollection = value.getCollections().get(name);
                    if (longClaHistogramCollection != null) {
                        sb.append(" ").append(pvType.toString().substring(0, 4)).append("(");
                        sb.append(longClaHistogramCollection.getItemsCount());
                        sb.append(",").append(longClaHistogramCollection.getGlobalCount());
                        sb.append(")");
                    }
                }
            }
        }
        sb.append(" ").append(claFile.getFullName());
        return sb.toString();
    }

    private String getPvAnalysisSummary(ClaFile claFile, PvAnalysisData pvAnalysisData, Integer maturityFilter) {
        StringBuilder sb = new StringBuilder();
        Map<String, ClaSuperCollection<Long>> collections = pvAnalysisData.getPvCollections().getCollections();
        int maxMaturity = 0;
        for (Map.Entry<String, ClaSuperCollection<Long>> stringClaSuperCollectionEntry : collections.entrySet()) {
            String part = stringClaSuperCollectionEntry.getKey();
            ClaSuperCollection<Long> value = stringClaSuperCollectionEntry.getValue();
            int partMaturity = maturityLevelService.getPartMaturityLevel(value);
            if (maxMaturity < partMaturity) {
                maxMaturity = partMaturity;
            }
            sb.append(claFile.getId());
            sb.append(",").append(part).append(":ml").append(partMaturity);
            if (value == null) {
                value = new ClaSuperCollection<>();
            }
            for (PVType pvType : PVType.values()) {
                String name = pvType.name();
                ClaHistogramCollection<Long> longClaHistogramCollection = value.getCollections().get(name);
                appendPvTypeSummary(longClaHistogramCollection, sb);
            }
            sb.append(",\"").append(claFile.getFullPath()).append("\"");
            sb.append(",\"").append(claFile.getBaseName()).append("\"");
            sb.append("\n");
        }
        if (maturityFilter != null && maxMaturity != maturityFilter) {
            return null;
        }
        return
                collections.size() > 1 ?
                        ("ml:" + maxMaturity + ", " + sb.toString()) :
                        sb.toString();
    }

    private void appendPvTypeSummary(ClaHistogramCollection<Long> pvEntry, StringBuilder sb) {
        if (pvEntry == null) {
            sb.append(",,,,");
        } else {
            sb.append(",").append(pvEntry.getGlobalCount());
            sb.append(",").append(pvEntry.getItemsCount());
            sb.append(",").append(pvEntry.getTotalCount());
            sb.append(",").append(pvEntry.getSkippedCount());
        }
    }

    /*
     * delete join group - creates the operation in db
     * will be handled in @see com.cla.filecluster.service.group.DeleteJoinedGroupOperation
     *
     * @param userGroupId - parent group
     */
    public void deleteJoinedGroup(String userGroupId) {
        FileGroup group = groupsService.findById(userGroupId);
        if (group == null) {
            throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.DELETE_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(DeleteJoinedGroupOperation.USER_GROUP_ID_PARAM, userGroupId);
        data.setUserOperation(true);
        data.setDescription(messageHandler.getMessage("operation-description.delete-joined-group",
                Lists.newArrayList(group.getNameForReport())));
        scheduledOperationService.createNewOperation(data);
    }

    /*
     * delete join group step 1
     * called from @see com.cla.filecluster.service.group.DeleteJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @AutoAuthenticate
    public void deleteJoinedGroupAddAssociationsToChildGroups(Map<String, String> opData) {
        String userGroupId = opData.get(DeleteJoinedGroupOperation.USER_GROUP_ID_PARAM);
        GroupDetailsDto groupDetailsDto = findGroupById(userGroupId);
        GroupDto userGroupDto = groupDetailsDto.getGroupDto();
        if (!userGroupDto.getGroupType().equals(GroupType.USER_GROUP)) {
            throw new RuntimeException(messageHandler.getMessage("group.delete.type.failure"));
        }
        addAssociationsToChildGroups(userGroupId, groupDetailsDto);
        fixDocTypesForChildGroups(userGroupId, groupDetailsDto);
    }

    /*
     * delete join group step 2
     * called from @see com.cla.filecluster.service.group.DeleteJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @AutoAuthenticate
    public void deleteJoinedGroupUpdFilesOfChildren(Map<String, String> opData) {
        String userGroupId = opData.get(DeleteJoinedGroupOperation.USER_GROUP_ID_PARAM);
        GroupDetailsDto groupDetailsDto = findGroupById(userGroupId);
        for (GroupDto subGroupDto : groupDetailsDto.getChildrenGroups()) {
            logger.debug("Set analysis group to be user group for files under analysis group {}", subGroupDto.getId());
            updateUserGroupForFilesAndContent(subGroupDto.getId(), subGroupDto.getId());
            // TODO: better do it in SOLR plugin - copy groupId onto userGroupId for groupIds in (...)
            // TODO: this way we can make one call to SOLR
            groupsService.updateUserGroupsCatFile(subGroupDto.getId(), Sets.newHashSet(subGroupDto.getId()), true);
        }
    }

    /*
     * delete join group step 3
     * called from @see com.cla.filecluster.service.group.DeleteJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    public void deleteJoinedGroupChildrenAndDelete(Map<String, String> opData) {
        String userGroupId = opData.get(DeleteJoinedGroupOperation.USER_GROUP_ID_PARAM);
        GroupDetailsDto groupDetailsDto = findGroupById(userGroupId);
        logger.info("Delete user group {} - disconnecting sub-groups", groupDetailsDto);
        for (GroupDto subGroupDto : groupDetailsDto.getChildrenGroups()) {
            groupsService.removeSubGroupFromUserGroup(subGroupDto.getId(), userGroupId);
        }
        logger.info("Delete user group {}", groupDetailsDto.getGroupDto());
        groupsService.deleteUserGroup(userGroupId);
    }

    private void addAssociationsToChildGroups(String userGroupId, GroupDetailsDto groupDetailsDto) {
        templateUtils.doInTransaction(() -> {
            AssociableEntity lastEntity = null;
            FileGroup userGroup = fileGroupService.getById(userGroupId);
            AssociationRelatedData assocData = associationsService.getAssociationRelatedData(userGroup);
            List<AssociationEntity> userGroupAssociations = assocData.getAssociations();
            if (userGroupAssociations != null) {
                for (AssociationEntity association : userGroupAssociations) {
                    try {
                        BasicScope scope = association.getAssociationScopeId() == null ? null :
                                assocData.getScopes().get(association.getAssociationScopeId());

                        lastEntity = associationsService.getEntityForAssociation(association);
                        for (GroupDto subGroupDto : groupDetailsDto.getChildrenGroups()) {
                            groupsService.addAssociableEntityToGroup(subGroupDto.getId(), lastEntity, scope, null);
                        }
                    } catch (RuntimeException e) { // The catch is here because I'm not sure of the solution stability yet
                        logger.error("Failed to add association ({}) to sub groups after deleting a joined group", lastEntity, e);
                    }
                }
            }
        });
    }

    private void fixDocTypesForChildGroups(String userGroupId, GroupDetailsDto groupDetailsDto) {
        templateUtils.doInTransaction(() -> {
            FileGroup userGroup = fileGroupService.getById(userGroupId);

            List<AssociationEntity> groupAssociation = AssociationsService.convertFromAssociationEntity(
                    userGroup.getId(), userGroup.getAssociations(), AssociationType.GROUP);
            Collection<AssociationEntity> userGroupDocTypeAssociations = AssociationsService.getDocTypeAssociation(groupAssociation);

            if (userGroupDocTypeAssociations != null && !userGroupDocTypeAssociations.isEmpty()) {
                for (AssociationEntity association : userGroupDocTypeAssociations) {
                    groupsService.applyDocTypeChangeForGroup(userGroupId, association, true);
                }
                for (GroupDto subGroupDto : groupDetailsDto.getChildrenGroups()) {
                    FileGroup subGroup = fileGroupService.getById(subGroupDto.getId());

                    List<AssociationEntity> subGroupAssociation = AssociationsService.convertFromAssociationEntity(
                            subGroup.getId(), userGroup.getAssociations(), AssociationType.GROUP);
                    Collection<AssociationEntity> subGroupDocTypeAssociations = AssociationsService.getDocTypeAssociation(subGroupAssociation);

                    if (subGroupDocTypeAssociations != null) {
                        subGroupDocTypeAssociations.forEach(association -> {
                            groupsService.applyDocTypeChangeForChildGroup(subGroup.getId(), association, false);
                        });
                    }
                }
            }
        });
    }

    /**
     * Join several groups into a new user group.
     * creates the operation to be performed unsync in
     * @see com.cla.filecluster.service.operation.CreateJoinedGroupOperation
     * This operation affects both MySQL and Solr, the MySQL update happens in several transactions.
     * @param newJoinedGroupDto DTO containing name of the new group and IDs of the groups to be joined
     */
    @Transactional
    public GroupDetailsDto createJoinedGroup(NewJoinedGroupDto newJoinedGroupDto) {
        String groupName = newJoinedGroupDto.getGroupName();
        List<String> childrenGroupIds = newJoinedGroupDto.getChildrenGroupIds();

        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.CREATE_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(CreateJoinedGroupOperation.GROUP_NAME_PARAM, groupName);
        data.getOpData().put(CreateJoinedGroupOperation.CHILD_GROUPS_PARAM, String.join(",", childrenGroupIds));
        data.setUserOperation(true);

        GroupDto result = createJoinedGroupAndAddChildrenDB(data.getOpData());

        data.setDescription(messageHandler.getMessage("operation-description.create-joined-group",
                Lists.newArrayList(result.getGroupName())));

        scheduledOperationService.createNewOperation(data);

        return findGroupById(result.getId());
    }

    /*
     * lock for create join group
     * called from @see com.cla.filecluster.service.group.CreateJoinedGroupOperation
     *
     * @param childrenGroupIds - the child groups to lock
     */
    public void lockGroupsForJoin(List<String> childrenGroupIds) {
        // make sure non of the child groups are currently being joined in another thread
        synchronized (joinInProgress) {
            if (Collections.disjoint(joinInProgress, childrenGroupIds) ) {
                joinInProgress.addAll(childrenGroupIds);
            }
            else{
                logger.warn("Concurrent group joins. Requested: {}, already being joined: {}.",
                        childrenGroupIds, Joiner.on(", ").join(joinInProgress));
                throw new BadRequestException(messageHandler.getMessage("group.operation.lock"), BadRequestType.GROUP_LOCKED);
            }
        }
    }

    /*
     * unlock for create join group
     * called from @see com.cla.filecluster.service.group.CreateJoinedGroupOperation
     *
     * @param childrenGroupIds - the child groups to unlock
     */
    public void unLockGroupsForJoin(List<String> childrenGroupIds) {
        synchronized (joinInProgress) {
            joinInProgress.removeAll(childrenGroupIds);
        }
    }

    /*
     * create join group step 1
     * called from @see com.cla.filecluster.service.group.CreateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    public GroupDto createJoinedGroupAndAddChildrenDB(Map<String, String> opData) {

        String groupName = opData.get(CreateJoinedGroupOperation.GROUP_NAME_PARAM);
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CreateJoinedGroupOperation.CHILD_GROUPS_PARAM).split(",")));

        List<String> updatedChildrenGroupIds = Lists.newArrayList();

        logger.debug("Creating joined group {} with name {}", childrenGroupIds, groupName);
        // create new group in separate transaction,
        // copy associations from sub groups
        // add sub groups to the new user group in MySQL
        GroupDto newUserGroup = templateUtils.doInTransaction(() -> {

            List<FileGroup> childGroupsUnfiltered = fileGroupRepository.getByIds(childrenGroupIds);
            List<FileGroup> childGroups = childGroupsUnfiltered.stream()
                    .filter(group -> Objects.isNull(group.getParentGroupId()))
                    .collect(Collectors.toList());

            updatedChildrenGroupIds.addAll(childGroups.stream().map(FileGroup::getId).collect(Collectors.toList()));

            if (childGroups.size() == 0){
                throw new BadRequestException(messageHandler.getMessage("group.join.already"), BadRequestType.UNSUPPORTED_VALUE);
            }

            FileGroup firstGroup = childGroups.get(0);
            GroupDto userGroup = groupsService.createNewUserGroup(groupName, firstGroup);

            String subGroupIdsString = childrenGroupIds.stream().collect(Collectors.joining());
            logger.info("Join groups {} under new group {}", subGroupIdsString, userGroup);

            for (FileGroup subGroup : childGroups) {
                if (GroupType.USER_GROUP.equals(subGroup.getGroupType())) {
                    updatedChildrenGroupIds.remove(subGroup.getId());
                    List<FileGroup> subGroups = groupsService.findSubGroups(subGroup.getId());
                    subGroups.forEach(f -> updatedChildrenGroupIds.add(f.getId()));
                    //groupsService.copyGroupAssociations(subGroup.getId(), userGroup.getId());
                }
                FileGroup fileGroupEntity = groupsService.addSubGroupToUserGroup(subGroup.getId(), userGroup.getId());

                userGroup = groupsService.convertGroupToDto(fileGroupEntity, null,null, false, null, null);
            }
            return userGroup;
        });

        opData.put(CreateJoinedGroupOperation.NEW_GROUP_ID_PARAM, newUserGroup.getId());
        opData.put(CreateJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM, String.join(",", updatedChildrenGroupIds));

        return newUserGroup;
    }

    /*
     * create join group step 2
     * called from @see com.cla.filecluster.service.group.CreateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @AutoAuthenticate
    public void createJoinedGroupStepUpdFilesDB(Map<String, String> opData) {

        String newUserGroupId = opData.get(CreateJoinedGroupOperation.NEW_GROUP_ID_PARAM);
        List<String> updatedChildrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CreateJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM)
                        .split(",")));

        // set new user group to files in MySQL, use paging in separate transactions
        logger.debug("Set new group {} to files under joined groups", newUserGroupId);
        for (String childGroupId : updatedChildrenGroupIds) {
            logger.debug("Set userGroup {} to files under analysis group {}", newUserGroupId, childGroupId);
            updateUserGroupForFilesAndContent(newUserGroupId, childGroupId);
            groupsService.updateGroupFinishedMarkingFiles(childGroupId);
        }
    }

    /*
     * create join group step 3
     * called from @see com.cla.filecluster.service.group.CreateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @AutoAuthenticate
    public void createJoinedGroupStepUpdSolrAndParent(Map<String, String> opData) {

        String newUserGroupId = opData.get(CreateJoinedGroupOperation.NEW_GROUP_ID_PARAM);
        List<String> updatedChildrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CreateJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM).split(",")));

        // update userGroupId field in cat files in SOLR
        groupsService.updateUserGroupsCatFile(newUserGroupId, updatedChildrenGroupIds, true);

        fileGroupService.updateUserGroupNumOfFilesAndFirstMember(newUserGroupId);
        logger.debug("Update number of files for user group {}", newUserGroupId);
    }



    /*
     * update join group - creates the operation in db
     * will be handled in @see com.cla.filecluster.service.group.UpdateJoinedGroupOperation
     *
     * @param userGroupId - parent group
     * @param newJoinedGroupDto - contains child groups
     */
    public void updateJoinedGroup(String userGroupId, NewJoinedGroupDto newJoinedGroupDto) {

        FileGroup group = groupsService.findById(userGroupId);
        if (group == null) {
            throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        List<String> childrenGroupIds = newJoinedGroupDto.getChildrenGroupIds();
        childrenGroupIds.remove(userGroupId);

        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.UPDATE_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(UpdateJoinedGroupOperation.USER_GROUP_ID_PARAM, userGroupId);
        data.getOpData().put(UpdateJoinedGroupOperation.CHILD_GROUPS_PARAM, String.join(",", childrenGroupIds));
        data.setUserOperation(true);
        data.setDescription(messageHandler.getMessage("operation-description.update-joined-group",
                Lists.newArrayList(group.getNameForReport())));
        scheduledOperationService.createNewOperation(data);
    }

    /*
     * update join group step 1
     * called from @see com.cla.filecluster.service.group.UpdateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @Transactional
    @AutoAuthenticate
    public void updateJoinedGroupRemoveChildren(Map<String, String> opData) {
        String userGroupId = opData.get(UpdateJoinedGroupOperation.USER_GROUP_ID_PARAM);

        GroupDetailsDto groupDetailsDto = updateUserGroup(userGroupId);

        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(UpdateJoinedGroupOperation.CHILD_GROUPS_PARAM).split(",")));

        //Remove groups if needed
        removeJoinedGroupsIfNeeded(groupDetailsDto, childrenGroupIds);
    }

    /*
     * update join group step 2
     * called from @see com.cla.filecluster.service.group.UpdateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @Transactional
    @AutoAuthenticate
    public void updateJoinedGroupAddChildren(Map<String, String> opData) {
        String userGroupId = opData.get(UpdateJoinedGroupOperation.USER_GROUP_ID_PARAM);

        GroupDetailsDto groupDetailsDto = updateUserGroup(userGroupId);

        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(UpdateJoinedGroupOperation.CHILD_GROUPS_PARAM).split(",")));

        addGroupsIfNeeded(userGroupId, childrenGroupIds, groupDetailsDto, opData);
    }

    /*
     * update join group step 3
     * called from @see com.cla.filecluster.service.group.UpdateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @Transactional
    @AutoAuthenticate
    public void updateJoinedGroupUpdateFiles(Map<String, String> opData) {
        String userGroupId = opData.get(UpdateJoinedGroupOperation.USER_GROUP_ID_PARAM);
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(AddToJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM).split(",")));
        addGroupsIfNeededUpdFilesAndContents(userGroupId, childrenGroupIds);
    }

    /*
     * update join group step 4
     * called from @see com.cla.filecluster.service.group.UpdateJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @Transactional
    public void updateJoinedGroupUpdateParent(Map<String, String> opData) {
        String userGroupId = opData.get(UpdateJoinedGroupOperation.USER_GROUP_ID_PARAM);
        fileGroupService.updateUserGroupNumOfFilesAndFirstMember(userGroupId);
    }

    private GroupDetailsDto updateUserGroup(String userGroupId) {
        GroupDetailsDto groupDetailsDto = findGroupById(userGroupId);
        GroupDto userGroupDto = groupDetailsDto.getGroupDto();
        if (!userGroupDto.getGroupType().equals(GroupType.USER_GROUP)) {
            throw new RuntimeException(messageHandler.getMessage("group.update.type.failure"));
        }
        logger.info("Update joined group {}", userGroupId);
        return groupDetailsDto;
    }

    private void removeJoinedGroupsIfNeeded(GroupDetailsDto groupDetailsDto, List<String> childrenGroupIds) {
        Set<String> updatedGroupIds = Sets.newHashSet(childrenGroupIds);
        String userGroupId = groupDetailsDto.getGroupDto().getId();
        for (GroupDto childGroupDto : groupDetailsDto.getChildrenGroups()) {
            if (updatedGroupIds.contains(childGroupDto.getId())) {
                continue; // Group was not removed
            }
            if (childGroupDto.getId().equals(userGroupId)) {
                continue; //We can't remove the joined group from itself
            }
            groupsService.removeSubGroupFromUserGroup(childGroupDto.getId(), userGroupId);
            logger.debug("Set analysis group to be user group for files under analysis group {}", childGroupDto.getId());
            final FileGroup analysisGroup = fileGroupService.getById(childGroupDto.getId());
            updateUserGroupForFilesAndContent(analysisGroup.getId(), childGroupDto.getId());
            // TODO: better do it in SOLR plugin - copy groupId onto userGroupId for groupIds in (...)
            // TODO: this way we can make one call to SOLR
            groupsService.updateUserGroupsCatFile(analysisGroup.getId(), Sets.newHashSet(childGroupDto.getId()), true);
        }
    }

    private void addGroupsIfNeeded(String userGroupId, List<String> childrenGroupIds, GroupDetailsDto groupDetailsDto, Map<String, String> opData) {
        logger.info("Add groups if needed ({})", childrenGroupIds);
        boolean childAdded = false;
        Set<String> existingChildrenIds = groupDetailsDto.getChildrenGroups().stream()
                .map(GroupDto::getId)
                .collect(Collectors.toSet());
        FileGroup userGroup = groupsService.findById(userGroupId);
        List<String> existingGroupAssociations = userGroup.getAssociations();
        List<String> updatedChildrenGroupIds = new ArrayList<>(childrenGroupIds);
        for (String childGroupId : childrenGroupIds) {
            if (existingChildrenIds.contains(childGroupId)) {
                continue; // Group already added. Nothing to do
            }
            if (userGroupId.equals(childGroupId)) {
                continue; // we can't add a group to itself
            }
            FileGroup subGroup = fileGroupService.getById(childGroupId);
            if (GroupType.USER_GROUP.equals(subGroup.getGroupType())) {
                updatedChildrenGroupIds.remove(childGroupId);
                List<FileGroup> subGroups = groupsService.findSubGroups(childGroupId);
                subGroups.forEach(f -> updatedChildrenGroupIds.add(f.getId()));
                groupsService.copyGroupAssociations(subGroup.getId(), userGroupId);
            }
            childAdded = true;
            groupsService.addSubGroupToUserGroup(childGroupId, userGroupId);
        }
        opData.put(AddToJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM, String.join(",", updatedChildrenGroupIds));

        if (childAdded && existingGroupAssociations != null && !existingGroupAssociations.isEmpty()) {
            existingGroupAssociations.forEach(assoc ->
                    associationsService.createGroupApplyOperation(userGroupId, assoc, SolrFieldOp.ADD, null));
        }
    }

    private void addGroupsIfNeededUpdFilesAndContents(String userGroupId, List<String> updatedChildrenGroupIds) {
        for (String childGroupId : updatedChildrenGroupIds) {
            logger.debug("Set userGroup {} to files under analysis group {}", userGroupId, childGroupId);
            updateUserGroupForFilesAndContent(userGroupId, childGroupId);
            groupsService.updateUserGroupsCatFile(userGroupId,  Sets.newHashSet(childGroupId), true);
        }
    }

    private void updateUserGroupForFilesAndContent(String userGroupId, String childGroupId) {

        groupsService.updateUserGroupsCatFile(userGroupId, Collections.singletonList(childGroupId), true);

        contentMetadataService.setNewUserGroupForAnalysisGroup(childGroupId, userGroupId, true);
    }

    /*
     * add to join group - creates the operation in db
     * will be handled in @see com.cla.filecluster.service.group.AddToJoinedGroupOperation
     *
     * @param userGroupId - parent group
     * @param childrenGroupIds - child groups
     */
    @Transactional
    public GroupDetailsDto addGroupsToJoinedGroup(String userGroupId, List<String> childrenGroupIds) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.ADD_TO_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(AddToJoinedGroupOperation.USER_GROUP_ID_PARAM, userGroupId);
        data.getOpData().put(AddToJoinedGroupOperation.CHILD_GROUPS_PARAM, String.join(",", childrenGroupIds));
        data.setUserOperation(true);

        childrenGroupIds.remove(userGroupId);
        GroupDetailsDto groupDetailsDto = updateUserGroup(userGroupId);
        addGroupsIfNeeded(userGroupId, childrenGroupIds, groupDetailsDto, data.getOpData());

        data.setDescription(messageHandler.getMessage("operation-description.add-joined-group",
                Lists.newArrayList(groupDetailsDto.getGroupDto().getGroupName())));

        scheduledOperationService.createNewOperation(data);
        return findGroupById(userGroupId);
    }

    /*
     * add to join group step 1
     * called from @see com.cla.filecluster.service.group.AddToJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @Transactional
    @AutoAuthenticate
    public void addGroupsToJoinedGroupUpdateFiles(Map<String, String> opData) {
        String userGroupId = opData.get(AddToJoinedGroupOperation.USER_GROUP_ID_PARAM);
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(AddToJoinedGroupOperation.UPDATED_CHILD_GROUPS_PARAM).split(",")));
        addGroupsIfNeededUpdFilesAndContents(userGroupId, childrenGroupIds);
    }

    /*
     * add to join group step 2
     * called from @see com.cla.filecluster.service.group.AddToJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    public void addGroupsToJoinedGroupUpdateParentGroup(Map<String, String> opData) {
        String userGroupId = opData.get(AddToJoinedGroupOperation.USER_GROUP_ID_PARAM);
        fileGroupService.updateNumOfFilesForUserGroup(userGroupId);
    }

    /*
     * remove from join group - creates the operation in db
     * will be handled in @see com.cla.filecluster.service.group.DelFromJoinedGroupOperation
     *
     * @param userGroupId - parent group
     * @param removedGroupId - child group to remove
     */
    @Transactional
    public GroupDetailsDto removeGroupFromJoinedGroup(String userGroupId, String removedGroupId) {

        if (userGroupId.equals(removedGroupId)) {
            logger.warn("Unable to remove a user group from itself");
            return findGroupById(userGroupId);
        }
        GroupDetailsDto group = updateUserGroup(userGroupId);

        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.DEL_FROM_JOINED_GROUP);
        data.setOpData(new HashMap<>());
        data.getOpData().put(DelFromJoinedGroupOperation.USER_GROUP_ID_PARAM, userGroupId);
        data.getOpData().put(DelFromJoinedGroupOperation.CHILD_GROUP_ID_PARAM, removedGroupId);
        data.setUserOperation(true);

        groupsService.removeSubGroupFromUserGroup(removedGroupId, userGroupId);

        data.setDescription(messageHandler.getMessage("operation-description.remove-joined-group",
                Lists.newArrayList(group.getGroupDto().getGroupName())));

        scheduledOperationService.createNewOperation(data);
        return findGroupById(userGroupId);
    }

    /*
     * remove from join group step 1
     * called from @see com.cla.filecluster.service.group.DelFromJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @AutoAuthenticate
    public void removeGroupFromJoinedGroupUpdFilesDB(Map<String, String> opData) {
        String userGroupId = opData.get(DelFromJoinedGroupOperation.USER_GROUP_ID_PARAM);
        String removedGroupId = opData.get(DelFromJoinedGroupOperation.CHILD_GROUP_ID_PARAM);
        fixDocTypesForChildGroupRemoved(userGroupId, removedGroupId);

        logger.debug("Set analysis group to be user group for files under analysis group {}", removedGroupId);
        updateUserGroupForFilesAndContent(removedGroupId, removedGroupId);
    }

    private void fixDocTypesForChildGroupRemoved(String userGroupId, String removedGroupId) {
        templateUtils.doInTransaction(() -> {
            FileGroup userGroup = fileGroupService.getById(userGroupId);

            List<AssociationEntity> groupAssociation = AssociationsService.convertFromAssociationEntity(
                    userGroup.getId(), userGroup.getAssociations(), AssociationType.GROUP);
            Collection<AssociationEntity> userGroupDocTypeAssociations = AssociationsService.getDocTypeAssociation(groupAssociation);

            if (userGroupDocTypeAssociations != null && !userGroupDocTypeAssociations.isEmpty()) {
                for (AssociationEntity association : userGroupDocTypeAssociations) {
                    groupsService.applyDocTypeChangeForChildGroup(removedGroupId, association, true);
                }
                FileGroup childRemoved =  fileGroupService.getById(removedGroupId);

                List<AssociationEntity> childGroupAssociation = AssociationsService.convertFromAssociationEntity(
                        childRemoved.getId(), userGroup.getAssociations(), AssociationType.GROUP);
                Collection<AssociationEntity> childGroupDocTypeAssociations = AssociationsService.getDocTypeAssociation(childGroupAssociation);

                if (childGroupDocTypeAssociations != null) {
                    childGroupDocTypeAssociations.forEach(association -> {
                        groupsService.applyDocTypeChangeForChildGroup(removedGroupId, association, false);
                    });
                }
            }
        });
    }

    /*
     * remove from join group step 2
     * called from @see com.cla.filecluster.service.group.DelFromJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @AutoAuthenticate
    public void removeGroupFromJoinedGroupUpdFilesSolr(Map<String, String> opData) {
        String removedGroupId = opData.get(DelFromJoinedGroupOperation.CHILD_GROUP_ID_PARAM);

        // update in Solr (set user gtoup to be the same as analysis group
        groupsService.updateUserGroupsCatFile(removedGroupId, Sets.newHashSet(removedGroupId), true);
    }

    /*
     * remove from join group step 3
     * called from @see com.cla.filecluster.service.group.DelFromJoinedGroupOperation
     *
     * @param opData - the parameters for the operation
     */
    @Transactional
    @AutoAuthenticate
    public void removeGroupFromJoinedGroupUpdParent(Map<String, String> opData) {
        String userGroupId = opData.get(DelFromJoinedGroupOperation.USER_GROUP_ID_PARAM);
        fileGroupService.updateGroupFirstMemberContentAndFile(userGroupId);
        fileGroupService.updateNumOfFilesForUserGroup(userGroupId);
    }

    @Transactional
    public void removeFileFromUserGroup(long fileId) {
        SolrFileEntity file = fileCatService.getFileEntity(fileId);

        int count = contentMetadataService.getContentGroupCount(file.getUserGroupId());

        if (count == 1) {
            // validate removed file isn't the last one from its analysis group.
            // Currently removing last (from its analysis-group) file from a user group isn't supported.
            logger.error("Attempted to remove last file in user group. file id: {}, user group id: {}", file.getFileId(), file.getUserGroupId());
            throw new NotSupportedException(messageHandler.getMessage("group.file.detach.failure.last", Lists.newArrayList(file.getBaseName())));
        }

        FileGroup analysisGroup = groupsService.findById(file.getAnalysisGroupId());
        SolrFileEntity claFile = fileCatService.getFileEntity(file.getFileId());
        String newGroupName = (Strings.isNullOrEmpty(analysisGroup.getGeneratedName()) ? claFile.getBaseName() : analysisGroup.getGeneratedName());
        FileGroup newUserGroup = groupsService.createNewUserGroup(newGroupName, claFile);

        String removedFromUserGroupId = file.getUserGroupId();
        List<SolrFileEntity> filesToBeExcluded;
        filesToBeExcluded = file.getContentId() != null ? fileCatService.findFilesByContentId(FileCatService.getContentId(file)) : Lists.newArrayList(claFile);
        filesToBeExcluded.forEach(fileToBeExcluded -> {
            Optional.ofNullable(file.getContentId()).ifPresent(cmd -> {
                contentMetadataService.updateUserGroup(Long.parseLong(cmd), newUserGroup);
            });
            FileGroup userGroupFileExcluded = groupsService.findById(fileToBeExcluded.getUserGroupId());
            logger.info("exclude file {} from user group {}", fileToBeExcluded.getFileId(), userGroupFileExcluded.getName());
            fileCatService.updateUserGroupId(fileToBeExcluded.getFileId(), FileCatService.getContentId(fileToBeExcluded), newUserGroup.getId(), true);
        });

        fileGroupService.updateUserGroupNumOfFilesAndFirstMember(removedFromUserGroupId);
        fileGroupService.updateUserGroupNumOfFilesAndFirstMember(newUserGroup.getId());
    }

    private Set<GroupDto> findSubGroups(String userGroupId) {
        List<FileGroup> groups = groupsService.findSubGroups(userGroupId);
        groups = groups.stream().filter(g -> !g.isDeleted()).collect(Collectors.toList());
        AssociationRelatedData groupsAssociations = associationsService.getAssociationRelatedDataGroups(groups);

        return groupsService.convertGroupsToDto(groups, null, groupsAssociations.getPerGroupDocTypes(),
                groupsAssociations.getPerGroupAssociations(), groupsAssociations.getEntities());
    }

    public GroupDetailsDto getGroupFullDetails(String groupId) {
        GroupDetailsDto groupFullData = findGroupById(groupId);

        if (groupFullData.getGroupDto().getGroupType() == GroupType.SUB_GROUP) {
            List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(groupFullData.getGroupDto().getRawAnalysisGroupId());
            AssociationRelatedData groupsAssociations = associationsService.getAssociationRelatedDataGroups(subGroupsList);

            Set<GroupDto> siblings = groupsService.convertGroupsToDto(subGroupsList, null,
                    groupsAssociations.getPerGroupDocTypes(), groupsAssociations.getPerGroupAssociations(), groupsAssociations.getEntities());
            groupFullData.setSiblingGroups(siblings);
        }

        groupFullData.getGroupDto().setGroupProcessing(isGroupProcessing(groupFullData.getGroupDto(), scheduledOperationService.getGroupsWithOperations()));

        return groupFullData;
    }

    public GroupDetailsDto findGroupById(String groupId) {
        return templateUtils.doInTransaction(() -> {
            FileGroup res = fileGroupService.getById(groupId);
            if (res == null) {
                throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
            }
            List<AssociationEntity> groupAssociations = AssociationsService.convertFromAssociationEntity(
                    res.getId(), res.getAssociations(), AssociationType.GROUP);
            List<AssociationEntity> groupDocTypeAssociations = AssociationsService.getDocTypeAssociation(groupAssociations);

            FileGroup parent = res.getParentGroupId() == null ? null : fileGroupService.getById(res.getParentGroupId());
            GroupDto groupDto = groupsService.convertGroupToDto(res, parent,null,true, groupDocTypeAssociations, groupAssociations);
            GroupDetailsDto groupDetailsDto = new GroupDetailsDto();
            groupDetailsDto.setGroupDto(groupDto);
            if (GroupType.USER_GROUP.equals(res.getGroupType())) {
                Set<GroupDto> subGroups = findSubGroups(groupId);
                groupDetailsDto.setChildrenGroups(subGroups);
            }
            return groupDetailsDto;
        }, TransactionDefinition.PROPAGATION_REQUIRED);
    }

    public GroupDetailsDto createJoinedGroupIfNeeded(String candidateGroupId, String groupName, List<String> subGroupsToJoin) {
        //Attach to existing group - get the relevant joined group id
        GroupDetailsDto groupById = findGroupById(candidateGroupId);
        GroupDto groupDto = groupById.getGroupDto();
        if (GroupType.USER_GROUP.equals(groupDto.getGroupType())) {
            //This is a joined group :-) Only attach the new subGroups to it
            return addGroupsToJoinedGroup(groupDto.getId(), subGroupsToJoin);
        } else {
            if (groupDto.getParentGroupId() != null) {
                logger.warn("User asked for a joined group on a group that is already under a joined group ({}). Using the parent group", groupDto.getParentGroupId());
                return addGroupsToJoinedGroup(groupDto.getParentGroupId(), subGroupsToJoin);
            }
            //Create a joined group with this sub-group in it
            if (groupName == null) {
                groupName = groupDto.getGroupName();
            }
            NewJoinedGroupDto newJoinedGroupDto = new NewJoinedGroupDto(groupName);
            List<String> childrenGroupIds = new ArrayList<>();
            childrenGroupIds.add(candidateGroupId);
            childrenGroupIds.addAll(subGroupsToJoin);
            newJoinedGroupDto.setChildrenGroupIds(childrenGroupIds);
            return createJoinedGroup(newJoinedGroupDto);
        }
    }

    public List<GroupDto> listGroupsByIds(List<String> groupIds) {
        List<FileGroup> fileGroups = groupsService.findByIds(groupIds);
        AssociationRelatedData groupsAssociations = associationsService.getAssociationRelatedDataGroups(fileGroups);

        Set<GroupDto> groupDtos = groupsService.convertGroupsToDto(fileGroups, null,
                groupsAssociations.getPerGroupDocTypes(), groupsAssociations.getPerGroupAssociations(), groupsAssociations.getEntities());

        return new ArrayList<>(groupDtos);
    }

    public void refreshGroup(@PathVariable String groupId) {
        groupsService.refreshGroup(groupId);
    }
    public void updateFileGroupParentInSolr(String fileGroupId) {
        FileGroup fg = fileGroupService.getById(fileGroupId);
        if (fg == null) {
            throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (fg.getParentGroupId() != null) {
            groupsService.updateUserGroupsCatFile(fg.getParentGroupId(), Collections.singletonList(fileGroupId), true);
            fileCatService.commitToSolr();
        } else {
            throw new BadRequestException(messageHandler.getMessage("group.parent.missing"), BadRequestType.OTHER);
        }
    }
}
