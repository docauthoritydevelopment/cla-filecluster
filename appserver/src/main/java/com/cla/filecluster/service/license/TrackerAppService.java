package com.cla.filecluster.service.license;

import com.cla.connector.utils.Pair;
import com.cla.filecluster.service.api.SummaryReportsApplicationService;
import com.cla.filecluster.service.crawler.scan.TrackingKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.Optional.ofNullable;

/**
 * Applicative business-context tracker based services
 */
@Service
public class TrackerAppService {


    private static Logger logger = LoggerFactory.getLogger(TrackerAppService.class);

    @Autowired
    private LicenseTrackerService licenseTrackerService;

    @Autowired
    private SummaryReportsApplicationService summaryReportsApplicationService;
    /*
     * TODO: Should verify that mandatory (license related) trackers exists and not-null upon server startup...
     * Otherwise, some activity should be disabled (scan, login, TBD).
     * Should consider upgrade from Electra where script will create the table but can not create the value with the correct signature.
     * Should have special code for this..
     */
    public long getLongCount(String trackerKey) {
        return Optional.ofNullable(licenseTrackerService.getTrackerLongValue(trackerKey))
                .orElse(0L);
    }


    public Long setLongCount(String trackerKey, long count) {
        return licenseTrackerService.setTrackerLongValue(trackerKey, count, false);
    }


    public long incrementLongCount(String trackerKey, long value) {
        return licenseTrackerService.incrementTrackerLongValue(trackerKey, value, false);
    }

    public double getDoubleCount(String trackerKey) {
        return Optional.ofNullable(licenseTrackerService.getTrackerDoubleValue(trackerKey))
                .orElse(0D);
    }

    public long incrementDoubleCount(String trackerKey, long value) {
        return licenseTrackerService.incrementTrackerLongValue(trackerKey, value, false);
    }

    public Double setDoubleCount(String trackerKey, double count) {
        return licenseTrackerService.setTrackerDoubleValue(trackerKey, count, false);
    }


    public long recoverTotalProcessedDataVolume() {
        long val = licenseTrackerService.getTrackerLongValue(TrackingKeys.TOTAL_FILE_VOLUME_KEY);
        if (val > 0) {
            logger.warn("Recovering TotalProcessedDataVolume while value exists ({})", val);
        }
        Pair<Double, Long> res = summaryReportsApplicationService.createAggregatedFileSize(true);
        long aggSizeBytes = ofNullable(res.getKey()).orElse(0d).longValue();
        logger.warn("Recovering TotalProcessedDataVolume setting value to {}", aggSizeBytes);
        licenseTrackerService.setTrackerLongValue(TrackingKeys.TOTAL_FILE_VOLUME_KEY, aggSizeBytes, true);
        return aggSizeBytes;
    }
}
