package com.cla.filecluster.service.export.exporters.settings.media;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface MediaConnectionHeaderFields {
    String NAME = "NAME";
    String MEDIA_TYPE = "MEDIA TYPE";
    String USERNAME = "USERNAME";
    String DOMAIN = "DOMAIN";
    String URL = "URL";
    String TENANT = "TENANT ID";
    String APPLICATION = "APPLICATION ID";
    String CLIENT = "CLIENT ID";
    String RESOURCE = "RESOURCE";
}
