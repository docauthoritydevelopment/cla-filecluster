package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.email.MailAddressDto;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.files.FileMailAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.VIEW_FILES;
import static com.cla.common.domain.dto.security.Authority.Const.VIEW_PERMITTED_FILES;

/**
 * Created by: yael
 * Created on: 2/3/2019
 */
@RestController
@RequestMapping("/api/mail")
public class FileMailApiController {

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private FileMailAppService fileMailAppService;

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/senderAddress/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getSenderAddressFileCounts(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = getRequestAndAudit(params, "sender address");
        return fileMailAppService.getSenderAddressFileCounts(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/recipientAddress/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getRecipientAddressFileCounts(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = getRequestAndAudit(params, "recipient address");
        return fileMailAppService.getRecipientAddressFileCounts(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/senderDomain/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getSenderDomainFileCounts(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = getRequestAndAudit(params, "sender domain");
        return fileMailAppService.getSenderDomainFileCounts(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/recipientDomain/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getRecipientDomainFileCounts(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = getRequestAndAudit(params, "recipient domain");
        return fileMailAppService.getRecipientDomainFileCounts(dataSourceRequest);
    }

    private DataSourceRequest getRequestAndAudit(Map<String, String> params, String auditObjectName) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover " + auditObjectName + " export", AuditAction.VIEW,  MailAddressDto.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover " + auditObjectName, AuditAction.VIEW,  MailAddressDto.class,null, dataSourceRequest);
        }
        return dataSourceRequest;
    }
}
