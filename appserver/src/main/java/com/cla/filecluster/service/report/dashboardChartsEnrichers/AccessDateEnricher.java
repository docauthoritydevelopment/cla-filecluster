package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import org.springframework.stereotype.Component;


@Component
public class AccessDateEnricher implements PeriodTypeDataEnricher {

    @Override
    public String getEnricherType() {
        return CatFileFieldType.LAST_ACCESS.getSolrName();
    }

}
