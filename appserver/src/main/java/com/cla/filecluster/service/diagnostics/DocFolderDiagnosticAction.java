package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.Range;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class DocFolderDiagnosticAction implements EntityDiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(DocFolderDiagnosticAction.class);
    private static final String FOLDER_ID_PARAM = "FOLDER ID";

    @Autowired
    private EventBus eventBus;

    @Autowired
    private SolrCatFolderRepository solrFolderRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Value("${diagnostics.doc-folders.page-size:5000}")
    private int pageSize;

    @Value("${diagnostics.doc-folders.start-after-min:80}")
    private int startAfterMin;

    @Value("${diagnostics.doc-folders.event-limit:100}")
    private int cycleEventThreshold;

    @Value("${diagnostics.doc-folder-validation.hash.enabled:true}")
    private boolean hashValidationEnabled;

    @Value("${diagnostics.doc-folder-validation.root-folder.enabled:true}")
    private boolean rfValidationEnabled;

    @Value("${diagnostics.doc-folder-validation.parent.enabled:true}")
    private boolean parentValidationEnabled;

    @Value("${diagnostics.doc-folder-validation.possible-file.enabled:true}")
    private boolean possibleFileValidationEnabled;

    @Value("${diagnostics.doc-folder-validation.possible-file.file-postfix:txt,doc,docx,xls,xlsx,pdf,csv,bak,log}")
    private String[] problemFileExtensionForDirectory;

    @Value("${diagnostics.doc-folder-validation.percentage:100}")
    private String percentageToTestConfig;

    @Value("${diagnostics.doc-folder-validation.pickRandomly:true}")
    private boolean partialTestingPickRandomly;

    private AtomicInteger currentEventCounter = new AtomicInteger();

    private Long lastEndRangeRun = null;

    private List<Integer> specialTypes = Lists.newArrayList(
            FolderType.ZIP.toInt(), FolderType.PST.toInt(), FolderType.MAILBOX.toInt());

    @Override
    public void runDiagnostic(Map<String, String> opData) {

        Long from = opData.containsKey(START_LIMIT) ? Long.parseLong(opData.get(START_LIMIT)) : null;

        Long to = opData.containsKey(END_LIMIT) ? Long.parseLong(opData.get(END_LIMIT)) : null;

        Long rootFolderId = opData.containsKey(ROOT_FOLDER_ID) ? Long.parseLong(opData.get(ROOT_FOLDER_ID)) : null;

        String idsStr = opData.get(SPECIFIC_IDS);

        float percentageTest = Float.parseFloat(opData.getOrDefault(PAGE_PERCENTAGE_TEST, percentageToTestConfig));

        if (currentEventCounter.get() >= cycleEventThreshold) {
            logger.debug("threshold passed for validating folders - skip");
            return;
        }

        if (!hashValidationEnabled && !rfValidationEnabled &&
                !parentValidationEnabled && !possibleFileValidationEnabled) {
            logger.debug("all validations are disabled. validating folders - skip");
            return;
        }

        Query query = Query.create().setRows(pageSize)
                .addSort(CatFoldersFieldType.ID, Sort.Direction.ASC)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));

        if (from != null && to != null) {
            logger.debug("validating folders update date range from {} to {}", from, to);
            query.addFilterWithCriteria(Criteria.create(CatFoldersFieldType.UPDATE_DATE, SolrOperator.IN_RANGE, Range.create(from, to)));
            lastEndRangeRun = to;
        } else if (rootFolderId != null) {
            logger.debug("validating folders for root folder id {}", rootFolderId);
            query.addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId));
        } else if (idsStr != null) {
            String[] split = idsStr.split(",");
            List<String> ids = Arrays.stream(split)
                    .collect(Collectors.toList());
            logger.debug("validating folders for specific ids size {}", ids.size());
            query.addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ID, SolrOperator.IN, ids));
        } // else no limitation run on entire collection

        String cursor = "*";
        boolean lastPage;
        int pageNum = 1;
        do {

            QueryResponse resp = solrFolderRepository.runQuery(query.setCursorMark(cursor).build());

            List<SolrFolderEntity> folders = resp.getBeans(SolrFolderEntity.class);
            cursor = resp.getNextCursorMark();

            int retrievedItemsCount = folders.size();
            if (folders != null && !folders.isEmpty()) {
                List<? extends SolrEntity> tempListForSampling = Lists.newArrayList(folders);
                tempListForSampling = getSolrEntitiesSample(tempListForSampling, percentageTest, partialTestingPickRandomly, pageSize);
                folders = (List<SolrFolderEntity>)tempListForSampling;

                Set<Long> folderIds = folders.stream()
                        .map(SolrFolderEntity::getParentFolderId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<SolrFolderEntity> parentFolders = (folderIds == null || folderIds.isEmpty() ? new ArrayList<>() : solrFolderRepository.find(folderIds));
                Map<Long, SolrFolderEntity> folderById = parentFolders.stream().collect(Collectors.toMap(SolrFolderEntity::getId, r -> r));

                Set<Long> rootFolderIds = folders.stream()
                        .map(SolrFolderEntity::getRootFolderId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<RootFolder> rootFolders = (rootFolderIds == null || rootFolderIds.isEmpty())
                        ? new ArrayList<>() : docStoreService.getRootFoldersCached(rootFolderIds);
                Map<Long, RootFolder> rootFolderById = rootFolders.stream().collect(Collectors.toMap(RootFolder::getId, r -> r));

                for (SolrFolderEntity folder : folders) {
                    validateFolder(folder, rootFolderById, folderById);
                    if (currentEventCounter.get() >= cycleEventThreshold) {
                        logger.debug("threshold passed for validating folders after id {} - skip rest", folder.getId());
                        break;
                    }
                }
            }
            logger.debug("check diagnostics page number {} of size {} folders (out of {})", pageNum, folders.size(), retrievedItemsCount);
            pageNum++;
            lastPage = resp.getResults().size() < pageSize;
        } while (!lastPage);
        logger.debug("done validating folders");
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.DOC_FOLDER;
    }

    @Override
    public Long getLastEndRangeRun() {
        return lastEndRangeRun;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        currentEventCounter.set(0);
        return Lists.newArrayList(addDiagnosticActionTimeParts());
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public int getStartAfterMin() {
        return startAfterMin;
    }

    private void validateFolder(SolrFolderEntity folder, Map<Long, RootFolder> rootFolderById,
                                Map<Long, SolrFolderEntity> folderById) {
        try {
            List<Event> events = new ArrayList<>();

            if (rfValidationEnabled) {
                validRootFolder(folder, rootFolderById).ifPresent(events::add);
            }

            if (hashValidationEnabled) {
                validHash(folder).ifPresent(events::add);
            }

            if (parentValidationEnabled) {
                validParentInfo(folder, folderById).ifPresent(events::add);
            }

            if (possibleFileValidationEnabled) {
                validPossibleFile(folder).ifPresent(events::add);
            }

            currentEventCounter.addAndGet(events.size());
            events.forEach(event -> eventBus.broadcast(Topics.DIAGNOSTICS, event));

        } catch (Exception e) {
            logger.error("problem validating folder {}", folder.getId() , e);
        }
    }

    private Optional<Event> validRootFolder(SolrFolderEntity folder, Map<Long, RootFolder> rootFolderById) {
        if (folder.getRootFolderId() == null || rootFolderById.get(folder.getRootFolderId()) == null) {
            Event event = Event.builder().withEventType(EventType.FOLDER_INVALID_ROOT_FOLDER)
                   .withEntry(FOLDER_ID_PARAM, folder.getId())
                    .withEntry("ROOT_FOLDER_ID", folder.getRootFolderId())
                    .build();
            return Optional.of(event);
        }
        return Optional.empty();
    }

    private Optional<Event> validHash(SolrFolderEntity folder) {
        Optional<Event> event = Optional.of(Event.builder().
                withEventType(EventType.FOLDER_INVALID_HASH)
                .withEntry(FOLDER_ID_PARAM, folder.getId())
                .withEntry("PATH", folder.getPath())
                .withEntry("ROOT_FOLDER_ID", folder.getRootFolderId())
                .withEntry("HASH", folder.getFolderHash())
                .build());

        if (folder.getPath() == null) {
            return event;
        }

        String calcHash = DocFolder.getHashedFolderString(folder.getPath(), folder.getRootFolderId());

        if (!calcHash.equals(folder.getFolderHash())) {
            return event;
        }

        return Optional.empty();
    }

    private Optional<Event> validPossibleFile(SolrFolderEntity folder) {
        Optional<Event> event = Optional.of(Event.builder().
                withEventType(EventType.FOLDER_INVALID_POSSIBLE_FILE)
                .withEntry(FOLDER_ID_PARAM, folder.getId())
                .withEntry("PATH", folder.getPath())
                .withEntry("NAME", folder.getName())
                .build());

        if (!Strings.isNullOrEmpty(folder.getName()) && !specialTypes.contains(folder.getType())) {
            int pos = folder.getName().lastIndexOf(".");
            int size = folder.getName().length();
            int diff = size - pos;
            if (pos > 0 && (diff == 4 || diff == 5) ) {
                for (String post : problemFileExtensionForDirectory) {
                    if (folder.getName().endsWith("." + post)) {
                        return event;
                    }
                }
            }
        }

        return Optional.empty();
    }

    private Optional<Event> validParentInfo(SolrFolderEntity folder, Map<Long, SolrFolderEntity> folderById) {
        Event.Builder builder = Event.builder().
                withEventType(EventType.FOLDER_INVALID_PARENT_INFO)
                .withEntry(FOLDER_ID_PARAM, folder.getId())
                .withEntry("DEPTH_FROM_ROOT", folder.getDepthFromRoot())
                .withEntry("PARENT_INFO", folder.getParentsInfo())
                .withEntry("PARENT_FOLDER", folder.getParentFolderId());

        if (folder.getParentsInfo() == null) {
            return Optional.of(builder.build());
        }

        String currentInfoStr = folder.getDepthFromRoot() + "." + folder.getId();
        if (folder.getParentFolderId() == null) {
            if (folder.getParentsInfo().size() != 1 || !folder.getParentsInfo().get(0).equals(currentInfoStr)) {
                return Optional.of(builder.build());
            }
        } else {

            SolrFolderEntity parentFolder = folderById.get(folder.getParentFolderId());

            if (parentFolder == null) {
                return Optional.of(builder
                        .withEntry("PARENT_FOLDER_MISSING", folder.getParentFolderId())
                        .build());
            }

            if (parentFolder.getParentsInfo() == null || parentFolder.getDeleted()) {
                return Optional.of(builder
                        .withEntry("PARENT_FOLDER_INFO", parentFolder.getParentsInfo())
                        .withEntry("PARENT_FOLDER_DELETED", parentFolder.getDeleted())
                        .build());
            }

            if (folder.getParentsInfo().size() -1 != parentFolder.getParentsInfo().size()) {
                return Optional.of(builder
                        .withEntry("PARENT_FOLDER_INFO", parentFolder.getParentsInfo())
                        .build());
            }

            if (!folder.getParentsInfo().contains(currentInfoStr)) {
                return Optional.of(builder.build());
            }

            for (String folderIdentifier : parentFolder.getParentsInfo()) {
                if (!folder.getParentsInfo().contains(folderIdentifier)) {
                    return Optional.of(builder
                            .withEntry("PARENT_FOLDER_INFO", parentFolder.getParentsInfo())
                            .build());
                }
            }
        }

        return Optional.empty();
    }
}
