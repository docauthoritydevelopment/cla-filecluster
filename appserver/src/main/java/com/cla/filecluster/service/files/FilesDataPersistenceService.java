package com.cla.filecluster.service.files;


import com.cla.common.constants.*;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.*;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.messages.ContentMetadataRequestMessage;
import com.cla.common.domain.dto.messages.ContentMetadataResponseMessage;
import com.cla.common.domain.dto.messages.IngestMessagePayload;
import com.cla.common.domain.dto.messages.IngestResultMessage;
import com.cla.common.utils.CsvUtils;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.EncryptionType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.converters.SolrContentMetadataBuilder;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.metadata.MetadataTranslationService;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.query.*;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.extractor.pattern.RegulationsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.syscomponent.AppServerStateService;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mysql.jdbc.PacketTooBigException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CursorMarkParams;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import static com.cla.common.constants.FileGroupsFieldType.*;
import static com.cla.common.constants.FileGroupsFieldType.ID;
import static com.cla.connector.domain.dto.error.ProcessingErrorType.ERR_LIMIT_EXCEEDED;
import static com.cla.connector.domain.dto.error.ProcessingErrorType.PACKAGE_CRAWL_PASSWORD_PROTECTED;

@SuppressWarnings("JavaDoc")
@Service
public class FilesDataPersistenceService {

    private final static Logger logger = LoggerFactory.getLogger(FilesDataPersistenceService.class);

    public static final String ERROR_MSG_PREFIX = "ingest.file.";

    @Value("${scanner.metadata.removeOwnerQuotes:false}")
    private boolean removeOwnerQuotes;

    @Value("${scanner.content.batch.size:10000}")
    private int contentUpdateBatchSize;

    @Value("${ingester.content.check-exists-before-save:true}")
    private boolean checkExistsBeforeSave;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private ScanDiffEventProvider scanDiffEventProvider;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private Tika tika;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    @Autowired
    private SolrGrpHelperRepository solrGrpHelperRepository;

    @Autowired
    private FileDTOSearchRepository fileDTOSearchRepository;

    @Autowired
    private SolrPvEntriesIngesterRepository solrPvEntriesIngesterRepository;

    @Autowired
    private AppServerStateService appServerStateService;

    @Autowired
    private RegulationsService regulationsService;

    @Autowired
    private MetadataTranslationService metadataTranslationService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${scan.file.commit-solr-during:false}")
    private boolean commitFileSolrDuringScan;

    @Value("${scan.content.commit-solr-during:false}")
    private boolean commitContentSolrDuringScan;

    @Value("${scan.content.commit:NONE}")
    private SolrCommitType scanContentCommitType;

    @Value("${ingest.email.parse-x500-addresses:true}")
    private boolean parseX500Addresses;

    @Value("${ingest.email.x500.extract-non-domain-addresses:true}")
    private boolean extractNonDomainX500CN;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    private final Object duplicateCounterLock = new Object();

    private TimeSource timeSource = new TimeSource();

    @Autowired
    private IngestBatchResultsProcessor ingestResultCollector;

    @Autowired
    private EventBus eventBus;

    @Transactional
    public void markFileAsDeleted(Long fileId) {
        //TODO - remove associations
        fileCatService.processFileDeletion(fileId);
        // if this file is used as sample file for its content - remove the reference
        // TODO: replace the reference by another file, if exists
        contentMetadataService.clearSampleFile(fileId);
    }

    /**
     * @param ingestResult ingestion result
     * @param errorType    error type
     * @param <T>          ingest message payload exact type
     * @return is error fatal
     */
    @Transactional
    @AutoAuthenticate
    public <T extends IngestMessagePayload> void addIngestionErrorRecord(IngestResultMessage<T> ingestResult,
                                                                         ProcessingErrorType errorType,
                                                                         ClaFileState claFileState,
                                                                         IngestBatchStore ingestBatchStore) {
        Long fileId = ingestResult.getClaFileId();
        long runId = ingestResult.getRunContext();

        SolrFileEntity fileEntity = ingestBatchStore.getFileEntity(fileId);
        if (fileEntity == null) {
            logger.error("Failed to retrieve file with file ID {}.", fileId);
            return;
        }

        String message = messageHandler.getMessage(ERROR_MSG_PREFIX + errorType.getText());

        if (ClaFileState.ERROR.name().equals(fileEntity.getState())) {
            logger.info("Reporting ingestion error while file is already in error state: " +
                            "[fileId {}, fileName {}, message {}, type {}]",
                    fileEntity.getId(), fileEntity.getFullName(), message, errorType);
            return;
        }

        RootFolder rf = docStoreService.getRootFolderCached(fileEntity.getRootFolderId());

        // update CatFile in SOLR --------------------------------
        FileCatBuilder fileCatBuilder = FileCatBuilder.create()
                .setClaFilePropertiesDto(ingestResult.getFileProperties())
                .setRootFolderPath(rf.getRealPath())
                .setRemoveOwnerQuotes(removeOwnerQuotes)
                .setIngestErrType(errorType)
                .setState(claFileState)
                .setParseX500Address(parseX500Addresses)
                .setExtractNonDomainX500CN(extractNonDomainX500CN)
                .setEncryptionType(PACKAGE_CRAWL_PASSWORD_PROTECTED.equals(errorType) ? EncryptionType.PASSWORD_PROTECTED : null);

        // upd acl
        final List<AclItemDto> aclItems = getAclItems(fileId, ingestResult.getFileProperties());

        metadataTranslationService.setMetadata(fileCatBuilder, ingestResult.getFileProperties());

        fileCatBuilder.update(fileEntity);

        fileCatService.updateIngestErrorDataFile(fileEntity, aclItems, fileEntity, ingestBatchStore);

        if (commitFileSolrDuringScan) {
            fileCatService.saveAndCommit(fileEntity);
        } else {
            ingestResultCollector.collectFileEntity(ingestResult.getTaskId(), fileEntity);
        }

        logger.info("Saving ingestion error: [fileId {}, fileName {}, message {}, type {}]",
                fileEntity.getId(), fileEntity.getFullName(), message, errorType);
        ingestErrorService.saveError(ingestResult.getTaskId(), runId, fileEntity, errorType, message,
                ingestResult.getPayload() == null ? null : ingestResult.getPayload().getExceptionText());
    }

    @Transactional(readOnly = true)
    ClaFileDto solrFileEntityToDto(SolrFileEntity fileEntity,
                                   boolean withDocFolders,
                                   boolean withFileTags,
                                   boolean withTagAssociations,
                                   boolean hideHiddenTags,
                                   Map<Long, BasicScope> folderScopes,
                                   List<AssociableEntityDto> associationEntities,
                                   ContentMetadataDto contentMetadataParam,
                                   Collection<AssociationEntity> userGroupAssociations,
                                   Collection<AssociationEntity> userGroupDocTypeAssociations,
                                   Collection<AssociationEntity> folderAssociations,
                                   Collection<AssociationEntity> docTypeAssociations,
                                   FileGroup userGroupParam, DocFolder docFolderParam) {
        if (fileEntity == null) {
            throw new RuntimeException("Cannot convert null file entity.");
        }

        String analysisGroupId = fileEntity.getAnalysisGroupId();
        String userGroupId = fileEntity.getUserGroupId();
        String groupName = null;

        if (userGroupId != null) {
            if (userGroupParam == null) {
                logger.trace("get user group {} from solr, no parameter", userGroupId);
            }
            FileGroup userGroup = userGroupParam != null ? userGroupParam : fileGroupService.getFileGroupById(userGroupId);
            groupName = userGroup.getNameForReport();
            if (groupName == null) {
                groupName = FileGroupService.UNKNOWN_GROUP_NAME;
            }
        }

        ClaFileDto claFileDto = new ClaFileDto();

        claFileDto.setFileId(fileEntity.getFileId());
        claFileDto.setFullPath(fileEntity.getFullPath());
        claFileDto.setType(FileType.valueOf(fileEntity.getType()));
        claFileDto.setBaseName(fileEntity.getBaseName());
        claFileDto.setFileName(fileEntity.getFullName());
        claFileDto.setFileNameHashed(fileEntity.getFileNameHashed());
        claFileDto.setFsFileSize(fileEntity.getSizeOnMedia());
        claFileDto.setFsLastModified(fileEntity.getModificationDate());
        claFileDto.setCreationDate(fileEntity.getAppCreationDate());
        claFileDto.setFsLastAccess(fileEntity.getAccessDate());
        claFileDto.setInitialScanDate(fileEntity.getInitialScanDate());
        claFileDto.setDeletionDate(fileEntity.getDeletionDate());
        claFileDto.setAnalysisGroupId(analysisGroupId);
        claFileDto.setGroupId(userGroupId);
        claFileDto.setGroupName(groupName);
        claFileDto.setRootFolderId(fileEntity.getRootFolderId());
        claFileDto.setCreationDate(fileEntity.getCreationDate());
        claFileDto.setExtension(fileEntity.getExtension());
        claFileDto.setState(ClaFileState.valueOf(fileEntity.getState()));
        claFileDto.setOwner(fileEntity.getOwner());
        claFileDto.setLastMetadataChangeDate(fileEntity.getLastMetadataChangeDate());
        claFileDto.setMediaEntityId(fileEntity.getMediaEntityId());
        if (fileEntity.getItemType() != null) {
            claFileDto.setItemType(ItemType.valueOf(fileEntity.getItemType()));
        }
        claFileDto.setNumOfAttachments(fileEntity.getNumOfAttachments());
        claFileDto.setContainerIdsByList(fileEntity.getContainerIds());
        claFileDto.setMailboxUpn(fileEntity.getMailboxUpn());
        claFileDto.setItemSubject(fileEntity.getItemSubject());
        claFileDto.setSortName(fileEntity.getSortName());

        // doc folder -------------------------------------------------
        if (withDocFolders) {
            Long folderId = fileEntity.getFolderId();
            if (folderId != null) {
                RootFolder rf = docStoreService.getRootFolderCached(fileEntity.getRootFolderId());
                if (docFolderParam == null) {
                    logger.trace("get doc folder {} from solr, no parameter", folderId);
                }
                DocFolder docFolder = docFolderParam != null ? docFolderParam : docFolderService.findById(folderId);
                if (docFolder != null) {
                    DocFolderDto docFolderDto = DocFolderService.convertDocFolder(
                            docFolder, true, false, rf, folderAssociations, folderScopes, associationEntities);
                    claFileDto.setDocFolder(docFolderDto);
                    claFileDto.setDocFolderId(docFolderDto.getId());
                    claFileDto.setDocFolderRealPath(docFolderDto.getPathNoPrefix());
                }
            }
        }
        // content metadata -------------------------------------
        if (fileEntity.getContentId() != null && claFileDto.getState().shouldHaveContent()) {
            claFileDto.setContentId(Long.valueOf(fileEntity.getContentId()));
            ContentMetadataDto contentMetadataDto = contentMetadataParam;
            if (contentMetadataDto == null) {
                logger.trace("get content {} from solr, no parameter", fileEntity.getContentId());
                ContentMetadata contentMetadata = getOneById(claFileDto.getContentId());
                contentMetadataDto = ContentMetadataService.convertContentMetaData(contentMetadata);
            }

            if (contentMetadataDto != null) {
                claFileDto.setContentMetadataDto(contentMetadataDto);
                if (claFileDto.getFsFileSize() == null) {
                    claFileDto.setFsFileSize(contentMetadataDto.getFsFileSize());
                }
                claFileDto.setAnalyzeHint(contentMetadataDto.getAnalyzeHint());
            }
        }

        AssociationRelatedData fileAssociations = associationsService.getAssociationRelatedData(fileEntity);
        List<AssociationEntity> fileDocTypeAssociationEntities = AssociationsService.getDocTypeAssociation(fileAssociations.getAssociations());
        List<AssociableEntityDto> entities = new ArrayList<>(fileAssociations.getEntities());
        if (associationEntities != null) {
            entities.addAll(associationEntities);
        }
        Map<Long, BasicScope> scopesForFile = new HashMap<>(fileAssociations.getScopes());
        if (folderScopes != null) {
            scopesForFile.putAll(folderScopes);
        }

        if (withFileTags) {
            claFileDto.setAssociatedBizListItems(AssociationUtils.collectAssociatedBizListItemsFromFile(entities, fileAssociations.getAssociations(), userGroupAssociations, folderAssociations));
            claFileDto.setAssociatedFunctionalRoles(AssociationUtils.collectAssociatedFunctionalRulesFromFile(entities, fileAssociations.getAssociations(), userGroupAssociations, folderAssociations));
        }

        List<DocTypeAssociationDto> allDocTypeAssociations = AssociationUtils.collectDocTypeAssociationsForFile(scopesForFile, entities,
                fileDocTypeAssociationEntities, docTypeAssociations, userGroupDocTypeAssociations);
        if (withTagAssociations) {
            claFileDto.setFileTagAssociations(AssociationUtils.collectFileTagAssociations(scopesForFile, entities, fileAssociations.getAssociations(),
                    AssociationUtils.mergeDocTypeAssociations(scopesForFile, entities, fileDocTypeAssociationEntities, docTypeAssociations, userGroupDocTypeAssociations),
                    userGroupAssociations,
                    folderAssociations, hideHiddenTags));
            claFileDto.setFileTagDtos(claFileDto.getFileTagAssociations().stream()
                    .map(FileTagAssociationDto::getFileTagDto)
                    .collect(Collectors.toSet()));

            claFileDto.setMatchedPatternsSingle(fileEntity.getMatchedPatternsSingle());
            claFileDto.setMatchedPatternsMulti(fileEntity.getMatchedPatternsMulti());
            claFileDto.setMatchedPatternsCounts(fileEntity.getMatchedPatternsCounts());
        }
        claFileDto.setDocTypeAssociations(allDocTypeAssociations);
        claFileDto.getDocTypeDtos().addAll(allDocTypeAssociations.stream().map(DocTypeAssociationDto::getDocTypeDto).collect(Collectors.toList()));

        claFileDto.setDepartmentAssociationDtos(AssociationUtils.collectDepartmentAssociations(folderAssociations, entities, true));
        claFileDto.setDepartmentDto(AssociationUtils.getDepartmentDto(claFileDto.getDepartmentAssociationDtos()));

        Set<SearchPatternCountDto> spcDtos = FileCatUtils.convertSearchPatternCounting(fileEntity.getSearchPatternsCounting());
        claFileDto.setSearchPatternsCounting(spcDtos);

        claFileDto.setSenderAddress(fileEntity.getSenderAddress());
        claFileDto.setSenderName(fileEntity.getSenderName());
        claFileDto.setSenderDomain(fileEntity.getSenderDomain());
        claFileDto.setSenderFullAddress(fileEntity.getSenderFullAddress());
        claFileDto.setRecipientsAddresses(fileEntity.getRecipientsAddresses());
        claFileDto.setRecipientsNames(fileEntity.getRecipientsNames());
        claFileDto.setRecipientsDomains(fileEntity.getRecipientsDomains());
        claFileDto.setRecipientsFullAddresses(fileEntity.getRecipientsFullAddresses());
        claFileDto.setSentDate(fileEntity.getSentDate());

        claFileDto.setDaLabels(fileEntity.getDaLabels());
        claFileDto.setExternalMetadata(fileEntity.getExternalMetadata());
        claFileDto.setActionsExpected(fileEntity.getActionsExpected());
        claFileDto.setRawMetadata(fileEntity.getRawMetadata());
        claFileDto.setDaMetadata(fileEntity.getDaMetadata());
        claFileDto.setDaMetadataType(fileEntity.getDaMetadataType());
        claFileDto.setGeneralMetadata(fileEntity.getGeneralMetadata());
        claFileDto.setActionConfirmation(fileEntity.getActionConfirmation());
        claFileDto.setLabelProcessDate(fileEntity.getLabelProcessDate());

        return claFileDto;
    }

    /**
     * Resolve content metadata for a given file
     * <ol>
     * <li> In case metadata exists just return it.</li>
     * <li> In case it does not exists but it's a duplicate, then do the following: </li>
     * <ol>
     * <li>Replace metadata id of file to the existing id</li>
     * <li>Return the existing content metadata</li>
     * </ol>
     * <li>In case it's not a duplicate the return a new content metadata</li>
     * </ol>
     *
     * @param taskId
     * @param file
     * @param claFileProperties
     * @param ingestBatchStore
     * @return contentMetadata
     */
    public ContentMetadata resolveContentMetadata(Long taskId, SolrFileEntity file,
                                                  ClaFilePropertiesDto claFileProperties, IngestBatchStore ingestBatchStore,
                                                  ProcessingErrorType errorType, ClaFileState claFileState) {
        ContentMetadata contentMetadata;
        // Backward compatibility, in case no metadata id on cla file just create it.
        if (file.getContentId() == null) {
            contentMetadata = createContentMetadata(file, claFileProperties, false);
            logger.trace("Content id is null for for file with id: {}, created new contentMetada: {}", file.getId(),
                    contentMetadata);
            file.setContentId(contentMetadata.getId().toString());
            replaceOldMetadataWithSelected(taskId, file, claFileProperties, ingestBatchStore, file.getFileId(),
                    contentMetadata, errorType, claFileState);
            return contentMetadata;
        }

        contentMetadata = ingestBatchStore.getContentMetadata(Long.valueOf(file.getContentId()));
        ContentMetadata contentMetadataBeforeChange = null;
        if (contentMetadata != null) {
            logger.trace("Content metadata exists in batch store, {}", contentMetadata);
            // Case equals then just return it
            if (Objects.equals(contentMetadata.getContentSignature(), claFileProperties.getContentSignature())) {
                logger.trace("No need to Update content Metadata for file id {} (content id: {})", file.getId(), file.getContentId());
                contentMetadata.setAlreadyAccessed(true);
                contentMetadata.setLastIngestTimestampMs(timeSource.currentTimeMillis());
                return contentMetadata;
            }

            // Other wise decrease it's file count
            logger.debug("File content has changed (fileId:{}, contentId: {}, fileName:{}), decreasing count from {} to {}",
                    file.getId(), contentMetadata.getId(), file.getFullName(), contentMetadata.getFileCount(), contentMetadata.getFileCount() - 1);
            SolrInputDocument metadataChange = AtomicUpdateBuilder.create().setId(ContentMetadataFieldType.ID, String.valueOf(contentMetadata.getId()))
                    .addModifier(ContentMetadataFieldType.FILE_COUNT, SolrOperator.INC, -1)
                    .addModifier(ContentMetadataFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                    .build();
            ingestResultCollector.collectMetadata(taskId, metadataChange);
            contentMetadataBeforeChange = contentMetadata;
        }

        // 1. Got here means that metadata does not exists or was changed
        if (checkExistsBeforeSave) {
            // Try to see if content already exists with different id in batch
            contentMetadata = handleIfSameExistsInBatch(ingestBatchStore, file, claFileProperties, taskId, errorType, claFileState);

            // If not try to see if metadata exists in repo
            if (contentMetadata == null) {
                logger.trace("No same metadata exists for file id: {} in beach, " +
                        "verify if contentMetadata already exists in repo", file.getId());
                contentMetadata = handleIfSameExistsInRepo(ingestBatchStore, file, claFileProperties, taskId, errorType, claFileState);
            }
        }

        //  2. Getting here means that #1 did not happen or this is a new metadata
        if (contentMetadata == null) {
            // We can use the existing content id only it was never used (i.e. contentMetadataBeforeChange did not exist)
            boolean useExistingContentId = contentMetadataBeforeChange == null;
            contentMetadata = createContentMetadata(file, claFileProperties, useExistingContentId);
            file.setContentId(contentMetadata.getId().toString());
            logger.trace("No duplicate content for changed file (fileId:{}, contentId: {}, fileName:{}), " +
                            "creating new content: {}",
                    file.getId(), file.getContentId(), file.getFullName(), contentMetadata.getId());

            // If contentMetadataBeforeChange existed then  we must replace the file to be with the new contentMetadata
            if (contentMetadataBeforeChange != null) {  // i.e. !useExistingContentId
                replaceOldMetadataWithSelected(taskId, file, claFileProperties, ingestBatchStore, contentMetadataBeforeChange.getId(), contentMetadata, errorType, claFileState);
                if (logger.isTraceEnabled()) {
                    logger.trace("Replacing content for file (fileId:{}, contentId: {}, fileName:{}) to content: {}",
                            file.getId(), contentMetadataBeforeChange.getId(), file.getFullName(), contentMetadata.getId());
                }
            }
        }

        return contentMetadata;
    }

    private ContentMetadata createContentMetadata(SolrFileEntity file,
                                                  ClaFilePropertiesDto claFileProperties, boolean useExistingContentId) {
        ContentMetadata contentMetadata = new ContentMetadata();
        Long contentId = FileCatService.getContentId(file);
        contentMetadata.setId(contentId);
        contentMetadata.setState(ClaFileState.SCANNED);
        contentMetadata.setLastIngestTimestampMs(timeSource.currentTimeMillis());
        contentMetadata.increaseFileCount();
        return updateContentMetadata(contentMetadata, file, claFileProperties);
    }

    public ContentMetadata updateContentMetadata(ContentMetadata contentMetadata, SolrFileEntity file, ClaFilePropertiesDto claFileProperties) {
        fillContentMetadata(contentMetadata, claFileProperties);
        contentMetadata.setType(file.getType());
        contentMetadata.setSampleFileId(file.getFileId());

        logger.debug("Update content metadata {} (sig: {}) for file {}", contentMetadata.getId(),
                contentMetadata.getContentSignature(), file.getId());
        return contentMetadata;
    }

    public void collectMetadataToBatch(Long taskId, ContentMetadata contentMetadata, ClaFilePropertiesDto claFileProperties, boolean shouldAnalyze, Long runId) {
        if (contentMetadata == null || contentMetadata.getId() == null) return;

        // if  file type has another type (better), use type of file
        if (contentMetadata.getType() != null && claFileProperties != null && claFileProperties.getType() != null
                && contentMetadata.getType() != claFileProperties.getType().toInt() &&
                (contentMetadata.getType() == FileType.OTHER.toInt() || claFileProperties.getType().isDocument())) {
            contentMetadata.setType(claFileProperties.getType().toInt());
        }

        SolrContentMetadataBuilder builder = SolrContentMetadataBuilder.create();
        builder.setProps(claFileProperties).setId(contentMetadata.getId().toString())
                .setState(contentMetadata.getState()).setSampleFileId(contentMetadata.getSampleFileId()).setType(contentMetadata.getType())
                .setAuthor(contentMetadata.getAuthor()).setCompany(contentMetadata.getCompany())
                .setTemplate(contentMetadata.getTemplate()).setSubject(contentMetadata.getSubject())
                .setKeywords(contentMetadata.getKeywords()).setGeneratingApp(contentMetadata.getGeneratingApp())
                .setAppCreationDate(contentMetadata.getAppCreationDate())
                .setTitle(contentMetadata.getTitle())
                .setItemsCountL1(contentMetadata.getItemsCountL1())
                .setItemsCountL2(contentMetadata.getItemsCountL2())
                .setItemsCountL3(contentMetadata.getItemsCountL3())
                .setFileCount(contentMetadata.getFileCount())
                .setGroupDirty(contentMetadata.isGroupDirty());

        if (contentMetadata.isPopulationDirty()) {
            builder.setPopulationDirty(contentMetadata.isPopulationDirty());
        }

        if (shouldAnalyze) {
            builder.setCurrentRunId(runId)
                    .setTaskState(TaskState.NEW.name())
                    .setTaskStateDate(System.currentTimeMillis())
                    .setRetries(0);
        }

        ingestResultCollector.collectMetadata(taskId, builder.buildAtomicUpdate());
    }

    public ContentMetadata getOneById(Long contentId) {
        return contentMetadataService.getById(contentId);
    }

    private ContentMetadata handleIfSameExistsInBatch(IngestBatchStore ingestBatchStore, SolrFileEntity file,
                                                      ClaFilePropertiesDto claFileProperties, Long taskId,
                                                      ProcessingErrorType errorType, ClaFileState claFileState) {
        Long fileContentMetadataId = FileCatService.getContentId(file);
        ContentMetadata contentMetadata;
        Long selectedContentId = ingestBatchStore.findIdenticalInBatch(claFileProperties.getContentSignature(), claFileProperties.getFileSize());
        if (selectedContentId != null) {// Case Identical content in batch
            file.setContentId(String.valueOf(selectedContentId));
            logger.trace("Found identical content {} in current ingest batch, setting the content id to file id: {}",
                    selectedContentId, file.getId());
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (ingestBatchStore) { // Safely create only one content metadata and update fileCount
                contentMetadata = ingestBatchStore.getContentMetadata(selectedContentId);
                if (contentMetadata == null) {

                    contentMetadata = createContentMetadata(file, claFileProperties, true);
                    contentMetadata.setLastIngestTimestampMs(timeSource.currentTimeMillis());
                    logger.trace("The selected content id {} has no mdetadata, created new one {}",
                            selectedContentId, contentMetadata);
                    ingestBatchStore.addContentMetadatas(Collections.singletonList(contentMetadata));
                }
                if (!selectedContentId.equals(fileContentMetadataId)) { // Delete file with old metadata id and create new file with the selectedContentId
                    logger.debug("For task (id:{} fileName:{}, contentId: {}}, replace with selectedContentId {}",
                            taskId, file.getFullName(), fileContentMetadataId, selectedContentId);
                    contentMetadata.increaseFileCount();
                    ingestBatchStore.addToFileWithDuplicateContent(file.getFileId());
                    ingestResultCollector.collectDirtyPopulationFlagById(taskId, contentMetadata.getId());
                    replaceOldMetadataWithSelected(taskId, file, claFileProperties, ingestBatchStore, fileContentMetadataId,
                            contentMetadata, errorType, claFileState);
                } else {
                    logger.debug("For task (id:{} fileName:{}, contentId: {}}, created  metadata with selectedContentId {}",
                            taskId, file.getFullName(), fileContentMetadataId, selectedContentId);
                }
            }
            return contentMetadata;
        }

        return null;
    }

    private ContentMetadata handleIfSameExistsInRepo(IngestBatchStore ingestBatchStore,
                                                     SolrFileEntity file, ClaFilePropertiesDto claFileProperties,
                                                     Long taskId, ProcessingErrorType errorType, ClaFileState claFileState) {
        List<SolrContentMetadataEntity> identicalContents = null;
        try {
            // Case found identical content in content metadata repo
            identicalContents = ingestBatchStore.findIdentical(claFileProperties.getContentSignature(), claFileProperties.getFileSize());
        } catch (Exception e) {
            logger.debug("Error during findContentBySignature({},{}). Error: {}",
                    claFileProperties.getContentSignature(), claFileProperties.getFileSize(), e.getMessage());
        }
        if (identicalContents != null && !identicalContents.isEmpty()) {
            SolrContentMetadataEntity selectedContent = identicalContents.get(0);
            logger.trace("Found {} identical copies for file id: {}. Using contentId {}",
                    identicalContents.size(), file.getId(), selectedContent.getId());

            Long contentIdToRemove = FileCatService.getContentId(file);
            logger.trace("Trying to get content for id {}", Long.valueOf(selectedContent.getId()));
            SolrContentMetadataEntity contentMetadataEntity = ingestBatchStore.getContentMetadataEntity(Long.valueOf(selectedContent.getId()));
            ContentMetadata tempMd = SolrContentMetadataRepository.fromEntity(contentMetadataEntity);
            tempMd.increaseFileCount();
            ingestBatchStore.addToFileWithDuplicateContent(file.getFileId());
            file.setContentId(selectedContent.getId());
            ingestResultCollector.collectDirtyPopulationFlagById(taskId, Long.valueOf(selectedContent.getId()));

            // original file content not exists, but file in solr with id of it
            // we need to attach the file to the existing content and create a new file entity with the correct id
            if (contentIdToRemove != null) {
                logger.trace("Replacing content Metadata (delete old record) for file id {} (content id: {})", file.getId(), contentIdToRemove);
                replaceOldMetadataWithSelected(taskId, file, claFileProperties, ingestBatchStore, contentIdToRemove, tempMd, errorType, claFileState);
            }

            return tempMd;
        }
        return null;
    }

    private void replaceOldMetadataWithSelected(Long taskId, SolrFileEntity file, ClaFilePropertiesDto claFileProperties,
                                                IngestBatchStore ingestBatchStore, Long oldContentMetadataId, ContentMetadata contentMetadata,
                                                ProcessingErrorType errorType, ClaFileState claFileState) {
        ingestResultCollector.collectDeletedFilesById(taskId,
                FileCatCompositeId.of(oldContentMetadataId, file.getFileId()).toString());
        file.setId(FileCatCompositeId.of(contentMetadata.getId(), file.getFileId()).toString());
        SolrFolderEntity solrDocFolder = ingestBatchStore.getFolderByPath(file.getRootFolderId(), file.getFullPath());
        if (solrDocFolder == null) {
            solrDocFolder = ingestBatchStore.getDocFolderEntityById(file.getFolderId());
        }
        if (solrDocFolder == null) {
            logger.warn("Null solrDocFolder returned from getFolderByPath({}, {}). file={}", file.getRootFolderId(), file.getFullPath(), file.toString());
            throw new RuntimeException("Null solrDocFolder in replaceOldMetadataWithSelected");     // TODO: consider better handling
        }

        if (file.getSortName() == null) {
            logger.warn("cant get SortName for file {} chk why not in solr ???", file.getId());
        }
       /* fileCatService.createNewDocForExistingContent(taskId, file, solrDocFolder.getParentsInfo(), claFileProperties,
                contentMetadata, errorType, claFileState);*/
    }


    @AutoAuthenticate
    public void markClaFileAsDeleted(Long fileId) {
        // Get content reference before it is cleared ...
        //TODO - remove associations
        markDirtyPopulationFlagByClaFileId(fileId);
        clearSampleFileIdIfNeededByClaFileId(fileId);
        fileCatService.markAsDeleted(fileId);
    }

    @AutoAuthenticate
    void handleContentsForDeletedFiles(List<Long> fileIds, List<Long> contentIds) {
        contentMetadataService.markDirtyPopulationFlagById(contentIds);
        contentMetadataService.fixContentsAfterFileDeletion(fileIds, true);
    }

    private void fillContentMetadata(ContentMetadata contentMetadata, ClaFilePropertiesDto claFileProperties) {
        if (claFileProperties != null) {
            if (claFileProperties.getFileSize() != null) {
                contentMetadata.setFsFileSize(claFileProperties.getFileSize());
            }
            contentMetadata.setContentSignature(claFileProperties.getContentSignature());
            contentMetadata.setUserContentSignature(claFileProperties.getUserContentSignature());
        }
    }

    @Transactional
    @AutoAuthenticate
    public void saveEmptyIngestionFile(Long taskId, long fileId, ClaFilePropertiesDto claFileProperties,
                                       IngestBatchStore ingestBatchStore) {
        SolrFileEntity entity = ingestBatchStore.getFileEntity(fileId);
        if (entity == null) {
            entity = fileCatService.getFileEntity(fileId);
        }

        if (entity == null) {
            logger.warn("Could not get file {} from DB.", claFileProperties.getFileName());
        } else {
            String entityId = entity.getId();

            entity.setId(FileCatCompositeId.of((Long) null, fileId).toString());
            entity.setContentId(null);
            entity.setState(ClaFileState.INGESTED.name());
            entity.setIngestDate(System.currentTimeMillis());
            entity.setLastMetadataChangeDate(System.currentTimeMillis());
            entity.setUpdateDate(System.currentTimeMillis());

            // upd acl
            final List<AclItemDto> aclItems = getAclItems(fileId, claFileProperties);

            fileCatService.updateIngestErrorDataFile(entity, aclItems, entity, ingestBatchStore);

            logger.debug("save empty file ingested id {} name {}", fileId, claFileProperties.getFileName());
            ingestResultCollector.collectEmptyFiles(taskId, Pair.of(entityId, entity));
        }
    }


    /**
     * Part of the server-side inter-process connector interface
     * TODO: It is here only temporarily, will move to real remote connector (REST)
     *
     * @param request inter-process request message
     * @return ContentMetadataResponseMessage
     */
    @Transactional
    public ContentMetadataResponseMessage processRequest(ContentMetadataRequestMessage request) {
        appServerStateService.incDupContentRequests();
        ContentMetadata metadata = findContentBySignature(request.getSignature(), request.getFileSize());
        if (metadata == null) {
            logger.trace("ContentMetadata check Request {} got null metadata", request);
        } else {
            logger.debug("ContentMetadata check Request {} found match with contentId {}", request, metadata);
            appServerStateService.incDupContentFound();
        }

        ContentMetadataResponseMessage msg = ContentMetadataResponseMessage.create()
                .setContentMetadataId(metadata != null ? metadata.getId() : null);


        if (request.getRootFolderId() > 0 && metadata != null) {
            RootFolder rootFolder = docStoreService.getRootFolderCached(request.getRootFolderId());
            if (rootFolder.getReingestTimeStampMs() > 0 && metadata.getLastIngestTimestampMs() < rootFolder.getReingestTimeStampMs()) {
                msg.setForceReIngest(true);
                logger.debug("ContentMetadata check Request {} with contentId {} sent force reingest", request, metadata);
            }
        }

        return msg;
    }


    private ContentMetadata findContentBySignature(String contentSignature, long fileSize) {
        List<ContentMetadata> contentBySignature = contentMetadataService.findIdenticalCM(contentSignature, fileSize);
        return reduceContentMetadatasList(contentBySignature, contentSignature);
    }

    private ContentMetadata reduceContentMetadatasList(List<ContentMetadata> metadatas, String contentSignature) {
        if (metadatas.size() == 0) {
            return null;
        } else if (metadatas.size() > 1) {
            // TODO: should re-unite content DB items
            logger.info("Duplicate (x{}) content signature in database {}. Ids: {}",
                    metadatas.size(), contentSignature,
                    metadatas.stream()
                            .map(metadata ->
                                    ("{id:" + metadata.getId().toString() +
                                            " fc:" + metadata.getFileCount() +
                                            " sf:" + metadata.getSampleFileId() + "}"))
                            .collect(Collectors.joining(",")));
        }
        return metadatas.get(0);
    }

    public void prepareAnalysisData(ProgressTracker progressTracker) {
        logger.debug("Prepare Analysis data");
        progressTracker.startStageTracking(20);
        solrAnalysisDataRepository.commit();
        solrPvEntriesIngesterRepository.commit();
        progressTracker.endCurStage();
        progressTracker.startStageTracking(11);
        contentMetadataService.commit();
        progressTracker.endCurStage();
        progressTracker.startStageTracking(9);
        solrGrpHelperRepository.commit();
        progressTracker.endCurStage();
        logger.debug("Prepare Analysis data - finished");
        //TODO - optimize?
    }

    private void attachExistingContentToFile(Long taskId, ContentMetadata contentMetadata, ClaFilePropertiesDto claFileProperties, SolrFileEntity fileEntity) {
        Long contentId = FileCatService.getContentId(fileEntity);
        if (contentId != null && !contentId.equals(contentMetadata.getId())) {
            logger.debug("File {}: {} was updated with new content, but new content already exists (id={}).",
                    fileEntity.getFileId(), fileEntity.getFullName(), contentId);
            ingestResultCollector.collectDirtyPopulationFlagById(taskId, contentId);
            ingestResultCollector.collectClearSampleFileIdIfNeededByIdAndClaFileId(taskId, contentId, fileEntity.getFileId());
            fileCatService.deleteByFileIdContentId(fileEntity.getFileId(), contentId);
        }

        logger.trace("attach Existing Content {} in state {} To File {} with properties {}",
                contentMetadata.getId(), contentMetadata.getState(), fileEntity, claFileProperties);

        RootFolder rf = docStoreService.getRootFolderCached(fileEntity.getRootFolderId());

        FileCatBuilder builder = FileCatBuilder.create()
                .setId(FileCatCompositeId.of(contentMetadata.getId(), fileEntity.getFileId()).toString())
                .setFileId(fileEntity.getFileId())
                .setContentId(contentMetadata.getId())
                .setDocFolderId(fileEntity.getFolderId())
                .setRootFolderId(fileEntity.getRootFolderId())
                .setMediaType(rf.getMediaType())
                .setParentFolderInfo(fileEntity.getParentFolderIds())
                .setDeleted(fileEntity.isDeleted())
                .setInitialScanDate(fileEntity.getInitialScanDate())
                .setRootFolderPath(rf.getRealPath())
                .setSortName(fileEntity.getSortName())
                .setClaFilePropertiesDto(claFileProperties)
                .setContainerIds(fileEntity.getContainerIds())
                .setParseX500Address(parseX500Addresses)
                .setExtractNonDomainX500CN(extractNonDomainX500CN)
                .setExtractedEntitiesIds(contentMetadata.getExtractedEntitiesIds());

        if (!ClaFileState.SCANNED.equals(contentMetadata.getState())) {
            builder.setState(contentMetadata.getState());
        }

        if (contentMetadata.getAnalysisGroupId() != null) {
            builder.setAnalysisGroupId(contentMetadata.getAnalysisGroupId());
        }

        if (contentMetadata.getUserGroupId() != null) {
            builder.setUserGroupId(contentMetadata.getUserGroupId());
        }

        if (contentMetadata.getAppCreationDate() != null) {
            builder.setAppCreationTimeMilli(contentMetadata.getAppCreationDate().getTime());
        }

        if (fileEntity.getType() != contentMetadata.getType()) {
            FileType newType = FileType.valueOf(contentMetadata.getType());
            logger.debug("changing type of file {} to {} to match content", fileEntity, newType);
            builder.setFileType(newType);
            claFileProperties.setType(newType);
        }

        metadataTranslationService.setMetadata(builder, claFileProperties);

        SolrInputDocument doc = builder.buildAtomicUpdate();
        ingestResultCollector.collectFileCatDocument(taskId, doc);
        //fileCatService.atomicUpdate(doc);
        if (commitFileSolrDuringScan) {
            fileCatService.softCommitNoWaitFlush();
        }
        ingestResultCollector.collectDirtyPopulationFlagById(taskId, contentMetadata.getId());
        //markDirtyPopulationFlagById(contentMetadata.getId());

        if (contentMetadata.getAnalysisGroupId() != null) {
            ingestResultCollector.collectMarkGroupAsDirty(taskId, contentMetadata.getAnalysisGroupId());
            //fileGroupService.markGroupAsDirty(contentMetadata.getAnalysisGroupId());
        }
        if (contentMetadata.getUserGroupId() != null) {
            ingestResultCollector.collectMarkGroupAsDirty(taskId, contentMetadata.getUserGroupId());
        }
    }


    private void markDirtyPopulationFlagByClaFileId(long claFileId) {
        try {
            Long contentId = fileCatService.getContentId(claFileId);
            if (contentId != null) {
                contentMetadataService.markDirtyPopulationFlagById(contentId);
            }
        } catch (DataAccessException e) {
            logger.debug("Exception during markDirtyPopulationFlagByClaFileId({}). {}", claFileId, e);
        } catch (Exception e) {
            logger.debug("Exception during markDirtyPopulationFlagByClaFileId({}). {}", claFileId, e, e);
        }
    }

    private void clearSampleFileIdIfNeededByClaFileId(long claFileId) {
        try {
            contentMetadataService.clearSampleFileIdIfNeededByClaFileId(claFileId);
        } catch (DataAccessException e) {
            logger.debug("Exception during clearSampleFileIdIfNeededByClaFileId({}). {}", claFileId, e);
        } catch (Exception e) {
            logger.debug("Exception during clearSampleFileIdIfNeededByClaFileId({}). {}", claFileId, e, e);
        }
    }

    public void fillFileProperties(ClaFilePropertiesDto claFileProperties, SolrFileEntity file) {
        file.setModificationDate(new Date(claFileProperties.getModTimeMilli()));
        file.setAccessDate(new Date(claFileProperties.getAccessTimeMilli()));

        String owner = CsvUtils.notNull(
                fileCatService.limitLength(claFileProperties.getOwnerName(), FileNamingUtils.DEFAULT_STR_FIELD_SIZE));
        if (removeOwnerQuotes) {
            owner = owner.replaceAll("\"", "");
        }
        file.setOwner(owner);

        file.setCreationDate(new Date(claFileProperties.getCreationTimeMilli()));
        file.setAclSignature(claFileProperties.getAclSignature());
        String aclInheritenceType = claFileProperties.getAclInheritanceType() == null ?
                null : claFileProperties.getAclInheritanceType().toString();
        file.setAclInheritanceType(aclInheritenceType);
        //file.setFullPath(file.getFullPath());
        if (claFileProperties.getFileSize() != null) {
            file.setSizeOnMedia(claFileProperties.getFileSize());
        }
        if (claFileProperties.getMime() != null) {
            file.setMime(claFileProperties.getMime());
        }
        if (claFileProperties.getType() != null) {
            file.setType(claFileProperties.getType().toInt());
        }

        metadataTranslationService.setMetadata(file, claFileProperties);
    }


    public List<AclItemDto> getAclItems(final Long claFileId, final ClaFilePropertiesDto cfp) {
        final List<AclItemDto> result = new ArrayList<>();

        CsvUtils.notNull(cfp.getAclReadAllowed()).forEach(a -> result.add(new AclItemDto(claFileId, a, AclType.READ_TYPE, AclLevel.document)));
        CsvUtils.notNull(cfp.getAclWriteAllowed()).forEach(a -> result.add(new AclItemDto(claFileId, a, AclType.WRITE_TYPE, AclLevel.document)));
        CsvUtils.notNull(cfp.getAclReadDenied()).forEach(a -> result.add(new AclItemDto(claFileId, a, AclType.DENY_READ_TYPE, AclLevel.document)));
        CsvUtils.notNull(cfp.getAclWriteDenied()).forEach(a -> result.add(new AclItemDto(claFileId, a, AclType.DENY_WRITE_TYPE, AclLevel.document)));

        return result;
    }

    public void softCommitSolr() {
        try {
            contentMetadataService.softCommitNoWaitFlush();
            fileCatService.softCommitNoWaitFlush();
            solrGrpHelperRepository.softCommitNoWaitFlush();
            solrAnalysisDataRepository.softCommitNoWaitFlush();
            solrPvEntriesIngesterRepository.softCommitNoWaitFlush();
        } catch (Exception e) {
            logger.error("soft commit to solr failed", e);
        }
    }

    public void softCommitSolrCF() {
        try {
            contentMetadataService.softCommitNoWaitFlush();
            fileCatService.softCommitNoWaitFlush();
        } catch (Exception e) {
            logger.error("soft commit to solr failed", e);
        }
    }


    public void commitSolr() {
        try {
            contentMetadataService.commit();
            fileCatService.commitToSolr();
            solrGrpHelperRepository.commit();
            solrAnalysisDataRepository.commit();
            solrPvEntriesIngesterRepository.commit();
            fileDTOSearchRepository.commit();
        } catch (Exception e) {
            logger.error("commit to solr failed", e);
        }
    }

    @Transactional
    public SolrFileEntity saveFileRecordInSolr(Long taskId, final SolrFileEntity file,
                                               final PvAnalysisData pvAnalysisData, List<AclItemDto> aclItemsDto,
                                               Long runId, List<String> proposedTitles,
                                               ContentMetadata contentMetadata, Integer maturityFileLevel,
                                               IngestBatchStore ingestBatchStore) {
        FileGroup analysisGroup = associationsService.getFromStoreOrLoad(file.getAnalysisGroupId(), ingestBatchStore);
        FileGroup userGroup = associationsService.getFromStoreOrLoad(file.getUserGroupId(), ingestBatchStore);

        return saveFileRecordInSolr(taskId, file, analysisGroup, userGroup, pvAnalysisData, aclItemsDto, runId,
                proposedTitles, contentMetadata, maturityFileLevel, ingestBatchStore);
    }

    @Transactional
    @AutoAuthenticate
    public void saveDuplicateContentFile(IngestResultMessage ingestResult, IngestBatchStore ingestBatchStore) {
        Long taskId = ingestResult.getTaskId();
        Long claFileId = ingestResult.getClaFileId();
        ClaFilePropertiesDto fileProperties = ingestResult.getFileProperties();

        SolrFileEntity claFile = fileCatService.getFileEntity(claFileId);
        ContentMetadata contentMetadata = getOneById(ingestResult.getContentMetadataId());
        attachExistingContentToFile(taskId, contentMetadata, fileProperties, claFile);
        SolrFileEntity file = ingestBatchStore.getFileEntity(claFileId);
        saveDuplicateContentFile(taskId, ingestResult.getRunContext(), file, fileProperties, ingestBatchStore);
    }

    @Transactional
    @AutoAuthenticate
    public SolrFileEntity saveOtherFileRecordInSolr(Long taskId, SolrFileEntity file,
                                                    ClaFilePropertiesDto claFileProperties,
                                                    Set<SearchPatternCountDto> searchPatternsCounting,
                                                    List<AclItemDto> aclItems,
                                                    ContentMetadata contentMetadata,
                                                    IngestBatchStore ingestBatchStore) {

        RootFolder rf = docStoreService.getRootFolderCached(file.getRootFolderId());
        FileCatBuilder builder = FileCatBuilder.create()
                .setId(file.getId())
                .setClaFilePropertiesDto(claFileProperties)
                .setRootFolderPath(rf.getRealPath())
                .setSortName(file.getSortName())
                .setRootFolderId(rf.getId())
                .setMediaType(rf.getMediaType())
                .setState(ClaFileState.valueOf(file.getState()))
                .setDeleted(file.isDeleted())
                .setDocFolderId(file.getFolderId())
                .setContentId(Long.valueOf(file.getContentId()))
                .setContainerIds(file.getContainerIds())
                .setParseX500Address(parseX500Addresses)
                .setExtractNonDomainX500CN(extractNonDomainX500CN)
                .setSearchPatterns(searchPatternsCounting);

        if (file.getParentFolderIds() != null) {
            builder.setParentFolderInfo(file.getParentFolderIds());
        }

        if (contentMetadata.getAppCreationDate() != null) {
            builder.setAppCreationTimeMilli(contentMetadata.getAppCreationDate().getTime());
        }

        metadataTranslationService.setMetadata(builder, claFileProperties);

        SolrInputDocument doc = builder.buildAtomicUpdate();

        fileCatService.updateAcls(doc, aclItems);
        associationsService.addTagDataToDocument(doc, file, false, ingestBatchStore);
        //todo: once set here, set again above ?
        regulationsService.addRegulationsDataToDocument(doc, searchPatternsCounting, false);

        ingestResultCollector.collectFileCatDocument(taskId, doc);
        return file;
    }

    private SolrFileEntity saveFileRecordInSolr(Long taskId, SolrFileEntity file,
                                                final FileGroup analysisGroup,
                                                final FileGroup userGroup,
                                                final PvAnalysisData pvAnalysisData, List<AclItemDto> aclItemsDto, Long runId,
                                                List<String> proposedTitles,
                                                ContentMetadata contentMetadata,
                                                Integer maturityFileLevel, IngestBatchStore ingestBatchStore) {

        Long contentId = FileCatService.getContentId(file);
        if (contentMetadata == null && contentId != null) {
            contentMetadata = ingestBatchStore.getContentMetadata(contentId);
        }

        SolrInputDocument catFile;
        KpiRecording kpiRecording;
        try {
            logger.trace("Saving file to FileCat. File {} {}", file.getId(), file.getFullName());
            file.setLastMetadataChangeDate(System.currentTimeMillis());
            kpiRecording = performanceKpiRecorder.startRecording("saveFileRecordInSolr",
                    "fileCatService.updateCategoryFile", 969);
            catFile = fileCatService.updateCategoryFile(taskId, file, contentMetadata, analysisGroup, userGroup,
                    aclItemsDto, null, ingestBatchStore);
            kpiRecording.end();

            kpiRecording = performanceKpiRecorder.startRecording("saveFileRecordInSolr", "jobManagerService.rootFolderWasAlreadyIngestedFinalized", 969);
            if (jobManagerService.rootFolderWasAlreadyIngestedFinalized(file.getRootFolderId())
                    && (contentMetadata == null || !contentMetadata.isAlreadyAccessed())) {
                generateScanDiffNewEvent(file, catFile);
            }
            kpiRecording.end();
        } catch (Exception e) {
            logger.warn("Error in saving file to FileCat. File {} {}. Exception: {}", file.getFullName(), file.getId(), e);
            throw e;
        }

        if (contentMetadata != null) {
            try {
                ContentMetadataDto contentMetadataDto = ContentMetadataService.convertContentMetaData(contentMetadata);
                contentMetadataDto.setProposedTitles(proposedTitles);
                if (contentMetadataDto.getState() != ClaFileState.ANALYSED) {
                    contentMetadataDto.setState(ClaFileState.INGESTED);
                }
                SolrContentMetadataBuilder builder = SolrContentMetadataBuilder.create();
                builder.setFromDto(contentMetadataDto);
                builder.setLastIngestMs(System.currentTimeMillis());
                kpiRecording = performanceKpiRecorder.startRecording("saveFileRecordInSolr", "contentMetadataService.atomicUpdate(builder.buildAtomicUpdate())", 992);
                ingestResultCollector.collectMetadata(taskId, builder.buildAtomicUpdate());
                kpiRecording.end();

                if (commitContentSolrDuringScan) {
                    contentMetadataService.softCommitNoWaitFlush();
                }
                kpiRecording = performanceKpiRecorder.startRecording("saveFileRecordInSolr", "solrGrpHelperRepository.createDocument(contentMetadataDto)", 999);
                SolrInputDocument doc = new SolrInputDocument();
                doc.setField("id", contentMetadataDto.getContentId());
                doc.setField("groupId", contentMetadataDto.getAnalysisGroupId());
                doc.setField("analysisHint", contentMetadataDto.getAnalyzeHint().name());
                ingestResultCollector.collectGrpHelperContentMetadata(taskId, doc);
                kpiRecording.end();
            } catch (Exception e) {
                logger.warn("Error in saving content metadata to Solr. File {} {}. Exception: {}",
                        file.getFullName(), file.getId(), e);
                throw e;
            }
        } else if (file.getSizeOnMedia() == null || file.getSizeOnMedia() > 0) {
            if (FileType.OTHER.toInt() == file.getType()) {
                logger.info("ClaFile {} is missing content metadata (sizeOnMedia: {})", file, file.getSizeOnMedia());
            } else if (!FileType.valueOf(file.getType()).isContainer()) {
                logger.warn("ClaFile {} is missing content metadata (sizeOnMedia: {})", file, file.getSizeOnMedia());
            }
        }

        if (pvAnalysisData != null) {
            kpiRecording = performanceKpiRecorder.startRecording("saveFileRecordInSolr", "savePvAnalysisData", 1010);
            savePvAnalysisData(taskId, runId, file, pvAnalysisData, maturityFileLevel);
            kpiRecording.end();
        } else {
            logger.debug("Missing PvAnalysis data for file {}", file);
        }
        return file;
    }

    private void generateScanDiffNewEvent(SolrFileEntity file, SolrInputDocument doc) {
        Map<String, Object> factsMap = Maps.newHashMap();
        doc.getFieldNames().forEach(f -> factsMap.put(f, doc.getFieldValues(f)));
        scanDiffEventProvider.generateScanDiffEvent(EventType.SCAN_DIFF_NEW, file.getFileId(), file.getFullName(), factsMap);
    }

    /**
     * Save PV analysis data in SOLR Analysis data core
     *
     * @param runId             scan run ID
     * @param file              cla file
     * @param pvAnalysisData    PVs data
     * @param maturityFileLevel maturity level
     */
    private void savePvAnalysisData(Long taskId, Long runId, SolrFileEntity file, PvAnalysisData pvAnalysisData, Integer maturityFileLevel) {
        int maturityLevel = (maturityFileLevel == null) ? 0 : maturityFileLevel;
        try {
            pvAnalysisData.setMaturityFile(maturityLevel);
            //pvAnalysisData.setMaturity(maturityLevel);
            //Save in Solr core
            SolrInputDocument inputDocument = solrAnalysisDataRepository.createInputDocument(pvAnalysisData);
            ingestResultCollector.collectPvAnalysisData(taskId, inputDocument);

            //Save in RDBMS
//            pvAnalysisDataRepository.save(pvAnalysisData);
            logger.trace("Saved pvAnalysisData for id {} to RDBMS", file.getId());
        } catch (Exception ex) {
            if (isRecordTooLong(ex)) {
                logger.warn("Can't save pvAnalysisData to RDBMS - record too long. Truncating collections. File {} {}",
                        file.getId(), file.getFullName());
                logger.debug("Record too long debug info1: File {}. Exception: {} cause {}, {}",
                        file.getId(), ex, ex.getCause(), ex.getCause().getCause());
                saveIngestionError(taskId, runId, file, ERR_LIMIT_EXCEEDED, "Sql record too big", ex);
                // Saving the file without the collections. The collections will be restored and saved
                // to the PV Entries store for reverse matching.
                final PvAnalysisData nullPvAnalysisData = new PvAnalysisData(pvAnalysisData.getContentId(),
                        (FileCollections) null, pvAnalysisData.getAnalysisMeadata());
                pvAnalysisData.setMaturityFile(maturityLevel);
                SolrInputDocument inputDocument = solrAnalysisDataRepository.createInputDocument(nullPvAnalysisData);
                ingestResultCollector.collectPvAnalysisData(taskId, inputDocument);
                //solrAnalysisDataRepository.createDocument(nullPvAnalysisData);
            } else {
                logger.warn("Error in saving pvAnalysisData to RDBMS. File {} {}. Exception: {}",
                        file.getFullName(), file.getId(), ex);
                throw ex;
            }
        }
    }

    @SuppressWarnings("SimplifiableIfStatement")
    private boolean isRecordTooLong(Exception ex) {
        Throwable cause = ex.getCause();
        if (cause == null || cause.getCause() == null) {
            return false;
        }
        return (cause.getCause() instanceof PacketTooBigException);
    }

    private void saveDuplicateContentFile(Long taskId, Long runId, SolrFileEntity file, ClaFilePropertiesDto claFileProperties, IngestBatchStore ingestBatchStore) {
        if (file == null) {
            throw new RuntimeException("Null ClaFile passed to saveDuplicateContentFile");
        }
        fillFileProperties(claFileProperties, file);
        file.setMime(tika.detect(file.getFullName()));
        final List<AclItemDto> aclItems = getAclItems(file.getFileId(), claFileProperties);

        FileGroup analysisGroup = null;
        FileGroup userGroup = null;
        if (file.getAnalysisGroupId() != null) {
            analysisGroup = ingestBatchStore.getFileGroup(file.getAnalysisGroupId());
            ingestResultCollector.collectMarkGroupAsDirty(taskId, analysisGroup.getId());
            logger.trace("Duplicate content file {} with analysis group {} and {} acl items",
                    file, analysisGroup.getId(), aclItems.size());
        } else {
            logger.trace("Duplicate content file {} with no group {} acl items", file, aclItems.size());
        }
        if (file.getUserGroupId() != null) {
            userGroup = ingestBatchStore.getFileGroup(file.getUserGroupId());
            ingestResultCollector.collectMarkGroupAsDirty(taskId, userGroup.getId());
        }
        if (ClaFileState.SCANNED.name().equals(file.getState())) {
            //Verify that we always finish ingesting
            file.setState(ClaFileState.SCANNED_AND_STOPPED.name());
            if (file.getSizeOnMedia() == null || file.getSizeOnMedia() > 0) {
                logger.warn("Set state to SCANNED_AND_STOPEED for file {}", file);
            } else {
                logger.debug("Set state to SCANNED_AND_STOPEED for file {} {} (size:{})",
                        file.getId(), file.getFullName(), file.getSizeOnMedia());
            }
        }
        file.setLastMetadataChangeDate(System.currentTimeMillis());
        saveFileRecordInSolr(taskId, file, analysisGroup, userGroup, null, aclItems, runId,
                null, null, null, ingestBatchStore);

        synchronized (duplicateCounterLock) {
            opStateService.incrementDuplicateCounter();
        }
    }

    @Transactional
    @AutoAuthenticate
    public void processDuplicateSignaturesPage(List<Object[]> duplicateUnJoinedContents) {
        //agg.content_signature, agg.tot_file_count,agg.content_count,agg.content_ids
        for (Object[] duplicateUnJoinedContent : duplicateUnJoinedContents) {
            processDuplicateSignature(duplicateUnJoinedContent);
        }
        if (!commitContentSolrDuringScan) {
            contentMetadataService.softCommitNoWaitFlush();
        }
    }

    private void processDuplicateSignature(Object[] duplicateUnJoinedContent) {
        String signature = (String) duplicateUnJoinedContent[0];
        int fileCountSum = ((Number) duplicateUnJoinedContent[1]).intValue();
        @SuppressWarnings("unchecked") List<Long> contentIds = (List<Long>) duplicateUnJoinedContent[2];
        long firstContentId = contentIds.stream().mapToLong(l -> l).min().orElse(0);
        boolean found = contentIds.remove(firstContentId);
        if (!found) {
            logger.warn("Expecting firstContentId {} to be in list {}", firstContentId, contentIds.toString());
        }
        // Move to trace ...
        logger.debug("Joining duplicates of signature {} into content id {} totalFiles={}", signature, firstContentId, fileCountSum);
        ContentMetadata contentMetadata = contentMetadataService.getById(firstContentId);
        fileCatService.mergeFilesToSingleContentId(contentMetadata, contentIds);
        contentMetadataService.updateContentIdFileCount(firstContentId, fileCountSum);
        contentMetadataService.markAsDeletedBySignatureExceptId(contentIds);
        solrPvEntriesIngesterRepository.markDeletedByContentId(contentIds); // for now cng maturity to -99
        logger.debug("Finished Joining duplicates of signature {}", signature);
    }


    @Transactional
    public void updateFileProperties(Long taskId, Long runId, Long fileId, ClaFilePropertiesDto claFilePropertiesDto,
                                     IngestBatchStore ingestBatchStore) {
        SolrFileEntity entity = ingestBatchStore.getFileEntity(fileId);
        try {
            Long fileSize = claFilePropertiesDto.getFileSize();
            ContentMetadata contentMetadata = null;
            Long contentId = FileCatService.getContentId(entity);
            if (contentId != null) {
                contentMetadata = ingestBatchStore.getContentMetadata(contentId);
            }
            Long cfFs = contentMetadata == null ? null : contentMetadata.getFsFileSize();
            if (cfFs == null) {
                logger.debug("File size null for propertyUpdateOnly event: newSize={} id={} name={}",
                        fileSize, entity.getId(), entity.getFullName());
            } else if (!cfFs.equals(fileSize)) {
                logger.warn("File size mismatch for propertyUpdateOnly event: oldSize={} newSize={} id={} name={}",
                        cfFs, fileSize, entity.getId(), entity.getFullName());
            }

            final List<AclItemDto> aclItems = ModelIngestUtils.getAclItems(entity.getFileId(), claFilePropertiesDto);
            //updateAclItems(runId, claFile, aclItems, false);

            // If content exists, set state as content state, else give anothey try to full ingestion

            FileGroup analysisGroup = null;
            FileGroup userGroup = null;
            if (entity.getAnalysisGroupId() != null) {
                analysisGroup = ingestBatchStore.getFileGroup(entity.getAnalysisGroupId());
                logger.debug("Duplicate content file {} with group {}", entity, analysisGroup.getId());
            } else if (contentMetadata != null && contentMetadata.getAnalysisGroupId() != null) {
                analysisGroup = associationsService.getFromStoreOrLoad(contentMetadata.getAnalysisGroupId(), ingestBatchStore);
                logger.debug("Duplicate content file {} with group {}", contentMetadata, analysisGroup.getId());
            } else {
                logger.debug("Duplicate content file {} with no group", entity);
            }
            if (entity.getUserGroupId() != null) {
                userGroup = ingestBatchStore.getFileGroup(entity.getUserGroupId());
            } else if (contentMetadata != null && contentMetadata.getUserGroupId() != null) {
                userGroup = associationsService.getFromStoreOrLoad(contentMetadata.getUserGroupId(), ingestBatchStore);
                logger.debug("Duplicate content file {} with group {}", contentMetadata, userGroup.getId());
            }

            saveFileRecordInSolr(taskId, entity, analysisGroup, userGroup, null, aclItems,
                    runId, null, contentMetadata, null, ingestBatchStore);

            RootFolder rf = docStoreService.getRootFolderCached(entity.getRootFolderId());

            FileCatBuilder builder = FileCatBuilder.create()
                    .setId(entity.getId())
                    .setContentId(contentMetadata == null ? null : contentMetadata.getId())
                    .setRootFolderPath(rf.getRealPath())
                    .setDocFolderId(entity.getFolderId())
                    .setClaFilePropertiesDto(claFilePropertiesDto)
                    .setParseX500Address(parseX500Addresses)
                    .setExtractNonDomainX500CN(extractNonDomainX500CN)
                    .setState(contentMetadata == null ?
                            ClaFileState.SCANNED_AND_STOPPED :
                            contentMetadata.getState());

            if (contentMetadata != null && contentMetadata.getAppCreationDate() != null) {
                builder.setAppCreationTimeMilli(contentMetadata.getAppCreationDate().getTime());
            }

            metadataTranslationService.setMetadata(builder, claFilePropertiesDto);
            logger.debug("updating file {} data", fileId);
            SolrInputDocument doc = builder.buildAtomicUpdate();
            ingestResultCollector.collectFileCatDocument(taskId, doc);
            //fileCatService.atomicUpdate(builder.buildAtomicUpdate());

            if (contentMetadata != null) {
                contentMetadata.setAlreadyAccessed(true);
            }

        } catch (Exception e) {
            logger.warn("Failed to update properties of file {}/{}. Error is {}.",
                    entity.getId(), entity.getFullName(), e.getMessage(), e);

            fileCatService.updateFieldByQueryMultiFields(
                    new String[]{CatFileFieldType.PROCESSING_STATE.getSolrName(), CatFileFieldType.ANALYSIS_DATE.getSolrName()},
                    new String[]{ClaFileState.ANALYSED.name(), String.valueOf(System.currentTimeMillis())},
                    SolrFieldOp.SET.getOpName(),
                    CatFileFieldType.ID.getSolrName() + ":" + entity.getId(), null, scanContentCommitType);

            if (commitFileSolrDuringScan) {
                fileCatService.softCommitNoWaitFlush();
            }
            throw e;
        }
    }

    public void contentMetadataAnalyzeFinalize(Long jobId, ProgressTracker progressTracker, long toDate) {
        logger.info("Start contentMetadataAnalyzeFinalize...");
        int totalUpdates4 = 0;

        jobManagerService.setOpMsg(jobId, "Count marked groups");
        final long totalDirty = contentMetadataService.countGroupDirty();
        progressTracker.setCurStageEstimatedTotal(totalDirty);

        // Clear dirty flags - must be last phase here
        jobManagerService.setOpMsg(jobId, "Clear contents group-flags");
        contentMetadataService.clearGroupDirtyFlag(toDate);
        logger.info("Cleared {} contentMetadata groupDirty flags", totalUpdates4);
        jobManagerService.setOpMsg(jobId, "Clear contents group-flags done");

    }

    @SuppressWarnings("SameParameterValue")
    private void saveIngestionError(Long taskId, Long runId, final SolrFileEntity file, final ProcessingErrorType type,
                                    final String text, final Exception exception) {
        if (file != null && !ClaFileState.ERROR.name().equals(file.getState())) {

            // update SOLR ---------------------------------
            SolrInputDocument doc = FileCatBuilder.create()
                    .setId(file.getId())
                    .setState(ClaFileState.ERROR)
                    .setIngestErrType(type)
                    .setDocFolderId(file.getFolderId())
                    .setParseX500Address(parseX500Addresses)
                    .setExtractNonDomainX500CN(extractNonDomainX500CN)
                    .setEncryptionType(PACKAGE_CRAWL_PASSWORD_PROTECTED.equals(type) ? EncryptionType.PASSWORD_PROTECTED : null)
                    .buildAtomicUpdate();

            fileCatService.atomicUpdate(doc);
            ingestErrorService.saveError(taskId, runId, file, type, text, exception == null ? null : exception.getMessage());
        } else {
            logger.error("Reporting ingestion error while file is in error-state. file-id={} fileName={} text={} type={} excp={}",
                    (file == null) ? -1 : file.getId(), (file == null) ? "" : file.getFullName(),
                    text, type.name(), exception, exception);
        }
    }


    @AutoAuthenticate
    public void copyGroupsFromContentToFile(long totalDirty) {
        logger.info("Starting loop for copying groups from {} dirty contents into CatFile", totalDirty);
        long offset = 0;
        do {
            long startTime = System.currentTimeMillis();
            List<Object[]> contents = contentMetadataService.getGroupDirtyContents(offset, contentUpdateBatchSize);
            Map<String, List<Long>> updByAnalysis = new HashMap<>();
            Map<String, List<Long>> updByUser = new HashMap<>();
            List<Long> contentIds = new LinkedList<>();

            logger.debug("Loop: Start Copy {} groups by content to files", contents.size());
            for (Object[] content : contents) {
                Long contentId = (Long) content[0];
                String analysisGroupId = (String) content[1];
                String userGroupId = (String) content[2];
                contentIds.add(contentId);
                updByAnalysis.computeIfAbsent(analysisGroupId, k -> new ArrayList<>());
                updByUser.computeIfAbsent(userGroupId, k -> new ArrayList<>());
                updByAnalysis.get(analysisGroupId).add(contentId);
                updByUser.get(userGroupId).add(contentId);
            }

            // Prepare update statements for cat files
            List<FileGroupDetails> fileCompositeIdsByContentIds = findFileGroupDetailsByContentIds(contentIds);

            Map<Long, List<FileGroupDetails>> filesGroupByContentId = fileCompositeIdsByContentIds.stream()
                    .collect(Collectors.groupingBy(fgd -> fgd.compositeId.getContentId()));


            List<SolrInputDocument> fileGroupUpdates = buildFileGroupUpdateStatements(
                    updByAnalysis, filesGroupByContentId, CatFileFieldType.ANALYSIS_GROUP_ID,
                    (groupId, fgd) -> !Objects.equals(groupId, fgd.analysisGroupId));
            fileGroupUpdates.addAll(buildFileGroupUpdateStatements(updByUser,
                    filesGroupByContentId, CatFileFieldType.USER_GROUP_ID,
                    (groupId, fgd) -> !Objects.equals(groupId, fgd.userGroupId)));

            if (!fileGroupUpdates.isEmpty()) {
                fileCatService.saveAll(fileGroupUpdates);
                List<String> newGroupedFileIds = fileGroupUpdates.stream()
                        .map(inputDocument -> (String) inputDocument.getFieldValue(CatFileFieldType.ID.getSolrName()))
                        .distinct()
                        .collect(Collectors.toList());
                eventBus.broadcast(Topics.FILE_GROUP_ATTACH, Event.builder()
                        .withEventType(com.cla.eventbus.events.EventType.BULK_GROUP_ATTACH)
                        .withEntry(EventKeys.NEW_GROUPED_FILES, newGroupedFileIds)
                        .build());
            } else if (contents.size() > 0) {
                logger.warn("No matching files for {} content ids", contents.size());
            }

            offset += contentUpdateBatchSize;
            logger.debug("Loop: Done Copy {} groups by content to files. Updates left: {}, time took: {} ms for {} contents and {} updates", fileGroupUpdates.size(),
                    Long.max(0L, totalDirty - offset), timeSource.millisSince(startTime), contents.size(), updByAnalysis.size());

        } while (offset < totalDirty);

        fileCatService.softCommitNoWaitFlush();
    }

    private List<FileGroupDetails> findFileGroupDetailsByContentIds(List<Long> contentIds) {
        if (contentIds.isEmpty()) {
            logger.debug("Please notice, passed 0 ids to findFileGroupDetailsByContentIds, ignoring request");
            return Lists.newArrayList();
        }

        return Lists.partition(contentIds, 500).stream()
                .map(p -> fileCatService.findAllByQuery(Query.create()
                        .addFilterWithCriteria(CatFileFieldType.CONTENT_ID, SolrOperator.IN, p)
                        .addField(CatFileFieldType.ID)
                        .addField(CatFileFieldType.USER_GROUP_ID)
                        .addField(CatFileFieldType.ANALYSIS_GROUP_ID)
                        .build(), CatFileFieldType.ID, Sort.Direction.ASC))
                .flatMap(Collection::stream)
                .map(this::toFileGroupDetails)
                .collect(Collectors.toList());
    }

    private FileGroupDetails toFileGroupDetails(SolrDocument document) {
        return new FileGroupDetails(
                FileCatCompositeId.of((String) document.getFieldValue(CatFileFieldType.ID.getSolrName())),
                (String) document.getFieldValue(CatFileFieldType.USER_GROUP_ID.getSolrName()),
                (String) document.getFieldValue(CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName())
        );
    }

    /**
     * Construct user group or analysis group statement to copying content group id to files.
     *
     * @param updByGroup            - the group ids to build statements
     * @param filesGroupByContentId - the relevant files to build the update statement
     * @param groupField            - which field to update, either USER_GROUP_ID or ANALYSIS_GROUP_ID
     * @param groupNotExistsFilter  - include only files that their group does not the match the group to copy
     * @return
     */
    private List<SolrInputDocument> buildFileGroupUpdateStatements(Map<String, List<Long>> updByGroup,
                                                                   Map<Long, List<FileGroupDetails>> filesGroupByContentId,
                                                                   CatFileFieldType groupField,
                                                                   BiPredicate<String, FileGroupDetails> groupNotExistsFilter) {
        List<SolrInputDocument> result = new LinkedList<>();
        updByGroup.forEach((groupId, contentIdList) -> contentIdList.stream()
                .filter(cId -> fileExistsAndIfNotWarn(filesGroupByContentId, cId))
                .flatMap(cId -> filesGroupByContentId.get(cId).stream())
                .filter(fileGroupDetails -> groupNotExistsFilter.test(groupId, fileGroupDetails))
                .map(fileGroupDetails -> fileGroupDetails.compositeId.toString())
                .forEach(cmpId -> result.add(AtomicUpdateBuilder.create()
                        .setId(CatFileFieldType.ID, cmpId)
                        .addModifier(groupField, SolrOperator.SET, groupId)
                        .addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                        .build())));
        return result;
    }

    private boolean fileExistsAndIfNotWarn(Map<Long, List<FileGroupDetails>> filesGroupByContentId, Long cId) {
        boolean containsContentId = filesGroupByContentId.containsKey(cId);
        if (!containsContentId) {
            logger.debug("No matching file found for content id {}, please verify if the content files were deleted", cId);
        }
        return containsContentId;
    }

    public void updateUserGroupInContentForJoinedGroups() {
        String cursor = "*";
        boolean lastPage;
        do {
            Query query = Query.create().addFilterWithCriteria(Criteria.create(DIRTY, SolrOperator.EQ, true))
                    .addFilterWithCriteria(Criteria.create(PARENT_GROUP_ID, SolrOperator.PRESENT, null))
                    .setRows(contentUpdateBatchSize).setCursorMark(cursor).addField(ID).addField(PARENT_GROUP_ID)
                    .addSort(ID, Sort.Direction.ASC);

            QueryResponse resp = fileGroupRepository.runQuery(query.build());
            cursor = resp.getNextCursorMark();
            if (resp.getResults().size() > 0) {
                Map<String, String> gIdToParentId = resp.getResults().stream()
                        .collect(Collectors.toMap(
                                doc -> (String) doc.getFieldValue(ID.getSolrName()),
                                doc -> (String) doc.getFieldValue(PARENT_GROUP_ID.getSolrName())
                        ));
                if (!gIdToParentId.isEmpty()) {
                    contentMetadataService.setNewUserGroupForAnalysisGroup(gIdToParentId);
                }
            }
            lastPage = resp.getResults().size() < contentUpdateBatchSize;
        } while (!lastPage);

        contentMetadataService.softCommitNoWaitFlush();
    }


    @AutoAuthenticate
    public void updateStateToAnalyzed(List<Long> contentIds, SkipAnalysisReasonType reason) {
        contentMetadataService.changeState(contentIds, ClaFileState.ANALYSED, reason);

        long now = System.currentTimeMillis();
        Criteria criteria = Criteria.create(CatFileFieldType.CONTENT_ID, SolrOperator.IN, contentIds);
        String stringQuery = SolrQueryGenerator.generateFilter(criteria);
        fileCatService.updateFieldByQueryMultiFields(
                new String[]{CatFileFieldType.PROCESSING_STATE.getSolrName(), CatFileFieldType.ANALYSIS_DATE.getSolrName()},
                new String[]{ClaFileState.ANALYSED.name(), String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                stringQuery, null, scanContentCommitType);
        logger.debug("Updated files & contents to ANALYZED took {} for content ids {} ", (System.currentTimeMillis() - now), contentIds);
    }

    public List<String> findProposedTitlesByContentId(long contentId) {
        return contentMetadataService.findProposedTitlesByContentId(contentId);
    }

    public List<String> findProposedTitlesByContent(SolrContentMetadataEntity content) {
        return contentMetadataService.findProposedTitlesByContent(content);
    }

    @Transactional
    SolrFileEntity updateClaFileAnalyzeHintAndOverrideGroups(long fileId, AnalyzeHint newAnalyzeHint, FileGroup fileGroup) {
        SolrFileEntity one = fileCatService.getFileEntity(fileId);
        Long contentId = FileCatService.getContentId(one);
        ContentMetadata contentMetadata = contentId == null ? null : contentMetadataService.getById(contentId);

        if (contentMetadata == null) {
            throw new RuntimeException("file " + fileId + " does not have a content, operation cannot proceed");
        }

        AnalyzeHint originalAnalyzeHint = contentMetadata.getAnalyzeHint();
        logger.debug("Mark file {} with Analyze Behavior: {}", one, newAnalyzeHint);

        AtomicUpdateBuilder atomicUpdateBuilder = AtomicUpdateBuilder.create()
                .setId(ContentMetadataFieldType.ID, contentId)
                .addModifier(ContentMetadataFieldType.ANALYSIS_HINT, SolrOperator.SET, newAnalyzeHint.name());

        if (fileGroup != null) {
            one.setAnalysisGroupId(fileGroup.getId());
            one.setUserGroupId(fileGroup.getId());
            atomicUpdateBuilder.addModifier(ContentMetadataFieldType.USER_GROUP_ID, SolrOperator.SET, fileGroup.getId());
            atomicUpdateBuilder.addModifier(ContentMetadataFieldType.GROUP_ID, SolrOperator.SET, fileGroup.getId());
        } else {
            one.setAnalysisGroupId(null);
            one.setUserGroupId(null);
            atomicUpdateBuilder.addModifier(ContentMetadataFieldType.USER_GROUP_ID, SolrOperator.REMOVE, null);
            atomicUpdateBuilder.addModifier(ContentMetadataFieldType.GROUP_ID, SolrOperator.REMOVE, null);
        }

        if (AnalyzeHint.EXCLUDED.equals(originalAnalyzeHint) &&
                !newAnalyzeHint.equals(AnalyzeHint.EXCLUDED)) {
            //We need to re-analyze the file
            one.setState(ClaFileState.INGESTED.name());
            one.setIngestDate(System.currentTimeMillis());

            fileCatService.updateFieldByQueryMultiFields(
                    new String[]{CatFileFieldType.PROCESSING_STATE.getSolrName(), CatFileFieldType.INGEST_DATE.getSolrName()},
                    new String[]{ClaFileState.INGESTED.name(), String.valueOf(one.getIngestDate())},
                    SolrFieldOp.SET.getOpName(),
                    CatFileFieldType.ID.getSolrName() + ":" + one.getId(), null, scanContentCommitType);

            atomicUpdateBuilder.addModifier(ContentMetadataFieldType.PROCESSING_STATE, SolrOperator.SET, ClaFileState.INGESTED.name());
        }

        atomicUpdateBuilder.addModifier(ContentMetadataFieldType.GROUP_DIRTY, SolrOperator.SET, true);
        atomicUpdateBuilder.addModifier(ContentMetadataFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis());
        atomicUpdateBuilder.addModifier(ContentMetadataFieldType.GROUP_DIRTY_DATE, SolrOperator.SET, System.currentTimeMillis());

        contentMetadataService.atomicUpdate(atomicUpdateBuilder.build());
        contentMetadataService.softCommitNoWaitFlush();
        return one;
    }

    @AutoAuthenticate
    public void updateContentMetadataStats(Long jobId, ProgressTracker progressTracker) {
        jobManagerService.setOpMsg(jobId, "update content stats");
        int totalUpdates1 = 0;
        int totalUpdates2 = 0;

        jobManagerService.setOpMsg(jobId, "Count updated contents");
        final long totalDirty = contentMetadataService.countPopulationDirty();
        if (totalDirty > 0) {
            logger.info("Start atomic num-of-files update in contentMetadata table for {} items", totalDirty);
            progressTracker.setCurStageEstimatedTotal(totalDirty * 2);

            // Update num of files
            jobManagerService.setOpMsg(jobId, "Mark files by content");
            Long offset = 0L;
            int sizeReturned;
            String solrPagingCursor = CursorMarkParams.CURSOR_MARK_START;
            do {

                ///////////////////////////////////////////
                Pair<String, Collection<Long>> contentIdsPopulationDirtyRes = contentMetadataService.getContentsPopulationDirty(solrPagingCursor, contentUpdateBatchSize);
                Collection<Long> contentIdsPopulationDirty = contentIdsPopulationDirtyRes.getValue();
                sizeReturned = (contentIdsPopulationDirty == null ? 0 : contentIdsPopulationDirty.size());
                solrPagingCursor = contentIdsPopulationDirtyRes.getKey();
                if (contentIdsPopulationDirty != null && !contentIdsPopulationDirty.isEmpty()) {
                    Map<String, Long> fileNumForContent = groupsService.getFileNumForContent(contentIdsPopulationDirty);
                    List<SolrInputDocument> atomicUpdates = new LinkedList<>();
                    for (Long contentId : contentIdsPopulationDirty) {
                        Long fileNum = fileNumForContent.get(contentId.toString());
                        if (fileNum == null) {
                            fileNum = 0L;
                        }
                        logger.trace("AtomicUpdate: Setting for contentId {} fileCount {}", contentId, fileNum);
                        atomicUpdates.add(AtomicUpdateBuilder.create()
                                .setId(ContentMetadataFieldType.ID, contentId)
                                .addModifier(ContentMetadataFieldType.FILE_COUNT, SolrOperator.SET, fileNum)
                                .addModifier(ContentMetadataFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                                .build());
                    }
                    contentMetadataService.saveAll(atomicUpdates);
                    contentMetadataService.softCommitNoWaitFlush();
                    logger.debug("Updated fileCounts for {} contents", atomicUpdates.size());
                }
                ///////////////////////////////////////////

                int updated = contentIdsPopulationDirty == null ? 0 : contentIdsPopulationDirty.size();
                totalUpdates1 += updated;
                long nextOffset = offset + contentUpdateBatchSize;
                long progressToReport = nextOffset < totalDirty ? contentUpdateBatchSize : totalDirty - offset;
                progressTracker.incrementProgress(progressToReport);
                offset = nextOffset;
                //    opStateService.incrementCurrentPhaseCounter(contentUpdateBatchSize, "offset:" + offset);
                logger.debug("Loop: updated num-of-files in contentMetadata table for {} items. total={}. Left {}", updated, totalUpdates1, Long.max(0L, totalDirty - offset));
            } while (sizeReturned > 0);


            // Update sample files
            jobManagerService.setOpMsg(jobId, "Update contents sample files");
            offset = 0L;
            solrPagingCursor = CursorMarkParams.CURSOR_MARK_START;
            logger.info("Update sample files for {} metadata table for {} items.", totalUpdates1);
            do {

                ////////////////////////////////////////////////////////////////////////////////////
                Pair<String, Collection<Long>> contentIdsPopulationDirtyRes = contentMetadataService.getContentsPopulationDirtyNeedSample(solrPagingCursor, contentUpdateBatchSize);
                Collection<Long> contentIdsPopulationDirty = contentIdsPopulationDirtyRes.getValue();
                sizeReturned = (contentIdsPopulationDirty == null ? 0 : contentIdsPopulationDirty.size());
                solrPagingCursor = contentIdsPopulationDirtyRes.getKey();
                if (contentIdsPopulationDirty != null && !contentIdsPopulationDirty.isEmpty()) {
                    List<SolrInputDocument> atomicUpdates = new LinkedList<>();
                    Map<Long, Long> firstMembers = getFirstMemberForContents(contentIdsPopulationDirty);
                    for (Long contentId : contentIdsPopulationDirty) {
                        Long sampleFileId = firstMembers.get(contentId);
                        logger.trace("AtomicUpdate: sampleFileId {} to contentId {}", sampleFileId, contentId);
                        atomicUpdates.add(AtomicUpdateBuilder.create()
                                .setId(ContentMetadataFieldType.ID, contentId)
                                .addModifier(ContentMetadataFieldType.SAMPLE_FILE_ID,
                                        SolrOperator.SET, sampleFileId)
                                .addModifier(ContentMetadataFieldType.UPDATE_DATE,
                                        SolrOperator.SET, System.currentTimeMillis())
                                .build());
                    }
                    contentMetadataService.saveAll(atomicUpdates);
                    contentMetadataService.softCommitNoWaitFlush();
                    logger.debug("Updated sampleFileIds for {} contents", atomicUpdates.size());
                }
                /////////////////////////////////////////////////////////////////////////////////////
                int updated = contentIdsPopulationDirty == null ? 0 : contentIdsPopulationDirty.size();
                totalUpdates2 += updated;
                long nextOffset = offset + contentUpdateBatchSize;
                long progressToReport = nextOffset < totalDirty ? contentUpdateBatchSize : totalDirty - offset;
                progressTracker.incrementProgress(progressToReport);
                offset = nextOffset;
                logger.debug("Loop: Removed {} sample files (contentMetadata). total={}. Left {}", updated, totalUpdates2, Long.max(0L, totalDirty - offset));
            } while (sizeReturned > 0);
            logger.info("Removed {} sample files (contentMetadata)", totalUpdates2);
        } else {
            logger.info("Skipping num-of-files and sampleFiles update, none of the content metatadas with dirty population");
        }
    }

    private Map<Long, Long> getFirstMemberForContents(Collection<Long> contentIds) {
        Query query = Query.create()
                .addFilterWithCriteria(CatFileFieldType.CONTENT_ID, SolrOperator.IN, contentIds)
                .addField(CatFileFieldType.FILE_ID)
                .setGroupField(CatFileFieldType.CONTENT_ID)
                .setGroupLimit(1);
        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        Map<Long, Long> result = new TreeMap<>();
        if (queryResponse.getGroupResponse() != null && queryResponse.getGroupResponse().getValues() != null) {
            queryResponse.getGroupResponse().getValues().forEach(g -> g.getValues().forEach(group -> {
                Long id = Long.parseLong(group.getGroupValue());
                Long fileId = (Long) group.getResult().get(0).get(CatFileFieldType.FILE_ID.getSolrName());
                result.put(id, fileId);
            }));
        }
        return result;
    }

    public void contentMetadataIngestFinalize(Long jobId, ProgressTracker progressTracker) {
        logger.info("Start contentMetadataIngestFinalize...");

        jobManagerService.setOpMsg(jobId, "Count updated contents");
        final long totalDirty = contentMetadataService.countPopulationDirty();
        progressTracker.setCurStageEstimatedTotal(totalDirty * 2);

        long startDate = System.currentTimeMillis();

        // Update groups by content
        Collection<String> groupIds = contentMetadataService.getGroupsByContentPopulationDirty();
        if (groupIds != null && !groupIds.isEmpty()) {
            fileGroupService.getGroupSqlHeavyOperationLock();
            try {
                fileGroupService.updateGroupsPopulationDirty(groupIds);
            } finally {
                fileGroupService.releaseGroupSqlHeavyOperationLock();
            }
        }
        logger.info("Updated groups by dirty content");

        // Clear dirty flags - must be last phase here
        jobManagerService.setOpMsg(jobId, "Clear updated content flags");
        contentMetadataService.clearPopulationDirtyFlag(startDate);
        logger.info("Cleared contentMetadata populationDirty flags");
        jobManagerService.setOpMsg(jobId, "Clear updated content flags done");
    }

    @Transactional(readOnly = true)
    void overrideSolrFileCatGroupAndAssociations(long fileId) {
        logger.debug("Override solr fileCat with file data on file {}", fileId);
        SolrFileEntity entity = fileCatService.getFileEntity(fileId);
        Optional<SolrFileEntity> solrCatFileOp = fileCatService.getOne(fileId);
        if (solrCatFileOp.isPresent()) {
            Optional<SolrFolderEntity> solrDocFolder = docFolderService.getOneById(solrCatFileOp.get().getFolderId());
            //noinspection ConstantConditions
            docFolderService.processParentsInfo(solrDocFolder.orElseGet(null));
            FileGroup analysisGroup = entity.getAnalysisGroupId() != null ? groupsService.findById(entity.getAnalysisGroupId()) : null;
            FileGroup userGroup = entity.getUserGroupId() != null ? groupsService.findById(entity.getUserGroupId()) : null;
            ContentMetadata contentMetadata = entity.getContentId() == null ? null :
                    contentMetadataService.getById(Long.valueOf(entity.getContentId()));
            SolrInputDocument fileCatDocument = fileCatService.createUpdateGroupDocument(
                    entity, analysisGroup, userGroup, contentMetadata);

            associationsService.addTagDataToDocument(fileCatDocument, fileId, entity.getAssociations(),
                    entity.getUserGroupId(), entity.getAnalysisGroupId(), true, solrDocFolder.orElseGet(null));
            associationsService.addDocTypeDataToDocument(fileCatDocument, entity, true);
            List<SolrInputDocument> solrInputFields = Lists.newArrayList();
            solrInputFields.add(fileCatDocument);
            fileCatService.createCategoryFilesFromDocuments(solrInputFields);
            fileCatService.commitToSolr();
        } else {
            logger.error("Failed to find file with ID {} in SOLR.", fileId);
        }
    }

    /**
     * Holds the details of the current existing group details (user and analysis group ids) of a file
     * It is used to build the copy statements from content to files
     */
    private static final class FileGroupDetails {

        public final String userGroupId;
        public final String analysisGroupId;
        public final FileCatCompositeId compositeId;

        private FileGroupDetails(FileCatCompositeId compositeId, String userGroupId, String analysisGroupId) {
            this.userGroupId = userGroupId;
            this.analysisGroupId = analysisGroupId;
            this.compositeId = compositeId;
        }
    }
}
