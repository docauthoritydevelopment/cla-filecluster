package com.cla.filecluster.service.extractor.pattern;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;

/**
 *
 */
@Service("RegulationsService")
public class RegulationsService {

    private final static Logger logger = LoggerFactory.getLogger(RegulationsService.class);

    @Value("${regulations.match.threshold.single:1}")
    private int matchCountThresholdSingle;

    @Value("${regulations.match.threshold.multi:10}")
    private int matchCountThresholdMulti;

    @Value("${regulations.counts-field.separator:.}")
    private String separator;

    private volatile boolean updateRequired = false;

    public void markUpdateRequired() {
        updateRequired = true;
    }

    public void addRegulationsDataToDocument(SolrInputDocument doc, ClaFile claFile, boolean atomicUpdate) {
        addRegulationsDataToDocument(doc, claFile.getSearchPatternsCounting(), atomicUpdate);
    }

    public void addRegulationsDataToDocument(SolrInputDocument doc, Set<SearchPatternCountDto> patternCountDtos, boolean atomicUpdate) {
        try {
            Map<Integer, Integer> matchesSingle = extractMatchedPatternsWithThreshold(patternCountDtos, matchCountThresholdSingle);
            Map<Integer, Integer> matchesidsMulti = extractMatchedPatternsWithThreshold(patternCountDtos, matchCountThresholdMulti);

            Set<String> counts = matchesSingle
                    .entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + separator + entry.getValue())
                    .collect(Collectors.toSet());

            doc.setField(MATCHED_PATTERNS_SINGLE.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(atomicUpdate, matchesSingle.keySet()));
            doc.setField(MATCHED_PATTERNS_MULTI.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(atomicUpdate, matchesidsMulti.keySet()));
            doc.setField(MATCHED_PATTERNS_COUNTS.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(atomicUpdate, counts));
        } catch (Exception e) {
            logger.error("failed adding regulation data to file {}", doc, e);
        }
    }

    /**
     *
     * @param searchPatternsCounting the extracted patterns and counts
     * @param threshold the threshold
     * @return a map of pattern-id -> pattern-count, if pattern-count surpassed the threshold.
     */
    private Map<Integer, Integer> extractMatchedPatternsWithThreshold(Set<SearchPatternCountDto> searchPatternsCounting, int threshold) {
        return Optional.ofNullable(searchPatternsCounting)
                .orElse(Collections.emptySet())
                .stream()
                // group by the pattern-id, sum the pattern counts... produces a map of pattern-id -> pattern count
                .collect(Collectors.groupingBy(SearchPatternCountDto::getPatternId, Collectors.summingInt(SearchPatternCountDto::getPatternCount)))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() >= threshold)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
