package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class DataRoleAggCountDtoMixin {

    @JsonUnwrapped
    private FunctionalRoleDto item;

    @JsonProperty(DataRoleHeaderFields.NUM_OF_FILES)
    private Integer count;
}
