package com.cla.filecluster.service.convertion;

import com.cla.common.domain.dto.category.FileCategoryConstants;
import com.cla.common.domain.dto.category.FileCategoryDto;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * Created by uri on 09/08/2015.
 */
@Service
public class FileCategoryConversionService {

    private static final Logger logger = LoggerFactory.getLogger(FileCategoryConversionService.class);

    public Page<FileCategoryDto> convertToDto(Page<FileCategory> fileCategories, PageRequest pageRequest) {
        List<FileCategoryDto> content = convertToDto(fileCategories.getContent());
        return new PageImpl<>(content,pageRequest,fileCategories.getTotalElements());
    }

    public List<FileCategoryDto> convertToDto(List<FileCategory> fileCategories) {
        return fileCategories.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    public FileCategoryDto convertToDto(FileCategory fileCategory) {
        return new FileCategoryDto(
                String.valueOf(fileCategory.getId()),
                FileCategoryConstants.UNKNOWN_SIZE,
                fileCategory.getName(),
                fileCategory.getType());
    }

    public List<FileCategoryDto> convertFacetFieldsToDto(List<FacetField> facetFields) {
        return null;
    }

    public Page<FileCategoryDto> convertFacetFieldsToDto(PageRequest pageRequest, List<FacetField> solrDocuments) {
        FacetField facetField = solrDocuments.get(0);
        List<FileCategoryDto> content = new ArrayList<>(facetField.getValueCount());
        for (FacetField.Count facetCount : facetField.getValues()) {
            //Extract the category ID
            try {
                String fileCategoryIdentifier = facetCount.getName();
                String[] split = StringUtils.split(fileCategoryIdentifier, ":");
                String fileCategoryId = split[1];

                long fileCategorySize = facetCount.getCount();
                FileCategoryDto fileCategoryDto = new FileCategoryDto(fileCategoryId,fileCategorySize);
                content.add(fileCategoryDto);
            } catch (RuntimeException e) {
                logger.error("Failed to convert SolrDocument "+facetCount.getName(),e);
                throw e;
            }
        }
        //Enhance category DTO from database...
        enhanceDtoListFromDatabase(content);

        return new PageImpl<>(content);
    }

    private void enhanceDtoListFromDatabase(List<FileCategoryDto> content) {

    }

}
