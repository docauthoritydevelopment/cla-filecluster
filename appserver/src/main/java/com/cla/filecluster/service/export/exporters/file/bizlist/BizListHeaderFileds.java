package com.cla.filecluster.service.export.exporters.file.bizlist;

public interface BizListHeaderFileds {
    String NAME = "Name";
    String BUSINESS_ID = "Business Id";
    String TYPE = "Type";
    String LIST = "List";
    String NUM_OF_FILTERED_FILES = "Number of filtered files";
}
