package com.cla.filecluster.service.export.exporters.scan_history.mixin;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.scan_history.ScanHistoryHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class RootFolderDtoMixin {

    @JsonProperty(NICKNAME)
    private String nickName;

    @JsonProperty(PATH)
    private String realPath;
}
