package com.cla.filecluster.service.doctype;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.doctype.DocTypeDetailsDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.doctype.DocTypeAssociation;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.TagRelatedBaseService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.filetag.FileTagSettingService;
import com.cla.filecluster.service.operation.GroupLockService;
import com.cla.filecluster.util.ControllerUtils;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DocTypeAppService extends TagRelatedBaseService {

    private static final Logger logger = LoggerFactory.getLogger(DocTypeAppService.class);

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private GroupLockService groupLockService;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private FileTagSettingService fileTagSettingService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${docTypeService.commit-each-association:false}")
    private boolean commitEachAssociation;

    @Value("${docTypeService.default-single-value:true}")
    private boolean configDefaultSingleValue;

    @Value("${docTypeService.ui-support-single-value:false}")
    private boolean uiSupport;

    @Value("${docTypeService.associationApplySize:1000}")
    private int associationApplySize;

    @Transactional
    public DocFolderDto assignDocTypeToFolder(Long folderId, long docTypeId, BasicScope basicScope) {
        DocTypeDto docType = docTypeService.getFromCache(docTypeId);
        final DocFolder docFolder = docFolderService.getById(folderId);
        if (!docType.isSingleValue()) {
            return assignMultiValueDocTypeToFolder(docFolder, docType, basicScope);
        } else {
            return assignSingleValueToDocFolder(docFolder, docType, basicScope);
        }
    }

    private DocFolderDto assignSingleValueToDocFolder(DocFolder docFolder, DocTypeDto docType, BasicScope basicScope) {
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(docFolder.getId(), docFolder.getAssociations(), AssociationType.FOLDER);
        List<AssociationEntity> doctypeAssociations = AssociationsService.getDocTypeAssociation(associations);
        Long scopeId = basicScope == null ? null : basicScope.getId();
        List<AssociableEntityDto> doctypes = associationsService.getEntitiesForAssociations(doctypeAssociations);
        Map<Long, DocTypeDto> doctypesById = doctypes.stream().collect(Collectors.toMap(d -> ((DocTypeDto) d).getId(), d -> ((DocTypeDto) d)));
        Map<Long, BasicScope> scopes = associationsService.getScopesForAssociations(doctypeAssociations);

        final List<AssociationEntity> explicitDocTypeAssociations = doctypeAssociations.stream()
                .filter(dta ->
                        !dta.getImplicit() && doctypesById.get(dta.getDocTypeId()).isSingleValue() &&
                                Objects.equals(dta.getAssociationScopeId(), scopeId))
                .collect(Collectors.toList());
        final boolean sameExplicitDocTypeAlreadyExists = explicitDocTypeAssociations.stream()
                .anyMatch(dta -> dta.getDocTypeId().equals(docType.getId()));

        if (sameExplicitDocTypeAlreadyExists) { // In this case we issue a warning and return the existing doc folder
            logger.warn("DocType {} already exists, ignoring request", docType);
        } else {
            if (!explicitDocTypeAssociations.isEmpty()) {
                final AssociationEntity existingDta = explicitDocTypeAssociations.iterator().next();
                // We remove the old doc type & add the new one
                markFolderAsDeleted(docFolder.getId(), existingDta.getDocTypeId(), scopes.get(existingDta.getAssociationScopeId()));
            }
            docFolderService.assignExplicitDocTypeToFolder(docFolder.getId(), docType, basicScope);
        }

        // After receiving the correct doc folder dto, refine its associations (by scope & priority)
        final DocFolderDto docFolderDtoToReturn = docFolderService.getByIdWithAssociations(docFolder.getId());
        refineAssociations(docFolderDtoToReturn, basicScope);

        return docFolderDtoToReturn;
    }

    private DocFolderDto assignMultiValueDocTypeToFolder(DocFolder docFolder, DocTypeDto docType, BasicScope basicScope) {
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(docFolder.getId(), docFolder.getAssociations(), AssociationType.FOLDER);
        List<AssociationEntity> doctypes = AssociationsService.getDocTypeAssociation(associations);
        Long scopeId = basicScope == null ? null : basicScope.getId();
        final boolean sameDocTypeAlreadyExists = doctypes.stream()
                .anyMatch(dta ->
                        !dta.getImplicit() &&
                                dta.getDocTypeId().equals(docType.getId()) &&
                                Objects.equals(dta.getAssociationScopeId(), scopeId));
        DocFolderDto docFolderDto;
        if (sameDocTypeAlreadyExists) { // In this case issue a warning and receive the doc folder dto
            logger.warn("DocType {} already exists, ignoring request", docType);
            docFolderDto = DocFolderService.convertDocFolder(docFolder, true, true, null,
                    associations, associationsService.getScopesForAssociations(associations), associationsService.getEntitiesForAssociations(associations));
        } else { // Otherwise, assign doc type to folder
            docFolderDto = docFolderService.assignExplicitDocTypeToFolder(docFolder.getId(), docType, basicScope);
        }

        // After receiving the correct doc folder dto, refine its associations (by scope & priority)
        refineAssociations(docFolderDto, basicScope);
        return docFolderDto;
    }

    @Transactional
    public GroupDto assignDocTypeToGroup(String groupId, long docTypeId, BasicScope basicScope) {
        if (!groupLockService.getLockForGroup(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("group.operation.in-process"), BadRequestType.GROUP_LOCKED);
        }

        try {
            DocType docType = getDocType(docTypeId);
            GroupDto groupDto;
            final FileGroup fileGroup = groupsService.findById(groupId);
            Long scopeId = basicScope == null ? null : basicScope.getId();
            List<AssociationEntity> groupAssociations = AssociationsService.convertFromAssociationEntity(
                    fileGroup.getId(), fileGroup.getAssociations(), AssociationType.GROUP);

            final List<AssociationEntity> existingExplicitDocTypeAssociations = groupAssociations.stream()
                    .filter(dta -> !dta.getImplicit() &&
                            dta.getDocTypeId() != null &&
                            Objects.equals(dta.getAssociationScopeId(), scopeId))
                    .collect(Collectors.toList());

            if (existingExplicitDocTypeAssociations.stream().anyMatch(dta -> dta.getDocTypeId() == docTypeId)) {
                logger.warn("DocType {} already exists for fileGroup {}, ignoring request", docType, fileGroup);
            } else {
                if (docType.isSingleValue() && !existingExplicitDocTypeAssociations.isEmpty()) {
                    existingExplicitDocTypeAssociations.stream()
                            .filter(dta -> docTypeService.getFromCache(dta.getDocTypeId()).isSingleValue())
                            .findFirst()
                            .ifPresent(dta -> groupsService.markDocTypeAssociationAsDeleted(groupId,
                                    docTypeService.getDocTypeById(dta.getDocTypeId()),
                                    dta.getAssociationScopeId() == null ? null : scopeService.getById(dta.getAssociationScopeId())));
                }
                associationsService.createGroupAssociation(DocTypeService.convertDocTypeToDtoNoPath(docType, basicScope), groupId, scopeId, AssociableEntityType.DOC_TYPE);
            }

            groupDto = groupsService.findDtoById(groupId);
            refineAssociations(groupDto, basicScope);
            return groupDto;
        } finally {
            groupLockService.releaseLockForGroup(groupId);
        }
    }

    @Transactional
    public GroupDto removeDocTypeFromGroup(String groupId, long docTypeId, BasicScope basicScope) {
        DocType docType = getDocType(docTypeId);

        if (!groupLockService.getLockForGroup(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("group.operation.in-process"), BadRequestType.GROUP_LOCKED);
        }

        try {
            final GroupDto groupDto = groupsService.markDocTypeAssociationAsDeleted(groupId, docType, basicScope);
            refineAssociations(groupDto, basicScope);
            return groupDto;
        } finally {
            groupLockService.releaseLockForGroup(groupId);
        }
    }

    @Transactional
    public DocFolderDto removeDocTypeFromFolder(Long folderId, long docTypeId, BasicScope basicScope) {
        final DocFolderDto docFolderDto = markFolderAsDeleted(folderId, docTypeId, basicScope);
        if (docFolderDto != null) {
            refineAssociations(docFolderDto, basicScope);
        }
        return docFolderDto;
    }

    private DocFolderDto markFolderAsDeleted(Long folderId, long docTypeId, BasicScope basicScope) {
        DocTypeDto docType = docTypeService.getFromCache(docTypeId);
        AssociationEntity explicitAssociation = associationsService.findExplicitFolderAssociationByScope(folderId, docType, basicScope);
        if (explicitAssociation == null) {
            logger.debug("Can not remove: DocType #{} {} is not associated with folder id {}", docType.getId(), docType.getName(), folderId);
            return null;
        }

        associationsService.markAssociationByOriginFolderAsDeleted(docType, folderId, basicScope == null ? null : basicScope.getId(), AssociableEntityType.DOC_TYPE);
        logger.debug("Marked {} implicit docTypes as deleted", docTypeId);
        return docFolderService.getByIdWithAssociations(folderId);
    }

    @Transactional
    public DocFolderDto clearCacheReturnFolder(Long folderId) {
        entityManager.flush();
        entityManager.clear();
        return docFolderService.getByIdWithAssociations(folderId);
    }

    private DocType getDocType(long docTypeId) {
        DocType docType = docTypeService.getDocTypeById(docTypeId);
        if (docType == null) {
            throw new BadRequestException(messageHandler.getMessage("doctype.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return docType;
    }

    @Transactional
    public ClaFileDto assignDocTypeToFile(long docTypeId, long fileId, BasicScope basicScope) {
        assignDocTypeToAFile(docTypeId, fileId, basicScope);
        List<ClaFileDto> res = filesApplicationService.enrichFiles(Lists.newArrayList(fileCatService.getFileEntity(fileId)), null, true);
        return res == null || res.isEmpty() ? null : res.get(0);
    }

    @Transactional
    public ClaFileDto removeDocTypeFromFile(long docTypeId, long fileId, Long basicScope) {
        removeDocTypeFromAFile(docTypeId, fileId, basicScope);
        List<ClaFileDto> res = filesApplicationService.enrichFiles(Lists.newArrayList(fileCatService.getFileEntity(fileId)), null, true);
        if (res == null || res.isEmpty()) {
            throw new RuntimeException(messageHandler.getMessage("file.missing"));
        }
        return res.iterator().next();
    }

    @AutoAuthenticate
    @Transactional
    @Scheduled(initialDelayString = "${delete-doc-types.initial-delay-millis:3600000}",
            fixedDelayString = "${delete-doc-types.frequency-millis:18000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void deleteDocTypesIfNeeded() {
        try {
            List<DocType> deleted = docTypeService.findDeleted();
            if (deleted == null || deleted.isEmpty()) return;

            List<Long> toDelete = new ArrayList<>();
            for (DocType docType : deleted) {
                if (associationsService.countDocTypeAssociations(docType) == 0) {
                    toDelete.add(docType.getId());
                }
            }
            if (toDelete.isEmpty()) return;

            toDelete.forEach(d -> {
                List<DocType> children = docTypeService.findByParentAll(d);
                if (children == null || children.isEmpty()) {
                    docTypeService.deleteDocType(d);
                } else {
                    logger.info("cannot delete doc type {} as it still has existing children", d);
                }
            });

            logger.debug("deleted doc types {}", toDelete);
        } catch (Exception e) {
            logger.error("failed deleting doc types", e);
        }
    }

    @Transactional(readOnly = true)
    public boolean isDocTypeFileInSolr(long fileId, long docTypeId, Long basicScope) {
        DocTypeDto docType = docTypeService.getFromCache(docTypeId);
        return associationsService.isDocTypeFileInSolr(fileId, docType, basicScope);
    }

    @Transactional(readOnly = true)
    public DocTypeDto getDocTypeByName(String docTypeName) {
        logger.debug("Get docType by name {}", docTypeName);
        DocType docType = docTypeService.getDocTypeByName(docTypeName);
        return docTypeService.getFromCache(docType.getId(), docType.getScope());
    }

    @Transactional(readOnly = true)
    public DocTypeDto getDocTypeById(long id) {
        return docTypeService.getFromCache(id);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<DocTypeDto>> findDocTypesFileCount(DataSourceRequest dataSourceRequest, boolean returnAllDocTypes, BasicScope basicScope) {
        logger.debug("list DocType Counts With File Filters");
        // set page size for facet to get all values so we can aggregate them
        SolrFacetQueryResponse solrFacetQueryResponse = getForDocTypesFileCount(dataSourceRequest, basicScope);
        SolrFacetQueryResponse solrFacetQueryResponseNoFilter = null;
        if (dataSourceRequest.hasFilter()) {
            dataSourceRequest.clearFilters();
            solrFacetQueryResponseNoFilter = getForDocTypesFileCount(dataSourceRequest, basicScope);
        }
        logger.debug("list file facets by Solr field tags returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        // create page request with original size for response
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        if (returnAllDocTypes) {
            return fetchAllDocTypesAndFillCountData(solrFacetQueryResponse, pageRequest, solrFacetQueryResponseNoFilter);
        } else {
            return convertFacetFieldResultToDto(solrFacetQueryResponse, pageRequest,
                    dataSourceRequest.getSelectedItemId(), solrFacetQueryResponseNoFilter);
        }
    }

    private SolrFacetQueryResponse getForDocTypesFileCount(DataSourceRequest dataSourceRequest, BasicScope basicScope) {
        PageRequest pageRequest = PageRequest.of(0, ControllerUtils.FACET_MAX_PAGE_SIZE_FILECOUNT);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.ACTIVE_ASSOCIATIONS);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setAdditionalFilter(buildFilter(basicScope));
        solrFacetSpecification.setFacetPrefix(DocTypeDto.ID_PREFIX);
        return fileCatService.runFacetQuery(solrFacetSpecification, pageRequest);
    }

    private String getItemId(DocTypeDto item) {
        return String.valueOf(item.getId());
    }

    private FacetPage<AggregationCountItemDTO<DocTypeDto>> convertFacetFieldResultToDto(
            SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, String selectedItemId,
            SolrFacetQueryResponse solrFacetQueryResponseNoFilter) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        List<AggregationCountItemDTO<DocTypeDto>> content = Lists.newArrayList();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        long pageAggCount = 0;

        Map<String, List<FacetField.Count>> groupedDocIdentNoFilterMap = new HashMap<>();
        if (solrFacetQueryResponseNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryResponseNoFilter.getFirstResult();
            List<Pair<String, List<FacetField.Count>>> groupedDocIdentNoFilter = extractGroupedDocTypeIdentifiers(facetFieldNoFilter);
            groupedDocIdentNoFilter.forEach(p -> groupedDocIdentNoFilterMap.put(p.getKey(), p.getValue()));
        }

        //We need to convert it with the help of mysql
        //First collect the result ids to a Set (original values look like dt5.4.3.2
        List<Pair<String, List<FacetField.Count>>> groupedDocIdentifiers = extractGroupedDocTypeIdentifiers(facetField);
        List<String> docTypeIdentifiers = groupedDocIdentifiers.stream().map(Pair::getKey).collect(Collectors.toList());
        Map<String, DocType> docTypeMap = docTypeService.getDocTypesByIdentiers(docTypeIdentifiers);
        //Map<String, AggregationCountItemDTO<DocTypeDto>> identifierToAggregationCount = Maps.newHashMap();
        logger.debug("convert Facet Field result for {} elements to DocTypeDto. DB returned {} elements", docTypeIdentifiers.size(), docTypeMap.size());
        for (Pair<String, List<FacetField.Count>> pair : groupedDocIdentifiers) {
            String docTypeIdentifier = pair.getKey();
            DocType docType = docTypeMap.get(docTypeIdentifier);
            if (docType == null) {
                logger.warn("Failed to find docType by identifier {} in the database. Solr and Database are not synced.",
                        docTypeIdentifier);
                continue;
            }

            long count = pair.getValue().stream().mapToLong(FacetField.Count::getCount).sum();
            DocTypeDto docTypeDto = docTypeService.getFromCache(docType.getId(), docType.getScope());
            AggregationCountItemDTO<DocTypeDto> item = createItem(docTypeDto, count, solrFacetQueryResponseNoFilter != null);

            if (groupedDocIdentNoFilterMap.containsKey(docTypeIdentifier)) {
                long count2 = groupedDocIdentNoFilterMap.get(docTypeIdentifier).stream().mapToLong(FacetField.Count::getCount).sum();
                item.setCount2(count2);
            }

            content.add(item);
            pageAggCount += count;
        }
        // even though we must go over all facet options to get correct values
        // we should return to ui only what was requested
        return ControllerUtils.getRelevantResultPage(content, pageRequest, selectedItemId,
                this::getItemId, pageAggCount, solrFacetQueryResponse.getNumFound());
    }

    private AggregationCountItemDTO<DocTypeDto> createItem(DocTypeDto docTypeDto, long itemCount, boolean dataFiltered) {
        return dataFiltered ? new AggregationCountItemDTO<>(docTypeDto, itemCount, 0, 0, 0)
                : new AggregationCountItemDTO<>(docTypeDto, itemCount);
    }

    private List<Pair<String, List<FacetField.Count>>> extractGroupedDocTypeIdentifiers(FacetField facetField) {
        List<Pair<String, List<FacetField.Count>>> result = new ArrayList<>(facetField.getValues().size());
        List<FacetField.Count> values = facetField.getValues();
        Map<String, Integer> nameByIndex = Maps.newHashMap();
        for (FacetField.Count value : values) {
            String docTypeIdentifier = docTypeIdentifier(value);
            Integer index = nameByIndex.get(docTypeIdentifier);
            if (index == null) {
                index = result.size();
                nameByIndex.put(docTypeIdentifier, index);
                List<FacetField.Count> counts = Lists.newLinkedList();
                result.add(Pair.of(docTypeIdentifier, counts));
            }
            result.get(index).getValue().add(value);
        }
        return result;
    }

    private FacetPage<AggregationCountItemDTO<DocTypeDto>> fetchAllDocTypesAndFillCountData(
            SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, SolrFacetQueryResponse solrFacetQueryResponseNoFilter) {
        logger.debug("fetch all docTypes - and fill in counts for the relevant docTypes");
        List<DocType> docTypes = docTypeService.getDocTypes();
        List<AggregationCountItemDTO<DocTypeDto>> content = new ArrayList<>();
        //Content map is used to update count2 (with children)
        Map<Long, AggregationCountItemDTO<DocTypeDto>> contentMap = new HashMap<>();
        long pageAggCount = 0;
        FacetField facetField = solrFacetQueryResponse.getFirstResult();

        Map<String, List<FacetField.Count>> facetValuesMapNoFilter = new HashMap<>();
        if (solrFacetQueryResponseNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryResponseNoFilter.getFirstResult();
            facetValuesMapNoFilter = facetFieldNoFilter.getValues().stream()
                    .collect(Collectors.groupingBy(this::docTypeIdentifier));
        }

        //Fetch all group ids from the database
        Map<String, List<FacetField.Count>> facetValuesMap = facetField.getValues().stream()
                .collect(Collectors.groupingBy(this::docTypeIdentifier));
        for (DocType docType : docTypes) {
            Long itemCount = 0L;
            String docTypeIdentifier = docType.getDocTypeIdentifier();
            if (facetValuesMap.containsKey(docTypeIdentifier)) {
                List<FacetField.Count> counts = facetValuesMap.get(docTypeIdentifier);
                itemCount += counts.stream().mapToLong(FacetField.Count::getCount).sum();
            }
            Long itemCount2 = null;
            if (facetValuesMapNoFilter.containsKey(docTypeIdentifier)) {
                List<FacetField.Count> counts = facetValuesMapNoFilter.get(docTypeIdentifier);
                itemCount2 = counts.stream().mapToLong(FacetField.Count::getCount).sum();
            }

            DocTypeDto docTypeDto = docTypeService.getFromCache(docType.getId(), docType.getScope());
            AggregationCountItemDTO<DocTypeDto> item = contentMap.get(docTypeDto.getId());
            if (item == null) {
                item = createItem(docTypeDto, itemCount, solrFacetQueryResponseNoFilter != null);
                content.add(item);
                contentMap.put(docTypeDto.getId(), item);
            } else {
                item.incrementCount(itemCount);
            }

            if (itemCount2 != null) {
                item.incrementCount2(itemCount2);
            }
            pageAggCount += itemCount;
        }

        fillDocTypeHierarchicalCounters(content, contentMap);
        return new FacetPage<>(content, pageRequest, pageAggCount, solrFacetQueryResponse.getNumFound());
    }

    private String docTypeIdentifier(FacetField.Count count) {
        String name = count.getName();
        String[] vars = name.split("\\" + FileCatUtils.CATEGORY_SEPARATOR);
        int start = (vars[0] + FileCatUtils.CATEGORY_SEPARATOR + vars[1]).length();
        // Case scope tags keep identifier
        if (name.contains("..")) { // Single value field
            return vars[0] + name.substring(start, name.indexOf(".."));
        } else { // MultiValue field
            return vars[0] + name.substring(start);
        }
    }

    private void fillDocTypeHierarchicalCounters(List<AggregationCountItemDTO<DocTypeDto>> content, Map<Long, AggregationCountItemDTO<DocTypeDto>> contentMap) {
        logger.trace("update tree child sum counters");
        for (AggregationCountItemDTO<DocTypeDto> docTypeDtoAggregationCountItemDTO : content) {
            long directCount = docTypeDtoAggregationCountItemDTO.getCount();
            long unfilteredDirect = docTypeDtoAggregationCountItemDTO.getCount2();
            DocTypeDto item = docTypeDtoAggregationCountItemDTO.getItem();

            //update all parents
            if (item.getParents() != null) {
                for (Long parentId : item.getParents()) {
                    AggregationCountItemDTO<DocTypeDto> parent = contentMap.get(parentId);
                    if (parent != null) {
                        parent.incrementSubtreeCount(directCount);
                        if (unfilteredDirect > 0) {
                            parent.incrementSubtreeCountUnfiltered(unfilteredDirect);
                        }
                    } else {
                        logger.warn("cant find parent object {} for item {}", parentId, item.getId());
                    }
                }
            }
        }
    }

    @AutoAuthenticate
    @Transactional
    public void applyTagsOnDocTypesToSolr(long maxAssociationsToProcess) {
        logger.debug("Apply tags of doc types to Solr");
        int processed = 0;
        int processedInPage;
        do {
            try {
                executionManagerService.lockFileCat("applyTagsOnDocTypesToSolr");
                processedInPage = applyTagsOnDocTypesToSolrByPage(associationApplySize);
                processed += processedInPage;
            } finally {
                executionManagerService.releaseFileCatLock();
            }
        }
        while (processedInPage > 0 && processed < maxAssociationsToProcess);
    }

    private int applyTagsOnDocTypesToSolrByPage(int associationApplySize) {
        final int[] processed = {0};
        Page<FileTagSetting> fileTagSettings = docTypeService.findDirtyFileTagSettings(associationApplySize);
        logger.info("About to update {} file tag settings to update", fileTagSettings.getTotalElements());
        fileTagSettings.forEach(fts -> {
            processed[0] += applyDocTypeTagsByTagSource(
                    fts,
                    associationApplySize,
                    FileTagSource.BUSINESS_ID,
                    association -> TagPriority.FILE_GROUP,
                    association -> Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, association.getGroupId()));
            processed[0] += applyDocTypeTagsByTagSource(
                    fts,
                    associationApplySize,
                    FileTagSource.FOLDER_EXPLICIT,
                    association -> TagPriority.folder(association.getOriginDepthFromRoot()),
                    association -> {
                        DocFolder docFolder = docFolderService.getById(association.getOriginFolderId());
                        List<String> decodeFolderParentsInfo = docFolder.getParentsInfo();
                        String folderParentsInfo = decodeFolderParentsInfo.get(decodeFolderParentsInfo.size() - 1);
                        return Criteria.create(CatFileFieldType.FOLDER_IDS, SolrOperator.EQ, folderParentsInfo);
                    });
            processed[0] += applyDocTypeTagsByTagSource(
                    fts,
                    associationApplySize,
                    FileTagSource.FILE,
                    association -> TagPriority.FILE,
                    association -> {
                        Long fileId = association.getClaFileId();
                        return Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.EQ, fileId);
                    });
            if (fts.isDeleted()) {
                docTypeService.deleteFileTagSettings(fts);
            } else {
                fts.setDirty(false);
                fileTagSettingService.saveFileTagSetting(fts);
            }
            fileCatService.softCommitNoWaitFlush();
        });
        return processed[0];
    }

    public List<DocTypeDto> assignDocTypeTagsByTagType(Table<Long, Long, List<Long>> docTypeTagSettingTable) {
        List<DocTypeDto> res = docTypeService.assignDocTypeTagsByTagType(docTypeTagSettingTable);
        associationsService.applyDocTypesTagsToSolr();
        return res;
    }

    private int applyDocTypeTagsByTagSource(FileTagSetting fts,
                                            int associationApplySize,
                                            FileTagSource fileTagSource,
                                            Function<DocTypeAssociation, TagPriority> tagPrioritySupplier,
                                            Function<DocTypeAssociation, Criteria> filterQuerySupplier) {
        FileTag fileTag = fts.getFileTag();
        Pageable pageRequest = PageRequest.of(0, associationApplySize);
        Page<DocTypeAssociation> associations = associationsService.findDocTypeAssociations(
                fts.getDocType(), fileTagSource, pageRequest.getOffset(), pageRequest.getPageSize());

        logger.debug("About to apply (tagId:{}, tagTypeId:{}) for {} to solr according to {} doc type associations",
                fileTag.getId(), fileTag.getType().getId(), fileTagSource, associations.getTotalElements());
        while (associations.hasContent()) {
            logger.debug("Apply (tagId:{}, tagTypeId:{} for {}, (page: {}/{}, size: {}) to solr",
                    fileTag.getId(), fileTag.getType().getId(), fileTagSource, associations.getNumber() + 1,
                    associations.getTotalPages(), associations.getNumberOfElements());
            associations.forEach(association -> {
                Long scopeId = association.getScope() == null ? null : association.getScope().getId();
                //Apply to solr
                try {
                    String solrKey = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fileTag, scopeId, tagPrioritySupplier.apply(association));

                    associationsService.updateScopedTagByQuery(
                            filterQuerySupplier.apply(association),
                            !association.isDeleted() && !fts.isDeleted(),
                            fts.getFileTag(),
                            solrKey, SolrCommitType.NONE);
                } catch (Exception e) {
                    logger.error("Failed to update association for {}", association, e);
                }
            });
            pageRequest = pageRequest.next();
            associations = associationsService.findDocTypeAssociations(
                    fts.getDocType(), fileTagSource, pageRequest.getOffset(), pageRequest.getPageSize());
        }
        return associations.getNumberOfElements();

    }

    @AutoAuthenticate
    @Transactional
    public void removeDocTypeIdentifierFromFiles(Long docTypeId, String identifier) {

        DocType docType = docTypeService.getDocTypeById(docTypeId);
        DocTypeDto docTypeDto = docTypeService.getFromCache(docTypeId, docType.getScope());
        List<DocTypeAssociation> associations = associationsService.getAssociationFromSolr(docType);
        docTypeDto.setDocTypeIdentifier(identifier);

        // for each association delete the old identifier of the doc type from solr
        associations.forEach(association -> {
            if (association.getAssociationSource().equals(FileTagSource.BUSINESS_ID)) {
                if (association.getGroupId() != null) {
                    associationsService.updateDocTypeToCategoryFiles(association.getGroupId(), CatFileFieldType.USER_GROUP_ID,
                            docTypeDto, commitEachAssociation, false, association.getScope(), TagPriority.FILE_GROUP);
                }
            } else if (association.getAssociationSource().equals(FileTagSource.FOLDER_EXPLICIT)) {
                Long docFolderId = association.getOriginFolderId();
                DocFolder docFolder = docFolderService.getById(docFolderId);
                List<String> decodeFolderParentsInfo = docFolder.getParentsInfo();
                String folderParentsInfo = decodeFolderParentsInfo.get(decodeFolderParentsInfo.size() - 1);
                associationsService.updateDocTypeToCategoryFiles(folderParentsInfo, CatFileFieldType.FOLDER_IDS,
                        docTypeDto, commitEachAssociation, false, association.getScope(), TagPriority.folder(docFolder.getDepthFromRoot()));
                //} else if (association.getAssociationSource().equals(FileTagSource.FOLDER_IMPLICIT)) {
                // ignore handled via explicit
            } else if (association.getAssociationSource().equals(FileTagSource.FILE)) {
                Long fileId = association.getClaFileId();
                associationsService.updateDocTypeToCategoryFiles(String.valueOf(fileId), CatFileFieldType.FILE_ID,
                        docTypeDto, commitEachAssociation, false, association.getScope(), TagPriority.FILE);
            }
        });

        if (!commitEachAssociation) {
            fileCatService.softCommitNoWaitFlush();
        }
    }

    @Transactional
    public DocTypeDto createDocType(DocTypeDto docTypeDto) {
        if (!uiSupport) {
            docTypeDto.setSingleValue(configDefaultSingleValue);
        }
        DocType docType = docTypeService.createDocType(docTypeDto);
        return docTypeService.convertDocTypeToDto(docType, docType.getScope());
    }

    @Transactional
    public DocTypeDto updateDocType(long id, DocTypeDto docTypeDto) {
        DocType docType = docTypeService.updateDocType(id, docTypeDto);
        return docTypeService.getFromCache(id, docType.getScope());
    }

    @Transactional
    public DocTypeDto moveDocType(long id, Long newParentId) {
        DocType oldDocType = docTypeService.getDocTypeById(id);
        String oldIdentifier = oldDocType.getDocTypeIdentifier();
        logger.debug("Move docType {} to sit under {}", id, newParentId);
        DocType docType = docTypeService.moveDocType(id, newParentId);
        logger.debug("Mark all associations with docType as dirty {}", docType.getId());
        associationsService.moveDocTypeSolrFiles(docType.getDocTypeIdentifier(), oldIdentifier);
        return docTypeService.getFromCache(docType.getId(), docType.getScope());
    }

    private Page<DocTypeDto> convertDocTypeToDto(PageRequest pageRequest, Page<DocType> allByAccount) {
        List<DocTypeDto> content = new ArrayList<>();
        allByAccount.forEach(docType -> content.add(docTypeService.getFromCache(docType.getId(), docType.getScope())));
        return new PageImpl<>(content, pageRequest, allByAccount.getTotalElements());
    }

    public Page<DocTypeDto> getAllDocTypesAsDto(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<DocType> allDocTypes = docTypeService.getAllDocTypes(pageRequest);
        Set<Long> scopeIds = scopeService.resolveScopeIds(null);
        Iterator<? extends ScopedEntity> iterator = allDocTypes.iterator();
        while (iterator.hasNext()) {
            ScopedEntity scopedEntity = iterator.next();
            //noinspection SuspiciousMethodCalls
            if (scopedEntity.getScope() != null && !scopeIds.contains(scopedEntity.getScope().getId())) {
                iterator.remove();
            }
        }
        return convertDocTypeToDto(pageRequest, allDocTypes);
    }

    @Transactional(readOnly = true)
    public DocTypeDetailsDto getDocTypeDetails(long docTypeId, boolean recursive) {
        DocType docType = docTypeService.getDocTypeById(docTypeId);
        long totalAssociations = associationsService.countDocTypeAssociations(docType);
        Map<FileTagSource, Long> fileTagSourceLongMap = associationsService.countDocTypeAssociationsBySource(docType);
        DocTypeDetailsDto docTypeDetailsDto = new DocTypeDetailsDto();
        docTypeDetailsDto.setDocTypeDto(docTypeService.getFromCache(docType.getId(), docType.getScope()));
        docTypeDetailsDto.setTotalAssociations(totalAssociations);
        Long fileAssociations = fileTagSourceLongMap.get(FileTagSource.FILE);
        if (fileAssociations != null) {
            docTypeDetailsDto.setFileAssociations(fileAssociations);
        }
        Long groupAssociations = fileTagSourceLongMap.get(FileTagSource.BUSINESS_ID);
        if (groupAssociations != null) {
            docTypeDetailsDto.setGroupAssociations(groupAssociations);
        }
        Long folderExplicit = fileTagSourceLongMap.get(FileTagSource.FOLDER_EXPLICIT);
        if (folderExplicit != null) {
            docTypeDetailsDto.setExplicitFolderAssociations(folderExplicit);
        }
        if (recursive) {
            List<DocType> docTypes = docTypeService.getChildrenDocTypes(docTypeId);
            for (DocType child : docTypes) {
                DocTypeDetailsDto childDetails = getDocTypeDetails(child.getId(), true);
                docTypeDetailsDto.addToTotalAssociations(childDetails.getTotalAssociations());
                docTypeDetailsDto.addToFileAssociations(childDetails.getFileAssociations());
                docTypeDetailsDto.addToGroupAssociations(childDetails.getTotalAssociations());
            }
        }
        return docTypeDetailsDto;
    }

    @Transactional
    public void removeAllDocTypeAssociations(long docTypeId, boolean recursive, boolean isUserRequested) {
        if (recursive) {
            logger.info("remove All DocType Associations from children of docType {}", docTypeId);
            List<DocType> docTypes = docTypeService.getChildrenDocTypes(docTypeId);
            for (DocType child : docTypes) {
                removeAllDocTypeAssociations(child.getId(), true, isUserRequested);
            }
        }
        logger.info("remove All DocType Associations from docType {}", docTypeId);
        DocType docType = getDocType(docTypeId);
        associationsService.deleteAllForDocType(docType);
    }

    @Transactional
    public void deleteDocType(long docTypeId, boolean recursive) {
        DocType docType = docTypeService.getDocTypeById(docTypeId);
        validateDocTypeAssociationsDoNotExist(docTypeId);

        if (recursive) {
            logger.debug("Delete all docType children of {}", docType);
            List<DocType> docTypes = docTypeService.getChildrenDocTypes(docTypeId);
            for (DocType child : docTypes) {
                validateDocTypeAssociationsDoNotExist(child.getId());
                docTypeService.deleteDocType(child.getId());
            }
        }
        logger.debug("Delete DocType {}", docType);
        docTypeService.deleteDocType(docTypeId);
    }

    private void validateDocTypeAssociationsDoNotExist(long docTypeId) {
        if (associationsService.docTypeAssociationExists(docTypeId)) {
            throw new BadRequestException(messageHandler.getMessage("doctype.delete.associations"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }
    }

    @Transactional
    public void markDeletedDocType(long docTypeId, boolean recursive) {
        DocType docType = docTypeService.getDocTypeById(docTypeId);
        validateDocTypeAssociationsDoNotExist(docTypeId);

        if (recursive) {
            logger.debug("Delete all docType children of {}", docType);
            List<DocType> docTypes = docTypeService.getChildrenDocTypes(docTypeId);
            for (DocType child : docTypes) {
                validateDocTypeAssociationsDoNotExist(child.getId());
                markDeletedDocType(child.getId(), recursive);
            }
        }
        logger.debug("Delete DocType {}", docType);
        docType.setDeleted(true);
    }

    private void assignDocTypeToAFile(long docTypeId, long fileId, BasicScope basicScope) {
        DocType docType = docTypeService.getDocTypeById(docTypeId);
        ClaFile claFile = fileCatService.getFileObject(fileId);

        List<AssociationEntity> associations = AssociationsService.getDocTypeAssociation(
                AssociationsService.convertFromAssociationEntity(claFile.getId(), claFile.getAssociations(), AssociationType.FILE));

        Long scopeId = basicScope == null ? null : basicScope.getId();
        final List<AssociationEntity> explicitDocTypeAssociations = associations == null ? new ArrayList<>() :
                associations.stream()
                        .filter(dta ->
                                Objects.equals(dta.getAssociationScopeId(), scopeId))
                        .collect(Collectors.toList());

        if (explicitDocTypeAssociations.stream().anyMatch(dta -> dta.getDocTypeId() == docTypeId)) {
            logger.warn("DocType {} already assigned to file {}, ignoring request", docType, fileId);

        } else {
            ClaFile file = fileCatService.getFileObject(fileId);
            if (docType.isSingleValue()) {
                explicitDocTypeAssociations.stream()
                        .filter(dta -> docTypeService.getFromCache(dta.getDocTypeId()).isSingleValue())
                        .findFirst()
                        .ifPresent(dta -> associationsService.removeFileTagAssociationFromFile(file,
                                docTypeService.getFromCache(dta.getDocTypeId()), dta.getAssociationScopeId()));
            }
            associationsService.assignDocTypeToFile(DocTypeService.convertDocTypeToDtoNoPath(docType, basicScope), file, basicScope, SolrCommitType.SOFT_NOWAIT);
            associationsService.updateTagsFromDocType(fileId, docType, scopeId, true);
            fileCatService.commitToSolr();
        }
    }

    private void removeDocTypeFromAFile(long docTypeId, long fileId, Long basicScopeId) {
        DocTypeDto docType = docTypeService.getFromCache(docTypeId);
        ClaFile file = fileCatService.getFileObject(fileId);
        associationsService.removeFileTagAssociationFromFile(file, docType, basicScopeId);
        fileCatService.commitToSolr();
    }
}
