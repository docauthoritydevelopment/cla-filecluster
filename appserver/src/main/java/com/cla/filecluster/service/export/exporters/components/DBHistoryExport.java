package com.cla.filecluster.service.export.exporters.components;

import com.cla.filecluster.service.export.ExportType;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/17/2019
 */
@Component
public class DBHistoryExport extends ComponentHistoryExport {

    private final static String[] header = {
            TIMESTAMP, DISK_SPACE};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DB_HISTORY);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

}