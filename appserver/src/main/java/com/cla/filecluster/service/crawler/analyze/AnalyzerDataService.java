package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.FileGroupsFieldType;
import com.cla.common.constants.GrpHelperFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.progress.ProgressTracker;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.pv.GroupUnificationHelper;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.jpa.GroupUnificationHelperRepository;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.repository.solr.SolrGrpHelperRepository;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.operation.SubGroupService;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.constants.FileGroupsFieldType.*;


@Component
public class AnalyzerDataService {
    private static final Logger logger = LoggerFactory.getLogger(AnalyzerDataService.class);

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private GroupUnificationHelperRepository groupUnificationHelperRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrGrpHelperRepository solrGrpHelperRepository;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private SubGroupService subGroupService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private AssociationsService associationsService;

    private AtomicInteger missedMatchesCounter = new AtomicInteger();

    @Value("${bonecp.driverClass:}")
    private String driverClass;

    @Value("${groupNaming.recalculate.threshold:0.2}")
    private double recalculateThreshold;

    @Value("${groupsUpdate.batch.size:5000}")
    private int groupsUpdateBatchSize;

    @Value("${analyze.missedMatches.slqDeadlockRetries:2}")
    private int slqDeadlockRetries;

    @Value("${group.process.byDirty:true}")
    private boolean groupProcessByDirty;

    @Value("${analyze.group-join.reject-size-threshold:1000}")
    private long rejectGroupJoinSizeThreshold;


    @Value("${analyze.group-join.strategy-class:com.cla.filecluster.service.crawler.analyze.AdvancedElectionStrategy}")
    private String electionStrategyClass;

    @Value("${fileGrouping.refinement-rules-active:true}")
    private boolean refinementRulesActive;

    // Indicates whether to truncate the group_unification_helper table instead of marking all entries as processed
    @Value("${table.group_unification_helper.truncate:true}")
    private boolean truncateGroupUnificationHelperTable;

    @Value("${solr.mismatches.field-update.interim-logging.ratio:10000}")
    private int fieldMismatchesInterimLoggingRatio;

    @Value("${solr.mismatches.field-update.commit-once.catfiles:true}")
    private boolean fieldMismatchesCommitOnceCatfiles;

    @Value("${solr.mismatches.field-update.commit-once.grphelper:true}")
    private boolean fieldMismatchesCommitOnceGrphelper;

    @Value("${solr.missmatches.query-group-ids-bulk:10000}")
    private int queryGroupIdsBulk;

    private JoinGroupsElectionStrategy electionStrategy;

    @PostConstruct
    public void init() {
        try {
            Class<?> strategyClass = Class.forName(electionStrategyClass);
            electionStrategy = (JoinGroupsElectionStrategy) strategyClass.newInstance();
            electionStrategy.setRejectSizeThreshold(rejectGroupJoinSizeThreshold);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            throw new RuntimeException("Failed to configure group election strategy.", e);
        }
    }

    public Collection<GroupUnificationHelper> replaceGroups(Collection<SolrFileGroupEntity> groups) {
        if (logger.isTraceEnabled()) {
            logger.trace("Joining {} groups: {}", groups.size(), groups);
        }

        // elect the target group that will receive the other groups' members
        JoinGroupsElectionResult result = electionStrategy.electTargetGroup(groups, fileGroupRepository, fileTagService);
        SolrFileGroupEntity electedGroup = result.getElectedGroup();
        String electedGroupId = electedGroup.getId();

        // get the set of groups to be merged into the target group and make the switch
        Collection<GroupUnificationHelper> unificationsToProcess = result.getAcceptedGroups().stream()
                .map(group -> new GroupUnificationHelper(group.getId(), electedGroupId))
                .collect(Collectors.toSet());

        if (logger.isTraceEnabled()) {
            List<AssociationEntity> electedGroupAssociations = AssociationsService.convertFromAssociationEntity(
                    electedGroupId, electedGroup.getAssociations(), AssociationType.GROUP);
            List<AssociationEntity> electedGroupDocTypeAssoc = AssociationsService.getDocTypeAssociation(electedGroupAssociations);
            logger.trace("Grouping unification: {} groups will be joined with elected group {} size={} #TAGs={} #FR={} %DT={}",
                    result.getAcceptedGroups().size(),
                    electedGroupId,
                    electedGroup.getNumOfFiles(),
                    electionStrategy.requiresFullGroupObjects() ? electedGroupAssociations == null ? 0 : electedGroupAssociations.size() : "N/A",
                    electionStrategy.requiresFullGroupObjects() ? electedGroupAssociations == null ? "None" : AssociationUtils.getFunctionalRoles(electedGroupAssociations).size() : "N/A",
                    electionStrategy.requiresFullGroupObjects() ? electedGroupDocTypeAssoc == null ? 0 : electedGroupDocTypeAssoc.size() : "N/A"
            );
        }


        // get the set of rejected groups that will not be merged into the target group
        // and create a warning message
        Collection<SolrFileGroupEntity> rejectedGroups = result.getRejectedGroups();
        if (rejectedGroups.size() > 0) {
            logger.warn("Grouping rejection: {} groups were rejected and will not be merged with elected group {}",
                    rejectedGroups.size(), electedGroupId);
            if (logger.isDebugEnabled()) {
                final StringBuilder rejectionSummary = new StringBuilder();
                rejectedGroups.forEach(group -> {
                    List<AssociationEntity> groupAssociations = AssociationsService.convertFromAssociationEntity(
                            electedGroupId, electedGroup.getAssociations(), AssociationType.GROUP);
                    List<AssociationEntity> groupDocTypeAssoc = AssociationsService.getDocTypeAssociation(groupAssociations);
                    rejectionSummary
                            .append("group ID: ")
                            .append(group.getId())
                            .append(" (size ")
                            .append(group.getNumOfFiles())
                            .append(")")
                            .append(" #TAGs=").append(electionStrategy.requiresFullGroupObjects() ? (groupAssociations==null?-1:groupAssociations.size()) : "N/A")
                            .append(" #FR=").append(electionStrategy.requiresFullGroupObjects() ? AssociationUtils.getFunctionalRoles(groupAssociations).size() : "N/A")
                            .append(" #DT=").append(electionStrategy.requiresFullGroupObjects() ? groupDocTypeAssoc.size() : "N/A")
                            .append("; ");
                        }
                );

                List<AssociationEntity> electedGroupAssociations = AssociationsService.convertFromAssociationEntity(
                        electedGroupId, electedGroup.getAssociations(), AssociationType.GROUP);
                List<AssociationEntity> electedGroupDocTypeAssoc = AssociationsService.getDocTypeAssociation(electedGroupAssociations);
                logger.debug("Grouping rejection: {} groups were rejected and will not be merged with elected group {} size={} #TAGs={} #FR={} #DT={}: \n{}",
                        rejectedGroups.size(),
                        electedGroupId,
                        electedGroup.getNumOfFiles(),
                        electionStrategy.requiresFullGroupObjects() ? (electedGroupAssociations==null?-1:electedGroupAssociations.size()) : "N/A",
                        electionStrategy.requiresFullGroupObjects() ? AssociationUtils.getFunctionalRoles(electedGroupAssociations).size() : "N/A",
                        electionStrategy.requiresFullGroupObjects() ? electedGroupDocTypeAssoc.size() : "N/A",
                        rejectionSummary.toString()
                );
            }
        }

        return unificationsToProcess;
    }

    void updateAssociationsDirtyForGroups(Long jobId) {
        final long totalGroups = fileGroupService.countNonDeletedDirtyFileGroupIds();
        if (totalGroups > 0) {
            jobManagerService.setOpMsg(jobId, "Groups mark dirty associations");

            String cursor = "*";
            boolean lastPage;
            long associations = 0;
            do {
                Query query = Query.create()
                        .addFilterWithCriteria(DIRTY, SolrOperator.EQ,true)
                        .addFilterWithCriteria(DELETED, SolrOperator.EQ,false)
                        .setRows(groupsUpdateBatchSize).setCursorMark(cursor).addField(ID).addField(GROUP_ASSOCIATIONS)
                        .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

                QueryResponse resp = fileGroupRepository.runQuery(query.build());
                cursor = resp.getNextCursorMark();
                if (resp.getResults().size() > 0) {
                    for (SolrDocument doc : resp.getResults()) {
                        String groupId = (String)doc.getFieldValue(ID.getSolrName());
                        List<String> groupAssoc = (List<String>)doc.getFieldValue(GROUP_ASSOCIATIONS.getSolrName());
                        List<AssociationEntity> groupAssociation = AssociationsService.convertFromAssociationEntity(
                                groupId, groupAssoc, AssociationType.GROUP);
                        groupAssociation.forEach(entity ->
                            associationsService.createGroupApplyOperation(groupId, entity, SolrFieldOp.ADD, null));
                        associations += groupAssociation.size();
                    }
                }
                lastPage = resp.getResults().size() < groupsUpdateBatchSize;
            } while (!lastPage);
            logger.debug("updated to dirty {} associations of {} dirty groups", associations, totalGroups);
        }
    }

    void updateNumOfFilesInAnalysisGroups(Long jobId) {
        logger.info("Start atomic num-of-files update in groups table...");
        int totalUpdates1 = 0;
        int totalUpdates2 = 0;
        if (driverClass.equals("org.h2.Driver")) {
//	    	totalUpdates1 = fileGroupRepository.updateNumOfFilesAtomic();
//	    	logger.info("Updated num-of-files in groups table for {} items.", totalUpdates1);
//	    	totalUpdates2 = fileGroupRepository.removeOrphans();
//			logger.info("Removed {} orphan groups (marked deleted)", totalUpdates2);
            logger.error("Unsupported ...");
        } else {
            jobManagerService.setOpMsg(jobId, "Groups statistic calculation");
            // TODO: update only groups marked dirty

            // Update num of files
            jobManagerService.setOpMsg(jobId, "Group size processing");

            String cursor = "*";
            boolean lastPage;
            int pageSize = 10000;
            do {
                Query query = Query.create()
                        .addFilterWithCriteria(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt())
                        .setRows(pageSize).setCursorMark(cursor).addField(ID)
                        .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

                if (groupProcessByDirty) {
                    query.addFilterWithCriteria(Criteria.create(DIRTY, SolrOperator.EQ, true));
                } else {
                    query.addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false));
                }

                QueryResponse resp = fileGroupRepository.runQuery(query.build());
                cursor = resp.getNextCursorMark();
                long updated = 0;
                if (resp.getResults().size() > 0) {
                    List<String> groupIds = resp.getResults().stream().map(
                            doc -> (String)doc.getFieldValue(ID.getSolrName())).collect(Collectors.toList());
                    updated = fileGroupService.updateNumOfFilesForAnalysisGroups(groupIds);
                    totalUpdates1 += updated;
                }
                logger.debug("Loop: updated num-of-files in groups table for {} items. total={}.", updated, totalUpdates1);
                lastPage = resp.getResults().size() < pageSize;
            } while (!lastPage);

            // Remove orphans
            jobManagerService.setOpMsg(jobId, "Identify deleted groups");
			/*do {
				long updated = groupProcessByDirty ?
						fileGroupService.removeDirtyOrphans(offset, groupsUpdateBatchSize) :
						fileGroupService.removeOrphansMySql(offset, groupsUpdateBatchSize);
				totalUpdates2 += updated;
				offset += groupsUpdateBatchSize;
				logger.debug("Loop: Removed {} orphan groups (marked deleted). total={}. Left {}", updated, totalUpdates2, Long.max(0L,totalGroups-offset));
			} while (offset < totalGroups);*/
            logger.info("Removed {} orphan groups (marked deleted)", totalUpdates2);
            if (groupProcessByDirty) {
                jobManagerService.setOpMsg(jobId, "Groups statistics processing cleanup...");
                int cleared = fileGroupService.clearDirtyForDeleted();
                logger.debug("Cleared dirty flag for {} groups", cleared);
            }
            jobManagerService.setOpMsg(jobId, "Groups statistics processing completed");
        }
    }

    void updateSampleFileInGroups(Long jobId) {
        logger.info("Start atomic sample member update in groups table...");
        int totalUpdates1 = 0;
        if (driverClass.equals("org.h2.Driver")) {
            logger.error("Unsupported ...");
        } else {
            jobManagerService.setOpMsg(jobId, "Groups samples update...");
            String cursor = "*";
            boolean lastPage;
            int pageSize = 10000;
            do {
                Query query = Query.create().addFilterWithCriteria(
                        Criteria.or(Criteria.create(DIRTY, SolrOperator.EQ, true), Criteria.create(FIRST_MEMBER_ID, SolrOperator.MISSING, null))
                        ).addFilterWithCriteria(Criteria.create(NUM_OF_FILES, SolrOperator.GT, 0))
                        .addFilterWithCriteria(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt())
                        .setRows(pageSize).setCursorMark(cursor).addField(ID)
                        .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

                QueryResponse resp = fileGroupRepository.runQuery(query.build());
                cursor = resp.getNextCursorMark();
                long updated = 0;
                if (resp.getResults().size() > 0) {
                    List<String> groupIds = resp.getResults().stream().map(
                            doc -> (String)doc.getFieldValue(ID.getSolrName())).collect(Collectors.toList());
                    updated = fileGroupService.updateSampleFileDirtyAtomic(groupIds);
                    totalUpdates1 += updated;
                }
                logger.debug("Loop: updated sample member in groups table for {} items. total={}.", updated, totalUpdates1);
                lastPage = resp.getResults().size() < pageSize;
            } while (!lastPage);
            jobManagerService.setOpMsg(jobId, "Groups samples update completed");
        }
    }

    void clearGroupsDirty(final boolean beforeGroupNaming, Long jobId, long toDate) {
        logger.info("Start clear groups dirty flags...");

        if (driverClass.equals("org.h2.Driver")) {
            logger.error("Unsupported ...");
        } else {
            jobManagerService.setOpMsg(jobId, "Groups statistic calculation");
            final long totalGroups = beforeGroupNaming ?
                    fileGroupService.countDirtyExceptGroupNaming(recalculateThreshold) :
                    fileGroupService.countDirty();
            logger.info("Need to clear {} groups dirty flags{}", totalGroups, (beforeGroupNaming ? " (beforeGroupNaming)" : ""));

            // Update num of files
            jobManagerService.setOpMsg(jobId, "Groups statistics processing cleanup...");

            long updated = beforeGroupNaming ?
                    fileGroupService.clearDirtyAtomicExceptGroupNaming(recalculateThreshold, toDate) :
                    fileGroupService.clearDirtyAtomicRepository(toDate);
            logger.debug("Loop: clear  groups dirty flag for {} items. out of={}.", updated, totalGroups);

            jobManagerService.setOpMsg(jobId, "Groups statistics processing cleanup completed");
            logger.debug("Clear groups dirty flags done for {}{}", updated, (beforeGroupNaming ? " (beforeGroupNaming)" : ""));
        }
    }

    public int incMissedMatchesCounter() {
        return missedMatchesCounter.incrementAndGet();
    }

    public int getMissedMatchesCounter() {
        return missedMatchesCounter.get();
    }

    @Transactional
    @AutoAuthenticate
    public void processGroupSwitches(Collection<GroupUnificationHelper> unprocessedUnifications, ProgressTracker progressTracker) {
        groupsService.aquireGroupModificationLock();
        try {

            // get all the unprocessed unifications
            //List<GroupUnificationHelper> unprocessedUnifications = groupUnificationHelperRepository.findAllUnprocessed();

            // set the new analysis group to ContentMetadata according to unprocessed GroupUnificationHelpers,
            // copy user group to ContentMetadata according to unprocessed GroupUnificationHelpers,
            // mark ContentMetadata as groupDirty = true
            contentMetadataService.changeGroupIdUnifyAll(unprocessedUnifications);
            logger.debug("Done: changeGroupIdUnifyAll  for {} unprocessedUnifications",
                    unprocessedUnifications.size());

            // set new user groups to ContentMetadatas that were moved from one group to another
            // the new user groups are taken from the parentGroup field of the target group
            filesDataPersistenceService.updateUserGroupInContentForJoinedGroups();
            logger.info("Done: updateUserGroupInContentForJoinedGroups");
            // get the number of ContentMetadatas marked as groupDirty = true //todo: what do we need this for?
            final long totalDirty = contentMetadataService.countGroupDirty();

            // copy group and user group from ContentMetadata to ClaFile
            filesDataPersistenceService.copyGroupsFromContentToFile(totalDirty);
            logger.debug("Done: copyGroupsFromContentToFile for {} dirty groups", totalDirty);


            progressTracker.setCurStageEstimatedTotal(unprocessedUnifications.size());
            updateGroupsInCatFilesAndGrpHelper(unprocessedUnifications);
            logger.debug("Done: updateGroupsInCatFilesAndGrpHelper for {} groups unification", unprocessedUnifications.size());

            List<String> newGidList = unprocessedUnifications.stream()
                    .map(u -> u.getNewGroupId()).distinct()
                    .collect(Collectors.toList());
            int markedGroups = newGidList.isEmpty() ? 0 : fileGroupService.markUnifiedGroupsByIds(newGidList);
            logger.debug("Done: marked {} unified groups as dirty", markedGroups);
        } finally {
            //TODO - narrow the lock to the relevant function only
            groupsService.releaseGroupModificationLock();
        }
    }

    private void updateGroupsInCatFilesAndGrpHelper(Collection<GroupUnificationHelper> unprocessedUnifications) {
        if (unprocessedUnifications.isEmpty()) {
            return;
        }
        // find all the groups that will receive new members
        Set<String> targetGroups = unprocessedUnifications.stream()
                .map(GroupUnificationHelper::getNewGroupId)
                .collect(Collectors.toSet());

        // map target groups to their parent user groups
        Map<String, String> groupsToParents;
        if (targetGroups != null && targetGroups.size() > 0) {
            groupsToParents = fileGroupService.getGroupIdsWithParentIds(targetGroups);
        } else {
            groupsToParents = Maps.newHashMap();
        }

        // Find cat files to update
        Map<String, GroupUnificationHelper> unprocessedUnificationsByOriginId = unprocessedUnifications.stream()
                .collect(Collectors.toMap(u -> u.getOrigGroupId(), u -> u));

        Iterables.partition(unprocessedUnificationsByOriginId.keySet(), queryGroupIdsBulk)
                .forEach(groupIds -> {
                    int updated = saveUserGroupAndAnalysisGroupUpdates(groupIds, groupsToParents, unprocessedUnificationsByOriginId,
                            query -> fileCatService.findAllByQuery(query, CatFileFieldType.ID, Sort.Direction.ASC),
                            updates -> fileCatService.saveAll(updates), true);
                    logger.debug("Done: saveUserGroupAndAnalysisGroupUpdates for {} unprocessedUnifications, updated {} cat files",
                            unprocessedUnifications.size(), updated);

                    updated = saveUserGroupAndAnalysisGroupUpdates(groupIds, groupsToParents, unprocessedUnificationsByOriginId,
                            query -> solrGrpHelperRepository.findAllByQuery(query, GrpHelperFieldType.ID, Sort.Direction.ASC),
                            updates -> solrGrpHelperRepository.saveAll(updates), false);
                    logger.debug("Done: saveUserGroupAndAnalysisGroupUpdates for {} unprocessedUnifications, updated {} grp helpers",
                            unprocessedUnifications.size(), updated);
                });

    }


    private int saveUserGroupAndAnalysisGroupUpdates(List<String> groupIds, Map<String, String> groupsToParents,
                                                     Map<String, GroupUnificationHelper> unprocessedUnificationsByOriginId,
                                                     Function<SolrQuery, List<SolrDocument>> queryExecutor,
                                                     Consumer<List<SolrInputDocument>> updater, boolean dateUpdate) {
        SolrQuery solrQuery = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.IN, groupIds))
                .addField(CatFileFieldType.ID)
                .addField(CatFileFieldType.ANALYSIS_GROUP_ID)
                .build();
        long start = System.currentTimeMillis();
        List<SolrDocument> documents = queryExecutor.apply(solrQuery);
        logger.debug("Find {} files for {} groupIds in {} ms", documents.size()
                , groupIds.size(), System.currentTimeMillis() - start);

        if (documents.size() == 0) {
            logger.debug("No results returned for updating, skipping request");
            return 0;
        }

        List<SolrInputDocument> updates = documents.stream()
                .map(d -> {
                    String id = (String) d.getFieldValue(CatFileFieldType.ID.getSolrName());
                    String analysisGroupId = (String) d.get(CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName());
                    GroupUnificationHelper groupUnificationHelper = unprocessedUnificationsByOriginId.get(analysisGroupId);
                    String parentId = groupsToParents.get(groupUnificationHelper.getNewGroupId());
                    String userGroupId = Strings.isNullOrEmpty(parentId) ?
                            groupUnificationHelper.getNewGroupId() : parentId;
                    AtomicUpdateBuilder builder = AtomicUpdateBuilder.create()
                            .setId(CatFileFieldType.ID, id)
                            .addModifier(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.SET, analysisGroupId)
                            .addModifier(CatFileFieldType.USER_GROUP_ID, SolrOperator.SET, userGroupId);
                    if (dateUpdate) {
                        builder.addModifier(CatFileFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis());
                    }
                    return builder.build();
                })
                .collect(Collectors.toList());


        updater.accept(updates);

        return updates.size();
    }

    /**
     * In AdvancedElectionStrategy we make sure that we never return a sub group but the
     * raw analysis group. so after the process is done we need to use the rules to assign the file
     * and contents to the sub groups according to the rules
     * This method goes over all raw analysis groups and if they have files, reassign them to
     * a sub group
     */
    public void handleRawAnalysisGroups() { //ProgressTracker progressTracker TODO ??

        if (!refinementRulesActive) return;

        groupsService.aquireGroupModificationLock();

        try {
            Set<String> groupsWithRefinement = groupsService.getAllAnalysisGroupsWithRefinement();
            //progressTracker.setCurStageEstimatedTotal(groupsWithRefinement.size());
            for (String group : groupsWithRefinement) {
                subGroupService.moveContentsFromRawToSubGroupsByRules(group);
                //progressTracker.incrementProgress(1L);
            }
        } catch (Exception e) {
            logger.error("error during handling of analysis groups with refinement");
        } finally {
            groupsService.releaseGroupModificationLock();
        }
    }

    /**
     * Depending on {@link #truncateGroupUnificationHelperTable}, either truncates the group_unification_helper table
     * or marks all its records with processed=true
     */
    private void clearGroupUnificationHelperTable() {
        if (truncateGroupUnificationHelperTable) {
            groupUnificationHelperRepository.truncateGroupUnificationHelper();
            logger.debug("Truncated group_unification_helper");
        } else {
            int marked = groupUnificationHelperRepository.markAllProcessed();
            logger.debug("Marked {} groupUnificationHelper records as 'processed'.", marked);
        }
    }


    void updateNumOfFilesInUserGroups() {
        logger.info("Update Number of files in User groups");
        fileGroupService.updateNumOfFilesInUserGroupsRepository();
        logger.info("Update Number of files in User groups finished");
    }

    public JoinGroupsElectionStrategy getElectionStrategy() {
        return electionStrategy;
    }
}
