package com.cla.filecluster.service.export.exporters.security.template;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public interface RoleTemplateHeaderFields {

    String NAME = "Name";
    String DESCRIPTION = "Description";
    String ROLES = "System roles";
}
