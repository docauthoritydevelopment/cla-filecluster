package com.cla.filecluster.service.export.exporters.file.mail;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/5/2019
 */
public class MailAddressDtoMixin {

    @JsonProperty(MailHeaderFields.ADDRESS)
    private String address;

    @JsonProperty(MailHeaderFields.DOMAIN)
    private String domain;
}
