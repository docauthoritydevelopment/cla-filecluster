package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UpdateDatePartitionEnricher extends DatePartitionEnricher{

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.LAST_MODIFIED, DatePartitionEnricher.DATE_PARTITION_DUMMY_PREFIX);
    }

    public String getFilterFieldName(){
        return CatFileFieldType.LAST_MODIFIED.getSolrName();
    }

}
