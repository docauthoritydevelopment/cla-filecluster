package com.cla.filecluster.service.util;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

public class SyncRequestLockMap<V> {
	private ConcurrentHashMap<String,CompletableFuture<V>> lockMap = new ConcurrentHashMap<>();
	public String addRequestToLockMap(CompletableFuture<V> completableFuture) {
		String uniqueID = UUID.randomUUID().toString();
		lockMap.put(uniqueID, completableFuture);

		return uniqueID;
	}

	public CompletableFuture<V> getCompletableFutureLockByRequestId(String requestId) {
		return lockMap.get(requestId);
	}

	public CompletableFuture<V> pop(String requestId) {
		return lockMap.remove(requestId);
	}

	public void removeRequestFromLockMap(String requestId) {
		lockMap.remove(requestId);
	}
}
