package com.cla.filecluster.service.chain;

/**
 * Chain of responsibility entry
 *
 * @see Chain
 */
public interface ChainLink<T extends ChainContext> {
    /**
     * Execute this link code
     * @param context chain context
     * @return if true, the chain should continue
     */
    boolean handle(T context);

    /**
     * Whether this link should act on the specified context
     * @param context chain context
     * @return true if this link should act
     */
    boolean shouldHandle(T context);
}
