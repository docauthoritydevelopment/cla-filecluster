package com.cla.filecluster.service.progress;

import com.cla.common.domain.dto.export.FileExportRequestDto;
import com.cla.common.domain.dto.progress.ProgressStatus;
import com.cla.common.domain.dto.progress.ProgressStatusDto;
import com.cla.connector.utils.Pair;
import com.google.common.base.Suppliers;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class  ProgressBean {

    public final static String REQUEST_PAYLOAD_KEY = "FileExportRequestDto";

    private static final long PROGRESS_UPDATE_TIME = 200; // millis

    private final String progressId;
    private ProgressBean father;
    private final ProgressType progressType;
    private String owner;
    private AtomicInteger percentage;
    private int compositePercentage;
    private Supplier<Integer> compositePercentageSupplier;
    private volatile boolean done;
    private volatile boolean failed;
    private Throwable exception;
    private volatile String message;
    private final Map<String, Object> payloads;
    private final long startTime;
    private volatile long lastActivityTime;

    // all sub-progresses composing this progress: ProgressBean -> wight of composing progress
    private final ConcurrentHashMap<ProgressBean, Float> composingProgresses;

    ProgressBean(ProgressType type, int initialPercentage, String initialMessage, Pair<String, Object> initialPayload) {
        this(type, ProgressMessage.of(initialPercentage, initialMessage, initialPayload, false));
    }

    ProgressBean(ProgressType type, ProgressMessage initialMessage) {
        this.progressId = UUID.randomUUID().toString();
        this.father = null;
        this.progressType = type;
        this.startTime = System.currentTimeMillis();
        this.lastActivityTime = startTime;
        this.percentage = new AtomicInteger(initialMessage.getPercentage());
        this.message = initialMessage.getMessage();
        this.payloads = new ConcurrentHashMap<>();
        if (initialMessage.getPayload() != null) {
            addPayload(initialMessage.getPayload());
        }
        this.composingProgresses = new ConcurrentHashMap<>();
        this.compositePercentageSupplier =
                Suppliers. memoizeWithExpiration(this::calcCompositePercentage, PROGRESS_UPDATE_TIME, TimeUnit.MILLISECONDS);
        this.done = false;
        this.failed = false;
        this.exception = null;
    }

    /**
     * simple reference copier
     * @param other the other object to copy all references from.
     */
    ProgressBean(ProgressBean other) {
        this.progressId = other.progressId;
        this.father = other.father;
        this.progressType = other.progressType;
        this.owner = other.owner;
        this.percentage = other.percentage;
        this.compositePercentage = other.compositePercentage;
        this.compositePercentageSupplier =
                Suppliers. memoizeWithExpiration(this::calcCompositePercentage, PROGRESS_UPDATE_TIME, TimeUnit.MILLISECONDS);
        this.done = other.done;
        this.failed = other.failed;
        this.exception = other.exception;
        this.message = other.message;
        this.payloads = other.payloads;
        this.startTime = other.startTime;
        this.lastActivityTime = other.lastActivityTime;
        this.composingProgresses = other.composingProgresses;
    }

    public void addPayload(String key, Object payload) {
        updateLastActivity();
        payloads.put(key, payload);
    }

    public void addPayload(Pair<String, Object> payload) {
        updateLastActivity();
        payloads.put(payload.getKey(), payload.getValue());
    }

    public void addPayload(String key, Object payload, String message) {
        updateLastActivity();
        setMessage(message);

        payloads.put(key, payload);
    }

    public int addPercentage (int percentage, String message) {
        updateLastActivity();
        setMessage(message);

        return this.percentage.addAndGet(percentage);
    }

    public void addComposingProgress(ProgressBean progress, float weight) {
        if (progress == null) {
            throw new NullPointerException("input progress cannot be null");
        }

        updateLastActivity();

        // turn the input progress back to regular if provided an immutable one
        if (progress instanceof ImmutableProgressBean) {
            progress = getOriginal();
        }

        progress.setFather(this);

        composingProgresses.put(progress, weight);
        unsetDone();
    }

    /**
     * find the original progress from the composing progresses (the self composing progress)
     * @return
     */
    private ProgressBean getOriginal() {
        return composingProgresses.searchKeys(100, (progress) -> {
            if (progress.getProgressId().equals(this.getProgressId())) {
                return progress;
            }
            return null;
        });
    }

    private void unsetDone() {
        updateLastActivity();
        this.done = false;

        // if this progress has composing child progresses,
        // set the local progress to 0 to enable calculation of the child progresses.
        if (!composingProgresses.isEmpty()) {
            this.percentage.set(0);
        }

        if (father != null) {
            father.unsetDone();
        }
    }

    public void setDone() {
        updateLastActivity();
        this.done = true;
        this.percentage.set(100);
    }

    public void setFailed (Throwable exception) {
        updateLastActivity();
        this.failed = true;
        this.exception = exception;
    }

    public void setFailed (String message, Throwable exception) {
        updateLastActivity();
        setMessage(message);
        this.failed = true;
        this.exception = exception;
    }

    private void setFather(ProgressBean father) {
        this.father = father;
    }

    public void setPercentage (int percentage, String message) {
        updateLastActivity();
        setMessage(message);

        this.percentage.set(percentage);
    }

    public void addProgressMessage(ProgressMessage progressMessage) {
        addPercentage(progressMessage.getPercentage(), progressMessage.getMessage());
        if (progressMessage.getPayload() != null) {
            addPayload(progressMessage.getPayload());
        }
    }

    public void setProgressMessage(ProgressMessage progressMessage) {
        setPercentage(progressMessage.getPercentage(), progressMessage.getMessage());

        if (progressMessage.getPayload() != null) {
            addPayload(progressMessage.getPayload());
        }

        if (progressMessage.isDone()) {
            setDone();
        }
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getProgressId() {
        return progressId;
    }

    public ProgressType getProgressType() {
        return progressType;
    }

    public int getPercentage() {
        return percentage.get();
    }

    public String getMessage() {
        return message;
    }

    public Map<String, Object> getPayloads() {
        return Collections.unmodifiableMap(payloads);
    }

    public Object getPayload(String key) {
        return payloads.get(key);
    }

    public long getStartTime() {
        return startTime;
    }

    public long getLastActivityTime() {
        return lastActivityTime;
    }

    public Map<ImmutableProgressBean, Float> getComposingProgresses() {
        return composingProgresses.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        e -> ImmutableProgressBean.of(e.getKey()), // key mapper
                        Map.Entry::getValue)); // value mapper (identity)
    }

    public int getCompositePercentage() {
        return compositePercentageSupplier.get();
    }

    public boolean isDone() {
        // compositePercentageSupplier.get(); // invoke calculation if time interval allows it
        return done;
    }

    public ProgressBean getFather() {
        return father;
    }

    private void setMessage(String message) {
        if (message != null) {
            this.message = message;
        }
    }

    private void updateLastActivity() {
        this.lastActivityTime = System.currentTimeMillis();
    }

    private int calcCompositePercentage() {

        int localPercentage = this.percentage.get();
        // if this progress has no children, or a percentage was set on this progress,
        // then use the local progress and don't calculate the children's progresses.
        if (composingProgresses.isEmpty() || localPercentage > 0) {
            compositePercentage = this.percentage.get();
        } else {
            float totalWeight = composingProgresses.reduceValues(100,
                    (total, curr) -> total + curr);

            float totalPercentage = composingProgresses.reduce(100,
                    (progress, weight) -> progress.getCompositePercentage() * weight,
                    (total, curr) -> total + curr);

            compositePercentage = Math.min(100, Math.round(totalPercentage / totalWeight));
        }

        return compositePercentage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProgressBean that = (ProgressBean) o;
        return Objects.equals(progressId, that.progressId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(progressId);
    }

    public ProgressStatusDto toStatusDto() {
        ProgressStatusDto dto = new ProgressStatusDto();
        FileExportRequestDto request = (FileExportRequestDto) getPayload(REQUEST_PAYLOAD_KEY);
        if (request != null) {
            dto.setUserParams(request.getUserParams());
        }
        dto.setCreatedBy(owner); // TODO complete (maybe put it in the user-params?)
        dto.setCreatedDate(this.startTime);
        dto.setId(this.progressId);
        dto.setProgressType(getProgressType().name());
        dto.setProgress(this.getCompositePercentage());
        dto.setMessage(getMessage());
        dto.setStatus(resolveStatus());
        dto.setLastUpdateDate(lastActivityTime);
        dto.setStatusChangedDate(lastActivityTime);
        dto.setCurrentTimeStamp(System.currentTimeMillis());
        return dto;
    }

    private ProgressStatus resolveStatus() {
        if (failed) {
            return ProgressStatus.FAILED;
        }
        if (getCompositePercentage() == 0) {
            return  ProgressStatus.NEW;
        }
        if (isDone()) {
            return  ProgressStatus.DONE;
        }
        return ProgressStatus.IN_PROGRESS;
    }
}
