package com.cla.filecluster.service.categories;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * files in group - get/update data
 *
 * Created by: yael
 * Created on: 5/22/2018
 */
@Service
public class ClaFileGroupOperations {

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private ExecutionManagerService executionManagerService;

    public QueryResponse runQuery(SolrQuery query) {
        return solrFileCatRepository.runQuery(query);
    }

    public List<SolrFileEntity> find(SolrQuery query) {
        return solrFileCatRepository.find(query);
    }

    public Long getFileNumForGroup(CatFileFieldType groupField, String groupId) {
        Query query = Query.create().addFilterWithCriteria(groupField, SolrOperator.EQ, groupId)
                .addFilterWithCriteria(CatFileFieldType.DELETED, SolrOperator.EQ, "false")
        .addFacetField(groupField)
        .setFacetMinCount(1)
        .setRows(0);
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        FacetField field = queryResponse.getFacetFields().get(0);
        List<FacetField.Count> values = field.getValues();
        return values == null || values.isEmpty() ? 0 : values.get(0).getCount();
    }

    public Map<String, Long> getFileNumForFieldByIds(CatFileFieldType fieldToSearch, Collection<String> idsForField) {
        Query query = Query.create().addFilterWithCriteria(fieldToSearch, SolrOperator.IN, idsForField)
                .addFilterWithCriteria(CatFileFieldType.DELETED, SolrOperator.EQ, "false")
                .addFacetField(fieldToSearch)
                .setFacetMinCount(1)
                .setFacetLimit(idsForField.size()+1)
                .setRows(0);
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());

        Map<String, Long> result = new TreeMap<>();
        if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
            FacetField field = queryResponse.getFacetFields().get(0);
            List<FacetField.Count> values = field.getValues();
            for (FacetField.Count c : values) {
                String fieldToSearchId = c.getName();
                long count = c.getCount();
                result.put(fieldToSearchId, count);
            }
        }
        return result;
    }

    public Map<String, Long> getFirstMemberForAnalysisGroups(List<String> groupIds) {
        Query query = Query.create()
                .addFilterWithCriteria(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.IN, groupIds)
                .addField(CatFileFieldType.FILE_ID)
                .setGroupField(CatFileFieldType.ANALYSIS_GROUP_ID)
                .setGroupLimit(1);
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Map<String, Long> result = new TreeMap<>();
        if (queryResponse.getGroupResponse() != null && queryResponse.getGroupResponse().getValues() != null) {
            queryResponse.getGroupResponse().getValues().forEach(g -> {
                g.getValues().forEach(group -> {
                    String id = group.getGroupValue();
                    Long fileId = (Long)group.getResult().get(0).get(CatFileFieldType.FILE_ID.getSolrName());
                    result.put(id, fileId);
                });
            });
        }
        return result;
    }

    /**
     * In SOLR CatFile core update the userGroupId of the files that belong to child groups
     *
     * @param userGroupId    new userGroupId to set
     * @param analysisGroups IDs of the analysis groups of the target files
     */
    public void updateUserGroupsCatFile(String userGroupId, Collection<String> analysisGroups, boolean commit) {
        executionManagerService.lockFileCatOrThrow(60000, "updateUserGroupsCatFile");
        try {
            String query = CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName() + ":";
            if (analysisGroups.size() > 1) {
                query += analysisGroups.stream().map(groupId -> "\"" + groupId + "\"")
                        .collect(Collectors.joining(",", "(", ")"));
            } else {
                query += analysisGroups.stream().findFirst().orElse("");
            }
            solrFileCatRepository.updateByQuery(query, CatFileFieldType.USER_GROUP_ID,
                    userGroupId, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
        } finally {
            executionManagerService.releaseFileCatLock();
        }
    }

    /**
     * in solr set new analysis group by old analysis groups
     */
    public void updateAnalysisGroupsCatFile(String newGroupId, Collection<String> oldGroupIds, boolean commit){
        executionManagerService.lockFileCatOrThrow(60000, "updateAnalysisGroupsCatFile");
        try {
            String query = CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName() + ":";
            if (oldGroupIds.size() > 1) {
                query += oldGroupIds.stream().map(groupId -> "\"" + groupId + "\"")
                        .collect(Collectors.joining(",", "(",  ")"));
            }
            else{
                query += oldGroupIds.stream().findFirst().orElse("");
            }
            solrFileCatRepository.updateByQuery(query, CatFileFieldType.ANALYSIS_GROUP_ID,
                    newGroupId, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
        }
        finally {
            executionManagerService.releaseFileCatLock();
        }
    }

    /**
     * In SOLR CatFile core update the userGroupId of the files that belong to child groups
     *
     * @param userGroupId    new userGroupId to set
     * @param analysisGroups IDs of the analysis groups of the target files
     */
    public void updateUserGroupsCatFileByOldUserGroup(String userGroupId, Collection<String> analysisGroups, boolean commit) {
        executionManagerService.lockFileCatOrThrow(60000, "updateUserGroupsCatFileByOldUserGroup");
        try {
            String query = CatFileFieldType.USER_GROUP_ID.getSolrName() + ":";
            if (analysisGroups.size() > 1) {
                query += analysisGroups.stream().map(groupId -> "\"" + groupId + "\"")
                        .collect(Collectors.joining(",", "(", ")"));
            } else {
                query += analysisGroups.stream().findFirst().orElse("");
            }
            solrFileCatRepository.updateByQuery(query, CatFileFieldType.USER_GROUP_ID,
                    userGroupId, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
        } finally {
            executionManagerService.releaseFileCatLock();
        }
    }

    /**
     * in solr set new analysis group by content ids
     */
    public void updateAnalysisGroupsCatFileByContents(String newGroupId, Collection<Long> contentIds, boolean commit){
        executionManagerService.lockFileCatOrThrow(60000, "updateAnalysisGroupsCatFileByContents");
        try {
            String query = CatFileFieldType.CONTENT_ID.getSolrName() + ":";
            if (contentIds.size() > 1) {
                query += contentIds.stream().map(groupId -> "\"" + groupId + "\"")
                        .collect(Collectors.joining(",", "(",  ")"));
            }
            else{
                query += contentIds.stream().findFirst().orElse(0L);
            }
            solrFileCatRepository.updateByQuery(query, CatFileFieldType.ANALYSIS_GROUP_ID,
                    newGroupId, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
        }
        finally {
            executionManagerService.releaseFileCatLock();
        }
    }

    /**
     * in solr set new user group by content ids and old user group
     */
    public void updateUserGroupsCatFileByContents(String oldGroupId, String newGroupId, Collection<Long> contentIds, boolean commit){
        executionManagerService.lockFileCatOrThrow(60000, "updateUserGroupsCatFileByContents");
        try {
            String query = CatFileFieldType.CONTENT_ID.getSolrName() + ":";
            if (contentIds.size() > 1) {
                query += contentIds.stream().map(groupId -> "\"" + groupId + "\"")
                        .collect(Collectors.joining(",", "(",  ")"));
            }
            else{
                query += contentIds.stream().findFirst().orElse(0L);
            }
            String oldGroupFilter = CatFileFieldType.USER_GROUP_ID.getSolrName() + ":" + oldGroupId;
            solrFileCatRepository.updateByQuery(query, Collections.singletonList(oldGroupFilter), CatFileFieldType.USER_GROUP_ID,
                    newGroupId, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
        }
        finally {
            executionManagerService.releaseFileCatLock();
        }
    }
}
