package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.security.CompositeRoleDto;
import com.cla.common.domain.dto.security.RoleTemplateDto;
import com.cla.common.domain.dto.security.RoleTemplateType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.RoleTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Role template holds the individual roles connected to a composite role
 * The point is that we don't have to duplicate the role list for similar roles on different folders
 * For example if there are several accountants each having access to different parts of the system but the same roles apply
 *
 * Created by: yael
 * Created on: 11/30/2017
 */
@RestController
@RequestMapping("/api/roletemplate")
public class RoleTemplateApiController {

    private final static Logger logger = LoggerFactory.getLogger(RoleTemplateApiController.class);

    @Autowired
    private RoleTemplateService roleTemplateService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<RoleTemplateDto> listRoleTemplateDto(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        RoleTemplateType type = RoleTemplateType.valueOf(params.getOrDefault("type", RoleTemplateType.NONE.name()).toUpperCase());
        return roleTemplateService.listRoleTemplates(dataSourceRequest, type);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<RoleTemplateDto> getRoleTemplateByIds(@RequestParam Map<String,String> params) {
        String templateIdsStr = params.get("templateIds");
        logger.debug("Get role template Ids: {}",templateIdsStr);
        List<Long> templateIds = Arrays.stream(templateIdsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        return roleTemplateService.getRoleTemplatesByIds(templateIds);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public RoleTemplateDto createRoleTemplate(@RequestBody RoleTemplateDto roleTemplateDto) {
        RoleTemplateDto roleTemplate = roleTemplateService.createRoleTemplate(roleTemplateDto);
        eventAuditService.audit(AuditType.ROLE_TEMPLATE,"Create roleTemplate", AuditAction.CREATE, RoleTemplateDto.class,roleTemplateDto.getId(),roleTemplateDto);
        return roleTemplate;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRoleTemplate(@PathVariable long id) {
        RoleTemplateDto roleTemplate = getRoleTemplate(id);
        roleTemplateService.deleteRoleTemplate(id);
        eventAuditService.audit(AuditType.ROLE_TEMPLATE,"Delete roleTemplate", AuditAction.DELETE, RoleTemplateDto.class, id, roleTemplate);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public RoleTemplateDto updateRoleTemplate(@PathVariable Long id, @RequestBody RoleTemplateDto roleTemplateDto) {
        RoleTemplateDto template = roleTemplateService.updateRoleTemplate(roleTemplateDto);
        eventAuditService.audit(AuditType.ROLE_TEMPLATE,"Update roleTemplate", AuditAction.UPDATE, RoleTemplateDto.class,roleTemplateDto.getId(),roleTemplateDto);
        return template;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public RoleTemplateDto getRoleTemplate(@PathVariable long id) {
        RoleTemplateDto roleTemplate = roleTemplateService.getRoleTemplate(id);
        if (roleTemplate == null) {
            throw new BadRequestException(messageHandler.getMessage("role-template.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return roleTemplate;
    }

    @RequestMapping(value = "/roles/{id}",method = RequestMethod.GET)
    public List<CompositeRoleDto> getRolesForRoleTemplate(@PathVariable long id) {
        return roleTemplateService.rolesForTemplate(id);
    }
}
