package com.cla.filecluster.service.crawler.pv;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.pv.*;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.service.crawler.analyze.ContentGroupsCache;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.pv.ContentGroupDetails;
import com.cla.filecluster.service.files.pv.ContentIdGroupsPool;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SimilarityMapper {
    private static final Logger logger = LoggerFactory.getLogger(SimilarityMapper.class);

    // All similarity parameters are arrays of the following meaning: [0] - default value; [1] - low maturity level; [2] medium ML; [3] high ML.

    @Value("${fileGrouping.filterGroupCandidatesSublistSize:1000}")
    private int filterGroupCandidatesSublistSize;

    @Value("${fileGrouping.filter-group-candidates.limit-excel-candidates.active:false}")
    private boolean isFilterGroupLimitExcelCandidates;

    @Value("${fileGrouping.filter-group-candidates.limit-excel-candidates.random-selection:true}")
    private boolean isFilterGroupLimitExcelCandidatesRandomSelection;

    @Value("${fileGrouping.filter-group-candidates.limit-excel-candidates.log-dropped:false}")
    private boolean isFilterGroupLimitExcelCandidatesLog;

    @Value("${fileGrouping.filter-group-candidates.limit-excel-candidates.threshold:500}")
    private int filterGroupExcelCandidatesThreshold;

    @Value("${fileGrouping.excel.returnMatches:true}")
    private boolean analyzeWorkReturnMatches;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private ContentGroupsCache contentGroupsCache;

    @Autowired
    private FileGroupsPoolManager fileGroupsPoolManager;

    @Value("${filter.same.group.candidates:true}")
    private boolean filterSameGroupCandidates;

    @Autowired
    private SimilarityHelper similarityHelper;

    private static long filterSameGroupCandidatesCount = 0;

    private TimeSource timeSource = new TimeSource();

    public void assignGroupToMatchedCandidates(Long taskId, final ContentMetadataDto contentMetadataDto, final Collection<Long> matchesCandidates,
                                               SimilaritySimulationContext simulationContext) {
        UUID inputDocumentGroup = similarityHelper.getContentGroupIdUUID(contentMetadataDto.getContentId());
        // find all the candidates that have no group or their group is different from the analysis group of the current content
        // if the current content doesn't have group - return all the candidates
        Collection<Long> filteredMatchedCandidates = filterGroupedAndDeletedCandidates(matchesCandidates, contentMetadataDto, simulationContext);
        contentGroupsCache.addContentsToCache(filteredMatchedCandidates, taskId);
        ContentIdGroupsPool contentIdGroupsPool = fileGroupsPoolManager.createContentPool(filteredMatchedCandidates);
        for (final Long candidateId : filteredMatchedCandidates) {
            if (candidateId == contentMetadataDto.getContentId()) {
                logger.error("Unable to assign group to self on candidate {}", candidateId);
                continue;
            }
            //Analyze element in sublist
            final GroupingResult groupingResult =
                    assignGroupToCandidate(taskId, contentMetadataDto, candidateId,
                            contentIdGroupsPool, inputDocumentGroup, simulationContext);
            if (groupingResult != null && groupingResult.getSourceFileGroup() != null) {
                //This is the group that we're assigning everyone
                inputDocumentGroup = groupingResult.getSourceFileGroup();
            }
        }
    }

    private Collection<Long> filterGroupedAndDeletedCandidates(Collection<Long> groupCandidates,
                                                               ContentMetadataDto contentMetadataDto,
                                                               SimilaritySimulationContext simulationContext) {
        AnalyzeType analyzeType = (simulationContext == null) ? AnalyzeType.STANDARD : simulationContext.getAnalyzeType();

        if (!filterSameGroupCandidates || AnalyzeType.SIMULATION == analyzeType) {
            return groupCandidates;
        }
        String analysisGroupId = contentMetadataService.getContentGroupId(contentMetadataDto.getContentId());
        if (analysisGroupId == null) {
            return groupCandidates;
        }
        Iterable<List<Long>> subLists = Iterables.partition(groupCandidates, filterGroupCandidatesSublistSize);
        Collection<Long> result = new ArrayList<>();
        for (List<Long> subList : subLists) {
            Collection<String> subListResult = contentMetadataService.filterNonEmptyByGroup(subList, analysisGroupId);
            if (subListResult != null) {
                result.addAll(subListResult.stream().map(Long::valueOf).collect(Collectors.toSet()));
            }
        }
        logger.debug("Filtered group candidates - from {} to {} on {}", groupCandidates.size(), result.size(), analysisGroupId);
        return result;
    }

    /**
     * @param contentMetadataDto  content metadata currently being processed
     * @param candidateId         ID of similarity candidate content metadata
     * @param contentIdGroupsPool cache of content IDs to group IDs
     * @param inputDocumentGroup  UUID of the group, to which the content metadata that is currently being processed belongs
     * @param simulationContext   context for analysis simulations
     * @return GroupingResult
     */
    private GroupingResult assignGroupToCandidate(Long taskId, ContentMetadataDto contentMetadataDto, Long candidateId,
                                                  ContentIdGroupsPool contentIdGroupsPool, UUID inputDocumentGroup,
                                                  SimilaritySimulationContext simulationContext) {
        AnalyzeType analyzeType = (simulationContext == null) ? AnalyzeType.STANDARD : simulationContext.getAnalyzeType();
        ContentGroupDetails contentGroupDetails = contentIdGroupsPool.getContentGroupId(candidateId);
        String candidateGroupId = (contentGroupDetails == null) ? null : contentGroupDetails.getGroupId();
        if (contentGroupDetails != null && AnalyzeHint.EXCLUDED.equals(contentGroupDetails.getAnalyzeHint())) {
            return GroupingResult.falseGroupingResult("?", 0);
        }
        final String simCalc = "?";
        GroupingResult groupingResult = null;
        if (analyzeType == AnalyzeType.SIMULATION) {
            groupingResult = new GroupingResult("?", 1, '+', true);
        } else if (analyzeType == AnalyzeType.STANDARD) {
            groupingResult = similarityHelper.putContentMetadataInGroup(taskId, inputDocumentGroup, candidateGroupId, contentMetadataDto, candidateId);
            groupingResult.setSimCalc(simCalc);
            groupingResult.setSimCalc2(1);
        }

        return groupingResult;
    }

    public int getNewlyGroupedCounter() {
        return similarityHelper.getNewlyGroupedCounter();
    }


    public int getMismatchedPregroupCounter() {
        return similarityHelper.getMismatchedPregroupCounter();
    }

    public List<Long> matchExcelWorkbooks(Long taskId, final ContentMetadataDto contentMetadataDto, Collection<Long> workBookCandidates, SimilaritySimulationContext simulationContext) {
        List<Long> matchedWorkbooks = new ArrayList<>();
        UUID inputDocumentGroup = similarityHelper.getContentGroupIdUUID(contentMetadataDto.getContentId());
        final ContentIdGroupsPool contentIdGroupsPool = fileGroupsPoolManager.createContentPool(workBookCandidates);
        for (Long workBookCandidate : workBookCandidates) {
            ContentGroupDetails doc2ContentGroupDetails = contentIdGroupsPool.getContentGroupId(workBookCandidate);
            GroupingResult groupingResult = matchWorkbook(taskId, contentMetadataDto, inputDocumentGroup, workBookCandidate, doc2ContentGroupDetails, simulationContext);
            if (groupingResult != null && groupingResult.getSourceFileGroup() != null) {
                inputDocumentGroup = groupingResult.getSourceFileGroup();
                if (analyzeWorkReturnMatches) {
                    matchedWorkbooks.add(workBookCandidate);
                }
            }
        }
        return matchedWorkbooks;
    }

    public Collection<Long> filterExcelSimilarityCandidates(Collection<Long> workBookCandidates,
                                                            ContentMetadataDto contentMetadataDto,
                                                            SimilaritySimulationContext simulationContext) {
        List<Long> keys = new ArrayList<>();
        String analysisGroupId = contentMetadataService.getContentGroupId(contentMetadataDto.getContentId());
        if (analysisGroupId == null) {
            if (isFilterGroupLimitExcelCandidates && filterGroupExcelCandidatesThreshold < workBookCandidates.size()) {
                keys.addAll(workBookCandidates);
                logger.debug("Filtered group candidates: not groupped - got {} ({})", workBookCandidates.size(),
                        (isFilterGroupLimitExcelCandidates && filterGroupExcelCandidatesThreshold < keys.size()) ? filterGroupExcelCandidatesThreshold : "-");
            } else {
                logger.debug("Filtered group candidates: not groupped - keep all {}", workBookCandidates.size());
                return workBookCandidates;
            }
        } else {
            // Perform the filter by group in pages
            Iterable<List<Long>> subLists = Iterables.partition(workBookCandidates, filterGroupCandidatesSublistSize);
            if (simulationContext != null && simulationContext.getAnalyzeType() == AnalyzeType.SIMULATION) {
                keys.addAll(workBookCandidates);
            } else {
                for (List<Long> subList : subLists) {
                    Collection<String> subListResult = filterSameGroupCandidates(analysisGroupId, subList);
                    if (subListResult != null) {
                        keys.addAll(subListResult.stream().map(Long::valueOf).collect(Collectors.toSet()));
                    }
                }
                logger.debug("Filtered group candidates - from {} to {} ({})", workBookCandidates.size(), keys.size(),
                        (isFilterGroupLimitExcelCandidates && filterGroupExcelCandidatesThreshold < keys.size()) ? filterGroupExcelCandidatesThreshold : "-");
            }
        }
        if (isFilterGroupLimitExcelCandidates && filterGroupExcelCandidatesThreshold < keys.size()) {
            // Limit number of candidates ...
            List<Long> dropped = new ArrayList<>();
            List<Long> subKeys = trimList(keys, filterGroupExcelCandidatesThreshold, dropped, isFilterGroupLimitExcelCandidatesRandomSelection);
            String droppedCandidates = "";
            if (isFilterGroupLimitExcelCandidatesLog && logger.isDebugEnabled()) {
                droppedCandidates = dropped.stream()
                        .map(Object::toString)
                        .limit(100)
                        .collect(Collectors.joining(",", ": (", ")"));
            }
            logger.debug("Further limit group candidates to {}. Dropping {}{}",
                    subKeys.size(), keys.size() - subKeys.size(), droppedCandidates);
            keys = subKeys;
        }
        return keys;
    }

    private GroupingResult matchWorkbook(Long taskId, ContentMetadataDto contentMetadataDto,
                                         UUID inputDocumentGroup,
                                         Long workBookCandidateId,
                                         ContentGroupDetails doc2ContentGroupDetails,
                                         SimilaritySimulationContext simulationContext) {
        if (AnalyzeHint.EXCLUDED.equals(contentMetadataDto.getAnalyzeHint())) {
            logger.debug("Source file is excluded - skip analysis");
            return GroupingResult.falseGroupingResult("?", 0);
        }
        AnalyzeType analyzeType = (simulationContext == null) ? AnalyzeType.STANDARD : simulationContext.getAnalyzeType();

        if (doc2ContentGroupDetails != null && AnalyzeHint.EXCLUDED.equals(doc2ContentGroupDetails.getAnalyzeHint())) {
            //File is excluded from grouping
            return GroupingResult.falseGroupingResult("?", 0);
        }

        GroupingResult groupingResult = null;
        final StringBuilder sim2DebInfo = new StringBuilder();
        if (analyzeType == AnalyzeType.STANDARD) {
            String candidateGroupId = doc2ContentGroupDetails == null ? null : doc2ContentGroupDetails.getGroupId();
            groupingResult = similarityHelper.putContentMetadataInGroup(taskId, inputDocumentGroup, candidateGroupId, contentMetadataDto, workBookCandidateId);
        } else {
            if (analyzeType == AnalyzeType.SIMULATION) {
                if (simulationContext != null) {
                    simulationContext.addToSimulationMatchesQueue(workBookCandidateId);
                }
                groupingResult = new GroupingResult("?", 1, '+', true);
            }
        }
        return groupingResult;
    }

    private Collection<String> filterSameGroupCandidates(String fileGroupId, List<Long> subList) {
        long startTime = timeSource.currentTimeMillis();
        Collection<String> subListResult = contentMetadataService.filterNonEmptyByGroup(subList, fileGroupId);
        long time = timeSource.millisSince(startTime);

        filterSameGroupCandidatesCount++;
        if (filterSameGroupCandidatesCount % 100 == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("filterSameGroupCandidates read in {}ms. in={} out={}",
                        time, subList == null ? 0 : subList.size(), subListResult == null ? 0 : subListResult.size());
            }
        } else {
            if (logger.isTraceEnabled()) {
                logger.trace("filterSameGroupCandidates read in {}ms. in={} out={}",
                        time, subList == null ? 0 : subList.size(), subListResult == null ? 0 : subListResult.size());
            }
        }
        return subListResult;
    }

    static private Random rnd = null;

    private List<Long> trimList(List<Long> keys, int trimSize, List<Long> dropped, boolean isRandom) {
        if (keys.size() <= trimSize) {
            return keys;
        }
        if (!isRandom) {
            List<Long> res = keys.subList(0, trimSize);
            if (dropped != null) {
                dropped.addAll(keys.subList(trimSize, keys.size()));
            }
            return res;
        }
        if (rnd == null) {
            long seed = System.nanoTime();
            rnd = new Random(seed);
        }
        if (dropped == null) {
            dropped = new ArrayList<>();
        }
        dropped.addAll(keys);
        List<Long> res = new ArrayList<>();
        for (int i = 0; i < trimSize; ++i) {
            Long k = dropped.get(rnd.nextInt(dropped.size()));
            res.add(k);
            dropped.remove(k);
        }
        return res;
    }
}
