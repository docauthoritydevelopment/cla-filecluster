package com.cla.filecluster.service.report.dashboardChartQueryBuilder;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.report.DashboardChartQueryBuilder;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import com.cla.filecluster.service.report.dashboardChartsEnrichers.DefaultTypeDataEnricher;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class DefaultTypeDataQueryBuilder implements
        DashboardChartQueryBuilder<SolrFacetQueryJsonResponse, DefaultTypeDataEnricher> {


    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    public ChartQueryType getQueryBuilderType() {
        return ChartQueryType.DEFAULT;
    }

    /**
     * @param solrSpecification
     * @param filter            - the filter (segment ) which we want the query to be accordingly , this field is null in case of total query
     * @param field             - the needed SOLR field which the query is accordingly
     * @param queryPrefix       - used if we want to search for results with specific prefix
     * @param neededValues      - used when we want the query
     * @param queryName         - can be use to define specific name to the query (we use this name when getting the results)
     * @return SolrFacetQueryJson
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private SolrFacetQueryJson buildQueryJsonFacet(SolrSpecification solrSpecification, FilterDescriptor filter,
                                                   SolrCoreSchema field, String queryPrefix,
                                                   DashboardChartValueTypeDto valueField, Object[] neededValues,
                                                   String queryName, boolean getAccurateNumOfBuckets, String additionalFilter) {
        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(queryName != null ? queryName : field.getSolrName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(field);
        if (queryPrefix != null) {
            theFacet.setPrefix(queryPrefix);
        }

        if (valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            theFacet.setSortField(FacetSortType.COUNT.getName());
            theFacet.setSortDirection(valueField.getSortDirection());
        } else {
            dashboardChartsUtil.addInnerFacetsAccordingToValue(valueField, getAccurateNumOfBuckets, theFacet);
        }

        List<String> filterStrList = new ArrayList<>();
        // checking if the filter is for total (in this case the filter will empty , only with name )
        if (filter != null && (filter.getValue() != null || !filter.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filter, solrSpecification));
        }
        if (neededValues != null && neededValues.length > 0) {
            filterStrList.add(field.inQuery(
                    Arrays.stream(neededValues)
                            .map(Object::toString)
                            .map(DashboardChartsUtil::fixSingleSolrSlashes)
                            .map(DashboardChartsUtil::addDoubleQuoteIfNeeded)
                            .collect(Collectors.toList())));
        }
        if (!Strings.isNullOrEmpty(additionalFilter)) {
            filterStrList.add(additionalFilter);
        }

        if (!filterStrList.isEmpty()) {
            // this replace is becuase the JSON facet  domain filter need double slash
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }
        return theFacet;
    }


    private void parseResponseToChartDataDtoWithCategories(SolrFacetQueryJsonResponse solrFacetQueryJsonResponse,
                                                           String seriesName, DashboardChartValueTypeDto valueField,
                                                           DashboardChartDataDto dashboardChartDataDto, boolean getAccurateNumOfBuckets) {
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse.getResponseBucket(seriesName)
                .getBuckets();
        ArrayList<DashboardChartSeriesDataDto> seriesList = new ArrayList<>();
        ArrayList<String> categories = new ArrayList<>();
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(seriesName);
        seriesObj.setData(new ArrayList<>());
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            String category = bucket.getStringVal();
            categories.add(category);
            Double countValue = dashboardChartsUtil.parseInnerBucketValue(dashboardChartDataDto, valueField, getAccurateNumOfBuckets, bucket);
            seriesObj.getData().add(countValue);
        }
        seriesList.add(seriesObj);
        dashboardChartDataDto.setSeries(seriesList);
        dashboardChartDataDto.setCategories(categories);
    }

    public SolrFacetQueryJsonResponse setInitialDataToDashboardChartDto(DashboardChartDataDto dashboardChartDataDto,
                                                                        CatFileFieldType groupedField,
                                                                        DataSourceRequest dataSourceRequest,
                                                                        FilterDescriptor filter,
                                                                        String queryPrefix, DashboardChartValueTypeDto valueField,
                                                                        Map<String, String> params,
                                                                        String additionalFilter,
                                                                        DefaultTypeDataEnricher defaultTypeChartEnricher) {
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse;
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                dataSourceRequest);
        SolrFacetQueryJson theJsonFacet = buildQueryJsonFacet(solrFacetSpecification, filter, groupedField, queryPrefix,
                valueField, new Object[]{}, dashboardChartsUtil.getNameOfFilter(filter), true,additionalFilter);
        theJsonFacet.setNumBuckets(true);

        defaultTypeChartEnricher.enrichJSONFacetQuery(theJsonFacet, filter);
        solrFacetSpecification.addFacetJsonObject(theJsonFacet);

        solrFacetQueryJsonResponse = solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);

        //dashboardChartDataDto.setTotalElements(solrFacetQueryJsonResponse.getResponseBucket(dashboardChartsUtil.getNameOfFilter(filter)).getTotalNumOfBuckets());

        parseResponseToChartDataDtoWithCategories(solrFacetQueryJsonResponse,
                dashboardChartsUtil.getNameOfFilter(filter), valueField, dashboardChartDataDto, true);


        return solrFacetQueryJsonResponse;
    }


    public SolrFacetQueryJson buildQueryJsonFacetByFilter(SolrFacetQueryJsonResponse initialGetDataResult,
                                                          SolrSpecification solrSpecification, FilterDescriptor filter,
                                                          SolrCoreSchema field, String queryPrefix,
                                                          DashboardChartValueTypeDto valueField, Object[] neededValues,
                                                          String queryName, Map<String, String> params,
                                                          boolean getAccurateNumOfBuckets, String additionalFilter) {
        if (neededValues != null && neededValues.length > 0) {
            return this.buildQueryJsonFacet(solrSpecification, filter, field, queryPrefix, valueField, neededValues, queryName, getAccurateNumOfBuckets, additionalFilter);
        } else {
            return null;
        }
    }

    public void parseResponseToDashboardChartDataDto(SolrFacetQueryJsonResponse initialGetDataResult,
                                                     DashboardChartDataDto dashboardChartDataDto,
                                                     List<SolrFacetJsonResponseBucket> buckets,
                                                     String seriesName, DashboardChartValueTypeDto valueField,
                                                     Map<String, String> params, boolean getAccurateNumOfBuckets) {
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(seriesName);
        ArrayList<Double> dataList = new ArrayList<>(Collections.nCopies(dashboardChartDataDto.getCategories().size(), 0d));
        seriesObj.setData(new ArrayList<>());
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            String category = bucket.getStringVal();
            Double countValue = dashboardChartsUtil.parseInnerBucketValue(dashboardChartDataDto, valueField, getAccurateNumOfBuckets, bucket);
            int categoryIndex = dashboardChartDataDto.getCategories().indexOf(category);
            if (categoryIndex > -1) {
                dataList.set(categoryIndex, countValue);
            }
        }
        seriesObj.setData(dataList);
        dashboardChartDataDto.getSeries().add(seriesObj);
    }

    public void enrichChartData(SolrFacetQueryJsonResponse initialGetDataResult, DashboardChartDataDto dashboardChartDataDto,Map<String, String> params,
                                DefaultTypeDataEnricher defaultTypeChartEnricher) {
        defaultTypeChartEnricher.enrichChartData(dashboardChartDataDto, initialGetDataResult);
    }

    public String getOtherQuery(SolrFacetQueryJsonResponse initialGetDataResult,List<String> categories, Map<String, String> params) {
        return  "(!("+categories.stream().collect(Collectors.joining(" "))+"))";
    }

    public FilterDescriptor[] getSeriesQueryResults(CatFileFieldType seriesField,
                                              String seriesFieldPrefix,
                                              DashboardChartValueTypeDto valueField,
                                              DataSourceRequest dataSourceRequest,
                                              Map<String, String> params,
                                              String additionalFilter,
                                              DefaultTypeDataEnricher defaultTypeChartEnricher) {

        DashboardChartDataDto seriesDashboardData = new DashboardChartDataDto();
        SolrFacetQueryJsonResponse response  = setInitialDataToDashboardChartDto(seriesDashboardData,
                seriesField, dataSourceRequest, DashboardChartsUtil.getUnfilterFilterDescriptor(), seriesFieldPrefix, valueField, null, additionalFilter, defaultTypeChartEnricher);
        List<String> categoriesIds = seriesDashboardData.getCategories();
        enrichChartData(response, seriesDashboardData,
                params, defaultTypeChartEnricher);
        List<String> categoriesNames = seriesDashboardData.getCategories();
        FilterDescriptor[] ans = new FilterDescriptor[(categoriesIds.size())];
        for (int count = 0 ; count < categoriesIds.size() ; count++) {
            FilterDescriptor fDescriptor = new FilterDescriptor();
            fDescriptor.setName(categoriesNames.get(count));
            fDescriptor.setField(FileDtoFieldConverter.getAlternativeNameByFieldName(seriesField));
            fDescriptor.setValue(categoriesIds.get(count));
            fDescriptor.setOperator(KendoFilterConstants.EQ);
            ans[count]=fDescriptor;
        }
        return ans;
    }

    public String getAdditionalFilterByCategories(SolrFacetQueryJsonResponse solrFacetQueryJsonResponse, List<String> categoriesIds, CatFileFieldType field, DefaultTypeDataEnricher defaultTypeChartEnricher, Map<String, String> params){
        if (categoriesIds == null || categoriesIds.size() == 0 ){
            return null;
        }
        return field.inQuery(
                categoriesIds.stream()
                        .map(Object::toString)
                        .map(DashboardChartsUtil::fixSingleSolrSlashes)
                        .map(DashboardChartsUtil::addDoubleQuoteIfNeeded)
                        .collect(Collectors.toList()));
    }
}
