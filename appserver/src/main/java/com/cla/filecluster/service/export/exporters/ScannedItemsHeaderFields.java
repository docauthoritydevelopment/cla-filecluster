package com.cla.filecluster.service.export.exporters;

public interface ScannedItemsHeaderFields {
    String DEPARTMENT = "Department";
    String MATTER = "Matter";
}
