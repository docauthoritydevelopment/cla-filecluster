package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.filetree.StoreLocation;
import com.cla.common.domain.dto.filetree.StorePurpose;
import com.cla.common.domain.dto.filetree.StoreSecurity;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.service.importEntities.ImportItemRow;

import java.util.List;

/**
 * Created by liora on 21/03/2017.
 */
public class RootfolderItemRow extends ImportItemRow {


    private String realPath;

    private String[] excludedFolders;

    private String scheduleGroupName;

    private MediaType mediaType;

    private StoreLocation storeLocation;

    private StorePurpose storePurpose;

    private StoreSecurity storeSecurity;

    private Boolean rescanActive;

    private FileType[] mappedFileTypes;

    private FileType[] ingestedFileTypes;

    private String mediaConnectionName;

    private String customerDataCenterName;

    private String nickName;

    private String description;

    private Boolean reingest;

    private Long reingestTimeInSeconds;

    private Integer scannedLimitCount;

    private Integer meaningfulFilesScannedLimitCount;

    private Integer scanTaskDepth;

    private String mailboxGroup;

    private Long fromDateScanFilter;

    private String departmentName;

    private List<String> folderExcludeRules;

    public Boolean getExtractBizLists() {
        return extractBizLists;
    }

    public void setExtractBizLists(Boolean extractBizLists) {
        this.extractBizLists = extractBizLists;
    }

    private Boolean extractBizLists;

    public String getCustomerDataCenterName() {
        return customerDataCenterName;
    }

    public void setCustomerDataCenterName(String customerDataCenterName) {
        this.customerDataCenterName = customerDataCenterName;
    }

    public StoreLocation getStoreLocation() {
        return storeLocation;
    }

    public void setStoreLocation(StoreLocation storeLocation) {
        this.storeLocation = storeLocation;
    }

    public StorePurpose getStorePurpose() {
        return storePurpose;
    }

    public void setStorePurpose(StorePurpose storePurpose) {
        this.storePurpose = storePurpose;
    }

    public StoreSecurity getStoreSecurity() {
        return storeSecurity;
    }

    public void setStoreSecurity(StoreSecurity storeSecurity) {
        this.storeSecurity = storeSecurity;
    }

    public Boolean getRescanActive() {
        return rescanActive;
    }

    public void setRescanActive(Boolean rescanActive) {
        this.rescanActive = rescanActive;
    }

    public FileType[] getMappedFileTypes() {
        return mappedFileTypes;
    }

    public void setMappedFileTypes(FileType[] mappedFileTypes) {
        this.mappedFileTypes = mappedFileTypes;
    }

    public FileType[] getIngestedFileTypes() {
        return ingestedFileTypes;
    }

    public void setIngestedFileTypes(FileType[] ingestedFileTypes) {
        this.ingestedFileTypes = ingestedFileTypes;
    }

    public String getMediaConnectionName() {
        return mediaConnectionName;
    }

    public void setMediaConnectionName(String mediaConnectionName) {
        this.mediaConnectionName = mediaConnectionName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RootfolderItemRow() {
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setExcludedFolders(String[] excludedFolders) {
        this.excludedFolders = excludedFolders;
    }

    public String[] getExcludedFolders() {
        return excludedFolders;
    }

    public String getScheduleGroupName() {
        return scheduleGroupName;
    }

    public void setScheduleGroupName(String scheduleGroupName) {
        this.scheduleGroupName=scheduleGroupName;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    public Boolean getReingest() {
        return reingest;
    }

    public void setReingest(Boolean reingest) {
        this.reingest = reingest;
    }

    public Long getReingestTimeInSeconds() {
        return reingestTimeInSeconds;
    }

    public void setReingestTimeInSeconds(Long reingestTimeInSeconds) {
        this.reingestTimeInSeconds = reingestTimeInSeconds;
    }

    public Integer getScannedLimitCount() {
        return scannedLimitCount;
    }

    public void setScannedLimitCount(Integer scannedLimitCount) {
        this.scannedLimitCount = scannedLimitCount;
    }

    public Integer getMeaningfulFilesScannedLimitCount() {
        return meaningfulFilesScannedLimitCount;
    }

    public void setMeaningfulFilesScannedLimitCount(Integer meaningfulFilesScannedLimitCount) {
        this.meaningfulFilesScannedLimitCount = meaningfulFilesScannedLimitCount;
    }

    public Integer getScanTaskDepth() {
        return scanTaskDepth;
    }

    public void setScanTaskDepth(Integer scanTaskDepth) {
        this.scanTaskDepth = scanTaskDepth;
    }

    public String getMailboxGroup() {
        return mailboxGroup;
    }

    public void setMailboxGroup(String mailboxGroup) {
        this.mailboxGroup = mailboxGroup;
    }

    public Long getFromDateScanFilter() {
        return fromDateScanFilter;
    }

    public void setFromDateScanFilter(Long fromDateScanFilter) {
        this.fromDateScanFilter = fromDateScanFilter;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public List<String> getFolderExcludeRules() {
        return folderExcludeRules;
    }

    public void setFolderExcludeRules(List<String> folderExcludeRules) {
        this.folderExcludeRules = folderExcludeRules;
    }

    @Override
    public String toString() {
        return "RootfolderItemRow{" +
                "realPath='" + realPath + '\'' +
                "scheduleGroupName='" + scheduleGroupName + '\'' +
                "customerDataCenterName='" + customerDataCenterName + '\'' +
                ", excludedFolders='" + excludedFolders + '\'' +
                ", reingest='" + reingest + '\'' +
               super.toString()+
                '}';
    }
}
