package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionDefinitionDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import com.cla.filecluster.domain.entity.actions.ActionParameterDefinition;
import com.cla.filecluster.repository.jpa.actions.ActionDefinitionRepository;
import com.cla.filecluster.repository.jpa.actions.ActionParameterDefinitionRepository;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class ActionDefinitionService {

    Logger logger = LoggerFactory.getLogger(ActionDefinitionService.class);

    @Autowired
    private ActionDefinitionRepository actionDefinitionRepository;

    @Autowired
    private ActionParameterDefinitionRepository actionParameterDefinitionRepository;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    private LoadingCache<String, Optional<ActionDefinitionDto>> actionDefinitionCache = null;

    @PostConstruct
    void init() {
        actionDefinitionCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<String, Optional<ActionDefinitionDto>>() {
                            public Optional<ActionDefinitionDto> load(String key) {
                                return dbTemplateUtils.doInTransaction(() -> {
                                    ActionDefinition actionDefinition = findActionDefinition(key);
                                    return Optional.ofNullable(convertActionDefinition(actionDefinition));
                                });
                            }
                        });
    }

    public ActionDefinitionDto getFromCache(String key) {
        ActionDefinitionDto dto = actionDefinitionCache.getUnchecked(key).orElse(null);
        return dto == null ? null : new ActionDefinitionDto(dto);
    }

    @Transactional
    public ActionDefinition createActionDefinition(String id,
                                                   String name,
                                                   String description,
                                                   ActionClass actionClass,
                                                   String methodName) {

        logger.debug("Create Action definition {}", id);
        ActionDefinition actionDefinition = new ActionDefinition();
        actionDefinition.setId(id);
        actionDefinition.setName(name);
        actionDefinition.setDescription(description);
        actionDefinition.setActionClass(actionClass);
        actionDefinition.setJavaMethodName(methodName);
        return actionDefinitionRepository.save(actionDefinition);
    }


    @Transactional(readOnly = true)
    public Page<ActionDefinition> listActionDefinitions(DataSourceRequest dataSourceRequest) {
        return actionDefinitionRepository.findAll(dataSourceRequest.createPageRequest());
    }

    @Transactional(readOnly = true)
    public ActionDefinition findActionDefinition(String id) {
        return actionDefinitionRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public ActionDefinition findActionDefinitionByName(String name) {
        return actionDefinitionRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    public List<ActionParameterDefinition> listActionParameters(String actionId) {
        return actionParameterDefinitionRepository.findByActionId(actionId);
    }

    @Transactional
    public ActionParameterDefinition createActionParameterDefinition(ActionDefinition actionDefinition, String id, String name, String description, String regex) {
        logger.debug("Create action parameter definition {} for action {}", id, actionDefinition.getId());
        ActionParameterDefinition actionParameterDefinition = new ActionParameterDefinition();
        actionParameterDefinition.setActionDefinition(actionDefinition);
        actionParameterDefinition.setId(id);
        actionParameterDefinition.setDescription(description);
        actionParameterDefinition.setName(name);
        actionParameterDefinition.setRegex(regex);
        return actionParameterDefinitionRepository.save(actionParameterDefinition);
    }

    public static Page<ActionDefinitionDto> convertActionDefinitions(@NotNull Page<ActionDefinition> actionDefinitions, PageRequest pageRequest) {
        if (actionDefinitions == null) {
            throw new RuntimeException("unable to convert action definitions - empty list");
        }
        List<ActionDefinitionDto> result = new ArrayList<>();
        for (ActionDefinition actionDefinition : actionDefinitions) {
            result.add(convertActionDefinition(actionDefinition));
        }
        return new PageImpl<>(result, pageRequest, actionDefinitions.getTotalElements());
    }


    public static ActionDefinitionDto convertActionDefinition(ActionDefinition actionDefinition) {
        if (actionDefinition == null) {
            return null;
        }
        ActionDefinitionDto result = new ActionDefinitionDto();
        result.setId(actionDefinition.getId());
        result.setName(actionDefinition.getName());
        result.setJavaMethodName(actionDefinition.getJavaMethodName());
        result.setDescription(actionDefinition.getDescription());
        result.setActionClass(actionDefinition.getActionClass());
        return result;
    }
}
