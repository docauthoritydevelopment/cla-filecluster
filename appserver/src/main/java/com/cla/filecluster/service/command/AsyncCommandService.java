package com.cla.filecluster.service.command;

import org.springframework.core.task.TaskRejectedException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Service for running asynchronous commands
 */
public class AsyncCommandService {

    protected String name;
    private int corePoolSize;
    private int maxPoolSize;
    private int queueCapacity;

    private TransactionalCommandService transactionalCommandService;

    private AtomicInteger successfulSubmits = new AtomicInteger(0);
    private AtomicInteger rejectedCommands = new AtomicInteger(0);

    private ThreadPoolTaskExecutor executor;

    @SuppressWarnings("UnusedDeclaration")
    public AsyncCommandService() {
    }

    public AsyncCommandService(String name, int corePoolSize, int maxPoolSize, int queueCapacity) {
        this.corePoolSize = corePoolSize;
        this.maxPoolSize = maxPoolSize;
        this.queueCapacity = queueCapacity;
        this.name = name;
    }

    @SuppressWarnings("UnusedDeclaration")
    public AsyncCommandService(String name, int corePoolSize, int maxPoolSize, int queueCapacity,
                               TransactionalCommandService transactionalCommandService) {
        this(name, corePoolSize, maxPoolSize, queueCapacity);
        this.transactionalCommandService = transactionalCommandService;
    }

    public static AsyncCommandService create(String name, int corePoolSize, int maxPoolSize, int queueCapacity,
                                             TransactionalCommandService transactionalCommandService) {
        AsyncCommandService newInst = new AsyncCommandService(name, corePoolSize, maxPoolSize, queueCapacity, transactionalCommandService);
        newInst.init();
        return newInst;
    }

    public void init(){
        executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix(name);
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.initialize();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void shutdown(){
        executor.shutdown();
    }

    public <T> Future<T> submit(BaseCommand<T> command) {
        try {
            successfulSubmits.incrementAndGet();
            if (command instanceof TransactionalCommand && transactionalCommandService != null) {
                ((TransactionalCommand)command).setTxService(transactionalCommandService);
            }
            return executor.submit( command );
        }
        catch (TaskRejectedException ex) {
            successfulSubmits.decrementAndGet();
            rejectedCommands.incrementAndGet();
            command.onRejection(ex);
            return null;
        }
    }

    /**
     * Executes command immediately in the same thread and transaction where it was called
     * @param command command
     * @param <T>  command return type
     */
    public <T> void execute(BaseCommand<T> command) {
        try {
            executor.execute(command::call);
        }
        catch (TaskRejectedException ex) {
            command.onRejection(ex);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCorePoolSize(int corePoolSize) {
        this.corePoolSize = corePoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public void setQueueCapacity(int queueCapacity) {
        this.queueCapacity = queueCapacity;
    }

    public void setTransactionalCommandService(TransactionalCommandService transactionalCommandService) {
        this.transactionalCommandService = transactionalCommandService;
    }
}
