package com.cla.filecluster.service.export.exporters.active_scans.mixin;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.cla.filecluster.service.export.serialize.RoundingSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.active_scans.JobListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
public class PhaseDetailsDtoMixin {

    @JsonProperty(RATE)
    @JsonSerialize(using = RoundingSerializer.class)
    private Double tasksPerSecRate;

    @JsonProperty(START_TIME)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long phaseStart;

    @JsonProperty(ERRORS)
    private Integer failedTasksCount;

    @JsonProperty(DETAILS)
    private String stateDescription;
}
