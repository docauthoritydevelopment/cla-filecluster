package com.cla.filecluster.service.export.exporters.schedule_groups.mixin;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.schedule_groups.ActiveScheduledGroupsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class ScheduleGroupSummaryInfoDtoMixin {

    @JsonUnwrapped
    private ScheduleGroupDto scheduleGroupDto;

    @JsonUnwrapped
    private CrawlRunDetailsDto lastCrawlRunDetails;

    @JsonProperty(STATUS)
    private RunStatus runStatus;

    @JsonProperty(NEXT_SCAN)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long nextPlannedWakeup;
}
