package com.cla.filecluster.service.syscomponent;

import com.cla.common.domain.dto.components.*;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.messages.control.ControlStatusResponsePayload;
import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.common.domain.dto.messages.control.ServerHandShakeResponsePayload;
import com.cla.connector.utils.Pair;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.repository.solr.SolrPvAnalyzerRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.communication.ActiveMqControlService;
import com.cla.filecluster.service.communication.ZookeeperControlService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import com.sun.management.OperatingSystemMXBean;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Responsible for collecting and showing information about all the components in the system
 * Created by uri on 23-May-17.
 */
@Service
public class ComponentsAppService {

    private final static Logger logger = LoggerFactory.getLogger(ComponentsAppService.class);

    private static final String BROKER_INSTANCE_PREFIX = "Broker";
    private static final String SOLR_CATFILE_INSTANCE_PREFIX = "SolrCatFile";
    private static final String SOLR_SIMILARITY_INSTANCE_PREFIX = "SolrSimilarity";
    private static final String ZOOKEEPER_SOLR_INSTANCE_ID = "ZooKeeperSolr";
    private static final String APPSERVER_INSTANCE_ID = "AppServer";
    private static final String DB_INSTANCE_ID = "DataBase";

    @Value("${component.unknown.media-processor.enable-on-handshake:true}")
    private boolean enableNewMediaProcessorOnFirstHandshake;

    @Value("${spring.data.solr.host}")
    private String solrServerURL;

    @Value("${server-component.url-prefix:http://localhost}")
    private String serverURL;

    @Value("${server.port:8080}")
    private Integer serverPort;

    @Value("${database-component.url-prefix:failover://tcp://}")
    private String dbURL;

    @Value("${bonecp.url}")
    private String bonecpUrl;

    @Value("${zookeeper-component.url-prefix:http://}")
    private String zookeeperURL;

    @Value("${spring.data.solr.type:http}")
    private String solrServerType;

    @Value("${component.media-processor.stats.max-size:300}")
    private int maxMediaProcessorsStatsSize;

    @Value("${component.media-processor.verify-stats:false}")
    private boolean verifyMediaProcessorsStats;

    private final Object syncMediaProcessorUpdateLock = new Object();

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    private ActiveMqControlService activeMqControlService;

    @Autowired
    private ZookeeperControlService zookeeperControlService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Value("${components.mediaprocessor.expire.ms:360000}")
    private long mediaProcessorExpireMs;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrPvAnalyzerRepository pvAnalyzerRepository;

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Autowired
    private AppServerStateService appServerStateService;

    private TimeSource timeSource = new TimeSource();

    private ObjectMapper objectMapper;

    @PostConstruct
    public void init() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);     // Allow null fields to be sent (and ignored during serialization)
        updateComponentAddress();

        // create new solr nodes as components in db
        if (solrServerType.equalsIgnoreCase("cloud")) {
            Pair<Boolean, List<String>> result = zookeeperControlService.getSolrNodes();
            List<String> solrNodes = result.getValue();
            if (result.getKey() && !solrNodes.isEmpty()) {
                List<ClaComponent> existing = sysComponentService.getByComponentType(ClaComponentType.SOLR_NODE);
                Set<String> addresses = existing.stream().map(ClaComponent::getExternalNetAddress).collect(Collectors.toSet());

                for (String node : solrNodes) {
                    if (!addresses.contains(node)) {
                        logger.debug("add new solr node to components address {}", node);
                        try {
                            sysComponentService.createBasicComponent(node, ClaComponentType.SOLR_NODE, node);
                        } catch (Throwable t) {
                            logger.warn("Failed to create solr basic component", t);
                        }
                    }
                }
            }
        }
    }

    @Scheduled(initialDelayString = "${status.reports.delay.ms:10000}", fixedRateString = "${status.reports.schedule.ms:300000}") // run every 5 min by default
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void checkComponentsSchedule() {
        DBTemplateUtils.doInTransaction(this::updateBrokerState);
        DBTemplateUtils.doInTransaction(this::markFaultyMediaProcessors);
        DBTemplateUtils.doInTransaction(this::updateSolrState);
        DBTemplateUtils.doInTransaction(this::updateAppServerState);
        DBTemplateUtils.doInTransaction(this::updateDBState);
        DBTemplateUtils.doInTransaction(this::updateZookeeperAndSolrNodeState);
    }

    public boolean isSolrUp() {
        try {
            SolrPingResponse solrPingResponse = fileCatService.pingSolr();
            return (solrPingResponse != null);
        } catch (Exception e) {
            return false;
        }
    }

    private void updateSolrState() {
        try {
            ComponentState state = isSolrUp() ? ComponentState.OK : ComponentState.FAULTY;
            ClaComponentDto claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.SOLR_CLAFILE, SOLR_CATFILE_INSTANCE_PREFIX);
            ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
            componentStatusDto.setComponentType(ClaComponentType.SOLR_CLAFILE.name());
            componentStatusDto.setComponentState(state);
            componentStatusDto.setTimestamp(timeSource.currentTimeMillis());
            sysComponentService.addSysComponentStatusAndUpdateComponent(claComponentDto.getId(), componentStatusDto);

            claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.SOLR_SIMILARITY, SOLR_SIMILARITY_INSTANCE_PREFIX);
            SolrPingResponse solrPingResponse = pvAnalyzerRepository.pingSolr();
            state = solrPingResponse == null ? ComponentState.FAULTY : ComponentState.OK;
            componentStatusDto = new ClaComponentStatusDto();
            componentStatusDto.setComponentType(ClaComponentType.SOLR_SIMILARITY.name());
            componentStatusDto.setComponentState(state);
            componentStatusDto.setTimestamp(timeSource.currentTimeMillis());
            sysComponentService.addSysComponentStatusAndUpdateComponent(claComponentDto.getId(), componentStatusDto);
        } catch (Exception e) {
            logger.error("Failed to updateSolrState", e);
        }
    }

    private void markFaultyMediaProcessors() {
        synchronized (syncMediaProcessorUpdateLock) {
            try {

                long minLastResponseTime = System.currentTimeMillis() - mediaProcessorExpireMs;
                List<ClaComponentDto> expiredComponents = sysComponentService.findExpiredComponents(ClaComponentType.MEDIA_PROCESSOR, minLastResponseTime);
                if (expiredComponents.size() > 0) {
                    for (ClaComponentDto expiredComponent : expiredComponents) {
                        logger.warn("Media Processor component did not send status report for {} ms. Setting its state to faulty {} ", mediaProcessorExpireMs, expiredComponent);
                        sysComponentService.updateComponentState(expiredComponent.getId(), ComponentState.FAULTY, null, false);
                    }

                }
            } catch (Exception e) {
                logger.error("Failed to mark faulty media processors", e);
            }
        }
    }

    public ClaComponentDto updateSysComponent(ClaComponentDto claComponentDto) {
        synchronized (syncMediaProcessorUpdateLock) {
            return sysComponentService.updateSysComponent(claComponentDto);
        }
    }

    public ClaComponentDto deleteSysComponent(long id) {
        synchronized (syncMediaProcessorUpdateLock) {
            return sysComponentService.deleteSysComponent(id);
        }
    }

    /**
     * Create the default components as part of a database reset
     */
    public void createDefaultComponents() {

        String databaseUrl = dbURL;
        try {
            int start = bonecpUrl.indexOf("://") + 3;
            int end = bonecpUrl.indexOf("/",start);
            databaseUrl += bonecpUrl.substring(start,end);
        } catch (Exception e) {
            logger.error("failed creating url for database component");
        }

        String localNetAddress = null;
        try {
            localNetAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {}
        sysComponentService.createBasicComponent(serverURL+":"+serverPort,ClaComponentType.APPSERVER, APPSERVER_INSTANCE_ID, localNetAddress);
        sysComponentService.createBasicComponent(databaseUrl, ClaComponentType.DB, DB_INSTANCE_ID);
        createBrokerComponent();
        createSolrComponents();
    }

    private void updateComponentAddress() {
        sysComponentService.updateComponentAddress(ClaComponentType.APPSERVER, APPSERVER_INSTANCE_ID, serverURL+":"+serverPort);

        String databaseUrl = dbURL;
        try {
            int start = bonecpUrl.indexOf("://") + 3;
            int end = bonecpUrl.indexOf("/",start);
            databaseUrl += bonecpUrl.substring(start,end);
        } catch (Exception e) {
            logger.error("failed creating url for database component");
        }

        sysComponentService.updateComponentAddress(ClaComponentType.DB, DB_INSTANCE_ID, databaseUrl);

        sysComponentService.updateComponentAddress(ClaComponentType.BROKER, BROKER_INSTANCE_PREFIX, activeMqControlService.getBrokerAddress());

        if (solrServerType.equalsIgnoreCase("cloud")) {

            String serverZoo = solrServerURL;
            try {
                serverZoo = serverZoo.replaceAll("http://", "");
                int start = serverZoo.indexOf("/");
                if (start > 0) {
                    serverZoo = serverZoo.substring(0, start);
                }
            } catch (Exception e) {
                logger.error("failed creating url for zookeeper component");
            }

            sysComponentService.updateComponentAddress(ClaComponentType.ZOOKEEPER, ZOOKEEPER_SOLR_INSTANCE_ID, zookeeperURL+serverZoo);
            sysComponentService.updateComponentAddress(ClaComponentType.SOLR_CLAFILE, SOLR_CATFILE_INSTANCE_PREFIX, solrServerURL);
            sysComponentService.updateComponentAddress(ClaComponentType.SOLR_SIMILARITY, SOLR_SIMILARITY_INSTANCE_PREFIX, solrServerURL);
        }
        else {
            sysComponentService.updateComponentAddress(ClaComponentType.SOLR_CLAFILE, SOLR_CATFILE_INSTANCE_PREFIX, solrServerURL);
            sysComponentService.updateComponentAddress(ClaComponentType.SOLR_SIMILARITY, SOLR_SIMILARITY_INSTANCE_PREFIX, solrServerURL);
        }
    }

    private void createSolrComponents() {
        if (solrServerType.equalsIgnoreCase("cloud")) {

            String serverZoo = solrServerURL;
            try {
                serverZoo = serverZoo.replaceAll("http://", "");
                int start = serverZoo.indexOf("/");
                if (start > 0) {
                    serverZoo = serverZoo.substring(0, start);
                }
            } catch (Exception e) {
                logger.error("failed creating url for zookeeper component");
            }

            sysComponentService.createBasicComponent(zookeeperURL+serverZoo, ClaComponentType.ZOOKEEPER, ZOOKEEPER_SOLR_INSTANCE_ID);
            sysComponentService.createBasicComponent(solrServerURL,ClaComponentType.SOLR_CLAFILE, SOLR_CATFILE_INSTANCE_PREFIX);
            sysComponentService.createBasicComponent(solrServerURL,ClaComponentType.SOLR_SIMILARITY, SOLR_SIMILARITY_INSTANCE_PREFIX);
        }
        else {
            sysComponentService.createBasicComponent(solrServerURL,ClaComponentType.SOLR_CLAFILE, SOLR_CATFILE_INSTANCE_PREFIX);
            sysComponentService.createBasicComponent(solrServerURL,ClaComponentType.SOLR_SIMILARITY, SOLR_SIMILARITY_INSTANCE_PREFIX);
        }
    }

    private void createZookeeperComponent(String zookeeperSolrRoot) {
        ClaComponentDto zookeeperComponent = new ClaComponentDto();
        zookeeperComponent.setInstanceId(ZOOKEEPER_SOLR_INSTANCE_ID);
        zookeeperComponent.setState(ComponentState.UNKNOWN);
        zookeeperComponent.setClaComponentType(ClaComponentType.ZOOKEEPER);
        zookeeperComponent.setComponentType(ClaComponentType.ZOOKEEPER.name());
        zookeeperComponent.setActive(true);
        sysComponentService.createSysComponent(zookeeperComponent);
    }

    private void createBrokerComponent() {
        ClaComponentDto brokerComponent = new ClaComponentDto();
        brokerComponent.setInstanceId(BROKER_INSTANCE_PREFIX);
        brokerComponent.setState(ComponentState.UNKNOWN);
        brokerComponent.setClaComponentType(ClaComponentType.BROKER);
        brokerComponent.setComponentType(ClaComponentType.BROKER.name());
        brokerComponent.setActive(true);
        brokerComponent.setExternalNetAddress(activeMqControlService.getBrokerAddress());
        sysComponentService.createSysComponent(brokerComponent);
    }

    public void updateZookeeperAndSolrNodeState() {
        if (solrServerType.equalsIgnoreCase("cloud")) {
            Pair<Boolean, List<String>> result = zookeeperControlService.getSolrNodes();

            ComponentState zookeeperState = ComponentState.OK;
            if (!result.getKey()) {
                logger.debug("zookeeper seems to be faulty");
                zookeeperState = ComponentState.FAULTY;
            } else {
                List<String> solrNodes = result.getValue();
                List<ClaComponent> existing = sysComponentService.getByComponentType(ClaComponentType.SOLR_NODE);

                for (ClaComponent comp : existing) {
                    ComponentState status = ComponentState.FAULTY;
                    if (solrNodes.contains(comp.getExternalNetAddress())) {
                        status = ComponentState.OK;
                    }

                    ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
                    componentStatusDto.setComponentState(status);
                    componentStatusDto.setTimestamp(timeSource.currentTimeMillis());
                    componentStatusDto.setClaComponentId(comp.getId());
                    componentStatusDto.setComponentType(comp.getComponentType());
                    sysComponentService.addSysComponentStatusAndUpdateComponent(comp.getId(), componentStatusDto);
                }
            }

            List<ClaComponent> existing = sysComponentService.getByComponentType(ClaComponentType.ZOOKEEPER);
            if (existing != null && existing.size() == 1) {
                ClaComponent comp = existing.get(0);
                ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
                componentStatusDto.setComponentState(zookeeperState);
                componentStatusDto.setTimestamp(timeSource.currentTimeMillis());
                componentStatusDto.setClaComponentId(comp.getId());
                componentStatusDto.setComponentType(comp.getComponentType());
                sysComponentService.addSysComponentStatusAndUpdateComponent(comp.getId(), componentStatusDto);
            }
        } else {
            logger.debug("not solr cloud - skip getting solr nodes status from zk");
        }
    }

    private void updateBrokerState() {
        try {
            Map<String, String> queueStatus = activeMqControlService.getActiveMQQueueStatus();
            if (queueStatus != null) {
                List<ClaComponent> brokerDcs = sysComponentService.getByComponentType(ClaComponentType.BROKER_DC);
                Map<Long, ClaComponent> brokerDcsMap = brokerDcs.stream().collect(Collectors.toMap(ClaComponent::getId, item -> item));
                queueStatus.forEach(
                        (dcId, status) -> {
                            ClaComponentDto claComponentDto;
                            if (dcId.equals("0")) {
                                claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.BROKER,BROKER_INSTANCE_PREFIX);
                            } else {
                                claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.BROKER_DC, dcId);
                                if (claComponentDto != null) {
                                    brokerDcsMap.remove(claComponentDto.getId());
                                }
                            }
                            if (claComponentDto == null) {
                                logger.debug("has active mq queue data for non existing data center {}", dcId);
                            } else {
                                ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
                                componentStatusDto.setComponentState(ComponentState.OK);
                                componentStatusDto.setTimestamp(timeSource.currentTimeMillis());
                                componentStatusDto.setClaComponentId(claComponentDto.getId());
                                componentStatusDto.setComponentType(claComponentDto.getComponentType());
                                componentStatusDto.setExtraMetrics(status);
                                sysComponentService.addSysComponentStatusAndUpdateComponent(claComponentDto.getId(), componentStatusDto);
                            }
                        }
                );
                if (!brokerDcsMap.isEmpty()) {
                    brokerDcsMap.values().forEach(c -> {
                        if (!c.getState().equals(ComponentState.UNKNOWN)) {
                            sysComponentService.updateComponentByInstanceId(ClaComponentType.BROKER_DC, c.getInstanceId(), ComponentState.FAULTY);
                        }
                    });
                }
            } else {
                updateBrokerFaulty();
            }

        } catch (Exception e) {
            try {
                updateBrokerFaulty();
                logger.error("Failed to update message broker state", e);
            } catch (Exception ex) {
                logger.error("Failed to update message broker state to faulty", e);
            }
        }
    }

    private void updateBrokerFaulty() {
        sysComponentService.updateComponentByInstanceId(ClaComponentType.BROKER,BROKER_INSTANCE_PREFIX, ComponentState.FAULTY);
        List<ClaComponent> brokerDcs = sysComponentService.getByComponentType(ClaComponentType.BROKER_DC);
        if (brokerDcs != null) {
            brokerDcs.forEach(c -> {
                if (!c.getState().equals(ComponentState.UNKNOWN)) {
                    sysComponentService.updateComponentByInstanceId(ClaComponentType.BROKER_DC, c.getInstanceId(), ComponentState.FAULTY);
                }
            });
        }
    }

    private void updateDBState() {
        try {
            long lastDBSave = sysComponentService.getLastDBsave();
            ComponentState componentState = lastDBSave + 20000 > System.currentTimeMillis() ? ComponentState.OK : ComponentState.FAULTY;
            ClaComponentDto claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.DB,DB_INSTANCE_ID);
            ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
            componentStatusDto.setComponentType(ClaComponentType.DB.name());
            componentStatusDto.setComponentState(componentState);
            componentStatusDto.setTimestamp(timeSource.currentTimeMillis());
            if (claComponentDto != null) {
                sysComponentService.addSysComponentStatusAndUpdateComponent(claComponentDto.getId(), componentStatusDto);
            }
        } catch (Exception e) {
            logger.error("Failed to update message db state", e);
        }
    }

    public ClaComponentDto registerMediaProcessor(String mediaProcessorId, ServerHandShakeResponsePayload payload, Long sendTime)
    {
        synchronized (syncMediaProcessorUpdateLock) {
            ClaComponentDto componentDto = findRegisteredMediaProcessor(mediaProcessorId);
            boolean create = false;
            if (componentDto == null) {
                componentDto = new ClaComponentDto();
                componentDto.setClaComponentType(ClaComponentType.MEDIA_PROCESSOR);
                componentDto.setComponentType(ClaComponentType.MEDIA_PROCESSOR.name());
                componentDto.setInstanceId(mediaProcessorId);
                componentDto.setActive(enableNewMediaProcessorOnFirstHandshake);
                create = true;
            }
            componentDto.setLastResponseDate(sendTime);
            componentDto.setState(ComponentState.STARTING);

            componentDto.setExternalNetAddress(payload.getHostName());
            componentDto.setLocalNetAddress(payload.getAddresses() != null ? payload.getAddresses().stream().collect(Collectors.joining(", ")) : null);
            componentDto = create ? sysComponentService.createSysComponent(componentDto) :
                    sysComponentService.updateSysComponentBySystem(componentDto);

            ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
            componentStatusDto.setComponentState(ComponentState.STARTING);
            componentStatusDto.setComponentType(ClaComponentType.MEDIA_PROCESSOR.name());
            componentStatusDto.setTimestamp(sendTime);

            if (logger.isDebugEnabled()) {
                logger.debug("Handled ({}) handshake component status {}. Component={}", (create ? "created" : "updated"), componentStatusDto, componentDto);
            }

            sysComponentService.addSysComponentStatusAndUpdateComponent(componentDto.getId(), componentStatusDto);

            return componentDto;
        }

    }

    Map<String, Integer> mediaProcessorsSignatureRequestStats = new ConcurrentHashMap<>();
    public int incrementMediaProcessorSignatureChecksCounter(String mediaProcessorId) {
        if (!verifyMediaProcessorsStats) {
            return 0;
        }
        Integer count = mediaProcessorsSignatureRequestStats.get(mediaProcessorId);
        if (count == null) {
            count = 0;
            if (mediaProcessorsSignatureRequestStats.size() > maxMediaProcessorsStatsSize) {
                logger.warn("mediaProcessorsSignatureRequestStats size exceeded {}", mediaProcessorsSignatureRequestStats.size());
                return 0;
            }
        }
        count++;
        mediaProcessorsSignatureRequestStats.put(mediaProcessorId, count);
        return count;
    }

    public int getMediaProcessorSignatureChecksCounter(String mediaProcessorId) {
        if (!verifyMediaProcessorsStats) {
            return 0;
        }
        Integer count = mediaProcessorsSignatureRequestStats.get(mediaProcessorId);
        return (count == null)?0:count;
    }

    public ClaComponentDto findRegisteredMediaProcessor(String mediaProcessorId) {
        return sysComponentService.findComponentByInstanceId(ClaComponentType.MEDIA_PROCESSOR, mediaProcessorId);
    }

    public void updateMediaProcessorAlive(String mediaProcessorId, ControlStatusResponsePayload payload, Long sendTime)
    {
        synchronized (syncMediaProcessorUpdateLock) {
            ClaComponentDto claComponentDto = findRegisteredMediaProcessor(mediaProcessorId);
            if (claComponentDto == null) {
                logger.warn("Component not found. Could not update media processor mediaProcessorId: {} {} ", mediaProcessorId, payload);
            } else {
                try {
                    ClaComponentStatusDto componentStatusDto = createComponentStatusDto(payload, sendTime);
                    componentStatusDto.setComponentType(ClaComponentType.MEDIA_PROCESSOR.name());

                    sysComponentService.addSysComponentStatusAndUpdateComponent(claComponentDto.getId(), componentStatusDto);
                    if (verifyMediaProcessorsStats) {
                        verifyMediaProcessorSingatureChecks(mediaProcessorId, payload.getIngestTaskRequestsFinished());
                    }
                } catch (Exception e) {
                    logger.error("failed creating status for MP component", e);
                }
            }
        }
    }

    private boolean verifyMediaProcessorSingatureChecks(String mediaProcessorId, long ingestTaskRequestsFinished) {
        int signatureChecksCount = getMediaProcessorSignatureChecksCounter(mediaProcessorId);
        long ingestCount = ingestTaskRequestsFinished;
        if (ingestCount == 0) {
            return true;
        }
        if (ingestCount > 100 && signatureChecksCount == 0) {
            logger.warn("MediaProcessor may be mis-configured. Ingest counter={}, signature checks={}", ingestCount, signatureChecksCount);
            return false;
        }
        return true;
    }

    private int operationSystemUsageCounter = 0;
    private boolean isAfterRestart = true;
    private void updateAppServerState() {
        try {
            OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

            /*
             * For a Windows platform the account running the process/service should be part of on of the following groups in order to get CPU performance data.
             * Administrators
             * ‘Performance Monitor Users’ - a builtin AD group
             * 'Performance Log Users' - a builtin local group
             */
            AppServerStatus status = new AppServerStatus();
            status.setProcessCpuLoad(osBean.getProcessCpuLoad());
            status.setSystemCpuLoad(osBean.getSystemCpuLoad());
            status.setTotalPhysicalMemorySize(osBean.getTotalPhysicalMemorySize());
            status.setFreePhysicalMemorySize(osBean.getFreePhysicalMemorySize());

            status.setAnalyzedRunningThroughput(appServerStateService.getAnalyzeRunningThroughput());
            status.setAnalyzedTaskRequestFailure(appServerStateService.getAnalyzeTaskRequestFailure());
            status.setAnalyzedTaskRequests(appServerStateService.getAnalyzeTaskRequests());
            status.setAnalyzedTaskRequestsFinished(appServerStateService.getAnalyzeTaskRequestsFinished());
            status.setAverageAnalyzedThroughput(appServerStateService.getAverageAnalyzeThroughput());

            status.setIngestRunningThroughput(appServerStateService.getIngestRunningThroughput());
            status.setIngestTaskRequestFailure(appServerStateService.getIngestTaskRequestFailure());
            status.setIngestTaskRequests(appServerStateService.getIngestTaskRequests());
            status.setIngestTaskRequestsFinished(appServerStateService.getIngestTaskRequestsFinished());
            status.setAverageIngestThroughput(appServerStateService.getAverageIngestThroughput());

            if (logger.isDebugEnabled() && (operationSystemUsageCounter++ % 100) == 0 ) {
                StringBuilder sb = new StringBuilder();
                sb.append("Arch=").append(osBean.getArch());
                sb.append(", Name=").append(osBean.getName());
                sb.append(", SystemCpuLoad=").append(status.getSystemCpuLoad());
                if (status.getSystemCpuLoad() < 0d) {
                    sb.append(" (check Performance Monitor permissions)");
                }
                sb.append(", ProcessCpuLoad=").append(status.getProcessCpuLoad());
                sb.append(", SystemLoadAverage=").append(osBean.getSystemLoadAverage());
                sb.append(", AvailableProcessors=").append(osBean.getAvailableProcessors());
                sb.append(", ProcessCpuTime=").append(osBean.getProcessCpuTime());
                sb.append(", FreePhysicalMemorySize=").append(status.getFreePhysicalMemorySize());
                sb.append(", CommittedVirtualMemorySize=").append(osBean.getCommittedVirtualMemorySize());
//                sb.append("=").append();
                logger.debug("osBead data: {}", sb.toString());
            }

            ClaComponentDto claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.APPSERVER,APPSERVER_INSTANCE_ID);
            ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
            componentStatusDto.setComponentType(ClaComponentType.APPSERVER.name());
            componentStatusDto.setComponentState(ComponentState.OK);
            componentStatusDto.setTimestamp(timeSource.currentTimeMillis());

            List<SimpleJob> optJobs = jobManager.getJobs(JobType.OPTIMIZE_INDEXES, JobState.IN_PROGRESS);
            boolean optRunning = (optJobs != null && optJobs.size() > 0);

            long dupContentFound = appServerStateService.getDupContentFound();
            long dupContentRequests = appServerStateService.getDupContentRequests();
            if (dupContentFound > 0 && dupContentRequests > 0) {
                status.setPrecentageDuplicateContentFound(dupContentFound*100/dupContentRequests);
            }

            status.setOptimizeIndexesRunning(optRunning);

            componentStatusDto.setExtraMetrics(objectMapper.writeValueAsString(status));

            if (claComponentDto == null){
                logger.warn("Failed to find AppServer monitoring component."); // shouldn't happen
            }
            else {
                sysComponentService.addSysComponentStatusAndUpdateComponent(claComponentDto.getId(), componentStatusDto, isAfterRestart);
            }
            isAfterRestart = false;
        }
        catch (Exception e) {
            logger.warn("Failed to update AppServer monitoring state", e);
        }

    }

    public DriveDto getDriveDataFromString(String driveData) {
        if (Strings.isNullOrEmpty(driveData)) {
            return null;
        }
        try {
            return objectMapper.readValue(driveData, DriveDto.class);
        } catch (Exception e) {
            return null;
        }
    }

    public AppServerStatus getFCStatusFromString(String fcStatus) {
        if (Strings.isNullOrEmpty(fcStatus)) {
            return null;
        }
        try {
            return objectMapper.readValue(fcStatus, AppServerStatus.class);
        } catch (Exception e) {
            return null;
        }
    }

    public MediaProcessorStatus getMpStatusFromString(String mpStatus) {
        if (Strings.isNullOrEmpty(mpStatus)) {
            return null;
        }
        try {
            return objectMapper.readValue(mpStatus, MediaProcessorStatus.class);
        } catch (Exception e) {
            return null;
        }
    }

    private ClaComponentStatusDto createComponentStatusDto(ControlStatusResponsePayload payload, Long sendTime) throws Exception {
        ClaComponentStatusDto componentStatusDto = new ClaComponentStatusDto();
        componentStatusDto.setComponentState(ComponentState.OK);
        componentStatusDto.setTimestamp(sendTime);

        MediaProcessorStatus status = new MediaProcessorStatus();

        status.setIngestTaskRequestFailure(payload.getIngestTaskRequestFailure());
        status.setIngestTaskRequests(payload.getIngestTaskRequests());
        status.setIngestTaskRequestsFinished(payload.getIngestTaskRequestsFinished());
        status.setIngestTaskRequestsFinishedWithError(payload.getIngestTaskRequestsFinishedWithError());
        status.setScanTaskFailedResponsesFinished(payload.getScanTaskFailedResponsesFinished());
        status.setScanTaskResponsesFinished(payload.getScanTaskResponsesFinished());
        status.setActiveScanTasks(payload.getActiveScanTasks());
        status.setStartScanTaskRequests(payload.getStartScanTaskRequests());
        status.setStartScanTaskRequestsFinished(payload.getStartScanTaskRequestsFinished());
        status.setIngestRunningThroughput(payload.getIngestRunningThroughput());
        status.setAverageIngestThroughput(payload.getAverageIngestThroughput());
        status.setScanRunningThroughput(payload.getScanRunningThroughput());
        status.setAverageScanThroughput(payload.getAverageScanThroughput());
        status.setProcessCpuLoad(payload.getProcessCpuLoad());
        status.setSystemCpuLoad(payload.getSystemCpuLoad());
        status.setTotalPhysicalMemorySize(payload.getTotalPhysicalMemorySize());
        status.setFreePhysicalMemorySize(payload.getFreePhysicalMemorySize());
        status.setFailedSignatureCheck(payload.getFailedSignatureCheck());
        status.setVersion(payload.getVersion());

        componentStatusDto.setExtraMetrics(objectMapper.writeValueAsString(status));

        return componentStatusDto;
    }
}
