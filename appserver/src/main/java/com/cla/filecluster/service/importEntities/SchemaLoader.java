package com.cla.filecluster.service.importEntities;

import com.beust.jcommander.internal.Lists;
import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.generic.ImportRowResultDto;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.filecluster.domain.exceptions.EntityAlreadyExistsException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;

import java.io.Reader;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * Created by liora on 27/04/2017.
 */
public abstract class SchemaLoader<T extends ImportItemRow, S> {

    private String[] requiredHeaders;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private DBTemplateUtils dBTemplateUtils;

    @Autowired
    protected MessageHandler messageHandler;

    public SchemaLoader(String[] requiredHeaders) {
        this.requiredHeaders = requiredHeaders;
    }

    protected abstract Logger getLogger();

    protected abstract T extractRowSpecific(SchemaData<T> schemaData, String[] fieldsValues, ArrayList<String> parsingWarningMessages );

    protected abstract boolean validateItemRow(S dto, T itemRow, SchemaData<T> schemaData, boolean afterAddToDB, boolean createStubs);

    protected abstract Boolean loadItemRowToRepository(SchemaData<T> schemaData, T itemRow, boolean updateDuplicates, boolean createStubs);

    protected abstract S createDtoFromRowItem(T itemRow);

    public abstract SchemaData<T> createSchemaInstance();

    protected void prepareForImport() {}

    public ImportSummaryResultDto doImport(boolean validateOnly, byte[] rawData, Integer maxErrorRowsReport, Boolean updateDuplicates) {
        return doImport(validateOnly,rawData,maxErrorRowsReport,updateDuplicates,false);
    }
   // @Transactional    save transaction per row is required
    public ImportSummaryResultDto doImport(boolean validateOnly, byte[] rawData, Integer maxErrorRowsReport, Boolean updateDuplicates,boolean createStubs) {

        SchemaData<T> schemaData = loadCsvSchema(rawData);
        ImportSummaryResultDto importResultDto;
        if (schemaData.getError()) {
            importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription(messageHandler.getMessage("import.error-csv") + ":" + schemaData.getMessage());
        }
        else {
            int successRowCounter;
            prepareForImport();
            if (validateOnly) {
                successRowCounter = validateSchema(schemaData, createStubs);
            }
            else {
                successRowCounter = loadSchema(schemaData, updateDuplicates,createStubs);
            }

            importResultDto = convertImportSummaryResultDto(schemaData, successRowCounter, maxErrorRowsReport);
        }
        return importResultDto;
    }


   private int loadSchema(SchemaData<T> schemaData, boolean updateDuplicates, boolean createStubs) //Transaction per row
   {
       getLogger().info("load schema with {} rows. ", schemaData.getRows().size());

       int successRowCounter = 0;
       for (T itemRow : schemaData.getRows()) {

           if (!itemRow.isParsingError()) {
               Boolean isValid = false;
               try {
                   //transaction is needed cause schedule group is inserted for rootfolder rows that may fail later
                   Callable<Boolean> task = () -> loadItemRowToRepository(schemaData,itemRow,updateDuplicates,createStubs);
                   isValid = dBTemplateUtils.doInTransaction(task);
               } catch (EntityAlreadyExistsException e) {
                   schemaData.addDupRowItem(itemRow, e.getMessage());

               } catch (Exception e) {
                   getLogger().warn("Failed to import row " + itemRow, e);
                   schemaData.addErrorRowItem(itemRow, e.getMessage());

               }
               if (isValid) {
                   successRowCounter++;
               }
           }
       }
       return successRowCounter;
   }

    private SchemaData<T> loadCsvSchema(byte[] rawData) {
        SchemaData<T> schemaData = createSchemaInstance();
        try {
            getLogger().debug("Extracting rows from raw data (size:{}) based on template ",rawData.length);

            final String[] headers = csvReader.getHeaders(rawData);
            for (int i=0; i< headers.length; i++) {
                headers[i] = headers[i].toUpperCase();
            }

            schemaData.setHeaders(headers, requiredHeaders);

            if(schemaData.getError()) { //Validate missing required fields
                return schemaData;
            }

			Reader reader = new StringReader(new String(rawData, Charsets.UTF_8));
            csvReader.readAndProcess(reader, row -> parseRow(schemaData, row), null,null);
        } catch (Exception e) {
            getLogger().error("Failed processing {}",e);
            schemaData.setMessage(messageHandler.getMessage("import.error-csv-load") + " " + e.getMessage());
            schemaData.setError(true);
        }
        return schemaData;

    }

    private void parseRow(SchemaData<T> schemaData, RowDTO rowDto) {
        T itemRowResult = null;
        try {
            final String[] fieldsValues = rowDto.getFieldsValues();

            if (getLogger().isDebugEnabled() && rowDto.getLineNumber() % 100 == 0) {
                getLogger().debug("Processing line #{} with the following field values {}",rowDto.getLineNumber(), Arrays.asList(fieldsValues));
            }
            if(fieldsValues.length == 0) {
                //empty line
                return;
            }
            ArrayList<String> parsingWarningMessages = new ArrayList<>();
            T itemRow = extractRowSpecific(schemaData, fieldsValues, parsingWarningMessages);
            if (itemRow != null) {
                itemRowResult = itemRow;
                itemRow.setRowDto(rowDto);
                parsingWarningMessages.forEach(item -> schemaData.addParsingErrorRowItem(itemRow, item));

                schemaData.addItemRow(itemRow);
            }
        } catch (final Exception e) {
            itemRowResult = itemRowResult!=null?itemRowResult: schemaData.createImportItemRowInstance();
            itemRowResult.setRowDto(rowDto);
            schemaData.addParsingErrorRowItem(itemRowResult,messageHandler.getMessage("import.error-parse-row") + ": " + e.getMessage());
            getLogger().error("Failed to extract row {}",rowDto,e);
        }

    }

    private int validateSchema(SchemaData<T> schemaData,boolean createStubs)
    {
        getLogger().info("validate schema with {} rows ", schemaData.getRows().size());
        int successRowCounter=0;
        S dto;
        for (T itemRow : schemaData.getRows()) {
            if(!itemRow.isParsingError()) {
                try {
                    dto = createDtoFromRowItem(itemRow);
                    if (validateItemRow(dto, itemRow, schemaData, false, createStubs)) {
                        successRowCounter++;
                    }
                } catch (Exception e) {
                    getLogger().error("Failed to import for row " + itemRow, e);
                    schemaData.addErrorRowItem(itemRow, e.getMessage());
                }
            } else {
                getLogger().info("csv item has parsing error {}", itemRow);
            }
        }
        return successRowCounter;
    }

    private ImportSummaryResultDto convertImportSummaryResultDto(SchemaData<T> schemaData, int successRowCounter,Integer maxErrorRowsReport) {
        ImportSummaryResultDto resultDto = new ImportSummaryResultDto();
        resultDto.setErrorImportRowResultDtos(convertImportItemRowsToDto(schemaData.getErrorRows(), maxErrorRowsReport));
        resultDto.setDuplicateImportRowResultDtos(convertImportItemRowsToDto(schemaData.getDuplicateRows(), maxErrorRowsReport));
        resultDto.setWarningImportRowResultDtos(convertImportItemRowsToDto(schemaData.getWarningRows(), maxErrorRowsReport));
        resultDto.setImportedRowCount(successRowCounter);
        resultDto.setDuplicateRowCount(schemaData.getDuplicateRows().size());
        resultDto.setErrorRowCount(schemaData.getErrorRows().size());
        resultDto.setWarningRowCount(schemaData.getWarningRows().size());
        resultDto.setRowHeaders(schemaData.getHeaders());
        resultDto.setResultDescription("OK");
        return resultDto;
    }

    protected String extractValue(SchemaData schemaData, String[] fieldsValues, String header, ArrayList<String> warningMessages){
        try {
            return schemaData.getHeaderIndex(header) != null ?
                    fieldsValues[schemaData.getHeaderIndex(header)] : null;
        }
        catch (Exception e) {
            getLogger().error("Failed to parse row field {} error: {} " , header,e);
            warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-str", Lists.newArrayList(header)));
            return null;
        }
    }

    protected Integer extractIntegerValue(SchemaData schemaData, String[] fieldsValues, String header, ArrayList<String> warningMessages){
        try {
            return schemaData.getHeaderIndex(header) != null && fieldsValues[schemaData.getHeaderIndex(header)] != null ?
                    Integer.valueOf(fieldsValues[schemaData.getHeaderIndex(header)]) : null;
        }
          catch (Exception e) {
              getLogger().info("Failed to parse row field {} error: {} " , header,e);
              warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-number", Lists.newArrayList(header)));
              return null;
        }
    }
    protected Long extractLongValue(SchemaData schemaData, String[] fieldsValues, String header, ArrayList<String> warningMessages){
        try {
            return schemaData.getHeaderIndex(header) != null && fieldsValues[schemaData.getHeaderIndex(header)] != null ?
                    Long.valueOf(fieldsValues[schemaData.getHeaderIndex(header)]) : null;
        }
        catch (Exception e) {
            getLogger().info("Failed to parse row field {} error: {} " , header,e);
            warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-number", Lists.newArrayList(header)));
            return null;
        }
    }
    protected Boolean extractBooleanValue(SchemaData schemaData, String[] fieldsValues, String header, ArrayList<String> warningMessages){
        try {
            return schemaData.getHeaderIndex(header) != null && fieldsValues[schemaData.getHeaderIndex(header)] != null ?
                    Boolean.valueOf(fieldsValues[schemaData.getHeaderIndex(header)]) : null;
        }
          catch (Exception e) {
              getLogger().info("Failed to parse row field {} error: {} " , header,e);
              warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-bool", Lists.newArrayList(header)));
              return null;

        }
    }
 protected LocalDateTime extractDateValue(SchemaData schemaData, String[] fieldsValues, String header, ArrayList<String> warningMessages){
     DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        try {
            return schemaData.getHeaderIndex(header) != null && fieldsValues[schemaData.getHeaderIndex(header)] != null ?
                    LocalDateTime.parse(fieldsValues[schemaData.getHeaderIndex(header)],formatter) : null;
        }
          catch (Exception e) {
              getLogger().info("Failed to parse row field {} error: {} " , header,e);
              warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-date", Lists.newArrayList(header)));
              return null;

        }
    }

    protected  <R extends Enum> List<R>  extractMultipleEnums(SchemaData schemaData, String[] fieldsValues, String header, Class<R> tClass ,ArrayList<String> warningMessages){
        if(schemaData.getHeaderIndex(header) == null || fieldsValues[schemaData.getHeaderIndex(header)] == null) {
            return Lists.newArrayList();
        }

        try {
            String enums = fieldsValues[schemaData.getHeaderIndex(header)];

            String[] split = enums.split(";");
            Stream<R> rStream = Stream.of(split)
                    .map(str -> (R) Enum.valueOf(tClass, str));
            List<R> rList = rStream.collect(Collectors.toList());
            return rList;
        }
        catch (Exception e) {
            getLogger().info("Failed to parse row field {} error: {} " , header,e);
             List<Enum> enumValues = new ArrayList<>(EnumSet.allOf(tClass));
            String enumNames = Stream.of(enumValues).map(Object::toString)
                    .collect(Collectors.joining(", "));
            warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-enum-arr", Lists.newArrayList(header, enumNames)));
            return Lists.newArrayList();
        }
    }

    @SuppressWarnings("unchecked")
    protected  String[]  extractMultipleStrings(SchemaData schemaData, String[] fieldsValues, String header ,ArrayList<String> warningMessages){
        if(schemaData.getHeaderIndex(header) == null || fieldsValues[schemaData.getHeaderIndex(header)] == null) {
            return null;
        }

        try {
            String vals = fieldsValues[schemaData.getHeaderIndex(header)];

            return vals.split(";");
        }
        catch (Exception e) {
            getLogger().info("Failed to parse row field {} error: {} " , header,e);
            warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-string-arr", Lists.newArrayList(header)));
            return null;
        }
    }


    @SuppressWarnings("unchecked")
    protected <R extends Enum> R extractValue(SchemaData schemaData, String[] fieldsValues, String header, Class<R> tClass ,ArrayList<String> warningMessages) {
        try {
            String result = schemaData.getHeaderIndex(header) != null &&  fieldsValues[schemaData.getHeaderIndex(header)]!=null ?
                    fieldsValues[schemaData.getHeaderIndex(header)] : null;
            if (Strings.isNullOrEmpty(result)) {
                return null;
            }
            return (R) Enum.valueOf(tClass, result);
        }
        catch (Exception e) {
            getLogger().info("Failed to parse row field {} error: {} " , header,e);
            List<Enum> enumValues = new ArrayList<>(EnumSet.allOf(tClass));
            String enumNames = Stream.of(enumValues).map(Object::toString)
                                     .collect(Collectors.joining(", "));
            warningMessages.add(messageHandler.getMessage("import.error-parse-row-field-enum", Lists.newArrayList(header, enumNames)));
            return null;
        }
    }

    public static List<ImportRowResultDto> convertImportItemRowsToDto(List<? extends ImportItemRow> importItemRows, Integer maxErrorRowsReport) {
        maxErrorRowsReport = maxErrorRowsReport == null ? Integer.MAX_VALUE : maxErrorRowsReport;
        if (importItemRows.size() == 0) {
            return new ArrayList<>();
        }
        maxErrorRowsReport = maxErrorRowsReport > importItemRows.size() ? importItemRows.size() : maxErrorRowsReport;
        return importItemRows.subList(0, maxErrorRowsReport).stream()
                .map(SchemaLoader::convertImportItemRowToDto)
                .collect(Collectors.toList());
    }

    private static ImportRowResultDto convertImportItemRowToDto(ImportItemRow itemRow) {
        ImportRowResultDto importRowResultDto = new ImportRowResultDto();
        importRowResultDto.setMessage(itemRow.getMessage());
        importRowResultDto.setImportFailed(itemRow.getError());
        importRowResultDto.setRowDto(itemRow.getRowDto());
        return importRowResultDto;
    }
}
