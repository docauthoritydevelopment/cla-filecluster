package com.cla.filecluster.service.export.exporters.active_scans.mixin;

import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.active_scans.ActiveScansHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class CrawlRunDetailsDtoMixin {

    @JsonProperty(PROCESSED_FILES)
    private Long totalProcessedFiles;

    @JsonProperty(START_DATE)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long scanStartTime;
}
