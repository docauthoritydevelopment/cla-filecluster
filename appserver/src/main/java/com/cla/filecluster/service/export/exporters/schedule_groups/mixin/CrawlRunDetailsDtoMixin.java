package com.cla.filecluster.service.export.exporters.schedule_groups.mixin;

import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.schedule_groups.ActiveScheduledGroupsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class CrawlRunDetailsDtoMixin {

    @JsonProperty(LAST_SCAN)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long scanStartTime;
}
