package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.stream.Collectors;


@Component
public class ClientExtractionEnricher implements DefaultTypeDataEnricher {


    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Autowired
    private BizListItemService bizListItemService;


    @Override
    public String convertSolrValueToId(String value) {
        return AssociationIdUtils.extractBizListItemIdentfier(value);
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return bizListItemService.getFromCache(convertSolrValueToId(category)).getName();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        String facetPrefix = FileDtoFieldConverter.BIZLIST_ITEM_SOLR_PREFIX
                + BizListItemType.CLIENTS.ordinal() + BizListAppService.BIZ_LIST_SEPERATOR;
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.EXTRACTED_ENTITIES_IDS, facetPrefix);
    }

}
