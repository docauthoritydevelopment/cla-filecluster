package com.cla.filecluster.service.chain;

import com.beust.jcommander.internal.Lists;
import com.cla.connector.utils.TimeSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

/**
 * This is a chain processor
 * it has links which process a given context until done
 * also has an error handler on failure
 *
 * Created by: yael
 * Created on: 8/4/2019
 */
public class Chain<T extends ChainContext> {

    private static final Logger logger = LoggerFactory.getLogger(Chain.class);

    private String name;
    private List<ChainLink<T>> links = Lists.newArrayList();
    private ChainLink<T> errorHandler;

    private TimeSource timeSource = new TimeSource();

    public Chain(String name) {
        this.name = name;
    }

    public static <S extends ChainContext> Chain<S> create(String name) {
        return new Chain<>(name);
    }

    public Chain<T> next(ChainLink<T> link) {
        links.add(link);
        return this;
    }

    public Chain<T> setErrorHandler(ChainLink<T> errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    public void handle(T context) {
        if (links.isEmpty()) {
            logger.warn("Chain {} with no links, cant handle context {}", name, context);
            return;
        }

        boolean shouldContinue = true;
        Iterator<ChainLink<T>> iterator = links.iterator();

        try {
            while (iterator.hasNext() && shouldContinue) {
                ChainLink<T> link = iterator.next();
                long start = timeSource.currentTimeMillis();
                logger.trace("Test whether should run chain {} link {} for context {}",
                        name, link.getClass().getCanonicalName(), context);
                if (link.shouldHandle(context)) {
                    logger.trace("Run chain {} link {} for context {}",
                            name, link.getClass().getCanonicalName(), context);
                    shouldContinue = link.handle(context);
                } else {
                    logger.trace("Ignore chain {} link {} for context {}",
                            name, link.getClass().getCanonicalName(), context);
                }
                logger.trace("Took {} ms to process chain {} link {} for context {}",
                        timeSource.millisSince(start), name, link.getClass().getCanonicalName(), context);
            }
        } catch (Exception e) {
            context.setErrorOccurred(true);
            logger.error("failed running chain {} for context {}", name, context, e);
        } finally {
            if (context.errorOccurred() && errorHandler != null) {
                errorHandler.handle(context);
            }
        }
    }
}
