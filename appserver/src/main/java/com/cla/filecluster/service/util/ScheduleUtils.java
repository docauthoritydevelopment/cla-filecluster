package com.cla.filecluster.service.util;

import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.common.domain.dto.schedule.ScheduleType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.support.CronSequenceGenerator;

import java.io.IOException;
import java.util.Arrays;
import java.util.TimeZone;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 6/3/2019
 */
public class ScheduleUtils {

    private static Logger logger = LoggerFactory.getLogger(ScheduleUtils.class);

    public static String convertScheduleConfigDtoToString(ScheduleConfigDto scheduleConfigDto) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(scheduleConfigDto);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert schedule config dto to JSON string (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert schedule config dto to JSON string (IO Exception): " + e.getMessage(), e);
        }
    }

    public static ScheduleConfigDto convertScheduleConfigDtoFromString(String schedulingJson) {
        if (StringUtils.isEmpty(schedulingJson)) {
            return null;
        }
        try {
            return ModelDtoConversionUtils.getMapper().readValue(schedulingJson, ScheduleConfigDto.class);
        } catch (IOException e) {
            logger.error("Failed to convert schedule config dto from string [" + schedulingJson + "]", e);
            throw new RuntimeException("Failed to convert schedule config dto from string [" + schedulingJson + "]", e);
        }
    }

    /*
    crontab: <Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week>

           field         allowed values
           -----         --------------
           minute        0-59
           hour          0-23
           day of month  1-31
           month         1-12 (or names, see below)
           day of week   0-7 (0 or 7 is Sun, or use names)
    */
    public static String convertScheduleConfigDtoToCronTrigger(ScheduleConfigDto scheduleConfigDto) {
        String template;
        switch (scheduleConfigDto.getScheduleType()) {
            case HOURLY:
                template = "0 {minutes} */{every} * * *";
                break;
            case DAILY:
                template = "0 {minutes} {hour} */{every} * *";
                break;
            case WEEKLY:
                template = "0 {minutes} {hour} * * {days}";
                break;
            case CUSTOM:
                template = scheduleConfigDto.getCustomCronConfig();
                break;
            default:
                throw new BadRequestException("scheduleType", String.valueOf(scheduleConfigDto.getScheduleType()), BadRequestType.UNSUPPORTED_VALUE);
        }

        if (scheduleConfigDto.getDaysOfTheWeek() != null && !scheduleConfigDto.getScheduleType().equals(ScheduleType.WEEKLY)) {
            scheduleConfigDto.setDaysOfTheWeek(null);
        }
        if (scheduleConfigDto.getHour() > 0 && scheduleConfigDto.getScheduleType().equals(ScheduleType.HOURLY)) {
            scheduleConfigDto.setHour(0);
        }
        if (scheduleConfigDto.getEvery() > 0 && scheduleConfigDto.getScheduleType().equals(ScheduleType.WEEKLY)) {
            scheduleConfigDto.setEvery(0);
        }
        if (!scheduleConfigDto.getScheduleType().equals(ScheduleType.CUSTOM)) {
            try {
                template = template.replace("{minutes}", String.valueOf(scheduleConfigDto.getMinutes()));
                template = template.replace("{hour}", String.valueOf(scheduleConfigDto.getHour()));
                template = template.replace("{every}", String.valueOf(scheduleConfigDto.getEvery()));
                if (scheduleConfigDto.getDaysOfTheWeek() != null && scheduleConfigDto.getDaysOfTheWeek().length > 0) {
                    String days = Arrays.stream(scheduleConfigDto.getDaysOfTheWeek()).distinct().collect(Collectors.joining(","));
                    template = template.replace("{days}", days);
                }
            } catch (RuntimeException e) {
                logger.error("Failed to convert scheduleConfigDto {} to cron string", scheduleConfigDto, e);
                throw new BadRequestException("Failed to convert scheduleConfigDto {} to cron string: " + e.getMessage(), e, BadRequestType.UNSUPPORTED_VALUE);
            }
        }
        logger.debug("Converted scheduleConfigDto {} to cron string {}", scheduleConfigDto, template);

        if (!StringUtils.isEmpty(template)) {
            try {
                new CronSequenceGenerator(template, TimeZone.getDefault());
            } catch (Exception e) {
                logger.warn("Invalid cron trigger expression {}", template, e);
                throw new BadRequestException("Illegal cron expression {}", template, BadRequestType.UNSUPPORTED_VALUE);
            }
        }

        return template;
    }
}
