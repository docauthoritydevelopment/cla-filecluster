package com.cla.filecluster.service.api.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.logging.LoggersEndpoint;
import org.springframework.boot.logging.LogLevel;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Extention for the Spring Boot Actuator Loggers endpoint.
 * Returns only loggers that have non-null configured level.
 */
@RestController
@RequestMapping("/api/custom-loggers")
public class CustomizedLoggersEndpointController {

    private static final String LOGGERS_KEY = "loggers";

    @Autowired
    private LoggersEndpoint delegate;

    @GetMapping("/loggers")
    public @ResponseBody Map<String, Object> loggers(){
        Map<String, Object> loggers = delegate.loggers();
        @SuppressWarnings("unchecked")
        Map<String, LoggersEndpoint.LoggerLevels> loggersMap =
                (Map<String, LoggersEndpoint.LoggerLevels>)loggers.get(LOGGERS_KEY);
        Map<String, LoggersEndpoint.LoggerLevels> loggersEntry = loggersMap.entrySet().stream()
                .filter(entry -> entry.getValue().getConfiguredLevel() != null)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        loggers.put(LOGGERS_KEY, loggersEntry);
        return loggers;
    }

    @RequestMapping(value="/configure", method = RequestMethod.POST)
    public void configureLogLevel(@RequestParam String name,
                                  @RequestParam LogLevel configuredLevel) {
        delegate.configureLogLevel(name, configuredLevel);
    }
}
