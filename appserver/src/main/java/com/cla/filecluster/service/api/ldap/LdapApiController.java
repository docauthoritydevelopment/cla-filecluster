package com.cla.filecluster.service.api.ldap;

import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.domain.dto.security.BizRoleDto;
import com.cla.common.domain.dto.security.LdapGroupMappingDto;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.entity.security.BizRole;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.operation.FileAclReTranslateDomain;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.security.BizRoleService;
import com.cla.filecluster.service.security.LdapGroupMappingService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.security.Authority.Const.GENERAL_ADMIN;
import static com.cla.common.domain.dto.security.Authority.Const.TECH_SUPPORT;

/**
 * Created by Itai Marko.
 */
@RestController
@RequestMapping("/api/ldap")
public class LdapApiController {

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private LdapGroupMappingService ldapGroupMappingService;

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/mappings", method = {RequestMethod.GET})
    public List<LdapGroupMappingDto> getLdapGroupMappings() {
        return ldapGroupMappingService.getAllGroupMappings();
    }

    @RequestMapping(value = "/mappings",  method = {RequestMethod.POST})
    public LdapGroupMappingDto saveLdapGroupMappings(@RequestBody LdapGroupMappingDto ldapGroupMappingDto) {
        //TODO: Move this to BizRole controller
        if(ldapGroupMappingDto!=null && !ldapGroupMappingDto.getDaRolesIds().isEmpty()) {
            Long roleId = ldapGroupMappingDto.getDaRolesIds().stream().findFirst().get();
            BizRole bizRole = bizRoleService.getBizRole(roleId);
            List<LdapGroupMappingDto> mappingDtos = ldapGroupMappingService.saveGroupMappingsForRole(bizRole, Sets.newHashSet(ldapGroupMappingDto));
            LdapGroupMappingDto resultDto =  mappingDtos.isEmpty()? null: mappingDtos.get(0);
            eventAuditService.auditDual(AuditType.OPERATIONAL_ROLE,"Attach ldap group to operational role",
                AuditAction.ATTACH, BizRoleDto.class, ldapGroupMappingDto.getDaRolesIds(),  LdapGroupMappingDto.class,resultDto.getId(),resultDto);
           return resultDto;
        }
        return null;
    }

    @RequestMapping(value = "/{bizRoleId}/mappings",  method = {RequestMethod.DELETE})
    public void deleteLdapGroupMappings(@PathVariable long bizRoleId) {
        ldapGroupMappingService.deleteAllGroupMappingsForDaRole(bizRoleId);
        eventAuditService.audit(AuditType.OPERATIONAL_ROLE,"Detach ldap group from operational role",
                AuditAction.DETACH, BizRoleDto.class, bizRoleId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {GENERAL_ADMIN, TECH_SUPPORT})
    @RequestMapping(value = "/re-translate/domain/{domain}", method = {RequestMethod.GET, RequestMethod.POST})
    public void createDeleteRootFolderOperation(@PathVariable String domain) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.ACL_RE_TRANSLATE_DOMAIN);
        data.setOpData(new HashMap<>());
        data.getOpData().put(FileAclReTranslateDomain.DOMAIN_PARAM, domain);
        data.setUserOperation(true);
        data.setDescription(messageHandler.getMessage("operation-description.re-translate-domain",
                Lists.newArrayList(domain)));
        scheduledOperationService.createNewOperation(data);
    }
}
