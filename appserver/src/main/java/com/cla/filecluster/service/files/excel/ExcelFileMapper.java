package com.cla.filecluster.service.files.excel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import org.springframework.stereotype.Component;
import org.springframework.util.SerializationUtils;

import com.cla.filecluster.domain.dto.excel.ExcelFile;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbook;

@Component
public class ExcelFileMapper {
	private static final int compressionThreshold = 800000;
	
	public ExcelFile toExcelFile(final ExcelWorkbook excelWorkbook){		
		try {
			final ExcelFile excelFile = new ExcelFile(excelWorkbook.getFileName());
			excelFile.setAnalyzedData(serialize(excelWorkbook));
			return excelFile;
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	private byte[] serialize(final ExcelWorkbook excelWorkbook) throws IOException {
//		String str = objectMapper.writeValueAsString(excelWorkbook);
//		claFile.setAnalyzedData(compress(str));
//		claFile.setAnalyzedData(SerializationUtils.serialize(excelWorkbook));
		return compress(SerializationUtils.serialize(excelWorkbook));
	}

	private byte[] compress(final String str) throws IOException {
		if (str == null || str.length() == 0) {
			return null;
		}
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final GZIPOutputStream gzip = new GZIPOutputStream(out);
		gzip.write(str.getBytes("UTF-8"));
		gzip.close();
		return out.toByteArray();
	}

	private byte[] compress(final byte[] str) throws IOException {
		if (str == null || str.length == 0) {
			return null;
		}
		/*
		 * Conditional compression:
		 * If data is small and doesn't match ZIP MAGIC prefix, don't compress it.
		 */
		// 
		if ((str.length < compressionThreshold) && (str[0] != 31 || str[1] != -117)) return str;

		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final GZIPOutputStream gzip = new GZIPOutputStream(out);
		gzip.write(str);
		gzip.close();
		return out.toByteArray();
	}

}
