package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.action.ActionScheduleDto;
import com.cla.common.domain.dto.action.ActionTriggerType;
import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.domain.entity.actions.ActionSchedule;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.actions.ActionScheduleRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.util.ScheduleUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;


/**
 * service for scheduled actions management (CRUD)
 *
 * Created by: yael
 * Created on: 3/5/2018
 */
@Service
public class ActionScheduleService {

    private static final Logger logger = LoggerFactory.getLogger(ActionScheduleService.class);

    @Autowired
    private ActionScheduleRepository actionScheduleRepository;

    @Autowired
    private ActionExecutionStatusService actionExecutionStatusService;

    @Autowired
    private TaskScheduler defaultScheduler;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${action-scheduler.active:true}")
    private boolean schedulerActive;

    @Value("${subprocess:false}")
    private boolean subProcess;

    private Map<Long, ScheduledFuture> activeSchedules = new ConcurrentHashMap<>();

    @PostConstruct
    void startSchedules() {
        if (!schedulerActive || subProcess) {
            logger.warn("Scheduler is not active.");
            return;
        }
        DBTemplateUtils.doInTransaction(this::startScheduleActions);
    }

    @Transactional
    private void startScheduleActions() {
        List<ActionSchedule> scheduleActions = actionScheduleRepository.findAll().stream().filter(ActionSchedule::isActive).collect(Collectors.toList());

        logger.info("Start scheduler. Schedule All active ({}) ScheduleActions", scheduleActions.size());
        for (ActionSchedule scheduleAction : scheduleActions) {
            try {
                startScheduleAction(convertActionSchedule(scheduleAction));
            } catch (RuntimeException e) {
                logger.error("Failed to start schedule action. Unexpected error {}", scheduleAction, e);
            }
        }
    }

    public boolean isActionScheduled(ActionScheduleDto scheduleAction) {
        return activeSchedules.containsKey(scheduleAction.getId());
    }

    @Transactional
    public void startScheduleAction(ActionScheduleDto scheduleAction) {
        if (isActionScheduled(scheduleAction)) {
            logger.warn("Unable to start scheduling action {}. Action already scheduled", scheduleAction);
            return;
        }
        logger.debug("start Schedule Action {}", scheduleAction);
        Trigger trigger = new CronTrigger(scheduleAction.getCronTriggerString(), TimeZone.getDefault());
        ActionRunnerInterface runner = applicationContext.getBean("actionRunner", ActionRunnerInterface.class);
        runner.setActionScheduleId(scheduleAction.getId());
        ScheduledFuture<?> schedule = defaultScheduler.schedule(runner, trigger);
        activeSchedules.put(scheduleAction.getId(), schedule);
    }

    public void stopScheduleAction(ActionSchedule scheduleAction, boolean interruptIfRunning) {
        logger.debug("stop Schedule Action {}", scheduleAction);
        ScheduledFuture scheduledFuture = activeSchedules.get(scheduleAction.getId());
        if (scheduledFuture != null) {
            scheduledFuture.cancel(interruptIfRunning);
            activeSchedules.remove(scheduleAction.getId());
        }
    }

    @Transactional
    public ActionScheduleDto createActionSchedule(ActionScheduleDto scheduleDto) {
        validateActionSchedule(scheduleDto, true);
        ActionSchedule schedule = convertActionScheduleDto(scheduleDto);
        ActionScheduleDto res = convertActionSchedule(actionScheduleRepository.save(schedule));
        if (res.getActive()) {
            startScheduleAction(res);
        }
        return res;
    }

    @Transactional(readOnly = true)
    public ActionScheduleDto getActionSchedule(Long scheduleId) {
        return actionScheduleRepository.findById(scheduleId)
                .map(ActionScheduleService::convertActionSchedule)
                .orElse(null);
    }

    @Transactional(readOnly = true)
    public ActionScheduleDto getActionScheduleByName(String name) {
        return convertActionSchedule(actionScheduleRepository.findByName(name));
    }

    @Transactional
    public void deleteActionSchedule(long id) {
        actionScheduleRepository.deleteById(id);
    }

    private void validateActionSchedule(ActionScheduleDto dto, boolean validateNameUnique) {

        if (Strings.isNullOrEmpty(dto.getName())) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.empty"), BadRequestType.MISSING_FIELD);
        }

        if (validateNameUnique) {
            ActionSchedule scheduleByName = actionScheduleRepository.findByName(dto.getName());
            if (scheduleByName != null) {
                throw new BadRequestException(messageHandler.getMessage("action-schedule.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }

        if (dto.getScheduleConfigDto() != null && dto.getScheduleConfigDto().getScheduleType() == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.schedule-type.empty"), BadRequestType.MISSING_FIELD);
        }
        if (dto.getActionExecutionTaskStatusId() < 0) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.task-status.empty"), BadRequestType.MISSING_FIELD);
        }

        ActionExecutionTaskStatus task = actionExecutionStatusService.getActionExecutionTaskStatus(dto.getActionExecutionTaskStatusId());
        if (task == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.task-status.empty"), BadRequestType.MISSING_FIELD);
        }

        if (!ActionTriggerType.POLICY.equals(task.getType())) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.task-type.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Transactional
    public ActionScheduleDto updateActionSchedule(ActionScheduleDto dto) {
        ActionSchedule one = actionScheduleRepository.findById(dto.getId()).orElse(null);
        if (one == null) {
            logger.error("Could not find ActionSchedule id {}.", dto.getId());
            return null;
        }
        validateActionSchedule(dto, !one.getName().equalsIgnoreCase(dto.getName()));
        logger.info("Update actionSchedule {}", one);
        one.setActionExecutionTaskStatusId(dto.getActionExecutionTaskStatusId());
        one.setCronTriggerString(dto.getCronTriggerString());
        one.setResultPath(dto.getResultPath());
        one.setName(dto.getName());
        boolean wasActive = one.isActive();
        one.setActive(dto.getActive());
        String originalCronString = one.getCronTriggerString();
        ScheduleConfigDto scheduleConfigDto = dto.getScheduleConfigDto();
        String cronTriggerString = ScheduleUtils.convertScheduleConfigDtoToCronTrigger(scheduleConfigDto);
        one.setSchedulingJson(ScheduleUtils.convertScheduleConfigDtoToString(scheduleConfigDto));
        one.setCronTriggerString(cronTriggerString);
        ActionScheduleDto res = convertActionSchedule(actionScheduleRepository.save(one));

        if (res.getActive() && isActionScheduled(res) && !cronTriggerString.equals(originalCronString)) {
            logger.info("Schedule action cronTrigger changed. Refreshing schedule");
            stopScheduleAction(one, false);
            startScheduleAction(res);
        } else if (wasActive && !isActionScheduled(res)) {
            logger.info("Schedule action {} changed to inactive. Stop scheduling it");
            stopScheduleAction(one, false);
        } else if (!wasActive && isActionScheduled(res)) {
            logger.info("Schedule action {} changed to active. Start scheduling it");
            startScheduleAction(res);
        }
        return res;
    }

    @Transactional(readOnly = true)
    public Page<ActionScheduleDto> listActionSchedules(DataSourceRequest dataSourceRequest) {
        logger.debug("List all action schedules ");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<ActionSchedule> schedules = actionScheduleRepository.findAll(pageRequest);
        List<ActionScheduleDto> res = convertActionSchedules(schedules.getContent());
        return new PageImpl<>(res, pageRequest, schedules.getTotalElements());
    }

    @Transactional
    public ActionScheduleDto updateScheduleActionActive(Long id, Boolean state) {
        if (state == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.state.empty"), BadRequestType.MISSING_FIELD);
        }
        logger.info("Set ScheduleAction active {} for ScheduleAction ID {}", (state ? "on" : "off"), id);
        ActionScheduleDto scheduleActionDto = getActionSchedule(id);
        if (scheduleActionDto == null) {
            logger.error("Could not find ActionSchedule id {}.", id);
            return null;
        }
        scheduleActionDto.setActive(state);
        return updateActionSchedule(scheduleActionDto);
    }

    public static List<ActionScheduleDto> convertActionSchedules(Collection<ActionSchedule> actionSchedule) {
        return actionSchedule.stream().map(ActionScheduleService::convertActionSchedule).collect(Collectors.toList());
    }

    public static ActionScheduleDto convertActionSchedule(ActionSchedule actionSchedule) {
        if (actionSchedule == null) return null;
        ActionScheduleDto actionScheduleDto = new ActionScheduleDto();
        actionScheduleDto.setId(actionSchedule.getId());
        actionScheduleDto.setName(actionSchedule.getName());
        actionScheduleDto.setDateCreated(actionSchedule.getDateCreated());
        actionScheduleDto.setDateModified(actionSchedule.getDateModified());
        actionScheduleDto.setActionExecutionTaskStatusId(actionSchedule.getActionExecutionTaskStatusId());
        actionScheduleDto.setCronTriggerString(actionSchedule.getCronTriggerString());
        actionScheduleDto.setResultPath(actionSchedule.getResultPath());
        actionScheduleDto.setActive(actionSchedule.isActive());
        actionScheduleDto.setLastExecutionDate(actionSchedule.getLastExecutionDate());
        ScheduleConfigDto scheduleConfigDto = ScheduleUtils.convertScheduleConfigDtoFromString(actionSchedule.getSchedulingJson());
        actionScheduleDto.setScheduleConfigDto(scheduleConfigDto);
        return actionScheduleDto;
    }

    public static ActionSchedule convertActionScheduleDto(ActionScheduleDto actionScheduleDto) {
        if (actionScheduleDto == null) return null;
        ActionSchedule actionSchedule = new ActionSchedule();
        actionSchedule.setId(actionScheduleDto.getId());
        actionSchedule.setName(actionScheduleDto.getName());
        actionSchedule.setDateCreated(actionScheduleDto.getDateCreated());
        actionSchedule.setDateModified(actionScheduleDto.getDateModified());
        actionSchedule.setActionExecutionTaskStatusId(actionScheduleDto.getActionExecutionTaskStatusId());
        actionSchedule.setCronTriggerString(actionScheduleDto.getCronTriggerString());
        actionSchedule.setResultPath(actionScheduleDto.getResultPath());
        actionSchedule.setActive(actionScheduleDto.getActive());
        actionSchedule.setLastExecutionDate(actionScheduleDto.getLastExecutionDate());
        ScheduleConfigDto scheduleConfigDto = actionScheduleDto.getScheduleConfigDto();
        String cronTriggerString = ScheduleUtils.convertScheduleConfigDtoToCronTrigger(scheduleConfigDto);
        actionSchedule.setSchedulingJson(ScheduleUtils.convertScheduleConfigDtoToString(scheduleConfigDto));
        actionSchedule.setCronTriggerString(cronTriggerString);
        return actionSchedule;
    }
}
