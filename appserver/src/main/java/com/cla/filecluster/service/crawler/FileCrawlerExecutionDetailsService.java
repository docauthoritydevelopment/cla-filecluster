package com.cla.filecluster.service.crawler;

import com.cla.common.domain.dto.crawler.*;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.jobmanager.RunSummaryInfoDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.schedule.RootFolderSchedule;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.managers.FileContentService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.cla.filecluster.util.FileCrawlerUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by uri on 28/10/2015.
 */
@Service
public class FileCrawlerExecutionDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(FileCrawlerExecutionDetailsService.class);

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private OpStateService opStateService;

    private final static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private FileContentService fileContentService;

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    private FileCrawlerExecDetailsServiceTxUtil fileCrawlerExecDetailsServiceTxUtil;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private EventBus eventBus;

    private final static long TIME_ZERO = 0L;

    private LoadingCache<Long, CrawlRun> crawlRunCache;

    @Value("${run-manager.scheduleGroup.create.new.scan.for.paused.rootfolder:false}")
    private boolean scheduleGroupNewScanForPausedRun;

    @SuppressWarnings("unused")
    @PostConstruct
    void init() {
        crawlRunCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, CrawlRun>() {
                    @Override
                    public CrawlRun load(Long crawlRunId) {
                        return getCrawlRun(crawlRunId);
                    }
                });
    }

    public CrawlRun getCrawlRunFromCache(Long crawlRunId) {
        return crawlRunCache.getUnchecked(crawlRunId);
    }

    public void updateCrawlRunGracefulStop(Long runId) {
        fileCrawlerExecDetailsServiceTxUtil.updateCrawlRunGracefulStop(runId);
        crawlRunCache.invalidate(runId);
    }

    private String createRunConfig(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error("Failed to write runConfig as json", e);
            return null;
        }
    }

    public void updateCrawlRunState(Long crawlRunId, RunStatus runStatus) {
        handleUpdateRunStateAndClean(crawlRunId, runStatus, null);
    }

    private CrawlRun createNewRun(String runConfig) {
        CrawlRun crawlRun = new CrawlRun();
        crawlRun.setRunConfig(runConfig);
        crawlRun.setRunStatus(RunStatus.RUNNING);
        crawlRun.setStartTime(new Date(System.currentTimeMillis()));
        crawlRun.setManual(true);
        crawlRun.setRootFolderAccessible(true);
        return crawlRunRepository.save(crawlRun);
    }

    @Transactional
    public CrawlRun createRootFolderCrawlRun(RootFolderDto rootFolderDto, List<String> opList) {
        String error = validateStartRootFolderCrawlRun(rootFolderDto);
        if (error != null) {
            logger.info(error);
            return null;
        }
        if (opList == null || opList.size() == 0) {
            throw new RuntimeException(messageHandler.getMessage("run.create.empty-op-list"));
        }

        String runConfig = createRunConfig(rootFolderDto);
        CrawlRun newRun = createNewRun(runConfig);
        newRun.setRunType(RunType.ROOT_FOLDER);
        newRun.setHasChildRuns(false);
        populateCrawlRunAndCreateJobs(newRun, rootFolderDto, opList);
        CrawlRun save = crawlRunRepository.save(newRun);
        docStoreService.evictFromCache(rootFolderDto.getId());
        docStoreService.attachRunToRootFolder(save.getId(), rootFolderDto.getId());
        rootFolderDto.setLastRunId(save.getId());
        return save;
    }

    private void populateCrawlRunAndCreateJobs(CrawlRun newRun, RootFolderDto rootFolderDto, List<String> opList) {
        //Create all mandatory jobs regardless of opList. Pause when opList jobs are missing to allow further resume.
        if (opList.contains(FileCrawlerUtils.SCAN)) {
            newRun.setScanFiles(true);
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.SCAN);
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.SCAN_FINALIZE);
        }

        if (opList.contains(FileCrawlerUtils.SCAN) || opList.contains(FileCrawlerUtils.INGEST)) {
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.INGEST);
        }

        if (opList.contains(FileCrawlerUtils.INGEST)) {
            newRun.setIngestFiles(true);
        } else {
            newRun.setIngestFiles(false);
        }

        if (opList.contains(FileCrawlerUtils.SCAN) || opList.contains(FileCrawlerUtils.INGEST)) {
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.OPTIMIZE_INDEXES);
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.INGEST_FINALIZE);
        }

        if (opList.contains(FileCrawlerUtils.SCAN) || opList.contains(FileCrawlerUtils.ANALYZE)) {
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.ANALYZE);
        }

        if (opList.contains(FileCrawlerUtils.ANALYZE)) {
            newRun.setAnalyzeFiles(true);
        } else {
            newRun.setAnalyzeFiles(false);
        }

        if (opList.contains(FileCrawlerUtils.SCAN) || opList.contains(FileCrawlerUtils.ANALYZE)) {
            jobManagerService.createRootFolderJob(rootFolderDto.getId(), newRun.getId(), JobType.ANALYZE_FINALIZE);
        }

        newRun.setRootFolderId(rootFolderDto.getId());
        newRun.setRootFolderAccessible(fileContentService.isDirectoryExists(rootFolderDto) == DirectoryExistStatus.YES);
        opStateService.informRunStarted(rootFolderDto, newRun.getId());
        if (rootFolderDto.getLastRunId() != null) {
            populateCrawlRunWithDataFromPreviousRun(rootFolderDto.getLastRunId(), newRun);
        }
    }

    @Transactional
    public CrawlRun updateScheduleGroupRootFolderCrawlRun(CrawlRun scheduleGroupCrawlRun, CrawlRun rootFolderCrawlRun) {
        if (rootFolderCrawlRun != null) {
            rootFolderCrawlRun.setScheduleGroupId(scheduleGroupCrawlRun.getScheduleGroupId());
            rootFolderCrawlRun.setParentRunId(scheduleGroupCrawlRun.getId());
            rootFolderCrawlRun.setHasChildRuns(false);
            rootFolderCrawlRun.setManual(scheduleGroupCrawlRun.isManual());
            return crawlRunRepository.save(rootFolderCrawlRun);
        }
        return null;
    }

    @Transactional
    public CrawlRun createScheduleGroupCrawlRun(ScheduleGroupDto scheduleGroup, List<RootFolderDto> rootFolderDtos, Boolean manual, List<String> opList) {
        String error = validateStartScheduleGroupCrawlRun(scheduleGroup, rootFolderDtos);
        if (error != null) {
            logger.info(error);
            return null;
        }
        return createScheduleGroupCrawlRun(scheduleGroup, manual, opList);
    }

    private boolean isRootFolderCurrentlyScanned(RootFolderDto rootFolderDto) {
        if (rootFolderDto.getLastRunId() != null) {
            CrawlRun crawlRun = getCrawlRun(rootFolderDto.getLastRunId());
            return !crawlRun.getRunStatus().isFinished();
        }
        return false;
    }

    public String validateStartRootFolderCrawlRun(RootFolderDto rootFolderDto) {
        licenseService.validateCrawlerExpiry();

        if (isRootFolderCurrentlyScanned(rootFolderDto)) {
            return messageHandler.getMessage("run.start.already-running", Lists.newArrayList(rootFolderDto.getId()));
        }

        Optional<MediaConnectionDetailsDto> connectionDetails =
                mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(rootFolderDto.getId(), false);

        if (!connectionDetails.isPresent()) {
            logger.info("Failed to get connection details on root folder {}", rootFolderDto.getId());
            return messageHandler.getMessage("run.start.no-conn", Lists.newArrayList(rootFolderDto.getId()));
        }

        return null;
    }

    public String validateStartScheduleGroupRootFolderCrawlRun(RootFolderDto rootFolderDto) {
        if (rootFolderDto.getLastRunId() != null) {
            CrawlRun crawlRun = getCrawlRun(rootFolderDto.getLastRunId());
            if (RunStatus.RUNNING.equals(crawlRun.getRunStatus())) {
                return messageHandler.getMessage("run.start.already-running", Lists.newArrayList(rootFolderDto.getId()));

            } else if (!scheduleGroupNewScanForPausedRun && RunStatus.PAUSED.equals(crawlRun.getRunStatus())) {
                return messageHandler.getMessage("run.group.skip-pause", Lists.newArrayList(rootFolderDto.getId()));
            }
        }
        Optional<MediaConnectionDetailsDto> connectionDetails =
                mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(rootFolderDto.getId(), false);

        if (!connectionDetails.isPresent()) {
            logger.info("Failed to get connection details on root folder {}", rootFolderDto.getId());
            return messageHandler.getMessage("run.start.no-conn", Lists.newArrayList(rootFolderDto.getId()));
        }

        return null;
    }

    public String validateStartScheduleGroupCrawlRun(ScheduleGroupDto scheduleGroup, List<RootFolderDto> rootFolderDtos) {
        licenseService.validateCrawlerExpiry();

        if (rootFolderDtos == null || rootFolderDtos.size() == 0) {
            return messageHandler.getMessage("run.group.root-folder-inactive", Lists.newArrayList(scheduleGroup.getId()));
        }
        if (scheduleGroup.getLastRunId() != null) {
            CrawlRun crawlRun = getCrawlRun(scheduleGroup.getLastRunId());
            if (RunStatus.RUNNING.equals(crawlRun.getRunStatus())) {
                return messageHandler.getMessage("run.group.already-running", Lists.newArrayList(scheduleGroup.getId()));

            }

        }

        long countRootFoldersThatCanRun = rootFolderDtos.stream().filter(rf -> validateStartScheduleGroupRootFolderCrawlRun(rf) == null).count();
        if (countRootFoldersThatCanRun == 0) {
            return messageHandler.getMessage("run.group.no-root-folder");
        }

        return null;
    }

    @Transactional
    private CrawlRun createScheduleGroupCrawlRun(ScheduleGroupDto scheduleGroup, boolean manual, List<String> opList) {
        String runConfig = createRunConfig(scheduleGroup);
        CrawlRun newRun = createNewRun(runConfig);
        if (opList == null || opList.size() == 0) {
            throw new RuntimeException(messageHandler.getMessage("run.create.empty-op-list"));
        }
        if (opList.contains(FileCrawlerUtils.EXTRACT) && opList.size() == 1) {
            //This is extract run only. Force extract ven on rootfolder not configured for extract
            newRun.setExtractFiles(true);
        } else {
            if (opList.contains(FileCrawlerUtils.SCAN)) {
                newRun.setScanFiles(true);
            } else {
                throw new RuntimeException(messageHandler.getMessage("run.group.no-map"));
            }

            if (opList.contains(FileCrawlerUtils.INGEST)) {
                newRun.setIngestFiles(true);
            }

            if (opList.contains(FileCrawlerUtils.ANALYZE)) {
                newRun.setAnalyzeFiles(true);
            }

            if (opList.contains(FileCrawlerUtils.EXTRACT)) {
                newRun.setExtractFiles(true);
            }

        }
        newRun.setScheduleGroupId(scheduleGroup.getId());
        newRun.setManual(manual);
        newRun.setRunType(RunType.SCHEDULE_GROUP);
        newRun.setHasChildRuns(true);
        opStateService.setScheduledGroupId(scheduleGroup.getId());
        CrawlRun save = crawlRunRepository.save(newRun);
        scheduleGroupService.attachRunToScheduleGroup(save.getId(), scheduleGroup.getId());
        return save;
    }

    private void populateCrawlRunWithDataFromPreviousRun(Long lastRunId, CrawlRun newRun) {
        CrawlRun crawlRun = getCrawlRun(lastRunId);
        if (crawlRun != null) {
            //Populate the crawl run with data from the last run
            newRun.setProcessedExcelFiles(crawlRun.getProcessedExcelFiles());
            newRun.setProcessedOtherFiles(crawlRun.getProcessedOtherFiles());
            newRun.setProcessedPdfFiles(crawlRun.getProcessedPdfFiles());
            newRun.setProcessedWordFiles(crawlRun.getProcessedWordFiles());
            newRun.setTotalProcessedFiles(crawlRun.getTotalProcessedFiles());
        }
    }

    @Transactional(readOnly = true)
    public CrawlRun getCrawlRun(Long crawlRunId) {
        return crawlRunRepository.findById(crawlRunId).orElse(null);
    }

    @Transactional(readOnly = true)
    public List<CrawlRun> getCrawlRunsByStatus(RunStatus runStatus) {
        return crawlRunRepository.findAllByStatus(runStatus);
    }

    @Transactional(readOnly = true)
    public List<CrawlRun> getCrawlRunsByStatus(List<RunStatus> runStatus) {
        return crawlRunRepository.findByStates(runStatus);
    }

    @Transactional(readOnly = true)
    public long getLastSuccessfulCrawlRun(Long rootFolderId) {
        CrawlRun lastRun = crawlRunRepository.findTop1ByRootFolderIdAndRunStatusOrderByStartTimeDesc(rootFolderId, RunStatus.FINISHED_SUCCESSFULLY);
        return Optional.ofNullable(lastRun != null ? lastRun.getStartTimeAsLong() : null).orElse(TIME_ZERO);
    }

    @Transactional(readOnly = true)
    public List<CrawlRun> getChildCrawlRuns(Long parentCrawlRunId) {
        return crawlRunRepository.findCrawlRunsByParentRunId(parentCrawlRunId);
    }

    @Transactional(readOnly = true)
    public List<CrawlRun> getExtractCrawlRunByState(List<RunStatus> runStatus) {
        return crawlRunRepository.findExtractByStates(runStatus);
    }

    @Transactional(readOnly = true)
    public Page<CrawlRunDetailsDto> findAllExtract(PageRequest pageRequest) {
        Page<CrawlRun> runs = crawlRunRepository.findAllExtract(pageRequest);
        return convertCrawlRuns(runs, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<CrawlRunDetailsDto> findAllExtract(PageRequest pageRequest, List<RunStatus> runStatus) {
        Page<CrawlRun> runs = crawlRunRepository.findAllExtractByStates(runStatus, pageRequest);
        return convertCrawlRuns(runs, pageRequest);
    }

    /*@Transactional(readOnly = true)
    public Page<CrawlRun> getCrawlRunHistory(PageRequest pageRequest) {
        logger.debug("Get Crawl Run history");
        return crawlRunRepository.findAll(pageRequest);
    }*/

    /*
     * Get crawl run history in the last X hours
     * @param hours         number of hours to include (e.g. 24 for the last day)
     * @param pageRequest   page request
     * @return              page of crawl runs
     *
    @Transactional(readOnly = true)
    public Page<CrawlRun> getCrawlRunHistory(int hours, PageRequest pageRequest) {
        return crawlRunRepository.findAllLastHours(hours, pageRequest);
    }*/

    /*
     * Get crawl run history by root folder ID in the last X hours
     * @param rootFolderId  root folder ID
     * @param hours         number of hours to include (e.g. 24 for the last day)
     * @param pageRequest   page request
     * @return              page of crawl runs

    public Page<CrawlRun> getCrawlRunHistoryByRootFolderId(Long rootFolderId, int hours, PageRequest pageRequest) {
        return crawlRunRepository.findByRootFolderIdLastHours(rootFolderId, hours, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<CrawlRun> getCrawlRunHistoryByRootFolderId(Long rootFolderId, PageRequest pageRequest) {
        logger.debug("Get Crawl Run history by root folder {}", rootFolderId);
        return crawlRunRepository.findByRootFolderId(rootFolderId, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<CrawlRun> getCrawlRunHistoryByScheduleGroupId(Long scheduleGroupId, PageRequest pageRequest) {
        return crawlRunRepository.findByScheduleGroupId(scheduleGroupId, pageRequest);
    }*/

    /*
     * Get crawl run history by schedule group ID in the last X hours
     * @param scheduleGroupId   schedule group ID
     * @param hours             number of hours to include (e.g. 24 for the last day)
     * @param pageRequest       page request
     * @return                  page of crawl runs

    @Transactional(readOnly = true)
    public Page<CrawlRun> getCrawlRunHistoryByScheduleGroupId(Long scheduleGroupId, int hours, PageRequest pageRequest) {
        return crawlRunRepository.findByScheduleGroupIdLastHours(scheduleGroupId, hours, pageRequest);
    }*/

    private Map<Long, CrawlRun> getRootFoldersCrawlRuns(List<RootFolder> content) {
        Map<Long, CrawlRun> result = new HashMap<>();
        Map<Long, RootFolder> lastRunRootFolderMap = new HashMap<>();
        content.stream().filter(f -> f.getLastRunId() != null).forEach(f -> lastRunRootFolderMap.put(f.getLastRunId(), f));
        if (lastRunRootFolderMap.size() == 0) {
            return result;
        }
        Set<CrawlRun> crawlRunSet = crawlRunRepository.findByIds(lastRunRootFolderMap.keySet());
        for (CrawlRun crawlRun : crawlRunSet) {
            RootFolder rootFolder = lastRunRootFolderMap.get(crawlRun.getId());
            result.put(rootFolder.getId(), crawlRun);
        }
        return result;
    }

    private Map<Long, CrawlRun> getScheduleGroupsCrawlRuns(List<ScheduleGroup> content) {
        Map<Long, CrawlRun> result = new HashMap<>();
        Map<Long, ScheduleGroup> lastMap = new HashMap<>();
        content.stream().filter(f -> f.getLastRunId() != null).forEach(f -> lastMap.put(f.getLastRunId(), f));
        if (lastMap.size() == 0) {
            return result;
        }
        Set<CrawlRun> crawlRunSet = crawlRunRepository.findByIds(lastMap.keySet());
        for (CrawlRun crawlRun : crawlRunSet) {
            ScheduleGroup scheduleGroup = lastMap.get(crawlRun.getId());
            result.put(scheduleGroup.getId(), crawlRun);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public CrawlRunDetailsDto getScheduleGroupCrawlRun(Long scheduleGroupId) {
        ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(scheduleGroupId);
        if (scheduleGroup != null && scheduleGroup.getLastRunId() != null) {
            CrawlRun crawlRun = crawlRunRepository.getOne(scheduleGroup.getLastRunId());
            return convertCrawlRun(crawlRun);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public Map<Long, Long> getRootFoldersForRun(Collection<Long> runIds) {
        Set<CrawlRun> crawlRunSet = crawlRunRepository.findByIds(runIds);
        return crawlRunSet.stream().collect(Collectors.toMap(CrawlRun::getId, CrawlRun::getRootFolderId));
    }

    @Transactional
    public Page<RootFolderSummaryInfo> convertRootFoldersToSummaryInfo(PageRequest pageRequest, Page<RootFolder> rootFolders, boolean getRules) {
        Page<RootFolderSummaryInfo> result = DocStoreService
                .convertRootFoldersToRootFolderSummaryInfo(rootFolders, pageRequest, getRules);
        //Now enrich with extra data
        Map<Long, CrawlRun> crawlRunMap = getRootFoldersCrawlRuns(rootFolders.getContent());
//        logger.debug("Enriching summary view with {} crawl runs",crawlRunMap.size());
        for (RootFolderSummaryInfo rootFolderSummaryInfo : result.getContent()) {
            CrawlRun crawlRun = crawlRunMap.get(rootFolderSummaryInfo.getRootFolderDto().getId());
            enrichRootFolderToSummaryInfo(crawlRun, rootFolderSummaryInfo);

        }
        return result;
    }

    @Transactional
    public Page<ScheduleGroupSummaryInfoDto> convertScheduleGroupsToSummaryInfo(PageRequest pageRequest, Page<ScheduleGroup> scheduleGroups) {
        Page<ScheduleGroupSummaryInfoDto> result = ScheduleGroupService.convertScheduleGroupsSummaryInfo(scheduleGroups, pageRequest);
        Map<Long, CrawlRun> crawlRunMap = getScheduleGroupsCrawlRuns(scheduleGroups.getContent());

        for (ScheduleGroupSummaryInfoDto scheduleGroupSummaryInfoDto : result.getContent()) {
            CrawlRun crawlRun = crawlRunMap.get(scheduleGroupSummaryInfoDto.getScheduleGroupDto().getId());
            populateScheduleGroupSummaryInfoDto(scheduleGroupSummaryInfoDto, crawlRun);
        }
        return result;
    }

    public ScheduleGroupSummaryInfoDto convertScheduleGroupsToSummaryInfo(ScheduleGroup scheduleGroup) {
        ScheduleGroupSummaryInfoDto result = ScheduleGroupService.convertScheduleGroupSummaryInfo(scheduleGroup);
        CrawlRun crawlRun = scheduleGroup.getLastRunId() == null ? null : getCrawlRun(scheduleGroup.getLastRunId());
        populateScheduleGroupSummaryInfoDto(result, crawlRun);
        return result;
    }

    private void populateScheduleGroupSummaryInfoDto(ScheduleGroupSummaryInfoDto result, CrawlRun crawlRun) {
        if (crawlRun != null) {
            result.setRunStatus(crawlRun.getRunStatus());
            result.setLastCrawlRunDetails(convertCrawlRun(crawlRun));
        } else {
            result.setRunStatus(RunStatus.NA);
        }
        result.setNextPlannedWakeup(scheduleAppService.getNextScheduleTime(result.getScheduleGroupDto().getId()));
    }

    public RootFolderSummaryInfo convertRootFolderToSummaryInfo(RootFolder rootFolder) {
        RootFolderSummaryInfo rootFolderSummaryInfo = DocStoreService.convertRootFolderToSummaryInfo(rootFolder);
        List<RootFolder> content = Lists.newArrayList();
        content.add(rootFolder);
        Map<Long, CrawlRun> crawlRunMap = getRootFoldersCrawlRuns(content);
        CrawlRun crawlRun = crawlRunMap.get(rootFolderSummaryInfo.getRootFolderDto().getId());
        enrichRootFolderToSummaryInfo(crawlRun, rootFolderSummaryInfo);
        return rootFolderSummaryInfo;
    }

    private void enrichRootFolderToSummaryInfo(CrawlRun crawlRun, RootFolderSummaryInfo rootFolderSummaryInfo) {
        RootFolderDto rootFolderDto = rootFolderSummaryInfo.getRootFolderDto();
        if (crawlRun != null) {
            rootFolderSummaryInfo.setCrawlRunDetailsDto(convertCrawlRun(crawlRun));
            scheduleGroupService.fillScheduleGroupDetails(rootFolderSummaryInfo, crawlRun);
            rootFolderSummaryInfo.setRunStatus(
                    simplifyRunStatus(crawlRun.getRunStatus(), crawlRun.getPauseReason(), crawlRun.getGracefulStop()));
        }
        DirectoryExistStatus status = DirectoryExistStatus.UNKNOWN;
        if (!rootFolderDto.isDeleted()) {
            status = fileContentService.isDirectoryExists(rootFolderDto);
        }

        rootFolderSummaryInfo.setAccessibleUnknown(DirectoryExistStatus.UNKNOWN.equals(status));
        rootFolderSummaryInfo.setAccessible(!DirectoryExistStatus.NO.equals(status));   // Indicate 'accessible' unless known to be not accessible
        long customerDataCenterId = rootFolderSummaryInfo.getRootFolderDto().getCustomerDataCenterDto().getId();
        List<Long> activeMediaProcessors = sysComponentService.getActiveMediaProcessors(customerDataCenterId, true);
        rootFolderSummaryInfo.setCustomerDataCenterReady(!activeMediaProcessors.isEmpty());

        List<RootFolderSchedule> rootFolderSchedules = scheduleGroupService.findRootFolderSchedules(rootFolderSummaryInfo.getRootFolderDto().getId());
        Set<ScheduleGroupDto> scheduleGroupDtos = ScheduleGroupService.convertRootFolderSchedulesMinimal(rootFolderSchedules);
        rootFolderSummaryInfo.setScheduleGroupDtos(scheduleGroupDtos);
    }

    @Transactional
    @AutoAuthenticate(replace = false)
    public void handleFinishedRun(Long crawlRunId, PauseReason pauseReason) {
        CrawlRun crawlRun = getCrawlRun(crawlRunId);
        if (crawlRun.getHasChildRuns() != null && crawlRun.getHasChildRuns()) {
            handleFinishedParentRun(crawlRunId, pauseReason);
        } else {
            logger.debug("handleFinishedRun({}):run={}", crawlRunId, crawlRun);

            TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
            tmpl.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
            tmpl.setReadOnly(true);

            List<SimpleJob> runJobs = tmpl.execute(status -> jobManagerService.getRunJobsObjects(crawlRunId));

            if (runJobs.size() > 0) {
                boolean noActiveJobs = runJobs.stream().noneMatch(j -> j.getState().isStarted());
                boolean atLeastOneJobIsPaused = runJobs.stream().anyMatch(j -> JobState.PAUSED.equals(j.getState()));
                boolean atLeastOneJobIsStopped =
                        runJobs.stream().
                                anyMatch(j -> JobState.DONE == j.getState() &&
                                        JobCompletionStatus.STOPPED == j.getCompletionStatus());
                if (atLeastOneJobIsPaused) {
                    logger.debug("run {} still has paused jobs, so set to paused", crawlRunId);
                    handleUpdateRunStateAndClean(crawlRunId, RunStatus.PAUSED, pauseReason);
                } else if (atLeastOneJobIsStopped) {
                    logger.debug("run {} still has stopped jobs, so set to stopped", crawlRunId);
                    handleUpdateRunStateAndClean(crawlRunId, RunStatus.STOPPED, null);
                } else if (noActiveJobs) {
                    boolean allJobsFinished = runJobs.stream()
                            .allMatch(j -> j.getCompletionStatus() != null && j.getCompletionStatus().isFinishedSuccessfully());
                    if (allJobsFinished) {
                        RunStatus newStatus = (crawlRun.getGracefulStop() != null && crawlRun.getGracefulStop() ?
                                RunStatus.STOPPED : RunStatus.FINISHED_SUCCESSFULLY);
                        logger.debug("run {} set to finished successfully", crawlRunId);
                        handleUpdateRunStateAndClean(crawlRunId, newStatus, null);
                        if (crawlRun.getRootFolderId() != null && crawlRun.isAnalyzeFiles()) {
                            docStoreService.updateFirstScanDone(crawlRun.getRootFolderId());
                        }
                    } else {
                        logger.debug("run {} has failed jobs, so set to failed", crawlRunId);
                        handleUpdateRunStateAndClean(crawlRunId, RunStatus.FAILED, null);
                    }
                    opStateService.informRunFinished();
                } else {
                    boolean atLeastOneJobIsRunning = runJobs.stream()
                            .anyMatch(j -> j.getState().isStarted());
                    logger.debug("run {} still has running jobs {}, so set to running if needed", crawlRunId, atLeastOneJobIsRunning);
                    if (atLeastOneJobIsRunning && !RunStatus.RUNNING.equals(crawlRun.getRunStatus())) {
                        handleUpdateRunStateAndClean(crawlRunId, RunStatus.RUNNING, null);
                    }

                }
            }
            handleFinishedParentRun(crawlRun.getParentRunId(), pauseReason);
        }
    }

    private void handleFinishedParentRun(Long parentRunId, PauseReason pauseReason) {
        if (parentRunId != null) {
            CrawlRun parentCrawlRun = getCrawlRun(parentRunId);
            if (parentCrawlRun.getHasChildRuns()) {

                TransactionTemplate tmpl = new TransactionTemplate(transactionManager);
                tmpl.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
                tmpl.setReadOnly(true);
                List<CrawlRun> childrenCrawlRuns = tmpl.execute(status -> getChildCrawlRuns(parentRunId));

                boolean noActiveRuns = childrenCrawlRuns.stream().noneMatch(r -> r.getRunStatus().isActive());
                Map<RunStatus, Long> countedByStatus = childrenCrawlRuns.stream().map(CrawlRun::getRunStatus)
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
                if (logger.isDebugEnabled()) {
                    logger.debug("handleFinishedParentRun({}): childRuns={}, countedJobsByStatus={}",
                            parentCrawlRun,
                            childrenCrawlRuns.stream()
                                    .map(r -> r.getId().toString())
                                    .collect(Collectors.joining(",", "{", "}")),
                            countedByStatus.entrySet().stream()
                                    .map(e -> e.getKey().toString() + ":" + e.getValue())
                                    .collect(Collectors.joining(",", "{", "}")));
                }
                boolean atLeastOneRunIsPaused = countedByStatus.getOrDefault(RunStatus.PAUSED, 0L) > 0;
                boolean atLeastOneRunIsStopped = countedByStatus.getOrDefault(RunStatus.STOPPED, 0L) > 0;
                boolean atLeastOneRunFailed = countedByStatus.getOrDefault(RunStatus.FAILED, 0L) > 0;

                if (noActiveRuns) {
                    if (atLeastOneRunIsPaused) {
                        logger.debug("run {} still has paused jobs, so set to paused", parentRunId);
                        handleUpdateRunStateAndClean(parentRunId, RunStatus.PAUSED, pauseReason);
                    } else if (atLeastOneRunIsStopped) {
                        logger.debug("run {} still has stopped jobs, so set to stopped", parentRunId);
                        handleUpdateRunStateAndClean(parentRunId, RunStatus.STOPPED, null);
                    } else if (atLeastOneRunFailed) {
                        logger.debug("run {} has failed jobs, so set to failed", parentRunId);
                        handleUpdateRunStateAndClean(parentRunId, RunStatus.FAILED, null);
                    } else {
                        logger.debug("run {} set to finished successfully", parentRunId);
                        handleUpdateRunStateAndClean(parentRunId, RunStatus.FINISHED_SUCCESSFULLY, null);
                    }
                } else {
                    boolean atLeastOneJobIsRunning =
                            childrenCrawlRuns.stream().anyMatch(r -> RunStatus.RUNNING.equals(r.getRunStatus()));
                    logger.debug("run {} still has running jobs {}, so set to running if needed", parentRunId, atLeastOneJobIsRunning);
                    if (atLeastOneJobIsRunning && !RunStatus.RUNNING.equals(parentCrawlRun.getRunStatus())) {
                        handleUpdateRunStateAndClean(parentRunId, RunStatus.RUNNING, null);
                    }
                }

            }
        }
    }

    private void handleUpdateRunStateAndClean(Long crawlRunId, RunStatus runStatus, PauseReason pauseReason) {
        fileCrawlerExecDetailsServiceTxUtil.updateCrawlRunState(crawlRunId, runStatus, pauseReason, opStateService);
        if (runStatus.isFinished()) {
            jobManagerService.removeTaskMarkingsForRun(crawlRunId);
        }
        if (!runStatus.isActive()) {
            eventBus.broadcast(Topics.RUN, Event.builder()
                    .withEventType(EventType.SCAN_STOP)
                    .withEntry(EventKeys.RUN_ID, crawlRunId)
                    .build());
        }
    }

    public CrawlRunDetailsDto getRunDetails(long runId) {
        return fileCrawlerExecDetailsServiceTxUtil.getRunDetails(runId);
    }

    public Long getRunStopTime(long runId) {
        return fileCrawlerExecDetailsServiceTxUtil.getRunDetails(runId).getStopTimeAsLong();
    }

    public void incNewFiles(long runId) {
        fileCrawlerExecDetailsServiceTxUtil.safeUpdateCounter(runId, (CrawlRunDetailsDto runDto) ->
                runDto.setNewFiles(runDto.getNewFiles() + 1));
    }

    public void incUpdatedContentFiles(long runId) {
        fileCrawlerExecDetailsServiceTxUtil.safeUpdateCounter(runId, (CrawlRunDetailsDto runDto) ->
                runDto.setUpdatedContentFiles(runDto.getUpdatedContentFiles() + 1));
    }

    public void incUpdatedMetadataFiles(long runId) {
        fileCrawlerExecDetailsServiceTxUtil.safeUpdateCounter(runId, (CrawlRunDetailsDto runDto) ->
                runDto.setUpdatedMetadataFiles(runDto.getUpdatedMetadataFiles() + 1));
    }

    public void incDeletedFiles(long runId, long increment) {
        fileCrawlerExecDetailsServiceTxUtil.safeUpdateCounter(runId, (CrawlRunDetailsDto runDto) ->
                runDto.setDeletedFiles(runDto.getDeletedFiles() + increment));
    }

    public CrawlRunDetailsDto getExtractLatestRun() {
        CrawlRun run = crawlRunRepository.getLatestExtractRun();
        return convertCrawlRun(run);
    }

    public List<String> generateOpList(Long crawlRunId) {
        CrawlRun crawlRun = getCrawlRun(crawlRunId);
        List<String> runOpList = new ArrayList<>();
        if (crawlRun.isScanFiles()) {
            runOpList.add(FileCrawlerUtils.SCAN);
        }
        if (crawlRun.isIngestFiles()) {
            runOpList.add(FileCrawlerUtils.INGEST);
        }
        if (crawlRun.isAnalyzeFiles()) {
            runOpList.add(FileCrawlerUtils.ANALYZE);
        }
        if (crawlRun.isExtractFiles()) {
            runOpList.add(FileCrawlerUtils.EXTRACT);
        }
        return runOpList;
    }

    public static RunSummaryInfoDto convertRunSummaryInfo(CrawlRun crawlRun, List<SimpleJob> simpleJobs, ConcurrentMap<Long, PhaseRunningRateDetails> phaseRunningRateDetailsMap) {
        RunSummaryInfoDto result = new RunSummaryInfoDto();
        List<PhaseDetailsDto> phaseDetailsDto = JobManagerService.convertRunJobsToPhaseDetailsDto(simpleJobs, phaseRunningRateDetailsMap);
        result.setJobs(phaseDetailsDto);
        result.setCrawlRunDetailsDto(convertCrawlRun(crawlRun));
        return result;
    }

    public static String simplifyRunStatus(RunStatus runStatus, PauseReason pauseReason, Boolean isStopping) {
        switch (runStatus) {
            case FINISHED_SUCCESSFULLY:
                return RootFolderSummaryInfo.OK_STATUS;
            case PAUSED:
                if (pauseReason != null && !pauseReason.equals(PauseReason.USER_INITIATED)) {
                    return RootFolderSummaryInfo.SUSPENDED_STATUS;
                }
            default:
                if (isStopping != null && isStopping) {
                    return RootFolderSummaryInfo.STOPPING_STATUS;
                }
                return runStatus.name();
        }
    }

    public static void mergeCrawlRun(final CrawlRun run, final CrawlRunDetailsDto dto) {
        run.setUpdatedContentFiles(dto.getUpdatedContentFiles());
        run.setUpdatedMetadataFiles(dto.getUpdatedMetadataFiles());
        run.setNewFiles(dto.getNewFiles());
        run.setDeletedFiles(dto.getDeletedFiles());
        run.setProcessedExcelFiles(dto.getProcessedExcelFiles());
        run.setProcessedWordFiles(dto.getProcessedWordFiles());
        run.setProcessedPdfFiles(dto.getProcessedPdfFiles());
        run.setProcessedOtherFiles(dto.getProcessedOtherFiles());
        run.setTotalProcessedFiles(dto.getTotalProcessedFiles());
    }

    public static Page<CrawlRunDetailsDto> convertCrawlRuns(Page<CrawlRun> crawlRuns, PageRequest pageRequest) {

        List<CrawlRunDetailsDto> content = new ArrayList<>();
        for (CrawlRun crawlRun : crawlRuns) {
            content.add(convertCrawlRun(crawlRun));
        }

        return new PageImpl<>(content, pageRequest, crawlRuns.getTotalElements());
    }

    public static CrawlRunDetailsDto convertCrawlRun(CrawlRun crawlRun) {
        if (crawlRun == null) return null;

        CrawlRunDetailsDto crawlRunDetailsDto = new CrawlRunDetailsDto();
        crawlRunDetailsDto.setId((crawlRun.getId()));
        crawlRunDetailsDto.setRunOutcomeState(crawlRun.getRunStatus());
        crawlRunDetailsDto.setScanStartTime(crawlRun.getStartTimeAsLong());
        crawlRunDetailsDto.setScanEndTime(crawlRun.getStopTimeAsLong());
        crawlRunDetailsDto.setScanFiles(crawlRun.isScanFiles());
        crawlRunDetailsDto.setIngestFiles(crawlRun.isIngestFiles());
        crawlRunDetailsDto.setAnalyzeFiles(crawlRun.isAnalyzeFiles());
        crawlRunDetailsDto.setManual(crawlRun.isManual());
        crawlRunDetailsDto.setStateString(crawlRun.getStateString());
        crawlRunDetailsDto.setRunType(crawlRun.getRunType());
        crawlRunDetailsDto.setHasChildRuns(crawlRun.getHasChildRuns());
        crawlRunDetailsDto.setParentRunId(crawlRun.getParentRunId());
        crawlRunDetailsDto.setPauseReason(crawlRun.getPauseReason());
        crawlRunDetailsDto.setStopTime(crawlRun.getStopTime());
        crawlRunDetailsDto.setGracefulStop(crawlRun.getGracefulStop());
        crawlRunDetailsDto.setProcessedExcelFiles(crawlRun.getProcessedExcelFiles());
        crawlRunDetailsDto.setProcessedWordFiles(crawlRun.getProcessedWordFiles());
        crawlRunDetailsDto.setProcessedPdfFiles(crawlRun.getProcessedPdfFiles());
        crawlRunDetailsDto.setProcessedOtherFiles(crawlRun.getProcessedOtherFiles());
        crawlRunDetailsDto.setTotalProcessedFiles(crawlRun.getTotalProcessedFiles());

        crawlRunDetailsDto.setNewFiles(crawlRun.getNewFiles());
        crawlRunDetailsDto.setUpdatedMetadataFiles(crawlRun.getUpdatedMetadataFiles());
        crawlRunDetailsDto.setUpdatedContentFiles(crawlRun.getUpdatedContentFiles());
        crawlRunDetailsDto.setDeletedFiles(crawlRun.getDeletedFiles());

        crawlRunDetailsDto.setScanErrors(crawlRun.getScanErrors());
        crawlRunDetailsDto.setScanProcessingErrors(crawlRun.getIngestionErrors());
        crawlRunDetailsDto.setAccessible(crawlRun.isRootFolderAccessible());
        if (crawlRun.getScheduleGroupId() != null) {
            crawlRunDetailsDto.setScheduleGroupId(crawlRun.getScheduleGroupId());
        }
        if (crawlRun.getRunConfig() != null && crawlRun.getRootFolderId() != null) {

            try {
                RootFolderDto rootFolderDto = mapper.readValue(crawlRun.getRunConfig(), RootFolderDto.class);
                crawlRunDetailsDto.setRootFolderDto(rootFolderDto);
            } catch (InvalidFormatException e) {
                logger.error("Failed to extract RootFolderDto from runConfig - dto probably changed: {}", e.getMessage());
                RootFolderDto rootFolderDto = new RootFolderDto();
                rootFolderDto.setId(crawlRun.getRootFolderId());
                rootFolderDto.setCustomerDataCenterDto(new CustomerDataCenterDto());
                crawlRunDetailsDto.setRootFolderDto(rootFolderDto);
            } catch (IOException | RuntimeException e) {
                logger.error("Failed to extract RootFolderDto from runConfig", e);
                RootFolderDto rootFolderDto = new RootFolderDto();
                rootFolderDto.setId(crawlRun.getRootFolderId());
                rootFolderDto.setCustomerDataCenterDto(new CustomerDataCenterDto());
                crawlRunDetailsDto.setRootFolderDto(rootFolderDto);
            }
        } else {
            RootFolderDto fakeRootFolderDto = new RootFolderDto();
            if (RunType.LABELING.equals(crawlRun.getRunType())) {
                fakeRootFolderDto.setNickName("Labeling extraction");
                crawlRunDetailsDto.setStateString(crawlRun.getStateString());
            } else if (!RunType.SCHEDULE_GROUP.equals(crawlRun.getRunType())) {
                fakeRootFolderDto.setNickName("Business list extraction");
                crawlRunDetailsDto.setStateString(crawlRun.getStateString());
            }
            fakeRootFolderDto.setCustomerDataCenterDto(new CustomerDataCenterDto());
            crawlRunDetailsDto.setRootFolderDto(fakeRootFolderDto);
        }

        if (crawlRun.getRunConfig() != null && RunType.SCHEDULE_GROUP.equals(crawlRun.getRunType())) {

            try {
                ScheduleGroupDto sc = mapper.readValue(crawlRun.getRunConfig(), ScheduleGroupDto.class);
                crawlRunDetailsDto.setScheduleGroupName(sc.getName());
                crawlRunDetailsDto.getRootFolderDto().setNickName(sc.getName());
                crawlRunDetailsDto.setStateString(sc.getSchedulingDescription());
            } catch (InvalidFormatException e) {
                logger.error("Failed to extract ScheduleGroup from runConfig - dto probably changed: {}", e.getMessage());
            } catch (IOException e) {
                logger.error("Failed to extract ScheduleGroup from runConfig", e);
            } catch (RuntimeException e) {
                logger.error("Failed to extract RootFolderDto from runConfig", e);
                RootFolderDto rootFolderDto = new RootFolderDto();
                rootFolderDto.setId(crawlRun.getRootFolderId());
                crawlRunDetailsDto.setRootFolderDto(rootFolderDto);
            }
        }

        return crawlRunDetailsDto;
    }

    public Long runActiveForRootFolder(Long rootFolderId) {
        List<RunStatus> states = Lists.newArrayList(RunStatus.RUNNING);
        Page<CrawlRun> filteredRuns = crawlRunRepository.findByStatesForRootFolder(rootFolderId, states, PageRequest.of(0, 1));
        if (filteredRuns != null && filteredRuns.getTotalElements() > 0) {
            return filteredRuns.getContent().get(0).getId();
        }
        return null;
    }
}
