package com.cla.filecluster.service.files;

import com.cla.common.domain.dto.filetree.MailboxDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.filetree.Mailbox;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.repository.jpa.filetree.MailboxRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 2/10/2019
 */
@Service
public class MailboxService {

    private static final Logger logger = LoggerFactory.getLogger(MailboxService.class);

    @Autowired
    private MailboxRepository mailboxRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional(readOnly = true)
    public MailboxDto getById(long id) {
        return fromEntity(getOneById(id));
    }

    private Mailbox getOneById(long id) {
        Optional<Mailbox> one = mailboxRepository.findById(id);
        if (!one.isPresent()) {
            throw new BadParameterException(messageHandler.getMessage("mailbox.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return one.get();
    }

    @Transactional(readOnly = true)
    public List<MailboxDto> getByRootFolderId(long rootFolderId) {
        List<Mailbox> mailboxs = mailboxRepository.getByRootFolderId(rootFolderId);
        List<MailboxDto> result = new ArrayList<>();

        mailboxs.forEach(mb -> {
            result.add(fromEntity(mb));
        });

        return result;
    }

    @Transactional
    public MailboxDto createMailboxDto(MailboxDto mailboxDto) {
        validateMailbox(mailboxDto);
        Mailbox mailbox = toEntity(mailboxDto);
        Mailbox result = mailboxRepository.save(mailbox);
        return fromEntity(result);
    }

    @Transactional
    public MailboxDto updateMailboxDto(MailboxDto mailboxDto) {
        Mailbox mailbox = getOneById(mailboxDto.getId());
        validateMailbox(mailboxDto);
        mailbox.setUpn(mailboxDto.getUpn());
        mailbox.setRootFolderId(mailboxDto.getRootFolderId());
        mailbox.setSyncState(mailboxDto.getSyncState());
        Mailbox result = mailboxRepository.save(mailbox);
        return fromEntity(result);
    }

    @Transactional
    public void updateMailboxDtoSyncState(long rootFolderId, String mailboxUpn, String syncState) {
        Mailbox mailbox = mailboxRepository.getByUpnAndRootFolderId(mailboxUpn, rootFolderId);
        if (mailbox == null) {
            logger.error("cannot find mailbox with root folder {} and upn {}", rootFolderId, mailboxUpn);
            return;
        }
        mailbox.setSyncState(syncState);
        mailboxRepository.save(mailbox);
    }

    @Transactional
    public void deleteMailbox(long id) {
        getOneById(id);
        mailboxRepository.deleteById(id);
    }

    private void validateMailbox(MailboxDto mailboxDto) {
        if (Strings.isNullOrEmpty(mailboxDto.getUpn())) {
            throw new BadParameterException(messageHandler.getMessage("mailbox.upn.empty"), BadRequestType.MISSING_FIELD);
        }

        if (mailboxDto.getRootFolderId() == null) {
            throw new BadParameterException(messageHandler.getMessage("mailbox.root-folder.empty"), BadRequestType.MISSING_FIELD);
        }

        if (docStoreService.getRootFolder(mailboxDto.getRootFolderId()) == null) {
            throw new BadParameterException(messageHandler.getMessage("mailbox.root-folder.missing"), BadRequestType.MISSING_FIELD);
        }
        Mailbox mailboxChkUnique = mailboxRepository.getByUpnAndRootFolderId(mailboxDto.getUpn(), mailboxDto.getRootFolderId());
        if (mailboxChkUnique != null) {
            if (mailboxDto.getId() == null || !mailboxChkUnique.getId().equals(mailboxDto.getId())) {
                throw new BadParameterException(messageHandler.getMessage("mailbox.upn.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    public static MailboxDto fromEntity(Mailbox mailbox) {
        if (mailbox == null) {
            return null;
        }

        MailboxDto mailboxDto = new MailboxDto();
        mailboxDto.setId(mailbox.getId());
        mailboxDto.setUpn(mailbox.getUpn());
        mailboxDto.setRootFolderId(mailbox.getRootFolderId());
        mailboxDto.setSyncState(mailbox.getSyncState());
        return mailboxDto;
    }

    public static Mailbox toEntity(MailboxDto mailboxDto) {
        if (mailboxDto == null) {
            return null;
        }

        Mailbox mailbox = new Mailbox();
        mailbox.setId(mailboxDto.getId());
        mailbox.setUpn(mailboxDto.getUpn());
        mailbox.setRootFolderId(mailboxDto.getRootFolderId());
        mailbox.setSyncState(mailboxDto.getSyncState());
        return mailbox;
    }
}
