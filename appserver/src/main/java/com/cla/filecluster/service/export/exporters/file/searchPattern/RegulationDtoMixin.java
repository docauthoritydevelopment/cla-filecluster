package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.NAME;

/**
 * Created by: ophir
 * Created on: 2/6/2019
 */
public class RegulationDtoMixin {

    @JsonProperty(TextSearchPatternHeaderFields.NAME)
    private String name;
}
