package com.cla.filecluster.service.export.exporters.root_folders;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.root_folders.mix_in.*;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.filecluster.service.export.exporters.root_folders.RootFoldersHeaderFields.*;

@Slf4j
@Component
public class RootFolderExporter extends PageCsvExcelExporter<RootFolderSummaryInfo> {

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private DepartmentService departmentService;

    private final static String[] header = {PATH, MEDIA_TYPE, NICK_NAME, MAPPED_FILE_TYPES, INGESTED_FILE_TYPES, DATA_CENTER,
            RESCAN, REINGEST, REINGEST_TIME, STORE_LOCATION, STORE_PURPOSE, STORE_SECURITY,
            DESCRIPTION, MEDIA_CONNECTION_NAME, SCHEDULE_GROUP_NAME, EXTRACT_BUSINESS_LISTS, MAILBOX_GROUP,
            FROM_DATE_SCAN_FILTER, SCAN_TASK_DEPTH, DEPARTMENT, MATTER, EXCLUDED_FOLDERS_RULES, STATUS,
            NUM_OF_EXCLUDED_FOLDERS_RULES, NUM_OF_FOUND_FOLDERS, ACCESSIBILITY, NUM_OF_PROCESSED_FILES, PDF_PROCESSED,
            WORD_PROCESSED, EXCEL_PROCESSED, OTHER_PROCESSED, SCAN_ERRORS, PROCESSING_ERRORS,
            SCANNED_LIMIT_COUNT, MEANINGFUL_SCANNED_LIMIT_COUNT};


    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.ROOT_FOLDERS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(CrawlRunDetailsDto.class, CrawlRunDetailsDtoMixIn.class)
                .addMixIn(CustomerDataCenterDto.class, CustomerDataCenterDtoMixIn.class)
                .addMixIn(RootFolderDto.class, RootFolderDtoMixIn.class)
                .addMixIn(ScheduleGroupDto.class, ScheduleGroupDtoMixin.class)
                .addMixIn(RootFolderSummaryInfo.class, RootFolderSummaryInfoMixIn.class);
    }

    @Override
    protected Page<RootFolderSummaryInfo> getData(Map<String, String> requestParams) {
        log.info("fetching RootFolderSummaryInfo data for exporting.");
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return fileTreeAppService.listRootFoldersSummaryInfo(dataSourceRequest, true, true);
    }

	@Override
	protected Map<String, Object> addExtraFields(RootFolderSummaryInfo base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        if (base.getDepartmentDto() != null) {
            result.put(departmentService.getLegalInstallationMode(), base.getDepartmentDto().getFullName());
        }
        if (base.getFolderExcludeRules() != null) {
            List<String> activeRules = base.getFolderExcludeRules().stream()
                    .filter(r -> !r.isDisabled())
                    .map(FolderExcludeRuleDto::getMatchString)
                    .collect(Collectors.toList());
            String rulesCommaSeparated = String.join(",", activeRules);
            result.put(EXCLUDED_FOLDERS_RULES, rulesCommaSeparated);
        }
		return result;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
