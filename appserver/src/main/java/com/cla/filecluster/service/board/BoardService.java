package com.cla.filecluster.service.board;

import com.cla.common.domain.dto.board.BoardDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.board.Board;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.board.BoardRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BoardService {

    private static final Logger logger = LoggerFactory.getLogger(BoardService.class);

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional(readOnly = true)
    public BoardDto getById(Long id) {
        Board board = boardRepository.getOne(id);
        return convertToDto(board);
    }

    @Transactional(readOnly = true)
    public Page<BoardDto> getAll(PageRequest pageRequest) {
        Page<Board> boards = boardRepository.findAll(pageRequest);
        List<BoardDto> boardDtos = convertToDtos(boards.getContent());
        return new PageImpl<>(boardDtos, pageRequest, boards.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<BoardDto> getForOwner(PageRequest pageRequest, long ownerId) {
        Page<Board> boards = boardRepository.findByOwnerPageRequest(ownerId, pageRequest);
        List<BoardDto> boardDtos = convertToDtos(boards.getContent());
        return new PageImpl<>(boardDtos, pageRequest, boards.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<BoardDto> getAllForOwner(PageRequest pageRequest, long ownerId) {
        Page<Board> boards = boardRepository.findAllOwnerPageRequest(ownerId, pageRequest);
        List<BoardDto> boardDtos = convertToDtos(boards.getContent());
        return new PageImpl<>(boardDtos, pageRequest, boards.getTotalElements());
    }

    @Transactional
    public BoardDto createBoard(BoardDto boardDto) {
        Board board = new Board();
        board.setName(boardDto.getName());
        board.setDescription(boardDto.getDescription());
        board.setOwnerId(boardDto.getOwnerId());
        board.setPublic(boardDto.isPublic());
        board.setBoardType(boardDto.getBoardType());
        return convertToDto(boardRepository.save(board));
    }

    @Transactional
    public BoardDto updateBoard(BoardDto boardDto) {
        Board board = boardRepository.findById(boardDto.getId()).orElse(null);
        if (board == null) {
            throw new BadRequestException(messageHandler.getMessage("board.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        board.setName(boardDto.getName());
        board.setDescription(boardDto.getDescription());
        board.setPublic(boardDto.isPublic());
        return convertToDto(boardRepository.save(board));
    }

    @Transactional
    public void deleteBoard(Long id) {
        boardRepository.deleteById(id);
    }

    public static List<BoardDto> convertToDtos(List<Board> boards) {
        List<BoardDto> boardDtos = new ArrayList<>();
        if (boards == null) {
            return boardDtos;
        }

        boards.forEach(b -> boardDtos.add(convertToDto(b)));
        return boardDtos;
    }

    public static BoardDto convertToDto(Board board) {
        if (board == null) {
            return null;
        }

        BoardDto boardDto = new BoardDto();
        boardDto.setId(board.getId());
        boardDto.setBoardType(board.getBoardType());
        boardDto.setDateCreated(board.getDateCreated());
        boardDto.setDateModified(board.getDateModified());
        boardDto.setName(board.getName());
        boardDto.setDescription(board.getDescription());
        boardDto.setOwnerId(board.getOwnerId());
        boardDto.setPublic(board.isPublic());
        return boardDto;
    }
}
