package com.cla.filecluster.service.export.exporters.components;

import com.cla.common.domain.dto.components.ClaComponentStatusDto;
import com.cla.filecluster.service.export.ExportType;
import com.google.common.base.Strings;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
@Component
public class BrokerHistoryExport extends ComponentHistoryExport {

    private final static String[] header = {
            TIMESTAMP, MAP_REQ, INGEST_REQ, REAL_TIME_REQ, CONTENT_REQ, MAP_RESP, INGEST_RESP, REAL_TIME_RESP, CONTENT_RESP, DISK_SPACE};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.BROKER_HISTORY);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected Map<String, Object> addExtraFields(ClaComponentStatusDto base, Map<String, String> requestParams) {
        Map<String, Object> res = super.addExtraFields(base, requestParams);

        if (!Strings.isNullOrEmpty(base.getExtraMetrics())) {
            String data = base.getExtraMetrics();

            res.put(MAP_REQ, getRequestAmountForQueue(data, "scan_requests"));
            res.put(INGEST_REQ, getRequestAmountForQueue(data, "ingest_requests"));
            res.put(REAL_TIME_REQ, getRequestAmountForQueue(data, "realtime_requests"));
            res.put(CONTENT_REQ, getRequestAmountForQueue(data, "contentcheck_requests"));

            res.put(MAP_RESP, getRequestAmountForQueue(data, "scan_responses"));
            res.put(INGEST_RESP, getRequestAmountForQueue(data, "ingest_responses"));
            res.put(REAL_TIME_RESP, getRequestAmountForQueue(data, "realtime_responses"));
            res.put(CONTENT_RESP, getRequestAmountForQueue(data, "contentcheck_responses"));
        }

        return res;
    }

    private String getRequestAmountForQueue(String data, String queueName) {
        if (data.contains(queueName)) {
            int startQueue = data.indexOf(queueName);
            if (startQueue > 0) {
                int start = data.indexOf("EnqueueCount", startQueue);
                if (start > 0 && !data.substring(startQueue, start).contains("}")) {
                    start = data.indexOf(":", start);
                    int end = data.indexOf(",", start);
                    if (end < 0) {
                        end = data.indexOf("}", start);
                    }
                    return data.substring(start+1, end);
                }
            }
        }
        return null;
    }
}


