package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.common.domain.dto.messages.ScanResultMessage;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.ScanSharePermissions;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolderSharePermission;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderSharePermissionRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.util.PermissionCheckUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.nio.file.attribute.AclEntryType;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@Service
public class SharePermissionService {

    private final static Logger logger = LoggerFactory.getLogger(SharePermissionService.class);

    @Autowired
    private RootFolderSharePermissionRepository rootFolderSharePermissionRepository;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private RootFolderRepository rootFolderRepository;

    @Autowired
    private EventBus eventBus;

    @Value("${security.share-permission.read-mask:131241}")
    private long readMaskSharePermission;

    @Value("${file-share.share-permission.enabled:true}")
    private boolean sharePermissionFeatureEnabled;

    @Value("${file-share.share-permission.generic-user:Everyone}")
    private String genericUserToDenyAccess;

    private LoadingCache<Long, List<RootFolderSharePermissionDto>> rootFolderSharePermissions = null;

    public boolean isSharePermissionFeatureEnabled() {
        return sharePermissionFeatureEnabled;
    }

    @PostConstruct
    public void init() {
        if (!sharePermissionFeatureEnabled) {
            return;
        }

        rootFolderSharePermissions = CacheBuilder.newBuilder()
                .build(
                        new CacheLoader<Long, List<RootFolderSharePermissionDto>>() {
                            public List<RootFolderSharePermissionDto> load(Long key) {
                                Callable<List<RootFolderSharePermission>> task = () -> rootFolderSharePermissionRepository.getForRootFolder(key);
                                List<RootFolderSharePermission> res = dbTemplateUtils.doInTransaction(task);
                                if (res == null) {
                                    res = Lists.newArrayList();
                                }
                                return convertRootFolderSharePermission(res);
                            }

                        });

        dbTemplateUtils.doInTransaction(() -> {
            List<RootFolderSharePermission> sharePermissions = rootFolderSharePermissionRepository.findAll();
            Map<Long, List<RootFolderSharePermissionDto>> res = new HashMap<>();
            sharePermissions.forEach(perm -> {
                Long rootFolderId = perm.getRootFolderId();
                List<RootFolderSharePermissionDto> perRf = res.getOrDefault(rootFolderId, new ArrayList<>());
                perRf.add(convertRootFolderSharePermission(perm));
                res.put(rootFolderId, perRf);
            });
            rootFolderSharePermissions.putAll(res);
        });
    }

    public Set<Long> getRelevantRootFolders() {
        if (!sharePermissionFeatureEnabled) {
            return Sets.newHashSet();
        }
        Map<Long, List<RootFolderSharePermissionDto>> all = rootFolderSharePermissions.asMap();
        Set<Long> result = new HashSet<>();

        for (Map.Entry<Long, List<RootFolderSharePermissionDto>> rfData : all.entrySet()) {
            if (rfData.getValue() != null && !rfData.getValue().isEmpty()) {
                result.add(rfData.getKey());
            }
        }
        return result;
    }

    public List<Long> getRelevantRootFolders(Long sharedPermissionId) {
        if (!sharePermissionFeatureEnabled) {
            return Lists.newArrayList();
        }
        RootFolderSharePermissionDto perm = getById(sharedPermissionId);
        List<Long> result = new ArrayList<>();
        if (perm != null) {
            String identifier = perm.toItemIdentifier();

            for(Map.Entry<Long, List<RootFolderSharePermissionDto>> entry : rootFolderSharePermissions.asMap().entrySet()) {
                if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                    for (RootFolderSharePermissionDto dto : entry.getValue()) {
                        if (dto.toItemIdentifier().equals(identifier)) { // use identifier to find identical permissions
                            result.add(entry.getKey());
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }

    public RootFolderSharePermissionDto getById(Long sharedPermissionId) {
        for (List<RootFolderSharePermissionDto> perms : rootFolderSharePermissions.asMap().values()) {
            for (RootFolderSharePermissionDto perm : perms) {
                if (perm.getId().equals(sharedPermissionId)) {
                    return perm;
                }
            }
        }
        return null;
    }

    public List<RootFolderSharePermissionDto> getRootFolderReadAllowSharePermissions(Long rootFolderId, MediaType mediaType) {
        List<RootFolderSharePermissionDto> res = getRootFolderSharePermissions(rootFolderId, mediaType);
        if (res != null) {
            return res.stream()
                    .filter(sp -> sp.getAclEntryType().equals(AclEntryType.ALLOW) &&
                            PermissionCheckUtils.hasPermission(sp.getMask(), readMaskSharePermission))
                    .collect(Collectors.toList());
        }
        return null;
    }

    public List<RootFolderSharePermissionDto> getRootFolderSharePermissions(Long rootFolderId, MediaType mediaType) {
        if (!sharePermissionFeatureEnabled) {
            return Lists.newArrayList();
        }
        if (MediaType.FILE_SHARE.equals(mediaType)) {
            return rootFolderSharePermissions.getUnchecked(rootFolderId);
        } else {
            return null;
        }
    }

    public Map<Long, List<RootFolderSharePermissionDto>> getAllSharePermissions() {
        if (!sharePermissionFeatureEnabled) {
            return Maps.newHashMap();
        }
        Map<Long, List<RootFolderSharePermissionDto>> all = rootFolderSharePermissions.asMap();
        Map<Long, List<RootFolderSharePermissionDto>> result = new HashMap<>();

        for (Map.Entry<Long, List<RootFolderSharePermissionDto>> rfData : all.entrySet()) {
            if (rfData.getValue() != null && !rfData.getValue().isEmpty()) {
                result.put(rfData.getKey(), rfData.getValue());
            }
        }
        return result;
    }

    public Map<Long, List<RootFolderSharePermissionDto>> getAllSharePermissions(AclType aclType) {
        Map<Long, List<RootFolderSharePermissionDto>> result = new HashMap<>();

        if (sharePermissionFeatureEnabled) {
            Map<Long, List<RootFolderSharePermissionDto>> data = rootFolderSharePermissions.asMap();
            for (Long rfId : data.keySet()) {
                List<RootFolderSharePermissionDto> perms = data.get(rfId);
                if (perms != null && !perms.isEmpty()) {
                    for (RootFolderSharePermissionDto perm : perms) {
                        if (matchCondition(aclType, perm)) {
                            List<RootFolderSharePermissionDto> resultPerm = result.getOrDefault(rfId, new ArrayList<>());
                            resultPerm.add(perm);
                            result.put(rfId, resultPerm);
                        }
                    }
                }
            }
        }
        return result;
    }

    private boolean matchCondition(AclType aclType, RootFolderSharePermissionDto perm) {
        if ((aclType.isAllow() && perm.getAclEntryType().equals(AclEntryType.ALLOW) ||
                (!aclType.isAllow() && perm.getAclEntryType().equals(AclEntryType.DENY)))) {
            if (aclType.isRead() && PermissionCheckUtils.hasPermission(perm.getMask(), readMaskSharePermission)) {
                return true;
            }

            if (!aclType.isRead()) { // dont have a write option mask yet
                return true;
            }
        }
        return false;
    }

    public void deleteRootFolder(Long rootFolderId) {
        rootFolderSharePermissionRepository.deleteForRootFolder(rootFolderId);
        if (sharePermissionFeatureEnabled) {
            rootFolderSharePermissions.invalidate(rootFolderId);
        }
    }

    @Transactional
    public void consumeSharePermissionsMessage(RootFolder rf, ScanResultMessage scanTaskMessage) {
        if (scanTaskMessage.getPayload() != null) {
            ScanSharePermissions p = (ScanSharePermissions)scanTaskMessage.getPayload();
            Long rootFolderId = p.getRootFolderId();
            if (p.calculateProcessErrorType().equals(ProcessingErrorType.ERR_NONE)) {
                List<String> sharePermissions = p.getSharePermissions();
                setSharePermissionsForRootFolder(rootFolderId, sharePermissions);
                rootFolderRepository.setSharePermissionFailure(rootFolderId, false);
            } else {
                scanErrorsService.addScanError(messageHandler.getMessage("root-folder.file-share.share-permission.failure"),
                        p.getErrorText(), null, scanTaskMessage.getPath(),
                        scanTaskMessage.getRunContext(), scanTaskMessage.getRootFolderId(), rf.getMediaType());

                eventBus.broadcast(Topics.MAP, Event.builder()
                        .withEventType(EventType.JOB_PROGRESS)
                        .withEntry(EventKeys.JOB_ID, scanTaskMessage.getJobId())
                        .withEntry(EventKeys.FILE_MAP_FAIL, 1).build());

                rootFolderRepository.setSharePermissionFailure(rootFolderId, true);

                // if root folder does not have any current permissions
                // set to deny all
                List<RootFolderSharePermissionDto> currentPerm =
                        rootFolderSharePermissions.getUnchecked(scanTaskMessage.getRootFolderId());
                if (currentPerm == null || currentPerm.isEmpty()) {
                    SharePermission sharePermission = new SharePermission();
                    sharePermission.setUser(genericUserToDenyAccess);
                    sharePermission.setMask(readMaskSharePermission);
                    sharePermission.setPerm("Read");
                    sharePermission.setType(AclEntryType.DENY.ordinal());
                    Set<SharePermission> sharePermissions = Sets.newHashSet(sharePermission);
                    setSharePermissionsForRootFolder(rootFolderId, sharePermissions);
                }
            }
        } else {
            logger.warn("received share perm msg with no payload {}", scanTaskMessage);
        }
    }

    private void setSharePermissionsForRootFolder(Long rootFolderId, List<String> sharePermissions) {
        if (sharePermissions != null) {
            Set<SharePermission> sharePermissionsObj = Sets.newHashSet();
            sharePermissions.forEach(perm -> {
                try {
                    perm = perm.replace("\\", "\\\\");
                    SharePermission sharePermission = ModelDtoConversionUtils.getMapper().readValue(perm, SharePermission.class);
                    sharePermissionsObj.add(sharePermission);
                } catch (Exception e) {
                    logger.error("problem reading json of shared permission {} for root folder {}, ignored", perm, rootFolderId, e);
                }
            });
            setSharePermissionsForRootFolder(rootFolderId, sharePermissionsObj);
        }
    }

    private void setSharePermissionsForRootFolder(Long rootFolderId, Set<SharePermission> sharePermissions) {
        if (sharePermissions != null && !sharePermissions.isEmpty()) {
            List<RootFolderSharePermission> currentRFPerms = new ArrayList<>();
            if (!sharePermissions.isEmpty()) {
                sharePermissions.forEach( sp -> {
                    if (!Strings.isNullOrEmpty(sp.getUser()) && sp.getMask() != null
                            && (sp.getType() == 0 || sp.getType() == 1)) { // permission type is allow or deny only
                        RootFolderSharePermission rfs = new RootFolderSharePermission();
                        rfs.setMask(sp.getMask());
                        if (sp.getPerm() != null) {
                            rfs.setTextPermissions(sp.getPerm().replace(" ", ""));
                        }
                        rfs.setAclEntryType(sp.getType() == 0 ? AclEntryType.ALLOW : AclEntryType.DENY);
                        rfs.setRootFolderId(rootFolderId);
                        rfs.setUser(sp.getUser());
                        currentRFPerms.add(rfs);
                    }
                });
            }

            rootFolderSharePermissionRepository.deleteForRootFolder(rootFolderId);
            if (!currentRFPerms.isEmpty()) {
                rootFolderSharePermissionRepository.saveAll(currentRFPerms);
            }
            rootFolderSharePermissions.invalidate(rootFolderId);
            rootFolderSharePermissions.getUnchecked(rootFolderId);
        }
    }

    private static List<RootFolderSharePermissionDto> convertRootFolderSharePermission(List<RootFolderSharePermission> perms) {
        if (perms == null) {
            return null;
        }
        return perms.stream().map(SharePermissionService::convertRootFolderSharePermission).collect(Collectors.toList());
    }

    private static RootFolderSharePermissionDto convertRootFolderSharePermission(RootFolderSharePermission perm) {
        if (perm == null) {
            return null;
        }
        RootFolderSharePermissionDto dto = new RootFolderSharePermissionDto();
        dto.setId(perm.getId());
        dto.setUser(perm.getUser());
        dto.setMask(perm.getMask());
        dto.setRootFolderId(perm.getRootFolderId());
        dto.setAclEntryType(perm.getAclEntryType());
        if (perm.getTextPermissions() != null) {
            dto.setTextPermissions(Sets.newHashSet(perm.getTextPermissions().split(",")));
        }
        return dto;
    }
}
