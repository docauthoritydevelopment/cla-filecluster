package com.cla.filecluster.service.audit;

import ch.qos.logback.core.db.DriverManagerConnectionSource;
import com.cla.filecluster.utils.EncUtils;
import com.google.common.base.Strings;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by: yael
 * Created on: 6/21/2018
 */
public class Connector extends DriverManagerConnectionSource {

    @Override
    public Connection getConnection() {

        // I am loading the properties from the resources folder
        try {
            Properties properties = new Properties();
            File propertiesFile = new File("./config","application.properties");
            if (!propertiesFile.exists()) {
                throw new RuntimeException("Failed to find application.properties file ("+propertiesFile.getAbsolutePath()+")");
            }
            try {
                properties.load(new FileInputStream(propertiesFile));
            } catch (IOException e) {
                throw new RuntimeException("Failed to load properties file (IO Exception)",e);
            }

            String url = properties.getProperty("bonecp.url");
            String userName = properties.getProperty("bonecp.username");
            String password = properties.getProperty("bonecp.password");
            String useEncPasswordStr = properties.getProperty("use-enc-pass");
            boolean useEncPass = false;
            if (!Strings.isNullOrEmpty(useEncPasswordStr)) {
                useEncPass = Boolean.parseBoolean(useEncPasswordStr);
            }
            password = useEncPass ? EncUtils.decrypt(password) : password;

            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(
                    url, userName, password);
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
