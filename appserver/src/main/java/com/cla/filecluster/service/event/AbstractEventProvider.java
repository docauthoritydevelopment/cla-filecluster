package com.cla.filecluster.service.event;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
public abstract class AbstractEventProvider<T> implements EventProvider<T> {

    protected void processEvent(T eventData, Class<T> dataType) {
        EventNormalizer<T> normalizer = getEventNormalizer(dataType);
        send(normalizer.normalize(eventData));
    }
}
