package com.cla.filecluster.service.files.ingest;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.repository.jpa.filetag.FileTagRepository;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is responsible to translate anny processing error type to file tag.
 */
@Component
public class ProcessingErrorTypeTagTranslator {

    public static String PROCESSING_ERROR_TAG_TYPE_NAME = "Processing Error";

    private static final Logger logger = LoggerFactory.getLogger(ProcessingErrorTypeTagTranslator.class);

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private FileTagRepository fileTagRepository;

    @Value("${is.toolExecutionMode:false}")
    private boolean isToolExecutionMode;

    private Map<ProcessingErrorType,FileTag> processingErrorTypeToTag = new HashMap<>();

    @PostConstruct
    private void init(){
        if (isToolExecutionMode) {
            logger.info("ToolExecutionMode: skip init()");
            return;
        }
        FileTagType fileTagType = fileTagTypeService.getFileTagTypeByName(PROCESSING_ERROR_TAG_TYPE_NAME);
        if (fileTagType == null) {
            logger.error("Did not find {} tag type, errors will not be registered to files in solr", PROCESSING_ERROR_TAG_TYPE_NAME);
            return;
        }
        List<FileTag> fileTags = fileTagRepository.findFileTagsByType(fileTagType.getId());
        if (fileTags == null || fileTags.isEmpty()){
            logger.error("No error tag is associated with {} tag type, errors will not be registered to files in solr",PROCESSING_ERROR_TAG_TYPE_NAME);
            return;
        }

        for (FileTag fileTag : fileTags) {
            ProcessingErrorType processingErrorType = ProcessingErrorType.byText(fileTag.getName());
            logger.debug("Associating {} to tag {}",processingErrorType,fileTag);
            processingErrorTypeToTag.put(processingErrorType,fileTag);
        }
    }

    public FileTag getFileTag(ProcessingErrorType processingErrorType){
        return processingErrorTypeToTag.get(processingErrorType);
    }
}
