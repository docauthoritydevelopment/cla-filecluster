package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.security.*;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.security.*;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.RoleRepository;
import com.cla.filecluster.repository.jpa.security.UserRepository;
import com.cla.filecluster.repository.jpa.security.WorkGroupRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by uri on 07/09/2016.
 */
@Service
public class UserAppService {
    private static final Logger logger = LoggerFactory.getLogger(UserAppService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleTemplateService roleTemplateService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private WorkGroupRepository workGroupRepository;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${businessRoles.default.file:./config/defaultOperRoles.csv}")
    private String bizRoleDefaultFile;

    private static final String NAME = "name";
    private static final String DISPLAY_NAME = "displayName";
    private static final String DESCRIPTION = "description";
    private static final String TYPE = "type";
    private static final String ROLES = "roles";
    private static final String TEMPLATE_TYPE = "templateType";
    private final static String[] requiredHeaders = {NAME, DESCRIPTION, ROLES};

    @Transactional
    public void populateDefaultRoles() {
        logger.debug("Populate default roles");

        List<Role> newRoles = Arrays.stream(Authority.values())
                .map(auth -> new Role(auth.getName(), auth.getDisplayName(), auth.getDescription(), auth))
                .collect(Collectors.toList());
        roleRepository.saveAll(newRoles);

        logger.debug("Default roles populated");
    }

    @Transactional
    public void populateDefaultBizRoles() {
        if (StringUtils.isNotEmpty(bizRoleDefaultFile)) {
            logger.info("Populate default bizRoles");
            Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, bizRoleDefaultFile, requiredHeaders);
            int failedRows = 0;
            try {
                failedRows = csvReader.readAndProcess(bizRoleDefaultFile, row -> createBizRoleFromRow(row, headersLocationMap),
                        null, null);
            } catch (Exception e) {
                logger.error("Failed to read business roles file.", e);
            }
            if (failedRows > 0) {
                logger.warn("Failed to insert {} business roles from file.", failedRows);
            }
        } else {
            logger.warn("Missing list of default bizRoles - cannot populate default bizRoles");
        }
    }

    @Transactional
    public User populateDefaultAccountUsers() {
        User accountsAdmin = userService.createAccountsAdmin();
        BizRole operator = bizRoleService.getBizRoleByName("operator");
        userService.createAccountAdditionalUsers(operator);
        return accountsAdmin;
    }

    private void createBizRoleFromRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues[0].startsWith("#")) {
            logger.info("Commented line {} in default businessRoles file. ignored: {}", row.getLineNumber(), row.toString());
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String displayName = fieldsValues[headersLocationMap.get(DISPLAY_NAME)].trim();
        String description = fieldsValues[headersLocationMap.get(DESCRIPTION)].trim();
        String type = fieldsValues[headersLocationMap.get(TYPE)];
        String rolesString = fieldsValues[headersLocationMap.get(ROLES)];
        String templateTypeStr = fieldsValues[headersLocationMap.get(TEMPLATE_TYPE)];
        RoleTemplateType templateType = RoleTemplateType.NONE;
        if (!Strings.isNullOrEmpty(templateTypeStr)) {
            templateType = RoleTemplateType.valueOf(templateTypeStr.toUpperCase());
        }
        rolesString = StringUtils.trim(rolesString);
        ArrayList<String> rolesList = rolesString != null ? Lists.newArrayList(rolesString.split(",")) : new ArrayList<>();
        List<Role> roles = roleService.getRolesByNames(rolesList);
        RoleTemplateDto template = roleTemplateService.createRoleTemplate(name, displayName, description, roles, templateType);
        if (Strings.isNullOrEmpty(type) || type.trim().equalsIgnoreCase("operrole")) {
            bizRoleService.createBizRole(name, displayName, description, RoleTemplateService.convertRoleTemplateDto(template, true));
        }
    }

    @Transactional
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {"MngRoles"})
    public BizRoleDto createBizRole(String name, String displayName, String description, Long templateId) {
        logger.debug("Create BizRole {} ({}) with {} template", name, description, templateId);
        RoleTemplateDto template = roleTemplateService.getRoleTemplate(templateId);
        BizRole bizRole = bizRoleService.createBizRole(name, displayName, description, RoleTemplateService.convertRoleTemplateDto(template, true));
        logger.debug("Created bizRole {}", bizRole);
        return BizRoleService.convertBizRole(bizRole);
    }

    @Transactional
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {"MngRoles"})
    public BizRoleDto createBizRole(BizRoleDto bizRoleDto) {
        logger.debug("Create BizRole {}", bizRoleDto);
        BizRole bizRole = bizRoleService.createBizRole(bizRoleDto);
        return BizRoleService.convertBizRole(bizRole);
    }

    @Transactional(readOnly = true)
    public Page<UserDto> getAllUsers(DataSourceRequest dataSourceRequest) {
        return UserService.convertUsersPage(userService.getAllNonSystemUsers(dataSourceRequest.createPageRequest()));
    }

    @Transactional(readOnly = true)
    public UserDto getUserByIdWithRoles(long userId) {
        User user = userService.getUserByIdWithRoles(userId, false);
        if (user == null) {
            return null;
        }
        return UserService.convertUser(user, false);
    }

    @Transactional(readOnly = true)
    public UserDto getUserById(long userId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return null;
        }
        return UserService.convertUser(user, false);
    }


    @Transactional
    public UserDto createUserOnCurrentAccount(UserDto userDto) {
        try {
            User user = userService.createUserOnCurrentAccount(userDto);
            updateUserBizRoles(user.getId(), userDto.getBizRoleDtos(), true);
            //    addSystemRolesToUser(user.getId(),userDto.getSystemRoleDtos(),true);
            Set<FunctionalRoleDto> functionalRoleDtos = userDto.getFuncRoleDtos();
            updateUserFunctionalRoles(user, functionalRoleDtos);
            return UserService.convertUser(userService.getUserById(user.getId()), false);
        } catch (BadRequestException e) {
            throw e;
        } catch (Exception e) {
            throw new BadRequestException(messageHandler.getMessage("user.create.failure",
                    Lists.newArrayList(e.getMessage())), BadRequestType.OPERATION_FAILED);
        }
    }

    /*@Transactional
    public void addSystemRolesToUser(Long userId, Set<SystemRoleDto> systemRoleDtos, boolean override) {
        if (systemRoleDtos != null && (systemRoleDtos.size() > 0 || override)) {
            Set<Role> newSystemRoles = new HashSet<>();
            for (SystemRoleDto systemRoleDto : systemRoleDtos) {
                if (systemRoleDto == null) {
                    throw new BadRequestException("systemRoles", "null value", BadRequestType.UNSUPPORTED_VALUE);
                }
                Role systemRole = roleService.getSystemRole(systemRoleDto.getId());
                newSystemRoles.add(systemRole);
            }
            userService.addSystemRolesToUser(userId, newSystemRoles, override);
        }

    }*/

    @Transactional
    public void updateUserBizRoles(Long userId, Set<BizRoleDto> bizRoleDtos, boolean override) {
        if (bizRoleDtos != null && (bizRoleDtos.size() > 0 || override)) {
            logger.debug("Update user {} bizRoles", userId);
            Set<BizRole> newBizRoles = new HashSet<>();
            for (BizRoleDto bizRoleDto : bizRoleDtos) {
                newBizRoles.add(bizRoleService.getBizRole(bizRoleDto.getId()));
            }
            userService.addBizRolesToUser(userId, newBizRoles, override);
        }
    }

    @Transactional
    public void updateUserFunctionalRoles(User user, Set<FunctionalRoleDto> functionalRoleDtos) {
        if (functionalRoleDtos != null && !functionalRoleDtos.isEmpty()) {
            List<Long> funcRoleIds = functionalRoleDtos.stream().map(FunctionalRoleDto::getId).collect(Collectors.toList());
            Set<FunctionalRole> functionalRoles = functionalRoleService.getFunctionalRoleByIds(funcRoleIds);
            user.setFunctionalRoles(functionalRoles);
            userRepository.save(user);
        } else {
            user.setFunctionalRoles(Collections.EMPTY_SET);
            userRepository.save(user);
        }
    }

    @Transactional
    public UserDto updateUser(long userId, UserDto updatedUser) {
        logger.debug("Update user id: {}", userId);
        try {
            User user = userService.updateUser(userId, updatedUser);
            Set<BizRoleDto> bizRoleDtos = updatedUser.getBizRoleDtos();
            if (bizRoleDtos != null) {
                updateUserBizRoles(userId, bizRoleDtos, true);
            }
            Set<FunctionalRoleDto> functionalRoleDtos = updatedUser.getFuncRoleDtos();
            updateUserFunctionalRoles(user, functionalRoleDtos);
            //   addSystemRolesToUser(user.getId(),updatedUser.getSystemRoleDtos(),true);
            return getUserByIdWithRoles(userId);
//            return ModelDtoConversionUtils.convertUser(user);
        } catch (Exception e) {
            throw new BadRequestException(messageHandler.getMessage("user.update.failure",
                    Lists.newArrayList(e.getMessage())), BadRequestType.OPERATION_FAILED);
        }
    }

    @Transactional
    public UserDto unlockUser(long userId) {
        logger.debug("Unlock user id: {}", userId);
        User user = userService.unlockUser(userId);
        return UserService.convertUser(user, false);
    }

    @Transactional(readOnly = true)
    public UserDto getCurrentUserWithRoles(boolean withAccessTokens) {
        //TODO - also get businessRoles and populate DTO
        return UserService.convertUser(userService.getCurrentUserWithRoles(), false, withAccessTokens);
    }

    @Transactional(readOnly = true)
    public Set<SystemRoleDto> getCurrentUserRoles() {
        User currentUserWithRoles = userService.getCurrentUserWithRoles();
        Set<Role> result = new HashSet<>(currentUserWithRoles.getRoles());
        for (BizRole bizRole : currentUserWithRoles.getBizRoles()) {
            result.addAll(bizRole.getTemplate().getRoles());
        }

        return RoleService.convertRolesToSet(result);
    }


    @Transactional
    public UserDto addBizRoleToUser(long userId, Long bizRoleId) {
        BizRole bizRole = bizRoleService.getBizRole(bizRoleId);
        return userService.addBizRoleToUser(userId, bizRole);
    }

    @Transactional
    public UserDto removeBizRoleFromUser(long userId, Long bizRoleId) {
        BizRole bizRole = bizRoleService.getBizRole(bizRoleId);
        return userService.removeBizRoleFromUser(userId, bizRole);
    }

    @Transactional
    public UserDto addWorkGroupToUser(long userId, Long workGroupId) {
        return workGroupRepository.findById(workGroupId)
                .map(workGroup -> userService.addWorkGroupToUser(userId, workGroup))
                .orElse(null);
    }

    public UserDto removeWorkGroupFromUser(long userId, Long workGroupId) {
        return workGroupRepository.findById(workGroupId)
                .map(workGroup -> userService.removeWorkGroupFromUser(userId, workGroup))
                .orElse(null);
    }

    public List<SystemRoleDto> getRolesWithAuthorities() {
        List<Role> rolesWithAuthorities = roleService.getRolesWithAuthorities();
        return RoleService.convertRoles(rolesWithAuthorities);
    }

    @Transactional(readOnly = true)
    public List<BizRoleDto> getBizRoles() {
        Set<BizRole> bizRoles = bizRoleService.getBizRoles();
        return BizRoleService.convertBizRoles(bizRoles);
    }

    @Transactional
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {"MngRoles"})
    public BizRoleDto archiveBizRole(long id) {
        return bizRoleService.archiveBizRole(id);
    }

    @Transactional
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {"MngRoles"})
    public BizRoleDto updateBizRole(Long id, BizRoleDto bizRoleDto) {
        BizRole bizRoleByName = bizRoleService.getBizRoleByName(bizRoleDto.getName());
        if (bizRoleByName != null && !id.equals(bizRoleByName.getId())) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
        RoleTemplate template = RoleTemplateService.convertRoleTemplateDto(bizRoleDto.getTemplate(), true);
        BizRole bizRole = bizRoleService.updateBizRole(id, bizRoleDto.getName(), bizRoleDto.getDescription(), template);
        return BizRoleService.convertBizRole(bizRole);
    }

    @Transactional(readOnly = true)
    public Page<UserDto> getCompRoleAssignedUsers(Long compRoleId, DataSourceRequest dataSourceRequest) {
        Page<User> users = userService.getCompRoleAssignedUsers(compRoleId, dataSourceRequest.createPageRequest());
        return UserService.convertUsersPage(users);
    }

    @Transactional(readOnly = true)
    public BizRoleDto getBizRole(long id) {
        BizRole bizRole = bizRoleService.getBizRole(id);
        return bizRole != null ? BizRoleService.convertBizRole(bizRole) : null;
    }

    @Transactional(readOnly = true)
    public BizRoleDto getBizRoleById(long id) {
        BizRole bizRole = bizRoleService.getBizRoleById(id);
        return bizRole != null ? BizRoleService.convertBizRole(bizRole) : null;
    }

    @Transactional(readOnly = true)
    public BizRoleDto getBizRoleByName(String name) {
        BizRole bizRole = bizRoleService.getBizRoleByName(name);
        return bizRole != null ? BizRoleService.convertBizRole(bizRole) : null;
    }

    @Transactional
    public SystemRoleDto getSystemRoleByName(String name) {
        Role role = roleService.getRoleByName(name);
        return RoleService.convertRole(role);
    }

    @Transactional
    public UserDto assignWorkGroups(long userId, List<Long> workGroupIds) {
        User userById = userService.getUserById(userId);
        List<WorkGroup> workGroups = workGroupRepository.findAllById(workGroupIds);
        userById.getWorkGroups().addAll(workGroups);
        return UserService.convertUser(userById, false);
    }

    @Transactional
    public UserDto unAssignWorkGroups(long userId, List<Long> workGroupIds) {
        User userById = userService.getUserById(userId);
        List<WorkGroup> workGroups = workGroupRepository.findAllById(workGroupIds);
        userById.getWorkGroups().removeAll(workGroups);
        return UserService.convertUser(userById, false);
    }


}
