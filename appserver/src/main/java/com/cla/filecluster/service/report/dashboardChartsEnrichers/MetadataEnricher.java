package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import org.springframework.stereotype.Component;
import java.util.stream.Collectors;


@Component
public class MetadataEnricher implements DefaultTypeDataEnricher {



    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) -> {
            return category.replace(';','/');
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return CatFileFieldType.DA_METADATA.getSolrName();
    }

}
