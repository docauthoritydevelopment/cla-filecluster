package com.cla.filecluster.service.files;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileSizePartitionDto;
import com.cla.common.utils.FileSizeUnits;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.repository.solr.SolrFileSizeQueryUtils;
import com.cla.filecluster.service.categories.FileCatAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class FileSizeRangeService {

    @Autowired
    private FileCatAppService fileCatAppService;

    @Value("${file-size-partitions:1GB,500MB-1GB,250MB-500MB,50MB-250MB,10MB-50MB,1MB-10MB,0MB-1MB}")
    private String partitionsStr;

    private Map<Long, FileSizePartitionDto> partitions = new HashMap<>();

    @PostConstruct
    void init() {
        long id = 1;
        String[] parts = partitionsStr.split(",", 30);
        for (String part : parts) {
            FileSizePartitionDto dto = new FileSizePartitionDto();
            dto.setId(id++);
            String[] range = part.split("-", 2);
            if (range.length > 0) {
                Integer amount = getAmount(range[0]);
                FileSizeUnits unit = getUnit(range[0]);
                dto.setFromValue(amount);
                dto.setFromType(unit);
            }
            if (range.length > 1) {
                Integer amount = getAmount(range[1]);
                FileSizeUnits unit = getUnit(range[1]);
                dto.setToValue(amount);
                dto.setToType(unit);
            }
            dto.setSolrQuery(SolrFileSizeQueryUtils.getFileSizeFilter(dto));
            partitions.put(dto.getId(), dto);
        }
    }

    public Collection<FileSizePartitionDto> getPartitions() {
        return partitions.values();
    }

    public FileSizePartitionDto getPartition(Long id) {
        return partitions.get(id);
    }

    private Integer getAmount(String part) {
        String amount = part.substring(0, part.length() - 2);
        return Integer.parseInt(amount);
    }

    private FileSizeUnits getUnit(String part) {
        String unit = part.substring(part.length() - 2);
        return FileSizeUnits.valueOf(unit);
    }

    public FacetPage<AggregationCountItemDTO<FileSizePartitionDto>> getFileSizeFileCount(DataSourceRequest dataSourceRequest) {
        Map<String, Integer> fileSizePartitionCounts = fileCatAppService.getFileSizePartitionCounts(dataSourceRequest, partitions);
        Map<String, Integer> fileSizePartitionCountsNoFilter = new HashMap<>();
        if (dataSourceRequest.hasFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            fileSizePartitionCountsNoFilter = fileCatAppService.getFileSizePartitionCounts(dataSourceRequest, partitions);
        }
        final FacetPage<AggregationCountItemDTO<FileSizePartitionDto>> facetPage = convertFacetFieldResultToDto(
                fileSizePartitionCounts, fileSizePartitionCountsNoFilter);
        facetPage.setTotalElements(partitions.size());
        return facetPage;
    }

    private FacetPage<AggregationCountItemDTO<FileSizePartitionDto>> convertFacetFieldResultToDto(
            Map<String, Integer> fileSizePartitionCounts, Map<String, Integer> fileSizePartitionCountsNoFilter) {
        List<AggregationCountItemDTO<FileSizePartitionDto>> content = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(0, partitions.size());
        long pageAggCount = 0;
        for (FileSizePartitionDto partition : partitions.values()) {
            String solrQuery = partition.getSolrQuery();
            String key = CatFileFieldType.SIZE.getSolrName() + ":" + solrQuery;
            Integer count = fileSizePartitionCounts.get(key);
            if (count == null) {
                count = 0;
            }
            Integer countNoFilter = fileSizePartitionCountsNoFilter.get(key);
            AggregationCountItemDTO<FileSizePartitionDto> item = new AggregationCountItemDTO<>(partition, count);
            if (countNoFilter != null) {
                item.setCount2(countNoFilter);
            }
            content.add(item);
            pageAggCount += count;
        }
        return new FacetPage<>(content, pageRequest, pageAggCount, pageAggCount);
    }
}
