package com.cla.filecluster.service.crawler.pv;

import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.pv.ContentGroupDetails;
import com.cla.filecluster.service.files.pv.ContentIdGroupsPool;
import com.cla.filecluster.service.files.pv.GrpHelperFetcher;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class FileGroupsPoolManager implements GrpHelperFetcher {
	private static final Logger logger = LoggerFactory.getLogger(FileGroupsPoolManager.class);

    @Autowired
    private ContentMetadataService contentMetadataService;

	private long count = 0;
	private long totalTime = 0;
	private long totalRecords = 0;

    public ContentIdGroupsPool createContentPool(Collection<Long> contentIdList) {
        return new ContentIdGroupsPool(this, contentIdList);
    }

    public long getCount() {
		return count;
	}

	private float getAveragePoolSize() {
		return ((float)totalRecords)/count;
	}

    private float getAverageReadTimeMS() {
		return ((float)totalTime)/count;
	}

    @Override
    public UUID getContentGroupIdUUID(long contentId) {
        String analysisGroupIdByContentId = getContentGroupId(contentId);
        return getGroupUUID(analysisGroupIdByContentId);
    }

    @Override
    public String getContentGroupId(long contentId) {
        return contentMetadataService.getContentGroupId(contentId);
    }

    private UUID getGroupUUID(String fileGroup) {
        return (fileGroup == null) ? null : UUID.fromString(fileGroup);
    }

    @Override
    public Map<Long, ContentGroupDetails> getContentGroupByIds(Collection<Long> contentIds) {
        if (contentIds == null || contentIds.size() == 0) {
            return Maps.newHashMap();
        }
        List<Object[]> results = contentMetadataService.findAnalysisGroupIdByContentIds(contentIds);
        Map<Long, ContentGroupDetails> recordMap = new HashMap<>(results.size());
        results.forEach(rec -> recordMap.put((Long) rec[0], new ContentGroupDetails((String) rec[1], AnalyzeHint.valueOf((String)rec[2]))));
        return recordMap;
    }

    @Override
    public ContentGroupDetails getContentGroupDetails(long contentId) {
        Optional<SolrContentMetadataEntity> contentOp = contentMetadataService.getOneById(String.valueOf(contentId));
        return contentOp.isPresent() ?
                new ContentGroupDetails(contentOp.get().getAnalysisGroupId(), AnalyzeHint.valueOf(contentOp.get().getAnalysisHint())) :
                null;
    }

    @Override
    public void updateStatistics(long time, int size) {
        totalTime += time;
        count++;
        totalRecords += size;


        if (count % 100 == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("ContentGroupsPoolManager read in {}ms. size={} count={} avgSize={} avgTime={}",
                        time, size, getCount(), getAveragePoolSize(), getAverageReadTimeMS());
            }
        } else {
            if (logger.isTraceEnabled()) {
                logger.trace("FileGroupsPoolManager read in {}ms. size={} count={} avgSize={} avgTime={}",
                        time, size, getCount(), getAveragePoolSize(), getAverageReadTimeMS());
            }
        }

    }
}