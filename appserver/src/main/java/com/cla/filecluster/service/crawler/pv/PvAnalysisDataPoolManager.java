package com.cla.filecluster.service.crawler.pv;

import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.repository.solr.SolrAnalysisDataRepository;
import com.cla.filecluster.service.files.pv.PvAnalysisDataFetcher;
import com.cla.filecluster.service.files.pv.PvAnalysisDataPool;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Component
public class PvAnalysisDataPoolManager implements PvAnalysisDataFetcher {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PvAnalysisDataPoolManager.class);

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    @PersistenceContext
    private EntityManager em;

    private long count = 0;
    private long totalTime = 0;
    private long totalRecords = 0;

    @Override
    public PvAnalysisData getPvAnalysisDataByContentId(Long contentId) {
        return solrAnalysisDataRepository.findByContentId(contentId);
    }

    @Override
    public Pair<PvAnalysisData, Integer> getPvAnalysisDataAndSizeByContentId(Long contentId) {
        return Pair.of(null, 0);
    }

    @Override
    public List<PvAnalysisData> getPvAnalysisDataByContentIds(Collection<Long> contentIdList) {
        return solrAnalysisDataRepository.findByContentId(contentIdList);
    }

    @Override
    public List<Pair<PvAnalysisData, Integer>> getPvAnalysisDataByContentIdsAndSize(Collection<Long> contentIdList) {
        return Lists.newArrayList();
    }

    @Override
    public void updateStatistics(long time, int size) {
        totalTime += time;
        count++;
        totalRecords += size;
        if (count % 100 == 0) {
            if (logger.isDebugEnabled()) {
                logger.debug("PvAnalysisDataPool read in {}ms. size={} count={} avgSize={} avgTime={}",
                        time, size, getCount(), getAveragePoolSize(), getAverageReadTimeMS());
            }
        } else {
            if (logger.isTraceEnabled()) {
                logger.trace("PvAnalysisDataPool read in {}ms. size={} count={} avgSize={} avgTime={}",
                        time, size, getCount(), getAveragePoolSize(), getAverageReadTimeMS());
            }
        }
    }

    public PvAnalysisDataPool createPool(final Collection<Long> contentIdList) {
        return new PvAnalysisDataPool(contentIdList,this);
    }

    public long getCount() {
        return count;
    }

    public long getTotalTimeMS() {
        return totalTime;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public float getAveragePoolSize() {
        return ((float) totalRecords) / count;
    }

    public float getAverageReadTimeMS() {
        return ((float) totalTime) / count;
    }

}