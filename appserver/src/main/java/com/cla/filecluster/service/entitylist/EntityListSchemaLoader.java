package com.cla.filecluster.service.entitylist;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.BizListItemRow;
import com.cla.filecluster.domain.entity.bizlist.BizListSchema;
import com.cla.filecluster.domain.exceptions.BadImportException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.util.csv.CsvReader;
import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.supercsv.cellprocessor.ift.CellProcessor;

import java.io.File;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;

/**
 * Created by uri on 23/08/2015.
 */
@Service
public class EntityListSchemaLoader {
    private static Logger logger = LoggerFactory.getLogger(EntityListSchemaLoader.class);

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${entity.list.clear.id.header}")
    private String clearIdHeader;

    @Value("${entity.list.clear.name.header}")
    private String clearNameHeader;

    @Value("${bizlist.client.importCsv.header.bid:BUSINESS ID}")
    private String clientCsvHeaderBid;
    @Value("${bizlist.client.importCsv.header.name:NAME}")
    private String clientCsvHeaderName;
    @Value("${bizlist.client.importCsv.header.aliases:ALIASES}")
    private String clientCsvHeaderAliases;
    @Value("${bizlist.client.importCsv.header.singleAliasPrefix:ALIAS}")
    private String clientCsvSingleAliasHeaderPrefix;
    @Value("${bizlist.client.importCsv.clearedSuffix:_CLEARED}")
    private String clientCsvClearedSuffix;

    @Value("${bizlist.consumer.template.validator.prefix:./config/extraction/entities-file-validation-}")
    private String consumerTemplateValidatorPrefix;

    @Value("${bizlist.consumer.encryptAllFields:true}")
    private boolean encryptAllConsumerFields;

    @Autowired
    private ResourceLoader resourceLoader;

    public BizListSchema loadCsvSchema(BizList bizList) {
        return loadCsvSchema(bizList.getBizListSource());
//        throw new RuntimeException("Unsupported BizList source type ["+ bizList.getEntityListSourceType()+"]");
    }

    private BizListSchema loadCsvSchema(String entitiesFileName) {
        try {
            logger.debug("Extracting entities from file {}",entitiesFileName);
            BizListSchema result = new BizListSchema();
            final String[] headers = csvReader.getHeaders(entitiesFileName);
            result.setHeaders(headers);
            updateClearedHeaders(headers, result);
            csvReader.readAndProcess(
                    entitiesFileName,
                    row -> parseRow(result,headers, row),
                    null,
                    new HashMap<String, CellProcessor>());

            logger.debug("BizList Schema {} loaded successfully", entitiesFileName);
            return result;
        } catch (final Exception e) {
            logger.error("Failed processing {}",e);
            throw new RuntimeException(messageHandler.getMessage("biz-list-item.load-file.failure", Lists.newArrayList(e.getMessage())),e);
        }

    }

    public BizListSchema loadCsvSchema(byte[] rawData, BizListItemType bizListItemType, String template) {
        try {
            logger.debug("Extracting entities from raw data (size:{}) based on template ",rawData.length,template);
            BizListSchema result = new BizListSchema();
            final String[] headers = csvReader.getHeaders(rawData);
            result.setHeaders(headers);
            for (int i=0; i< headers.length; i++) {
                headers[i] = headers[i].toUpperCase();
                if (headers[i].contains("[")) {
                    headers[i] = headers[i].replace("[","").replace("]","").trim();
                }
            }
            List<String> headersList = Lists.newArrayList(headers);
            switch (bizListItemType) {
                case SIMPLE:
                case CLIENTS:
                    mapClientListHeaders(result, headersList);
                    break;
                case CONSUMERS:
                    mapConsumerListHeaders(result,headersList,template);
                    break;
            }

			Reader reader = new StringReader(new String(rawData, Charsets.UTF_8));
            csvReader.readAndProcess(reader,
                    row -> parseRow(result,headers, row),
                    null,
                    new HashMap<String, CellProcessor>());
            return result;
        } catch (Exception e) {
            logger.error("Failed processing {}",e);
            throw new BadImportException(messageHandler.getMessage("biz-list-item.load-file.failure", Lists.newArrayList(e.getMessage())),e);
        }

    }

    private void mapConsumerListHeaders(BizListSchema result, List<String> headersList, String template) {
        for (int i=0; i<headersList.size(); i++) {
            String header = headersList.get(i);
            //Find _CLEAR header names
            if (encryptAllConsumerFields || header.endsWith(clientCsvClearedSuffix)) {
                String clearHeader = header.endsWith(clientCsvClearedSuffix) ?
                        header.substring(0,header.length()-clientCsvClearedSuffix.length()) : header;
                switch (clearHeader.toUpperCase()) {
                    case "NAME":
                        result.setClearNameHeaderIndex(i);
                        break;
                    case "ID":
                        result.setClearIdHeaderIndex(i);
                        break;
                    default:
                        result.setClearIdHeaderIndex(i);
                        result.setClearNameHeaderIndex(i);

                }
            }
        }
        if (result.getClearIdHeaderIndex() < 0) {
            throw new BadRequestException(messageHandler.getMessage("biz-list-item.load-file.missing-header",
                    Lists.newArrayList(clientCsvHeaderBid, clientCsvClearedSuffix)), BadRequestType.MISSING_FIELD);
        }
        if (template != null) {
            logger.debug("Verify all required headers are here ({})",template);
            Map<String, CellProcessor> stringCellProcessorMap = loadCellValidator(template);
            for (String requiredHeader : stringCellProcessorMap.keySet()) {
                if (!headersList.contains(requiredHeader) && !headersList.contains(requiredHeader.toUpperCase())) {
                    StringJoiner stringJoiner = new StringJoiner(",");
                    headersList.forEach(f -> stringJoiner.add(f));
                    logger.error("Failed to find required header {} in headerList ({})",requiredHeader, stringJoiner);
                    throw new RuntimeException(messageHandler.getMessage("biz-list-item.load-file.missing-header", Lists.newArrayList(stringJoiner, requiredHeader)));
                }
            }

        }
    }

    public Map<String, CellProcessor> loadCellValidator(String templateName) {
        String configurationFileName = consumerTemplateValidatorPrefix + templateName + ".xml";
        String location = "file:" + configurationFileName;
        logger.info("Loading CSV cell validator from "+location);
        if (!new File(configurationFileName).exists()) {
            logger.error("Illegal Cell validator parameter value. CSV Cell validator file not found ("+configurationFileName+")");
            throw new RuntimeException("Illegal Cell validator parameter value. CSV Cell validator file not found ("+configurationFileName+")");
        }
        org.springframework.core.io.Resource resource = resourceLoader.getResource(location);
        logger.debug("Loading XML Bean reader");
        DefaultListableBeanFactory xbf = new DefaultListableBeanFactory();
        XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(xbf);
        logger.debug("Loading Bean definitions");
        reader.loadBeanDefinitions(resource);
        logger.debug("Loading Bean definitions");
        return (Map<String,CellProcessor>) xbf.getBean("cellValidationProcessorsMap");
    }

    public void mapClientListHeaders(BizListSchema result, List<String> headersList) {
        if (headersList.indexOf(clientCsvHeaderBid) == -1) {
            throw new BadRequestException(messageHandler.getMessage("biz-list-item.load-file.missing-header",
                    Lists.newArrayList(clientCsvHeaderBid, clientCsvHeaderBid)), BadRequestType.MISSING_FIELD);
        }
        result.setClearIdHeaderIndex(headersList.indexOf(clientCsvHeaderBid));
        if (headersList.indexOf(clientCsvHeaderName) == -1) {
            throw new BadRequestException(messageHandler.getMessage("biz-list-item.load-file.missing-header",
                    Lists.newArrayList(clientCsvHeaderName, clientCsvHeaderName)), BadRequestType.MISSING_FIELD);
        }
        result.setClearNameHeaderIndex(headersList.indexOf(clientCsvHeaderName));
        if (headersList.indexOf(clientCsvHeaderAliases) != -1) {
            logger.info("Loading import CSV style");
            result.setAliasesHeaderIndex(headersList.indexOf(clientCsvHeaderAliases));
        }
        else {
            // Look for single alias prefix only if 'ALIASES' field not present
            int inx = 0;
            for (String header : headersList) {
                if (header.startsWith(clientCsvSingleAliasHeaderPrefix)) {
                    result.addSingleAliasHeadersIndex(inx);
                }
                ++inx;
            }
        }
    }

    private void updateClearedHeaders(String[] headers, BizListSchema result) {
        List<String> headersList = Lists.newArrayList(headers);
        if (headersList.indexOf(clearIdHeader) > -1) {
            result.setClearIdHeaderIndex(headersList.indexOf(clearIdHeader));
        }
        if (clearNameHeader != null && headersList.indexOf(clearNameHeader) > -1) {
            result.setClearNameHeaderIndex(headersList.indexOf(clearNameHeader));
        }

    }

    private void parseRow(BizListSchema schema, String[] headers, RowDTO row) {
        try {
            BizListItemRow bizListItemRow = new BizListItemRow();
            final String[] fieldsValues = row.getFieldsValues();

            if (logger.isDebugEnabled() && row.getLineNumber() % 100 == 0) {
                logger.debug("Processing line #{} with the following field values {}",row.getLineNumber(), Arrays.asList(fieldsValues));
            }

            String clearId = fieldsValues[schema.getClearIdHeaderIndex()];
            bizListItemRow.setClearId(clearId);
            if (schema.isNameHeaderInUse()) {
                int clearNameHeaderIndex = schema.getClearNameHeaderIndex();
                String clearedName = fieldsValues[clearNameHeaderIndex];
                bizListItemRow.setClearName(clearedName);
            }
            if (schema.getAliasesHeaderIndex() > -1) {
                String aliases = fieldsValues[schema.getAliasesHeaderIndex()];
                String[] aliasesStrArr = null;
                try {
                    aliasesStrArr = aliases.split(";");
                } catch (Exception e) {}
                if (aliasesStrArr != null && aliasesStrArr.length > 0) {
                    bizListItemRow.setFieldValues(aliasesStrArr);
                }
            }
            else if (schema.getSingleAliasHeadersIndexes() != null) {
                String[] aliases = schema.getSingleAliasHeadersIndexes().stream().map(inx->fieldsValues[inx])
                        .filter(s -> s!=null)
                        .toArray(size -> new String[size]);
                if (aliases.length > 0) {
                    bizListItemRow.setFieldValues(aliases);
                }
            }
            else {
                bizListItemRow.setFieldValuesOnly(fieldsValues);
            }
            schema.addEntityListItem(bizListItemRow);
        } catch (final Exception e) {
            logger.error("Failed to extract entity for row {}",row,e);
        }

    }


}
