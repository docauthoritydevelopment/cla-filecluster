package com.cla.filecluster.service.files.content;

import com.cla.common.connectors.ExistingContentConnector;
import com.cla.common.domain.dto.messages.ContentMetadataRequestMessage;
import com.cla.common.domain.dto.messages.ContentMetadataResponseMessage;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * Created by uri on 07-Mar-17.
 */
@Service
public class InternalExistingContentCheckerService implements ExistingContentConnector {

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Override
    public ContentMetadataResponseMessage checkForExistingContent(ContentMetadataRequestMessage mdRequest) {
        return filesDataPersistenceService.processRequest(mdRequest);
    }
}
