package com.cla.filecluster.service.export.exporters.components;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface ComponentHeaderFields {

    String TIMESTAMP = "Timestamp";
    String INGEST_TASK_REQ = "Ingest Task Requests";
    String ANALYSE_TASK_REQ = "Analyze Task Requests";
    String INGEST_THROUGHPUT = "Ingest Throughput";
    String ANALYSE_THROUGHPUT = "Analyze Throughput";
    String DISK_SPACE = "Disk space utilization";
    String MEMORY = "Memory";
    String CPU = "System CPU";
    String START_MAP = "Start Map";
    String THROUGHPUT = "Throughput";

    String NAME = "Name";
    String INSTANCE = "Instance Id";
    String STATE = "State";
    String DATA_CENTER = "Data center";
    String EXTERNAL_ADDRESS = "External Net Address";
    String RESPONSE_DATE = "Last response date";
    String STATE_CNG_DATE = "State Change Date";

    String MAP_REQ = "Map Requests";
    String INGEST_REQ = "Ingest Requests";
    String REAL_TIME_REQ = "Real time Requests";
    String CONTENT_REQ = "Content Check Requests";

    String MAP_RESP = "Map Responses";
    String INGEST_RESP = "Ingest Responses";
    String REAL_TIME_RESP = "Real time Responses";
    String CONTENT_RESP = "Content Check Responses";

}
