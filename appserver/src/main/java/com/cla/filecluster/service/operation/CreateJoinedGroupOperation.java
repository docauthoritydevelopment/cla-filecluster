package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * create a new user group to bind several child groups operation
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
@Service
public class CreateJoinedGroupOperation implements ScheduledOperation {

    private static final Logger logger = LoggerFactory.getLogger(CreateJoinedGroupOperation.class);

    public static String GROUP_NAME_PARAM = "GROUP_NAME";
    public static String CHILD_GROUPS_PARAM = "CHILD_GROUPS";
    public static String UPDATED_CHILD_GROUPS_PARAM = "UPDATED_CHILD_GROUPS";
    public static String NEW_GROUP_ID_PARAM = "NEW_GROUP_ID";

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 2;
    }

    @Override
    public void preOperation(Map<String, String> opData) {
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CreateJoinedGroupOperation.CHILD_GROUPS_PARAM).split(",")));
        groupsApplicationService.lockGroupsForJoin(childrenGroupIds);
    }

    @Override
    public List<String> getRelevantGroupIds(Map<String, String> opData) {
        List<String> result = new ArrayList<>();
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CHILD_GROUPS_PARAM).split(",")));
        result.addAll(childrenGroupIds);
        result.add(opData.get(NEW_GROUP_ID_PARAM));
        return result;
    }

    @Override
    public void postOperation(Map<String, String> opData) {
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CreateJoinedGroupOperation.CHILD_GROUPS_PARAM).split(",")));
        groupsApplicationService.unLockGroupsForJoin(childrenGroupIds);
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            groupsApplicationService.createJoinedGroupStepUpdFilesDB(opData);
        } else if (step == 1) {
            groupsApplicationService.createJoinedGroupStepUpdSolrAndParent(opData);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String groupName = opData.get(GROUP_NAME_PARAM);
        if (Strings.isNullOrEmpty(groupName)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.name.empty"), BadRequestType.MISSING_FIELD);
        }
        String childGroups = opData.get(CHILD_GROUPS_PARAM);
        if (Strings.isNullOrEmpty(childGroups)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.child.empty"), BadRequestType.MISSING_FIELD);
        }
        String groupId = opData.get(NEW_GROUP_ID_PARAM);
        if (Strings.isNullOrEmpty(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.new.empty"), BadRequestType.MISSING_FIELD);
        }

        String childGroupsJoined = opData.get(UPDATED_CHILD_GROUPS_PARAM);
        if (Strings.isNullOrEmpty(childGroupsJoined)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.join-child.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.CREATE_JOINED_GROUP;
    }

    void setMessageHandler(MessageHandler messageHandler) { // for tests
        this.messageHandler = messageHandler;
    }
}
