package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePointDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.date.DateRangePartition;
import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.service.date.DateRangeAppService;
import com.cla.filecluster.service.date.DateRangeService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
abstract class DatePartitionEnricher implements CategoryTypeDataEnricher {

    protected static final String DATE_PARTITION_DUMMY_PREFIX = "datePartition";

    @Autowired
    private DateRangeService dateRangeService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;


    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;


    @Override
    public boolean needToRemoveEmptyValues(){
        return false;
    }

    @Override
    public Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params) {
        Long datePartitionId = dashboardChartsUtil.extractDatePartitionId(params);
        Map<String, String> ans = new HashMap<>();
        DateRangePartition dateRangePartition = dateRangeService.getDateRangePartitionWithItems(datePartitionId);
        List<SimpleDateRangeItem> simpleDateRangeItems = dateRangePartition.getSimpleDateRangeItems();
        List<DateRangeItemDto> dateRangeItemDtos = DateRangeAppService.convertSimpleDateRangeItems(simpleDateRangeItems, false);
        DateRangePointDto previousStart = null;
        for (DateRangeItemDto dateRangeItemDto : dateRangeItemDtos) {
            DateRangePointDto start = dateRangeItemDto.getStart();
            DateRangePointDto end = dateRangeItemDto.getEnd() != null ? dateRangeItemDto.getEnd() : previousStart;
            if (start == null) {
                throw new BadRequestException("start at " + dateRangeItemDto, "null", BadRequestType.MISSING_FIELD);
            }
            if (end == null) {
                continue;
            }
            String filter = solrSpecificationFactory.buildDateTimeFilter(start, end);
            ans.put(String.valueOf(dateRangeItemDto.getId()),"(" + filter + ")");
            previousStart = start;
        }
        return ans;
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
        Long datePartitionId = dashboardChartsUtil.extractDatePartitionId(params);
        DateRangePartition dateRangePartition = dateRangeService.getDateRangePartitionWithItems(datePartitionId);
        Map<Long, SimpleDateRangeItem> dPartitionMap = dateRangePartition.getSimpleDateRangeItems().stream().collect(Collectors.toMap(SimpleDateRangeItem::getId, ftc -> ftc));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return dPartitionMap.get(Long.parseLong(category)).getName();
        }).collect(Collectors.toList()));
    }


}
