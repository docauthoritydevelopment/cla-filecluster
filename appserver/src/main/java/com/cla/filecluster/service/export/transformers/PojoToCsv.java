package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.progress.ProgressService;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.google.common.collect.Iterables;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import reactor.core.publisher.Flux;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;

@Slf4j
public class PojoToCsv<V> extends ProgressedTransformer<Iterable<V>, String> {

    public static <T> PojoToCsv<T> with(String progressId,
                                        ProgressService.Handler progress,
                                        Map<String, String> requestParams) {
        return new PojoToCsv<>(progressId, progress, requestParams);
    }

    // same as above, but receiving a specific input type - helps the compiler with ".compose" chains
    public static <T> PojoToCsv<T> with(String progressId,
                                        ProgressService.Handler progress,
                                        Map<String, String> requestParams,
                                        Class<T> dataType) {
        return new PojoToCsv<>(progressId, progress, requestParams);
    }

    @Getter
    private final CsvMapper mapper;
    private final Map<String, String> requestParams;
    private BiFunction<Map<String, String>, Iterable<V>, CsvSchema> schemaProvider;

    private PojoToCsv(String progressId, ProgressService.Handler progress, Map<String, String> requestParams, CsvMapper mapper) {
        super(progressId, progress);
        this.requestParams = requestParams;
        this.mapper = mapper;
    }

    public PojoToCsv(String progressId, ProgressService.Handler progress, Map<String, String> requestParams) {
        this(progressId, progress, requestParams, initMapper());
    }

    public <T> PojoToCsv<T> addMixIn(Class<?> target, Class<?> mixInSource) {
        mapper.addMixIn(target, mixInSource);
        return (PojoToCsv<T>) this;
    }

    @SuppressWarnings("unchecked")
    public <T> PojoToCsv<T> configureMapper(Consumer<CsvMapper> mapperConfigurator) {
        mapperConfigurator.accept(mapper);
        return (PojoToCsv<T>) this;
    }

    private static CsvMapper initMapper() {
        CsvMapper mapper = new CsvMapper();
        mapper.configure(JsonGenerator.Feature.IGNORE_UNKNOWN,true);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        return mapper;
    }

    public <T> PojoToCsv<T> setScemaProvider(BiFunction<Map<String, String>, Iterable<V>, CsvSchema> schemaProvider) {
        this.schemaProvider = schemaProvider;
        return (PojoToCsv<T>) this;
    }

    @Override
    public Flux<String> apply(Flux<Iterable<V>> vFlux) {
        return vFlux.map(this::createCsv);
    }

    private String createCsv(Iterable<V> objects) {
        CsvSchema schema = schemaProvider.apply(requestParams, objects);

        // get the csv writer
        ObjectWriter csvWriter = mapper.writer(schema.withHeader());

        try (ByteArrayOutputStream resultsStream = new ByteArrayOutputStream(); SequenceWriter csvRowWriter = csvWriter.writeValues(resultsStream)) {
            if (Iterables.isEmpty(objects)) {
                csvRowWriter.write(new Object());
            } else {
                writeCsvData(objects, csvRowWriter);
            }
            csvRowWriter.flush();
            getProgress().set(100, "Done");
            return resultsStream.toString("utf-8");
        } catch (IOException e) {
            throw new RuntimeException(String.format("error processing CSV conversion for export: %s", getExportId()), e);
        }
    }

    private void writeCsvData(Iterable<V> objects, SequenceWriter csvRowWriter) {
        objects.forEach(element -> {
            try {
                log.trace("writing element: {}", element);
                csvRowWriter.write(element);
            } catch (IOException e) {
                log.error("error while exporting {}", element, e);
                throw new RuntimeException("error while exporting, Failing export JOB", e);
            }
        });
    }

    protected DataBuffer createDataBuffer(String csv) {
        return new DefaultDataBufferFactory().wrap(csv.getBytes());
    }
}
