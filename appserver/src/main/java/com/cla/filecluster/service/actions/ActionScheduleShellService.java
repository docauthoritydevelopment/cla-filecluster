package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.action.ActionScheduleDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.domain.entity.actions.ActionSchedule;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.actions.ActionExecutionStatusRepository;
import com.cla.filecluster.repository.jpa.actions.ActionScheduleRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * service to work with shell script to CRUD scheduled actions until we have ui for it
 *
 * Created by: yael
 * Created on: 3/6/2018
 *
 * To be removed with shell script once ui is written!
 */
@Service
public class ActionScheduleShellService {

    private static final Logger logger = LoggerFactory.getLogger(ActionScheduleShellService.class);

    @Autowired
    private ActionScheduleRepository actionScheduleRepository;

    @Autowired
    private ActionScheduleService actionScheduleService;

    @Autowired
    private ActionExecutionStatusRepository actionExecutionStatusRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional
    public ActionScheduleDto createActionSchedule (ActionSchedule newSchedule) {
        ActionSchedule res = actionScheduleRepository.save(newSchedule);
        ActionScheduleDto actionScheduleDto = ActionScheduleService.convertActionSchedule(res);
        if (actionScheduleDto.getActive()) {
            actionScheduleService.startScheduleAction(actionScheduleDto);
        }
        return actionScheduleDto;
    }

    @Transactional
    public ActionScheduleDto updateScheduleActionActive(String name, boolean state) {
        if (Strings.isNullOrEmpty(name)) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.empty"), BadRequestType.MISSING_FIELD);
        }
        ActionSchedule res = actionScheduleRepository.findByName(name);
        if (res == null) {
            logger.error("Could not find ActionSchedule by name {}.", name);
            return null;
        }
        boolean wasActive = res.isActive();
        res.setActive(state);
        ActionScheduleDto actionScheduleDto = ActionScheduleService.convertActionSchedule(actionScheduleRepository.save(res));
        if (wasActive && !state) {
            actionScheduleService.stopScheduleAction(res, false);
        } else if (state && !wasActive) {
            actionScheduleService.startScheduleAction(actionScheduleDto);
        }
        return actionScheduleDto;
    }

    @Transactional
    public ActionScheduleDto updateScheduleActionCron(String name, String cron) {
        if (Strings.isNullOrEmpty(name)) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.empty"), BadRequestType.MISSING_FIELD);
        }
        ActionSchedule res = actionScheduleRepository.findByName(name);
        if (res == null) {
            logger.error("Could not find ActionSchedule by name {}.", name);
            return null;
        }
        res.setCronTriggerString(cron);
        ActionScheduleDto actionScheduleDto = ActionScheduleService.convertActionSchedule(actionScheduleRepository.save(res));
        if (actionScheduleDto.getActive()) {
            actionScheduleService.stopScheduleAction(res, false);
            actionScheduleService.startScheduleAction(actionScheduleDto);
        }
        return actionScheduleDto;
    }

    @Transactional
    public ActionScheduleDto updateScheduleActionPath(String name, String path) {
        if (Strings.isNullOrEmpty(name)) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.empty"), BadRequestType.MISSING_FIELD);
        }
        ActionSchedule res = actionScheduleRepository.findByName(name);
        if (res == null) {
            logger.error("Could not find ActionSchedule by name {}.", name);
            return null;
        }
        res.setResultPath(path);
        return ActionScheduleService.convertActionSchedule(actionScheduleRepository.save(res));
    }

    @Transactional
    public ActionScheduleDto updateScheduleActionFilter(String name, FilterDescriptor filter) {
        if (Strings.isNullOrEmpty(name)) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.empty"), BadRequestType.MISSING_FIELD);
        }
        ActionSchedule res = actionScheduleRepository.findByName(name);
        if (res == null) {
            logger.error("Could not find ActionSchedule by name {}.", name);
            return null;
        }

        ActionExecutionTaskStatus actionExecutionTaskStatus = actionExecutionStatusRepository.getOne(res.getActionExecutionTaskStatusId());
        actionExecutionTaskStatus.setFilterDescriptor(filter);
        actionExecutionStatusRepository.save(actionExecutionTaskStatus);

        return ActionScheduleService.convertActionSchedule(actionScheduleRepository.save(res));
    }

    @Transactional
    public ActionScheduleDto deleteActionSchedule(String name) {
        ActionSchedule res = actionScheduleRepository.findByName(name);
        if (res == null) {
            logger.error("Could not find ActionSchedule by name {}.", name);
            return null;
        }
        if (res.isActive()) {
            actionScheduleService.stopScheduleAction(res, true);
        }
        actionScheduleRepository.deleteById(res.getId());
        return ActionScheduleService.convertActionSchedule(res);
    }
}
