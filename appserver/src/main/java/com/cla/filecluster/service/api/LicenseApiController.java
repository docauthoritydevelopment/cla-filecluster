package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.common.domain.dto.license.LicenseImportResultDto;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.license.LicenseAppService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static com.cla.common.domain.dto.security.Authority.Const.*;

/**
 * Created by uri on 27/06/2016.
 */
@RestController
@RequestMapping("/api/license")
public class LicenseApiController {

    @Autowired
    private LicenseAppService licenseAppService;

    @Autowired
    private MessageHandler messageHandler;

    private Logger logger = LoggerFactory.getLogger(LicenseApiController.class);

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN, TECH_SUPPORT, MNG_LICENSE})
    @RequestMapping(value = "/last",method = RequestMethod.GET)
    public DocAuthorityLicenseDto getLastAppliedLicense() {
        return licenseAppService.getLastAppliedLicense();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN, TECH_SUPPORT, MNG_LICENSE})
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public @ResponseBody
    LicenseImportResultDto importLicense(@RequestParam("file") MultipartFile file,
                                         @RequestParam(required = false, defaultValue = "false") boolean save) {
        if (file.isEmpty()) {
//            throw new BadHttpRequestException("Uploaded file is empty");
            logger.warn("License file {} is empty.", file.getName());
            return new LicenseImportResultDto(false, "License file is empty");
        }
        try {
            logger.info("import License File");
            byte[] bytes = file.getBytes();
            String licenseFileContents = new String(bytes);
            return licenseAppService.importLicense(licenseFileContents, save);
        } catch (Exception e) {
            logger.error("Bad license file",e);
            throw new RuntimeException(messageHandler.getMessage("license.file-invalid", Lists.newArrayList(e.getMessage())));
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN, TECH_SUPPORT, MNG_LICENSE})
    @RequestMapping(value = "/import/packed", method = RequestMethod.POST)
    public @ResponseBody LicenseImportResultDto importLicensePacked(@RequestParam("file") MultipartFile file,
                                                                    @RequestParam(required = false, defaultValue = "false") boolean save) {
        if (file.isEmpty()) {
            logger.warn("License file {} is empty.", file.getName());
            return new LicenseImportResultDto(false, messageHandler.getMessage("license.file-empty"));
        }
        try {
            logger.info("import License File");
            byte[] bytes = file.getBytes();
            String licenseFileContents = new String(bytes);
            return licenseAppService.importLicensePacked(licenseFileContents, save);
        } catch (Exception e) {
            logger.error("Bad license file",e);
            throw new RuntimeException(messageHandler.getMessage("license.file-invalid", Lists.newArrayList(e.getMessage())));
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN, TECH_SUPPORT, MNG_LICENSE})
    @RequestMapping(value = "/import/string", method = {RequestMethod.POST, RequestMethod.GET})
    public void importLicenseString(@RequestParam("license") String licenseString, @RequestParam("signature") String signature,
                                                      @RequestParam(required = false, defaultValue = "false") boolean save) {
        licenseAppService.importLicense(licenseString,signature, save);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN, TECH_SUPPORT, MNG_LICENSE})
    @RequestMapping(value = "/import/packed/string", method = {RequestMethod.POST, RequestMethod.GET})
    public LicenseImportResultDto importLicenseString(@RequestParam("license") String licenseString, @RequestParam(required = false, defaultValue = "false") boolean save) {
        return licenseAppService.importLicensePacked(licenseString, save);
    }

    @RequestMapping("/login/expired")
    public Boolean isLoginExpired() {
        return licenseAppService.isLoginExpired();
    }
}
