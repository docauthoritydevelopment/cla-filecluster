package com.cla.filecluster.service.export.exporters.active_scans;

import com.cla.common.domain.dto.crawler.PhaseDetailsDto;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.active_scans.mixin.PhaseDetailsDtoMixin;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.active_scans.JobListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
@Slf4j
@Component
public class JobListExporter extends PageCsvExcelExporter<PhaseDetailsDto> {

    @Autowired
    private JobManagerAppService jobManagerAppService;

    private final static String[] header = {NAME, STATUS, DETAILS, ELAPSED_TIME, START_TIME, PROGRESS, RATE, ERRORS};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.JOB_LIST);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(PhaseDetailsDto.class, PhaseDetailsDtoMixin.class);

    }

    @Override
    protected Page<PhaseDetailsDto> getData(Map<String, String> requestParams) {
        long runId = 0L;
        if (requestParams.containsKey("runId")) {
            String runIdStr = requestParams.get("runId");
            if (!Strings.isNullOrEmpty(runIdStr)) {
                runId = Long.parseLong(runIdStr);
            }
        }
        return new PageImpl<>(jobManagerAppService.listRunJobs(runId));
    }

    @Override
    protected Map<String, Object> addExtraFields(PhaseDetailsDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();

        result.put(NAME, base.getJobType().getName());
        result.put(STATUS, base.getJobState().getDisplayName());

        if (base.getPhaseStart() > 0) {
            if (base.getPhaseLastStopTime() != null) {
                long time = base.getPhaseLastStopTime() - base.getPhaseStart();
                result.put(ELAPSED_TIME, getElapsedTimeHoursMinutesFromMilliseconds(time));
            }
            if (base.getPhaseMaxMark() > 0 && base.getPhaseCounter() != null) {
                result.put(PROGRESS, base.getPhaseCounter() + "/" + base.getPhaseMaxMark());
            } else if (base.getPhaseCounter() != null) {
                result.put(PROGRESS, base.getPhaseCounter());
            }
        }
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    public static String getElapsedTimeHoursMinutesFromMilliseconds(long milliseconds) {
        String format = String.format("%%0%dd", 2);
        long elapsedTime = milliseconds / 1000;
        String seconds = String.format(format, elapsedTime % 60);
        String minutes = String.format(format, (elapsedTime % 3600) / 60);
        String hours = String.format(format, elapsedTime / 3600);
        return  hours + ":" + minutes + ":" + seconds;
    }
}
