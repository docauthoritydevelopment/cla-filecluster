package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.progress.ProgressService;
import com.cla.filecluster.service.util.ValidationUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

public class CsvToExcel extends ProgressedTransformer<String, Workbook> {

    public static CsvToExcel with(String progressId, ProgressService.Handler progress) {
        return new CsvToExcel(progressId, progress);
    }

    private CsvToExcel(String progressId, ProgressService.Handler progress) {
        super(progressId, progress);
    }

    @Override
    protected DataBuffer createDataBuffer(Workbook workbook) {
        try {
            DataBuffer result = new DefaultDataBufferFactory().allocateBuffer();
            workbook.write(result.asOutputStream());
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Error writing workbook to ByteBuffer", e);
        }
    }

    @Override
    public Flux<Workbook> apply(Flux<String> stringFlux) {
        return stringFlux.map(this::csvToExcel);
    }



    private Workbook csvToExcel(String csvString) {
        try (Reader stringReader = new StringReader(csvString);
             CsvListReader listReader = new CsvListReader(stringReader, CsvPreference.STANDARD_PREFERENCE)) {

            Workbook workbook = new SXSSFWorkbook();

            Sheet sheet = workbook.createSheet("data");

            String[] header = listReader.getHeader(true);

            if (header != null) {
                // write header with header format
                writeHeader(header, workbook, sheet);

                // write the rest of the rows with text format
                List<String> line;
                int currentRowNum = 1;
                while ((line = listReader.read()) != null) {
                    Row row = sheet.createRow(currentRowNum);
                    writeLine(line, row);
                    currentRowNum++;
                }
            }
            getProgress().set(100, "Excel Conversion - Done");
            return workbook;
        } catch (IOException e) {
            throw new RuntimeException(String.format("Error converting csv to excel on export: %s", getExportId()), e);
        }
    }

    private void writeLine(List<String> line, Row row) {
        for (int i = 0 ; i < line.size() ; i++) {
            row.createCell(i).setCellValue(line.get(i));
            String value = line.get(i);
            if (ValidationUtils.isNumeric(value)) {
                row.createCell(i, CellType.NUMERIC).setCellValue(Double.parseDouble(line.get(i)));
            } else {
                row.createCell(i).setCellValue(line.get(i));
            }
        }
    }

    private void writeHeader(String[] header, Workbook workbook, Sheet sheet) {
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        //headerFont.setFontHeightInPoints((short) 14);
        //headerFont.setColor(IndexedColors.LIGHT_BLUE.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < header.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(header[i]);
            cell.setCellStyle(headerCellStyle);
        }
    }


}
