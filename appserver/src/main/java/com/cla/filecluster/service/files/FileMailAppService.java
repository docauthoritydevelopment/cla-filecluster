package com.cla.filecluster.service.files;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.email.MailAddressDto;
import com.cla.common.domain.dto.email.MailAddressType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 2/3/2019
 */
@Service
public class FileMailAppService {

    private static final Logger logger = LoggerFactory.getLogger(FileMailAppService.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getSenderAddressFileCounts(DataSourceRequest dataSourceRequest) {
        return getFileCounts(dataSourceRequest, CatFileFieldType.SENDER_ADDRESS, MailAddressType.SENDER, false);
    }

    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getRecipientAddressFileCounts(DataSourceRequest dataSourceRequest) {
        return getFileCounts(dataSourceRequest, CatFileFieldType.RECIPIENTS_ADDRESSES, MailAddressType.RECIPIENT, false);
    }

    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getSenderDomainFileCounts(DataSourceRequest dataSourceRequest) {
        return getFileCounts(dataSourceRequest, CatFileFieldType.SENDER_DOMAIN, MailAddressType.SENDER, true);
    }

    public FacetPage<AggregationCountItemDTO<MailAddressDto>> getRecipientDomainFileCounts(DataSourceRequest dataSourceRequest) {
        return getFileCounts(dataSourceRequest, CatFileFieldType.RECIPIENTS_DOMAINS, MailAddressType.RECIPIENT, true);
    }

    private List<String> escapeValues(List<String> values) {
        if (values == null) return null;
        return values.stream().map(ClientUtils::escapeQueryChars).collect(Collectors.toList());
    }

    private FacetPage<AggregationCountItemDTO<MailAddressDto>> getFileCounts(
            DataSourceRequest dataSourceRequest, CatFileFieldType field, MailAddressType type, boolean isDomain) {
        logger.debug("Get Owner File Counts With File Filters");
        final long totalMailFieldFacetCount = fileCatAppService.getTotalFacetCount(dataSourceRequest, field);
        SolrFacetQueryResponse solrFacetQueryResponse = getFileCountsResponse(dataSourceRequest, field, null);
        logger.debug("Get Owner File Counts returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());

        Map<String, Long> resultsNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            SolrFacetQueryResponse solrFacetQueryRespNoFilter = getFileCountsResponse(dataSourceRequest, field,
                    field.inQuery(escapeValues(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse))));
            FacetField facetFieldNoFilter = solrFacetQueryRespNoFilter.getFirstResult();
            resultsNoFilter = facetFieldNoFilter.getValues().stream()
                    .collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount));
        }

        FacetPage<AggregationCountItemDTO<MailAddressDto>> facetPage = convertFacetFieldResultToMailAddressDto(
                solrFacetQueryResponse.getFirstResult(), resultsNoFilter, solrFacetQueryResponse.getPageRequest(),
                type, isDomain);
        facetPage.setTotalElements(totalMailFieldFacetCount);
        return facetPage;
    }

    private SolrFacetQueryResponse getFileCountsResponse(DataSourceRequest dataSourceRequest, CatFileFieldType field, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, field);
        solrFacetSpecification.setMinCount(1);
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    private FacetPage<AggregationCountItemDTO<MailAddressDto>> convertFacetFieldResultToMailAddressDto(
            FacetField facetField, Map<String, Long> resultsNoFilter, PageRequest pageRequest, MailAddressType type, boolean isDomain) {
        List<AggregationCountItemDTO<MailAddressDto>> content = new ArrayList<>();
        for (FacetField.Count count : facetField.getValues()) {
            MailAddressDto dto = new MailAddressDto();
            if (isDomain) {
                dto.setDomain(count.getName());
            } else {
                dto.setAddress(count.getName());
                if (count.getName() != null && count.getName().contains("@")) {
                    dto.setDomain(count.getName().substring(count.getName().indexOf("@") + 1));
                }
            }
            dto.setMailAddressType(type);
            AggregationCountItemDTO<MailAddressDto> item = new AggregationCountItemDTO<>(dto, count.getCount());
            content.add(item);
            Long countNoFilter = resultsNoFilter == null ? null : resultsNoFilter.get(count.getName());
            if (countNoFilter != null) {
                item.setCount2(countNoFilter);
            }
        }

        return new FacetPage<>(content, pageRequest);
    }
}
