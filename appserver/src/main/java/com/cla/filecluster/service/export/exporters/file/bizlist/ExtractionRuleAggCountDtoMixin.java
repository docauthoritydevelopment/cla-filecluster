package com.cla.filecluster.service.export.exporters.file.bizlist;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class ExtractionRuleAggCountDtoMixin {

    @JsonProperty(BizListHeaderFileds.NAME)
    private String item;

    @JsonProperty(BizListHeaderFileds.NUM_OF_FILTERED_FILES)
    private Integer count;
}
