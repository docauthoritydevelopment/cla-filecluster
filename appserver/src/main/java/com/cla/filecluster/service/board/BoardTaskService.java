package com.cla.filecluster.service.board;

import com.cla.common.domain.dto.board.BoardTaskDto;
import com.cla.common.domain.dto.board.BoardTaskState;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.board.BoardTask;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.board.BoardTaskRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class BoardTaskService {

    private static final Logger logger = LoggerFactory.getLogger(BoardTaskService.class);

    @Autowired
    private BoardTaskRepository boardTaskRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional(readOnly = true)
    public BoardTaskDto getById(Long id) {
        BoardTask board = boardTaskRepository.getOne(id);
        return convertToDto(board);
    }

    @Transactional(readOnly = true)
    public Page<BoardTaskDto> findByOwnerPageRequest(long ownerId, Pageable pageRequest) {
        Page<BoardTask> boardTasks = boardTaskRepository.findByOwnerPageRequest(ownerId, pageRequest);
        List<BoardTaskDto> boardTaskDtos = convertToDtos(boardTasks.getContent());
        return new PageImpl<>(boardTaskDtos, pageRequest, boardTasks.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<BoardTaskDto> findByAssigneePageRequest(long assignee, Pageable pageRequest) {
        Page<BoardTask> boardTasks = boardTaskRepository.findByAssigneePageRequest(assignee, pageRequest);
        List<BoardTaskDto> boardTaskDtos = convertToDtos(boardTasks.getContent());
        return new PageImpl<>(boardTaskDtos, pageRequest, boardTasks.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<BoardTaskDto> findByOwnerAndBoard(long boardId, long ownerId, Pageable pageRequest) {
        Page<BoardTask> boardTasks = boardTaskRepository.findByOwnerAndBoard(boardId, ownerId, pageRequest);
        List<BoardTaskDto> boardTaskDtos = convertToDtos(boardTasks.getContent());
        return new PageImpl<>(boardTaskDtos, pageRequest, boardTasks.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<BoardTaskDto> findByAssigneeAndBoard(long boardId, long assignee, Pageable pageRequest) {
        Page<BoardTask> boardTasks = boardTaskRepository.findByAssigneeAndBoard(boardId, assignee, pageRequest);
        List<BoardTaskDto> boardTaskDtos = convertToDtos(boardTasks.getContent());
        return new PageImpl<>(boardTaskDtos, pageRequest, boardTasks.getTotalElements());
    }


    @Transactional(readOnly = true)
    public Page<BoardTaskDto> findByBoard(long boardId, Pageable pageRequest) {
        Page<BoardTask> boardTasks = boardTaskRepository.findByBoard(boardId, pageRequest);
        List<BoardTaskDto> boardTaskDtos = convertToDtos(boardTasks.getContent());
        return new PageImpl<>(boardTaskDtos, pageRequest, boardTasks.getTotalElements());
    }

    @Transactional
    public BoardTaskDto createBoardTask(BoardTaskDto boardTaskDto) {
        BoardTask boardTask = new BoardTask();
        boardTask.setOwnerId(boardTaskDto.getOwnerId());
        boardTask.setBoardId(boardTaskDto.getBoardId());
        boardTask.setAssignee(boardTaskDto.getAssignee());
        boardTask.setBoardTaskState(boardTaskDto.getBoardTaskState());
        boardTask.setBoardEntityType(boardTaskDto.getBoardEntityType());
        boardTask.setBoardTaskType(boardTaskDto.getBoardTaskType());
        boardTask.setEntityId(boardTaskDto.getEntityId());
        boardTask.setDueDate(boardTaskDto.getDueDate());
        return convertToDto(boardTaskRepository.save(boardTask));
    }

    @Transactional
    public BoardTaskDto updateBoardTask(BoardTaskDto boardTaskDto) {
        BoardTask boardTask = getBoardTaskForUpdate(boardTaskDto.getId());
        boardTask.setAssignee(boardTaskDto.getAssignee());
        boardTask.setBoardTaskState(boardTaskDto.getBoardTaskState());
        boardTask.setBoardEntityType(boardTaskDto.getBoardEntityType());
        boardTask.setEntityId(boardTaskDto.getEntityId());
        boardTask.setDueDate(boardTaskDto.getDueDate());
        return convertToDto(boardTaskRepository.save(boardTask));
    }

    @Transactional
    public BoardTaskDto updateBoardTaskState(Long boardTaskId, BoardTaskState state) {
        BoardTask boardTask = getBoardTaskForUpdate(boardTaskId);
        boardTask.setBoardTaskState(state);
        return convertToDto(boardTaskRepository.save(boardTask));
    }

    @Transactional
    public BoardTaskDto updateBoardTaskAssignee(Long boardTaskId, long assignee) {
        BoardTask boardTask = getBoardTaskForUpdate(boardTaskId);
        boardTask.setAssignee(assignee);
        boardTask.setBoardTaskState(BoardTaskState.ASSIGNED);
        return convertToDto(boardTaskRepository.save(boardTask));
    }

    private BoardTask getBoardTaskForUpdate(Long boardTaskId) {
        BoardTask boardTask = boardTaskRepository.findById(boardTaskId).orElse(null);
        if (boardTask == null) {
            throw new BadRequestException(messageHandler.getMessage("board-task.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return boardTask;
    }

    @Transactional
    public void deleteByBoard(long boardId) {
        boardTaskRepository.deleteByBoard(boardId);
    }

    @Transactional
    public void createBulk(List<BoardTask> tasks) {
        boardTaskRepository.saveAll(tasks);
    }

    @Transactional
    public void deleteBoardTask(Long id) {
        boardTaskRepository.deleteById(id);
    }

    public static List<BoardTaskDto> convertToDtos(List<BoardTask> boardTasks) {
        List<BoardTaskDto> boardTaskDtos = new ArrayList<>();
        if (boardTasks == null) {
            return boardTaskDtos;
        }

        boardTasks.forEach(b -> boardTaskDtos.add(convertToDto(b)));
        return boardTaskDtos;
    }

    public static BoardTaskDto convertToDto(BoardTask boardTask) {
        if (boardTask == null) {
            return null;
        }

        BoardTaskDto boardTaskDto = new BoardTaskDto();
        boardTaskDto.setId(boardTask.getId());
        boardTaskDto.setBoardId(boardTask.getBoardId());
        boardTaskDto.setDateCreated(boardTask.getDateCreated());
        boardTaskDto.setDateModified(boardTask.getDateModified());
        boardTaskDto.setAssignee(boardTask.getAssignee());
        boardTaskDto.setBoardTaskState(boardTask.getBoardTaskState());
        boardTaskDto.setOwnerId(boardTask.getOwnerId());
        boardTaskDto.setBoardEntityType(boardTask.getBoardEntityType());
        boardTaskDto.setBoardTaskType(boardTask.getBoardTaskType());
        boardTaskDto.setEntityId(boardTask.getEntityId());
        boardTaskDto.setDueDate(boardTask.getDueDate());
        return boardTaskDto;
    }
}
