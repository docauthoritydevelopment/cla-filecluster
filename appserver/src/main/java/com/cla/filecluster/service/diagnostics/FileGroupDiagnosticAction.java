package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.FileGroupsFieldType;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.Range;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class FileGroupDiagnosticAction implements EntityDiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(FileGroupDiagnosticAction.class);
    private static final String GROUP_ID_PARAM = "FILE GROUP ID";

    @Autowired
    private EventBus eventBus;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private FileGroupService fileGroupService;

    @Value("${diagnostics.file-group.page-size:5000}")
    private int pageSize;

    @Value("${diagnostics.file-group.start-after-min:60}")
    private int startAfterMin;

    @Value("${diagnostics.file-group.event-limit:100}")
    private int cycleEventThreshold;

    @Value("${diagnostics.group-validation.file-count.enabled:true}")
    private boolean fileCountValidationEnabled;

    @Value("${diagnostics.group-validation.percentage:100}")
    private String percentageToTestConfig;

    @Value("${diagnostics.group-validation.pickRandomly:true}")
    private boolean partialTestingPickRandomly;

    private AtomicInteger currentEventCounter = new AtomicInteger();

    private Long lastEndRangeRun = null;

    public Logger getLogger() {
        return logger;
    }

    @Override
    public void runDiagnostic(Map<String, String> opData) {

        Long from = opData.containsKey(START_LIMIT) ? Long.parseLong(opData.get(START_LIMIT)) : null;

        Long to = opData.containsKey(END_LIMIT) ? Long.parseLong(opData.get(END_LIMIT)) : null;

        String idsStr = opData.get(SPECIFIC_IDS);

        float percentageTest = Float.parseFloat(opData.getOrDefault(PAGE_PERCENTAGE_TEST, percentageToTestConfig));

        if (currentEventCounter.get() >= cycleEventThreshold) {
            logger.debug("threshold passed for validating file groups - skip");
            return;
        }

        if (!fileCountValidationEnabled) {
            logger.debug("all validations are disabled. validating file groups - skip");
            return;
        }

        Query query = Query.create().setRows(pageSize)
                .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

        if (from != null && to != null) {
            logger.debug("validating file groups update date range from {} to {}", from, to);
            query.addFilterWithCriteria(Criteria.create(FileGroupsFieldType.UPDATE_DATE, SolrOperator.IN_RANGE, Range.create(from, to)));
            lastEndRangeRun = to;
        } else if (idsStr != null) {
            String[] split = idsStr.split(",");
            List<String> ids = Arrays.stream(split)
                    .collect(Collectors.toList());
            logger.debug("validating file groups for specific ids size {}", ids.size());
            query.addFilterWithCriteria(Criteria.create(FileGroupsFieldType.ID, SolrOperator.IN, ids));
        } // else no limitation run on entire collection

        String cursor = "*";
        boolean lastPage;
        int pageNum = 1;
        do {
            QueryResponse resp = fileGroupService.runQuery(query.setCursorMark(cursor).build());

            List<SolrFileGroupEntity> groups = resp.getBeans(SolrFileGroupEntity.class);
            cursor = resp.getNextCursorMark();

            int retrievedItemsCount = groups.size();
            if (groups != null && retrievedItemsCount > 0) {
                List<? extends SolrEntity> tempListForSampling = Lists.newArrayList(groups);
                tempListForSampling = getSolrEntitiesSample(tempListForSampling, percentageTest, partialTestingPickRandomly, pageSize);
                groups = (List<SolrFileGroupEntity>)tempListForSampling;

                List<String> userGroupIds = groups.stream()
                        .filter(g -> g.getType().equals(GroupType.USER_GROUP.toInt()))
                        .map(SolrFileGroupEntity::getId).collect(Collectors.toList());
                List<String> analysisGroupIds = groups.stream()
                        .filter(g -> g.getType().equals(GroupType.ANALYSIS_GROUP.toInt()))
                        .map(SolrFileGroupEntity::getId).collect(Collectors.toList());

                Map<String, Long> fileNumUserGroups = userGroupIds.isEmpty() ? new HashMap<>() :
                        groupsService.getFileNumForUserGroups(userGroupIds);
                Map<String, Long> fileNumAnalysisGroups = analysisGroupIds.isEmpty() ? new HashMap<>() :
                        groupsService.getFileNumForUserGroups(analysisGroupIds);

                for (SolrFileGroupEntity group : groups) {
                    validateFileGroup(group, fileNumAnalysisGroups, fileNumUserGroups);
                    if (currentEventCounter.get() >= cycleEventThreshold) {
                        logger.debug("threshold passed for validating file groups after id {} - skip rest", group.getId());
                        break;
                    }
                }
            }
            logger.debug("check diagnostics page number {} of size {} file groups (out of {})", pageNum, groups.size(), retrievedItemsCount);
            pageNum++;
            lastPage = resp.getResults().size() < pageSize;
        } while (!lastPage);

        logger.debug("done validating file groups update");
    }

    private void validateFileGroup(SolrFileGroupEntity group,
                                   Map<String, Long> fileNumAnalysisGroups,
                                   Map<String, Long> fileNumUserGroups) {
        try {
            List<Event> events = new ArrayList<>();

            if (fileCountValidationEnabled) {
                validFileCount(group, fileNumAnalysisGroups, fileNumUserGroups).ifPresent(events::add);
            }

            currentEventCounter.addAndGet(events.size());
            events.forEach(event -> eventBus.broadcast(Topics.DIAGNOSTICS, event));
        } catch (Exception e) {
            logger.error("problem validating file group {}", group.getId() , e);
        }
    }

    private Optional<Event> validFileCount(SolrFileGroupEntity group,
                                            Map<String, Long> fileNumAnalysisGroups, Map<String, Long> fileNumUserGroups) {
        Event.Builder eventBuilder = Event.builder().
                withEventType(EventType.FILE_GROUP_INVALID_COUNT)
                .withEntry(GROUP_ID_PARAM, group.getId());

        if (group.getDirty() == null || !group.getDirty()) {
            if (group.getType().equals(GroupType.USER_GROUP.toInt())) {
                Long count = fileNumUserGroups.get(group.getId());
                if ((count == null && !group.getDeleted() && group.getNumOfFiles() != null && group.getNumOfFiles() > 0) ||
                        (count != null && !count.equals(group.getNumOfFiles()))) {
                    eventBuilder.withEntry("TYPE", "USER_GROUP");
                    eventBuilder.withEntry("COUNT", count);
                    eventBuilder.withEntry("NUM_OF_FILES", group.getNumOfFiles());
                    return Optional.of(eventBuilder.build());
                }
            } else if (group.getType().equals(GroupType.ANALYSIS_GROUP.toInt())) {
                Long count = fileNumAnalysisGroups.get(group.getId());
                if ((count == null && !group.getDeleted() && group.getNumOfFiles() != null && group.getNumOfFiles() > 0) ||
                        (count != null && !count.equals(group.getNumOfFiles()))) {
                    eventBuilder.withEntry("TYPE", "ANALYSIS_GROUP");
                    eventBuilder.withEntry("COUNT", count);
                    eventBuilder.withEntry("NUM_OF_FILES", group.getNumOfFiles());
                    return Optional.of(eventBuilder.build());
                }
            }
        }

        return Optional.empty();
    }

    @Override
    public int getStartAfterMin() {
        return startAfterMin;
    }

    @Override
    public Long getLastEndRangeRun() {
        return lastEndRangeRun;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        currentEventCounter.set(0);
        return Lists.newArrayList(addDiagnosticActionTimeParts());
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.FILE_GROUP;
    }
}
