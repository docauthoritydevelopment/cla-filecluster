package com.cla.filecluster.service.export.serialize;

import com.cla.common.domain.dto.security.CompositeRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

import static com.cla.filecluster.service.export.exporters.file.CollectionResolverHelper.resolveCollectionValue;

public class FunctionalRolesSerializer extends JsonSerializer<Set<FunctionalRoleDto>> {
    @Override
    public void serialize(Set<FunctionalRoleDto> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(resolveCollectionValue(value,
                CompositeRoleDto::getName, FunctionalRoleDto::getImplicit));
    }
}
