package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.service.metadata.MetadataAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class ReTransformFileMetadata implements ScheduledOperation {

    @Autowired
    private MetadataAppService metadataAppService;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            metadataAppService.reTranslateFileMetadata();
        }
        return true;
    }

    @Override
    public void validate(Map<String, String> opData) {
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.RE_TRANSLATE_FILE_METADATA;
    }
}
