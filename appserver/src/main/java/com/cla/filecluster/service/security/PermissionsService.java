package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.security.*;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.security.FunctionalItem;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.security.access.AccessDecisionVoter.ACCESS_DENIED;
import static org.springframework.security.access.AccessDecisionVoter.ACCESS_GRANTED;

/**
 * Service for calculating permissions for currently logged in user.
 * The service calculates both global (origination in biz roles) permissions
 * and contextual (originating in functional roles per functional item) permissions.
 */
@Service
public class PermissionsService {

    @Autowired
    private UserService userService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FileGroupService groupRepo;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    /**
     * Given set of permissions, determine whether currently logged in user
     * has access to method/operation/resource that requires those permissions
     * with regard to the specified functional item
     *
     * @param requiredPermissions required permissions
     * @param item  functional item
     * @return access int (ACCESS_DENIED, ACCESS_GRANTED, ACCESS_ABSTAIN)
     */
    @Transactional(readOnly = true)
    public int getAccess(Set<String> requiredPermissions, FunctionalItem item){
        Set<String> userPermissions = getPermissions(getItemFuncRoles(item));
        return matchPermissions(requiredPermissions, userPermissions);
    }

    public int getAccess(Set<String> requiredPermissions, FunctionalItemDto item){
        Set<String> userPermissions = getPermissions(getItemFuncRoles(item));
        return matchPermissions(requiredPermissions, userPermissions);
    }

    /**
     * Given set of permissions, determine whether currently logged in user
     * has access to method/operation/resource that requires those permissions
     * with regard to the functional item specified by ID and type
     *
     * @param requiredPermissions required permissions
     * @param functionalItemId ID of the functional item
     * @param functionItemType concrete type of the functional item
     * @return access int (ACCESS_DENIED, ACCESS_GRANTED, ACCESS_ABSTAIN)
     */
    @Transactional(readOnly = true)
    public int getAccess(Set<String> requiredPermissions, Object functionalItemId, String functionItemType) {
        Set<String> userPermissions = getPermissions(functionalItemId, functionItemType);
        return matchPermissions(requiredPermissions, userPermissions);
    }

    /**
     * Determine what permissions currently logged in user
     * has for the functional item specified by ID and type
     *
     * @param functionalItemId ID of the functional item
     * @param functionItemType concrete type of the functional item
     *
     * TODO: support collection of IDs
     * @return set of permissions
     */
    private Set<String> getPermissions(Object functionalItemId, String functionItemType) {

        Object item = null;
        switch(functionItemType){
            case FunctionalItem.FILE:
                item = fileCatService.getFileObject((Long)functionalItemId);
                break;
            case FunctionalItem.FOLDER:
                item = docFolderService.getByIdWithAssociations((Long)functionalItemId);
                break;
            case FunctionalItem.GROUP:
                item = groupRepo.getById((String) functionalItemId);
                break;
            case FunctionalItem.DOC_TYPE:
                break;
            case FunctionalItem.TAG:
                break;
        }

        return getPermissions(getItemFuncRoles(item));
    }

    /**
     * Determine what permissions currently logged in user
     * has for the specified functional item
     *
     * @param itemFuncRoles functional roles of item (or DTO)
     * @return set of permissions
     */
    private Set<String> getPermissions(Set<FunctionalRoleDto> itemFuncRoles) {

        // get currently logged in user
        UserDto currentUser = getCurrentUser();
        if (currentUser == null){
            return Sets.newHashSet();
        }

        Set<BizRoleDto> userBizRoles = currentUser.getBizRoleDtos();
        Set<FunctionalRoleDto> userFuncRoles = currentUser.getFuncRoleDtos();

        // find functional roles that are common to the current user and current item
        Set<FunctionalRoleDto> intersection = Sets.intersection(userFuncRoles, itemFuncRoles);

        // get all the user's global and contextual roles into one set
        Set<SystemRoleDto> systemRoles = Sets.newHashSet();
        userBizRoles.forEach(bizRole -> systemRoles.addAll(bizRole.getTemplate().getSystemRoleDtos()));
        intersection.forEach(funcRole -> systemRoles.addAll(funcRole.getTemplate().getSystemRoleDtos()));

        return systemRoles.stream().map(SystemRoleDto::getName).collect(Collectors.toSet());
    }

    /**
     * Get functional role DTOs from FunctionalItem or FunctionalItemDto
     * @param item  FunctionalItem or FunctionalItemDto
     * @return set of FunctionalRoleDto
     */
    private Set<FunctionalRoleDto> getItemFuncRoles(Object item){
        if (item instanceof FunctionalItem) {
            return ((FunctionalItem)item).getFunctionalRoles().stream()
                    .map(FunctionalRoleService::convertFunctionalRole)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        } else if (item instanceof FunctionalItemDto){
            return ((FunctionalItemDto)item).getAssociatedFunctionalRoles().stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());
        } else if (item instanceof ClaFile) {
            ClaFile file = (ClaFile)item;
            List<AssociationEntity> associationEntities = AssociationsService.convertFromAssociationEntity(
                    file.getId(), file.getAssociations(), AssociationType.FILE);
            return associationEntities == null ? new HashSet<>() : associationEntities.stream()
                    .map(AssociationEntity::getFunctionalRoleId)
                    .filter(Objects::nonNull)
                    .map(id -> functionalRoleService.getFromCache(id))
                    .collect(Collectors.toSet());
        } else if (item instanceof FileGroup) {
            FileGroup group = (FileGroup) item;
            List<AssociationEntity> associationEntities = AssociationsService.convertFromAssociationEntity(
                    group.getId(), group.getAssociations(), AssociationType.GROUP);
            return associationEntities == null ? new HashSet<>() : associationEntities.stream()
                    .map(AssociationEntity::getFunctionalRoleId)
                    .filter(Objects::nonNull)
                    .map(id -> functionalRoleService.getFromCache(id))
                    .collect(Collectors.toSet());
        } else{
            throw new IllegalArgumentException("Unexpected parameter type " + item.getClass());
        }
    }


    /**
     * Match required permissions to existing ones.
     * Make distinction between mandatory permissions (marked with "+") and non-mandatory ones.
     * Mandatory permissions are required even if other permissions are present.
     * Abscence of any mandatory permission will lead to ACCESS_DENIED.
     *
     * @param requiredPermissions permissions required for access to an item
     * @param userPermissions permissions posessed by a user
     * @return ACCESS_GRANTED or ACCESS_DENIED;
     */
    int matchPermissions(Set<String> requiredPermissions, Set<String> userPermissions){
        boolean nonMandatoryExist = false;
        boolean nonMandatoryRequired = false;
        for (String rp : requiredPermissions) {
            boolean mandatory = rp.startsWith("+");
            String permToCheck = mandatory ? rp.substring(1) : rp;
            if (mandatory || !nonMandatoryExist) {
                boolean exists = userPermissions.contains(permToCheck);
                if (mandatory && !exists) {
                    return ACCESS_DENIED;
                }
                nonMandatoryExist = nonMandatoryExist || (exists && !mandatory);
                nonMandatoryRequired = nonMandatoryRequired || !mandatory;
            }
        }
        return (nonMandatoryExist || !nonMandatoryRequired) ? ACCESS_GRANTED : ACCESS_DENIED;
    }

    private UserDto getCurrentUser(){
        return userService.getCurrentUser();
    }
}
