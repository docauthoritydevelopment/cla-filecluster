package com.cla.filecluster.service.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.common.domain.dto.messages.FoldersListResultMessage;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MicrosoftConnectionDetailsDtoBase;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.connector.mediaconnector.microsoft.onedrive.OneDriveMediaConnector;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * SharePointMediaAppService
 * Created by uri on 07/08/2016.
 */
@Service
public class MicrosoftMediaAppService implements MediaSpecificAppService<OneDriveMediaConnector, SharePointConnectionDetailsDto> {

//    public static final String SHAREPOINT_CONNECTION_DETAILS_KEY = "sharepoint.connection.details";

    private final static Logger logger = LoggerFactory.getLogger(MicrosoftMediaAppService.class);

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Override
    public List<ServerResourceDto> listFoldersUnderPath(MediaConnectionDetailsDto connectionDetails, Optional<String> pathOpt, Long customerDataCenterId, boolean isTest) {
        logger.trace("List server folder {}", pathOpt);
        CompletableFuture<FoldersListResultMessage> listFolderFut = new CompletableFuture<>();
        String requestId = requestLockMap.addRequestToLockMap(listFolderFut);
        remoteMediaProcessingService.issueListFoldersRequest(requestId, pathOpt.orElse("/"), customerDataCenterId, connectionDetails, isTest);
        try {
            return listFolderFut.get(3, TimeUnit.MINUTES)
                    .getFoldersList();
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.error("Failed to obtain folder-list", e);
        }

        return null;
    }

//    public List<SharePointRoleAssignment> getListPermissions(long connectionId, String listId) {
//        logger.debug("get List permissions");
//        SharePointMediaConnector wrapper = createWrapperIfNeeded(connectionId);
//        try {
//            List<SharePointRoleAssignment> listPermissions = wrapper.getListPermissions(listId);
//            logger.debug("List has {} permissions", listPermissions.size());
//            return listPermissions;
//        } catch (ServiceException e) {
//            throw new RuntimeException("Failed to get list permissions", e);
//        }
//    }

    public TestResultDto testConnection(MicrosoftConnectionDetailsDtoBase sharePointConnectionDetailsDto, Long mediaCenterId) {
        logger.trace("Test SharePoint connection to {}", sharePointConnectionDetailsDto);
        CompletableFuture<FoldersListResultMessage> testConnFuture = new CompletableFuture<>();
        String requestId = requestLockMap.addRequestToLockMap(testConnFuture);
        sharePointConnectionDetailsDto.setUsername(BoxMediaAppService.BOX_CONNECTION_TEST_LABEL); // TODO Oren move & rename constant to a common location with common name.

        remoteMediaProcessingService.issueListFoldersRequest(requestId, null, mediaCenterId, sharePointConnectionDetailsDto, true);
        try {
            FoldersListResultMessage result = testConnFuture.get(3, TimeUnit.MINUTES);
            String err = result.getErrors() != null && result.getErrors().size() > 0 ? result.getErrors().get(0).getExceptionText() : null;
            return new TestResultDto(!result.hasErrors(), err);
        } catch (BadRequestException e) {
            return new TestResultDto(false, e.getMessage());
        } catch (Exception e) {
            logger.info("Test share point connection failed", e);
            return new TestResultDto(false, e.getMessage());
        }
    }
}