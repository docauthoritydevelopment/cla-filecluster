package com.cla.filecluster.service.report;
import com.cla.common.domain.dto.report.ChartQueryType;

// this interface is used for enrich the solr query result according to specific fields
public interface DashboardDataEnricher {

    String getEnricherType();

    default ChartQueryType getQueryType() {
        return ChartQueryType.DEFAULT;
    }

    default String convertSolrValueToId(String value) {
        return value;
    }
}