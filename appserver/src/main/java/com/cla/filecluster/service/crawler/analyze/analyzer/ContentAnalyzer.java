package com.cla.filecluster.service.crawler.analyze.analyzer;

import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.pv.SimilaritySimulationContext;

import java.util.Collection;

/**
 * Created by uri on 22-Mar-17.
 */
public interface ContentAnalyzer {

    Collection<Long> analyzeContent(Long taskId, ContentMetadataDto contentMetadata);

    Collection<Long> analyzeContent(Long taskId, ContentMetadataDto contentMetadataDto,
                                    SimilaritySimulationContext simulationContext);
}
