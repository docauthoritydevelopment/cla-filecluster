package com.cla.filecluster.service.report;

import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;

import java.util.Map;

public interface DashboardChartDtoBuilder {
    ChartType getType();

    DashboardChartDataDto buildChartDataDto(Map<String, String> params);

    String getAuditMsg(Map<String, String> params);
}
