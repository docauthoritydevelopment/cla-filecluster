package com.cla.filecluster.service.export.exporters.active_scans;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface ActiveScansHeaderFields {

    String PATH = "PATH";
    String PROCESSED_FILE_TYPES = "processed files by type";
    String STATUS = "Status";
    String PROGRESS = "Progress";
    String PHASE = "Phase";
    String PROCESSED_FILES = "Processed files";
    String START_DATE = "Last scanned start time";
    String ELAPSED_TIME = "Last scanned elapsed time";
    String MEDIA_TYPE = "MEDIA TYPE";
    String NICK_NAME = "NICK NAME";
    String MAPPED_FILE_TYPES = "MAPPED FILE TYPES";
    String INGESTED_FILE_TYPES = "INGESTED FILE TYPES";
    String DATA_CENTER = "CUSTOMER DATA CENTER";
    String RESCAN = "RESCAN";
    String REINGEST = "REINGEST";
    String REINGEST_TIME = "REINGEST TIME IN SECONDS";
    String STORE_LOCATION = "STORE LOCATION";
    String STORE_PURPOSE = "STORE PURPOSE";
    String STORE_SECURITY = "STORE SECURITY";
    String DESCRIPTION = "DESCRIPTION";
    String MEDIA_CONNECTION = "MEDIA CONNECTION NAME";
    String SCAN_TASK_DEPTH = "SCAN_TASK_DEPTH";
    String SCHEDULE_GROUP = "SCHEDULE GROUP NAME";
}
