package com.cla.filecluster.service.files;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.bizlist.ExtractionSummaryDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.*;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.cla.common.domain.dto.report.CounterReportDto;
import com.cla.common.domain.dto.report.MultiValueListDto;
import com.cla.common.domain.dto.report.MultiValueListType;
import com.cla.common.domain.dto.report.ReportItems;
import com.cla.common.domain.dto.workflow.WorkflowFileData;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.specification.ClaFileByFolderSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.DeepPageRequest;
import com.cla.filecluster.service.TagRelatedBaseService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.extractor.pattern.ClaFileSearchPatternService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.filecluster.service.files.ingest.ModelIngestUtils;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.label.LabelingService;
import com.cla.filecluster.service.security.DomainAccessHandler;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class FilesApplicationService extends TagRelatedBaseService {

    private static final Logger logger = LoggerFactory.getLogger(FilesApplicationService.class);

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private SolrGrpHelperRepository solrGrpHelperRepository;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private LabelingService labelingService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private DomainAccessHandler domainAccessHandler;

    @Autowired
    private BizListService bizListService;

    @Autowired
    private EventBus eventBus;

    @Value("${bizlist.extraction.samples.max:10}")
    private int maxSamplesPerBizList;

    @Value("${file-application-service.file-grouping-operations.commit:SOFT_NOWAIT}")
    private SolrCommitType fileGroupingOperationsCommitType;

    @Value("${file.recipient-display-limit:50}")
    private int recipientDisplayLimit;


    @Value("${regulations.counts-field.separator:.}")
    private String countFieldSeparator;

    @Value("${regulations.match.threshold.multi:10}")
    private int matchCountThresholdMulti;

    @Value("${file.email.parse-x500-addresses:true}")
    private boolean parseX500Addresses;

    @Value("${file.email.x500.extract-non-domain-addresses:true}")
    private boolean extractNonDomainX500CN;

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO> findFileTypesCount(DataSourceRequest dataSourceRequest) {
        logger.debug("Facet on file types");
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.TYPE);
        solrFacetSpecification.setMinCount(1);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrFacetSpecification, pageRequest);
        logger.debug("list file facets by Solr field type returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        return convertFacetFieldResultToFileTypeDto(solrFacetQueryResponse, pageRequest);
    }

    private FacetPage<AggregationCountItemDTO> convertFacetFieldResultToFileTypeDto(SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        List<AggregationCountItemDTO> content = new ArrayList<>();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        long pageAggCount = 0;
        for (FacetField.Count count : facetField.getValues()) {
            FileType fileType = convertIdToFileType(count.getName());
            if (fileType == null) {
                throw new RuntimeException(messageHandler.getMessage("file.type.solr-corrupted", Lists.newArrayList(count.getName())));
            }
            AggregationCountItemDTO<FileType> item = new AggregationCountItemDTO<>(fileType, (int) count.getCount());
            content.add(item);
            pageAggCount += count.getCount();
        }
        return new FacetPage<>(content, pageRequest, pageAggCount, solrFacetQueryResponse.getNumFound());

    }

    private FileType convertIdToFileType(String id) {
        Integer value = Integer.valueOf(id);
        return FileType.valueOf(value);
    }


    private long getTotalWithoutDuplicationsFacetCount(DataSourceRequest dataSourceRequest, String... additionalFilters) {
        SolrFacetQueryJson solrFacetQueryJson = new SolrFacetQueryJson(CatFileFieldType.CONTENT_ID.getSolrName());
        solrFacetQueryJson.setType(FacetType.SIMPLE);
        solrFacetQueryJson.setField(CatFileFieldType.CONTENT_ID);
        solrFacetQueryJson.setFunc(FacetFunction.HLL);
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = fileCatService.runJsonFacetQuery(dataSourceRequest, Lists.newArrayList(solrFacetQueryJson), additionalFilters);
        return Long.parseLong(solrFacetQueryJsonResponse.getRawResponse().get(CatFileFieldType.CONTENT_ID.getSolrName()).toString());
    }

    @Transactional(readOnly = true)
    public PageImpl<ClaFileDto> listFilesDeNormalized(@NotNull DataSourceRequest dataSourceRequest) {
        logger.debug("list Files DeNormalized (Solr)");
        PageRequest pageRequest;
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
        if (!Strings.isNullOrEmpty(dataSourceRequest.getSolrPagingCursor())) {
            solrSpecification.setSortField(CatFileFieldType.ID.getSolrName()); // must be unique field
            pageRequest = new DeepPageRequest(dataSourceRequest.getPageSize(), dataSourceRequest.getSolrPagingCursor());
        } else {
            solrSpecification.setSortField(CatFileFieldType.SORT_NAME.getSolrName());
            pageRequest = getPageRequest(dataSourceRequest);
        }
        try {
            logger.debug("List files DeNormalized using Solr specification {} and pageRequest {}", solrSpecification, pageRequest);
            long start = System.currentTimeMillis();
            //TODO: vladi falcon : optimize this. Need to figure out what to do with selected item ID
            SolrQueryResponse solrQueryResponse = fileCatService.runQuery(solrSpecification, pageRequest, dataSourceRequest.getSelectedItemId());
            if (!Strings.isNullOrEmpty(dataSourceRequest.getSolrPagingCursor())) {
                String nextCursorMark = solrQueryResponse.getCursorMark();
                dataSourceRequest.setSolrPagingCursor(nextCursorMark);
            }
            long afterQuery = System.currentTimeMillis();
            long duration = afterQuery - start;
            logger.debug("list Files DeNormalized returned {} NumFound in {}ms", solrQueryResponse.getQueryResponse().getResults().getNumFound(), duration);

            List<SolrFileEntity> beans = solrQueryResponse.getQueryResponse().getBeans(SolrFileEntity.class);
            long totalNumber = solrQueryResponse.getQueryResponse().getResults().getNumFound();
            if (dataSourceRequest.isCollapseDuplicates()) {
                totalNumber = getTotalWithoutDuplicationsFacetCount(dataSourceRequest);
            }
            PageImpl<ClaFileDto> results = convertQueryResultsToClaFileDto(beans, solrQueryResponse.getPageRequest(), dataSourceRequest.getWorkflowIdentifier(), totalNumber, false);
            long afterEnrichment = System.currentTimeMillis();
            logger.debug("list Files DeNormalized enrichment took {}ms. Total duration is: {}ms", afterEnrichment - afterQuery, afterEnrichment - start);
            return results;
        } catch (RuntimeException e) {
            String message = e.getMessage();
            if (message != null && message.contains("org.apache.solr.search.SyntaxError: Cannot parse 'content:")) {
                // Probably user search text issue. Don't report error.
                logger.info("Exception during fileCatService.runQuery. {}", message, e);
            } else if (e instanceof BadRequestException) {
                logger.warn("BadRequestException during fileCatService.runQuery. dataSourceRequest={} {}",
                        dataSourceRequest, e.getSuppressed(), e);
            } else {
                logger.error("Exception during fileCatService.runQuery. {}", e.getSuppressed(), e);
            }
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public MultiValueListDto countFilesComparison(DataSourceRequest dataSourceRequest) {
        logger.debug("Create count files comparison (filter/sub filter)");
        List<CounterReportDto> data = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(0, 1);
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
        SolrSpecification subsetSolrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest.getSubsetFilter());

        try {
            SolrQueryResponse solrQueryResponse = fileCatService.runQuery(solrSpecification, pageRequest);
            QueryResponse queryResponse = solrQueryResponse.getQueryResponse();
            SolrQueryResponse subsetQueryResponse = fileCatService.runQuery(subsetSolrSpecification, pageRequest);
            data.add(new CounterReportDto("filter", queryResponse.getResults().getNumFound()));
            data.add(new CounterReportDto("subsetFilter", subsetQueryResponse.getQueryResponse().getResults().getNumFound()));
            return new MultiValueListDto(ReportItems.FILE_COUNT, data, MultiValueListType.HISTOGRAM);
        } catch (RuntimeException e) {
            logger.error("Exception during fileCatService.runQuery. {}", e.getSuppressed(), e);
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Long countFilesDeNormalized(DataSourceRequest dataSourceRequest) {
        logger.debug("count Files DeNormalized (Solr)");
        PageRequest pageRequest = PageRequest.of(0, 1);
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
        SolrQueryResponse solrQueryResponse;
        try {
            solrQueryResponse = fileCatService.runQuery(solrSpecification, pageRequest);
            QueryResponse queryResponse = solrQueryResponse.getQueryResponse();
            logger.debug("Count Files DeNormalized returned {} NumFound", queryResponse.getResults().getNumFound());
        } catch (RuntimeException e) {
            logger.error("Exception during fileCatService.runQuery. {}", e.getSuppressed(), e);
            throw e;
        }
        return solrQueryResponse.getQueryResponse().getResults().getNumFound();
    }

    @Transactional(readOnly = true)
    public List<ClaFileDto> enrichFiles(List<SolrFileEntity> results, String workflowIdentifier, boolean includeDocFolders) {

        if (results == null || results.isEmpty()) {
            return new ArrayList<>();
        }

        //Fetch all group ids from the databasepatternsSet  = new HashSet<Integer>();

        Set<Long> folderIds = results.stream()
                .map(SolrFileEntity::getFolderId)
                .collect(Collectors.toSet());

        Set<Integer> patternsSet = new HashSet<Integer>();
        results.stream().map(SolrFileEntity::getMatchedPatternsCounts).forEach((List<String> patternStrs) -> {
            if (patternStrs != null) {
                patternStrs.forEach((patternStr) -> {
                    if (patternStr != null && patternStr.indexOf(countFieldSeparator) > 0) {
                        patternsSet.add(Integer.parseInt(patternStr.substring(0, patternStr.indexOf(countFieldSeparator))));
                    }
                });
            }
        });

        Map<Integer, TextSearchPattern> searchPatternMap = claFileSearchPatternService.getSearchPatternByIds(patternsSet).stream()
                .distinct()
                .collect(Collectors.toMap(TextSearchPattern::getId, r -> r));


        Set<Long> rootFolderIds = results.stream()
                .map(SolrFileEntity::getRootFolderId)
                .distinct()
                .collect(Collectors.toSet());

        Map<Long, RootFolder> rfs = docStoreService.getRootFolders(rootFolderIds).stream()
                .distinct()
                .collect(Collectors.toMap(RootFolder::getId, r -> r));

        Set<String> userGroupIds = results.stream()
                .map(SolrFileEntity::getUserGroupId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        Map<String, FileGroup> groups = groupsService.findByIds(userGroupIds).stream()
                .collect(Collectors.toMap(FileGroup::getId, Function.identity()));

        if (logger.isInfoEnabled() && userGroupIds.size() > groups.size()) {
            logger.info("Got partial results from groups repo: {} requested {} returned", userGroupIds.size(), groups.size());
        }

        Set<Long> contentIds = results.stream()
                .map(FileCatService::getContentId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
        Map<Long, ContentMetadataDto> contents =
                SolrContentMetadataRepository.fromEntityToDto(contentMetadataService.findByIds(contentIds)).stream()
                        .collect(Collectors.toMap(ContentMetadataDto::getContentId, Function.identity()));

        if (logger.isInfoEnabled() && contentIds.size() > contents.size()) {
            logger.info("Got partial results from repo: {} requested {} returned", contentIds.size(), contents.size());
        }

        List<DocFolder> folders = docFolderService.findByIds(folderIds);

        Map<Long, DocFolder> foldersById = new HashMap<>();
        if (includeDocFolders) {
            foldersById = folders.stream()
                    .collect(Collectors.toMap(DocFolder::getId, r -> r));
        }

        AssociationRelatedData foldersAssociations = associationsService.getAssociationRelatedData(folders);

        AssociationRelatedData groupsAssociations = associationsService.getAssociationRelatedDataGroups(groups.values());

        List<AssociableEntityDto> entities = new ArrayList<>();
        if (foldersAssociations.getEntities() != null) {
            entities.addAll(foldersAssociations.getEntities());
        }
        if (groupsAssociations.getEntities() != null) {
            entities.addAll(groupsAssociations.getEntities());
        }

        //Convert database results to a map (we need that to keep consistent order in results)
        List<ClaFileDto> content = new ArrayList<>(results.size());
        logger.debug("Convert result for {} elements to ClaFileDto. (includeDocFolders={})", results.size(), includeDocFolders);
        Set<String> simpleItemIdsToExtract = new HashSet<>();
        for (SolrFileEntity result : results) {

            ClaFileDto claFileDto;
            try {

                Collection<AssociationEntity> userGroupAssociations = null;
                Collection<AssociationEntity> userGroupDocTypeAssociations = null;
                if (!Strings.isNullOrEmpty(result.getUserGroupId()) && groups.get(result.getUserGroupId()) != null) {
                    userGroupAssociations = groupsAssociations.getPerGroupAssociations().get(result.getUserGroupId());
                    userGroupDocTypeAssociations = AssociationsService.getDocTypeAssociation(userGroupAssociations);
                }

                Collection<AssociationEntity> folderAssociations = foldersAssociations.getPerEntityAssociations().get(result.getFolderId());
                Collection<AssociationEntity> folderDocTypeAssociations = AssociationsService.getDocTypeAssociation(folderAssociations);

                claFileDto = filesDataPersistenceService
                        .solrFileEntityToDto(result, includeDocFolders, true, true, true,
                                foldersAssociations.getScopes(), entities,
                                contents.get(FileCatService.getContentId(result)),
                                userGroupAssociations, userGroupDocTypeAssociations, folderAssociations,
                                folderDocTypeAssociations,
                                groups.get(result.getUserGroupId()),
                                foldersById.get(result.getFolderId()));

                refineAssociations(claFileDto, null);

            } catch (Exception e) {
                logger.error("Could not convert model DTO for file with ID {} and name {}.", result.getId(), result.getFullName(), e);
                continue;
            }

            if (!Strings.isNullOrEmpty(workflowIdentifier)) {
                claFileDto.setWorkflowFileData(extractWorkflowData(result, workflowIdentifier));
            }

            //Add extraction data
            List<SimpleBizListItemDto> sampleItems = addExtractionData(claFileDto, result.getExtractedEntitiesIds(), maxSamplesPerBizList);

            sampleItems.forEach(f -> simpleItemIdsToExtract.add(f.getId()));

            RootFolder rootFolder = rfs.get(claFileDto.getRootFolderId());
            if (rootFolder != null) {
                claFileDto.setRootFolderPath(rootFolder.getPath());
                claFileDto.setRootFolderRealPath(rootFolder.getRealPath());
                claFileDto.setMediaType(rootFolder.getMediaType());
                claFileDto.setMailboxGroup(rootFolder.getMailboxGroup());
            }

            if (result.getMatchedPatternsCounts() != null) {
                Set<SearchPatternCountDto> searchPatternCountsDtos = new HashSet<>();
                result.getMatchedPatternsCounts().forEach((String patternStr) -> {
                    if (patternStr != null) {
                        int delimPosition = patternStr.indexOf(countFieldSeparator);
                        if (delimPosition > 0) {
                            TextSearchPattern tsp = searchPatternMap.get(Integer.parseInt(patternStr.substring(0, delimPosition)));
                            if (tsp != null && tsp.isActive() && (tsp.getSubCategories() != null) && (tsp.getSubCategories().size() > 0) &&
                                    tsp.getSubCategories().iterator().next().isActive()) {
                                searchPatternCountsDtos.add(ClaFileSearchPatternService.convertTextSearchPatternToSearchPatternCountDto(tsp,
                                        Integer.parseInt(patternStr.substring(delimPosition + 1)), matchCountThresholdMulti));
                            }
                        }
                    }
                });
                claFileDto.setSearchPatternsCounting(searchPatternCountsDtos);
            }


            fixEmailAddressForFileDto(claFileDto);
            claFileDto.setDaLabelDtos(labelingService.getByIds(claFileDto.getDaLabels()));
            content.add(claFileDto);
        }

        //Enrich extraction data
        logger.trace("Enrich extraction data");
        Map<String, SimpleBizListItem> bizListItemMap = bizListItemService.getSimpleBizListItemsAsMap(simpleItemIdsToExtract);

        enrichClaFilesWithBizListItemsNameId(content, bizListItemMap);

        logger.debug("Done convert result for {} elements to ClaFileDto. (includeDocFolders={})", results.size(), includeDocFolders);

        return content;
    }

    private void fixEmailAddressForFileDto(ClaFileDto claFileDto) {
        if (claFileDto.getItemType() != null && claFileDto.getItemType().isEmailRelated()) {
            EmailAddress sender = ModelIngestUtils.getEmailObject(claFileDto.getSenderFullAddress());
            List<EmailAddress> recepients = ModelIngestUtils.getEmailObjects(claFileDto.getRecipientsFullAddresses());
            claFileDto.fixEmailDataForDtoDisplay(recipientDisplayLimit, sender, recepients, parseX500Addresses, extractNonDomainX500CN);
        }
    }

    private PageImpl<ClaFileDto> convertQueryResultsToClaFileDto(List<SolrFileEntity> results, PageRequest pageRequest, String workflowIdentifier, long total, boolean includeDocFolders) {

        if (results == null || results.isEmpty()) {
            return new PageImpl<>(
                    new ArrayList<ClaFileDto>(1),
                    pageRequest == null ? PageRequest.of(0, 1) : pageRequest,
                    total);
        }

        return new PageImpl<>(enrichFiles(results, workflowIdentifier, includeDocFolders), pageRequest, total);
    }

    private WorkflowFileData extractWorkflowData(SolrFileEntity result, String workflowIdentifier) {
        Collection<String> fieldValues = result.getWorkflowData();
        if (fieldValues != null) {
            String workflowIdentifierPrefix = workflowIdentifier + FileCatUtils.CATEGORY_SEPARATOR;
            Optional<String> wfDataOptional = fieldValues.stream().filter(wfData -> wfData.startsWith(workflowIdentifierPrefix)).findFirst();
            if (wfDataOptional.isPresent()) {
                return AssociationIdUtils.extractWorkflowFileDataDto(wfDataOptional.get());
            }
        }

        // Case no such match return null, should not happen!!!
        return null;
    }


    private void enrichClaFilesWithBizListItemsNameId(List<ClaFileDto> content, Map<String, SimpleBizListItem> bizListItemMap) {
        for (ClaFileDto claFileDto : content) {
            List<ExtractionSummaryDto> bizListExtractionSummary = claFileDto.getBizListExtractionSummary();
            if (bizListExtractionSummary != null && bizListExtractionSummary.size() > 0) {
                for (ExtractionSummaryDto extractionSummaryDto : bizListExtractionSummary) {
                    for (SimpleBizListItemDto simpleBizListItemDto : extractionSummaryDto.getSampleExtractions()) {
                        SimpleBizListItem simpleBizListItem = bizListItemMap.get(simpleBizListItemDto.getId());
                        if (simpleBizListItem == null) {
                            //Couldn't find it in the list from MySql
                            if (simpleBizListItemDto.getId().startsWith(FileDtoFieldConverter.AGG_RULE_PREFIX)) {
                                simpleBizListItemDto.setName("Aggregated Rule");
                                simpleBizListItemDto.setAggregated(true);
                            } else if (simpleBizListItemDto.isAggregated()) {
                                simpleBizListItemDto.setName("Aggregated Rule");
                            } else {
                                logger.error("BizListItem {} exists in Solr but not in SQL ({}) - unable to enrich it", simpleBizListItemDto.getId(), bizListItemMap.size());
                            }
                            continue;
                        }
                        simpleBizListItemDto.setName(simpleBizListItem.getName());
                        simpleBizListItemDto.setBusinessId(simpleBizListItem.getEntityId());

                        BizList bizList = bizListService.findById(simpleBizListItem.getBizListId());
                        simpleBizListItemDto.setBizListName(bizList != null ? bizList.getName() : null);
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    private List<SimpleBizListItemDto> addExtractionData(ClaFileDto claFileDto, Collection<String> extractedEntitiesIds, Integer maxSamplesPerBizList) {
        List<SimpleBizListItemDto> sampleItems = new ArrayList<>();
        if (extractedEntitiesIds != null && extractedEntitiesIds.size() > 0) {
            //Populate extracted entities in claFIleDto
            List<SimpleBizListItemDto> simpleBizListItemDtos = solrSpecificationFactory.extractBizListItemsFromSolrIds(extractedEntitiesIds);
            Map<Long, ExtractionSummaryDto> extractionSummaryDtos = new HashMap<>();
            for (SimpleBizListItemDto simpleBizListItemDto : simpleBizListItemDtos) {

                if (simpleBizListItemDto.getBizListName() == null) {
                    BizList bizList = bizListService.findById(simpleBizListItemDto.getBizListId());
                    simpleBizListItemDto.setBizListName(bizList != null ? bizList.getName() : null);
                }

                ExtractionSummaryDto extractionSummaryDto = extractionSummaryDtos.get(simpleBizListItemDto.getBizListId());
                if (extractionSummaryDto == null) {
                    extractionSummaryDto = new ExtractionSummaryDto();
                    extractionSummaryDto.setBizListId(simpleBizListItemDto.getBizListId());
                    extractionSummaryDto.setBizListName(simpleBizListItemDto.getBizListName());
                    extractionSummaryDto.setBizListItemType(simpleBizListItemDto.getType());
                    extractionSummaryDto.setSampleExtractions(Lists.newArrayList(simpleBizListItemDto));
                    extractionSummaryDtos.put(simpleBizListItemDto.getBizListId(), extractionSummaryDto);
                } else if (maxSamplesPerBizList == null || extractionSummaryDto.getCount() <= maxSamplesPerBizList) {
                    extractionSummaryDto.getSampleExtractions().add(simpleBizListItemDto);
                }

                sampleItems.add(simpleBizListItemDto);
                extractionSummaryDto.setCount(extractionSummaryDto.getCount() + 1);
            }
            claFileDto.setBizListExtractionSummary(new ArrayList<>(extractionSummaryDtos.values()));
        }
        return sampleItems;
    }

    @Transactional(readOnly = true)
    public Page<ClaFileDto> listFilesByGroup(DataSourceRequest dataSourceRequest, String groupId, boolean withOrder) {
        PageRequest pageRequest = getPageRequest(dataSourceRequest);
        //Specification<ClaFile> filterSpecification = new ClaFileByUserGroupSpecification(dataSourceRequest.getFilter(), groupId);
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, groupId))
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        solrSpecificationFactory.addFilterDeletedFiles(query);
        if (withOrder) {
            query.addSort(CatFileFieldType.BASE_NAME, Sort.Direction.ASC)
                    .addSort(CatFileFieldType.FULLPATH, Sort.Direction.ASC)
                    .addSort(CatFileFieldType.FILE_ID, Sort.Direction.ASC);
        }
        List<SolrFileEntity> entities = fileCatService.find(query.build());
        return convertQueryResultsToClaFileDto(entities, pageRequest, dataSourceRequest.getWorkflowIdentifier(), entities == null ? 0 : entities.size(), true);
    }

    @Transactional(readOnly = true)
    public Page<ClaFileDto> listFilesByFolder(long folderId, DataSourceRequest dataSourceRequest) {
        ClaFileByFolderSpecification filterSpecification = new ClaFileByFolderSpecification(dataSourceRequest.getFilter(), folderId);
        return listFilesByFolder(dataSourceRequest, filterSpecification);
    }

    @Transactional(readOnly = true)
    public Page<ClaFileDto> listFilesByFolderRecursive(long docFolderId, DataSourceRequest dataSourceRequest) {
        DocFolder docFolder = docFolderService.findById(docFolderId);
        PageRequest pageRequest = getByFolderPageRequest(dataSourceRequest);
        List<SolrFileEntity> entities = fileCatService.listFilesByFolderRecursive(docFolder,
                pageRequest,
                dataSourceRequest.getFilter(),
                dataSourceRequest.getMaxDepth());


        return convertQueryResultsToClaFileDto(entities, pageRequest, dataSourceRequest.getWorkflowIdentifier(), entities == null ? 0 : entities.size(), true);
    }

    private Page<ClaFileDto> listFilesByFolder(DataSourceRequest dataSourceRequest, ClaFileByFolderSpecification filterSpecification) {
        PageRequest pageRequest = getByFolderPageRequest(dataSourceRequest);
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FOLDER_ID, SolrOperator.EQ, filterSpecification.getFolderId()))
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        solrSpecificationFactory.addFilterDeletedFiles(query);
        List<SolrFileEntity> entities = fileCatService.find(query.build());
        return convertQueryResultsToClaFileDto(entities, pageRequest, dataSourceRequest.getWorkflowIdentifier(), entities == null ? 0 : entities.size(), true);
    }

    private PageRequest getPageRequest(DataSourceRequest dataSourceRequest) {
        if (dataSourceRequest.getPageSize() == 0) {
            return null;
        }
        Sort sort;
        if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
            //Order the records according to default order TODO - field names should come from Hibernate/JPA and not hardcoded
            Sort.Order baseNameOrder = new Sort.Order(Sort.Direction.ASC, "baseName");
            Sort.Order fileNameOrder = new Sort.Order(Sort.Direction.ASC, "fullPath");
            Sort.Order idOrder = new Sort.Order(Sort.Direction.ASC, "id");
            sort = Sort.by(baseNameOrder, fileNameOrder, idOrder);
        } else {
            sort = DataSourceRequest.convertSort(dataSourceRequest, FileDtoFieldConverter::convertDtoField);
        }
        return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(), sort);
    }

    private PageRequest getByFolderPageRequest(DataSourceRequest dataSourceRequest) {
        Sort sort;
        if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
            //Order the records according to default order TODO - field names should come from Hibernate/JPA and not hardcoded
            //Sort.Order docFolderDepthOrder = new Sort.Order(Sort.Direction.ASC, "docFolder.depthFromRoot");
            Sort.Order fileNameOrder = new Sort.Order(Sort.Direction.ASC, "fullPath");
            sort = Sort.by(fileNameOrder);
        } else {
            sort = DataSourceRequest.convertSort(dataSourceRequest, FileDtoFieldConverter::convertDtoField);
        }
        return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(), sort);
    }

    public Page<ClaFile> findAllFull(PageRequest pageRequest) {
        return fileCatService.findAllFull(pageRequest);
    }

    @Transactional(readOnly = true)
    public List<AggregationCountItemDTO<String>> createAccessHistogram(DataSourceRequest dataSourceRequest) {
        CatFileFieldType solrField = CatFileFieldType.LAST_MODIFIED;
        logger.debug("Create File Access date histogram");

        List<String> queryValues = solrSpecificationFactory.createTimeHistogramQueryValues();
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest,
                CatFileFieldType.LAST_MODIFIED, queryValues);

        solrSpecification.setMinCount(1);
        SolrQueryResponse solrQueryResponse = fileCatService.runQuery(solrSpecification, null);
        Map<String, Integer> facetQuery = solrQueryResponse.getQueryResponse().getFacetQuery();
        logger.debug("list file facets by Solr field {} returned {} results", solrField, facetQuery.size());
        List<AggregationCountItemDTO<String>> result = new ArrayList<>();
        for (Map.Entry<String, Integer> counter : facetQuery.entrySet()) {
            String label = counter.getKey();
            if (counter.getValue() > 0 || !solrSpecificationFactory.isTimeHistogramValuesToDrop(label)) {
                result.add(new AggregationCountItemDTO<>(label, counter.getValue()));
            }
        }
        return result;
    }

    @Transactional(readOnly = true)
    public ClaFileDetailsDto findFileDetailsById(long fileId) {
        SolrFileEntity entity = fileCatService.getFileEntity(fileId);
        if (entity == null) {
            throw new BadParameterException(messageHandler.getMessage("file-action.get.not-found"), BadRequestType.ITEM_NOT_FOUND);
        }
        final ClaFile claFile = fileCatService.getFromEntity(entity);
        ClaFileDto claFileDto = FileCatUtils.convertClaFile(claFile);
        ClaFileDetailsDto claFileDetailsDto = new ClaFileDetailsDto();
        claFileDetailsDto.setFileDto(claFileDto);

        RootFolder rootFolder = docStoreService.getRootFolder(claFile.getRootFolderId());
        if (rootFolder != null) {
            RootFolderDto rootFolderDto = DocStoreService.convertRootFolderToDto(rootFolder, rootFolder.getDocStore().getId(), false);
            rootFolderDto.setRootFolderSharePermissions(sharePermissionService.getRootFolderReadAllowSharePermissions(claFile.getRootFolderId(), rootFolder.getMediaType()));

            claFileDetailsDto.setFileRootFolder(rootFolderDto);
            claFileDto.setRootFolderPath(rootFolder.getPath());
            claFileDto.setRootFolderRealPath(rootFolder.getRealPath());
            claFileDto.setMediaType(rootFolder.getMediaType());
            claFileDto.setMailboxGroup(rootFolder.getMailboxGroup());
        }

        claFileDetailsDto.setFileOwner(claFile.getOwner());
        claFileDetailsDto.setFileProcessingState(claFile.getState());

        AssociationRelatedData fileAssociationData = associationsService.getAssociationRelatedData(claFile);
        List<AssociationEntity> fileDocTypeAssociationEntities = AssociationsService.getDocTypeAssociation(fileAssociationData.getAssociations());

        List<AssociableEntityDto> entities = new ArrayList<>(fileAssociationData.getEntities());
        Map<Long, BasicScope> scopes = new HashMap<>(fileAssociationData.getScopes());

        DocFolder folder = docFolderService.getById(claFile.getDocFolderId());
        AssociationRelatedData folderAssociationData = associationsService.getAssociationRelatedData(folder);
        List<AssociationEntity> folderDocTypeAssociation = AssociationsService.getDocTypeAssociation(folderAssociationData.getAssociations());

        entities.addAll(folderAssociationData.getEntities());
        scopes.putAll(folderAssociationData.getScopes());

        Collection<AssociationEntity> userGroupAssociations = null;
        Collection<AssociationEntity> userGroupDocTypes = null;
        if (claFile.getUserGroupId() != null) {
            FileGroup userGroup = groupsService.findByIdAssociations(claFile.getUserGroupId());
            if (userGroup != null) {
                AssociationRelatedData groupAssociationData = associationsService.getAssociationRelatedData(userGroup);
                userGroupAssociations = groupAssociationData.getAssociations();
                userGroupDocTypes = AssociationsService.getDocTypeAssociation(userGroupAssociations);
                claFileDto.setGroupName(userGroup.getNameForReport());
                entities.addAll(groupAssociationData.getEntities());
                scopes.putAll(groupAssociationData.getScopes());
            }
        }

        claFileDto.setAssociatedBizListItems(AssociationUtils.collectAssociatedBizListItemsFromFile(
                entities, fileAssociationData.getAssociations(), userGroupAssociations, folderAssociationData.getAssociations()));
        claFileDto.setAssociatedFunctionalRoles(AssociationUtils.collectAssociatedFunctionalRulesFromFile(
                entities, fileAssociationData.getAssociations(), userGroupAssociations, folderAssociationData.getAssociations()));

        List<DocTypeAssociationDto> allDocTypeAssociations = AssociationUtils.collectDocTypeAssociationsForFile(scopes, entities,
                fileDocTypeAssociationEntities, folderDocTypeAssociation, userGroupDocTypes);
        claFileDto.setDocTypeAssociations(allDocTypeAssociations);
        claFileDto.getDocTypeDtos().addAll(allDocTypeAssociations.stream().map(DocTypeAssociationDto::getDocTypeDto).collect(Collectors.toList()));

        claFileDto.setFileTagAssociations(AssociationUtils.collectFileTagAssociations(scopes, entities, fileAssociationData.getAssociations(),
                AssociationUtils.mergeDocTypeAssociations(scopes, entities, fileDocTypeAssociationEntities, folderDocTypeAssociation, userGroupDocTypes),
                userGroupAssociations,
                folderAssociationData.getAssociations(), true));
        claFileDto.setFileTagDtos(claFileDto.getFileTagAssociations().stream()
                .map(FileTagAssociationDto::getFileTagDto)
                .collect(Collectors.toSet()));

        claFileDto.setDepartmentAssociationDtos(AssociationUtils.collectDepartmentAssociations(folderAssociationData.getAssociations(), entities, true));
        claFileDto.setDepartmentDto(AssociationUtils.getDepartmentDto(claFileDto.getDepartmentAssociationDtos()));

        claFileDto.setMatchedPatternsSingle(entity.getMatchedPatternsSingle());
        claFileDto.setMatchedPatternsMulti(entity.getMatchedPatternsMulti());
        claFileDto.setMatchedPatternsCounts(entity.getMatchedPatternsCounts());

        if (ClaFileState.ERROR.equals(claFile.getState())) {
            List<FileProcessingErrorDto> fileIngestionErrors = ingestErrorService.getFileIngestionErrors(claFile.getId());
            if (fileIngestionErrors.size() > 0) {
                logger.debug("File {} has {} ingestion errors", claFile.getId(), fileIngestionErrors.size());
                claFileDetailsDto.setIngestionError(fileIngestionErrors.get(0));
            }
        }

        if (claFile.getContentMetadataId() != null & claFile.getType().hasContent()) {
            List<String> proposedTitles = contentMetadataService.findProposedTitlesByContentId(claFile.getContentMetadataId());
            claFileDetailsDto.setProposedTitles(proposedTitles);

            ContentMetadata cmd = contentMetadataService.getById(claFile.getContentMetadataId());
            if (cmd != null) {
                claFileDto.setContentMetadataDto(ContentMetadataService.convertContentMetaData(cmd));
                claFileDto.setFsFileSize(cmd.getFsFileSize());
                claFileDto.setAnalyzeHint(cmd.getAnalyzeHint());

                claFileDetailsDto.setSubject(cmd.getSubject());
                claFileDetailsDto.setTitle(cmd.getTitle());
                claFileDetailsDto.setAuthor(cmd.getAuthor());
                claFileDetailsDto.setTemplate(cmd.getTemplate());
            }
        }

        FileAclDataDto aclDataDto = FileCatUtils.extractAclData(entity);
        changeFileAclToDisplayFormat(aclDataDto);
        claFileDetailsDto.setAclDataDto(aclDataDto);
        setExtractedEntitiesForFile(claFileDto, entity, 20);

        if (entity.getMatchedPatternsCounts() != null && !entity.getMatchedPatternsCounts().isEmpty()) {
            Set<SearchPatternCountDto> patternCountList = new HashSet<>();
            for (String patternStr : entity.getMatchedPatternsCounts()) {
                Integer patternId = null;
                Integer patternCount;
                if (patternStr != null && patternStr.indexOf(countFieldSeparator) > 0) {
                    patternId = Integer.parseInt(patternStr.substring(0, patternStr.indexOf(countFieldSeparator)));
                    patternCount = Integer.parseInt(patternStr.substring(patternStr.indexOf(countFieldSeparator) + 1));
                    Optional<TextSearchPattern> optionalTextSearchPattern = textSearchPatternRepository.findById(patternId);
                    if (optionalTextSearchPattern.isPresent()) {
                        TextSearchPattern textSearchPattern = optionalTextSearchPattern.get();
                        if (textSearchPattern.isActive() && textSearchPattern.getSubCategories().size() > 0 && textSearchPattern.getSubCategories().iterator().next().isActive()) {
                            patternCountList.add(new SearchPatternCountDto(textSearchPattern.getId(), textSearchPattern.getName(), patternCount, null));
                        }
                    }
                }
            }
            claFileDto.setSearchPatternsCounting(patternCountList);
        }

        refineAssociations(claFileDetailsDto.getFileDto(), null);
        claFileDto.setDaLabelDtos(labelingService.getByIds(claFileDto.getDaLabels()));
        fixEmailAddressForFileDto(claFileDto);

        return claFileDetailsDto;
    }

    private void changeFileAclToDisplayFormat(FileAclDataDto aclDataDto) {
        aclDataDto.setAclReadData(changeAclToDisplayFormat(aclDataDto.getAclReadData()));
        aclDataDto.setAclWriteData(changeAclToDisplayFormat(aclDataDto.getAclWriteData()));
        aclDataDto.setAclDeniedReadData(changeAclToDisplayFormat(aclDataDto.getAclDeniedReadData()));
        aclDataDto.setAclDeniedWriteData(changeAclToDisplayFormat(aclDataDto.getAclDeniedWriteData()));
    }

    private List<String> changeAclToDisplayFormat(List<String> aclData) {
        return aclData == null ? null : aclData.stream().map(acl -> domainAccessHandler.getAclInConfigFormat(acl)).collect(Collectors.toList());
    }

    public void setExtractedEntitiesForFile(ClaFileDto claFileDto, SolrFileEntity categoryFile, Integer maxSamplesPerBizList) {
        Collection<String> extractedEntitiesIds = categoryFile.getExtractedEntitiesIds();
        List<SimpleBizListItemDto> sampleItems = addExtractionData(claFileDto, extractedEntitiesIds, maxSamplesPerBizList);
        Set<String> simpleItemIdsToExtract = new HashSet<>();
        sampleItems.forEach(f -> simpleItemIdsToExtract.add(f.getId()));
        Map<String, SimpleBizListItem> bizListItemMap = bizListItemService.getSimpleBizListItemsAsMap(simpleItemIdsToExtract);
        ArrayList<ClaFileDto> content = Lists.newArrayList(claFileDto);
        enrichClaFilesWithBizListItemsNameId(content, bizListItemMap);
    }

    @Transactional(readOnly = true)
    public List<ClaFileDto> listFilesOfSpecificContentId(long contentId) {
        List<SolrFileEntity> files = fileCatService.findFilesByContentId(contentId);
        List<ClaFileDto> result = new ArrayList<>();
        files.forEach(entity -> result.add(
                filesDataPersistenceService.solrFileEntityToDto(entity, true, false,
                        false, false, null, null, null,
                        null, null, null, null, null, null)
        ));
        return result;
    }

    @Transactional(readOnly = true)
    public boolean isFileInJoinedGroup(long fileId) {
        SolrFileEntity claFile1 = fileCatService.getFileEntity(fileId);
        String userGroupId = claFile1.getUserGroupId();
        String analysisGroupId = claFile1.getAnalysisGroupId();
        if (userGroupId != null && analysisGroupId != null
                && !userGroupId.equals(analysisGroupId)) {
            //File is part of a joined group. break the group instead
            logger.debug("File {} is part of joined group {}", claFile1, userGroupId);
            return true;
        }
        return false;
    }

    @Transactional
    public void excludeFileFromGrouping(long fileId) {
        ClaFile claFile = fileCatService.getFileObject(fileId);
        logger.info("exclude file {} from grouping", fileId);
        AnalyzeHint analyzeHint = AnalyzeHint.EXCLUDED;
        FileGroup analysisGroup = claFile.getAnalysisGroupId() == null ? null : groupsService.findByIdFull(claFile.getAnalysisGroupId());

        if (analysisGroup == null) {
            throw new RuntimeException(messageHandler.getMessage("file.analysis-group.invalid", Lists.newArrayList(fileId, claFile.getAnalysisGroupId())));
        }

        if (analysisGroup.getNumOfFiles() == 1) {
            logger.debug("Excluding a file from an analysis group of size 1. No need to exclude from grouping mechanism");
            analyzeHint = AnalyzeHint.NORMAL;
        } else if (contentMetadataService.getContentGroupCount(claFile.getAnalysisGroupId()) == 1) {
            logger.debug("Excluding a file from an analysis group of content size 1. No need to exclude from grouping mechanism");
            analyzeHint = AnalyzeHint.NORMAL;
        }
        filesDataPersistenceService.updateClaFileAnalyzeHintAndOverrideGroups(fileId, analyzeHint, null);
        Long contentId = claFile.getContentMetadataId();

        ContentMetadata cmd = contentMetadataService.getById(contentId);
        int fileCount = cmd.getFileCount();

        solrGrpHelperRepository.excludeFileFromGrouping(contentId);

        // update groups for files with duplicate content as well
        updateSolrGroupByQuery(CatFileFieldType.CONTENT_ID.getSolrName(), contentId, null, analyzeHint);

        //Update group dirty to all file duplicates
        // override group size and mark as dirty
        Long firstMember = analysisGroup.getFirstMemberId();
        groupsService.overrideGroupNumOfFiles(analysisGroup.getId(), analysisGroup.getNumOfFiles() - fileCount);
        filesDataPersistenceService.overrideSolrFileCatGroupAndAssociations(fileId);
        if (claFile.getId().equals(firstMember)) {
            fileGroupService.updateGroupFirstMemberContentAndFile(analysisGroup.getId());
        }
    }

    @Transactional
    public void overrideFileGroup(long fileId, String newGroupId) {
        FileGroup newGroup = groupsService.findById(newGroupId);
        logger.info("exclude file {} from grouping and attach to group {}", fileId, newGroup);
        SolrFileEntity claFile = fileCatService.getFileEntity(fileId);
        Long contentId = FileCatService.getContentId(claFile);
        FileGroup oldGroup = claFile.getUserGroupId() == null ? null : groupsService.findById(claFile.getUserGroupId());

        filesDataPersistenceService.updateClaFileAnalyzeHintAndOverrideGroups(fileId, AnalyzeHint.EXCLUDED, newGroup);

        solrGrpHelperRepository.updateAnalyzeHint(AnalyzeHint.EXCLUDED, contentId, true);

        // update groups for files with duplicate content as well
        updateSolrGroupByQuery(CatFileFieldType.CONTENT_ID.getSolrName(), contentId, newGroupId);

        if (oldGroup != null) {
            groupsService.overrideGroupNumOfFiles(oldGroup.getId(), oldGroup.getNumOfFiles() - 1);
        }
        groupsService.overrideGroupNumOfFiles(newGroupId, newGroup.getNumOfFiles() + 1);
        filesDataPersistenceService.overrideSolrFileCatGroupAndAssociations(fileId);
    }

    @Transactional
    public void removeFileGroupingOverrides(long fileId) {
        logger.info("Remove file {} grouping overrides", fileId);
        SolrFileEntity claFile = filesDataPersistenceService.updateClaFileAnalyzeHintAndOverrideGroups(fileId, AnalyzeHint.NORMAL, null);
        Long contentId = FileCatService.getContentId(claFile);

        // update groups for files with duplicate content as well
        updateSolrGroupByQuery(CatFileFieldType.CONTENT_ID.getSolrName(), contentId, null);

        solrGrpHelperRepository.updateAnalyzeHint(AnalyzeHint.NORMAL, contentId, true);
    }

    @Transactional
    public List<String> createAnalysisGroupsForFiles(List<Long> fileIds) {
        List<String> result = new ArrayList<>();
        for (Long fileId : fileIds) {
            String analysisGroupId = createAnalysisGroupForFile(fileId, result);
            result.add(analysisGroupId);
        }
        return result;
    }

    private String createAnalysisGroupForFile(Long fileId, List<String> groupIds) {
        SolrFileEntity claFile = fileCatService.getFileEntity(fileId);

        if (claFile.getAnalysisGroupId() != null) {
            if (groupIds != null && groupIds.contains(claFile.getAnalysisGroupId())) {
                // File is probably a copy of a file that was just set to its own group
                logger.debug("createAnalysisGroupForFile: added more than one copy of new gid {}", claFile.getAnalysisGroupId());
                return claFile.getAnalysisGroupId();
            }
            throw new BadRequestException(messageHandler.getMessage("file-action.create-group.assigned"), BadRequestType.UNSUPPORTED_VALUE);
        }
        Long contentId = null;
        if (claFile.getContentId() != null) {
            ContentMetadata cmd = contentMetadataService.getById(FileCatService.getContentId(claFile));
            if (cmd == null) {
                logger.error("CANT GET CONTENT from solr id {} for file {} maybe empty", claFile.getContentId(), claFile.getId());
                throw new BadRequestException(messageHandler.getMessage("file-action.create-group.no-content"), BadRequestType.OPERATION_FAILED);
            }
            if (cmd.getAnalysisGroupId() != null) {
                throw new BadRequestException(messageHandler.getMessage("file-action.create-group.content-has-group"), BadRequestType.UNSUPPORTED_VALUE);
            }
            contentId = FileCatService.getContentId(claFile);
        } else {
            logger.debug("Creating analysis group for file with no content {}", claFile);
        }
        logger.debug("Creating analysis group for file {}", claFile);
        FileGroup newAnalysisGroup = groupsService.createNewAnalysisGroup(claFile, claFile.getBaseName(), true);
        String analysisGroupId = newAnalysisGroup.getId();
        if (contentId != null) {
            groupsService.userAttachContentToGroup(contentId, analysisGroupId);
            updateSolrGroupByQuery(CatFileFieldType.CONTENT_ID.getSolrName(), contentId, analysisGroupId);
            eventBus.broadcast(Topics.FILE_GROUP_ATTACH, Event.builder()
                    .withEventType(EventType.SINGLE_FILE_GROUP_ATTACH)
                    .withEntry(EventKeys.ATTACHED_GROUPED_FILE_ID, fileId)
                    .withEntry(EventKeys.ROOT_FOLDER_ID, claFile.getRootFolderId())
                    .withEntry(EventKeys.FILE_MEDIA_TYPE, MediaType.valueOf(claFile.getMediaType()))
                    .build());
        } else {
            // Setting group ID to a single (content-less) file
            updateSolrGroupByQuery(CatFileFieldType.FILE_ID.getSolrName(), claFile.getFileId(), analysisGroupId);
            eventBus.broadcast(Topics.FILE_GROUP_ATTACH, Event.builder()
                    .withEventType(EventType.SINGLE_FILE_GROUP_ATTACH)
                    .withEntry(EventKeys.ATTACHED_GROUPED_FILE_ID, fileId)
                    .withEntry(EventKeys.ROOT_FOLDER_ID, claFile.getRootFolderId())
                    .withEntry(EventKeys.FILE_MEDIA_TYPE, MediaType.valueOf(claFile.getMediaType()))
                    .build());
        }
        return analysisGroupId;
    }

    private void updateSolrGroupByQuery(String field, long value, String groupId) {
        // update groups for files with duplicate content as well
        fileCatService.updateFieldByQueryMultiFields(
                new String[]{CatFileFieldType.USER_GROUP_ID.getSolrName(), CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName()},
                new String[]{groupId, groupId},
                SolrFieldOp.SET.getOpName(),
                field + ":" + value, null, fileGroupingOperationsCommitType);
    }

    private void updateSolrGroupByQuery(String field, long value, String groupId, AnalyzeHint analyzeHint) {
        // update groups for files with duplicate content as well
        fileCatService.updateFieldByQueryMultiFields(
                new String[]{CatFileFieldType.USER_GROUP_ID.getSolrName(), CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName(), CatFileFieldType.ANALYSIS_HINT.getSolrName()},
                new String[]{groupId, groupId, analyzeHint.name()},
                SolrFieldOp.SET.getOpName(),
                field + ":" + value, null, fileGroupingOperationsCommitType);
    }
}
