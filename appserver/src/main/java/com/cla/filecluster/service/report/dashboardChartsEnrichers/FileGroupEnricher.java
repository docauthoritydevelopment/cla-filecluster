package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class FileGroupEnricher implements DefaultTypeDataEnricher {
    @Autowired
    private GroupsService groupsService;


    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        List<String> ids = new ArrayList<>(dashboardChartDataDto.getCategories());
        List<FileGroup> groups = groupsService.findByIds(ids);
        Map<String, FileGroup> groupsMap = groups.stream().collect(
                Collectors.toMap(FileGroup::getId, fileGroup -> fileGroup));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            FileGroup fGroup = groupsMap.get(category);
            String groupName = fGroup.getName();
            if (groupName == null) {
                groupName = fGroup.getGeneratedName();
            }
            return groupName;
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return CatFileFieldType.USER_GROUP_ID.getSolrName();
    }

}
