package com.cla.filecluster.service.crawler;

import com.cla.common.domain.dto.pv.RefinementRuleDto;
import com.cla.common.domain.dto.pv.RefinementRuleField;
import com.cla.common.domain.dto.pv.RefinementRulePredicate;
import com.cla.common.domain.dto.pv.RuleOperator;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.RefinementRule;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.RefinementRuleRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.extractor.GroupsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.beust.jcommander.internal.Lists.newArrayList;

/**
 * Service to manage refinement rules for a raw analysis group
 * <p>
 * Created by: yael
 * Created on: 2/5/2018
 */
@Service
public class RefinementRuleService {

    @Autowired
    private RefinementRuleRepository refinementRuleRepository;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private ExtractTokenizer extractTokenizer;

    @Autowired
    private MessageHandler messageHandler;

    // in-memory cache of existing rules
    private LoadingCache<String, Map<Long, RefinementRuleDto>> groupRuleCache;

    private static final Logger logger = LoggerFactory.getLogger(RefinementRuleService.class);

    @PostConstruct
    void init() {
        groupRuleCache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .build(new CacheLoader<String, Map<Long, RefinementRuleDto>>() {
                    @Override
                    public Map<Long, RefinementRuleDto> load(@NotNull String key) {
                        return getRulesFromDBForCache(key);
                    }
                });
    }

    @Transactional(readOnly = true)
    Map<Long, RefinementRuleDto> getRulesFromDBForCache(String rawAnalysisGroupId) {
        List<RefinementRuleDto> rules = convertRefinementRulesToDto(refinementRuleRepository.getByRawAnalysisGroupId(rawAnalysisGroupId));
        Map<Long, RefinementRuleDto> rulesMap = new HashMap<>();
        for (RefinementRuleDto r : rules) {
            rulesMap.put(r.getId(), r);
        }
        return rulesMap;
    }

    @Transactional(readOnly = true)
    public RefinementRuleDto getRefinementRule(Long refinementRuleId) {
        return refinementRuleRepository.findById(refinementRuleId)
                .map(RefinementRuleService::convertRefinementRule)
                .orElse(null);
    }

    @Transactional(readOnly = true) // no deleted rules
    public List<RefinementRuleDto> getByRawAnalysisGroupId(String rawAnalysisGroupId) {
        Map<Long, RefinementRuleDto> rules = groupRuleCache.getUnchecked(rawAnalysisGroupId);
        if (rules != null) {
            Set<RefinementRuleDto> notDeleted = rules.values().stream().filter(r -> !r.isDeleted()).collect(Collectors.toCollection(TreeSet::new));
            return newArrayList(notDeleted);
        }
        return newArrayList();
    }

    @Transactional(readOnly = true)
    public List<RefinementRuleDto> getByRawAnalysisGroupIdAll(String rawAnalysisGroupId) {
        Map<Long, RefinementRuleDto> rules = groupRuleCache.getUnchecked(rawAnalysisGroupId);
        if (rules != null) {
            Set<RefinementRuleDto> notDeleted = new TreeSet<>(rules.values());
            return newArrayList(notDeleted);
        }
        return newArrayList();
    }

    @Transactional(readOnly = true) // no deleted or inactive rules
    public List<RefinementRuleDto> getByRawAnalysisGroupIdActive(String rawAnalysisGroupId) {
        Map<Long, RefinementRuleDto> rules = groupRuleCache.getUnchecked(rawAnalysisGroupId);
        if (rules != null) {
            Set<RefinementRuleDto> notDeletedActive = rules.values().stream().filter(RefinementRuleDto::isActive).filter(r -> !r.isDeleted()).collect(Collectors.toCollection(TreeSet::new));
            return newArrayList(notDeletedActive);
        }
        return newArrayList();
    }

    @Transactional
    public RefinementRuleDto createRefinementRule(RefinementRuleDto dto) {
        validateRefinementRule(dto);

        FileGroup parent = groupsService.findById(dto.getRawAnalysisGroupId());
        if (parent == null) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.raw-analysis-group-id.invalid"), BadRequestType.ITEM_NOT_FOUND);
        }

        try {
            RefinementRule rule = convertRefinementRule(dto, new RefinementRule());
            rule.setDirty(true);
            RefinementRuleDto result = convertRefinementRule(refinementRuleRepository.save(rule));
            if (!parent.isHasSubgroups()) {
                groupsService.updateAnalysisGroupHasRefinement(dto.getRawAnalysisGroupId(), true);
            }

            updateCache(result);
            return result;
        } catch (JsonProcessingException e) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.predicate.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private synchronized void updateCache(RefinementRuleDto dto) {
        Map<Long, RefinementRuleDto> rules = groupRuleCache.getUnchecked(dto.getRawAnalysisGroupId());
        if (rules == null) {
            rules = new HashMap<>();
        }
        rules.put(dto.getId(), dto);
        groupRuleCache.put(dto.getRawAnalysisGroupId(), rules);
    }

    private synchronized void deleteRuleFromCache(RefinementRuleDto dto) {
        Map<Long, RefinementRuleDto> rules = groupRuleCache.getUnchecked(dto.getRawAnalysisGroupId());
        if (rules != null) {
            rules.remove(dto.getId());
        }
    }

    private synchronized void evictAnalysisGroupRulesFromCache(String rawAnalysisGroupId) {
        groupRuleCache.invalidate(rawAnalysisGroupId);
    }

    @Transactional
    public RefinementRuleDto updateRefinementRule(RefinementRuleDto dto, boolean shouldValidate) {
        if (shouldValidate) {
            validateRefinementRule(dto);
        }

        RefinementRule one = getRefinementRuleForUpdate(dto.getId());

        if (!one.getRawAnalysisGroupId().equals(dto.getRawAnalysisGroupId())) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.raw-analysis-group-id.change"), BadRequestType.UNAUTHORIZED);
        }

        boolean shouldAlsoChangeGroupName = false;
        if (one.getSubgroupId() != null && !one.getSubgroupName().equals(dto.getSubgroupName())) {
            shouldAlsoChangeGroupName = true;
        }

        logger.info("Update Refinement rule {}", one);
        try {
            one = convertRefinementRule(dto, one);
            one.setDirty(true);
            RefinementRuleDto result = convertRefinementRule(refinementRuleRepository.save(one));
            updateCache(result);

            if (shouldAlsoChangeGroupName) {
                groupsService.updateGroupNameInDB(one.getSubgroupId(), one.getSubgroupName());
            }

            return result;
        } catch (JsonProcessingException e) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.predicate.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private RefinementRule getRefinementRuleForUpdate(Long id) {
        RefinementRule one = refinementRuleRepository.findById(id).orElse(null);

        if (one == null) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rule.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return one;
    }

    private RefinementRuleDto updateRefinementRule(RefinementRule rule) {
        RefinementRuleDto result = convertRefinementRule(refinementRuleRepository.save(rule));
        updateCache(result);
        return result;
    }

    @Transactional
    public RefinementRuleDto updateRefinementRuleActive(Long id, Boolean state) {
        if (state == null) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.state.mandatory"), BadRequestType.MISSING_FIELD);
        }

        logger.info("Set Refinement rule active {} for Refinement rule ID {}", (state ? "on" : "off"), id);
        RefinementRule one = getRefinementRuleForUpdate(id);

        one.setActive(state);
        one.setDirty(true);
        return updateRefinementRule(one);
    }

    @Transactional
    public RefinementRuleDto updateRefinementRuleDirty(Long id, Boolean dirty) {
        if (dirty == null) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.dirty.mandatory"), BadRequestType.MISSING_FIELD);
        }

        logger.info("Set Refinement rule dirty {} for Refinement rule ID {}", (dirty ? "on" : "off"), id);
        RefinementRule one = getRefinementRuleForUpdate(id);

        one.setDirty(dirty);
        return updateRefinementRule(one);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public RefinementRuleDto updateRefinementRuleSubGroup(Long id, String subGroupId) {
        if (Strings.isNullOrEmpty(subGroupId)) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.sub-group.mandatory"), BadRequestType.MISSING_FIELD);
        }

        logger.info("Set Refinement rule sub group {} for Refinement rule ID {}", subGroupId, id);
        RefinementRule one = getRefinementRuleForUpdate(id);
        one.setSubgroupId(subGroupId);
        one.setDirty(true);
        return updateRefinementRule(one);
    }

    @Transactional
    public RefinementRuleDto markRefinementRuleDeleted(long id) {
        RefinementRule one = getRefinementRuleForUpdate(id);
        logger.debug("mark RefinementRule deleted {} ", one);
        one.setDeleted(true);
        one.setDirty(true);
        return updateRefinementRule(one);
    }

    @Transactional
    public void markRefinementRulesDeleted(String rawAnalysisGroupId) {
        List<RefinementRuleDto> currentRules = getByRawAnalysisGroupId(rawAnalysisGroupId);
        if (currentRules != null && !currentRules.isEmpty()) {
            for (RefinementRuleDto rule : currentRules) {
                markRefinementRuleDeleted(rule.getId());
                eventAuditService.audit(AuditType.REFINEMENT_RULE, "Delete refinementRule", AuditAction.DELETE, RefinementRuleDto.class, rule.getId(), rule);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteRefinementRule(long id) {
        RefinementRule one = refinementRuleRepository.getOne(id);
        logger.debug("Delete RefinementRule {} ", one);
        if (one != null) {
            refinementRuleRepository.delete(one);
            RefinementRuleDto result = convertRefinementRule(one);
            deleteRuleFromCache(result);
        }
    }

    @Transactional
    public List<RefinementRuleDto> updateRefinmentRulesByPhrasesList(List<String> phrases, String rawAnalysisGroupId, String rawAnalysisGroupName) {
        List<RefinementRuleDto> toCreate = new ArrayList<>();
        List<RefinementRuleDto> toUpdate = new ArrayList<>();
        List<RefinementRuleDto> toDelete = new ArrayList<>();
        List<RefinementRuleDto> currentRules = getByRawAnalysisGroupId(rawAnalysisGroupId);

        phrases = phrases.stream().filter(v -> !Strings.isNullOrEmpty(v) && !Strings.isNullOrEmpty(v.trim())).map(String::trim).collect(Collectors.toList());
        phrases = phrases.stream().filter(v -> !RefinementRulePredicate.stopWords.contains(v.toUpperCase()) && !v.contains(" ")).collect(Collectors.toList());

        if (phrases == null || phrases.isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("refinement-rules.update.phrases.empty"), BadRequestType.MISSING_FIELD);
        }

        if (currentRules == null || currentRules.isEmpty()) { // all rules are new
            int priority = 1;
            for (String p : phrases) {
                toCreate.add(createNewRule(rawAnalysisGroupId, rawAnalysisGroupName, p, priority));
                ++priority;
            }

        } else { // some rules already exists - update and create where needed
            int priority = 1;
            for (String p : phrases) {
                Long exists = 0L;
                for (RefinementRuleDto cr : currentRules) {
                    if (cr.getPredicate().getValues().get(0).equalsIgnoreCase(p)) {
                        exists = cr.getId();
                        break;
                    }
                }
                if (exists > 0) {
                    RefinementRuleDto r = createNewRule(rawAnalysisGroupId, rawAnalysisGroupName, p, priority);
                    r.setId(exists);
                    toUpdate.add(r);
                } else {
                    toCreate.add(createNewRule(rawAnalysisGroupId, rawAnalysisGroupName, p, priority));
                }
                ++priority;
            }
        }

        // find rules to be deleted
        if (currentRules != null && !currentRules.isEmpty()) {
            for (RefinementRuleDto cr : currentRules) {
                boolean found = false;
                for (String p : phrases) {
                    if (cr.getPredicate().getValues().get(0).equalsIgnoreCase(p)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    toDelete.add(cr);
                }
            }
        }

        // update the db and fire audit
        try {
            for (RefinementRuleDto r : toDelete) {
                RefinementRuleDto refinementRule = markRefinementRuleDeleted(r.getId());
                eventAuditService.audit(AuditType.REFINEMENT_RULE, "Delete refinementRule", AuditAction.DELETE, RefinementRuleDto.class, r.getId(), refinementRule);
            }
            for (RefinementRuleDto r : toUpdate) {
                RefinementRuleDto refinementRule = updateRefinementRule(r, false);
                eventAuditService.audit(AuditType.REFINEMENT_RULE, "Update refinementRule", AuditAction.UPDATE, RefinementRuleDto.class, r.getId(), refinementRule);
            }
            for (RefinementRuleDto r : toCreate) {
                RefinementRuleDto refinementRule = createRefinementRule(r);
                r.setId(refinementRule.getId());
                eventAuditService.audit(AuditType.REFINEMENT_RULE, "Create refinementRule", AuditAction.CREATE, RefinementRuleDto.class, refinementRule.getId(), refinementRule);
            }
        } catch (Throwable t) {
            evictAnalysisGroupRulesFromCache(rawAnalysisGroupId); // if failed, remove all from cache
            throw t;
        }

        // return updated rules
        return getByRawAnalysisGroupId(rawAnalysisGroupId);
    }

    private RefinementRuleDto createNewRule(String rawAnalysisGroupId, String rawAnalysisGroupName, String phrase, int priority) {
        RefinementRuleDto r = new RefinementRuleDto();
        r.setSubgroupName(rawAnalysisGroupName + " - " + phrase);
        r.setPriority(priority);
        r.setRawAnalysisGroupId(rawAnalysisGroupId);
        r.setActive(true);
        r.setPredicate(new RefinementRulePredicate());
        r.getPredicate().setValues(Collections.singletonList(phrase));
        r.getPredicate().setOperator(RuleOperator.CONTAINS);
        r.getPredicate().setField(RefinementRuleField.CONTENT);
        return r;
    }

    private void validateRefinementRule(RefinementRuleDto dto) {
        if (StringUtils.isEmpty(dto.getSubgroupName()) || StringUtils.isEmpty(dto.getSubgroupName().trim())) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.sub-group-name.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        if (StringUtils.isEmpty(dto.getRawAnalysisGroupId())) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.parent-group.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        if (dto.getPriority() <= 0) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.order.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        // we also need to validate the term using tokenizer rules
        // see that its not too short etc
        Function<String, Boolean> tokenizerValidator = input -> !Strings.isNullOrEmpty(extractTokenizer.getHash(input));

        if (dto.getPredicate() != null && !dto.getPredicate().validateRule(tokenizerValidator)) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.predicate.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        Map<Long, RefinementRuleDto> rulesForGroup = groupRuleCache.getUnchecked(dto.getRawAnalysisGroupId());
        for (RefinementRuleDto rule : rulesForGroup.values()) {
            if (!rule.isDeleted() && !rule.getId().equals(dto.getId())) {
                if (rule.getPriority() == dto.getPriority()) {
                    throw new BadRequestException(messageHandler.getMessage("refinement-rules.order.invalid"), BadRequestType.ITEM_ALREADY_EXISTS);
                } else if (dto.getSubgroupName().equals(rule.getSubgroupName())) {
                    throw new BadRequestException(messageHandler.getMessage("refinement-rules.sub-group-name.invalid"), BadRequestType.ITEM_ALREADY_EXISTS);
                }
            }
        }
    }

    public static RefinementRule convertRefinementRule(RefinementRuleDto dto, RefinementRule rule) throws JsonProcessingException {
        String body = null;

        if (rule == null) return null;
        if (dto == null) return null;

        if (dto.getPredicate() != null) {
            body = ModelDtoConversionUtils.getMapper().writeValueAsString(dto.getPredicate());
        }

        rule.setId(dto.getId());
        rule.setActive(dto.isActive());
        rule.setBody(body);
        rule.setDirty(dto.isDirty());
        rule.setPriority(dto.getPriority());
        rule.setRawAnalysisGroupId(dto.getRawAnalysisGroupId());
        rule.setSubgroupName(dto.getSubgroupName());
        rule.setDeleted(dto.isDeleted());
        return rule;
    }

    public static RefinementRuleDto convertRefinementRule(RefinementRule rule) {
        if (rule == null) return null;

        RefinementRuleDto dto = new RefinementRuleDto();
        dto.setId(rule.getId());
        dto.setActive(rule.isActive());
        dto.setDirty(rule.isDirty());
        dto.setPriority(rule.getPriority());
        dto.setPredicate(convertFromJson(rule.getBody()));
        dto.setSubgroupId(rule.getSubgroupId());
        dto.setSubgroupName(rule.getSubgroupName());
        dto.setRawAnalysisGroupId(rule.getRawAnalysisGroupId());
        dto.setDeleted(rule.isDeleted());
        return dto;
    }

    public static List<RefinementRuleDto> convertRefinementRulesToDto(List<RefinementRule> rules) {
        List<RefinementRuleDto> content = new ArrayList<>(rules.size());
        rules.forEach(rule -> content.add(convertRefinementRule(rule)));
        return content;
    }

    private static RefinementRulePredicate convertFromJson(String json) {
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        try {
            return ModelDtoConversionUtils.getMapper().readValue(json, RefinementRulePredicate.class);
        } catch (IOException e) {
            logger.error("Failed to convert RefinementRulePredicate string [" + json + "] to object", e);
            throw new RuntimeException("Failed to convert RefinementRulePredicate string [" + json + "] to object", e);
        }
    }
}
