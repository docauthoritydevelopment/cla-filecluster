package com.cla.filecluster.service.export;

import lombok.Builder;
import lombok.Data;
import org.springframework.core.io.Resource;

@Data
@Builder
public class ExportResult {
    private final Resource data;
    private final String fileName;
    private final String extension;
    private final String contentType;
    private Long downloadTime;
}
