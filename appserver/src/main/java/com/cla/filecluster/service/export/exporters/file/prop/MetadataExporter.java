package com.cla.filecluster.service.export.exporters.file.prop;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.acl.AclAggCountDtoMixin;
import com.cla.filecluster.service.metadata.MetadataAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

@Slf4j
@Component
public class MetadataExporter extends PageCsvExcelExporter<AggregationCountItemDTO<String>> {

    private final static String[] HEADER = {TYPE, NAME, NUM_OF_FILES};

    @Autowired
    private MetadataAppService metadataAppService;

    @Value("${metadata.separator:}")
    private String metadataSeparator;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AclAggCountDtoMixin.class);

    }

    @Override
    protected Page<AggregationCountItemDTO<String>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<String>> res = metadataAppService.getFileCountsForExport(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<String> base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();

        String[] data = base.getItem().split(metadataSeparator);

        result.put(TYPE, data[0]);

        if (data.length > 1) {
            result.put(NAME, data[1]);
        }
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DA_METADATA);
    }
}
