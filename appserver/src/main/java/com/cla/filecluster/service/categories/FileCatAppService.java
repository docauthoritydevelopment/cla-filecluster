package com.cla.filecluster.service.categories;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.crawler.FileStateDto;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePointDto;
import com.cla.common.domain.dto.file.*;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.utils.CollectionsUtils;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.ID;
import static java.util.stream.Collectors.toList;

@Service
public class FileCatAppService {

    private static final Logger logger = LoggerFactory.getLogger(FileCatAppService.class);

    private static final String RANGE_FACET_QUERY_PREFIX = "range_";

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FileCatAclService fileCatAclService;

    @Value("${acl.re-transformer.page-size:50000}")
    private int aclReTransformerPageSize;

    @Value("${solr.facet-count-hll.lower-bound:200}")
    private int lowerBoundFacetCountHll;

    @Value("${solr.facet-count-hll.upper-bound:300}")
    private int upperBoundFacetCountHll;

    public FacetPage<AggregationCountItemDTO<FileContentDuplicationDto>> getContentFileCounts(DataSourceRequest dataSourceRequest) {
        SolrFacetQueryJson duplicateFileCountJson = buildJsonFacetDuplicateFileCount();
        final SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = fileCatService.runJsonFacetQuery(
                dataSourceRequest, Lists.newArrayList(duplicateFileCountJson));
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse.getResponseBucket(
                CatFileFieldType.CONTENT_ID.getSolrName()).getBuckets();

        Map<String, Long> resultsWithoutFilter = fetchResultsWithoutFilter(dataSourceRequest, buckets);
        List<AggregationCountItemDTO<FileContentDuplicationDto>> content = buildAggregationCountItemDTO(buckets, resultsWithoutFilter);
        return new FacetPage<>(content, solrFacetQueryJsonResponse.getPageRequest());
    }

    private List<AggregationCountItemDTO<FileContentDuplicationDto>> buildAggregationCountItemDTO(
            List<SolrFacetJsonResponseBucket> buckets, Map<String, Long> resultsWithoutFilter) {
        List<AggregationCountItemDTO<FileContentDuplicationDto>> content = new ArrayList<>();
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            List<SolrFacetJsonResponseBucket> innerBuckets = bucket.getBucketBasedFacets(CatFileFieldType.BASE_NAME.getSolrName())
                    .getBuckets();
            String id = bucket.getStringVal();
            FileContentDuplicationDto dto = new FileContentDuplicationDto(id, innerBuckets.get(0).getStringVal());
            Long bucketCount = bucket.getLongCountNumber();
            AggregationCountItemDTO<FileContentDuplicationDto> item = new AggregationCountItemDTO<>(dto,
                    bucketCount);
            content.add(item);
            Optional.ofNullable(resultsWithoutFilter.get(id)).ifPresent(item::setCount2);
        }
        return content;
    }

    private Map<String, Long> fetchResultsWithoutFilter(DataSourceRequest dataSourceRequest, List<SolrFacetJsonResponseBucket> buckets) {
        Map<String, Long> resultsNoFilter = new HashMap<>();

        if (dataSourceRequest.hasFilter() && buckets.size() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            List<String> contentIds = buckets.stream()
                    .map(SolrFacetJsonResponseBucket::getStringVal)
                    .collect(toList());
            final SolrFacetQueryJson duplicateFileCountJson = buildJsonFacetDuplicateFileCount();
            SolrFacetQueryJsonResponse solrFacetQueryJsonResponseNoFilter = fileCatService.runJsonFacetQuery(
                    dataSourceRequest,
                    Lists.newArrayList(duplicateFileCountJson),
                    CatFileFieldType.CONTENT_ID.inQuery(contentIds));
            final SolrFacetJsonResponseBucket contentsBucket = solrFacetQueryJsonResponseNoFilter
                    .getResponseBucket(CatFileFieldType.CONTENT_ID.getSolrName());
            resultsNoFilter = new HashMap<>();
            for (SolrFacetJsonResponseBucket bucket : contentsBucket.getBuckets()) {
                resultsNoFilter.put(bucket.getStringVal(), bucket.getLongCountNumber());
            }
        }
        return resultsNoFilter;
    }

    private SolrFacetQueryJson buildJsonFacetDuplicateFileCount() {
        SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.BASE_NAME.getSolrName());
        innerFacet.setType(FacetType.TERMS);
        innerFacet.setField(CatFileFieldType.SORT_NAME);
        innerFacet.setLimit(1);
        SolrFacetQueryJson contentIdFacet = new SolrFacetQueryJson(CatFileFieldType.CONTENT_ID.getSolrName());
        contentIdFacet.setType(FacetType.TERMS);
        contentIdFacet.setField(CatFileFieldType.CONTENT_ID);
        contentIdFacet.setMincount(2);
        contentIdFacet.addFacet(innerFacet);

        return contentIdFacet;
    }

    public Map<String, Integer> getDateRangePartitionCounts(DataSourceRequest dataSourceRequest,
                                                            CatFileFieldType catFileFieldType,
                                                            List<DateRangeItemDto> dateRangeItemDtos) {
        fillTimeHistogramQueryValues(dateRangeItemDtos);
        Collection<String> queryValues = dateRangeItemDtos.stream()
                .map(DateRangeItemDto::getDateFilter)
                .collect(Collectors.toList());
        SolrSpecification solrSpecification = solrSpecificationFactory
                .buildSolrFacetSpecification(dataSourceRequest, catFileFieldType, queryValues);
        return fileCatService.runFacetRangeQuery(solrSpecification);
    }

    private void fillTimeHistogramQueryValues(List<DateRangeItemDto> dateRangeItemDtos) {
        DateRangePointDto previousStart = null;
        for (DateRangeItemDto dateRangeItemDto : dateRangeItemDtos) {
            DateRangePointDto start = dateRangeItemDto.getStart();
            DateRangePointDto end = dateRangeItemDto.getEnd() != null ? dateRangeItemDto.getEnd() : previousStart;
            if (start == null) {
                throw new BadRequestException("start at " + dateRangeItemDto, "null", BadRequestType.MISSING_FIELD);
            }
            if (end == null) {
                //This point is a fiction for the UI. Not a real point
                return;
            }
            String filter = solrSpecificationFactory.buildDateTimeFilter(start, end);
            dateRangeItemDto.setDateFilter(filter);
            previousStart = start;
        }

    }

    public Map<String, Integer> getFileSizePartitionCounts(DataSourceRequest dataSourceRequest,
                                                           Map<Long, FileSizePartitionDto> partitions) {
        List<String> queryValues = SolrFileSizeQueryUtils.getFileSizeQueryValues(partitions.values());
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(
                dataSourceRequest, CatFileFieldType.SIZE, queryValues);
        return fileCatService.runFacetRangeQuery(solrSpecification);
    }

    public Page<AggregationCountItemDTO<DocFolderDto>> listDocFoldersInGroup(FileGroup fileGroup, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();

        Query query = Query.create();
        Query ugFilter = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, fileGroup.getId()));
        query.addFilterQuery(ugFilter);
        query.addFacetField(CatFileFieldType.FOLDER_ID);
        query.setRows(0);
        query.setFacetMinCount(1);
        solrSpecificationFactory.addFilterDeletedFiles(query);

        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        Map<Long, Long> result = new TreeMap<>();
        if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
            FacetField field = queryResponse.getFacetFields().get(0);
            List<FacetField.Count> values = field.getValues();
            for (FacetField.Count c : values) {
                String folderId = c.getName();
                long count = c.getCount();
                result.put(Long.parseLong(folderId), count);
            }
        }

        //noinspection unchecked
        result = CollectionsUtils.getSubMap((TreeMap) result, (int) pageRequest.getOffset(), pageRequest.getPageSize());
        List<DocFolder> folders = docFolderService.findByIds(result.keySet());

        List<AggregationCountItemDTO<DocFolderDto>> resultList = new ArrayList<>();
        for (DocFolder f : folders) {
            DocFolderDto docFolderDto = new DocFolderDto(f.getId(), f.getName(), f.getPath(), 0);
            resultList.add(new AggregationCountItemDTO<>(docFolderDto, result.get(f.getId())));
        }

        return new PageImpl<>(resultList, pageRequest, result.size());
    }

    public FacetPage<AggregationCountItemDTO<FileExtensionDto>> getExtensionFileCounts(DataSourceRequest dataSourceRequest) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildTermJsonFacetSpecification(
                dataSourceRequest, CatFileFieldType.EXTENSION);
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = fileCatService.runFacetQueryJson(
                solrFacetSpecification);
        final SolrFacetJsonResponseBucket extensionBucket = solrFacetQueryJsonResponse.getResponseBucket(
                CatFileFieldType.EXTENSION.getSolrName());
        Map<String, Long> totalMap = getTermJsonTotalMap(extensionBucket, dataSourceRequest,
                CatFileFieldType.EXTENSION);
        List<AggregationCountItemDTO<FileExtensionDto>> content = parseTermJsonFacetQueryWithNewTotal(
                extensionBucket, totalMap, this::getExtensionDtoById);
        final FacetPage<AggregationCountItemDTO<FileExtensionDto>> facetPage = new FacetPage<>(content,
                solrFacetQueryJsonResponse.getPageRequest());
        facetPage.setTotalElements(getTotalFacetCount(dataSourceRequest, CatFileFieldType.EXTENSION));
        facetPage.setTotalPageNumber(ControllerUtils.roundUpDivision(facetPage.getTotalElements(), dataSourceRequest.getPageSize()));
        return facetPage;
    }

    private Map<String, Long> getTermJsonTotalMap(SolrFacetJsonResponseBucket resultBucket,
                                                  DataSourceRequest dataSourceRequest,
                                                  CatFileFieldType catFileFieldType) {
        Map<String, Long> countNoFilter = new HashMap<>();
        List<SolrFacetJsonResponseBucket> buckets = resultBucket.getBuckets();
        if (dataSourceRequest.getFilter() != null) {
            SolrSpecification totalSolrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                    dataSourceRequest);
            totalSolrFacetSpecification.setFilterDescriptor(null);
            List<String> fieldKeys = new LinkedList<>();
            for (SolrFacetJsonResponseBucket bucket : buckets) {
                String fieldKey = bucket.getStringVal();
                fieldKeys.add(fieldKey);
                SolrFacetQueryJson rangeFacet = new SolrFacetQueryJson(RANGE_FACET_QUERY_PREFIX + fieldKey);
                rangeFacet.setType(FacetType.QUERY);
                rangeFacet.setField(catFileFieldType);
                rangeFacet.setQueryValueString(fieldKey);
                totalSolrFacetSpecification.addFacetJsonObject(rangeFacet);
            }

            SolrFacetQueryJsonResponse facetQueryJsonResponse = fileCatService.runFacetQueryJson(
                    totalSolrFacetSpecification);

            fieldKeys.forEach(key -> {
                final SolrFacetJsonResponseBucket bucketBasedFacets = facetQueryJsonResponse
                        .getResponseBucket(RANGE_FACET_QUERY_PREFIX + key);
                countNoFilter.put(key, bucketBasedFacets.getLongCountNumber());
            });
        }
        return countNoFilter;
    }

    private FileExtensionDto getExtensionDtoById(String id) {
        return new FileExtensionDto(String.valueOf(id));
    }

    private <T> List<AggregationCountItemDTO<T>> parseTermJsonFacetQueryWithNewTotal(
            SolrFacetJsonResponseBucket resultBucket,
            Map<String, Long> countNoFilter,
            Function<String, T> instanceCreator) {

        List<AggregationCountItemDTO<T>> content = new ArrayList<>();
        List<SolrFacetJsonResponseBucket> buckets = resultBucket.getBuckets();
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            String fieldKey = bucket.getStringVal();
            T dto = instanceCreator.apply(fieldKey);
            Long bucketCount = bucket.getLongCountNumber();
            AggregationCountItemDTO<T> item = new AggregationCountItemDTO<>(dto, bucketCount.intValue());

            item.setTotalSize(bucket.getDoubleValueByKey(CatFileFieldType.SIZE.getSolrName()));
            if (countNoFilter != null && countNoFilter.get(fieldKey) != null) {
                item.setCount2(countNoFilter.get(fieldKey).intValue());
            }
            content.add(item);
        }
        return content;
    }

    public Page<AggregationCountItemDTO<FileUserDto>> countFilesByOwner(PageRequest pageRequest) {
        Query query = Query.create();
        query.addFacetPivot(CatFileFieldType.OWNER);
        query.addFacetPivot(CatFileFieldType.USER_GROUP_ID);
        query.setRows(0);
        query.setFacetMinCount(1);
        query.setFacetPivotAsOne(true);
        solrSpecificationFactory.addFilterDeletedFiles(query);
        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        List<AggregationCountItemDTO<FileUserDto>> result = new ArrayList<>();
        queryResponse.getFacetPivot().forEach((k, v) -> {
            v.forEach(f -> {
                String user = (String) f.getValue();
                int userGroupIds = f.getPivot() == null ? 0 : f.getPivot().size();
                int fileNum = f.getCount();
                result.add(new AggregationCountItemDTO<>(new FileUserDto(user), fileNum, userGroupIds));
            });
        });
        return new PageImpl<>(result, pageRequest, queryResponse.getFacetPivot().size());
    }

    public Map<String, Long> countFilesByGroupForOwner(String owner) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.OWNER, SolrOperator.EQ, owner));
        query.addFacetField(CatFileFieldType.USER_GROUP_ID);
        query.setRows(0);
        query.setFacetMinCount(1);
        solrSpecificationFactory.addFilterDeletedFiles(query);

        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        Map<String, Long> result = new TreeMap<>();
        FacetField field = queryResponse.getFacetFields().get(0);
        List<FacetField.Count> values = field.getValues();
        for (FacetField.Count c : values) {
            String group = c.getName();
            Long count = c.getCount();
            result.put(group, count);
        }
        return result;
    }

    public Page<AggregationCountItemDTO<FileUserDto>> countFilesByOwnerForGroup(PageRequest pageRequest, String groupId) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, groupId));
        query.addFacetField(CatFileFieldType.OWNER);
        query.setRows(0);
        query.setFacetMinCount(1);
        solrSpecificationFactory.addFilterDeletedFiles(query);

        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        List<AggregationCountItemDTO<FileUserDto>> result = new ArrayList<>();
        FacetField field = queryResponse.getFacetFields().get(0);
        List<FacetField.Count> values = field.getValues();
        for (FacetField.Count c : values) {
            String userId = c.getName();
            Long count = c.getCount();
            result.add(new AggregationCountItemDTO<>(new FileUserDto(userId), count));
        }
        return new PageImpl<>(result, pageRequest, values.size());
    }

    public List<AggregationCountItemDTO<FileStateDto>> countFilesByStateAndType() {
        Query query = Query.create();
        query.addFacetPivot(CatFileFieldType.PROCESSING_STATE);
        query.addFacetPivot(CatFileFieldType.TYPE);
        query.setRows(0);
        query.setFacetMinCount(1);
        query.setFacetPivotAsOne(true);
        solrSpecificationFactory.addFilterDeletedFiles(query);
        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        List<AggregationCountItemDTO<FileStateDto>> result = new ArrayList<>();
        queryResponse.getFacetPivot().forEach((k, v) ->
                v.forEach(f -> {
                    ClaFileState state = ClaFileState.valueOf((String) f.getValue());
                    f.getPivot().forEach(pv -> {
                        FileType fileType = FileType.valueOf((Integer) pv.getValue());
                        FileStateDto fileStateDto = new FileStateDto();
                        fileStateDto.setFileState(state);
                        fileStateDto.setFileType(fileType);
                        result.add(new AggregationCountItemDTO<>(fileStateDto, pv.getCount()));
                    });
                }));
        return result;
    }

    public SolrFacetQueryResponse runFacetQuery(DataSourceRequest dataSourceRequest, SolrSpecification solrFacetSpecification) {
        return fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    public long getTotalFacetCountByPrefix(DataSourceRequest dataSourceRequest, CatFileFieldType catFileFieldType,
                                           String prefix) {
        SolrFacetQueryJson solrFacetQueryJson = buildJsonFacetByField(catFileFieldType);
        solrFacetQueryJson.setPrefix(prefix);
        final SolrFacetQueryJsonResponse solrFacetQueryJsonResponse =
                fileCatService.runJsonFacetQuery(dataSourceRequest, Lists.newArrayList(solrFacetQueryJson));
        long result = solrFacetQueryJsonResponse.getResponseBucket(catFileFieldType.getSolrName()).getTotalNumOfBuckets();

        return testFacetTotalResultIfNeeded(result, dataSourceRequest, catFileFieldType, solrFacetQueryJson);
    }

    public long getTotalFacetCount(DataSourceRequest dataSourceRequest, CatFileFieldType catFileFieldType,
                                   String... additionFilters) {
        SolrFacetQueryJson solrFacetQueryJson = buildJsonFacetByField(catFileFieldType);
        final SolrFacetQueryJsonResponse solrFacetQueryJsonResponse =
                fileCatService.runJsonFacetQuery(dataSourceRequest, Lists.newArrayList(solrFacetQueryJson),
                        additionFilters);
        long result = solrFacetQueryJsonResponse.getResponseBucket(catFileFieldType.getSolrName()).getTotalNumOfBuckets();

        return testFacetTotalResultIfNeeded(result, dataSourceRequest, catFileFieldType, solrFacetQueryJson, additionFilters);
    }

    private long testFacetTotalResultIfNeeded(long hllResult, DataSourceRequest dataSourceRequest,
                                              CatFileFieldType catFileFieldType,
                                              SolrFacetQueryJson solrFacetQueryJson, String... additionFilters) {
        if (hllResult < upperBoundFacetCountHll) {

            logger.trace("got facet for field {} below lower bound {}, get actual facet to get limit", catFileFieldType, upperBoundFacetCountHll);

            int pageSize = dataSourceRequest.getPageSize();
            int pageNum = dataSourceRequest.getPage();

            dataSourceRequest.setPageSize(lowerBoundFacetCountHll + 1);
            dataSourceRequest.setPage(1);

            solrFacetQueryJson.setLimit(lowerBoundFacetCountHll + 1);
            solrFacetQueryJson.setOffset(0L);

            SolrFacetQueryJsonResponse solrFacetQueryJsonResponseGet =
                    fileCatService.runJsonFacetQuery(dataSourceRequest, Lists.newArrayList(solrFacetQueryJson),
                            additionFilters);

            long exactResult = solrFacetQueryJsonResponseGet.getResponseBucket(catFileFieldType.getSolrName()).getBuckets().size();

            dataSourceRequest.setPageSize(pageSize);
            dataSourceRequest.setPage(pageNum);

            if (exactResult < lowerBoundFacetCountHll + 1) {
                return exactResult;
            } else if (hllResult > lowerBoundFacetCountHll) { // this means we dont have exact result and should return hll
                return hllResult;
            } else { // this means exact = low bound + 1 and hll <= low bound so we have no idea whats going on
                logger.warn("hll total for field {} is {} while exact is {}", catFileFieldType, hllResult, exactResult);
                return exactResult;
            }
        }

        return hllResult;
    }

    private SolrFacetQueryJson buildJsonFacetByField(CatFileFieldType catFileFieldType) {
        SolrFacetQueryJson solrFacetQueryJson = new SolrFacetQueryJson(catFileFieldType.getSolrName());
        solrFacetQueryJson.setType(FacetType.TERMS);
        solrFacetQueryJson.setField(catFileFieldType);
        solrFacetQueryJson.setShowTotalNumberOfBuckets(true);
        return solrFacetQueryJson;
    }

    // go over all files with metadata and try to re-translate using current config file
    @AutoAuthenticate
    public void reTranslateFileAcl(String fileShareDomain) {
        logger.debug("start retranslate domain {}", fileShareDomain);

        Map<CatFileFieldType, AclType> fullFields = new HashMap<>();
        fullFields.put(CatFileFieldType.ACL_READ_F, AclType.READ_TYPE);
        fullFields.put(CatFileFieldType.ACL_WRITE_F, AclType.WRITE_TYPE);
        fullFields.put(CatFileFieldType.ACL_DENIED_READ_F, AclType.DENY_READ_TYPE);
        fullFields.put(CatFileFieldType.ACL_DENIED_WRITE_F, AclType.DENY_WRITE_TYPE);

        int pageCount = 0;
        String cursor = "*";
        boolean lastPage;
        do {
            Query query = Query.create()
                    .addFilterWithCriteria(Criteria.or(
                            Criteria.create(CatFileFieldType.ACL_READ_F, SolrOperator.EQ, fileShareDomain+"*"),
                            Criteria.create(CatFileFieldType.ACL_WRITE_F, SolrOperator.EQ, fileShareDomain+"*"),
                            Criteria.create(CatFileFieldType.ACL_DENIED_READ_F, SolrOperator.EQ, fileShareDomain+"*"),
                            Criteria.create(CatFileFieldType.ACL_DENIED_WRITE_F, SolrOperator.EQ, fileShareDomain+"*")
                    ))
                    .setRows(aclReTransformerPageSize).setCursorMark(cursor)
                    .addField(ID)
                    .addField(CatFileFieldType.ACL_READ_F)
                    .addField(CatFileFieldType.ACL_READ)
                    .addField(CatFileFieldType.ACL_WRITE_F)
                    .addField(CatFileFieldType.ACL_WRITE)
                    .addField(CatFileFieldType.ACL_DENIED_READ_F)
                    .addField(CatFileFieldType.ACL_DENIED_READ)
                    .addField(CatFileFieldType.ACL_DENIED_WRITE_F)
                    .addField(CatFileFieldType.ACL_DENIED_WRITE)
                    .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

            long start = System.currentTimeMillis();
            logger.debug("loading page {} of {} files", pageCount, aclReTransformerPageSize);

            QueryResponse resp = fileCatService.runQuery(query.build());

            logger.trace("get page {} of {} files took {} ms", pageCount, resp.getResults().size(), (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();

            cursor = resp.getNextCursorMark();
            List<SolrInputDocument> filesToCng = new ArrayList<>();
            if (resp.getResults().size() > 0) {

                logger.debug("handling result {}", resp.getResults().size());

                resp.getResults().forEach(doc -> {
                    String id = (String) doc.getFieldValue(ID.getSolrName());
                    SolrInputDocument docResult = new SolrInputDocument();
                    docResult.setField(ID.getSolrName(), id);

                    handleAclField(fullFields, doc, docResult);
                    
                    if (docResult.getFieldNames().size() > 1) {
                        SolrFileCatRepository.setField(docResult, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
                        filesToCng.add(docResult);
                    }
                });

                if (!filesToCng.isEmpty()) {
                    logger.debug("{} changes out of {} docs", filesToCng.size(), resp.getResults().size());
                    fileCatService.saveAll(filesToCng);
                    fileCatService.softCommitNoWaitFlush();
                } else {
                    logger.debug("no changes out of {} docs", resp.getResults().size());
                }
            }
            logger.trace("update page {} of {} files took {} ms", pageCount, resp.getResults().size(), (System.currentTimeMillis() - start));
            pageCount++;
            lastPage = resp.getResults().size() < aclReTransformerPageSize;
        } while (!lastPage);
    }

    private void handleAclField(Map<CatFileFieldType, AclType> fullFields, SolrDocument doc, SolrInputDocument result) {
        List<AclItemDto> aclItems = new ArrayList<>();
        fullFields.forEach((full, type) -> {
            List<String> fullFieldValue = (List<String>) doc.getFieldValue(full.getSolrName());
            if (fullFieldValue != null && !fullFieldValue.isEmpty()) {
                for (String acl : fullFieldValue) {
                    AclItemDto item = new AclItemDto(1L, acl, type,  AclLevel.document);
                    aclItems.add(item);
                }
            }
        });
        if (!aclItems.isEmpty()) {
            fileCatAclService.updateAcls(result, aclItems, false);
        }
    }
}
