package com.cla.filecluster.service.files.managers;

import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.entity.CatFileExtractionService;
import com.cla.filecluster.util.executors.BlockingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Itay on 10/01/2016.
 */
@Service("catFileExtractionAppService")
public class CatFileExtractionAppService {
    private static final Logger logger = LoggerFactory.getLogger(CatFileExtractionAppService.class);

    @Autowired
    private BlockingExecutor utilBlockingExecutor;

    @Value("${create.extraction.file.page.size:5000}")
    private int createExtractionFilePageSize;

    @Autowired
    private OpStateService opStateService;

    @Value("${categories.filesBetweenCommits:1000}")
    private int filesBetweenCommits;

    @Autowired
    private CatFileExtractionService catFileExtractionService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private JobManagerService jobManagerService;

    public void updateCatFilesBizListExtractions(final Long bizListId, Long jobId) {
        jobManagerService.setOpMsg(jobId,"Update extraction indexes data - scan extractions");

        int offset = 0;
        int found;
        int addedSinceLastCommit = 0;
        do {
            Set<Long> allClaFileIds = catFileExtractionService.getAllExtractionsFileIds(bizListId, createExtractionFilePageSize, offset);
            if (allClaFileIds == null || allClaFileIds.size() == 0) {
                logger.info("No claFileIds found in extractions tables");
                found = 0;
            } else {
                found = allClaFileIds.size();
                logger.trace("handling cycle of files for extraction offset {} page size {} found {}", offset, createExtractionFilePageSize, found);
                jobManagerService.updateJobCounters(jobId, 0, allClaFileIds.size());
                final List<Long> claFileIdsCopy = new ArrayList<>(allClaFileIds);

                utilBlockingExecutor.execute(() ->
                        catFileExtractionService.updateBizListExtractionsOnSolr(bizListId, claFileIdsCopy, null));

                String hint = (claFileIdsCopy.size() > 0) ? String.valueOf(claFileIdsCopy.get(0)) : "empty-sub-list";
                opStateService.incrementCurrentPhaseCounter(claFileIdsCopy.size(), hint);
                addedSinceLastCommit += claFileIdsCopy.size();
                if (addedSinceLastCommit > filesBetweenCommits) {
                    fileCatService.softCommitNoWaitFlush();
                    addedSinceLastCommit = 0;
                }
            }
            offset += createExtractionFilePageSize;
        } while (found > 0);

        jobManagerService.setOpMsg(jobId,"Update extraction indexes data (wait for remaining items)");
        utilBlockingExecutor.blockUntilProcessingFinished();

        jobManagerService.setOpMsg(jobId,"Optimizing indexes");
        fileCatService.commitToSolr();
        fileCatService.optimize();

        jobManagerService.setOpMsg(jobId,"Indexes optimization completed");
    }
}
