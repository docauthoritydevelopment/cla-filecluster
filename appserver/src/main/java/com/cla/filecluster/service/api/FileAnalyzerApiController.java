package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.pv.AnalyzeType;
import com.cla.common.domain.dto.pv.SimilaritySimulationContext;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.analyze.AnalyzeAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * Created by uri on 04/05/2016.
 */
@RestController()
@RequestMapping("/api")
public class FileAnalyzerApiController {

    private Logger logger = LoggerFactory.getLogger(FileAnalyzerApiController.class);

    private final static Pattern commaSeparatorPattern = Pattern.compile(",");

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AnalyzeAppService analyzeAppService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value="/analyze/sim/file/{fileId}",method={RequestMethod.GET})
    public String simulateClaFileAnalysis(@PathVariable long fileId) {
        try {
            Long contentId = fileCatService.getClaFileContentId(fileId);
            logger.info("simulateClaFileAnalysis file: {}/{}", fileId, contentId);
            SimilaritySimulationContext simulationContext = new SimilaritySimulationContext(AnalyzeType.SIMULATION, false, false);
            Collection<Long> matches = analyzeAppService.simulateClaFileContentAnalysis(contentId, simulationContext);
            StringBuilder sb = new StringBuilder();
            dumpMatches(sb, matches);
            return sb.toString();
        } catch (Exception e) {
            logger.error("error simulate analysis for file {}",fileId, e);
            throw new BadRequestException(messageHandler.getMessage("operation.failed"), BadRequestType.OPERATION_FAILED);
        }
    }

    private void dumpMatches(Writer writer, Collection<Long> matches) throws IOException {
        dumpMatches(writer, matches, null);
    }
    private void dumpMatches(StringBuilder sb, Collection<Long> matches)  {
        dumpMatches(sb, matches, null, null);
    }
    private void dumpMatches(Writer writer, Collection<Long> matches, Long highlight) throws IOException {
        final StringBuilder sb = new StringBuilder();
        dumpMatches(sb, matches, highlight, null);
        writer.write(sb.toString());
    }

    private void dumpMatches(Writer writer, Collection<Long> matches, Long highlight, Map<Long, Integer> simulationMatchesResultMap) throws IOException {
        final StringBuilder sb = new StringBuilder();
        dumpMatches(sb, matches, highlight, simulationMatchesResultMap);
        writer.write(sb.toString());
    }

    private void dumpMatches(StringBuilder sb, Collection<Long> matches, Long highlight, Map<Long, Integer> simulationMatchesResultMap) {
        if (matches==null || matches.isEmpty()) {
            sb.append("<null><br/>\n");
            return;
        }
        int counter = 0;
        for (Long m : matches) {
            if (m.equals(highlight)) {
                sb.append("<strong>").append(m.toString()).append("</strong>");
            }
            else {
                sb.append(m.toString());
            }
            if (simulationMatchesResultMap != null) {
                Integer res = simulationMatchesResultMap.get(m);
                if (res != null) {
                    sb.append(":").append(res);
                }
            }
            sb.append(", ");
            if ((++counter % 20) == 0) {
                sb.append("<br/>\n");
            }
        }
        if ((counter % 20) != 0) {
            sb.append("<br/>\n");
        }
    }

    @RequestMapping(value="/analyze/sim/files/{files}",method={RequestMethod.GET})
    public String simulateClaFileAnalysisMany(@PathVariable String files) {
        try {
            List<Long> fileIds;
            try {
                fileIds = Arrays.stream(commaSeparatorPattern.split(files)).map(Long::parseLong).collect(Collectors.toList());
            } catch (Exception e) {
                logger.debug("Error: can not convert '{}' to list of longs. Ingoring", files);
                return "Error: can not convert '" + files + "' to list of longs. Ingored.";
            }
            logger.info("simulateClaFileAnalysis file: {}", files);
            Collection<Long> allMatches = new HashSet<>();
            StringBuilder sb = new StringBuilder();
            fileIds.forEach(f -> {
                Long contentId = fileCatService.getClaFileContentId(f);
                SimilaritySimulationContext simulationContext = new SimilaritySimulationContext(AnalyzeType.SIMULATION, false, false);
                Collection<Long> matches = analyzeAppService.simulateClaFileContentAnalysis(contentId, simulationContext);
                allMatches.addAll(matches);
            });
            dumpMatches(sb, allMatches);
            return sb.toString();
        } catch (Exception e) {
            logger.error("error simulate analysis for files {}",files, e);
            throw new BadRequestException(messageHandler.getMessage("operation.failed"), BadRequestType.OPERATION_FAILED);
        }
    }

    @RequestMapping(value="/analyze/sim/connection/{fileId1}/{fileId2}",method={RequestMethod.GET})
    public void simulateClaFileAnalysisConnection(@PathVariable Long fileId1, @PathVariable Long fileId2,
                                                  @RequestParam(defaultValue="50000") final int limit,
                                                  @RequestParam(value="disqualified",defaultValue="false") final boolean showDisqualified,
                                                  @RequestParam(value="asymmetric",defaultValue="false") final boolean showAsymmetric,
                                                  HttpServletRequest request,
                                                  HttpServletResponse response) {
        HttpSession httpSession = request.getSession();
        httpSession.setMaxInactiveInterval(24*60*60);		// increase session timeout before reset to 1 day (not working - probably need to add  something like public class HttpSessionChecker implements HttpSessionListener )
        try {
            simulateClaFileAnalysisConnection(fileId1, fileId2, limit, showDisqualified, showAsymmetric, response);
        } catch (Exception e) {
            logger.error("error simulate analysis connection for files {}, {}",fileId1, fileId2, e);
            throw new BadRequestException(messageHandler.getMessage("operation.failed"), BadRequestType.OPERATION_FAILED);
        }
    }

    private void simulateClaFileAnalysisConnection(Long fileId1, Long fileId2,
                                                  final int limit,
                                                  final boolean showDisqualified,
                                                  final boolean showAsymmetric,
                                                  HttpServletResponse response) throws Exception {

        Long contentId1 = fileCatService.getClaFileContentId(fileId1);
        Long contentId2 = fileCatService.getClaFileContentId(fileId2);

        logger.debug("simulateClaFileAnalysisConnection(fileId1={}/{}, fileId2={}/{}, limit={},showDisqualified={},showAsymmetric={})",
                fileId1, contentId1, fileId2, contentId2, limit, showDisqualified, showAsymmetric);

        File file = new File("./logs/simDeb_"+fileId1+"_"+fileId2+".html");
        Writer fWriter = new PrintWriter(file, "utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        response.flushBuffer();
        Writer rWriter = response.getWriter();
        StringBuilder sb1 = new StringBuilder();

        String header = "<!DOCTYPE html>"
                + "<html>"
                + "<head>"
                + "<meta charset='utf-8'>"
                + "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
                + "<title>DocAuthority | Debug "
                + fileId1 + "->" + fileId2
                + "</title>"
                + "</head>\n"
                + "<body>\n";

        sb1.append(header)
            .append("<h2>AnalyzeSimConnection from ")
            .append(fileId1.toString())
            .append(" to ")
            .append(fileId2.toString())
            .append("</h2>\n");

        fWriter.write(sb1.toString());
        fWriter.flush();

        rWriter.write(sb1.toString());
        rWriter.write(" <a href='file:///"+file.getAbsolutePath()+"' target='_blank'>Detailed report</a><br/>\n");
        rWriter.flush();
        response.flushBuffer();

        final List<Long> checked = new ArrayList<>();
        final List<Long> checkedFid = new ArrayList<>();
        final Queue<Long> matchesToCheckQueue = new LinkedList<>();
        final Map<Long,Integer> countersMap = new HashMap<>();
        final Map<Long,Integer> matcheSizeMap = new HashMap<>();
        final Map<Long,List<Long>> matchingGraphMap = new HashMap<>();
        final Map<Long,Integer> asymmetricMatchesCountMap = new HashMap<>();
        boolean shortestRouteFound = false;

        matchesToCheckQueue.offer(contentId1);
        int counter = 0;

        while ((counter++)<limit && (!matchesToCheckQueue.isEmpty()) && (!checked.contains(contentId2)) && (!matchesToCheckQueue.contains(contentId2))) {
            Long cidToCheck = matchesToCheckQueue.poll();
            if (!checked.contains(cidToCheck)) {
                checked.add(cidToCheck);
                checkedFid.add((Objects.equals(cidToCheck, contentId1)) ? fileId1 :
                        ((Objects.equals(cidToCheck, contentId2)) ? fileId2 : getFileByContentId(cidToCheck)));
                SimilaritySimulationContext simulationContext = new SimilaritySimulationContext(AnalyzeType.SIMULATION, showDisqualified, false);
//                Long contentId = getContentId(fidToCheck);
                final Collection<Long> matches = analyzeAppService.simulateClaFileContentAnalysis(contentId1, simulationContext);

                int disqualifiedCount = simulationContext.getSimulationDisqualifiedCount();
                Map<Long, Integer> simulationMatchesResultMap = simulationContext.getSimulationMatchesResultMap();
//	        	String ingestDetails = groupsApplicationService.dumpIngestSimpleDataForFile(fidToCheck);
                fWriter.write("Added by <strong>");
                fWriter.write(cidToCheck.toString());
                fWriter.write("</strong> ("+matches.size()+"/"+disqualifiedCount+"):");
                if (simulationMatchesResultMap!=null) {
                    Map<Integer, Long> simulationMatchesResultCountsMap = simulationMatchesResultMap.entrySet().stream().collect(Collectors.groupingBy(Map.Entry::getValue, Collectors.counting()));
                    String simulationMatchesResultCountsMapStr = simulationMatchesResultCountsMap.entrySet().stream()
                            .map(e->(String.valueOf(e.getKey()+":"+e.getValue()))).collect(Collectors.joining(",", " simStats=(", ")"));
                    fWriter.write(simulationMatchesResultCountsMapStr);
                }
                fWriter.write("<br/>\n");
//	        	fWriter.write("</strong> ("+matches.size()+"): <small>");
//	        	fWriter.write(ingestDetails);
//	        	fWriter.write("</small><br/>\n");
                dumpMatches(fWriter, matches, contentId2, simulationMatchesResultMap);

                if (showDisqualified) {
                    Collection<Long> simulationDisqualifiedList = simulationContext.getSimulationDisqualifiedList();
                    if (simulationDisqualifiedList != null) {
                        fWriter.write("Disqualified ("+matches.size()+"/"+simulationDisqualifiedList.size()+"):<br/>\n");
                        dumpMatches(fWriter, simulationDisqualifiedList);
                        fWriter.write("<br/>\n");
                    }
                }

                // Add all matches that were not already checked to the matching list
                final int matchesToCheckBeforeNew = matchesToCheckQueue.size();
                matches.stream().filter(m->!checked.contains(m)).filter(m->!matchesToCheckQueue.contains(m)).forEach(matchesToCheckQueue::offer);
                matches.forEach(m->{
                    // count instances
                    Integer c = countersMap.get(m);
                    if (c == null) c = 0;
                    c++;
                    countersMap.put(m, c);
                });
                matcheSizeMap.put(cidToCheck, matches.size());
                final int matchesToCheckAfterNew = matchesToCheckQueue.size();

                if (logger.isDebugEnabled()) {
                    logger.debug("analyzeSimConnection: matched {}. Got {} matches. Added {} files to match. More {} to check",
                            cidToCheck, matches.size(), matchesToCheckBeforeNew,
                            (matchesToCheckAfterNew - matchesToCheckBeforeNew), matchesToCheckAfterNew);
                }
                fWriter.flush();

                if (showAsymmetric) {
                    matchingGraphMap.put(cidToCheck, Lists.newArrayList(matches));
                }

                simulationContext.clear();
            }
        }
        sb1 = new StringBuilder();
        if (counter >= limit) {
            logger.debug("analyzeSimConnection: matches count limit exceeded {}", counter);
            sb1.append("matches count limit exceeded ");
            sb1.append(String.valueOf(counter));
            sb1.append("<br/>\n");
        }

        if (showAsymmetric) {
            // Find shortest path from destination to source (possible only if all matches were kept as full connection graph)
            if (counter < limit) {	// can not find route if stop before reaching destination
                final Map <Long, Integer> routeId2DistMap = new HashMap<>();
                final Map <Integer,List<Long>> routeDist2IdsMap = new HashMap<>();
                Integer currDist = 0;
                addToRouteMaps(routeId2DistMap, routeDist2IdsMap, contentId2, currDist);

                // Loop until we get to the source id - mark each node in route with its distance from destination
                while (routeDist2IdsMap.get(currDist) != null && !routeDist2IdsMap.get(currDist).contains(contentId1) && currDist <= checked.size()) {
                    List <Long> listToProcess = routeDist2IdsMap.get(currDist);
                    currDist++;
                    for (Long id : listToProcess) {
                        for (Map.Entry<Long, List<Long>> e : matchingGraphMap.entrySet()) {
                            if (e.getValue().contains(id)) {
                                // Mark the entry key as having distance currDist from destination
                                addToRouteMaps(routeId2DistMap, routeDist2IdsMap, e.getKey(), currDist);
                            }
                        }
                    }
                }
                if (routeDist2IdsMap.get(currDist) == null || !routeDist2IdsMap.get(currDist).contains(contentId1)) {
                    logger.info("Error: Chain seems to be broken. Could not get from {} to {}", contentId1, contentId2);
                    fWriter.write("<br/>\nError: Chain seems to be broken. Could not get from destination to source<br/>\n");
                }
                else {
                    // Build route from source to destination
                    List<Long> route = new ArrayList<>();
                    List<Long> routeFids = new ArrayList<>();
                    Long currNode = contentId1;
                    route.add(currNode);
                    routeFids.add(fileId1);
                    while (!currNode.equals(contentId2) && currDist >= 0) {
                        currDist--;	// protect against endless loop in case chain is broken
                        List<Long> currNodeMatches = matchingGraphMap.get(currNode);
                        if (currNodeMatches==null) {
                            fWriter.write("<br/>\nError: could not find currNodeMatches. route="+ route.toString() + "<br/>\n");
                            break;
                        }
                        // Get the node with the shortest distance to destination
                        Long nextNode = currNodeMatches.stream()
                                .filter(id->(routeId2DistMap.get(id)!=null))
                                .min(Comparator.comparingInt(routeId2DistMap::get)).orElse(null);
                        if (nextNode==null) {
                            //
                            fWriter.write("<br/>\nError: could not find next node. route="+ route.toString() + "<br/>\n");
                            break;
                        }
                        currNode = nextNode;
                        route.add(currNode);
                        routeFids.add((Objects.equals(currNode, contentId2)) ? fileId2 : getFileByContentId(currNode));
                    }
                    shortestRouteFound = true;
                    logger.debug("analyzeSimConnection: found shortest route with {} items out of {}", route.size(), checked.size()+1);
                    fWriter.write("<h4>Shortest route: ("+route.size()+")</h4>");
                    String ingestDetails = groupsApplicationService.dumpIngestSimpleDataForFiles(routeFids, matcheSizeMap, countersMap, asymmetricMatchesCountMap, true);
                    fWriter.write(ingestDetails);
                }
            }	// shortest route

            fWriter.write("<h4>Asymmetric Matches:</h4>\n");
            for (Long k : matchingGraphMap.keySet()) {
                List<Long> asymMatches = matchingGraphMap.get(k).stream().filter(m->{
                    List<Long> l = matchingGraphMap.get(m);
                    return (l !=null && !l.contains(k));
                }).collect(Collectors.toList());
                if (!asymMatches.isEmpty()) {
                    fWriter.write(k.toString() + ": (" + asymMatches.size() + ")<br/>\n");
                    dumpMatches(fWriter, asymMatches);
                    asymmetricMatchesCountMap.put(k, asymMatches.size());
                }
            }
        }

        fWriter.write("<h4>Matched: ("+checked.size()+")</h4>");
        checked.add(contentId2);
        checkedFid.add(fileId2);
        String ingestDetails = groupsApplicationService.dumpIngestSimpleDataForFiles(checkedFid, matcheSizeMap, countersMap, showAsymmetric ?asymmetricMatchesCountMap : null, !shortestRouteFound);
        fWriter.write(ingestDetails);
//        dumpMatches(sb1, checked, fileId2);
        sb1.append("<h4>Left to be matched:(").append(matchesToCheckQueue.size()).append(")</h4>\n");
        fWriter.write(sb1.toString());
        rWriter.write(sb1.toString());

        ingestDetails = groupsApplicationService.dumpIngestSimpleDataForFiles((LinkedList<Long>)matchesToCheckQueue, matcheSizeMap, countersMap);
        fWriter.write(ingestDetails);

        dumpMatches(rWriter, matchesToCheckQueue, contentId2);

        sb1 = new StringBuilder();
        sb1.append("</body></html>");

        fWriter.write(sb1.toString());
        fWriter.flush();
        fWriter.close();
        rWriter.write(sb1.toString());
        rWriter.flush();
        rWriter.close();
        response.flushBuffer();
    }

    private Long getFileByContentId(Long cId) {
        List<SolrFileEntity> files = fileCatService.findFilesByContentId(cId);
        if (files == null || files.isEmpty()) {
            return null;
        }
        return files.get(0).getFileId();
    }

    private void addToRouteMaps(Map<Long, Integer> routeId2DistMap, Map<Integer, List<Long>> routeDist2IdsMap,
                                Long id, Integer dist) {

        Integer prevDist = routeId2DistMap.get(id);
        if (prevDist != null && prevDist < dist) {
            return;
        }
        routeId2DistMap.put(id, dist);
        List<Long> l = routeDist2IdsMap.computeIfAbsent(dist, k -> Lists.newArrayList());
        l.add(id);
    }


}
