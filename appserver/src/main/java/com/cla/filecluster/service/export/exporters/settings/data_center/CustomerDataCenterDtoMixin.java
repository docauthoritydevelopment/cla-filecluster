package com.cla.filecluster.service.export.exporters.settings.data_center;

import com.cla.filecluster.service.export.serialize.DateToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;

import static com.cla.filecluster.service.export.exporters.settings.data_center.CustomerDataCenterHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class CustomerDataCenterDtoMixin {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(LOCATION)
    private String location;

    @JsonProperty(DESCRIPTION)
    private String description;

    @JsonSerialize(using = DateToExcelDateSerializer.class)
    @JsonProperty(CREATED)
    private Date createdOnDB;

    @JsonProperty(DEFAULT)
    private boolean defaultDataCenter;
}
