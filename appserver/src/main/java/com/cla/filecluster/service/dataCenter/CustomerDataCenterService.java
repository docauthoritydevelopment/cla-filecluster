package com.cla.filecluster.service.dataCenter;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.components.ClaComponentType;
import com.cla.common.domain.dto.components.ComponentState;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterSummaryInfo;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.dataCenter.CustomerDataCenterRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.repository.jpa.monitor.ClaComponentRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.util.ValidationUtils;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


@Service
public class CustomerDataCenterService {

    @Value("${customer.dataCenter.file:./config/dataCenter/defaultCustomerDataCenter.csv}")
    private String defaultCustomerDataCenterFile;

    @Autowired
    private CustomerDataCenterRepository customerDataCenterRepository;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private ClaComponentRepository claComponentRepository;

    @Autowired
    private RootFolderRepository rootFolderRepository;

    @Autowired
    private MessageHandler messageHandler;

    private Logger logger = LoggerFactory.getLogger(CustomerDataCenterService.class);

    public CustomerDataCenter getBootstrapCustomerDataCenter() {
      return customerDataCenterRepository.findFirstByIsDefault(true);
    }

    @Transactional(readOnly = true)
    public Page<CustomerDataCenterSummaryInfo> listCustomerDataCentersSummary(DataSourceRequest dataSourceRequest) {
        logger.debug("List all customer data centers summary info ");
        Page<CustomerDataCenterDto> customerDataCenterDtos = listCustomerDataCenters(dataSourceRequest);
        List<CustomerDataCenterSummaryInfo> content = new ArrayList<>();
        for (CustomerDataCenterDto customerDataCenterDto : customerDataCenterDtos) {
            CustomerDataCenterSummaryInfo newInfo = new CustomerDataCenterSummaryInfo();
            newInfo.setCustomerDataCenterDto(customerDataCenterDto);
            long rootfoldersCount = rootFolderRepository.countRootfoldersUnderCustomerDataCenter(customerDataCenterDto.getId());
            newInfo.setRootFolderCount(rootfoldersCount);
            int mediaProcessorsCount = claComponentRepository.countByCustomerDataCenterId(customerDataCenterDto.getId(), ClaComponentType.MEDIA_PROCESSOR);
            newInfo.setMediaProcessorCount(mediaProcessorsCount);
            content.add(newInfo);
        }
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<CustomerDataCenterSummaryInfo> summaryInfoList = new PageImpl<>(content, pageRequest, customerDataCenterDtos.getTotalElements());
        return summaryInfoList;
    }

    @Transactional(readOnly = true)
    public Page<CustomerDataCenterDto> listCustomerDataCenters(DataSourceRequest dataSourceRequest) {
        logger.debug("List all customer data centers ");
        Sort sortOption = new Sort(Sort.Direction.ASC, "id");
        PageRequest pageRequest = dataSourceRequest.createPageRequest(sortOption);
        Page<CustomerDataCenter> all = customerDataCenterRepository.findAll(pageRequest);
        return convertCustomerDataCenterItems(all,pageRequest);
    }

    @Transactional(readOnly = true)
    public CustomerDataCenter getCustomerDataCenterById(long id) {
        logger.debug("Get customer data center id {} ",id);
        CustomerDataCenter byId = customerDataCenterRepository.findById(id);
        return byId;
    }

    @Transactional(readOnly = true)
    public Map<Long, String> getCustomerDataCentersNames() {
        List<Object[]> dcs = customerDataCenterRepository.getDataCenterNames();
        Map<Long, String> result = new TreeMap<>();
        for (Object[] dc : dcs) {
            Long id =  ((BigInteger)dc[0]).longValue();
            String name = (String)dc[1];
            result.put(id, name);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public CustomerDataCenterDto getCustomerDataCenter(long id) {
        logger.debug("Get customer data center id {} ",id);

        CustomerDataCenter byId = customerDataCenterRepository.findById(id);
        return convertCustomerDataCenterToDto(byId);
    }

    @Transactional(readOnly = true)
    public CustomerDataCenterDto getCustomerDataCenter(String name) {
        CustomerDataCenter byName = customerDataCenterRepository.findByName(name);
        return convertCustomerDataCenterToDto(byName);
    }

    @Transactional
    public CustomerDataCenterDto updateCustomerDataCenter(CustomerDataCenterDto customerDataCenterDto) {
        logger.debug("Update customer data center to {}", customerDataCenterDto);
        CustomerDataCenter one = customerDataCenterRepository.getOne(customerDataCenterDto.getId());
        customerDataCenterDto.setName(isValidForCustomerDataCenterUpdate(one.getName(),
                customerDataCenterDto.getName(),one.isDefault(),customerDataCenterDto.getDefaultDataCenter()));
        CustomerDataCenter customerDataCenter = convertToCustomerDataCenter(customerDataCenterDto);
        customerDataCenter.setId(one.getId());

        try {
            setPrevUnDefaultIfNeeded(customerDataCenter);
            CustomerDataCenter saved = customerDataCenterRepository.save(customerDataCenter);
            return convertCustomerDataCenterToDto(saved);

        } catch (RuntimeException e) {
            logger.error("Exception while trying to update customer data center "+customerDataCenterDto.getId()+" details: "+e.getMessage(),e);
            throw e;
        }
    }

    @Transactional
    public void deleteCustomerDataCenter(long id) {
        CustomerDataCenter customerDataCenter = customerDataCenterRepository.getOne(id);
        if (customerDataCenter != null) {
            if(customerDataCenter.isDefault()) {
                throw new BadRequestException(messageHandler.getMessage("data-center.delete.default"), BadRequestType.UNAUTHORIZED);
            }
            int mediaProcessorsCount = claComponentRepository.countByCustomerDataCenterId(id, ClaComponentType.MEDIA_PROCESSOR);
            if (mediaProcessorsCount > 0) {
                throw new BadRequestException(messageHandler.getMessage("data-center.delete.has-mp"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
            }
            deleteClaComponent(id);
            customerDataCenterRepository.deleteById(id);
        }
        else {
            throw new BadRequestException(messageHandler.getMessage("data-center.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
    }

    private String isValidForCustomerDataCenterCreation(String name) {
        name = ValidationUtils.validateStringParameter(name, messageHandler.getMessage("data-center.name.empty")
                , messageHandler.getMessage("data-center.name.invalid"));

        CustomerDataCenter customerDataCenter = customerDataCenterRepository.findByName(name);
        if (customerDataCenter != null) {
            logger.warn("Can't create customerDataCenter by name {} - already exists", name);
            throw new BadRequestException(messageHandler.getMessage("data-center.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
        return name;
    }

    private String isValidForCustomerDataCenterUpdate(String name, String newName, boolean isDefaultDataCenter,boolean newIsDefaultDataCenter) {
        if(!name.equalsIgnoreCase(newName)) {
            newName = ValidationUtils.validateStringParameter(newName, messageHandler.getMessage("data-center.name.empty")
                    , messageHandler.getMessage("data-center.name.invalid"));

            CustomerDataCenter customerDataCenter = customerDataCenterRepository.findByName(newName);
            if (customerDataCenter != null) {
                logger.warn("Can't update customerDataCenter with name {} - already exists", newName);
                throw new BadRequestException(messageHandler.getMessage("data-center.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
        if(isDefaultDataCenter && !newIsDefaultDataCenter) {
            throw new BadRequestException(messageHandler.getMessage("data-center.default.failure"), BadRequestType.UNSUPPORTED_VALUE);
        }
        return newName;
    }

    @Transactional
    public CustomerDataCenterDto createCustomerDataCenter(CustomerDataCenterDto customerDataCenterDto) {
        logger.debug("Creating new data center  ",customerDataCenterDto);

        customerDataCenterDto.setName(isValidForCustomerDataCenterCreation(customerDataCenterDto.getName()));
        try {
            CustomerDataCenter newDataCenter = convertToCustomerDataCenter(customerDataCenterDto);
            setPrevUnDefaultIfNeeded(newDataCenter);
            CustomerDataCenter customerDataCenter = customerDataCenterRepository.save(newDataCenter);
            createClaComponent(customerDataCenter);
            return convertCustomerDataCenterToDto(customerDataCenter);
        }
        catch (Exception e) {
            logger.error("Failed to create customer data center",e);
            throw new BadRequestException(messageHandler.getMessage("data-center.create.failure", Lists.newArrayList(e)), BadRequestType.OPERATION_FAILED);
        }
    }

    @Transactional(readOnly = true)
    public boolean isCustomerDataCenterExists(String name) {
        CustomerDataCenter byName = customerDataCenterRepository.findByName(name);
        return byName != null;
    }

    private void setPrevUnDefaultIfNeeded(CustomerDataCenter newDataCenter) {
        if(newDataCenter.isDefault()) {
            logger.debug("Updated/New data center {} is set as default one. Restrict to only one default",newDataCenter.getName());
            CustomerDataCenter firstIsDefault = customerDataCenterRepository.findFirstByIsDefault(true);
            if(firstIsDefault != null && firstIsDefault.getId() != newDataCenter.getId()) { //At first initation the first is null
                firstIsDefault.setDefault(false);
                logger.info("Found existing default customer data center, set it as not default: {} ",firstIsDefault.getName());
                customerDataCenterRepository.save(firstIsDefault);
            }
        }
    }

    public static CustomerDataCenter convertToCustomerDataCenter(CustomerDataCenterDto customerDataCenterDto) {
        CustomerDataCenter dataCenter = new CustomerDataCenter();
        dataCenter.setName(customerDataCenterDto.getName());
        dataCenter.setDescription(customerDataCenterDto.getDescription());
        dataCenter.setLocation(customerDataCenterDto.getLocation());
        dataCenter.setDefault(customerDataCenterDto.getDefaultDataCenter());
        dataCenter.setPublicKey(customerDataCenterDto.getPublicKey());
        dataCenter.setIngestTaskExpireTime(customerDataCenterDto.getIngestTaskExpireTime());
        dataCenter.setIngestTaskRetryTime(customerDataCenterDto.getIngestTaskRetryTime());
        dataCenter.setIngestTasksQueueLimit(customerDataCenterDto.getIngestTasksQueueLimit());
        return dataCenter;
    }

    /**
     * Create the default components as part of a database reset
     */
    public void createDefaultComponents() { //TODO: read csv via import csv
        logger.debug("Create preset customer data centers");
        if (defaultCustomerDataCenterFile == null || defaultCustomerDataCenterFile.isEmpty()) {
            logger.info("No default customer data center file defined - ignored");
        }
        logger.info("Load default customer data center from file {}", defaultCustomerDataCenterFile);
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, defaultCustomerDataCenterFile, new String[]{});
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(defaultCustomerDataCenterFile, row -> createCustomerDataCenterFromCsvRow(row, headersLocationMap), null, null);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load customer data center - problem in csv file "+e.getMessage(),e);
        }
        if (failedRows > 0) {
            throw new RuntimeException("Failed to load "+failedRows+" customer data center - problem in csv file");
        }
    }

    private void createClaComponent(CustomerDataCenter dc) {
        ClaComponent claComponent = new ClaComponent();
        claComponent.setActive(true);
        claComponent.setClaComponentType(ClaComponentType.BROKER_DC);
        claComponent.setComponentType(ClaComponentType.BROKER_DC.name());
        claComponent.setInstanceId(String.valueOf(dc.getId()));
        claComponent.setState(ComponentState.UNKNOWN);
        claComponent.setCustomerDataCenter(dc);
        claComponentRepository.save(claComponent);
    }

    private void deleteClaComponent(Long dcId) {
        ClaComponent claComponent = claComponentRepository.findComponentByInstanceId(ClaComponentType.BROKER_DC, String.valueOf(dcId));
        if (claComponent != null) {
            claComponentRepository.delete(claComponent);
        }
    }

    private void createCustomerDataCenterFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
        customerDataCenterDto.setName(fieldsValues[headersLocationMap.get("name")].trim());
        String isDefault = fieldsValues[headersLocationMap.get("isDefault")].trim();
        customerDataCenterDto.setDefaultDataCenter(Boolean.valueOf(isDefault));
        customerDataCenterDto.setDescription(fieldsValues[headersLocationMap.get("description")].trim());
        customerDataCenterDto.setLocation(fieldsValues[headersLocationMap.get("location")].trim());

        logger.debug("Create Scheduling Group {}",customerDataCenterDto);
        createCustomerDataCenter(customerDataCenterDto);
    }

    public static Page<CustomerDataCenterDto> convertCustomerDataCenterItems(Page<CustomerDataCenter> listPage, PageRequest pageRequest) {
        List<CustomerDataCenterDto> content = new ArrayList<>();
        for (CustomerDataCenter item : listPage.getContent()) {
            content.add(convertCustomerDataCenterToDto(item));
        }
        return new PageImpl<>(content, pageRequest, listPage.getTotalElements());
    }

    public static CustomerDataCenterDto convertCustomerDataCenterToDto(CustomerDataCenter customerDataCenter) {
        if (customerDataCenter != null) {
            CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
            customerDataCenterDto.setId(customerDataCenter.getId());
            customerDataCenterDto.setName(customerDataCenter.getName());
            customerDataCenterDto.setCreatedOnDB(customerDataCenter.getCreatedOnDB());
            customerDataCenterDto.setLocation(customerDataCenter.getLocation());
            customerDataCenterDto.setDescription(customerDataCenter.getDescription());
            customerDataCenterDto.setDefaultDataCenter(customerDataCenter.isDefault());
            customerDataCenterDto.setPublicKey(customerDataCenter.getPublicKey());
            customerDataCenterDto.setIngestTaskExpireTime(customerDataCenter.getIngestTaskExpireTime());
            customerDataCenterDto.setIngestTaskRetryTime(customerDataCenter.getIngestTaskRetryTime());
            customerDataCenterDto.setIngestTasksQueueLimit(customerDataCenter.getIngestTasksQueueLimit());
            return customerDataCenterDto;
        }
        return null;
    }
}
