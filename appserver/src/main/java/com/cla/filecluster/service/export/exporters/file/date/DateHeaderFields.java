package com.cla.filecluster.service.export.exporters.file.date;

public interface DateHeaderFields {
    String NAME = "Name";
    String START_DATE = "Start date";
    String NUM_OF_FILTERED = "Number of filtered files";
    String NUM_OF_FILES = "Number of files";
}
