package com.cla.filecluster.service.files.filetree;

import com.cla.filecluster.service.importEntities.SchemaData;

/**
 * Created by liora on 21/03/2017.
 */
public class RootfolderSchemaData extends SchemaData<RootfolderItemRow>
{
    @Override
    public RootfolderItemRow createImportItemRowInstance() {
        return new RootfolderItemRow();
    }

}
