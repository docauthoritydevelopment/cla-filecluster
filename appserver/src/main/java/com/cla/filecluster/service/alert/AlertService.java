package com.cla.filecluster.service.alert;

import com.cla.common.domain.dto.alert.AlertDto;
import com.cla.common.domain.dto.alert.AlertSeverity;
import com.cla.common.domain.dto.event.EventType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.alert.Alert;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.policy.PolicyMappings;
import com.cla.filecluster.policy.domain.PolicyConfig;
import com.cla.filecluster.policy.domain.dto.ActionPluginsDefinition;
import com.cla.filecluster.policy.domain.entity.Policy;
import com.cla.filecluster.policy.plugin.transformer.AlertTransformer;
import com.cla.filecluster.policy.repository.jpa.PolicyRepository;
import com.cla.filecluster.repository.jpa.alert.AlertRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * service for alerts management (CRUD)
 *
 * Created by: yael
 * Created on: 1/15/2018
 */
@Service
public class AlertService {

    @Autowired
    private AlertRepository alertRepository;

    @Autowired
    private PolicyMappings policyMappings;

    @Autowired
    private PolicyRepository policyRepository;

    @Autowired
    private MessageHandler messageHandler;

    private Logger logger = LoggerFactory.getLogger(AlertService.class);

    @Transactional(readOnly = true)
    public Page<AlertDto> getAllAlerts(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        return convertAlertsPage(alertRepository.findAll(pageRequest));
    }

    @Transactional(readOnly = true)
    public Page<AlertDto> getNotAcknowledgedAlerts(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        return convertAlertsPage(alertRepository.findAllNotAcknowledged(pageRequest));
    }

    @Transactional(readOnly = true)
    public Page<AlertDto> getNotAcknowledgedBySeverity(DataSourceRequest dataSourceRequest, AlertSeverity severity) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Alert> alerts = alertRepository.findAllNotAcknowledgedBySeverity(severity, pageRequest);
        return convertAlertsPage(alerts);
    }

    @Transactional(readOnly = true)
    public Page<AlertDto> getNotAcknowledgedByType(DataSourceRequest dataSourceRequest, EventType type) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Alert> alerts = alertRepository.findAllNotAcknowledgedByType(type, pageRequest);
        return convertAlertsPage(alerts);
    }

    @Transactional(readOnly = true)
    public Page<AlertDto> getNotAcknowledgedBySeverityAndType(DataSourceRequest dataSourceRequest, AlertSeverity severity, EventType type) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Alert> alerts = alertRepository.findAllNotAcknowledgedBySeverityAndType(severity, type, pageRequest);
        return convertAlertsPage(alerts);
    }

    @Transactional
    public void setAlertAcknowledged(Long id, boolean acknowledged) {
        Alert alert = alertRepository.getById(id);
        if (alert == null) {
            throw new BadRequestException(messageHandler.getMessage("alert.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (alert.isAcknowledged() != acknowledged) {
            alert.setAcknowledged(acknowledged);
            alertRepository.save(alert);
        }
    }

    @Transactional
    public PolicyConfig changeAlertSeverity(EventType eventType, AlertSeverity alertSeverity) {
        Policy alertPolicy = policyRepository.getByName("Alert Policy");
        if (alertPolicy != null) {
            PolicyConfig policyConfig = alertPolicy.getPolicyConfig();
            Optional<ActionPluginsDefinition> actionPluginsDefinitionOptional = policyConfig.getActionPluginsDefinitions().stream()
                    .filter(apd -> apd.getPluginInputTransformer() instanceof AlertTransformer)
                    .findFirst();
            if (actionPluginsDefinitionOptional.isPresent()) {
                AlertTransformer alertTransformer = (AlertTransformer) actionPluginsDefinitionOptional.get().getPluginInputTransformer();
                alertTransformer.getSeverityMappings().put(eventType, alertSeverity);
                policyRepository.save(alertPolicy);
                policyMappings.reloadPolicies();
                return policyConfig;
            }
        }
        return null;
    }

    public static Page<AlertDto> convertAlertsPage(Page<Alert> alerts) {
        List<AlertDto> content = convertAlerts(alerts.getContent());
        Pageable pageable = PageRequest.of(alerts.getNumber(), alerts.getSize());
        return new PageImpl<>(content, pageable, alerts.getTotalElements());
    }

    private static List<AlertDto> convertAlerts(List<Alert> alerts) {
        List<AlertDto> result = new ArrayList<>();
        for (Alert alert : alerts) {
            result.add(convertAlert(alert));
        }
        return result;
    }

    private static AlertDto convertAlert(Alert alert) {
        AlertDto dto = new AlertDto();
        dto.setId(alert.getId());
        dto.setDateCreated(alert.getDateCreated());
        dto.setMessage(alert.getMessage());
        dto.setDescription(alert.getDescription());
        dto.setEventType(alert.getEventType());
        dto.setSeverity(alert.getSeverity());
        dto.setAcknowledged(alert.isAcknowledged());
        return dto;
    }
}
