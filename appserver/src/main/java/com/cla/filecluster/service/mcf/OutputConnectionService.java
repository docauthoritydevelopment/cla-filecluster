package com.cla.filecluster.service.mcf;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Deprecated
@Service
public class OutputConnectionService {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OutputConnectionService.class);

//	@Value("classpath:mcf/outputConnection.json")
//	private Resource outputConnection;

	private String outputConnectionTemplate;

	private HttpHost httpHost;

	@Value("${clusterOutputConnectionName:cla-cluster-output-connector}")
	private String clusterOutputConnectionName;
	
	@Value("${fetcherOutputConnectionName:cla-fetcher-output-connector}")
	private String fetcherOutputConnectionName;
	
	
	
	
	

	private boolean claOutputCollectorCreated;

	public boolean isClaOutputCollectorCreated() {
		return claOutputCollectorCreated;
	}
	
	

	public void setClaOutputCollectorCreated(final boolean claOutputCollectorCreated) {
		this.claOutputCollectorCreated = claOutputCollectorCreated;
	}



	public String getClusterOutputConnectionName() {
		return clusterOutputConnectionName;
	}
	
	



	public String getFetcherOutputConnectionName() {
		return fetcherOutputConnectionName;
	}


    @Value("${subprocess:false}")
    private boolean subProcess;

	@PostConstruct
	private void init() throws IOException {
        if (subProcess) {
            return;
        }
//		httpHost = new HttpHost("localhost", 8345, "http");
//		try (InputStream is = outputConnection.getInputStream()) {
//			outputConnectionTemplate = IOUtils.toString(is);
//		}
	}
//	@PostConstruct
//	private void init() throws IOException {
//		httpHost = new HttpHost("localhost", 8345, "http");
//		try (InputStream is = outputConnection.getInputStream()) {
//			outputConnectionTemplate = IOUtils.toString(is);
//		}
//	}

	public String findAll() {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			final HttpGet getRequest = new HttpGet("/mcf-api-service/json/outputconnections");
			final HttpResponse response = httpclient.execute(httpHost, getRequest);

			if (response.getStatusLine().getStatusCode() != 200)
				throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			final String outputconnections = EntityUtils.toString(response.getEntity());
			return outputconnections;
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void createClaOutputConnections() {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			createConnector(httpclient,clusterOutputConnectionName,"com.cla.mcf.output.filecluster.FileClusterOutputConnector");
			createConnector(httpclient,fetcherOutputConnectionName,"com.cla.mcf.output.filecluster.FileFetcherOutputConnector");
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}



	private void createConnector(final CloseableHttpClient httpclient,final String connectionConnectionName, final String outputConnectionClass) throws UnsupportedEncodingException, IOException,
			ClientProtocolException {
		final HttpPut putRequest = new HttpPut("/mcf-api-service/json/outputconnections/" + connectionConnectionName);
		final String outputConnection = String.format(outputConnectionTemplate, connectionConnectionName,outputConnectionClass);
		logger.trace("Creating the following outputconnection:\n {}", outputConnection);
		final StringEntity se = new StringEntity(outputConnection);
		se.setContentType("application/json");
		putRequest.setEntity(se);
		final HttpResponse response = httpclient.execute(httpHost, putRequest);

		final int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode < 200 || statusCode >= 300)
			throw new IOException("Failed : HTTP error code : " + statusCode);
		claOutputCollectorCreated = true;
	}
}
