package com.cla.filecluster.service.communication;

import com.cla.filecluster.service.command.BaseCommand;
import com.cla.filecluster.util.template.TemplateContext;

/**
 * Abstract class for all communication commands
 * Created by vladi on 6/23/2015.
 */
abstract class AbstractCommunicationCommand extends BaseCommand<Boolean> {

    TemplateContext bodyTemplate;

    void setBodyTemplate(TemplateContext bodyTemplate) {
        this.bodyTemplate = bodyTemplate;
    }
}
