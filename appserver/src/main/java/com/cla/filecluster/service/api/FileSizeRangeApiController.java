package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileSizePartitionDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.files.FileSizeRangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 5/14/2019
 */
@RestController
@RequestMapping("/api/filesize")
public class FileSizeRangeApiController {

    @Autowired
    private FileSizeRangeService fileSizeRangeService;

    @RequestMapping(value = "/partitions", method = RequestMethod.GET)
    public Collection<FileSizePartitionDto> listFileSizePartitions() {
        return fileSizeRangeService.getPartitions();
    }

    @RequestMapping(value = "/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<FileSizePartitionDto>> getFileSizeFileCount(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return fileSizeRangeService.getFileSizeFileCount(dataSourceRequest);
    }
}
