package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.utils.CollectionsUtils;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Set;

/**
 * lock a group during a change upon it to avoid multiple actions that can ruin each other
 *
 * Created by: yael
 * Created on: 5/27/2018
 */
@Service
public class GroupLockService {

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    // used to lock groups so we dont try to move files for the same raw analysis group at the same time
    private Set<String> subGroupToRawAnalysisGroup;

    @PostConstruct
    void init() {
        subGroupToRawAnalysisGroup = CollectionsUtils.newConcurrentHashSet();
    }

    public boolean isRawAnalysisGroupLockTaken(String groupId) {
        return subGroupToRawAnalysisGroup.contains(groupId);
    }

    // lock group for processing
    public synchronized boolean getLockForRawAnalysisGroup(String groupId) {
        return subGroupToRawAnalysisGroup.add(groupId);
    }

    // release group for processing
    public synchronized boolean releaseLockForRawAnalysisGroup(String groupId) {
        return subGroupToRawAnalysisGroup.remove(groupId);
    }

    public boolean getLockForGroup(String groupId) {
        return fileGroupRepository.getById(groupId)
                .filter(fileGroup -> fileGroup.getGroupType().equals(GroupType.SUB_GROUP))
                .map(fileGroup -> getLockForRawAnalysisGroup(fileGroup.getRawAnalysisGroupId()))
                .orElse(getLockForRawAnalysisGroup(groupId));
    }

    public void releaseLockForGroup(String groupId) {
        fileGroupRepository.getById(groupId)
                .filter(fileGroup -> fileGroup.getGroupType().equals(GroupType.SUB_GROUP))
                .map(fileGroup -> releaseLockForRawAnalysisGroup(fileGroup.getRawAnalysisGroupId()))
                .orElse(releaseLockForRawAnalysisGroup(groupId));
    }
}
