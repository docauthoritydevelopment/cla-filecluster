package com.cla.filecluster.service.security;

import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cla.common.domain.dto.security.Authority;

@RestController
@RequestMapping("/authorities")
@Transactional
public class AuthorityApiController {
	
	
	
	@RequestMapping
	public Authority[] getAuhorities(){
		return Authority.values();
	}


}
