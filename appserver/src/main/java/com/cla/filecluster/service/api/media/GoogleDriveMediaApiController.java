package com.cla.filecluster.service.api.media;

import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.filecluster.service.media.GoogleDriveAppService;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.List;

/**
 * Created by uri on 25/07/2016.
 */
@RestController
@RequestMapping("/api/media/drive")
public class GoogleDriveMediaApiController {

    @Autowired
    private GoogleDriveAppService googleDriveAppService;

    private final Logger logger = LoggerFactory.getLogger(GoogleDriveMediaApiController.class);

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response) {
        String sessionId = request.getSession().getId();
        String authorizeUrl = buildDriveAuthorizeUrl(sessionId);
        logger.debug(authorizeUrl);
        try {
            if (response instanceof Response) {
                ((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
            }
            response.sendRedirect(authorizeUrl);
        } catch (IOException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        } catch (RuntimeException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/login/file", method = RequestMethod.GET)
    public void loginToFile(HttpServletRequest request, HttpServletResponse response) {
        String authorizeUrl = buildDriveAuthorizeUrl("file");
        logger.debug(authorizeUrl);
        try {
            if (response instanceof Response) {
                ((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
            }
            response.sendRedirect(authorizeUrl);
        } catch (IOException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        } catch (RuntimeException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/response", method = {RequestMethod.GET, RequestMethod.POST})
    public String boxOauthResponse(@QueryParam("code") String code,
                                   @QueryParam("state") String state,
                                   @QueryParam("error") String error,
                                   @QueryParam("errorDescription") String errorDescription) {
        logger.debug("Got box Oauth response: {} {} {} {}", code, state, error, errorDescription);
        try {
            if (state.equals("file")) {
                googleDriveAppService.handleDriveOauthResponse(code, true);
            } else {
                googleDriveAppService.fillAuthentication(state);
                googleDriveAppService.handleDriveOauthResponse(code, false);

            }
        } catch (RuntimeException e) {
            logger.error("Failed to handle Box response", e);
            throw e;
        }
        return "OK";
    }

    private String buildDriveAuthorizeUrl(String sessionId) {
        return googleDriveAppService.buildAuthorizeUrl(sessionId);
    }

    @RequestMapping(value = "/server/folders", method = RequestMethod.GET)
    public List<ServerResourceDto> listServerFolders(@RequestParam(required = false) String id) {
        return googleDriveAppService.listServerFolders(id);
    }

}
