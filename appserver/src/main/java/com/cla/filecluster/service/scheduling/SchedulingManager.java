package com.cla.filecluster.service.scheduling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Can stop and start services in which scheduled spring methods ar ran
 * using a parameter
 *
 * Created by: yael
 * Created on: 7/16/2019
 */
@Service
public class SchedulingManager {

    @Autowired
    private List<ControlledScheduling> controlledServices;

    private boolean isSchedulingActive = false;

    public SchedulingManager() {
    }

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
        if (controlledServices != null && !controlledServices.isEmpty()) {
            controlledServices.forEach(controlledScheduling -> controlledScheduling.setSchedulingActive(isSchedulingActive));
        }
    }
}
