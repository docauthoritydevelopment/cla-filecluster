package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePointDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.date.DateRangePartition;
import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.service.categories.FileTypeCategoryService;
import com.cla.filecluster.service.date.DateRangeAppService;
import com.cla.filecluster.service.date.DateRangeService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class CreationDatePartitionEnricher extends DatePartitionEnricher{

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.CREATION_DATE, DatePartitionEnricher.DATE_PARTITION_DUMMY_PREFIX);
    }

    public String getFilterFieldName(){
        return CatFileFieldType.CREATION_DATE.getSolrName();
    }

}
