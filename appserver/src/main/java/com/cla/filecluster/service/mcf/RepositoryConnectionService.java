package com.cla.filecluster.service.mcf;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;

@Deprecated
@Service
public class RepositoryConnectionService {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RepositoryConnectionService.class);
	
//	@Value("classpath:mcf/windowsSharedRepositoryConnection.json")
//	private Resource wsRepositoryConnection;
//
//    @Value("classpath:mcf/fileSystemRepositoryConnection.json")
//    private Resource fsRepositoryConnection;

	private String wsRepositoryConnectionTemplate;

    private String fsRepositoryConnectionTemplate;

    private HttpHost httpHost;
	
	@Value("${mcfHost:localhost}")
	private String mcfHost;
	
	@Value("${mcfPort:8345}")
	private String mcfPort;

    @Value("${subprocess:false}")
    private boolean subProcess;


	public String findAll() {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			final HttpGet getRequest = new HttpGet("/mcf-api-service/json/repositoryconnections");
			final HttpResponse response = httpclient.execute(httpHost,getRequest);

			if (response.getStatusLine().getStatusCode() != 200)
				throw new IOException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			final String repos = EntityUtils.toString(response.getEntity());
			return repos;
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void createWindowsSharedRepositoryConnection(final String name, final String server, final String user, final String password) {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			final HttpPut putRequest = new HttpPut("/mcf-api-service/json/repositoryconnections/"+name);
			final String repositoryConnection = String.format(wsRepositoryConnectionTemplate,name,server,user,password);
			logger.trace("Creating the following repositoryConnection:\n {}",repositoryConnection);
			final StringEntity se = new StringEntity(repositoryConnection);
			se.setContentType("application/json");
			putRequest.setEntity(se);
			final HttpResponse response = httpclient.execute(httpHost,putRequest);

			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode < 200 || statusCode >= 300)
				throw new IOException("Failed : HTTP error code : " + statusCode);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

    public void createFileSystemRepositoryConnection(final String name) {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            final HttpPut putRequest = new HttpPut("/mcf-api-service/json/repositoryconnections/"+name);
            final String repositoryConnection = String.format(fsRepositoryConnectionTemplate,name);
            logger.trace("Creating the following repositoryConnection:\n {}",repositoryConnection);
            final StringEntity se = new StringEntity(repositoryConnection);
            se.setContentType("application/json");
            putRequest.setEntity(se);
            final HttpResponse response = httpclient.execute(httpHost,putRequest);

            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode >= 300)
                throw new IOException("Failed : HTTP error code : " + statusCode);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

}
