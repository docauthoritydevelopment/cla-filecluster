package com.cla.filecluster.service.export.exporters.file.prop;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.label.LabelingAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.acl.AclAggCountDtoMixin;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.NAME;
import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.NUM_OF_FILES;

/**
 * Created by: yael
 * Created on: 7/8/2019
 */
@Slf4j
@Component
public class LabelingExporter extends PageCsvExcelExporter<AggregationCountItemDTO<LabelDto>> {

    private final static String[] HEADER = {NAME, NUM_OF_FILES};

    @Autowired
    private LabelingAppService labelingAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AclAggCountDtoMixin.class);

    }

    @Override
    protected Page<AggregationCountItemDTO<LabelDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<LabelDto>> res = labelingAppService.getLabelFileCounts(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<LabelDto> base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        result.put(NAME, base.getItem().getName());
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.LABELING);
    }
}
