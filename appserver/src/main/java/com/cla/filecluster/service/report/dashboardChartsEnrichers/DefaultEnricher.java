package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import org.springframework.stereotype.Component;


@Component

public class DefaultEnricher implements DefaultTypeDataEnricher {

    public static String DEFAULT_ENRICHER_TYPE="DEFAULT";

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
    }

    @Override
    public String getEnricherType() {
        return DEFAULT_ENRICHER_TYPE;
    }


}