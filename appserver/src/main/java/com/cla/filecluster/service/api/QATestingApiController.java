package com.cla.filecluster.service.api;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.diagnostics.DiagnosticAction;
import com.cla.filecluster.service.diagnostics.DiagnosticService;
import com.cla.filecluster.service.diagnostics.DiagnosticType;
import com.cla.filecluster.service.diagnostics.EntityDiagnosticAction;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.GENERAL_ADMIN;

/**
 * Controller for QA/Automation purposes only, not to be used in regular flows
 * 
 * Created by: yael
 * Created on: 3/7/2019
 */
@RestController
@RequestMapping("/api/qa")
public class QATestingApiController {

    private final static Logger logger = LoggerFactory.getLogger(QATestingApiController.class);

    @Value("${qa-api-enabled:false}")
    private boolean enabled;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private DiagnosticService diagnosticService;

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN})
    @RequestMapping(value = "/runScheduledOperations", method = RequestMethod.POST)
    public void runScheduledOperations() {
        checkEnabled();
        scheduledOperationService.runOperations();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN})
    @RequestMapping(value = "/runDiagnostics", method = RequestMethod.GET)
    public boolean runDiagnostics(@RequestParam Map<String,String> params) {
        checkEnabled();

        if (params != null && !Strings.isNullOrEmpty(params.get("type"))) {
            DiagnosticType type = DiagnosticType.valueOf(params.get("type"));
            DiagnosticAction action = diagnosticService.getActionForType(type);
            if (action != null && (action instanceof EntityDiagnosticAction)) {
                Diagnostic d = ((EntityDiagnosticAction)action).addDiagnosticActionTimeParts(
                        params.get("percentage"),
                        params.containsKey("rootFolderId") ? Long.parseLong(params.get("rootFolderId")) : null,
                        params.get("specificIds"),
                        System.currentTimeMillis());
                diagnosticService.createDiagnosticProcedure(d);
                logger.debug("created diagnostics {} with params {} to run ASAP", type, params);
                return true;
            }
        }
        logger.info("failed to create diagnostics with params {}", params);
        return false;
    }

    private void checkEnabled() {
        if (!enabled) {
            throw new BadRequestException("api disabled", BadRequestType.OPERATION_FAILED);
        }
    }
}
