package com.cla.filecluster.service.export.exporters.file.list.mix_in;

import com.cla.common.domain.dto.bizlist.ExtractionSummaryDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.service.export.exporters.file.list.FileListHeaderFields;
import com.cla.filecluster.service.export.serialize.AssociatedBizListSerializer;
import com.cla.filecluster.service.export.serialize.BizListExtractionSummarySerializer;
import com.cla.filecluster.service.export.serialize.DateToExcelDateSerializer;
import com.cla.filecluster.service.export.serialize.DocTypesSerializer;
import com.cla.filecluster.service.export.serialize.FunctionalRolesSerializer;
import com.cla.filecluster.service.export.serialize.TagsSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Date;
import java.util.List;
import java.util.Set;

public abstract class ClaFileDtoMixIn {

    @JsonProperty(FileListHeaderFields.NAME)
    String fileName;

    @JsonProperty(FileListHeaderFields.TYPE)
    FileType type;

    @JsonProperty(FileListHeaderFields.SIZE)
    Long fsFileSize;

    @JsonSerialize(using = DateToExcelDateSerializer.class)
    @JsonProperty(FileListHeaderFields.MODIFIED_DATE)
    Date fsLastModified;

    @JsonSerialize(using = DateToExcelDateSerializer.class)
    @JsonProperty(FileListHeaderFields.ACCESSED_DATE)
    Date fsLastAccess;

    @JsonSerialize(using = DateToExcelDateSerializer.class)
    @JsonProperty(FileListHeaderFields.CREATION_DATE)
    Date creationDate;

    @JsonProperty(FileListHeaderFields.GROUP_NAME)
    String groupName;

    @JsonSerialize(using = TagsSerializer.class)
    @JsonProperty(FileListHeaderFields.TAGS)
    Set<FileTagDto> fileTagDtos;

    @JsonSerialize(using = DocTypesSerializer.class)
    @JsonProperty(FileListHeaderFields.DOC_TYPES)
    Set<DocTypeDto> docTypeDtos;

    @JsonProperty(FileListHeaderFields.OWNER)
    String owner;

    @JsonProperty(FileListHeaderFields.NUM_OF_COPIES)
    ContentMetadataDto contentMetadataDto;

    @JsonSerialize(using = BizListExtractionSummarySerializer.class)
    @JsonProperty(FileListHeaderFields.EXTRACTIONS)
    abstract List<ExtractionSummaryDto> getBizListExtractionSummary();

    @JsonSerialize(using = FunctionalRolesSerializer.class)
    @JsonProperty(FileListHeaderFields.ROLES)
    Set<FunctionalRoleDto> associatedFunctionalRoles;

    @JsonSerialize(using = AssociatedBizListSerializer.class)
    @JsonProperty(FileListHeaderFields.ASSOCIATIONS)
    List<SimpleBizListItemDto> associatedBizListItems;
}
