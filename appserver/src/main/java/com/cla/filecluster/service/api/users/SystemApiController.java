package com.cla.filecluster.service.api.users;

import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.SystemSettingsService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping({"/api/system/settings", "/api/system/system/settings"})
public class SystemApiController {

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private SystemSettingsService systemSettingsService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "", method = {RequestMethod.GET})
    public List<SystemSettingsDto> getSystemSettings() {
        return systemSettingsService.getSystemSettings();
    }

    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public SystemSettingsDto getSystemSettingsById(@PathVariable Long id) {
        SystemSettingsDto dto = systemSettingsService.getSystemSettings(id);
        if (dto == null) {
            throw new BadRequestException(messageHandler.getMessage("system-settings.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dto;
    }

    @RequestMapping(value = "/name", method = {RequestMethod.GET})
    public List<SystemSettingsDto> getSystemSettingsByName(@RequestParam String name) {
        List<SystemSettingsDto> dto = systemSettingsService.getSystemSettings(name);
        if (CollectionUtils.isEmpty(dto)) {
            throw new BadRequestException(messageHandler.getMessage("system-settings.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dto;
    }

    @RequestMapping(value = "/prefix", method = {RequestMethod.GET})
    public List<SystemSettingsDto> getSystemSettingsByPrefix(@RequestParam String prefix) {
        List<SystemSettingsDto> dto = systemSettingsService.getSystemSettingsByPrefix(prefix);
        if (CollectionUtils.isEmpty(dto)) {
            throw new BadRequestException(messageHandler.getMessage("system-settings.prefix.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dto;
    }

    @RequestMapping(value = "", method = {RequestMethod.PUT})
    public SystemSettingsDto addSystemSettings(@RequestBody SystemSettingsDto systemSettingsDto) {
        SystemSettingsDto res = systemSettingsService.addSystemSettings(systemSettingsDto);
        eventAuditService.audit(AuditType.SETTINGS,"Add system settings", AuditAction.CREATE, SystemSettingsDto.class, systemSettingsDto);
        return res;
    }

    @RequestMapping(value = "", method = {RequestMethod.POST})
    public SystemSettingsDto updateSystemSettings(@RequestBody SystemSettingsDto systemSettingsDto, @RequestParam(defaultValue = "true") final boolean createIfNeeded) {
        SystemSettingsDto res = createIfNeeded ?
                systemSettingsService.updateOrCreateSystemSettings(systemSettingsDto) :
                systemSettingsService.updateSystemSettings(systemSettingsDto);
        eventAuditService.audit(AuditType.SETTINGS,"Update system settings", AuditAction.UPDATE, SystemSettingsDto.class, systemSettingsDto);
        return res;
    }

    @RequestMapping(value = "", method = {RequestMethod.DELETE})
    public void deleteSystemSettings(@RequestBody SystemSettingsDto systemSettingsDto) {
        getSystemSettingsById(systemSettingsDto.getId());
        systemSettingsService.deleteSystemSettings(systemSettingsDto.getId());
        eventAuditService.audit(AuditType.SETTINGS,"Delete system settings", AuditAction.DELETE, SystemSettingsDto.class, systemSettingsDto);
    }
}
