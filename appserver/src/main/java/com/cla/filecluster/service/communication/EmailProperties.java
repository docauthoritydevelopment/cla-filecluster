package com.cla.filecluster.service.communication;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;

import java.util.List;
import java.util.Map;

/**
 * Single mail properties
 * Created by vladi on 3/27/2017.
 */
@SuppressWarnings("WeakerAccess")
public class EmailProperties {
    private String plainSubject;
    private String subjectTemplate;
    private String bodyTemplate;
    private List<String> toEmails;
    private String fromEmail;
    private String replyToEmail;
    private Map<String, Object> context;

    public static EmailProperties create(){
        return new EmailProperties();
    }

    EmailProperties() {
        toEmails = Lists.newLinkedList();
        context = Maps.newHashMap();
    }

    public EmailProperties put(String key, Object value){
        context.put(key, value);
        return this;
    }

    public Map<String, Object> getContext() {
        return context;
    }

    public String getSubjectTemplate() {
        return subjectTemplate;
    }

    public EmailProperties setSubjectTemplate(String subjectTemplate) {
        this.subjectTemplate = subjectTemplate;
        return this;
    }

    public String getPlainSubject() {
        return plainSubject;
    }

    public EmailProperties setPlainSubject(String plainSubject) {
        this.plainSubject = plainSubject;
        return this;
    }

    public String getBodyTemplate() {
        return bodyTemplate;
    }

    public EmailProperties setBodyTemplate(String bodyTemplate) {
        this.bodyTemplate = bodyTemplate;
        return this;
    }

    public List<String> getToEmails() {
        return toEmails;
    }

    public void setToEmails(List<String> toEmails) {
        this.toEmails = toEmails;
    }

    public EmailProperties addToEmail(String email) {
        this.toEmails.add(email);
        return this;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public EmailProperties setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
        return this;
    }

    public String getReplyToEmail() {
        return replyToEmail;
    }

    public EmailProperties setReplyToEmail(String replyToEmail) {
        this.replyToEmail = replyToEmail;
        return this;
    }
}
