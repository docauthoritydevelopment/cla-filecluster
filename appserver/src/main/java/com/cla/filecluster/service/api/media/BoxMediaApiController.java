package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.BoxConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.media.BoxMediaAppService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 *
 * Created by uri on 23/05/2016.
 */
@RestController
@RequestMapping("/api/media/box")
public class BoxMediaApiController {

    private static final String BOX_API_OBJECT_KEY = "box.api.object";

    @Autowired
    private BoxMediaAppService boxMediaAppService;

	@Autowired
	private MediaConnectionDetailsService connectionDetailsService;

    @Autowired
    private UserService userService;

	@Autowired
	private MessageHandler messageHandler;

    private final Logger logger = LoggerFactory.getLogger(GoogleDriveMediaApiController.class);


	@RequestMapping(value = "/connections/new", method = RequestMethod.POST)
	public BoxConnectionDetailsDto newConnection(@RequestBody BoxConnectionDetailsDto connectionDetails){
		BoxConnectionDetailsDto boxConnection = connectionDetailsService.createBoxConnection(connectionDetails);
		return boxConnection;
	}

	@RequestMapping(value = "/server/folders", method = RequestMethod.GET)
	public List<ServerResourceDto> listServerFolders(@RequestParam long connectionId,
													 @RequestParam long customerDataCenterId,
													 @RequestParam(required = false, name = "id") String folderId) {
		BoxConnectionDetailsDto connectionDetailsDto = (BoxConnectionDetailsDto) connectionDetailsService.getMediaConnectionDetailsById(connectionId);
		if(connectionDetailsDto == null){
			throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
		}
		return boxMediaAppService.listFoldersUnderPath(connectionDetailsDto, Optional.ofNullable(folderId), customerDataCenterId, false);
	}

	@RequestMapping(value = "/connections/test/dataCenter/{customerDataCenterId}", method = RequestMethod.POST)
	public TestResultDto testConnection(@RequestBody BoxConnectionDetailsDto boxConnectionDetailsDto, @PathVariable long customerDataCenterId){
		try {

			if (boxConnectionDetailsDto.getId() == null) {//test new connection
				Preconditions.checkArgument(MediaConnectionDetailsService.isValidJson(boxConnectionDetailsDto.getJwt()), "invalid JWT");
			} else if (boxConnectionDetailsDto.getJwt() == null) { //else test existing connection
                BoxConnectionDetailsDto boxConnectionDetailsDtoExt = (BoxConnectionDetailsDto)
                        connectionDetailsService.getMediaConnectionDetailsById(boxConnectionDetailsDto.getId());
                boxConnectionDetailsDto.setJwt(boxConnectionDetailsDtoExt.getJwt());
			}
			boxConnectionDetailsDto.setUsername(BoxMediaAppService.BOX_CONNECTION_TEST_LABEL);
			List<ServerResourceDto> serverResourceDtos = boxMediaAppService.listFoldersUnderPath(boxConnectionDetailsDto, Optional.empty(), customerDataCenterId, true);
			return new TestResultDto(true, serverResourceDtos.get(0).getFullName());
		} catch (Exception e) {
			return new TestResultDto(false, e.getMessage());
		}
	}

	/*
	*	Both following API calls are abomination and should be abolished (as there is already a dedicated media connection service/controller)
	*/
	@RequestMapping(value = "/connections", method = RequestMethod.GET)
	public List<BoxConnectionDetailsDto> getConnectionDetails() {
		return connectionDetailsService.getMediaConnectionDetailsDtoByType(MediaType.BOX, true);
	}

	@RequestMapping(value = "/connections/{id}", method = RequestMethod.GET)
	public BoxConnectionDetailsDto getConnectionDetails(@PathVariable long id) {
		MediaConnectionDetailsDto mediaDto = connectionDetailsService.getMediaConnectionDetailsById(id, false);
		if(mediaDto == null){
			throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
		}
		BoxConnectionDetailsDto boxDto = new BoxConnectionDetailsDto();
		boxDto.setId(mediaDto.getId());
		boxDto.setName(mediaDto.getName());
		boxDto.setTimestamp(mediaDto.getTimestamp());
		boxDto.setUsername(mediaDto.getUsername());
		boxDto.setMediaType(MediaType.BOX);
		return boxDto;
	}

	@RequestMapping(value = "/connections/{id}", method = RequestMethod.POST)
	public BoxConnectionDetailsDto updateConnectionDetails(@PathVariable long id, @RequestBody BoxConnectionDetailsDto boxConnectionDetailsDto) {
		boolean isValidConnectionDetails = boxConnectionDetailsDto == null
				|| (boxConnectionDetailsDto.getJwt() != null && !MediaConnectionDetailsService.isValidJson(boxConnectionDetailsDto.getJwt()) )
													|| Strings.isNullOrEmpty( boxConnectionDetailsDto.getName());
		if(isValidConnectionDetails){
			throw new BadRequestException(messageHandler.getMessage("media-conn.invalid"), BadRequestType.OPERATION_FAILED);
		}
		if(connectionDetailsService.getMediaConnectionDetailsDtoByConnectionId(id) == null){
			throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
		}
		return connectionDetailsService.updateConnectionDetails(id, boxConnectionDetailsDto);
	}

	//region ...legacy methods. Not in use.
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public void login(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = request.getSession().getId();
		String authorizeUrl = buildBoxAuthorizeUrl(sessionId);
		logger.debug(authorizeUrl);
		try {
			if (response instanceof Response) {
				((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
			}
			response.sendRedirect(authorizeUrl);
		} catch (IOException | RuntimeException e) {
			logger.error("Failed to send redirect", e);
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value = "/login/file", method = RequestMethod.GET)
	public void loginToFile(HttpServletRequest request, HttpServletResponse response) {
		String authorizeUrl = buildBoxAuthorizeUrl("file");
		logger.debug(authorizeUrl);
		try {
			if (response instanceof Response) {
				((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
			}
			response.sendRedirect(authorizeUrl);
		} catch (IOException | RuntimeException e) {
			logger.error("Failed to send redirect", e);
			throw new RuntimeException(e);
		}
	}

	@RequestMapping(value = "/response", method = {RequestMethod.GET, RequestMethod.POST})
	public String boxOauthResponse(@QueryParam("code") String code,
								   @QueryParam("state") String state,
								   @QueryParam("error") String error,
								   @QueryParam("errorDescription") String errorDescription) {
		logger.debug("Got box Oauth response: {} {} {} {}", code, state, error, errorDescription);
		Long currentUserId = 1L; //TODO: get current user ID
		try {
			if (state.equals("file")) {
				boxMediaAppService.handleBoxOauthResponse(currentUserId, code, true);
			} else {
				boxMediaAppService.fillAuthentication(state);
				boxMediaAppService.handleBoxOauthResponse(currentUserId, code, false);

			}
		} catch (RuntimeException e) {
			logger.error("Failed to handle Box response", e);
			throw e;
		}
		return "OK";
	}
	private String buildBoxAuthorizeUrl(String sessionId) {
		return boxMediaAppService.buildAuthorizeUrl(sessionId);
	}

	/*
    @RequestMapping(value = "/server/folders", method = RequestMethod.GET)
    public List<ServerResourceDto> listServerFolders(@RequestParam(required = false) String id) {
        User currentUser = userService.getCurrentUserEntity();
        UserSavedData boxApiObject = userService.getUserSavedData(BOX_API_OBJECT_KEY);

        return boxMediaAppService.listServerFolders(currentUser.getId(), id, boxApiObject.getData());
    }
*/
	//endregion
}
