package com.cla.filecluster.service.extractor.entity;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrBizListEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrBizListRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.SolrRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.google.common.base.Strings;
import com.google.common.collect.*;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.common.constants.CatFileFieldType.TAGS_2;
import static java.util.stream.Collectors.toList;

/**
 * Created by uri on 12/01/2016.
 */
@Service
public class CatFileExtractionService {

    private final static Logger logger = LoggerFactory.getLogger(CatFileExtractionService.class);

    @Autowired
    private SolrBizListRepository solrBizListRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Value("${solr.tags2.populate:true}")
    private boolean populateTags2;

    @Transactional(readOnly = true)
    public Set<Long> getAllExtractionsFileIds(Long bizListId, int limit, int offset) {
        if (bizListId == null) {
            return solrBizListRepository.getFilesByDirtyAndList(null, limit, offset);
        }
        else {
            return solrBizListRepository.getFilesByDirtyAndList(bizListId, limit, offset);
        }
    }

   private Multimap<Long, String> findDistinctBizListItemsByFiles(Long bizListIdToFind, List<Long> claFileIds) {
        Set<SolrBizListEntity> rs = solrBizListRepository.getFilesDirtyData(bizListIdToFind, claFileIds);
        Multimap<Long,String> result = ArrayListMultimap.create(claFileIds.size(),2);
        Multiset<String> aggregatedRulesHits = HashMultiset.create();
        for (SolrBizListEntity row : rs) {
            Long fileId = row.getFileId();
            BizListItemType bizListItemType = BizListItemType.valueOf(row.getBizListItemType());

            Long bizListId = row.getBizListId();
            String bizListItemId = row.getBizListItemSid();
            String ruleName = row.getRuleName();
            int aggMinCount = row.getAggMinCount();
            if (aggMinCount >0) {
                String identifier = fileId + "." + ruleName + "." + bizListId;
                int beforeCount = aggregatedRulesHits.count(identifier);
                aggregatedRulesHits.add(identifier, row.getCounter().intValue());
                if (aggregatedRulesHits.count(identifier) >= aggMinCount && beforeCount < aggMinCount) {
                    String value = FileDtoFieldConverter.convertAggBizListItemToSolrValue(bizListItemType, bizListId , ruleName);
                    logger.trace("Got aggregated hit count: {}. convertAggBizListItemToSolrValue={}", identifier, value);
                    result.put(fileId,value);
                }
            }
            else {
                String value = FileDtoFieldConverter.convertBizListItemToSolrValue(true, bizListItemType, bizListId, bizListItemId
                        , ruleName);
                result.put(fileId,value);
            }
        }
        return result;
    }

    @AutoAuthenticate
    public void updateBizListExtractionsOnSolr(final Long bizListId, List<Long> claFileIds, Map<String, FileTag> tagsForExtraction) {
        final int inLastBatch = claFileIds.size();
        if (inLastBatch == 0) {
            //Empty page
            logger.info("Got empty page of files to update");
        }
        try {
            Query query = Query.create()
                    .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.IN, claFileIds))
                    .addField(CatFileFieldType.FILE_ID)
                    .addField(CatFileFieldType.ID)
                    .addField(CatFileFieldType.CONTENT_ID)
                    .addField(CatFileFieldType.EXTRACTED_ENTITIES_IDS)
                    .setRows(claFileIds.size())
                    ;
            List<SolrFileEntity> solrDocuments = fileCatService.find(query.build());
            if (solrDocuments.size() == 0) {
                //Empty page
                logger.info("Got empty page of filtered items to update");
                return;
            }
            List<String> compositeIds = solrDocuments.stream().map(SolrFileEntity::getId).collect(Collectors.toList());
            List<Long> fileIds = solrDocuments.stream().map(SolrFileEntity::getFileId).collect(Collectors.toList());
            Multimap<Long, String> extractedItemsByClaFile = findDistinctBizListItemsByFiles(bizListId, fileIds);
            if (extractedItemsByClaFile.size() == 0) {
                return;
            }

            // remove existing values so we dont set them twice
            for (SolrFileEntity doc : solrDocuments) {
                Long fileId = doc.getFileId();
                List<String> currentValues = doc.getExtractedEntitiesIds();
                if (currentValues != null) {
                    currentValues.forEach(value -> extractedItemsByClaFile.remove(fileId, value));
                }
            }

            // create data for contents
            Multimap<String, String> contentExtractedItems = ArrayListMultimap.create();
            solrDocuments.forEach(file -> {
                String contentId = file.getContentId();
                Long fileId = file.getFileId();
                if (!Strings.isNullOrEmpty(contentId)) {
                    Collection<String> item = extractedItemsByClaFile.get(fileId);
                    contentExtractedItems.putAll(contentId, item);
                }
            });
            contentMetadataService.updateExtractionDataToCategoryFiles(contentExtractedItems);
            Map<Long, Set<FileTag>> extractionTagsByClaFile = (tagsForExtraction==null) ? null : assignExtractionTags(extractedItemsByClaFile, tagsForExtraction);
            int size = tagsForExtraction == null ? 0 : tagsForExtraction.size();
            logger.debug("Update {} extractions for {} files (after filter). {} entExt", size, compositeIds.size(), extractedItemsByClaFile.size());
            updateExtractionDataToCategoryFiles(compositeIds, extractedItemsByClaFile, null, extractionTagsByClaFile);
            logger.debug("Set bizListItems as not dirty id {}", bizListId);
            setBizListItemsByFilesNotDirty(bizListId, fileIds);
        } catch (RuntimeException e) {
            logger.error("Failed to insert a page of extraction detail items to Solr", e);
            opStateService.addCurrentPhaseError();
            if (opStateService.isStopOnError()) {
                return; // Ask caller to stop
            }
        }
    }

    private void setBizListItemsByFilesNotDirty(final Long bizListId, List<Long> claFileIds) {
        solrBizListRepository.updateNotDirtyByFileIds(bizListId, claFileIds);
        solrBizListRepository.commit();
    }

    private Map<Long, Set<FileTag>> assignExtractionTags(Multimap<Long, String> extractedItemsByClaFile, Map<String, FileTag> tagsForExtraction) {
        logger.debug("Update extracted entities tags for {} files in Database", extractedItemsByClaFile.size());
        Collection<ClaFile> claFiles = fileCatService.findByFilesIds(extractedItemsByClaFile.keySet());
        List<ClaFile> updatedFiles = new ArrayList<>();
        Map<Long, Set<FileTag>> result = Maps.newHashMap();

        for (ClaFile claFile : claFiles) {
            Collection<String> entities = extractedItemsByClaFile.get(claFile.getId());
            if (entities == null || entities.size() == 0) {
                continue;
            }
            Set<FileTag> fileTags = new HashSet<>();
            for (String entity : entities) {
                FileTag fileTag = tagsForExtraction.get(entity);
                if (fileTag == null) {
                    logger.error("SEVERE ERROR - got extracted entity {}, but filetag was not created for it", entity);
                } else {
                    fileTags.add(fileTag);
                }
            }
            boolean updated = updateClaFileFileTags(fileTags, claFile);
            if (updated) {
                updatedFiles.add(claFile);
            }
            result.put(claFile.getId(), fileTags);
        }

        return result;
    }

    private boolean updateClaFileFileTags(Set<FileTag> fileTags, ClaFile claFile) {
        claFile.setLastMetadataChangeDate(System.currentTimeMillis());
        List<AssociationEntity> fileTagAssociations = AssociationsService.convertFromAssociationEntity(
                claFile.getId(), claFile.getAssociations(), AssociationType.FILE);

        Set<FileTag> toAdd = Sets.newHashSet();
        toAdd.addAll(fileTags);
        Map<Long, FileTag> fileTagsById = fileTags.stream().collect(
                Collectors.toMap(FileTag::getId,
                Function.identity()));

        if (fileTagAssociations != null){
            for (AssociationEntity claFileTagAssociation : fileTagAssociations) {
                if (claFileTagAssociation.getFileTagId() != null) {
                    FileTag tag = fileTagsById.get(claFileTagAssociation.getFileTagId());
                    toAdd.remove(tag);
                }
            }
        }

        if (toAdd.size() > 0) {
            logger.debug("Update ClaFile {}. Add {} new tags", claFile.getId(), toAdd.size());
            SolrInputDocument solrInputDocument = new SolrInputDocument();
            solrInputDocument.setField(CatFileFieldType.ID.getSolrName(),
                    FileCatCompositeId.of(claFile.getContentMetadataId(), claFile.getId()).toString());
            FileCatService.addField(solrInputDocument, CatFileFieldType.UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
            for (FileTag fileTag : toAdd) {
                AssociationEntity assoc = new AssociationEntity();
                assoc.setFileTagId(fileTag.getId());
                assoc.setCreationDate(System.currentTimeMillis());
                String json = AssociationsService.convertFromAssociationEntity(assoc);
                FileCatService.addField(solrInputDocument, CatFileFieldType.FILE_ASSOCIATIONS, json, SolrFieldOp.ADD);
            }
            fileCatService.atomicUpdate(solrInputDocument);
            fileCatService.softCommitNoWaitFlush();
            return true;
        }
        return false;
    }

    private void updateExtractionDataToCategoryFiles(List<String> compositeIds,
                                                    Multimap<Long, String> extractedEntitiesByFile,
                                                    Map<Long, List<AggregationCountItemDTO<String>>> extractedTextByFile,
                                                    Map<Long, Set<FileTag>> extractionTagsByClaFile) {

        List<SolrInputDocument> solrDocs = new ArrayList<>(compositeIds.size());

        for (String compositeId : compositeIds) {
            long claFileId = FileCatCompositeId.of(compositeId).getFileId();
            List<AggregationCountItemDTO<String>> extractedText =
                    extractedTextByFile != null ? extractedTextByFile.get(claFileId) : null;
            Collection<String> extractedEntities =
                    extractedEntitiesByFile != null ? extractedEntitiesByFile.get(claFileId) : null;
            SolrInputDocument doc = createExtractionDataToCategoryFileSolrDoc(compositeId, extractedEntities,
                    extractedText);
            if (doc != null) {
                if (extractionTagsByClaFile != null) {
                    //Add tags
                    addTagsToDocument(doc, extractionTagsByClaFile.get(claFileId));
                    SolrFileCatRepository.setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
                }
                solrDocs.add(doc);
            }
        }
        // Write docs to Solr
        if (solrDocs.size() > 0) {
            try {
                UpdateResponse updateResponse = fileCatService.saveAll(solrDocs);
                if (updateResponse == null) {
                    throw new RuntimeException("Failed to update category file in Solr.");
                }
                logger.debug("ExtractionData: {} files written to Solr, response: {}", solrDocs.size(), updateResponse);
            } catch (Exception e) {
                String collect = compositeIds.stream().map(String::toString).collect(Collectors.joining(","));
                logger.error("Failed to update category file in Solr ({} docs): {}. {}", solrDocs.size(),
                        collect, e);
                throw new RuntimeException("Failed to update category file in Solr. #docs=" + compositeIds.size(), e);
            }
        }
    }

    private SolrInputDocument createExtractionDataToCategoryFileSolrDoc(String compositeId, Collection<String> extractedEntities, List<AggregationCountItemDTO<String>> extractedText) {
        if ((extractedEntities == null || extractedEntities.size() == 0) &&
                (extractedText == null || extractedText.size() == 0)) {
            return null; //Workaround
        }
        SolrInputDocument doc = new SolrInputDocument();
//        String compositeId = FileCatUtils.generateId(claFileId, contentId);
        doc.setField(ID.getSolrName(), compositeId);
        if (extractedEntities != null && extractedEntities.size() > 0) {
            logger.trace("set Entities to Cateogory file id {} entity size {}", compositeId, extractedEntities.size());
            List<String> ids = extractedEntities.stream().map(SolrRepository::generateExtractedEntityId).distinct().collect(toList());
            Map<String, Object> fieldModifier = SolrRepository.atomicAddValue(ids);
            doc.setField(EXTRACTED_ENTITIES_IDS.getSolrName(), fieldModifier);  // add the map as the field value
        }
        if (extractedText != null && extractedText.size() > 0) {
            logger.trace("set {} Extracted Text elements to Cateogory file id {}", extractedText.size(), compositeId);
            List<String> ids = extractedText.stream().map(this::generateExtractedTextId).collect(toList());
            Map<String, Object> fieldModifier = SolrRepository.atomicAddValue(ids);
            doc.setField(EXTRACTED_TEXT_IDS.getSolrName(), fieldModifier);  // add the map as the field value
        }
        return doc;
    }

    private String generateExtractedTextId(AggregationCountItemDTO<String> aggregationCountItemDTO) {
        return aggregationCountItemDTO.getItem();
    }

    private void addTagsToDocument(SolrInputDocument doc, Set<FileTag> fileTags) {
        if (fileTags == null || fileTags.size() == 0) {
            return;
        }
        Map<String, Object> fieldModifier = new HashMap<>(1);
        List<String> fileTagKeys = new ArrayList<>();
        for (FileTag fileTag : fileTags) {
            String solrKey = AssociationIdUtils.createTagIdentifier(fileTag);
            fileTagKeys.add(solrKey);
        }
        fieldModifier.put("add", fileTagKeys);
        doc.addField(TAGS.getSolrName(), fieldModifier);  // add the map as the field value
        if (populateTags2) {
            Map<String, Object> fieldModifier2 = new HashMap<>(1);
            List<String> fileTag2Keys = new ArrayList<>();
            for (FileTag fileTag : fileTags) {
                String solrKey2 = AssociationIdUtils.createTag2Identifier(fileTag);
                fileTag2Keys.add(solrKey2);
            }
            fieldModifier2.put("add", fileTag2Keys);
            doc.addField(TAGS_2.getSolrName(), fieldModifier2);  // add the map as the field value
        }
    }
}
