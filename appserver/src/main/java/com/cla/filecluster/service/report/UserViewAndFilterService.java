package com.cla.filecluster.service.report;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.report.DisplayType;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.filter.SavedFilterAppService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.base.Strings;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;

@Service
public class UserViewAndFilterService {

    private Logger logger = LoggerFactory.getLogger(UserViewAndFilterService.class);

    public static final String NAME = "NAME";
    private static final String DISPLAY_TYPE = "DISPLAY_TYPE";
    private static final String DISPLAY_DATA = "DISPLAY_DATA";
    private static final String FILTER_NAME = "FILTER_NAME";
    private static final String FILTER_DESCRIPTOR = "FILTER_DESCRIPTOR";
    private static final String FILTER_RAW = "FILTER_RAW";
    private static final String IS_FILTER_GLOBAL = "IS_FILTER_GLOBAL";
    private final static String[] requiredHeaders = {NAME, DISPLAY_TYPE,DISPLAY_DATA,FILTER_NAME,FILTER_DESCRIPTOR,FILTER_RAW,IS_FILTER_GLOBAL};

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserViewDataService userViewDataService;

    @Autowired
    private SavedFilterAppService savedFilterAppService;

    @Value("${ingester.searchPatternsFile:./config/extraction/default-user-view-data.csv}")
    private String userViewDataFileName;

    @Value("${ingester.searchPatternsFile:./config/extraction/legal-mode-user-view-data.csv}")
    private String userViewDataLegalModeFileName;

    @Value("${analytics.default-dashboard-name}")
    private String defaultAnalyticsDashboardName;

    public void createDefaultAnalyticsDashboard() {
        UserViewDataDto userViewDataDto = new UserViewDataDto();
        userViewDataDto.setName(defaultAnalyticsDashboardName);
        userViewDataDto.setDisplayType(DisplayType.DASHBOARD);
        userViewDataDto.setGlobalFilter(true);
        createUserViewData(userViewDataDto);
    }

    @Transactional
    public void populateDefaultUserViewData() {
        if (departmentService.isLegalInstallationMode()) {
            populateDefaultUserViewData(userViewDataLegalModeFileName);
        }
        else {
            populateDefaultUserViewData(userViewDataFileName);
        }
    }

    @Transactional
    private void populateDefaultUserViewData(final String importFileName) {
        logger.info("populateDefaultSearchPatterns");
        if (StringUtils.isEmpty(importFileName) || !Files.exists(Paths.get(importFileName))) {
            logger.info("User view data file '{}' not defined, or doesn't exist", importFileName);
            return;
        }
        logger.info("Load user view data file {}",importFileName);

        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, importFileName, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(importFileName, row -> createUserViewDataFromCsvRow(row, headersLocationMap), null, null);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load user view data from file - problem in csv file "+e.getMessage(),e);
        }
        if (failedRows > 0) {
            throw new RuntimeException("Failed to load "+failedRows+" search patterns in file - problem in csv file");
        }
    }

    private void createUserViewDataFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length==0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String displayTypeStr = fieldsValues[headersLocationMap.get(DISPLAY_TYPE)].trim();
        DisplayType displayType = DisplayType.DISCOVER;
        if (!Strings.isNullOrEmpty(displayTypeStr)) {
            displayType = DisplayType.valueOf(displayTypeStr.toUpperCase());
        }
        String filterName = Optional.ofNullable(fieldsValues[headersLocationMap.get(FILTER_NAME)]).orElse("").trim();
        boolean isGlobalFilter= Boolean.valueOf(Optional.ofNullable(fieldsValues[headersLocationMap.get(IS_FILTER_GLOBAL)])
                .orElse("FALSE").trim());
        String displayData;
        String filterDescriptorStr;
        String filterRow;
        try {
            displayData = URLDecoder.decode(Optional.ofNullable(fieldsValues[headersLocationMap.get(DISPLAY_DATA)]).orElse("")
                    .trim(), "UTF-8");
            filterDescriptorStr = URLDecoder.decode(Optional.ofNullable(fieldsValues[headersLocationMap.get(FILTER_DESCRIPTOR)])
                    .orElse("").trim(), "UTF-8");
            filterRow = URLDecoder.decode(Optional.ofNullable(fieldsValues[headersLocationMap.get(FILTER_RAW)]).orElse("")
                    .trim(), "UTF-8");
        }
        catch (UnsupportedEncodingException uee) {
            throw new RuntimeException(uee);
        }
        SavedFilterDto filter = null;
        if (!filterName.equals("")) {
            filter = new SavedFilterDto();
            filter.setName(filterName);
            filter.setRawFilter(filterRow);
            filter.setFilterDescriptor(userViewDataService.getFilterDescriptionByJsonString(filterDescriptorStr));
        }

        UserViewDataDto userViewDataDto = new UserViewDataDto();
        userViewDataDto.setFilter(filter);
        userViewDataDto.setName(name);
        userViewDataDto.setDisplayType(displayType);
        userViewDataDto.setDisplayData(displayData);
        userViewDataDto.setGlobalFilter(isGlobalFilter);
        createUserViewData(userViewDataDto);
    }

    // used only for data populator !
    private void createUserViewData(UserViewDataDto newUserView) {
        if (newUserView.getFilter() != null) {
            SavedFilterDto filter = savedFilterAppService.createSavedFilter(
                    newUserView.getFilter().getName(), newUserView.getFilter().getRawFilter(),
                    newUserView.getFilter().getFilterDescriptor(),newUserView.getFilter().isGlobalFilter(), newUserView.getFilter().getScope());
            newUserView.setFilter(filter);
        }
        userViewDataService.createUserViewData(newUserView);
    }
}
