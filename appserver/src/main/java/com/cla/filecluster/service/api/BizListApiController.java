package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.*;
import com.cla.common.domain.dto.classification.EvaluateBizListInstructions;
import com.cla.common.domain.dto.classification.EvaluationProgressDto;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.BizListExtractionManager;
import com.cla.filecluster.mediaproc.BizListScheduleRunStatus;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 *
 * Created by uri on 30/12/2015.
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/api/bl")
public class BizListApiController {

    @Autowired
    private BizListAppService bizListAppService;

    @Autowired
    private BizListService bizListService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private BizListExtractionManager bizListExtractionManager;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MessageHandler messageHandler;

    private Future<Void> future;

    private final static Logger logger = LoggerFactory.getLogger(BizListApiController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<BizListDto> getAllBizLists(@RequestParam(required = false) String bizListType) {
        if (!StringUtils.isEmpty(bizListType)) {
            try {
                BizListItemType bizListItemType = BizListItemType.valueOf(bizListType);
                return bizListAppService.getBizListsByType(bizListItemType);
            } catch (IllegalArgumentException e) {
                logger.error("Illegal bizListType [{}] given to getAllBizLists");
                throw new BadRequestException(messageHandler.getMessage("biz-list.type.invalid"), BadRequestType.UNSUPPORTED_VALUE);
            }
        }
        return bizListAppService.getAllBizLists();
    }

    @RequestMapping(value="/summary/list", method = RequestMethod.GET)
    public Page<BizListSummaryInfo> listBizList(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return bizListAppService.listBizListSummary(dataSourceRequest);
    }


    @RequestMapping(value = "types", method = RequestMethod.GET)
    public List<BizListItemType> getBizListItemTypes() {
        return bizListAppService.getBizListItemTypes();
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public BizListDto createBizList(@RequestBody BizListDto bizListDto) {
        BizListDto bizList = bizListAppService.createBizList(bizListDto);
        eventAuditService.audit(AuditType.BIZ_LIST,"Create biz list ", AuditAction.CREATE, BizListDto.class,bizList.getId(),bizList);
        return bizList;
    }

    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public BizListDto updateBizList(@PathVariable("id") long bizListId, @RequestBody BizListDto bizListDto) {
        BizListDto bizListDto1 = bizListAppService.updateBizList(bizListId, bizListDto);
        eventAuditService.audit(AuditType.BIZ_LIST,"Update biz list ", AuditAction.UPDATE, BizListDto.class,bizListDto1.getId(),bizListDto1);
        return bizListDto1;
    }

    @RequestMapping(value = "/{id}/active", method = RequestMethod.POST)
    public BizListDto updateBizListActive(@PathVariable("id") long bizListId, @RequestBody Boolean active) {
        BizListDto bizListDto = bizListAppService.updateBizListActive(bizListId, active);
        eventAuditService.audit(AuditType.BIZ_LIST,"Update biz list ", AuditAction.UPDATE, BizListDto.class,bizListDto.getId(),bizListDto);
        return bizListDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteBizList(@PathVariable("id") long bizListId) {
        BizList bizList = getBizList(bizListId);
        bizListAppService.deleteBizList(bizList.getId());
        BizListDto bizListDto = BizListAppService.convertBizList(bizList);
        eventAuditService.audit(AuditType.BIZ_LIST,"Delete biz list ", AuditAction.DELETE, BizListDto.class,bizListId,bizListDto);
    }


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public BizList getBizList(@PathVariable long id) {
        BizList bizList = bizListService.getBizListById(id,true);
        if (bizList == null) {
            throw new BadRequestException(messageHandler.getMessage("biz-list.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return bizList;
    }

    @NotNull
    private SimpleBizListItemDto getBizListItem(String id) {
        SimpleBizListItemDto simpleBizListItemDto = bizListItemService.getSimpleBizListItemDto(id);
        if (simpleBizListItemDto == null) {
            throw new BadRequestException(messageHandler.getMessage("biz-list-item.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return simpleBizListItemDto;
    }

    @RequestMapping(value = "/scheduling/active", method = RequestMethod.POST)
    public void updateBizListSchedulingActive(@RequestBody Boolean active) {
        bizListExtractionManager.setShouldRunNextExtractFlag(active);
        eventAuditService.audit(AuditType.BIZ_LIST,"Update biz list scheduling " +(active ? "active" : "paused"), AuditAction.UPDATE);
    }

    @RequestMapping(value = "/scheduling/state", method = RequestMethod.GET)
    public BizListScheduleRunStatus getBizListSchedulingState() {
       return bizListExtractionManager.getBizListSchedulingState();
    }

    @RequestMapping(value = "/{id}/extract", method = {RequestMethod.DELETE})
    public void clearCatFileExtractionFromBizList(@PathVariable("id") long bizListId, Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        Long jobId = null;
        if (dataSourceRequest.isWait()) {
            bizListAppService.clearBizListExtractionFromCatFileSync(bizListId, jobId);
            bizListAppService.clearPreviousExtraction(bizListId, jobId);
        }
        else {
            executionManagerService.startAsyncTask(() -> {
                bizListAppService.clearBizListExtractionFromCatFileSync(bizListId, jobId);
                bizListAppService.clearPreviousExtraction(bizListId, jobId);
                return null;
            });
        }
    }

    @RequestMapping(value = "/{bizListId}/item/{itemId}/extract", method = {RequestMethod.DELETE})
    public void clearCatFileBizListItemExtractionFromBizList(@PathVariable("bizListId") long bizListId,
                                                  @PathVariable("itemId") String itemId ,
                                                  Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isWait()) {
            bizListAppService.clearCatFileBizListItemExtractionFromBizList(bizListId,itemId);
        }
        else {
            executionManagerService.startAsyncTask(() -> {
                bizListAppService.clearCatFileBizListItemExtractionFromBizList(bizListId,itemId);
                return null;
            });
        }
    }


    @RequestMapping(value = "/{bizListId}/item", method = RequestMethod.PUT)
    public SimpleBizListItemDto createBizListItem(@PathVariable("bizListId") long bizListId,@RequestBody SimpleBizListItemDto entityDto) {
        BizList bizList = getBizList(bizListId);
        SimpleBizListItemDto simpleBizListItemDto = bizListAppService.createBizListItem(bizList.getId(), entityDto);
        eventAuditService.auditDual(AuditType.BIZ_LIST,"Create biz list item ", AuditAction.CREATE_ITEM, BizListDto.class,bizList.getId(),SimpleBizListItemDto.class, simpleBizListItemDto.getId(),simpleBizListItemDto);
        return simpleBizListItemDto;
    }

    @RequestMapping(value = "/{bizListId}/items", method = RequestMethod.GET)
    public Page<? extends SimpleBizListItemDto> listBizListItems(@PathVariable("bizListId") long bizListId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return bizListAppService.listBizListItems(bizListId, dataSourceRequest);
    }

    @RequestMapping(value = "/{bizListId}/item/{id}", method = RequestMethod.POST)
    public SimpleBizListItemDto updateBizListItem(@PathVariable("bizListId") long bizListId, @PathVariable("id") String itemId, @RequestBody SimpleBizListItemDto entityDto) {
        BizList bizList = getBizList(bizListId);
        SimpleBizListItemDto simpleBizListItemDto = bizListAppService.updateBizListItem(bizListId, itemId, entityDto);
        eventAuditService.auditDual(AuditType.BIZ_LIST,"Update biz list item ", AuditAction.UPDATE_ITEM, BizListDto.class,bizList.getId(),SimpleBizListItemDto.class,simpleBizListItemDto.getId(),simpleBizListItemDto);
        return simpleBizListItemDto;
    }

    /**
     * POST /api/bl/10/item/10_12/status?newStatus=ACTIVE&oldStatus=PENDING HTTP/1.1
     * @param bizListId biz list ID
     * @param itemId    list entry (item) ID
     * @param newStatus item status to update
     * @return biz list item DTO
     */
    @RequestMapping(value = "/{bizListId}/item/{id}/status", method = RequestMethod.POST)
    public SimpleBizListItemDto updateBizListItemStatus(@PathVariable("bizListId") long bizListId, @PathVariable("id") String itemId, @RequestParam String newStatus) {
        BizList bizList = getBizList(bizListId);
        SimpleBizListItemDto simpleBizListItemDto = bizListAppService.updateBizListItemStatus(bizListId, itemId, newStatus);
        eventAuditService.auditDual(AuditType.BIZ_LIST,"Update biz list item ", AuditAction.UPDATE_ITEM, BizListDto.class,bizList.getId(),SimpleBizListItemDto.class,simpleBizListItemDto);
        return simpleBizListItemDto;
    }

    @RequestMapping(value = "/{bizListId}/items/status", method = RequestMethod.POST)
    public void updateBizListItemsStatus(@PathVariable("bizListId") long bizListId,
                                         @RequestParam(required = false) Long importId,
                                         @RequestParam(required = false) BizListItemState oldStatus,
                                         @RequestParam BizListItemState newStatus) {
        bizListAppService.updateBizListItemsState(bizListId, importId, oldStatus, newStatus);
    }

    @RequestMapping(value = "/{bizListId}/item/{id}", method = RequestMethod.DELETE)
    public void deleteBizListItem(@PathVariable("bizListId") long bizListId, @PathVariable("id") String itemId) {
        BizList bizList = getBizList(bizListId);
        SimpleBizListItemDto bizListItem = getBizListItem(itemId);
        bizListAppService.deleteBizListItem(bizListId,itemId);
        eventAuditService.auditDual(AuditType.BIZ_LIST,"Delete biz list item", AuditAction.DELETE_ITEM,BizListDto.class,bizList.getId(), SimpleBizListItemDto.class,bizListItem.getId(),bizListItem);
    }

    @RequestMapping(value = "/{bizListId}/items", method = RequestMethod.DELETE)
    public void deleteBizListItems(@PathVariable("bizListId") long bizListId, @RequestParam long importId) {
        bizListAppService.deleteBizListItemsByImportId(bizListId,importId);
    }

    @RequestMapping(value="/{bizListId}/import/csv", method=RequestMethod.POST)
    public @ResponseBody BizListImportResultDto importCsvFile(@PathVariable("bizListId") long bizListId, @RequestParam("file") MultipartFile file){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("biz-list.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import CSV File");
            byte[] bytes = file.getBytes();
            BizListImportResultDto bizListImportResultDto = bizListAppService.importCsvFile(bizListId, bytes);
            eventAuditService.auditDual(AuditType.BIZ_LIST,"Import biz list items from csv file", AuditAction.IMPORT_ITEMS, BizListDto.class,bizListId,MultipartFile.class,file.getOriginalFilename() );
            return bizListImportResultDto;
        } catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(),e);
            BizListImportResultDto bizListImportResultDto = new BizListImportResultDto();
            bizListImportResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return bizListImportResultDto;
        }
    }

    @RequestMapping(value="/{bizListId}/import/folder", method=RequestMethod.POST)
    public @ResponseBody BizListImportResultDto importFromFolderNames(@PathVariable("bizListId") long bizListId,
                                                 @RequestParam String folderPath,
                                                 @RequestParam(required = false, defaultValue = "1") int relativeDepth) {
        if (Strings.isNullOrEmpty(folderPath)) {
            throw new BadRequestException(messageHandler.getMessage("biz-list.import.folder.empty"), BadRequestType.MISSING_FIELD);
        }

        Long docFolderId = docFolderService.locateFolderByFullPath(folderPath);

        logger.info("Import bizListItems into bizListId {} from subfolders at depth {} of folder {}", bizListId, relativeDepth, docFolderId);
        BizListImportResultDto bizListImportResultDto = bizListAppService.importFromFolderNames(bizListId, docFolderId, relativeDepth);

        eventAuditService.audit(AuditType.BIZ_LIST,"Create biz list items from folder names", AuditAction.CREATE, SimpleBizListItemDto.class,bizListId, Pair.of("relativeDepth",relativeDepth),Pair.of("folderId",docFolderId));

        return bizListImportResultDto;
    }

    @RequestMapping(value="/extraction/rules/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<String>> findBizListExtractionRulesFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover extraction rules export", AuditAction.VIEW,  String.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover extraction rules", AuditAction.VIEW,  String.class,null, dataSourceRequest);
        }
        return bizListAppService.findBizListExtractionRulesFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/extraction/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<BizListDto>> findBizListExtractionFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY,"Discover biz list extractions", AuditAction.VIEW,  BizListDto.class,null, dataSourceRequest);
        return bizListAppService.findBizListFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/{bizListId}/items/extraction/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsExtractionFileCounts(@PathVariable long bizListId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY,"Discover biz list items extractions for bizListId", AuditAction.VIEW,  SimpleBizListItemDto.class,bizListId, dataSourceRequest);
        return bizListAppService.findBizListItemsExtractionFileCounts(dataSourceRequest,bizListId);
    }

    @RequestMapping(value="/type/{bizListItemType}/items/extraction/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsExtractionFileCountsByType(@PathVariable String bizListItemType, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        BizListItemType bizListItemTypeEnum = BizListItemType.valueOf(bizListItemType);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover biz list items extractions export", AuditAction.VIEW,  SimpleBizListItemDto.class,bizListItemType, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover biz list items extractions", AuditAction.VIEW,  SimpleBizListItemDto.class,bizListItemType, dataSourceRequest);
        }
        return bizListAppService.findBizListItemsExtractionFileCountsByType(dataSourceRequest,bizListItemTypeEnum);
    }

    @RequestMapping(value="/items/extraction/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsExtractionFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY,"Discover biz list items extractions", AuditAction.VIEW,  SimpleBizListItemDto.class,null, dataSourceRequest);
        return bizListAppService.findBizListItemsExtractionFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/{bizListId}/items/association/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsAssociationFileCounts(@PathVariable long bizListId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY,"Discover biz list items extractions for bizListId", AuditAction.VIEW,  SimpleBizListItemDto.class,bizListId, dataSourceRequest);
        return bizListAppService.findBizListItemsAssociationFileCounts(dataSourceRequest,bizListId);
    }

    @RequestMapping(value="/type/{bizListItemType}/items/association/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsAssociationFileCountsByType(@PathVariable String bizListItemType, @RequestParam final Map params) {
        BizListItemType bizListItemTypeEnum = BizListItemType.valueOf(bizListItemType);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY,"Discover biz list items extractions for bizListItemType", AuditAction.VIEW,  SimpleBizListItemDto.class,bizListItemType, dataSourceRequest);
        return bizListAppService.findBizListItemsAssociationFileCounts(dataSourceRequest,bizListItemTypeEnum);
    }

    @RequestMapping(value="/items/association/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsAssociationFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover biz list client associations export", AuditAction.VIEW,  SimpleBizListItemDto.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover biz list client associations", AuditAction.VIEW,  SimpleBizListItemDto.class,null, dataSourceRequest);
        }
        return bizListAppService.findBizListItemsAssociationFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/file/{fileId}", method = RequestMethod.PUT)
    public ClaFileDto associateBizListItemWithFile(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable long fileId) {
        SimpleBizListItemDto bizListItem = getBizListItem(bizListItemId);
        ClaFileDto claFileDto = bizListAppService.associateFileWithBizListItem(bizListItemId, fileId);
        if(claFileDto!=null) {
            eventAuditService.auditDual(AuditType.BIZ_LIST, "Assign biz list item to file", AuditAction.ASSIGN, SimpleBizListItemDto.class, bizListItem.getId(), ClaFileDto.class, claFileDto.getId(), claFileDto);
        }
        return claFileDto;
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/files", method = RequestMethod.PUT)
    public void associateBizListItemWithFiles(@PathVariable long bizListId, @PathVariable String bizListItemId, @RequestParam String fileIds) {
        String[] fileIdArray = StringUtils.split(fileIds, ',');
        for (String fileId : fileIdArray) {
            associateBizListItemWithFile(bizListId, bizListItemId, Long.getLong(fileId));
        }
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/file/{fileId}", method = RequestMethod.DELETE)
    public ClaFileDto removeBizListItemAssociationFromFile(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable long fileId) {
        SimpleBizListItemDto bizListItem = getBizListItem(bizListItemId);
        ClaFileDto claFileDto = bizListAppService.removeBizListItemAssociationFromFile(bizListId, bizListItemId, fileId);
        if(claFileDto!=null) {
            eventAuditService.auditDual(AuditType.BIZ_LIST, "Unassign biz list item from file", AuditAction.UNASSIGN,
                    SimpleBizListItemDto.class, bizListItem.getId(), ClaFileDto.class, claFileDto.getId(), claFileDto);
        }
        return claFileDto;
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/files", method = RequestMethod.DELETE)
    public void removeBizListItemAssociationFromFiles(@PathVariable long bizListId, @PathVariable String bizListItemId, @RequestParam String fileIds) {
        String[] fileIdArray = StringUtils.split(fileIds, ',');
        for (String fileId : fileIdArray) {
            removeBizListItemAssociationFromFile(bizListId,bizListItemId,Long.getLong(fileId));
        }
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/folder/{docFolderId}", method = RequestMethod.PUT)
    public DocFolderDto associateBizListItemWithDocFolder(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable long docFolderId) {
        SimpleBizListItemDto bizListItem = getBizListItem(bizListItemId);
        DocFolderDto docFolderDto = bizListAppService.associateFolderWithBizListItem(bizListItemId, docFolderId);
        if(docFolderDto!=null) {
            eventAuditService.auditDual(AuditType.BIZ_LIST, "Assign biz list item to folder", AuditAction.ASSIGN, SimpleBizListItemDto.class, bizListItem.getId(), DocFolderDto.class, docFolderDto.getId(), docFolderDto);
        }
        return docFolderDto;
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/folders", method = RequestMethod.PUT)
    public void associateBizListItemWithDocFolders(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable String folderIds) {
        String[] folderIdsArray = StringUtils.split(folderIds, ',');
        for (String docFolderId : folderIdsArray) {
            associateBizListItemWithDocFolder(bizListId,bizListItemId,Long.valueOf(docFolderId));
        }
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/folder/{docFolderId}", method = RequestMethod.DELETE)
    public DocFolderDto removeBizListItemAssociationFromDocFolder(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable long docFolderId) {
        logger.debug("remove BizListItem {} Association From Folder {}",bizListItemId,docFolderId);
        SimpleBizListItemDto bizListItem = getBizListItem(bizListItemId);
        DocFolderDto docFolderDto = bizListAppService.removeBizListItemAssociationFromFolder(bizListId, bizListItemId, docFolderId);
        if(docFolderDto!=null) {
            eventAuditService.auditDual(AuditType.BIZ_LIST, "Unassign biz list item from folder", AuditAction.UNASSIGN, SimpleBizListItemDto.class, bizListItemId, DocFolderDto.class, docFolderId, docFolderDto);
        }
        return  docFolderDto;
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/folders", method = RequestMethod.DELETE)
    public void removeBizListItemAssociationFromDocFolders(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable String folderIds) {
        String[] folderIdsArray = StringUtils.split(folderIds, ',');
        for (String docFolderId : folderIdsArray) {
            removeBizListItemAssociationFromDocFolder(bizListId,bizListItemId,Long.valueOf(docFolderId));
        }
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/group/{groupId}", method = RequestMethod.PUT)
    public GroupDto associateBizListItemWithGroup(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable String groupId) {
        logger.debug("Associate Group id {} with bizList Item {}",groupId,bizListItemId);
        SimpleBizListItemDto bizListItem = getBizListItem(bizListItemId);
        GroupDto groupDto = bizListAppService.associateGroupWithBizListItem(bizListId, bizListItemId, groupId);
        if(groupDto!=null) {
            eventAuditService.auditDual(AuditType.BIZ_LIST, "Assign biz list item to group", AuditAction.ASSIGN, SimpleBizListItemDto.class, bizListItemId, GroupDto.class,groupId,groupDto);
        }
        return groupDto;
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/groups", method = RequestMethod.PUT)
    public void associateBizListItemWithGroups(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable String groupIds) {
        String[] groupIdsArray = StringUtils.split(groupIds, ',');
        for (String groupId : groupIdsArray) {
            associateBizListItemWithGroup(bizListId,bizListItemId,groupId);
        }
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/group/{groupId}", method = RequestMethod.DELETE)
    public GroupDto removeBizListItemAssociationFromGroup(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable String groupId) {
        logger.debug("remove BizListItem {} Association From Group {}",bizListItemId,groupId);
        SimpleBizListItemDto bizListItem = getBizListItem(bizListItemId);
        GroupDto groupDto = bizListAppService.removeBizListItemAssociationFromGroup(bizListId, bizListItemId, groupId);
        if(groupDto!=null) {
            eventAuditService.auditDual(AuditType.BIZ_LIST, "Unassign biz list item from group", AuditAction.UNASSIGN, SimpleBizListItemDto.class, bizListItemId, GroupDto.class,groupId,groupDto);
        }
        return groupDto;
    }

    @RequestMapping(value="/{bizListId}/item/{bizListItemId}/associate/groups", method = RequestMethod.DELETE)
    public void removeBizListItemAssociationFromGroups(@PathVariable long bizListId, @PathVariable String bizListItemId, @PathVariable String groupIds) {
        String[] groupIdsArray = StringUtils.split(groupIds, ',');
        for (String groupId : groupIdsArray) {
            removeBizListItemAssociationFromGroup(bizListId,bizListItemId,groupId);
        }
    }

    @RequestMapping(value="/templates", method = RequestMethod.GET)
    public List<ConsumerTemplateDto> getAllConsumerTemplates() {
        return bizListAppService.getAllConsumerTemplates();
    }

    @RequestMapping(value="/evaluate/status", method = RequestMethod.GET)
    public EvaluationProgressDto getEvaluationStatus() {
        EvaluationProgressDto result = new EvaluationProgressDto();
        if (future == null) {
            result.setRunStatus(RunStatus.NA);
        }
        else if (future.isDone()) {
            result.setRunStatus(RunStatus.FINISHED_SUCCESSFULLY);
        }
        else if (future.isCancelled()) {
            result.setRunStatus(RunStatus.STOPPED);
        }
        else {
            result.setRunStatus(RunStatus.RUNNING);
        }
        return result;
    }

    @RequestMapping(value="/type/{bizListItemType}/groups/extractions/evaluate", method = RequestMethod.POST)
    public void evaluateGroupsByExtraction(@PathVariable String bizListItemType, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        BizListItemType bizListItemTypeEnum = BizListItemType.valueOf(bizListItemType);
        EvaluateBizListInstructions evaluateBizListInstructions = bizListAppService.createEvaluationBizListInstructions(params);
        if (dataSourceRequest.isWait()) {
            bizListAppService.evaluateGroupsByExtraction(bizListItemTypeEnum, evaluateBizListInstructions);
        }
        else {
            future = executionManagerService.startAsyncTask(() -> {
                bizListAppService.evaluateGroupsByExtraction(bizListItemTypeEnum, evaluateBizListInstructions);
                return null;
            });
        }
    }

}
