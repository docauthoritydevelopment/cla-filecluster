package com.cla.filecluster.service.export.exporters.file;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.security.CompositeRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.CollectionResolverHelper.resolveCollectionValue;
import static com.cla.filecluster.service.export.exporters.file.FolderListHeaderFields.*;
import static java.lang.String.format;

@Slf4j
@Component
public class FolderListExporter extends PageCsvExcelExporter<AggregationCountFolderDto> {

	private final static String[] HEADER = {PATH, ASSOCIATION, DEPARTMENT, MATTER, DOC_TYPES, TAGS,
			ROLES, NUM_OF_DIRECT_FILTERED, NUM_OF_DIRECT, NUM_OF_FILES};

	@Autowired
	private FileTreeAppService fileTreeAppService;

	@Autowired
	private DepartmentService departmentService;

	@Override
	protected String[] getHeader() {
		return HEADER;
	}

	@Override
	protected void configureMapper(CsvMapper mapper) {
	}

	@Override
	protected Iterable<AggregationCountFolderDto> getData(Map<String, String> requestParams) {
		DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
		FacetPage<AggregationCountFolderDto> foldersFileCount = fileTreeAppService.findFoldersFileCount(dataSourceRequest);
		return foldersFileCount.getContent();
	}

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountFolderDto base, Map<String, String> requestParams) {
		Map<String, Object> result = new HashMap<>();
		DocFolderDto item = base.getItem();
		result.put(PATH, item.getPath());

		result.put(ASSOCIATION, resolveCollectionValue(item.getAssociatedBizListItems(),
				SimpleBizListItemDto::getName, SimpleBizListItemDto::isImplicit));

		result.put(DOC_TYPES, resolveCollectionValue(item.getDocTypeDtos(),
				DocTypeDto::getName, DocTypeDto::isImplicit));

		result.put(TAGS, resolveCollectionValue(item.getFileTagDtos(),
				t-> format("%s:%s", t.getTypeName(), t.getName()), FileTagDto::isImplicit));

		result.put(ROLES, resolveCollectionValue(item.getAssociatedFunctionalRoles(),
				CompositeRoleDto::getName, FunctionalRoleDto::getImplicit));


		if (item.getDepartmentDto() != null) {
			result.put(departmentService.getLegalInstallationMode(), item.getDepartmentDto().getFullName());
		}

		result.put(NUM_OF_DIRECT_FILTERED, base.getNumOfDirectFiles());
		result.put(NUM_OF_DIRECT, item.getNumOfDirectFiles());
		result.put(NUM_OF_FILES, item.getNumOfAllFiles());

		return result;
	}

	@Override
	public PagingMethod getPagingMethod() {
		return PagingMethod.MULTI;
	}

	@Override
	public boolean doesAccept(ExportType exportType, Map<String, String> params) {
		return exportType.equals(ExportType.FOLDER_LIST);
	}
}
