package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.export.ExtraFieldsWrapper;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.Collections;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class AddExtraFields<V> implements Transformer<Iterable<V>, Iterable<ExtraFieldsWrapper<V>>> {

    private final Map<String, String> params;
    private BiFunction<V, Map<String, String>, Map<String, Object>> conversionFunction;

    private AddExtraFields(Map<String, String> requestParams,
                                        BiFunction<V, Map<String, String>, Map<String, Object>> conversionFunction) {
        this.params = Collections.unmodifiableMap(requestParams);
        this.conversionFunction = conversionFunction;
    }

    public static <T> AddExtraFields<T> with(Map<String, String> requestParams,
                                                          BiFunction<T, Map<String, String>, Map<String, Object>> conversionFunction) {
        return new AddExtraFields<>(requestParams, conversionFunction);
    }

    @Override
    public Function<Flux<Iterable<V>>, Publisher<DataBuffer>> asDataBuffer() {
        throw new UnsupportedOperationException("ExtraFieldsAdderTransformer cannot yet be transformed directly to a data output stream");
    }

    @Override
    public Publisher<Iterable<ExtraFieldsWrapper<V>>> apply(Flux<Iterable<V>> inputFlux) {
        return inputFlux
                .map(iterable -> iterableToStream(iterable)
                    .map(t -> getExtraFieldsWrapper(t, params))
                    .collect(Collectors.toList()));
    }

    @NotNull
    private Stream<V> iterableToStream(Iterable<V> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false);
    }

    private ExtraFieldsWrapper<V> getExtraFieldsWrapper(V base, Map<String, String> requestParams) {
        Map<String, Object> extraFields = conversionFunction.apply(base, requestParams);
        ExtraFieldsWrapper<V> result = new ExtraFieldsWrapper<>(base);
        if (extraFields != null) {
            result.putExtraFields(extraFields);
        }
        return result;
    }
}
