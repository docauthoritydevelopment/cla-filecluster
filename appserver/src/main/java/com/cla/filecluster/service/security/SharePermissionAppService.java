package com.cla.filecluster.service.security;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SharePermissionAppService {

    private static final Logger logger = LoggerFactory.getLogger(SharePermissionAppService.class);

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private FileCatService fileCatService;

    public FacetPage<AggregationCountItemDTO<RootFolderSharePermissionDto>> listCountWithFileFilter(
            DataSourceRequest dataSourceRequest, AclType aclType) {
        try {
            PageRequest pageRequest = dataSourceRequest.createPageRequest();

            Map<Long, List<RootFolderSharePermissionDto>> sharePermissions =
                    sharePermissionService.getAllSharePermissions(aclType);
            if (sharePermissions == null || sharePermissions.isEmpty()) {
                return null;
            }
            String additionalFilter = (CatFileFieldType.ROOT_FOLDER_ID.inQuery(sharePermissions.keySet()));
            Pair<Map<String, Long>, Map<String, Long>> res = runQuery(dataSourceRequest, additionalFilter);

            if (res == null || res.getKey() == null || res.getKey().isEmpty()) {
                logger.debug("list file facets by Solr field root folder returned no results");
                return new FacetPage<>(new ArrayList<>(), pageRequest);
            }

            logger.debug("list file facets by Solr field root folder returned {} results", res.getKey().size());

            return convertFacetFieldResultToDto(res,
                    pageRequest,
                    dataSourceRequest.getSelectedItemId(), aclType);
        } catch (RuntimeException e) {
            logger.error("Failed to list shared permission counts with filter {}", dataSourceRequest.getFilter(), e);
            throw e;
        }
    }

    private Pair<Map<String, Long>, Map<String, Long>> runQuery(DataSourceRequest dataSourceRequest, String additionalFilter) {
        int page = dataSourceRequest.getPage();
        int pageSize = dataSourceRequest.getPageSize();

        dataSourceRequest.setPage(1);
        dataSourceRequest.setPageSize(ControllerUtils.FACET_MAX_PAGE_SIZE_FILECOUNT);
        Map<String, Long> filteredMap = fileCatService.getFieldTermCountMap(dataSourceRequest,
                CatFileFieldType.ROOT_FOLDER_ID, additionalFilter);

        Map<String, Long> noFilterMap = new HashMap<>();
        if (dataSourceRequest.hasFilter()) {
            dataSourceRequest.clearFilters();
            noFilterMap = fileCatService.getFieldTermCountMap(dataSourceRequest, CatFileFieldType.ROOT_FOLDER_ID,
                    additionalFilter);
        }

        dataSourceRequest.setPage(page);
        dataSourceRequest.setPageSize(pageSize);

        return Pair.of(filteredMap, noFilterMap);
    }

    private AggregationCountItemDTO<RootFolderSharePermissionDto> getIfExists(
            List<AggregationCountItemDTO<RootFolderSharePermissionDto>> all, RootFolderSharePermissionDto current) {
        for (AggregationCountItemDTO<RootFolderSharePermissionDto> item : all) {
            RootFolderSharePermissionDto dto = item.getItem();
            if (current.getAclEntryType().equals(dto.getAclEntryType()) &&
                    current.getUser().equals(dto.getUser())) {
                return item;
            }
        }
        return null;
    }

    private static String createItemIdentifier(RootFolderSharePermissionDto dto) {
        return dto.toItemIdentifier();
    }

    private FacetPage<AggregationCountItemDTO<RootFolderSharePermissionDto>> convertFacetFieldResultToDto(
            Pair<Map<String, Long>, Map<String, Long>> result, PageRequest pageRequest, String selectedItemId,
            AclType aclType) {

        List<AggregationCountItemDTO<RootFolderSharePermissionDto>> content = new ArrayList<>();

        Map<String, Long> filteredResults = result.getKey();
        Map<String, Long> notFilteredResults = result.getValue();

        Map<Long, List<RootFolderSharePermissionDto>> sharePermissions = sharePermissionService.getAllSharePermissions(aclType);

        logger.debug("convert Facet Field result for {} elements. DB returned {} elements", filteredResults.size());
        long pageAggCount = 0;
        RootFolderSharePermissionDto selected = null;
        for (Map.Entry<String, Long> entry : filteredResults.entrySet()) {
            long rootFolderId = Long.parseLong(entry.getKey());
            Long countFiltered = entry.getValue();
            Long countNotFiltered = (notFilteredResults == null ? null : notFilteredResults.get(entry.getKey()));

            List<RootFolderSharePermissionDto> rfSP = sharePermissions.get(rootFolderId);
            if (rfSP != null) {
                for (RootFolderSharePermissionDto permission : rfSP) {
                    AggregationCountItemDTO<RootFolderSharePermissionDto> item = getIfExists(content, permission);
                    if (item == null) {
                        RootFolderSharePermissionDto dto = new RootFolderSharePermissionDto();
                        dto.setId(permission.getId());
                        dto.setUser(permission.getUser());
                        dto.setAclEntryType(permission.getAclEntryType());
                        dto.setMask(permission.getMask());
                        dto.setTextPermissions(permission.getTextPermissions());
                        dto.setRootFolderIds(Lists.newArrayList(rootFolderId));
                        item = new AggregationCountItemDTO<>(dto, countFiltered);
                        content.add(item);
                        if (countNotFiltered != null) {
                            item.setCount2(countNotFiltered);
                        }
                        if (selectedItemId != null && selectedItemId.equals(String.valueOf(permission.getId()))) {
                            selected = permission;
                        }
                    } else {
                        item.incrementCount(countFiltered);
                        if (countNotFiltered != null) {
                            item.incrementCount2(countNotFiltered);
                        }
                        item.getItem().getRootFolderIds().add(rootFolderId);
                    }
                }
            }
            pageAggCount += countFiltered;
        }
        // even though we must go over all facet options to get correct values
        // we should return to ui only what was requested
        return ControllerUtils.getRelevantResultPage(content, pageRequest,
                selected == null ? null : selected.toItemIdentifier(),
                SharePermissionAppService::createItemIdentifier,
                pageAggCount, (long) filteredResults.size());
    }
}
