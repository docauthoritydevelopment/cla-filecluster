package com.cla.filecluster.service.progress;

import com.cla.connector.utils.Pair;
import lombok.Data;

@Data(staticConstructor = "of")
public class ProgressMessage {
    private final int percentage;
    private final String message;
    private final Pair<String, Object> payload;
    private final boolean done;
}
