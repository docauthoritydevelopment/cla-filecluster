package com.cla.filecluster.service.crawler.scan;

import com.cla.common.constants.CatFileFieldType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.license.TrackerAppService;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Responsible to track the count of new files that becomes grouped mainly via analyze finalize
 */
@Component
public class CopyFileGroupToFilesTracker {

    private static final Logger logger = LoggerFactory.getLogger(CopyFileGroupToFilesTracker.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private TrackerAppService trackerAppService;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @PostConstruct
    private void init() {
        eventBus.subscribe(Topics.FILE_GROUP_ATTACH, event -> {
            if (event.getEventType() != EventType.BULK_GROUP_ATTACH) { // Case not BULK_GROUP_ATTACH drop it
                if (logger.isTraceEnabled()) {
                    logger.trace("Listening only to {}, Dropping event {}", EventType.BULK_GROUP_ATTACH, event);
                }
                return;
            }

            // Case required keys not exists drop it
            if (!event.getContext().containsKey(EventKeys.NEW_GROUPED_FILES)) {
                logger.trace("Event is missing key {} dropping it , {}", EventKeys.NEW_GROUPED_FILES, event);
                return;
            }

            AuthenticationHelper.doWithAuthentication(AutoAuthenticate.ADMIN, true,
                    () -> accumulateNewGroupedFiles(event), null);
        });
    }

    private void accumulateNewGroupedFiles(Event event) {
        final Context context = event.getContext();

        List<String> newGroupedFileIds = context.get(EventKeys.NEW_GROUPED_FILES, List.class);
        if (newGroupedFileIds.isEmpty()) {
            logger.warn("Received empty new grouped files, discarding event {}", event);
            return;
        }


        logger.debug("Incrementing new grouped files by {}", newGroupedFileIds.size());
        trackerAppService.incrementLongCount(TrackingKeys.GROUPED_FILES, newGroupedFileIds.size());

        Map<Long, Long> countNewGroupedFilesByRootFolders = countNewGroupedFilesByField(newGroupedFileIds,
                CatFileFieldType.ROOT_FOLDER_ID, Long::valueOf, 10000);
        logger.debug("Incrementing new grouped files by {} for {} root folders",
                countNewGroupedFilesByRootFolders.keySet().size(), newGroupedFileIds.size());
        countNewGroupedFilesByRootFolders.forEach((rfId, count) ->
                trackerAppService.incrementLongCount(TrackingKeys.rootFolderNewGroupedFilesCountKey(rfId), count));

        Map<MediaType, Long> countNewGroupedFilesByMediaType = countNewGroupedFilesByField(newGroupedFileIds,
                CatFileFieldType.MEDIA_TYPE,
                facetName -> MediaType.valueOf(Integer.valueOf(facetName)),
                MediaType.values().length);
        logger.debug("Incrementing new grouped files by {} for {} media types",
                countNewGroupedFilesByMediaType.keySet().size(), newGroupedFileIds.size());
        countNewGroupedFilesByMediaType.forEach((mediaType, count) ->
                trackerAppService.incrementLongCount(TrackingKeys.mediaNewGroupedFilesCountKey(mediaType), count));
    }

    private <T> Map<T, Long> countNewGroupedFilesByField(List<String> newGroupedFiles,
                                                         CatFileFieldType catFileFieldType,
                                                         Function<String, T> conversion,
                                                         int limit) {
        // Build query
        final SolrQuery solrQuery = Query.create()
                .setFacetMinCount(1)
                .setFacetLimit(limit)
                .setRows(0)
                .addFacetField(catFileFieldType)
                .addFilterQuery(Query.create()
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.ID, SolrOperator.IN, newGroupedFiles)))
                .build();

        // Run query
        QueryResponse queryResponse = solrFileCatRepository.runQuery(solrQuery);
        if (queryResponse.getFacetFields() == null || queryResponse.getFacetFields().isEmpty()) {
            int toIndex = Math.min(5, newGroupedFiles.size());
            logger.warn("Empty count of new grouped files by {}!!, sample ids {}", catFileFieldType,
                    newGroupedFiles.subList(0, toIndex));
            return Maps.newHashMap();
        }

        // Extract results
        FacetField facetField = queryResponse.getFacetFields().get(0);
        return facetField.getValues().stream()
                .collect(Collectors.toMap(count -> conversion.apply(count.getName()), FacetField.Count::getCount));
    }

}
