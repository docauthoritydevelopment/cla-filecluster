package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.security.DomainAccessHandler;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class FileAclReTranslateDomain implements ScheduledOperation {

    public static String DOMAIN_PARAM = "DOMAIN";

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private DomainAccessHandler domainAccessHandler;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            String domain = opData.get(DOMAIN_PARAM);
            fileCatAppService.reTranslateFileAcl(domain);
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String domain = opData.get(DOMAIN_PARAM);
        if (Strings.isNullOrEmpty(domain)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.domain.empty"), BadRequestType.MISSING_FIELD);
        }
        if (!domainAccessHandler.doesFileShareDomainHasTranslation(domain)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.domain.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.ACL_RE_TRANSLATE_DOMAIN;
    }
}
