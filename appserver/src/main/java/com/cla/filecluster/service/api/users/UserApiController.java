package com.cla.filecluster.service.api.users;

import com.cla.common.domain.dto.security.BizRoleDto;
import com.cla.common.domain.dto.security.PasswordUpdateDto;
import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.security.UserSavedData;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.service.security.UserAppService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.collect.Lists;
import com.rometools.utils.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.cla.common.domain.dto.security.Authority.Const.*;

/**
 * Created by uri on 17/07/2016.
 */
@SuppressWarnings("UnusedReturnValue")
@RestController
@RequestMapping("/api/user")
public class UserApiController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserAppService userAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private MessageHandler messageHandler;

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_USERS})
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<UserDto> getAllUsers(@RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return userAppService.getAllUsers(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_USERS})
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public UserDto getUserByIdWithRoles(@PathVariable final long userId) {
        return userAppService.getUserByIdWithRoles(userId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS})
    @RequestMapping(value = "/new", method = RequestMethod.PUT)
    public UserDto createUserOnCurrentAccount(@RequestBody final UserDto userDto) {
        UserDto userOnCurrentAccount = userAppService.createUserOnCurrentAccount(userDto);
        eventAuditService.audit(AuditType.USER, "Create new user", AuditAction.CREATE, UserDto.class, userDto);
        return userOnCurrentAccount;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS, MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}", method = RequestMethod.POST)
    public UserDto updateUser(@PathVariable long userId, @RequestBody final UserDto updatedUser) {
        getUser(userId);
        UserDto userDto = userAppService.updateUser(userId, updatedUser);
        eventAuditService.audit(AuditType.USER, "Update user", AuditAction.UPDATE, UserDto.class, userDto);
        return userDto;
    }


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS, MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}/workgroups/assign", method = RequestMethod.POST)
    public UserDto assignUserWorkGroups(@PathVariable long userId, @RequestBody List<Long> workGroupIds) {
        getUser(userId);
        UserDto userDto = userAppService.assignWorkGroups(userId, workGroupIds);
        eventAuditService.audit(AuditType.USER, "Assign user to work group(s)", AuditAction.ASSIGN, UserDto.class, userDto, workGroupIds);
        return userDto;
    }


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS, MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}/workgroups/unassign", method = RequestMethod.POST)
    public UserDto unAssignUserWorkGroups(@PathVariable long userId, @RequestBody List<Long> workGroupIds) {
        getUser(userId);
        UserDto userDto = userAppService.unAssignWorkGroups(userId, workGroupIds);
        eventAuditService.audit(AuditType.USER, "UnAssign user from work group(s)", AuditAction.UNASSIGN, UserDto.class, userDto, workGroupIds);
        return userDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS})
    @RequestMapping(value = "/{userId}/unlock", method = RequestMethod.POST)
    public UserDto unlockUser(@PathVariable long userId) {
        getUser(userId);
        UserDto userDto = userAppService.unlockUser(userId);
        eventAuditService.audit(AuditType.USER, "Unlock user", AuditAction.UPDATE, UserDto.class, userDto);
        return userDto;
    }


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS})
    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    public void deleteUserById(@PathVariable final int userId) {
        getUser(userId);
        try {
            UserDto userDto = userService.archiveUserById(userId);
            eventAuditService.audit(AuditType.USER, "Delete user", AuditAction.DELETE, UserDto.class, userId, userDto);
        } catch (BadRequestException e) {
            throw e;
        } catch (Exception e) {
            throw new BadRequestException(messageHandler.getMessage("user.delete.failure", Lists.newArrayList(e)), BadRequestType.OPERATION_FAILED);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}/role/{roleId}", method = RequestMethod.PUT)
    public void addRoleToUser(@PathVariable final int userId, @PathVariable final int roleId) {
        userService.addRoleToUser(userId, roleId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}/role/{roleId}", method = RequestMethod.DELETE)
    public void removeRoleFromUser(@PathVariable final int userId, @PathVariable final int roleId) {
        getUser(userId);
        userService.removeRoleFromUser(userId, roleId);

    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}/bizrole/{bizRoleId}", method = RequestMethod.PUT)
    public UserDto addBizRoleToUser(@PathVariable final long userId, @PathVariable final Long bizRoleId) {
        getUser(userId);
        getBizRole(bizRoleId);
        UserDto userDto = userAppService.addBizRoleToUser(userId, bizRoleId);
        if (userDto != null) {
            eventAuditService.auditDual(AuditType.USER, "Attach role to user", AuditAction.ATTACH, UserDto.class, userId, BizRoleDto.class, bizRoleId, userDto);
        }
        return userDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USER_ROLES})
    @RequestMapping(value = "/{userId}/bizrole/{bizRoleId}", method = RequestMethod.DELETE)
    public UserDto removeBizRoleFromUser(@PathVariable final long userId, @PathVariable final Long bizRoleId) {
        getUser(userId);
        getBizRole(bizRoleId);
        UserDto userDto = userAppService.removeBizRoleFromUser(userId, bizRoleId);
        if (userDto != null) {
            eventAuditService.auditDual(AuditType.USER, "DeAttach role from user", AuditAction.DETACH, UserDto.class, userId, BizRoleDto.class, bizRoleId, userDto);
        }
        return userDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public UserDto getCurrentUserWithRoles() {
        UserDto currentUserWithRoles = userAppService.getCurrentUserWithRoles(false);
        currentUserWithRoles.setLoginLicenseExpired(licenseService.isLoginExpired());
        return currentUserWithRoles;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/current/with-access-tokens", method = RequestMethod.GET)
    public UserDto getCurrentUserWithRolesAndAccessTokens() {
        UserDto currentUserWithRoles = userAppService.getCurrentUserWithRoles(true);
        currentUserWithRoles.setLoginLicenseExpired(licenseService.isLoginExpired());
        return currentUserWithRoles;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/current/roles", method = RequestMethod.GET)
    public Set<SystemRoleDto> getCurrentUserRoles() {
        return userAppService.getCurrentUserRoles();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS})
    @RequestMapping(value = "/{userId}/password", method = RequestMethod.POST)
    public void updateUserPassword(@PathVariable long userId, @RequestBody final PasswordUpdateDto passwordUpdate) {
        if (passwordUpdate == null || passwordUpdate.getNewPassword() == null) {
            throw new BadRequestException(messageHandler.getMessage("user.password.empty"), BadRequestType.MISSING_FIELD);
        }
        try {
            userService.resetUserPassword(userId, passwordUpdate.getNewPassword());
            eventAuditService.audit(AuditType.USER, "Reset user password", AuditAction.UPDATE, UserDto.class, userId);
        } catch (Exception e) {
            if (e.getMessage().toLowerCase().contains("password")) {
                throw new BadRequestException(messageHandler.getMessage("user.pw-cng.unauth"), BadRequestType.UNAUTHORIZED);
            }
            throw new BadRequestException(messageHandler.getMessage("user.pw-cng.failure", Lists.newArrayList(e)), BadRequestType.OPERATION_FAILED);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USERS, MNG_USER_ROLES})
    @RequestMapping(value = "/current", method = RequestMethod.POST)
    public UserDto updateCurrentUser(@RequestBody final UserDto updatedUser) {
        UserDto userDto = UserService.convertUser(userService.updateCurrentUser(updatedUser), false);
        eventAuditService.audit(AuditType.USER, "User update profile", AuditAction.UPDATE, UserDto.class, userDto.getId(), userDto);
        return userDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/current/password", method = RequestMethod.POST)
    public void changeCurrentUserPassword(@RequestBody final PasswordUpdateDto passwordUpdate) {
        if (passwordUpdate == null || passwordUpdate.getOldPassword() == null || passwordUpdate.getNewPassword() == null) {
            throw new BadRequestException(messageHandler.getMessage("user.password.empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            userService.changeCurrentUserPassword(passwordUpdate.getNewPassword(), passwordUpdate.getOldPassword());
            eventAuditService.audit(AuditType.USER, "User change password", AuditAction.UPDATE);
        } catch (Exception e) {
            if (e.getMessage().toLowerCase().contains("password")) {
                throw new BadRequestException(messageHandler.getMessage("user.pw-cng.unauth"), BadRequestType.UNAUTHORIZED);
            }
            throw new BadRequestException(messageHandler.getMessage("user.pw-cng.failure", Lists.newArrayList(e)), BadRequestType.OPERATION_FAILED);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/saveddata/{key}", method = RequestMethod.PUT)
    public void addUserSavedData(@PathVariable final String key, @RequestBody String savedData) {
        userService.addUserSavedData(key, savedData);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/saveddata/{key}", method = RequestMethod.GET)
    public UserSavedData getUserSavedData(@PathVariable final String key) {
        return userService.getUserSavedData(key);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @RequestMapping(value = "/saveddata/{key}", method = RequestMethod.DELETE)
    public void deleteUserSavedData(@PathVariable final String key, @RequestBody String savedData) {
        userService.deleteUserSavedData(key, savedData);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USER_ROLES})
    @RequestMapping(value = "/roles" ,method = {RequestMethod.GET, RequestMethod.POST})
    public List<SystemRoleDto> getRolesWithAuthorities() {
        return userAppService.getRolesWithAuthorities();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_USER_ROLES})
    @RequestMapping(value = "/roles/name/{name}",method = {RequestMethod.GET, RequestMethod.POST})
    public SystemRoleDto getSystemRoleByName(@PathVariable String name) {
        return userAppService.getSystemRoleByName(name);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES})
    @RequestMapping(value = "/bizroles",method = {RequestMethod.GET, RequestMethod.POST})
    public List<BizRoleDto> getBizRoles() {
        return userAppService.getBizRoles();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES})
    @RequestMapping(value = "/bizroles/{id}", method = RequestMethod.GET)
    public BizRoleDto getBizRole(@PathVariable long id) {
        BizRoleDto bizRole = userAppService.getBizRoleById(id);
        if (bizRole == null) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return bizRole;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES})
    @RequestMapping(value = "/bizroles", method = RequestMethod.PUT)
    public BizRoleDto createBizRole(@RequestBody BizRoleDto bizRoleDto) {
        if (Strings.isEmpty(bizRoleDto.getName())) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.name.empty"), BadRequestType.MISSING_FIELD);
        }
        if (Strings.isEmpty(bizRoleDto.getDisplayName())) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.display-name.empty"), BadRequestType.MISSING_FIELD);
        }
        if (bizRoleDto.getTemplate() == null || bizRoleDto.getTemplate().getId() == null || bizRoleDto.getTemplate().getId() < 1) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.role-template.empty"), BadRequestType.MISSING_FIELD);
        }
        BizRoleDto bizRole = userAppService.createBizRole(bizRoleDto);
        eventAuditService.audit(AuditType.OPERATIONAL_ROLE, "Create operational role", AuditAction.CREATE, BizRoleDto.class, bizRole.getId(), bizRole);
        return bizRole;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES, MNG_USERS, MNG_USER_ROLES})
    @RequestMapping(value = "/bizroles/{id}", method = RequestMethod.DELETE)
    public BizRoleDto deleteBizRole(@PathVariable long id) {
        BizRoleDto bizRole = getBizRole(id);
        BizRoleDto bizRoleDto = userAppService.archiveBizRole(id);
        eventAuditService.audit(AuditType.OPERATIONAL_ROLE, "Delete operational role", AuditAction.DELETE, BizRoleDto.class, bizRole.getId(), bizRole);
        return bizRoleDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES})
    @RequestMapping(value = "/bizroles/{id}", method = RequestMethod.POST)
    public BizRoleDto updateBizRole(@PathVariable Long id, @RequestBody BizRoleDto bizRoleDto) {
        BizRoleDto bizRoleDto1 = userAppService.updateBizRole(id, bizRoleDto);
        eventAuditService.audit(AuditType.OPERATIONAL_ROLE, "Update operational role", AuditAction.UPDATE, BizRoleDto.class, bizRoleDto1.getId(), bizRoleDto1);
        return bizRoleDto1;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES})
    @RequestMapping(value = "/comproles/{id}/users", method = {RequestMethod.GET, RequestMethod.POST})
    public Page<UserDto> getCompRoleAssignedUsers(@PathVariable Long id, @RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return userAppService.getCompRoleAssignedUsers(id, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_ROLES})
    @RequestMapping(value = "/bizroles/name/{name}", method = RequestMethod.GET)
    public BizRoleDto getBizRoleByName(@PathVariable String name) {
        BizRoleDto role = userAppService.getBizRoleByName(name);
        if (role == null) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return role;
    }

    private UserDto getUser(long id) {
        UserDto userById = userAppService.getUserById(id);
        if (userById == null) {
            throw new BadRequestException(messageHandler.getMessage("user.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return userById;
    }
}
