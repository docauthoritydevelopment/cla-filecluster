package com.cla.filecluster.service.export.exporters.components;

import com.cla.common.domain.dto.components.ClaComponentDto;
import com.cla.common.domain.dto.components.ClaComponentSummaryDto;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.components.mixin.ClaComponentDtoMixin;
import com.cla.filecluster.service.export.exporters.components.mixin.CustomerDataCenterDtoMixin;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
@Slf4j
@Component
public class ServerComponentsExporter extends PageCsvExcelExporter<ClaComponentDto> {

    @Autowired
    private SysComponentService sysComponentService;

    private final static String[] header = {
            NAME, INSTANCE, STATE, DATA_CENTER, EXTERNAL_ADDRESS, RESPONSE_DATE, STATE_CNG_DATE};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.SYSTEM_COMPONENTS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ClaComponentDto.class, ClaComponentDtoMixin.class)
            .addMixIn(CustomerDataCenterDto.class, CustomerDataCenterDtoMixin.class);
    }

    @Override
    protected Page<ClaComponentDto> getData(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        Page<ClaComponentSummaryDto> dtos = sysComponentService.listComponentsWithStatus(dataSourceRequest, params);
        List<ClaComponentDto> result = new ArrayList<>();
        dtos.getContent().forEach(dto -> {
            result.add(dto.getClaComponentDto());
        });
        return new PageImpl<>(result);
    }

    @Override
    protected Map<String, Object> addExtraFields(ClaComponentDto base, Map<String, String> requestParams) {
        return null;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
