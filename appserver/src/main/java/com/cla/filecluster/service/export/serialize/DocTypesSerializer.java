package com.cla.filecluster.service.export.serialize;

import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

import static com.cla.filecluster.service.export.exporters.file.CollectionResolverHelper.resolveCollectionValue;

public class DocTypesSerializer extends JsonSerializer<Set<DocTypeDto>> {
    @Override
    public void serialize(Set<DocTypeDto> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(resolveCollectionValue(value,
                DocTypeDto::getFullName, DocTypeDto::isImplicit));
    }
}
