package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.associations.DepartmentHeaderFields.*;

@Slf4j
@Component
public class DepartmentListExporter extends DepartmentExporter {

    private final static String[] HEADER = {NAME, NUM_OF_FILES, DESCRIPTION};

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected Page<AggregationCountItemDTO<DepartmentDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<DepartmentDto>> res = departmentAppService.findDepartmentFileCount(dataSourceRequest, false);
        return new PageImpl<>(res.getContent());
    }

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<DepartmentDto> base, Map<String, String> requestParams) {
        Map<String, Object> res = super.addExtraFields(base, requestParams);
        res.put(NUM_OF_FILES, base.getCount());
        return res;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DEPARTMENTS_LIST) || exportType.equals(ExportType.MATTERS_LIST);
    }
}
