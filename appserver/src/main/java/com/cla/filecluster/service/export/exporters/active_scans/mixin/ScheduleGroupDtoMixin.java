package com.cla.filecluster.service.export.exporters.active_scans.mixin;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.active_scans.ActiveScansHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class ScheduleGroupDtoMixin {

    @JsonProperty(SCHEDULE_GROUP)
    private String name;

}
