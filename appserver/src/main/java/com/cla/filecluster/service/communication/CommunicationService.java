package com.cla.filecluster.service.communication;

import com.cla.common.domain.dto.email.SmtpConfigurationDto;
import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.filecluster.service.command.AsyncCommandService;
import com.cla.filecluster.util.template.TemplateContext;
import com.cla.filecluster.util.template.TemplateEngine;
import com.cla.filecluster.util.template.VelocityTemplateEngine;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;


/**
 * Basic service for sending emails
 * Created by vladi on 3/27/2017.
 */
@Service
public class CommunicationService {

    private final static Logger logger = LoggerFactory.getLogger(CommunicationService.class);

    @Value("${smtp.debug:true}")
    private Boolean smtpDebug;

    @Value("${smtp.timeout.millis:5000}")
    private Integer smtpTimeout;

    @Value("${smtp.connectionTimeout.millis:5000}")
    private Integer connectionTimeout;

    @Value("${smtp.sender.core-pool-size:3}")
    private int corePoolSize;

    @Value("${smtp.sender.max-pool-size:5}")
    private int maxPoolSize;

    @Value("${smtp.sender.queue-capacity:100}")
    private int queueCapacity;

    private AsyncCommandService commandService;
    private JavaMailSenderImpl mailSenderCache;
    private TemplateEngine templateEngine;


    @Autowired
    private EmailConfigurationService emailConfigurationService;

    @Autowired
    private ApplicationContext ctx;

    @PostConstruct
    public void init() {
        commandService = AsyncCommandService.create("comm-serv-acs-",
                corePoolSize, maxPoolSize, queueCapacity, null);
        templateEngine = new VelocityTemplateEngine();
    }


    public TestResultDto testConnection(SmtpConfigurationDto smtpConfigurationDto) {
        try {
            createMailSender(smtpConfigurationDto).testConnection();
            return new TestResultDto(true);
        } catch (MessagingException e) {
            logger.warn("SMTP connection failed. " + e.getMessage());
            return new TestResultDto(false, e.getMessage());
        }
    }

    private JavaMailSenderImpl getMailSender() {
        if (mailSenderCache == null) {
            mailSenderCache = createMailSender();
        }
        return mailSenderCache;
    }

    public void send(EmailProperties props) {
        SendEmailCommand sendEmailCommand = propertiesToCommand(props);
        commandService.submit(sendEmailCommand);
    }

    String createSampleHtml(EmailProperties properties) {
        if (properties.getBodyTemplate() == null) {
            throw new RuntimeException("Missing body template. Can't create Sample HTML");
        }
        SendEmailCommand emailCommand = propertiesToCommand(properties);
        return emailCommand.createBodyFromTemplate();
    }

    private SendEmailCommand propertiesToCommand(EmailProperties props) {
        TemplateContext subjectTemplate = null;
        if (props.getSubjectTemplate() != null) {
            subjectTemplate = getTemplateContext(props.getSubjectTemplate());
            subjectTemplate.putAll(props.getContext());
        }
        TemplateContext bodyTemplate = getTemplateContext(props.getBodyTemplate());
        bodyTemplate.putAll(props.getContext());

        String replyToEmail = props.getReplyToEmail() != null ? props.getReplyToEmail() : props.getFromEmail();
        SendEmailCommand command = ctx.getBean(SendEmailCommand.class)
                .setMailSender(getMailSender())
                .setFromEmail(props.getFromEmail())
                .setReplyToEmail(replyToEmail)
                .setToEmails(props.getToEmails().toArray(new String[props.getToEmails().size()]))
                .setPlainSubject(props.getPlainSubject())
                .setSubjectTemplate(subjectTemplate);
        command .setBodyTemplate(bodyTemplate);
        return command;
    }

    private TemplateContext getTemplateContext(String templateName) {
        TemplateContext template;
        try {
            template = templateEngine.getTemplate(templateName);
        } catch (Exception e) {
            logger.error("Cannot get template name {}. {}", templateName, e.getMessage());
            throw new RuntimeException("Cannot get template name " + templateName, e);
        }
        return template;
    }

    /**
     * Create SMTP sender
     *
     * @return JavaMailSenderImpl
     */
    private JavaMailSenderImpl createMailSender() {
        SmtpConfigurationDto smtpConfigurationDto = emailConfigurationService.getSmtpConfiguration(false);
        return createMailSender(smtpConfigurationDto);
    }

    private JavaMailSenderImpl createMailSender(SmtpConfigurationDto smtpConfigurationDto) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setDefaultEncoding(CharEncoding.UTF_8);
        mailSender.setHost(smtpConfigurationDto.getHost());
        if (smtpConfigurationDto.getPort() != null) {
            mailSender.setPort(smtpConfigurationDto.getPort());
        }
        mailSender.setUsername(smtpConfigurationDto.getUsername());
        String password = smtpConfigurationDto.getPassword();
        if (smtpConfigurationDto.isPasswordEncrypted()) {
            password = emailConfigurationService.decryptPassword(password);
        }
        mailSender.setPassword(password);

        mailSender.getJavaMailProperties().put("mail.debug", smtpDebug);
        mailSender.getJavaMailProperties().put("mail.smtp.timeout", smtpTimeout);
        mailSender.getJavaMailProperties().put("mail.smtp.connectiontimeout", connectionTimeout);
        mailSender.getJavaMailProperties().put("mail.smtp.auth", smtpConfigurationDto.getSmtpAuthentication());
        mailSender.getJavaMailProperties().put("mail.smtp.starttls.enable", smtpConfigurationDto.isUseTls());
        if (smtpConfigurationDto.isUseSsl()) {
            mailSender.getJavaMailProperties().put("mail.transport.protocol", "smtps");
        }
        return mailSender;
    }

    public void resetCache() {
        logger.debug("Reset MailSender cache");
        mailSenderCache = null;
    }
}
