package com.cla.filecluster.service.export.exporters.settings.tag;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface TagHeaderFields {
    String TYPE = "Type";
    String NAME = "Name";
    String PROPERTIES = "Properties";
    String SINGLE_VAL = "Single value type";
    String DESCRIPTION = "Description";
    String TYPE_DESCRIPTION = "Type Description";
}
