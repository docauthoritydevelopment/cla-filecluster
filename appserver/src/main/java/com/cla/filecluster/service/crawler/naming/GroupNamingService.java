package com.cla.filecluster.service.crawler.naming;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.FileGroupsFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.group.GroupNamingInfoDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.mediaproc.excel.IngestConstants;
import com.cla.common.domain.dto.pv.TitleToken;
import com.cla.common.utils.LoggingUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.progress.ProgressTracker;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.SolrFileGroupBuilder;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.jobmanager.service.CoordinationLockDeniedException;
import com.cla.filecluster.jobmanager.service.JobCoordinator;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.solr.GroupSearchRepository;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.repository.solr.TitlesSearchRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.pv.SolrDocHelper;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.operation.SubGroupService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.cla.common.constants.FileGroupsFieldType.*;

@Service
public class GroupNamingService {

    private static final Logger logger = LoggerFactory.getLogger(GroupNamingService.class);

    @Value("${groupNaming.maxGroupIndexedFiles:1000}")
    private int maxGroupIndexedFiles;

    @Value("${groupNaming.sleepAfterSolrCommitTimeMs:100}")
    private long sleepAfterSolrCommitTimeMs;

    @Value("${groupNaming.indexAllBeforeNaming:false}")
    private boolean indexAllBeforeNaming;

    // Number of files to index together for group naming
    @Value("${groupNaming.filesCountNamingThreshold:100}")
    private int filesCountNamingThreshold;

    @Value("${groupNaming.maxSizeForTitles:10}")
    private int maxSizeForTitles;

    @Value("${groupNaming.dbPpageSize:1000}")
    private int nameingDbPpageSize;

    @Value("${groupNaming.pageSizeForIndexingTitles:100}")
    private int pageSizeForIndexingTitles;

    @Value("${groupNaming.recalculate.threshold:0.2}")
    private double recalculateThreshold;

    @Value("${groupNaming.skipTermsThreshold:0.3}")
    private double skipTermsThresholdProp;

    @Value("${groupNaming.topTitleTokenFactor.tfidf:4}")
    private int topTitleTokenFactorTfidf;
    @Value("${groupNaming.topTitleTokenFactor.tf:10}")
    private int topTitleTokenFactorTf;
    @Value("${groupNaming.topTitleTokenFactor.length:14}")
    private int topTitleTokenFactorLength;
    @Value("${groupNaming.topTitleTokenFactor.numWords:6}")
    private int topTitleTokenFactorNumWords;
    @Value("${groupNaming.topTitleTokenFactor.boost:10}")
    private int topTitleTokenFactorBoost;
    @Value("${groupNaming.topTitleTokenFactor.titleScore:6}")
    private int topTitleTokenFactorTitleScore;
    @Value("${groupNaming.topTitleTokenFactor.TitleOrder:3}")
    private int topTitleTokenFactorTitleOrder;
    @Value("${groupNaming.topTitleTokenFactor.optimalTitleLength:40}")
    private int optimalTitleLength;
    @Value("${groupNaming.topTitleTokenFactor.minRatedTitleLength:3}")
    private int minRatedTitleLength;
    @Value("${groupNaming.topTitleTokenFactor.maxRatedTitleLength:120}")
    private int maxRatedTitleLength;

    @Value("${groupNaming.topTitleBoost.firstTitleValue:1.0}")
    private double firstTitleBoostValue;
    @Value("${groupNaming.topTitleBoost.secondTitleValue:0.7}")
    private double secondTitleBoostValue;
    @Value("${groupNaming.topTitleBoost.thirdTitleValue:0.3}")
    private double thirdTitleBoostValue;
    @Value("${groupNaming.topTitleBoost.titlesValue:0}")
    private double titlesBoostValue;

    @Value("${groupNaming.topTitleBoost.topTitlesValue:20.0}")
    private double topTitlesBoostValue;
    @Value("${groupNaming.topTitleBoost.fileNameValue:4.0}")
    private double fileNameBoostValue;
    @Value("${groupNaming.topTitleBoost.parentDirValue:2.0}")
    private double parentDirBoostValue;
    @Value("${groupNaming.topTitleBoost.grandParentDirValue:1.0}")
    private double grandParentDirBoostValue;

    @Value("${groupNaming.useFilename:true}")
    private boolean useFilename;

    @Value("${group.naming.byDirty:true}")
    private boolean groupNamingByDirty;

    @Value("${fileGrouping.refinement-rules-active:true}")
    private boolean refinementRulesActive;

    @Value("${fileGrouping.groups-repository.do-optimize:false}")
    private boolean groupsRepositoryDoOptimize;

    @Value("${fileGrouping.titles-search-repository.do-optimize:false}")
    private boolean titlesSearchRepositoryDoOptimize;

    @Autowired
    private UserService userService;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private BlockingExecutor utilBlockingExecutor;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private GroupSearchRepository groupSearchRepository;

    @Autowired
    private TitlesSearchRepository titlesSearchRepository;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private JobCoordinator jobCoordinator;

    @Autowired
    private SubGroupService subGroupService;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @PostConstruct
    private void init() {
        TitleToken.setTitleTokenFactors(topTitleTokenFactorTfidf, topTitleTokenFactorTf, topTitleTokenFactorLength, topTitleTokenFactorNumWords,
                topTitleTokenFactorBoost, topTitleTokenFactorTitleScore, topTitleTokenFactorTitleOrder,
                optimalTitleLength, minRatedTitleLength, maxRatedTitleLength);
    }

//    @RequestMapping(value = "/updatename", method = RequestMethod.POST)
//    public void updateGroupName(@RequestParam(value = "id", required = true) final String id, @RequestParam(value = "name", required = true) final String name) {
//        logger.info("Update group name. id={} name={}", id, name);
//        fileGroupRepository.updateNameForId(name, id);
//    }

    public void reindexTitlesOld(final boolean deleteBeforeProposal, Long jobId) {
        logger.info("Started indexTitles....");
        if (deleteBeforeProposal) {
            jobManagerService.setOpMsg(jobId, "Delete previous titles index");
            logger.warn("Delete previous titles index");
            groupSearchRepository.deleteAllForAccount();
            titlesSearchRepository.deleteAllForAccount();
        }
        logger.info("Start indexTitles...");
        jobManagerService.setOpMsg(jobId, "Started indexTitles....");
        long count = countGroupedFiles();
        opStateService.setCurrentPhaseProgressFinishMark(count);

        Page<SolrFileEntity> files = fileCatService.findAllForCatFile(new PageRequest(0, pageSizeForIndexingTitles));
        for (int i = 1; !files.getContent().isEmpty(); i++) {
            final List<SolrFileEntity> filesPageCopy = files.getContent();

            //  String hint = filesPageCopy.size() > 0 ? filesPageCopy.get(0).getFileName() : "";

//			filesPageCopy.forEach(f->
//				utilBlockingExecutor.execute(()->handleFile(f)));
            utilBlockingExecutor.execute(() -> reindexTitlesOldHandleFilePage(filesPageCopy));
            jobManagerService.incJobCounters(jobId, filesPageCopy.size(), 0);

            files = fileCatService.findAllForCatFile(new PageRequest(i, pageSizeForIndexingTitles));
            if (logger.isDebugEnabled()) {
                if (0 == i % 10) {
                    logger.debug("10 pages were processed ({})", i * pageSizeForIndexingTitles);
                }
            }
        }
        jobManagerService.setOpMsg(jobId, "Wait for indexTitles threads");
        utilBlockingExecutor.blockUntilProcessingFinished();
        jobManagerService.setOpMsg(jobId, "commit groups and titles");
        groupSearchRepository.commit();
        titlesSearchRepository.commit();
        if (groupsRepositoryDoOptimize) {
            logger.warn("Please notice, fileGrouping.group-repository.do-optimize set to true, will start a potentially heavy optimize on Groups index");
            jobManagerService.setOpMsg(jobId, "Optimizing group index...");
            groupSearchRepository.optimize();
        } else {
            logger.debug("fileGrouping.group-repository.do-optimize set to false, skipping Groups index optimization");
        }
        if (titlesSearchRepositoryDoOptimize) {
            logger.warn("Please notice, fileGrouping.titles-search-repository.do-optimize set to true, will start a potentially heavy optimize on Titles index");
            jobManagerService.setOpMsg(jobId, "Optimizing titles index...");
            titlesSearchRepository.optimize();
        } else {
            logger.debug("fileGrouping.titles-search-repository.do-optimize set to false, skipping Titles index optimization");
        }

        jobManagerService.setOpMsg(jobId, "Finished indexing titles");
        logger.info("Finished index Titles.");
    }

    private long countGroupedFiles() {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.EXISTS, ""));
        return fileCatService.getTotalCount(query, CatFileFieldType.DELETED, null);
    }

    private void reindexTitlesOldHandleFilePage(List<SolrFileEntity> filesPage) {
        try {
            List<String> groupIds = filesPage.stream().map(SolrFileEntity::getAnalysisGroupId).filter(Objects::nonNull).collect(Collectors.toList());
            List<FileGroup> groups = groupIds == null || groupIds.isEmpty() ? new ArrayList<>() : fileGroupRepository.getByIds(groupIds);
            Map<String, FileGroup> groupsMap = groups.stream().collect(Collectors.toMap(FileGroup::getId, g -> g));
            List<Long> contentIds = filesPage.stream().map(FileCatService::getContentId).filter(Objects::nonNull).collect(Collectors.toList());
            List<SolrContentMetadataEntity> contents = contentMetadataService.findByIds(contentIds);
            Map<String, SolrContentMetadataEntity> contentsMap = contents.stream().collect(Collectors.toMap(SolrContentMetadataEntity::getId, g -> g));

            final List<SolrInputDocument> gDocs = filesPage.stream()
                    .map(f -> groupSearchRepository.createFileToGroupDocument(groupsMap.get(f.getAnalysisGroupId()),
                            filesDataPersistenceService.findProposedTitlesByContent(contentsMap.get(f.getContentId())),
                            FileCatService.getBaseNameNoExtention(f), f.getFullName(), maxGroupIndexedFiles))
                    .collect(Collectors.toList());
            groupSearchRepository.writeDocuments(gDocs);
            gDocs.clear();

            final List<SolrInputDocument> tDocs = filesPage.stream()
                    .flatMap(f -> titlesSearchRepository.addFileToGroupDocument(groupsMap.get(f.getAnalysisGroupId()),
                            filesDataPersistenceService.findProposedTitlesByContent(contentsMap.get(f.getContentId()))).stream())
                    .collect(Collectors.toList());
            titlesSearchRepository.writeDocuments(tDocs);
            tDocs.clear();
        } catch (RuntimeException e) {
            opStateService.addCurrentPhaseError();
            throw e;
        }
    }

    /*
     * Used for processing files by groups and by pages
     */
    private void getTitlesSolrDocsForFilePage(List<SolrFileEntity> filesPage, SolrDocHelper groupDoc, List<SolrInputDocument> titlesDocs) {
        try {

            List<String> groupIds = filesPage.stream().map(SolrFileEntity::getAnalysisGroupId).filter(Objects::nonNull).collect(Collectors.toList());
            List<FileGroup> groups = groupIds == null || groupIds.isEmpty() ? new ArrayList<>() : fileGroupRepository.getByIds(groupIds);
            Map<String, FileGroup> groupsMap = groups.stream().collect(Collectors.toMap(FileGroup::getId, g -> g));

            List<Long> contentIds = filesPage.stream().map(FileCatService::getContentId).filter(Objects::nonNull).collect(Collectors.toList());
            List<SolrContentMetadataEntity> contents = contentMetadataService.findByIds(contentIds);
            Map<String, SolrContentMetadataEntity> contentsMap = contents.stream().collect(Collectors.toMap(SolrContentMetadataEntity::getId, g -> g));

            for (SolrFileEntity f : filesPage) {
                groupSearchRepository.createFileToGroupDocument(groupsMap.get(f.getAnalysisGroupId()),
                        filesDataPersistenceService.findProposedTitlesByContent(contentsMap.get(f.getContentId())),
                        FileCatService.getBaseNameNoExtention(f), f.getFullName(), maxGroupIndexedFiles, groupDoc);

                titlesDocs.addAll(titlesSearchRepository.addFileToGroupDocument(groupsMap.get(f.getAnalysisGroupId()),
                        filesDataPersistenceService.findProposedTitlesByContent(contentsMap.get(f.getContentId()))));
            }
        } catch (RuntimeException e) {
            opStateService.addCurrentPhaseError();
            throw e;
        }
    }

    @AutoAuthenticate
    public void fullGroupNamingByGroups(final boolean deleteBeforeProposal,
                                        final boolean noRename,
                                        final double skipTermsThreshold,
                                        final boolean indexOnly, Long jobId,
                                        ProgressTracker progressTracker) throws CoordinationLockDeniedException {

        if (indexOnly && !indexAllBeforeNaming) {
            logger.info("No full titles index before group naming");
            progressTracker.endCurStage();
            return;
        }
        boolean index = (indexOnly || !indexAllBeforeNaming);
        if (indexAllBeforeNaming && !indexOnly) {
            KpiRecording kpiRecording = performanceKpiRecorder.startRecording("GroupNamingService", "fullGroupNamingByGroups.commitAndIndex", 313);
            logger.info("Optimzing before full name generation");
            groupSearchRepository.commit();
            titlesSearchRepository.commit();
            if (groupsRepositoryDoOptimize) {
                logger.warn("Please notice, fileGrouping.group-repository.do-optimize set to true, will start a potentially heavy optimize on Groups index");
                jobManagerService.setOpMsg(jobId, "Optimizing group index...");
                groupSearchRepository.optimize();
            } else {
                logger.debug("fileGrouping.group-repository.do-optimize set to false, skipping Groups index optimization");
            }

            if (titlesSearchRepositoryDoOptimize) {
                logger.warn("Please notice, fileGrouping.titles-search-repository.do-optimize set to true, will start a potentially heavy optimize on Titles index");
                jobManagerService.setOpMsg(jobId, "Optimizing titles index...");
                titlesSearchRepository.optimize();
            }else{
                logger.debug("fileGrouping.titles-search-repository.do-optimize set to false, skipping Titles index optimization");
            }
            kpiRecording.end();
//	        opStateService.setOpMsg("Finished indexing titles");
        }
        logger.info("Started groups naming ... (recalculate existing names={})", !noRename);

        if ((deleteBeforeProposal && !indexAllBeforeNaming) || (indexAllBeforeNaming && indexOnly)) {
            jobManagerService.setOpMsg(jobId, "Delete previous group naming index");
            logger.warn("Delete previous group naming index");
            KpiRecording kpiRecording = performanceKpiRecorder.startRecording("GroupNamingService", "fullGroupNamingByGroups.deleteAllForAccount", 330);
            groupSearchRepository.deleteAllForAccount();
            titlesSearchRepository.deleteAllForAccount();
            kpiRecording.end();
        }
        jobManagerService.setOpMsg(jobId, "Groups " + (indexOnly ? "indexing" : "naming"));
        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("GroupNamingService", "fullGroupNamingByGroups.countGroups", 339);
        int groupsTotalCount = (noRename && groupNamingByDirty) ?
                fileGroupService.countNonDeletedDirtyFileGroupIds() :
                fileGroupService.countNonDeletedFileGroupIdsOrderedBySize();
        kpiRecording.end();
        progressTracker.setCurStageEstimatedTotal(groupsTotalCount);

        int pageCount = 0;
        String cursor = "*";
        boolean lastPage;
        try {
            logger.debug("Requesting job coordination lock for processing group naming");
            long[] analyzeFinJobIds = jobId == null ? new long[0] : new long[]{jobId};
            jobCoordinator.requestLock(JobType.ANALYZE_FINALIZE, analyzeFinJobIds);
            logger.debug("Acquired job coordination lock for processing group naming");
            do {
                long start = System.currentTimeMillis();
                kpiRecording = performanceKpiRecorder.startRecording("GroupNamingService", "fullGroupNamingByGroups.getGroups", 355);

                Query query = Query.create()
                        .addFilterWithCriteria(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt())
                        .setRows(nameingDbPpageSize).setCursorMark(cursor).addField(ID)
                        .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

                if (noRename && groupNamingByDirty) {
                    query.addFilterWithCriteria(Criteria.create(DIRTY, SolrOperator.EQ, true));
                } else {
                    query.addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false));
                    query.addFilterWithCriteria(Criteria.create(NUM_OF_FILES, SolrOperator.GT, 0));
                }

                QueryResponse resp = fileGroupService.runQuery(query.build());
                cursor = resp.getNextCursorMark();

                List<String> groups = resp.getResults().stream()
                        .map(doc -> (String)doc.getFieldValue(ID.getSolrName()))
                        .collect(Collectors.toList());

                kpiRecording.end();
                long queryDuration = System.currentTimeMillis() - start;
                pageCount++;
                List<List<String>> subLists = Lists.partition(groups, pageSizeForIndexingTitles);
                for (List<String> groupsSubList : subLists) {
                    final List<String> groupIdsCopy = groupsSubList;
                    utilBlockingExecutor.execute(() -> {
                        AuthenticationHelper.doWithAuthentication(AutoAuthenticate.ADMIN, false,
                                Executors.callable(() -> {
                                    KpiRecording kpiRecordingn = performanceKpiRecorder.startRecording("GroupNamingService", "fullGroupNamingByGroups.processGroupNamingForGroupsPage", 367);
                                    processGroupNamingForGroupsPage(groupIdsCopy, noRename, skipTermsThreshold, index, !indexOnly);
                                    kpiRecordingn.end();
                                }),
                                userService);

                        progressTracker.incrementProgress(groupIdsCopy.size());
                    });
                }
                if (logger.isDebugEnabled()) {
                    if (0 == pageCount % 10) {
                        logger.debug("10 pages were processed ({}). Query for page took: {}ms", pageCount * nameingDbPpageSize, queryDuration);
                    }
                }
                logger.debug("Yielding job coordination lock for processing group naming");
                jobCoordinator.yieldToWaitingJobs(analyzeFinJobIds);
                logger.debug("Got back from yielding job coordination lock for processing group naming");
                // TODO Itai: handle paused
                lastPage = resp.getResults().size() < nameingDbPpageSize;
            } while (!lastPage);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("Interrupted", e);
        } finally {
            logger.debug("Releasing job coordination lock for processing group naming");
            jobCoordinator.release();
            logger.debug("Released job coordination lock for processing group naming");
        }
        jobManagerService.setOpMsg(jobId, "Last items in queue " + (indexOnly ? "indexing" : "naming"));
        utilBlockingExecutor.blockUntilProcessingFinished(progress -> {
            jobManagerService.setOpMsg(jobId, "Last items in queue " + (indexOnly ? "indexing" : "naming") + "(" + progress + "%)");
            logger.debug("Block progress {}/{}", progress, 100);
        });
        if (logger.isDebugEnabled()) {
            logger.info("Finished groups naming {} {}", indexAllBeforeNaming ? "indexAllBeforeNaming" : "", indexOnly ? "indexOnly" : "generate");
        }
        jobManagerService.setOpMsg(jobId, "Finished groups " + (indexOnly ? "indexing" : "naming"));
    }

    private void processGroupNamingForGroupsPage(final List<String> groupIds, final boolean noRename,
                                                 final double skipTermsThreshold, final boolean index, final boolean generate) {
        try {
            List<FileGroup> groups = fileGroupRepository.getByIds(groupIds);
            processGroupNamingForGroupsPage(groups, noRename, skipTermsThreshold, true, index, generate, null, null);
        } catch (RuntimeException e) {
            logger.error("Failed to process names for a page of groups: {}", e.getMessage(), e);
            throw e;
        }
    }

    @Transactional
    private void processGroupNamingForGroupsPage(final List<FileGroup> groups, final boolean noRename, final double skipTermsThreshold,
                                                 final boolean saveName, final boolean index, final boolean generate,
                                                 final List<String> debugInfo, GroupNamingInfoDto groupNamingInfoDto) {

        logger.debug("processGroupNamingForGroupsPage of {} groups... {} {}", groups.size(), index ? "index" : "", generate ? "generate" : "");
        long start = System.currentTimeMillis();
        int filesCount = 0;
        List<String> groupsToProcess = new ArrayList<>();
        List<SolrDocHelper> groupHelperDocs = new ArrayList<>();
        List<SolrInputDocument> titleSolrDocs = new ArrayList<>();

        int groupsNotSkipped = 0;

        if (index) {
            for (FileGroup group : groups) {
                if (group.getNumOfFiles() == 0) {
                    continue; //Skip empty groups (should never happen)
                }
                long differenceFromLastRename = group.getNumOfFiles() - group.getCountOnLastNamingNotNull();
                if (noRename && (!indexAllBeforeNaming) && group.getGeneratedName() != null) {
//                    logger.trace("No rename for group {} - {}", group.getId(), group.getProposedNames());
                    continue;
                } else {
                    if (group.getCountOnLastNamingNotNull() > 0) {
                        logger.debug("Recalculate name for group {} - {} (diff={})", group.getId(), group.getProposedNames(), differenceFromLastRename);
                    } else {
                        logger.debug("Calculate name for group {} - {} (size={})", group.getId(), group.getProposedNames(), group.getNumOfFiles());
                    }
                    groupsNotSkipped++;
                    filesCount += group.getNumOfFiles();
                    groupsToProcess.add(group.getId());
                    try {
                        // Index group files
                        indexSingleGroupTitles(group.getId(), skipTermsThreshold, groupHelperDocs, titleSolrDocs);
                    } catch (RuntimeException e) {
                        logger.error("Failed to index single group titles: {}", e.getMessage(), e);
                    }

                    if (filesCount > filesCountNamingThreshold) {
                        logger.debug("Processing mid-page {} files of {} groups", filesCount, groupsToProcess.size());
                        nameGroupsAndClear(groupsToProcess, skipTermsThreshold, groupHelperDocs, titleSolrDocs, saveName,
                                index, generate, debugInfo, groupNamingInfoDto);
                        groupsToProcess.clear();
                        filesCount = 0;
                    }
                }
            }
            // Process left files
            if (!groupsToProcess.isEmpty()) {
                logger.debug("Processing {} left files of {} groups", filesCount, groupsToProcess.size());
                nameGroupsAndClear(groupsToProcess, skipTermsThreshold, groupHelperDocs, titleSolrDocs, saveName, index,
                        generate, debugInfo, groupNamingInfoDto);
                groupsToProcess.clear();
                filesCount = 0;
            }
        } else {
            logger.debug("Non-index processing of {} groups", groups.size());
            groupsToProcess = groups.stream().map(FileGroup::getId).collect(Collectors.toList());
            nameGroupsAndClear(groupsToProcess, skipTermsThreshold, groupHelperDocs, titleSolrDocs, saveName, index,
                    generate, debugInfo, groupNamingInfoDto);
        }
        long duration = System.currentTimeMillis() - start;
        if (duration > 500) {
            logger.debug("processGroupNamingForGroupsPage on {} non skipped groups took {}ms", groupsNotSkipped, duration);
        }
    }

    public void nameAGroup(String groupId) {
        FileGroup group = fileGroupService.getFileGroupById(groupId);
        if (group == null) {
            throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        if (group.getNumOfFiles() == 0) {
            throw new BadRequestException(messageHandler.getMessage("group.empty"), BadRequestType.UNSUPPORTED_VALUE);
        }

        List<SolrDocHelper> groupHelperDocs = new ArrayList<>();
        List<SolrInputDocument> titleSolrDocs = new ArrayList<>();

        // Index group files
        indexSingleGroupTitles(group.getId(), 0, groupHelperDocs, titleSolrDocs);

        // name group
        nameGroupsAndClear(Collections.singletonList(group.getId()), -1, groupHelperDocs, titleSolrDocs, true,
                false, true, null, null);

        // if generated a name but user named it as well, remove user naming
        if (group.getName() != null && group.getGeneratedName() != null) {
            fileGroupRepository.getById(groupId)
                    .ifPresent(g -> {
                        SolrInputDocument doc = new SolrInputDocument();
                        doc.setField(ID.getSolrName(), groupId);
                        SolrFileGroupRepository.addField(doc, NAME, null, SolrFieldOp.SET);
                        SolrFileGroupRepository.addField(doc, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
                        fileGroupRepository.atomicUpdate(doc);
                        fileGroupRepository.softCommitNoWaitFlush();
                    });
        }
    }

    @Transactional
    private void nameGroupsAndClear(List<String> groupIdsToProcess, final double skipTermsThreshold,
                                    List<SolrDocHelper> groupHelperDocs, List<SolrInputDocument> titleSolrDocs,
                                    boolean saveName, boolean index, boolean generate, List<String> debugInfo,
                                    GroupNamingInfoDto groupNamingInfoDto) {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Process {} groups... {} {}", groupIdsToProcess.size(), index ? "index" : "", generate ? "generate" : "");
            }
            if (index) {
                logger.debug("Writing docs...");
                groupHelperDocs = sanitizeDocHelpers(groupHelperDocs);
                groupSearchRepository.writeDocuments(groupHelperDocs.stream()
                        .map(SolrDocHelper::toSolrDoc)
                        .collect(Collectors.toList()));
                titlesSearchRepository.writeDocuments(titleSolrDocs);
            }
            if (generate) {
                if (index) {
                    logger.debug("Commiting ...");
                    groupSearchRepository.softCommitNoWaitFlush();
                    titlesSearchRepository.softCommitNoWaitFlush();
                    if (sleepAfterSolrCommitTimeMs > 0) {
                        try {
                            Thread.sleep(sleepAfterSolrCommitTimeMs);
                        } catch (InterruptedException e) {
                            logger.error("Sleep after Solr commit was interrupted. {}", e);
                            //				throw new RuntimeException("Sleep after Solr commit was interrupted. {}", e);
                        }
                    }
                }
                logger.debug("Naming {} groups. Looping ...", groupIdsToProcess.size());
                final double skipTermsThresholdValue = (skipTermsThreshold < 0f) ? skipTermsThresholdProp : skipTermsThreshold;

                // Generate names
                List<SolrInputDocument> groupsToSave = new ArrayList<>();
                for (String processedGroupId : groupIdsToProcess) {
                    fileGroupRepository.getById(processedGroupId)
                            .ifPresent(groupToProcess -> {
                                boolean hasName = !Strings.isNullOrEmpty(groupToProcess.getName()) || !Strings.isNullOrEmpty(groupToProcess.getGeneratedName());
                                SolrInputDocument doc = updateGroupProposedNames(groupToProcess, skipTermsThresholdValue, saveName, debugInfo, groupNamingInfoDto);
                                if (saveName) {
                                    //                    groupToProcess.setDirty(false);   // clear dirty flag should be done at the end
                                    SolrFileGroupRepository.addField(doc, FileGroupsFieldType.COUNT_LAST_NAMING, groupToProcess.getNumOfFiles(), SolrFieldOp.SET);

                                    if (refinementRulesActive && !hasName && groupToProcess.isHasSubgroups()) {
                                        subGroupService.renameOthersGroupIfExists(groupToProcess);
                                    }
                                }
                                groupsToSave.add(doc);
                            });
                }
                if (!groupsToSave.isEmpty()) {
                    fileGroupRepository.saveAll(groupsToSave);
                    fileGroupRepository.softCommitNoWaitFlush();
                }
            }
            if (index && generate) {
                logger.debug("Deleting title indexes for goups page. Size {}", groupIdsToProcess.size());
                // Delete related documents (by groupId)
                groupSearchRepository.deleteByGroupIds(groupIdsToProcess);
                titlesSearchRepository.deleteByGroupIds(groupIdsToProcess);
            }
        } catch (Exception e) {
            logger.error("Failed to name groups and clear: {}", e.getMessage(), e);
        }
    }

    private List<SolrDocHelper> sanitizeDocHelpers(List<SolrDocHelper> groupHelperDocs) {
        List<SolrDocHelper> result = Lists.newLinkedList();
        for (SolrDocHelper helper : groupHelperDocs) {
            if (helper.getValue("id") == null) {
                List<Object> entries = helper.getSingleValueFields().entrySet().stream()
                        .map((Function<Map.Entry<String, Object>, Object>) input -> input.getKey() + ":" + input.getValue())
                        .collect(Collectors.toList());
                String entriesStr = LoggingUtils.collectionToString(entries);
                logger.warn("Encountered document without ID, content: {}, skipping.", entriesStr);
            } else {
                result.add(helper);
            }
        }
        return result;
    }

    private void indexSingleGroupTitles(final String groupId, final double skipTermsThreshold, List<SolrDocHelper> groupHelperDocs, List<SolrInputDocument> titleSolrDocs) {
        logger.debug("Started index titles for group {} ...", groupId);

        /*
         * Process titles for all files in a group and add its info to a given SolrDocs lists
         */
        List<SolrFileEntity> files;
        SolrDocHelper groupDoc = new SolrDocHelper();
        int pageCount = 0;
        do {
            files = fileCatService.findFilesByAnalysisGroupId(new PageRequest(pageCount, pageSizeForIndexingTitles), groupId);

            pageCount++;
            if (!files.isEmpty()) {
                getTitlesSolrDocsForFilePage(files, groupDoc, titleSolrDocs);

                if (logger.isDebugEnabled()) {
                    if (0 == pageCount % 10) {
                        logger.debug("10 pages were processed ({})", pageCount * pageSizeForIndexingTitles);
                    }
                }
                if (maxGroupIndexedFiles > 0 && (pageCount * pageSizeForIndexingTitles) >= maxGroupIndexedFiles) {
                    logger.info("Skip group title indexing after {} pages ({} files) for group {}", pageCount, pageCount * pageSizeForIndexingTitles, groupId);
                    break;
                }
            }
        } while (!files.isEmpty());

        if (pageCount == 0) {
            logger.warn("The group {} was empty, no titles will be suggested.", groupId);
            groupDoc.setValue("id", groupId);
        }
        // Add new group doc to list (titles were already added to the list
        groupHelperDocs.add(groupDoc);
    }

    public void proposeGroupsNames(final boolean deleteBeforeProposal, final double skipTermsThreshold, Long jobId) {
        logger.info("Started proposeGroupsNames...");
//		if(deleteBeforeProposal) {
//            opStateService.setOpMsg("clear Proposed Names");
//			fileGroupRepository.clearProposedNamesForAccount(accountService.getCurrentAccountId());
//		}
        final double skipTermsThresholdValue = (skipTermsThreshold < 0f) ? skipTermsThresholdProp : skipTermsThreshold;
        logger.info("Start updateProposedNames (min tf={}%)...", (int) (100 * skipTermsThresholdValue));
        jobManagerService.setOpMsg(jobId, "Updating Proposed Names");
        if (!useFilename) {
            logger.info("Configured not to use filename for groupname generation");
        }
        int pageCount = 0;
        String cursor = "*";
        boolean lastPage;
        do {
            Query query = Query.create().addFilterWithCriteria(
                    Criteria.create(DELETED, SolrOperator.EQ, false))
                    .setRows(pageSizeForIndexingTitles).setCursorMark(cursor)
                    .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

            QueryResponse resp = fileGroupService.runQuery(query.build());
            cursor = resp.getNextCursorMark();

            List<SolrFileGroupEntity> res = resp.getBeans(SolrFileGroupEntity.class);

            jobManagerService.updateJobCounters(jobId, 0, resp.getResults().getNumFound(), 0);
            pageCount++;
            res.forEach(g -> utilBlockingExecutor.execute(() -> {
                    SolrInputDocument doc = updateGroupProposedNames(SolrFileGroupRepository.getFromEntity(g), skipTermsThresholdValue);
                    fileGroupRepository.atomicUpdate(doc);
            }));
            if (logger.isDebugEnabled()) {
                if (0 == pageCount % 10) {
                    logger.debug("10 pages were processed ({})", pageCount * pageSizeForIndexingTitles);
                }
            }
            jobManagerService.incJobCounters(jobId, resp.getResults().size(), 0);
            lastPage = resp.getResults().size() < pageSizeForIndexingTitles;
        } while (!lastPage);
        jobManagerService.setOpMsg(jobId, "block Until Processing Finished");
        utilBlockingExecutor.blockUntilProcessingFinished();
        fileGroupRepository.softCommitNoWaitFlush();
        logger.info("Finished updateProposedNames.");
        jobManagerService.setOpMsg(jobId, "Finished updateProposedNames");
    }

    private SolrInputDocument updateGroupProposedNames(final FileGroup group, final double skipTermsThreshold) {
        return updateGroupProposedNames(group, skipTermsThreshold, true, null, null);
    }

    private SolrInputDocument updateGroupProposedNames(final FileGroup group, final double skipTermsThreshold,
                                          final boolean saveName, final List<String> debugInfo, GroupNamingInfoDto groupNamingInfoDto) {
        try {
            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setId(group.getId());
            List<TitleToken> fnTv = getTokensList("fileNamesShingle", group, skipTermsThreshold);
            builder.setFileNamestv(getTokens(fnTv));
            List<TitleToken> parentDirTv = getTokensList("parentDirShingle", group, skipTermsThreshold);
            builder.setParentDirtv(getTokens(parentDirTv));
            List<TitleToken> grandParentDirTv = getTokensList("grandParentDirShingle", group, skipTermsThreshold);
            builder.setGrandParentDirtv(getTokens(grandParentDirTv));
            final List<TitleToken> titlesShingle = getTokensList("titlesShingle", group, skipTermsThreshold);
            builder.setProposedNames(getTokens(titlesShingle));

            // TODO: Remove redundant fields + old code, that are no longer used:
            // 			Groups: firstTitlesShingle, secondTitlesShingle, thirdTitlesShingle, titles
            final List<TitleToken> firstTitlesShingle = getTokensList("firstTitlesShingle", group, skipTermsThreshold);
            builder.setFirstProposedNames(getTokens(firstTitlesShingle));
            final List<TitleToken> secondTitlesShingle = getTokensList("secondTitlesShingle", group, skipTermsThreshold);
            builder.setSecondProposedNames(getTokens(secondTitlesShingle));
            final List<TitleToken> thirdTitlesShingle = getTokensList("thirdTitlesShingle", group, skipTermsThreshold);
            builder.setThirdProposedNames(getTokens(thirdTitlesShingle));

            final List<TitleToken> newProposedTitles = getTokensListTitles(group, skipTermsThreshold, titlesShingle);
            builder.setProposedNewNames(getTokens(newProposedTitles));
            if (!useFilename) {
                fnTv = null;
                parentDirTv = null;
                grandParentDirTv = null;
            }
            final TitleToken topTitle = getTopTitleToken(firstTitlesShingle, secondTitlesShingle, thirdTitlesShingle, titlesShingle, fnTv);
            final List<TitleToken> newTopTitles = getNewTopTitle(newProposedTitles, fnTv, parentDirTv, grandParentDirTv);
            final TitleToken newTopTitle = TitleToken.copyFirstQualified(newTopTitles);

            logger.debug("proposed group name for {}: {} ({})", group.getId(), (newTopTitle == null) ? "" : newTopTitle.getToken(), (topTitle == null) ? "" : topTitle.getToken());

//			if (topTitle != null) {
//				group.setGeneratedName(topTitle.getToken());
//			}
            if (newTopTitle != null) {
                builder.setGeneratedName(getSanitizedString(newTopTitle.getToken()));
            }

            if (debugInfo != null) {
                if (newTopTitle != null) {
                    debugInfo.add(String.format("Generated name for group %s: %s (%s) ",
                            group.getId(), newTopTitle.getToken(), newTopTitle.toString()));
                }

                updateDebugInfo(newProposedTitles, debugInfo, "Titles based:");
                updateDebugInfo(fnTv, debugInfo, "Filename based:");
                updateDebugInfo(parentDirTv, debugInfo, "Parent dir based:");
                updateDebugInfo(grandParentDirTv, debugInfo, "Grand parent dir based:");
                updateDebugInfo(newTopTitles, debugInfo, "Combined weighted list:");
            }

            if (groupNamingInfoDto != null) {
                if (newTopTitle != null) {
                    groupNamingInfoDto.setNewName(newTopTitle.toString());
                    groupNamingInfoDto.setTopToken(newTopTitle);
                }
                if (debugInfo != null) {
                    debugInfo.add("Titles based:");
                }
                if (newProposedTitles != null && !newProposedTitles.isEmpty()) {
                    groupNamingInfoDto.setTitleBasedTokens(newProposedTitles);
                }
                if (fnTv != null && !fnTv.isEmpty()) {
                    groupNamingInfoDto.setFilenameBasedTokens(fnTv);
                }
                if (parentDirTv != null && !parentDirTv.isEmpty()) {
                    groupNamingInfoDto.setParentDirBasedTokens(parentDirTv);
                }
                if (grandParentDirTv != null && !grandParentDirTv.isEmpty()) {
                    groupNamingInfoDto.setGrandparentDirBasedTokens(grandParentDirTv);
                }
                if (newTopTitles != null && !newTopTitles.isEmpty()) {
                    groupNamingInfoDto.setCombinedListTokens(newTopTitles);
                }
            }
            return builder.buildAtomicUpdate();
        } catch (final JsonProcessingException e) {
            opStateService.addCurrentPhaseError();
            throw new RuntimeException("Failed to update group " + group.getId(), e);
        }
    }

    private void updateDebugInfo(List<TitleToken> titles, List<String> debugInfo, String info) {
        if (titles != null && !titles.isEmpty()) {
            if (!Strings.isNullOrEmpty(info)) {
                debugInfo.add(info);
            }
            titles.stream().map(t -> (" " + t.toString())).forEach(debugInfo::add);
            debugInfo.add("");
        }
    }


    private List<TitleToken> getNewTopTitle(final List<TitleToken> newProposedTitles, final List<TitleToken> filenameShingle, final List<TitleToken> parentDirTv, final List<TitleToken> grandParentDirTv) {

        // TODO: change lists merge into more colaborative form:
        //			pick n top from each list, set with list-based-boost value, merge and sort.
        //			Then get the top as the new name and some following items as alternatives.
        //			Make sure alternatives doesn't share prefix.
        //
        List<TitleToken> topTitles = Arrays.asList(
                TitleToken.copyFirstQualified(newProposedTitles),
                TitleToken.copyFirstQualified(filenameShingle),
                TitleToken.copyFirstQualified(parentDirTv),
                TitleToken.copyFirstQualified(grandParentDirTv));
        TitleToken.setBoostIfNotNull(topTitles.get(0), topTitlesBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(1), fileNameBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(2), parentDirBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(3), grandParentDirBoostValue);
        topTitles = topTitles.stream().filter(Objects::nonNull).collect(Collectors.toList());
        TitleToken.normalizeTitleTokensList(topTitles);
        TitleToken.sort(topTitles);
        return topTitles;
//		final TitleToken topTitle = TitleToken.copyFirstQualified(topTitles);
//		return topTitle;
    }

    private TitleToken getTopTitle(final List<TitleToken> newProposedTitles) {
        if (newProposedTitles == null || newProposedTitles.isEmpty()) {
            return null;
        }
        return newProposedTitles.get(0);        // List is sorted ...
    }

    private TitleToken getTopTitleToken(
            final List<TitleToken> firstTitlesShingle,
            final List<TitleToken> secondTitlesShingle,
            final List<TitleToken> thirdTitlesShingle,
            final List<TitleToken> titlesShingle,
            final List<TitleToken> filenameShingle) {

        List<TitleToken> topTitles = Arrays.asList(
                TitleToken.copyFirstQualified(firstTitlesShingle),
                TitleToken.copyFirstQualified(secondTitlesShingle),
                TitleToken.copyFirstQualified(thirdTitlesShingle),
                TitleToken.copyFirstQualified(titlesShingle),
                TitleToken.copyFirstQualified(filenameShingle));
        TitleToken.setBoostIfNotNull(topTitles.get(0), firstTitleBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(1), secondTitleBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(2), thirdTitleBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(3), titlesBoostValue);
        TitleToken.setBoostIfNotNull(topTitles.get(4), fileNameBoostValue);
        topTitles = topTitles.stream().filter(Objects::nonNull).collect(Collectors.toList());
        TitleToken.normalizeTitleTokensList(topTitles);
        TitleToken.sort(topTitles);
        return TitleToken.copyFirstQualified(topTitles);
    }


    private String getTokens(final List<TitleToken> tokens) throws JsonProcessingException {
//		final String writeValueAsString = objectMapper.writeValueAsString(tokens.subList(0, Math.min(tokens.size(), maxSizeForTitles)));
        if (tokens == null)
            return "-";

        String writeValueAsString = tokens.subList(0, Math.min(tokens.size(), maxSizeForTitles)).toString();
        writeValueAsString = getSanitizedString(writeValueAsString);
        return writeValueAsString;
    }

    private static String getSanitizedString(String writeValueAsString) {
        if (writeValueAsString == null) {
            return null;
        }
        if (writeValueAsString.equals("null")) {
            return null;
        }
        writeValueAsString = writeValueAsString.replaceAll(IngestConstants.UTF8_FOUR_BYTES, "?");
        return writeValueAsString;
    }

    @SuppressWarnings("unused")
    private String getTokens(final String fieldName, final FileGroup group, final double skipTermsThreshold) throws JsonProcessingException {
        final List<TitleToken> tokens = getTokensList(fieldName, group, skipTermsThreshold);
        final List<TitleToken> tokensTitles = getTokensListTitles(group, skipTermsThreshold, tokens);
        return getTokens(tokens);
    }

    private List<TitleToken> getTokensList(final String fieldName, final FileGroup group, final double skipTermsThreshold) throws JsonProcessingException {
        final List<TitleToken> tokens;
        try {
            tokens = groupSearchRepository.getTermVector(fieldName, group.getId(), skipTermsThreshold);
        } catch (final Exception e) {
            logger.error("Error during getTermVector({},{},{}). {}", fieldName, group.getId(), skipTermsThreshold, e.getMessage(), e);
            return new ArrayList<TitleToken>();
        }
        try {
            TitleToken.normalizeTitleTokensList(tokens, group.getNumOfFiles());
            TitleToken.sort(tokens);
        } catch (final Exception e) {
            logger.warn("Error during tokenListGeneration: tokens={} tftif={} factorTf={} factorLen={}. Error ignored.",
                    tokens.toString(), topTitleTokenFactorTfidf, topTitleTokenFactorTf, topTitleTokenFactorLength, e);
        }
        return tokens;
    }

    private List<TitleToken> getTokensListTitles(final FileGroup group, final double skipTermsThreshold, final List<TitleToken> groupTokens) throws JsonProcessingException {
        // Create group level tokens info map
        final Map<String, TitleToken> groupTokensMap = new HashMap<>(groupTokens.size());
        groupTokens.forEach(t -> groupTokensMap.put(t.getToken(), t));

        List<TitleToken> tokens;
        try {
            int numOfFilesForTermsSearch = (int) group.getNumOfFiles();
            // In case of limit to the number of indexed files per group, use that limit as the number used for minCount limit calc.
            if (maxGroupIndexedFiles > 0 && numOfFilesForTermsSearch >= maxGroupIndexedFiles) {
                logger.debug("Setting numOfFilesForTermsSearch to {} (was {})", maxGroupIndexedFiles, numOfFilesForTermsSearch);
                numOfFilesForTermsSearch = maxGroupIndexedFiles;
            }
            tokens = titlesSearchRepository.getTermVector(group.getId(), numOfFilesForTermsSearch, skipTermsThreshold, groupTokensMap);
            if (tokens == null) {
                return null;
            }
            tokens = tokens.stream().filter(t -> (t != null)).collect(Collectors.toList());
            if (tokens.isEmpty()) {
                return tokens;
            }
        } catch (final Exception e) {
            logger.error("Error during getTermVector({},{}). {}", group.getId(), skipTermsThreshold, e.getMessage(), e);
            return new ArrayList<TitleToken>();
        }
        try {
            TitleToken.normalizeTitleTokensList(tokens, group.getNumOfFiles());

            // Sort by tokenScore desc.
            tokens.sort((t1, t2) -> Double.compare(t2.getTokenScore(), t1.getTokenScore()));
        } catch (final Exception e) {
            logger.warn("Error during tokenListGeneration: tokens={} tftif={} factorTf={} factorLen={}. Error ignored.",
                    (tokens == null ? "<null>" : tokens.toString()), topTitleTokenFactorTfidf, topTitleTokenFactorTf, topTitleTokenFactorLength, e);
        }
        return tokens;
    }

    public void deleteTitlesIndex(Long jobId) {
        jobManagerService.setOpMsg(jobId, "Delete titles index");
        logger.info("Delete titles index");
        groupSearchRepository.deleteAllForAccount();
        titlesSearchRepository.deleteAllForAccount();
        jobManagerService.setOpMsg(jobId, "Delete index ended");
    }

    /**
     * DEPRECATED - mechanism moved to analyzer data service instead
     */
    @Transactional
    @Deprecated
    public void markUpdatedGroupsForNameCalculation() {
        logger.info("markUpdatedGroupsForNameCalculation");
        int marked = fileGroupService.markAnalysisGroupsChangedGroupsForNameCalculation(recalculateThreshold);   // TODO: run in batches ...
        if (marked > 0) {
            logger.warn("{} groups marked for name recalculation", marked);
        }
    }
}
