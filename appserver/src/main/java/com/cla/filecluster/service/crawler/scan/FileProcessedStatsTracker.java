package com.cla.filecluster.service.crawler.scan;

import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.Pair;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.service.license.TrackerAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Responsible to track the  processed volume and count of new/updated files.
 */
@Component
public class FileProcessedStatsTracker {

    private static final Logger logger = LoggerFactory.getLogger(FileProcessedStatsTracker.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private TrackerAppService trackerAppService;

    @PostConstruct
    private void init() {
        eventBus.subscribe(Topics.MAP, event -> {
            if (event.getEventType() != EventType.RUN_PROGRESS) { // Case not RUN_PROGRESS TYPE  drop it
                if (logger.isTraceEnabled()) {
                    logger.trace("Listening only to {}, Dropping event {}", EventType.RUN_PROGRESS, event);
                }
                return;
            }

            boolean isAFile = event.getContext().getOrDefault(EventKeys.IS_A_FILE, false);
            if (!isAFile) {
                logger.trace("Event is no associated with a file, dropping it, {}", event);
                return;
            }

            // Case required keys not exists drop it
            if (!hasEventKeys(event)) {
                logger.trace("Event is missing necessary keys for tracking dropping it, {}", event);
                return;
            }

            // Otherwise we're good to go
            accumulateFileStats(event);
        });
    }

    private boolean hasEventKeys(Event event) {
        return event.getContext().containsKey(EventKeys.NEW_FILE) ||
                event.getContext().containsKey(EventKeys.FILE_CONTENT_UPDATE);
    }

    private void accumulateFileStats(Event event) {
        final Context context = event.getContext();

        long sizeToAdd = 0;
        if (context.containsKey(EventKeys.NEW_FILE)) {
            ClaFilePropertiesDto claFilePropertiesDto = context.get(EventKeys.NEW_FILE_PAYLOAD, ClaFilePropertiesDto.class);
            sizeToAdd = claFilePropertiesDto.getFileSize();
        } else if (context.containsKey(EventKeys.FILE_CONTENT_UPDATE)) {
            Pair<Long, Long> oldSizeAndNewSize = context.get(EventKeys.FILE_DIFF_PAYLOAD, Pair.class);
            long diff = oldSizeAndNewSize.getValue() - oldSizeAndNewSize.getKey(); // check newSize - oldSize
            sizeToAdd = diff > 0 ? diff : 0; // Case  diff bigger than add it to the count
        }

        trackerAppService.incrementLongCount(TrackingKeys.TOTAL_FILE_COUNT_KEY, 1);
        trackerAppService.incrementLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY, sizeToAdd);

        MediaType mediaType = context.get(EventKeys.FILE_MEDIA_TYPE, MediaType.class);
        trackerAppService.incrementLongCount(TrackingKeys.mediaCountKey(mediaType), 1);
        trackerAppService.incrementLongCount(TrackingKeys.mediaVolumeKey(mediaType), sizeToAdd);

        long rootFolderId = context.get(EventKeys.ROOT_FOLDER_ID, Long.class);
        trackerAppService.incrementLongCount(TrackingKeys.rootFolderCountKey(rootFolderId), 1);
        trackerAppService.incrementLongCount(TrackingKeys.rootFolderVolumeKey(rootFolderId), sizeToAdd);
    }
}
