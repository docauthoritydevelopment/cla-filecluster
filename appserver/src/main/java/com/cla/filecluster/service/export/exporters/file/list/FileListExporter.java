package com.cla.filecluster.service.export.exporters.file.list;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.GenericFileExporter;
import com.cla.filecluster.service.export.exporters.file.list.mix_in.ClaFileDtoMixIn;
import com.cla.filecluster.service.export.exporters.file.list.mix_in.ContentMetadataDtoMixIn;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.list.FileListHeaderFields.*;

@Slf4j
@Component
public class FileListExporter extends PageCsvExcelExporter<ClaFileDto> {

    @Autowired
    private GenericFileExporter genericFileExporter;

    @Autowired
    private DepartmentService departmentService;

    private String[] header = {NAME, TYPE, NUM_OF_COPIES, EXTRACTIONS, ASSOCIATIONS, DEPARTMENT, MATTER,
            DOC_TYPES, ROLES, TAGS, GROUP_NAME, SIZE, OWNER, PATTERNS, MODIFIED_DATE, CREATION_DATE, ACCESSED_DATE};


    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ClaFileDto.class, ClaFileDtoMixIn.class)
                .addMixIn(ContentMetadataDto.class, ContentMetadataDtoMixIn.class);
    }

    @Override
    protected Page<ClaFileDto> getData(Map<String, String> requestParams) {
        return genericFileExporter.getData(requestParams);
    }

	@Override
	protected Map<String, Object> addExtraFields(ClaFileDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();

        if (base.getDepartmentDto() != null) {
            result.put(departmentService.getLegalInstallationMode(), base.getDepartmentDto().getFullName());
        }

        if (base.getSearchPatternsCounting() != null ) {
            StringBuffer patternStr = new StringBuffer();
            for (SearchPatternCountDto spc : base.getSearchPatternsCounting()) {
                patternStr.append(spc.getPatternName()).append("-").append(spc.getPatternCount()).append(";");
            }
            result.put(PATTERNS, patternStr);
        }
		return result;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI_DEEP;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return ExportType.DISCOVER_RIGHT.equals(exportType);
    }
}
