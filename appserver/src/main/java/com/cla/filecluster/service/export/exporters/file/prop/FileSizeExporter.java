package com.cla.filecluster.service.export.exporters.file.prop;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileSizePartitionDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.acl.AclAggCountDtoMixin;
import com.cla.filecluster.service.files.FileSizeRangeService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.NAME;
import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.NUM_OF_FILES;

/**
 * Created by: yael
 * Created on: 6/3/2019
 */
@Slf4j
@Component
public class FileSizeExporter  extends PageCsvExcelExporter<AggregationCountItemDTO<FileSizePartitionDto>> {

    final static String[] HEADER = {NAME, NUM_OF_FILES};

    @Autowired
    private FileSizeRangeService fileSizeRangeService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AclAggCountDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<FileSizePartitionDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<FileSizePartitionDto>> res = fileSizeRangeService.getFileSizeFileCount(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<FileSizePartitionDto> base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        result.put(NAME, base.getItem().getLabel());
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.FILE_SIZE);
    }
}
