package com.cla.filecluster.service.export.exporters.root_folders.mix_in;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.filetree.StoreLocation;
import com.cla.common.domain.dto.filetree.StorePurpose;
import com.cla.common.domain.dto.filetree.StoreSecurity;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.service.export.serialize.ToSecondsSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.root_folders.RootFoldersHeaderFields.*;

public abstract class RootFolderDtoMixIn {

    @JsonProperty(PATH)
    private String realPath;

    @JsonProperty(NICK_NAME)
    private String nickName;

    @JsonProperty(DESCRIPTION)
    private String description;

    @JsonProperty("# found folders")
    private Integer numberOfFoldersFound;

    @JsonProperty(STORE_LOCATION)
    private StoreLocation storeLocation;

    @JsonProperty(STORE_PURPOSE)
    private StorePurpose storePurpose;

    @JsonProperty(STORE_SECURITY)
    private StoreSecurity storeSecurity;

    @JsonProperty(RESCAN)
    private boolean rescanActive;

    @JsonProperty(MAPPED_FILE_TYPES)
    private FileType[] fileTypes;

    @JsonProperty(INGESTED_FILE_TYPES)
    private FileType[] ingestFileTypes;

    @JsonProperty("# Excluded folders rules")
    private int folderExcludeRulesCount;

    @JsonProperty(MEDIA_TYPE)
    private MediaType mediaType;

    @JsonProperty(MEDIA_CONNECTION_NAME)
    private String mediaConnectionName;

    @JsonProperty(DATA_CENTER)
    private CustomerDataCenterDto customerDataCenterDto;

    @JsonSerialize(using = ToSecondsSerializer.class)
    @JsonProperty(REINGEST_TIME)
    private long reingestTimeStampMs;

    @JsonProperty(REINGEST)
    public abstract boolean isReingest();

    @JsonProperty("Accessibility")
    private DirectoryExistStatus isAccessible;

    @JsonProperty(MAILBOX_GROUP)
    private String mailboxGroup;

    @JsonProperty(FROM_DATE_SCAN_FILTER)
    private Long fromDateScanFilter;

    @JsonProperty(SCANNED_LIMIT_COUNT)
    private Integer scannedFilesCountCap;

    @JsonProperty(MEANINGFUL_SCANNED_LIMIT_COUNT)
    private Integer scannedMeaningfulFilesCountCap;

    @JsonProperty(SCAN_TASK_DEPTH)
    private Integer scanTaskDepth;
}
