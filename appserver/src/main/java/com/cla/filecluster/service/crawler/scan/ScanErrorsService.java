package com.cla.filecluster.service.crawler.scan;

import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ScanError;
import com.cla.filecluster.repository.jpa.ScanErrorsRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ScanErrorsService {
	private static final Logger logger = LoggerFactory.getLogger(ScanErrorsService.class);

    private static final int EXCEPTION_TEXT_MAX_LENGTH = 4000;
	
	@Autowired
	private ScanErrorsRepository scanErrorsRepository;

    @Autowired
    private RootFolderRepository rootFolderRepository;

    @Autowired
    private DBTemplateUtils dBTemplateUtils;

    @Transactional
    public void addScanError(String text, Exception e, DocFolder docFolder,
                             Path path, Long runId, Long rootFolderId, MediaType mediaType) {
        addScanError(text, e, docFolder, path.toString(), runId, rootFolderId, mediaType);
    }

    @Transactional
    public void addScanError(String text, Exception e, DocFolder docFolder,
                             String path, Long runId, Long rootFolderId, MediaType mediaType) {
        String exceptionText = getExceptionText(e);
        addScanError(text, exceptionText, docFolder, path, runId, rootFolderId, mediaType);
    }

    @Transactional
    public void addScanError(String text, String exText, DocFolder docFolder,
                             String path, Long runId, Long rootFolderId, MediaType mediaType) {
        try {
            final ScanError scanError = new ScanError(text, exText,
                    docFolder == null ? null : docFolder.getId(), path, runId, rootFolderId, mediaType);
            scanErrorsRepository.save(scanError);
        } catch (Exception e1) {
            logger.error("Failed to save scan error",e1);
        }
    }

    private String getExceptionText(final Exception exception) {
		if (exception == null) {
			return null;
		}
		
		String eText = exception.getMessage();
		if (exception.getCause() != null) {
			Throwable c = exception.getCause();
			if (eText.contains(c.getMessage()) && c.getCause() != null) {
				c = c.getCause();
			}
			eText += ". Cause: " + c.getMessage();
		}

		if (eText.length() > EXCEPTION_TEXT_MAX_LENGTH){
		    eText = eText.substring(0, EXCEPTION_TEXT_MAX_LENGTH);
        }
		return eText;
	}

    @Transactional
    public void deleteErrorsByRootFolder(Long rootFolderId) {
        logger.warn("delete all scan errors by root folder {}", rootFolderId);
        int deletedRows = scanErrorsRepository.deleteByRootFolder(rootFolderId);
        logger.info("Deleted {} errors", deletedRows);
    }

    private String buildQuery(Map<String, String> params, long offset, int pageSize, boolean isCount) {
        String query = "select ";

        query += isCount ? "count(*)" :
                "err.id, err.date_created, err.doc_folder_id, err.exception_text," +
                        "err.path, err.root_folder_id, err.run_id, err.text, err.type, err.media_type";

        query += " from err_scanned_files err, root_folder rf" +
                " where rf.id = err.root_folder_id " +
                " and rf.deleted = false ";

        Long dateFrom = params != null && params.containsKey("dateFrom") ? Long.parseLong(params.get("dateFrom")) : null;
        Long dateTo = params != null && params.containsKey("dateTo") ? Long.parseLong(params.get("dateTo")) : null;

        if (dateFrom == null && dateTo != null) {
            dateFrom = 0L;
        } else if (dateFrom != null && dateTo == null) {
            dateTo = System.currentTimeMillis();
        }

        if (dateFrom != null) {
            query += " and err.date_created >= " + dateFrom;
            query += " and err.date_created <= " + dateTo;
        }

        Long rootFolderId = params != null && params.containsKey("rootFolderId") ? Long.parseLong(params.get("rootFolderId")) : null;
        if (rootFolderId != null) {
            query += " and err.root_folder_id = " + rootFolderId;
        }

        if (params != null && params.containsKey("mediaType") && !Strings.isNullOrEmpty(params.get("mediaType"))) {
            String[] types = params.get("mediaType").split(",");
            List<String> tyPesInt = new ArrayList<>();
            for (String type : types) {
                MediaType typeObj = MediaType.valueOf(type);
                tyPesInt.add(String.valueOf(typeObj.getValue()));
            }

            if (!tyPesInt.isEmpty()) {
                String values = String.join(",",  tyPesInt);;
                query += " and err.media_type in (" + values + ") ";
            }
        }

        if (!isCount) {
            query += " order by err.date_created desc limit " + offset + ", " + pageSize;
        }

        return query;
    }

    public Page<FileProcessingErrorDto> getScanErrors(DataSourceRequest dataSourceRequest, Map<String, String> params) {
        logger.debug("Get scan errors");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        List<ScanError> scanErrors = new ArrayList<>();

        String statement = buildQuery(params, pageRequest.getOffset(), pageRequest.getPageSize(), false);

        dBTemplateUtils.doInTransaction(jdbcTemplate -> {
            SqlRowSet res = jdbcTemplate.queryForRowSet(statement);
            while (res.next()) {
                ScanError err = new ScanError();
                err.setId(res.getBigDecimal("id").longValue());
                err.setDocFolder(res.getObject("doc_folder_id") == null ? null :
                        res.getBigDecimal("doc_folder_id").longValue());
                err.setRootFolderId(res.getObject("root_folder_id") == null ? null :
                        res.getBigDecimal("root_folder_id").longValue());
                err.setExceptionText(res.getString("exception_text"));
                err.setDateCreated(res.getObject("date_created") == null ? null :
                        res.getBigDecimal("date_created").longValue());
                err.setPath(res.getString("path"));
                err.setRunId(res.getObject("run_id") == null ? null :
                        res.getBigDecimal("run_id").longValue());
                err.setText(res.getString("text"));
                err.setType(res.getObject("type") == null ? null :
                        ProcessingErrorType.ordinalOf(res.getInt("type")));
                err.setMediaType(res.getObject("media_type") == null ? null :
                        MediaType.valueOf(res.getInt("media_type")));
                scanErrors.add(err);
            }
        });

        String countStatement = buildQuery(params, pageRequest.getOffset(), pageRequest.getPageSize(), true);
        List<Integer> result = new ArrayList<>();
        dBTemplateUtils.doInTransaction(jdbcTemplate -> {
            SqlRowSet res = jdbcTemplate.queryForRowSet(countStatement);
            if (res.next()) {
                result.add(res.getInt(1));
            }
        });
        int total = result.size() == 0 ? 0 : result.get(0);

        Set<Long> rootFolderIds = scanErrors.stream().map(ScanError::getRootFolderId).filter(Objects::nonNull).collect(Collectors.toSet());
        List<RootFolder> rootFolders = rootFolderIds.isEmpty() ? new ArrayList<>() : rootFolderRepository.findAllById(rootFolderIds);
        Map<Long, RootFolder> rootFoldersById = rootFolders.stream()
                .collect(Collectors.toMap(RootFolder::getId, folder -> folder));

        return convertScanErrors(scanErrors, pageRequest, total, rootFoldersById);
    }

    @Transactional(readOnly = true)
    public long countScanErrorsByRunId(Long crawlRunId) {
        return scanErrorsRepository.countByRunId(crawlRunId);
    }

    private static Page<FileProcessingErrorDto> convertScanErrors(List<ScanError> all, PageRequest pageRequest,
                                                                 long totalElements, Map<Long, RootFolder> rootFoldersById) {
        List<FileProcessingErrorDto> content = new ArrayList<>();

        for (ScanError scanError : all) {
            content.add(convertScanError(scanError, rootFoldersById));
        }
        return new PageImpl<>(content, pageRequest, totalElements);
    }

    private static FileProcessingErrorDto convertScanError(ScanError scanError, Map<Long, RootFolder> rootFoldersById) {
        FileProcessingErrorDto result = new FileProcessingErrorDto();
        result.setId(scanError.getId());
        result.setText(scanError.getText());
        result.setDetailText((scanError.getExceptionText() == null) ? "" :
                scanError.getExceptionText().replaceAll(ModelDtoConversionUtils.CLEAR_UNPRINTABLE_CHARS_REGEX, "?"));
        result.setMediaType(scanError.getMediaType());
        result.setRootFolderId(scanError.getRootFolderId());
        result.setFolder(scanError.getPath());
        result.setRunId(scanError.getRunId());
        if (scanError.getDateCreated() != null) {
            result.setTimeStamp(scanError.getDateCreated());
        } else if (scanError.getCreatedOnDB() != null) {
            result.setTimeStamp(scanError.getCreatedOnDB().getTime());
        }


        if (scanError.getRootFolderId() != null && rootFoldersById != null) {
            RootFolder rf = rootFoldersById.get(scanError.getRootFolderId());
            if (rf != null) {
                result.setRootFolder(DocStoreService.getRootFolderIdentifier(rf));
            }
        }

        return result;
    }
}
