package com.cla.filecluster.service.importEntities;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.util.csv.CsvUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by liora on 21/03/2017.
 */
public abstract class SchemaData<T extends ImportItemRow> {

    private String[] headers;

    private Map<String, Integer> headersLocationMap;
    private boolean error;
    private String message;
    private BadRequestType errorType;

    private List<T> rows = new ArrayList<>();
    private List<T> errorRows = new ArrayList<>();
    private List<T> duplicateRows = new ArrayList<>();

    private List<T> warningRows = new ArrayList<>();

    public abstract T createImportItemRowInstance();

    public void addParsingErrorRowItem(T errorRow, String message) {
        String prevMessage = errorRow.getMessage()!=null?errorRow.getMessage()+"; ":"";
        errorRow.setParsingError(true);
        this.addErrorRowItem(errorRow,prevMessage+message);
    }

    public void addErrorRowItem(T errorRow, String message) {
        errorRow.setMessage(message);
        errorRow.setError(true);
        this.addErrorItemRow(errorRow);
    }
    public void addWarningRowItem(T warningRow, String message) { //concat warning messages
        String prevMessage = warningRow.getMessage()!=null?warningRow.getMessage()+"; ":""; //TODO: prev message maybe the error or duplicate
        warningRow.setMessage(prevMessage+message);
        warningRow.setWarning(true);
        this.addWarningItemRow(warningRow);
    }
    public void addDupRowItem(T dupRow, String message) {
        dupRow.setMessage(message);
        dupRow.setWarning(true);
        this.addDuplicatedItemRow(dupRow);
    }

    public void setHeaders(String[] actualHeaders,String[] requiredHeaders) {
        this.headers = actualHeaders;
        headersLocationMap = CsvUtils.handleCsvHeaders( this.headers, requiredHeaders);
        if (headersLocationMap==null) {
            this.message = "Missing CSV required headers. Required ["+ Arrays.toString(requiredHeaders) +"]" ;
            this.errorType = BadRequestType.MISSING_FIELD;
            this.error=true;
        }
    }

    public void addItemRow(T itemRow) {

        rows.add(itemRow);
    }

    private void addErrorItemRow(T itemRow) {
        if(!errorRows.contains(itemRow)) {
            errorRows.add(itemRow);
        }
    }

    private void addDuplicatedItemRow(T itemRow) {
        if(!duplicateRows.contains(itemRow)) {
            duplicateRows.add(itemRow);
        }
    }
    private void addWarningItemRow(T itemRow) {
        if(!warningRows.contains(itemRow)) {
            warningRows.add(itemRow);
        }
    }

    public String[] getHeaders() {
        return headers;
    }

    public List<T> getRows() {
        return rows;
    }

    public List<T> getErrorRows() {
        return errorRows;
    }

    public List<T> getDuplicateRows() {
        return duplicateRows;
    }

    public List<T> getWarningRows() {
        return warningRows;
    }

    public Integer getHeaderIndex(String header) {
        if(headersLocationMap == null) {
            throw new RuntimeException("headers not set");
        }
        return headersLocationMap.get(header);
    }

    public boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public BadRequestType getErrorType() {
        return errorType;
    }

    public void setErrorType(BadRequestType errorType) {
        this.errorType = errorType;
    }
}
