package com.cla.filecluster.service.export.exporters.settings.tag;

import com.cla.common.domain.dto.filetag.FileTagTypeAndTagsDto;
import com.cla.filecluster.domain.entity.security.ClaUserDetails;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.settings.tag.mixin.TagExportDataMixIn;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.tag.TagHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/12/2019
 */
@Slf4j
@Component
public class TagTypeExporter extends PageCsvExcelExporter<TagExportData> {

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    private final static String[] header = {TYPE, NAME, PROPERTIES, SINGLE_VAL, TYPE_DESCRIPTION, DESCRIPTION};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.TAG_TYPE_SETTINGS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(TagExportData.class, TagExportDataMixIn.class);
    }

    @Override
    protected Page<TagExportData> getData(Map<String, String> requestParams) {
        Long userId = 0L;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal != null && principal instanceof ClaUserDetails) {
            userId = ((ClaUserDetails)principal).getUser().getId();
        }
        List<FileTagTypeAndTagsDto> dataPerType = fileTaggingAppService.getAllFileTagTypesAndChildren(userId);
        List<TagExportData> tags = new ArrayList<>();
        dataPerType.forEach(type -> {
            type.getFileTags().forEach(tag -> {
                TagExportData tagData = new TagExportData();
                tagData.setName(tag.getName());
                tagData.setType(type.getFileTagTypeDto().getName());
                tagData.setTypeDescription(type.getFileTagTypeDto().getDescription());
                tagData.setDescription(tag.getDescription());
                tagData.setSingleValue(type.getFileTagTypeDto().isSingleValueTag());
                tagData.setProperties(type.getFileTagTypeDto().isSensitive());
                tags.add(tagData);
            });
        });
        return new PageImpl<>(tags);
    }

    @Override
    protected Map<String, Object> addExtraFields(TagExportData base, Map<String, String> requestParams) {
        return null;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }
}
