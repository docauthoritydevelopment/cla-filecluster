package com.cla.filecluster.service.filetag;

import com.cla.common.domain.dto.filetag.FileTagTypeSettingsDto;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.repository.jpa.filetag.FileTagSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by: yael
 * Created on: 9/5/2018
 */
@Service
public class FileTagSettingService {

    @Autowired
    private FileTagSettingRepository fileTagSettingRepository;

    public void deleteByFileTag(long fileTagId) {
        List<FileTagSetting> fileTagSetting = fileTagSettingRepository.findByFileTag(fileTagId);
        if (fileTagSetting != null && !fileTagSetting.isEmpty()) {
            fileTagSetting.forEach( s -> {
                s.setDeleted(true);
                s.setDirty(true);
            });
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveFileTagSetting(FileTagSetting fts) {
        fileTagSettingRepository.save(fts);
    }

    public static List<FileTagTypeSettingsDto> convertFileTagTypeSettings(Set<FileTagSetting> fileTagSettings) {
        return fileTagSettings.stream()
                .filter(fts -> !fts.isDeleted())
                .collect(Collectors.groupingBy(ft -> ft.getFileTag().getType()))
                .entrySet().stream()
                .map(entry -> createFileTagTypeSettingsDto(entry.getKey(), entry.getValue()))
                .collect(toList());
    }

    private static FileTagTypeSettingsDto createFileTagTypeSettingsDto(FileTagType fileTagType, List<FileTagSetting> fileTagSettings) {
        FileTagTypeSettingsDto fileTagTypeSettingsDto = new FileTagTypeSettingsDto();
        fileTagTypeSettingsDto.setFileTagType(FileTagTypeService.convertFileTagTypeToDto(fileTagType));
        fileTagTypeSettingsDto.setFileTags(fileTagSettings.stream()
                .map(fileTagSetting -> FileTagService.convertFileTagToDto(fileTagSetting.getFileTag()))
                .collect(toList()));
        return fileTagTypeSettingsDto;
    }
}
