package com.cla.filecluster.service.export.exporters.security.bizrole;

import com.cla.common.domain.dto.security.BizRoleDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class BizRoleSummaryInfoDtoMixin {

    @JsonUnwrapped
    private BizRoleDto bizRoleDto;
}
