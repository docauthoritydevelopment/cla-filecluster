package com.cla.filecluster.service.extractor.entity;

import java.io.Closeable;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

public class CsvReaderIterable  implements  Iterable<List<String>>, Closeable {

	ICsvListReader listReader;
	List<String> row;
	private String[] headers;

	CsvReaderIterable(final String fileName, final boolean withHeaders) throws Exception {
		listReader = new CsvListReader(new FileReader(fileName), CsvPreference.STANDARD_PREFERENCE);
		if (withHeaders) {
			headers = listReader.getHeader(true);
		}
		row = listReader.read();
	}

	public String[] getHeaders() {
		return headers;
	}

	@Override
	public void close() throws IOException {
		if (listReader!=null) {
			listReader.close();
			listReader = null;
		}
	}

	@Override
	public Iterator<List<String>> iterator() {
		return new Iterator<List<String>>() {

			@Override
			public boolean hasNext() {
				return row!=null;
			}

			@Override
			public List<String> next() {
				final List<String> result = row;
				try {
					row = listReader.read();
				} catch (final IOException e) {
					row = null;
				}
				return result;
			}
		};
	}
	
}
