package com.cla.filecluster.service.export.exporters;


import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.export.ResultType;
import com.cla.filecluster.service.export.RxExporter;
import com.cla.filecluster.service.export.retrievers.DeepPageRetriever;
import com.cla.filecluster.service.export.retrievers.MultiPageRetriever;
import com.cla.filecluster.service.export.retrievers.Retriever;
import com.cla.filecluster.service.export.retrievers.SupplierIterableRetriever;
import com.cla.filecluster.service.export.transformers.AddExtraFields;
import com.cla.filecluster.service.export.transformers.BufferByRows;
import com.cla.filecluster.service.export.transformers.CsvToExcel;
import com.cla.filecluster.service.export.transformers.PojoToCsv;
import com.cla.filecluster.service.export.transformers.RateLimiter;
import com.cla.filecluster.service.progress.ProgressService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.Map;

@Slf4j
@Component
public abstract class PageCsvExcelExporter<T> implements RxExporter {

    @Value("${exporter.page-size:10000}")
    private Integer pageSize;

    @Value("${exporter.buffer-size:1048575}")
    protected Integer bufferSize;

    @Value("${exporter.rate-limit.high-tide:3}")
    private Integer highTide;

    @Value("${exporter.rate-limit.low-tide:1}")
    private Integer lowTide;

    @Autowired
    private ProgressService progressService;

    @Autowired
    private MessageHandler messageHandler;


    @Override
    public Flux<DataBuffer> createExportFlux(Map<String, String> params, String exportId) {

        if (params.containsKey(DataSourceRequest.TAKE) && Integer.parseInt(params.get(DataSourceRequest.TAKE)) > bufferSize) {
            throw new BadRequestException(messageHandler.getMessage("export.download-limit"), BadRequestType.UNSUPPORTED_VALUE);
        }

        // get DTO
        Retriever<Iterable<T>> pageRetriever = getRetriever(params, exportId);

        // transformations....
        return pageRetriever.createFlux()
                .compose(RateLimiter.with(highTide, lowTide))
                .compose(BufferByRows.with(bufferSize))
                .compose(AddExtraFields.with(params, this::addExtraFields))
				.compose(PojoToCsv.with(exportId, newChildProgress(exportId, "Converting to table..."), params)
                        .configureMapper(this::configureMapper)
                        .setScemaProvider(this::defineSchema))
                .compose(CsvToExcel.with(exportId, newChildProgress(exportId, "Creating Excel...")).asDataBuffer());
                // TODO add support for multiple packaged files.
                //.compose(DumpToFile.with(exportId, newChildProgress(exportId, "Saving to file/s")));
                //.compose(ZipIfNeeded.with(exportId, newChildProgress(exportId, "Zipping")));

    }

    protected CsvSchema defineSchema(Map<String,String> requestParams, Iterable data) {
        String[] header = getHeader();
        CsvSchema.Builder schemaBuilder = CsvSchema.builder();
        Arrays.stream(header).forEach(schemaBuilder::addColumn);
        return schemaBuilder.build();
    }

    private Retriever<Iterable<T>> getRetriever(Map<String, String> params, String exportId) {
        val pagingMethod = getPagingMethod();
        switch (pagingMethod) {
            case SINGLE:
                return (SupplierIterableRetriever) () -> progressedGetData(exportId, params);
            case MULTI:
                return new MultiPageRetriever<>(params, pageSize, (pageParams) -> progressedGetData(exportId, pageParams));
            case MULTI_DEEP:
                return new DeepPageRetriever<>(params, pageSize, (pageParams) -> progressedGetData(exportId, pageParams));
            default:
                throw new RuntimeException("Unknown paging method: " + pagingMethod);

        }
    }

    private ProgressService.Handler newChildProgress(String exportId, String message) {
        return progressService.createChildHandler(exportId, message);
    }

    protected abstract String[] getHeader();

    protected abstract void configureMapper(CsvMapper mapper);

    protected abstract Iterable<T> getData(Map<String, String> requestParams);

    protected abstract Map<String, Object> addExtraFields(T base, Map<String, String> requestParams);

    public abstract PagingMethod getPagingMethod();

    protected Iterable<T> progressedGetData(String exportId, Map<String, String> requestParams) {
        ProgressService.Handler progress =
                newChildProgress(exportId, "fetching data...");
        progress.set(0, "started fetching data for exporting");
        Iterable<T> result = getData(requestParams);
        progress.set(100, "done fetching data for exporting");
        return result;
    }

    @Override
    public ResultType getExportResultType(String fileNameHint) {
        return ResultType.of(fileNameHint == null ? "root-folders" : fileNameHint ,"xlsx",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" );
    }
}
