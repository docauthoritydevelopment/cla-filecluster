package com.cla.filecluster.service.files.managers;

import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.converters.SolrFileGroupBuilder;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static com.cla.common.constants.FileGroupsFieldType.*;

/**
 * operations on file groups
 * <p>
 * Created by: yael
 * Created on: 12/27/2017
 */
@Service
public class FileGroupService implements ControlledScheduling {

    public static final String UNKNOWN_GROUP_NAME = "???";

    private static final Logger logger = LoggerFactory.getLogger(FileGroupService.class);

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private GroupsService groupsService;

    @Value("${group.orphan.min-age-remove-millis:3600000}")
    private int groupOrphanMinimumAgeMillis;

    @Value("${groupsUpdate.batch.size:10000}")
    private int groupsUpdateBatchSize;

    private ReadWriteLock groupSqlHeavyOperationLock = new ReentrantReadWriteLock();

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    public void getGroupSqlHeavyOperationLock() {
        groupSqlHeavyOperationLock.writeLock().lock();
    }

    public void releaseGroupSqlHeavyOperationLock() {
        groupSqlHeavyOperationLock.writeLock().unlock();
    }

    @Scheduled(initialDelayString = "${file-group.orphan-groups-init-millis:90000}",
            fixedRateString = "${file-group.orphan-groups-rate-millis:18000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    void cleanOrphanFileGroups() {
        if (!isSchedulingActive) {
            return;
        }
        getGroupSqlHeavyOperationLock();
        try {
            deleteOrphans();
        } catch (Exception e) {
            logger.error("problem in cleanOrphanFileGroups", e);
        } finally {
            releaseGroupSqlHeavyOperationLock();
        }
    }

    public int clearDirtyAtomicExceptGroupNaming(double recalculateThreshold, long toDate) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, true)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(TYPE, SolrOperator.EQ, GroupType.USER_GROUP.toInt())));

        filterQueries.add(SolrQueryGenerator.generateFilter(
                Criteria.or(
                        Criteria.create(DIRTY_DATE, SolrOperator.MISSING, null),
                        Criteria.create(DIRTY_DATE, SolrOperator.LT, toDate))));

        Integer res = fileGroupRepository.updateFieldByQuery(DIRTY.getSolrName(), "false",
                SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);

        filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, true)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(COUNT_LAST_NAMING, SolrOperator.GT, 0)));
        filterQueries.add("{!frange incu=false u="+recalculateThreshold+"} div(sub("+
                NUM_OF_FILES.getSolrName()+","+COUNT_LAST_NAMING.getSolrName()+"), "+COUNT_LAST_NAMING.getSolrName()+")");

        Integer res2 = fileGroupRepository.updateFieldByQuery(DIRTY.getSolrName(), "false", SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
        return (res == null ? 0 : res) + (res2 == null ? 0 : res2);
    }

    public int countDirtyExceptGroupNaming(double recalculateThreshold) {

        SolrQuery query = new SolrQuery();
        query.setQuery(SolrQueryGenerator.ALL_QUERY);
        query.setRows(0);
        query.addFilterQuery(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, true)));
        query.addFilterQuery(SolrQueryGenerator.generateFilter(Criteria.create(TYPE, SolrOperator.EQ, GroupType.USER_GROUP.toInt())));

        QueryResponse resp = fileGroupRepository.runQuery(query);
        int res = (int)resp.getResults().getNumFound();

        query = new SolrQuery();
        query.setQuery(SolrQueryGenerator.ALL_QUERY);
        query.setRows(0);
        query.addFilterQuery(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, true)));
        query.addFilterQuery(SolrQueryGenerator.generateFilter(Criteria.create(COUNT_LAST_NAMING, SolrOperator.GT, 0)));
        query.addFilterQuery("{!frange incu=false u="+recalculateThreshold+"} div(sub("+
                NUM_OF_FILES.getSolrName()+","+COUNT_LAST_NAMING.getSolrName()+"), "+COUNT_LAST_NAMING.getSolrName()+")");

        resp = fileGroupRepository.runQuery(query);
        return res + (int)resp.getResults().getNumFound();
    }

    public Map<String, String> getGroupIdsWithParentIds(Collection<String> groupIds) {
        Query query = Query.create().setRows(groupIds.size())
                .addFilterWithCriteria(Criteria.create(ID, SolrOperator.IN, groupIds))
                .addFilterWithCriteria(Criteria.create(PARENT_GROUP_ID, SolrOperator.PRESENT, null))
                .addField(ID).addField(PARENT_GROUP_ID)
                ;
        List<SolrFileGroupEntity> entities = fileGroupRepository.find(query.build());

        Map<String, String> results = new HashMap<>();
        entities.forEach(entity -> results.put(entity.getId(), entity.getParentGroupId()));
        return results;
    }

    public int countDirty() {
        Query query = Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false))
                .addFilterWithCriteria(Criteria.create(DIRTY, SolrOperator.EQ, true));
        QueryResponse resp = fileGroupRepository.runQuery(query.build());
        return (int)resp.getResults().getNumFound();
    }


    public int clearDirtyAtomicRepository(long toDate) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, true)));
        filterQueries.add(SolrQueryGenerator.generateFilter(
                Criteria.or(
                        Criteria.create(DIRTY_DATE, SolrOperator.MISSING, null),
                        Criteria.create(DIRTY_DATE, SolrOperator.LT, toDate))));
        Integer res = fileGroupRepository.updateFieldByQuery(DIRTY.getSolrName(), "false", SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
        return (res == null ? 0 : res);
    }

    public int updateSampleFileDirtyAtomic(List<String> groupIds) {
        Map<String, Long> firstMembers = groupsService.getFirstMemberForAnalysisGroups(groupIds);
        List<SolrInputDocument> queries = groupsService.getFirstMemberQueries(groupIds, firstMembers);
        fileGroupRepository.saveAll(queries);
        fileGroupRepository.softCommitNoWaitFlush();
        return groupIds.size();
    }

    public int markAnalysisGroupsChangedGroupsForNameCalculation(double recalculateThreshold) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(GENERATED_NAME, SolrOperator.PRESENT, null)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(NUM_OF_FILES, SolrOperator.GT, 0)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt())));
        filterQueries.add("(("+ SolrQueryGenerator.generateFilter(Criteria.create(COUNT_LAST_NAMING, SolrOperator.GT, 0))
                +") OR ({!frange incl=false l="+recalculateThreshold+"}div(sub("+
                NUM_OF_FILES.getSolrName()+","+COUNT_LAST_NAMING.getSolrName()+"), "+COUNT_LAST_NAMING.getSolrName()+")))");

        //Integer res = fileGroupRepository.updateFieldByQuery(GENERATED_NAME.getSolrName(), null, SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
        Integer res = fileGroupRepository.updateFieldByQueryMultiFields(new String[]{GENERATED_NAME.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{null, String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries,
                SolrCommitType.SOFT_NOWAIT);


        return (res == null ? 0 : res);
    }

    @Transactional
    public void updateGroupFirstMemberContentAndFile(String groupId) {
        groupsService.updateGroupFirstMemberContentAndFile(groupId);
    }


    public int clearDirtyForDeleted() {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, true)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DELETED, SolrOperator.EQ, true)));
        Integer res = fileGroupRepository.updateFieldByQuery(DIRTY.getSolrName(), "false", SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
        return (res == null ? 0 : res);
    }

    public int updateNumOfFilesForAnalysisGroups(List<String> groupIds) {
        Map<String, Long> sizes = groupsService.getFileNumForAnalysisGroups(groupIds);
        List<SolrInputDocument> queries = getFileNumQueries(groupIds, sizes);
        fileGroupRepository.saveAll(queries);
        fileGroupRepository.softCommitNoWaitFlush();
        return groupIds.size();
    }

    public int countNonDeletedDirtyAnalysisGroups() {
        Query query = Query.create().setRows(0)
                .addFilterWithCriteria(Criteria.create(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt()))
                .addFilterWithCriteria(Criteria.create(DIRTY, SolrOperator.EQ, true));

        QueryResponse resp = fileGroupRepository.runQuery(query.build());
        return (int)resp.getResults().getNumFound();
    }

    public QueryResponse runQuery(SolrQuery query) {
        return fileGroupRepository.runQuery(query);
    }

    public int countNonDeletedFileGroupIdsOrderedBySize() {
        Query query = Query.create().setRows(0)
                .addFilterWithCriteria(Criteria.create(NUM_OF_FILES, SolrOperator.GT, 0))
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false));

        QueryResponse resp = fileGroupRepository.runQuery(query.build());
        return (int)resp.getResults().getNumFound();
    }

    public int countNonDeletedDirtyFileGroupIds() {
        Query query = Query.create().setRows(0)
                .addFilterWithCriteria(Criteria.create(DIRTY, SolrOperator.EQ, true));

        QueryResponse resp = fileGroupRepository.runQuery(query.build());
        return (int)resp.getResults().getNumFound();
    }

    public void updateNumOfFilesForUserGroup(String id) {
        long numOfFilesInUserGroup = groupsService.getFileNumForUserGroup(id);
        updateNumOfFiles(numOfFilesInUserGroup, id);
    }

    public FileGroup getById(String groupId) {
        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public void updateNumOfFilesInUserGroupsRepository() {
        String cursor = "*";
        boolean lastPage;
        int pageSize = 10000;
        do {
            Query query = Query.create().addFilterWithCriteria(
                    Criteria.or(Criteria.create(DIRTY, SolrOperator.EQ, true), Criteria.create(FIRST_MEMBER_ID, SolrOperator.MISSING, null))
            ).addFilterWithCriteria(Criteria.create(NUM_OF_FILES, SolrOperator.GT, 0))
                    .addFilterWithCriteria(TYPE, SolrOperator.EQ, GroupType.USER_GROUP.toInt())
                    .setRows(pageSize).setCursorMark(cursor).addField(ID)
                    .addSort(ID, Sort.Direction.ASC);

            QueryResponse resp = fileGroupRepository.runQuery(query.build());
            cursor = resp.getNextCursorMark();
            if (resp.getResults().size() > 0) {
                List<String> groupIds = resp.getResults().stream().map(
                        doc -> (String)doc.getFieldValue(ID.getSolrName())).collect(Collectors.toList());
                updateNumOfFilesInUserGroups(groupIds);
            }
            lastPage = resp.getResults().size() < pageSize;
        } while (!lastPage);
    }

    public void updateNumOfFilesInUserGroups(Collection<String> groupIds) {
        if (groupIds == null || groupIds.isEmpty()) {
            return;
        }

        List<List<String>> partitions;
        if (groupIds.size() > groupsUpdateBatchSize) {
            partitions = Lists.partition(new ArrayList<>(groupIds), groupsUpdateBatchSize);
        } else {
            partitions = new ArrayList<>();
            partitions.add(new ArrayList<>(groupIds));
        }

        for (List<String> groups : partitions) {
            Map<String, Long> sizes = groupsService.getFileNumForUserGroups(groups);
            List<SolrInputDocument> queries = getFileNumQueries(groups, sizes);
            fileGroupRepository.saveAll(queries);
        }
        fileGroupRepository.softCommitNoWaitFlush();
    }

    private List<SolrInputDocument> getFileNumQueries(Collection<String> groupIds, Map<String, Long> sizes) {
        List<SolrInputDocument> queries = new ArrayList<>();

        groupIds.forEach(gid -> {
            Long size = sizes.getOrDefault(gid, 0L);
            SolrInputDocument doc = new SolrInputDocument();
            doc.setField(ID.getSolrName(), gid);
            SolrFileGroupRepository.addField(doc, NUM_OF_FILES, size, SolrFieldOp.SET);
            SolrFileGroupRepository.addField(doc, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
            queries.add(doc);
        });

        return queries;
    }

    public void updateUserGroupNumOfFilesAndFirstMember(String userGroupId) {
        groupsService.updateGroupFirstMemberContentAndFile(userGroupId);
        Long fileNum = groupsService.getFileNumForUserGroup(userGroupId);
        updateNumOfFiles(fileNum, userGroupId);
    }

    public void updateNumOfFiles(long numOfFiles, String id) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(id);
        builder.setNumOfFiles(numOfFiles);
        fileGroupRepository.saveGroupChanges(builder);
    }

    public FileGroup getFileGroupById(String id) {
        return fileGroupRepository.getById(id).orElse(null);
    }

    private void deleteOrphans() {
        int totalUpdates2 = 0;

        QueryResponse resp = fileGroupRepository.runQuery(
                Query.create().setRows(0)
                        .addFilterWithCriteria(DELETED, SolrOperator.EQ, false)
                        .addFilterWithCriteria(
                                Criteria.create(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt())).build());


        final long totalGroups = resp.getResults().getNumFound();

        if (totalGroups == 0) return;

        long timestamp = System.currentTimeMillis() - groupOrphanMinimumAgeMillis;
        String cursor = "*";
        boolean lastPage;
        do {
            Query query = Query.create()
                    .addFilterWithCriteria(DIRTY, SolrOperator.EQ, false)
                    .addFilterWithCriteria(DELETED, SolrOperator.EQ, false)
                    .addFilterWithCriteria(Criteria.create(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt()))
                    .addFilterWithCriteria(Criteria.create(HAS_SUBGROUPS, SolrOperator.EQ, false))
                    .addFilterWithCriteria(Criteria.create(RAW_ANALYSIS_GROUP_ID, SolrOperator.MISSING, null))
                    .addFilterWithCriteria(Criteria.create(CREATION_DATE, SolrOperator.LT, timestamp))
                    .addFilterWithCriteria(Criteria.or(
                            Criteria.create(NUM_OF_FILES, SolrOperator.MISSING, null),
                            Criteria.create(NUM_OF_FILES, SolrOperator.EQ, 0)
                    ))
                    .setRows(groupsUpdateBatchSize).setCursorMark(cursor).addField(ID)
                    .addSort(ID, Sort.Direction.ASC);
                    ;

            resp = fileGroupRepository.runQuery(query.build());
            cursor = resp.getNextCursorMark();
            long updated = 0;
            if (resp.getResults().size() > 0) {
                List<String> groupIds = resp.getResults().stream().map(
                        doc -> (String)doc.getFieldValue(ID.getSolrName())).collect(Collectors.toList());
                updated = deleteOrphansQueries(groupIds);
                totalUpdates2 += updated;
            }
            logger.debug("Loop: Removed {} orphan groups (marked deleted). total={}. ", updated, totalUpdates2);
            lastPage = resp.getResults().size() < groupsUpdateBatchSize;
        } while (!lastPage);
        logger.info("Removed {} orphan groups (marked deleted)", totalUpdates2);
    }

    public List<SolrFileGroupEntity> findByIds(Collection<String> ids) {
        return fileGroupRepository.findByIds(ids);
    }

    public List<SolrFileGroupEntity> find(SolrQuery query) {
        return fileGroupRepository.find(query);
    }

    private int deleteOrphansQueries(Collection<String> groupIds) {
        List<SolrInputDocument> queries = new ArrayList<>();

        groupIds.forEach(gid -> {
            SolrInputDocument doc = new SolrInputDocument();
            doc.setField(ID.getSolrName(), gid);
            SolrFileGroupRepository.addField(doc, DELETED, true, SolrFieldOp.SET);
            SolrFileGroupRepository.addField(doc, PARENT_GROUP_ID, null, SolrFieldOp.SET);
            SolrFileGroupRepository.addField(doc, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
            queries.add(doc);
        });

        fileGroupRepository.saveAll(queries);
        fileGroupRepository.softCommitNoWaitFlush();
        return queries.size();
    }

    public void markGroupAsDirtyByIds(Collection<String> ids) {
        if (ids == null || ids.isEmpty()) {
            return;
        }
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.IN, ids)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, false)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DELETED, SolrOperator.EQ, false)));

        fileGroupRepository.updateFieldByQueryMultiFields(new String[]{DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), DIRTY_DATE.getSolrName()},
                new String[]{"true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries,
                SolrCommitType.SOFT_NOWAIT);
        //fileGroupRepository.updateFieldByQuery(DIRTY.getSolrName(), "true", SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
    }

    public synchronized int markUnifiedGroupsByIds(List<String> groupIds) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(ID, SolrOperator.IN, groupIds)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DIRTY, SolrOperator.EQ, false)));
        filterQueries.add(SolrQueryGenerator.generateFilter(Criteria.create(DELETED, SolrOperator.EQ, false)));
        Integer res = fileGroupRepository.updateFieldByQueryMultiFields(new String[]{DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), DIRTY_DATE.getSolrName()},
                new String[]{"true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                SolrQueryGenerator.ALL_QUERY, filterQueries,
                SolrCommitType.SOFT_NOWAIT);

         //fileGroupRepository.updateFieldByQuery(DIRTY.getSolrName(), "true", SolrFieldOp.SET, null, filterQueries, SolrCommitType.SOFT_NOWAIT);
        return (res == null ? 0 : res);
    }

    public void updateGroupsPopulationDirty(Collection<String> groupIds) {
        List<String> filterQueries = Lists.newLinkedList();
        filterQueries.add(SolrQueryGenerator.generateFilter(
                Criteria.or(Criteria.create(DIRTY, SolrOperator.EQ, false),
                            Criteria.create(DELETED, SolrOperator.EQ, true))
        ));
        fileGroupRepository.updateFieldByQueryMultiFields(new String[]{DELETED.getSolrName(), DIRTY.getSolrName(), UPDATE_DATE.getSolrName(), DIRTY_DATE.getSolrName()},
                new String[]{"false", "true", String.valueOf(System.currentTimeMillis()), String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                ID.inQuery(groupIds), filterQueries,
                SolrCommitType.SOFT_NOWAIT);
    }

    public Pair<Long, List<FileGroup>> findAll(int offset, int pageSize) {
        Query query = Query.create()
                .setRows(pageSize)
                .setStart(offset)
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false))
                .addSort(ID, Sort.Direction.ASC);

        return fileGroupRepository.getByQueryWithTotal(query);
    }

    public Pair<Long, List<FileGroup>> findAllReportFullPage(int offset, int pageSize) {
        Query query = Query.create()
                .setRows(pageSize)
                .setStart(offset)
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false))
                .addSort(NUM_OF_FILES, Sort.Direction.DESC)
                .addSort(NAME, Sort.Direction.ASC);

        return fileGroupRepository.getByQueryWithTotal(query);
    }

    public Pair<Long, List<GroupDto>> listAllGroupsWithFirstFileDefaultSort(int offset, int pageSize) {
        Query query = Query.create()
                .setRows(pageSize)
                .setStart(offset)
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false))
                .addSort(NUM_OF_FILES, Sort.Direction.DESC)
                .addSort(NAME, Sort.Direction.ASC)
                .addSort(ID, Sort.Direction.ASC)
                .addField(ID).addField(NAME).addField(NUM_OF_FILES).addField(FIRST_MEMBER_ID)
                .addField(GENERATED_NAME).addField(TYPE);

        return returnPageGroupDto(query);
    }

    public Pair<Long, List<GroupDto>> listAllGroupsWithFirstFile(int offset, int pageSize) {
        Query query = Query.create()
                .setRows(pageSize)
                .setStart(offset)
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false))
                .addSort(ID, Sort.Direction.ASC)
                .addField(ID).addField(NAME).addField(NUM_OF_FILES).addField(FIRST_MEMBER_ID)
                .addField(GENERATED_NAME).addField(TYPE);

        return returnPageGroupDto(query);
    }

    private Pair<Long, List<GroupDto>> returnPageGroupDto(Query query) {
        QueryResponse queryResponse = fileGroupRepository.runQuery(query.build());
        List<SolrFileGroupEntity> entities = queryResponse.getBeans(SolrFileGroupEntity.class);
        return Pair.of(queryResponse.getResults().getNumFound(), entities.stream().map(entity ->
                new GroupDto(entity.getId(), entity.getName(), entity.getNumOfFiles(), entity.getFirstMemberId(),
                        entity.getGeneratedName(), GroupType.valueOf(entity.getType()))).collect(Collectors.toList()));
    }
}
