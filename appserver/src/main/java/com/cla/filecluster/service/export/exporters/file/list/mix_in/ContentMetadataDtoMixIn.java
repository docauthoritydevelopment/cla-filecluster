package com.cla.filecluster.service.export.exporters.file.list.mix_in;

import com.fasterxml.jackson.annotation.JsonValue;

public abstract class ContentMetadataDtoMixIn {
    @JsonValue
    private int fileCount;
}
