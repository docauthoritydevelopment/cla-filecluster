package com.cla.filecluster.service.media;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.service.importEntities.ImportItemRow;

/**
 * Created by liora on 21/03/2017.
 */


public class MediaConnectionDetailsItemRow extends ImportItemRow {


    private String name;
    private String username;
    private MediaType mediaType;
    private String url;
    private String domain;
    private String tenantId;
    private String applicationId;
    private String clientId;
    private String resource;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String password;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    @Override
    public String toString() {
        return "MediaConnectionDetailsItemRow{" +
                "name='" + name + '\'' +
                "mediaType='" + mediaType + '\'' +
                "username='" + username + '\'' +
                ", url='" + url + '\'' +
                ", domain='" + domain + '\'' +
                ", tenantId='" + tenantId + '\'' +
                ", applicationId='" + applicationId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", resource='" + resource + '\'' +
                super.toString()+
                '}';
    }
}


