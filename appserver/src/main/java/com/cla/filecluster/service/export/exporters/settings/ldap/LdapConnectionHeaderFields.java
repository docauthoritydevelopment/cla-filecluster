package com.cla.filecluster.service.export.exporters.settings.ldap;

/**
 * Created by: yael
 * Created on: 5/27/2019
 */
public interface LdapConnectionHeaderFields {
    String NAME = "NAME";
    String HOST = "HOST";
    String ACTIVE = "Active";
    String TYPE = "TYPE";
    String MANAGE_DN = "MANAGE DN";
    String PORT = "PORT";
    String SSL = "SSL";
}
