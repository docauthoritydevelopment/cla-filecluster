package com.cla.filecluster.service.export.exporters.file.prop;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileContentDuplicationDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.acl.AclAggCountDtoMixin;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

@Slf4j
@Component
public class ContentExporter extends PageCsvExcelExporter<AggregationCountItemDTO<FileContentDuplicationDto>> {

    private final static String[] HEADER = {ID, NAME, NUM_OF_FILES};

    @Autowired
    private FileCatAppService fileCatAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AclAggCountDtoMixin.class);
        mapper.addMixIn(FileContentDuplicationDto.class, FileContentDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<FileContentDuplicationDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<FileContentDuplicationDto>> res = fileCatAppService.getContentFileCounts(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<FileContentDuplicationDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.CONTENTS);
    }
}
