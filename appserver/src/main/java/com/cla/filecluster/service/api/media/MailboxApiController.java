package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.filetree.MailboxDto;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.files.MailboxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by: yael
 * Created on: 2/11/2019
 */
@RestController
@RequestMapping("/api/mailbox")
public class MailboxApiController {

    private static final Logger logger = LoggerFactory.getLogger(MailboxApiController.class);

    @Autowired
    private MailboxService mailboxService;

    @Autowired
    private EventAuditService eventAuditService;

    @RequestMapping(value = "/listByRootFolder/{rootFolderId}", method = RequestMethod.GET)
    public List<MailboxDto> getAllRootFolderMailboxs(@PathVariable long rootFolderId) {
        return mailboxService.getByRootFolderId(rootFolderId);
    }

    @RequestMapping(value = "/{mailboxId}", method = RequestMethod.GET)
    public MailboxDto getMailbox(@PathVariable long mailboxId) {
        return mailboxService.getById(mailboxId);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public MailboxDto addMailbox(@RequestBody MailboxDto mailboxDto) {
        MailboxDto result = mailboxService.createMailboxDto(mailboxDto);
        eventAuditService.audit(AuditType.MAILBOX, "Create mailbox", AuditAction.CREATE, MailboxDto.class, mailboxDto);
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public MailboxDto updateMailbox(@PathVariable long id, @RequestBody MailboxDto mailboxDto) {
        MailboxDto result = mailboxService.updateMailboxDto(mailboxDto);
        eventAuditService.audit(AuditType.MAILBOX, "Update mailbox", AuditAction.UPDATE, MailboxDto.class, mailboxDto.getId(), mailboxDto);
        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteMailbox(@PathVariable long id) {
        mailboxService.deleteMailbox(id);
        eventAuditService.audit(AuditType.MAILBOX, "Delete mailbox ", AuditAction.DELETE, MailboxDto.class, id);
    }
}
