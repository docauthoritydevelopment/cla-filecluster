package com.cla.filecluster.service.extractor.pattern;

import com.beust.jcommander.internal.Lists;
import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filesearchpatterns.*;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.jpa.searchpattern.RegulationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Created by ophir on 10/04/2019.
 */
@Service
public class RegulationAppService {

    private static final Logger logger = LoggerFactory.getLogger(RegulationAppService.class);

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private RegulationRepository regulationRepository;

    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    static final private Pattern commaPattern = Pattern.compile("[,]");

    @Transactional(readOnly = true)
    public Page<RegulationDto> listRegulations(DataSourceRequest dataSourceRequest) {
        return listRegulations(dataSourceRequest, false);
    }

    @Transactional(readOnly = true)
    public Page<RegulationDto> listRegulations(DataSourceRequest dataSourceRequest, Boolean getFullRegulation) {
        Page<Regulation> regulations = regulationRepository.findAll(dataSourceRequest.createPageRequestWithSort());
        return convertRegulationsToDto(regulations, getFullRegulation);
    }


    @Transactional
    public RegulationDto updateRegulation(RegulationDto regulationDto) {
        validateRegulation(regulationDto);
        logger.debug("Update regulation to {}", regulationDto);
        Regulation regulation = regulationRepository.getOne(regulationDto.getId());
        convertRegulationDtoToRegulation(regulation, regulationDto);
        Regulation ansRegulation = regulationRepository.save(regulation);
        return convertRegulationToDto(ansRegulation, false);
    }


    private void convertRegulationDtoToRegulation(Regulation targetRegulation, RegulationDto regulationDto) {
        targetRegulation.setName(regulationDto.getName());
        targetRegulation.setActive(regulationDto.isActive());
    }

    @Transactional
    public RegulationDto createRegulation(RegulationDto regulationDto) {
        validateRegulation(regulationDto);
        logger.debug("Create Regulation {}", regulationDto);
        Regulation regulation = new Regulation();
        convertRegulationDtoToRegulation(regulation, regulationDto);
        Regulation ansRegulation = regulationRepository.save(regulation);
        return convertRegulationToDto(ansRegulation, false);
    }

    @Transactional(readOnly = true)
    public RegulationDto getRegulationById(Integer id) {
        Regulation regulation = regulationRepository.findById(id).orElse(null);
        if (regulation == null) {
            throw new BadRequestException(messageHandler.getMessage("regulation.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return convertRegulationToDto(regulation, true);
    }

    private void validateRegulation(RegulationDto regulationDto) {
        if (regulationDto == null) {
            throw new BadRequestException(messageHandler.getMessage("regulations.empty-value"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(regulationDto.getName()) || regulationDto.getName().trim().isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("regulations.name.mandatory"), BadRequestType.MISSING_FIELD);
        }

        Regulation regulation = getRegulationByName(regulationDto.getName());
        if (regulation != null && !regulation.getId().equals(regulationDto.getId())) {
            throw new BadRequestException(messageHandler.getMessage("regulations.name.exist",
                    Collections.singletonList("'" + regulationDto.getName() + "'")), BadRequestType.ITEM_ALREADY_EXISTS);
        }

        Matcher matcher = commaPattern.matcher(regulationDto.getName());
        if (matcher.find()) {
            throw new BadRequestException(messageHandler.getMessage("regulations.name.illegal-characters",
                    Collections.singletonList(regulationDto.getName())), BadRequestType.ILLEGAL_CHARACTERS);
        }

    }

    @Transactional
    public void deleteRegulation(int id) {
        List<TextSearchPattern> patterns = textSearchPatternRepository.findByRegulationId(id);
        if (patterns != null && patterns.size() > 0) {
            throw new BadRequestException(messageHandler.getMessage("regulations.delete.attached-pattern"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }
        regulationRepository.deleteById(id);
    }

    @Transactional
    public RegulationDto updateRegulationState(int id, boolean state) {
        Optional<Regulation> regulationOption = regulationRepository.findById(id);
        if (!regulationOption.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("regulation.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        Regulation regulation = regulationOption.get();
        regulation.setActive(state);
        Regulation ansRegulation = regulationRepository.save(regulation);
        return convertRegulationToDto(ansRegulation, false);
    }

    @Transactional
    Regulation getRegulationByName(String name) {
        return regulationRepository.findByName(name);
    }

    private String getRegulationIdStr(RegulationDto item) {
        return String.valueOf(item.getId());
    }

    private List<SolrFacetQueryJson> buildJsonFacets(List<Regulation> regulations) {
        return regulations.stream()
                .filter(Regulation::isActive)
                .map(regulation -> {
                    SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.REGULATION_ID.getSolrName() + regulation.getId());
                    innerFacet.setType(FacetType.QUERY);
                    innerFacet.setField(CatFileFieldType.MATCHED_PATTERNS_SINGLE);
                    if (regulation.getPatterns() != null) {
                        List<String> patternIds = new ArrayList<>();
                        regulation.getPatterns().forEach((TextSearchPattern pattern) -> {
                            if (pattern.isActive() && pattern.getSubCategories().iterator().next().isActive()) {
                                patternIds.add(pattern.getId().toString());
                            }
                        });
                        if (patternIds.size() > 0) {
                            innerFacet.setQueryValues(patternIds);
                            return innerFacet;
                        }
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());


    }

    @SuppressWarnings("unchecked")
    public FacetPage<AggregationCountItemDTO<RegulationDto>> getRegulationsFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("Get Regulation File Counts With File Filters");
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        List<Regulation> regulations = regulationRepository.findAll();
        List<SolrFacetQueryJson> jsonFacets = buildJsonFacets(regulations);
        if (jsonFacets.isEmpty()) {
            return new FacetPage<>(new ArrayList<AggregationCountItemDTO<RegulationDto>>(), dataSourceRequest.createPageRequest());
        }

        SolrFacetQueryJsonResponse queryResponseNoFilter = null;
        SolrFacetQueryJsonResponse queryResponse = fileCatService.runJsonFacetQuery(dataSourceRequest, jsonFacets);
        if (dataSourceRequest.hasFilter() && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            jsonFacets = buildJsonFacets(regulations);
            queryResponseNoFilter = fileCatService.runJsonFacetQuery(dataSourceRequest, jsonFacets);
        }

        List<AggregationCountItemDTO<RegulationDto>> ansRegulations = Lists.newArrayList();
        for (Regulation regulation : regulations) {
            if (regulation.isActive()) {
                String regulationIdKey = CatFileFieldType.REGULATION_ID.getSolrName() + regulation.getId();
                SolrFacetJsonResponseBucket regulationResultObject = queryResponse.getResponseBucket(regulationIdKey);
                RegulationDto dto = convertRegulationToDto(regulation, false);
                if (!regulationResultObject.isEmpty()) {
                    Long bucketCount = regulationResultObject.getLongCountNumber();
                    if (bucketCount > 0) {
                        AggregationCountItemDTO<RegulationDto> item =
                                new AggregationCountItemDTO<>(dto, bucketCount.intValue());
                        ansRegulations.add(item);
                        if (queryResponseNoFilter != null) {
                            regulationResultObject = queryResponseNoFilter.getResponseBucket(regulationIdKey);

                            if (!regulationResultObject.isEmpty()) {
                                Long noFilterBucketCount = regulationResultObject.getLongCountNumber();
                                if (noFilterBucketCount > 0) {
                                    item.setCount2(noFilterBucketCount.intValue());
                                }
                            }
                        }
                    }
                }
            }
        }
        List<AggregationCountItemDTO<RegulationDto>> sortedAnsRegulations = ansRegulations.stream()
                .sorted((r1, r2) -> (int) (r2.getCount() - r1.getCount())).collect(toList());
        String selectItemId = dataSourceRequest.getSelectedItemId();
        return  ControllerUtils.getRelevantResultPage(
                sortedAnsRegulations, pageRequest, selectItemId, this::getRegulationIdStr);
    }

    private static RegulationDto convertRegulationToDto(Regulation regulation, Boolean getFullRegulation) {
        RegulationDto regulationDto = new RegulationDto();
        regulationDto.setId(regulation.getId());
        regulationDto.setName(regulation.getName());
        regulationDto.setActive(regulation.isActive());
        if (getFullRegulation) {
            regulationDto.setPatterns(ClaFileSearchPatternService.convertTextSearchPatternsToDto(new ArrayList<>(regulation.getPatterns())));
        }
        return regulationDto;
    }

    private static List<RegulationDto> convertRegulationsToDto(List<Regulation> regulations, Boolean getFullRegulation) {
        List<RegulationDto> result = new ArrayList<>();
        for (Regulation regulation : regulations) {
            RegulationDto regulationDto = convertRegulationToDto(regulation, getFullRegulation);
            result.add(regulationDto);
        }
        return result;
    }

    private static Page<RegulationDto> convertRegulationsToDto(Page<Regulation> regulations, Boolean getFullRegulation) {
        List<RegulationDto> content = convertRegulationsToDto(regulations.getContent(), getFullRegulation);
        Pageable pageable = PageRequest.of(regulations.getNumber(), regulations.getSize());
        return new PageImpl<>(content, pageable, regulations.getTotalElements());
    }

    public static Set<RegulationDto> collectRegulations(Collection<Regulation> regulations) {
        Set<RegulationDto> regulationsDtos = new HashSet<>();
        if (regulations == null) return regulationsDtos;
        for (Regulation regulation : regulations) {
            if (regulation == null) {
                continue;
            }
            RegulationDto regulationDto = convertRegulationToDto(regulation, false);
            regulationsDtos.add(regulationDto);
        }
        return regulationsDtos;
    }
}
