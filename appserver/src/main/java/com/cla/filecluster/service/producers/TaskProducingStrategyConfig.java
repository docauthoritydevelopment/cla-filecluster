package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.mediaproc.StrategyType;

import java.util.concurrent.TimeUnit;

public class TaskProducingStrategyConfig {
    private StrategyType selectedStrategy;
    private int ingestTasksQueueLimit;
    private int analyzeTasksQueueLimit;
    private long timeoutGraceSec  = TimeUnit.HOURS.toSeconds(1);;
    private JobType exclusiveMode = JobType.INGEST;
    private int cooldownTasksThreshold = 10;
    private long highThreshold;
    private long lowThreshold;
    private int percentageThreshold;

    public StrategyType getSelectedStrategy() {
        return selectedStrategy;
    }

    public void setSelectedStrategy(StrategyType selectedStrategy) {
        this.selectedStrategy = selectedStrategy;
    }

    public int getIngestTasksQueueLimit() {
        return ingestTasksQueueLimit;
    }

    public void setIngestTasksQueueLimit(int ingestTasksQueueLimit) {
        this.ingestTasksQueueLimit = ingestTasksQueueLimit;
    }

    public int getAnalyzeTasksQueueLimit() {
        return analyzeTasksQueueLimit;
    }

    public void setAnalyzeTasksQueueLimit(int analyzeTasksQueueLimit) {
        this.analyzeTasksQueueLimit = analyzeTasksQueueLimit;
    }

    public long getTimeoutGraceSec() {
        return timeoutGraceSec;
    }

    public void setTimeoutGraceSec(long timeoutGraceSec) {
        this.timeoutGraceSec = timeoutGraceSec;
    }

    public JobType getExclusiveMode() {
        return exclusiveMode;
    }

    public void setExclusiveMode(JobType exclusiveMode) {
        this.exclusiveMode = exclusiveMode;
    }

    public int getCooldownTasksThreshold() {
        return cooldownTasksThreshold;
    }

    public void setCooldownTasksThreshold(int cooldownTasksThreshold) {
        this.cooldownTasksThreshold = cooldownTasksThreshold;
    }

    public long getHighThreshold() {
        return highThreshold;
    }

    public void setHighThreshold(long highThreshold) {
        this.highThreshold = highThreshold;
    }

    public long getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(long lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public int getPercentageThreshold() {
        return percentageThreshold;
    }

    public void setPercentageThreshold(int percentageThreshold) {
        this.percentageThreshold = percentageThreshold;
    }
}
