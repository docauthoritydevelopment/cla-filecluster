package com.cla.filecluster.service.export.exporters.file.list_as_files;

import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.GenericFileExporter;
import com.cla.filecluster.service.export.exporters.file.list_as_files.mix_in.ClaFileDtoMixIn;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.list_as_files.AsFilesHeaderFields.*;

@Slf4j
@Component
public class FileListAsFilesExporter extends PageCsvExcelExporter<ClaFileDto> {

    @Autowired
    private GenericFileExporter genericFileExporter;

    private String[] header = {TYPE, FILE_NAME, PATH, FILE_SIZE, CREATION_DATE,
            MODIFIED_DATE, ACCESSED_DATE, OWNER, GROUP_NAME};

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ClaFileDto.class, ClaFileDtoMixIn.class);
    }

    @Override
    protected Page<ClaFileDto> getData(Map<String, String> requestParams) {
        return genericFileExporter.getData(requestParams);
    }

	@Override
	protected Map<String, Object> addExtraFields(ClaFileDto base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI_DEEP;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return ExportType.DISCOVER_RIGHT_AS_FILES.equals(exportType);
    }
}
