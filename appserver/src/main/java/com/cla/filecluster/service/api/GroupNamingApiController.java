package com.cla.filecluster.service.api;

import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;
import com.cla.filecluster.jobmanager.service.CoordinationLockDeniedException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.naming.GroupNamingService;
import com.cla.filecluster.service.util.ValidationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by uri on 23/11/2016.
 */
@RestController
@RequestMapping("/api/groupsnaming")
public class GroupNamingApiController {

    @Autowired
    private GroupNamingService groupNamingService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/reindexTitles/old", method = RequestMethod.POST)
    public void reindexTitlesOld(@RequestParam(defaultValue = "true") final boolean deleteBeforeProposal) {
        Long jobId = null;
        groupNamingService.reindexTitlesOld(deleteBeforeProposal, jobId);
    }

    @RequestMapping(value = "/proposeGroupsNames/pages", method = {RequestMethod.POST, RequestMethod.GET})
    // Todo: should be only POST
    public void fullGroupNamingByGroups(@RequestParam(defaultValue = "false") final boolean deleteBeforeProposal,
                                        @RequestParam(defaultValue = "true") final boolean noRename,
                                        @RequestParam(defaultValue = "-1") final double skipTermsThreshold,
                                        @RequestParam(defaultValue = "false") final boolean indexOnly) {

        Long jobId = null;
        ProgressTracker progressTracker = new DummyProgressTracker();
        try {
            groupNamingService.fullGroupNamingByGroups(
                    deleteBeforeProposal, noRename, skipTermsThreshold, indexOnly, jobId, progressTracker);
        } catch (CoordinationLockDeniedException e) {
            throw new RuntimeException("fullGroupNamingByGroups failed due to job coordination lock denial:", e);
        }
    }

    @RequestMapping(value = "/proposeGroupsNames/all", method = {RequestMethod.POST, RequestMethod.GET})
    // Todo: should be only POST
    public void proposeGroupsNames(@RequestParam(defaultValue = "true") final boolean deleteBeforeProposal, @RequestParam(defaultValue = "-1") final double skipTermsThreshold) {
        Long jobId = null;
        groupNamingService.proposeGroupsNames(deleteBeforeProposal,skipTermsThreshold, jobId);
    }

    @RequestMapping(value = "/nameAGroup/{id}", method = {RequestMethod.POST, RequestMethod.GET})
    public void nameAGroup(@PathVariable("id") String groupId) {
        ValidationUtils.validateStringNotEmpty(groupId, messageHandler.getMessage("group.id.empty"));
        groupNamingService.nameAGroup(groupId);
    }
}
