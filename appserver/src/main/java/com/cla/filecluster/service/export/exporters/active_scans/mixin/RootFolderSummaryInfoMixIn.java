package com.cla.filecluster.service.export.exporters.active_scans.mixin;

import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public abstract class RootFolderSummaryInfoMixIn {

    @JsonUnwrapped
    private CrawlRunDetailsDto crawlRunDetailsDto;

    @JsonUnwrapped
    private RootFolderDto rootFolderDto;

    @JsonUnwrapped
    public abstract ScheduleGroupDto getScheduleGroupDto();
}
