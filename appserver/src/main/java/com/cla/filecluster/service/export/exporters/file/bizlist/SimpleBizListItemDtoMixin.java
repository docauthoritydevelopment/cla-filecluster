package com.cla.filecluster.service.export.exporters.file.bizlist;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class SimpleBizListItemDtoMixin {

    @JsonProperty(BizListHeaderFileds.BUSINESS_ID)
    private String businessId;

    @JsonProperty(BizListHeaderFileds.NAME)
    private String name;

    @JsonProperty(BizListHeaderFileds.TYPE)
    private BizListItemType type;

    @JsonProperty(BizListHeaderFileds.LIST)
    private String bizListName;
}
