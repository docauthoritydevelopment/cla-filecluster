package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.common.domain.dto.action.ActionClass;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class ActionHelperImplementation implements ActionHelper {

    private ActionsAppService actionsAppService;

    private Path actionResultsFolder;

    private Logger logger = LoggerFactory.getLogger(ActionHelperImplementation.class);

    public ActionHelperImplementation(ActionsAppService actionsAppService) {
        this.actionsAppService = actionsAppService;
        String actionResultsFolder = actionsAppService.getActionResultsFolder();
        this.actionResultsFolder = Paths.get(actionResultsFolder).toAbsolutePath().normalize();
    }

    private String actionIdFolderNameFormatter(long actionTriggerId) {
        return String.format("%1$04d", actionTriggerId);
    }

    @Override
    public File getActionClassReportsFolder(long actionTriggerId, ActionClass actionClass) {
        File file = actionsAppService.isActionResultsFolderSeparateById() ?
                (new File(actionResultsFolder.toString(), actionIdFolderNameFormatter(actionTriggerId))) :
                (new File(actionResultsFolder.toString()));
        file.mkdirs();
        return file;
    }

    @Override
    public File getActionClassScriptsFolder(ActionExecutionTaskStatusDto actionTrigger, ActionClass actionClass) {
        File file;
        if (actionTrigger.getActionParams() != null && actionTrigger.getActionParams().getProperty(ActionRunner.ACTION_RESULT_PATH) != null) {
            String path = actionTrigger.getActionParams().getProperty(ActionRunner.ACTION_RESULT_PATH);
            file = new File(path);
        } else {
            file = actionsAppService.isActionResultsFolderSeparateById() ?
                    (new File(actionResultsFolder.toString(), actionIdFolderNameFormatter(actionTrigger.getActionTriggerId()))) :
                    (new File(actionResultsFolder.toString()));
        }

        file.mkdirs();
        return file;
    }

    @Override
    public Stream<Path> getAllActionClassFolders(long actionTriggerId) {
        // ???
        List<Path> result = new ArrayList<>();
        if (actionsAppService.isActionResultsFolderSeparateById()) {
            result.add(actionResultsFolder.resolve(actionIdFolderNameFormatter(actionTriggerId)));
        }
        else {
            result.add(actionResultsFolder);
        }

        return result.stream();
    }

    @Override
    public String extractSingleParameterIfExists(String parameterName, Properties actionParameters, String actionId, String path) {
        String parameter = actionParameters.getProperty(parameterName);
        return parameter;
    }

    @Override
    public String extractSingleParameter(String parameterName, Properties actionParameters, String actionId, String path) {
        String parameter = actionParameters.getProperty(parameterName);
        if (StringUtils.isEmpty(parameter)) {
            parameter = actionParameters.getProperty(PARAMETER_1);
        }
        if (StringUtils.isEmpty(parameter)) {
//            String path = actionParameters.getProperty("path");
            StringJoiner stringJoiner = new StringJoiner(",");
            actionParameters.forEach((k,v) -> stringJoiner.add(k.toString()));
            logger.warn("Illegal {} action request on path {}. Missing paramter [{}]. Existing parameters: [{}]",actionId,path,parameterName,stringJoiner.toString());
            throw new RuntimeException("Illegal "+actionId+" action request on path "+path+". Missing parameter ["+parameterName+"]");
        }
        return parameter;
    }

    @Override
    public List<String> extractParameterValues(String parameterName, Properties actionParameters, String actionId, String path) {
        String parameter = extractSingleParameter(parameterName, actionParameters, actionId, path);
        parameter = parameter.replace("[", "");
        parameter = parameter.replace("]", "");
        String[] split = StringUtils.split(parameter, ',');
        return Arrays.asList(split);
    }

    @Override
    public String getBackupFolder() {
        return actionsAppService.getDefaultBackupFolder();
    }

    @Override
    public Map<String, Object> getActionExecutorProperties(String actionExecutorName) {
        return actionsAppService.getActionExecutorProperties(actionExecutorName);
    }
}
