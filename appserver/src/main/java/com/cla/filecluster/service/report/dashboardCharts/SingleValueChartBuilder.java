package com.cla.filecluster.service.report.dashboardCharts;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.report.DashboardChartDtoBuilder;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SingleValueChartBuilder implements DashboardChartDtoBuilder {

    private static final String TOTAL_QUERY = "Total";
    private static final String FILTERED_QUERY = "Filter";

    @Override
    public ChartType getType() {
        return ChartType.SINGLE_VALUE_CHART;
    }

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;


    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;


    public String getAuditMsg(Map<String, String> params) {
        CatFileFieldType groupedField = dashboardChartsUtil.extractGroupedField(params);
        String queryPrefix = dashboardChartsUtil.extractPrefixField(params);
        return "Report " +
                dashboardChartsUtil.translateSolrFieldName(groupedField, queryPrefix).toLowerCase();
    }


    private DashboardChartSeriesDataDto createEmptySeries(String seriesName) {
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(seriesName);
        ArrayList<Double> dataList = new ArrayList<>();
        dataList.add(0d);
        seriesObj.setData(dataList);
        return seriesObj;
    }


    private DashboardChartSeriesDataDto parseResponseToDashboardChartSeriesDataDto(DashboardChartDataDto dashboardChartDataDto, SolrFacetJsonResponseBucket bucketBasedFacets,
                                                                                   String seriesName,
                                                                                   SolrCoreSchema field,
                                                                                   DashboardChartValueTypeDto valueField,
                                                                                   String prefix) {
        List<SolrFacetJsonResponseBucket> buckets = bucketBasedFacets.getBuckets();
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(seriesName);
        ArrayList<Double> dataList = new ArrayList<>();
        if (!field.equals(CatFileFieldType.FILE_ID)) {  //in case the preffix is not null we are using different method for getting the number of buckets (there is also different behavior at the query creator )
            Double countValue = DashboardChartsUtil.getResponseDoubleValueFromObject(bucketBasedFacets.getTotalNumOfBuckets());
            countValue = dashboardChartsUtil.calculateAccurateCountValue(dashboardChartDataDto, countValue, bucketBasedFacets, valueField);
            dataList.add(countValue);
        } else if (buckets.size() == 0) {  //in case we are not getting results (there are 0 results)
            dataList.add(0d);
        } else {
            Double countValue = 0d;
            for (SolrFacetJsonResponseBucket bucket : buckets) {
                boolean isDeleted = bucket.getBooleanVal();
                if (!isDeleted) {
                    if (valueField.getSolrFunction().equals(FacetFunction.COUNT) && field.equals(CatFileFieldType.FILE_ID)) {
                        countValue = bucket.getDoubleCountNumber();
                    } else {
                        countValue = bucket.getDoubleValueByKey(field.getSolrName());
                    }
                }
            }
            dataList.add(countValue);
        }
        seriesObj.setData(dataList);
        return seriesObj;
    }

    /**
     * @param solrSpecification - the main solr specifications
     * @param field             - the needed SOLR field which the query is accordingly
     * @param queryPrefix       - used if we want to search for results with specific prefix
     * @param valueField        - used for define what the calculate need to be accordingly
     * @param filterField       - optional - can be use to define specific filter
     * @return SolrFacetQueryJson - return solr json facet object
     */
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private SolrFacetQueryJson buildQueryJsonFacet(SolrSpecification solrSpecification,
                                                   SolrCoreSchema field, String queryPrefix,
                                                   DashboardChartValueTypeDto valueField,
                                                   FilterDescriptor filterField, String queryName) {

        List<String> filterStrList = new ArrayList<>();
        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(queryName);
        theFacet.setType(FacetType.TERMS);
        if (filterField != null && (filterField.getValue() != null || !filterField.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filterField, solrSpecification));
        }
        if (!filterStrList.isEmpty()) {
            // this replace is becuase the JSON facet  domain filter need double slash
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }


        theFacet.setField(CatFileFieldType.DELETED);
        if (!field.equals(CatFileFieldType.FILE_ID)) {
            theFacet.setField(field);
            theFacet.setPrefix(queryPrefix);
            theFacet.setNumBuckets(true);
            theFacet.setLimit(dashboardChartsUtil.getLowerBoundFacetCountHll());
        } else if (valueField.getSolrField().equals(CatFileFieldType.SIZE)) {
            SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(field.getSolrName());
            innerFacet.setType(FacetType.SIMPLE);
            innerFacet.setField(CatFileFieldType.SIZE);
            innerFacet.setFunc(FacetFunction.SUM);
            theFacet.addFacet(innerFacet);
        }
        return theFacet;
    }

    /**
     * @param params - request query params
     */
    public DashboardChartDataDto buildChartDataDto(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        FilterDescriptor filterField = dashboardChartsUtil.extractFilterField(params);
        CatFileFieldType groupedField = dashboardChartsUtil.extractGroupedField(params);
        DashboardChartValueTypeDto valueField = dashboardChartsUtil.extractChartValueField(params);
        boolean compareToTotal = dashboardChartsUtil.extractCompareToTotal(params);
        String queryPrefix = dashboardChartsUtil.extractPrefixField(params);
        String firstQueryName = (filterField != null ? filterField.getName() : TOTAL_QUERY);
        ArrayList<DashboardChartSeriesDataDto> series = new ArrayList<>();
        DashboardChartDataDto dashboardChartDataDto = new DashboardChartDataDto();

        if (valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            dashboardChartDataDto.setCategories(Collections.singletonList(dashboardChartsUtil.
                    translateSolrFieldName(groupedField, queryPrefix)));
        } else {
            dashboardChartDataDto.setCategories(Collections.singletonList(dashboardChartsUtil.
                    translateSolrSizeFieldName(groupedField, queryPrefix)));
        }

        if (compareToTotal && !Objects.isNull(filterField) && filterField.containsContentFilter()) {
            SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                    dataSourceRequest);

            solrFacetSpecification.addFacetJsonObject(buildQueryJsonFacet(solrFacetSpecification,
                    groupedField, queryPrefix, valueField, filterField, FILTERED_QUERY));

            SolrFacetQueryJsonResponse queryResponse = solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);

            SolrFacetJsonResponseBucket bucketBasedFacets = queryResponse.getResponseBucket(FILTERED_QUERY);
            if (!bucketBasedFacets.isEmpty()) {
                series.add(parseResponseToDashboardChartSeriesDataDto(dashboardChartDataDto, bucketBasedFacets,
                        firstQueryName, groupedField, valueField, queryPrefix));
            } else {
                series.add(createEmptySeries(firstQueryName));
            }

            SolrSpecification totalSolrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                    dataSourceRequest);

            totalSolrFacetSpecification.addFacetJsonObject(buildQueryJsonFacet(totalSolrFacetSpecification,
                    groupedField, queryPrefix, valueField, null, TOTAL_QUERY));

            SolrFacetQueryJsonResponse totalQueryResponse = solrFileCatRepository.runFacetQueryJson(totalSolrFacetSpecification);
            SolrFacetJsonResponseBucket totalBucketBasedFacets = totalQueryResponse.getResponseBucket(TOTAL_QUERY);
            if (!totalBucketBasedFacets.isEmpty()) {
                series.add(parseResponseToDashboardChartSeriesDataDto(dashboardChartDataDto, totalBucketBasedFacets, TOTAL_QUERY,
                        groupedField, valueField, queryPrefix));
            } else {
                series.add(createEmptySeries(TOTAL_QUERY));
            }
        } else {
            // Creating the first query that run the facet according to the first segment
            SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                    dataSourceRequest);

            solrFacetSpecification.addFacetJsonObject(buildQueryJsonFacet(solrFacetSpecification,
                    groupedField, queryPrefix, valueField, filterField, FILTERED_QUERY));

            if (compareToTotal) {
                solrFacetSpecification.addFacetJsonObject(buildQueryJsonFacet(solrFacetSpecification,
                        groupedField, queryPrefix, valueField, null, TOTAL_QUERY));
            }

            SolrFacetQueryJsonResponse queryResponse = solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);
            SolrFacetJsonResponseBucket bucketBasedFacets = queryResponse.getResponseBucket(FILTERED_QUERY);
            if (!bucketBasedFacets.isEmpty()) {
                series.add(parseResponseToDashboardChartSeriesDataDto(dashboardChartDataDto, bucketBasedFacets,
                        firstQueryName, groupedField, valueField, queryPrefix));
            } else {
                series.add(createEmptySeries(firstQueryName));
            }

            if (compareToTotal) {
                bucketBasedFacets = queryResponse.getResponseBucket(TOTAL_QUERY);
                if (!bucketBasedFacets.isEmpty()) {
                    series.add(parseResponseToDashboardChartSeriesDataDto(dashboardChartDataDto, bucketBasedFacets, TOTAL_QUERY,
                            groupedField, valueField, queryPrefix));
                } else {
                    series.add(createEmptySeries(TOTAL_QUERY));
                }
            }
        }

        dashboardChartDataDto.setSeries(series);
        return dashboardChartDataDto;
    }
}
