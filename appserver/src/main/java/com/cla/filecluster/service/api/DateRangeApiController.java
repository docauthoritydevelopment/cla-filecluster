package com.cla.filecluster.service.api;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.common.domain.dto.date.DateRangePartitionDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.date.DateRangeAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by uri on 05/06/2016.
 */
@RestController
@RequestMapping("/api/daterange")
public class DateRangeApiController {

    @Autowired
    private DateRangeAppService dateRangeAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/partitions", method = RequestMethod.GET)
    public Page<DateRangePartitionDto> listDateRangePartitions(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return dateRangeAppService.listDateRangePartitions(dataSourceRequest,false);
    }

    @RequestMapping(value = "/partitions/full", method = RequestMethod.GET)
    public Page<DateRangePartitionDto> listFullDateRangePartitions(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return dateRangeAppService.listDateRangePartitions(dataSourceRequest, true);
    }

    @RequestMapping(value = "/partitions/{partitionId}/items", method = RequestMethod.GET)
    public DateRangePartitionDto listDateRangePartitionItems(@PathVariable long partitionId, @RequestParam Map<String,String> params) {
        return dateRangeAppService.listDateRangePartitionItems(partitionId);
    }

    @RequestMapping(value = "/partitions", method = RequestMethod.PUT)
    public DateRangePartitionDto createDatePartition(@RequestBody DateRangePartitionDto dateRangePartitionDto) {
        DateRangePartitionDto dateRangePartition = dateRangeAppService.createDateRangePartition(dateRangePartitionDto);
        eventAuditService.audit(AuditType.DATE_PARTITION,"Create date partition", AuditAction.CREATE, DateRangePartitionDto.class,dateRangePartitionDto.getId(),dateRangePartitionDto);
        return dateRangePartition;
    }


    @RequestMapping(value = "/partitions/{partitionId}", method = RequestMethod.POST)
    public DateRangePartitionDto updateDateRangePatition(@PathVariable long partitionId, @RequestBody DateRangePartitionDto dateRangePartitionDto) {
        DateRangePartitionDto dateRangePartitionDto1 = dateRangeAppService.updateDateRangePartition(partitionId, dateRangePartitionDto);
        eventAuditService.audit(AuditType.DATE_PARTITION,"Update date partition", AuditAction.UPDATE, DateRangePartitionDto.class,dateRangePartitionDto.getId(),dateRangePartitionDto);
        return dateRangePartitionDto1;
    }

    @RequestMapping(value = "/partitions/{partitionId}", method = RequestMethod.DELETE)
    public void deleteDateRangePartition(@PathVariable long partitionId) {
        DateRangePartitionDto dateRangePartition = getDateRangePartition(partitionId);
        if (dateRangePartition.isSystem()) {
            throw new BadRequestException(messageHandler.getMessage("date-range-partition.delete-default"), BadRequestType.OPERATION_FAILED);
        }
        dateRangeAppService.deleteDateRangePartition(partitionId);
        eventAuditService.audit(AuditType.DATE_PARTITION,"Delete date partition", AuditAction.DELETE, DateRangePartitionDto.class,dateRangePartition.getId(),dateRangePartition);
    }

    @RequestMapping(value="/partitions/{partitionId}/filecount/lastmodified")
    public FacetPage<AggregationCountItemDTO<DateRangeItemDto>> findDateRangeLastModifiedFileCount(@PathVariable long partitionId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover file last modified dates export", AuditAction.VIEW, DateRangeItemDto.class, partitionId, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover file last modified dates", AuditAction.VIEW,  DateRangeItemDto.class,partitionId, dataSourceRequest);
        }
        return dateRangeAppService.findDateRangeFileCount(CatFileFieldType.LAST_MODIFIED,partitionId,dataSourceRequest);
    }

    @RequestMapping(value="/partitions/{partitionId}/filecount/created")
    public FacetPage<AggregationCountItemDTO<DateRangeItemDto>> findDateRangeCreatedFileCount(@PathVariable long partitionId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover file creation dates export", AuditAction.VIEW, DateRangeItemDto.class, partitionId, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover file creation dates", AuditAction.VIEW, DateRangeItemDto.class, partitionId, dataSourceRequest);
        }
        return dateRangeAppService.findDateRangeFileCount(CatFileFieldType.CREATION_DATE, partitionId, dataSourceRequest);
    }

    @RequestMapping(value="/partitions/{partitionId}/filecount/lastaccess")
    public FacetPage<AggregationCountItemDTO<DateRangeItemDto>> findDateRangeLastAccessFileCount(@PathVariable long partitionId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover file last access dates export", AuditAction.VIEW, DateRangeItemDto.class, partitionId, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover file last access dates", AuditAction.VIEW,  DateRangeItemDto.class, partitionId, dataSourceRequest);
        }
        return dateRangeAppService.findDateRangeFileCount(CatFileFieldType.LAST_ACCESS,partitionId,dataSourceRequest);
    }


    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public DateRangePartitionDto getDateRangePartition(@PathVariable Long id) {
        DateRangePartitionDto dateRangePartition = dateRangeAppService.getDateRangePartition(id);
        if (dateRangePartition == null) {
            throw new BadRequestException(messageHandler.getMessage("date-range-partition.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dateRangePartition;
    }
}
