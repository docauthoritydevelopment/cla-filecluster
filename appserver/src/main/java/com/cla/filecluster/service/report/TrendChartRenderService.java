package com.cla.filecluster.service.report;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.NamedIdentifiableDto;
import com.cla.common.domain.dto.report.TrendChartRecordedValue;
import com.cla.common.domain.dto.report.TrendChartResolutionDto;
import com.cla.common.domain.dto.report.TrendChartSeriesDto;
import com.cla.filecluster.domain.entity.report.TrendChart;
import com.cla.filecluster.domain.entity.report.TrendChartData;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.convertion.ModelConversionService;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by uri on 03/08/2016.
 */
@Service
public class TrendChartRenderService {

    private static Logger logger = LoggerFactory.getLogger(TrendChartRenderService.class);

    @Autowired
    private TrendChartService trendChartService;

    @Autowired
    private ScriptEngine javaScriptEngine;

    public static final String UNIQUE_VALUE_PREFIX = "##";

    @Autowired
    ModelConversionService modelConversionService;


    public Map<String, TrendChartSeriesDto> renderTrendChartData(TrendChart trendChart, TrendChart collectorTrendChart, List<TrendChartData> trendChartDataList,
                                                                 Long startTime, Long endTime, TrendChartResolutionDto tcResolutionDto) {
        return recentSampleAggregationRenderer(trendChart, collectorTrendChart, trendChartDataList, startTime, endTime, tcResolutionDto);
    }

    private Map<String, TrendChartSeriesDto> recentSampleAggregationRenderer(TrendChart trendChart, TrendChart collectorTrendChart, List<TrendChartData> trendChartDataList,
                                                                             Long startTime, Long endTime, TrendChartResolutionDto tcResolutionDto) {
        if (trendChartDataList == null) {
            return new HashMap<>();
        }
        // Shift the range by a single resolution period so that the aggregator will get the values at the end of each partition.
//        startTime += viewResolutionMs;
//        endTime += viewResolutionMs;
        startTime = tcResolutionDto.calcPeriodStartMs(startTime, 1);
        endTime = tcResolutionDto.calcPeriodStartMs(endTime, 1);

        int dataSampleCount = trendChartDataList.size();
//        int viewSampleCount = (int) ((endTime-startTime) / viewResolutionMs + 1);
        int viewSampleCount = tcResolutionDto.calcSampleCount(startTime, endTime);
        logger.debug("Render trend chart {}. Times (shifted): {}-{} at res:{} . Data ({} real samples, {} view samples)",
                trendChart, startTime, endTime, tcResolutionDto, dataSampleCount, viewSampleCount);

        Map<String, TrendChartSeriesDto> trendChartSeriesDtoMap = new HashMap<>();
//        long time = startTime;
        int dataSampleLocation = 0;
        TrendChartData trendChartData = null;
        boolean facetCollector = !trendChartService.isFilesCollector(collectorTrendChart.getSeriesType());
        FilterDescriptor[] trendChartFilters = trendChartService.getTrendChartFilters(collectorTrendChart);
        for (int viewSampleLocation = 0; viewSampleLocation < viewSampleCount; viewSampleLocation++) {
            long time = tcResolutionDto.calcPeriodStartMs(startTime, viewSampleLocation);

            //Find the most updated trend chart point. The dataSampleLocation will point after it
            while (dataSampleLocation < trendChartDataList.size()
                    && trendChartDataList.get(dataSampleLocation).getRequestedTimeStamp() <= time) {
                trendChartData = trendChartDataList.get(dataSampleLocation);
                dataSampleLocation++;
            }
            //dataSampleLocation is now pointing to the future
            if (trendChartData != null) {
                renderTrendChartSample(trendChartFilters, viewSampleLocation, viewSampleCount, trendChartSeriesDtoMap, trendChartData, trendChart.getTransformerScript(), facetCollector);
            } else {
                // TODO: missing points should be populated with sample values equal to '0'.
                // Make sure this is done during final DTO conversion
            }
//            time+=viewResolutionMs;
        }
        if (trendChartData == null && dataSampleCount > 0) {
            logger.warn("Render of trendChart {} produced no view points ({}) between {}-{}", trendChart, viewSampleCount, startTime, endTime);
//            time = startTime;
            for (int viewSampleLocation = 0; viewSampleLocation < viewSampleCount; viewSampleLocation++) {
                long time = tcResolutionDto.calcPeriodStartMs(startTime, viewSampleLocation);
                logger.debug("View point: {}", time);
//                time+=viewResolutionMs;
            }
            for (TrendChartData chartData : trendChartDataList) {
                logger.debug("Input chartData: {}", chartData);
            }
        }
        if (facetCollector) {
            logger.debug("Render of trendChart {} produced {} series", trendChart.getId(), trendChartSeriesDtoMap.size());
        }
        return trendChartSeriesDtoMap;
    }

    private void renderTrendChartSample(FilterDescriptor[] trendChartFilters, int viewSampleLocation, int viewSampleCountInt, Map<String, TrendChartSeriesDto> trendChartSeriesDtoMap, TrendChartData trendChartData, String transformerScript, boolean facetCollector) {
        TrendChartRecordedValue[] trendChartRecordedValues = convertJsonToTrendChartRecordedValues(trendChartData.getValue());
        Map<Integer, String> filterNames = getFilterNames(trendChartFilters, trendChartRecordedValues);
        if (transformerScript != null) {
            if (facetCollector) {
                trendChartRecordedValues = transformFacetSamples(filterNames, transformerScript, trendChartRecordedValues);
            } else {
                trendChartRecordedValues = transformSample(filterNames, transformerScript, trendChartRecordedValues);
            }
        }
        for (TrendChartRecordedValue trendChartRecordedValue : trendChartRecordedValues) {
            //Transform the filterIndex to a filter Name if possible
            String filterName = filterNames.get(trendChartRecordedValue.getFilterIndex());
            TrendChartSeriesDto trendChartSeriesDto = getOrCreateTrendChartSeriesDto(filterName, trendChartSeriesDtoMap, trendChartRecordedValue, viewSampleCountInt);
            trendChartSeriesDto.getValues()[viewSampleLocation] = trendChartRecordedValue.getValue();
        }
    }

    private Map<Integer, String> getFilterNames(FilterDescriptor[] trendChartFilters, TrendChartRecordedValue[] trendChartRecordedValues) {
        Map<Integer, String> result = new HashMap<>();
        for (TrendChartRecordedValue trendChartRecordedValue : trendChartRecordedValues) {
            int filterIndex = trendChartRecordedValue.getFilterIndex();
            FilterDescriptor trendChartFilter = trendChartFilters[filterIndex];
            if (trendChartFilter != null && trendChartFilter.getName() != null) {
                result.put(filterIndex, trendChartFilter.getName());
            }
        }
        return result;
    }

    private TrendChartRecordedValue[] transformFacetSamples(Map<Integer, String> filterNames, String transformerScript, TrendChartRecordedValue[] trendChartRecordedValues) {
        //Populate the input map
        Map<String, ? extends Object> input = createFacetInputMap(filterNames, trendChartRecordedValues);
        Map<String, Object> output = runTransformScript(transformerScript, input);
        //Transform the results using the transformer script
        List<TrendChartRecordedValue> transformedSampleValues = new LinkedList<>();
        //Convert the output back to a valid result set
        int i = 0;
        for (Map.Entry<String, Object> outputEntry : output.entrySet()) {
            filterNames.put(i, outputEntry.getKey());
            Map<String, Object> facetValues = (Map<String, Object>) outputEntry.getValue();
            for (Map.Entry<String, Object> facetValuesEntry : facetValues.entrySet()) {
                String id = facetValuesEntry.getKey();
                Number value = (Number) facetValuesEntry.getValue();
                transformedSampleValues.add(new TrendChartRecordedValue(id, value.doubleValue(), i));
            }
            i++;
        }
        return transformedSampleValues.toArray(new TrendChartRecordedValue[transformedSampleValues.size()]);
    }


    private TrendChartRecordedValue[] transformSample(Map<Integer, String> filterNames, String transformerScript, TrendChartRecordedValue[] trendChartRecordedValues) {
        //Populate the input map
        Map<String, ? extends Object> input = createInputMap(filterNames, trendChartRecordedValues);
        Map<String, Object> output = runTransformScript(transformerScript, input);
        //Transform the results using the transformer script
        TrendChartRecordedValue[] transformedSampleValues = new TrendChartRecordedValue[output.keySet().size()];
        //Convert the output back to a valid result set
        int i = 0;
        for (Map.Entry<String, Object> outputEntry : output.entrySet()) {
            String id = UNIQUE_VALUE_PREFIX + outputEntry.getKey();
            transformedSampleValues[i] = new TrendChartRecordedValue(id, NumberUtils.toDouble(outputEntry.getValue().toString()), i);
            filterNames.put(i, outputEntry.getKey());
            i++;
        }
        return transformedSampleValues;
    }

    private Map<String, Object> runTransformScript(String transformerScript, Map<String, ? extends Object> input) {
        Map<String, Object> output = new HashMap<>();
        try {
            // define a different script context
            ScriptContext newContext = new SimpleScriptContext();
            newContext.setBindings(javaScriptEngine.createBindings(), ScriptContext.ENGINE_SCOPE);
            Bindings engineScope = newContext.getBindings(ScriptContext.ENGINE_SCOPE);
            engineScope.put("input", input);
            engineScope.put("output", output);
            javaScriptEngine.eval(transformerScript, engineScope);
        } catch (ScriptException e) {
            logger.warn("Failed to evaluate trendChart transformation script {}", transformerScript, e);
        }
        return output;
    }

    private Map<String, ? extends Object> createFacetInputMap(Map<Integer, String> filterNames, TrendChartRecordedValue[] trendChartRecordedValues) {
        Map<String, Map<String, Double>> input = new HashMap<>();
        for (TrendChartRecordedValue trendChartRecordedValue : trendChartRecordedValues) {
            Double value = Double.valueOf(trendChartRecordedValue.getValue());
            String id = trendChartRecordedValue.getId();
            String name = getInputMapValueName(filterNames, trendChartRecordedValue, id);

            Map<String, Double> facetValues = input.get(name);
            if (facetValues == null) {
                facetValues = new HashMap<>();
            }
            facetValues.put(id, value);
            input.put(name, facetValues);
        }
        return input;
    }

    private Map<String, Double> createInputMap(Map<Integer, String> filterNames, TrendChartRecordedValue[] trendChartRecordedValues) {
        Map<String, Double> input = new HashMap<>();
        for (TrendChartRecordedValue trendChartRecordedValue : trendChartRecordedValues) {
            Double value = Double.valueOf(trendChartRecordedValue.getValue());
            String id = trendChartRecordedValue.getId();
            String name = getInputMapValueName(filterNames, trendChartRecordedValue, id);
            input.put(name, value);
        }
        return input;
    }

    private String getInputMapValueName(Map<Integer, String> filterNames, TrendChartRecordedValue trendChartRecordedValue, String id) {
        String name = filterNames.get(trendChartRecordedValue.getFilterIndex());
        if (name == null) {
            if (id.startsWith(UNIQUE_VALUE_PREFIX)) {
                name = id.substring(UNIQUE_VALUE_PREFIX.length());
            } else {
                name = id;
//                name = String.valueOf(trendChartRecordedValue.getFilterIndex());
            }
        }
        if (name.contains(" ")) {
            name = name.replace(" ", "_");
        }
        return name;
    }

    private TrendChartSeriesDto getOrCreateTrendChartSeriesDto(String filterName, Map<String, TrendChartSeriesDto> trendChartSeriesDtoMap,
                                                               TrendChartRecordedValue trendChartRecordedValue,
                                                               int sampleCount) {
        String id = trendChartRecordedValue.getId();
        int filterIndex = trendChartRecordedValue.getFilterIndex();
        String filter = filterName == null ? String.valueOf(filterIndex) : filterName;
        String mapId = filterIndex + "." + id;
        TrendChartSeriesDto trendChartSeriesDto = trendChartSeriesDtoMap.get(mapId);
        if (trendChartSeriesDto == null) {
            trendChartSeriesDto = new TrendChartSeriesDto();
            trendChartSeriesDto.setId(id);
            if (id.startsWith(UNIQUE_VALUE_PREFIX)) {
                trendChartSeriesDto.setName(id.substring(UNIQUE_VALUE_PREFIX.length()));
            }
            trendChartSeriesDto.setFilter(filter);
            double[] values = new double[sampleCount];
            trendChartSeriesDto.setValues(values);
            trendChartSeriesDtoMap.put(mapId, trendChartSeriesDto);
        }
        return trendChartSeriesDto;
    }


    public void enrichTrendChartSeriesDtoList(Map<String, TrendChartSeriesDto> trendChartSeriesDtoMap, String seriesType) {
        if (trendChartService.isFilesCollector(seriesType)) {
            return;
        }
        CatFileFieldType catFileFieldType = FileDtoFieldConverter.convertToCatFileField(seriesType);
        List<String> collect = trendChartSeriesDtoMap.keySet().stream()
                .map(f -> extractSolrIdFromMapKey(f))
                .filter(f -> !f.startsWith(UNIQUE_VALUE_PREFIX))
                .collect(Collectors.toList());
        Map<String, NamedIdentifiableDto> namesByIds = modelConversionService.findNamesIdsBySolrIds(collect, catFileFieldType);
        for (TrendChartSeriesDto trendChartSeriesDto : trendChartSeriesDtoMap.values()) {
            NamedIdentifiableDto namedIdentifiableDto = namesByIds.get(trendChartSeriesDto.getId());
            if (namedIdentifiableDto != null) {
                trendChartSeriesDto.setName(namedIdentifiableDto.getName());
                trendChartSeriesDto.setId(namedIdentifiableDto.getId()); // Replace the solr id with the real id
            } else if (trendChartSeriesDto.getName() == null) {
                trendChartSeriesDto.setName(trendChartSeriesDto.getId()); //At least prevent nulls
            }
        }

    }

    private String extractSolrIdFromMapKey(String key) {
        int i = key.indexOf(".");
        if (i >= 0) {
            return key.substring(i + 1);
        }
        return key;
    }

    public static TrendChartRecordedValue[] convertJsonToTrendChartRecordedValues(String value) {
        try {
            return ModelDtoConversionUtils.getMapper().readValue(value, TrendChartRecordedValue[].class);
        } catch (IOException e) {
            logger.error("Failed to convert headers value map [" + value + "] to map", e);
            throw new RuntimeException("Failed to convert headers value map [" + value + "] to map", e);
        }
    }
}
