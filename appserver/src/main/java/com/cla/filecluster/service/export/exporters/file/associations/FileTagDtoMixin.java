package com.cla.filecluster.service.export.exporters.file.associations;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public abstract class FileTagDtoMixin {

    @JsonProperty(FileTagHeaderFields.NAME)
    private String name;

    @JsonProperty(FileTagHeaderFields.DESCRIPTION)
    private String description;

    @JsonProperty(FileTagHeaderFields.TYPE)
    public abstract String getTypeName();
}
