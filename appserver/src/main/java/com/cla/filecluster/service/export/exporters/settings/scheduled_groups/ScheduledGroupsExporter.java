package com.cla.filecluster.service.export.exporters.settings.scheduled_groups;

import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.common.domain.dto.schedule.ScheduleType;
import com.cla.filecluster.service.api.ScheduleApiController;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.settings.scheduled_groups.mixin.*;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.CaseFormat;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.scheduled_groups.ScheduledGroupsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
@Slf4j
@Component
public class ScheduledGroupsExporter extends PageCsvExcelExporter<ScheduleGroupSummaryInfoDto> {

    @Autowired
    private ScheduleApiController scheduleApiController;

    private NumberFormat formatter = new DecimalFormat("00");

    private final static String[] header = { NAME, SCHEDULE_DETAILS, ACTIVE, TYPE, DESCRIPTION, SCHEDULE_CRON, SCHEDULE_EVERY,
            SCHEDULE_HOUR, SCHEDULE_MINUTES, SCHEDULE_DAYS_OF_THE_WEEK, ROOT_FOLDERS};

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ScheduleGroupSummaryInfoDto.class, ScheduleGroupSummaryInfoDtoMixIn.class)
        .addMixIn(ScheduleGroupDto.class, ScheduleGroupDtoMixIn.class)
        .addMixIn(ScheduleConfigDto.class, ScheduleConfigDtoMixin.class);
    }

    @Override
    protected Iterable<ScheduleGroupSummaryInfoDto> getData(Map<String, String> requestParams) {
        return scheduleApiController.listScheduleGroupsSummaryInfo(requestParams);
    }

    @Override
    protected Map<String, Object> addExtraFields(ScheduleGroupSummaryInfoDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();

        ScheduleConfigDto config = base.getScheduleGroupDto().getScheduleConfigDto();

        String scheduleTypePart = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, config.getScheduleType().toString());

        String scheduleTimePart = ", every " + config.getEvery() + " " + (config.getScheduleType().equals(ScheduleType.DAILY) ?
                (config.getEvery() == 1 ? "day" : "days") : config.getScheduleType().equals(ScheduleType.HOURLY) ?
                (config.getEvery() == 1 ?  "hour" : "hours") : "week") + " at " + (
                        formatter.format(config.getHour())+":"+formatter.format(config.getMinutes()));

        String scheduleDaysOfWeekPart = "";
        if (config.getDaysOfTheWeek() != null && config.getDaysOfTheWeek().length > 0) {
            String[] res = new String[config.getDaysOfTheWeek().length];
            for (int i = 0; i < config.getDaysOfTheWeek().length; ++i) {
                res[i] = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, config.getDaysOfTheWeek()[i]);
            }
            scheduleDaysOfWeekPart = " on " + StringUtils.join(res, ",");
        }

        result.put(SCHEDULE_DETAILS, scheduleTypePart + scheduleTimePart + scheduleDaysOfWeekPart);

        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.SCHEDULED_GROUPS_SETTINGS);
    }
}
