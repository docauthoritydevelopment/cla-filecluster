package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.messages.ScanTypeSpecification;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.filetree.DocStore;
import com.cla.filecluster.domain.entity.filetree.FolderExcludeRule;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.RootFolderSpecification;
import com.cla.filecluster.repository.jpa.dataCenter.CustomerDataCenterRepository;
import com.cla.filecluster.repository.jpa.filetree.DocStoreRepository;
import com.cla.filecluster.repository.jpa.filetree.FolderExcludeRuleRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.repository.jpa.media.MediaConnectionDetailsRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.operation.CreateDepartmentAssociationForRootFolder;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.vertx.core.impl.ConcurrentHashSet;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 11/08/2015.
 */
@SuppressWarnings("NullableProblems")
@Service
public class DocStoreService {

    private static final String DEFAULT_DOC_STORE = "Default DocStore";

    @Value("${app.process.files.types:WORD,EXCEL,PDF,OTHER}")
    private String[] defaultFileTypes;

    private final static Logger logger = LoggerFactory.getLogger(DocStoreService.class);

    @Autowired
    private DocStoreRepository docStoreRepository;

    @Autowired
    private RootFolderRepository rootFolderRepository;

    @Autowired
    private FolderExcludeRuleRepository folderExcludeRuleRepository;

    @Autowired
    private MediaConnectionDetailsRepository mediaConnectionDetailsRepository;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private CustomerDataCenterRepository customerDataCenterRepository;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${associations.apply-department-associations-retry:5}")
    private int retryForApplyDepartmentAssociationToFiles;

    @Value("${scanner.filesToProcess.ignoreExt:}")
    private String[] scanOtherIgnoreExt;            // set a full ignore black list - no records will be created.

    /**
     * A list of extensions that we shouldn't scan
     */
    private Set<String> scanOtherIgnoreExtSet;

    private LoadingCache<Long, Long> rootFolderCustomerDataCenterCache = null;

    private LoadingCache<Long, RootFolder> rootFolderCacheForScan = null;

    private LoadingCache<Long, ScanTypeSpecification> rootFolderSpecification = null;

    private LoadingCache<Long, Predicate<? super String>> rootFolderIngestPredicate = null;

    private TimeSource timeSource = new TimeSource();

    private static Long defaultDocStoreId = null;

    private Set<Long> deletedRootFolderIds = new HashSet<>();

    private Set<Long> rootFolderForceAccessibilityCheck = new ConcurrentHashSet<>();

    private ScheduledOperationService scheduledOperationService;

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        scheduledOperationService = applicationContext.getBean(ScheduledOperationService.class);
    }

    private void addRootFolderForceAccessibilityCheck(Long rootFolderId) {
        rootFolderForceAccessibilityCheck.add(rootFolderId);
    }

    public boolean isRootFolderForceAccessibilityCheck(Long rootFolderId) {
        return rootFolderForceAccessibilityCheck.contains(rootFolderId);
    }

    public void removeRootFolderForceAccessibilityCheck(Long rootFolderId) {
        rootFolderForceAccessibilityCheck.remove(rootFolderId);
    }

    @PostConstruct
    public void init() {
        createRootFolderCaches();

        //noinspection ConstantConditions
        scanOtherIgnoreExtSet = (scanOtherIgnoreExt != null) ?
                Stream.of(scanOtherIgnoreExt).collect(Collectors.toSet()) :
                (new HashSet<>());
    }

    private void createRootFolderCaches() {
        rootFolderCustomerDataCenterCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .build(
                        new CacheLoader<Long, Long>() {
                            public Long load(Long key) {
                                return checkCustomerDataCenter(key);
                            }
                        });

        rootFolderSpecification = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, ScanTypeSpecification>() {
                            public ScanTypeSpecification load(Long key) {
                                RootFolder rootFolder = getRootFolderCached(key);
                                if (rootFolder == null) {
                                    return null;
                                }
                                final Map<String, FileType> concreteScanFileTypesMap = Maps.newHashMap();
                                List<FileType> typesToScan = DocStoreService.getRootFolderFileTypes(rootFolder.getMapFileTypes(), concreteScanFileTypesMap);
                                return new ScanTypeSpecification(typesToScan, concreteScanFileTypesMap.keySet(), getScanOtherIgnoreExtSet());
                            }
                        });

        rootFolderIngestPredicate = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, Predicate<? super String>>() {
                            public Predicate<? super String> load(Long key) {
                                RootFolder rootFolder = getRootFolderCached(key);
                                if (rootFolder == null) {
                                    return null;
                                }
                                final Map<String, FileType> concreteIngestFileTypesMap = Maps.newHashMap();
                                List<FileType> typesToIngest = DocStoreService.getRootFolderFileTypes(rootFolder.getIngestFileTypes(), concreteIngestFileTypesMap);
                                ScanTypeSpecification spec = new ScanTypeSpecification(typesToIngest, concreteIngestFileTypesMap.keySet(), getScanOtherIgnoreExtSet());
                                return FileTypeUtils.createFileTypesPredicate(spec);
                            }
                        });

        rootFolderCacheForScan = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, RootFolder>() {
                            public RootFolder load(Long key) {
                                Callable<RootFolder> task = () -> {
                                    RootFolder rf = getRootFolder(key);
                                    if (rf == null) return null;
                                    rf.getDocStore().getId();
                                    rf.getCustomerDataCenter().getId();
                                    if (rf.getMediaConnectionDetails() != null) {
                                        rf.getMediaConnectionDetails().getId();
                                    }
                                    return rf;
                                };
                                return dbTemplateUtils.doInTransaction(task);
                            }
                        });

        Callable<List<Long>> task = () -> {
            return rootFolderRepository.getDeletedRootFolderIds();
        };
        List<Long> deletedRFs = dbTemplateUtils.doInTransaction(task);
        if (deletedRFs != null && !deletedRFs.isEmpty()) {
            deletedRootFolderIds.addAll(deletedRFs);
        }
    }

    public Set<Long> getDeletedRootFolderIds() {
        return deletedRootFolderIds;
    }

    public Set<String> getScanOtherIgnoreExtSet() {
        return scanOtherIgnoreExtSet;
    }

    public ScanTypeSpecification getRootFolderScanTypeSpecification(Long rootFolderId) {
        return rootFolderSpecification.getUnchecked(rootFolderId);
    }

    public void evictFromCache(Long rootFolderId) {
        rootFolderCacheForScan.invalidate(rootFolderId);
        rootFolderSpecification.invalidate(rootFolderId);
        rootFolderIngestPredicate.invalidate(rootFolderId);
    }

    public Predicate<? super String> getRootFolderIngestTypePredicate(Long rootFolderId) {
        return rootFolderIngestPredicate.getUnchecked(rootFolderId);
    }

    public RootFolder getRootFolderCached(Long rootFolderId) {
        try {
            return rootFolderCacheForScan.getUnchecked(rootFolderId);
        } catch (Exception e) {
            return getRootFolder(rootFolderId);
        }
    }

    public List<RootFolder> getRootFoldersCached(Collection<Long> rootFolderIds) {
        List<RootFolder> result = new ArrayList<>();
        rootFolderIds.forEach(r ->{
            if (r != null) {
                RootFolder rf = getRootFolderCached(r);
                if (rf != null) {
                    result.add(rf);
                }
            }});
        return result;
    }

    @Transactional(readOnly = true)
    public Long getRootFolderCustomerDataCenter(Long rootFolderId, boolean useCache) {
        if (useCache) {
            return rootFolderCustomerDataCenterCache.getUnchecked(rootFolderId);
        } else {
            return checkCustomerDataCenter(rootFolderId);
        }
    }

    private Long checkCustomerDataCenter(Long rootFolderId) {
        try {
            RootFolder rootFolder = getRootFolder(rootFolderId);
            return rootFolder.getCustomerDataCenter().getId();
        } catch (Exception e) {
            logger.error("Failed to check rootfolder customerDataCenter", rootFolderId, e);
        }
        return null;
    }


//    private void extractScanningPaths(List<Path> pathList2Scan, String sourceScanningString) throws IOException {
//        Files.lines(Paths.get(sourceScanningString), StandardCharsets.UTF_8)
//                .filter(l -> !l.isEmpty()).filter(l -> !l.startsWith("#"))                // Filter out empty lines or commented lines
//                .map(l -> Paths.get(l))
//                .forEach(p -> pathList2Scan.add(p));
//    }

    @Transactional
    public RootFolderDto createRootFolder(long docStoreId, MediaType mediaType, String normalizedPath, String realPath, String rootFolderMediaEntityId) {
        if (mediaType == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.media-type.empty"), BadRequestType.MISSING_FIELD);
        }
        if (mediaType.equals(MediaType.FILE_SHARE)) {
            RootFolder rootFolderByPath = getRootFolderByPath(normalizedPath);
            if (rootFolderByPath != null) {
                logger.error("Can't create root folder by path {} - already exists", normalizedPath);
                throw new BadRequestException(messageHandler.getMessage("root-folder.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
        return docStoreRepository.findById(docStoreId)
                .map(docStore -> createRootFolder(docStore, mediaType, normalizedPath, realPath, rootFolderMediaEntityId))
                .map(rootFolder -> convertRootFolderToDto(rootFolder, docStoreId, false))
                .orElse(null);
    }

    private RootFolder createRootFolder(DocStore docStore, MediaType mediaType, String path, String realPath, String rootFolderMediaEntityId) {
        RootFolder rootFolder = new RootFolder();
        rootFolder.setDocStore(docStore);
//        Path canonicalPath = normalizeFileSharePath(scanPath.toString());
        logger.info("create root folder ({}) normalized path: {}", realPath, path);
        rootFolder.setPath(path);
        rootFolder.setRealPath(realPath);
        rootFolder.setBaseDepth(DocFolderUtils.calculateDepth(path));
        rootFolder.setFoldersCount(0);
        String fileTypes = convertFileTypesToString(defaultFileTypes);
        rootFolder.setMapFileTypes(fileTypes);
        rootFolder.setIngestFileTypes(fileTypes);
        rootFolder.setMediaType(mediaType);
        rootFolder.setCrawlerType(CrawlerType.MEDIA_PROCESSOR);
        rootFolder.setAccessible(DirectoryExistStatus.UNKNOWN);
        rootFolder.setMediaEntityId(rootFolderMediaEntityId);
        rootFolder = rootFolderRepository.save(rootFolder);
        return rootFolder;
    }

    @Transactional(readOnly = true)
    public DocStoreDto getDefaultDocStoreAsDto() {
        DocStore docStore = docStoreRepository.findDocStoreByDescription(DEFAULT_DOC_STORE).iterator().next();
        return convertDocStoreWithRootFoldersToDto(docStore);
    }

    @Transactional(readOnly = true)
    public Long getDefaultDocStoreId() {
        if (defaultDocStoreId != null) return defaultDocStoreId;

        synchronized (this) {
            DocStore docStore = docStoreRepository.findDocStoreByDescription(DEFAULT_DOC_STORE).iterator().next();
            defaultDocStoreId = docStore.getId();
        }

        return defaultDocStoreId;
    }

    private DocStoreDto convertDocStoreWithRootFoldersToDto(DocStore docStore) {
        DocStoreDto docStoreDto = new DocStoreDto(docStore.getId(), docStore.getDescription(), docStore.getPathDescriptorType());
        List<RootFolderDto> rootFolders = new ArrayList<>();
        if (docStore.getRootFolders() != null) {
            for (RootFolder rootFolder : docStore.getRootFolders()) {
                RootFolderDto rootFolderDto = convertRootFolderToDto(rootFolder, docStoreDto.getId(), false);
                rootFolders.add(rootFolderDto);
            }
        }
        docStoreDto.setRootFolders(rootFolders);
        return docStoreDto;
    }

    @Transactional(readOnly = true)
    public RootFolder getRootFolder(Long id) {
        return rootFolderRepository.findOneJoinDocStoreMediaConnection(id);
    }

    @Transactional(readOnly = true)
    public RootFolder getRootFolderAllState(Long id) {
        return rootFolderRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public RootFolderDto getRootFolderDto(long rootFolderId) {
        RootFolder rootFolder = getRootFolder(rootFolderId);
        return convertRootFolderToDto(rootFolder, rootFolder.getDocStore().getId(), true);
    }

    public RootFolder getRootFolderByPath(String rootFolderPath) {
        return rootFolderRepository.findOneByPathJoinDocStore(rootFolderPath);
    }

    public List<RootFolder> findByMailboxGroup(String mailboxGroup) {
        return rootFolderRepository.findByMailboxGroup(mailboxGroup);
    }

    public List<RootFolder> findByPathPrefix(String rootFolderPath, MediaType mediaType) {
        if (!rootFolderPath.endsWith(mediaType.getPathSeparator())) {
            rootFolderPath += mediaType.getPathSeparator();
        }
        return rootFolderRepository.findByPathPrefix(rootFolderPath);
    }

    public List<RootFolder> findByPaths(List<String> paths) {
        return rootFolderRepository.findByPaths(paths);
    }

    @Transactional(readOnly = true)
    public List<RootFolder> getRootFolders() {
        logger.debug("Get all root folders");
        return rootFolderRepository.findAllJoinDocStore();
    }

    @Transactional(readOnly = true)
    public Page<RootFolder> getAllRootFolderIdsSorted(Pageable pageRequest, Collection<Long> rootIds) {
        return rootFolderRepository.getAllRootFolderIdsSorted(rootIds, pageRequest);
    }

    @Transactional(readOnly = true)
    public List<RootFolderDto> getRootFoldersDto(List<Long> rootFoldersToScan) {
        Long defaultDocStoreAsDto = getDefaultDocStoreId();
        List<RootFolder> rootFolders = getRootFolders(rootFoldersToScan);
        return convertRootFoldersToDto(defaultDocStoreAsDto, rootFolders);
    }

    @Transactional(readOnly = true)
    public List<RootFolder> getRootFolders(Collection<Long> rootFoldersToScan) {
        if (rootFoldersToScan != null && !rootFoldersToScan.isEmpty()) {
            logger.debug("Get root folders by ids - for {} ids", rootFoldersToScan.size());
            List<RootFolder> rootFolders = rootFolderRepository.findByIdsJoinDocStore(rootFoldersToScan);
            logger.debug("Get root folders by ids - returned {} folders", rootFolders.size());
            return rootFolders;
        } else {
            logger.debug("Get all root folders");
            return getRootFolders();
        }
    }

    @Transactional(readOnly = true)
    public List<RootFolder> getRootFoldersForDataCenter(Long customerDataCenterId) {
        logger.debug("Get root folders for data center {} ", customerDataCenterId);
        List<RootFolder> rootFolders = rootFolderRepository.getRootfoldersUnderCustomerDataCenter(customerDataCenterId);
        logger.debug("Get root folders for data center {} - returned {} folders", customerDataCenterId, rootFolders.size());
        return rootFolders;
    }

    private List<RootFolderDto> convertRootFoldersToDto(long docStoreId, List<RootFolder> rootFolders) {
        return rootFolders.stream().map(rf -> convertRootFolderToDto(rf, docStoreId, true)).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Page<RootFolder> listRootFolders(Long id, PageRequest pageRequest, FilterDescriptor filterDescriptor) {
        logger.trace("Get all root folders (specification)");
        RootFolderSpecification rootFolderSpecification = new RootFolderSpecification(id, filterDescriptor);
        return rootFolderRepository.findAll(rootFolderSpecification, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<RootFolder> listRootFolders(Long id, PageRequest pageRequest, FilterDescriptor filterDescriptor, String namingSearchTerm, boolean hideDeleted) {
        logger.trace("Get all root folders (specification)");
        RootFolderSpecification rootFolderSpecification = new RootFolderSpecification(id, filterDescriptor, namingSearchTerm, hideDeleted);
        return rootFolderRepository.findAll(rootFolderSpecification, pageRequest);
    }

    @Transactional(readOnly = true)
    public List<RootFolder> listRootFolders(Long id, FilterDescriptor filterDescriptor, String namingSearchTerm, boolean hideDeleted) {
        logger.trace("Get all root folders (specification)");
        RootFolderSpecification rootFolderSpecification = new RootFolderSpecification(id, filterDescriptor, namingSearchTerm, hideDeleted);
        return rootFolderRepository.findAll(rootFolderSpecification);
    }

    @Transactional(readOnly = true)
    public List<Long> listRootFoldersIds(String namingSearchTerm) {
        logger.trace("Get all root folders (specification)");
        return rootFolderRepository.findLikeNaming(namingSearchTerm);
    }

    @Transactional(readOnly = true)
    public List<RootFolder> findContainsPath(String namingSearchTerm) {
        return rootFolderRepository.findLikePath(namingSearchTerm);
    }

    @Transactional
    public void deleteRootFolder(RootFolder rootFolder) {
        rootFolderRepository.delete(rootFolder);
        rootFolderCustomerDataCenterCache.invalidate(rootFolder.getId());
        rootFolderCacheForScan.invalidate(rootFolder.getId());
        sharePermissionService.deleteRootFolder(rootFolder.getId());
    }

    @Transactional
    public void updateRootFolderFoldersCount(long id, int size) {
        rootFolderRepository.findById(id)
                .ifPresent(rootFolder -> {
                    rootFolder.setFoldersCount(size);
                    rootFolderRepository.save(rootFolder);
                });
    }

    private FolderExcludeRule getFolderExcludeRule(long rootFolderId, String matchString, String operator) {
        return folderExcludeRuleRepository.findByRootFolderIdMatchStringOperator(rootFolderId, matchString, operator);
    }

    @Transactional
    public FolderExcludeRule addFolderExcludeRule(Long rootFolderId, FolderExcludeRuleDto folderExcludeRuleDto) {
        String matchString = folderExcludeRuleDto.getMatchString();
        String operator = folderExcludeRuleDto.getOperator();
        if (rootFolderId != null) {
            FolderExcludeRule folderExcludeRule = getFolderExcludeRule(rootFolderId, matchString, operator);
            if (folderExcludeRule != null) {
                logger.warn("User tried to create a folder exclude rule that already exists in the system {}, {}, {}", rootFolderId, matchString, operator);
                throw new BadRequestException(messageHandler.getMessage("root-folder.folder-exclude.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
        FolderExcludeRule folderExcludeRule = new FolderExcludeRule();
        RootFolder rootFolder = rootFolderRepository.getOne(rootFolderId);
        folderExcludeRule.setRootFolder(rootFolder);
        if (StringUtils.isEmpty(matchString)) {
            logger.warn("unable to add folder Exclude rule - match string is empty");
            throw new BadRequestException(messageHandler.getMessage("root-folder.folder-exclude.match.empty"), BadRequestType.MISSING_FIELD);
        }
        folderExcludeRule.setMatchString(matchString);
        if (operator == null) {
            operator = KendoFilterConstants.EQ;
        }
        folderExcludeRule.setOperator(operator);
        return folderExcludeRuleRepository.save(folderExcludeRule);
    }

    public FolderExcludeRule addGlobalFolderExcludeRule(FolderExcludeRuleDto folderExcludeRuleDto) {
        FolderExcludeRule folderExcludeRule = new FolderExcludeRule();
        folderExcludeRule.setMatchString(folderExcludeRuleDto.getMatchString());
        folderExcludeRule.setOperator(folderExcludeRuleDto.getOperator());
        return folderExcludeRuleRepository.save(folderExcludeRule);
    }

    public Page<FolderExcludeRule> listFolderExcludeRules(PageRequest pageRequest) {
        return folderExcludeRuleRepository.findAll(pageRequest);
    }

    @Transactional
    public void deleteFolderExcludeRule(long folderExcludeRuleId) {
        folderExcludeRuleRepository.deleteById(folderExcludeRuleId);
    }

    public Page<FolderExcludeRule> listFolderExcludeRulesForRootFolder(PageRequest pageRequest, Long rootFolderId) {
        return folderExcludeRuleRepository.findByRootFolderId(rootFolderId, pageRequest);
    }

    // we currently have a problem scanning pst and zip on network storage
    // we should not let user set these types for map or ingest
    private FileType[] limitFileTypesForMediaType(FileType[] fileTypes, MediaType mediaType) {
        if (mediaType.equals(MediaType.FILE_SHARE) || fileTypes == null || fileTypes.length == 0) {
            return fileTypes;
        }

        Set<FileType> result = new HashSet<>();
        for (FileType type : fileTypes) {
            if (!type.isContainer()) {
                result.add(type);
            }
        }
        return result.toArray(new FileType[result.size()]);
    }

    @Transactional
    public RootFolder updateRootFolder(Long id, RootFolderDto rootFolderDto, boolean rfHasFilesOrFolders) {
        logger.debug("Update rootFolder {}. Set to: {}", id, rootFolderDto);
        RootFolder rootFolder = rootFolderRepository.getOne(id);
        rootFolder.setStoreLocation(rootFolderDto.getStoreLocation());
        rootFolder.setStorePurpose(rootFolderDto.getStorePurpose());
        rootFolder.setStoreSecurity(rootFolderDto.getStoreSecurity());

        if (rootFolderDto.getMediaConnectionDetailsId() != null) {
            mediaConnectionDetailsRepository.findById(rootFolderDto.getMediaConnectionDetailsId())
                    .ifPresent(rootFolder::setMediaConnectionDetails);
        }
        Long prevCustomerDataCenter = rootFolder.getCustomerDataCenter() != null ? rootFolder.getCustomerDataCenter().getId() : null; //possible null cause create root folder is done in 2 stages. One only with min value and only after with update
        if (rootFolderDto.getCustomerDataCenterDto() != null) {
            CustomerDataCenter one = customerDataCenterRepository.findById(rootFolderDto.getCustomerDataCenterDto().getId());
            rootFolder.setCustomerDataCenter(one);
        } else {
            CustomerDataCenter defaultDataCenter = customerDataCenterRepository.findFirstByIsDefault(true);
            rootFolder.setCustomerDataCenter(defaultDataCenter);
        }

        if (rootFolderDto.getPath() != null && !rootFolderDto.getPath().isEmpty()) {
            if (!rootFolderDto.getPath().equals(rootFolder.getPath())) {
                rootFolder.setPath(rootFolderDto.getPath());
                rootFolder.setBaseDepth(DocFolderUtils.calculateDepth(rootFolderDto.getPath()));
                rootFolder.setAccessible(DirectoryExistStatus.UNKNOWN);
                rootFolder.setAccessabilityLastCngDate(System.currentTimeMillis());
                addRootFolderForceAccessibilityCheck(id);
            }
        }

        if (rootFolderDto.getRealPath() != null && !rootFolderDto.getRealPath().isEmpty()) {
            rootFolder.setRealPath(rootFolderDto.getRealPath());
        }

        rootFolder.setDescription(rootFolderDto.getDescription());
        rootFolder.setScannedFilesCountCap(rootFolderDto.getScannedFilesCountCap());
        rootFolder.setScannedMeaningfulFilesCountCap(rootFolderDto.getScannedMeaningfulFilesCountCap());
        rootFolder.setNickName(rootFolderDto.getNickName());
        //Update crawl settings
        String fileTypes = convertFileTypesToString(limitFileTypesForMediaType(rootFolderDto.getFileTypes(), rootFolder.getMediaType()));
        rootFolder.setMapFileTypes(fileTypes);
        fileTypes = convertFileTypesToString(limitFileTypesForMediaType(rootFolderDto.getIngestFileTypes(), rootFolder.getMediaType()));
        rootFolder.setIngestFileTypes(fileTypes);

        rootFolder.setRescanActive(rootFolderDto.isRescanActive());
        rootFolder.setReingestTimeStampMs(rootFolderDto.getReingestTimeStampMs());
        rootFolder.setScanTaskDepth(rootFolderDto.getScanTaskDepth());
        rootFolder.setFromDateScanFilter(rootFolderDto.getFromDateScanFilter());
        rootFolder.setMailboxGroup(rootFolderDto.getMailboxGroup());

        // department change
        if ((rootFolderDto.getDepartmentId() != null && rootFolder.getDepartmentId() == null) ||
                (rootFolderDto.getDepartmentId() == null && rootFolder.getDepartmentId() != null) ||
                (rootFolderDto.getDepartmentId() != null && rootFolder.getDepartmentId() != null &&
                        !rootFolder.getDepartmentId().equals(rootFolderDto.getDepartmentId()))
                ) {
            rootFolder.setDepartmentId(rootFolderDto.getDepartmentId());
            departmentAssociationForRootFolderOperation(id, rootFolderDto.getIdentifier(), rfHasFilesOrFolders);
        }

        CrawlerType crawlerType = rootFolderDto.getCrawlerType() == null ? CrawlerType.MEDIA_PROCESSOR : rootFolderDto.getCrawlerType();
        rootFolder.setCrawlerType(crawlerType);
        RootFolder save = rootFolderRepository.save(rootFolder);
        mediaConnectionDetailsService.cleanCacheMediaConnectionDetailsForRootFolder(rootFolder.getId());
        if (prevCustomerDataCenter == null || prevCustomerDataCenter != save.getCustomerDataCenter().getId()) {
            rootFolderCustomerDataCenterCache.invalidate(rootFolder.getId());
        }
        evictFromCache(rootFolder.getId());
        return save;
    }

    @Transactional
    public RootFolderDto updateRootFolderActive(long rootFolderId, Boolean state) {
        if (state == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.state.mandatory"), BadRequestType.MISSING_FIELD);
        }
        logger.info("Set rootFolder active {} for rootFolder ID {}", (state ? "on" : "off"), rootFolderId);
        RootFolder rootFolder = rootFolderRepository.getOne(rootFolderId);
        rootFolder.setRescanActive(state);
        evictFromCache(rootFolder.getId());
        return convertRootFolderToDto(
                rootFolderRepository.save(rootFolder), null, false);

    }

    @Transactional // internal use when we dont want to mark dirty as change came from association anyway
    public void updateRootFolderDepartment(long rootFolderId, Long departmentId) {
        logger.info("Set rootFolder department {} for rootFolder ID {}", departmentId, rootFolderId);
        RootFolder rootFolder = rootFolderRepository.getOne(rootFolderId);
        rootFolder.setDepartmentId(departmentId);
        evictFromCache(rootFolder.getId());
    }

    @Transactional
    public void updateFirstScanDone(long rootFolderId) {
		RootFolder rootFolder =  rootFolderRepository.getOne(rootFolderId);
		if (rootFolder.getFirstScan() == null || rootFolder.getFirstScan()) {
            logger.debug("rootFolder update id={}, lastRun={}", rootFolderId, rootFolder.getLastRunId());
            rootFolder.setFirstScan(false);
            rootFolderRepository.save(rootFolder);
        }
    }

    @Transactional
    public void updateRootFolderAccessible(long rootFolderId, DirectoryExistStatus isAccessible) {
    	if (isAccessible == null) {
            isAccessible = DirectoryExistStatus.UNKNOWN;
        }
		logger.debug("rootFolder update id={}",rootFolderId);
		rootFolderRepository.updateAccessability(isAccessible.getValue(), System.currentTimeMillis(), rootFolderId);
		rootFolderCacheForScan.invalidate(rootFolderId);
	}

    public void markRootFolderAsDeleted(long rootFolderId) {
        logger.debug("rootFolder set deleted id={}",rootFolderId);
        rootFolderRepository.setDeleted(rootFolderId);
        evictFromCache(rootFolderId);
        deletedRootFolderIds.add(rootFolderId);
    }

    @Transactional
    public RootFolderDto updateRootFolderReingest(long rootFolderId, Boolean reingest) {
        if (reingest == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.re-ingest.mandatory"), BadRequestType.MISSING_FIELD);
        }
        logger.info("Set rootFolder reIngest {} for rootFolder ID {}", reingest, rootFolderId);
        RootFolder rootFolder = rootFolderRepository.getOne(rootFolderId);
        rootFolder.setReingestTimeStampMs(reingest ? timeSource.currentTimeMillis() : 0L);
        evictFromCache(rootFolder.getId());
        return convertRootFolderToDto(
                rootFolderRepository.save(rootFolder), null, false);
    }


    @Transactional(readOnly = true)
    public List<FolderExcludeRuleDto> getFolderExcludeRulesDto(Long rootFolderId) {
        List<FolderExcludeRule> rootFolderRelevantExcludeRules = folderExcludeRuleRepository.getRootFolderRelevantExcludeRules(rootFolderId);
        return convertFolderExcludeRules(rootFolderRelevantExcludeRules);
    }

    @Transactional
    public void disableFolderExcludeRule(long id, boolean disabled) {
        FolderExcludeRule one = folderExcludeRuleRepository.getOne(id);
        one.setDisabled(disabled);
    }

    @Transactional
    public FolderExcludeRule updateFolderExcludeRule(long id, boolean disabled, String operator, String matchString) {
        FolderExcludeRule one = folderExcludeRuleRepository.getOne(id);
        one.setDisabled(disabled);
        one.setOperator(operator);
        one.setMatchString(matchString);
        return one;
    }

    @Transactional
    public void attachRunToRootFolder(Long runId, Long rootFolderId) {
        logger.debug("Attach run {} to root Folder {}", runId, rootFolderId);
        RootFolder rootFolder = rootFolderRepository.findOneJoinDocStore(rootFolderId);
        rootFolder.setLastRunId(runId);
        rootFolderRepository.save(rootFolder);
    }

    private String[] getFileTypes(final String fileTypes) {
        String[] fileTypesArr;
        if (fileTypes == null) {
            fileTypesArr = defaultFileTypes;
        } else {
            fileTypesArr = fileTypes.split(",");
        }
        return fileTypesArr;
    }


    @Transactional(readOnly = true)
    public String[] getRootFolderMapFileTypesAsStringArray(Long rootFolderId) {
        RootFolder one = rootFolderRepository.getOne(rootFolderId);
        return getFileTypes(one.getMapFileTypes());
    }

    @Transactional(readOnly = true)
    public List<Long> getAllRootFolderIds() {
        return rootFolderRepository.getAllRootFolderIds();
    }

    @Transactional(readOnly = true)
    public List<Long> getRootFolderIdsByMediaConnectionId(Long mediaConnectionId) {
        return rootFolderRepository.getRootFolderIdsByMediaConnectionId(mediaConnectionId);
    }

    @Transactional
    public DocStore createDefaultDocStore() {
        DocStore docStore = new DocStore();
        docStore.setDescription(DEFAULT_DOC_STORE);
        if (File.separator.equals("/")) {
            docStore.setPathDescriptorType(PathDescriptorType.LINUX);
        } else {
            docStore.setPathDescriptorType(PathDescriptorType.WINDOWS);
        }
        docStore = docStoreRepository.save(docStore);
        return docStore;
    }

    @Transactional(readOnly = true)
    public long countRootFolders() {
        return rootFolderRepository.count();
    }

    @Transactional(readOnly = true)
    public Map<Long, CustomerDataCenter> getDCForRootFolders(Collection<Long> rootFolderIds) {
        List<RootFolder> rfs = rootFolderRepository.findByIds(rootFolderIds);
        return rfs.stream().collect(Collectors.toMap(RootFolder::getId, r -> {
            CustomerDataCenter dc = r.getCustomerDataCenter();
            dc.getName();
            return dc;
        }));
    }

    @Transactional
    public void deleteAll() {
        rootFolderRepository.deleteAll();
        rootFolderCustomerDataCenterCache.invalidateAll();
        rootFolderCacheForScan.invalidateAll();
    }

    @Transactional(readOnly = true)
    public List<RootFolder> findByIds(Collection<Long> rootIds) {
        return rootFolderRepository.findByIds(rootIds);
    }

    @Transactional(readOnly = true)
    public long countUnDeletedDepartmentAssociations(Long departmentId) {
        return rootFolderRepository.countUnDeletedDepartmentAssociations(departmentId);
    }

    public static List<FileType> getRootFolderFileTypes(String fileTypes, Map<String, FileType> concreteScanFileTypesMap) {
        List<FileType> typesToScan = Arrays.asList(convertFileTypesForRootFolder(fileTypes));
        FileTypeUtils.getFileTypesToScan(concreteScanFileTypesMap, typesToScan);
        return typesToScan;
    }

    public static RootFolderDto convertRootFolderToDto(RootFolder rootFolder, Long docStoreId, boolean addFolderExcludeRules) {
        return convertRootFolderToDto(rootFolder, docStoreId, addFolderExcludeRules, null);
    }

    public static RootFolderDto convertRootFolderToDto(RootFolder rootFolder, Long docStoreId, boolean addFolderExcludeRules,
                                                       List<RootFolderSharePermissionDto> rootFolderSharePermissions) {
        String path = rootFolder.getPath();
        if (path != null && rootFolder.getRealPath() != null && rootFolder.getRealPath().contains("\\")) {
            path = FileNamingUtils.convertFromUnixToWindows(path);
        }
        RootFolderDto rootFolderDto = new RootFolderDto(rootFolder.getId(), path, docStoreId);
        rootFolderDto.setRealPath(rootFolder.getRealPath());
        rootFolderDto.setNumberOfFoldersFound(rootFolder.getFoldersCount());
        rootFolderDto.setStoreLocation(rootFolder.getStoreLocation());
        rootFolderDto.setStorePurpose(rootFolder.getStorePurpose());
        rootFolderDto.setStoreSecurity(rootFolder.getStoreSecurity());
        rootFolderDto.setStoreRescanMethod(rootFolder.getStoreRescanMethod());
        rootFolderDto.setMediaType(rootFolder.getMediaType());
        rootFolderDto.setMediaConnectionDetailsId(rootFolder.getMediaConnectionDetailsId());
        rootFolderDto.setMediaConnectionName(rootFolder.getMediaConnectionName());
        rootFolderDto.setCrawlerType(rootFolder.getCrawlerType());
        rootFolderDto.setDescription(rootFolder.getDescription());
        rootFolderDto.setLastRunId(rootFolder.getLastRunId());
        rootFolderDto.setScannedFilesCountCap(rootFolder.getScannedFilesCountCap());
        rootFolderDto.setScannedMeaningfulFilesCountCap(rootFolder.getScannedMeaningfulFilesCountCap());
        rootFolderDto.setNickName(rootFolder.getNickName());
        rootFolderDto.setRescanActive(rootFolder.isRescanActive());
        rootFolderDto.setScanTaskDepth(rootFolder.getScanTaskDepth());
        rootFolderDto.setCustomerDataCenterDto(CustomerDataCenterService.convertCustomerDataCenterToDto(rootFolder.getCustomerDataCenter())); //TODO: make separate query for all data centers
        FileType[] fileTypes = convertFileTypesForRootFolder(rootFolder.getMapFileTypes());
        rootFolderDto.setFileTypes(fileTypes);

        fileTypes = convertFileTypesForRootFolder(rootFolder.getIngestFileTypes());
        rootFolderDto.setIngestFileTypes(fileTypes);
        rootFolderDto.setRootFolderSharePermissions(rootFolderSharePermissions);
        rootFolderDto.setReingestTimeStampMs(rootFolder.getReingestTimeStampMs());
        rootFolderDto.setIsAccessible(rootFolder.getAccessible());
        rootFolderDto.setAccessabilityLastCngDate(rootFolder.getAccessabilityLastCngDate());
        rootFolderDto.setFirstScan(rootFolder.getFirstScan());
        rootFolderDto.setFromDateScanFilter(rootFolder.getFromDateScanFilter());
        rootFolderDto.setMailboxGroup(rootFolder.getMailboxGroup());
        rootFolderDto.setDepartmentId(rootFolder.getDepartmentId());
        rootFolderDto.setDeleted(rootFolder.getDeleted());
        rootFolderDto.setShareFolderPermissionError(rootFolder.isFailureGettingSharePermission());
        if (addFolderExcludeRules) {
            if (rootFolder.getFolderExcludeRules() != null) {
                int configuredExcludedFoldersCount = rootFolder.getFolderExcludeRules().size();
                rootFolderDto.setFolderExcludeRulesCount(configuredExcludedFoldersCount);
            }
        }

//        boolean directoryExists = Files.exists(Paths.get(rootFolder.getPath()));
//        rootFolderDto.setAccessible(directoryExists);
        return rootFolderDto;
    }

    public static RootFolderSummaryInfo convertRootFolderToSummaryInfo(RootFolder rootFolder) {
        RootFolderSummaryInfo rootFolderSummaryInfo = new RootFolderSummaryInfo();
        rootFolderSummaryInfo.setRootFolderDto(
                convertRootFolderToDto(rootFolder, null, true));
        rootFolderSummaryInfo.setRunStatus(RunStatus.NA.name()); // It never ran before...
        return rootFolderSummaryInfo;
    }

    private static FileType[] convertFileTypesForRootFolder(String fileTypes) {
        return Strings.isNullOrEmpty(fileTypes)
                ? new FileType[]{} : convertFileTypes(fileTypes);
    }

    private static FileType[] convertFileTypes(String fileTypes) {
        String[] split = fileTypes.split(",");
        FileType[] result = new FileType[split.length];
        for (int i = 0; i < split.length; i++) {
            String fileTypeString = split[i];
            result[i] = FileType.valueOf(fileTypeString);
        }
        return result;
    }

    public static Page<RootFolderSummaryInfo> convertRootFoldersToRootFolderSummaryInfo(
            Page<RootFolder> rootFolders, PageRequest pageRequest, boolean getRules) {
        List<RootFolderSummaryInfo> content = new ArrayList<>();
        for (RootFolder rootFolder : rootFolders) {
            RootFolderSummaryInfo rootFolderSummaryInfo = convertRootFolderToSummaryInfo(rootFolder);
            if (getRules && rootFolder.getFolderExcludeRules() != null) {
                rootFolderSummaryInfo.setFolderExcludeRules(
                convertFolderExcludeRules(rootFolder.getFolderExcludeRules()));
            }
            content.add(rootFolderSummaryInfo);
        }
        return new PageImpl<>(content, pageRequest, rootFolders.getTotalElements());
    }

    public static Page<RootFolderDto> convertRootFoldersPage(PageRequest pageRequest, Long docStoreId, Page<RootFolder> response) {
        List<RootFolderDto> content = new ArrayList<>();
        for (RootFolder rootFolder : response.getContent()) {
            RootFolderDto rootFolderDto = convertRootFolderToDto(rootFolder, docStoreId, true);
            content.add(rootFolderDto);
        }

        return new PageImpl<>(content, pageRequest, response.getTotalElements());
    }

    public static List<RootFolderDto> convertRootFoldersToDto(List<RootFolder> rootFolders, Long docStoreId, boolean addFolderExcludeRules) {
        List<RootFolderDto> content = new ArrayList<>();
        for (RootFolder rootFolder : rootFolders) {
            RootFolderDto rootFolderDto = convertRootFolderToDto(rootFolder, docStoreId, addFolderExcludeRules);
            content.add(rootFolderDto);
        }

        return content;
    }

    public static FolderExcludeRuleDto convertFolderExcludeRule(FolderExcludeRule folderExcludeRule) {
        FolderExcludeRuleDto folderExcludeRuleDto = new FolderExcludeRuleDto();
        folderExcludeRuleDto.setId(folderExcludeRule.getId());
        folderExcludeRuleDto.setMatchString(folderExcludeRule.getMatchString());
        folderExcludeRuleDto.setOperator(folderExcludeRule.getOperator());
        folderExcludeRuleDto.setDisabled(folderExcludeRule.isDisabled());
        if (folderExcludeRule.getRootFolder() != null) {
            folderExcludeRuleDto.setRootFolderId(folderExcludeRule.getRootFolder().getId());
        }
        return folderExcludeRuleDto;
    }

    public static Page<FolderExcludeRuleDto> convertFolderExcludeRules(Page<FolderExcludeRule> folderExcludeRules, PageRequest pageRequest) {
        List<FolderExcludeRuleDto> content = new ArrayList<>();
        for (FolderExcludeRule folderExcludeRule : folderExcludeRules.getContent()) {
            content.add(convertFolderExcludeRule(folderExcludeRule));
        }
        return new PageImpl<>(content, pageRequest, folderExcludeRules.getTotalElements());
    }

    private static List<FolderExcludeRuleDto> convertFolderExcludeRules(Collection<FolderExcludeRule> folderExcludeRules) {
        List<FolderExcludeRuleDto> content = new ArrayList<>();
        for (FolderExcludeRule folderExcludeRule : folderExcludeRules) {
            content.add(convertFolderExcludeRule(folderExcludeRule));
        }
        return content;
    }

    private static String convertFileTypesToString(FileType[] fileTypesParam) {
        FileType[] fileTypes = fileTypesParam != null && fileTypesParam.length > 0 ?
                fileTypesParam : new FileType[]{};
        StringJoiner stringJoiner = new StringJoiner(",");
        for (FileType fileType : fileTypes) {
            stringJoiner.add(fileType.name());
        }
        return stringJoiner.toString();
    }

    private static String convertFileTypesToString(String[] fileTypes) {
        StringJoiner stringJoiner = new StringJoiner(",");
        for (String fileType : fileTypes) {
            stringJoiner.add(fileType);
        }
        return stringJoiner.toString();
    }

    private void departmentAssociationForRootFolderOperation(Long id, String identifier, boolean rfHasFilesOrFolders) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.CREATE_DEPARTMENT_ASSOCIATIONS_FOR_ROOT_FOLDER);
        data.setOpData(new HashMap<>());
        data.getOpData().put(CreateDepartmentAssociationForRootFolder.ROOT_FOLDER_ID_PARAM, String.valueOf(id));
        data.setUserOperation(rfHasFilesOrFolders);
        data.setRetryLeft(retryForApplyDepartmentAssociationToFiles);
        data.setDescription(messageHandler.getMessage("operation-description.assign-department-root-folder",
                Lists.newArrayList(identifier)));
        scheduledOperationService.createNewOperation(data);
    }

    public static String getRootFolderIdentifier(RootFolder rf) {
        if (!Strings.isNullOrEmpty(rf.getRealPath())) {
            return rf.getRealPath();
        }
        if (!Strings.isNullOrEmpty(rf.getMailboxGroup())) {
            return rf.getMailboxGroup();
        }
        if (!Strings.isNullOrEmpty(rf.getNickName())) {
            return rf.getNickName();
        }
        return "";
    }
}
