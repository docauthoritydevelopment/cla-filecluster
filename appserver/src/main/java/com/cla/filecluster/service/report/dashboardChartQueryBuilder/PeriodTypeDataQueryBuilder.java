package com.cla.filecluster.service.report.dashboardChartQueryBuilder;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.report.DashboardChartQueryBuilder;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import com.cla.filecluster.service.report.dashboardChartsEnrichers.DefaultTypeDataEnricher;
import com.cla.filecluster.service.report.dashboardChartsEnrichers.PeriodTypeDataEnricher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PeriodTypeDataQueryBuilder implements DashboardChartQueryBuilder<Object, PeriodTypeDataEnricher> {

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;
    private static final String INNER_PERIOD_FACET_NAME = "INNER_PERIOD_FACET";
    private static final String PERIOD_TYPE_MONTH = "MONTH";
    private static final String PERIOD_TYPE_DAY = "DAY";


    public ChartQueryType getQueryBuilderType() {
        return ChartQueryType.PERIOD;
    }

    public Object setInitialDataToDashboardChartDto(DashboardChartDataDto dashboardChartDataDto, CatFileFieldType groupedField,
                                                    DataSourceRequest dataSourceRequest, FilterDescriptor filter,
                                                    String queryPrefix, DashboardChartValueTypeDto valueField,
                                                    Map<String, String> params,
                                                    String additionalFilter,
                                                    PeriodTypeDataEnricher periodTypeChartEnricher) {

        return null;
    }


    public SolrFacetQueryJson buildQueryJsonFacetByFilter(Object initialGetDataResult, SolrSpecification solrSpecification,
                                                          FilterDescriptor filter,
                                                          SolrCoreSchema field, String queryPrefix,
                                                          DashboardChartValueTypeDto valueField,
                                                          Object[] neededValues,
                                                          String queryName, Map<String, String> params,
                                                          boolean getAccurateNumOfBuckets, String additionalFilter) {
        int itemsNumber = dashboardChartsUtil.extractTakeNumber(params);
        int page = dashboardChartsUtil.extractPageNumber(params);
        String periodType = dashboardChartsUtil.extractPeriodType(params);

        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(queryName != null ? queryName : field.getSolrName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(CatFileFieldType.DELETED);
        if (queryPrefix != null) {
            theFacet.setPrefix(queryPrefix);
        }
        List<String> filterStrList = new ArrayList<>();
        // checking if the filter is for total (in this case the filter will empty , only with name )
        if (filter != null && (filter.getValue() != null || !filter.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filter, solrSpecification));
        }
        if (!filterStrList.isEmpty()) {
            // this replace is becuase the JSON facet  domain filter need double slash
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }

        SolrFacetQueryJson innerRangeFacet = new SolrFacetQueryJson(INNER_PERIOD_FACET_NAME);
        innerRangeFacet.setType(FacetType.RANGE);
        innerRangeFacet.setField(field);
        if (page == 1) {
            innerRangeFacet.setStart("NOW/" + periodType + "-" + (itemsNumber - 1) + periodType);
            innerRangeFacet.setEnd("NOW");
        } else {
            innerRangeFacet.setStart("NOW/" + periodType + "-" + (page * itemsNumber - 1) + periodType);
            innerRangeFacet.setEnd("NOW/" + periodType + "-" + ((page - 1) * itemsNumber - 1) + periodType);
        }
        innerRangeFacet.setGap("+1" + periodType);

        if (!valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            dashboardChartsUtil.addInnerFacetsAccordingToValue(valueField, getAccurateNumOfBuckets, innerRangeFacet);
        }

        theFacet.addFacet(innerRangeFacet);
        return theFacet;
    }

    public void parseResponseToDashboardChartDataDto(Object initialGetDataResult, DashboardChartDataDto dashboardChartDataDto,
                                                     List<SolrFacetJsonResponseBucket> buckets,
                                                     String seriesName, DashboardChartValueTypeDto valueField,
                                                     Map<String, String> params, boolean getAccurateNumOfBuckets) {

        String periodType = dashboardChartsUtil.extractPeriodType(params);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy");
        if (periodType.equals(PERIOD_TYPE_MONTH)) {
            dateFormatter = new SimpleDateFormat("MMM yyyy");
        } else if (periodType.equals(PERIOD_TYPE_DAY)) {
            dateFormatter = new SimpleDateFormat("MMM d yyyy");
        }

        boolean needToFillCategories = dashboardChartDataDto.getCategories().size() == 0;
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(seriesName);
        ArrayList<Double> dataList = new ArrayList<>(Collections.nCopies(dashboardChartDataDto.getCategories().size(), 0d));
        seriesObj.setData(new ArrayList<>());
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            boolean isDeleted = bucket.getBooleanVal();
            if (!isDeleted) {
                List<SolrFacetJsonResponseBucket> innerBuckets = new ArrayList<>();
                if (bucket.containsKey(INNER_PERIOD_FACET_NAME)) {
                    innerBuckets = bucket.getBucketBasedFacets(INNER_PERIOD_FACET_NAME).getBuckets();
                }
                for (SolrFacetJsonResponseBucket innerBucket : innerBuckets) {
                    Date rangeDate = (Date) innerBucket.getDateVal();
                    String category = (dateFormatter.format(rangeDate));

                    Double countValue = dashboardChartsUtil.parseInnerBucketValue(dashboardChartDataDto, valueField, getAccurateNumOfBuckets, innerBucket);
                    if (needToFillCategories) {
                        dashboardChartDataDto.getCategories().add(0, category);
                        dataList.add(0, countValue);
                    } else {
                        int categoryIndex = dashboardChartDataDto.getCategories().indexOf(category);
                        if (categoryIndex > -1) {
                            dataList.set(categoryIndex, countValue);
                        }
                    }
                }
            }
        }
        seriesObj.setData(dataList);
        //dashboardChartDataDto.setTotalElements(Long.valueOf(dashboardChartDataDto.getCategories().size()));
        dashboardChartDataDto.getSeries().add(seriesObj);
    }

    public void enrichChartData(Object initialGetDataResult, DashboardChartDataDto dashboardChartDataDto,
                                Map<String, String> params, PeriodTypeDataEnricher periodTypeChartEnricher) {
    }

    public FilterDescriptor[] getSeriesQueryResults(CatFileFieldType seriesField,
                          String seriesFieldPrefix,
                          DashboardChartValueTypeDto valueField,
                          DataSourceRequest dataSourceRequest,
                          Map<String, String> params,
                          String additionalFilter,
                          PeriodTypeDataEnricher periodTypeChartEnricher) {
        return new FilterDescriptor[(0)];
    }

    public String getOtherQuery(Object initialGetDataResult,List<String> categories, Map<String, String> params) {
        int itemsNumber = dashboardChartsUtil.extractTakeNumber(params);
        int page = dashboardChartsUtil.extractPageNumber(params);
        String periodType = dashboardChartsUtil.extractPeriodType(params);
        String ans;
        if (page == 1) {
            ans = "(![NOW/" + periodType + "-" + (itemsNumber - 1) + periodType + " TO NOW])";
        } else {
            ans = "(![NOW/" + periodType + "-" + (page * itemsNumber - 1) + periodType + "TO NOW/" + periodType + "-" + ((page - 1) * itemsNumber - 1) + periodType + "])";
        }
        return  ans;
    }

    public String getAdditionalFilterByCategories(Object placeHolder, List<String> categoriesIds, CatFileFieldType field, PeriodTypeDataEnricher periodTypeChartEnricher, Map<String, String> params){
        int itemsNumber = dashboardChartsUtil.extractTakeNumber(params);
        int page = dashboardChartsUtil.extractPageNumber(params);
        String periodType = dashboardChartsUtil.extractPeriodType(params);
        String ans;
        if (page == 1) {
            ans = field.getSolrName() + ":([NOW/" + periodType + "-" + (itemsNumber - 1) + periodType + " TO NOW])";
        } else {
            ans = field.getSolrName() + ":([NOW/" + periodType + "-" + (page * itemsNumber - 1) + periodType + "TO NOW/" + periodType + "-" + ((page - 1) * itemsNumber - 1) + periodType + "])";
        }
        return  ans;
    }
}
