package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: ophir
 * Created on: 1/5/2019
 */
public class SearchPatternAggCountDtoMixin {

    @JsonUnwrapped
    FileUserDto item;

    @JsonProperty(FileCountHeaderFields.NUM_OF_FILES)
    private Integer count;
}
