package com.cla.filecluster.service.communication;

import com.cla.common.domain.dto.email.MailNotificationConfigurationDto;
import com.cla.common.domain.dto.email.SmtpConfigurationDto;
import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.convertion.DataEncryptionService;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

/**
 * Created by uri on 12-Apr-17.
 */
@Service
public class EmailConfigurationService {

    private static final Logger logger = LoggerFactory.getLogger(EmailConfigurationService.class);

    private static final String SMTP_CONFIGURATION_KEY = "smtpConfiguration";
    private static final String SEND_EMAIL_REPORT_ENABLED_KEY = "sendEmailReportEnabled";
    private static final String EMAIL_ADDRESSES_KEY = "emailAddresses";

    @Value("${email-progress-report.addresses:}")
    private String defaultAddresses;

    @Value("${email-progress-report.enabled:false}")
    private boolean emailEnabledByDefault;

    @Value("${smtp.tls:false}")
    private boolean useTls;

    @Value("${smtp.ssl:false}")
    private boolean useSsl;

    @Value("${smtp.username:}")
    private String username;

    @Value("${smtp.password:}")
    private String password;

    @Value("${smtp.host:}")
    private String host;

    @Value("${smtp.port:25}")
    private Integer port;

    @Value("${smtp.auth:false}")
    private boolean smtpAuthentication;

    @Autowired
    private DataEncryptionService dataEncryptionService;

    @Autowired
    private SystemSettingsService systemSettingsService;

    private String addressesCache;

    private Boolean sendReportEnabledCache;

    private SmtpConfigurationDto smtpConfigurationCache;

    public void initDefaultSettings() {
        try {
            logger.debug("Init default email settings");
            smtpConfigurationCache = initSmtpConfigurationFromApplicationProperties();
            String smtpConfigurationString = convertSmtpConfigurationToJson(smtpConfigurationCache);

            systemSettingsService.addSystemSettings(SMTP_CONFIGURATION_KEY, smtpConfigurationString);
            systemSettingsService.addSystemSettings(EMAIL_ADDRESSES_KEY, defaultAddresses);
            logger.debug("Email notification enabled={}", emailEnabledByDefault);
            systemSettingsService.addSystemSettings(SEND_EMAIL_REPORT_ENABLED_KEY, String.valueOf(emailEnabledByDefault));

        } catch (Exception e) {
            logger.error("Failed to init default email settings", e);
        }
    }

    @Transactional(readOnly = true)
    public MailNotificationConfigurationDto getNotificationConfigurationSettings() {
        String addresses = getAddresses();
        SmtpConfigurationDto smtpConfiguration = getSmtpConfiguration(true);
        boolean sendReportEnabled = isSendReportEnabled();
        MailNotificationConfigurationDto mailNotificationConfigurationDto = new MailNotificationConfigurationDto();
        mailNotificationConfigurationDto.setAddresses(addresses);
        mailNotificationConfigurationDto.setEnabled(sendReportEnabled);
        mailNotificationConfigurationDto.setSmtpConfigurationDto(smtpConfiguration);
        return mailNotificationConfigurationDto;
    }

    @Transactional
    public SmtpConfigurationDto setNotificationConfiguration(MailNotificationConfigurationDto mailNotificationConfigurationDto) {
        logger.info("Set Notification configuration to {}", mailNotificationConfigurationDto);

        SmtpConfigurationDto smtpConfigurationDto = mailNotificationConfigurationDto.getSmtpConfigurationDto();
        if (!smtpConfigurationDto.isPasswordEncrypted() && smtpConfigurationDto.getPassword() != null) {
            String password = smtpConfigurationDto.getPassword();
            smtpConfigurationDto.setPassword(dataEncryptionService.encrypt(password));
            smtpConfigurationDto.setPasswordEncrypted(true);
        } else {
            SmtpConfigurationDto currSmtpConfiguration = getSmtpConfiguration(false);
            smtpConfigurationDto.setPassword(currSmtpConfiguration.getPassword());
            smtpConfigurationDto.setPasswordEncrypted(true);
        }
        String smtpConfigurationString = convertSmtpConfigurationToJson(smtpConfigurationDto);

        systemSettingsService.updateOrCreateSystemSettings(SMTP_CONFIGURATION_KEY, smtpConfigurationString);

        systemSettingsService.updateOrCreateSystemSettings(EMAIL_ADDRESSES_KEY, mailNotificationConfigurationDto.getAddresses());

        systemSettingsService.updateOrCreateSystemSettings(SEND_EMAIL_REPORT_ENABLED_KEY, String.valueOf(mailNotificationConfigurationDto.isEnabled()));

        //Reset cache
        addressesCache = null;
        smtpConfigurationCache = null;
        sendReportEnabledCache = null;
        return smtpConfigurationDto;
    }

    @Transactional
    public boolean isNotExsistingCurrentSmtpPassword() {
        SmtpConfigurationDto currSmtpConfiguration = getSmtpConfiguration(false);
        String currPassword = currSmtpConfiguration.getPassword();
        return StringUtils.isEmpty(currPassword) ||
                dataEncryptionService.encrypt("").equals(currPassword);
    }

    String getAddresses() {
        if (addressesCache != null) {
            return addressesCache;
        }
        List<SystemSettingsDto> systemSettings = systemSettingsService.getSystemSettings(EMAIL_ADDRESSES_KEY);
        if (!CollectionUtils.isEmpty(systemSettings)) {
            addressesCache = systemSettings.get(0).getValue();
        }
        return addressesCache;
    }


    boolean isSendReportEnabled() {
        if (sendReportEnabledCache != null) {
            return sendReportEnabledCache;
        }
        List<SystemSettingsDto> systemSettings = systemSettingsService.getSystemSettings(SEND_EMAIL_REPORT_ENABLED_KEY);
        if (!CollectionUtils.isEmpty(systemSettings)) {
            sendReportEnabledCache = Boolean.valueOf(systemSettings.get(0).getValue());
            logger.debug("Email report enabled={}", sendReportEnabledCache);
        } else {
            logger.debug("Email report configuration is missing.");
            sendReportEnabledCache = false;
        }
        return sendReportEnabledCache;
    }

    synchronized SmtpConfigurationDto getSmtpConfiguration(boolean hidePassword) {
        if (smtpConfigurationCache == null) {
            List<SystemSettingsDto> systemSettings = systemSettingsService.getSystemSettings(SMTP_CONFIGURATION_KEY);
            if (!CollectionUtils.isEmpty(systemSettings)) {
                String data = systemSettings.get(0).getValue();
                smtpConfigurationCache = convertJsonToSmtpConfiguration(data);
            }
        }
        if (hidePassword) {
            SmtpConfigurationDto smtpConfigurationDto = new SmtpConfigurationDto(smtpConfigurationCache);
            smtpConfigurationDto.setPassword(null);
            return smtpConfigurationDto;
        }
        return smtpConfigurationCache;
    }

    private SmtpConfigurationDto initSmtpConfigurationFromApplicationProperties() {
        SmtpConfigurationDto smtpConfigurationDto = new SmtpConfigurationDto();
        smtpConfigurationDto.setHost(host);
        smtpConfigurationDto.setPassword(dataEncryptionService.encrypt(password));
        smtpConfigurationDto.setPasswordEncrypted(true);
        smtpConfigurationDto.setPort(port);
        smtpConfigurationDto.setUsername(username);
        smtpConfigurationDto.setSmtpAuthentication(smtpAuthentication);
        smtpConfigurationDto.setUseSsl(useSsl);
        smtpConfigurationDto.setUseTls(useTls);
        return smtpConfigurationDto;
    }

    String decryptPassword(String password) {
        return dataEncryptionService.decrypt(password);
    }

    public static SmtpConfigurationDto convertJsonToSmtpConfiguration(String data) {
        try {
            return ModelDtoConversionUtils.getMapper().readValue(data, SmtpConfigurationDto.class);
        } catch (IOException e) {
            logger.error("Failed to convert SmtpConfigurationDto (" + data + ") to SmtpConfigurationDto (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert SmtpConfigurationDto data to SmtpConfigurationDto (IO Exception): " + e.getMessage());
        }
    }

    public static String convertSmtpConfigurationToJson(SmtpConfigurationDto smtpConfiguration) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(smtpConfiguration);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert SmtpConfigurationDto (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert SmtpConfigurationDto (IO Exception): " + e.getMessage());
        }
    }
}
