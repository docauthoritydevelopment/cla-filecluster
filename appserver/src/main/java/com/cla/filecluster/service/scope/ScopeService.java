package com.cla.filecluster.service.scope;

import com.cla.common.domain.dto.scope.Scope;
import com.cla.common.domain.dto.scope.UserScopeDto;
import com.cla.common.domain.dto.scope.WorkGroupsScopeDto;
import com.cla.common.domain.dto.security.UserDto;
import com.cla.common.domain.dto.security.WorkGroupDto;
import com.cla.filecluster.domain.entity.BasicEntity;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.scope.UserScope;
import com.cla.filecluster.domain.entity.scope.WorkGroupsScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.entity.security.WorkGroup;
import com.cla.filecluster.repository.jpa.scope.BasicScopeRepository;
import com.cla.filecluster.repository.jpa.scope.UserScopeRepository;
import com.cla.filecluster.repository.jpa.scope.WorkGroupsScopeRepository;
import com.cla.filecluster.repository.jpa.security.WorkGroupRepository;
import com.cla.filecluster.service.security.UserService;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class ScopeService {

    private static final Logger logger = LoggerFactory.getLogger(ScopeService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserScopeRepository userScopeRepository;

    @Autowired
    private WorkGroupsScopeRepository workGroupsScopeRepository;

    @Autowired
    private WorkGroupRepository workGroupRepository;

    @Autowired
    private BasicScopeRepository basicScopeRepository;

    private LoadingCache<Long, BasicScope> scopeCache = null;

    @PostConstruct
    void init() {
        scopeCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, BasicScope>() {
                            public BasicScope load(Long key) {
                                return getById(key);
                            }
                        });
        basicScopeRepository.findAll().forEach(scope -> {
            scopeCache.put(scope.getId(), scope);
        });
    }

    public BasicScope getFromCache(Long scopeId) {
        return scopeCache.getUnchecked(scopeId);
    }

    @Transactional(readOnly = true)
    public BasicScope getById(Long scopeId) {
        return basicScopeRepository.getOne(scopeId);
    }

    @Transactional
    public BasicScope getOrCreateScope(Scope scope) {
        BasicScope basicScope = null;
        long now = System.currentTimeMillis();
        assertScopeBelongToCurrentUser(scope);
        if (scope instanceof UserScopeDto) {
            UserScopeDto userScopeDto = (UserScopeDto) scope;
            basicScope = userScopeRepository.findByUserId(userScopeDto.getScopeBindings().getId());
            if (basicScope == null) {
                UserScope userScope = new UserScope();
                User userById = userService.getUserById(userScopeDto.getScopeBindings().getId());
                userScope.setScopeBindings(userById);
                userScope.setDateCreated(now);
                userScope.setDateModified(now);
                basicScope = userScopeRepository.save(userScope);
            }
        } else if (scope instanceof WorkGroupsScopeDto) {
            WorkGroupsScopeDto workGroupsScopeDto = (WorkGroupsScopeDto) scope;
            basicScope = workGroupsScopeRepository.findByName(workGroupsScopeDto.getName());
            if (basicScope == null) {
                WorkGroupsScope workGroupsScope = new WorkGroupsScope();
                workGroupsScope.setDescription(workGroupsScopeDto.getDescription());
                workGroupsScope.setName(workGroupsScopeDto.getName());
                workGroupsScope.setDateCreated(now);
                workGroupsScope.setDateModified(now);
                Set<WorkGroupDto> scopeBindings = workGroupsScopeDto.getScopeBindings();
                Set<WorkGroup> workGroups = Sets.newHashSet(workGroupRepository.findAllById(scopeBindings.stream().map(b -> b.getId()).collect(Collectors.toList())));
                workGroupsScope.setScopeBindings(workGroups);
                basicScope = workGroupsScopeRepository.save(workGroupsScope);
            }
        }
        return basicScope;
    }

    public BasicScope getByTypeAndId(Long id, String type) {
        if (Strings.isNullOrEmpty(type) || id == null) return null;
        if (type.equalsIgnoreCase("UserScope")) {
            return userScopeRepository.findById(id).orElse(null);
        } else if (type.equalsIgnoreCase("WorkGroupsScope")) {
            return workGroupsScopeRepository.findById(id).orElse(null);
        }
        return null;
    }

    @NotNull
    /**
     * Get all allowed scopes for query build (doc types and tags)
     *
     * first assert scope allowed for user if logged in
     *
     * 1) for logged in user, get its scope if exists and
     * if no scope parameter also add user's work group scopes (otherwise add param if work group)
     * 2) if no logged in user, add scope param if sent and is a workgroup scope
     */
    @Transactional
    public Set<Long> resolveScopeIds(BasicScope basicScope) {
        assertScopeBelongToCurrentUser(basicScope);
        Set<Long> scopeIds = new HashSet<>();
        boolean hasUser = (userService.getCurrentUser() != null && userService.getCurrentUser().getId() != null);
        UserScope userScope = !hasUser ? null : userScopeRepository.findByUserId(userService.getCurrentUser().getId());
        if (userScope != null) { // Add user scope if exists
            scopeIds.add(userScope.getId());
        }
        if (basicScope == null) { // Means scope is global, then look for user work group scopes
            List<BigInteger> workGroupScopeIds = !hasUser ? null : workGroupsScopeRepository.findWorkGroupScopeIds(userService.getCurrentUser().getId());
            if (workGroupScopeIds != null) {
                scopeIds.addAll(workGroupScopeIds.stream().map(BigInteger::longValue).collect(Collectors.toSet()));
            }
        } else if (basicScope instanceof WorkGroupsScope) { // Means the tag type associated to a specific work group scope
            scopeIds.add(basicScope.getId());
        }
        return scopeIds;
    }

    private void assertScopeBelongToCurrentUser(Scope scope) {
        // Case scope is not global then we check if user can use this given scope
        if (scope != null) {
            UserDto currentUser = userService.getCurrentUser();
            Long userId = extractUserId(scope);
            if (userId != null) {
                // Case passed user id is not the same of the current user then throw assertion error
                if (!userId.equals(currentUser.getId())) {
                    logger.error("UserScope passed with userId:{} is not of current user [id:{},name:{}]", userId, currentUser.getId(), currentUser.getName());
                    throw new RuntimeException(String.format("UserScope passed with userId:%d is not of current user %s", currentUser.getId(), currentUser.getName()));
                }
                return;
            }

            Set<Long> workgroupIds = extractWorkgroupIds(scope);
            if (workgroupIds != null) {
                User user = userService.getUserByName(currentUser.getUsername());

                // Case user has not assigned to any workgroup then throw assertion error
                if (user.getWorkGroups() == null || user.getWorkGroups().isEmpty()) {
                    logger.error("Current user [id:{},name:{}] is not assigned to any workgroup(s) at all", currentUser.getId(), currentUser.getName());
                    throw new RuntimeException(String.format("Current user %s is not assign to any workgroup(s) at all", currentUser.getName()));
                }

                // Collect all user workgroup ids
                Set<Long> existingUserWorkgroupIds = user.getWorkGroups().stream()
                        .map(BasicEntity::getId)
                        .collect(Collectors.toSet());

                // Case at least one passed wg is not part of the user workgroups then throw assertion error
                if (!workgroupIds.stream().allMatch(existingUserWorkgroupIds::contains)) {
                    logger.error("Current user [id:{},name:{}] is not part of all scope workgroup(s) {}", currentUser.getId(), currentUser.getName(), workgroupIds);
                    throw new RuntimeException(String.format("Current user %s is not part of all passed workgroup(s) %s", currentUser.getName(), workgroupIds));
                }
            }
        }

        // If reached here then all is OK
    }

    private Set<Long> extractWorkgroupIds(Scope scope) {
        if (scope instanceof WorkGroupsScope) {
            return ((WorkGroupsScope) scope).getScopeBindings().stream().map(BasicEntity::getId).collect(Collectors.toSet());
        }

        if (scope instanceof WorkGroupsScopeDto) {
            return ((WorkGroupsScopeDto) scope).getScopeBindings().stream().map(WorkGroupDto::getId).collect(Collectors.toSet());
        }
        return null;
    }

    private Long extractUserId(Scope scope) {
        if (scope instanceof UserScopeDto) {
            return ((UserScopeDto) scope).getScopeBindings().getId();
        }
        if (scope instanceof UserScope) {
            return ((UserScope) scope).getScopeBindings().getId();
        }
        return null;
    }
}
