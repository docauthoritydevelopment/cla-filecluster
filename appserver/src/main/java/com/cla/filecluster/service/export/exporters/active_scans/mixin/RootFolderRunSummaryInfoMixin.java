package com.cla.filecluster.service.export.exporters.active_scans.mixin;

import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class RootFolderRunSummaryInfoMixin {

    @JsonUnwrapped
    private RootFolderSummaryInfo rootFolderSummaryInfo;
}
