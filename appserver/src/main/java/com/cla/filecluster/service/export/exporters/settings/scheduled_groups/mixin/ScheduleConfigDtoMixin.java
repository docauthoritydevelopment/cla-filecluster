package com.cla.filecluster.service.export.exporters.settings.scheduled_groups.mixin;

import com.cla.common.domain.dto.schedule.ScheduleType;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.settings.scheduled_groups.ScheduledGroupsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class ScheduleConfigDtoMixin {

    @JsonProperty(TYPE)
    ScheduleType scheduleType;

    @JsonProperty(SCHEDULE_HOUR)
    int hour;

    @JsonProperty(SCHEDULE_MINUTES)
    int minutes;

    @JsonProperty(SCHEDULE_EVERY)
    int every;

    @JsonProperty(SCHEDULE_DAYS_OF_THE_WEEK)
    String[] daysOfTheWeek;
}
