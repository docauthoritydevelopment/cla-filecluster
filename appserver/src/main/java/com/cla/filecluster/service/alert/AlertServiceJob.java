package com.cla.filecluster.service.alert;

import com.cla.filecluster.repository.jpa.alert.AlertRepository;
import com.cla.filecluster.service.audit.EventAuditServiceJob;
import com.cla.filecluster.service.util.PartitionManagementJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Service to run scheduled job to create alert table partitions
 *
 * Created by: yael
 * Created on: 1/11/2018
 */
@Service
@Profile({ "prod", "win", "dev", "default" })
public class AlertServiceJob extends PartitionManagementJob {

    private Logger logger = LoggerFactory.getLogger(EventAuditServiceJob.class);

    @Value("${alert-manager.mng-partition-days-add-partition:7}")
    private int daysToAddPartition;

    @Value("${alert-manager.mng-partition-days-delete-partition:365}")
    private int daysToDeletePartition;

    @Autowired
    private AlertRepository alertRepository;

    @Scheduled(initialDelayString = "${audit-manager.mng-partition-initial-delay-millis:30000}",
            fixedRateString = "${audit-manager.mng-partition-frequency-millis:18000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void managePartitions() {
        managePartitions(daysToAddPartition, daysToDeletePartition, alertRepository);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
