package com.cla.filecluster.service.export.retrievers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import reactor.core.publisher.Flux;

import java.util.function.Supplier;



public interface SupplierIterableRetriever<T> extends Supplier<Iterable<T>>, Retriever<Iterable<T>> {

    default Flux<Iterable<T>> createFlux() {
        return Flux.zip(ReactiveSecurityContextHolder.getContext().flux(), Flux.defer(() -> Flux.just(this))) // use with subscriberContext near the subscription. see RxExportService.java
                .map(tuple -> {
                    LogHolder.log.trace("setting up security context:");
                    SecurityContextHolder.getContext().setAuthentication(tuple.getT1().getAuthentication());
                    return tuple.getT2().get();
                });
    }
}


class LogHolder {
    public static Logger log = LoggerFactory.getLogger(SupplierIterableRetriever.class);
}