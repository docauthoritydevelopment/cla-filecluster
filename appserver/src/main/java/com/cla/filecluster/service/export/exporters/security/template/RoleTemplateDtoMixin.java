package com.cla.filecluster.service.export.exporters.security.template;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.security.template.RoleTemplateHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class RoleTemplateDtoMixin {

    @JsonProperty(NAME)
    protected String name;

    @JsonProperty(DESCRIPTION)
    protected String description;

}
