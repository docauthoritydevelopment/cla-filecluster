package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.export.FileExportRequestDto;
import com.cla.common.domain.dto.progress.ProgressStatusDto;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.export.ExportResult;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.RxExportService;
import com.cla.filecluster.service.progress.ProgressBean;
import com.cla.filecluster.service.progress.ProgressService;
import com.cla.filecluster.service.progress.ProgressType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.io.*;


/**
 * created by yonatan on 25/09/2018
 */
@RestController()
@RequestMapping({"/api/exporter2", "/api/exporter"})
public class RxExporterApiController {
    private static final Logger logger = LoggerFactory.getLogger(RxExporterApiController.class);

    @Autowired
    private RxExportService exportService;

    @Autowired
    private ProgressService progressService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;


    @RequestMapping(value = "/export/{exportTypeStr}", method = RequestMethod.POST)
    public ProgressStatusDto export(@RequestBody FileExportRequestDto request,
                                    @PathVariable final String exportTypeStr) {

        ExportType exportType = ExportType.convertApiName(exportTypeStr)
                .orElseThrow(() -> new RuntimeException(String.format("export type: %s is unknown", exportTypeStr)));

        String artifactId = exportService.background(exportType, request);
        logger.info("export process of type {} started. Artifact id is: {}", exportType, artifactId);

        String typeAudit = messageHandler.getMessage("export."+exportTypeStr);
        eventAuditService.audit(AuditType.EXPORT, "Export of "+typeAudit, AuditAction.VIEW,  String.class, null, request);

        return progressService.getProgress(artifactId).toStatusDto();
    }

    @RequestMapping(value = "export/{exportId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable final String exportId) {
        // TODO...
        // get the subscribed flux
        // call cancel

        // remove resource from cache
        logger.info("removing artifact for export {}", exportId);
        exportService.markDownloaded(exportId);
        progressService.closeProgress(exportId);
    }

    @RequestMapping(value = "/download/{exportId}", method = RequestMethod.GET)
    public ResponseEntity<Resource> get(@PathVariable final String exportId) throws IOException {
        try {
            logger.debug("getting exported result stream for: {}", exportId);
            ExportResult result = exportService.getResults(exportId);

            Resource resource = result.getData();
            String fileName = result.getFileName()+"."+result.getExtension();
            MediaType mediaType = new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName)
                    .contentType(mediaType)
                    .contentLength(resource.getFile().length())
                    .body(new InputStreamResource(resource.getInputStream()));
        } finally {
            logger.info("removing artifact {} after download or error", exportId);
            delete(exportId);
        }
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Flux<ProgressStatusDto> list() {
        logger.trace("export progress request received.");
        return Flux.defer(() -> Flux.fromIterable(progressService.getProgresses(ProgressType.EXPORT))).map(ProgressBean::toStatusDto);
    }
}
