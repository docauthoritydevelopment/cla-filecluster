package com.cla.filecluster.service.department;

import com.cla.filecluster.service.importEntities.ImportItemRow;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
public class DepartmentItemRow extends ImportItemRow {

    private String fullName;

    private String description;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "DepartmentItemRow{" +
                "fullName='" + fullName + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
