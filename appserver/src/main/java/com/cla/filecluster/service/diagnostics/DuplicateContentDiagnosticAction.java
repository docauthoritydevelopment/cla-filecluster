package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.service.files.ContentMetadataService;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class DuplicateContentDiagnosticAction implements DiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(DuplicateContentDiagnosticAction.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Value("${diagnostics.duplicate-contents.start-after-min:1440}")
    private int startAfterMin;

    @Value("${diagnostics.duplicate-contents.event-limit:50}")
    private int cycleEventThreshold;

    @Value("${diagnostics.duplicate-contents.by-signature.enabled:true}")
    private boolean bySigEnabled;

    @Override
    public void runDiagnostic(Map<String, String> opData) {
        logger.debug("searching for duplicate contents");
        int duplicateFound = 0;

        if (!bySigEnabled) {
            logger.debug("searching for duplicate contents by signature is disabled - exit");
            return;
        }

        QueryResponse resp = contentMetadataService.runQuery(Query.create().setRows(0)
                .addFacetField(ContentMetadataFieldType.CONTENT_SIGNATURE)
                .setFacetMinCount(2)
                .setFacetLimit(cycleEventThreshold)
                .build());

        if (resp != null && resp.getFacetFields() != null && resp.getFacetFields().size() > 0) {
            FacetField field = resp.getFacetFields().get(0);
            for (FacetField.Count bucket : field.getValues()) {
                String hash = bucket.getName();
                Event event = Event.builder().withEventType(EventType.DUPLICATE_CONTENT)
                        .withEntry("CONTENT HASH", hash)
                        .withEntry("COUNT", bucket.getCount())
                        .build();
                eventBus.broadcast(Topics.DIAGNOSTICS, event);
                duplicateFound++;
                if (duplicateFound > cycleEventThreshold) {
                    logger.info("reached threshold for duplicate contents, stop");
                    return;
                }
            }
        }

        logger.debug("done searching for duplicate contents");
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.DUPLICATE_CONTENTS;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        List<Diagnostic> results = new ArrayList<>();
        results.add(addDiagnosticActionParts());
        return results;
    }

    public int getStartAfterMin() {
        return startAfterMin;
    }

}
