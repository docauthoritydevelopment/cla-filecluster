package com.cla.filecluster.service.report;

import com.cla.common.domain.dto.date.DateRangePointDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.SortType;
import com.cla.common.domain.dto.report.TimeResolutionType;
import com.cla.common.domain.dto.report.TrendChartDto;
import com.cla.common.domain.dto.report.TrendChartRecordedValue;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.report.TrendChart;
import com.cla.filecluster.domain.entity.report.TrendChartData;
import com.cla.filecluster.domain.entity.report.TrendChartSchedule;
import com.cla.filecluster.domain.specification.TrendChartDataSpecification;
import com.cla.filecluster.repository.jpa.report.TrendChartDataRepository;
import com.cla.filecluster.repository.jpa.report.TrendChartRepository;
import com.cla.filecluster.repository.jpa.report.TrendChartScheduleRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.filter.SavedFilterService;
import com.cla.filecluster.service.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by uri on 27/07/2016.
 */
@Service
public class TrendChartService {

    @Autowired
    private TrendChartRepository trendChartRepository;

    @Autowired
    private TrendChartDataRepository trendChartDataRepository;

    @Autowired
    private TrendChartScheduleRepository trendChartScheduleRepository;

    private static Logger logger = LoggerFactory.getLogger(TrendChartService.class);

    @Autowired
    private UserService userService;

    @Transactional(readOnly = false)
    public TrendChart updateTrendChart(TrendChartDto trendChartDto) {
        logger.info("Update trend chart {}", trendChartDto);
        return trendChartRepository.findById(trendChartDto.getId())
                .map(trendChart -> {
                    fillTrendChartFromDto(trendChartDto, trendChart);
                    return trendChart;
                })
                .map(trendChart -> saveTrendChart(trendChart))
                .orElse(null);

    }

    @Transactional(readOnly = false)
    public TrendChart createTrendChart(TrendChartDto newTrendChart) {
        logger.info("Create trend chart {}", newTrendChart);
        TrendChart trendChart = new TrendChart();

        fillTrendChartFromDto(newTrendChart, trendChart);

        return saveTrendChart(trendChart);
    }

    @Transactional(readOnly = true)
    public TrendChart saveTrendChart(TrendChart trendChart) {
        logger.debug("Creating trendChart {}", trendChart);
        TrendChart save = trendChartRepository.save(trendChart);
        return save;
    }

    public void fillTrendChartFromDto(TrendChartDto newTrendChart, TrendChart trendChart) {
        trendChart.setChartDisplayType(newTrendChart.getChartDisplayType());
        trendChart.setChartValueFormatType(newTrendChart.getChartValueFormatType());
        trendChart.setFilters(newTrendChart.getFilters());
        trendChart.setHidden(newTrendChart.isHidden());
        trendChart.setName(newTrendChart.getName());
        trendChart.setSortType(newTrendChart.getSortType() == null ? SortType.NAME : newTrendChart.getSortType());
        trendChart.setSeriesType(newTrendChart.getSeriesType());
        trendChart.setValueType(newTrendChart.getValueType());

        trendChart.setTrendChartCollectorId(newTrendChart.getTrendChartCollectorId());
        trendChart.setOwner(userService.getCurrentUserEntity());

        DateRangePointDto viewFromDto = newTrendChart.getViewFrom();
        DateRangePoint viewFrom = new DateRangePoint();
        if (viewFromDto != null) {
            fillDateRangePoint(viewFromDto, viewFrom);
        } else {
            fillDateRangePoint(new DateRangePointDto(System.currentTimeMillis() - 1000), viewFrom);
        }
        trendChart.setViewFrom(viewFrom);

        DateRangePointDto viewToDto = newTrendChart.getViewTo();
        if (viewToDto != null) {
            DateRangePoint viewTo = new DateRangePoint();
            fillDateRangePoint(viewToDto, viewTo);
            trendChart.setViewTo(viewTo);
        }

        trendChart.setResolutionType(newTrendChart.getResolutionType() == null ? TimeResolutionType.TR_miliSec : newTrendChart.getResolutionType());

        trendChart.setViewResolutionMs(newTrendChart.getViewResolutionMs());

        trendChart.setViewResolutionCount(newTrendChart.getViewResolutionCount());
        trendChart.setViewOffset(newTrendChart.getViewOffset());
        trendChart.setViewResolutionStep(newTrendChart.getViewResolutionStep());

        trendChart.setViewSampleCount(newTrendChart.getViewSampleCount());
        trendChart.setViewConfiguration(newTrendChart.getViewConfiguration());
    }

    public void fillDateRangePoint(DateRangePointDto viewFromDto, DateRangePoint viewFrom) {
        viewFrom.setType(viewFromDto.getType());
        viewFrom.setRelativePeriod(viewFromDto.getRelativePeriod());
        viewFrom.setRelativePeriodAmount(viewFromDto.getRelativePeriodAmount());
        if (viewFromDto.getAbsoluteTime() != null) {
            viewFrom.setAbsoluteTime(new Date(viewFromDto.getAbsoluteTime()));
        }
    }

    @Transactional(readOnly = true)
    public List<TrendChart> listTrendCharts(boolean includeDeleted) {
        if (includeDeleted) {
            return trendChartRepository.findAll();
        } else {
            return trendChartRepository.findNonDeleted();
        }
    }

    @Transactional(readOnly = false)
    public void markTrendChartAsDeleted(long id) {
        logger.debug("mark trend chart {} as deleted", id);
        TrendChart one = trendChartRepository.getOne(id);
        one.setDeleted(true);
    }

    @Transactional(readOnly = false)
    public void deleteTrendChartData(long id) {
        logger.debug("Delete trend chart {} data", id);
        int i = trendChartDataRepository.deleteByChartId(id);
        logger.debug("Deleted {} chart items", i);
    }

    @Transactional(readOnly = true)
    public TrendChart getTrendChart(long id) {
        return trendChartRepository.getOne(id);
    }

    public List<TrendChartData> getTrendChartData(long id, Long startTime, Long endTime) {
        TrendChart trendChart = trendChartRepository.getOne(id);
        logger.debug("Get trend chart {} view: {}-{}", trendChart, startTime, endTime);
        Specification<TrendChartData> specification = new TrendChartDataSpecification(id, startTime, endTime);
        List<TrendChartData> trendChartDataList = trendChartDataRepository.findAll(specification);
        return trendChartDataList;
    }

    @Transactional(readOnly = false)
    public TrendChartSchedule createTrendChartSchedule(String cronTriggerString) {
        logger.info("Create TrendChart Schedule {}", cronTriggerString);
        TrendChartSchedule trendChartSchedule = new TrendChartSchedule();
        trendChartSchedule.setName("default");
        trendChartSchedule.setActive(true);
        trendChartSchedule.setCronTriggerString(cronTriggerString);
        return trendChartScheduleRepository.save(trendChartSchedule);
    }

    @Transactional(readOnly = true)
    public TrendChartSchedule findFirstTrendChartSchedule() {
        List<TrendChartSchedule> all = trendChartScheduleRepository.findAll();
        return all.size() == 0 ? null : all.get(0);
    }

    @Transactional(readOnly = true)
    public TrendChartSchedule getTrendChartSchedule(Long trendChartScheduleId) {
        return trendChartScheduleRepository.getOne(trendChartScheduleId);
    }

//    @Transactional(readOnly = false)
//    public void collectTrendChartsData() {
//    }

    public FilterDescriptor[] getTrendChartFilters(TrendChart trendChart) {
        String filters = trendChart.getFilters();
        if (filters != null && !filters.isEmpty() && filters.startsWith("[")) {
            return convertFilterDescriptorJsonArray(filters);
        } else {
            FilterDescriptor filterDescriptor = SavedFilterService.convertFilterDescriptorJson(filters);
            return new FilterDescriptor[]{filterDescriptor};
        }
    }

    @Transactional(readOnly = false)
    public TrendChartData createTrendChartData(List<TrendChartRecordedValue> recordedValues
            , long requestedTimeStamp, TrendChart trendChart, boolean logMessage) {
        if (logMessage) {
            logger.debug("add {} recorded values for trendChart {}", recordedValues.size(), trendChart);
        }
        TrendChartData trendChartData = new TrendChartData();
        trendChartData.setRequestedTimeStamp(requestedTimeStamp);
        trendChartData.setActualTimeStamp(System.currentTimeMillis());
        trendChartData.setTrendChart(trendChart);
        trendChartData.setViewResolutionMs(trendChart.getViewResolutionMs());
        String value = convertTrendRecordedValuesToJson(recordedValues);
        trendChartData.setValue(value);
        return trendChartDataRepository.save(trendChartData);
    }

    @Transactional(readOnly = true)
    public TrendChart getTrendChartByName(String name, boolean flushSession) {
        if (flushSession) {
            trendChartRepository.flush();
        }
        List<TrendChart> byName = trendChartRepository.findByName(name);
        if (byName != null && byName.size() > 0) {
            return byName.get(0);
        }
        logger.debug("Failed to find trendChart by name {}", name);
        return null;
    }

    public boolean isFilesCollector(String seriesType) {
        return seriesType == null || "file.id".equalsIgnoreCase(seriesType) || "id".equalsIgnoreCase(seriesType);
    }

    public static String convertTrendRecordedValuesToJson(List<TrendChartRecordedValue> recordedValues) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(recordedValues);
        } catch (IOException e) {
            logger.error("Failed to convert TrendChart recorded values to json (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert TrendChart recorded values to json (IO Exception): " + e.getMessage());
        }
    }

    public static FilterDescriptor[] convertFilterDescriptorJsonArray(String filterDescriptonJsonArray) {
        try {
            //{"logic":null,"filters":null,"field":"docTypeId","value":1,"operator":{"present":true},"ignoreCase":true}
            return ModelDtoConversionUtils.getMapper().readValue(filterDescriptonJsonArray, FilterDescriptor[].class);
        } catch (IOException e) {
            logger.error("Failed to convert filterDescriptorJson (" + filterDescriptonJsonArray + ") to FilterDescriptor[] (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert filterDescriptorJson to FilterDescriptor[] (IO Exception): " + e.getMessage());
        }

    }
}
