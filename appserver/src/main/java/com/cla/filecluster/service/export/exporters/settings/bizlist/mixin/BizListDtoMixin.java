package com.cla.filecluster.service.export.exporters.settings.bizlist.mixin;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.filecluster.service.export.serialize.UpperToCamelCaseSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.settings.bizlist.BizListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class BizListDtoMixin {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(DESCRIPTION)
    private String description;

    @JsonProperty(TYPE)
    @JsonSerialize(using = UpperToCamelCaseSerializer.class)
    private BizListItemType bizListItemType;

    @JsonProperty(ACTIVE)
    private Boolean active;

}
