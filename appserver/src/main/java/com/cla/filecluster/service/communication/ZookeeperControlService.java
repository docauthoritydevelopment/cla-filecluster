package com.cla.filecluster.service.communication;

import com.cla.connector.utils.Pair;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Connecting to zookeeper and getting info
 *
 * Created by: yael
 * Created on: 10/28/2018
 */
@Service
public class ZookeeperControlService {

    private static final Logger logger = LoggerFactory.getLogger(ZookeeperControlService.class);

    @Value("${spring.data.solr.host}")
    private String solrServerURL;

    public Pair<Boolean, List<String>> getSolrNodes() {
        List<String> children = new ArrayList<>();
        Boolean isUp = true;
        ZooKeeper zk = null;
        String url = null;
        try {
            logger.debug("try get solr nodes from zk {}", solrServerURL);
            Watcher w = event -> {};

            String host = solrServerURL;

            if (host.contains("://")) {
                host = host.substring(host.indexOf("://")+3);
            }

            String[] parts = host.split("/");
            String address = parts[0];
            url = "/" + parts[1];

            zk = new ZooKeeper(address, 30000, w);
            children = zk.getChildren(url, w);
            if (children.contains("live_nodes")) {
                url += "/live_nodes";
                children = zk.getChildren(url, w);
                children = children.stream().map(c -> c.substring(0, c.indexOf(":"))).collect(Collectors.toList());
                logger.debug("got solr nodes from zk {}", children);
            } else {
                logger.debug("no live_nodes in zk {} with url {} children are {}", solrServerURL, url, children);
            }
        } catch (Exception e) {
            isUp = false;
            logger.error("cant get solr nodes from zk {} with url {}", solrServerURL, url, e);
        } finally {
            if (zk != null) {
                try {zk.close(); } catch (Exception e) {}
            }
        }
        return Pair.of(isUp, children);
    }
}
