package com.cla.filecluster.service.security;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.security.BizRoleDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.security.BizRole;
import com.cla.common.domain.dto.security.BizRoleSummaryInfoDto;
import com.cla.common.domain.dto.security.LdapGroupMappingDto;
import com.cla.filecluster.domain.entity.security.RoleTemplate;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.BizRoleRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by uri on 07/09/2016.
 */
@Service
public class BizRoleService {

    @Autowired
    private BizRoleRepository bizRoleRepository;

    @Autowired
    private LdapGroupMappingService ldapGroupMappingService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageHandler messageHandler;

    private static final Logger logger = LoggerFactory.getLogger(BizRoleService.class);

    @Transactional(readOnly = true)
    public BizRole createBizRole(BizRoleDto bizRoleDto) {
        validateBizRoleForCreation(bizRoleDto);
        BizRole bizRole = new BizRole();
        bizRole.setName(bizRoleDto.getName());
        bizRole.setDisplayName(bizRoleDto.getDisplayName());
        bizRole.setDescription(bizRoleDto.getDescription());
        bizRole.setTemplate(RoleTemplateService.convertRoleTemplateDto(bizRoleDto.getTemplate(), true));
        return bizRoleRepository.save(bizRole);
    }

    @Transactional(readOnly = true)
    public Set<BizRole> getBizRoles() {
        return bizRoleRepository.findNotDeleted();
    }

    @Transactional
    public BizRole createBizRole(String name, String displayName, String description, RoleTemplate template) {
        logger.debug("Create bizRole {}",name);
        BizRole bizRole = new BizRole();
        bizRole.setName(name);
        bizRole.setDisplayName(displayName);
        bizRole.setDescription(description);
        bizRole.setTemplate(template);
        BizRoleDto bizRoleDto = convertBizRole(bizRole);
        validateBizRoleForCreation(bizRoleDto);
        return bizRoleRepository.saveAndFlush(bizRole);
    }

    @Transactional
    public BizRoleDto archiveBizRole(long id) {
        Optional<BizRole> one = bizRoleRepository.findById(id);
        if (!one.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<User> connectedUsers = userService.getCompRoleAssignedUsers(id, dataSourceRequest.createPageRequest());
        if (!connectedUsers.getContent().isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.delete.has-users"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }

        BizRole role = one.get();
        logger.info("Archive bizRole {}",role);
        role.setDeleted(true);
        ldapGroupMappingService.deleteAllGroupMappingsForDaRole(role.getId());
        BizRole save = bizRoleRepository.save(role);
        return convertBizRole(save);
    }

    @Transactional
    public BizRole updateBizRole(Long id, String displayName, String description, RoleTemplate template) {
        Optional<BizRole> one = bizRoleRepository.findById(id);
        if (!one.isPresent()) {
            logger.error("Could not find BizRole id {}.", id);
            return null;
        }
        BizRole role = one.get();
        logger.info("Update bizRole {}",role);
        role.setDisplayName(displayName);
        role.setDescription(description);
        role.setTemplate(template);
        return bizRoleRepository.save(role);
    }

    @Transactional(readOnly = true)
    public BizRole getBizRole(Long bizRoleId) {
        return bizRoleRepository.getOneWithRoles(bizRoleId);
    }

    @Transactional(readOnly = true)
    public BizRole getBizRoleById(Long bizRoleId) {
        return bizRoleRepository.getOne(bizRoleId);
    }

    @Transactional(readOnly = true)
    public BizRole getBizRoleByName(String name) {
        return bizRoleRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    public Set<BizRole> getBizRolesByIds(Collection<Long> bizRoleIds) {
        return bizRoleRepository.findByIds(bizRoleIds);
    }

    @Transactional(readOnly = true)
    public List<BizRoleDto> getBizRolesByRoleTemplateId(Long roleTemplateId) {
        return bizRoleRepository.findByRoleTemplate(roleTemplateId).stream().map(BizRoleService::convertBizRole).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Page<BizRoleSummaryInfoDto> listBizRoles(DataSourceRequest dataSourceRequest) {
        logger.debug("List all biz roles ");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<BizRole> bizRoles = bizRoleRepository.findNotDeleted(pageRequest);
        Page<BizRoleSummaryInfoDto> bizRoleSummaryInfoDtos =
                convertBizRolesToSummaryInfo(bizRoles, pageRequest);
        bizRoleSummaryInfoDtos.forEach(r -> { // TODO Itai: this would be redundant with the impl of DAMAIN-2523
            List<LdapGroupMappingDto> mappingsByRoleId = ldapGroupMappingService.getMappingsByRoleId(r.getBizRoleDto().getId());
            r.setLdapGroupMappingDtos(mappingsByRoleId);
        });
        return bizRoleSummaryInfoDtos;
    }

    @Transactional
    public BizRoleSummaryInfoDto createBizRole(BizRoleDto bizRoleDto, List<LdapGroupMappingDto> ldapGroupMappingDtos) {
        logger.debug("Create biz role {} ", bizRoleDto);
        validateBizRoleForCreation(bizRoleDto);
        if (ldapGroupMappingDtos == null) {
            ldapGroupMappingDtos = new ArrayList<>();
        }

        BizRole bizRole = createBizRole(bizRoleDto);
        BizRoleSummaryInfoDto bizRoleSummaryInfoDto = new BizRoleSummaryInfoDto();
        bizRoleSummaryInfoDto.setBizRoleDto(convertBizRole(bizRole));
        logger.debug("Create biz role mappings {} ", ldapGroupMappingDtos);
        List<LdapGroupMappingDto> updatedMappings =
                ldapGroupMappingService.saveGroupMappingsForRole(bizRole, new HashSet<>(ldapGroupMappingDtos));
        bizRoleSummaryInfoDto.setLdapGroupMappingDtos(updatedMappings);
        return bizRoleSummaryInfoDto;
    }

    @Transactional
    public BizRoleSummaryInfoDto updateBizRole(BizRoleDto bizRoleDto, List<LdapGroupMappingDto> ldapGroupMappingDtos) {
        isValidObject(bizRoleDto);
        BizRole one = bizRoleRepository.getOne(bizRoleDto.getId());
        logger.debug("Update biz role {} ", bizRoleDto);
        if(!one.getName().equals(bizRoleDto.getName())) {
            isValidName(bizRoleDto);
        }
        if(!one.getDisplayName().equals(bizRoleDto.getDisplayName())) {
            isValidDisplayName(bizRoleDto);
        }
        if (ldapGroupMappingDtos == null) {
            ldapGroupMappingDtos = new ArrayList<>();
        }
        one.setName(bizRoleDto.getName());
        one.setDisplayName(bizRoleDto.getDisplayName());
        one.setDescription(bizRoleDto.getDescription());
        one.setTemplate(RoleTemplateService.convertRoleTemplateDto(bizRoleDto.getTemplate(), true));

        BizRole bizRole = bizRoleRepository.save(one);
        BizRoleSummaryInfoDto bizRoleSummaryInfoDto = new BizRoleSummaryInfoDto();
        bizRoleSummaryInfoDto.setBizRoleDto(convertBizRole(bizRole));
        logger.debug("Update biz role mapping {} ",ldapGroupMappingDtos);
        List<LdapGroupMappingDto> updatedMappings =
                ldapGroupMappingService.saveGroupMappingsForRole(bizRole, new HashSet<>(ldapGroupMappingDtos));
        bizRoleSummaryInfoDto.setLdapGroupMappingDtos(updatedMappings);
        return bizRoleSummaryInfoDto;
    }

    private void isValidName(BizRoleDto bizRoleDto) {
        BizRole roleByName = bizRoleRepository.findByNameAll(bizRoleDto.getName());
        if (roleByName != null) {
            if (roleByName.isDeleted()) {
                roleByName.setName(roleByName.getName()+System.currentTimeMillis());
                bizRoleRepository.saveAndFlush(roleByName);
            } else {
                throw new BadRequestException(messageHandler.getMessage("operational-role.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void isValidDisplayName(BizRoleDto bizRoleDto) {
        BizRole roleByDispName = bizRoleRepository.getBizRoleByDisplayNameAll(bizRoleDto.getDisplayName());
        if (roleByDispName != null) {
            if (roleByDispName.isDeleted()) {
                roleByDispName.setDisplayName(roleByDispName.getDisplayName()+System.currentTimeMillis());
                bizRoleRepository.saveAndFlush(roleByDispName);
            } else {
                throw new BadRequestException(messageHandler.getMessage("operational-role.display-name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void isValidObject(BizRoleDto bizRoleDto) {
        if (Strings.isNullOrEmpty(bizRoleDto.getName()) || Strings.isNullOrEmpty(bizRoleDto.getName().trim())) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.name.empty"), BadRequestType.MISSING_FIELD);
        }
        if (bizRoleDto.getTemplate() == null) {
            throw new BadRequestException(messageHandler.getMessage("operational-role.role-template.empty"), BadRequestType.MISSING_FIELD);
        }
        if (Strings.isNullOrEmpty(bizRoleDto.getDisplayName())) {
            logger.debug("Creating BizRole from BixToleDto with null displayName. Using name: {} instead", bizRoleDto.getName());
            bizRoleDto.setDisplayName(bizRoleDto.getName());
        }
    }

    private void validateBizRoleForCreation(BizRoleDto bizRoleDto) {
        isValidObject(bizRoleDto);
        isValidName(bizRoleDto);
        isValidDisplayName(bizRoleDto);
    }

    public static List<BizRoleDto> convertBizRoles(Collection<BizRole> bizRoles) {
        List<BizRoleDto> result = new ArrayList<>();
        for (BizRole bizRole : bizRoles) {
            result.add(convertBizRole(bizRole));
        }
        return result;
    }

    public static BizRoleDto convertBizRole(BizRole bizRole) {
        BizRoleDto bizRoleDto = new BizRoleDto();
        bizRoleDto.setDescription(bizRole.getDescription());
        bizRoleDto.setId(bizRole.getId());
        bizRoleDto.setDisplayName(bizRole.getDisplayName());
        bizRoleDto.setName(bizRole.getName());
        bizRoleDto.setDeleted(bizRole.isDeleted());
        bizRoleDto.setTemplate(RoleTemplateService.convertRoleTemplate(bizRole.getTemplate(), true));
        return bizRoleDto;
    }

    public static Page<BizRoleSummaryInfoDto> convertBizRolesToSummaryInfo(Page<BizRole> bizRoles, PageRequest pageRequest) {
        List<BizRole> entityPageContent = bizRoles.getContent();
        List<BizRoleSummaryInfoDto> dtoPageContent = new ArrayList<>(entityPageContent.size());
        for (BizRole bizRole : entityPageContent) {
            dtoPageContent.add(convertBizRoleToSummaryInfo(bizRole));
        }
        return new PageImpl<>(dtoPageContent, pageRequest, bizRoles.getTotalElements());
    }

    private static BizRoleSummaryInfoDto convertBizRoleToSummaryInfo(BizRole bizRole) {
        BizRoleSummaryInfoDto bizRoleSummaryInfoDto = new BizRoleSummaryInfoDto();
        bizRoleSummaryInfoDto.setBizRoleDto(convertBizRole(bizRole));
        return bizRoleSummaryInfoDto;
    }
}
