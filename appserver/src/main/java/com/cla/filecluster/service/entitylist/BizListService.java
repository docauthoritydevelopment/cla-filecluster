package com.cla.filecluster.service.entitylist;

import com.cla.common.domain.dto.bizlist.BizListDto;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.BizListSchema;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.entitylist.BizListRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by uri on 20/08/2015.
 */
@Service
public class BizListService {

    private static final Logger logger = LoggerFactory.getLogger(BizListService.class);

    @Value("${bizlist.clients.rules.file:./config/extraction/clients-extraction-rules.txt}")
    private String defaultClientRulesFileName;

    @Value("${bizlist.extraction.template.prefix:./config/extraction/extraction-rules-}")
    private String extractionRulesTemplatePrefix;

    private TimeSource timeSource = new TimeSource();

    @Autowired
    private BizListRepository bizListRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private EntityListSchemaLoader entityListSchemaLoader;

    private Map<Long, BizListSchema> entityListSchemaCache = new ConcurrentHashMap<>();

    private LoadingCache<Long, BizList> bizListsCache;

    private Object entityListSchemaCacheLock = new Object();

    @PostConstruct
    public void init() {
        bizListsCache = CacheBuilder.newBuilder()
                .maximumSize(20)
                .build(new CacheLoader<Long, BizList>() {
                    public BizList load(Long bizListId) {
                        return findById(bizListId);
                    }
                });
    }

    @Transactional(readOnly = false)
    public BizList findByName(String name, boolean withSchema) {
        BizList bizList = bizListRepository.findByName(name);
        if (withSchema) {
            populateSchema(bizList);
        }
        return bizList;
    }

    @Transactional(readOnly = false)
    public List<BizList> find(boolean withSchema) {
        List<BizList> bizLists = bizListRepository.find();
        if (withSchema) {
            populateSchema(bizLists);
        }
        return bizLists;
    }

    @Transactional(readOnly = true)
    public List<BizList> findActiveBizLists() {
        List<BizList> bizLists = bizListRepository.findActiveBizLists(true);
        return bizLists;
    }

    @Transactional
    public List<BizList> findByType(BizListItemType bizListItemType) {
        return bizListRepository.findByType(bizListItemType);
    }

    @Transactional
    public Page<BizList> findAll(Pageable pageable) {
        return bizListRepository.findAll(pageable);
    }


    @Transactional
    public BizList findByName(String name) {
        return findByName(name, false);
    }

    private void populateSchema(List<BizList> bizLists) {
        for (BizList bizList : bizLists) {
            populateSchema(bizList);
        }

    }

    @Transactional(readOnly = true)
    public BizList getBizListById(Long bizListId, boolean useCache) {
        if (useCache) {
            return bizListsCache.getUnchecked(bizListId);
        } else {
            return findById(bizListId);
        }
    }

    public BizListSchema loadCsvFile(byte[] rawData, BizListItemType bizListItemType, String template) {
        BizListSchema schema = entityListSchemaLoader.loadCsvSchema(rawData, bizListItemType, template);
        return schema;
    }

    private void populateSchema(BizList bizList) {
        if (bizList.getBizListSource() == null) {
            logger.debug("New style bizList - no need to populate schema");
            return;
        }
        long entityListId = bizList.getId();
        BizListSchema bizListSchema = entityListSchemaCache.get(entityListId);
        if (bizListSchema == null) {
            //We need to populate the list
            logger.debug("Populate the Entity List {} schema in the cache", entityListId);
            synchronized (entityListSchemaCacheLock) {
                if (entityListSchemaCache.containsKey(entityListId)) {
                    //Somebody filled it while we waited
                    logger.debug("Entity List {} schema already populated in the cache", entityListId);
                    bizList.setBizListSchema(entityListSchemaCache.get(entityListId));
                    return;
                }
                //Populate the schema and load into the cache
                logger.debug("Load schema for bizList {}", entityListId);
                BizListSchema schema = entityListSchemaLoader.loadCsvSchema(bizList);
                entityListSchemaCache.put(entityListId, schema);
            }
            logger.debug("Entity List {} schema populated in the cache", entityListId);
        }
        bizList.setBizListSchema(entityListSchemaCache.get(entityListId));
    }

    private String getRulesFileName(BizListItemType bizListItemType, String template) {
        if (bizListItemType == BizListItemType.CONSUMERS) {
            return extractionRulesTemplatePrefix + template + ".txt";
        }
        //TODO - support all list types
        return defaultClientRulesFileName;
    }

    @Transactional
    public BizList createBizList(BizListDto bizListDto) {
        BizList bizList = new BizList();
        bizList.setName(bizListDto.getName());
        bizList.setDescription(bizListDto.getDescription());
        bizList.setBizListItemType(bizListDto.getBizListItemType());
        bizList.setTemplate(bizListDto.getTemplate());
        bizList.setActive(bizListDto.getActive());
        bizList.setCreationTimeStamp(new java.util.Date().getTime());
        String rulesFileName = getRulesFileName(bizListDto.getBizListItemType(), bizListDto.getTemplate());
        bizList.setRulesFileName(rulesFileName);
        BizList save = bizListRepository.save(bizList);
        //Set bizList solr Id
        save.setBizListSolrId(FileDtoFieldConverter.BIZLIST_SOLR_PREFIX + save.getId());
        return bizListRepository.save(save);
    }

    @Transactional
    public BizList updateBizList(long bizListId, BizListDto bizListDto) {
        BizList bizList = bizListRepository.findById(bizListId).orElse(null);
        if (bizList == null) {
            throw new BadRequestException(messageHandler.getMessage("biz-list.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (bizList.getActive() != bizListDto.getActive()) {
            bizList.setLastUpdateTimeStamp(timeSource.currentTimeMillis());
        }
        bizList.setName(bizListDto.getName());
        bizList.setActive(bizListDto.getActive());
        bizList.setDescription(bizListDto.getDescription());

        BizList save = bizListRepository.save(bizList);
        bizListsCache.invalidate(bizListId);
        return save;
    }

    @Transactional
    public BizList updateBizListActive(long bizListId, boolean active) {
        BizList bizList = bizListRepository.findById(bizListId).orElse(null);
        if (bizList == null) {
            throw new BadRequestException(messageHandler.getMessage("biz-list.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (bizList.getActive() != active) {
            bizList.setLastUpdateTimeStamp(timeSource.currentTimeMillis());
        }
        bizList.setActive(active);
        BizList save = bizListRepository.save(bizList);
        bizListsCache.invalidate(bizListId);
        return save;
    }

    @Transactional
    public void deleteBizList(long bizListId) {
        bizListRepository.deleteById(bizListId);
    }

    @Transactional(readOnly = true)
    public BizList findById(long bizListId) {
        return bizListRepository.findById(bizListId).orElse(null);
    }

    @Transactional
    public BizList updateBizListExtractionSession(long bizListId, long extractionSessionId) {
        BizList bizList = bizListRepository.findById(bizListId).orElse(null);
        bizList.setExtractionSessionId(extractionSessionId);
        BizList save = bizListRepository.save(bizList);
        bizListsCache.invalidate(bizListId);
        return save;
    }

    @Transactional
    public void updateBizListUpdateDate(BizList bizList) {
        bizList.setLastUpdateTimeStamp(timeSource.currentTimeMillis());
        bizListRepository.save(bizList);
        bizListsCache.invalidate(bizList.getId());

    }

    @Transactional(readOnly = true)
    public Map<Long, BizList> getBizListsAsMap(List<Long> ids) {
        List<BizList> bizLists = bizListRepository.findIds(ids);
        Map<Long, BizList> result = new HashMap<>();
        for (BizList bizList : bizLists) {
            result.put(bizList.getId(), bizList);
        }
        return result;
    }

    @Transactional
    public void updateLastSuccessfulRunDate(Long bizListId) {
        long date = new java.util.Date().getTime();
        if (bizListRepository.updateLastSuccessfulRunDate(bizListId, date) == 0) {
            logger.warn("Failed to update extract finalize last successful date {} bizListId: {}", date, bizListId);
        }
        bizListsCache.invalidate(bizListId);
    }


}
