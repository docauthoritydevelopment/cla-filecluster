package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.RootFolderService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * delete a root folder, all its files, folders, associations etc
 *
 * Created by: yael
 * Created on: 11/6/2018
 */
@Service
public class DeleteRootFolderOperation implements ScheduledOperation {

    public static String RF_ID_PARAM = "ROOT_FOLDER_ID";

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            Long rfId = Long.parseLong(opData.get(RF_ID_PARAM));
            rootFolderService.deleteRootFolder(rfId);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String rfId = opData.get(RF_ID_PARAM);
        if (Strings.isNullOrEmpty(rfId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.root-folder.empty"), BadRequestType.MISSING_FIELD);
        }
        Long.parseLong(rfId);
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.DELETE_ROOT_FOLDER;
    }
}
