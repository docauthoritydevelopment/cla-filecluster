package com.cla.filecluster.service.api;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.joran.spi.JoranException;
import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.messages.*;
import com.cla.common.utils.LogExportUtils;
import com.cla.common.utils.tokens.NGramTokenizerHashed;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.config.security.FunctionalItemId;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.security.FunctionalItem;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.ExpressionLookupConsumerService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.files.ingest.ExpressionLookupResult;
import com.cla.filecluster.service.files.managers.FileClusterAppService;
import com.cla.filecluster.service.files.managers.FileContentService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.util.LocalFileUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import static com.cla.common.domain.dto.security.Authority.Const.*;

@RestController()
@RequestMapping("/api")
public class UiServicesApiController {

    private static final Logger logger = LoggerFactory.getLogger(UiServicesApiController.class);

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private FileClusterAppService fileClusterAppService;

    @Autowired
    private FileContentService fileContentService;

    @Autowired
    private ExpressionLookupConsumerService expressionLookupConsumerService;

    @Autowired
    private ExtractTokenizer extractTokenizer;

    @Autowired
    private NGramTokenizerHashed nGramTokenizerHashed;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private EventAuditService eventAuditService;

    @Value("${log.files-to-export:}")
    private String[] exportFiles;

    @Value("${log.top-index-to-export:1}")
    private int exportFilesTopIndex;

    @Value("${log.max-days-to-export:2}")
    private int exportFilesTopDays;

    @Value("${logging.config}")
    private String logbackConfigPath;

    @Value(("${file-content-service.file-get-timeout-min:3}"))
    private int fileGetTimeoutMinutes;

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/flush", method = RequestMethod.GET)
    public void flushLogs() {
        if (!flushLogback()) {
            for (int i = 0; i < 65; i++) {
                logger.debug("*************************************************************************************************************");
            }
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/fetch", method = RequestMethod.GET)
    public void fetchLogs(HttpServletResponse response) {
        logger.info("Fetch log files from FileCluster");
        flushLogs();
        try {
            List<Path> files = LogExportUtils.getFileListByRegex("./logs", ".*\\.log(\\.[1-5])?", 0);
            zipFilesToResponse(response, "DA_logs", files);
            logger.debug("log files fetched");
        } catch (IOException ex) {
            logger.info("Error writing log files to output stream.", ex);
            throw new RuntimeException(messageHandler.getMessage("log-fetch.write.error"));
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/reloadConfig", method = RequestMethod.GET)
    public String reloadLogsConfig(HttpServletResponse response) {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.reset();
        JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        try {
            configurator.doConfigure(logbackConfigPath);
        } catch (JoranException e) {
            logger.info("Error reloading logs config", e);
            throw new RuntimeException(messageHandler.getMessage("log-config.reload.failure"));
        }
        logger.info("Reloading logs config according to {}", logbackConfigPath);
        return messageHandler.getMessage("log-config.reload.ok");
    }


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/fetchErrMon", method = RequestMethod.GET)
    public void fetchErrMonLog(HttpServletResponse response) {
        logger.info("Fetch error-monitoring log from FileCluster");
        try {
            List<Path> files = LogExportUtils.getFileListByRegex("./logs", "fc_stdout.log(\\.[1-5])?", 0);
            zipFilesToResponse(response, "DA_errMon_log", files);
            logger.debug("fc_stdout.log file fetched");
        } catch (IOException ex) {
            logger.info("Error writing fc_stdout.log files to output stream.", ex);
            throw new RuntimeException(messageHandler.getMessage("log-fetch.write.error"));
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/fetchAll", method = RequestMethod.GET)
    public void fetchAllLogs(HttpServletResponse response) {
        logger.info("Fetch all logs");
        try {
            List<Path> files = LogExportUtils.getLogFilesToExport(exportFiles, exportFilesTopIndex, exportFilesTopDays);

            // create nd return zip
            zipFilesToResponse(response, "DA_all_logs", files);
        } catch (IOException ex) {
            logger.info("Error writing log files to output stream.", ex);
            throw new RuntimeException(messageHandler.getMessage("log-fetch.write.error"));
        }
    }

    @RequestMapping(value = "/version", method = {RequestMethod.GET})
    public String getVersion() {
        return opStateService.getVersion();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/catfile/clear/field", method = {RequestMethod.GET})
    public void clearSolrField(@RequestParam String fieldName) {
        CatFileFieldType catFileFieldType = CatFileFieldType.valueOf(fieldName);
        logger.warn("Clear Solr Field {}", fieldName);
        fileClusterAppService.clearSolrField(catFileFieldType);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/catfile/remove/value/field", method = {RequestMethod.GET})
    public void clearSolrField(@RequestParam String fieldName, @RequestParam String value) {
        CatFileFieldType catFileFieldType = CatFileFieldType.valueOf(fieldName);
        logger.warn("Remove Value {} from Solr Field {}", value, fieldName);
        fileClusterAppService.removeValueFromSolrField(catFileFieldType, value);
    }

    @RequestMapping(value = "/files/check-access/{id}", method = {RequestMethod.GET})
    @PreAuthorize("isFullyAuthenticated()")
    public boolean userAllowedToViewFile(@PathVariable("id") final long id){
        return fileContentService.userAllowedViewFile(id);
    }

    @RequestMapping(value = "/files/content/check-access/{id}", method = {RequestMethod.GET})
    @PreAuthorize("isFullyAuthenticated()")
    public boolean userAllowedDownloadFile(@PathVariable("id") final long id){
        return fileContentService.userAllowedDownloadFile(id);
    }

    @RequestMapping(value = "/files/content/{id}", method = {RequestMethod.GET})
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
    public void getFileContent(final HttpServletRequest request, final HttpServletResponse response,
                               @PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) final long id) throws IOException {
        logger.debug("Get File {} content", id);

        if (!fileContentService.userAllowedDownloadFile(id)) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().append(messageHandler.getMessage("file.access.unauthorize", Lists.newArrayList(id)));
            return;
        }

        try {
            CompletableFuture<FileContentResultMessage> completableFuture = new CompletableFuture<>();
            String requestId = fileContentService.addRequestToLockMap(completableFuture);
            fileContentService.extractFileContent(id, requestId);
            FileContentResultMessage fileContentResultMessage = completableFuture.get(fileGetTimeoutMinutes, TimeUnit.MINUTES);
            if (fileContentResultMessage.getRequestType().equals(FileContentType.ERROR)) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                response.getWriter().append(fileContentResultMessage.getPayload().getContent());
            } else {
                fileContentService.openFile(fileContentResultMessage, request, response, id);
            }
            eventAuditService.audit(AuditType.FILE, "download file", AuditAction.VIEW, ClaFile.class, Long.toString(id));
        } catch (BadRequestException e) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            response.getWriter().append(messageHandler.getMessage("file.missing", Lists.newArrayList(id)));
        } catch (TimeoutException e) {
            logger.error("Timeout exception while waiting for download file from media processor", e);
            response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);

        } catch (Exception e) {
            logger.error("Failed to download file ", e);
        }

    }

    @RequestMapping(value = "/files/ingest/{id}", method = {RequestMethod.GET})
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    public void ingestFileContent(final HttpServletRequest request, final HttpServletResponse response,
                                  @PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) final long id) throws IOException {
        logger.debug("Ingest File {} content", id);

        if (!fileContentService.userAllowedDownloadFile(id)) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().append(messageHandler.getMessage("file.access.unauthorize", Lists.newArrayList(id)));
            return;
        }

        try {
            CompletableFuture<IngestResultMessage> completableFuture = new CompletableFuture<>();
            String requestId = fileContentService.addIngestRequestToLockMap(completableFuture);
            fileContentService.doFileIngestOperationAsync(id, requestId, false);
            IngestResultMessage result = completableFuture.get(fileGetTimeoutMinutes, TimeUnit.MINUTES);
            if (result.hasErrors()) {
                response.getWriter().append(messageHandler.getMessage("file.ingest.failure", Lists.newArrayList(id)));
                logger.debug("ingest file {} returned with errors. result {}", id, result);
            } else if (result.getFileProperties().getFileSize() == 0) {
                response.getWriter().append(messageHandler.getMessage("file.empty", Lists.newArrayList(id)));
            } else {
                ExpressionLookupResult exp = expressionLookupConsumerService.expressionLookup(result);
                if (exp != null) {
                    String resultFileName = "file-" + id + "-expressionLookupResult.txt";
                    byte[] bytesToSend = exp.toExternalString().getBytes();
                    fileContentService.openFile(bytesToSend, resultFileName,
                            resultFileName, bytesToSend.length, request, response, id, null);
                } else {
                    logger.warn("Expression lookup is not supported for this file type.");
                    response.getWriter().append(messageHandler.getMessage("file.type.failure", Lists.newArrayList(id)));
                }
            }
            eventAuditService.audit(AuditType.FILE, "ingest file result", AuditAction.VIEW, ClaFile.class, Long.toString(id));
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            logger.error("Timeout exception while waiting for download file from media processor", e);
            response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
        } catch (Exception e) {
            logger.error("Failed to ingest file ", e);
            response.getWriter().append(messageHandler.getMessage("file.ingest.failure", Lists.newArrayList(id)));
        }
    }

    @RequestMapping(value = "/files/tokens/{id}", method = {RequestMethod.GET})
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    public void ingestFileContentToken(final HttpServletRequest request, final HttpServletResponse response,
                                       @PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) final long id, @RequestParam("phrase") String phrase) throws IOException {
        logger.debug("Ingest File {} content", id);

        if (!fileContentService.userAllowedDownloadFile(id)) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getWriter().append(messageHandler.getMessage("file.access.unauthorize", Lists.newArrayList(id)));
            return;
        }

        try {
            CompletableFuture<IngestResultMessage> completableFuture = new CompletableFuture<>();
            String requestId = fileContentService.addIngestRequestToLockMap(completableFuture);
            fileContentService.doFileIngestOperationAsync(id, requestId, true);
            IngestResultMessage result = completableFuture.get(fileGetTimeoutMinutes, TimeUnit.MINUTES);
            if (result.hasErrors()) {
                response.getWriter().append(messageHandler.getMessage("file.ingest.failure", Lists.newArrayList(id)));
                logger.debug("ingest file {} returned with errors. result {}", id, result);
            } else if (result.getFileProperties().getFileSize() == 0) {
                response.getWriter().append(messageHandler.getMessage("file.empty", Lists.newArrayList(id)));
            } else {
                ClaFilePropertiesDto fileProperties = result.getFileProperties();
                FileType fileType = fileProperties.getType();
                StringBuilder sb = new StringBuilder();
                sb.append("you searched for the phrase '");
                sb.append(phrase);

                String hashed = extractTokenizer.getHash(phrase);
                sb.append("'\n hashed (extractTokenizer.getHash): ");
                sb.append(hashed);

                hashed = nGramTokenizerHashed.getHash(phrase, null);
                sb.append("'\n hashed CORRECT (nGramTokenizerHashed.getHash): ");
                sb.append(hashed);

                sb.append("\n in file ");
                sb.append(result.getFileProperties().getFileName());
                sb.append("\n\n===========================REVERSE DICTIONARY===========================\n\n");
                IngestMessagePayload payload = (IngestMessagePayload)result.getPayload();
                //noinspection unchecked
                sb.append(getMapAsString(payload.getReverseDictionary()));
                sb.append("\n\n===========================CONTENT===========================\n\n");
                switch (fileType) {
                    case WORD:
                    case PDF:
                        WordIngestResult word = (WordIngestResult) result.getPayload();
                        sb.append(word.getOriginalContent());
                        sb.append("\n\n===========================TOKENS===========================\n\n");
                        sb.append(word.getContent());
                        break;
                    case EXCEL:
                        ExcelIngestResult excelWorkbook = (ExcelIngestResult) result.getPayload();
                        for (ExcelSheetIngestResult sheet : excelWorkbook.getSheets()) {
                            sb.append(sheet.getOrigContentTokens());
                        }
                        sb.append("\n\n===========================TOKENS===========================\n\n");
                        for (ExcelSheetIngestResult sheet : excelWorkbook.getSheets()) {
                            sb.append(sheet.getContentTokens());
                        }
                        break;
                    default:
                        OtherFile otherFile = (OtherFile) result.getPayload();
                        sb.append(otherFile.getExtractedContent());
                }

                String resultFileName = "file-" + id + "-ingestResult.txt";
                byte[] bytesToSend = sb.toString().getBytes();
                fileContentService.openFile(bytesToSend, resultFileName,
                        resultFileName, bytesToSend.length, request, response, id, null);
                eventAuditService.audit(AuditType.FILE, "ingest file token result", AuditAction.VIEW, ClaFile.class, Long.toString(id));
            }
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            logger.error("Timeout exception while waiting for download file from media processor", e);
            response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
        } catch (Exception e) {
            logger.error("Failed to ingest file ", e);
            response.getWriter().append(messageHandler.getMessage("file.ingest.failure", Lists.newArrayList(id)));
        }
    }

    private String getMapAsString(Map<Long, String> map) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Map.Entry<Long, String> entry : map.entrySet()) {
            String hashedStr = String.format("%X", entry.getKey());
            sb.append(hashedStr);
            sb.append("=");
            sb.append(entry.getValue());
            sb.append(" ");
        }
        sb.append("}");
        return sb.toString();
    }

    //-------------------------- private methods -----------------------------------------------

    private boolean flushLogback() {
        boolean res = false;
        if (logger instanceof ch.qos.logback.classic.Logger) {
            res = true;
            ch.qos.logback.classic.Logger lbLogger = (ch.qos.logback.classic.Logger) logger;
            Map<String, Integer> ftMap = new HashMap<>();
            Iterator<Appender<ILoggingEvent>> appendersIterator = lbLogger.iteratorForAppenders();
            while (appendersIterator.hasNext()) {
                Appender<ILoggingEvent> appender = appendersIterator.next();
                if (appender instanceof ch.qos.logback.core.AsyncAppenderBase) {
                    ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent> lbApp = (ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent>) appender;
                    ftMap.put(lbApp.getName(), lbApp.getMaxFlushTime());
                    lbApp.setMaxFlushTime(0);
                } else {
                    logger.info("Found appender {} of class {}", appender.getName(), appender.getClass());
                }
            }
            lbLogger.info("*****************************************************************************************");
            lbLogger.info("****************************** Flushing logger ************************ ({})",
                    ftMap.entrySet().stream()
                            .map(e -> e.getKey() + ":" + e.getValue())
                            .collect(Collectors.joining(",", "{", "}")));
            lbLogger.info("*****************************************************************************************");
            appendersIterator = lbLogger.iteratorForAppenders();
            while (appendersIterator.hasNext()) {
                Appender<ILoggingEvent> appender = appendersIterator.next();
                if (appender instanceof ch.qos.logback.core.AsyncAppenderBase) {
                    ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent> lbApp = (ch.qos.logback.core.AsyncAppenderBase<ILoggingEvent>) appender;
                    Integer mft = ftMap.get(lbApp.getName());
                    if (mft != null) {
                        logger.debug("Restore appender {} to {}", lbApp.getName(), mft);
                        lbApp.setMaxFlushTime(mft);
                    } else {
                        logger.debug("Could not Restore appender {} MaxFlushTime", lbApp.getName());
                    }
                }
            }
        }
        return res;
    }

    private void zipFilesToResponse(HttpServletResponse response, String zipFilePrefix, List<Path> files) throws IOException {
        if (files == null || files.isEmpty()) {
            return;
        }

        response.setContentType("application/zip");
        final Date curDate = new Date();
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_hhmmss");
        final String suffix = format.format(curDate);
        final String headerKey = "Content-Disposition";
        final String headerValue = String.format("attachment; filename=\"%s.%s.zip\"", zipFilePrefix, suffix);
        response.setHeader(headerKey, headerValue);

        logger.debug("zip each log file in <{}> into {}", files, headerValue);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(baos);
        files.forEach(f -> {
            try {
                LocalFileUtils.addFileToZip(f, zipOutputStream);
            } catch (IOException e) {
                logger.error("Failed to add log file " + f + " to zip stream", e);
            }
        });
        logger.debug("close zip stream");
        zipOutputStream.close();
        baos.close();
        logger.debug("write response");
        response.setContentLength(baos.size());
        response.getOutputStream().write(baos.toByteArray());
        response.getOutputStream().flush();
    }

    @RequestMapping(value = "/healthCheck", method = RequestMethod.GET)
    public Object healthCheck() {
        return Response.ok().build();
    }

}
