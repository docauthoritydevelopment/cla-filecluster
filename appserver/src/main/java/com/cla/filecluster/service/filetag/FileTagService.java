package com.cla.filecluster.service.filetag;

import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagMinimalDto;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.*;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.filetag.FileTagRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.util.ValidationUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by uri on 24/08/2015.
 */
@Service
public class FileTagService {

    private static final Logger logger = LoggerFactory.getLogger(FileTagService.class);

    @Autowired
    private FileTagRepository fileTagRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private MessageHandler messageHandler;

    private LoadingCache<Long, Optional<FileTagDto>> tagsCache = null;

    @PostConstruct
    void init() {
        tagsCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, Optional<FileTagDto>>() {
                            public Optional<FileTagDto> load(Long key) {
                                return dbTemplateUtils.doInTransaction(() -> {
                                    FileTag tag = getFileTag(key);
                                    return Optional.ofNullable(convertFileTagToDto(tag));
                                });
                            }
                        });

        dbTemplateUtils.doInTransaction(() -> {
            fileTagRepository.findAllFileTags().forEach(tag -> {
                tagsCache.put(tag.getId(), Optional.ofNullable(convertFileTagToDto(tag)));
            });
        });
    }

    public FileTagDto getFromCache(Long tagId) {
        FileTagDto dto = tagsCache.getUnchecked(tagId).orElse(null);
        return dto == null ? null : new FileTagDto(dto);
    }

    @Transactional
    public FileTag createFileTag(String uniqueName, String description, String patternName, FileTagType type, FileTagAction action) {
        User user = userService.getCurrentUserEntity();
        long userId = (user == null) ? 1 : user.getId();

        return createFileTag(uniqueName, description, patternName, type, action, userId);
    }

    public FileTag createFileTag(String uniqueName, String description, String patternName, FileTagType type, FileTagAction action, long userId) {
        FileTag fileTag = new FileTag();
        fileTag.setName(uniqueName);
        if (type.getName().equals("user")) {
            //Make sure the identifier is unique yet still helps us on debugging the FileTag
            fileTag.setIdentifier(userId + "_" + System.currentTimeMillis());
        } else {
            fileTag.setIdentifier(generateFileTagIdentifier(patternName, type.getName()));
        }
        fileTag.setAction(action);
        fileTag.setDescription(description);
        fileTag.setOwnerId(userId);
        fileTag.setType(type);
        fileTag = fileTagRepository.save(fileTag);
        return fileTag;
    }

    public String generateFileTagIdentifier(String name, String typeName) {
        String identifier = name + "_" + typeName;
        if (!ValidationUtils.validateNameLength(identifier)) {
            return identifier.substring(0, ValidationUtils.LIMIT_NAME_LENGTH);
        }
        return identifier;
    }

    public String[] splitFileTagIdentifier(String name) {
        if (!name.contains("_")) {
            return null;
        }
        return name.split("_");
    }

    @Transactional
    public FileTag createFileTag(String name, String description, @NotNull FileTagType fileTagType, DocType docType, String entityId, FileTagAction action) {
        if (fileTagType == null) {
            logger.error("FileTagType is null - can't create FileTag {}", name);
            throw new BadRequestException(messageHandler.getMessage("tag.type.empty"), BadRequestType.MISSING_FIELD);
        }
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            throw new BadRequestException(messageHandler.getMessage("tag.name.empty"), BadRequestType.MISSING_FIELD);
        }
        FileTag fileTag = createFileTag(name, description, name, fileTagType, action);
        fileTag.setDocTypeContext(docType);
        fileTag.setEntityIdContext(entityId);
        return fileTagRepository.save(fileTag);
    }

    @Transactional(readOnly = true)
    public FileTag getFileTag(FileTagType fileTagType, DocType docType, String entityId, FileTagAction action) {
        if (docType != null && entityId != null) {
            return fileTagRepository.findOneByUniqueIdentifiers(fileTagType, docType, entityId, action);
        } else if (docType != null) {
            return fileTagRepository.findOneByUniqueIdentifiers(fileTagType, docType, action);
        } else if (entityId != null) {
            return fileTagRepository.findOneByUniqueIdentifiers(fileTagType, entityId, action);
        }
        return fileTagRepository.findOneByUniqueIdentifiers(fileTagType, action);
    }

    @Transactional(readOnly = true)
    public FileTag getFileTag(long tagId) {
        return fileTagRepository.findOneWithDocTypeContext(tagId);
    }

    @Transactional(readOnly = true)
    public List<FileTag> getFileTags() {
        return fileTagRepository.findAllFileTags();
    }

    @Transactional(readOnly = true)
    public List<FileTag> getFileTagsByType(Long typeId) {
        return fileTagRepository.findFileTagsByType(typeId);
    }

    @Transactional(readOnly = true)
    public List<FileTag> getSensitiveFileTags() {
        return fileTagRepository.findSensitiveFileTags();
    }

    @Transactional(readOnly = true)
    public FileTagDto getFileTagDto(long tagId) {
        try {
            FileTag fileTag = fileTagRepository.findOneWithDocTypeContext(tagId);
            return convertFileTagToDto(fileTag);
        } catch (RuntimeException e) {
            logger.error("Exception while trying to get File Tag " + tagId + " details: " + e.getMessage(), e);
            throw e;
        }
    }

    @Transactional(readOnly = true)
    public Set<FileTag> getFileTagsById(List<Long> tagIds) {
        List<FileTag> fileTags = findByIds(tagIds);
        return Sets.newHashSet(fileTags);
    }

    @Transactional(readOnly = true)
    public Map<Long, FileTag> getFileTagsByIdAsMap(List<Long> tagIds) {
        List<FileTag> fileTags = findByIds(tagIds);
        return fileTags.stream().collect(Collectors.toMap(FileTag::getId, g -> g));
    }

    @Transactional(readOnly = true)
    public List<FileTag> findByIds(List<Long> ids) {
        return fileTagRepository.findFileTagsById(ids);
    }

    @Transactional(readOnly = true)
    public FileTag getFileTagByName(String name) {
        return fileTagRepository.findOneByName(name);
    }

    @Transactional(readOnly = true)
    public List<FileTag> getFileTagsByNames(Collection<String> names) {
        return fileTagRepository.findTagsByNames(Lists.newArrayList(names));
    }

    @Transactional(readOnly = true)
    public FileTag getFileTagByName(String name, FileTagType fileTagType) {
        return fileTagRepository.findOneByNameAndType(name, fileTagType);
    }

    @Transactional(readOnly = true)
    public FileTag getFileTagByName(String name, long fileTagTypeId) {
        return fileTagRepository.findOneByNameAndType(name, fileTagTypeId);
    }

    @Transactional(readOnly = true)
    public List<FileTag> getFileTagChildren(long fileTagId) {
        return fileTagRepository.findFileTagChildren(fileTagId);
    }

    @Transactional
    public FileTag updateFileTagName(FileTag fileTag, String name) {
        logger.info("Update file tag {} name from [{}] to [{}]", fileTag.getId(), fileTag.getName(), name);
        fileTag.setName(name);
        tagsCache.invalidate(fileTag.getId());
        return fileTagRepository.save(fileTag);
    }

    @Transactional
    public FileTag updateFileTagDescription(FileTag fileTag, String description) {
        logger.info("Update file tag {} description from [{}] to [{}]", fileTag.getId(), fileTag.getDescription(), description);
        fileTag.setDescription(description);
        tagsCache.invalidate(fileTag.getId());
        return fileTagRepository.save(fileTag);
    }

    @Transactional
    public FileTag updateFileTagAction(FileTag fileTag, FileTagAction fileTagAction) {
        logger.info("Update file tag {} action from [{}] to [{}]", fileTag.getId(), fileTag.getAction(), fileTagAction);
        fileTag.setAction(fileTagAction);
        tagsCache.invalidate(fileTag.getId());
        return fileTagRepository.save(fileTag);
    }

    @Transactional
    public FileTagDto deleteFileTag(long fileTagId) {
        Optional<FileTag> fileTag = fileTagRepository.findById(fileTagId);

        if (!fileTag.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("file-tag.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        return fileTag.map(ft -> {
                    logger.debug("Delete fileTag {} with id {}", ft.getName(), fileTagId);
                    ft.setDeleted(true);
                    fileTagRepository.save(ft);
                    tagsCache.invalidate(ft.getId());
                    return ft;
                })
                .map(FileTagService::convertFileTagToDto).orElse(null);
    }

    public List<FileTag> findByTagIdentifiers(List<String> tagIdentifiers) {
        return fileTagRepository.findFileTagsByIdentifiers(tagIdentifiers);
    }


    public static List<FileTagDto> convertFileTagsToDto(List<FileTag> fileTags) {
        return fileTags.stream().map(FileTagService::convertFileTagToDto).collect(Collectors.toList());
    }

    public static Set<FileTagDto> convertFileTagsToDto(Set<FileTag> tags) {
        Set<FileTagDto> fileTagDtos = new HashSet<>();
        tags.forEach(tag -> fileTagDtos.add(FileTagService.convertFileTagToDto(tag)));
        return fileTagDtos;
    }

    public static FileTagDto convertFileTagToDto(FileTag fileTag) {
        if (fileTag == null) {
            return null;
        }
        FileTagDto fileTagDto = new FileTagDto();
        fileTagDto.setId(fileTag.getId());
        fileTagDto.setName(fileTag.getName());
        fileTagDto.setDescription(fileTag.getDescription());
        FileTagTypeDto fileTagTypeDto = FileTagTypeService.convertFileTagTypeToDto(fileTag.getType());
        fileTagDto.setType(fileTagTypeDto);
        fileTagDto.setFileTagAction(fileTag.getAction());
        if (fileTag.getDocTypeContext() != null) {
            fileTagDto.setDocTypeContextId(fileTag.getDocTypeContext().getId());
            fileTagDto.setDocTypeContextName(fileTag.getDocTypeContext().getName());
        }
        fileTagDto.setEntityIdContextId(fileTag.getEntityIdContext());
        return fileTagDto;
    }

    public static Set<FileTagMinimalDto> convertFileTagsDtoToMinimal(Collection<FileTagDto> fileTagDtos) {
        Set<FileTagMinimalDto> result = new HashSet<>();
        for (FileTagDto fileTagDto : fileTagDtos) {
            result.add(convertFileTagDtoToMinimal(fileTagDto));
        }

        return result;
    }

    private static FileTagMinimalDto convertFileTagDtoToMinimal(FileTagDto fileTagDto) {
        return new FileTagMinimalDto(fileTagDto.getId(), fileTagDto.getName(), fileTagDto.getType());
    }
}
