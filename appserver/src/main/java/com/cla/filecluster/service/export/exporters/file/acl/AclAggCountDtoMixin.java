package com.cla.filecluster.service.export.exporters.file.acl;

import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class AclAggCountDtoMixin {

    @JsonUnwrapped
    FileUserDto item;

    @JsonProperty(FileCountHeaderFields.NUM_OF_FILES)
    private Integer count;
}
