package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Component

public class FolderEnricher implements DefaultTypeDataEnricher {
    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private DocStoreService docStoreService;

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        List<String> ids = new ArrayList<>(dashboardChartDataDto.getCategories());
        List<Long> actualIds = ids.stream().map(Long::parseLong).collect(Collectors.toList());
        List<DocFolder> folders = docFolderService.findByIds(actualIds);
        Set<Long> rootFolderIds = folders.stream()
                .map(DocFolder::getRootFolderId)
                .collect(Collectors.toSet());

        Map<Long, RootFolder> rfs = docStoreService.getRootFoldersCached(rootFolderIds).stream()
                .collect(Collectors.toMap(RootFolder::getId, r -> r));

        Map<Long, DocFolder> fMap = folders.stream().collect(
                Collectors.toMap(DocFolder::getId,
                        x -> x));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            DocFolder dFolder = fMap.get(Long.parseLong(category));
            RootFolder rFolder = rfs.get(dFolder.getRootFolderId());
            return rFolder.getNickName() + dFolder.getPath();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return CatFileFieldType.FOLDER_ID.getSolrName();
    }

}
