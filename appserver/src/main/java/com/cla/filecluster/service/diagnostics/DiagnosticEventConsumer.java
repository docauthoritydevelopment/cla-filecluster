package com.cla.filecluster.service.diagnostics;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by: yael
 * Created on: 5/5/2019
 */
public abstract class DiagnosticEventConsumer implements Consumer<Event> {

    @Autowired
    private EventBus eventBus;

    @PostConstruct
    void init() {
        eventBus.subscribe(Topics.DIAGNOSTICS, this);
    }

    @Override
    public void accept(Event event) {
        List<EventType> eventTypes = getHandledEventTypes();
        if (eventTypes != null && !eventTypes.contains(event.getEventType())) {
            return;
        }
        handleEvent(event);
    }

    abstract List<EventType> getHandledEventTypes();

    public abstract void handleEvent(Event event);
}
