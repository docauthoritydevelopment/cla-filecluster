package com.cla.filecluster.service.export.exporters.security.bizrole;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public interface BizRoleHeaderFields {
    String NAME = "Name";
    String DESCRIPTION = "Description";
    String LDAP_GROUP = "LDAP group";
    String ROLE_TEMPLATE = "Role template";
}
