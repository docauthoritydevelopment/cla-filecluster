package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.CatFileFieldType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DuplicateFilesDiagnosticAction implements DiagnosticAction {
    private static final Logger logger = LoggerFactory.getLogger(DuplicateFilesDiagnosticAction.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Value("${diagnostics.duplicate-files.start-after-min:1440}")
    private int startAfterMin;

    @Value("${diagnostics.duplicate-files.event-limit:50}")
    private int cycleEventThreshold;

    @Value("${diagnostics.duplicate-files.by-ids.enabled:true}")
    private boolean byIdsEnabled;

    @Value("${diagnostics.duplicate-files.by-path.enabled:true}")
    private boolean byPathEnabled;

    @Override
    public void runDiagnostic(Map<String, String> opData) {
        logger.debug("searching for duplicate files");
        int duplicateFound = 0;

        // duplicate file ids //////////////////////////
        if (byIdsEnabled) {
            QueryResponse resp = solrFileCatRepository.runQuery(Query.create()
                    .addFacetField(CatFileFieldType.FILE_ID)
                    .setRows(0)
                    .setFacetLimit(cycleEventThreshold)
                    .setFacetMinCount(2).build());

            if (resp != null && resp.getFacetFields() != null && resp.getFacetFields().size() > 0) {
                FacetField facetField = resp.getFacetFields().get(0);
                for (FacetField.Count count : facetField.getValues()) {
                    Event event = Event.builder().withEventType(EventType.DUPLICATE_FILE)
                            .withEntry("FILE ID", count.getName())
                            .withEntry("COUNT", count.getCount())
                            .build();
                    eventBus.broadcast(Topics.DIAGNOSTICS, event);
                    duplicateFound++;
                    if (limitReached(duplicateFound)) {
                        return;
                    }
                }
            }
        } else {
            logger.debug("searching for duplicate files by ids is disabled");
        }

        if (!byPathEnabled) {
            logger.debug("searching for duplicate files by path is disabled - exit");
            return;
        }

        // duplicate path //////////////////////////
        List<RootFolder> rfs = docStoreService.getRootFolders();
        List<Long> rfIds = rfs == null ? Lists.newArrayList() :
                rfs.stream().filter(rf -> rf.getFirstScan() != null && !rf.getFirstScan())
                        .map(RootFolder::getId).collect(Collectors.toList());

        for (Long rfId : rfIds) {
            QueryResponse resp = solrFileCatRepository.runQuery(Query.create().setRows(0)
                    .addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rfId))
                    .addFacetField(CatFileFieldType.FILE_NAME_HASHED)
                    .setFacetMinCount(2)
                    .setFacetLimit(cycleEventThreshold)
                    .build());
            
            Map<String, Long> possibleDuplicates = new HashMap<>();
            if (resp != null && resp.getFacetFields() != null && resp.getFacetFields().size() > 0) {
                FacetField field = resp.getFacetFields().get(0);
                field.getValues().forEach(bucket -> {
                        String hash = bucket.getName();
                        possibleDuplicates.put(hash, rfId);
                    });

            }

            for (Map.Entry<String, Long> dup : possibleDuplicates.entrySet()) {
                List<SolrFileEntity> files = solrFileCatRepository.find(Query.create()
                        .addQuery(Criteria.create(CatFileFieldType.FILE_NAME_HASHED, SolrOperator.EQ, dup.getKey()))
                        .addQuery(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, dup.getValue()))
                        .addField(CatFileFieldType.FULL_NAME)
                        .build());
                Set<String> names = files.stream().map(SolrFileEntity::getFullName).collect(Collectors.toSet());
                if (names.size() < files.size()) {
                    Event event = Event.builder().withEventType(EventType.DUPLICATE_FILE)
                            .withEntry("FILE HASH", dup.getKey())
                            .withEntry("ROOT FOLDER ID", dup.getValue())
                            .build();
                    eventBus.broadcast(Topics.DIAGNOSTICS, event);
                    duplicateFound++;
                    if (limitReached(duplicateFound)) {
                        return;
                    }
                }
            }
        }

        logger.debug("done searching for duplicate files");
    }

    private boolean limitReached(int counter) {
        if (counter > cycleEventThreshold) {
            logger.info("reached threshold for duplicate files, stop");
            return true;
        }
        return false;
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.DUPLICATE_FILES;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        List<Diagnostic> results = new ArrayList<>();
        results.add(addDiagnosticActionParts());
        return results;
    }

    public int getStartAfterMin() {
        return startAfterMin;
    }

}