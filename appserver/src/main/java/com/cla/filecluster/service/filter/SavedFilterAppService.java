package com.cla.filecluster.service.filter;

import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.scope.Scope;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.filter.SavedFilter;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.service.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by uri on 08/11/2015.
 */
@Service
public class SavedFilterAppService {

    @Autowired
    private SavedFilterService savedFilterService;

    @Autowired
    private UserService userService;

    @Autowired
    private ScopeService scopeService;

    private Logger logger = LoggerFactory.getLogger(SavedFilterAppService.class);

    public SavedFilterDto getSavedFilterByName(String name) {
        return convertSavedFilter(savedFilterService.getSavedFilterByName(name));
    }

    @Transactional
    public SavedFilterDto createSavedFilter(String name, String rawFilter, FilterDescriptor desc, boolean global, Scope scope) {
        User owner = null;
        if (global) {
            logger.debug("Create global filter {} with rawFilter {}",name,rawFilter);
        }
        else {
            owner = userService.getCurrentUserEntity();
            logger.debug("Create filter {} with json {} for user {}",name,rawFilter,owner);
        }
        SavedFilter savedFilter = savedFilterService.createSavedFilter(name, rawFilter, desc, owner, scope);
        return convertSavedFilter(savedFilter);
    }

    public static SavedFilterDto convertSavedFilter(SavedFilter savedFilter) {
        if (savedFilter == null) return null;
        SavedFilterDto savedFilterDto = new SavedFilterDto();
        savedFilterDto.setId(savedFilter.getId());
        savedFilterDto.setName(savedFilter.getName());
        savedFilterDto.setRawFilter(savedFilter.getRawFilter());
        savedFilterDto.setFilterDescriptor(savedFilter.getFilterDescriptor());
        savedFilterDto.setScope(AssociationUtils.convertBasicScopeToDto(savedFilter.getScope()));
        savedFilterDto.setGlobalFilter(savedFilter.getOwner() == null);
        return savedFilterDto;
    }

    public SavedFilter convertSavedFilter(SavedFilterDto savedFilterDto) {
        if (savedFilterDto == null) return null;
        SavedFilter savedFilter = new SavedFilter();
        savedFilter.setId(savedFilterDto.getId());
        savedFilter.setName(savedFilterDto.getName());
        savedFilter.setRawFilter(savedFilterDto.getRawFilter());
        savedFilter.setFilterDescriptor(savedFilterDto.getFilterDescriptor());
        BasicScope basicScope = savedFilterDto.getScope() == null ? null : scopeService.getOrCreateScope(savedFilterDto.getScope());
        savedFilter.setScope(basicScope);
        return savedFilter;
    }

    private Page<SavedFilterDto> convertSavedFilters(Page<SavedFilter> savedFilters,PageRequest pageRequest) {
        List<SavedFilterDto> result = savedFilters.getContent().stream().map(SavedFilterAppService::convertSavedFilter).collect(Collectors.toList());
        return new PageImpl<>(result,pageRequest,savedFilters.getTotalElements());
    }

    @Transactional(readOnly = true)
    public SavedFilterDto getById(long id) {
        return convertSavedFilter(savedFilterService.getById(id));
    }

    @Transactional(readOnly = true)
    public Page<SavedFilterDto> listSavedFiltes(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<SavedFilter> savedFilters = savedFilterService.listSavedFilters(pageRequest);
        return convertSavedFilters(savedFilters,pageRequest);
    }

    @Transactional
    public void deleteSavedFilter(long id) {
        savedFilterService.deleteSavedFilter(id);
    }

    @Transactional
    public SavedFilterDto updateSavedFilter(SavedFilterDto updateSaveFilterData) {
        SavedFilter updatedFilter = savedFilterService.updateSavedFilterData(
                updateSaveFilterData.getId(),updateSaveFilterData.getName(),
                updateSaveFilterData.getRawFilter(), updateSaveFilterData.getFilterDescriptor(), updateSaveFilterData.getScope());
        return convertSavedFilter(updatedFilter);
    }
}
