package com.cla.filecluster.service.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This controller is a work around so we can see swagger json in installed environments
 * we assume the problem is the swagger is in http while the ui runs in https
 * so the credentials do not pass to it and we get unauthorized error (401) accessing swagger
 * we want to create this new path under /api so that the node js knows to pass the credentials
 * and all it does is forward the request to swagger
 * to use it when ssl is on in the ui call: https://localhost:9000/api/v2/api-docs
 *
 * Created by: yael
 * Created on: 28/11/2017
 */
@Controller
public class SwaggerController {
    @RequestMapping("/api/v2/api-docs")
    public void myMethod(HttpServletRequest request, HttpServletResponse response) throws Exception {
        RequestDispatcher rd = request.getRequestDispatcher("/v2/api-docs");
        rd.forward(request, response);
    }
}
