package com.cla.filecluster.service.files.managers;

import com.cla.common.domain.dto.crawler.FileCrawlPhase;
import com.cla.common.domain.dto.crawler.FileCrawlingStateDto;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.exceptions.StoppedByUserException;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@Component
@ManagedResource(objectName = "cla:name=opStateService")
public class OpStateService {

    private static final Logger logger = LoggerFactory.getLogger(OpStateService.class);

    private static final String VERSION_PROPERTIES_FILE = "." + File.separator + "config" + File.separator + "version.properties";

    private String opMsg = "";
    private RunStatus lastRunStatus = RunStatus.NA;
    private String lastRunError = "";
    private boolean fileCrawlerRunning = false;

    private boolean pauseProcessing = false;

    private long scanCounter = 0;
    private long duplicateCounter = 0;
    private long scanStartTime = 0;
    private long analyzeStartTime = 0;
    private Long sleepingThreadsCount = 0L;
    private String lastErrorMsg = "";

    private Long runId;

    private double averageAnalyzeThroughput = 0;
    private double averageScanThroughput = 0;

    private Long lastRunStartTime;
    private Long lastRunStopTime;

    private long currentPhaseProgressCounter;
    private long currentPhaseProgressFinishMark;
    private FileCrawlPhase currentPhase;
    private double currentPhaseRate;
    private long currentPhaseStartTime;
    private int currentPhaseErrors;

    //cpr - Current Phase Rate processing
    private long cprPeriodMs0 = 0;
    private long cprPeriodMs1 = 0;
    private long cprCount0 = 0;
    private long cprCount1 = 0;
    private Double currentPhaseAvgRunningRate = 0d;
    private long cprLastPeriodMs = 0;

    private boolean calculateAverageRate = true;
    private boolean stopOnError = false;

    private Long scannedRootFolderId;
    private Long scheduledGroupId;

    private long messageCounter;


    public void setScheduledGroupId(Long scheduledGroupId) {
        if (scheduledGroupId == null) {
            logger.debug("No schedule group in use");
        }
        else {
            logger.debug("Schedule group in use: {}",scheduledGroupId);
        }
        this.scheduledGroupId = scheduledGroupId;
    }

    public void fillLastRunDetails(CrawlRun lastRun) {
        runId = lastRun.getId();
        scheduledGroupId = lastRun.getScheduleGroupId();
        scannedRootFolderId = lastRun.getRootFolderId();
        lastRunStatus = lastRun.getRunStatus();
        lastRunStartTime = lastRun.getStartTimeAsLong();
        lastRunStopTime = lastRun.getStopTimeAsLong();
    }


    public void checkIfPause() {
        while (pauseProcessing) {
            try {
                logger.warn("System is paused");
                Thread.sleep(20000);
            } catch (InterruptedException e) {
                logger.warn("Pause interrupted by Interrupted Exception");
                return;
            }
        }
    }

    public void setRunId(Long runId) {
        this.runId = runId;
    }


    class IntervalTimerExecutor {
        Timer timer;
        long seconds;

        public IntervalTimerExecutor(int seconds) {
            timer = new Timer("OpState-calc", true);
            this.seconds = seconds;
            timer.scheduleAtFixedRate(new IntervalTask(), seconds * 1000, seconds * 1000);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        intervalTimerExecutor.timer.cancel();
        super.finalize();
    }

    class IntervalTask extends TimerTask {
        public void run() {
//    		logger.debug("timer interval");
            processCurrentPhaseRate();
        }
    }


    // Update running rate every 15 seconds
    private IntervalTimerExecutor intervalTimerExecutor = new IntervalTimerExecutor(15);

    private String version;

    @Value("${subprocess:false}")
    private boolean subProcess;

    @ManagedAttribute
    public void processCurrentPhaseRate() {
        processCurrentPhaseRunningRate(cprPeriodMs0 == 0);
    }

    public void processCurrentPhaseRunningRate(boolean newPhase) {
        if (!calculateAverageRate) {
            return;
        }
        if (newPhase) {
            synchronized (currentPhaseAvgRunningRate) {
                cprPeriodMs1 = System.currentTimeMillis();
                cprPeriodMs0 = cprPeriodMs1;
                currentPhaseAvgRunningRate = 0d;
                cprLastPeriodMs = 0;
                cprCount1 = getCurrentPhaseProgressCounter();
            }
            return;
        }
        cprCount0 = cprCount1;
        cprCount1 = getCurrentPhaseProgressCounter();
        long deltaCount = cprCount1 - cprCount0;
        if (deltaCount > 0) {
            cprPeriodMs0 = cprPeriodMs1;
        }
        cprPeriodMs1 = System.currentTimeMillis();
        long deltaMs = cprPeriodMs1 - cprPeriodMs0;
        if (deltaMs > 0) {
            synchronized (currentPhaseAvgRunningRate) {
                currentPhaseAvgRunningRate = (1000d * deltaCount) / deltaMs;
                cprLastPeriodMs = deltaMs;
                if (deltaCount > 0) {
                    logger.debug("Current phase running rate {}", currentPhaseAvgRunningRate);
                }
            }
        }
    }



    public void setOpMsg(final String opMsg) {
        this.opMsg = opMsg;
        logger.trace("setOpMsg: {}", opMsg);
    }

    @ManagedAttribute
    public long getScanCounter() {
        return scanCounter;
    }

    public void setScanCounter(final long scanCounter) {
        this.currentPhaseProgressCounter = scanCounter;
        this.scanCounter = scanCounter;
    }

    @ManagedAttribute
    public long getDuplicateCounter() {
        return duplicateCounter;
    }


    @ManagedAttribute
    public String getLastErrorMsg() {
        return lastErrorMsg;
    }


    public void incrementSleepingThreads() {
        synchronized (sleepingThreadsCount) {
            this.sleepingThreadsCount++;
        }
    }

    public void decrementSleepingThreads() {
        synchronized (sleepingThreadsCount) {
            this.sleepingThreadsCount--;
        }
    }

    @ManagedAttribute
    public long getSleepingThreadsCount() {
        return sleepingThreadsCount;
    }

    public void incrementDuplicateCounter() {
        ++duplicateCounter;
    }

    public void updateAvarageAnalyzeThroughput(long analyzeCounter) {
        double seconds = (System.currentTimeMillis() - getAnalyzeStartTime()) / 1000d;
        if (seconds > 0d) {
            this.averageAnalyzeThroughput = (double) analyzeCounter / seconds;
        } else {
            logger.debug("updateAvarageAnalyzeThroughput got 0 seconds");
        }
    }

    public void updateAvarageScanThroughput(long scanCounter) {
        double seconds = (System.currentTimeMillis() - getScanStartTime()) / 1000d;
        if (seconds > 0d) {
            this.averageScanThroughput = (double) scanCounter / seconds;
        } else {
            logger.debug("updateAvarageScanThroughput got 0 seconds");
        }
        updateCurrentPhaseRate();
    }


    public void resetAnalyzeStartTime() {
        analyzeStartTime = System.currentTimeMillis();
    }

    @ManagedAttribute
    public double getAverageAnalyzeThroughput() {
        return averageAnalyzeThroughput;
    }


    @ManagedAttribute
    public long getAnalyzeStartTime() {
        return analyzeStartTime;
    }

    public void resetScanStartTime() {
        scanStartTime = System.currentTimeMillis();
    }

    @ManagedAttribute
    public long getScanStartTime() {
        return scanStartTime;
    }

    @ManagedAttribute
    public double getAverageScanThroughput() {
        return averageScanThroughput;
    }

    public void informRunStarted(Long runId) {
        logger.info("FileCrawler is now running");
        fileCrawlerRunning = true;
        lastRunStartTime = System.currentTimeMillis();
        lastRunStopTime = null;
        lastRunStatus = RunStatus.NA;
        scannedRootFolderId = null;
        scheduledGroupId = null;
         this.runId = runId;
    }

    public void updateCurrentPhaseState(FileCrawlingStateDto fileCrawlingStateDto) {
        if (fileCrawlingStateDto.getRate() != null) {
            this.currentPhaseRate = fileCrawlingStateDto.getRate();
        }
        if (fileCrawlingStateDto.getRunningRate() != null) {
            this.currentPhaseAvgRunningRate = fileCrawlingStateDto.getRunningRate();
        }
        this.currentPhaseProgressFinishMark = fileCrawlingStateDto.getCurrentPhaseProgressFinishMark();
        this.currentPhaseProgressCounter = fileCrawlingStateDto.getCurrentPhaseProgressCounter();
        this.currentPhaseErrors = fileCrawlingStateDto.getErrorsCount();
        String stateString = fileCrawlingStateDto.getStateString();
        if (stateString != null && stateString.startsWith("fileProcessor:")) {
            this.opMsg = stateString;
        }
        else {
            this.opMsg = "fileProcessor: " + stateString;
        }
    }

    public void informRunStarted(RootFolderDto rootFolderDto, Long runId) {
        informRunStarted(runId);
        scannedRootFolderId = rootFolderDto.getId();
    }

    @ManagedAttribute
    public Long getLastRunStartTime() {
        return lastRunStartTime;
    }

    public void informRunStoppedOnException(Throwable e) {
        logger.info("Run Stopped on exception: ", e);
        fileCrawlerRunning = false;
        lastRunStopTime = System.currentTimeMillis();
        lastRunStatus = RunStatus.FAILED;
        String stackTrace = ExceptionUtils.getStackTrace(e);
        lastRunError = "Exception: " + e.getMessage() + "\n" + stackTrace;
        setCurrentPhase(FileCrawlPhase.NOT_RUNNING);
    }

    public void informRunStoppedOnError(Error e) {
        logger.info("Run Stopped on error: ", e);
        fileCrawlerRunning = false;
        lastRunStopTime = System.currentTimeMillis();
        lastRunStatus = RunStatus.FAILED;
        String stackTrace = ExceptionUtils.getStackTrace(e);
        lastRunError = "ERROR: " + e.getMessage() + "\n" + stackTrace;
        setCurrentPhase(FileCrawlPhase.NOT_RUNNING);
    }

    public void informRunFinished() {
        logger.info("Run Finished");
        fileCrawlerRunning = false;
        lastRunStopTime = System.currentTimeMillis();
        lastRunStatus = RunStatus.FINISHED_SUCCESSFULLY;
        setCurrentPhase(FileCrawlPhase.NOT_RUNNING);
    }

    public void informRunStoppedByUser() {
        logger.info("Run stopped by user");
        fileCrawlerRunning = false;
        lastRunStopTime = System.currentTimeMillis();
        lastRunStatus = RunStatus.STOPPED;
        setCurrentPhase(FileCrawlPhase.NOT_RUNNING);
    }



    @ManagedAttribute
    public Long getLastRunStopTime() {
        return lastRunStopTime;
    }

    @ManagedAttribute
    public RunStatus getLastRunStatus() {
        return lastRunStatus;
    }

    @ManagedAttribute
    public String getLastRunError() {
        return lastRunError;
    }

    @ManagedAttribute
    public boolean isFileCrawlerRunning() {
        return fileCrawlerRunning;
    }

    @ManagedAttribute
    public long getCurrentPhaseProgressFinishMark() {
        return currentPhaseProgressFinishMark;
    }

    public void setCurrentPhaseProgressFinishMark(long currentPhaseProgressFinishMark) {
        this.currentPhaseProgressFinishMark = currentPhaseProgressFinishMark;
    }

    public void clearCurrentPhaseProgressFinishMark() {
        setCurrentPhaseProgressFinishMark(0);
    }

    @ManagedAttribute
    public long getCurrentPhaseProgressCounter() {
        return currentPhaseProgressCounter;
    }

    @Deprecated
    public void incrementCurrentPhaseCounter() {
        currentPhaseProgressCounter++;
    }

    public void incrementCurrentPhaseCounter(long amount) {
        currentPhaseProgressCounter += amount;
        updateCurrentPhaseRate();
    }

    public void incrementCurrentPhaseCounter(long amount, String hint) {
        currentPhaseProgressCounter += amount;
        updateCurrentPhaseRate();
    }

    public void incrementMessageCounter(int size) {
        messageCounter+=size;
    }

    @ManagedAttribute
    public long getMessageCounter() {
        return messageCounter;
    }

    public void setCurrentPhaseProgressCounter(long currentPhaseProgressCounter) {
        if (currentPhaseProgressCounter < this.currentPhaseProgressCounter) {
            this.currentPhaseProgressCounter = currentPhaseProgressCounter;
            // Sub-phase progress calc reset. Avoid negative rates
            currentPhaseRate = 0;
            processCurrentPhaseRunningRate(true);
            return;
        }
        this.currentPhaseProgressCounter = currentPhaseProgressCounter;
        updateCurrentPhaseRate();
    }

    public void setCurrentPhase(FileCrawlPhase currentPhase) {
        logger.info("Set current phase to {}", currentPhase);
        this.currentPhase = currentPhase;
        currentPhaseStartTime = System.currentTimeMillis();
        currentPhaseProgressCounter = 0;
        currentPhaseRate = 0;
        currentPhaseProgressFinishMark = 0;
        currentPhaseErrors = 0;
        processCurrentPhaseRunningRate(true);
    }

    public FileCrawlPhase getCurrentPhase() {
        return currentPhase;
    }


    @ManagedAttribute
    public double getCurrentPhaseRate() {
        return currentPhaseRate;
    }

    public void updateCurrentPhaseRate() {
        if (currentPhaseStartTime > 0) {
            Long duration = (System.currentTimeMillis() - currentPhaseStartTime) + 1;
            this.currentPhaseRate = 1000d * currentPhaseProgressCounter / duration.doubleValue();
        }
    }

    @ManagedAttribute
    public long getCurrentPhaseStartTime() {
        return currentPhaseStartTime;
    }

    @ManagedAttribute
    public String getVersion() {
        if (version == null) {
            String fileName = VERSION_PROPERTIES_FILE;
            version = readVersionFromFile(fileName);
        }
        return version;
    }

    private String readVersionFromFile(String fileName) {
        String readString = null;
        try {
            FileReader fr = new FileReader(fileName);
            char[] cbuf = new char[100];
            int read = fr.read(cbuf);
            readString = new String(cbuf, 0, read);
        }
        catch (IOException e) {
            logger.error("Failed to open " + VERSION_PROPERTIES_FILE + " file", e);
        }
        return readString;
    }

    @ManagedAttribute
    public int getCurrentPhaseErrors() {
        return currentPhaseErrors;
    }

    public void addCurrentPhaseError() {
        currentPhaseErrors++;
    }

    @ManagedAttribute
    public boolean isStopOnError() {
        return stopOnError;
    }

    public void setStopOnError(boolean stopOnError) {
        this.stopOnError = stopOnError;
    }

    @ManagedAttribute
    public Long getRunId() {
        return runId;
    }

    public void checkIfStopRequested() {
        if (Thread.currentThread().isInterrupted()) {
            logger.warn("Thread was interrupted - stopping execution");
            if (lastRunStatus == null ||lastRunStatus.equals(RunStatus.STOPPED)) {
                throw new StoppedByUserException("Thread was interrupted - stopping execution");
            }
            else {
                throw new RuntimeException("Thread was interrupted by something else - stopping execution");
            }
        }
    }

    public void setCalculateAverageRate(boolean calculateAverageRate) {
        this.calculateAverageRate = calculateAverageRate;
    }

}
