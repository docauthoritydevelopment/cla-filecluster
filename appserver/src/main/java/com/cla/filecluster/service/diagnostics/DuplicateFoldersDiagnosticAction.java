package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DuplicateFoldersDiagnosticAction implements DiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(DuplicateFoldersDiagnosticAction.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private SolrCatFolderRepository solrFolderRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Value("${diagnostics.duplicate-folders.start-after-min:1440}")
    private int startAfterMin;

    @Value("${diagnostics.duplicate-folders.event-limit:50}")
    private int cycleEventThreshold;

    @Value("${diagnostics.duplicate-folders.by-path.enabled:true}")
    private boolean byPathEnabled;

    @Override
    public void runDiagnostic(Map<String, String> opData) {
        logger.debug("searching for duplicate folders");
        int duplicateFound = 0;

        if (!byPathEnabled) {
            logger.debug("searching for duplicate folders by path is disabled - exit");
            return;
        }

        // duplicate path //////////////////////////
        List<RootFolder> rfs = docStoreService.getRootFolders();
        List<Long> rfIds = rfs == null ? Lists.newArrayList() :
                rfs.stream().filter(rf -> rf.getFirstScan() != null && !rf.getFirstScan())
                        .map(RootFolder::getId).collect(Collectors.toList());

        for (Long rfId : rfIds) {
            QueryResponse resp = solrFolderRepository.runQuery(Query.create().setRows(0)
                    .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rfId))
                    .addFacetField(CatFoldersFieldType.FOLDER_HASH)
                    .setFacetMinCount(2)
                    .setFacetLimit(cycleEventThreshold)
                    .build());

            Map<String, Long> possibleDuplicates = new HashMap<>();
            if (resp != null && resp.getFacetFields() != null && resp.getFacetFields().size() > 0) {
                FacetField field = resp.getFacetFields().get(0);
                field.getValues().forEach(bucket -> {
                    String hash = bucket.getName();
                    possibleDuplicates.put(hash, rfId);
                });
            }

            for (Map.Entry<String, Long> dup : possibleDuplicates.entrySet()) {
                List<SolrFolderEntity> folders = solrFolderRepository.find(Query.create()
                        .addQuery(Criteria.create(CatFoldersFieldType.FOLDER_HASH, SolrOperator.EQ, dup.getKey()))
                        .addQuery(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, dup.getValue()))
                        .addField(CatFoldersFieldType.REAL_PATH)
                        .build());
                Set<String> names = folders.stream().map(SolrFolderEntity::getRealPath).collect(Collectors.toSet());
                if (names.size() < folders.size()) {
                    Event event = Event.builder().withEventType(EventType.DUPLICATE_FOLDER)
                            .withEntry("FOLDER HASH", dup.getKey())
                            .withEntry("ROOT FOLDER ID", dup.getValue())
                            .build();
                    eventBus.broadcast(Topics.DIAGNOSTICS, event);
                    duplicateFound++;
                    if (duplicateFound > cycleEventThreshold) {
                        logger.info("reached threshold for duplicate folders, stop");
                        return;
                    }
                }
            }
        }

        logger.debug("done searching for duplicate folders");
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.DUPLICATE_FOLDERS;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        List<Diagnostic> results = new ArrayList<>();
        results.add(addDiagnosticActionParts());
        return results;
    }

    public int getStartAfterMin() {
        return startAfterMin;
    }
}
