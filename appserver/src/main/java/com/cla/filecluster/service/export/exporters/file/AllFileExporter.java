package com.cla.filecluster.service.export.exporters.file;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
@Slf4j
@Component
public class AllFileExporter extends PageCsvExcelExporter<AggregationCountItemDTO<String>> {

    private final static String[] HEADER = {NAME, NUM_OF_FILES};

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AllFilesAggCountDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<String>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        Long res = filesApplicationService.countFilesDeNormalized(dataSourceRequest);
        AggregationCountItemDTO<String> item = new AggregationCountItemDTO<>("Total files", res == null ? 0 : res);
        return new PageImpl<>(Lists.newArrayList(item));
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<String> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.ALL_FILES);
    }
}
