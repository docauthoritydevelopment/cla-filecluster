package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.common.domain.dto.security.LdapConnectionDetailsDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.LdapUtils;
import com.cla.filecluster.domain.entity.security.LdapConnectionDetails;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.LdapConnectionDetailsRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.convertion.DataEncryptionService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ldap.AuthenticationException;
import org.springframework.ldap.CommunicationException;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.SpringSecurityLdapTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.naming.directory.SearchControls;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Itai Marko.
 */
@Service
public class LdapConnectionDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(LdapConnectionDetailsService.class);

    @Value("${ldap-connection.ssl.cert.disable-validation:false}")
    private Boolean disableSslCertValidation;

    @Value("${ldap-connection.ssl.crypto-protocol.name:TLS}")
    private String commCryptoProtocolName;

    @Value("${ldap.test-connection.enable-credentials-validation:true}")
    private Boolean enableCredentialsValidationInConnectionTest;

    @Autowired
    private LdapConnectionDetailsRepository ldapConnectionDetailsRepository;

    @Autowired
    private DataEncryptionService dataEncryptionService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private LdapUtils ldapUtils;

    @PostConstruct
    public void init() {
        if (disableSslCertValidation) {
            disableSslCertValidation();
        }
    }

    @Transactional(readOnly = true)
    LdapConnectionDetails getConnectionDetailsByName(String name) {
        return ldapConnectionDetailsRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    LdapConnectionDetails getAnyEnabled() {
        List<LdapConnectionDetails> content = getEnabledLdapConnections();
        return (content.size() == 0) ? null : content.get(0);
    }

    @Transactional(readOnly = true)
    public LdapConnectionDetailsDto getAnyEnabledDto() {
        LdapConnectionDetails ldapConnectionDetails = getAnyEnabled();
        if (ldapConnectionDetails == null) {
            return null;
        }
        return convertLdapConnectionDetails(ldapConnectionDetails);
    }

    @Transactional(readOnly = true)
    public Page<LdapConnectionDetailsDto> getAllLdapConnectionDtos(PageRequest pageRequest) {
        Page<LdapConnectionDetails> ldapConnectionDetailsPage = ldapConnectionDetailsRepository.findAll(pageRequest);
        return convertLdapConnectionsDetailsPage(ldapConnectionDetailsPage, false);
    }

    @Transactional(readOnly = true)
    public Optional<LdapConnectionDetailsDto> getLdapConnectionDtosById(long id) {
        return ldapConnectionDetailsRepository.findById(id).map(ldapConnectionDetails -> {
            LdapConnectionDetailsDto dto = convertLdapConnectionDetails(ldapConnectionDetails);
            dto.setManagerPass(null);
            return dto;
        });
    }

    @Transactional(readOnly = true)
    public List<LdapConnectionDetailsDto> getEnabledLdapConnectionDtos(boolean includePasswordInDto) {
        List<LdapConnectionDetails> ldapConnections = getEnabledLdapConnections();
        return convertLdapConnectionsDetails(ldapConnections, includePasswordInDto);
    }

    /**
     * Creates a new LdapConnectionDetails from the given dto, saves it and returns a dto of the saved one.
     * <p/>
     * A valid DTO for adding a new LdapConnectionDetails must contain all fields except ID, which must be null, and the
     * name must not belong to an already existing LDAP connection details.
     * If the given dto is invalid, nothing gets saved and an appropriate exception.
     *
     * @param dto Contains the properties to set in the newly added LdapConnectionDetails
     * @return A dto of the saved LdapConnectionDetails
     */
    @Transactional()
    public LdapConnectionDetailsDto addNewConnDetails(LdapConnectionDetailsDto dto) {
        logger.debug("Saving new LDAP connection details: {}", dto);
        validateDtoForSaving(dto, true);
        LdapConnectionDetails connDetails = new LdapConnectionDetails();
        copyFromDto(connDetails, dto);
        LdapConnectionDetails updatedConnDetails;
        updatedConnDetails = ldapConnectionDetailsRepository.save(connDetails);
        LdapConnectionDetailsDto resultDto = convertLdapConnectionDetails(updatedConnDetails);
        resultDto.setManagerPass(null);
        return resultDto;
    }

    /**
     * Updates an already saved LdapConnectionDetails of th given ID with details from the given dto and returns an
     * optional dto of the updated one.
     * <p/>
     * If the there's no saved {@link LdapConnectionDetails} with the given id nothing gets updated and
     * {@link Optional#EMPTY} is returned.
     *
     * @param dto Contains the properties to set in the updated LdapConnectionDetails
     * @return An optional dto of the updates LdapConnectionDetails or Optional.EMPTY if not updates
     */
    @Transactional()
    public Optional<LdapConnectionDetailsDto> updateLdapConnectionDetailsById(LdapConnectionDetailsDto dto) {
        logger.debug("Updating LDAP connection details with id: {}, dto: {}",  dto.getId(), dto);
        validateDtoForSaving(dto, false);
        return ldapConnectionDetailsRepository.findById(dto.getId()).map(ldapConnectionDetails -> {
            copyFromDto(ldapConnectionDetails, dto);
            LdapConnectionDetails updatedConnDetails = ldapConnectionDetailsRepository.save(ldapConnectionDetails);
            LdapConnectionDetailsDto resultDto = convertLdapConnectionDetails(updatedConnDetails);
            resultDto.setManagerPass(null);
            return resultDto;
        });
    }

    @Transactional
    public boolean deleteLdapConnectionDetailsById(long connDetailsId) {
        if (ldapConnectionDetailsRepository.existsById(connDetailsId)) {
            ldapConnectionDetailsRepository.deleteById(connDetailsId);
            return true;
        } else {
            return false;
        }
    }

    public Optional<LdapConnectionDetailsDto> setLdapConnectionDetailsEnabledById(long id, boolean enabled) {
        return ldapConnectionDetailsRepository
                .findById(id)
                .map(ldapConnectionDetails -> {
                    ldapConnectionDetails.setEnabled(enabled);
                    LdapConnectionDetails updatedConnDetails = ldapConnectionDetailsRepository.save(ldapConnectionDetails);
                    LdapConnectionDetailsDto resultDto = convertLdapConnectionDetails(updatedConnDetails);
                    resultDto.setManagerPass(null);
                    return resultDto;
                });
    }

    public String getConnectionUrl(LdapConnectionDetailsDto ldapConnectionDetailsDto) {
        String protocol = ldapConnectionDetailsDto.isSsl() ? "ldaps" : "ldap";
        return protocol + "://" + ldapConnectionDetailsDto.getHost() + ":" + ldapConnectionDetailsDto.getPort();
    }

    /**
     * Tests if a connection to an LDAP server can be established and returns an optional {@link TestResultDto}
     * representing the result of the test (duh..).
     * <p/>
     * The connection details of the LDAP server are specified in the given DTO.
     * <p/>
     * If the id is not specified (is null) then a password must be specified, otherwise an {@link Optional#EMPTY} is
     * returned.
     * <p/>
     * If the id is specified then the password must be null and there must be a record of the details with the
     * specified id, otherwise an {@link Optional#EMPTY} is returned.
     *
     * @param dto Specifies the connection details to the LDAP server
     * @return An optional TestResultDto representing the result of the connection test, or Optional.EMPTY as
     *          explained above
     */
    public Optional<TestResultDto> testConnection(LdapConnectionDetailsDto dto) {
        Optional<String> managerPass = getPassFromDtoOrDb(dto);
        if (!managerPass.isPresent()) {
            return Optional.empty();
        }

        String manageDN = dto.getManageDN();
        String ldapUrl = getConnectionUrl(dto);
        logTestConnectionDetailsRequest(manageDN, ldapUrl);

        LdapContextSource contextSource = new DefaultSpringSecurityContextSource(ldapUrl);
        contextSource.setUserDn(manageDN);
        contextSource.setPassword(managerPass.get());
        contextSource.afterPropertiesSet();

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        SpringSecurityLdapTemplate template = new SpringSecurityLdapTemplate(contextSource);
        template.setSearchControls(searchControls);

        try {
            DirContextOperations result = template.retrieveEntry(manageDN, new String[0]);
            logger.info("Tested connection to LDAP server Successfully. Result: {}", result);
            if (enableCredentialsValidationInConnectionTest) {
                logger.info("Testing LDAP credentials for {}", manageDN);
                ldapUtils.logRootDSE(contextSource);
                logger.info("Tested LDAP credentials for {} Successfully", manageDN);
            }
            return Optional.of(new TestResultDto(true));
        } catch (CommunicationException commExp) {
            logger.info("Failed to connect to LDAP server: {} with manageDN: {}", ldapUrl, manageDN, commExp);
            return Optional.of(new TestResultDto(false, "No Connection"));
        } catch (AuthenticationException authExp) {
            logger.info("Failed to authenticate manageDN: {} in LDAP server: {}", manageDN, ldapUrl, authExp);
            return Optional.of(new TestResultDto(false, "Bad credentials"));
        } catch (Exception e) {
            logger.info("Failed to test connection to LDAP server: {} with manageDN: {}", ldapUrl, manageDN, e);
            return Optional.of(new TestResultDto(false, "Failed"));
        }
    }

    //<editor-fold desc="Private Methods">

    private List<LdapConnectionDetails> getEnabledLdapConnections() {
        Page<LdapConnectionDetails> page = ldapConnectionDetailsRepository.findAllByEnabledTrue(PageRequest.of(0, 10000));
        return page.getContent();
    }

    private void copyFromDto(LdapConnectionDetails connectionDetails, LdapConnectionDetailsDto dto) {

        connectionDetails.setName(dto.getName());
        connectionDetails.setServerDialect(dto.getServerDialect());
        connectionDetails.setHost(dto.getHost());
        connectionDetails.setPort(dto.getPort());
        connectionDetails.setIsSSL(dto.isSsl());
        connectionDetails.setManageDN(dto.getManageDN());
        connectionDetails.setEnabled(dto.isEnabled());
        if (!Strings.isNullOrEmpty(dto.getManagerPass())) {
            connectionDetails.setManagerPass(dataEncryptionService.encrypt((dto.getManagerPass())));
        }
    }

    private Optional<String> getPassFromDtoOrDb(LdapConnectionDetailsDto dto) {
        Long id = dto.getId();
        String managerPass = dto.getManagerPass();
        if ((id == null && managerPass == null) ||
            (id != null &&  !ldapConnectionDetailsRepository.existsById(id)) ) {
            return Optional.empty();
        }
        if (managerPass == null) {
            LdapConnectionDetails ldapConnDetailsFromDb = ldapConnectionDetailsRepository.getOne(id);
            String passFromDb = ldapConnDetailsFromDb.getManagerPass();
            if (Strings.isNullOrEmpty(passFromDb)) {
                throw new IllegalStateException(messageHandler.getMessage("ldap-connection.manager-pw.empty", Lists.newArrayList(id)));
            }
            managerPass = dataEncryptionService.decrypt(passFromDb);
        }

        return Optional.of(managerPass);
    }

    private void validateDtoForSaving(LdapConnectionDetailsDto dto, boolean isForAddingNew) {

        if (Strings.isNullOrEmpty(dto.getName()))
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.name.empty"), BadRequestType.MISSING_FIELD);
        if (Strings.isNullOrEmpty(dto.getHost()))
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.host.empty"), BadRequestType.MISSING_FIELD);
        if (dto.getPort() == null)
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.port.empty"), BadRequestType.MISSING_FIELD);
        if (dto.getPort() < 0 || 65535 < dto.getPort())
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.port.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        if (dto.getServerDialect() == null)
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.dialect.empty"), BadRequestType.MISSING_FIELD);
        if (Strings.isNullOrEmpty(dto.getManageDN()))
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.manage-dn.empty"), BadRequestType.MISSING_FIELD);

        if (isForAddingNew) {
            if (dto.getId() != null)
                throw new BadRequestException(messageHandler.getMessage("ldap-connection.id.invalid"), BadRequestType.UNSUPPORTED_VALUE);
            if (Strings.isNullOrEmpty(dto.getManagerPass()))
                throw new BadRequestException(messageHandler.getMessage("ldap-connection.password.empty"), BadRequestType.MISSING_FIELD);
            if (ldapConnectionDetailsRepository.existsByName(dto.getName()))
                throw new BadRequestException(messageHandler.getMessage("ldap-connection.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        } else { // The DTO is for updating an existing connection details
            if (dto.getId() == null)
                throw new BadRequestException(messageHandler.getMessage("ldap-connection.id.empty"), BadRequestType.MISSING_FIELD);
            if (dto.getId() < 0)
                throw new BadRequestException(messageHandler.getMessage("ldap-connection.id.invalid-negative"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private void logTestConnectionDetailsRequest(String manageDN, String ldapUrl) {
        logger.info("Testing LDAP connection:");
        logger.info("   Searching manageDN {}", manageDN);
        logger.info("   LDAP connection URL: {}", ldapUrl);
    }

    private void disableSslCertValidation() {
        logger.info("Trying to disable SSL certificate validation");
        boolean sslCertValidationDisabled = false;
        TrustManager trustManager = new GullibleTrustManager();

        try {
            SSLContext sslContext = SSLContext.getInstance(commCryptoProtocolName);
            sslContext.init(null, new TrustManager[] { trustManager }, new SecureRandom());
            SSLContext.setDefault(sslContext);
            sslCertValidationDisabled = true;
        } catch (NoSuchAlgorithmException e) {
            logger.error("Can't disable certificate validation." +
                    " {} isn't a registered communication cryptographic protocol", commCryptoProtocolName, e);
        } catch (KeyManagementException e) {
            logger.error("Can't disable certificate validation.", e);
        }

        if (sslCertValidationDisabled) {
            logger.info("Disabled SSL certificate validation successfully");
        } else {
            logger.warn("Failed to disable certificate validation");
        }
    }

    public static Page<LdapConnectionDetailsDto> convertLdapConnectionsDetailsPage
            (Page<LdapConnectionDetails> ldapConnectionsDetails, boolean includePasswordInDto) {
        List<LdapConnectionDetailsDto> content = convertLdapConnectionsDetails(ldapConnectionsDetails.getContent(), includePasswordInDto);
        Pageable pageable = PageRequest.of(ldapConnectionsDetails.getNumber(), ldapConnectionsDetails.getSize());
        return new PageImpl<>(content, pageable, ldapConnectionsDetails.getTotalElements());
    }

    public static List<LdapConnectionDetailsDto> convertLdapConnectionsDetails
            (List<LdapConnectionDetails> ldapConnectionsDetails, boolean includePasswordInDto) {

        List<LdapConnectionDetailsDto> dtos = new ArrayList<>(ldapConnectionsDetails.size());
        for (LdapConnectionDetails ldapConnectionDetails : ldapConnectionsDetails) {
            LdapConnectionDetailsDto dto = convertLdapConnectionDetails(ldapConnectionDetails);
            if (!includePasswordInDto) {
                dto.setManagerPass(null);
            }
            dtos.add(dto);
        }
        return dtos;
    }

    public static LdapConnectionDetailsDto convertLdapConnectionDetails(
            LdapConnectionDetails ldapConnectionDetails) {
        LdapConnectionDetailsDto ldapConnectionDetailsDto = new LdapConnectionDetailsDto();
        ldapConnectionDetailsDto.setId(ldapConnectionDetails.getId());
        ldapConnectionDetailsDto.setPort(ldapConnectionDetails.getPort());
        ldapConnectionDetailsDto.setName(ldapConnectionDetails.getName());
        ldapConnectionDetailsDto.setServerDialect(ldapConnectionDetails.getServerDialect());
        ldapConnectionDetailsDto.setHost(ldapConnectionDetails.getHost());
        ldapConnectionDetailsDto.setManagerPass(ldapConnectionDetails.getManagerPass());
        ldapConnectionDetailsDto.setSsl(ldapConnectionDetails.isSSL());
        ldapConnectionDetailsDto.setManageDN(ldapConnectionDetails.getManageDN());
        ldapConnectionDetailsDto.setEnabled(ldapConnectionDetails.isEnabled());

        return ldapConnectionDetailsDto;

    }

    //</editor-fold>
}
