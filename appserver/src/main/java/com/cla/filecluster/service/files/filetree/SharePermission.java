package com.cla.filecluster.service.files.filetree;

/**
 * Created by: yael
 * Created on: 9/16/2019
 *
 * e.g {'user':'Everyone', 'mask':1179817, 'type':0, 'perm':'ReadAndExecute, Synchronize'}
 */
class SharePermission {

    private String user;

    private Long mask;

    private Integer type;

    private String perm;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getMask() {
        return mask;
    }

    public void setMask(Long mask) {
        this.mask = mask;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPerm() {
        return perm;
    }

    public void setPerm(String perm) {
        this.perm = perm;
    }

    @Override
    public String toString() {
        return "SharePermission{" +
                "user='" + user + '\'' +
                ", mask=" + mask +
                ", type=" + type +
                ", perm='" + perm + '\'' +
                '}';
    }
}
