package com.cla.filecluster.service.event;

import com.cla.common.domain.dto.event.DAEvent;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
public interface EventNormalizer<T> {
    DAEvent normalize(T eventData);
}
