package com.cla.filecluster.service.diagnostics;

import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.repository.jpa.diagnostics.DiagnosticsRepository;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class DiagnosticService implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(DiagnosticService.class);

    @Autowired
    private DiagnosticsRepository diagnosticsRepository;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private List<DiagnosticAction> operations;

    @Value("${diagnostics.service-page-size:1}")
    private int diagnosticsPageSize;

    @Value("${diagnostics.service-solr-check-min-interval-minutes:5}")
    private int solrCheckIntervalMinutes;

    @Value("${diagnostics.service-is-on:true}")
    private boolean isDiagnosticsOn;

    @Value("${diagnostics.service-creation-check-min-interval-minutes:10}")
    private int creationCheckIntervalMinutes;

    private AtomicBoolean currentlyRunning = new AtomicBoolean(false);

    private boolean isSchedulingActive = false;
    private long lastCreationCheckTime = 0;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @Scheduled(initialDelayString = "${diagnostics.actions-initial-delay-millis:60000}",
            fixedDelayString = "${diagnostics.actions-frequency-millis:60000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @AutoAuthenticate
    @Transactional
    public void runOperations() {
        if (!isSchedulingActive) {
            return;
        }

        if (!isDiagnosticsOn) {
            logger.trace("diagnostics service is turned off");
            return;
        }

        if (!currentlyRunning.compareAndSet(false, true)) {
            return;
        }

        try {
            createIfNeeded();
            Page<Diagnostic> toRun = diagnosticsRepository.findAllPending(PageRequest.of(0, diagnosticsPageSize));
            if (toRun != null && !toRun.getContent().isEmpty()) {
                runDiagnostics(toRun.getContent());
            }
        } catch (Exception e) {
            logger.error("failed run of diagnostics", e);
        } finally {
            currentlyRunning.set(false);
        }
    }

    private void runDiagnostics(List<Diagnostic> diagnostics) {
        long lastChkSolrUp = 0L;
        for (Diagnostic diagnostic : diagnostics) {

            if (lastChkSolrUp < System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(solrCheckIntervalMinutes)) {
                if (!componentsAppService.isSolrUp()) {
                    logger.info("solr seems to be down, try again later");
                    return;
                }
                lastChkSolrUp = System.currentTimeMillis();
            }

            try {
                DiagnosticAction action = getActionForType(diagnostic.getDiagnosticType());
                if (action == null) {
                    logger.error("cannot find diagnostic action for type {}", diagnostic.getDiagnosticType());
                } else {
                    action.runDiagnostic(diagnostic.getOpData());
                }
                diagnosticsRepository.deleteById(diagnostic.getId());
            } catch (Exception e) {
                logger.error("failed run of diagnostic {}", diagnostic, e);
            }
        }
    }

    private void createIfNeeded() {
        if (lastCreationCheckTime > System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(creationCheckIntervalMinutes)) {
            return;
        }
        lastCreationCheckTime = System.currentTimeMillis();

        List<Object> types = diagnosticsRepository.getAllExistingTypes();
        List<DiagnosticType> toCreate = new ArrayList<>();
        for (DiagnosticType type : DiagnosticType.values()) {
            if (types == null || !types.contains(type.name())) {
                toCreate.add(type);
            }
        }

        List<Diagnostic> allToCreate = new ArrayList<>();
        toCreate.forEach(type -> {
            DiagnosticAction action = getActionForType(type);
            if (action == null) {
                logger.error("cannot find diagnostic action for type {}", type);
            } else {
                List<Diagnostic> partsToCreate = action.createNewDiagnosticActionParts();
                if (partsToCreate != null) {
                    allToCreate.addAll(partsToCreate);
                }
            }
        });

        if (!allToCreate.isEmpty()) {
            dbTemplateUtils.doInTransaction(() -> diagnosticsRepository.saveAll(allToCreate),
                    TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        }
    }

    @Transactional
    public void createDiagnosticProcedure(Diagnostic d) {
        diagnosticsRepository.save(d);
    }

    public DiagnosticAction getActionForType(DiagnosticType type) {
        if (operations != null && !operations.isEmpty()) {
            for (DiagnosticAction action : operations) {
                if (action.getType().equals(type)) {
                    return action;
                }
            }
        }
        return null;
    }
}
