package com.cla.filecluster.service.export.exporters.file.error;

import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.error.ErrorHeaderFields.*;

/**
 * Created by: yael
 * Created on: 2/7/2019
 */
@Slf4j
@Component
public class IngestErrorExporter extends PageCsvExcelExporter<FileProcessingErrorDto> {

    private final static String[] HEADER = {NAME, TYPE, ROOT_FOLDER, FOLDER,
            TEXT, DETAILS, RUN_ID, FILE_TYPE, DATE, MEDIA_TYPE, FILE_EXTENSION};

    @Autowired
    private IngestErrorService ingestErrorService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(FileProcessingErrorDto.class, FileProcessingErrorDtoMixin.class);
    }

    @Override
    protected Page<FileProcessingErrorDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return ingestErrorService.getIngestionErrors(dataSourceRequest, requestParams);
    }

	@Override
	protected Map<String, Object> addExtraFields(FileProcessingErrorDto base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.INGEST_ERROR);
    }
}
