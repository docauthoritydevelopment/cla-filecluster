package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filesearchpatterns.RegulationDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.extractor.pattern.RegulationAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;
import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.NUM_OF_FILES;

/**
 * Created by: ophir
 * Created on: 1/5/2019
 */
@Slf4j
@Component
public class RegulationExporter extends PageCsvExcelExporter<AggregationCountItemDTO<RegulationDto>> {

    private final static String[] HEADER = {TextSearchPatternHeaderFields.NAME, NUM_OF_FILES};

    @Autowired
    private RegulationAppService regulationAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, SearchPatternAggCountDtoMixin.class);
        mapper.addMixIn(RegulationDto.class, RegulationDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<RegulationDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<RegulationDto>> res = regulationAppService.getRegulationsFileCounts(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<RegulationDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.REGULATION);
    }
}
