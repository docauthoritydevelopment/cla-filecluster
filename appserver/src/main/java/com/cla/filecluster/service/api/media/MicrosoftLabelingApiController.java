package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MIPConnectionParametersDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.MIPConnectionDetailsDto;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.media.MicrosoftLabelingAppService;
import com.google.common.base.Strings;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
@RestController
@RequestMapping("/api/media/mip")
public class MicrosoftLabelingApiController {

    private final Logger logger = LoggerFactory.getLogger(MicrosoftLabelingApiController.class);

    @Autowired
    private MediaConnectionDetailsService connectionDetailsService;

    @Autowired
    private MicrosoftLabelingAppService microsoftLabelingAppService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/connections/new", method = {RequestMethod.PUT, RequestMethod.POST})
    public MIPConnectionDetailsDto newConnection(@RequestBody MIPConnectionDetailsDto connectionDetails){
        MIPConnectionDetailsDto labelingConnection = connectionDetailsService.createMicrosoftLabelingConnection(connectionDetails);
        labelingConnection.getMIPConnectionParametersDto().setPassword(null);
        return labelingConnection;
    }

    @RequestMapping(value = "/connections/test/dataCenter/{customerDataCenterId}", method = RequestMethod.POST)
    public TestResultDto testConnection(@RequestBody MIPConnectionDetailsDto connectionDetails, @PathVariable long customerDataCenterId){
        try {
            MIPConnectionDetailsDto labelingConnectionDetailsDto = connectionDetails;
            if (connectionDetails.getMIPConnectionParametersDto() == null) {
                connectionDetails.setMIPConnectionParametersDto(new MIPConnectionParametersDto());
            }

            if (connectionDetails.getId() != null && connectionDetails.getMIPConnectionParametersDto().getPassword() == null) {//test new connection
                MIPConnectionDetailsDto labelingConnectionDetailsDtoExt = (MIPConnectionDetailsDto)
                        connectionDetailsService.getMediaConnectionDetailsById(connectionDetails.getId());
                if (labelingConnectionDetailsDtoExt.getMIPConnectionParametersDto() != null) {
                    connectionDetails.getMIPConnectionParametersDto().setPassword(
                            labelingConnectionDetailsDtoExt.getMIPConnectionParametersDto().getPassword());
                }
            }
            List<ServerResourceDto> serverResourceDtos = microsoftLabelingAppService.listFoldersUnderPath(
                    labelingConnectionDetailsDto, Optional.empty(), customerDataCenterId, true);
            return new TestResultDto(true, serverResourceDtos.get(0).getFullName());
        } catch (Exception e) {
            return new TestResultDto(false, e.getMessage());
        }
    }

    /*
     *	Both following API calls are abomination and should be abolished (as there is already a dedicated media connection service/controller)
     */
    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public List<MIPConnectionDetailsDto> getConnectionDetails() {
        return connectionDetailsService.getMediaConnectionDetailsDtoByType(MediaType.MIP, true);
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.GET)
    public MIPConnectionDetailsDto getConnectionDetails(@PathVariable long id) {
        MediaConnectionDetailsDto mediaDto = connectionDetailsService.getMediaConnectionDetailsById(id, false);
        if (mediaDto == null) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        } else if (!(mediaDto instanceof MIPConnectionDetailsDto)) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        MIPConnectionDetailsDto dto = (MIPConnectionDetailsDto)mediaDto;
        dto.getMIPConnectionParametersDto().setPassword(null);
        return dto;
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.POST)
    public MIPConnectionDetailsDto updateConnectionDetails(@PathVariable long id, @RequestBody MIPConnectionDetailsDto labelingConnectionDetailsDto) {
        MediaConnectionDetailsDto mediaDto = connectionDetailsService.getMediaConnectionDetailsDtoByConnectionId(id);
        if(mediaDto == null){
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (labelingConnectionDetailsDto.getMIPConnectionParametersDto() != null &&
                Strings.isNullOrEmpty(labelingConnectionDetailsDto.getMIPConnectionParametersDto().getPassword())) {
            MIPConnectionDetailsDto oldDto = (MIPConnectionDetailsDto)mediaDto;
            labelingConnectionDetailsDto.getMIPConnectionParametersDto().setPassword(oldDto.getMIPConnectionParametersDto().getPassword());
        }
        return connectionDetailsService.updateConnectionDetails(id, labelingConnectionDetailsDto);
    }

    //region ...legacy methods. Not in use.
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response) {
        String sessionId = request.getSession().getId();
        String authorizeUrl = buildAuthorizeUrl(sessionId);
        logger.debug(authorizeUrl);
        try {
            if (response instanceof Response) {
                ((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
            }
            response.sendRedirect(authorizeUrl);
        } catch (IOException | RuntimeException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/login/file", method = RequestMethod.GET)
    public void loginToFile(HttpServletRequest request, HttpServletResponse response) {
        String authorizeUrl = buildAuthorizeUrl("file");
        logger.debug(authorizeUrl);
        try {
            if (response instanceof Response) {
                ((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
            }
            response.sendRedirect(authorizeUrl);
        } catch (IOException | RuntimeException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/response", method = {RequestMethod.GET, RequestMethod.POST})
    public String labelingOauthResponse(@QueryParam("code") String code,
                                        @QueryParam("state") String state,
                                        @QueryParam("error") String error,
                                        @QueryParam("errorDescription") String errorDescription) {
        logger.debug("Got labeling Oauth response: {} {} {} {}", code, state, error, errorDescription);
        Long currentUserId = 1L; //TODO: get current user ID
        try {
            if (state.equals("file")) {
                microsoftLabelingAppService.handleOauthResponse(currentUserId, code, true);
            } else {
                microsoftLabelingAppService.fillAuthentication(state);
                microsoftLabelingAppService.handleOauthResponse(currentUserId, code, false);

            }
        } catch (RuntimeException e) {
            logger.error("Failed to handle labeling response", e);
            throw e;
        }
        return "OK";
    }

    private String buildAuthorizeUrl(String sessionId) {
        return microsoftLabelingAppService.buildAuthorizeUrl(sessionId);
    }

}
