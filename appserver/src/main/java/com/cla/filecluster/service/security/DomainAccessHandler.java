package com.cla.filecluster.service.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Service
public class DomainAccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(DomainAccessHandler.class);

    @Value("${ldap.user-tokens.add-domain-prefix.join-string:\\\\}")    // Solr query requires escaping of the backslash
    private String accessTokensDomainNameJoinString;

    @Value("${ldap.user-tokens.add-domain-prefix.split-string:\\}")
    private String accessTokensDomainNameSplitString;

    @Value("${ldap.user-tokens.add-orig-domain-prefix.join-string:@}")
    private String accessTokensOriginalDomainNameJoinString;

    @Value("${ldap.user-tokens.add-domain-prefix.map:}")
    private String[] accessTokensDomainNameMapStrings;

    @Value("${acl.email-display-format:true}")
    private boolean useEmailAclFormat;

    private HashMap<String, String> accessTokensDomainNameMap = new HashMap<>();
    private HashMap<String, String> prefixDomainNameMap = new HashMap<>();

    @PostConstruct
    public void init() {
        if (accessTokensDomainNameMapStrings != null && accessTokensDomainNameMapStrings.length > 0) {
            for (String m : accessTokensDomainNameMapStrings) {
                String[] sp = m.split(":", 2);
                if (sp.length == 2 && !sp[0].isEmpty() && !sp[1].isEmpty()) {
                    accessTokensDomainNameMap.put(sp[0], sp[1]);
                    prefixDomainNameMap.put(sp[1], sp[0]);
                    logger.info("Adding accessTokensDomainNameMap: {} -> {}", sp[0], sp[1]);
                }
            }
        }
    }

    public boolean doesFileShareDomainHasTranslation(String fileShareDomain) {
        return prefixDomainNameMap.containsKey(fileShareDomain);
    }

    public String getFileShareToken(String token, String domainName) {
        return getFileShareToken(token, domainName, accessTokensDomainNameJoinString);
    }

    private String getFileShareToken(String token, String domainName, String separator) {
        String mappedDomainName = accessTokensDomainNameMap.getOrDefault(domainName, domainName);
        return mappedDomainName + separator + token;
    }

    public String getEmailToken(String token, String domainName) {
        return token.endsWith(accessTokensOriginalDomainNameJoinString + domainName) ?
                token : (token + accessTokensOriginalDomainNameJoinString + domainName);
    }

    public String getAclInEmailFormat(String acl) {
        if (acl.contains(accessTokensOriginalDomainNameJoinString) ||
                (!acl.contains(accessTokensDomainNameSplitString) &&
                !acl.contains(accessTokensDomainNameJoinString))) {
            return acl;
        }


        int partsNum, userPart;
        if (acl.contains(accessTokensDomainNameJoinString)) {
            partsNum = 3;
            userPart = 2;
        } else {
            partsNum = 2;
            userPart = 1;
        }
        String[] parts = acl.split(accessTokensDomainNameJoinString);
        if (parts.length != partsNum) {
            return acl;
        }
        String domainName = parts[0];
        String user = parts[userPart];

        String mappedDomainName = prefixDomainNameMap.get(domainName);
        return mappedDomainName == null ? acl : getEmailToken(user, mappedDomainName);
    }

    public String getAclInFileShareFormat(String acl) {
        if (!acl.contains(accessTokensOriginalDomainNameJoinString) || acl.contains(accessTokensDomainNameJoinString)) {
            return acl;
        }

        String[] parts = acl.split(accessTokensOriginalDomainNameJoinString);
        if (parts.length != 2) {
            return acl;
        }
        String domainName = parts[1];
        String user = parts[0];

        return accessTokensDomainNameMap.containsKey(domainName) ?
                getFileShareToken(user, domainName, accessTokensDomainNameSplitString) : acl;
    }

    public String getAclInConfigFormat(String acl) {
        if (useEmailAclFormat) {
            return getAclInEmailFormat(acl);
        } else {
            return getAclInFileShareFormat(acl);
        }
    }

    // for testing purposes
    public void setAccessTokensDomainNameJoinString(String accessTokensDomainNameJoinString) {
        this.accessTokensDomainNameJoinString = accessTokensDomainNameJoinString;
    }

    public void setAccessTokensOriginalDomainNameJoinString(String accessTokensOriginalDomainNameJoinString) {
        this.accessTokensOriginalDomainNameJoinString = accessTokensOriginalDomainNameJoinString;
    }

    public void setAccessTokensDomainNameMapStrings(String[] accessTokensDomainNameMapStrings) {
        this.accessTokensDomainNameMapStrings = accessTokensDomainNameMapStrings;
    }

    public void setAccessTokensDomainNameSplitString(String accessTokensDomainNameSplitString) {
        this.accessTokensDomainNameSplitString = accessTokensDomainNameSplitString;
    }
}
