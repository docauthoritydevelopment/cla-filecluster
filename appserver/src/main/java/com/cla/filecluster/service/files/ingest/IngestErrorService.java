package com.cla.filecluster.service.files.ingest;

import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ErrProcessedFile;
import com.cla.filecluster.repository.jpa.ErrProcessedFileRepository;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class IngestErrorService {
    private static final Logger logger = LoggerFactory.getLogger(IngestErrorService.class);

    @Autowired
    private ErrProcessedFileRepository errProcessedFileRepository;

    @Autowired
    private RootFolderRepository rootFolderRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private IngestBatchResultsProcessor ingestBatchResultsProcessor;

    @Autowired
    private DBTemplateUtils dBTemplateUtils;

    public List<String> getErrorTypes() {
        List<String> result = new ArrayList<>();
        for (ProcessingErrorType type : ProcessingErrorType.values()) {
            if (type.isAssigned()) {
                result.add(type.name());
            }
        }
        return result;
    }

    public void saveError(Long taskId, Long runId, SolrFileEntity file, ProcessingErrorType type, String text, String exceptionText) {
        MediaType mediaType = MediaType.valueOf(file.getMediaType());
        ErrProcessedFile errProcessedFile = new ErrProcessedFile(file.getFileId(), type, text, exceptionText,
                runId, file.getExtension(), mediaType);
        errProcessedFile.setFileType(file.getType());
        errProcessedFile.setFullPath(getPath(file.getFullName(), file.getRootFolderId()));
        errProcessedFile.setFolderId(file.getFolderId());
        errProcessedFile.setRootFolderId(file.getRootFolderId());
        errProcessedFile.setContentId(FileCatService.getContentId(file));
        ingestBatchResultsProcessor.collectIngestionError(taskId, errProcessedFile);
    }

    private String getPath(String mediaEntityId, Long rfId) {
        String path = mediaEntityId;
        if (rfId != null) {
            RootFolder rf = docStoreService.getRootFolderCached(rfId);
            path = DocStoreService.getRootFolderIdentifier(rf) + path;
        }
        return path;
    }

    private String buildQuery(Map<String, String> params, long offset, int pageSize, boolean isCount) {
        String query = "select ";

        query += isCount ? "count(*)" :
                "err.id, err.cla_file_id, err.content_id, err.created_ondb, err.date_created, err.exception_text," +
                "err.file_type, err.folder_id, err.full_path, err.root_folder_id, err.run_id, err.text, err.type, " +
                "err.media_type, err.extension";

        query += " from err_processed_files err, root_folder rf " +
                " where rf.id = err.root_folder_id " +
                " and rf.deleted = false ";

        Long dateFrom = params != null && params.containsKey("dateFrom") ? Long.parseLong(params.get("dateFrom")) : null;
        Long dateTo = params != null && params.containsKey("dateTo") ? Long.parseLong(params.get("dateTo")) : null;

        if (dateFrom == null && dateTo != null) {
            dateFrom = 0L;
        } else if (dateFrom != null && dateTo == null) {
            dateTo = System.currentTimeMillis();
        }

        if (dateFrom != null) {
            query += " and err.date_created >= " + dateFrom;
            query += " and err.date_created <= " + dateTo;
        }

        Long rootFolderId = params != null && params.containsKey("rootFolderId") ? Long.parseLong(params.get("rootFolderId")) : null;
        if (rootFolderId != null) {
            query += " and err.root_folder_id = " + rootFolderId;
        }

        if (params != null && params.containsKey("errorType") && !Strings.isNullOrEmpty(params.get("errorType"))) {
            String[] types = params.get("errorType").split(",");
            List<String> tyPesInt = new ArrayList<>();
            for (String type : types) {
                ProcessingErrorType typeObj = ProcessingErrorType.valueOf(type);
                tyPesInt.add(String.valueOf(typeObj.ordinal()));
            }

            if (!tyPesInt.isEmpty()) {
                String values = String.join(",",  tyPesInt);;
                query += " and err.type in (" + values + ") ";
            }
        }

        if (params != null && params.containsKey("mediaType") && !Strings.isNullOrEmpty(params.get("mediaType"))) {
            String[] types = params.get("mediaType").split(",");
            List<String> tyPesInt = new ArrayList<>();
            for (String type : types) {
                MediaType typeObj = MediaType.valueOf(type);
                tyPesInt.add(String.valueOf(typeObj.getValue()));
            }

            if (!tyPesInt.isEmpty()) {
                String values = String.join(",",  tyPesInt);;
                query += " and err.media_type in (" + values + ") ";
            }
        }

        String extension = params != null && params.containsKey("extension") ? params.get("extension") : null;
        if (extension != null) {
            query += " and err.extension = '" + extension + "'";
        }

        if (!isCount) {
            query += " order by err.date_created desc limit " + offset + ", " + pageSize;
        }

        return query;
    }

    @Transactional(readOnly = true)
    public Page<FileProcessingErrorDto> getIngestionErrors(DataSourceRequest dataSourceRequest, Map<String, String> params) {
        logger.debug("Get ingestion errors");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        List<ErrProcessedFile> ingestErrors = new ArrayList<>();

        String statement = buildQuery(params, pageRequest.getOffset(), pageRequest.getPageSize(), false);

        dBTemplateUtils.doInTransaction(jdbcTemplate -> {
            SqlRowSet res = jdbcTemplate.queryForRowSet(statement);
            while (res.next()) {
                ErrProcessedFile err = new ErrProcessedFile();
                err.setId(res.getBigDecimal("id").longValue());
                err.setClaFile(res.getObject("cla_file_id") == null ? null :
                        res.getBigDecimal("cla_file_id").longValue());
                err.setContentId(res.getObject("content_id") == null ? null :
                        res.getBigDecimal("content_id").longValue());
                err.setFolderId(res.getObject("folder_id") == null ? null :
                        res.getBigDecimal("folder_id").longValue());
                err.setRootFolderId(res.getObject("root_folder_id") == null ? null :
                        res.getBigDecimal("root_folder_id").longValue());
                err.setExceptionText(res.getString("exception_text"));
                err.setDateCreated(res.getObject("date_created") == null ? null :
                        res.getBigDecimal("date_created").longValue());
                err.setFileType(res.getObject("file_type") == null ? null :
                        res.getInt("file_type"));
                err.setFullPath(res.getString("full_path"));
                err.setRunId(res.getObject("run_id") == null ? null :
                        res.getBigDecimal("run_id").longValue());
                err.setText(res.getString("text"));
                err.setType(res.getObject("type") == null ? null :
                        ProcessingErrorType.ordinalOf(res.getInt("type")));
                err.setMediaType(res.getObject("media_type") == null ? null :
                        MediaType.valueOf(res.getInt("media_type")));
                err.setExtension(res.getString("extension"));
                ingestErrors.add(err);
            }
        });

        String countStatement = buildQuery(params, pageRequest.getOffset(), pageRequest.getPageSize(), true);
        List<Integer> result = new ArrayList<>();
        dBTemplateUtils.doInTransaction(jdbcTemplate -> {
            SqlRowSet res = jdbcTemplate.queryForRowSet(countStatement);
            if (res.next()) {
                result.add(res.getInt(1));
            }
        });
        int total = result.size() == 0 ? 0 : result.get(0);

        return convertIngestionErrors(ingestErrors, pageRequest, total);
    }

    @Transactional(readOnly = true)
    public List<FileProcessingErrorDto> getFileIngestionErrors(long fileId) {
        logger.debug("Get ingestion errors for file {}", fileId);
        List<ErrProcessedFile> fileErrors = errProcessedFileRepository.getFileErrors(fileId);
        return convertIngestionErrors(fileErrors);
    }

    @Transactional(readOnly = true)
    public long countIngestErrorsByRunId(Long crawlRunId) {
        return errProcessedFileRepository.countByRunId(crawlRunId);
    }

    @Transactional
    public void deleteErrorsByRootFolder(RootFolder rootFolder) {
        logger.warn("delete all ingest errors by root folder {}", rootFolder);
        int deletedRows = errProcessedFileRepository.deleteByRootFolder(rootFolder.getId());
        logger.info("Deleted {} errors", deletedRows);
    }

    private Page<FileProcessingErrorDto> convertIngestionErrors(List<ErrProcessedFile> ingestErrors, Pageable pageRequest, long total) {
        return new PageImpl<>(convertIngestionErrors(ingestErrors), pageRequest, total);
    }

    private List<FileProcessingErrorDto> convertIngestionErrors(List<ErrProcessedFile> ingestErrors) {
        if (ingestErrors == null || ingestErrors.size() == 0) {
            return Lists.newLinkedList();
        }

        Set<Long> rootFolderIds = ingestErrors.stream().map(ErrProcessedFile::getRootFolderId).filter(Objects::nonNull).collect(Collectors.toSet());
        List<RootFolder> rootFolders = rootFolderIds.isEmpty() ? new ArrayList<>() : rootFolderRepository.findAllById(rootFolderIds);
        Map<Long, RootFolder> rootFoldersById = rootFolders.stream()
                .collect(Collectors.toMap(RootFolder::getId, folder -> folder));

        return ingestErrors.stream()
                .map(err -> convertIngestionError(err, rootFoldersById))
                .collect(Collectors.toList());
    }

    private FileProcessingErrorDto convertIngestionError(ErrProcessedFile ingestError,
                                                         Map<Long, RootFolder> rootFoldersById) {
        FileProcessingErrorDto fileProcessingErrorDto = new FileProcessingErrorDto();

        fileProcessingErrorDto.setId(ingestError.getId());
        fileProcessingErrorDto.setDetailText(ingestError.getExceptionText());
        fileProcessingErrorDto.setText(ingestError.getText());
        fileProcessingErrorDto.setType(ingestError.getType());

        fileProcessingErrorDto.setFileId(ingestError.getClaFile());
        String fullPath = ingestError.getFullPath();
        fileProcessingErrorDto.setFileName(fullPath);
        fileProcessingErrorDto.setBaseName(FileNamingUtils.getFilenameBaseName(fullPath));
        fileProcessingErrorDto.setFolder(FileNamingUtils.getFilenamePath(fullPath));
        fileProcessingErrorDto.setMediaType(ingestError.getMediaType());
        fileProcessingErrorDto.setExtension(ingestError.getExtension());

        if (ingestError.getFileType() != null) {
            fileProcessingErrorDto.setFileType(FileType.valueOf(ingestError.getFileType()));
        }
        fileProcessingErrorDto.setRunId(ingestError.getRunId());

        if (ingestError.getRootFolderId() != null) {
            RootFolder rf = rootFoldersById.get(ingestError.getRootFolderId());
            fileProcessingErrorDto.setRootFolderId(ingestError.getRootFolderId());
            if (rf != null) {
                fileProcessingErrorDto.setRootFolder(DocStoreService.getRootFolderIdentifier(rf));
            }
        }

        if (ingestError.getDateCreated() != null) {
            fileProcessingErrorDto.setTimeStamp(ingestError.getDateCreated());
        } else if (ingestError.getCreatedOnDB() != null) {
            fileProcessingErrorDto.setTimeStamp(ingestError.getCreatedOnDB().getTime());
        }

        return fileProcessingErrorDto;
    }
}
