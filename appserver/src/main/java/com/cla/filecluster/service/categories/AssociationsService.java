package com.cla.filecluster.service.categories;

import com.cla.common.constants.*;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagInterface;
import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.schedule.ScheduledOperationPriority;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.tag.scope.PriorityScopedValueResolver;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.doctype.DocTypeAssociation;
import com.cla.filecluster.domain.entity.filetag.*;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.TagData;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.cla.filecluster.service.operation.*;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.common.constants.FileGroupsFieldType.ID;
import static com.cla.common.constants.FileGroupsFieldType.UPDATE_DATE;
import static com.cla.common.constants.SolrFieldOp.REMOVE;

/**
 * tagging/doctype operations
 * <p>
 * Created by: yael
 * Created on: 5/22/2018
 */
@Service
public class AssociationsService {

    private final static Logger logger = LoggerFactory.getLogger(AssociationsService.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileCrawlerExecutionDetailsService runService;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    @Autowired
    private JobManagerService jobManagerService;

    @Value("${solr.tags2.populate:true}")
    private boolean populateTags2;

    @Value("${associations.apply-filter-associations-retry:5}")
    private int retryForApplyFilterAssociationToFiles;

    @Value("${associations.apply-folder-associations-retry:5}")
    private int retryForApplyFolderAssociationToFiles;

    @Value("${associations.apply-group-associations-retry:5}")
    private int retryForApplyGroupAssociationToFiles;

    @Value("${docTypeService.associationApplySize:1000}")
    private int associationApplySize;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    private SolrCatFolderRepository solrCatFolderRepository;
    private SolrFileCatRepository solrFileCatRepository;

    @EventListener
    @AutoAuthenticate
    public void handleContextRefresh(ContextRefreshedEvent event) {
        solrCatFolderRepository = applicationContext.getBean(SolrCatFolderRepository.class);
        solrFileCatRepository = applicationContext.getBean(SolrFileCatRepository.class);
    }

    /////////////////////// files

    public void removeAssociationFromFile(SolrFileEntity fileEntity, AssociableEntityDto associableEntity, Long basicScopeId, SolrCommitType commitType) {
        List<AssociationEntity> associations = convertFromAssociationEntity(fileEntity.getFileId(), fileEntity.getAssociations(), AssociationType.FILE);
        AssociationEntity entity = getFileAssociation(associableEntity, fileEntity.getFileId(), associations, basicScopeId);
        removeAssociationFromFile(fileEntity, associableEntity, entity, commitType);
    }

    public void removeAssociationFromFile(SolrFileEntity fileEntity, AssociableEntityDto associableEntity, AssociationEntity entity, SolrCommitType commitType) {
        Long contentId = FileCatService.getContentId(fileEntity);
        Long scopeId = entity.getAssociationScopeId();
        removeTagIdentifierFromCategoryFile(fileEntity.getFileId(), contentId, associableEntity, scopeId, commitType);
        addRemoveFileAssociation(SolrFieldOp.REMOVE, fileEntity.getFileId(), entity, commitType);
    }

    private void addRemoveFileAssociation(SolrFieldOp operation, Long fileId, AssociationEntity entity, SolrCommitType commitType) {
        String association = AssociationsService.convertFromAssociationEntity(entity);
        String query = CatFileFieldType.FILE_ID.getSolrName() + ":" + fileId;
        solrFileCatRepository.updateFieldByQuery(CatFileFieldType.FILE_ASSOCIATIONS.getSolrName(), association,
                operation, query, null, commitType);
    }

    public void addRemoveFolderAssociation(SolrFieldOp operation, String query, AssociationEntity entity, List<String> filters) {
        if (operation.equals(SolrFieldOp.REMOVE_REGEX)) {
            entity.setCreationDate(999999999999L);
        }
        String association = AssociationsService.convertFromAssociationEntity(entity);
        if (operation.equals(SolrFieldOp.REMOVE_REGEX)) {
            association = association.replace("999999999999", "[0-9]*");
            association = association.replace("{", ".*");
            association = association.replace("}", ".*");
            association = association.replace(":", "\\:");
        }
        solrCatFolderRepository.updateFieldByQuery(CatFoldersFieldType.FOLDER_ASSOCIATIONS.getSolrName(), association,
                operation, query, filters, SolrCommitType.NONE);
        solrCatFolderRepository.updateFieldByQuery(CatFoldersFieldType.UPDATE_DATE.getSolrName(),
                String.valueOf(System.currentTimeMillis()),
                SolrFieldOp.SET, query, filters, SolrCommitType.NONE);
    }

    private AssociationEntity getFileAssociation(AssociableEntityDto associableEntity, Long fileId,
                                                 List<AssociationEntity> fileTagAssociations, Long basicScopeId) {
        if (fileTagAssociations == null) {
            return null;
        }
        for (AssociationEntity fileTagAssociation : fileTagAssociations) {
            if (((associableEntity instanceof FileTagDto && fileTagAssociation.getFileTagId() != null && ((FileTagDto) associableEntity).getId().equals(fileTagAssociation.getFileTagId())) ||
                    (associableEntity instanceof DocTypeDto && fileTagAssociation.getDocTypeId() != null && ((DocTypeDto) associableEntity).getId().equals(fileTagAssociation.getDocTypeId())) ||
                    (associableEntity instanceof FunctionalRoleDto && ((FunctionalRoleDto) associableEntity).getId().equals(fileTagAssociation.getFunctionalRoleId())) ||
                    (associableEntity instanceof SimpleBizListItemDto && ((SimpleBizListItemDto) associableEntity).getSid().equals(fileTagAssociation.getSimpleBizListItemSid()))) &&
                    Objects.equals(fileTagAssociation.getAssociationScopeId(), basicScopeId)) {
                logger.debug("{} associated to claFile {}", associableEntity, fileId);
                return fileTagAssociation;
            }
        }
        return null;
    }

    public ClaFileDto addAssociationToFile(AssociableEntityDto associableEntity, ClaFile claFile, BasicScope basicScope, SolrCommitType commitType) {
        List<AssociationEntity> associations = convertFromAssociationEntity(claFile.getId(), claFile.getAssociations(), AssociationType.FILE);
        AssociationEntity entity = getFileAssociation(associableEntity, claFile.getId(), associations, basicScope == null ? null : basicScope.getId());

        //Check if fileTag is already in
        if (entity != null) {
            return null;
        }

        return associateToFile(associableEntity, claFile, basicScope, commitType);
    }

    private static AssociationEntity createForFile(AssociableEntityDto associableEntity, BasicScope basicScope) {
        AssociationEntity entity = new AssociationEntity();
        entity.setCreationDate(System.currentTimeMillis());
        entity.setAssociationScopeId(basicScope == null ? null : basicScope.getId());
        setEntityForAssociation(entity, associableEntity);
        return entity;
    }

    public static AssociationEntity createForFile(AssociableEntityDto associableEntity, Long scopeId) {
        AssociationEntity entity = new AssociationEntity();
        entity.setCreationDate(System.currentTimeMillis());
        entity.setAssociationScopeId(scopeId);
        setEntityForAssociation(entity, associableEntity);
        return entity;
    }

    private static void setEntityForAssociation(AssociationEntity entity, AssociableEntityDto associableEntity) {
        if (associableEntity instanceof FileTagDto) {
            entity.setFileTagId(((FileTagDto)associableEntity).getId());
        } else if (associableEntity instanceof DocTypeDto) {
            entity.setDocTypeId(((DocTypeDto)associableEntity).getId());
        } else if (associableEntity instanceof FunctionalRoleDto) {
            entity.setFunctionalRoleId(((FunctionalRoleDto)associableEntity).getId());
        } else if (associableEntity instanceof SimpleBizListItemDto) {
            entity.setSimpleBizListItemSid(((SimpleBizListItemDto)associableEntity).getSid());
        } else if (associableEntity instanceof DepartmentDto) {
            entity.setDepartmentId(((DepartmentDto)associableEntity).getId());
        }
    }

    private ClaFileDto associateToFile(AssociableEntityDto associableEntity, ClaFile claFile, BasicScope basicScope, SolrCommitType commitType) {
        AssociationEntity entity = createForFile(associableEntity, basicScope);
        addRemoveFileAssociation(SolrFieldOp.ADD, claFile.getId(), entity, commitType);

        claFile.setLastMetadataChangeDate(System.currentTimeMillis());
        logger.debug("Attach association {} to claFile {}", associableEntity, claFile.getId());
        setMetaDataCngDate(claFile.getId(), claFile.getContentMetadataId());

        Long contentId = claFile.getContentMetadataId();
        if (!solrFileCatRepository.checkIfTagIdentifierExistsForFile(claFile.getId(), associableEntity.createIdentifierString())) {
            Long scopeId = basicScope == null ? null : basicScope.getId();
            updateTagIdentfierToCategoryFile(claFile.getId(), contentId, associableEntity, scopeId, true, commitType);
        }
        return FileCatUtils.convertClaFile(claFile);
    }

    public ClaFileDto removeFileTagAssociationFromFile(ClaFile file, DocTypeDto docType, Long basicScopeId) {
        return removeDocTypeFromFile(docType, file, basicScopeId);
    }

    /////////////////////// folders

    public AssociationEntity createFolderAssociation(AssociableEntityDto associableEntity, DocFolder baseFolder,
                                                     boolean implicit, Long folderId, Long scopeId, AssociableEntityType associationType) {
        long startOperation = System.currentTimeMillis();
        Set<Pair<Long, Long>> pendingBatchTaskIds = mapBatchResultsProcessor.getPendingBatchTaskIds();
        AssociationEntity entity = new AssociationEntity();
        setEntityForAssociation(entity, associableEntity);
        entity.setCreationDate(System.currentTimeMillis());
        entity.setImplicit(implicit);
        entity.setAssociationScopeId(scopeId);

        Long rootFolderId;
        if (baseFolder != null) {
            entity.setOriginFolderId(baseFolder.getId());
            entity.setOriginDepthFromRoot(baseFolder.getDepthFromRoot());
            rootFolderId = baseFolder.getRootFolderId();
        } else {
            DocFolder docFolder = solrCatFolderRepository.getById(folderId);
            rootFolderId = docFolder.getRootFolderId();
        }
        String query = CatFoldersFieldType.ID.getSolrName() + ":" + folderId;
        addRemoveFolderAssociation(SolrFieldOp.ADD, query, entity, null);
        if (!implicit) {
            createFolderApplyOperation(folderId, entity, SolrFieldOp.ADD, startOperation, rootFolderId, associationType, pendingBatchTaskIds);
            entity.setImplicit(true); // for sub folders
            String pattern = baseFolder.getDepthFromRoot() + "." + baseFolder.getId();
            query = CatFoldersFieldType.PARENTS_INFO.getSolrName() + ":" + pattern;
            List<String> filters = Lists.newArrayList("-" + CatFoldersFieldType.ID.getSolrName() + ":" +baseFolder.getId());
            addRemoveFolderAssociation(SolrFieldOp.ADD, query, entity, filters);
            entity.setImplicit(false); // for return value
        }
        solrCatFolderRepository.softCommitNoWaitFlush();
        return entity;
    }

    public void createFolderApplyOperation(Long docFolderId, AssociationEntity entity, SolrFieldOp operation, long startOperation,
                                           Long rootFolderId, AssociableEntityType associationType, Set<Pair<Long, Long>> pendingBatchTaskIds) {
        String association = AssociationsService.convertFromAssociationEntity(entity);
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_FOLDER_ASSOCIATIONS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyFolderAssociationOnFiles.DOC_FOLDER_ID_PARAM, String.valueOf(docFolderId));
        data.getOpData().put(ApplyFolderAssociationOnFiles.ASSOCIATION_PARAM, association);
        data.getOpData().put(ApplyFolderAssociationOnFiles.ACTION_PARAM, operation.name());
        data.setUserOperation(true);
        data.setRetryLeft(retryForApplyFolderAssociationToFiles);

        Long runId = runService.runActiveForRootFolder(rootFolderId);
        if (runId != null) {
            Long jobId = jobManagerService.getJobIdCached(runId, JobType.SCAN);
            if (jobId != null) {
                if (!jobManagerService.getCachedJobState(jobId).equals(JobState.DONE)) {
                    data.setOpCondition(ApplyFolderAssociationOnFiles.createCondition(pendingBatchTaskIds));
                    data.getOpData().put(ApplyFolderAssociationOnFiles.START_OP_PARAM, String.valueOf(startOperation));
                }
            }
        }

        data.setDescription(operation.equals(SolrFieldOp.ADD) ?
                        messageHandler.getMessage("operation-description.assign-association-folder",
                                Lists.newArrayList(associationType == null ? "" : messageHandler.getMessage(associationType.getMsgCodeKey()))) :
                        messageHandler.getMessage("operation-description.remove-association-folder",
                                Lists.newArrayList(associationType == null ? "" : messageHandler.getMessage(associationType.getMsgCodeKey()))));
        scheduledOperationService.createNewOperation(data);
    }

    private void deleteFolderAssociationByOriginFolder(Long originFolder,
                                                       AssociationEntity explicitFileTagAssociation, AssociableEntityType associationType) {
        long startOperation = System.currentTimeMillis();
        Set<Pair<Long, Long>> pendingBatchTaskIds = mapBatchResultsProcessor.getPendingBatchTaskIds();
        DocFolder baseFolder = solrCatFolderRepository.getById(originFolder);
        String query = CatFoldersFieldType.ID.getSolrName() + ":" + originFolder;
        addRemoveFolderAssociation(SolrFieldOp.REMOVE, query, explicitFileTagAssociation, null);
        createFolderApplyOperation(originFolder, explicitFileTagAssociation, SolrFieldOp.REMOVE, startOperation, baseFolder.getRootFolderId(), associationType, pendingBatchTaskIds);
        explicitFileTagAssociation.setImplicit(true);
        String pattern = baseFolder.getDepthFromRoot() + "." + baseFolder.getId();
        query = CatFoldersFieldType.PARENTS_INFO.getSolrName() + ":" + pattern;
        List<String> filters = Lists.newArrayList("-" + CatFoldersFieldType.ID.getSolrName() + ":" +baseFolder.getId());
        addRemoveFolderAssociation(SolrFieldOp.REMOVE_REGEX, query, explicitFileTagAssociation, filters);
        solrCatFolderRepository.softCommitNoWaitFlush();
    }

    public void markFolderAssociationAsDeleted(Long folderId, AssociableEntityDto associableEntity, Long scopeId, AssociableEntityType associationType) {
        AssociationEntity explicitFileTagAssociation = findExplicitFolderAssociationFilterDeleted(folderId, associableEntity, scopeId);
        if (explicitFileTagAssociation == null) {
            throw new BadRequestException(messageHandler.getMessage("association.missing",
                    Lists.newArrayList(associableEntity.getName(), folderId)), BadRequestType.ITEM_NOT_FOUND);
        }

        logger.debug("No implicit tag on base folder - remove associations related to baseFolder");
        deleteFolderAssociationByOriginFolder(folderId, explicitFileTagAssociation, associationType);
    }

    private AssociationEntity findFolderAssociation(long folderId, AssociableEntityDto associableEntity, Long scopeId, boolean implicit) {
        DocFolder folder = solrCatFolderRepository.getById(folderId);
        List<AssociationEntity> associations = convertFromAssociationEntity(folderId, folder.getAssociations(), AssociationType.FOLDER);

        if (associations != null && !associations.isEmpty()) {
            for (AssociationEntity folderAssociation : associations) {
                if (((associableEntity instanceof FileTagDto && folderAssociation.getFileTagId() != null && ((FileTagDto) associableEntity).getId().equals(folderAssociation.getFileTagId())) ||
                        (associableEntity instanceof DocTypeDto && folderAssociation.getDocTypeId() != null && ((DocTypeDto) associableEntity).getId().equals(folderAssociation.getDocTypeId())) ||
                        (associableEntity instanceof FunctionalRoleDto && ((FunctionalRoleDto) associableEntity).getId().equals(folderAssociation.getFunctionalRoleId())) ||
                        (associableEntity instanceof DepartmentDto && ((DepartmentDto) associableEntity).getId().equals(folderAssociation.getDepartmentId())) ||
                        (associableEntity instanceof SimpleBizListItemDto && ((SimpleBizListItemDto) associableEntity).getSid().equals(folderAssociation.getSimpleBizListItemSid()))) &&
                        Objects.equals(folderAssociation.getAssociationScopeId(), scopeId) &&
                        folderAssociation.getImplicit() != null && folderAssociation.getImplicit() == implicit) {
                    logger.debug("{} associated to folder {}", associableEntity, folderId);
                    return folderAssociation;
                }
            }
        }
        return null;
    }

    private AssociationEntity findExplicitFolderAssociationFilterDeleted(Long folderId,
                                                                         AssociableEntityDto associableEntity, Long scopeId) {
        return findFolderAssociation(folderId, associableEntity, scopeId, false);
    }

    public long countUnDeletedDepartmentAssociations(Department associableEntity) {
        QueryResponse resp = solrCatFolderRepository.runQuery(Query.create()
                .addField(CatFoldersFieldType.ID)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*departmentId\\\":"+associableEntity.getId()+",*",
                                "*departmentId\\\":"+associableEntity.getId()+"\\}*")
                )).build());
        return resp.getResults().getNumFound();
    }

    public boolean bizListAssociationExists(String simpleBizListItemId) {
        QueryResponse resp = solrCatFolderRepository.runQuery(Query.create()
                .addField(CatFoldersFieldType.ID)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*simpleBizListItemSid\\\":"+simpleBizListItemId+",*",
                                "*simpleBizListItemSid\\\":"+simpleBizListItemId+"\\}*"))).build());
        long res = resp.getResults().getNumFound();
        if (res > 0) {
            return true;
        }

        resp = fileGroupRepository.runQuery(Query.create()
                .addField(CatFoldersFieldType.ID)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*simpleBizListItemSid\\\":"+simpleBizListItemId+",*",
                                "*simpleBizListItemSid\\\":"+simpleBizListItemId+"\\}*"))).build());
        res = resp.getResults().getNumFound();
        if (res > 0) {
            return true;
        }

        resp = solrFileCatRepository.runQuery(Query.create()
                .addField(CatFileFieldType.ID)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*simpleBizListItemSid\\\":"+simpleBizListItemId+",*",
                        "*simpleBizListItemSid\\\":"+simpleBizListItemId+"\\}*"))).build());
        res = resp.getResults().getNumFound();
        return res > 0;
    }

    public boolean funcRoleAssociationExists(Long funcRoleId) {
        return checkAssociationExistsInSolr("functionalRoleId", funcRoleId);
    }

    public boolean docTypeAssociationExists(Long docTypeId) {
        return checkAssociationExistsInSolr("docTypeId", docTypeId);
    }

    public boolean tagAssociationExists(Long tagId) {
        return checkAssociationExistsInSolr("fileTagId", tagId);
    }

    public long docTypeAssociationCountForGroups(long minGroupSize) {
        String associationName = "docTypeId";
        QueryResponse resp = fileGroupRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.DELETED, SolrOperator.EQ, false))
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.PARENT_GROUP_ID, SolrOperator.MISSING, null))
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.NUM_OF_FILES, SolrOperator.GT, minGroupSize))
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":*",
                                "*"+associationName+"\\\":*"))).build());

        return resp.getResults().getNumFound();
    }

    private boolean checkAssociationExistsInSolr(String associationName, Long associationId) {
        QueryResponse resp = solrCatFolderRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        long res = resp.getResults().getNumFound();
        if (res > 0) {
            return true;
        }

        resp = fileGroupRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        res = resp.getResults().getNumFound();
        if (res > 0) {
            return true;
        }

        resp = solrFileCatRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        res = resp.getResults().getNumFound();
        return res > 0;
    }

    private long countAssociationExistsInSolr(String associationName, Long associationId) {
        QueryResponse resp = solrCatFolderRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        long res = resp.getResults().getNumFound();

        resp = fileGroupRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        res += resp.getResults().getNumFound();

        resp = solrFileCatRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        res += resp.getResults().getNumFound();
        return res;
    }

    public Page<DocTypeAssociation> findDocTypeAssociations(DocType docType, FileTagSource associationSource, long offset, int rows) {
        Page<DocTypeAssociation> assoc = null;
        switch(associationSource) {
            case BUSINESS_ID:
                assoc = getAssociationFromSolrForGroup(docType, offset, rows);
                break;
            case FOLDER_EXPLICIT:
            case FOLDER_IMPLICIT:
                assoc = getAssociationFromSolrForFolder(docType, offset, rows);
                break;
            case FILE:
                assoc = getAssociationFromSolrForFile(docType, offset, rows);
                break;
        }

        return assoc;
    }

    public List<DocTypeAssociation> getAssociationFromSolr(DocType docType) {
        List<DocTypeAssociation> result = new ArrayList<>();

        result.addAll(getAssociationFromSolrForFolder(docType, 0, 100000).getContent());
        result.addAll(getAssociationFromSolrForFile(docType, 0, 100000).getContent());
        result.addAll(getAssociationFromSolrForGroup(docType, 0,100000).getContent());

        return result;
    }

    private Page<DocTypeAssociation> getAssociationFromSolrForFolder(DocType docType, long offset, int rows) {
        String associationName = "docTypeId";
        String associationId = String.valueOf(docType.getId());
        List<DocTypeAssociation> result = new ArrayList<>();

        QueryResponse resp = solrCatFolderRepository.runQuery(Query.create().setRows(rows).setStart((int)offset)
                .addField(CatFoldersFieldType.ID).addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        if (resp.getResults().size() > 0) {
            resp.getResults().forEach(doc -> {
                String folderId = (String)doc.getFieldValue(CatFoldersFieldType.ID.getSolrName());
                List<String> associations = (List<String>)doc.getFieldValue(CatFoldersFieldType.FOLDER_ASSOCIATIONS.getSolrName());
                DocTypeAssociation dta = findAndConvert(associations, docType, AssociationType.FOLDER, folderId);
                if (dta != null) {
                    dta.setFolderId(Long.parseLong(folderId));
                    dta.setAssociationSource(dta.isImplicit() ? FileTagSource.FOLDER_IMPLICIT : FileTagSource.FOLDER_EXPLICIT);
                }
                result.add(dta);
            });
        }

        return new PageImpl<>(result, PageRequest.of(0, rows), resp.getResults().getNumFound());
    }

    public Map<Long, List<String>> getAssociationFromSolrForFile(String associationName, String associationId, long offset, int rows) {
        Map<Long, List<String>> result = new HashMap<>();
        QueryResponse resp = solrFileCatRepository.runQuery(Query.create().setRows(rows).setStart((int)offset)
                .addField(CatFileFieldType.FILE_ID).addField(CatFileFieldType.FILE_ASSOCIATIONS)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        if (resp.getResults().size() > 0) {
            resp.getResults().forEach(doc -> {
                Long fileId = (Long)doc.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());
                List<String> associations = (List<String>)doc.getFieldValue(CatFileFieldType.FILE_ASSOCIATIONS.getSolrName());
                result.put(fileId, associations);
            });
        }
        return result;
    }

    private Page<DocTypeAssociation> getAssociationFromSolrForFile(DocType docType, long offset, int rows) {
        String associationName = "docTypeId";
        String associationId = String.valueOf(docType.getId());
        List<DocTypeAssociation> result = new ArrayList<>();

        QueryResponse resp = solrFileCatRepository.runQuery(Query.create().setRows(rows).setStart((int)offset)
                .addField(CatFileFieldType.FILE_ID).addField(CatFileFieldType.FILE_ASSOCIATIONS)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        if (resp.getResults().size() > 0) {
            resp.getResults().forEach(doc -> {
                Long fileId = (Long)doc.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());
                List<String> associations = (List<String>)doc.getFieldValue(CatFileFieldType.FILE_ASSOCIATIONS.getSolrName());
                DocTypeAssociation dta = findAndConvert(associations, docType, AssociationType.FILE, String.valueOf(fileId));
                if (dta != null) {
                    dta.setClaFileId(fileId);
                    dta.setAssociationSource(FileTagSource.FILE);
                }
                result.add(dta);
            });
        }

        return new PageImpl<>(result, PageRequest.of(0, rows), resp.getResults().getNumFound());
    }

    private Page<DocTypeAssociation> getAssociationFromSolrForGroup(DocType docType, long offset, int rows) {
        String associationName = "docTypeId";
        String associationId = String.valueOf(docType.getId());
        List<DocTypeAssociation> result = new ArrayList<>();

        QueryResponse resp = fileGroupRepository.runQuery(Query.create().setRows(rows).setStart((int)offset)
                .addField(FileGroupsFieldType.ID).addField(FileGroupsFieldType.GROUP_ASSOCIATIONS)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        if (resp.getResults().size() > 0) {
            resp.getResults().forEach(doc -> {
                String groupId = (String)doc.getFieldValue(FileGroupsFieldType.ID.getSolrName());
                List<String> associations = (List<String>)doc.getFieldValue(FileGroupsFieldType.GROUP_ASSOCIATIONS.getSolrName());
                DocTypeAssociation dta = findAndConvert(associations, docType, AssociationType.GROUP, groupId);
                if (dta != null) {
                    dta.setGroupId(groupId);
                    dta.setAssociationSource(FileTagSource.BUSINESS_ID);
                }
                result.add(dta);
            });
        }
        return new PageImpl<>(result, PageRequest.of(0, rows), resp.getResults().getNumFound());
    }

    private DocTypeAssociation findAndConvert(List<String> associationsStr, DocType docType, AssociationType type, String id) {
        List<AssociationEntity> associations = convertFromAssociationEntity(id, associationsStr, type);
        List<AssociationEntity> docTypes = getDocTypeAssociation(associations);

        for (AssociationEntity dta : docTypes) {
            if (dta.getDocTypeId().equals(docType.getId())) {
                DocTypeAssociation result = new DocTypeAssociation();
                result.setDocType(docType);
                if (dta.getAssociationScopeId() != null) {
                    result.setScope(scopeService.getFromCache(dta.getAssociationScopeId()));
                }
                if (dta.getImplicit() != null) {
                    result.setImplicit(dta.getImplicit());
                }
                result.setDeleted(false);
                result.setDirty(false);
                result.setOriginDepthFromRoot(dta.getOriginDepthFromRoot());
                result.setOriginFolderId(dta.getOriginFolderId());
                return result;
            }
        }
        return null;
    }

    /////////////////////// groups

    public AssociationEntity createGroupAssociation(AssociableEntity associableEntity, String groupId, Long scopeId, AssociableEntityType associationType) {
        AssociableEntityDto entity = getForAssociableEntity(associableEntity);
        return createGroupAssociation(entity, groupId, scopeId, associationType);
    }

    public AssociationEntity createGroupAssociation(AssociableEntityDto associableEntity, String groupId, Long scopeId, AssociableEntityType associationType) {
        AssociationEntity entity = new AssociationEntity();
        setEntityForAssociation(entity, associableEntity);
        entity.setCreationDate(System.currentTimeMillis());
        entity.setImplicit(false);
        entity.setAssociationScopeId(scopeId);
        addRemoveGroupAssociation(SolrFieldOp.ADD, groupId, entity);
        createGroupApplyOperation(groupId, entity, SolrFieldOp.ADD, associationType);
        fileGroupRepository.softCommitNoWaitFlush();
        return entity;
    }

    public void createGroupApplyOperation(String groupId, AssociationEntity entity, SolrFieldOp operation, AssociableEntityType associationType) {
        String association = AssociationsService.convertFromAssociationEntity(entity);
        createGroupApplyOperation(groupId, association, operation, associationType);
    }

    public void createGroupApplyOperation(String groupId, String association, SolrFieldOp operation, AssociableEntityType associationType) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_GROUP_ASSOCIATIONS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyGroupAssociationOnFiles.GROUP_ID_PARAM, groupId);
        data.getOpData().put(ApplyGroupAssociationOnFiles.ASSOCIATION_PARAM, association);
        data.getOpData().put(ApplyGroupAssociationOnFiles.ACTION_PARAM, operation.name());
        data.setUserOperation(true);
        data.setPriority(ScheduledOperationPriority.LOW);
        data.setRetryLeft(retryForApplyGroupAssociationToFiles);
        data.setDescription(operation.equals(SolrFieldOp.ADD) ?
                messageHandler.getMessage("operation-description.assign-association-group",
                        Lists.newArrayList(associationType == null ? "" : messageHandler.getMessage(associationType.getMsgCodeKey()))) :
                messageHandler.getMessage("operation-description.remove-association-group",
                        Lists.newArrayList(associationType == null ? "" : messageHandler.getMessage(associationType.getMsgCodeKey()))));
        scheduledOperationService.createNewOperation(data);
    }

    private void addRemoveGroupAssociation(SolrFieldOp operation, String groupId, AssociationEntity entity) {
        String association = AssociationsService.convertFromAssociationEntity(entity);
        SolrInputDocument doc = new SolrInputDocument();
        doc.setField(FileGroupsFieldType.ID.getSolrName(), groupId);
        SolrFileGroupRepository.addField(doc, FileGroupsFieldType.GROUP_ASSOCIATIONS, association, operation);
        SolrFileGroupRepository.addField(doc, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
        fileGroupRepository.atomicUpdate(doc);
    }

    public void deleteGroupAssociation(String groupId, AssociationEntity entity, AssociableEntityType associationType) {
        addRemoveGroupAssociation(SolrFieldOp.REMOVE, groupId, entity);
        createGroupApplyOperation(groupId, entity, SolrFieldOp.REMOVE, associationType);
        fileGroupRepository.softCommitNoWaitFlush();
    }

    public AssociationEntity findGroupAssociation(String groupId, AssociableEntity associableEntity, Long scopeId) {
        FileGroup group = fileGroupRepository.getById(groupId).orElse(null);
        List<AssociationEntity> associations = convertFromAssociationEntity(groupId, group.getAssociations(), AssociationType.GROUP);

        if (associations != null && !associations.isEmpty()) {
            for (AssociationEntity folderAssociation : associations) {
                if (((associableEntity instanceof FileTag && folderAssociation.getFileTagId() != null && ((FileTag) associableEntity).getId().equals(folderAssociation.getFileTagId())) ||
                        (associableEntity instanceof DocType && folderAssociation.getDocTypeId() != null && folderAssociation.getDocTypeId().equals(((DocType) associableEntity).getId())) ||
                        (associableEntity instanceof FunctionalRole && ((FunctionalRole) associableEntity).getId().equals(folderAssociation.getFunctionalRoleId())) ||
                        (associableEntity instanceof Department && ((Department) associableEntity).getId().equals(folderAssociation.getDepartmentId())) ||
                        (associableEntity instanceof SimpleBizListItem && ((SimpleBizListItem) associableEntity).getSid().equals(folderAssociation.getSimpleBizListItemSid()))) &&
                        Objects.equals(folderAssociation.getAssociationScopeId(), scopeId)) {
                    logger.debug("{} associated to group {}", associableEntity, groupId);
                    return folderAssociation;
                }
            }
        }
        return null;
    }

    public AssociationEntity findGroupAssociation(List<AssociationEntity> associations, AssociableEntity associableEntity, Long scopeId) {
        if (associations != null && !associations.isEmpty()) {
            for (AssociationEntity folderAssociation : associations) {
                if (((associableEntity instanceof FileTag && folderAssociation.getFileTagId() != null && ((FileTag) associableEntity).getId().equals(folderAssociation.getFileTagId())) ||
                        (associableEntity instanceof DocType && folderAssociation.getDocTypeId() != null && folderAssociation.getDocTypeId().equals(((DocType) associableEntity).getId())) ||
                        (associableEntity instanceof FunctionalRole && ((FunctionalRole) associableEntity).getId().equals(folderAssociation.getFunctionalRoleId())) ||
                        (associableEntity instanceof Department && ((Department) associableEntity).getId().equals(folderAssociation.getDepartmentId())) ||
                        (associableEntity instanceof SimpleBizListItem && ((SimpleBizListItem) associableEntity).getSid().equals(folderAssociation.getSimpleBizListItemSid()))) &&
                        Objects.equals(folderAssociation.getAssociationScopeId(), scopeId)) {
                    return folderAssociation;
                }
            }
        }
        return null;
    }

    private FileGroup getFileGroupByIdAssociations(String fileGroupId) {
        return fileGroupRepository.getById(fileGroupId).orElse(null);
    }

    ///////////////////// doc type

    private ClaFileDto removeDocTypeFromFile(ClaFile claFile, DocTypeDto docType, Long basicScopeId) {

        List<AssociationEntity> associations = convertFromAssociationEntity(claFile.getId(), claFile.getAssociations(), AssociationType.FILE);
        AssociationEntity association = getDocTypeAssociation(associations, docType.getId(), basicScopeId);
        if (association == null) {
            logger.debug("Explicit docType {} is not associated with file {} Can't remove", docType.getId(), claFile.getId());
            return null;
        }
        //File tag is assoicated - remove it
        logger.debug("Remove docType {} from claFile {}", docType.getId(), claFile.getId());
        addRemoveFileAssociation(SolrFieldOp.REMOVE, claFile.getId(), association, SolrCommitType.SOFT_NOWAIT);
        claFile.setLastMetadataChangeDate(System.currentTimeMillis());
        setMetaDataCngDate(claFile.getId(), claFile.getContentMetadataId());
        return FileCatUtils.convertClaFile(claFile);

    }

    private ClaFileDto removeDocTypeFromFile(DocTypeDto docType, ClaFile file, Long basicScopeId) {
        //Remove from claFile
        ClaFileDto claFileDto = removeDocTypeFromFile(file, docType, basicScopeId);
        //Remove from solr if needed
        if (isDocTypeFileInSolr(file.getId(), docType, basicScopeId)) {
            //Remove
            Long contentId = file.getContentMetadataId();
            removeDocTypeFromCategoryFile(file.getId(), contentId, docType, basicScopeId, true);
        }
        return claFileDto;
    }

    private DocTypeAssociationDto getExplicitDocTypeAssociation(Set<DocTypeAssociationDto> associations, DocTypeDto docType, BasicScope basicScope) {
        if (associations == null) {
            return null;
        }
        for (DocTypeAssociationDto association : associations) {
            if (association.getFileTagSource() == FileTagSource.FILE &&
                    !association.isImplicit() &&
                    Objects.equals(association.getDocTypeDto().getId(), docType.getId()) &&
                    Objects.equals(association.getScope(), basicScope)) {
                //Found it
                return association;
            }
        }
        return null;
    }

    public AssociationEntity getDocTypeAssociation(Collection<AssociationEntity> associations, Long docTypeId, Long basicScopeId) {
        if (associations == null) {
            return null;
        }
        for (AssociationEntity association : associations) {
            if (Objects.equals(association.getDocTypeId(), docTypeId) &&
                    Objects.equals(association.getAssociationScopeId(), basicScopeId)) {
                //Found it
                return association;
            }
        }
        return null;
    }

    public boolean isDocTypeFileInSolr(long fileId, DocTypeDto docTypeDto, Long scopeId) {
        return solrFileCatRepository.docTypeFileExists(fileId, docTypeDto, scopeId);
    }

    public long countDocTypeAssociations(DocType docType) {
        return countAssociationExistsInSolr("docTypeId", docType.getId());
    }

    public Map<FileTagSource, Long> countDocTypeAssociationsBySource(DocType docType) {
        Map<FileTagSource, Long> result = new HashMap<>();
        String associationName = "docTypeId";
        Long associationId = docType.getId();

        QueryResponse resp = solrCatFolderRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*implicit"+"\\\":false,*"))).build());
        long res = resp.getResults().getNumFound();
        result.put(FileTagSource.FOLDER_EXPLICIT, res);

        resp = solrCatFolderRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*implicit"+"\\\":true,*"))).build());
        res = resp.getResults().getNumFound();
        result.put(FileTagSource.FOLDER_IMPLICIT, res);

        resp = fileGroupRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        res = resp.getResults().getNumFound();
        result.put(FileTagSource.BUSINESS_ID, res);

        resp = solrFileCatRepository.runQuery(Query.create()
                .setRows(0)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*"+associationName+"\\\":"+associationId+",*",
                                "*"+associationName+"\\\":"+associationId+"\\}*"))).build());
        res = resp.getResults().getNumFound();
        result.put(FileTagSource.FILE, res);

        return result;
    }

    public void markAssociationByOriginFolderAsDeleted(DocTypeDto docType, Long docFolder, Long basicScopeId, AssociableEntityType associationType) {
        markFolderAssociationAsDeleted(docFolder, docType, basicScopeId, associationType);
    }

    public AssociationEntity findExplicitFolderAssociationByScope(Long folderId, DocTypeDto docType, BasicScope basicScope) {
        return findFolderAssociation(folderId, docType, basicScope == null ? null : basicScope.getId() , false);
    }

    public void updateTagsFromDocType(long fileId, DocType docType, Long scopeId, boolean addTag) {
        docType.getFileTagSettings().forEach(fts -> {
            FileTag fileTag = fts.getFileTag();
            String solrKey = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fileTag, scopeId, TagPriority.FILE);
            try {
                updateScopedTagByQuery(
                        Criteria.create(FILE_ID, SolrOperator.EQ, fileId),
                        addTag,
                        fts.getFileTag(),
                        solrKey,
                        SolrCommitType.SOFT_NOWAIT);
            } catch (Exception e) {
                throw new RuntimeException(String.format("Failed to apply for file (id:%s), file tag settings (tagId:%d, tagTypeId:%d)",
                        fileId, fileTag.getId(), fileTag.getType().getId()), e);
            }
        });
    }

    ///////////////////// generic

    private TagPriority resolveTagPriority(Object association) {
        if (association instanceof TagAssociation) {
            AssociationType type = ((TagAssociation)association).getAssociationType();
            switch (type) {
                case FILE:
                    return TagPriority.FILE;
                case FOLDER:
                    return TagPriority.folder(((TagAssociation) association).getOriginDepthFromRoot());
                case GROUP:
                    return TagPriority.FILE_GROUP;
            }
        } else if (association instanceof DocTypeAssociation) {
            FileTagSource associationSource = ((DocTypeAssociation) association).getAssociationSource();
            switch (associationSource) {
                case FOLDER_EXPLICIT:
                case FOLDER_IMPLICIT:
                    return TagPriority.folder(((DocTypeAssociation) association).getOriginDepthFromRoot());
                case FILE:
                    return TagPriority.FILE;
                case BUSINESS_ID:
                    return TagPriority.FILE_GROUP;
            }
        } else if (association instanceof AssociationEntity) {
            AssociationEntity entity = (AssociationEntity) association;
            if (entity.getOriginDepthFromRoot() != null) {
                return TagPriority.folder(entity.getOriginDepthFromRoot());
            } else {
                return TagPriority.FILE_GROUP;
            }
        }
        throw new RuntimeException("Unsupported association type " + association);
    }

    public void deleteAllForDocType(DocType docType) {
        deleteFolderAssociations(docType);
        deleteGroupAssociations(docType);
        deleteFileAssociations(docType);
    }

    private void deleteGroupAssociations(DocType docType) {
        List<SolrFileGroupEntity> groups = fileGroupRepository.find(Query.create()
                .addField(FileGroupsFieldType.ID)
                .addField(FileGroupsFieldType.GROUP_ASSOCIATIONS)
                .addFilterWithCriteria(Criteria.create(FileGroupsFieldType.GROUP_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*docTypeId\\\":"+docType.getId()+",*",
                                "*docTypeId\\\":"+docType.getId()+"\\}*")
                )).build());

        if (groups == null) {
            return;
        }

        groups.forEach(group -> {
            List<AssociationEntity> associations =
                    AssociationsService.convertFromAssociationEntity(group.getId(), group.getAssociations(), AssociationType.GROUP);
            associations.forEach(assoc -> {
                if (!assoc.getImplicit() && assoc.getDocTypeId() != null && assoc.getDocTypeId().equals(docType.getId())) {
                    deleteGroupAssociation(group.getId(), assoc, AssociableEntityType.DOC_TYPE);
                }
            });
        });
    }


    private void deleteFolderAssociations(DocType docType) {
        List<SolrFolderEntity> folders = solrCatFolderRepository.find(Query.create()
                .addField(CatFoldersFieldType.ID)
                .addField(CatFoldersFieldType.FOLDER_ASSOCIATIONS)
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.FOLDER_ASSOCIATIONS, SolrOperator.IN,
                        Lists.newArrayList("*docTypeId\\\":"+docType.getId()+",*",
                                "*docTypeId\\\":"+docType.getId()+"\\}*")
                )).build());

        if (folders == null) {
            return;
        }

        folders.forEach(folder -> {
            List<AssociationEntity> associations =
                    AssociationsService.convertFromAssociationEntity(folder.getId(), folder.getAssociations(), AssociationType.FOLDER);
            associations.forEach(assoc -> {
                if (!assoc.getImplicit() && assoc.getDocTypeId() != null && assoc.getDocTypeId().equals(docType.getId())) {
                    markAssociationByOriginFolderAsDeleted(
                            docTypeService.getFromCache(docType.getId()), folder.getId(), assoc.getAssociationScopeId(), AssociableEntityType.DOC_TYPE);
                }
            });
        });
    }

    @SuppressWarnings("unchecked")
    private void deleteFileAssociations(DocType docType) {
        int numFound;
        do {
            Query query = Query.create().setStart(0).setRows(1000)
                    .addFilterWithCriteria(Criteria.create(CatFileFieldType.DOC_TYPES, SolrOperator.EQ, docType.getDocTypeIdentifier()))
                    .addField(CatFileFieldType.ID).addField(CatFileFieldType.FILE_ID).addField(CatFileFieldType.ACTIVE_ASSOCIATIONS)
                    .addField(CatFileFieldType.DOC_TYPES).addField(CatFileFieldType.SCOPED_TAGS)
                    .addField(CatFileFieldType.TAGS_2).addField(CatFileFieldType.FILE_ASSOCIATIONS);
            QueryResponse resp = solrFileCatRepository.runQuery(query.build());
            numFound = resp.getResults().size();
            if (numFound > 0) {
                List<SolrInputDocument> changes = new ArrayList<>();
                resp.getResults().forEach(doc -> {
                    SolrInputDocument cng = new SolrInputDocument();
                    cng.setField(CatFileFieldType.ID.getSolrName(), doc.getFieldValue(CatFileFieldType.ID.getSolrName()));
                    SolrFileCatRepository.setFieldOnce(cng, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));

                    Map<String, Object> fieldModifier = Maps.newHashMapWithExpectedSize(1);
                    fieldModifier.put("remove", docType.getDocTypeIdentifier());
                    cng.setField(CatFileFieldType.DOC_TYPES.getSolrName(), fieldModifier);
                    cng.setField(CatFileFieldType.TAGS_2.getSolrName(), fieldModifier);

                    Long fileId = (Long)doc.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());

                    List<String> jsons = (List<String>)doc.getFieldValue(CatFileFieldType.FILE_ASSOCIATIONS.getSolrName());
                    List<String> jsonToRem = new ArrayList<>();
                    List<String> solrKeyToRem = new ArrayList<>();
                    if (jsons != null && !jsons.isEmpty()) {
                        for (String json : jsons) {
                            AssociationEntity entity = convertFromAssociationEntity(fileId, json, AssociationType.FILE);
                            if (entity != null && entity.getDocTypeId() != null && entity.getDocTypeId() == docType.getId()) {
                                jsonToRem.add(json);
                                String solrKey = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(docType.getDocTypeIdentifier(), entity.getAssociationScopeId(), TagPriority.FILE);
                                solrKeyToRem.add(solrKey);
                            }
                        }
                        jsons.removeAll(jsonToRem);
                        SolrFileCatRepository.setField(cng, CatFileFieldType.FILE_ASSOCIATIONS, SolrFileCatRepository.atomicSetValue(jsons));
                    }

                    String[] scopedTags = ((List<String>) doc.getFieldValue(CatFileFieldType.SCOPED_TAGS.getSolrName()))
                            .toArray(new String[0]);
                    if (scopedTags.length > 0 && !solrKeyToRem.isEmpty()) {
                        for (String key : solrKeyToRem) {
                            PriorityScopedValueResolver priorityScopedValueResolver = new PriorityScopedValueResolver(key);
                            scopedTags = priorityScopedValueResolver.resolveNewScopedValues(scopedTags, false);
                        }
                        SolrFileCatRepository.setField(cng, CatFileFieldType.SCOPED_TAGS, SolrFileCatRepository.atomicSetValue(scopedTags));
                    }

                    List<String> activeAssoc = ((List<String>)doc.getFieldValue(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName()));
                    if (activeAssoc != null && !activeAssoc.isEmpty() && !solrKeyToRem.isEmpty()) {
                        for (String key : solrKeyToRem) {
                            if (activeAssoc.contains(key)) {
                                activeAssoc.remove(key);
                            }
                        }
                        SolrFileCatRepository.setField(cng, CatFileFieldType.ACTIVE_ASSOCIATIONS, SolrFileCatRepository.atomicSetValue(activeAssoc));
                    }
                    changes.add(cng);
                });

                if (changes.size() > 0) {
                    solrFileCatRepository.saveAll(changes);
                    solrFileCatRepository.softCommitNoWaitFlush();
                }
            }
        } while(numFound > 0);
    }

    @SuppressWarnings("unchecked")
    public void moveDocTypeSolrFiles(String newIdentifier, String oldIdentifier) {
        int numFound;
        do {
            Query query = Query.create().setStart(0).setRows(1000)
                    .addFilterWithCriteria(Criteria.create(CatFileFieldType.DOC_TYPES, SolrOperator.EQ, oldIdentifier))
                    .addField(CatFileFieldType.ID).addField(CatFileFieldType.FILE_ID).addField(CatFileFieldType.ACTIVE_ASSOCIATIONS)
                    .addField(CatFileFieldType.DOC_TYPES).addField(CatFileFieldType.SCOPED_TAGS)
                    .addField(CatFileFieldType.TAGS_2).addField(CatFileFieldType.FILE_ASSOCIATIONS);
            QueryResponse resp = solrFileCatRepository.runQuery(query.build());
            numFound = resp.getResults().size();
            if (numFound > 0) {
                List<SolrInputDocument> changes = new ArrayList<>();
                resp.getResults().forEach(doc -> {
                    SolrInputDocument cng = new SolrInputDocument();
                    cng.setField(CatFileFieldType.ID.getSolrName(), doc.getFieldValue(CatFileFieldType.ID.getSolrName()));
                    SolrFileCatRepository.setFieldOnce(cng, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));

                    List<String> docTypes = (List<String>)doc.getFieldValue(CatFileFieldType.DOC_TYPES.getSolrName());
                    docTypes.remove(oldIdentifier);
                    docTypes.add(newIdentifier);
                    SolrFileCatRepository.setField(cng, CatFileFieldType.DOC_TYPES, SolrFileCatRepository.atomicSetValue(docTypes));

                    List<String> tags2 = (List<String>)doc.getFieldValue(CatFileFieldType.TAGS_2.getSolrName());
                    tags2.remove(oldIdentifier);
                    tags2.add(newIdentifier);
                    SolrFileCatRepository.setField(cng, CatFileFieldType.TAGS_2, SolrFileCatRepository.atomicSetValue(tags2));

                    String oldPart = FileCatUtils.CATEGORY_SEPARATOR + AssociationIdUtils.docTypeIdHierarchy(oldIdentifier) + FileCatUtils.CATEGORY_SEPARATOR;
                    String newPart = FileCatUtils.CATEGORY_SEPARATOR + AssociationIdUtils.docTypeIdHierarchy(newIdentifier) + FileCatUtils.CATEGORY_SEPARATOR;

                    String[] scopedTags = ((List<String>) doc.getFieldValue(CatFileFieldType.SCOPED_TAGS.getSolrName())).toArray(new String[0]);
                    List<String> newScopedTags = new ArrayList<>();
                    if (scopedTags != null && scopedTags.length > 0) {

                        for (String tag : scopedTags) {
                            if (tag.startsWith(DocTypeDto.ID_PREFIX)) {
                                newScopedTags.add(tag.replace(oldPart, newPart));
                            } else {
                                newScopedTags.add(tag);
                            }
                        }
                        SolrFileCatRepository.setField(cng, CatFileFieldType.SCOPED_TAGS, SolrFileCatRepository.atomicSetValue(newScopedTags));
                    }

                    List<String> activeAssociations = ((List<String>)doc.getFieldValue(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName()));
                    List<String> newActiveAssociations = new ArrayList<>();
                    if (activeAssociations != null && !activeAssociations.isEmpty()) {
                        activeAssociations.forEach(assoc -> {
                            if (assoc.startsWith(DocTypeDto.ID_PREFIX)) {
                                newActiveAssociations.add(assoc.replace(oldPart, newPart));
                            } else {
                                newActiveAssociations.add(assoc);
                            }
                        });
                        SolrFileCatRepository.setField(cng, CatFileFieldType.ACTIVE_ASSOCIATIONS, SolrFileCatRepository.atomicSetValue(newActiveAssociations));
                    }
                    changes.add(cng);
                });

                if (changes.size() > 0) {
                    solrFileCatRepository.saveAll(changes);
                    solrFileCatRepository.softCommitNoWaitFlush();
                }
            }
        } while(numFound > 0);
    }


    private void addDocTypeAssociationToIds(Collection<AssociationEntity> associations, Set<String> ids) {
        for (AssociationEntity claFileDocTypeAssociation : associations) {
            if (claFileDocTypeAssociation.getDocTypeId() != null) {
                String docTypeIdentifier = docTypeService.getFromCache(claFileDocTypeAssociation.getDocTypeId()).getDocTypeIdentifier();
                ids.add(docTypeIdentifier);
            }
        }
    }


    // populate scopedTags field for doc type associations
    private void addDocTypeAssociationsData(Collection<AssociationEntity> docTypeAssociations, Set<String> ids3, TagPriority priority) {
        Set<String> priorityScopedResults = new HashSet<>(ids3);
        Set<String> scopedResults = new HashSet<>();
        for (AssociationEntity association : docTypeAssociations) {
            if (association.getDocTypeId() != null) {
                Long scopeId = association.getAssociationScopeId();
                DocTypeDto docType = docTypeService.getFromCache(association.getDocTypeId());
                String identifier = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(docType.getDocTypeIdentifier(), scopeId,
                        priority != null ? priority : resolveTagPriority(association));
                if (docType.isSingleValue()) {
                    PriorityScopedValueResolver priorityScopedValueResolver =
                            new PriorityScopedValueResolver(identifier);
                    String[] scopedTagValues = priorityScopedResults.toArray(new String[]{});
                    priorityScopedResults = Sets.newHashSet(priorityScopedValueResolver.resolveNewScopedValues(scopedTagValues, true));
                } else {
                    scopedResults.add(identifier);
                }
            }
        }
        ids3.clear();
        ids3.addAll(priorityScopedResults);
        ids3.addAll(scopedResults);
    }

    private String getDepartment(Collection<AssociationEntity> associations) {
        String department = null;
        for (AssociationEntity association : associations) {
            if (association.getDepartmentId() != null) {
                String identifier = AssociationIdUtils.createDepartmentIdentifier(association.getDepartmentId(), resolveTagPriority(association));
                PriorityScopedValueResolver priorityScopedValueResolver = new PriorityScopedValueResolver(identifier);
                String[] values = department == null ? new String[]{} : new String[]{department};
                String[] result = priorityScopedValueResolver.resolveNewScopedValues(values , true);
                department = result[0];
            }
        }
        return department;
    }

    // add tags, data roles and biz list item associations
    // populate tags and scopedTags fields for tags
    // populate tags2 for all 3
    private void addAssociationIds(TagPriority priority, List<AssociationEntity> associations, Set<String> ids,
                                   Set<String> ids2, Set<String> ids3, Set<String> dataRoles, Set<String> bizListItems) {
        Set<String> priorityScopedResults = new HashSet<>(ids3);
        Set<String> scopedResults = new HashSet<>();
        for (AssociationEntity association : associations) {
            if (priority == null) {
                priority = resolveTagPriority(association);
            }
            if (association.getFileTagId() != null) {
                FileTagDto fileTag = fileTagService.getFromCache(association.getFileTagId());
                ids.add(AssociationIdUtils.createTagIdentifier(fileTag));
                ids2.add(AssociationIdUtils.createTag2Identifier(fileTag));
                Long scopeId = association.getAssociationScopeId();
                String identifier = AssociationIdUtils.createPriorityScopedTagIdentifier(fileTag, scopeId, priority);
                if (fileTag.getType().isSingleValueTag()) {
                    PriorityScopedValueResolver priorityScopedValueResolver = new PriorityScopedValueResolver(identifier);
                    String[] scopedTagValues = priorityScopedResults.toArray(new String[]{});
                    priorityScopedResults = Sets.newHashSet(priorityScopedValueResolver.resolveNewScopedValues(scopedTagValues, true));
                } else {
                    scopedResults.add(identifier);
                }
            } else if (association.getFunctionalRoleId() != null) {
                FunctionalRoleDto fr = functionalRoleService.getFromCache(association.getFunctionalRoleId());
                ids2.add(AssociationIdUtils.createFunctionalRoleIdentfier(fr));
                dataRoles.add(AssociationIdUtils.createFunctionalRoleIdentfier(fr, priority));
            } else if (association.getSimpleBizListItemSid() != null) {
                SimpleBizListItemDto bli = bizListItemService.getFromCache(association.getSimpleBizListItemSid());
                ids2.add(AssociationIdUtils.createSimpleBizListItemIdentfier(bli));
                bizListItems.add(AssociationIdUtils.createSimpleBizListItemIdentfier(bli, priority));
            } else if (association.getDepartmentId() != null || association.getDocTypeId() != null) {
                // ignore department and doc type associations here
            } else {
                logger.warn("Unsupported file association: {}", association);
            }
        }
        ids3.clear();
        ids3.addAll(priorityScopedResults);
        ids3.addAll(scopedResults);
    }

    // add tags from doc type associations to scopedTags field
    private void addDocTypeTagIds(Collection<AssociationEntity> docTypeAssociations, TagPriority priority, Set<String> ids) {
        Set<String> priorityScopedResults = new HashSet<>(ids);
        Set<String> scopedResults = new HashSet<>();
        for (AssociationEntity association : docTypeAssociations) {
            DocType docType = docTypeService.getDocTypeByIdWithTagSettings(association.getDocTypeId());
            if (docType == null) {
                logger.error("cannot find doc type {} in db for association {}", association.getDocTypeId(), association);
            } else {
                Set<FileTagSetting> fileTagSettings = docType.getFileTagSettings();
                if (fileTagSettings != null) {
                    for (FileTagSetting fileTagSetting : fileTagSettings) {
                        if (fileTagSetting.isDeleted()) {
                            continue; // Not relevant
                        }
                        FileTag fileTag = fileTagSetting.getFileTag();
                        if (fileTag != null) {
                            String identifier = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fileTag,
                                    association.getAssociationScopeId(),
                                    priority != null ? priority : resolveTagPriority(association));
                            if (fileTag.getType().isSingleValueTag()) {
                                PriorityScopedValueResolver priorityScopedValueResolver = new PriorityScopedValueResolver(identifier);
                                String[] scopedTagValues = priorityScopedResults.toArray(new String[]{});
                                priorityScopedResults = Sets.newHashSet(priorityScopedValueResolver.resolveNewScopedValues(scopedTagValues, true));
                            } else {
                                scopedResults.add(identifier);
                            }
                        }
                    }
                }
            }
        }
        ids.clear();
        ids.addAll(priorityScopedResults);
        ids.addAll(scopedResults);
    }

    public void addTagDataToDocument(SolrInputDocument doc, long fileId, List<String> associationsList,
                                     String userGroupId, String analysisGroupId, boolean atomicUpdate, SolrFolderEntity folder) {
        addTagDataToDocument(doc, fileId, associationsList,
                userGroupId, analysisGroupId, atomicUpdate, folder, SolrFieldOp.SET);
    }

    public void addTagDataToDocument(SolrInputDocument doc, long fileId, List<String> associationsList,
                                     String userGroupId, String analysisGroupId, boolean atomicUpdate, SolrFolderEntity folder, SolrFieldOp operation) {
        try {
            FileGroup userGroup = userGroupId == null ? null : fileGroupRepository.getById(userGroupId).orElse(null);
            FileGroup analysisGroup = analysisGroupId == null ? null : fileGroupRepository.getById(analysisGroupId).orElse(null);
            TagData tagData = new TagData(userGroup, analysisGroup, folder);
            addTagDataToDocument(doc, fileId, associationsList, userGroupId, analysisGroupId, atomicUpdate, tagData, operation);
        } catch (Exception e) {
            logger.error("failed adding tag data to file {}", fileId, e);
        }
    }

    public FileGroup getFromStoreOrLoad(String fileGroupId, IngestBatchStore ingestBatchStore) {
        FileGroup group = null;
        if (fileGroupId != null) {
            group = ingestBatchStore.getFileGroup(fileGroupId);
            if (group == null) {
                group = fileGroupRepository.getById(fileGroupId).orElse(null);
            }
            if (group != null) {
                ingestBatchStore.addFileGroup(fileGroupId, group);
            }
        }
        return group;
    }

    public void addTagDataToDocument(SolrInputDocument doc, SolrFileEntity entity, boolean atomicUpdate, IngestBatchStore ingestBatchStore) {
        try {
            FileGroup userGroup = getFromStoreOrLoad(entity.getUserGroupId(), ingestBatchStore);
            FileGroup analysisGroup = getFromStoreOrLoad(entity.getAnalysisGroupId(), ingestBatchStore);

            SolrFolderEntity folder = ingestBatchStore.getDocFolderEntityById(entity.getFolderId());
            TagData tagData = new TagData(userGroup, analysisGroup, folder);
            addTagDataToDocument(doc, entity, atomicUpdate, tagData, SolrFieldOp.SET);
        } catch (Exception e) {
            logger.error("failed adding tag data to file {}", entity == null ? null : entity.getId(), e);
        }
    }

    ///////////////////// set data on file solr
    private void addTagDataToDocument(SolrInputDocument doc, SolrFileEntity file, boolean atomicUpdate, TagData tagData, SolrFieldOp operation) {
        addTagDataToDocument(doc, file.getFileId(), file.getAssociations(),
                file.getUserGroupId(), file.getAnalysisGroupId(),
                atomicUpdate, tagData, operation);
    }

    private void addTagDataToDocument(SolrInputDocument doc, long fileId, List<String> associationsList,
                                      String userGroupId, String analysisGroupId, boolean atomicUpdate,
                                      TagData tagData, SolrFieldOp operation) {
        KpiRecording kpiRecording;
        List<AssociationEntity> fileAssociations = AssociationsService.convertFromAssociationEntity(fileId,
                associationsList, AssociationType.FILE);
        List<AssociationEntity> fileDocTypeAssociations = getDocTypeAssociation(fileAssociations);
        List<AssociationEntity> folderTagAssociations = convertFromAssociationEntity(tagData.getFolder().getId(),
                tagData.getFolder().getAssociations(), AssociationType.FOLDER);
        List<AssociationEntity> folderDocTypeAssociations = getDocTypeAssociation(folderTagAssociations);
        Set<String> tags = Sets.newHashSet();
        Set<String> tags2 = Sets.newHashSet();
        Set<String> scopedTags = Sets.newHashSet();
        Set<String> docTypes = Sets.newHashSet();
        Set<String> dataRoles = Sets.newHashSet();
        Set<String> bizListItems = Sets.newHashSet();
        Set<String> activeAssociations = Sets.newHashSet();
        String department = null;

        Collection<Object> currentTags = doc.getFieldValues(TAGS.getSolrName());
        if (currentTags != null) {
            currentTags.forEach(t -> tags.add((String) t));
        }
        Collection<Object> currentTags2 = doc.getFieldValues(TAGS_2.getSolrName());
        if (currentTags2 != null) {
            currentTags2.forEach(t -> tags2.add((String) t));
        }

        Collection<Object> currentTags3 = doc.getFieldValues(SCOPED_TAGS.getSolrName());
        if (currentTags3 != null) {
            currentTags3.forEach(t -> scopedTags.add((String) t));
        }

        Collection<Object> currentDocTypes = doc.getFieldValues(DOC_TYPES.getSolrName());
        if (currentDocTypes != null) {
            currentDocTypes.forEach(t -> docTypes.add((String) t));
        }

        Collection<Object> currentDataRoles = doc.getFieldValues(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName());
        if (currentDataRoles != null) {
            currentDataRoles.forEach(t -> dataRoles.add((String) t));
        }

        Collection<Object> currentBizListItems = doc.getFieldValues(BIZLIST_ITEM_ASSOCIATION.getSolrName());
        if (currentBizListItems != null) {
            currentBizListItems.forEach(t -> bizListItems.add((String) t));
        }

        Collection<Object> currentActiveAssociations = doc.getFieldValues(ACTIVE_ASSOCIATIONS.getSolrName());
        if (currentActiveAssociations != null) {
            currentActiveAssociations.forEach(t -> activeAssociations.add((String) t));
        }

        if (fileAssociations != null && !fileAssociations.isEmpty()) {
            kpiRecording = performanceKpiRecorder.startRecording("addTagDataToDocument", "fileTagAssociations", 120);
            addAssociationIds(TagPriority.FILE, fileAssociations, tags, tags2, scopedTags, dataRoles, bizListItems);
            addDocTypeAssociationToIds(fileDocTypeAssociations, docTypes);
            addDocTypeAssociationsData(fileDocTypeAssociations, scopedTags, TagPriority.FILE);
            addDocTypeTagIds(fileDocTypeAssociations, TagPriority.FILE, scopedTags);
            kpiRecording.end();
        }

        if (folderTagAssociations != null) {
            kpiRecording = performanceKpiRecorder.startRecording("addTagDataToDocument", "folderTagAssociations", 120);
            addAssociationIds(null, folderTagAssociations, tags, tags2, scopedTags, dataRoles, bizListItems);
            department = getDepartment(folderTagAssociations); // department can only come from folder level
            addDocTypeAssociationToIds(folderDocTypeAssociations, docTypes);
            addDocTypeAssociationsData(folderDocTypeAssociations, scopedTags, null);
            addDocTypeTagIds(folderDocTypeAssociations, null, scopedTags);
            kpiRecording.end();
        }

        if (userGroupId != null) {
            kpiRecording = performanceKpiRecorder.startRecording("addTagDataToDocument", "userGroupAssociations", 136);
            FileGroup userGroup = tagData.getUserGroup();
            List<AssociationEntity> groupTagAssociations = convertFromAssociationEntity(userGroupId,
                    userGroup.getAssociations(), AssociationType.GROUP);
            addAssociationIds(TagPriority.FILE_GROUP, groupTagAssociations, tags, tags2, scopedTags, dataRoles, bizListItems);
            List<AssociationEntity> groupDocTypeAssociations = getDocTypeAssociation(groupTagAssociations);
            addDocTypeAssociationToIds(groupDocTypeAssociations, docTypes);
            addDocTypeTagIds(groupDocTypeAssociations, TagPriority.FILE_GROUP, scopedTags);
            addDocTypeAssociationsData(groupDocTypeAssociations, scopedTags, TagPriority.FILE_GROUP);
            kpiRecording.end();
        }

        if (analysisGroupId != null && !analysisGroupId.equals(userGroupId)) {
            kpiRecording = performanceKpiRecorder.startRecording("addTagDataToDocument", "analysisGroupAssociations", 147);
            FileGroup analysisGroup = tagData.getAnalysisGroup();
            List<AssociationEntity> groupTagAssociations = convertFromAssociationEntity(analysisGroupId,
                    analysisGroup.getAssociations(), AssociationType.GROUP);
            addAssociationIds(TagPriority.FILE_GROUP, groupTagAssociations, tags, tags2, scopedTags, dataRoles, bizListItems);
            List<AssociationEntity> groupDocTypeAssociations = getDocTypeAssociation(groupTagAssociations);
            addDocTypeAssociationToIds(groupDocTypeAssociations, docTypes);
            addDocTypeTagIds(groupDocTypeAssociations, TagPriority.FILE_GROUP, scopedTags);
            addDocTypeAssociationsData(groupDocTypeAssociations, scopedTags, TagPriority.FILE_GROUP);
            kpiRecording.end();
        }

        if (!tags.isEmpty()) {
            SolrFileCatRepository.setField(doc, TAGS, SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, tags, operation));
        }

        if (populateTags2 && !tags2.isEmpty()) {
            SolrFileCatRepository.setField(doc, TAGS_2, SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, tags2, operation));
        }

        if (!scopedTags.isEmpty()) {
            doc.setField(SCOPED_TAGS.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, scopedTags, operation));
        }

        if (department != null) {
            doc.setField(DEPARTMENT_ID.getSolrName(), department);
        }

        if (!dataRoles.isEmpty()) {
            doc.setField(FUNCTIONAL_ROLE_ASSOCIATION.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, dataRoles, operation));
        }

        if (!bizListItems.isEmpty()) {
            doc.setField(BIZLIST_ITEM_ASSOCIATION.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, bizListItems, operation));
        }

        if (!docTypes.isEmpty()) {
            doc.setField(DOC_TYPES.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, docTypes, operation));
        }

        AssociationUtils.setActiveAssociationValues(scopedTags, activeAssociations);
        AssociationUtils.setActiveAssociationValues(Lists.newArrayList(department), activeAssociations);

        if (!activeAssociations.isEmpty()) {
            doc.setField(ACTIVE_ASSOCIATIONS.getSolrName(), SolrFileCatRepository.atomicConditionalSetValue(
                    atomicUpdate, activeAssociations, operation));
        }
    }

    public static Map<CatFileFieldType, Object> getDocAssociationData(SolrInputDocument doc) {
        Map<CatFileFieldType, Object> result = new HashMap<>();
        List<CatFileFieldType> associationFields = Lists.newArrayList(SCOPED_TAGS, DEPARTMENT_ID,
                FUNCTIONAL_ROLE_ASSOCIATION, BIZLIST_ITEM_ASSOCIATION, TAGS, TAGS_2);

        for (CatFileFieldType field : associationFields) {
            if (doc.containsKey(field.getSolrName())) {
                result.put(field, doc.getFieldValue(field.getSolrName()));
            }
        }
        return result;
    }

    public void addDocTypeDataToDocument(SolrInputDocument doc, SolrFileEntity file, boolean atomicUpdate) {
        try {
            DocFolder folder = solrCatFolderRepository.getById(file.getFolderId());
            List<AssociationEntity> folderDocTypeAssociations =
                    getDocTypeAssociation(convertFromAssociationEntity(file.getFolderId(),
                            folder.getAssociations(), AssociationType.FOLDER));
            List<AssociationEntity> fileDocTypeAssociations =
                    getDocTypeAssociation(convertFromAssociationEntity(file.getFileId(),
                            file.getAssociations(), AssociationType.FILE));

            Set<String> ids = new HashSet<>();
            if (fileDocTypeAssociations != null && !fileDocTypeAssociations.isEmpty()) {
                addDocTypeAssociationToIds(fileDocTypeAssociations, ids);
            }
            if (folderDocTypeAssociations != null) {
                addDocTypeAssociationToIds(folderDocTypeAssociations, ids);
            }
            if (file.getUserGroupId() != null) {
                FileGroup userGroup = fileGroupRepository.getById(file.getUserGroupId()).orElse(null);
                List<AssociationEntity> groupDocTypeAssociations =
                        getDocTypeAssociation(convertFromAssociationEntity(file.getUserGroupId(),
                                userGroup.getAssociations(), AssociationType.GROUP));
                if (groupDocTypeAssociations != null) {
                    addDocTypeAssociationToIds(groupDocTypeAssociations, ids);
                }
            }
            SolrFileCatRepository.setField(doc, DOC_TYPES, SolrFileCatRepository.atomicConditionalSetValue(atomicUpdate, ids));

        } catch (Exception e) {
            logger.error("failed adding doc type data to file {}", file == null ? null : file.getFileId(), e);
        }
    }

    //////////////////// solr actions

    @AutoAuthenticate
    public void updateAssociationToCategoryFilesByGroupId(String groupId, AssociableEntityDto associableEntity, Long scopeId, boolean addDocType, SolrCommitType commitType) {
        Criteria filterQuery = Criteria.create(USER_GROUP_ID, SolrOperator.EQ, groupId);
        updateFileTags(filterQuery, associableEntity, scopeId, TagPriority.FILE_GROUP, addDocType, commitType);
        solrFileCatRepository.softCommitNoWaitFlush();
    }

    private AssociableEntityDto getForAssociableEntity(AssociableEntity entity) {
        if (entity instanceof FileTag) {
            return fileTagService.getFromCache(((FileTag)entity).getId());
        } else if (entity instanceof DocType) {
            return docTypeService.getFromCache(((DocType)entity).getId());
        } else if (entity instanceof FunctionalRole) {
            return functionalRoleService.getFromCache(((FunctionalRole)entity).getId());
        } else if (entity instanceof Department) {
            return departmentService.getByIdCached(((Department)entity).getId());
        } else if (entity instanceof SimpleBizListItem) {
            return bizListItemService.getFromCache(((SimpleBizListItem)entity).getSid());
        }
        return null;
    }

    public void updateDocTypeToCategoryFiles(String entityId, CatFileFieldType entityField, DocTypeDto docType, boolean commit, boolean addTag, BasicScope basicScope, TagPriority tagPriority) {
        updateDocTypeToCategoryFiles(entityId, entityField, docType, addTag, basicScope, tagPriority, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    private void updateDocTypeToCategoryFiles(String entityId, CatFileFieldType entityField, DocTypeDto docType,
                                             boolean addTag, BasicScope basicScope, TagPriority tagPriority, SolrCommitType commitType) {
        try {
            String solrKey = docType.getDocTypeIdentifier();
            final SolrQuery query = solrFileCatRepository.createUpdateTagsQuery(Criteria.create(entityField, SolrOperator.EQ, entityId),
                    docType.getName(), addTag, solrKey, DOC_TYPES, true, commitType);
            query.add(SolrRequestParams.FIELD, DOC_TYPES.getSolrName());
            QueryResponse queryResponse = runAssociationUpdateQuery(query);
            logger.trace("Update docType to category files by " + entityField + " response: {}", queryResponse == null ? null : queryResponse.toString());

            updateScopedDocTypesByQuery(entityId, entityField, docType, addTag, basicScope, tagPriority, commitType);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update category files in Solr by folderId " + entityId, e);
        }
    }

    private void updateScopedDocTypesByQuery(String entityId, CatFileFieldType entityField, DocTypeDto docType, boolean addTag, BasicScope basicScope, TagPriority tagPriority, SolrCommitType commitType) throws Exception {
        Long scopeId = basicScope == null ? null : basicScope.getId();
        String solrKey1 = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(docType.getDocTypeIdentifier(), scopeId, tagPriority);
        SolrQuery query1 = solrFileCatRepository.createUpdateTagsQuery(Criteria.create(entityField, SolrOperator.EQ, entityId), docType.getName(), addTag, solrKey1, SCOPED_TAGS, false, commitType);
        query1.add(SolrRequestParams.FIELD, SCOPED_TAGS.getSolrName());
        query1.add(SolrRequestParams.TAG_SINGLE_VALUE, String.valueOf(docType.isSingleValue()));
        query1.add(SolrRequestParams.TAG_MULTI_VALUE, String.valueOf(!docType.isSingleValue()));
        logger.info("solrKey: {}, singleValue: {}", solrKey1, docType.isSingleValue());
        QueryResponse queryResponse = runAssociationUpdateQuery(query1);
        logger.debug("Update {} to category files by filterQuery:{}, response: {}", SCOPED_TAGS, query1, queryResponse == null ? null : queryResponse.toString());
    }

    @AutoAuthenticate
    public void updateDocTypeToCategoryFiles(String entityId, CatFileFieldType entityField, Long docTypeId, boolean addTag, BasicScope basicScope,
                                             TagPriority tagPriority, SolrCommitType commitType)  {
        DocTypeDto docTypeDto = docTypeService.getFromCache(docTypeId);
        updateDocTypeToCategoryFiles(entityId, entityField, docTypeDto, addTag, basicScope, tagPriority, commitType);
        solrFileCatRepository.softCommitNoWaitFlush();
        if (docTypeDto.getFileTagTypeSettings() != null && !docTypeDto.getFileTagTypeSettings().isEmpty()) {
            Long scopeId = basicScope == null ? null : basicScope.getId();
            docTypeDto.getFileTagTypeSettings().forEach(fts ->
                    fts.getFileTags().forEach(fileTag -> {
                try {
                    String solrKey = AssociationIdUtils.createViaDocTypePriorityScopedTagIdentifier(fileTag, scopeId, tagPriority);
                    updateScopedTagByQuery(
                            Criteria.create(entityField, SolrOperator.EQ, entityId),
                            addTag,
                            fileTag,
                            solrKey, commitType);
                    solrFileCatRepository.softCommitNoWaitFlush();
                } catch (Exception e) {
                    logger.error("problem applying doc type {} tag {} to files of entity {}", docTypeId, fileTag.getId(), entityField + ":" + entityId, e);
                }
            }));
        }
    }

    private void setMetaDataCngDate(Long fileId, Long contentId) {
        setMetaDataCngDate(FileCatCompositeId.of(contentId, fileId).toString());
    }

    private void setMetaDataCngDate(String id) {
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create()
                .setId(ID, id)
                .addModifier(CatFileFieldType.LAST_ASSOC_CNG_DATE, SolrOperator.SET, System.currentTimeMillis())
                .addModifier(CatFileFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                ;
        solrFileCatRepository.atomicUpdate(builder.build());
    }

    ////////// biz list

    @Transactional
    public void removeBizListItemWithFile(SolrFileEntity fileEntity, SimpleBizListItemDto simpleBizListItem, SolrCommitType commitType) {

        List<AssociationEntity> associations = convertFromAssociationEntity(fileEntity.getFileId(), fileEntity.getAssociations(), AssociationType.FILE);
        AssociationEntity association = getFileBizListItemAssociation(simpleBizListItem, fileEntity, associations);
        if (association == null) {
            logger.debug("BizListItem {} is not associated with file {} Can't remove bizListITem ", simpleBizListItem.getSid(), fileEntity.getFileId());
            return;
        }
        removeAssociationFromFile(fileEntity, simpleBizListItem, association, commitType);
        setMetaDataCngDate(fileEntity.getId());
    }

    private AssociationEntity getFileBizListItemAssociation(SimpleBizListItemDto simpleBizListItem, SolrFileEntity fileEntity, Collection<AssociationEntity> fileTagAssociations) {
        if (fileTagAssociations != null) {
            for (AssociationEntity fileTagAssociation : fileTagAssociations) {
                if (fileTagAssociation.getSimpleBizListItemSid() != null
                        && fileTagAssociation.getSimpleBizListItemSid().equals(simpleBizListItem.getSid())) {
                    logger.debug("BizListItem {} associated to claFile {}", simpleBizListItem.getSid(), fileEntity.getFileId());
                    return fileTagAssociation;
                }
            }
        }
        return null;
    }

    ////////// func role

    @Transactional
    public void removeFunctionalRoleFromFile(SolrFileEntity fileEntity, FunctionalRoleDto functionalRole, SolrCommitType commitType) {
        List<AssociationEntity> associations = convertFromAssociationEntity(fileEntity.getFileId(), fileEntity.getAssociations(), AssociationType.FILE);
        AssociationEntity association = getFileFunctionalRoleAssociation(functionalRole, fileEntity, associations);
        if (association == null) {
            logger.debug("FunctionalRole {} is not associated with file {} Can't remove functionalRole ", functionalRole.getId(), fileEntity.getFileId());
            return;
        }
        removeAssociationFromFile(fileEntity, functionalRole, association, commitType);
        setMetaDataCngDate(fileEntity.getId());
    }

    private AssociationEntity getFileFunctionalRoleAssociation(FunctionalRoleDto functionalRole, SolrFileEntity fileEntity, Collection<AssociationEntity> fileTagAssociations) {
        if (fileTagAssociations != null) {
            for (AssociationEntity fileTagAssociation : fileTagAssociations) {
                if (fileTagAssociation.getFunctionalRoleId() != null && fileTagAssociation.getFunctionalRoleId().equals(functionalRole.getId())) {
                    logger.debug("functionalRole {} associated to claFile {}", functionalRole.getId(), fileEntity.getFileId());
                    return fileTagAssociation;
                }
            }
        }
        return null;
    }

    @Transactional
    public ClaFileDto associateFunctionalRoleItemWithFile(ClaFile claFile, FunctionalRoleDto functionalRole, SolrCommitType commitType) {
        return addAssociationToFile(functionalRole, claFile, null, commitType);
    }

    public List<AssociableEntityDto> getEntitiesForAssociations(Collection<AssociationEntity> associationEntities) {
        List<AssociableEntityDto> result = new ArrayList<>();
        if (associationEntities != null) {
            associationEntities.forEach(associationEntity -> {
                AssociableEntityDto dto = null;
                if (associationEntity.getSimpleBizListItemSid() != null) {
                    dto = bizListItemService.getFromCache(associationEntity.getSimpleBizListItemSid());
                } else if (associationEntity.getFunctionalRoleId() != null) {
                    dto = functionalRoleService.getFromCache(associationEntity.getFunctionalRoleId());
                } else if (associationEntity.getFileTagId() != null) {
                    dto = fileTagService.getFromCache(associationEntity.getFileTagId());
                } else if (associationEntity.getDocTypeId() != null) {
                    dto = docTypeService.getFromCache(associationEntity.getDocTypeId());
                } else if (associationEntity.getDepartmentId() != null) {
                    dto = departmentService.getByIdCached(associationEntity.getDepartmentId());
                }

                if (dto != null) {
                    result.add(dto);
                }
            });
        }
        return result;
    }

    public AssociableEntity getEntityForAssociation(AssociationEntity association) {
        if (association.getSimpleBizListItemSid() != null) {
            return bizListItemService.getSimpleBizListItem(association.getSimpleBizListItemSid());
        } else if (association.getFunctionalRoleId() != null) {
            return functionalRoleService.getDataRoleById(association.getFunctionalRoleId());
        } else if (association.getFileTagId() != null) {
            return fileTagService.getFileTag(association.getFileTagId());
        } else if (association.getDocTypeId() != null) {
            return docTypeService.getDocTypeById(association.getDocTypeId());
        } else if (association.getDepartmentId() != null) {
            return departmentService.getDepartmentById(association.getDepartmentId());
        }
        return null;
    }

    public static List<AssociationEntity> getDocTypeAssociation(Collection<AssociationEntity> associationEntities) {
        List<AssociationEntity> result = new ArrayList<>();
        if (associationEntities == null) {
            return result;
        }
        associationEntities.forEach(associationEntity -> {
            if (associationEntity.getDocTypeId() != null) {
                result.add(associationEntity);
            }
        });
        return result;
    }

    public static String convertFromAssociationEntity(AssociationEntity entity) {
        if (entity == null) {
            return null;
        }
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(entity);
        } catch (Exception e) {
            logger.warn("problem translating association {} to json", entity);
            return null;
        }
    }

    public static List<AssociationEntity> convertFromAssociationEntity(Object entityId, List<String> jsons, AssociationType type) {
        List<AssociationEntity> result = new ArrayList<>();
        if (jsons == null) {
            return result;
        }
        jsons.forEach(json -> {
            AssociationEntity entity = convertFromAssociationEntity(entityId, json, type);
            if (entity != null) {
                result.add(entity);
            }
        });
        return result;
    }

    public AssociationRelatedData getAssociationRelatedData(FileGroup group) {
        List<AssociationEntity> groupAssociation = AssociationsService.convertFromAssociationEntity(
                group.getId(), group.getAssociations(), AssociationType.GROUP);
        return new AssociationRelatedData(groupAssociation, getEntitiesForAssociations(groupAssociation),
                getScopesForAssociations(groupAssociation));
    }

    public AssociationRelatedData getAssociationRelatedData(DocFolder folder) {
        List<AssociationEntity> folderAssociation = AssociationsService.convertFromAssociationEntity(
                folder.getId(), folder.getAssociations(), AssociationType.FOLDER);
        return new AssociationRelatedData(folderAssociation, getEntitiesForAssociations(folderAssociation),
                getScopesForAssociations(folderAssociation));
    }

    public AssociationRelatedData getAssociationRelatedData(ClaFile claFile) {
        List<AssociationEntity> associationEntities = AssociationsService.convertFromAssociationEntity(
                claFile.getId(), claFile.getAssociations(), AssociationType.FILE);
        return new AssociationRelatedData(associationEntities, getEntitiesForAssociations(associationEntities),
                getScopesForAssociations(associationEntities));
    }

    public AssociationRelatedData getAssociationRelatedData(SolrFileEntity fileEntity) {
        List<AssociationEntity> associationEntities = AssociationsService.convertFromAssociationEntity(
                fileEntity.getFileId(), fileEntity.getAssociations(), AssociationType.FILE);
        return new AssociationRelatedData(associationEntities, getEntitiesForAssociations(associationEntities),
                getScopesForAssociations(associationEntities));
    }

    public AssociationRelatedData getAssociationRelatedData(Collection<DocFolder> folders) {
        Map<Long, List<AssociationEntity>> byFolderAssociations =  folders.stream().collect(Collectors.toMap(DocFolder::getId,
                f -> AssociationsService.convertFromAssociationEntity(f.getId(), f.getAssociations(), AssociationType.FOLDER)));

        List<AssociationEntity> allFolderAssociations = folders.stream().map(f -> byFolderAssociations.get(f.getId()))
                .filter(Objects::nonNull).flatMap(List::stream).collect(Collectors.toList());
        List<AssociableEntityDto> entities = getEntitiesForAssociations(allFolderAssociations);
        Map<Long, BasicScope> scopes = getScopesForAssociations(allFolderAssociations);
        return new AssociationRelatedData(allFolderAssociations, entities,
                scopes, byFolderAssociations);
    }

    public AssociationRelatedData getAssociationRelatedDataGroups(Collection<FileGroup> groups) {
        Map<String, List<AssociationEntity>> byGroupAssociations =  groups.stream().collect(Collectors.toMap(FileGroup::getId,
                f -> AssociationsService.convertFromAssociationEntity(f.getId(), f.getAssociations(), AssociationType.GROUP)));

        List<AssociationEntity> allGroupAssociations = groups.stream().map(f -> byGroupAssociations.get(f.getId()))
                .filter(Objects::nonNull).flatMap(List::stream).collect(Collectors.toList());
        List<AssociableEntityDto> entities = getEntitiesForAssociations(allGroupAssociations);
        Map<Long, BasicScope> scopes = getScopesForAssociations(allGroupAssociations);
        AssociationRelatedData data = new AssociationRelatedData(allGroupAssociations, entities, scopes);
        data.setPerGroupAssociations(byGroupAssociations);
        return data;
    }

    public static AssociationEntity convertFromAssociationEntity(Object entityId, String json, AssociationType type) {
        AssociationEntity entity;
        if (Strings.isNullOrEmpty(json)) {
            return null;
        }
        try {
            entity = ModelDtoConversionUtils.getMapper().readValue(json, AssociationEntity.class);
        } catch (Exception e) {
            logger.warn("problem translating association json {} to object for {} {}", json, type, entityId);
            return null;
        }

        if (entity == null || !entity.isValid()) {
            logger.warn("problem with invalid association json {} for {} {}", json, type, entityId);
            return null;
        }
        return entity;
    }

    public Map<Long, BasicScope> getScopesForAssociations(List<AssociationEntity> associationEntities) {
        Map<Long, BasicScope> scopes = new HashMap<>();
        if (associationEntities == null) {
            return scopes;
        }
        associationEntities.forEach(associationEntity -> {
            if (associationEntity.getAssociationScopeId() != null) {
                BasicScope scope = scopeService.getFromCache(associationEntity.getAssociationScopeId());
                if (scope != null) {
                    scopes.put(scope.getId(), scope);
                }
            }
        });
        return scopes;
    }

    public void associateContentRelatedItems(SolrInputDocument doc, FileGroup analysisGroup, FileGroup userGroup) {
        Set<String> associableEntities = new HashSet<>();
        Set<String> fileTags = new HashSet<>();
        Set<String> docTypes = new HashSet<>();

        if (analysisGroup != null) {
            AssociationRelatedData groupData = getAssociationRelatedData(analysisGroup);
            addAssociations(associableEntities, fileTags, groupData);
            addDocTypes(groupData, docTypes);
        }
        if (userGroup != null) {
            AssociationRelatedData groupData = getAssociationRelatedData(userGroup);
            addAssociations(associableEntities, fileTags, groupData);
            addDocTypes(groupData, docTypes);
        }

        doc.setField(TAGS.getSolrName(), fileTags);
        doc.setField(TAGS_2.getSolrName(), associableEntities);
        doc.setField(DOC_TYPES.getSolrName(), docTypes);
    }

    private void addAssociations(Set<String> associableEntities, Set<String> fileTags, AssociationRelatedData groupAssociations) {
        if (groupAssociations == null) return;
        groupAssociations.getAssociations().stream()
                .filter(assoc -> assoc.getFileTagId() != null)
                .map(assoc -> (FileTagDto)AssociationUtils.getEntityFromList(FileTagDto.class, assoc.getFileTagId(), groupAssociations.getEntities()))
                .forEach(fileTagDto -> {
                    associableEntities.add(AssociationIdUtils.createTag2Identifier(fileTagDto));
                    fileTags.add(AssociationIdUtils.createTagIdentifier(fileTagDto));
                });
    }

    private void addDocTypes(AssociationRelatedData groupAssociations, Set<String> docTypes) {
        getDocTypeAssociation(groupAssociations.getAssociations()).stream()
                .map(assoc -> (DocTypeDto)AssociationUtils.getEntityFromList(
                        DocTypeDto.class, assoc.getDocTypeId() ,groupAssociations.getEntities()))
                .map(DocTypeDto::getDocTypeIdentifier)
                .forEach(docTypes::add);
    }

    public void applyDocTypesTagsToSolr() {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.APPLY_DOC_TYPES_TAGS_TO_SOLR);
        data.setOpData(new HashMap<>());
        data.getOpData().put(ApplyDocTypesTagsToSolrOperation.MAX_DOC_TYPES_PARAM, String.valueOf(associationApplySize));
        scheduledOperationService.createNewOperation(data);
    }

    public void associationOnFilesFilter(AssociationEntity entity, SolrFieldOp operation, String filter, String userName, AssociableEntityType associationType) {
        String association = AssociationsService.convertFromAssociationEntity(entity);
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.ASSOCIATION_FOR_FILES_BY_FILTER);
        data.setOpData(new HashMap<>());
        data.getOpData().put(AssociationForFilesByFilterOperation.ACTION_PARAM, operation.name());
        data.getOpData().put(AssociationForFilesByFilterOperation.FILTER_PARAM, filter);
        data.getOpData().put(AssociationForFilesByFilterOperation.ASSOCIATION_PARAM, association);
        if (userName != null) {
            data.getOpData().put(AssociationForFilesByFilterOperation.USER_PARAM, userName);
        }
        data.setUserOperation(true);
        data.setDescription(operation.equals(SolrFieldOp.ADD) ?
                messageHandler.getMessage("operation-description.assign-association-filter",
                        Lists.newArrayList(associationType == null ? "" : messageHandler.getMessage(associationType.getMsgCodeKey()))) :
                messageHandler.getMessage("operation-description.remove-association-filter",
                        Lists.newArrayList(associationType == null ? "" : messageHandler.getMessage(associationType.getMsgCodeKey()))));
        data.setRetryLeft(retryForApplyFilterAssociationToFiles);
        scheduledOperationService.createNewOperation(data);
    }

    private void updateTagIdentfierToCategoryFile(Long claFileId, Long contentId, AssociableEntityDto associableEntity, Long scopeId, boolean addTag, SolrCommitType commitType) {
        String compositeId = FileCatCompositeId.of(contentId, claFileId).toString();
        updateFileTags(Criteria.create(ID, SolrOperator.EQ, compositeId), associableEntity, scopeId, TagPriority.FILE, addTag, commitType);
    }

    @AutoAuthenticate
    public void updateAssociationToCategoryFilesByFolderId(DocFolder docFolder, AssociableEntityDto associableEntity, Long scopeId, boolean addTag, SolrCommitType commitType) {
        List<String> decodeFolderParentsInfo = docFolder.getParentsInfo();
        String folderParentsInfo = decodeFolderParentsInfo.get(decodeFolderParentsInfo.size() - 1);
        Criteria filterQuery = Criteria.create(FOLDER_IDS, SolrOperator.EQ, folderParentsInfo);
        updateFileTags(filterQuery, associableEntity, scopeId, TagPriority.folder(docFolder.getDepthFromRoot()), addTag, commitType);
    }

    private void removeTagIdentifierFromCategoryFile(long fileId, Long contentId, AssociableEntityDto associableEntity, Long scopeId, SolrCommitType commitType) {
        String compositeId = FileCatCompositeId.of(contentId, fileId).toString();
        updateFileTags(Criteria.create(ID, SolrOperator.EQ, compositeId), associableEntity, scopeId, TagPriority.FILE, false, commitType);
    }

    private void updateFileTags(Criteria filterQuery, AssociableEntityDto associableEntity, Long scopeId, TagPriority priority, boolean addTag, SolrCommitType commitType) {
        try {
            SolrQuery query;
            QueryResponse queryResponse;

            if (associableEntity instanceof FileTagInterface) {
                FileTagInterface fileTag = (FileTagInterface) associableEntity;
                String solrKey;

                solrKey = AssociationIdUtils.createTagIdentifier(fileTag);
                query = solrFileCatRepository.createUpdateTagsQuery(filterQuery, associableEntity.toString(), addTag, solrKey, TAGS, true, commitType);
                queryResponse = runAssociationUpdateQuery(query);
                logger.debug("Update {} to category files by filterQuery:{}, response: {}", TAGS, filterQuery, queryResponse == null ? null : queryResponse.toString());
                solrKey = AssociationIdUtils.createPriorityScopedTagIdentifier(fileTag, scopeId, priority);
                updateScopedTagByQuery(filterQuery, addTag, fileTag, solrKey, commitType);
            }

            if (associableEntity instanceof DepartmentDto) {
                DepartmentDto department = (DepartmentDto)associableEntity;
                String solrKey = AssociationIdUtils.createDepartmentIdentifier(department.getId(), priority);
                updateAssociationByQuery(filterQuery, addTag, department.toString(), solrKey, CatFileFieldType.DEPARTMENT_ID, true, commitType);
            }

            if (associableEntity instanceof FunctionalRoleDto) {
                FunctionalRoleDto functionalRole = (FunctionalRoleDto)associableEntity;
                String solrKey = AssociationIdUtils.createFunctionalRoleIdentfier(functionalRole, priority);
                updateAssociationByQuery(filterQuery, addTag, functionalRole.toString(), solrKey, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, false, commitType);
            }

            if (associableEntity instanceof SimpleBizListItemDto) {
                SimpleBizListItemDto bizListItem = (SimpleBizListItemDto)associableEntity;
                String solrKey = AssociationIdUtils.createSimpleBizListItemIdentfier(bizListItem, priority);
                updateAssociationByQuery(filterQuery, addTag, bizListItem.toString(), solrKey, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, false, commitType);
            }

            if (populateTags2) {
                String solrKey2 = associableEntity.createIdentifierString();
                SolrQuery query1 = solrFileCatRepository.createUpdateTagsQuery(filterQuery, associableEntity.getName(), addTag, solrKey2, TAGS_2,
                        true, commitType);
                query1.add("field", TAGS_2.getSolrName());
                queryResponse = runAssociationUpdateQuery(query1);
                logger.debug("Update {} to category files by filterQuery:{}, response: {}", TAGS_2, filterQuery, queryResponse == null ? null : queryResponse.toString());
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to update category file in Solr. filterQuery: " + filterQuery, e);
        }
    }

    public void updateAssociationByQuery(Criteria filterQuery, boolean addTag, String objectName, String solrKey, CatFileFieldType field, boolean singleValue, SolrCommitType commitType) {
        SolrQuery query = solrFileCatRepository.createUpdateTagsQuery(filterQuery, objectName, addTag, solrKey, field, false, commitType);
        query.add(SolrRequestParams.FIELD, field.getSolrName());
        query.add(SolrRequestParams.TAG_SINGLE_VALUE, String.valueOf(singleValue));
        query.add(SolrRequestParams.TAG_MULTI_VALUE, String.valueOf(!singleValue));
        QueryResponse queryResponse = runAssociationUpdateQuery(query);
        logger.debug("Update {} to category files by filterQuery:{}, response: {}", field, filterQuery, queryResponse == null ? null : queryResponse.toString());
    }

    public void updateScopedTagByQuery(Criteria filterQuery, boolean addTag, FileTagInterface fileTag, String solrKey, SolrCommitType commitType) {
        SolrQuery query = solrFileCatRepository.createUpdateTagsQuery(filterQuery, fileTag.toString(), addTag, solrKey, SCOPED_TAGS, false, commitType);
        query.add(SolrRequestParams.FIELD, SCOPED_TAGS.getSolrName());
        query.add(SolrRequestParams.TAG_SINGLE_VALUE, String.valueOf(fileTag.isSingleValueTagType()));
        query.add(SolrRequestParams.TAG_MULTI_VALUE, String.valueOf(!fileTag.isSingleValueTagType()));
        QueryResponse queryResponse = runAssociationUpdateQuery(query);
        logger.debug("Update {} to category files by filterQuery:{}, response: {}", SCOPED_TAGS, filterQuery, queryResponse == null ? null : queryResponse.toString());
    }

    private void removeDocTypeFromCategoryFile(long fileId, Long contentId, DocTypeDto docType, Long scopeId, boolean commit) {
        try {
            logger.debug("Remove DocType {} From Solr CatFile by id {}", docType.getName(), fileId);
            SolrInputDocument doc = new SolrInputDocument();
            String compositeId = FileCatCompositeId.of(contentId, fileId).toString();
            doc.addField(ID.getSolrName(), compositeId);

            String solrKey = docType.getDocTypeIdentifier();
            Map<String, Object> fieldModifier = new HashMap<>(1);
            fieldModifier.put(REMOVE.getOpName(), solrKey);
            doc.addField(DOC_TYPES.getSolrName(), fieldModifier);  // add the map as the field value

            SolrFileCatRepository.setFieldOnce(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));

            UpdateResponse updateResponse = solrFileCatRepository.atomicUpdate(doc);
            if (updateResponse == null) {
                throw new RuntimeException("Failed to update category file in Solr.");
            }
            logger.debug("Update docType to category file response: {}", updateResponse.toString());

            // Remove doc type from scoped or priority scoped values
            SolrQuery query;
            Criteria filterQuery = Criteria.create(ID, SolrOperator.EQ, compositeId);
            solrKey = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(docType.getDocTypeIdentifier(),
                    scopeId, TagPriority.FILE);
            query = solrFileCatRepository.createUpdateTagsQuery(filterQuery, docType.toString(), false, solrKey, SCOPED_TAGS, false,
                    commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
            query.add(SolrRequestParams.FIELD, SCOPED_TAGS.getSolrName());
            query.add(SolrRequestParams.TAG_SINGLE_VALUE, String.valueOf(docType.isSingleValue()));
            query.add(SolrRequestParams.TAG_MULTI_VALUE, String.valueOf(!docType.isSingleValue()));
            QueryResponse queryResponse = runAssociationUpdateQuery(query);
            logger.info("solrKey: {}, singleValue: {}", solrKey, docType.isSingleValue());
            logger.debug("Remove doc type to file by filterQuery:{}, response: {}", filterQuery, queryResponse == null ? null : queryResponse.toString());
        } catch (Exception e) {
            logger.error("Failed to update category file in Solr", e);
            throw new RuntimeException("Failed to update category file in Solr. File-ID:" + fileId, e);
        }
    }

    public void assignDocTypeToFile(DocTypeDto docType, ClaFile file, BasicScope basicScope, SolrCommitType commitType) {
        //Add to claFile
        ClaFileDto claFileDto = addDocTypeToFile(file, docType, basicScope, commitType);
        if (claFileDto != null) {
            //Add to solr if needed
            Long scopeId = basicScope == null ? null : basicScope.getId();
            if (!isDocTypeFileInSolr(file.getId(), docType, scopeId)) {
                addDocTypeToCategoryFile(
                        file.getId(), file.getContentMetadataId(), docType, basicScope, true);
            }
        }
    }

    private ClaFileDto addDocTypeToFile(ClaFile claFile, DocTypeDto docType, BasicScope docTypeScope, SolrCommitType commitType) {

        // Case single value doc type, collect all inflicted doc types from parents
        // to avoid re-adding the same doc type
        Set<DocTypeAssociationDto> inflictedAssociations = new HashSet<>();
        if (docType.isSingleValue()) {
            if (claFile.getUserGroupId() != null) {
                FileGroup fg = getFileGroupByIdAssociations(claFile.getUserGroupId());
                AssociationRelatedData groupAssocData = getAssociationRelatedData(fg);
                List<AssociationEntity> docTypAssoc = AssociationsService.getDocTypeAssociation(groupAssocData.getAssociations());
                docTypAssoc.forEach(da -> {
                    DocTypeAssociationDto docTypeAssociation = AssociationUtils.convertToDocTypeAssociation(
                            da, groupAssocData.getEntities(), groupAssocData.getScopes(), FileTagSource.BUSINESS_ID, false);
                    if (docTypeAssociation != null) {
                        inflictedAssociations.add(docTypeAssociation);
                    }
                });
            }
            if (claFile.getAnalysisGroupId() != null) {
                FileGroup fg = getFileGroupByIdAssociations(claFile.getAnalysisGroupId());
                AssociationRelatedData groupAssocData = getAssociationRelatedData(fg);
                List<AssociationEntity> docTypAssoc = AssociationsService.getDocTypeAssociation(groupAssocData.getAssociations());
                docTypAssoc.forEach(da -> {
                    DocTypeAssociationDto docTypeAssociation = AssociationUtils.convertToDocTypeAssociation(
                            da, groupAssocData.getEntities(), groupAssocData.getScopes(), FileTagSource.BUSINESS_ID, false);
                    if (docTypeAssociation != null) {
                        inflictedAssociations.add(docTypeAssociation);
                    }
                });
            }
            Long docFolderId = claFile.getDocFolderId();
            addAllFolderParentsDocTypeAssociations(docFolderId, inflictedAssociations);
        }

        //DocType already exists in inflicted associations then do not proceed
        if (getExplicitDocTypeAssociation(inflictedAssociations, docType, docTypeScope) != null) {
            throw new BadRequestException(messageHandler.getMessage("doctype.file.association-exists"), BadRequestType.ITEM_ALREADY_EXISTS);
        }

        return associateToFile(docType, claFile, docTypeScope, commitType);
    }

    private void addAllFolderParentsDocTypeAssociations(Long docFolderId, Set<DocTypeAssociationDto> allAssociations) {
        while (docFolderId != null) {
            DocFolder folder = solrCatFolderRepository.getById(docFolderId);
            List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(docFolderId, folder.getAssociations(), AssociationType.FOLDER);
            List<AssociationEntity> docTypeAssociations = AssociationsService.getDocTypeAssociation(associations);
            List<AssociableEntityDto> entities = getEntitiesForAssociations(docTypeAssociations);
            Map<Long, BasicScope> scopes = getScopesForAssociations(docTypeAssociations);

            if (docTypeAssociations != null) {
                docTypeAssociations.forEach(da -> {
                    FileTagSource source = da.getImplicit() ? FileTagSource.FOLDER_IMPLICIT : FileTagSource.FOLDER_EXPLICIT;
                    DocTypeAssociationDto docTypeAssociation = AssociationUtils.convertToDocTypeAssociation(
                            da, entities, scopes, source,false);
                    if (docTypeAssociation != null) {
                        allAssociations.add(docTypeAssociation);
                    }
                });
            }
            docFolderId = folder.getParentFolderId();
        }
    }

    private void addDocTypeToCategoryFile(long fileId, Long contentId, DocTypeDto docType, BasicScope basicScope, boolean commit) {
        Long scopeId = basicScope == null ? null : basicScope.getId();
        try {
            // TODO: Maybe on future version remove this (added to the scoped/priority scoped values)
            // But For now preserve the save to doc type field as well
            logger.debug("Attach DocType {} to Solr CatFile by id {}", docType.getName(), fileId);
            String compositeId = FileCatCompositeId.of(contentId, fileId).toString();
            String identifier = docType.getDocTypeIdentifier();
            SolrInputDocument doc = new SolrInputDocument();
            Map<String, Object> fieldModifier = new HashMap<>(1);
            fieldModifier.put("add", identifier);

            doc.addField(ID.getSolrName(), compositeId);
            doc.addField(DOC_TYPES.getSolrName(), fieldModifier);  // add the map as the field value
            SolrFileCatRepository.setFieldOnce(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
            UpdateResponse updateResponse = solrFileCatRepository.atomicUpdate(doc);
            if (updateResponse == null) {
                throw new RuntimeException("Failed to update category file in Solr.");
            }
            logger.debug("Update tag to category file response: {}", updateResponse.toString());

            // Add doc type from scoped or priority scoped values
            SolrQuery query;
            String solrKey = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(docType.getDocTypeIdentifier(), scopeId, TagPriority.FILE);
            Criteria filterQuery = Criteria.create(ID, SolrOperator.EQ, compositeId);

            query = solrFileCatRepository.createUpdateTagsQuery(filterQuery, docType.toString(), true, solrKey, SCOPED_TAGS, false,
                    commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
            query.add(SolrRequestParams.FIELD, SCOPED_TAGS.getSolrName());
            query.add(SolrRequestParams.TAG_SINGLE_VALUE, String.valueOf(docType.isSingleValue()));
            query.add(SolrRequestParams.TAG_MULTI_VALUE, String.valueOf(!docType.isSingleValue()));
            logger.info("solrKey: {}, singleValue: {}", solrKey, docType.isSingleValue());
            QueryResponse queryResponse = runAssociationUpdateQuery(query);
            logger.debug("Update {} with doc type to category files by filterQuery:{}, response: {}", query.get("field"), filterQuery, queryResponse == null ? null : queryResponse.toString());
        } catch (Exception e) {
            logger.error("Failed to update category file in Solr", e);
            throw new RuntimeException("Failed to update category file in Solr. File-ID:" + fileId, e);
        }
    }

    private QueryResponse runAssociationUpdateQuery(SolrQuery query) {
        try {
            solrFileCatRepository.acquireFileDeleteReadLock();
            return solrFileCatRepository.runQuery(query);
        } finally {
            solrFileCatRepository.releaseFileDeleteReadLock();
        }
    }
}
