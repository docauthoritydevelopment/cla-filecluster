package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.filecluster.service.DriveHealthStatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/health")
public class DriveHealthController {
	private static final Logger logger = LoggerFactory.getLogger(DriveHealthController.class);

	@Autowired
	private DriveHealthStatusService driveHealthStatusService;

	/**
	 * TODO: VOID because no response is expected. It should be called by queue listener instead of REST controller.
	 * @param driveDto
	 */
	@RequestMapping(value = "/drive", method = RequestMethod.POST)
	public void saveDriveStatus(@RequestBody DriveDto driveDto){
		driveHealthStatusService.handleDriveHealth(driveDto);
	}
}
