package com.cla.filecluster.service;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.Identifiable;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.scope.ScopedEntity;
import com.cla.common.domain.dto.security.AssociableDto;
import com.cla.common.domain.dto.security.EntityAssociationDetails;
import com.cla.common.tag.priority.TagPriority;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Superclass for all services that need to perform manipulation on scoped tag/doc type associations
 */
public abstract class TagRelatedBaseService {

    private static final Logger logger = LoggerFactory.getLogger(TagRelatedBaseService.class);

    @Autowired
    protected DocFolderService docFolderService;

    @Autowired
    protected ScopeService scopeService;

    protected void refineAssociations(AssociableDto associableDto, BasicScope basicScope) {
        refineAssociations(associableDto, scopeService.resolveScopeIds(basicScope));
    }

    private void refineAssociations(AssociableDto associableDto, Set<Long> relevantScopeIds) {
        try {
            // Remove all out of scope tag & doc type associations
            removeOutOfScopeAssociations(associableDto.getFileTagDtos(), associableDto.getFileTagAssociations(), relevantScopeIds);
            removeOutOfScopeAssociations(associableDto.getDocTypeDtos(), associableDto.getDocTypeAssociations(), relevantScopeIds);

            // Partition associations by single value true or false
            keepOnlyMultiValueAndHighPriorities(associableDto.getFileTagAssociations());
            keepMatchingAssociationsTagDtos(associableDto);
            // Partition associations by single value true or false
            keepOnlyMultiValueAndHighPriorities(associableDto.getDocTypeAssociations());
            keepMatchingAssociationsDocTypeDtos(associableDto);
        } catch (Exception e) {
            logger.error("problem for dto tags {} dts {}", associableDto.getFileTagAssociations(), associableDto.getDocTypeAssociations());
            throw e;
        }
    }

    private void keepMatchingAssociationsDocTypeDtos(AssociableDto associableDto) {
        List<DocTypeDto> relevantDtos = Lists.newLinkedList();
        if (associableDto == null || associableDto.getDocTypeAssociations() == null){
            return;
        }
        for (EntityAssociationDetails fileTagAssociationDto : associableDto.getDocTypeAssociations()) {
            Optional<DocTypeDto> optionalMatchingFileTagDto = associableDto.getDocTypeDtos().stream()
                    .filter(ft -> ft.getId().equals(fileTagAssociationDto.getEntityId())
                            && ft.isImplicit() == fileTagAssociationDto.isImplicit()).findFirst();
            optionalMatchingFileTagDto.ifPresent(relevantDtos::add);
        }
        associableDto.getDocTypeDtos().clear();
        associableDto.getDocTypeDtos().addAll(relevantDtos);
    }

    private void keepMatchingAssociationsTagDtos(AssociableDto associableDto) {
        List<FileTagDto> relevantDtos = Lists.newLinkedList();
        if (associableDto == null || associableDto.getFileTagAssociations() == null){
            return;
        }
        for (EntityAssociationDetails fileTagAssociationDto : associableDto.getFileTagAssociations()) {
            Optional<FileTagDto> optionalMatchingFileTagDto = associableDto.getFileTagDtos().stream()
                    .filter(ft -> ft.getId().equals(fileTagAssociationDto.getEntityId())
                            && ft.isImplicit() == fileTagAssociationDto.isImplicit()).findFirst();
            optionalMatchingFileTagDto.ifPresent(relevantDtos::add);
        }
        associableDto.getFileTagDtos().clear();
        associableDto.getFileTagDtos().addAll(relevantDtos);
    }

    private <T extends ScopedEntity & EntityAssociationDetails> void removeOutOfScopeAssociations(
            Collection<? extends Identifiable<Long>> identifiables, Collection<T> associations, Set<Long> relevantScopeIds) {
        if (associations == null){
            return;
        }
        //noinspection SuspiciousMethodCalls
        List<T> outOfScopedEntities = associations.stream()
                .filter(se -> se.getScope() != null && !relevantScopeIds.contains(se.getScope().getId()))
                .collect(Collectors.toList());
        for (EntityAssociationDetails outOfScopeAssociation : outOfScopedEntities) {
            identifiables.removeIf(identifiable -> Objects.equals(identifiable.getId(), outOfScopeAssociation.getEntityId()));
        }
    }

    private <T extends EntityAssociationDetails> void keepOnlyMultiValueAndHighPriorities(List<T> prioritizedEntities) {
        if (prioritizedEntities == null){
            return;
        }
        Map<Boolean, List<T>> partitionedBySingleValue = prioritizedEntities.stream()
                .collect(Collectors.partitioningBy(EntityAssociationDetails::isSingleValue));

        // Clear all associations
        prioritizedEntities.clear();

        // add as default all the multi-value tags
        List<T> multiValueAssociations = partitionedBySingleValue.get(false);
        prioritizedEntities.addAll(filterMultiValuesByPriority(multiValueAssociations));

        // Add first single value associations by priority (order of FILE, FILE_GROUP, FOLDER depth)
        List<T> singleValueAssociations = partitionedBySingleValue.get(true);
        final List<T> highestPriorityAssociations = filterSingleValuesByPriority(singleValueAssociations);
        prioritizedEntities.addAll(highestPriorityAssociations);
    }

    private <T extends EntityAssociationDetails> List<T> filterMultiValuesByPriority(List<T> multiValueAssociations) {
        return filterValuesByPriority(multiValueAssociations, EntityAssociationDetails::getEntityId);
    }

    private <T extends EntityAssociationDetails> List<T> filterSingleValuesByPriority(List<T> singleValueAssociations) {
        return filterValuesByPriority(singleValueAssociations, EntityAssociationDetails::getCategoryId);
    }

    private <T extends EntityAssociationDetails> List<T> filterValuesByPriority(List<T> associations, Function<T, Long> categorySupp) {
        Table<Long, TagPriority, T> prioritizedTable = TreeBasedTable.create(Long::compareTo, TagPriority::compareTo);
        for (T prioritizedEntity : associations) {
            switch (prioritizedEntity.getFileTagSource()) {
                case FILE:
                    addToPriorityTable(prioritizedTable, prioritizedEntity, TagPriority.FILE, categorySupp);
                    break;
                case BUSINESS_ID:
                    addToPriorityTable(prioritizedTable, prioritizedEntity, TagPriority.FILE_GROUP, categorySupp);
                    break;
                case FOLDER_IMPLICIT:
                case FOLDER_EXPLICIT:
                    int depthFromRoot = -1;
                    Long originalFolderId = prioritizedEntity.getOriginalFolderId();
                    if (originalFolderId != null) {
                        depthFromRoot = prioritizedEntity.getOriginDepthFromRoot();
                    }
                    TagPriority folder = TagPriority.folder(depthFromRoot);
                    addToPriorityTable(prioritizedTable, prioritizedEntity, folder, categorySupp);
            }
        }

        // Take from the table according to each tag type only the first one
        List<T> result = Lists.newLinkedList();
        Set<Long> tagTypeIds = prioritizedTable.rowKeySet();
        for (Long tagTypeId : tagTypeIds) {
            Map<TagPriority, T> row = prioritizedTable.row(tagTypeId);
            // Add only first to list
            result.add(row.values().iterator().next());
        }

        return result;
    }

    private static <T extends EntityAssociationDetails> void addToPriorityTable(
            Table<Long, TagPriority, T> prioritizedTable, T prioritizedEntity, TagPriority tagPriority,
            Function<T, Long> categorySupp) {
        Long rowKey = categorySupp.apply(prioritizedEntity);
        T existingPrioritizedEntity = prioritizedTable.get(rowKey, tagPriority);
        if (existingPrioritizedEntity == null) {
            prioritizedTable.put(rowKey, tagPriority, prioritizedEntity);
        } else {
            // Case we already have the same priority and it is implicit then this is via doc type tag
            // So we need to replace it with real priority
            if (existingPrioritizedEntity.isImplicit()) {
                prioritizedTable.remove(rowKey, tagPriority);
                prioritizedTable.put(rowKey, tagPriority, prioritizedEntity);
            }
        }
    }

    protected String buildFilter(BasicScope basicScope) {
        Set<Long> scopeIds = scopeService.resolveScopeIds(basicScope);
        StringBuilder sb = new StringBuilder("(");
        if (basicScope == null) {
            sb.append(scopeIdFilter(FileCatUtils.GLOBAL_SCOPE_IDENTIFIER)).append(" or ");
        }
        for (Long id : scopeIds) {
            sb.append(scopeIdFilter(String.valueOf(id))).append(" or ");

        }
        return sb.delete(sb.lastIndexOf(" or "), sb.length()).append(")").toString();
    }

    @NotNull
    private String scopeIdFilter(String scopeId) {
        return CatFileFieldType.SCOPED_TAGS.getSolrName() + ":" + DocTypeDto.ID_PREFIX + scopeId + FileCatUtils.CATEGORY_SEPARATOR + "*";
    }
}
