package com.cla.filecluster.service.export.exporters.components;

import com.cla.common.domain.dto.components.ClaComponentStatusDto;
import com.cla.common.domain.dto.components.MediaProcessorStatus;
import com.cla.filecluster.service.export.ExportType;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/17/2019
 */
@Component
public class MediaProcessorHistoryExport extends ComponentHistoryExport {

    private final static String[] header = {
            TIMESTAMP, START_MAP, INGEST_TASK_REQ,
            THROUGHPUT, DISK_SPACE, MEMORY, CPU};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.MP_HISTORY);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected Map<String, Object> addExtraFields(ClaComponentStatusDto base, Map<String, String> requestParams) {
        Map<String, Object> res = super.addExtraFields(base, requestParams);

        String extraMetrics = base.getExtraMetrics();
        MediaProcessorStatus extra = componentsAppService.getMpStatusFromString(extraMetrics);
        if (extra != null) {
            res.put(INGEST_TASK_REQ, extra.getIngestTaskRequestsFinished() + "/" +
                    extra.getIngestTaskRequestsFinishedWithError() + "/" +
                    extra.getIngestTaskRequests());
            res.put(START_MAP,  extra.getActiveScanTasks() + "/" +
                    extra.getStartScanTaskRequestsFinished() + "/" +
                    extra.getStartScanTaskRequests() + " - " +
                    extra.getScanTaskResponsesFinished()
            );

            res.put(THROUGHPUT, decimalFormatter.format(extra.getScanRunningThroughput()) + "/" +
                    decimalFormatter.format(extra.getIngestRunningThroughput()));

            int mem = (int)(((float)extra.getFreePhysicalMemorySize()/extra.getTotalPhysicalMemorySize())*100);
            res.put(MEMORY, mem+"%");
            res.put(CPU, decimalFormatter.format(extra.getSystemCpuLoad()));
        }

        return res;
    }
}
