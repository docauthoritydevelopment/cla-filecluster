package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.workflow.*;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.util.ValidationUtils;
import com.cla.filecluster.service.workflow.WorkflowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * api for workflow management (CRUD)
 *
 * Created by: yael
 * Created on: 2/22/2018
 */
@RestController
@RequestMapping("/api/workflow")
public class WorkflowApiController {

    private final static Logger logger = LoggerFactory.getLogger(WorkflowApiController.class);

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<WorkflowDto> listWorkflows(@RequestParam Map<String, String> params) {
        return workflowService.listWorkflows(params);
    }

    @RequestMapping(value = "/listByIds", method = RequestMethod.GET)
    public List<WorkflowDto> getWorkflowByIds(@RequestParam Map<String, String> params) {
        String workflowIdsStr = params.get("workflowIds");
        ValidationUtils.validateStringNotEmpty(workflowIdsStr, messageHandler.getMessage("workflow.ids.empty"));
        logger.debug("Get workflow Ids: {}", workflowIdsStr);
        List<Long> workflowIds = Arrays.stream(workflowIdsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        return workflowService.getWorkflowByIds(workflowIds);
    }


    @RequestMapping(value = "", method = RequestMethod.PUT)
    public WorkflowDto createWorkflow(@RequestBody WorkflowDto workflowDto,
                                      @RequestParam(value = "comment", required = false) String userComment) {
        WorkflowDto workflow = workflowService.createWorkflow(workflowDto, userComment);
        eventAuditService.audit(AuditType.WORKFLOW, "Create workflow", AuditAction.CREATE, WorkflowDto.class, workflowDto.getId(), workflowDto);
        return workflow;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteWorkflow(@PathVariable long id) {
        WorkflowDto workflow = getWorkflow(id);
        workflowService.deleteWorkflow(id);
        eventAuditService.audit(AuditType.WORKFLOW, "Delete workflow", AuditAction.DELETE, WorkflowDto.class, id, workflow);
    }

    @RequestMapping(value = "/deleteByIds", method = RequestMethod.DELETE)
    public List<Long> deleteWorkflowByIds(@RequestParam Map<String, String> params) {
        String workflowIdsStr = params.get("workflowIds");
        ValidationUtils.validateStringNotEmpty(workflowIdsStr, messageHandler.getMessage("workflow.ids.empty"));
        logger.debug("Get workflow Ids: {}", workflowIdsStr);
        List<Long> workflowIds = Arrays.stream(workflowIdsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        List<Long> failed = workflowService.deleteWorkflowByIds(workflowIds);
        for (Long id : workflowIds) {
            if (!failed.contains(id)) {
                eventAuditService.audit(AuditType.WORKFLOW, "Delete workflow", AuditAction.DELETE, WorkflowDto.class, id);
            }
        }
        return failed;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public WorkflowDto updateWorkflow(@PathVariable Long id, @RequestBody WorkflowDto workflowDto,
                                      @RequestParam(value = "comment", required = false) String comment) {
        WorkflowDto workflow = workflowService.updateWorkflow(id, workflowDto, comment);
        eventAuditService.audit(AuditType.WORKFLOW, "Update workflow", AuditAction.UPDATE, WorkflowDto.class, workflowDto.getId(), workflowDto);
        return workflow;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public WorkflowDto getWorkflow(@PathVariable long id) {
        WorkflowDto workflow = workflowService.getWorkflow(id);
        if (workflow == null) {
            throw new BadRequestException(messageHandler.getMessage("workflow.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return workflow;
    }

    @RequestMapping(value = "/{id}/history", method = RequestMethod.GET)
    public Page<WorkflowHistoryDto> getWorkflowHistory(@PathVariable long id, @RequestParam Map<String, String> params) {
        assertWorkflowExists(id);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return workflowService.getWorkflowHistory(dataSourceRequest, id);
    }


    @RequestMapping(value = "/{id}/discover/filter", method = RequestMethod.PUT)
    public WorkflowFilterDescriptorDto saveFilter(@PathVariable long id,
                                                  @RequestBody WorkflowFilterDescriptorDto workflowFilterDescriptorDto,
                                                  @RequestParam(value = "comment", required = false) String comment) {
        assertWorkflowExists(id);
        workflowFilterDescriptorDto = workflowService.saveWorkflowFilter(id, workflowFilterDescriptorDto, comment);
        eventAuditService.audit(AuditType.WORKFLOW, "Create workflow population filter", AuditAction.CREATE, WorkflowFilterDescriptorDto.class, workflowFilterDescriptorDto.getId(), workflowFilterDescriptorDto);
        return workflowFilterDescriptorDto;
    }


    @RequestMapping(value = "/{id}/discover/filter/{filterId}", method = RequestMethod.DELETE)
    public void deleteFilter(@PathVariable long id, @PathVariable long filterId,
                             @RequestParam(value = "comment", required = false) String comment) {
        assertWorkflowExists(id);
        WorkflowFilterDescriptorDto workflowFilterDescriptorDto = workflowService.deleteWorkflowFilter(id, filterId, comment);
        eventAuditService.audit(AuditType.WORKFLOW, "Delete workflow population filter", AuditAction.CREATE, WorkflowFilterDescriptorDto.class, workflowFilterDescriptorDto.getId(), workflowFilterDescriptorDto);
    }

    @RequestMapping(value = "/{id}/discover/filter/clear", method = RequestMethod.DELETE)
    public WorkflowDto clearAllFilters(@PathVariable long id,
                                       @RequestParam(value = "comment", required = false) String comment) {
        assertWorkflowExists(id);
        WorkflowDto workflowDto = workflowService.clearAllFilters(id, comment);
        eventAuditService.audit(AuditType.WORKFLOW, "Delete all workflow population filter", AuditAction.DELETE_ITEM, WorkflowDto.class, workflowDto.getId(), workflowDto);
        return workflowDto;
    }

    @RequestMapping(value = "/{id}/action/filter", method = RequestMethod.POST)
    public void updateWorkflowFileDataByFilter(@PathVariable long id, @RequestBody WorkflowFilterDescriptorDto workflowFilterDescriptorDto,
                                               @RequestParam Map<String,String> additionalParams) {
        assertWorkflowExists(id);

        workflowService.updateFilesWorkflowDataByFilter(id, workflowFilterDescriptorDto, additionalParams.get("comment"),additionalParams);
        eventAuditService.audit(AuditType.WORKFLOW, "Update workflow population with data by filter", AuditAction.UPDATE, WorkflowDto.class, id, workflowFilterDescriptorDto);
    }

    @RequestMapping(value = "/countByState", method = RequestMethod.GET)
    public Map<WorkflowState, Integer> countWorkflowsByState() {
        return workflowService.countWorkflowsByState();
    }

    @RequestMapping(value = "/{id}/fileCountByAction", method = RequestMethod.GET)
    public Map<FileAction, Long> fileCountByAction(@PathVariable long id, @RequestParam final Map<String, String> params) {
        assertWorkflowExists(id);
        return workflowService.findFileCountByAction(id, params);
    }

    private void assertWorkflowExists(@PathVariable long id) {
        WorkflowDto workflow = workflowService.getWorkflow(id);
        if (workflow == null) {
            throw new BadRequestException(messageHandler.getMessage("workflow.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
    }
}
