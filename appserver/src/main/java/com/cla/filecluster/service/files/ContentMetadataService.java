package com.cla.filecluster.service.files;

import com.cla.common.constants.*;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.pv.GroupUnificationHelper;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.SolrRepository;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.cla.common.constants.ContentMetadataFieldType.*;
import static java.util.stream.Collectors.toList;

/**
 * Handles persistence of ContentMetadata
 */
@Service
public class ContentMetadataService {

    private final static Logger logger = LoggerFactory.getLogger(ContentMetadataService.class);

    @Autowired
    private SolrContentMetadataRepository repository;

    @Value("${scan.content.commit-solr-during:false}")
    private boolean commitSolrDuringScan;

    private LoadingCache<String, Object> contentLocks;

    private ReadWriteLock populationDirtyLock = new ReentrantReadWriteLock();

    @SuppressWarnings("unused")
    @PostConstruct
    void init() {
        contentLocks = CacheBuilder.newBuilder()
                .maximumSize(10000)
                .build(new CacheLoader<String, Object>() {
                    @Override
                    public Object load(String key) {
                        return new Object();
                    }
                });
    }

    public Optional<SolrContentMetadataEntity> getOneById(String id) {
        return repository.getOneById(id);
    }

    public ContentMetadata getById(Long id) {
        return repository.getById(id);
    }

    public SolrContentMetadataEntity findById(Long contentId) {
        return repository.findById(contentId);
    }

    /**
     * Clear reference to the file with the specified ID from the sampleFileId field in all ContentMetadatas that contain it
     *
     * @param fileId file ID to clear
     */
    public void clearSampleFile(long fileId) {
        repository.clearSampleFileIdIfNeededByClaFileId(fileId, commitSolrDuringScan ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    /**
     * File analysis group ID of content metadata by metadata ID
     *
     * @param contentId content metadata ID
     * @return analysis group ID
     */
    public String getContentGroupId(long contentId) {
        return repository.getAnalysisGroupId(String.valueOf(contentId));
    }

    /**
     * Find content ID, analysis group ID and analysis hint for each content ID in provided list
     *
     * @param contentIdsList list of content metadata IDs
     * @return list of field -> value maps
     */
    public List<Map<SolrCoreSchema, Object>> findAnalysisDataByContentIds(Collection<Long> contentIdsList) {
        return repository.findAnalysisDataByContentIds(contentIdsList);
    }

    public void changeAnalysisGroupIdAll(String newdGid, String oldGid) {
        repository.setGroup(newdGid, oldGid, ContentMetadataFieldType.GROUP_ID, SolrCommitType.SOFT_NOWAIT);
    }

    public void changeUserGroupIdAll(String newdGid, String oldGid) {
        repository.setGroup(newdGid, oldGid, ContentMetadataFieldType.USER_GROUP_ID, SolrCommitType.SOFT_NOWAIT);
    }

    public void updateAnalysisGroupIdByContentIds(Collection<Long> contentIdList, String newGid) {
        repository.updateAnalysisGroupIdByContentIds(contentIdList, newGid, SolrCommitType.SOFT_NOWAIT);
    }

    public void updateUserGroupIdByContentIds(Collection<Long> contentIdList, String newGid, String oldGid) {
        repository.updateUserGroupIdByContentIds(contentIdList, newGid, oldGid, SolrCommitType.SOFT_NOWAIT);
    }

    public void changeState(List<Long> contentIds, ClaFileState claFileState, SkipAnalysisReasonType reason) {
        List<SolrInputDocument> stateUpdates = contentIds.stream()
                .map(cId -> AtomicUpdateBuilder.create()
                        .setId(ContentMetadataFieldType.ID, cId)
                        .addModifier(ContentMetadataFieldType.PROCESSING_STATE, SolrOperator.SET, claFileState.name())
                        .addModifier(ContentMetadataFieldType.SKIP_ANALYSIS_REASON, SolrOperator.SET, reason == null ? null : reason.toInt())
                        .addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                        .build())
                .collect(toList());
        repository.saveAll(stateUpdates);
    }

    public void updateContentIdFileCount(long contentId, int fileCount) {
        repository.updateContentIdFileCount(contentId, fileCount, commitSolrDuringScan ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    public void markAsDeletedBySignatureExceptId(List<Long> contentIds) {
        repository.markAsDeletedBySignatureExceptId(contentIds);
        repository.softCommitNoWaitFlush();
    }

    public void changeStateAll(ClaFileState state, ClaFileState oldState) {
        repository.changeStateAll(state, oldState, SolrCommitType.SOFT_NOWAIT);
    }

    public void markDirtyPopulationFlagById(long contentId) {
        repository.markDirtyPopulationFlagById(contentId, commitSolrDuringScan ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    public void markDirtyPopulationFlagById(Collection<Long> contentIds) {
        markDirtyPopulationFlagById(contentIds, commitSolrDuringScan ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);

    }

    public void markDirtyPopulationFlagById(Collection<Long> contentIds, SolrCommitType solrCommitType) {
        repository.markDirtyPopulationFlagById(contentIds, solrCommitType);
    }

    public void clearSampleFileIdIfNeededByClaFileId(long claFileId) {
        repository.clearSampleFileIdIfNeededByClaFileId(claFileId, commitSolrDuringScan ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    public void setNewUserGroupForAnalysisGroup(String childrenGroupId, String newGroupId, boolean commit) {
        repository.setNewUserGroupForAnalysisGroup(childrenGroupId, newGroupId);

        if (commit) {
            repository.softCommitNoWaitFlush();
        }
    }

    public void clearPopulationDirtyFlag(long toDate) {
        repository.clearPopulationDirtyFlag(SolrCommitType.SOFT_NOWAIT, toDate);

    }

    public void clearGroupDirtyFlag(long toDate) {
        repository.clearGroupDirtyFlag(SolrCommitType.SOFT_NOWAIT, toDate);
    }

    public Table<String, Long, Long> getFirstMemberContentsForUserGroups(List<String> groupIds) {
        Query query = Query.create()
                .addFilterWithCriteria(ContentMetadataFieldType.USER_GROUP_ID, SolrOperator.IN, groupIds)
                .addField(ContentMetadataFieldType.SAMPLE_FILE_ID).addField(ContentMetadataFieldType.ID)
                .setGroupField(ContentMetadataFieldType.USER_GROUP_ID)
                .setGroupLimit(1);
        QueryResponse queryResponse = repository.runQuery(query.build());
        Table<String, Long, Long> result = HashBasedTable.create();
        if (queryResponse.getGroupResponse() != null && queryResponse.getGroupResponse().getValues() != null) {
            queryResponse.getGroupResponse().getValues().forEach(g -> {
                g.getValues().forEach(group -> {
                    String id = group.getGroupValue();
                    Long fileId = (Long) group.getResult().get(0).get(ContentMetadataFieldType.SAMPLE_FILE_ID.getSolrName());
                    String contentId = (String) group.getResult().get(0).get(ContentMetadataFieldType.ID.getSolrName());
                    if (contentId == null || fileId == null || id == null) {
                        logger.error("SOMETHING NULL content id {} file {} group {}", contentId, fileId, id);
                    } else {
                        result.put(id, fileId, Long.parseLong(contentId));
                    }
                });
            });
        }
        return result;
    }

    /**
     * Find content by ID, set its analysis group and user group to specified group ID,
     * but only if analysis group field is empty
     *
     * @param contentId content metadata ID
     * @param groupId   group ID to set
     */
    public Integer setGroupIfEmpty(String contentId, String groupId) {
        Object lock = contentLocks.getUnchecked(contentId);
        synchronized (lock) {
            return repository.setGroupIfEmpty(contentId, groupId);
        }
    }

    public void updateUserGroup(Long cmd, FileGroup newUserGroup) {
        repository.updateUserGroupIdByContentIds(Collections.singletonList(cmd), newUserGroup.getId(), null, SolrCommitType.SOFT_NOWAIT);
    }

    public void setGroupAndHintForContentManual(String contentId, String groupId) {
        repository.setGroupAndHintForContentManual(contentId, groupId);
        repository.softCommitNoWaitFlush();
    }

    public void changeGroupIdUnifyAll(Collection<GroupUnificationHelper> notProcessed) {
        // set the new analysis group to ContentMetadata according to unprocessed GroupUnificationHelpers,
        // mark ContentMetadata as groupDirty = true //todo: do we need to mark? currently used for copying to ClaFile
        repository.setGroupByUnification(notProcessed, ContentMetadataFieldType.GROUP_ID);

        // copy user group to ContentMetadata according to unprocessed GroupUnificationHelpers,
        // mark ContentMetadata as groupDirty = true //todo: do we need to mark? currently used for copying to ClaFile
        repository.setGroupByUnification(notProcessed, ContentMetadataFieldType.USER_GROUP_ID);
    }

    public void fixContentsAfterFileDeletion(Collection<Long> fileIds, boolean commit) {
        if (fileIds == null || fileIds.isEmpty()) return;
        repository.updateContentsSampleFilesWithNoFiles(fileIds);

        if (commit) {
            repository.softCommitNoWaitFlush();
        }
    }

    public long countPopulationDirty() {
        return repository.countPopulationDirty();
    }

    public long countGroupDirty() {
        return repository.countGroupDirty();
    }

    public List<ContentMetadata> findByAnalysisGroup(String groupId, int offset, int count) {
        Query query = Query.create().setRows(count).setStart(offset)
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.GROUP_ID, SolrOperator.EQ, groupId));
        List<SolrContentMetadataEntity> queryResponse = repository.runQuery(query.build()).getBeans(SolrContentMetadataEntity.class);
        return SolrContentMetadataRepository.fromEntity(queryResponse);
    }

    public List<SolrContentMetadataEntity> getByIds(Collection<String> contentIdList) {
        if (contentIdList == null || contentIdList.size() == 0) {
            return Lists.newArrayList();
        }

        return repository.find(Query.create()
                .addFilterWithCriteria(ContentMetadataFieldType.ID, SolrOperator.IN, contentIdList)
                .setRows(contentIdList.size())
                .build());
    }

    public List<ContentMetadataDto> findDtoByIds(Collection<Long> contentIdList) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.ID, SolrOperator.IN, contentIdList))
                .addField(ContentMetadataFieldType.ID).addField(ContentMetadataFieldType.GROUP_ID).addField(ContentMetadataFieldType.USER_GROUP_ID)
                .addField(ContentMetadataFieldType.TYPE).addField(ContentMetadataFieldType.SIZE).addField(ContentMetadataFieldType.SAMPLE_FILE_ID)
                .addField(ContentMetadataFieldType.PROCESSING_STATE).addField(ContentMetadataFieldType.FILE_COUNT)
                .addField(ContentMetadataFieldType.ANALYSIS_HINT).addField(ContentMetadataFieldType.ITEMS_COUNT3);

        QueryResponse queryResponse = repository.runQuery(query.build());
        List<ContentMetadataDto> result = new ArrayList<>();
        queryResponse.getResults().forEach(d -> {
            Long size = (Long) d.getFieldValue(ContentMetadataFieldType.SIZE.getSolrName());
            Object fileCountObj = d.getFieldValue(ContentMetadataFieldType.FILE_COUNT.getSolrName());
            Integer fileCount = (fileCountObj == null ? 0 : (Integer) fileCountObj);
            String analyzeHintStr = (String) d.getFieldValue(ContentMetadataFieldType.ANALYSIS_HINT.getSolrName());
            String stateStr = (String) d.getFieldValue(ContentMetadataFieldType.PROCESSING_STATE.getSolrName());
            ContentMetadataDto dto = new ContentMetadataDto(
                    Long.parseLong((String) d.getFieldValue(ContentMetadataFieldType.ID.getSolrName())),
                    (String) d.getFieldValue(ContentMetadataFieldType.GROUP_ID.getSolrName()),
                    (String) d.getFieldValue(ContentMetadataFieldType.USER_GROUP_ID.getSolrName()),
                    (Integer) d.getFieldValue(ContentMetadataFieldType.TYPE.getSolrName()),
                    size,
                    (Long) d.getFieldValue(ContentMetadataFieldType.SAMPLE_FILE_ID.getSolrName()),
                    stateStr != null ? ClaFileState.valueOf(stateStr) : null,
                    fileCount,
                    analyzeHintStr != null ? AnalyzeHint.valueOf(analyzeHintStr) : null,
                    (Long) d.getFieldValue(ContentMetadataFieldType.ITEMS_COUNT3.getSolrName()));
            result.add(dto);
        });

        return result;
    }

    public List<Object[]> findAnalysisGroupIdByContentIds(Collection<Long> contentIdList) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.ID, SolrOperator.IN, contentIdList))
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.GROUP_ID, SolrOperator.PRESENT, null))
                .addField(ContentMetadataFieldType.ID).addField(ContentMetadataFieldType.GROUP_ID).addField(ContentMetadataFieldType.ANALYSIS_HINT);

        QueryResponse queryResponse = repository.runQuery(query.build());
        List<Object[]> result = new ArrayList<>();
        queryResponse.getResults().forEach(d -> {
            Long id = Long.parseLong((String) d.getFieldValue(ContentMetadataFieldType.ID.getSolrName()));
            String group = (String) d.getFieldValue(ContentMetadataFieldType.GROUP_ID.getSolrName());
            String hint = (String) d.getFieldValue(ContentMetadataFieldType.ANALYSIS_HINT.getSolrName());
            Object[] row = {id, group, hint};
            result.add(row);
        });

        return result;
    }

    public List<Object[]> getGroupDirtyContents(long offset, int count) {
        Query query = Query.create().setRows(count).setStart((int) offset)
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.GROUP_DIRTY, SolrOperator.EQ, "true"))
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.GROUP_ID, SolrOperator.PRESENT, null))
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.FILE_COUNT, SolrOperator.GT, 0))
                .addField(ContentMetadataFieldType.ID).addField(ContentMetadataFieldType.GROUP_ID).addField(ContentMetadataFieldType.USER_GROUP_ID);

        QueryResponse queryResponse = repository.runQuery(query.build());
        List<Object[]> result = new ArrayList<>();
        queryResponse.getResults().forEach(d -> {
            Long id = Long.parseLong((String) d.getFieldValue(ContentMetadataFieldType.ID.getSolrName()));
            String group = (String) d.getFieldValue(ContentMetadataFieldType.GROUP_ID.getSolrName());
            String userGroup = (String) d.getFieldValue(ContentMetadataFieldType.USER_GROUP_ID.getSolrName());
            Object[] row = {id, group, userGroup};
            result.add(row);
        });

        return result;
    }

    public Map<String, List<Long>> getContentIdsForSignature(Collection<String> signatures) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.CONTENT_SIGNATURE, SolrOperator.IN, signatures))
                .addField(ContentMetadataFieldType.CONTENT_SIGNATURE).addField(ContentMetadataFieldType.ID);

        Map<String, List<Long>> result = new HashMap<>();
        QueryResponse queryResponse = repository.runQuery(query.build());
        queryResponse.getResults().forEach(d -> {
            String id = (String) d.getFieldValue(ContentMetadataFieldType.ID.getSolrName());
            String signature = (String) d.getFieldValue(ContentMetadataFieldType.CONTENT_SIGNATURE.getSolrName());
            List<Long> list = result.getOrDefault(signature, new ArrayList<>());
            list.add(Long.parseLong(id));
            result.put(signature, list);
        });
        return result;
    }

    public int countByState(ClaFileState state) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.FILE_COUNT, SolrOperator.GT, 0))
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.PROCESSING_STATE, SolrOperator.EQ, state.name()));
        Long res = repository.getTotalCount(query, ContentMetadataFieldType.PROCESSING_STATE, state.name());
        return (res == null ? 0 : res.intValue());
    }

    public int getDirtySignaturesCount() {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.POPULATION_DIRTY, SolrOperator.EQ, "true"))
                .addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.CONTENT_SIGNATURE, SolrOperator.PRESENT, null));
        Long res = repository.getTotalCount(query, ContentMetadataFieldType.TYPE, null);
        return (res == null ? 0 : res.intValue());
    }

    public int getContentGroupCount(String userGroupId) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.USER_GROUP_ID, SolrOperator.EQ, userGroupId));
        Long res = repository.getTotalCount(query, ContentMetadataFieldType.USER_GROUP_ID, null);
        return (res == null ? 0 : res.intValue());
    }

    public Map<String, Long> getDuplicateSignatures() {
        return repository.getDuplicateSignatures();
    }

    public Collection<String> filterNonEmptyByGroup(List<Long> subList, String analysisGroupId) {
        return repository.filterNonEmptyByGroup(subList, analysisGroupId);
    }

    public List<String> findProposedTitlesByContentId(long contentId) {
        return repository.findProposedTitlesByContentId(contentId);
    }

    public List<String> findProposedTitlesByContent(SolrContentMetadataEntity content) {
        return repository.findProposedTitlesByContent(Optional.ofNullable(content));
    }

    public Long getNextAvailableId() {
        return repository.getNextAvailableId();
    }

    public void atomicUpdate(SolrInputDocument entity) {
        repository.atomicUpdate(entity);
    }

    public void softCommitNoWaitFlush() {
        repository.softCommitNoWaitFlush();
    }

    public Map<Pair<String, Long>, List<SolrContentMetadataEntity>> findIdenticals(List<Pair<String, Long>> sigAndSizePairList) {
        return repository.findIdenticals(sigAndSizePairList);
    }

    public List<ContentMetadata> findIdenticalCM(String contentSignature, Long fileSize) {
        return SolrContentMetadataRepository.fromEntity(repository.findIdentical(contentSignature, fileSize));
    }

    public void commit() {
        repository.commit();
    }

    public Collection<String> getGroupsByContentPopulationDirty() {
        return repository.getGroupsByContentPopulationDirty();
    }

    public Pair<String, Collection<Long>> getContentsPopulationDirtyNeedSample(String solrPagingCursor, Integer count) {
        return repository.getContentsPopulationDirtyNeedSample(solrPagingCursor, count);
    }

    public Pair<String, Collection<Long>> getContentsPopulationDirty(String solrPagingCursor, Integer count) {
        return repository.getContentsPopulationDirty(solrPagingCursor, count);
    }

    public List<SolrContentMetadataEntity> findByIds(Collection<Long> contentMetadataIds) {
        if (contentMetadataIds == null || contentMetadataIds.isEmpty()) {
            return new ArrayList<>();
        }
        return repository.findContentMetadataEntitiesByIds(contentMetadataIds);
    }

    public void setNewUserGroupForAnalysisGroup(Map<String, String> gIdToParentId) {
        SolrQuery solrQuery = Query.create()
                .addFilterWithCriteria(Criteria.create(GROUP_ID, SolrOperator.IN, gIdToParentId.keySet()))
                .addField(ContentMetadataFieldType.ID)
                .addField(GROUP_ID)
                .build();
        List<SolrDocument> documents = repository.findAllByQuery(solrQuery, ContentMetadataFieldType.ID, Sort.Direction.ASC);
        List<SolrInputDocument> contentGroupUpdates = documents.stream()
                .map(d -> {
                    String cId = (String) d.getFieldValue(ContentMetadataFieldType.ID.getSolrName());
                    return AtomicUpdateBuilder.create()
                            .setId(ContentMetadataFieldType.ID, cId)
                            .addModifier(USER_GROUP_ID, SolrOperator.SET, gIdToParentId.get(d.getFieldValue(GROUP_ID.getSolrName())))
                            .addModifier(GROUP_DIRTY, SolrOperator.SET, true)
                            .addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                            .addModifier(GROUP_DIRTY_DATE, SolrOperator.SET, System.currentTimeMillis())
                            .build();
                }).collect(toList());

        repository.saveAll(contentGroupUpdates);
    }

    public void saveAll(List<SolrInputDocument> atomicUpdates) {
        repository.saveAll(atomicUpdates);
    }

    public void updateExtractionDataToCategoryFiles(Multimap<String, String> extractedEntitiesByFileContent) {
        List<SolrInputDocument> solrDocs = new ArrayList<>();
        extractedEntitiesByFileContent.keySet().forEach(content -> {
            AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
            builder.setId(ID, content);
            Collection<String> extractedEntities = extractedEntitiesByFileContent.get(content);
            if (extractedEntities != null && extractedEntities.size() > 0) {
                logger.trace("set Entities to content id {} entity size {}", content, extractedEntities.size());
                List<String> ids = extractedEntities.stream().map(SolrRepository::generateExtractedEntityId).distinct().collect(toList());

                builder.addModifier(EXTRACTED_ENTITIES_IDS, SolrOperator.ADD, ids);  // add the map as the field value
                builder.addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis());
                solrDocs.add(builder.build());
            }
        });
        if (!solrDocs.isEmpty()) {
            repository.saveAll(solrDocs);
            repository.softCommitNoWaitFlush();
        }
    }

    public static ContentMetadataDto convertContentMetaData(ContentMetadata contentMetadata) {
        if (contentMetadata == null) {
            return null;
        }
        String analysisGroupId = contentMetadata.getAnalysisGroupId();
        String userGroupId = contentMetadata.getUserGroupId();
        ContentMetadataDto dto = new ContentMetadataDto(contentMetadata.getId(), analysisGroupId, userGroupId,
                contentMetadata.getType(), contentMetadata.getFsFileSize(),
                contentMetadata.getSampleFileId(), contentMetadata.getState(),
                contentMetadata.getFileCount(), contentMetadata.getAnalyzeHint(), contentMetadata.getItemsCountL3());
        dto.setContentSignature(contentMetadata.getContentSignature());
        dto.setUserContentSignature(contentMetadata.getUserContentSignature());
        if (contentMetadata.getItemsCountL1() != null) {
            dto.setItemsCountL1(contentMetadata.getItemsCountL1());
        }
        if (contentMetadata.getItemsCountL2() != null) {
            dto.setItemsCountL2(contentMetadata.getItemsCountL2());
        }
        if (contentMetadata.getItemsCountL3() != null) {
            dto.setItemsCountL3(contentMetadata.getItemsCountL3());
        }
        dto.setAppCreationDate(contentMetadata.getAppCreationDate());
        return dto;
    }

    public QueryResponse runQuery(SolrQuery query) {
        return repository.runQuery(query);
    }

    public List<SolrContentMetadataEntity> find(SolrQuery query) {
        return repository.find(query);
    }

    public Integer updateFieldByQueryMultiFields(String[] fields, String[] values, String operation, String query, List<String> filters, SolrCommitType solrCommitType) {
        return repository.updateFieldByQueryMultiFields(fields, values, operation, query, filters, solrCommitType);
    }

    public void clearSampleFileIdIfNeededByIdAndClaFileIdPairs(List<Pair<Long, Long>> pairs, SolrCommitType commitType) {
        repository.clearSampleFileIdIfNeededByIdAndClaFileIdPairs(pairs, commitType);
    }

    public void acquirePopulationDirtyWriteLock() {
        populationDirtyLock.writeLock().lock();
    }

    public void releasePopulationDirtyWriteLock() {
        populationDirtyLock.writeLock().unlock();
    }

    public void acquirePopulationDirtyReadLock() {
        populationDirtyLock.readLock().lock();
    }

    public void releasePopulationDirtyReadLock() {
        populationDirtyLock.readLock().unlock();
    }


}
