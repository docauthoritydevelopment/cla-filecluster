package com.cla.filecluster.service.diagnostics;

import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by: yael
 * Created on: 5/5/2019
 */
@Service
public class LogWriterListener extends DiagnosticEventConsumer {

    private static final Logger logger = LoggerFactory.getLogger(LogWriterListener.class);

    @Override
    public void handleEvent(Event event) {
        logger.warn(event.eventType.name() + " " + event.getContext());
    }

    @Override
    List<EventType> getHandledEventTypes() {
        return null;
    }
}
