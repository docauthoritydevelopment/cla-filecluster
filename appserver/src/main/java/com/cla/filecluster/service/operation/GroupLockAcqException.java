package com.cla.filecluster.service.operation;

/**
 * Created by: yael
 * Created on: 5/31/2018
 */
public class GroupLockAcqException extends RuntimeException {

    public GroupLockAcqException() {
    }

    public GroupLockAcqException(String message) {
        super(message);
    }
}
