package com.cla.filecluster.service.crawler.analyze.analyzer;

import com.cla.common.constants.SkipAnalysisReasonType;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.pv.*;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.PvAnalyzerRepository;
import com.cla.filecluster.service.crawler.analyze.ContentGroupsCache;
import com.cla.filecluster.service.crawler.pv.SimilarityMapper;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ExcelAnalyzer implements ContentAnalyzer {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ExcelAnalyzer.class);

    @Autowired
    private SimilarityMapper similarityMapper;

    @Autowired
    private PvAnalyzerRepository pvAnalyzerRepository;

    @Autowired
    private ContentGroupsCache contentGroupsCache;

    @Value("${fileGrouping.excel.skip-grouped.sheet-count:true}")
    private boolean skipGroupedExcelFilesWithSheetCount;

    @Value("${fileGrouping.excel.skip-grouped.sheet-count.threshold:10}")
    private int skipGroupedExcelFilesWithSheetCountThreshold;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Override
    public Collection<Long> analyzeContent(Long taskId, final ContentMetadataDto contentMetadata) {
        return analyzeContent(taskId, contentMetadata, null);
    }

    @Override
    public Collection<Long> analyzeContent(Long taskId, final ContentMetadataDto contentMetadataDto,
                                           final SimilaritySimulationContext simulationContext) {
        String hint = contentMetadataDto.getHint();
        long contentId = contentMetadataDto.getContentId();
        logger.debug("Start handling content {} (hint:{})...", contentId, hint);
        List<Long> matchedWorkbooks;
        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("ExcelAnalyzer.analyzeContent", "analyzeWorkBookContentInSolr", 104);
        matchedWorkbooks = analyzeWorkBookContentInSolr(taskId, contentMetadataDto, simulationContext);
        kpiRecording.end();
        return matchedWorkbooks;
    }

    private List<Long> analyzeWorkBookContentInSolr(Long taskId, ContentMetadataDto contentMetadataDto, SimilaritySimulationContext simulationContext) {
        logger.debug("analyze WorkBook Content {}", contentMetadataDto);
        final long start = System.currentTimeMillis();
        long contentId = contentMetadataDto.getContentId();
        List<Long> matchedWorkBooks = Lists.newArrayList();
        if (skipGroupedExcelFilesWithSheetCount && contentMetadataDto.getItemsCountL3() != null
                && contentMetadataDto.getItemsCountL3() > skipGroupedExcelFilesWithSheetCountThreshold) {
            //Check if the file is grouped and skip if grouped.
            if (checkIfGrouped(contentId)) {
                logger.debug("Skip grouped excel content {} due to sheet count", contentId);
                return matchedWorkBooks;
            }
        }
        Collection<Long> workBookCandidates = pvAnalyzerRepository.analyzeAllPvsForWorkBook(contentMetadataDto);
        workBookCandidates = similarityMapper.filterExcelSimilarityCandidates(workBookCandidates, contentMetadataDto, simulationContext);
        logger.debug("Finished finding workbook candidates on contentId {}. Matching {} candidates", contentMetadataDto, workBookCandidates.size());
        if (workBookCandidates != null && workBookCandidates.size() > 0) {
            contentGroupsCache.addContentsToCache(workBookCandidates, taskId);
            matchedWorkBooks = similarityMapper.matchExcelWorkbooks(taskId, contentMetadataDto, workBookCandidates, simulationContext);
        } else {
            logger.debug("Workbook {} has no candidates", contentId);
        }
        final long duration = System.currentTimeMillis() - start;
        logger.debug("Finished matching workbook {} in {}ms", contentMetadataDto, duration);
        return matchedWorkBooks;
    }

    private boolean checkIfGrouped(long contentId) {
        ContentMetadata contentMetadata1 = contentMetadataService.getById(contentId);
        if (contentMetadata1.getAnalysisGroupId() != null) {
            logger.info("Skip analyzing a content id {} which is already grouped to {}", contentMetadata1.getId(), contentMetadata1.getAnalysisGroupId());
            contentMetadataService.changeState(Lists.newArrayList(contentId), ClaFileState.ANALYSED, SkipAnalysisReasonType.GROUPED_BY_ANOTHER);
            return true;
        }
        return false;
    }
}
