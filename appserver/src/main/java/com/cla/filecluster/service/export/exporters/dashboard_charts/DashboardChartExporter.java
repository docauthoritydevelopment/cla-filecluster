package com.cla.filecluster.service.export.exporters.dashboard_charts;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartPointDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.filecluster.repository.solr.FacetFunction;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.ExtraFieldsWrapper;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.report.DashboardChartFactory;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by: ophir
 * Created on: 2/9/2019
 */
@Slf4j
@Component
public class DashboardChartExporter extends PageCsvExcelExporter<Map<String, Object>> {


    @Autowired
    private DashboardChartFactory dashboardChartFactory;
    private static final String CHART_TYPE_FIELD = "chartType";
    private static final String HEADER_NAME = "Name";
    private static final String PERIOD_TYPE_MONTH = "MONTH";
    private static final String PERIOD_TYPE_DAY = "DAY";



    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    protected void configureMapper(CsvMapper mapper) {
    }

    @Override
    protected CsvSchema defineSchema(Map<String, String> requestParams, Iterable data) {
        String[] header = new String[0];
        if (data.iterator().hasNext()) {
            Map<String, Object> dataMap = ((ExtraFieldsWrapper<Map<String, Object>>) data.iterator().next()).getBaseObj();
            header = dataMap.keySet().toArray(new String[0]);
        }
        CsvSchema.Builder schemaBuilder = CsvSchema.builder();
        Arrays.stream(header).forEach(schemaBuilder::addColumn);
        return schemaBuilder.build();
    }

    @Override
    protected String[] getHeader() {
        return new String[0];
    }

    private ChartType extractChartType(Map<String, String> params) {
        String chartTypeStr = params.get(CHART_TYPE_FIELD);
        return ChartType.valueOf(chartTypeStr);
    }


    private String formatSizeValue(double bytes, int digits) {
        String[] dictionary = {"bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
        int index;
        for (index = 0; index < dictionary.length; index++) {
            if (bytes < 1024) {
                break;
            }
            bytes = bytes / 1024;
        }
        if (bytes == 0) {
            return "0 " + dictionary[index];
        }
        return String.format("%." + digits + "f", bytes) + " " + dictionary[index];
    }

    @Override
    protected Page<Map<String, Object>> getData(Map<String, String> requestParams) {
        ChartType cType = extractChartType(requestParams);
        String periodType = dashboardChartsUtil.extractPeriodType(requestParams);

        SimpleDateFormat dateFormatter = null;
        if (periodType != null) {
            dateFormatter = new SimpleDateFormat("yyyy");
            if (periodType.equals(PERIOD_TYPE_MONTH)) {
                dateFormatter = new SimpleDateFormat("MMM yyyy");
            } else if (periodType.equals(PERIOD_TYPE_DAY)) {
                dateFormatter = new SimpleDateFormat("MMM d yyyy");
            }
        }

        DashboardChartValueTypeDto cValueType = dashboardChartsUtil.extractChartValueField(requestParams);
        DashboardChartDataDto dashboardChartDataDto = dashboardChartFactory.getBuilder(cType).buildChartDataDto(requestParams);
        List<String> categories = dashboardChartDataDto.getCategories();
        List<DashboardChartSeriesDataDto> series = dashboardChartDataDto.getSeries();
        List<Map<String, Object>> result = new ArrayList<>();
        if (dashboardChartDataDto.getPoints() != null) {
            for (int count = 0; count < dashboardChartDataDto.getPoints().size(); count++) {
                Map<String, Object> singleDataSeries = new LinkedHashMap<>();
                DashboardChartPointDataDto point = dashboardChartDataDto.getPoints().get(count);
                singleDataSeries.put("ID", point.getId());
                singleDataSeries.put("REAL_PATH", point.getDescription());
                singleDataSeries.put("PARENT_ID", point.getParent());
                singleDataSeries.put("VALUE", getStringValue(dateFormatter, cValueType, point.getValue()));
                singleDataSeries.put("DIRECT_VALUE", getStringValue(dateFormatter, cValueType, point.getDirectValue()));
                result.add(singleDataSeries);
            }
        }
        else {
            for (int count = 0; count < categories.size(); count++) {
                Map<String, Object> singleDataSeries = new LinkedHashMap<>();
                singleDataSeries.put(HEADER_NAME, categories.get(count));
                for (DashboardChartSeriesDataDto dashboardChartSeriesDataDto : series) {
                    Double numValue = dashboardChartSeriesDataDto.getData().get(count);
                    singleDataSeries.put(dashboardChartSeriesDataDto.getName(), getStringValue(dateFormatter, cValueType, numValue));
                }
                result.add(singleDataSeries);
            }
        }
        return new PageImpl<>(result);
    }

    private String getStringValue(SimpleDateFormat dateFormatter, DashboardChartValueTypeDto cValueType, Double numValue) {
        String strValue;
        if (cValueType.getSolrFunction().equals(FacetFunction.COUNT)) {
            strValue = String.valueOf(numValue);
        } else if (cValueType.getSolrField().equals(CatFileFieldType.CREATION_DATE) ||
                cValueType.getSolrField().equals(CatFileFieldType.LAST_MODIFIED) ||
                cValueType.getSolrField().equals(CatFileFieldType.LAST_ACCESS)) {
            strValue = dateFormatter.format(new Date(numValue.longValue()));
        } else {
            strValue = formatSizeValue(numValue, 2);
        }
        return strValue;
    }

    @Override
    protected Map<String, Object> addExtraFields(Map<String, Object> base, Map<String, String> requestParams) {
        return base;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DASHBOARD_CHART);
    }
}
