package com.cla.filecluster.service.api.users;

import com.cla.common.domain.dto.email.MailNotificationConfigurationDto;
import com.cla.common.domain.dto.email.SmtpConfigurationDto;
import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.common.domain.dto.security.LdapConnectionDetailsDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.communication.CommunicationService;
import com.cla.filecluster.service.communication.EmailConfigurationService;
import com.cla.filecluster.service.communication.EmailProgressReportService;
import com.cla.filecluster.service.security.LdapConnectionDetailsService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.*;

/**
 * Created by uri on 12-Apr-17.
 */
@RestController()
@RequestMapping("/api/communication")
public class CommunicationApiController {

    private static final Logger logger = LoggerFactory.getLogger(CommunicationApiController.class);

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private EmailConfigurationService emailConfigurationService;

    @Autowired
    private EmailProgressReportService emailProgressReportService;

    @Autowired
    private CommunicationService communicationService;

    @Autowired
    private LdapConnectionDetailsService ldapConnectionDetailsService;

    @Autowired
    private MessageHandler messageHandler;

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/email/notification/settings", method = {RequestMethod.GET})
    public MailNotificationConfigurationDto getNotificationSettings() {
        return emailConfigurationService.getNotificationConfigurationSettings();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/email/notification/settings", method = {RequestMethod.POST})
    public void configureNotificationSettings(@RequestBody MailNotificationConfigurationDto mailNotificationConfigurationDto) {
        validateMailConfiguration(mailNotificationConfigurationDto);
        mailNotificationConfigurationDto.getSmtpConfigurationDto().setPasswordEncrypted(false);     // Work around UI limitation
        SmtpConfigurationDto smtpConfigurationDto = emailConfigurationService.setNotificationConfiguration(mailNotificationConfigurationDto);
        communicationService.resetCache();
        eventAuditService.audit(AuditType.SETTINGS, "Update email notification settings", AuditAction.UPDATE,
                SmtpConfigurationDto.class, smtpConfigurationDto.getUsername(), smtpConfigurationDto);
    }

    private void validateMailConfiguration(MailNotificationConfigurationDto mailNotificationConfigurationDto) {
        SmtpConfigurationDto smtpConfigurationDto = mailNotificationConfigurationDto.getSmtpConfigurationDto();

        // only in case we are required to authenticate we should validate that these fields are supplied
        if (smtpConfigurationDto.getSmtpAuthentication() && emailConfigurationService.isNotExsistingCurrentSmtpPassword()) {
            if (Strings.isNullOrEmpty(smtpConfigurationDto.getPassword())) {
                throw new BadRequestException(messageHandler.getMessage("smtp.password.empty"), BadRequestType.MISSING_FIELD);
            }

            if (Strings.isNullOrEmpty(smtpConfigurationDto.getUsername())) {
                throw new BadRequestException(messageHandler.getMessage("smtp.user.empty"), BadRequestType.MISSING_FIELD);
            }
        }


        if (Strings.isNullOrEmpty(smtpConfigurationDto.getHost())) {
            throw new BadRequestException(messageHandler.getMessage("smtp.host.empty"), BadRequestType.MISSING_FIELD);
        }

        if (smtpConfigurationDto.getPort() <= 0 || smtpConfigurationDto.getPort() > 65535) {
            throw new BadRequestException(messageHandler.getMessage("smtp.port.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {GENERAL_ADMIN, TECH_SUPPORT})
    @RequestMapping(value = "/email/notification/test", method = {RequestMethod.POST})
    public TestResultDto testEmail(@RequestBody MailNotificationConfigurationDto mailNotificationConfigurationDto) {
        mailNotificationConfigurationDto.getSmtpConfigurationDto().setPasswordEncrypted(false);     // Work around UI limitation
        return communicationService.testConnection(mailNotificationConfigurationDto.getSmtpConfigurationDto());
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {RUN_SCANS})
    @RequestMapping(value = "/scan/email-report", method = {RequestMethod.GET})
    public void emailProgressReport() {
        emailProgressReportService.sendEmailReport();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/email/notification/sample", produces = MediaType.TEXT_HTML_VALUE)
    public void generateHtmlReport(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        String sampleHtml = emailProgressReportService.createSampleHtml();
        response.getWriter().append(sampleHtml);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/settings", method = {RequestMethod.GET})
    public Page<LdapConnectionDetailsDto> getAllLdapSettings(@RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        return ldapConnectionDetailsService.getAllLdapConnectionDtos(pageRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/settings/{id}", method = {RequestMethod.GET})
    public LdapConnectionDetailsDto getLdapSettings(@PathVariable long id) {
        LdapConnectionDetailsDto dto = ldapConnectionDetailsService.getLdapConnectionDtosById(id).orElse(null);
        if (dto == null) {
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/settings", method = {RequestMethod.POST})
    public LdapConnectionDetailsDto addNewLdapSettings(@RequestBody LdapConnectionDetailsDto dtoFromRequest) {
        LdapConnectionDetailsDto resultDto = ldapConnectionDetailsService.addNewConnDetails(dtoFromRequest);
        eventAuditService.audit(AuditType.SETTINGS, "Add ldap connection settings", AuditAction.CREATE, LdapConnectionDetailsDto.class, resultDto.getId(), resultDto);
        return resultDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/settings/{id}", method = {RequestMethod.PUT})
    public LdapConnectionDetailsDto updateLdapSettings(@PathVariable long id,
                                                       @RequestBody LdapConnectionDetailsDto dtoFromRequest) {
        Long idFromDto = dtoFromRequest.getId();
        if (idFromDto != id) {
            logger.error("Got LDAP connection details update request with miss-matching IDs. ID from path: {}, ID from DTO: {}", id, idFromDto);
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.miss-match-ids"), BadRequestType.OTHER);
        }

        LdapConnectionDetailsDto  dtoFromService = ldapConnectionDetailsService.updateLdapConnectionDetailsById(dtoFromRequest).orElse(null);
        if (dtoFromService == null) {
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.SETTINGS, "Update ldap connection settings", AuditAction.UPDATE, LdapConnectionDetailsDto.class, dtoFromService.getId(), dtoFromService);
        return dtoFromService;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/settings/{id}", method = {RequestMethod.DELETE})
    public void deleteLdapSettings(@PathVariable long id) {
        try {
            if (!ldapConnectionDetailsService.deleteLdapConnectionDetailsById(id)) {
                logger.debug("Failed to delete LDAP settings with ID: {}", id);
                throw new BadRequestException(messageHandler.getMessage("ldap-connection.missing") , BadRequestType.ITEM_NOT_FOUND);
            }
        } catch (Exception e) {
            logger.debug("Error occurred when trying to delete LDAP settings with ID: {}", id, e);
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.delete.failure", Lists.newArrayList(e)), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/settings/{id}/enabled", method = {RequestMethod.PUT})
    public LdapConnectionDetailsDto setLdapSettingsEnabled(@PathVariable long id, @RequestBody boolean enabled) {
        LdapConnectionDetailsDto dto = ldapConnectionDetailsService.setLdapConnectionDetailsEnabledById(id, enabled).orElse(null);
        if (dto == null) {
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dto;

    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_USERS, MNG_ROLES, MNG_USER_ROLES, GENERAL_ADMIN})
    @RequestMapping(value = "/ldap/test", method = {RequestMethod.POST})
    public TestResultDto testLdap(@RequestBody LdapConnectionDetailsDto dto) {
        TestResultDto resultDto = ldapConnectionDetailsService.testConnection(dto).orElse(null);
        if (resultDto == null) {
            throw new BadRequestException(messageHandler.getMessage("ldap-connection.test-conn.failure"), BadRequestType.UNSUPPORTED_VALUE);
        }
        return resultDto;
    }
}
