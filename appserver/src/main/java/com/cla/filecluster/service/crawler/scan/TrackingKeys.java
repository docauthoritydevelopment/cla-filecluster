package com.cla.filecluster.service.crawler.scan;

import com.cla.connector.domain.dto.media.MediaType;

/**
 * Holds all the count/volume keys we wish to track
 */
public final class TrackingKeys {

    private static final String ROOT_FOLDER_INDICATION = "ROOT_FOLDER";
    private static final String MEDIA_TYPE_INDICATION = "MEDIA_TYPE";
    private static final String PROCESSED_TOTAL_PREFIX = "PROCESSED_TOTAL";
    private static final String PROCESSED_ROOTFOLDER_PREFIX = "PROCESSED_" + ROOT_FOLDER_INDICATION;
    private static final String PROCESSED_MEDIA_PREFIX = "PROCESSED_" + MEDIA_TYPE_INDICATION;
    private static final String VOLUME_INDICATION = "VOLUME";
    private static final String COUNT_INDICATION = "COUNT";

    public static final String TOTAL_FILE_COUNT_KEY = PROCESSED_TOTAL_PREFIX + "_" + COUNT_INDICATION;
    public static final String TOTAL_FILE_VOLUME_KEY = PROCESSED_TOTAL_PREFIX + "_" + VOLUME_INDICATION;


    private static final String ROOT_FOLDER_FILE_COUNT_KEY_PREFIX = PROCESSED_ROOTFOLDER_PREFIX + "_"
            + COUNT_INDICATION;
    private static final String ROOT_FOLDER_FILE_VOLUME_KEY_PREFIX = PROCESSED_ROOTFOLDER_PREFIX + "_"
            + VOLUME_INDICATION;

    private static final String MEDIA_FILE_COUNT_KEY_PREFIX = PROCESSED_MEDIA_PREFIX + "_"
            + COUNT_INDICATION;
    private static final String MEDIA_FILE_VOLUME_KEY_PREFIX = PROCESSED_MEDIA_PREFIX + "_"
            + VOLUME_INDICATION;

    public static final String GROUPED_FILES = "GROUPED_FILES";
    public static final String ROOT_FOLDER_GROUPED_FILES_PREFIX = GROUPED_FILES + "_" + ROOT_FOLDER_INDICATION + "_";
    public static final String MEDIA_TYPE_GROUPED_FILES_PREFIX = GROUPED_FILES + "_" + MEDIA_TYPE_INDICATION + "_";


    public static final String rootFolderVolumeKey(long rootFolderId) {
        return ROOT_FOLDER_FILE_VOLUME_KEY_PREFIX + "_" + rootFolderId;
    }


    public static final String rootFolderCountKey(long rootFolderId) {
        return ROOT_FOLDER_FILE_COUNT_KEY_PREFIX + "_" + rootFolderId;
    }


    public static final String mediaVolumeKey(MediaType mediaType) {
        return MEDIA_FILE_VOLUME_KEY_PREFIX + "_" + mediaType;
    }


    public static final String mediaCountKey(MediaType mediaType) {
        return MEDIA_FILE_COUNT_KEY_PREFIX + "_" + mediaType;
    }

    public static String rootFolderNewGroupedFilesCountKey(Long rfId) {
        return ROOT_FOLDER_GROUPED_FILES_PREFIX + rfId;
    }

    public static String mediaNewGroupedFilesCountKey(MediaType mediaType) {
        return MEDIA_TYPE_GROUPED_FILES_PREFIX + mediaType;
    }
}
