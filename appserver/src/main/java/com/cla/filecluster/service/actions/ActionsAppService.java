package com.cla.filecluster.service.actions;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.action.*;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.domain.entity.actions.ActionParameterDefinition;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.util.GlobalPropertiesService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Actions service - creating scripts to perform actions on files in the system
 * the scripts can then be ran by the sys admin on the relevant server
 * e.g encrypt/decrypt/change permissions etc
 *
 * Created by uri on 02/02/2016.
 */
@Service
public class ActionsAppService {

    private Logger logger = LoggerFactory.getLogger(ActionsAppService.class);

    @Value("${action.definition.defaults.file:./config/actions/action-definitions.csv}")
    private String actionDefinitionListFile;

    @Value("${action.definition.defaults.file:./config/actions/action-class-executors.csv}")
    private String actionClassExecutorsFile;

    @Value("${action.definition.parameters.file:./config/actions/action-parameter-definitions.csv}")
    private String actionParameterDefinitionsFile;

    @Value("${action.folder.results:./temp/actions}")
    private String actionResultsFolder;

    @Value("${action.folder.separateById:true}")
    private Boolean actionResultsFolderSeparateById;

    @Value("${action.folder.backup:./temp/backup}")
    private String actionsDefaultBackupFolder;

    @Autowired
    private ActionDefinitionService actionDefinitionService;

    @Autowired
    private FileActionsEngineService fileActionsEngineService;

    @Autowired
    private ActionClassExecutorService actionClassExecutorService;

    @Autowired
    private ActionExecutionStatusService actionExecutionStatusService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private GlobalPropertiesService globalPropertiesService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;


    private final static String ACTION_ID_FIELD = "actionId";
    private final static String ID_FIELD = "id";
    private final static String NAME_FIELD = "name";
    private final static String DESCRIPTION_FIELD = "description";
    private final static String ACTION_CLASS_FIELD = "actionClass";
    private final static String JAVA_CLASS_NAME = "javaClassName";
    private static final String REGEX_FIELD = "regex";

    private static final String JAVA_METHOD_NAME = "methodName";
    private final static String[] requiredHeadersActionsDefinitions = {ID_FIELD, NAME_FIELD, DESCRIPTION_FIELD,ACTION_CLASS_FIELD, JAVA_METHOD_NAME};
    private String[] requiredHeadersActionsExecutors = {ID_FIELD, NAME_FIELD, DESCRIPTION_FIELD,ACTION_CLASS_FIELD, JAVA_CLASS_NAME};
    //actionId, id, name, description, regex
    private String[] requiredHeadersActionParameterDefinitions = {ACTION_ID_FIELD, ID_FIELD, NAME_FIELD, DESCRIPTION_FIELD, REGEX_FIELD};

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;


    @Transactional
    public void loadDefaultActionConfiguration() {
        logger.info("Load Action Class Executors :{}",actionClassExecutorsFile);
        if (actionClassExecutorsFile== null || actionClassExecutorsFile.isEmpty()) {
            logger.info("No default action class executors file defined - ignored");
        }
        else {
            Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, actionClassExecutorsFile, requiredHeadersActionsExecutors);
            int failedRows = 0;
            try {
                failedRows = csvReader.readAndProcess(actionClassExecutorsFile, row -> createActionClassExecutorFromRow(row, headersLocationMap), null, null);
            } catch (Exception e) {
                throw new RuntimeException("Failed to load action class - problem in csv file "+e.getMessage(),e);
            }
            if (failedRows > 0) {
                throw new RuntimeException("Failed to load "+failedRows+" action class - problem in csv file");
            }
        }
        logger.info("load Default Actions List :{}",actionDefinitionListFile);
        if (actionDefinitionListFile==null || actionDefinitionListFile.isEmpty()) {
            logger.info("No default action definitions file defined - ignored");
        }
        else {
            Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, actionDefinitionListFile, requiredHeadersActionsDefinitions);
            int failedRows = 0;
            try {
                failedRows = csvReader.readAndProcess(actionDefinitionListFile, row -> createActionDefinitionFromRow(row, headersLocationMap), null, null);
            } catch (Exception e) {
                throw new RuntimeException("Failed to load action class - problem in csv file "+e.getMessage(),e);
            }
            if (failedRows > 0) {
                throw new RuntimeException("Failed to load "+failedRows+" action class - problem in csv file");
            }
        }
        logger.info("Load actions parameters list : {}",actionParameterDefinitionsFile);
        if (actionParameterDefinitionsFile==null || actionParameterDefinitionsFile.isEmpty()) {
            logger.info("No default action parameter definitions file defined - ignored");
        }
        else {
            Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, actionParameterDefinitionsFile, requiredHeadersActionParameterDefinitions);
            int failedRows = 0;
            try {
                failedRows = csvReader.readAndProcess(actionParameterDefinitionsFile, row -> createActionParameterDefinitionFromRow(row, headersLocationMap), null, null);
            } catch (Exception e) {
                throw new RuntimeException("Failed to load action class - problem in csv file "+e.getMessage(),e);
            }
            if (failedRows > 0) {
                throw new RuntimeException("Failed to load "+failedRows+" action class - problem in csv file");
            }
        }

    }

    private void createActionParameterDefinitionFromRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length==0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String actionId = fieldsValues[headersLocationMap.get(ACTION_ID_FIELD)].trim();
        String id = fieldsValues[headersLocationMap.get(ID_FIELD)].trim();
        String name = fieldsValues[headersLocationMap.get(NAME_FIELD)].trim();
        String description = fieldsValues[headersLocationMap.get(DESCRIPTION_FIELD)].trim();
        String regex = fieldsValues[headersLocationMap.get(REGEX_FIELD)];
        regex = regex != null ? regex.trim() :".*";
        logger.debug("Create action parameter definition id {}, name {} and description {}", id, name, description);
        ActionDefinition actionDefinition = actionDefinitionService.findActionDefinition(actionId);
        actionDefinitionService.createActionParameterDefinition(actionDefinition,id,name,description,regex);
    }

    private void createActionClassExecutorFromRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length==0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String id = fieldsValues[headersLocationMap.get(ID_FIELD)].trim();
        String name = fieldsValues[headersLocationMap.get(NAME_FIELD)].trim();
        String description = fieldsValues[headersLocationMap.get(DESCRIPTION_FIELD)].trim();
        String actionClassField = fieldsValues[headersLocationMap.get(ACTION_CLASS_FIELD)].trim();
        String javaClassName = fieldsValues[headersLocationMap.get(JAVA_CLASS_NAME)].trim();
        logger.debug("Create action class executor by id {}, name {} and description {}", id, name, description);
        ActionClass actionClass = ActionClass.valueOf(actionClassField);
        actionClassExecutorService.createActionClassExecutorDefinition(id,name,description,actionClass,javaClassName);

    }

    private void createActionDefinitionFromRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length==0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String id = fieldsValues[headersLocationMap.get(ID_FIELD)].trim();
        String name = fieldsValues[headersLocationMap.get(NAME_FIELD)].trim();
        String description = fieldsValues[headersLocationMap.get(DESCRIPTION_FIELD)].trim();
        String actionClassField = fieldsValues[headersLocationMap.get(ACTION_CLASS_FIELD)].trim();
        String methodName = fieldsValues[headersLocationMap.get(JAVA_METHOD_NAME)].trim();
        logger.debug("Create action definition by id {}, name {} and description {}", id, name, description);
        ActionClass actionClass = ActionClass.valueOf(actionClassField);
        actionDefinitionService.createActionDefinition(id,name,description,actionClass,methodName);
    }

    @Transactional(readOnly = true)
    public Page<ActionDefinitionDto> listActionDefinitions(DataSourceRequest dataSourceRequest) {
        logger.debug("List action definitions");
        Page<ActionDefinition> results = actionDefinitionService.listActionDefinitions(dataSourceRequest);
        return ActionDefinitionService.convertActionDefinitions(results,dataSourceRequest.createPageRequest());
    }

    @Transactional(readOnly = true)
    public ActionDefinitionDto findAction(String id, String name) {
        if (name != null) {
            return ActionDefinitionService.convertActionDefinition(actionDefinitionService.findActionDefinitionByName(name));
        }
        else {
            return ActionDefinitionService.convertActionDefinition(actionDefinitionService.findActionDefinition(id));
        }
    }

    public ActionExecutionTaskStatusDto setActionTriggerRunning(long triggerId) {
        ActionExecutionTaskStatus actionExecutionTaskStatus = setActionTriggerState(triggerId, RunStatus.RUNNING);
        return convertActionExecutionTaskStatus(actionExecutionTaskStatus);
    }

    public ActionExecutionTaskStatus setActionTriggerState(long triggerId, RunStatus state) {
        return actionExecutionStatusService.updateActionTriggerState(triggerId, state);
    }

    public void updateErrorCount(Long triggerId, ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        if (actionExecutionTaskStatusDto.getErrorsCount() > 0) {
            actionExecutionStatusService.updateErrorsCount(triggerId,actionExecutionTaskStatusDto.getErrorsCount(), actionExecutionTaskStatusDto.getFirstError());
        }
    }

    public void increaseActionTriggerCounter(Long triggerId, int executedActions, ActionClass actionClass) {
        if (executedActions > 0) {
            logger.debug("A total of {} actions were executed for actionClass {}", executedActions, actionClass);
            actionExecutionStatusService.increaseActionTriggerCounter(triggerId, executedActions);
        }
    }

    @Transactional(readOnly = true)
    public ActionExecutionTaskStatusDto getActionExecutionTaskStatus(long actionTriggerId) {
        ActionExecutionTaskStatus actionExecutionTaskStatus = actionExecutionStatusService.getActionTrigger(actionTriggerId);
        ActionExecutionTaskStatusDto actionExecutionTaskStatusDto = convertActionExecutionTaskStatus(actionExecutionTaskStatus);
        addScriptFoldersToDto(actionExecutionTaskStatus,actionExecutionTaskStatusDto);
        return actionExecutionTaskStatusDto;
    }

    public static Long getDifferentialScriptFromTime(Properties parameters) {
        if (parameters == null) return null;

        if (parameters.getProperty(ActionRunner.DIFFERENTIAL_SCRIPT_FROM_TIME) != null) {
            return Long.parseLong(parameters.getProperty(ActionRunner.DIFFERENTIAL_SCRIPT_FROM_TIME));
        }

        return null;
    }

    public Map<Long, DocFolder> getFoldersForFiles(Collection<ClaFile> files) {
        if (files == null) return null;
        Set<Long> folderIds = files.stream().map(ClaFile::getDocFolderId).collect(Collectors.toSet());
        List<DocFolder> folders = docFolderService.findByIds(folderIds);
        return folders.stream().collect(Collectors.toMap(DocFolder::getId, r -> r));
    }

    public Map<Long, RootFolder> getRootFoldersForFiles(Collection<ClaFile> files) {
        if (files == null) return null;
        Set<Long> rootFolderIds = files.stream().map(ClaFile::getRootFolderId).collect(Collectors.toSet());
        List<RootFolder> rootFolders = docStoreService.getRootFoldersCached(rootFolderIds);
        return rootFolders.stream().collect(Collectors.toMap(RootFolder::getId, r -> r));
    }

    /**
     * Creates the action and also (if createPolicyOnly is false) does most of the work of extracting the data into the
     * act_file_action_desired table. Later, the action will process that data.
     */
    @Transactional
    public long queueActionOnFilterAndDispatchPolicy(FilterDescriptor filterDescriptor, String actionId,
                                               Properties actionParams, Long userId) {

        ActionQueueingResult actionQueueingResult = queueActionOnFilter(
                filterDescriptor, actionId, actionParams, userId, true);

        return actionQueueingResult.actionTriggerId;
    }

    public ActionQueueingResult queueActionOnFilter(FilterDescriptor filterDescriptor, String actionId,
                                                    Properties actionParams, Long userId, boolean createPolicyOnly) {

        logger.info("Queue action {} on filter {}",actionId,filterDescriptor);

        ActionDefinition actionDefinition = actionDefinitionService.findActionDefinition(actionId);
        if (actionDefinition == null) {
            throw new BadRequestException(messageHandler.getMessage("action.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        long actionTriggerId =  fileActionsEngineService.createActionTriggerEvent(
                System.currentTimeMillis(), createPolicyOnly ? ActionTriggerType.POLICY : ActionTriggerType.IMMEDIATE,
                filterDescriptor, actionId, actionParams, null, null, userId);

        return new ActionQueueingResult(actionTriggerId, actionDefinition);
    }



    public SolrSpecification buildSolrSpecification(Long fromTime, FilterDescriptor filterDescriptor) {
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(filterDescriptor);
        if (fromTime != null && fromTime > 0) {
            solrSpecification.setAdditionalFilter(
                    CatFileFieldType.LAST_METADATA_CNG_DATE.getSolrName()+":["+fromTime+" TO *]");
        }
        solrSpecification.setAdditionalFilter(CatFileFieldType.DELETED.filter("false"));
        solrSpecification.setSortField(CatFileFieldType.ID.getSolrName());
        solrSpecification.setSortOrder(SolrQuery.ORDER.asc);
        return solrSpecification;
    }

    public String resolveMediaConnectorUserName(ClaFile file) {
        return Optional.ofNullable(file)
                .map(ClaFile::getRootFolderId)
                .flatMap(id -> mediaConnectionDetailsService.getMediaConnectionDetailsByRootFolderId(id, true))
                .map(MediaConnectionDetailsDto::getUsername)
                .orElse("");
    }

    @Transactional(readOnly = true)
    public Page<ActionExecutionTaskStatusDto> getAllActionTriggers(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<ActionExecutionTaskStatus> actionTriggers = actionExecutionStatusService.getAllActionTriggers(pageRequest);
        return convertActionExecutionStatusList(actionTriggers,pageRequest);
    }

    public void stopExecutionEngine() {
        fileActionsEngineService.requestStopEngine();
    }

    @Transactional(readOnly = true)
    public List<ActionExecutionTaskStatusDto> getRunningActionTriggers() {
        List<ActionExecutionTaskStatus> actionExecutionTaskStatuses = actionExecutionStatusService.getRunningActionTriggers();
        return convertActionExecutionStatusList(actionExecutionTaskStatuses);
    }

    @Transactional(readOnly = true)
    public ActionExecutionTaskStatusDto getLastRunningActionExecution() {
        ActionExecutionTaskStatus lastActionExecutionRun = actionExecutionStatusService.getLastRunningActionExecution();
        ActionExecutionTaskStatusDto actionExecutionTaskStatusDto = convertActionExecutionTaskStatus(lastActionExecutionRun);
        addScriptFoldersToDto(lastActionExecutionRun, actionExecutionTaskStatusDto);
        return actionExecutionTaskStatusDto;
    }

    private void addScriptFoldersToDto(ActionExecutionTaskStatus lastActionExecutionRun, ActionExecutionTaskStatusDto actionExecutionTaskStatusDto) {
        if (lastActionExecutionRun != null) {
            //Add the server folders to the execution details
            Stream<Path> actionExecutionReportsFolderPaths = getActionExecutionReportsFolderPaths(lastActionExecutionRun.getId());
            StringJoiner stringJoiner = new StringJoiner(";");
            actionExecutionReportsFolderPaths.forEach(p -> stringJoiner.add(p.toString()));
            actionExecutionTaskStatusDto.setScriptFolders(stringJoiner.toString());
        }
    }

    @Transactional(readOnly = true)
    public ActionDefinitionDetailsDto listActionParamters(String actionId) {
        List<ActionParameterDefinition> actionParameterDefinitions = actionDefinitionService.listActionParameters(actionId);
        ActionDefinitionDetailsDto actionDetailsDto = new ActionDefinitionDetailsDto();
        actionDetailsDto.setActionParameters(convertActionParameterDefinitions(actionParameterDefinitions));
        actionDetailsDto.setActionId(actionId);
        return actionDetailsDto;
    }

    public Stream<Path> getActionExecutionReportsFolderPaths(long actionTriggerId) {
        ActionHelperImplementation actionHelperImplementation = new ActionHelperImplementation(this);
        return actionHelperImplementation.getAllActionClassFolders(actionTriggerId);
        // ???
    }

    String getActionResultsFolder() {
        return actionResultsFolder;
    }

    String getDefaultBackupFolder() {
        return actionsDefaultBackupFolder;
    }

    boolean isActionResultsFolderSeparateById() {
        return actionResultsFolderSeparateById != null && actionResultsFolderSeparateById;
    }


    Map<String, Object> getActionExecutorProperties(String actionExecutorName) {
        return globalPropertiesService.getPropertiesStartingWith("actions."+actionExecutorName+".",true);
    }

    public static class ActionQueueingResult {
        public final long actionTriggerId;
        public final ActionDefinition actionDefinition;

        ActionQueueingResult(long actionTriggerId, ActionDefinition actionDefinition) {
            this.actionTriggerId = actionTriggerId;
            this.actionDefinition = Objects.requireNonNull(actionDefinition);
        }
    }

    private static ActionExecutionTaskStatusDto convertActionExecutionTaskStatus(ActionExecutionTaskStatus actionExecutionTaskStatus) {
        if (actionExecutionTaskStatus == null) {
            return null;
        }
        ActionExecutionTaskStatusDto result = new ActionExecutionTaskStatusDto();
        result.setStatus(actionExecutionTaskStatus.getStatus());
        result.setTriggerTime(actionExecutionTaskStatus.getTriggerTime());
        result.setActionTriggerId(actionExecutionTaskStatus.getId());
        result.setProgressMark(actionExecutionTaskStatus.getProgressMark());
        result.setProgressMaxMark(actionExecutionTaskStatus.getProgressMaxMark());
        result.setErrorsCount(actionExecutionTaskStatus.getErrorsCount());
        result.setFirstError(actionExecutionTaskStatus.getFirstError());
        result.setType(actionExecutionTaskStatus.getType());
        result.setUserId(actionExecutionTaskStatus.getUserId());
        result.setActionParams(actionExecutionTaskStatus.getActionParams());
        result.setActionId(actionExecutionTaskStatus.getActionId());
        result.setEntityId(actionExecutionTaskStatus.getEntityId());
        result.setEntityType(actionExecutionTaskStatus.getEntityType());
        result.setFilterDescriptor(actionExecutionTaskStatus.getFilterDescriptor());
        return result;
    }

    private static Page<ActionExecutionTaskStatusDto> convertActionExecutionStatusList(Page<ActionExecutionTaskStatus> actionTriggers, Pageable pageRequest) {
        List<ActionExecutionTaskStatusDto> content = new ArrayList<>();
        for (ActionExecutionTaskStatus actionExecutionTaskStatus : actionTriggers.getContent()) {
            content.add(convertActionExecutionTaskStatus(actionExecutionTaskStatus));
        }

        return new PageImpl<>(content, pageRequest, actionTriggers.getTotalElements());
    }

    private static List<ActionExecutionTaskStatusDto> convertActionExecutionStatusList(List<ActionExecutionTaskStatus> actionExecutionTaskStatuses) {
        List<ActionExecutionTaskStatusDto> content = new ArrayList<>();
        for (ActionExecutionTaskStatus actionExecutionTaskStatus : actionExecutionTaskStatuses) {
            content.add(convertActionExecutionTaskStatus(actionExecutionTaskStatus));
        }
        return content;
    }

    private static List<ActionParameterDto> convertActionParameterDefinitions(List<ActionParameterDefinition> actionParameterDefinitions) {
        List<ActionParameterDto> result = new ArrayList<>();
        for (ActionParameterDefinition actionParameterDefinition : actionParameterDefinitions) {
            result.add(convertActionParameterDefinition(actionParameterDefinition));
        }
        return result;
    }

    private static ActionParameterDto convertActionParameterDefinition(ActionParameterDefinition actionParameterDefinition) {
        ActionParameterDto actionParameterDefinitionDto = new ActionParameterDto();
        actionParameterDefinitionDto.setName(actionParameterDefinition.getName());
        actionParameterDefinitionDto.setRegex(actionParameterDefinition.getRegex());
        actionParameterDefinitionDto.setDescription(actionParameterDefinition.getDescription());
        actionParameterDefinitionDto.setId(actionParameterDefinition.getId());
        return actionParameterDefinitionDto;
    }
}
