package com.cla.filecluster.service.export.exporters.file.prop;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

/**
 * Created by: ophir
 * Created on: 3/27/2019
 */
public class FileTypeCategoryDtoMixin {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(ID)
    private String id ;
}
