package com.cla.filecluster.service.functionalrole;

import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleSummaryInfoDto;
import com.cla.common.domain.dto.security.LdapGroupMappingDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.FunctionalRoleRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.security.LdapGroupMappingService;
import com.cla.filecluster.service.security.RoleTemplateService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class FunctionalRoleService {

    private static final Logger logger = LoggerFactory.getLogger(FunctionalRoleService.class);

    @Autowired
    private FunctionalRoleRepository functionalRoleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private LdapGroupMappingService ldapGroupMappingService;

    private LoadingCache<Long, Optional<FunctionalRoleDto>> funcRoleCache = null;

    @PostConstruct
    void init() {
        funcRoleCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, Optional<FunctionalRoleDto>>() {
                            public Optional<FunctionalRoleDto> load(Long key) {
                                return dbTemplateUtils.doInTransaction(() -> {
                                    FunctionalRole fr = getDataRoleByIdWithRoles(key);
                                    return Optional.ofNullable(convertFunctionalRole(fr));
                                });
                            }
                        });

        dbTemplateUtils.doInTransaction(() -> {
            functionalRoleRepository.findAllWithRoles().forEach(funcRole -> {
                funcRoleCache.put(funcRole.getId(), Optional.ofNullable(convertFunctionalRole(funcRole)));
            });
        });
    }

    public FunctionalRoleDto getFromCache(Long funcRoleId) {
        FunctionalRoleDto dto = funcRoleCache.getUnchecked(funcRoleId).orElse(null);
        return dto == null ? null : new FunctionalRoleDto(dto);
    }

    private int findSelectedItemIndex(Long selectedItemId, List<FunctionalRole> all) {
        int i = 0;
        for (FunctionalRole one : all) {
            if (one.getId().equals(selectedItemId)) {
                return i;
            }
            i++;
        }
        return i;
    }

    @Transactional(readOnly = true)
    public Page<FunctionalRoleSummaryInfoDto> listAllFunctionRoles(DataSourceRequest dataSourceRequest) {
        logger.debug("List all function roles ");

        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<FunctionalRole> functionalRoles;
        if (dataSourceRequest.getSelectedItemId() != null) {
            List<FunctionalRole> allRoles = functionalRoleRepository.findNotDeleted();
            Long selectedItemId = Long.valueOf(dataSourceRequest.getSelectedItemId());
            logger.debug("listScheduleGroupsSummaryInfo selectedItemId={}", selectedItemId);
            int index = findSelectedItemIndex(selectedItemId, allRoles);
            logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
            if (index > 0) {
                //calculate page
                int fromPage = (index / pageRequest.getPageSize());
                pageRequest = new PageRequest(fromPage, pageRequest.getPageSize());
            }
            int fromIndex = (int) pageRequest.getOffset();
            int total = allRoles.size();
            int toIndex = Math.min(total, fromIndex + pageRequest.getPageSize());
            List<FunctionalRole> content = new ArrayList<>(allRoles.subList(fromIndex, toIndex));
            functionalRoles = new PageImpl<>(content, pageRequest, total);
        } else {
            functionalRoles = functionalRoleRepository.findNotDeleted(pageRequest);
        }

        Page<FunctionalRoleSummaryInfoDto> functionalRoleSummaryInfoDtos = convertFunctionalRolesToSummaryInfoPage(functionalRoles, pageRequest);
        functionalRoleSummaryInfoDtos.forEach(r ->  //TODO Liora: make this in one query to DB (Note that this would be redundant with the impl of DAMAIN-2523)
        {
            List<LdapGroupMappingDto> mappingsByRoleId = ldapGroupMappingService.getMappingsByRoleId(r.getFunctionalRoleDto().getId());
            r.setLdapGroupMappingDtos(mappingsByRoleId);
        });
        return functionalRoleSummaryInfoDtos;
    }

    @Transactional
    public FunctionalRoleSummaryInfoDto createFunctionalRole(FunctionalRoleDto functionalRoleDto, List<LdapGroupMappingDto> ldapGroupMappingDtos) {
        logger.debug("Create functional role {} ", functionalRoleDto);
        isValidForFunctionalRoleCreation(functionalRoleDto);
        FunctionalRole functionalRole = new FunctionalRole();
        functionalRole.setName(functionalRoleDto.getName());
        functionalRole.setDisplayName(functionalRoleDto.getDisplayName());
        functionalRole.setDescription(functionalRoleDto.getDescription());

        functionalRole.setManagerUsername(functionalRoleDto.getManagerUsername());
        functionalRole.setTemplate(RoleTemplateService.convertRoleTemplateDto(functionalRoleDto.getTemplate(), true));
        FunctionalRole newFunctionalRole = functionalRoleRepository.save(functionalRole);

        FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto = new FunctionalRoleSummaryInfoDto();
        functionalRoleSummaryInfoDto.setFunctionalRoleDto(convertFunctionalRole(newFunctionalRole));

        logger.debug("Create functional role mappings {} ", ldapGroupMappingDtos);
        List<LdapGroupMappingDto> updatedMappings = updateMappingsForFunctionalRole(newFunctionalRole, ldapGroupMappingDtos != null ? ldapGroupMappingDtos.stream().collect(Collectors.toSet()) : null);
        functionalRoleSummaryInfoDto.setLdapGroupMappingDtos(updatedMappings);
        return functionalRoleSummaryInfoDto;
    }

    private void isValidName(FunctionalRoleDto functionalRoleDto) {
        FunctionalRole roleByName = functionalRoleRepository.findByNameAll(functionalRoleDto.getName());
        if (roleByName != null) {
            if (roleByName.isDeleted()) {
                roleByName.setName(roleByName.getName() + System.currentTimeMillis());
                functionalRoleRepository.saveAndFlush(roleByName);
            } else {
                throw new BadRequestException(messageHandler.getMessage("data-role.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void isValidDisplayName(FunctionalRoleDto functionalRoleDto) {
        FunctionalRole roleByDispName = functionalRoleRepository.getFunctionalRoleByDisplayNameAll(functionalRoleDto.getDisplayName());
        if (roleByDispName != null) {
            if (roleByDispName.isDeleted()) {
                roleByDispName.setDisplayName(roleByDispName.getDisplayName() + System.currentTimeMillis());
                functionalRoleRepository.saveAndFlush(roleByDispName);
            } else {
                throw new BadParameterException(messageHandler.getMessage("data-role.display-name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void isValidObject(FunctionalRoleDto functionalRoleDto) {
        if (Strings.isNullOrEmpty(functionalRoleDto.getName()) || Strings.isNullOrEmpty(functionalRoleDto.getName().trim())) {
            throw new BadParameterException(messageHandler.getMessage("data-role.name.empty"), BadRequestType.MISSING_FIELD);
        }

        if (functionalRoleDto.getTemplate() == null) {
            throw new BadParameterException(messageHandler.getMessage("data-role.role-template.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void isValidForFunctionalRoleCreation(FunctionalRoleDto functionalRoleDto) {
        isValidObject(functionalRoleDto);
        isValidName(functionalRoleDto);
        isValidDisplayName(functionalRoleDto);
    }

    @Transactional(readOnly = true)
    public FunctionalRoleDto getFunctionalRoleById(long id) {
        return getFromCache(id);
    }

    @Transactional(readOnly = true)
    public FunctionalRole getDataRoleById(long id) {
        return functionalRoleRepository.getOne(id);
    }

    @Transactional(readOnly = true)
    public FunctionalRole getDataRoleByIdWithRoles(long id) {
        return functionalRoleRepository.getOneWithRoles(id);
    }

    @Transactional
    public void deleteFunctionalRole(long id) {
        Optional<FunctionalRole> one = functionalRoleRepository.findById(id);
        if (!one.isPresent()) {
            throw new BadParameterException(messageHandler.getMessage("data-role.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        Page<User> connectedUsers = userService.getCompRoleAssignedUsers(id, dataSourceRequest.createPageRequest());
        if (!connectedUsers.getContent().isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("data-role.delete.has-users"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }

        FunctionalRole role = one.get();
        logger.debug("Delete functional role {} ", role);
        ldapGroupMappingService.deleteAllGroupMappingsForDaRole(id);
        role.setDeleted(true);
        functionalRoleRepository.save(role);
        funcRoleCache.invalidate(id);
    }

    @Transactional
    public FunctionalRoleSummaryInfoDto updateFunctionalRole(FunctionalRoleDto functionalRoleDto, List<LdapGroupMappingDto> ldapGroupMappingDtos) {
        isValidObject(functionalRoleDto);
        FunctionalRole one = functionalRoleRepository.getOne(functionalRoleDto.getId());
        logger.debug("Update functional role {} ", functionalRoleDto);
        if (!one.getName().equals(functionalRoleDto.getName())) {
            isValidName(functionalRoleDto);
        }
        if (!one.getDisplayName().equals(functionalRoleDto.getDisplayName())) {
            isValidDisplayName(functionalRoleDto);
        }
        one.setName(functionalRoleDto.getName());
        one.setDisplayName(functionalRoleDto.getDisplayName());
        one.setDescription(functionalRoleDto.getDescription());
        one.setManagerUsername(functionalRoleDto.getManagerUsername());
        one.setTemplate(RoleTemplateService.convertRoleTemplateDto(functionalRoleDto.getTemplate(), true));
        FunctionalRole saveFuncRole = functionalRoleRepository.save(one);
        funcRoleCache.invalidate(functionalRoleDto.getId());
        FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto = new FunctionalRoleSummaryInfoDto();
        functionalRoleSummaryInfoDto.setFunctionalRoleDto(convertFunctionalRole(saveFuncRole));
        logger.debug("Update functional role mapping {} ", ldapGroupMappingDtos);
        List<LdapGroupMappingDto> updatedMappings = updateMappingsForFunctionalRole(saveFuncRole, ldapGroupMappingDtos != null ? ldapGroupMappingDtos.stream().collect(Collectors.toSet()) : null);
        functionalRoleSummaryInfoDto.setLdapGroupMappingDtos(updatedMappings);

        return functionalRoleSummaryInfoDto;
    }

    private List<LdapGroupMappingDto> updateMappingsForFunctionalRole(FunctionalRole saveFuncRole, Set<LdapGroupMappingDto> ldapGroupMappingDtos) {
        List<LdapGroupMappingDto> updatedLdapGroupMappingDtos = new ArrayList<>();
        if (ldapGroupMappingDtos != null) {
            updatedLdapGroupMappingDtos = ldapGroupMappingService.saveGroupMappingsForRole(saveFuncRole, ldapGroupMappingDtos);
        }
        return updatedLdapGroupMappingDtos;
    }

    @Transactional(readOnly = true)
    public Set<FunctionalRole> getFunctionalRoleByIds(Collection<Long> daRolesIds) {
        return Sets.newHashSet(functionalRoleRepository.findByIds(daRolesIds));
    }

    @Transactional(readOnly = true)
    public Set<FunctionalRoleDto> getFunctionalRoleDtoByIds(Collection<Long> daRolesIds) {
        return getFunctionalRoleByIds(daRolesIds).stream().map(FunctionalRoleService::convertFunctionalRole).collect(Collectors.toSet());
    }

    @Transactional(readOnly = true)
    public List<FunctionalRoleDto> getFunctionalRoleDtoByRoleTemplateId(Long roleTemplateId) {
        return functionalRoleRepository.findByRoleTemplate(roleTemplateId).stream().map(FunctionalRoleService::convertFunctionalRole).collect(Collectors.toList());
    }

    public static Page<FunctionalRoleSummaryInfoDto> convertFunctionalRolesToSummaryInfoPage(Page<FunctionalRole> functionalRoles, PageRequest pageRequest) {
        List<FunctionalRoleSummaryInfoDto> content = new ArrayList<>();
        for (FunctionalRole item : functionalRoles.getContent()) {
            content.add(convertFunctionalRoleToSummaryInfo(item));
        }
        return new PageImpl<>(content, pageRequest, functionalRoles.getTotalElements());
    }

    private static FunctionalRoleSummaryInfoDto convertFunctionalRoleToSummaryInfo(FunctionalRole functionalRole) {
        FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto = new FunctionalRoleSummaryInfoDto();
        functionalRoleSummaryInfoDto.setFunctionalRoleDto(convertFunctionalRole(functionalRole));
        return functionalRoleSummaryInfoDto;
    }

    public static List<FunctionalRoleDto> convertFunctionalRoles(Collection<FunctionalRole> functionalRole) {
        return functionalRole.stream().map(FunctionalRoleService::convertFunctionalRole).collect(Collectors.toList());
    }

    public static FunctionalRoleDto convertFunctionalRole(FunctionalRole functionalRole) {
        if (functionalRole == null) {
            return null;
        }
        FunctionalRoleDto functionalRoleDto = new FunctionalRoleDto();
        functionalRoleDto.setDescription(functionalRole.getDescription());
        functionalRoleDto.setId(functionalRole.getId());
        functionalRoleDto.setDisplayName(functionalRole.getDisplayName());
        functionalRoleDto.setDeleted(functionalRole.isDeleted());
        functionalRoleDto.setName(functionalRole.getName());
        functionalRoleDto.setManagerUsername(functionalRole.getManagerUsername());
        functionalRoleDto.setTemplate(RoleTemplateService.convertRoleTemplate(functionalRole.getTemplate(), true));
        return functionalRoleDto;
    }
}
