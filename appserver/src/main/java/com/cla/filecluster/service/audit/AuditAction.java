package com.cla.filecluster.service.audit;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by oren on 03/10/2017.
 */
public enum AuditAction {
    CREATE(true),
    CREATE_ITEM(true),
    UPDATE(true),
    UPDATE_ITEM(true),
    DELETE(true),
    DELETE_ITEM(true),
    ASSIGN(true),
    UNASSIGN(true),
    IMPORT(true),
    IMPORT_ITEMS(true),
    IMPORT_ITEM_ATTACH(true),
    IMPORT_ITEM_CREATE(true),
    IMPORT_ITEM_UPDATE(true),
    START(true),
    STOP(true),
    STOP_GRACEFUL(true),
    PAUSE(true),
    RESUME(true),
    ATTACH(true),
    DETACH(true),
    VIEW(true),
    SUCCEEDED(true),
    FAILED(true),
    AUTO(true),
    VIEW_INFO(true),
    UNDELETE(true),
    RECOVER(true);

    private boolean isInUse;

    AuditAction(boolean isInUse) {
        this.isInUse = isInUse;
    }

    public static List<AuditAction> getInUse() {
        return Arrays.stream(values()).filter(v -> v.isInUse).sorted(new AuditActionComparator()).collect(Collectors.toList());
    }

    static class AuditActionComparator implements Comparator<AuditAction> {
        public int compare(AuditAction o1, AuditAction o2) {
            return o1.name().compareTo(o2.name()); // this compares length
        }
    }
}
