package com.cla.filecluster.service.export.exporters.schedule_groups.mixin;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.schedule_groups.ActiveScheduledGroupsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class ScheduleGroupDtoMixin {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(PERIOD)
    private String schedulingDescription;

}
