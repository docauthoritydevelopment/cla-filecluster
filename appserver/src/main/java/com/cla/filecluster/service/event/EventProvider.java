package com.cla.filecluster.service.event;

import com.cla.common.domain.dto.event.DAEvent;
import com.cla.common.domain.dto.event.EventProviderType;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
public interface EventProvider<T> {

    EventProviderType getType();

    EventNormalizer<T> getEventNormalizer(Class<T> dataType);

    void send(DAEvent event);
}
