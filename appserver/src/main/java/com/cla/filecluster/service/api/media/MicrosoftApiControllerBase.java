package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.media.MicrosoftConnectionDetailsDtoBase;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.media.MicrosoftMediaAppService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.function.Function;

/**
 *
 * Created by uri on 07/08/2016.
 */
public abstract class MicrosoftApiControllerBase {

    @Autowired
    protected MicrosoftMediaAppService microsoftMediaAppService;

    @Autowired
    protected MediaConnectionDetailsService connectionDetailsService;

    /**
     *
     * @param dto DTO to use
     * @param getConnectionDetailsFromDB Function to retrieve connection details from db if none received.
     */
    TestResultDto testConnection(SharePointConnectionParametersDto sharePointConnectionParamDto,
                                           Long customerDataCenterId, Optional<Long> connectionId,
                                           MicrosoftConnectionDetailsDtoBase dto,
                                           Function<Long, ? extends MicrosoftConnectionDetailsDtoBase> getConnectionDetailsFromDB) {
        if (sharePointConnectionParamDto == null && connectionId.isPresent()) {
            MicrosoftConnectionDetailsDtoBase savedConnection = getConnectionDetailsFromDB.apply(connectionId.get());
            if (savedConnection == null) {
                return new TestResultDto(false, "Couldn't resolve connection details.");
            }
            sharePointConnectionParamDto = savedConnection.getSharePointConnectionParametersDto();
        }

        if (sharePointConnectionParamDto == null) {
            sharePointConnectionParamDto = new SharePointConnectionParametersDto();
        }

        String password = Optional.ofNullable(sharePointConnectionParamDto.getPassword())
                .orElse(resolvePassword(connectionId, getConnectionDetailsFromDB));
        if (password == null) {
            return new TestResultDto(false, "No password was provided");
        }

        String encPassword = connectionDetailsService.encrypt(password);
        sharePointConnectionParamDto.setPassword(encPassword);
        dto.setSharePointConnectionParametersDto(sharePointConnectionParamDto);
        return microsoftMediaAppService.testConnection(dto, customerDataCenterId);
    }

    private String resolvePassword(Optional<Long> connectionId, Function<Long, ? extends MicrosoftConnectionDetailsDtoBase> getConnectionDetailsFromDB) {
        return connectionId.map(aLong -> Optional.ofNullable(getConnectionDetailsFromDB.apply(aLong))
                .map(dto -> dto.getSharePointConnectionParametersDto().getPassword())
                .orElse(null))
                .orElse(null);

    }
}
