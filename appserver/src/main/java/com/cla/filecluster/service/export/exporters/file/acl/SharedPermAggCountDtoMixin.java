package com.cla.filecluster.service.export.exporters.file.acl;

import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 9/24/2019
 */
public class SharedPermAggCountDtoMixin {

    @JsonUnwrapped
    RootFolderSharePermissionDto item;

    @JsonProperty(FileCountHeaderFields.NUM_OF_FILES)
    private Integer count;
}
