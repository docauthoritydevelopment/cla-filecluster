package com.cla.filecluster.service.syscomponent;

import com.cla.common.domain.dto.components.amq.QueueProperty;
import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.components.*;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;
import com.cla.filecluster.domain.entity.monitor.ClaComponentStatus;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import com.cla.filecluster.repository.jpa.monitor.ClaComponentRepository;
import com.cla.filecluster.repository.jpa.monitor.ClaComponentStatusRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Management of system components and their status (e.g app server, mp, solr, db etc)
 * Created by liora on 02/03/2017.
 */
@Service
public class SysComponentService {

    @Autowired
    private ClaComponentRepository claComponentRepository;

    @Autowired
    private ClaComponentStatusRepository claComponentStatusRepository;

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${folder.max.size.threshold.mb:0}")
    private int folderMaxSizeThreshold;

    @Value("${disk.free.space.threshold.percent:10}")
    private int freeSpaceThreshold;

    @Value("${disk.free.space.check-lookup.min:30}")
    private int timeForMemoryCheckMin;

    private static Logger logger = LoggerFactory.getLogger(SysComponentService.class);

    private LoadingCache<InstanceCacheKey, Optional<Long>> instanceIdCache;

    private LoadingCache<Long, List<Long>> customerDataCenterActiveMediaProcessorsCache;

    private TimeSource timeSource = new TimeSource();

    private long lastDBsave = 0;

    private Map<Long, String> latestDriveDataForComponent = new HashMap<>();

    long getLastDBsave() {
        return lastDBsave;
    }

    @PostConstruct
    public void init() {
        instanceIdCache = CacheBuilder.newBuilder().expireAfterWrite(30, TimeUnit.MINUTES)
                .build(new CacheLoader<InstanceCacheKey, Optional<Long>>() {

                    @Override
                    @Transactional
                    public Optional<Long> load(@NotNull InstanceCacheKey key) {
                        ClaComponent componentByInstanceId = claComponentRepository.findComponentByInstanceId(key.getType(), key.getInstanceId());
                        if (componentByInstanceId != null) {
                            return Optional.of(componentByInstanceId.getId());
                        }
                        return Optional.empty();
                    }
                });

        customerDataCenterActiveMediaProcessorsCache = CacheBuilder.newBuilder()
                .build(
                        new CacheLoader<Long,  List<Long>>() {
                            public List<Long> load(@NotNull Long customerDataCenterId) {
                                return checkCustomerDataCenterMediaProcessors(customerDataCenterId);
                            }
                        });


        initDriveData();
    }

    @Transactional(readOnly = true)
    public void initDriveData() {
        try {
            List<Object[]> claComponents = claComponentRepository.findAllWithLatestStatus();

            for (Object[] res : claComponents) {
                Long id = ((BigInteger) res[0]).longValue();
                String driveData = (res[16] == null ? null : (String) res[16]);
                if (driveData != null) {
                    latestDriveDataForComponent.put(id, driveData);
                }
            }
        } catch (Exception e) {
            logger.error("cannot fill latest drive data", e);
        }
    }

    @Transactional(readOnly = true)
    public List<ClaComponent> getByComponentType(ClaComponentType type) {
        return claComponentRepository.findByComponentType(type);
    }

    public boolean doesCustomerDataCenterHaveActiveMediaProcessor(Long customerDataCenterId) {
        try {
            int activeMP = claComponentRepository.countActiveByCustomerDataCenterId(customerDataCenterId, ClaComponentType.MEDIA_PROCESSOR, ComponentState.OK);
            return activeMP > 0;
        } catch (Exception e) {
            logger.error("Failed to check customerDataCenter {} MP status", customerDataCenterId, e);
        }
        return false;
    }

    private List<Long> checkCustomerDataCenterMediaProcessors(Long customerDataCenterId) {
        try {
            List<ClaComponent> sysComponents = claComponentRepository.findByCustomerDataCenterId(customerDataCenterId, ClaComponentType.MEDIA_PROCESSOR);
            return sysComponents.stream()
                    .filter(c -> c.getState() == ComponentState.STARTING || c.getState() == ComponentState.OK)
                    .map(ClaComponent::getId)
                    .collect(Collectors.toList());
        }
        catch (Exception e) {
            logger.error("Failed to check  customerDataCenter {} sysComponents", customerDataCenterId, e);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public List<Long> getActiveMediaProcessors(Long customerDataCenterId, boolean useCache) {
        if (useCache) {
            return customerDataCenterActiveMediaProcessorsCache.getUnchecked(customerDataCenterId);
        } else {
            return checkCustomerDataCenterMediaProcessors(customerDataCenterId);
        }
    }

    @Transactional(readOnly = true)
    public Page<ClaComponentStatusDto> listSysComponentsStatus(long componentId, long timeTo, Long dcId, DataSourceRequest dataSourceRequest) {
        logger.debug("List all sys components status");
        Sort sort = new Sort(Sort.Direction.DESC, "timestamp");
        PageRequest pageRequest = dataSourceRequest.createPageRequest(sort);
        Page<ClaComponentStatus> claComponentsStatus = claComponentStatusRepository.findByComponentId(componentId, timeTo, pageRequest);
        return convertClaComponentsStatus(claComponentsStatus, dcId, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<ClaComponentDto> findSysComponentByCustomerDataCenter(long customerDataCenterId, ClaComponentType claComponentType, DataSourceRequest dataSourceRequest ) {
        PageRequest pageRequestWithSort = dataSourceRequest.createPageRequestWithSort();
        Page<ClaComponent> byCustomerDataCenterId = claComponentRepository.findByCustomerDataCenterId(customerDataCenterId, claComponentType, pageRequestWithSort );
        return convertClaComponents(byCustomerDataCenterId,pageRequestWithSort);
    }

    @Transactional(readOnly = true)
    public Page<ClaComponentDto> listAllSysComponents(DataSourceRequest dataSourceRequest) {
        logger.debug("List all sys components ");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<ClaComponent> claComponents = claComponentRepository.findAll(pageRequest);
        return convertClaComponentsPage(claComponents, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<ClaComponentSummaryDto> findSysComponentByCustomerDataCenterWithStatus(boolean isHistoricalRequest,
            long customerDataCenterId, ClaComponentType claComponentType, long timestampFrom, long timestampTo,
            DataSourceRequest dataSourceRequest ) {
        PageRequest pageRequestWithSort = dataSourceRequest.createPageRequestWithSort();
        List<Object[]> byCustomerDataCenterId = claComponentRepository.findByDataCenterIdWithStatus(customerDataCenterId, claComponentType.ordinal(), timestampFrom, timestampTo);
        return convertByRequest(isHistoricalRequest, byCustomerDataCenterId,pageRequestWithSort);
    }

    public Page<ClaComponentSummaryDto> listComponentsWithStatus(DataSourceRequest dataSourceRequest, Map<String, String> params) {
        String customerDataCenterId = params.get("customerDataCenterId");
        String timestampLimitStr = params.get("timestamp");

        boolean isHistoricalRequest = false;
        if (!Strings.isNullOrEmpty(timestampLimitStr)) {
            isHistoricalRequest = true;
        }

        Long timestampLimit = Strings.isNullOrEmpty(timestampLimitStr) ? null : Long.parseLong(timestampLimitStr);
        long timestampTo = System.currentTimeMillis();
        if (timestampLimit != null && timestampLimit > 0) {
            timestampTo = timestampLimit;
        }
        long timestampFrom = timestampTo - TimeUnit.HOURS.toMillis(6); // 6 hr limit for this query

        if (customerDataCenterId != null) {
            return findSysComponentByCustomerDataCenterWithStatus(
                    isHistoricalRequest, Long.valueOf(customerDataCenterId), ClaComponentType.MEDIA_PROCESSOR, timestampFrom, timestampTo, dataSourceRequest);
        }

        return listAllSysComponentsWithStatus(isHistoricalRequest, timestampFrom, timestampTo, dataSourceRequest);
    }

    @Transactional(readOnly = true)
    public Page<ClaComponentSummaryDto> listAllSysComponentsWithStatus(boolean isHistoricalRequest,
                                  long timestampFrom, long timestampTo, DataSourceRequest dataSourceRequest) {
        logger.debug("List all sys components ");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        List<Object[]> claComponents = claComponentRepository.findAllWithStatus(timestampFrom, timestampTo);
        return convertByRequest(isHistoricalRequest, claComponents, pageRequest);
    }

    private Page<ClaComponentSummaryDto> convertByRequest(boolean isHistoricalRequest, List<Object[]> claComponents, PageRequest pageRequest) {
        Map<Long, ClaComponentSummaryDto> results = new TreeMap<>();
        for (Object[] res : claComponents) {
            ClaComponentSummaryDto summary = new ClaComponentSummaryDto();
            ClaComponentDto claComponentDto = new ClaComponentDto();

            claComponentDto.setId(((BigInteger)res[0]).longValue());

            if (res[3] != null) {
                ClaComponentType type = ClaComponentType.valueOf((String) res[3]);
                claComponentDto.setClaComponentType(type);
                claComponentDto.setComponentType(type.name());
            } else if (res[2] != null) {
                ClaComponentType type = ClaComponentType.valueOf((int)res[2]);
                claComponentDto.setClaComponentType(type);
                claComponentDto.setComponentType(type.name());
            }

            Long statusTimestamp = (res[13] == null ? null : ((BigInteger)res[13]).longValue());

            if (res[4] != null) {
                claComponentDto.setExternalNetAddress((String) res[4]);
            }

            if (res[5] != null) {
                claComponentDto.setInstanceId((String) res[5]);
            }

            if (res[10] != null) {
                claComponentDto.setStateChangeDate(((BigInteger)res[10]).longValue());
            }

            if (res[7] != null) {
                claComponentDto.setLocalNetAddress((String) res[7]);
            }

            if (res[6] != null) {
                claComponentDto.setLastResponseDate(((BigInteger)res[6]).longValue());
            }

            if (!isHistoricalRequest) {
                claComponentDto.setState(ComponentState.of((int) res[9]));
            } else if (res[14] != null) {
                claComponentDto.setState(ComponentState.of((int) res[14]));
            }

            if (!isHistoricalRequest) {
                claComponentDto.setActive((Boolean) res[1]);
            } else {
                if (claComponentDto.getState() == ComponentState.OK || claComponentDto.getState() == ComponentState.STARTING) {
                    claComponentDto.setActive(true);
                } else {
                    claComponentDto.setActive(false);
                }
            }

            if (res[11] != null) {
                CustomerDataCenterDto dc = new CustomerDataCenterDto();
                dc.setId(((BigInteger)res[11]).longValue());
                if (res[15] != null) {
                    dc.setName((String)res[15]);
                }
                claComponentDto.setCustomerDataCenterDto(dc);
            }

            String extraMetrics = (res[12] == null ? null : (String) res[12]);
            if (claComponentDto.getClaComponentType().equals(ClaComponentType.BROKER) ||
                    claComponentDto.getClaComponentType().equals(ClaComponentType.BROKER_DC)) {
                extraMetrics = fixExtraMetricsForBroker(extraMetrics, null);
            }

            String driveData = (res[16] == null ? null : (String) res[16]);
            summary.setDriveData(driveData);

            summary.setClaComponentDto(claComponentDto);
            summary.setExtraMetrics(extraMetrics);
            summary.setStatusTimestamp(statusTimestamp);

            if (results.containsKey(claComponentDto.getId())) {
                ClaComponentSummaryDto previous = results.get(claComponentDto.getId());
                if (previous.getStatusTimestamp() == null ||
                        (summary.getStatusTimestamp() != null && previous.getStatusTimestamp() < summary.getStatusTimestamp())) {
                    results.put(claComponentDto.getId(), summary);
                }
            } else {
                results.put(claComponentDto.getId(), summary);
            }
        }
        ArrayList<ClaComponentSummaryDto> all = new ArrayList<>(results.values());

        int from = (int) pageRequest.getOffset();
        int to = Math.min(results.size(),from + pageRequest.getPageSize());

        if (from > all.size()) {
            return new PageImpl<>(new ArrayList<>(1), pageRequest, all.size());
        }

        return new PageImpl<>(all.subList(from, to), pageRequest, results.size());
    }

    private void isValidForSysComponentCreation(String instanceId, ClaComponentType type) {
        if (Strings.isNullOrEmpty(instanceId)) {
            throw new BadRequestException(messageHandler.getMessage("system-component.instance.empty"), BadRequestType.MISSING_FIELD);
        }

        ClaComponent one = claComponentRepository.findComponentByInstanceId(type,instanceId);
        if (one != null) {
            logger.warn("Can't create SysComponent. InstanceId {} for type {} - already exists", instanceId,type);
            throw new BadRequestException(messageHandler.getMessage("system-component.instance.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    private void isValidForSysComponentUpdate(String instanceId, String newInstanceId, ClaComponentType type, ComponentState componentState) {
        if(ComponentState.PENDING_INIT.equals(componentState)) {
            if (Strings.isNullOrEmpty(newInstanceId)) {
                throw new BadRequestException(messageHandler.getMessage("system-component.instance.empty"), BadRequestType.MISSING_FIELD);
            }
            if (!newInstanceId.equals(instanceId)) {
                ClaComponent one = claComponentRepository.findComponentByInstanceId(type, newInstanceId);
                if (one != null) {
                    logger.warn("Can't create SysComponent. InstanceId {} for type {} - already exists", newInstanceId, type);
                    throw new BadRequestException(messageHandler.getMessage("system-component.instance.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
                }
            }
        }
    }

    @Transactional
    public void updateComponentAddress(ClaComponentType type, String instanceId, String external) {
        ClaComponent one = claComponentRepository.findComponentByInstanceId(type, instanceId);
        if (one != null) {
            one.setExternalNetAddress(external);
            claComponentRepository.save(one);
        }
    }

    @Transactional
    public ClaComponentDto createSysComponent(ClaComponentDto claComponentDto) {
        logger.debug("Create sys component {}", claComponentDto);
        isValidForSysComponentCreation(claComponentDto.getInstanceId(),claComponentDto.getClaComponentType());

        ClaComponent claComponent = new ClaComponent();
        claComponent.setActive(claComponentDto.getActive());
        claComponent.setClaComponentType(claComponentDto.getClaComponentType());
        claComponent.setComponentType(claComponentDto.getComponentType());
        claComponent.setExternalNetAddress(claComponentDto.getExternalNetAddress());
        claComponent.setInstanceId(claComponentDto.getInstanceId());
        claComponent.setLocalNetAddress(claComponentDto.getLocalNetAddress());
        claComponent.setLocation(claComponentDto.getLocation());
        claComponent.setState(claComponentDto.getState());
        if (claComponentDto.getStateChangeDate() != null) {
            claComponent.setStateChangeDate(claComponentDto.getStateChangeDate());
        }
        updateSysComponentDataCenter(claComponentDto, claComponent);

        try {
            ClaComponent newClaComponent = claComponentRepository.save(claComponent);
            InstanceCacheKey instanceCacheKey = new InstanceCacheKey(newClaComponent.getInstanceId(), newClaComponent.getClaComponentType());
            instanceIdCache.invalidate(instanceCacheKey);
            if (claComponentDto.getClaComponentType().equals(ClaComponentType.MEDIA_PROCESSOR)) {
                customerDataCenterActiveMediaProcessorsCache.invalidate(newClaComponent.getCustomerDataCenter().getId());
            }
            return convertClaComponent(newClaComponent);
        } catch (IllegalArgumentException e) {
            logger.error("Failed to create sys component {}", e.getMessage());
            throw new BadRequestException(messageHandler.getMessage("system-component.create.failure",
                    Lists.newArrayList(e.getMessage())), BadRequestType.OPERATION_FAILED);
        }
    }

    private void updateSysComponentDataCenter(ClaComponentDto claComponentDto,ClaComponent claComponentToUpdate) {
        if (claComponentDto.getClaComponentType().equals(ClaComponentType.MEDIA_PROCESSOR)) {
            CustomerDataCenterDto requestedDataCenter = claComponentDto.getCustomerDataCenterDto();
            if (requestedDataCenter == null) {
                //set default data center when no data center is specified
                CustomerDataCenter defaultDataCenter = customerDataCenterService.getBootstrapCustomerDataCenter();
                claComponentToUpdate.setCustomerDataCenter(defaultDataCenter);
            } else {
                CustomerDataCenter one = customerDataCenterService.getCustomerDataCenterById(claComponentDto.getCustomerDataCenterDto().getId());
                claComponentToUpdate.setCustomerDataCenter(one);
            }
        }
    }


    @Transactional
    public ClaComponentDto updateSysComponent(ClaComponentDto claComponentDto) {
        logger.debug("Update sys component to {}", claComponentDto);
        ClaComponent one = claComponentRepository.getOne(claComponentDto.getId());
        isValidForSysComponentUpdate(one.getInstanceId(), claComponentDto.getInstanceId(),claComponentDto.getClaComponentType(),one.getState());

        try {
            Boolean prevActive = one.getActive();
            one.setActive(claComponentDto.getActive());
            if(ComponentState.PENDING_INIT.equals(one.getState())) {
                one.setInstanceId(claComponentDto.getInstanceId());
            }
            one.setComponentType(claComponentDto.getComponentType());
            //     one.setExternalNetAddress(claComponentDto.getExternalNetAddress());
            //    one.setLocalNetAddress(claComponentDto.getLocalNetAddress());
            one.setLocation(claComponentDto.getLocation());
            long prevCustomerDataCenterId=0;

            boolean isMediaProcessor = ClaComponentType.MEDIA_PROCESSOR.equals(claComponentDto.getClaComponentType());
            if (isMediaProcessor) {
                if (one.getCustomerDataCenter() != null) {
                    prevCustomerDataCenterId = one.getCustomerDataCenter().getId();
                }
                updateSysComponentDataCenter(claComponentDto, one); //broadcast change listeners
            }

            ClaComponent updatedClaComponent = claComponentRepository.save(one);

            if(isMediaProcessor) {
                if (updatedClaComponent.getCustomerDataCenter() == null || prevCustomerDataCenterId != updatedClaComponent.getCustomerDataCenter().getId() ||
                        updatedClaComponent.getActive() != prevActive ) { //need to remove un-active components from cache and vise versa
                    if (prevCustomerDataCenterId != 0) {
                        customerDataCenterActiveMediaProcessorsCache.invalidate(prevCustomerDataCenterId);
                    }
                    if (updatedClaComponent.getCustomerDataCenter() != null) {
                        customerDataCenterActiveMediaProcessorsCache.invalidate(updatedClaComponent.getCustomerDataCenter().getId());
                    }
                    if(!ComponentState.PENDING_INIT.equals(one.getState())) {
                        logger.info("New CustomerDataCenter for Existing Media Processor or Active state changed. Need to redirect media processor for new listening endpoint {}", updatedClaComponent);
                        remoteMediaProcessingService.updateMediaProcessorCommunicationChannel(updatedClaComponent, updatedClaComponent.getCustomerDataCenter());
                    }
                }
            }
            return convertClaComponent(updatedClaComponent);

        } catch (RuntimeException e) {
            logger.error("Exception while trying to update sys component {}", e);
            throw e;
        }
    }

    @Transactional
    public ClaComponentDto updateSysComponentBySystem(ClaComponentDto claComponentDto) {
        //TODO  what can be updated by the system without overriding existing user updates
        logger.trace("Update sys component by sys  {}", claComponentDto);
        boolean stateChange = false;

        ClaComponent one = claComponentRepository.getOne(claComponentDto.getId());
        if (claComponentDto.getActive() != null) {
            one.setActive(claComponentDto.getActive());
        }
        if (!Strings.isNullOrEmpty(claComponentDto.getComponentType())) {
            one.setComponentType(claComponentDto.getComponentType());
        }
        if (!Strings.isNullOrEmpty(claComponentDto.getExternalNetAddress())) {
            one.setExternalNetAddress(claComponentDto.getExternalNetAddress());
        }
        if (!Strings.isNullOrEmpty(claComponentDto.getLocalNetAddress())) {
            one.setLocalNetAddress(claComponentDto.getLocalNetAddress());
        }
        if (!Strings.isNullOrEmpty(claComponentDto.getLocation())) {
            one.setLocation(claComponentDto.getLocation());
        }

        if (!claComponentDto.getState().equals(one.getState())) {
            stateChange = true;
        }
        one.setState(claComponentDto.getState());
        if (claComponentDto.getLastResponseDate() != null) {
            one.setLastResponseDate(claComponentDto.getLastResponseDate());
        }
        if (claComponentDto.getStateChangeDate() != null) {
            one.setStateChangeDate(claComponentDto.getStateChangeDate());
        }
        if (claComponentDto.getLastStatusId() != null) {
            one.setLastStatusId(claComponentDto.getLastStatusId());
        }

        try {
            ClaComponent updatedClaComponent = claComponentRepository.save(one);

            if ((ComponentState.PENDING_INIT.equals(one.getState()) && ComponentState.PENDING_INIT.equals(updatedClaComponent.getState())) || //pending init are not in cache, need to update
                    (stateChange && ClaComponentType.MEDIA_PROCESSOR.equals(one.getClaComponentType()))) {
                customerDataCenterActiveMediaProcessorsCache.invalidate(one.getCustomerDataCenter().getId());
                if (one.getCustomerDataCenter().getId() != updatedClaComponent.getCustomerDataCenter().getId()) {
                    customerDataCenterActiveMediaProcessorsCache.invalidate(updatedClaComponent.getCustomerDataCenter().getId());
                }
            }
            return convertClaComponent(updatedClaComponent);
        } catch (RuntimeException e) {
            logger.error("Exception while trying to update sys component " + claComponentDto.getId() + " details: " + e.getMessage(), e);
            throw e;
        }
    }

    @Transactional
    public ClaComponentDto deleteSysComponent(long id) {
        ClaComponent one = claComponentRepository.findById(id).orElse(null);
        if (one != null && one.getClaComponentType().equals(ClaComponentType.MEDIA_PROCESSOR) &&
                ((one.getState()!=ComponentState.STARTING &&  one.getState()!=ComponentState.OK ) || !one.getActive()) ) {
            claComponentRepository.deleteById(id);
            InstanceCacheKey instanceCacheKey = new InstanceCacheKey(one.getInstanceId(), one.getClaComponentType());
            instanceIdCache.invalidate(instanceCacheKey);
            if(one.getClaComponentType().equals(ClaComponentType.MEDIA_PROCESSOR)) {
                customerDataCenterActiveMediaProcessorsCache.invalidate(one.getCustomerDataCenter().getId());
            }
            return convertClaComponent(one);
        }
        return null;
    }


    public void updateComponentStatusFromDto(ClaComponentStatusDto claComponentStatusDto, ClaComponentStatus claComponentStatus) {
        claComponentStatus.setClaComponentId(claComponentStatusDto.getClaComponentId());

        claComponentStatus.setComponentState(claComponentStatusDto.getComponentState());
        claComponentStatus.setComponentType(claComponentStatusDto.getComponentType());

        claComponentStatus.setTimestamp(claComponentStatusDto.getTimestamp());
        claComponentStatus.setExtraMetrics(claComponentStatusDto.getExtraMetrics());
    }

    @Transactional(readOnly = true)
    public List<ClaComponentDto> findExpiredComponents(ClaComponentType componentType,
                                                       long minLastResponseTime) {
        List<ClaComponent> claComponents = claComponentRepository.findActiveComponentsNotResponsiveSince(componentType, minLastResponseTime);
        return convertClaComponents(claComponents);
    }

    @Transactional
    public ClaComponentDto findComponentByInstanceId(ClaComponentType componentType, String instanceId) {
        InstanceCacheKey key = new InstanceCacheKey(instanceId,componentType);
        Optional<Long> id = instanceIdCache.getUnchecked(key);
        if (id.isPresent()) {
            ClaComponent claComponent = claComponentRepository.getOne(id.get());
            return convertClaComponent(claComponent);
        } else {
            instanceIdCache.invalidate(key); // to make sure we retry to get from db
            return null;
        }
    }

    @Transactional
    public ClaComponentDto findComponentById(Long id) {
        ClaComponent claComponent = claComponentRepository.findById(id).orElse(null);
        return claComponent == null ? null : convertClaComponent(claComponent);
    }

    @Transactional
    public void updateComponentByInstanceId(ClaComponentType componentType, String instanceId, ComponentState state) {
        InstanceCacheKey key = new InstanceCacheKey(instanceId,componentType);
        Optional<Long> idOp = instanceIdCache.getUnchecked(key);
        idOp.ifPresent(id -> updateComponentState(id, state, null, false));
    }

    public synchronized void saveDriveData(Long id, String driveData) {
        latestDriveDataForComponent.put(id, driveData);
    }

    public boolean isComponentOutOfMemory(ClaComponentType type) {
        long fromTimestamp = timeSource.timeSecondsAgo(60 * timeForMemoryCheckMin);

        // get records for all components of the wanted type
        List<Object[]> records = claComponentRepository.findDriveDataForTypeAndTime(type.ordinal(), fromTimestamp);
        Map<Long, Long> faulty = new HashMap<>();
        Map<Long, Long> ok = new HashMap<>();

        // save per component latest ok/faulty records
        for (Object[] record : records) {
            Long id = ((BigInteger)record[0]).longValue();
            String driveData = (String)record[1];
            Long timestamp = ((BigInteger)record[2]).longValue();
            try {
                DriveDto driveDto = ModelDtoConversionUtils.getMapper().readValue(driveData, DriveDto.class);
                Map<DriveDto.DriveCapacityMetrics, Integer> lowSpaceDrives = calculateLowSpaceDrives(driveDto.getDriveCapacities());
                if (lowSpaceDrives.size() > 0) {
                    logger.trace("low disk space for type {} record {}", type, driveData);
                    setIfNewer(id, timestamp, faulty);
                } else {
                    logger.trace("ok disk space for type {} record {}", type, driveData);
                    setIfNewer(id, timestamp, ok);
                }
            } catch (Exception e) {
                logger.warn("cannot translate drive data to object {} error {}", driveData, e.getMessage());
            }
        }

        // if there is any faulty record with no later ok record for any component - report out of memory
        for (Map.Entry<Long, Long> comp : faulty.entrySet()) {
            if (!ok.containsKey(comp.getKey()) || ok.get(comp.getKey()) < comp.getValue()) {
                logger.trace("out of memory type {} ok records {} faulty records {}", type, ok, faulty);
                logger.debug("component type {} report out of memory", type);
                return true;
            }
        }

        return false;
    }

    private void setIfNewer(Long id, Long timestamp, Map<Long, Long> records) {
        Long currentTimestamp = records.get(id);
        if (currentTimestamp == null || currentTimestamp < timestamp) {
            records.put(id, timestamp);
        }
    }

    @Transactional
    public void updateDriveDataAndState(Long id, ComponentState state, String driveData) {
        ClaComponent one = claComponentRepository.getOne(id);

        saveDriveData(id, driveData);

        if (state != null) {
            one.setState(state);
        }

        if (one.getLastStatusId() != null && one.getLastStatusId() > 0) {
            List<ClaComponentStatus> stat = claComponentStatusRepository.findByIdAndComponent(id, one.getLastStatusId());
            if (stat != null && stat.size() == 1) {
                ClaComponentStatus record = stat.get(0);
                if (state != null) {
                    record.setComponentState(state);
                }
                record.setDriveData(driveData);
            } else {
                logger.warn("cant update drive state for component {} cant find status record {}", id, one.getLastStatusId());
            }
        } else {
            logger.warn("cant update drive state for component {} no existing status record", id);
        }
    }

    @Transactional
    public ClaComponentDto updateComponentState(Long id , ComponentState state, Long lastStatusId, boolean isAfterRestart) {
        ClaComponent one = claComponentRepository.getOne(id);
        ClaComponentDto claComponentDto = convertClaComponent(one);

        if (lastStatusId != null && lastStatusId > 0) {
            claComponentDto.setLastStatusId(lastStatusId);
        }

        boolean shouldUpdate = false;

        if (state == ComponentState.OK || state == ComponentState.STARTING) {
            claComponentDto.setLastResponseDate(timeSource.currentTimeMillis());
            shouldUpdate = true;
        }

        if (state != one.getState()) {
            claComponentDto.setState(state);
            claComponentDto.setStateChangeDate(timeSource.currentTimeMillis());
            shouldUpdate = true;
        } else if (isAfterRestart) {
            claComponentDto.setStateChangeDate(timeSource.currentTimeMillis());
            shouldUpdate = true;
        }

        if (shouldUpdate) {
            return updateSysComponentBySystem(claComponentDto);
        } else {
            return claComponentDto;
        }
    }

    @Transactional
    public void createBasicComponent(String url, ClaComponentType type, String instanceId) {
        createBasicComponent(url, type, instanceId, null);
    }

    @Transactional
    public void createBasicComponent(String url, ClaComponentType type, String instanceId, String localNetAddress) {
        ClaComponentDto component = new ClaComponentDto();
        component.setInstanceId(instanceId);
        component.setState(ComponentState.UNKNOWN);
        component.setActive(true);
        component.setClaComponentType(type);
        component.setComponentType(type.name());
        component.setExternalNetAddress(url);
        component.setLocalNetAddress(localNetAddress);
        createSysComponent(component);
    }

    @Transactional
    public void addSysComponentStatusAndUpdateComponent(Long claComponentId, ClaComponentStatusDto componentStatusDto) {
        addSysComponentStatusAndUpdateComponent(claComponentId, componentStatusDto, false);
    }

    @Transactional
    public void addSysComponentStatusAndUpdateComponent(Long claComponentId, ClaComponentStatusDto componentStatusDto, boolean isAfterRestart) {
        if (claComponentId == null) {
            logger.warn("Failed to add component status, Could not find component. {} {}",componentStatusDto);
        }

        ClaComponentStatus componentStatus = new ClaComponentStatus();
        componentStatusDto.setClaComponentId(claComponentId);
        updateComponentStatusFromDto(componentStatusDto, componentStatus);

        if (componentStatus.getDriveData() == null) {
            componentStatus.setDriveData(latestDriveDataForComponent.get(componentStatus.getClaComponentId()));
        }

        ClaComponentStatus newComponentStatus = claComponentStatusRepository.save(componentStatus);
        ClaComponentDto save = updateComponentState(claComponentId, componentStatusDto.getComponentState(), newComponentStatus.getId(), isAfterRestart);
        synchronized (this) {
            lastDBsave = System.currentTimeMillis();
        }
        logger.trace("Component status added: {} for {} ", newComponentStatus, save);
    }

    public static class InstanceCacheKey {
        private String instanceId;
        private ClaComponentType type;

        InstanceCacheKey(String instanceId, ClaComponentType type) {
            this.instanceId = instanceId;
            this.type = type;
        }

        public String getInstanceId() {
            return instanceId;
        }

        public ClaComponentType getType() {
            return type;
        }

        public void setType(ClaComponentType type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "InstanceCacheKey{" +
                    "instanceId='" + instanceId + '\'' +
                    ", type=" + type +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            InstanceCacheKey that = (InstanceCacheKey) o;

            return (instanceId != null ? instanceId.equals(that.instanceId) : that.instanceId == null)
                    && type == that.type;
        }

        @Override
        public int hashCode() {
            int result = instanceId != null ? instanceId.hashCode() : 0;
            result = 31 * result + (type != null ? type.hashCode() : 0);
            return result;
        }
    }

    public static ClaComponentDto convertClaComponent(ClaComponent claComponent) {
        ClaComponentDto claComponentDto = new ClaComponentDto();
        claComponentDto.setId(claComponent.getId());
        claComponentDto.setLocation(claComponent.getLocation());
        claComponentDto.setComponentType(claComponent.getComponentType());
        claComponentDto.setState(claComponent.getState());
        claComponentDto.setActive(claComponent.getActive());
        claComponentDto.setInstanceId(claComponent.getInstanceId());
        claComponentDto.setClaComponentType(claComponent.getClaComponentType());
        claComponentDto.setCreatedOnDB(claComponent.getCreatedOnDB());
        claComponentDto.setExternalNetAddress(claComponent.getExternalNetAddress());
        claComponentDto.setLocalNetAddress(claComponent.getLocalNetAddress());
        claComponentDto.setLastResponseDate(claComponent.getLastResponseDate());
        claComponentDto.setStateChangeDate(claComponent.getStateChangeDate());
        claComponentDto.setCustomerDataCenterDto(CustomerDataCenterService.convertCustomerDataCenterToDto(claComponent.getCustomerDataCenter()));
        if (claComponentDto.getStateChangeDate() != null) {
            claComponentDto.setStateChangeDate(claComponent.getStateChangeDate());
        }

        return claComponentDto;
    }


    @SuppressWarnings("unused") // TODO Itai: Can we remove this unused method
    public static Page<ClaComponentStatusDto> convertClaComponentsStatus(Page<ClaComponentStatus> claComponentsStatusList,
                                                                         PageRequest pageRequest) {
        return convertClaComponentsStatus( claComponentsStatusList, null, pageRequest);
    }

    public static Page<ClaComponentStatusDto> convertClaComponentsStatus(Page<ClaComponentStatus> claComponentsStatusList,
                                                                         Long dcId, PageRequest pageRequest) {
        List<ClaComponentStatusDto> content = new ArrayList<>();
        for (ClaComponentStatus claComponentStatus : claComponentsStatusList.getContent()) {
            content.add(convertClaComponentStatus(claComponentStatus, dcId));
        }

        return new PageImpl<>(content, pageRequest, claComponentsStatusList.getTotalElements());
    }

    public static ClaComponentStatusDto convertClaComponentStatus(ClaComponentStatus claComponentStatus) {
        return convertClaComponentStatus(claComponentStatus, null);
    }

    public static String fixExtraMetricsForBroker(String extraMetrics, Long dcId) {
        if (extraMetrics == null) return null;
        for (QueueProperty q : QueueProperty.values()) {
            extraMetrics = extraMetrics.replace("\"" + q.getInitials() + "\"", "\"" + q.name() + "\"");
        }
        if (dcId != null) {
            try {
                Map<String, Map<String, Object>> data = ModelDtoConversionUtils.getMapper().readValue(extraMetrics, Map.class);
                Map<String, Object> result = new HashMap<>();
                for (Map.Entry<String, Map<String, Object>> entry : data.entrySet()) {
                    String dcInEntryStr = (String)entry.getValue().get(QueueProperty.DataCenterId.name());
                    Long dcInEntry = Strings.isNullOrEmpty(dcInEntryStr) ? null : Long.parseLong(dcInEntryStr);
                    if (dcId == 0 && dcInEntry == null) {
                        result.put(entry.getKey(), entry.getValue());
                    } else if (dcId > 0 && dcId.equals(dcInEntry)) {
                        result.put(entry.getKey(), entry.getValue());
                    }
                }
                extraMetrics = ModelDtoConversionUtils.getMapper().writeValueAsString(result);
            } catch (Exception e) {
                logger.info("problem creating object from extra metrics",e);
            }
        }
        return extraMetrics;
    }

    private static ClaComponentStatusDto convertClaComponentStatus(ClaComponentStatus claComponentStatus, Long dcId) {
        ClaComponentStatusDto claComponentStatusDto = new ClaComponentStatusDto();
        claComponentStatusDto.setId(claComponentStatus.getId());
        claComponentStatusDto.setComponentState(claComponentStatus.getComponentState());
        claComponentStatusDto.setClaComponentId(claComponentStatus.getClaComponentId());
        claComponentStatusDto.setComponentType(claComponentStatus.getComponentType());
        claComponentStatusDto.setTimestamp(claComponentStatus.getTimestamp());
        claComponentStatusDto.setDriveData(claComponentStatus.getDriveData());

        ClaComponentType type = ClaComponentType.valueOf(claComponentStatus.getComponentType());
        if (type.equals(ClaComponentType.BROKER) || type.equals(ClaComponentType.BROKER_DC)) {
            String extraMetrics = fixExtraMetricsForBroker(claComponentStatus.getExtraMetrics(), dcId);
            claComponentStatusDto.setExtraMetrics(extraMetrics);
        } else {
            claComponentStatusDto.setExtraMetrics(claComponentStatus.getExtraMetrics());
        }
        return claComponentStatusDto;
    }

    public static Page<ClaComponentDto> convertClaComponentsPage(Page<ClaComponent> claComponents, PageRequest pageRequest) {
        List<ClaComponentDto> content = convertClaComponents(claComponents.getContent());

        return new PageImpl<>(content, pageRequest, claComponents.getTotalElements());
    }

    public static Page<ClaComponentDto> convertClaComponents(Page<ClaComponent> claComponents, PageRequest pageRequest) {

        List<ClaComponentDto> content = new ArrayList<>();
        for (ClaComponent claComponent : claComponents) {
            content.add(convertClaComponent(claComponent));
        }

        return new PageImpl<>(content, pageRequest, claComponents.getTotalElements());
    }

    public static List<ClaComponentDto> convertClaComponents(List<ClaComponent> claComponents) {
        List<ClaComponentDto> content = new ArrayList<>();
        for (ClaComponent claComponent : claComponents) {
            content.add(convertClaComponent(claComponent));
        }
        return content;
    }

    /**
     * returns the folder/size pairs for all folders whose size is larger than threshold.
     * NOTE:If the threshold is negative or 0, the result always empty.
     * @return pairs of folder path and it`s size
     */
    public Map<String,Integer> calculateOversizedFolders(Map<String, Integer> measuredFolders) {
        HashMap<String, Integer> oversizesFolders = new HashMap<>();
        if(folderMaxSizeThreshold <= 0) return oversizesFolders;
        measuredFolders.forEach((k,v)-> { if(v >= folderMaxSizeThreshold) oversizesFolders.put(k, v); });

        return oversizesFolders;
    }

    /**
     * Calculates the percentage free space of each drive (root of all given folders)
     * @return pairs of drive and it`s free space percent below the threshold
     */
    public Map<DriveDto.DriveCapacityMetrics, Integer> calculateLowSpaceDrives(List<DriveDto.DriveCapacityMetrics> driveCapacityMetricsList){
        if(freeSpaceThreshold <= 0) return new HashMap<>();

        Map<DriveDto.DriveCapacityMetrics, Integer> driveHighUsagePercentiles = driveCapacityMetricsList.stream()
                .map(drive -> Pair.of(drive, 100 * drive.getFreeSpace() / drive.getTotalSpace()))
                .filter(pair -> pair.getValue() <= freeSpaceThreshold)
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        return driveHighUsagePercentiles;
    }

    public boolean isSolrWorking() {
        Boolean isSolrUp;

        List<ClaComponent> components = getByComponentType(ClaComponentType.SOLR_CLAFILE);
        if (components == null || components.isEmpty()) {
            logger.debug("cant find SOLR_CLAFILE component in db");
            return false;
        } else {
            ComponentState currentState = components.get(0).getState();
            isSolrUp = currentState != null && currentState.equals(ComponentState.OK);
        }

        if (isSolrUp) {
            components = getByComponentType(ClaComponentType.SOLR_SIMILARITY);
            if (components == null || components.isEmpty()) {
                logger.debug("cant find SOLR_SIMILARITY component in db ");
                return false;
            } else {
                ComponentState currentState = components.get(0).getState();
                isSolrUp = currentState != null && currentState.equals(ComponentState.OK);
            }
        } else {
            logger.debug("SOLR_CLAFILE component in db not ok");
        }

        if (isSolrUp) {
            if (isComponentOutOfMemory(ClaComponentType.SOLR_NODE)) {
                logger.debug("SOLR_NODE out of memory");
                return false;
            }
        } else {
            logger.debug("SOLR_SIMILARITY component in db not ok");
        }

        return isSolrUp;
    }
}
