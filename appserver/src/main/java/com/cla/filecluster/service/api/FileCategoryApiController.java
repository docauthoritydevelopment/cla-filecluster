package com.cla.filecluster.service.api;

import com.cla.common.constants.ExtractionFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.category.FileCategoryDto;
import com.cla.common.domain.dto.category.MergeCategoryRequestDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.FileDTOSearchResult;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.repository.solr.query.SolrQueryGenerator;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCategoryApplicationService;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.extractor.entity.ContentSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/categories")
public class FileCategoryApiController {

    @Autowired
    private ContentSearchService contentSearchService;

	@Autowired
	private ExtractTokenizer extractTokenizer;

    @Autowired
    private FileCategoryApplicationService categoryApplicationService;

	@Autowired
	private MessageHandler messageHandler;

	//http://localhost:8080/cat/search
	//http://localhost:8080/cat/search?queryText=yanai&facets=groupId,type&pivotFacetField=groupId,type
	//http://localhost:8080/cat/search?queryText=yanai&facets=groupId,type&pivotFacetField=groupId,type&filters=groupId:64682175-a747-45b9-a869-cc6498ee7dab
	// -- facet only:
	//http://localhost:8080/cat/search?queryText=yanai&facets=groupId,type&rows=0
	// -- Pivotal facet only:
	//http://localhost:8080/cat/search?queryText=yanai&pivotFacetField=groupId,type&rows=0
	// -- search only with paging (first mark is empty or *, successive calls should contain the returned value of nextCursorMark.
	//http://localhost:8080/cat/search?queryText=yanai&rows=0&cursorMark=
	@RequestMapping("/search")
	public FileDTOSearchResult search(@RequestParam(required=false) final String queryText,
			@RequestParam(required=false) final String facets,
			@RequestParam(required=false) final String pivotFacetField,
			@RequestParam(required=false) final String filters,
			@RequestParam(required=false) final Integer rows,
			@RequestParam(required=false) final String cursorMark){
		String[] filtersArray=null;
		if(filters!=null) {
			filtersArray = filters.split(",");
		}
		
		// TODO: implement support for all search/filter types 
		String query;
		if(queryText==null) {
			query = SolrQueryGenerator.ALL_QUERY;
		} else {
			query = ExtractionFieldType.CONTENT.filter(extractTokenizer.getHash(queryText));
		}		
		
		final String[] facetArray = (facets==null)?new String[]{}: facets.split(",");
		
		final FileDTOSearchResult fileDTOSearchResult = contentSearchService.findInSolrExtractionCore(query, facetArray,pivotFacetField, filtersArray, rows, cursorMark);
		return fileDTOSearchResult;
	}

    @RequestMapping(value = "/account/all",method = RequestMethod.GET)
    public List<FileCategoryDto> listAllAccountCategories(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return categoryApplicationService.listAllAccountCategories(dataSourceRequest);
    }

    @RequestMapping(value = "/account",method = RequestMethod.GET)
    public Page<FileCategoryDto> listAccountCategories(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return categoryApplicationService.listAccountCategories(dataSourceRequest);
    }

    @RequestMapping(value = "/{categoryId}/files",method = RequestMethod.GET)
    public Page<ClaFileDto> listFileCategoryFiles(@PathVariable long categoryId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return categoryApplicationService.listCategoryFiles(categoryId, dataSourceRequest);
    }

    @RequestMapping(value = "/merge",method = RequestMethod.POST)
    public void mergeCategories(@RequestBody final MergeCategoryRequestDto mergeCategoryRequestDto) {
        categoryApplicationService.mergeCategories(mergeCategoryRequestDto);
    }

	@RequestMapping(value = "/facet",method = RequestMethod.GET)
	public FacetPage<AggregationCountItemDTO<String>> listFileFacetsByField(@RequestParam final Map params) {
		DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
		String facetField = (String) params.get("facetField");
		if (facetField == null) {
			throw new BadRequestException(messageHandler.getMessage("file-category.facet-field.empty"), BadRequestType.MISSING_FIELD);
        }
        return categoryApplicationService.listFileFacetsByField(dataSourceRequest,facetField);
    }
}
