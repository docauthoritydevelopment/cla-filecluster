package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.filecluster.domain.entity.security.SystemSettings;
import com.cla.filecluster.repository.jpa.security.SystemSettingsRepository;
import org.apache.commons.collections.CollectionUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SystemSettingsService {

    private static final Logger logger = LoggerFactory.getLogger(SystemSettingsService.class);

    @Autowired
    private SystemSettingsRepository systemSettingsRepository;

    @Autowired
    private SystemSettingsDefaultProperties confSystemSettings;

    @Transactional(readOnly = true)
    public SystemSettingsDto getSystemSettings(Long id) {
        return systemSettingsRepository.findById(id)
                .map(this::getDtoForObject)
                .orElse(null);
    }

    @Transactional(readOnly = true)
    public List<SystemSettingsDto> getSystemSettings(String key) {
        List<SystemSettingsDto> settingsByName = getDtos(systemSettingsRepository.findByName(key));
        if (CollectionUtils.isEmpty(settingsByName)) {
            logger.info("couldn't find system settings by name: {} " +
                    "from repository, searching in application properties.", key);
            settingsByName = getDefaultSetting(key);
        }
        return settingsByName;
    }

    @Transactional(readOnly = true)
    public List<SystemSettingsDto> getSystemSettingsByPrefix(String prefix) {
        List<SystemSettingsDto> settingsByPrefix = getDtos(systemSettingsRepository.findByPrefix(prefix));
        logger.trace("checking for additional default settings for the prefix {}", prefix);
        Set<String> blackList = extractKeys(settingsByPrefix);
        settingsByPrefix.addAll(getDefaultSettingsByPrefix(prefix, blackList));
        return settingsByPrefix;
    }

    @Transactional(readOnly = true)
    public List<SystemSettingsDto> getSystemSettings() {
        List<SystemSettingsDto> settingsByPrefix = getDtos(systemSettingsRepository.findAll());
        logger.info("checking for additional default settings...");
        Set<String> blackList = extractKeys(settingsByPrefix);
        settingsByPrefix.addAll(getDefaultSettings(blackList));
        return settingsByPrefix;
    }

    @Transactional(readOnly = true)
    public SystemSettingsDto findFirstSystemSettings(String key) {
        List<SystemSettingsDto> settings = getSystemSettings(key);
        return (settings != null && settings.size() > 0) ? settings.get(0) : null;
    }

    @Transactional
    public SystemSettingsDto addSystemSettings(SystemSettingsDto dto) {
        SystemSettings settings = getObjectFromDto(dto);
        settings.setCreatedTimeStampMs(System.currentTimeMillis());
        settings.setModifiedTimeStampMs(System.currentTimeMillis());
        return getDtoForObject(systemSettingsRepository.save(settings));
    }

    @Transactional
    public SystemSettingsDto updateSystemSettings(SystemSettingsDto dto) {
        SystemSettings settings = getObjectFromDto(dto);
        settings.setModifiedTimeStampMs(System.currentTimeMillis());
        return getDtoForObject(systemSettingsRepository.save(settings));
    }

    @Transactional
    public void deleteSystemSettings(final Long id) {
        systemSettingsRepository.deleteById(id);
    }

    public SystemSettingsDto addSystemSettings(String name, String value) {
        SystemSettingsDto dto = createSystemSettings(name, value);
        return addSystemSettings(dto);
    }

    @Transactional
    public SystemSettingsDto updateOrCreateSystemSettings(SystemSettingsDto dto) {
        SystemSettings settings = getObjectFromDto(dto);
        systemSettingsRepository.deleteByName(settings.getName());
        settings.setModifiedTimeStampMs(System.currentTimeMillis());
        return getDtoForObject(systemSettingsRepository.save(settings));
    }

    @Transactional
    public SystemSettingsDto updateOrCreateSystemSettings(String name, String value) {
        systemSettingsRepository.deleteByName(name);
        return addSystemSettings(name, value);
    }

    public List<SystemSettingsDto> getDefaultSetting(String key) {
        List<SystemSettingsDto> result = new ArrayList<>();
        String val = confSystemSettings.getDefault().get(key);
        if (val != null) {
            result.add(getBasicDto(key, val));
        }
        return result;
    }

    private List<SystemSettingsDto> getDefaultSettingsByPrefix(String prefix, Set<String> blackList) {
        return confSystemSettings.getDefault().entrySet()
                .stream()
                .filter(entry -> entry.getKey().startsWith(prefix))
                .filter(entry -> !blackList.contains(entry.getKey()))
                .map(entry -> getBasicDto(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    private List<SystemSettingsDto> getDefaultSettings(Set<String> blackList) {
        return confSystemSettings.getDefault().entrySet()
                .stream()
                .filter(entry -> !blackList.contains(entry.getKey()))
                .map(entry -> getBasicDto(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    @NotNull
    private SystemSettingsDto getBasicDto(String key, String value) {
        SystemSettingsDto dto = new SystemSettingsDto();
        dto.setName(key);
        dto.setValue(value);
        return dto;
    }

    private Set<String> extractKeys(Collection<SystemSettingsDto> dtos) {
        return dtos.stream()
                .map(SystemSettingsDto::getName)
                .collect(Collectors.toSet());
    }

    private SystemSettingsDto createSystemSettings(String name, String value) {
        SystemSettingsDto s = new SystemSettingsDto();
        s.setName(name);
        s.setValue(value);
        s.setCreatedTimeStampMs(System.currentTimeMillis());
        s.setModifiedTimeStampMs(System.currentTimeMillis());
        return s;
    }

    private List<SystemSettingsDto> getDtos(List<SystemSettings> settings) {
        if (settings == null) {
            return null;
        }
        List<SystemSettingsDto> dtos = new ArrayList<>();
        for (SystemSettings s : settings) {
            dtos.add(getDtoForObject(s));
        }
        return dtos;
    }

    private SystemSettingsDto getDtoForObject(SystemSettings settings) {
        if (settings == null) {
            return null;
        }
        SystemSettingsDto dto = new SystemSettingsDto();
        dto.setId(settings.getId());
        dto.setName(settings.getName());
        dto.setValue(settings.getValue());
        dto.setCreatedTimeStampMs(settings.getCreatedTimeStampMs());
        dto.setModifiedTimeStampMs(settings.getModifiedTimeStampMs());
        return dto;
    }

    private SystemSettings getObjectFromDto(SystemSettingsDto dto) {
        if (dto == null) {
            return null;
        }
        SystemSettings settings = new SystemSettings();
        settings.setId(dto.getId());
        settings.setName(dto.getName());
        settings.setValue(dto.getValue());
        settings.setCreatedTimeStampMs(dto.getCreatedTimeStampMs());
        settings.setModifiedTimeStampMs(dto.getModifiedTimeStampMs());
        return settings;
    }

}
