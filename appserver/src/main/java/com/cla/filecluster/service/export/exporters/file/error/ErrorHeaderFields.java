package com.cla.filecluster.service.export.exporters.file.error;

public interface ErrorHeaderFields {
    String NAME = "Name";
    String TYPE = "Type";
    String ROOT_FOLDER = "Root Folder";
    String FOLDER = "Folder";
    String TEXT = "Text";
    String DETAILS = "Details";
    String RUN_ID = "RunId";
    String DATE = "Date";
    String FILE_TYPE = "File Type";
    String MEDIA_TYPE = "Media Type";
    String FILE_EXTENSION = "File Extension";
}
