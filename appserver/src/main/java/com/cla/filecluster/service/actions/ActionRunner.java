package com.cla.filecluster.service.actions;

import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.domain.entity.actions.ActionSchedule;
import com.cla.filecluster.repository.jpa.actions.ActionScheduleRepository;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Properties;

/**
 * Execute scheduled actions according to their schedule
 *
 * Created by: yael
 * Created on: 3/5/2018
 */
@Component
@Scope("prototype")
public class ActionRunner implements ActionRunnerInterface {

    private static final Logger logger = LoggerFactory.getLogger(ActionRunner.class);

    public static final String ACTION_RESULT_PATH = "ActionResultPath";
    public static final String DIFFERENTIAL_SCRIPT_FROM_TIME = "DifferentialScriptFromTime";

    @Autowired
    private ActionScheduleRepository actionScheduleRepository;

    @Autowired
    private ActionExecutionStatusService actionExecutionStatusService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private ActionsAppService actionsAppService;

    @Autowired
    private ActionExecutorService actionExecutorService;

    @Value("${action.differential-script-active:true}")
    private Boolean differentialScriptActive;

    private Long actionScheduleId;

    @Override
    @AutoAuthenticate
    public void run() {
        try {
            DBTemplateUtils.doInTransaction(() -> {
                ActionSchedule schedule = actionScheduleRepository.findById(actionScheduleId).orElse(null);
                ActionExecutionTaskStatus task = actionExecutionStatusService.getActionExecutionTaskStatus(schedule.getActionExecutionTaskStatusId());

                if (!Strings.isNullOrEmpty(schedule.getResultPath())) {
                    if (task.getActionParams() == null) {
                        task.setActionParams(new Properties());
                    }
                    task.getActionParams().setProperty(ACTION_RESULT_PATH, schedule.getResultPath());
                }

                if (differentialScriptActive && schedule.getLastExecutionDate() > 0) {
                    task.getActionParams().setProperty(DIFFERENTIAL_SCRIPT_FROM_TIME, String.valueOf(schedule.getLastExecutionDate()));
                }

                ActionsAppService.ActionQueueingResult actionQueueingResult = actionsAppService.queueActionOnFilter(
                        task.getFilterDescriptor(), task.getActionId(), task.getActionParams(), task.getUserId(), false);

                Long actionTriggerId = actionQueueingResult.actionTriggerId;

                if (actionTriggerId != null) {
                    updateScheduleActionLastRun(actionScheduleId, System.currentTimeMillis());
                    actionExecutorService.dispatchAndStartActionExecution(false,
                            task.getFilterDescriptor(), task.getActionParams(), actionQueueingResult, task.getUserId());
                }
            });

        } catch (Exception | Error e) {
            logger.error("Failed to execute schedule action {} ({})", actionScheduleId, e.getMessage(), e);
        }
    }

    public void setActionScheduleId(Long actionScheduleId) {
        this.actionScheduleId = actionScheduleId;
    }

    @Transactional
    private void updateScheduleActionLastRun(Long id, long lastExecutionDate) {
        logger.info("Set ScheduleAction lastExecutionDate {} for ScheduleAction ID {}", lastExecutionDate, id);
        ActionSchedule one = actionScheduleRepository.findById(id).orElse(null);
        if (one == null) {
            logger.error("Could not find ActionSchedule id {}.", id);
            return;
        }
        one.setLastExecutionDate(lastExecutionDate);
        actionScheduleRepository.save(one);
    }
}
