package com.cla.filecluster.service.api.groups;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.NewJoinedGroupDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.FunctionalItemId;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.security.FunctionalItem;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.*;

@RestController
@RequestMapping("/api/group")
public class GroupsApiController {
	private static final Logger logger = LoggerFactory.getLogger(GroupsApiController.class);
	
	@Value("${reportPageSize:20}")		// TODO: allow page size config, either in request or as a user config.
	private int defaultPageSize;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private FileCatAppService fileCatAppService;


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
	@RequestMapping(value="/full")
	public Page<FileGroup> findAllFull(@RequestParam(defaultValue="1") int page, @RequestParam(defaultValue="0") int pageSize) {
		if (page == 0) {
			logger.warn("Zero page value found in 1-origin service set to 1");
			page = 1;
		}
		if (pageSize == 0) {
			pageSize = defaultPageSize;
		}

        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Pair<Long, List<FileGroup>> groupRes = fileGroupService.findAllReportFullPage((int)pageRequest.getOffset(), pageRequest.getPageSize());

		return new PageImpl<>(groupRes.getValue(), pageRequest, groupRes.getKey());
	}

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public Page<GroupDto> listAllGroups(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return groupsApplicationService.listAllGroups(dataSourceRequest);
	}

    @PreAuthorize("isFullyAuthenticated()")
    //TODO: support collection of IDs
    @RequestMapping(value="/list/ids", method = RequestMethod.GET)
    public List<GroupDto> listGroupsByIds(@RequestParam final List<String> groupIds) {
        return groupsApplicationService.listGroupsByIds(groupIds);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
	@RequestMapping(value="/full/{id}")
	public GroupDetailsDto findGroupById(@PathVariable("id") @FunctionalItemId(FunctionalItem.GROUP) String groupId){
        return groupsApplicationService.getGroupFullDetails(groupId);
    }

	@SuppressWarnings("unchecked")
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value="/{id}/rootfolders")
	public Page<AggregationCountItemDTO<RootFolderDto>> listGroupRootFolders(
	        @PathVariable("id") @FunctionalItemId(FunctionalItem.GROUP) String groupId, @RequestParam Map params) {
		DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
		return groupsApplicationService.listGroupRootFolders(groupId, dataSourceRequest);
	}

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
	@RequestMapping(value="/full/file/{id}")
	public FileGroup findGroupByFileId(@PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) long fileId){
		// Null response if not found
        SolrFileEntity file = fileCatService.getFileEntity(fileId);
        String groupId = file == null ? null : file.getUserGroupId();
        if (!Strings.isNullOrEmpty(groupId)) {
            return groupsService.findById(groupId);
        }
        return null;
	}

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={GENERAL_ADMIN})
    @RequestMapping(value="/updateinsolr/{id}")
    public void updateGroupInSolrById(@PathVariable("id") @FunctionalItemId(FunctionalItem.GROUP) String groupId) {
        groupsApplicationService.updateFileGroupParentInSolr(groupId);
    }

    /**
     * List groups with a file count for each group.
     * The Files in the groups can be filtered by various filters.
     * Only groups with files > 0 after filter are shown.
     * File counts are after filters of course...
     * @param params parameters
     * @return FacetPage
     */
    // TODO: SQL filters
    @RequestMapping(value="/filecount")
    public FacetPage<AggregationCountItemDTO<GroupDto>> findGroupsFileCount(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
		AuditType auditType = dataSourceRequest.isExport() ? AuditType.EXPORT : AuditType.QUERY;
		String auditMessage = "Discover groups"  + (dataSourceRequest.isExport() ? " export" : "");
		eventAuditService.audit(auditType, auditMessage, AuditAction.VIEW, GroupDto.class, null, dataSourceRequest);
        return groupsApplicationService.listGroupsFileCountWithFileAndGroupFilter(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
	@RequestMapping(value="/owners/{id}")
	public Page<AggregationCountItemDTO<FileUserDto>> getGroupOwners(
			@PathVariable("id") @FunctionalItemId(FunctionalItem.GROUP) String groupId,
			@RequestParam(defaultValue="0") final int page) {
		
    	PageRequest pageRequest = PageRequest.of(adjustPageNumebr(page), defaultPageSize);
		return fileCatAppService.countFilesByOwnerForGroup(pageRequest, groupId);
	}

    @SuppressWarnings("unchecked")
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS, VIEW_FILES, VIEW_PERMITTED_FILES})
	@RequestMapping(value="/{id}/folders")
    public Page<AggregationCountItemDTO<DocFolderDto>> listDocFoldersInGroup(
    		@PathVariable("id") @FunctionalItemId(FunctionalItem.GROUP) String groupId,
    		@SuppressWarnings("rawtypes") @RequestParam final Map params) {
    	
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        FileGroup fileGroup = groupsService.findById(groupId);
        return fileCatAppService.listDocFoldersInGroup(fileGroup,dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value="/updatename", method = RequestMethod.POST)
    public void updateGroupName(@RequestBody GroupDto groupDto, @RequestParam final Map<String,String> params)  {
        String name = groupDto.getGroupName();
        if (Strings.isNullOrEmpty(name) || name.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("group.name.empty"), BadRequestType.MISSING_FIELD);
        }
        String groupId = groupDto.getId();
        if (Strings.isNullOrEmpty(groupId) || groupId.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("group.id.empty"), BadRequestType.MISSING_FIELD);
        }
        eventAuditService.audit(AuditType.GROUP,"Update group name ", AuditAction.UPDATE, GroupDto.class,
                groupId, Pair.of("name",name));
        logger.info("Update group name. id={} name={}",groupId,name);
        String wait = params.getOrDefault(DataSourceRequest.WAIT,"false");
        groupsApplicationService.updateGroupName(groupId, name, Boolean.valueOf(wait));
    }


    @RequestMapping(value = "/{id}/ingest/dump", method = RequestMethod.GET)
    public void dumpIngestDataForGroup(@PathVariable String id,
    		@RequestParam(defaultValue="0") final int offset,
    		@RequestParam(defaultValue="5000") final int limit,
    		@RequestParam(value="ml", required=false) final Integer maturityFilter,
    		HttpServletResponse response) throws Exception {
    	
        logger.info("Dump Ingest data for group {}",id);
        response.setContentType("application/csv");
        final String headerKey = "Content-Disposition";
        final String headerValue = String.format("attachment; filename=\"ingestDump_%s.%s\"", id, "csv");
        response.setHeader(headerKey, headerValue);
        groupsApplicationService.dumpIngestDataForGroup(id,response.getOutputStream(), offset, limit, maturityFilter);
        response.flushBuffer();
        logger.info("Dump Ingest data for group {} finished",id);
    }

    @RequestMapping(value = "/{id}/ingest/maturityStats", method = RequestMethod.GET)
    public void dumpIngestMaturityStatsForGroup(@PathVariable String id,
    		@RequestParam(defaultValue="0") final int offset,
    		@RequestParam(defaultValue="5000") final int limit,
    		HttpServletResponse response) throws Exception {

		File file = new File("./logs/maturityStats_"+id+".html");
		Writer fWriter = new PrintWriter(file, "utf-8");

        logger.info("Dump Ingest maturity stats for group {}",id);
		response.setContentType("text/html");
		response.setCharacterEncoding("utf-8");
        response.flushBuffer();

        StringBuffer sb = new StringBuffer();
        groupsApplicationService.dumpIngestMaturityStatsForGroup(id, sb, offset, limit);

        Writer writer = response.getWriter();
        writer.write(sb.toString());
        writer.flush();
        fWriter.write(sb.toString());
        fWriter.flush();
        response.flushBuffer();
        logger.info("Dump Ingest data for group {} finished",id);
    }

    @RequestMapping(value = "/joinedGroup", method = RequestMethod.PUT)
    public GroupDetailsDto createJoinedGroup(@RequestBody NewJoinedGroupDto newJoinedGroupDto,
                                             @RequestParam Map<String,String> params) {
        if (newJoinedGroupDto.getChildrenGroupIds() == null) {
            throw new BadParameterException(messageHandler.getMessage("group.children.empty"), BadRequestType.MISSING_FIELD);
        }
        if (Strings.isNullOrEmpty(newJoinedGroupDto.getGroupName())) {
            throw new BadParameterException(messageHandler.getMessage("group.name.empty"), BadRequestType.MISSING_FIELD);
        }
        eventAuditService.audit(AuditType.GROUP,"Create user group ", AuditAction.CREATE, NewJoinedGroupDto.class,newJoinedGroupDto);
        return groupsApplicationService.createJoinedGroup(newJoinedGroupDto);
    }

    @RequestMapping(value = "/joinedGroup/{userGroupId}", method = RequestMethod.DELETE)
    public void deleteJoinedGroup(@PathVariable String userGroupId,@RequestParam Map params) {
        eventAuditService.audit(AuditType.GROUP,"Delete user group ", AuditAction.DELETE, GroupDto.class, userGroupId, params);
        groupsApplicationService.deleteJoinedGroup(userGroupId);
    }

    @RequestMapping(value = "/joinedGroup/{userGroupId}", method = RequestMethod.POST)
    public void updateJoinedGroup(@PathVariable String userGroupId, @RequestBody NewJoinedGroupDto newJoinedGroupDto,@RequestParam Map params) {
        if (newJoinedGroupDto.getChildrenGroupIds() == null) {
            throw new BadParameterException(messageHandler.getMessage("group.children.empty"), BadRequestType.MISSING_FIELD);
        }
        groupsApplicationService.updateJoinedGroup(userGroupId, newJoinedGroupDto);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/joinedGroup/{userGroupId}/addChildren", method = RequestMethod.POST)
    public GroupDetailsDto addGroupsToJoinedGroup(@PathVariable String userGroupId,
                                                  @RequestBody NewJoinedGroupDto newJoinedGroupDto,
                                                  @RequestParam Map params) {
        eventAuditService.audit(AuditType.GROUP,"Add groups to user group ", AuditAction.UPDATE, NewJoinedGroupDto.class,userGroupId, params, Pair.of("userGroupId",userGroupId));
        String wait = (String)params.getOrDefault(DataSourceRequest.WAIT,"false");
        if (!Strings.isNullOrEmpty(newJoinedGroupDto.getGroupName())) {
            groupsApplicationService.updateGroupName(userGroupId, newJoinedGroupDto.getGroupName(), Boolean.valueOf(wait));
        }
        return groupsApplicationService.addGroupsToJoinedGroup(userGroupId, newJoinedGroupDto.getChildrenGroupIds());
    }

    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/joinedGroup/{userGroupId}/removeChild/{childGroupId}", method = RequestMethod.POST)
    public GroupDetailsDto removeGroupFromJoinedGroup(@PathVariable String userGroupId,
                                                      @PathVariable String childGroupId,
                                                      @RequestParam Map params) {
        eventAuditService.audit(AuditType.GROUP,"move group from user group ", AuditAction.DETACH, GroupDto.class,userGroupId, Pair.of("childGroupId",childGroupId));
        return groupsApplicationService.removeGroupFromJoinedGroup(userGroupId, childGroupId);
    }

    //---------------------------------- private methods ---------------------------------------------------------------
    private int adjustPageNumebr(final int page) {
        return (page > 0) ? (page - 1) : page;
    }

}
