package com.cla.filecluster.service.entitylist;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.*;
import com.cla.common.domain.dto.classification.EvaluateBizListInstructions;
import com.cla.common.domain.dto.extraction.ExtractedEntityDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.BizListItemRow;
import com.cla.filecluster.domain.entity.bizlist.BizListSchema;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.extraction.ExtractionRule;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.FilterBuildUtils;
import com.cla.filecluster.domain.specification.SimpleBizListItemSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.cla.filecluster.util.executors.BlockingExecutor;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

import static com.cla.filecluster.service.convertion.FileDtoFieldConverter.DtoFields.bizList;

/**
 * Created by uri on 30/12/2015.
 */
@Service
public class BizListAppService {

    private static final Logger logger = LoggerFactory.getLogger(BizListAppService.class);

    public static final char BIZ_LIST_SEPERATOR = '.';
    private static final int CLASSIFY_PAGE_SIZE = 1000;
    private static final String GROUP_SIZE_MINIMUM = "group.size.minimum";
    private static final String SINGLE_BLI_DOMINANT_PERCENTAGE = "singleBli.dominant.percentage";
    private static final String SINGLE_BLI_OTHERS_PERCENTAGE = "singleBli.others.percentage";
    private static final String SINGLE_BLI_OTHERS_MAX_COUNT = "singleBli.others.maxCount";
    private static final String MULTI_MATCH_BLI_MIN_COUNT = "multiMatch.bli.minCount";
    private static final String MULTI_MATCH_BLI_OTHERS_PERCENTAGE = "multiMatch.bli.others.percentage";

    @Autowired
    private BizListService bizListService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private BlockingExecutor extractionBlockingExecutor;

    @Value("${bizlist.item.type.options:CONSUMERS,CLIENTS}")
    private String bizListItemTypeOptions;

    @Value("${bizlist.default.template:NameOnly}")
    private String defaultBizListTemplate;

    @Value("${bizlist.templates:NameOnly:Common names search,Healthcare:Health care PII search}")
    private String templateList;

    @Value("${datapopulator.predefined.bizlist.name:}")
    private String predefinedBizListName;

    @Value("${datapopulator.predefined.bizlist.type:}")
    private String predefinedBizListType;

    @Value("${datapopulator.predefined.bizlist.template:}")
    private String predefinedBizListTemplate;

    @Value("${datapopulator.predefined.bizlist.source:}")
    private String predefinedBizListSource;

    @Autowired
    private BizListExtractionService bizListExtractionService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${bizlist.evaluate.group.size.minimum:10}")
    private int classifyMinimumGroupSize;

    @Value("${bizlist.evaluate.singleBli.dominant.percentage:0.7}")
    private double classificationSingleBliDominantPercantage;

    @Value("${bizlist.evaluate.singleBli.others.percentage:0.1}")
    private double classificationSingleBliMaxOthersPercentage;

    @Value("${bizlist.evaluate.singleBli.others.maxCount:5}")
    private int classificationSingleBliMaxOthers;

    @Value("${bizlist.evaluate.multiMatch.bli.minCount:5}")
    private int classificationMultiMatchMinBli;

    @Value("${bizlist.evaluate.multiMatch.bli.others.percentage:0.5}")
    private double classificationMultiMatchMinOthersPercentage;

    @Value("${bizlist.evaluate.singleBli.tagType:Single-Business-List-Match}")
    private String singleBliMatch;

    @Value("${bizlist.evaluate.multiMatch.tagType:Multi-Business-List-Match}")
    private String multiBliMatch;

    public EvaluateBizListInstructions createEvaluationBizListInstructions(Map<String, String> params) {
        EvaluateBizListInstructions evaluateBizListInstructions = new EvaluateBizListInstructions();
        evaluateBizListInstructions.setClassifyMinimumGroupSize(classifyMinimumGroupSize);
        evaluateBizListInstructions.setClassifcationSingleBliDominantPercentage(classificationSingleBliDominantPercantage);
        evaluateBizListInstructions.setClassificationSingleBliMaxOthersPercentage(classificationSingleBliMaxOthersPercentage);
        evaluateBizListInstructions.setClassificationSingleBliMaxOthers(classificationSingleBliMaxOthers);
        evaluateBizListInstructions.setClassificationMultiMatchMinBli(classificationMultiMatchMinBli);
        evaluateBizListInstructions.setClassificationMultiMatchMinOthersPercentage(classificationMultiMatchMinOthersPercentage);
        if (params.containsKey(GROUP_SIZE_MINIMUM)) {
            evaluateBizListInstructions.setClassifyMinimumGroupSize(Integer.valueOf(params.get(GROUP_SIZE_MINIMUM)));
        }
        if (params.containsKey(SINGLE_BLI_DOMINANT_PERCENTAGE)) {
            evaluateBizListInstructions.setClassifcationSingleBliDominantPercentage(Double.valueOf(params.get(SINGLE_BLI_DOMINANT_PERCENTAGE)));
        }
        if (params.containsKey(SINGLE_BLI_OTHERS_PERCENTAGE)) {
            evaluateBizListInstructions.setClassificationSingleBliMaxOthersPercentage(Double.valueOf(params.get(SINGLE_BLI_OTHERS_PERCENTAGE)));
        }
        if (params.containsKey(SINGLE_BLI_OTHERS_MAX_COUNT)) {
            evaluateBizListInstructions.setClassificationSingleBliMaxOthers(Integer.valueOf(params.get(SINGLE_BLI_OTHERS_MAX_COUNT)));
        }
        if (params.containsKey(MULTI_MATCH_BLI_MIN_COUNT)) {
            evaluateBizListInstructions.setClassificationMultiMatchMinBli(Integer.valueOf(params.get(MULTI_MATCH_BLI_MIN_COUNT)));
        }
        if (params.containsKey(MULTI_MATCH_BLI_OTHERS_PERCENTAGE)) {
            evaluateBizListInstructions.setClassificationMultiMatchMinOthersPercentage(Double.valueOf(params.get(MULTI_MATCH_BLI_OTHERS_PERCENTAGE)));
        }
        return evaluateBizListInstructions;
    }

    public void evaluateGroupsByExtraction(BizListItemType bizListItemType, EvaluateBizListInstructions evaluateBizListInstructions) {
        logger.info("Classify groups by extraction of biz list type {}", bizListItemType);
        //Find the tag type for classification
        FileTagType singleBliMatchType = fileTaggingAppService.getFileTagType(singleBliMatch);
        FileTagType multiBliMatchType = fileTaggingAppService.getFileTagType(multiBliMatch);
        if (singleBliMatchType == null || multiBliMatchType == null) {
            logger.error("Missing fileTag types Single-Business-List-Match and Multi-Business-List-Match");
            throw new RuntimeException(messageHandler.getMessage("biz-list.tag.missing"));
        }
        evaluateBizListInstructions.setSingleBliMatchTypeId(singleBliMatchType.getId());
        evaluateBizListInstructions.setMultiBliMatchTypeId(multiBliMatchType.getId());
        FileTag multiMatchTag = fileTaggingAppService.createFileTagIfNeeded("Multiple-" + bizListItemType,
                "Multiple " + bizListItemType + " Matches",
                multiBliMatchType.getId(), FileTagAction.SUGGESTION);
        evaluateBizListInstructions.setMultiMatchTagId(multiMatchTag.getId());
        //Find all groups that have files with extractions and are over a minimum size
        int page = 0;
        int groupsInBatch;
        do {
            int minCount = evaluateBizListInstructions.getClassifyMinimumGroupSize();
            List<FacetField.Count> values = findGroupsWithExtractionsToEvaluate(bizListItemType, page, minCount);
            logger.debug("Try to classify {} groups by extraction", values.size());
            values.forEach(v ->
                    extractionBlockingExecutor.execute(
                            () -> evaluateGroupByExtraction(v.getName(), bizListItemType, evaluateBizListInstructions)));
            groupsInBatch = values.size();
            page++;
        }
        while (groupsInBatch == CLASSIFY_PAGE_SIZE);

        logger.info("Block until all groups are classified");
        extractionBlockingExecutor.blockUntilProcessingFinished();
    }

    @Transactional
    public void createDefaultBizList() {
        if (StringUtils.isEmpty(predefinedBizListName)) {
            return;
        }
        BizListItemType bizListItemType = BizListItemType.valueOf(predefinedBizListType);
        BizListDto bizListDto = new BizListDto(predefinedBizListName, bizListItemType);
        bizListDto.setDescription("predefined bizList");
        bizListDto.setTemplate(predefinedBizListTemplate);
        bizListDto.setActive(true);
        BizListDto bizList = createBizList(bizListDto);
        if (StringUtils.isEmpty(predefinedBizListSource)) {
            return;
        }
        try {
            InputStream fileInputStream = new FileInputStream(predefinedBizListSource);
            byte[] rawData = IOUtils.toByteArray(fileInputStream);
            BizListImportResultDto bizListImportResultDto = importCsvFile(bizList.getId(), rawData);
            updateBizListItemsState(bizList.getId(), bizListImportResultDto.getImportId(), null, BizListItemState.ACTIVE);
        } catch (IOException e) {
            logger.error("Failed to load predefined bizList {} from source {}", predefinedBizListName, predefinedBizListSource);
        }
    }

    @Transactional(readOnly = true)
    public List<BizListDto> getAllBizLists() {
        List<BizList> bizLists = bizListService.find(false);
        return convertBizLists(bizLists);
    }

    @Transactional(readOnly = true)
    public List<BizListDto> getBizListsByType(BizListItemType bizListItemType) {
        List<BizList> bizLists = bizListService.findByType(bizListItemType);
        return convertBizLists(bizLists);
    }

    @Transactional(readOnly = true)
    public Page<BizListSummaryInfo> listBizListSummary(DataSourceRequest dataSourceRequest) {
        logger.debug("List all customer data centers summary info ");
        Page<BizListDto> bizListDtos = listBizLists(dataSourceRequest);
        List<BizListSummaryInfo> content = new ArrayList<>();
        for (BizListDto bizListDto : bizListDtos) {
            BizListSummaryInfo newInfo = new BizListSummaryInfo();
            newInfo.setBizListDto(bizListDto);
            long itemsCount = bizListItemService.countBizListItems(bizListDto.getId());
            newInfo.setItemsCount(itemsCount);
            long activeItemsCount = bizListItemService.countActiveBizListItems(bizListDto.getId());
            newInfo.setActiveItemsCount(activeItemsCount);

            if (bizListDto.getExtractionSessionId() > 0) {
                CrawlRun crawlRun = fileCrawlerExecutionDetailsService.getCrawlRun(bizListDto.getExtractionSessionId());
                newInfo.setCrawlRunDetailsDto(FileCrawlerExecutionDetailsService.convertCrawlRun(crawlRun));
            }
            content.add(newInfo);
        }
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<BizListSummaryInfo> summaryInfoList = new PageImpl<>(content, pageRequest, bizListDtos.getTotalElements());
        return summaryInfoList;
    }


    @Transactional(readOnly = true)
    public Page<BizListDto> listBizLists(DataSourceRequest dataSourceRequest) {
        logger.debug("List page of biz lists {}", dataSourceRequest);
        Sort sortOption = new Sort(Sort.Direction.ASC, "id");
        PageRequest pageRequest = dataSourceRequest.createPageRequest(sortOption);
        Page<BizList> all = bizListService.findAll(pageRequest);
        return convertBizLists(all, pageRequest);
    }

    @Transactional(readOnly = true)
    public BizListDto getBizListByName(String bizListName) {
        BizList bizList = bizListService.findByName(bizListName);
        if (bizList == null) {
            return null;
        }
        return convertBizList(bizList);
    }

    @Transactional
    public BizListDto createBizList(BizListDto bizListDto) {
        validateBizListName(bizListDto.getName(), null);

        if (bizListDto.getBizListItemType().equals(BizListItemType.CONSUMERS) && StringUtils.isEmpty(bizListDto.getTemplate())) {
            //Use the default template name
            bizListDto.setTemplate(defaultBizListTemplate);
        }
        BizList bizList = bizListService.createBizList(bizListDto);
        return convertBizList(bizList);
    }

    private void validateBizListName(String name, Long id) {
        if (Strings.isNullOrEmpty(name) || name.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("biz-list.name.empty"), BadRequestType.MISSING_FIELD);
        }

        BizList byName = bizListService.findByName(name);
        if (byName != null && (id == null || byName.getId() != id)) {
            throw new BadParameterException(messageHandler.getMessage("biz-list.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    @Transactional
    public BizListImportResultDto importFromFolderNames(long bizListId, long docFolderId, int relativeDepth) {
        BizList bizList = bizListService.findById(bizListId);
        DocFolder baseFolder = docFolderService.findById(docFolderId);
        List<DocFolder> docFoldersList = docFolderService.listFolders(baseFolder, relativeDepth, relativeDepth);
        Set<DocFolder> docFolders = new HashSet<>(docFoldersList);
        logger.debug("Import bizListItems into bizList {} from {} subfolders at depth {} of folder {}",
                bizList, docFolders.size(), relativeDepth, baseFolder);
        BizListSchema bizListSchema = new BizListSchema();
        long importId = loadDocFoldersToBizList(bizList, docFolders, bizListSchema);
        BizListImportResultDto resultDto = new BizListImportResultDto();
        resultDto.setImportedRowCount(bizListSchema.getBizListItemRows().size());
        resultDto.setDuplicateRowCount(bizListSchema.getDuplicateBizListItemRows().size());
        resultDto.setErrorRowCount(bizListSchema.getErrorBizListItemRows().size());
        resultDto.setResultDescription("OK");
        resultDto.setImportId(importId);
        return resultDto;
    }

    @Transactional
    public BizListImportResultDto importCsvFile(long bizListId, byte[] rawData) {
        BizList bizList = bizListService.findById(bizListId);
        logger.info("import CSV File to bizList {} using template {}", bizList, bizList.getTemplate());
        BizListSchema bizListSchema = bizListService.loadCsvFile(rawData, bizList.getBizListItemType(), bizList.getTemplate());
        long importId = loadSchemaToBizList(bizList, bizListSchema);
        BizListImportResultDto resultDto = new BizListImportResultDto();
        resultDto.setImportedRowCount(bizListSchema.getBizListItemRows().size());
        resultDto.setDuplicateRowCount(bizListSchema.getDuplicateBizListItemRows().size());
        resultDto.setErrorRowCount(bizListSchema.getErrorBizListItemRows().size());
        resultDto.setResultDescription("OK");
        resultDto.setImportId(importId);
        return resultDto;
    }

    @Transactional
    public BizListDto updateBizList(long entityListId, BizListDto bizListDto) {
        validateBizListName(bizListDto.getName(), entityListId);
        BizList bizList = bizListService.updateBizList(entityListId, bizListDto);
        return convertBizList(bizList);
    }

    @Transactional
    public BizListDto updateBizListActive(long entityListId, boolean active) {
        BizList bizList = bizListService.updateBizListActive(entityListId, active);
        return convertBizList(bizList);
    }

    @Transactional
    public void deleteBizList(long entityListId) {
        List<Long> aliasIds = bizListItemService.getAliasByBizListId(entityListId);
        if (aliasIds != null && !aliasIds.isEmpty()) {
            bizListItemService.deleteAliasById(aliasIds);
        }
        if (bizListItemService.countBizListItems(entityListId) > 0) {
            //throw new BadHttpRequestException("cannot delete biz list that has existing items", BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
            bizListItemService.deleteByBizListItems(entityListId);
        }
        bizListService.deleteBizList(entityListId);
    }

    @Transactional
    public SimpleBizListItemDto createBizListItem(long entityListId, SimpleBizListItemDto entityDto) {
        BizList bizList = bizListService.findById(entityListId);
        SimpleBizListItem result;
        switch (bizList.getBizListItemType()) {
            case SIMPLE:
                result = bizListItemService.createSimpleBizListItem(bizList, entityDto, true);
                break;
            case CLIENTS:
                result = bizListItemService.createClientBizListItem(bizList, entityDto);
                break;
            default:
                throw new RuntimeException(messageHandler.getMessage("biz-list-item.type.invalid", Lists.newArrayList(bizList.getBizListItemType())));
        }
        return BizListItemService.convertSimpleBizListItem(result);
    }

    @Transactional(readOnly = true)
    public Page<? extends SimpleBizListItemDto> listBizListItems(long bizListId, DataSourceRequest dataSourceRequest) {
        Page<? extends SimpleBizListItem> result;
        BizList bizList = bizListService.findById(bizListId);
        PageRequest pageRequest = dataSourceRequest.createPageRequest(new Sort(new Sort.Order(Sort.Direction.ASC, "name")));
        switch (bizList.getBizListItemType()) {
            case CLIENTS:
                result = bizListItemService.listClientBizListItems(bizListId, pageRequest, dataSourceRequest);
                break;
            default:
                result = bizListItemService.listSimpleBizListItems(bizListId, pageRequest, dataSourceRequest);
                break;
        }
        logger.debug("list bizListItems for {} got a result of {} items", bizListId, result.getTotalElements());
        return BizListItemService.convertBizListItems(bizList.getBizListItemType(), result, pageRequest);
    }

    @Transactional
    public SimpleBizListItemDto updateBizListItem(long bizListId, String bizListItemId, SimpleBizListItemDto bizListItemDto) {
        BizList bizList = bizListService.findById(bizListId);
        SimpleBizListItem result;
        switch (bizList.getBizListItemType()) {
            default:
                result = bizListItemService.updateSimpleBizListItem(bizListItemId, bizListItemDto);
                break;
        }
        return BizListItemService.convertSimpleBizListItem(result);
    }

    @Transactional
    public void deleteBizListItemsByImportId(long bizListId, long importId) {
        BizList bizList = bizListService.findById(bizListId);
        switch (bizList.getBizListItemType()) {
            case CLIENTS:
                bizListItemService.deleteClientsBizListItemsByImportId(bizList, importId);
                break;
            default:
                bizListItemService.deleteSimpleBizListItemsByImportId(bizList, importId);
                break;
        }
    }

    @Transactional
    public void deleteBizListItem(long bizListId, String itemId) {
        logger.debug("delete BizListItem {}", itemId);

        if (associationsService.bizListAssociationExists(itemId)) {
            throw new BadRequestException(messageHandler.getMessage("biz-list-item.delete.associations"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }

        try {
            BizList bizList = bizListService.findById(bizListId);
            switch (bizList.getBizListItemType()) {
                case CLIENTS:
                    bizListItemService.deleteClientsBizListItem(itemId);
                    break;
                default:
                    bizListItemService.deleteSimpleBizListItem(itemId);
                    break;
            }
        } catch (RuntimeException e) {
            logger.error("Failed to delete bizListItem {}", bizListId, e);
            throw e;
        }
    }

    @Transactional
    public void updateBizListItemsState(long bizListId, Long importId, BizListItemState oldState, BizListItemState newState) {
        FilterDescriptor importIdFilter = null;
        if (importId != null) {
            logger.debug("update bizList items state for items with importId={}", importId);
            importIdFilter = new FilterDescriptor("importId", importId, "eq");
        }
        FilterDescriptor oldStatusFilter = null;
        if (oldState != null) {
            logger.debug("update bizList items state for items with state={}", oldState);
            oldStatusFilter = new FilterDescriptor("state", oldState, "eq");
        }
        FilterDescriptor baseFilter = FilterBuildUtils.createAndFilter(importIdFilter, oldStatusFilter);
        Specification<SimpleBizListItem> specification = new SimpleBizListItemSpecification(bizListId, baseFilter);
        Page<SimpleBizListItem> simpleBizListItems = bizListItemService.getSimpleBizListItems(new PageRequest(0, 10000000), specification);
        List<SimpleBizListItem> content = simpleBizListItems.getContent();
        logger.debug("update bizList {}. Set {} items state to state={}", bizListId, content.size(), newState);
        bizListItemService.updateBizListItemsState(content, newState);
    }

    public List<BizListItemType> getBizListItemTypes() {
        String[] split = StringUtils.split(bizListItemTypeOptions, ",");
        List<BizListItemType> result = new ArrayList<>();
        for (String s : split) {
            BizListItemType bizListItemType = BizListItemType.valueOf(s);
            result.add(bizListItemType);
        }
        return result;
    }

    public void clearPreviousExtraction(long bizListId, Long jobId) {
        jobManagerService.setOpMsg(jobId, "Clear previous extraction for bizList " + bizListId);
        logger.info("Clear previous extraction for bizList: {}", bizListId);
        bizListExtractionService.clearPreviousExtraction(bizListId);
    }

    public void clearBizListExtractionFromCatFileSync(long bizListId, Long jobId) {
        logger.info("Clear CatFile Extraction on bizList {}", bizListId);
        jobManagerService.setOpMsg(jobId, "Clear extraction data from index for bizList " + bizList);
        BizList bizList = bizListService.findById(bizListId);
        int bizListType = bizList.getBizListItemType().ordinal();
        String filter = FileDtoFieldConverter.BIZLIST_SOLR_PREFIX + bizListType + "." + bizList.getId();
        String regex = "blt\\." + bizListType + "\\." + bizList.getId() + "\\s.*";
        fileCatService.removeRegexFromFieldInCatFile(CatFileFieldType.EXTRACTED_ENTITIES_IDS, regex, filter);
    }

    public void clearCatFileBizListItemExtractionFromBizList(long bizListId, String itemId) {
        logger.info("Clear CatFile Extraction on bizList {} from bizList item {}", bizListId, itemId);
        BizList bizList = bizListService.findById(bizListId);
        int bizListType = bizList.getBizListItemType().ordinal();
        String filter = FileDtoFieldConverter.BIZLIST_SOLR_PREFIX + bizListType + "." + bizListId;
        //blt.1.2 bli.1.2.2_Thomas erb.1.aliases.2_Thomas er.aliases
        String regex = ".*bli\\." + bizListType + "\\." + bizList.getId() + "\\." + itemId + "\\s.*";
        fileCatService.removeRegexFromFieldInCatFile(CatFileFieldType.EXTRACTED_ENTITIES_IDS, regex, filter);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsExtractionFileCounts(DataSourceRequest dataSourceRequest, long bizListId) {
        BizList bizList = bizListService.findById(bizListId);
        logger.debug("List BizListItems Extraction File Counts With File Filters");
        SolrFacetQueryResponse solrFacetQueryResponse = getRespItemsExtractionFileCounts(dataSourceRequest, bizList, null);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.EXTRACTED_ENTITIES_IDS, solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getRespItemsExtractionFileCounts(dataSourceRequest, bizList,
                    CatFileFieldType.EXTRACTED_ENTITIES_IDS.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
        }

        return convertFacetResultsToSimpleBizListItems(solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
    }

    private SolrFacetQueryResponse getRespItemsExtractionFileCounts(DataSourceRequest dataSourceRequest, BizList bizList, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.EXTRACTED_ENTITIES_IDS);
        solrFacetSpecification.setMinCount(1);
        String facetPrefix = FileDtoFieldConverter.BIZLIST_ITEM_SOLR_PREFIX
                + bizList.getBizListItemType().ordinal()
                + BIZ_LIST_SEPERATOR + bizList.getId() + BIZ_LIST_SEPERATOR;
        solrFacetSpecification.setFacetPrefix(facetPrefix);
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsExtractionFileCountsByType(DataSourceRequest dataSourceRequest, BizListItemType bizListItemType) {
        logger.debug("List BizListItems Extraction File Counts With File Filters");
        final long totalBizListItemTypeFacet = fileCatAppService.getTotalFacetCountByPrefix(dataSourceRequest,
                CatFileFieldType.EXTRACTED_ENTITIES_IDS,
                SimpleBizListItemDto.ID_PREFIX + bizListItemType.ordinal() + FileCatUtils.CATEGORY_SEPARATOR);
        SolrFacetQueryResponse solrFacetQueryResponse = getRespItemsExtractionFileCountsByType(dataSourceRequest, bizListItemType, null);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.EXTRACTED_ENTITIES_IDS, solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getRespItemsExtractionFileCountsByType(dataSourceRequest, bizListItemType,
                    CatFileFieldType.EXTRACTED_ENTITIES_IDS.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
        }

        FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> facetPage = convertFacetResultsToSimpleBizListItems(
                solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
        facetPage.setTotalElements(totalBizListItemTypeFacet);
        return facetPage;
    }

    private SolrFacetQueryResponse getRespItemsExtractionFileCountsByType(DataSourceRequest dataSourceRequest, BizListItemType bizListItemType, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.EXTRACTED_ENTITIES_IDS);
        solrFacetSpecification.setMinCount(1);
        String facetPrefix = FileDtoFieldConverter.BIZLIST_ITEM_SOLR_PREFIX
                + bizListItemType.ordinal() + BIZ_LIST_SEPERATOR;
        solrFacetSpecification.setFacetPrefix(facetPrefix);
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsExtractionFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("List BizListItems Extraction File Counts With File Filters");
        SolrFacetQueryResponse solrFacetQueryResponse = getRespItemsExtractionFileCounts(dataSourceRequest, null);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.EXTRACTED_ENTITIES_IDS, solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getRespItemsExtractionFileCounts(dataSourceRequest,
                    CatFileFieldType.EXTRACTED_ENTITIES_IDS.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
        }

        return convertFacetResultsToSimpleBizListItems(solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
    }

    private SolrFacetQueryResponse getRespItemsExtractionFileCounts(DataSourceRequest dataSourceRequest, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.EXTRACTED_ENTITIES_IDS);
        solrFacetSpecification.setMinCount(1);
        String facetPrefix = FileDtoFieldConverter.BIZLIST_ITEM_SOLR_PREFIX;
        solrFacetSpecification.setFacetPrefix(facetPrefix);
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsAssociationFileCounts(DataSourceRequest dataSourceRequest, long bizListId) {
        logger.debug("List BizListItems Association File Counts With File Filters");
        BizList bizList = bizListService.findById(bizListId);
        //bli.type.bizListId.bizListItemId
        logger.debug("list BLI File Count With File Filters");
        SolrFacetQueryResponse solrFacetQueryResponse = getRespItemsAssociationFileCounts(dataSourceRequest, bizList, null);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getRespItemsAssociationFileCounts(dataSourceRequest, bizList,
                    CatFileFieldType.BIZLIST_ITEM_ASSOCIATION.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
        }

        return convertFacetResultsToSimpleBizListItems(solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
    }

    private SolrFacetQueryResponse getRespItemsAssociationFileCounts(DataSourceRequest dataSourceRequest, BizList bizList, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix("bli." + bizList.getBizListItemType().ordinal() + "." + bizList.getId() + ".");
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsAssociationFileCounts(DataSourceRequest dataSourceRequest, BizListItemType type) {
        //bli.type.bizListId.bizListItemId
        logger.debug("List BizListItems Association File Counts With File Filters");
        SolrFacetQueryResponse solrFacetQueryResponse = getRespItemsAssociationFileCounts(dataSourceRequest, type, null);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getRespItemsAssociationFileCounts(dataSourceRequest, type,
                    CatFileFieldType.BIZLIST_ITEM_ASSOCIATION.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
        }

        return convertFacetResultsToSimpleBizListItems(solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
    }

    private SolrFacetQueryResponse getRespItemsAssociationFileCounts(DataSourceRequest dataSourceRequest, BizListItemType type, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.filterByBizListType(type);
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    public FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> findBizListItemsAssociationFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("List BizListItems Association File Counts With File Filters");
        final long bliTotalFacetCount = fileCatAppService.getTotalFacetCountByPrefix(dataSourceRequest,
                CatFileFieldType.BIZLIST_ITEM_ASSOCIATION,
                SimpleBizListItemDto.ID_PREFIX + BizListItemType.CLIENTS.ordinal() + FileCatUtils.CATEGORY_SEPARATOR);
        SolrFacetQueryResponse solrFacetQueryResponse = getRespItemsAssociationFileCounts(dataSourceRequest, null);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.BIZLIST_ITEM_ASSOCIATION, solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getRespItemsAssociationFileCounts(dataSourceRequest,
                    CatFileFieldType.BIZLIST_ITEM_ASSOCIATION.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
        }

        FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> facetPage = convertFacetResultsToSimpleBizListItems(
                solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
        facetPage.setTotalElements(bliTotalFacetCount);
        return facetPage;
    }

    private SolrFacetQueryResponse getRespItemsAssociationFileCounts(DataSourceRequest dataSourceRequest, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix("bli.");
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<String>> findBizListExtractionRulesFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("List BizList Rules Counts With File Filters");
        final long totalExtractionRuleFacetCount = fileCatAppService.getTotalFacetCountByPrefix(dataSourceRequest, CatFileFieldType.EXTRACTED_ENTITIES_IDS,
                FileDtoFieldConverter.EXTRACTION_RULE_SOLR_PREFIX);
        SolrFacetQueryResponse solrFacetQueryResponse = getExtractionRulesFileCountsRes(dataSourceRequest);
        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryRespNoFilter = getExtractionRulesFileCountsRes(dataSourceRequest);
        }
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.EXTRACTED_ENTITIES_IDS, solrFacetQueryResponse.getFirstResult().getValueCount());
        FacetPage<AggregationCountItemDTO<String>> facetPage = convertFacetResultsToExtractionRules(
                solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest(), solrFacetQueryRespNoFilter);
        facetPage.setTotalElements(totalExtractionRuleFacetCount);
        return facetPage;
    }

    private SolrFacetQueryResponse getExtractionRulesFileCountsRes(DataSourceRequest dataSourceRequest) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.EXTRACTED_ENTITIES_IDS);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix(FileDtoFieldConverter.EXTRACTION_RULE_SOLR_PREFIX);
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<BizListDto>> findBizListFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("List BizList File Counts With File Filters");
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.EXTRACTED_ENTITIES_IDS);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix(FileDtoFieldConverter.BIZLIST_SOLR_PREFIX);
        SolrFacetQueryResponse solrFacetQueryResponse = runFacetQuery(dataSourceRequest, solrFacetSpecification);
        logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.EXTRACTED_ENTITIES_IDS, solrFacetQueryResponse.getFirstResult().getValueCount());
        return convertFacetResultsToBizLists(solrFacetQueryResponse, solrFacetQueryResponse.getPageRequest());
    }

    public SolrFacetQueryResponse runFacetQuery(DataSourceRequest dataSourceRequest, SolrSpecification solrFacetSpecification) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        String selectedItemId = dataSourceRequest.getSelectedItemId();
        return fileCatService.runFacetQuery(solrFacetSpecification, c -> extractLastElementId(c.getName()), pageRequest, selectedItemId);
    }

    @Transactional
    public ClaFileDto associateFileWithBizListItem(String bizListItemId, long fileId) {
        logger.debug("Associate File id {} with bizList Item {}", fileId, bizListItemId);

        //New code - uses bizListItem associations
        SimpleBizListItemDto simpleBizListItem = bizListItemService.getFromCache(bizListItemId);
        ClaFile file = fileCatService.getFileObject(fileId);
        ClaFileDto claFileDto = associationsService.addAssociationToFile(simpleBizListItem, file, null, SolrCommitType.SOFT_NOWAIT);

        return claFileDto;
    }

    @Transactional
    public ClaFileDto removeBizListItemAssociationFromFile(long bizListId, String bizListItemId, long fileId) {
        logger.debug("remove BizListItem {} Association From File {}", bizListItemId, fileId);
        //New code - uses bizListItem associations
        SimpleBizListItemDto simpleBizListItem = bizListItemService.getFromCache(bizListItemId);
        associationsService.removeBizListItemWithFile(fileCatService.getFileEntity(fileId), simpleBizListItem, SolrCommitType.SOFT_NOWAIT);
        ClaFileDto claFileDto = FileCatUtils.convertClaFile(fileCatService.getFileObject(fileId));
        return claFileDto;
    }

    @Transactional
    public DocFolderDto associateFolderWithBizListItem(String bizListItemId, long docFolderId) {
        logger.debug("Associate Folder id {} with bizList Item {}", docFolderId, bizListItemId);
        SimpleBizListItemDto simpleBizListItem = bizListItemService.getFromCache(bizListItemId);
        DocFolderDto docFolderDto = docFolderService.addExplicitAssociableEntityToFolder(docFolderId, simpleBizListItem, AssociableEntityType.BUSINESS_LIST_ITEM);
        return docFolderDto;
    }

    @Transactional
    public DocFolderDto removeBizListItemAssociationFromFolder(long bizListId, String bizListItemId, long folderId) {
        SimpleBizListItemDto simpleBizListItem = bizListItemService.getFromCache(bizListItemId);
        associationsService.markFolderAssociationAsDeleted(folderId, simpleBizListItem, null, AssociableEntityType.BUSINESS_LIST_ITEM);
        return docFolderService.getByIdWithAssociations(folderId);
    }

    @Transactional
    public GroupDto associateGroupWithBizListItem(long bizListId, String bizListItemId, String groupId) {
        SimpleBizListItem simpleBizListItem = bizListItemService.getSimpleBizListItem(bizListItemId);
        return groupsService.addSimpleBizListItemToGroup(groupId, simpleBizListItem);
    }

    @Transactional
    public GroupDto removeBizListItemAssociationFromGroup(long bizListId, String bizListItemId, String groupId) {
        SimpleBizListItem simpleBizListItem = bizListItemService.getSimpleBizListItem(bizListItemId);
        return groupsService.markSimpleBizListItemAssociationAsDeleted(groupId, simpleBizListItem);
    }

    public long getBizListItemCounts(long id) {
        return bizListItemService.countBizListItems(id);
    }

    @Transactional
    public SimpleBizListItemDto updateBizListItemStatus(long bizListId, String itemId, String newStatus) {
        BizListItemState bizListItemState = BizListItemState.valueOf(newStatus);
        SimpleBizListItem simpleBizListItem = bizListItemService.updateBizListItemStatus(bizListId, itemId, bizListItemState);
        return BizListItemService.convertSimpleBizListItem(simpleBizListItem);
    }

    public List<ConsumerTemplateDto> getAllConsumerTemplates() {
        String[] consumerTemplates = StringUtils.split(templateList, ",");
        List<ConsumerTemplateDto> result = new ArrayList<>();
        for (String consumerTemplateString : consumerTemplates) {
            String[] nameDesc = StringUtils.split(consumerTemplateString, ":");
            if (nameDesc.length > 1) {
                result.add(new ConsumerTemplateDto(nameDesc[0], nameDesc[1]));
            } else {
                result.add(new ConsumerTemplateDto(consumerTemplateString));
            }
        }
        return result;
    }

    @Transactional
    public void processExtractionsForSingleBizListItem(BizList bizList, List<ExtractionRule> extractionRules,
                                                       Date filterDate, boolean isFilterDateAvtive, SimpleBizListItem bizListItem,
                                                       Runnable callback) {
        try {
            ExtractedEntityDto extractedEntityDto = bizListExtractionService.convertBizListItem(bizListItem, bizList, true);
            for (ExtractionRule extractionRule : extractionRules) {
                logger.trace("Extract BizList items  rule {} on {}", extractionRule, bizListItem.getEntityId());
                if (extractionRule == null) {
                    continue;
                }
                bizListExtractionService.extractBizListItemDto(extractedEntityDto, extractionRule, filterDate, isFilterDateAvtive);
                if (callback != null) {
                    callback.run();
                }
            }
        } catch (RuntimeException e) {
            logger.error("Failed to processExtractionsForSingleBizListItem on bizListItem {}. Got exception: {}", bizListItem, e.getMessage(), e);
            throw e;
        }
    }

    // ---------------------------- private method -----------------------------------------------------------------------

    private long loadDocFoldersToBizList(BizList bizList, Collection<DocFolder> docFolders, BizListSchema bizListSchema) {
        long importId = System.currentTimeMillis();
        logger.info("load {} docFolders to BizList {} with import id {}", docFolders.size(), bizList, importId);
        for (DocFolder docFolder : docFolders) {
            bizListItemService.createBizListItemFromDocFolder(bizList, docFolder, importId, bizListSchema);
        }
        return importId;
    }

    private long loadSchemaToBizList(BizList bizList, BizListSchema bizListSchema) {
        long importId = System.currentTimeMillis();
        logger.info("load schema with {} rows to BizList {} with import id {}", bizListSchema.getBizListItemRows().size(), bizList, importId);
        ArrayList<String> headers = Lists.newArrayList(bizListSchema.getHeaders());
        for (BizListItemRow bizListItemRow : bizListSchema.getBizListItemRows()) {
            try {
                bizListItemService.createBizListItemFromRow(bizList, bizListItemRow, headers, importId, bizListSchema);
                bizListService.updateBizListUpdateDate(bizList);
            } catch (Exception e) {
                logger.error("Failed to import bizListItem from row " + bizListItemRow, e);
                bizListSchema.getErrorBizListItemRows().add(bizListItemRow);
            }
        }
        return importId;
    }

    private String extractLastElementId(String name) {
        int li = name.lastIndexOf(BIZ_LIST_SEPERATOR);
        return name.substring(li + 1);
    }

    private FacetPage<AggregationCountItemDTO<SimpleBizListItemDto>> convertFacetResultsToSimpleBizListItems(
            SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, SolrFacetQueryResponse solrFacetQueryRespNoFilter) {
        List<AggregationCountItemDTO<SimpleBizListItemDto>> content = Lists.newArrayList();
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set (original values look like type.id
        List<String> ids = facetField.getValues().stream().map(c -> AssociationIdUtils.extractBizListItemIdentfier(c.getName())).collect(Collectors.toList());
        //Fetch all bizLists from the database
        Map<String, SimpleBizListItem> bizListItemsMap = bizListItemService.getSimpleBizListItemsAsMap(ids);

        Map<String, Long> resultsNoFilter = null;
        if (solrFacetQueryRespNoFilter != null) {
            resultsNoFilter = solrFacetQueryRespNoFilter.getFirstResult().getValues().stream()
                    .collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount));
        }

        logger.debug("convert Facet Field result for {} elements to SimpleBizListItemDto. DB returned {} elements", ids.size(), bizListItemsMap.size());
        long pageAggCount = 0;
        Map<String, AggregationCountItemDTO<SimpleBizListItemDto>> items = new LinkedHashMap<>();
        for (FacetField.Count count : facetField.getValues()) {
            String bizListItemId = AssociationIdUtils.extractBizListItemIdentfier(count.getName());
            AggregationCountItemDTO<SimpleBizListItemDto> item;
            if (items.containsKey(bizListItemId)) {
                item = items.get(bizListItemId);
                item.incrementCount(count.getCount());
            } else {
                SimpleBizListItemDto simpleBizListItemDto;
                if (bizListItemId.startsWith(FileDtoFieldConverter.AGG_RULE_PREFIX)) {
                    simpleBizListItemDto = new SimpleBizListItemDto();
                    simpleBizListItemDto.setName(bizListItemId.substring(FileDtoFieldConverter.AGG_RULE_PREFIX.length()));
                    simpleBizListItemDto.setAggregated(true);
                    simpleBizListItemDto.setType(BizListItemType.CONSUMERS); //TODO
                } else {
                    SimpleBizListItem simpleBizListItem = bizListItemsMap.get(bizListItemId);
                    if (simpleBizListItem == null) {
                        throw new RuntimeException("Failed to find bizListItem by id " + bizListItemId + " (" + count.getName() + ") in the database. Solr and Database are not synced.");
                    }
                    simpleBizListItemDto = BizListItemService.convertSimpleBizListItem(simpleBizListItem);
                    if (simpleBizListItem.getBizList() != null) {
                        simpleBizListItemDto.setBizListName(simpleBizListItem.getBizList().getName());
                    }
                }
                item = new AggregationCountItemDTO<>(simpleBizListItemDto, count.getCount());
            }
            items.put(bizListItemId, item);
            pageAggCount += count.getCount();
            if (resultsNoFilter != null && resultsNoFilter.get(count.getName()) != null) {
                item.setCount2(resultsNoFilter.get(count.getName()).intValue());
            }

        }
        return new FacetPage<>(new ArrayList<>(items.values()), pageRequest, pageAggCount, solrFacetQueryResponse.getNumFound());
    }

    private FacetPage<AggregationCountItemDTO<String>> convertFacetResultsToExtractionRules(
            SolrFacetQueryResponse queryResponse, PageRequest pageRequest, SolrFacetQueryResponse solrFacetQueryRespNoFilter) {
        List<AggregationCountItemDTO<String>> content = new ArrayList<>();
        FacetField facetField = queryResponse.getFirstResult();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }

        Map<String, Long> countNoFilterMap = new HashMap<>();
        if (solrFacetQueryRespNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryRespNoFilter.getFirstResult();
            for (FacetField.Count count : facetFieldNoFilter.getValues()) {
                String ruleName = extractLastElementId(count.getName());
                countNoFilterMap.put(ruleName, count.getCount());
            }
        }

        long pageAggCount = 0;
        for (FacetField.Count count : facetField.getValues()) {
            String ruleName = extractLastElementId(count.getName());
            AggregationCountItemDTO<String> item = new AggregationCountItemDTO<>(ruleName, count.getCount());
            if (countNoFilterMap.containsKey(ruleName)) {
                item.setCount2(countNoFilterMap.get(ruleName).intValue());
            }
            content.add(item);
            pageAggCount += count.getCount();
        }
        return new FacetPage<>(content, pageRequest, pageAggCount, queryResponse.getNumFound());
    }

    private FacetPage<AggregationCountItemDTO<BizListDto>> convertFacetResultsToBizLists(SolrFacetQueryResponse queryResponse, PageRequest pageRequest) {
        List<AggregationCountItemDTO<BizListDto>> content = new ArrayList<>();
        FacetField facetField = queryResponse.getFirstResult();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set (original values look like type.id
        List<Long> ids = facetField.getValues().stream().map(c -> Long.valueOf(extractLastElementId(c.getName()))).collect(Collectors.toList());
        //Fetch all bizLists from the database
        Map<Long, BizList> bizListMap = bizListService.getBizListsAsMap(ids);

        logger.debug("convert Facet Field result for {} elements to BizListDto. DB returned {} elements", ids.size(), bizListMap.size());
        long pageAggCount = 0;
        for (FacetField.Count count : facetField.getValues()) {
            long bizListId = Long.valueOf(extractLastElementId(count.getName()));
            BizList bizList = bizListMap.get(bizListId);
            if (bizList == null) {
                throw new RuntimeException(messageHandler.getMessage("biz-list.unsync-vals", Lists.newArrayList(bizListId, count.getName())));
            }
            BizListDto bizListDto = convertBizList(bizList);
            AggregationCountItemDTO<BizListDto> item = new AggregationCountItemDTO<>(bizListDto, count.getCount());
            content.add(item);
            pageAggCount += count.getCount();
        }
        return new FacetPage<>(content, pageRequest, pageAggCount, queryResponse.getNumFound());

    }


    private void evaluateGroupByExtraction(String groupId, BizListItemType bizListItemType, EvaluateBizListInstructions evaluateBizListInstructions) {
        //For each group, run a facet on the extraction details
        //Analyze the facet results and tag (classify) accordingly
        fileTaggingAppService.evaluateAndTagGroupByExtraction(groupId, bizListItemType, evaluateBizListInstructions);
    }

    private List<FacetField.Count> findGroupsWithExtractionsToEvaluate(BizListItemType bizListItemType, int page, int minCount) {
        String bizListTypeField = FileDtoFieldConverter.DtoFields.bizListItemExtractionType.name();
        FilterDescriptor extractionTypeFilter = new FilterDescriptor(bizListTypeField, bizListItemType, "eq");
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(extractionTypeFilter, CatFileFieldType.USER_GROUP_ID);
        solrSpecification.setMinCount(minCount);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrSpecification, new PageRequest(page, CLASSIFY_PAGE_SIZE));
        return solrFacetQueryResponse.getFirstResult().getValues();
    }

    public static Page<BizListDto> convertBizLists(Page<BizList> listPage, PageRequest pageRequest) {
        List<BizListDto> content = new ArrayList<>();
        for (BizList item : listPage.getContent()) {
            content.add(convertBizList(item));
        }
        return new PageImpl<>(content, pageRequest, listPage.getTotalElements());
    }

    public static List<BizListDto> convertBizLists(List<BizList> bizLists) {
        List<BizListDto> result = new ArrayList<>();
        for (BizList bizList : bizLists) {
            result.add(convertBizList(bizList));
        }

        return result;
    }

    public static BizListDto convertBizList(BizList bizList) {
        BizListDto bizListDto = new BizListDto();
        bizListDto.setId(bizList.getId());
        bizListDto.setName(bizList.getName());
        bizListDto.setDescription(bizList.getDescription());
        bizListDto.setBizListItemType(bizList.getBizListItemType());
        bizListDto.setTemplate(bizList.getTemplate());
        bizListDto.setLastSuccessfulRunDate(bizList.getLastSuccessfulRunDate());
        bizListDto.setCreationTimeStamp(bizList.getCreationTimeStamp());
        bizListDto.setLastUpdateTimeStamp(bizList.getLastUpdateTimeStamp());
        bizListDto.setActive(bizList.getActive());
        bizListDto.setExtractionSessionId(bizList.getExtractionSessionId());
        return bizListDto;
    }
}
