package com.cla.filecluster.service.export.retrievers;

import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.google.common.collect.Iterables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import reactor.core.publisher.Flux;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Stream;

@Slf4j
public class MultiPageRetriever<T> implements Retriever<Iterable<T>> {

    final Function<Map<String, String>, Iterable<T>> pageRetriever;

    private final AtomicInteger currentPage;
    private final int total;
    private final Map<String, String> requestParams;
    private final AtomicInteger totalTaken;
    private final int pageSize;
    private boolean lastPage;

    public MultiPageRetriever(Map<String, String> requestParams, int pageSize, Function<Map<String, String>, Iterable<T>> pageRetriever) {
        this.requestParams = new HashMap<>(requestParams);
        this.pageRetriever = pageRetriever;
        this.currentPage = new AtomicInteger(0);
        this.totalTaken = new AtomicInteger(0);
        this.pageSize = pageSize;
        this.total = Integer.parseInt(requestParams.get(DataSourceRequest.TAKE));
        this.lastPage = false;
    }

    @Override
    public Flux<Iterable<T>> createFlux() {
        // ReactiveSecurityContextHolder is used to preserve the context of the logged-on user.
        // used with subscriberContext near the subscription. see RxExportService.java
        return Flux.zip(ReactiveSecurityContextHolder.getContext().cache().repeat(), Flux.fromStream(Stream.generate(() -> this)))
                .map(tuple -> {
                    log.trace("setting up security context: {}", tuple.getT1().getAuthentication());
                    SecurityContextHolder.getContext().setAuthentication(tuple.getT1().getAuthentication());
                    return tuple.getT2().nextPage();
                })
                .takeWhile(Optional::isPresent) // continue the flux while nextPage() returns a value.
                .map(Optional::get);            // convert optional to actual.
    }

    private int extractCurrPage() {
        try {
            return Integer.parseInt(requestParams.get(DataSourceRequest.PAGE_NUMBER));
        } catch (NumberFormatException e) {
            log.warn("error getting current page number from request parameters, assuming first page");
            return 1;
        }
    }

    private Optional<Iterable<T>> nextPage() {
        Optional<Map<String, String>> pageParams = getPageParams();
        log.trace("emitting page {}, params{}", currentPage.get(), pageParams.orElse(null));
        // get params for the next page, and if available get the next page.
        return pageParams.map(pageRetriever).map(page -> {
            if (Iterables.isEmpty(page)) {
                lastPage = true;
            }
            return page;
        });
    }

    Optional<Map<String, String>> getPageParams() {
        Map<String, String> result = null;
        if (total > totalTaken.get() && !lastPage) {
            int take = calcTakeVal();
            if (take > 0) {
                result = new HashMap<>(requestParams);
                result.put(DataSourceRequest.PAGE_NUMBER, String.valueOf(currentPage.incrementAndGet()));
                result.put(DataSourceRequest.TAKE, String.valueOf(take));
            }
        }
        return Optional.ofNullable(result);
    }

    private int calcTakeVal() {
        try {
            int currTake = total - totalTaken.get();
            int result = Math.min(currTake, pageSize);
            totalTaken.addAndGet(result);
            return result;
        } catch (Exception e) {
            log.warn("error resolving page size for export, assuming {}", pageSize);
            return pageSize;
        }
    }

    public void setParams(Map<String, String> params) {
        requestParams.putAll(params);
    }
}
