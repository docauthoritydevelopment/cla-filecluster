package com.cla.filecluster.service.report;


import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.report.DisplayType;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.api.reports.UserViewAndFilterApiController;
import com.cla.filecluster.service.filter.SavedFilterAppService;
import com.cla.filecluster.service.importEntities.SchemaData;
import com.cla.filecluster.service.importEntities.SchemaLoader;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by ophir on 7/10/2019.
 */
@Service
public class UserViewDataSchemaLoader extends SchemaLoader<UserViewDataItemRow,UserViewDataDto> {

    private Logger logger = LoggerFactory.getLogger(UserViewDataSchemaLoader.class);

    @Autowired
    private UserViewDataService userViewDataService;

    @Autowired
    private UserViewAndFilterApiController userViewAndFilterApiController;

    @Autowired
    private SavedFilterAppService savedFilterAppService;

    @Autowired
    private MessageHandler messageHandler;

    private final static String CSV_HEADER_NAME = "NAME";
    private final static String CSV_HEADER_DISPLAY_TYPE = "DISPLAY_TYPE";
    private final static String CSV_HEADER_DISPLAY_DATA = "DISPLAY_DATA";
    private final static String CSV_HEADER_FILTER_NAME = "FILTER_NAME";
    private final static String CSV_HEADER_FILTER_DESCRIPTOR = "FILTER_DESCRIPTOR";
    private final static String CSV_HEADER_FILTER_RAW = "FILTER_RAW";
    private final static String CSV_HEADER_CONTAINER_NAME = "CONTAINER_NAME";


    private final static String[] REQUIRED_HEADERS = {CSV_HEADER_NAME,
            CSV_HEADER_DISPLAY_TYPE,CSV_HEADER_DISPLAY_DATA,CSV_HEADER_FILTER_NAME,CSV_HEADER_FILTER_DESCRIPTOR,
            CSV_HEADER_FILTER_RAW,CSV_HEADER_CONTAINER_NAME};

    public UserViewDataSchemaLoader() {
        super(REQUIRED_HEADERS);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public SchemaData<UserViewDataItemRow> createSchemaInstance() {
        return new SchemaData<UserViewDataItemRow>(){
            @Override
            public UserViewDataItemRow createImportItemRowInstance() {
                return new UserViewDataItemRow();
            }
        };
    }

    @Override
    protected UserViewDataItemRow extractRowSpecific(SchemaData<UserViewDataItemRow> schemaData, String[] fieldsValues,
                                                     ArrayList<String> parsingWarningMessages) {
        UserViewDataItemRow itemRow = new UserViewDataItemRow();
        itemRow.setName(extractValue(schemaData, fieldsValues, CSV_HEADER_NAME, parsingWarningMessages));
        if (itemRow.getName().startsWith("#")) {
            return null;    // Ignore comment
        }
        itemRow.setDisplayType(extractValue(schemaData, fieldsValues, CSV_HEADER_DISPLAY_TYPE, parsingWarningMessages));
        itemRow.setDisplayData(extractValue(schemaData, fieldsValues, CSV_HEADER_DISPLAY_DATA, parsingWarningMessages));
        itemRow.setFilterName(extractValue(schemaData, fieldsValues, CSV_HEADER_FILTER_NAME, parsingWarningMessages));
        itemRow.setFilterDescription(extractValue(schemaData, fieldsValues, CSV_HEADER_FILTER_DESCRIPTOR, parsingWarningMessages));
        itemRow.setFilterRaw(extractValue(schemaData, fieldsValues, CSV_HEADER_FILTER_RAW, parsingWarningMessages));
        itemRow.setContainerName(extractValue(schemaData, fieldsValues, CSV_HEADER_CONTAINER_NAME, parsingWarningMessages));
        itemRow.setFilterGlobal(false);
        return itemRow;
    }


    @Override
    protected boolean validateItemRow(UserViewDataDto userViewDataDto, UserViewDataItemRow itemRow,
                                      SchemaData<UserViewDataItemRow> schemaData, boolean afterAddToDB, boolean createStubs ) {

        if (Strings.isNullOrEmpty(itemRow.getName())) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("user-view-data.upload.missing-name") );
            return false;
        }

        if (itemRow.isFilterGlobal() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("user-view-data.upload.missing-global-filter") );
            return false;
        }

        if (itemRow.getDisplayType() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("user-view-data.upload.missing-display-type"));
            return false;
        }
        else {
            try {
                DisplayType.valueOf(itemRow.getDisplayType());
            }
            catch(IllegalArgumentException iae) {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("user-view-data.upload.display-type-incorrect"));
                return false;
            }
        }

        //if (!afterAddToDB && !Strings.isNullOrEmpty(itemRow.getFilterName())) {
        //    if (savedFilterAppService.getSavedFilterByName(itemRow.getFilterName())!= null){
        //        schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("user-view-data.upload.filter-name-exist"));
        //        return false;
        //    }
        //}
        return true;
    }


    @Override
    protected Boolean loadItemRowToRepository(SchemaData<UserViewDataItemRow> schemaData, UserViewDataItemRow itemRow, boolean updateDuplicates, boolean createStubs) {
        UserViewDataDto userViewDataDto = createDtoFromRowItem(itemRow);
        UserViewDataDto resultUserViewDataDto;
        resultUserViewDataDto = userViewAndFilterApiController.createUserViewData(userViewDataDto);
		if (resultUserViewDataDto == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("import.error-insert-row"));
            return false;
        } else {
            return validateItemRow(resultUserViewDataDto, itemRow, schemaData, true, createStubs);
        }
    }



    @Override
    protected UserViewDataDto createDtoFromRowItem(UserViewDataItemRow itemRow) {
        UserViewDataDto userViewDataDto = new UserViewDataDto();
        userViewDataDto.setName(itemRow.getName());
        userViewDataDto.setDisplayType(DisplayType.valueOf(itemRow.getDisplayType()));
        userViewDataDto.setDisplayData(itemRow.getDisplayData());
        if (itemRow.isFilterGlobal() != null) {
            userViewDataDto.setGlobalFilter(itemRow.isFilterGlobal());
        }
        if (itemRow.getFilterName() != null) {
            SavedFilterDto sfd = new SavedFilterDto();
            sfd.setName(itemRow.getFilterName());
            sfd.setRawFilter(itemRow.getFilterRaw());
            if (itemRow.isFilterGlobal() != null) {
                sfd.setGlobalFilter(itemRow.isFilterGlobal());
            }
            sfd.setFilterDescriptor(userViewDataService.getFilterDescriptionByJsonString(itemRow.getFilterDescription()));
            userViewDataDto.setFilter(sfd);
        }
        if (itemRow.getContainerName() != null) {
            DisplayType containerType = DisplayType.DASHBOARD;
            /*
            if (itemRow.getDisplayType().equals(DisplayType.DASHBOARD_WIDGET.toString())){
                containerType = DisplayType.DASHBOARD;
            }*/
            Optional<UserViewDataDto> containerDtoOptional = userViewDataService.getFirstByNameAndType(itemRow.getContainerName(), containerType);
            UserViewDataDto containerDto;
            if (containerDtoOptional.isPresent()) {
                containerDto = containerDtoOptional.get();
            }
            else {
                UserViewDataDto containerUserViewDataDto  = new UserViewDataDto();
                containerUserViewDataDto.setName(itemRow.getContainerName());
                containerUserViewDataDto.setDisplayType(containerType);
                containerDto = userViewDataService.createUserViewData(containerUserViewDataDto);
            }
            userViewDataDto.setContainerId(containerDto.getId());
        }
        return userViewDataDto;
    }

}
