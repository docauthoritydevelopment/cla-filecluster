package com.cla.filecluster.service.api.actions;

import com.cla.common.domain.dto.action.ActionScheduleDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.actions.ActionScheduleService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * api for scheduled actions CRUD
 *
 * Created by: yael
 * Created on: 3/5/2018
 */
@RestController
@RequestMapping("/api/action/schedule")
public class ActionScheduleApiController {

    private final static Logger logger = LoggerFactory.getLogger(ActionScheduleApiController.class);

    @Autowired
    private ActionScheduleService actionScheduleService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<ActionScheduleDto> listActionScheduleDto(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return actionScheduleService.listActionSchedules(dataSourceRequest);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ActionScheduleDto createActionSchedule(@RequestBody ActionScheduleDto actionScheduleDto) {
        ActionScheduleDto actionSchedule = actionScheduleService.createActionSchedule(actionScheduleDto);
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Create actionSchedule", AuditAction.CREATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionSchedule;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteActionSchedule(@PathVariable long id) {
        ActionScheduleDto actionSchedule = getActionSchedule(id);
        actionScheduleService.deleteActionSchedule(id);
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Delete actionSchedule", AuditAction.DELETE, ActionScheduleDto.class, id, actionSchedule);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ActionScheduleDto updateActionSchedule(@PathVariable Long id, @RequestBody ActionScheduleDto actionScheduleDto) {
        ActionScheduleDto actionSchedule = actionScheduleService.updateActionSchedule(actionScheduleDto);
        if (actionSchedule == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Update actionSchedule", AuditAction.UPDATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionSchedule;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ActionScheduleDto getActionSchedule(@PathVariable long id) {
        ActionScheduleDto actionSchedule = actionScheduleService.getActionSchedule(id);
        if (actionSchedule == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return actionSchedule;
    }

    @RequestMapping(value = "/{name}",method = RequestMethod.GET)
    public ActionScheduleDto getActionScheduleByName(@PathVariable String name) {
        ActionScheduleDto actionSchedule = actionScheduleService.getActionScheduleByName(name);
        if (actionSchedule == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return actionSchedule;
    }

    @RequestMapping(value = "/{id}/active", method = RequestMethod.POST)
    public ActionScheduleDto updateScheduleActionActive(@PathVariable Long id, @RequestBody Boolean state) {
        ActionScheduleDto actionScheduleDto = actionScheduleService.updateScheduleActionActive(id, state);
        if (actionScheduleDto == null) {
            throw new BadRequestException(messageHandler.getMessage("action-schedule.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        eventAuditService.audit(AuditType.ACTION_SCHEDULE,"Update actionSchedule", AuditAction.UPDATE, ActionScheduleDto.class,actionScheduleDto.getId(),actionScheduleDto);
        return actionScheduleDto;
    }
}
