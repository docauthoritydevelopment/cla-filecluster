package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;


@Component
public class ReadSharePermissionEnricher implements CategoryTypeDataEnricher {

    private static String UNIQUE_SHARE_PERMISSION_KEY_DELIMITER="_";
    private static final String READ_SHARE_PERMISSION_DUMMY_PREFIX = "readSharePermission";

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params) {
        HashMap<String, String> ans = new HashMap<>();
        Map<Long, List<RootFolderSharePermissionDto>> sharePermissions =
                sharePermissionService.getAllSharePermissions(AclType.READ_TYPE);
        HashMap<String, List<String>> sharePermissionMap = new HashMap<>();
        for (Long rootFolderId : sharePermissions.keySet()) {
            for (RootFolderSharePermissionDto rootFolderSharePermissionDto: sharePermissions.get(rootFolderId)) {
                String key = getSharePermissionMapKey(rootFolderSharePermissionDto);
                List<String> permissionsList;
                if (sharePermissionMap.containsKey(key)) {
                    permissionsList = sharePermissionMap.get(key);
                } else {
                    permissionsList = new ArrayList<>();
                    sharePermissionMap.put(key, permissionsList);
                }
                permissionsList.add(String.valueOf(rootFolderId));
                ans.put(String.valueOf(rootFolderSharePermissionDto.getId()), DashboardChartsUtil.getQueryTypeValueStringAccordingToValuesList(permissionsList));
            }
        }
        return ans;
    }

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.SHARED_PERMISSION, READ_SHARE_PERMISSION_DUMMY_PREFIX);
    }

    private String getSharePermissionMapKey(RootFolderSharePermissionDto rootFolderSharePermissionDto) {
        return rootFolderSharePermissionDto.getAclEntryType().name() + UNIQUE_SHARE_PERMISSION_KEY_DELIMITER + rootFolderSharePermissionDto.getMask() + UNIQUE_SHARE_PERMISSION_KEY_DELIMITER + rootFolderSharePermissionDto.getUser();
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return sharePermissionService.getById(Long.parseLong(category)).getUser();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getFilterFieldName() {
        return FileDtoFieldConverter.DtoFields.sharedPermission.getAlternativeName();
    }
}
