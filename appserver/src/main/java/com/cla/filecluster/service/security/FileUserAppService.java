package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.service.files.filetree.SharePermissionService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class FileUserAppService {

    private static final Logger logger = LoggerFactory.getLogger(FileUserAppService.class);

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Autowired
    private DomainAccessHandler domainAccessHandler;

    public FacetPage<AggregationCountItemDTO<FileUserDto>> getOwnerFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("Get Owner File Counts With File Filters");
        long totalOwnerFacetCount = fileCatAppService.getTotalFacetCount(dataSourceRequest, CatFileFieldType.OWNER);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.OWNER);
        solrFacetSpecification.setMinCount(1);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatAppService.runFacetQuery(dataSourceRequest, solrFacetSpecification);

        SolrFacetQueryResponse solrFacetQueryResponseNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            SolrSpecification solrFacetSpecificationNoFilter =
                    solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.OWNER);
            solrFacetSpecificationNoFilter.setMinCount(1);
            List<String> facetValues = SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)
                    .stream().map(ClientUtils::escapeQueryChars).collect(Collectors.toList());
            solrFacetSpecificationNoFilter.setAdditionalFilter(
                    CatFileFieldType.OWNER.inQuery(facetValues));
            solrFacetQueryResponseNoFilter = fileCatAppService.runFacetQuery(dataSourceRequest, solrFacetSpecificationNoFilter);
        }

        logger.debug("Get Owner File Counts returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        final FacetPage<AggregationCountItemDTO<FileUserDto>> facetPage = convertFacetFieldResultToFileUserDto(solrFacetQueryResponse.getFirstResult(),
                solrFacetQueryResponseNoFilter, solrFacetQueryResponse.getPageRequest());
        facetPage.setTotalElements(totalOwnerFacetCount);
        return facetPage;
    }


    private FacetPage<AggregationCountItemDTO<FileUserDto>> convertFacetFieldResultToFileUserDto(
            FacetField facetField, SolrFacetQueryResponse solrFacetQueryResponseNoFilter, PageRequest pageRequest) {
        Map<String, AggregationCountItemDTO<FileUserDto>> content = new LinkedHashMap<>();
        for (FacetField.Count count : facetField.getValues()) {
            String displayName = domainAccessHandler.getAclInConfigFormat(count.getName());
            FileUserDto dto = new FileUserDto(displayName);
            AggregationCountItemDTO<FileUserDto> item = new AggregationCountItemDTO<>(dto, count.getCount());
            content.put(count.getName(), item);
        }

        if (solrFacetQueryResponseNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryResponseNoFilter.getFirstResult();
            for (FacetField.Count count : facetFieldNoFilter.getValues()) {
                AggregationCountItemDTO<FileUserDto> item = content.get(count.getName());
                if (item != null) {
                    item.setCount2(count.getCount());
                }
            }
        }

        return new FacetPage<>(new ArrayList<>(content.values()), pageRequest);
    }


    @Transactional(readOnly = true)
    @AutoAuthenticate(replace = false)
    public Page<AggregationCountItemDTO<FileUserDto>> getAllOwners(PageRequest pageRequest) {
        return fileCatAppService.countFilesByOwner(pageRequest);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<FileUserDto>> getAclFileCounts(AclType aclType, DataSourceRequest dataSourceRequest) {
        logger.debug("Get ACL {} File Counts With File Filters", aclType);

        String sharedPermissionFilter = null;
        if (dataSourceRequest.isIgnoreSharedPermissions()) {
            Set<Long> rootFolderIds = sharePermissionService.getRelevantRootFolders();
            if (rootFolderIds != null && !rootFolderIds.isEmpty()) {
                sharedPermissionFilter = CatFileFieldType.ROOT_FOLDER_ID.notInQuery(rootFolderIds);
            }
        }

        CatFileFieldType facetType = FileDtoFieldConverter.convertAclToCatFileField(aclType);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
        solrFacetSpecification.setMinCount(1);
        if (sharedPermissionFilter != null) {
            solrFacetSpecification.setAdditionalFilter(sharedPermissionFilter);
        }
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatAppService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
        logger.debug("Get ACL File Counts returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());

        long totalCount = fileCatAppService.getTotalFacetCount(dataSourceRequest, facetType,
                sharedPermissionFilter);

        SolrFacetQueryResponse solrFacetQueryResponseNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            SolrSpecification solrFacetSpecificationNoFilter =
                    solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
            solrFacetSpecificationNoFilter.setMinCount(1);
            if (sharedPermissionFilter != null) {
                solrFacetSpecification.setAdditionalFilter(sharedPermissionFilter);
            }

            solrFacetSpecificationNoFilter.setAdditionalFilter(
                    facetType.inQuery(escapeAclValues(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse))));
            solrFacetQueryResponseNoFilter = fileCatAppService.runFacetQuery(dataSourceRequest, solrFacetSpecificationNoFilter);
        }

        final FacetPage<AggregationCountItemDTO<FileUserDto>> facetPage = convertFacetFieldResultToFileUserDto(
                solrFacetQueryResponse.getFirstResult(), solrFacetQueryResponseNoFilter, solrFacetQueryResponse.getPageRequest());
        facetPage.setTotalElements(totalCount);
        return facetPage;
    }

    private List<String> escapeAclValues(List<String> acls) {
        if (acls == null) return null;
        return acls.stream().map(ClientUtils::escapeQueryChars).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public Page<AggregationCountItemDTO<GroupDto>> getGroupsByOwner(String owner, PageRequest pageRequest) {
        Map<String, Long> result = fileCatAppService.countFilesByGroupForOwner(owner);
        Set<String> groupIds = result.keySet();
        List<SolrFileGroupEntity> groups = groupIds == null || groupIds.isEmpty() ? new ArrayList<>() : fileGroupService.findByIds(groupIds);
        Map<String, SolrFileGroupEntity> groupsMap = groups.stream().collect(Collectors.toMap(SolrFileGroupEntity::getId, g -> g));
        List<AggregationCountItemDTO<GroupDto>> content = new ArrayList<>(result.size());
        result.forEach((gId, size) -> {
            SolrFileGroupEntity group = groupsMap.get(gId);
            content.add(
                    new AggregationCountItemDTO<>(
                            new GroupDto(group.getId(),
                                    group.getName(),
                                    group.getNumOfFiles(),
                                    null,
                                    null,
                                    null,
                                    group.getGeneratedName(),
                                    null),
                            size));
        });
        return new PageImpl<>(content, pageRequest, result.size());
    }
}
