package com.cla.filecluster.service.export.exporters.file.associations;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class FunctionalRoleDtoMixin {

    @JsonProperty(DataRoleHeaderFields.NAME)
    private String name;

    @JsonProperty(DataRoleHeaderFields.DESCRIPTION)
    private String description;
}
