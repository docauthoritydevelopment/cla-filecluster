package com.cla.filecluster.service.export.exporters.user_view;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: ophir
 * Created on: 7/10/2019
 */
public abstract class UserViewDataDtoMixin {

    @JsonProperty(UserViewDataHeaderFields.NAME)
    private String name;

    @JsonProperty(UserViewDataHeaderFields.DISPLAY_DATA)
    private String displayData;

    @JsonProperty(UserViewDataHeaderFields.DISPLAY_TYPE)
    private boolean displayType;

    @JsonProperty(UserViewDataHeaderFields.IS_FILTER_GLOBAL)
    private boolean globalFilter;
}
