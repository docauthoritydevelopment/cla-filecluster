package com.cla.filecluster.service.api.board;

import com.cla.common.domain.dto.board.BoardDto;
import com.cla.common.domain.dto.board.BoardTaskDto;
import com.cla.common.domain.dto.board.BoardTaskState;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.board.BoardAppService;
import com.cla.filecluster.service.board.BoardService;
import com.cla.filecluster.service.board.BoardTaskService;
import com.cla.filecluster.service.security.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/api/board")
public class BoardApiController {

    private final static Logger logger = LoggerFactory.getLogger(BoardApiController.class);

    @Autowired
    private BoardAppService boardAppService;

    @Autowired
    private BoardService boardService;

    @Autowired
    private BoardTaskService boardTaskService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private EventAuditService eventAuditService;

    @RequestMapping(value="/list", method = RequestMethod.GET)
    public Page<BoardDto> listBoards(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        User owner = userService.getCurrentUserEntity();
        if (owner != null) {
            return boardService.getAllForOwner(dataSourceRequest.createPageRequest(), owner.getId());
        }
        return Page.empty();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public BoardDto getBoard(@PathVariable("id") long boardId) {
        return boardService.getById(boardId);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public BoardDto createBoard(@RequestBody BoardDto boardDto) {
        User owner = userService.getCurrentUserEntity();
        if (owner != null) {
            boardDto.setOwnerId(owner.getId());
            BoardDto boardDtoRes = boardService.createBoard(boardDto);
            eventAuditService.audit(AuditType.BOARD, "Create board", AuditAction.CREATE, BoardDto.class, boardDtoRes.getId(), boardDtoRes);
            return boardDtoRes;
        }
        throw new BadRequestException(messageHandler.getMessage("operation.user-not-allowed"), BadRequestType.UNAUTHORIZED);
    }

    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public BoardDto updateBoard(@PathVariable("id") long boardId, @RequestBody BoardDto boardDto) {
        BoardDto boardDtoRes = boardService.updateBoard(boardDto);
        eventAuditService.audit(AuditType.BOARD,"Update board", AuditAction.UPDATE, BoardDto.class, boardDtoRes.getId(), boardDtoRes);
        return boardDtoRes;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteBoard(@PathVariable("id") long boardId) {
        BoardDto boardDtoRes = boardService.getById(boardId);
        if (boardDtoRes != null) {
            boardAppService.deleteBoard(boardId);
            eventAuditService.audit(AuditType.BOARD, "Delete board", AuditAction.DELETE, BoardDto.class, boardId, boardDtoRes);
        } else {
            throw new BadRequestException(messageHandler.getMessage("board.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/task", method = RequestMethod.PUT)
    public BoardTaskDto createBoardTask(@RequestBody BoardTaskDto boardTaskDto) {
        User owner = userService.getCurrentUserEntity();
        if (owner != null) {
            boardTaskDto.setOwnerId(owner.getId());
            boardTaskDto.setAssignee(owner.getId());
            BoardTaskDto boardDtoRes = boardTaskService.createBoardTask(boardTaskDto);
            eventAuditService.audit(AuditType.BOARD_TASK,"Create board task", AuditAction.CREATE, BoardTaskDto.class, boardDtoRes.getId(), boardDtoRes);
            return boardDtoRes;
        }
        throw new BadRequestException(messageHandler.getMessage("operation.user-not-allowed"), BadRequestType.UNAUTHORIZED);
    }

    @RequestMapping(value = "/task/{id}", method = RequestMethod.DELETE)
    public void deleteBoardTask(@PathVariable("id") long boardTaskId) {
        BoardTaskDto boardDtoRes = boardTaskService.getById(boardTaskId);
        if (boardDtoRes != null) {
            boardTaskService.deleteBoardTask(boardTaskId);
            eventAuditService.audit(AuditType.BOARD_TASK, "Delete board task", AuditAction.DELETE, BoardTaskDto.class, boardTaskId, boardDtoRes);
        } else {
            throw new BadRequestException(messageHandler.getMessage("board-task.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
    }

    @RequestMapping(value = "/tasks/{id}", method = RequestMethod.GET)
    public Page<BoardTaskDto> getBoardTasks(@PathVariable("id") long boardId, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return boardTaskService.findByBoard(boardId, dataSourceRequest.createPageRequest());
    }

    @RequestMapping(value = "/tasks/owner/{id}", method = RequestMethod.POST)
    public Page<BoardTaskDto> getOwnerBoardTasks(@PathVariable("id") long boardId, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        User owner = userService.getCurrentUserEntity();
        if (owner != null) {
            return boardTaskService.findByOwnerAndBoard(boardId, owner.getId(), dataSourceRequest.createPageRequest());
        }
        return Page.empty();
    }

    @RequestMapping(value = "/tasks/assignee/{id}", method = RequestMethod.GET)
    public Page<BoardTaskDto> getAssigneeBoardTasks(@PathVariable("id") long boardId, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        User assignee = userService.getCurrentUserEntity();
        if (assignee != null) {
            return boardTaskService.findByAssigneeAndBoard(boardId, assignee.getId(), dataSourceRequest.createPageRequest());
        }
        return Page.empty();
    }

    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/task/{id}/state", method = RequestMethod.POST)
    public BoardTaskDto updateBoardTaskState(@PathVariable("id") long boardTaskId, BoardTaskState boardTaskState) {
        BoardTaskDto boardTaskDtoRes = boardTaskService.updateBoardTaskState(boardTaskId, boardTaskState);
        eventAuditService.audit(AuditType.BOARD_TASK,"Update board task", AuditAction.UPDATE, BoardTaskDto.class, boardTaskDtoRes.getId(), boardTaskDtoRes);
        return boardTaskDtoRes;
    }

    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/task/{id}/assignee", method = RequestMethod.POST)
    public BoardTaskDto updateBoardTaskAssignee(@PathVariable("id") long boardTaskId, long assignee) {
        BoardTaskDto boardTaskDtoRes = boardTaskService.updateBoardTaskAssignee(boardTaskId, assignee);
        eventAuditService.audit(AuditType.BOARD_TASK,"Update board task", AuditAction.UPDATE, BoardTaskDto.class, boardTaskDtoRes.getId(), boardTaskDtoRes);
        return boardTaskDtoRes;
    }

    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/task/{id}", method = RequestMethod.POST)
    public BoardTaskDto updateBoardTask(@PathVariable("id") long boardTaskId, @RequestBody BoardTaskDto boardTaskDto) {
        BoardTaskDto boardTaskDtoRes = boardTaskService.updateBoardTask(boardTaskDto);
        eventAuditService.audit(AuditType.BOARD_TASK,"Update board task", AuditAction.UPDATE, BoardTaskDto.class, boardTaskDtoRes.getId(), boardTaskDtoRes);
        return boardTaskDtoRes;
    }

    @RequestMapping(value = "/tasks/board/{id}", method = RequestMethod.POST)
    public void createBoardTasksByFilter(@PathVariable("id") long boardId, @RequestParam final Map<String,String> params) {
        BoardDto boardDto = boardService.getById(boardId);
        User owner = userService.getCurrentUserEntity();
        if (boardDto != null && owner != null) {
            boardAppService.createTasksForFolderFilter(boardDto, owner.getId(), params);
        }
    }
}
