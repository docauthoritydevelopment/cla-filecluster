package com.cla.filecluster.service.diagnostics;

import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public interface DiagnosticAction {

    void runDiagnostic(Map<String,String> opData);
    DiagnosticType getType();
    List<Diagnostic> createNewDiagnosticActionParts();
    int getStartAfterMin();

    default Diagnostic addDiagnosticActionParts() {
        long startTime = System.currentTimeMillis() + TimeUnit.MINUTES.toMillis(getStartAfterMin());
        return createNew(null, startTime);
    }

    default Diagnostic createNew(Map<String, String> opData, long startTime) {
        Diagnostic d = new Diagnostic();
        d.setDiagnosticType(getType());
        d.setStartTime(startTime);
        d.setOpData(opData);
        return d;
    }
}
