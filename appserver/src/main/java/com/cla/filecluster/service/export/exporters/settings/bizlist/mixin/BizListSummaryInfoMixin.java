package com.cla.filecluster.service.export.exporters.settings.bizlist.mixin;

import com.cla.common.domain.dto.bizlist.BizListDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class BizListSummaryInfoMixin {

    @JsonUnwrapped
    private BizListDto bizListDto;

    @JsonUnwrapped
    private CrawlRunDetailsDto crawlRunDetailsDto;

}
