package com.cla.filecluster.service.files.managers;

import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Deprecated
public class HtmlReportService {

	@Value("${htmlReport.static.groupingTopHtmlTemplateFile:./config/groupingTopHtmlTemplate.vm}")
	private String staticGroupingTopHtmlTemplateFileName;

	@Value("${htmlReport.static.groupFilesHtmlTemplateFile:./config/groupFilesHtmlTemplate.vm}")
	private String staticGroupFilesHtmlTemplateFileName;
	
	@Value("${htmlReport.dynamic.groupingTopHtmlTemplateFile:./config/dynamicGroupingTopHtmlTemplate.vm}")
	private String dynamicGroupingTopHtmlTemplateFileName;

	@Value("${htmlReport.dynamic.groupFilesHtmlTemplateFile:./config/dynamicGroupFilesHtmlTemplate.vm}")
	private String dynamicGroupFilesHtmlTemplateFileName;
	
	@Value("${htmlReport.file:groupingReport.html}")
	private String htmlReportFileName;
	
	@Value("${htmlReport.static.folder:./logs}")
	private String staticHtmlReportFolderName;
	
	@Value("${htmlReport.dynamic.folder:./static/htmlReports}")
	private String dynamicHtmlReportFolderName;
	
	@Value("${groupsHtml.folder:groupsHtml}")
	private String groupsHtmlFolderName;
	
	@Value("${groupsHtml.suffix:.html}")
	private String groupsHtmlSuffix;
	
//	@Value("${rulesFile:./config/extraction/extraction-rules.txt}")
//	private String rulesFileName;
	
	@Value("${velocity.runtime.log:./logs/velocity.log}")
	private String velocityRuntimeLog;

	@Value("${htmlReportFilesByGroupFetchPageSize:200}")
	private int pageSize;

	@Value("${htmlReport.resolveAbsolutPath:false}")
	private boolean resolveAbsolutPath;
	private static boolean resolveAbsolutPathStatic;

	@Value("${htmlReport.includeGroupInfo:true}")
	private boolean includeGroupInfo;
	
	@Value("${htmlReport.extractionHighlightingHtmlTemplate:./config/extractionHighlightingHtmlTemplate.vm}")
	private String extractionHighlightingHtmlTemplateFileName;

    @Autowired
    private OpStateService opStateService;
    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

	@Autowired
    private JobManagerService jobManagerService;

	@PostConstruct
	private void init() {
		resolveAbsolutPathStatic = resolveAbsolutPath;
	}

	public void createExtractionHighlightingHtml(final Writer writer, final ClaFile claFile, final String clearTokens, 
			List<String> highlights, Map<String, List<String>> tokensLocations, String rule, String entityDesc, final String groupName) {
		try {
			final VelocityEngine ve = new VelocityEngine();
			ve.setProperty("runtime.log", velocityRuntimeLog);
			ve.init();		
			final Template t = ve.getTemplate( extractionHighlightingHtmlTemplateFileName );
			final VelocityContext context = new VelocityContext();
			
			if (highlights==null) {
				highlights = new ArrayList<>();
			}
			if (tokensLocations==null) {
				tokensLocations = new HashMap<>();
			}
			if (rule==null) {
				rule = "";
			}
			if (entityDesc==null) {
				entityDesc = "";
			}
			
			context.put("filename", claFile.getFileName());
			context.put("basename", claFile.getBaseName());
			context.put("clearTokens", clearTokens);
			context.put("rule", rule);
			context.put("entity", entityDesc);
			context.put("group", groupName);
			context.put("highlights", highlights);
			context.put("locations", tokensLocations);
			context.put("htmlFormatter", this);
			t.merge( context, writer );
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

}
