package com.cla.filecluster.service.report.dashboardChartQueryBuilder;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.report.DashboardChartQueryBuilder;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import com.cla.filecluster.service.report.dashboardChartsEnrichers.CategoryTypeDataEnricher;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class CategoryTypeDataQueryBuilder implements DashboardChartQueryBuilder<Map<String, String>, CategoryTypeDataEnricher> {

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    private static final String SOLR_QUERY_FILTER_SEPARATOR = ":";

    public ChartQueryType getQueryBuilderType() {
        return ChartQueryType.CATEGORY;
    }


    public Map<String, String> setInitialDataToDashboardChartDto(DashboardChartDataDto dashboardChartDataDto,
                                                                 CatFileFieldType groupedField,
                                                                 DataSourceRequest dataSourceRequest, FilterDescriptor filter,
                                                                 String queryPrefix, DashboardChartValueTypeDto valueField,
                                                                 Map<String, String> params,
                                                                 String additionalFilter,
                                                                 CategoryTypeDataEnricher dashboardChartEnricher) {
        int itemsNumber = dashboardChartsUtil.extractTakeNumber(params);
        int page = dashboardChartsUtil.extractPageNumber(params);

        Map<String, String> categoriesMap = dashboardChartEnricher.getCategoryFieldQueryValuesMap(params);

        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                dataSourceRequest);

        SolrFacetQueryJson theJsonFacet = buildQueryJsonFacet(categoriesMap, solrFacetSpecification,
                filter, groupedField, queryPrefix, valueField, categoriesMap.keySet().toArray(),
                dashboardChartsUtil.getNameOfFilter(filter), params, true, additionalFilter);

        solrFacetSpecification.addFacetJsonObject(theJsonFacet);

        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);

        parseResponseToDashboardChartDataDto(categoriesMap, dashboardChartDataDto,
                solrFacetQueryJsonResponse.getResponseBucket(theJsonFacet.getName()).getBuckets()
                , dashboardChartsUtil.getNameOfFilter(filter),
                valueField, params, true);

        if (dashboardChartEnricher.needToRemoveEmptyValues()) {
            dashboardChartsUtil.removeEmptyCategories(dashboardChartDataDto);
        }
        //dashboardChartDataDto.setTotalElements(Long.valueOf(dashboardChartDataDto.getCategories().size()));
        applySortAndSpliceResultToNeededPageOfDashboardChartDataDto(dashboardChartDataDto, valueField.getSortDirection(),
                page, itemsNumber);

        return categoriesMap;
    }

    private void applySortAndSpliceResultToNeededPageOfDashboardChartDataDto(DashboardChartDataDto dashboardChartDataDto,
                                                                             Sort.Direction direction, int page, int itemsNumber) {
        List<Double> dataList = dashboardChartDataDto.getSeries().get(0).getData();
        int[] sortedIndices = IntStream.range(0, dataList.size())
                .boxed().sorted(direction.equals(Sort.Direction.DESC) ?
                        Comparator.comparing(dataList::get).reversed() :
                        Comparator.comparing(dataList::get))
                .mapToInt(ele -> ele).toArray();

        List<String> newCategories = new ArrayList<>();
        List<Double> newData = new ArrayList<>();
        for (int theIndex : sortedIndices) {
            newCategories.add(dashboardChartDataDto.getCategories().get(theIndex));
            newData.add(dataList.get(theIndex));
        }

        int fromIndex = (page - 1) * itemsNumber;
        int toIndex = page * itemsNumber;
        if (fromIndex < newCategories.size()) {
            if (toIndex > newCategories.size()) {
                toIndex = newCategories.size();
            }
            dashboardChartDataDto.setCategories(newCategories.subList(fromIndex, toIndex));
            dashboardChartDataDto.getSeries().get(0).setData(newData.subList(fromIndex, toIndex));
        } else {
            dashboardChartDataDto.setCategories(new ArrayList<>());
            dashboardChartDataDto.getSeries().get(0).setData(new ArrayList<>());
        }
    }


    public SolrFacetQueryJson buildQueryJsonFacetByFilter(Map<String, String> categoriesMap,
                                                          SolrSpecification solrSpecification, FilterDescriptor filter,
                                                          SolrCoreSchema field, String queryPrefix,
                                                          DashboardChartValueTypeDto valueField, Object[] neededValues,
                                                          String queryName, Map<String, String> params, boolean getAccurateNumOfBuckets, String additionalFilter) {
        if (neededValues != null && neededValues.length > 0) {
            return this.buildQueryJsonFacet(categoriesMap, solrSpecification, filter, field, queryPrefix, valueField,
                    neededValues, queryName, params, getAccurateNumOfBuckets, additionalFilter);
        } else {
            return null;
        }
    }

    public SolrFacetQueryJson buildQueryJsonFacet(Map<String, String> categoriesMap, SolrSpecification solrSpecification,
                                                  FilterDescriptor filter,
                                                  SolrCoreSchema field, String queryPrefix,
                                                  DashboardChartValueTypeDto valueField, Object[] neededValues,
                                                  String queryName, Map<String, String> params, boolean getAccurateNumOfBuckets, String additionalFilter) {

        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(queryName != null ? queryName : filter.getName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(CatFileFieldType.DELETED);
        List<String> filterStrList = new ArrayList<>();
        if (filter != null && (filter.getValue() != null || !filter.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filter, solrSpecification));
        }
        if (!Strings.isNullOrEmpty(additionalFilter)) {
            filterStrList.add(additionalFilter);
        }
        if (!filterStrList.isEmpty()) {
            // this replace is becuase the JSON facet  domain filter need double slash
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }
        Arrays.stream(neededValues)
                .map(Object::toString)
                .collect(Collectors.toList()).forEach((String categoryName) -> {
            SolrFacetQueryJson catFacet = new SolrFacetQueryJson(categoryName);
            catFacet.setType(FacetType.QUERY);
            catFacet.setField(field);
            catFacet.setQueryValueString(categoriesMap.get(categoryName));
            if (!valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
                dashboardChartsUtil.addInnerFacetsAccordingToValue(valueField, getAccurateNumOfBuckets, catFacet);
            }
            theFacet.addFacet(catFacet);
        });
        return theFacet;
    }


    public void parseResponseToDashboardChartDataDto(Map<String, String> categoriesMap,
                                                     DashboardChartDataDto dashboardChartDataDto,
                                                     List<SolrFacetJsonResponseBucket> buckets,
                                                     String seriesName, DashboardChartValueTypeDto valueField,
                                                     Map<String, String> params, boolean getAccurateNumOfBuckets) {

        boolean needToFillCategories = dashboardChartDataDto.getCategories().size() == 0;
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(seriesName);
        ArrayList<Double> dataList = new ArrayList<>(Collections.nCopies(dashboardChartDataDto.getCategories().size(), 0d));
        seriesObj.setData(new ArrayList<>());
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            boolean isDeleted = bucket.getBooleanVal();
            if (!isDeleted) {
                List<String> neededBucketNames = needToFillCategories ? new ArrayList<>(categoriesMap.keySet()) : dashboardChartDataDto.getCategories();
                neededBucketNames.forEach((String categoryName) -> {
                    SolrFacetJsonResponseBucket categoryBucket = bucket.getBucketBasedFacets(categoryName);
                    Double countValue = dashboardChartsUtil.parseInnerBucketValue(dashboardChartDataDto, valueField, getAccurateNumOfBuckets, categoryBucket);
                    if (needToFillCategories) {
                        dashboardChartDataDto.getCategories().add(categoryName);
                        dataList.add(countValue);
                    } else {
                        int categoryIndex = dashboardChartDataDto.getCategories().indexOf(categoryName);
                        if (categoryIndex > -1) {
                            dataList.set(categoryIndex, countValue);
                        }
                    }
                });
            }
        }
        seriesObj.setData(dataList);
        dashboardChartDataDto.getSeries().add(seriesObj);
    }

    @Override
    public void enrichChartData(Map<String, String> initialGetDataResult, DashboardChartDataDto dashboardChartDataDto,
                                Map<String, String> params, CategoryTypeDataEnricher categoryTypeChartEnricher) {
        categoryTypeChartEnricher.enrichChartData(dashboardChartDataDto, params);
    }

    public String getOtherQuery(Map<String, String> categoriesMap,List<String> categories, Map<String, String> params) {
        ArrayList<String> allItems  = new ArrayList<>();
        for (String category :  categories) {
            allItems.add(categoriesMap.get(category));
        }
        return  "(!("+allItems.stream().collect(Collectors.joining(" "))+"))";
    }


    public FilterDescriptor[] getSeriesQueryResults(CatFileFieldType seriesField,
                                              String seriesFieldPrefix,
                                              DashboardChartValueTypeDto valueField,
                                              DataSourceRequest dataSourceRequest,
                                              Map<String, String> params,
                                              String additionalFilter,
                                              CategoryTypeDataEnricher dashboardChartEnricher) {

        DashboardChartDataDto seriesDashboardData = new DashboardChartDataDto();
        Map<String, String> response = setInitialDataToDashboardChartDto(seriesDashboardData,
                seriesField, dataSourceRequest, DashboardChartsUtil.getUnfilterFilterDescriptor(), seriesFieldPrefix, valueField, params, additionalFilter, dashboardChartEnricher);


        List<String> categoriesIds = seriesDashboardData.getCategories();
        enrichChartData(response, seriesDashboardData, params, dashboardChartEnricher);
        List<String> categoriesNames = seriesDashboardData.getCategories();
        FilterDescriptor[] ans = new FilterDescriptor[(categoriesNames.size())];
        for (int count = 0 ; count < categoriesNames.size() ; count++) {
            FilterDescriptor fDescriptor = new FilterDescriptor();
            fDescriptor.setName(categoriesNames.get(count));
            fDescriptor.setField(dashboardChartEnricher.getFilterFieldName());
            fDescriptor.setValue(categoriesIds.get(count));
            fDescriptor.setOperator(KendoFilterConstants.EQ);
            ans[count]=fDescriptor;
        }
        return ans;
    }


    public String getAdditionalFilterByCategories(Map<String, String> categoriesMap, List<String> categories, CatFileFieldType groupedField, CategoryTypeDataEnricher dashboardChartEnricher, Map<String, String> params){
        if (categories == null || categories.size() == 0 ){
            return null;
        }
        return groupedField.getSolrName() + SOLR_QUERY_FILTER_SEPARATOR + categories.stream()
                .map(categoriesMap::get)
                .map((String filterStr)->{
                    return filterStr.substring(1,filterStr.length()-1);
                })
                .collect(Collectors.joining(" ", "(", ")"));

    }

}
