package com.cla.filecluster.service.diagnostics;

import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.google.common.base.Strings;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public interface EntityDiagnosticAction extends DiagnosticAction {

    String START_LIMIT = "START_LIMIT";
    String END_LIMIT = "END_LIMIT";
    String ROOT_FOLDER_ID = "ROOT_FOLDER_ID";
    String SPECIFIC_IDS = "SPECIFIC_IDS";
    String PAGE_PERCENTAGE_TEST = "PAGE_PERCENTAGE_TEST";

    Long getLastEndRangeRun();

    Logger getLogger();

    default long getTimeSlotSize() {
        return TimeUnit.DAYS.toMillis(1L);
    }

    default Diagnostic addDiagnosticActionTimeParts() {
        long min = getLastEndRangeRun() == null ? System.currentTimeMillis() : getLastEndRangeRun();
        long max = min + getTimeSlotSize();

        Map<String, String> opData = new HashMap<>();
        opData.put(START_LIMIT, String.valueOf(min));
        opData.put(END_LIMIT, String.valueOf(max));

        long startTime = max + TimeUnit.MINUTES.toMillis(getStartAfterMin());

        return createNew(opData, startTime);
    }

    default Diagnostic addDiagnosticActionTimeParts(
            String percentageToTest, Long rootFolderId, String specificIds, long startTime) {

        Map<String, String> opData = new HashMap<>();

        if (!Strings.isNullOrEmpty(percentageToTest)) {
            opData.put(PAGE_PERCENTAGE_TEST, percentageToTest);
        }

        if (rootFolderId != null) {
            opData.put(ROOT_FOLDER_ID, String.valueOf(rootFolderId));
        }

        if (!Strings.isNullOrEmpty(specificIds)) {
            opData.put(SPECIFIC_IDS, specificIds);
        }

        return createNew(opData, startTime);
    }

    default List<? extends SolrEntity> getSolrEntitiesSample(List<? extends SolrEntity> entities, float percentageTest, boolean pickRandomly, int pageSize) {
        int realPageSize = (int) (pageSize * percentageTest / 100f + 0.5);
        if (realPageSize <= 0) {
            realPageSize = 1;
        }
        int retrievedItemsCount = entities.size();
        if (entities.size() > realPageSize) {
            if (pickRandomly && realPageSize > 2) {
                // Get a statistical sample of about the right number of entities out of the entire set
                double ratio = percentageTest / 100d;
                entities = entities.stream().filter(f->(Math.random()<ratio)).collect(Collectors.toList());
                realPageSize = entities.size();
                if (getLogger().isTraceEnabled()) {
                    getLogger().trace("Randomly picked {} entities out of {}. ids: {}", realPageSize, retrievedItemsCount,
                            entities.stream().map(SolrEntity::getIdAsString).collect(Collectors.joining(",")));
                } else {
                    getLogger().debug("Randomly picked {} entities out of {}.", realPageSize, retrievedItemsCount);
                }
            } else {
                // Take the first items
                entities = entities.subList(0, realPageSize);
            }
        }
        return entities;
    }
}
