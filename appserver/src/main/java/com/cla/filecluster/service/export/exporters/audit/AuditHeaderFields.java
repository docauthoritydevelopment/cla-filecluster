package com.cla.filecluster.service.export.exporters.audit;

public interface AuditHeaderFields {
    String AUDIT = "Audit";
    String ACTION = "Action";
    String DATE = "Date";
    String DETAILS = "Details";
    String USERNAME = "Username";
}
