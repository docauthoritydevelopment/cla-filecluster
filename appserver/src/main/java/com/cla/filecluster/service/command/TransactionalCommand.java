package com.cla.filecluster.service.command;

import org.hibernate.StaleObjectStateException;

/**
 * Command wrapper to run commands in transaction
 */
public abstract class TransactionalCommand<T> extends BaseCommand<T>{

    private TransactionalCommandService txService;


    @Override
    protected T doExecute() {
        T result;
        try{
            // We first need to call runInTransaction and store it in a variable in order to return value only after
            // transaction was fully committed.
            result = txService.runInTransaction(this);
        }
        catch( StaleObjectStateException  ex ){
            if ( logger.isDebugEnabled() ){
                logger.debug( "Failed to run command:" + ex.getEntityName() + ", retrying once.", ex );
            }
            try {
                result = txService.runInTransaction(this);
            } catch (Exception e) {
                logger.error("Error running transactional command.", e);
                throw e;
            }

        }
        return result;
    }

    void setTxService(TransactionalCommandService txService) {
        this.txService = txService;
    }
}
