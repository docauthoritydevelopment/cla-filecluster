package com.cla.filecluster.service.export.exporters.file.acl;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileUserDto;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.security.FileUserAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
@Slf4j
@Component
public class AclWriteExporter extends PageCsvExcelExporter<AggregationCountItemDTO<FileUserDto>> {

    private final static String[] HEADER = {NAME, NUM_OF_FILES};

    @Autowired
    private FileUserAppService fileUserAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AclAggCountDtoMixin.class);
        mapper.addMixIn(FileUserDto.class, FileUserDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<FileUserDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<FileUserDto>> res = fileUserAppService.getAclFileCounts(
                AclType.WRITE_TYPE, dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<FileUserDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.ACL_WRITE);
    }
}
