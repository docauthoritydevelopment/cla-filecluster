package com.cla.filecluster.service.export.exporters.settings.tag;

/**
 * Created by: yael
 * Created on: 3/12/2019
 */
public class TagExportData {

    private String type;

    private String name;

    private Boolean properties;

    private String typeDescription;

    private String description;

    private Boolean singleValue;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getProperties() {
        return properties;
    }

    public void setProperties(Boolean properties) {
        this.properties = properties;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSingleValue() {
        return singleValue;
    }

    public void setSingleValue(Boolean singleValue) {
        this.singleValue = singleValue;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }
}
