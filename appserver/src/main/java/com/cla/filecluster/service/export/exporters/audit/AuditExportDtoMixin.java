package com.cla.filecluster.service.export.exporters.audit;

import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class AuditExportDtoMixin {
	@JsonProperty(AuditHeaderFields.AUDIT)
	private AuditType auditType;

	@JsonProperty(AuditHeaderFields.ACTION)
	private AuditAction crudAction;

	@JsonProperty(AuditHeaderFields.DATE)
	@JsonSerialize(using = LongToExcelDateSerializer.class)
	private long timestamp;

	@JsonProperty(AuditHeaderFields.DETAILS)
	private String	message;

	@JsonProperty(AuditHeaderFields.USERNAME)
	private String	username;
}
