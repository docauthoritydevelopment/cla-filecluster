package com.cla.filecluster.service.syscomponent;

import com.cla.connector.utils.TimeSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by: yael
 * Created on: 2/11/2018
 */
@Service
public class AppServerStateService {

    private static final Logger logger = LoggerFactory.getLogger(AppServerStateService.class);

    private double analyzeRunningThroughput =0;
    private double averageAnalyzeThroughput =0;

    private AtomicLong analyzeTaskRequests = new AtomicLong(0);
    private AtomicLong analyzeTaskRequestsFinished  = new AtomicLong(0);
    private AtomicLong analyzeTaskRequestFailure  = new AtomicLong(0);

    private long prevAnalyzeTaskRequestsFinishedCounter  ;
    private long prevAnalyzeTaskRequestsFinishedCounterTime ;
    private long prevAnalyzePerTimeCount;

    private final Object analyzeRateLock = new Object();
    private final Object ingestRateLock = new Object();

    private double ingestRunningThroughput =0;
    private double averageIngestThroughput =0;

    private AtomicLong ingestTaskRequests = new AtomicLong(0);
    private AtomicLong ingestTaskRequestsFinished  = new AtomicLong(0);
    private AtomicLong ingestTaskRequestFailure  = new AtomicLong(0);

    private long prevIngestTaskRequestsFinishedCounter  ;
    private long prevIngestTaskRequestsFinishedCounterTime ;

    private AtomicLong dupContentRequests = new AtomicLong(0);
    private AtomicLong dupContentFound  = new AtomicLong(0);

    private TimeSource timeSource = new TimeSource();

    @Scheduled(initialDelayString = "${status.reports.initial.ms:30000}", fixedRateString = "${status.reports.cycle.ms:300000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void periodicStatusReportScheduler() {
        try {
            updateAnalyzeThrouputStats();
            updateIngestThrouputStats();
        } catch (Exception e) {
            logger.error("Failed to send status report to AppServer ({})",e);
        }
    }

    public void incDupContentRequests() {
        dupContentRequests.incrementAndGet();
    }
    public void incDupContentFound() {
        dupContentFound.incrementAndGet();
    }
    public long getDupContentRequests() {
        return dupContentRequests.get();
    }
    public long getDupContentFound() {
        return dupContentFound.get();
    }

    public long getAnalyzeTaskRequests() {
        return analyzeTaskRequests.get();
    }

    public long getAnalyzeTaskRequestsFinished() {
        return analyzeTaskRequestsFinished.get();
    }

    public long getAnalyzeTaskRequestFailure() {
        return analyzeTaskRequestFailure.get();
    }

    public void incAnalyzeTaskRequested() {
        analyzeTaskRequests.incrementAndGet();
    }

    public void incAnalyzeTaskFinished() {
        analyzeTaskRequestsFinished.incrementAndGet();
    }

    public void incAnalyzeTaskRequestFailure() {
        analyzeTaskRequestFailure.incrementAndGet();
    }

    public double getAnalyzeRunningThroughput() {
        return analyzeRunningThroughput;
    }

    public double getAverageAnalyzeThroughput() {
        return averageAnalyzeThroughput;
    }

    public long getIngestTaskRequests() {
        return ingestTaskRequests.get();
    }

    public long getIngestTaskRequestsFinished() {
        return ingestTaskRequestsFinished.get();
    }

    public long getIngestTaskRequestFailure() {
        return ingestTaskRequestFailure.get();
    }

    public void incIngestTaskRequested() {
        ingestTaskRequests.incrementAndGet();
    }

    public void incIngestTaskFinished() {
        ingestTaskRequestsFinished.incrementAndGet();
    }

    public void incIngestTaskRequestFailure() {
        ingestTaskRequestFailure.incrementAndGet();
    }

    public double getIngestRunningThroughput() {
        return ingestRunningThroughput;
    }

    public double getAverageIngestThroughput() {
        return averageIngestThroughput;
    }

    public void updateAnalyzeThrouputStats() {
        synchronized (analyzeRateLock) {
            long currentAnalyze = analyzeTaskRequests.get();
            long analyzePerTime = currentAnalyze - prevAnalyzeTaskRequestsFinishedCounter;

            long currentTime = timeSource.currentTimeMillis();
            double seconds = timeSource.secondsSince(prevAnalyzeTaskRequestsFinishedCounterTime);
            this.analyzeRunningThroughput = (seconds > 0) ? (analyzePerTime / seconds) : 0d;
            //     this.averageAnalyzeThroughput =  this.analyzeRunningThroughput * prevAnalyzePerTimeCount; // TODO Liora: calc this
            logger.debug("Analyze rate {} for the last {} sec. finished analyzes sent {}", averageAnalyzeThroughput, seconds, analyzePerTime);
            prevAnalyzePerTimeCount = analyzePerTime;
            prevAnalyzeTaskRequestsFinishedCounter = currentAnalyze;
            prevAnalyzeTaskRequestsFinishedCounterTime = currentTime;
        }
    }

    public void updateIngestThrouputStats() {
        synchronized (ingestRateLock) {
            long currentIngest = ingestTaskRequests.get();
            long ingestPerTime = currentIngest - prevIngestTaskRequestsFinishedCounter;

            long currentTime = timeSource.currentTimeMillis();
            double seconds = timeSource.secondsSince(prevIngestTaskRequestsFinishedCounterTime);
            this.ingestRunningThroughput = (seconds > 0) ? (ingestPerTime / seconds) : 0d;
            logger.debug("Ingest rate {} for the last {} sec. finished ingests sent {}", averageIngestThroughput, seconds, ingestPerTime);
            prevIngestTaskRequestsFinishedCounter = currentIngest;
            prevIngestTaskRequestsFinishedCounterTime = currentTime;
        }
    }
}
