package com.cla.filecluster.service.media;

import com.cla.common.utils.EncryptionUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.media.*;
import com.cla.connector.utils.TextUtils;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommService;
import com.cla.filecluster.repository.jpa.media.MediaConnectionDetailsRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static com.cla.connector.domain.dto.media.MediaType.FILE_SHARE;

/**
 * Service for managing media connection details
 * Created by uri on 08/08/2016.
 */
@Service
public class MediaConnectionDetailsService {

    private static Logger logger = LoggerFactory.getLogger(MediaConnectionDetailsService.class);

    @Autowired
    private MediaConnectionDetailsRepository mediaConnectionDetailsRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Value("d0c#Pocs${media.encryption.suffix:328753985628935629386}")
    private String encryptionKey;

    @Value("${media.encryption.algorithm:AES/ECB/PKCS5Padding}")
    private String encryptionAlgorithm;

    @Autowired
    private MediaProcessorCommService mediaProcessorCommService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
	private EventAuditService eventAuditService;

    private LoadingCache<Long, MediaConnectionDetailsDto> connectionDetailsLoadingCache;

    private LoadingCache<Long, Optional<Long>> connectionDetailsIdForRootFolderCache;

    private final static TimeSource timeSource = new TimeSource();

    @PostConstruct
    public void init(){
        connectionDetailsLoadingCache = CacheBuilder.newBuilder()
                .maximumSize(10)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, MediaConnectionDetailsDto>() {
                    public MediaConnectionDetailsDto load(Long connectionId) {
                        MediaConnectionDetailsWrapper wrapper = MediaConnectionDetailsWrapper.create();
                        getMediaConnectionDetailsInTx(connectionId, wrapper);
                        return wrapper.getDetails();
                    }
                });

        connectionDetailsIdForRootFolderCache = CacheBuilder.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, Optional<Long>>() {
                    public Optional<Long> load(Long rootFolderId) {
                        return getMediaConnDetailsIdByRootFolderId(rootFolderId);
                    }
                });
    }

    public void cleanCacheMediaConnectionDetailsForRootFolder(Long rootFolderId) {
        try {
            connectionDetailsIdForRootFolderCache.invalidate(rootFolderId);
        } catch (Exception e) {
            logger.info("problem invalidate conn detail cache for root folder "+rootFolderId, e);
        }
    }

    @Transactional
    void isValidForMediaConnectionCreation(String name) {
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            throw new BadRequestException("name", name,messageHandler.getMessage("media-conn.name.empty"), BadRequestType.MISSING_FIELD);
        }
     }

    public void checkAlreadyExists(String name, Long id) {
        MediaConnectionDetails mediaConnectionDetails = mediaConnectionDetailsRepository.findByName(name);
        if (mediaConnectionDetails != null && (id == null || !id.equals(mediaConnectionDetails.getId()))) {
            throw new BadRequestException("name", name,messageHandler.getMessage("media-conn.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    private void isValidUrlForMediaConnectionCreation(String url) {
        if (!Strings.isNullOrEmpty(url)) {
            if (Strings.isNullOrEmpty(url.trim())) {
                throw new BadRequestException("url", url,messageHandler.getMessage("media-conn.url.invalid"), BadRequestType.UNSUPPORTED_VALUE);
            }
            try {
                URL u = new URL(url); // this would check for the protocol
                u.toURI(); // does the extra checking required for validation of URI
            } catch (URISyntaxException|MalformedURLException e) {
                throw new BadRequestException("url", url,messageHandler.getMessage("media-conn.url.invalid"), BadRequestType.UNSUPPORTED_VALUE);
            }
        }
     }

    @Transactional
    public MediaConnectionDetails createConnectionDetails(String name, String username, String url, MediaType mediaType, String encryptedDetails) {
        logger.info("Create Media Connection details {} with user {} url {} mediaType {}", name, username, url,mediaType);

        isValidForMediaConnectionCreation(name);
        checkAlreadyExists(name, null);
        isValidUrlForMediaConnectionCreation(url);
        MediaConnectionDetails  mediaConnectionDetails = MediaConnectionDetails.create();
        mediaConnectionDetails.setMediaType(mediaType);
        mediaConnectionDetails.setConnectionDetailsJson(encryptedDetails);
        mediaConnectionDetails.setName(name);
        mediaConnectionDetails.setOwner(userService.getCurrentUserEntity());
        mediaConnectionDetails.setUsername(username);
        mediaConnectionDetails.setUrl(url);
        mediaConnectionDetails.setPstCacheEnabled(isPstCacheEnabledByDefault(mediaType));
		MediaConnectionDetails connectionDetails = mediaConnectionDetailsRepository.save(mediaConnectionDetails);
		eventAuditService.audit(AuditType.MEDIA_CONNECTION_DETAILS, "Create media connection details", AuditAction.CREATE, MediaConnectionDetails.class, connectionDetails.getId(), connectionDetails);
		return connectionDetails;
    }

    public static boolean isPstCacheEnabledByDefault(MediaType mediaType) {
        return mediaType != FILE_SHARE;
    }

    @Transactional
    MediaConnectionDetailsDto updateConnectionDetails(MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        logger.debug("Update Media Connection details {}", mediaConnectionDetailsDto.getName());

        MediaConnectionDetails mediaConnectionDetails = mediaConnectionDetailsRepository.findByName(mediaConnectionDetailsDto.getName());
        if(mediaConnectionDetails == null) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.name.missing"), BadRequestType.ITEM_NOT_FOUND);

        }
        if (mediaConnectionDetailsDto.getMediaType() != null && !mediaConnectionDetailsDto.getMediaType().equals(mediaConnectionDetails.getMediaType())) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.type.change"), BadRequestType.UNSUPPORTED_VALUE);
        }
        mediaConnectionDetails.setMediaType(mediaConnectionDetailsDto.getMediaType());
        mediaConnectionDetails.setConnectionDetailsJson(mediaConnectionDetailsDto.getConnectionParametersJson());
        mediaConnectionDetails.setUsername(mediaConnectionDetailsDto.getUsername());
        mediaConnectionDetails.setUrl(mediaConnectionDetailsDto.getUrl());
        mediaConnectionDetails.setPstCacheEnabled(mediaConnectionDetailsDto.isPstCacheEnabled());
        mediaConnectionDetails = mediaConnectionDetailsRepository.save(mediaConnectionDetails);
		eventAuditService.audit(AuditType.MEDIA_CONNECTION_DETAILS, "Update media connection details", AuditAction.UPDATE, MediaConnectionDetails.class, mediaConnectionDetails.getId(), mediaConnectionDetails);
        MediaConnectionDetailsDto connectionDetailsDto = convertMediaConnectionDetails(mediaConnectionDetails);
        mediaConnectionDetailsDto.setId(mediaConnectionDetails.getId());
        refreshConnectorDetails(mediaConnectionDetailsDto);
        return connectionDetailsDto;
    }
    @Transactional(readOnly = true)
    public boolean isConnectionDetailsExists(String name) {
        logger.debug("Is Media Connection details {} exists ", name);
        MediaConnectionDetails mediaConnectionDetails = mediaConnectionDetailsRepository.findByName(name);
        return mediaConnectionDetails != null;
    }

    @Transactional
    MediaConnectionDetailsDto createConnectionDetails(MediaConnectionDetailsDto mediaConnectionDetailsDto) {
        isValidForMediaConnectionCreation(mediaConnectionDetailsDto.getName());
        checkAlreadyExists(mediaConnectionDetailsDto.getName(), null);
        MediaConnectionDetails mediaConnectionDetails = new MediaConnectionDetails();
        mediaConnectionDetails.setUrl(mediaConnectionDetailsDto.getUrl());
        mediaConnectionDetails.setConnectionDetailsJson(mediaConnectionDetailsDto.getConnectionParametersJson());
        mediaConnectionDetails.setName(mediaConnectionDetailsDto.getName());
        mediaConnectionDetails.setUsername(mediaConnectionDetailsDto.getUsername());
        mediaConnectionDetails.setMediaType(mediaConnectionDetailsDto.getMediaType());
        mediaConnectionDetails.setPstCacheEnabled(mediaConnectionDetailsDto.isPstCacheEnabled());

        mediaConnectionDetails = mediaConnectionDetailsRepository.save(mediaConnectionDetails);
		eventAuditService.audit(AuditType.MEDIA_CONNECTION_DETAILS, "Create media connection details", AuditAction.CREATE, MediaConnectionDetails.class, mediaConnectionDetails.getId(), mediaConnectionDetails);
        return convertMediaConnectionDetails(mediaConnectionDetails);
    }

    @Transactional(readOnly = true)
    public MediaConnectionDetails getMediaConnectionDetails(String name) {
        return mediaConnectionDetailsRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    public List<MediaConnectionDetails> getMediaConnectionDetails(MediaType mediaType) {
        return mediaConnectionDetailsRepository.listMediaConnectionDetails(mediaType);
    }

    @Transactional(readOnly = true)
    public List<MediaConnectionDetailsDto> listConnections(MediaType mediaType) {
        List<MediaConnectionDetails> details = getMediaConnectionDetails(mediaType);
        details = getDetailsDecryptNoPassword(details);
        return convertMediaConnectionDetailsList(details);
    }

    @Transactional(readOnly = true)
    public List<MediaConnectionDetailsDto> listConnections() {
        List<MediaConnectionDetails> details = getMediaConnectionDetails();
        details = getDetailsDecryptNoPassword(details);
        return convertMediaConnectionDetailsList(details);
    }

    private List<MediaConnectionDetails> getDetailsDecryptNoPassword(List<MediaConnectionDetails> details) {
        details.forEach(d -> {
            if (d.getConnectionDetailsJson() != null) {
                String json = decrypt(d.getConnectionDetailsJson());
                json = TextUtils.removePassword(json);
                d.setConnectionDetailsJson(json);
            }
        });
        return details;
    }

    @Transactional(readOnly = true)
    public MediaConnectionDetailsDto getMediaConnectionDetailsById(Long mediaConnectionDetailsId) {
        return getMediaConnectionDetailsById(mediaConnectionDetailsId,true);
    }

    @Transactional(readOnly = true)
    public MediaConnectionDetailsDto getMediaConnectionDetailsDtoByConnectionId(Long mediaConnectionDetailsId) {
        MediaConnectionDetailsDto connectionDetails = getMediaConnectionDetailsById(mediaConnectionDetailsId);
        switch (connectionDetails.getMediaType()) {
            case SHARE_POINT:
                return getSharePointConnectionDetailsById(mediaConnectionDetailsId, false);
            case ONE_DRIVE:
                return getOneDriveConnectionDetailsById(mediaConnectionDetailsId, false);
			case BOX:
				return getBoxConnectionDetailsById(mediaConnectionDetailsId, false);
            case EXCHANGE_365:
                return getExchangeConnectionDetailsById(mediaConnectionDetailsId, false);
            case MIP:
                return getMicrosoftLabelingConnectionDetailsById(mediaConnectionDetailsId, false);
            default:
                throw new NotSupportedException("Connection type isn't supported");
        }
    }

	@Transactional(readOnly = true)
	public <T extends MediaConnectionDetailsDto> List<T> getMediaConnectionDetailsDtoByType(MediaType mediaType, boolean hideProtectedProps) {
		return getMediaConnectionDetails(mediaType).stream()
				.map(mcd -> convertMediaConnectionDetails(mcd, decrypt(mcd.getConnectionDetailsJson()), hideProtectedProps))
				.map(mcd -> (T)mcd)
				.collect(Collectors.toList());
	}

	private BoxConnectionDetailsDto getBoxConnectionDetailsById(Long mediaConnectionDetailsId, boolean hideProtectedProperties) {
		MediaConnectionDetailsDto details = getMediaConnectionDetailsById(mediaConnectionDetailsId);
		Preconditions.checkNotNull(details, messageHandler.getMessage("media-conn.missing"));
		return (BoxConnectionDetailsDto) details;
	}

    private Exchange365ConnectionDetailsDto getExchangeConnectionDetailsById(Long mediaConnectionDetailsId, boolean hideProtectedProperties) {
        MediaConnectionDetailsDto details = getMediaConnectionDetailsById(mediaConnectionDetailsId);
        Preconditions.checkNotNull(details, messageHandler.getMessage("media-conn.missing"));
        return (Exchange365ConnectionDetailsDto) details;
    }

    private MIPConnectionDetailsDto getMicrosoftLabelingConnectionDetailsById(Long mediaConnectionDetailsId, boolean hideProtectedProperties) {
        MediaConnectionDetailsDto details = getMediaConnectionDetailsById(mediaConnectionDetailsId);
        Preconditions.checkNotNull(details, messageHandler.getMessage("media-conn.missing"));
        return (MIPConnectionDetailsDto) details;
    }

    @Transactional(readOnly = true)
    public Optional<MediaConnectionDetailsDto> getMediaConnectionDetailsByRootFolderId(long rootFolderId, boolean encryptPassword) {
        try {
            Optional<Long> connectionId = connectionDetailsIdForRootFolderCache.getUnchecked(rootFolderId);
            if (!connectionId.isPresent()) {
                return createTemporalConnectionDetails(MediaType.FILE_SHARE);
            }
            return getMediaConnectionDetailsByConnId(connectionId.get(), encryptPassword);
        } catch (Exception e) {
            logger.warn("exception getting media conn details for root folder "+rootFolderId, e);
            return Optional.empty();
        }
    }

	@Transactional(readOnly = true)
    public Optional<MediaConnectionDetailsDto> getMediaConnectionDetailsByConnId(long connectionId, boolean encryptPassword) {
        MediaConnectionDetailsDto connectionDetails = connectionDetailsLoadingCache.getUnchecked(connectionId);
        if (connectionDetails == null) {
            return createTemporalConnectionDetails(MediaType.FILE_SHARE);
        }

        if (encryptPassword) {
            if (connectionDetails instanceof MicrosoftConnectionDetailsDtoBase) {

                MicrosoftConnectionDetailsDtoBase conn = ((MicrosoftConnectionDetailsDtoBase) connectionDetails).copy();
                SharePointConnectionParametersDto params = conn.getSharePointConnectionParametersDto();
                if (params != null) {
                    params.setPassword(encrypt(params.getPassword()));
                }
                connectionDetails = conn;

            } else if (connectionDetails instanceof BoxConnectionDetailsDto) {

                BoxConnectionDetailsDto boxConnectionDetailsDto = (BoxConnectionDetailsDto) connectionDetails;
                BoxConnectionDetailsDto newBoxConnectionDetailsDto = new BoxConnectionDetailsDto();
                BeanUtils.copyProperties(boxConnectionDetailsDto, newBoxConnectionDetailsDto);
                newBoxConnectionDetailsDto.setJwt(encrypt(boxConnectionDetailsDto.getJwt()))
                        .setConnectionParametersJson(encrypt(boxConnectionDetailsDto.getConnectionParametersJson()));
                connectionDetails = newBoxConnectionDetailsDto;

            }  else if (connectionDetails instanceof Exchange365ConnectionDetailsDto) {

                Exchange365ConnectionDetailsDto exchange365ConnectionDetailsDto = (Exchange365ConnectionDetailsDto) connectionDetails;
                Exchange365ConnectionDetailsDto newDto = new Exchange365ConnectionDetailsDto();
                Exchange365ConnectionParametersDto newParamsDto = new Exchange365ConnectionParametersDto();
                BeanUtils.copyProperties(exchange365ConnectionDetailsDto, newDto);
                if (exchange365ConnectionDetailsDto.getExchangeConnectionParametersDto() != null) {
                    BeanUtils.copyProperties(exchange365ConnectionDetailsDto.getExchangeConnectionParametersDto(), newParamsDto);
                    newParamsDto.setPassword(encrypt(newParamsDto.getPassword()));
                }
                newDto.setExchangeConnectionParametersDto(newParamsDto);
                newDto.setConnectionParametersJson(convertExchangeConnectionParamsDtoToJson(newParamsDto));
                connectionDetails = newDto;

            } else if (connectionDetails instanceof MIPConnectionDetailsDto) {
                MIPConnectionDetailsDto MIPConnectionDetailsDto = (MIPConnectionDetailsDto) connectionDetails;
                MIPConnectionDetailsDto newDto = new MIPConnectionDetailsDto();
                MIPConnectionParametersDto newParamsDto = new MIPConnectionParametersDto();
                BeanUtils.copyProperties(MIPConnectionDetailsDto, newDto);
                if (MIPConnectionDetailsDto.getMIPConnectionParametersDto() != null) {
                    BeanUtils.copyProperties(MIPConnectionDetailsDto.getMIPConnectionParametersDto(), newParamsDto);
                }
                newParamsDto.setPassword(encrypt(newParamsDto.getPassword()));
                newDto.setMIPConnectionParametersDto(newParamsDto);
                newDto.setConnectionParametersJson(convertMicrosoftLabelingConnectionParamsDtoToJson(newParamsDto));
                connectionDetails = newDto;
            }
        }
        return Optional.of(connectionDetails);
    }

    @Transactional(readOnly = true)
    private Optional<Long> getMediaConnDetailsIdByRootFolderId(long rootFolderId){
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        if (rootFolder == null){
            return Optional.empty();
        }
        return getMediaConnDetailsIdByRootFolderId(rootFolder);
    }

    @Transactional(readOnly = true)
    private Optional<Long> getMediaConnDetailsIdByRootFolderId(RootFolder rootFolder){
        MediaConnectionDetails mediaConnectionDetails = rootFolder.getMediaConnectionDetails();
        if (mediaConnectionDetails == null) {
            return Optional.empty();
        }
        return Optional.of(mediaConnectionDetails.getId());
    }

    // ------------------ Share Point Specific --------------------------------------------
    @Transactional(readOnly = true)
    public SharePointConnectionDetailsDto getSharePointConnectionDetailsById(long connectionId, boolean hidePassword) {
        SharePointConnectionDetailsDto connDetails = (SharePointConnectionDetailsDto) getMediaConnectionDetailsById(connectionId);
        if (hidePassword) {
            connDetails = connDetails.copy();
            encryptPassword(connDetails);
        }
        return connDetails;
    }

    private void encryptPassword(MicrosoftConnectionDetailsDtoBase connDetails) {
        SharePointConnectionParametersDto params = connDetails.getSharePointConnectionParametersDto();
        if (params != null) {
            String pass = params.getPassword();
            if (!Strings.isNullOrEmpty(pass)) {
                params.setPassword(encrypt(pass));
            }
        }
    }

    @Transactional(readOnly = true)
    public OneDriveConnectionDetailsDto getOneDriveConnectionDetailsById(long connectionId, boolean hidePassword) {
        OneDriveConnectionDetailsDto connDetails = (OneDriveConnectionDetailsDto) getMediaConnectionDetailsById(connectionId);
        if (hidePassword) {
            connDetails = connDetails.copy();
            encryptPassword(connDetails);
        }
        return connDetails;
    }

    private MediaConnectionDetails updatedConnectionDetails(long id, MicrosoftConnectionDetailsDtoBase connectionDetailsDto) {
        SharePointConnectionParametersDto connectionParametersDto = connectionDetailsDto.getSharePointConnectionParametersDto();
        if (connectionParametersDto.getPassword() == null) {// No password was sent - try to fetch from persisted connection.
            MediaConnectionDetailsDto details = Optional.ofNullable(getMediaConnectionDetailsById(id))
                    .orElseThrow(() -> new NotFoundException(messageHandler.getMessage("media-conn.missing")));

            if (details.getMediaType() != MediaType.SHARE_POINT && details.getMediaType() != MediaType.ONE_DRIVE) {
                logger.debug("Connection {} isn't a MS connection {}", id, details);
                throw new RuntimeException(messageHandler.getMessage("media-conn.invalid"));
            }
            String password = ((MicrosoftConnectionDetailsDtoBase) details).getSharePointConnectionParametersDto().getPassword();
            connectionParametersDto.setPassword(password);
        }

        String sharePointParameters = convertSharePointConnectionParamsDtoToJson(connectionParametersDto);
        String encryptedParameters = encrypt(sharePointParameters);
        return updateConnectionDetails(id, connectionDetailsDto.getName(), connectionParametersDto.getUrl(), connectionParametersDto.getUsername(), sharePointParameters, encryptedParameters, Optional.empty());
    }

    @Transactional
    public SharePointConnectionDetailsDto updateSharePointConnectionDetailsById(long id, SharePointConnectionDetailsDto sharePointConnectionDetailsDto) {
        MediaConnectionDetails mediaConnectionDetails = updatedConnectionDetails(id, sharePointConnectionDetailsDto);
        String connectionParams = decrypt(mediaConnectionDetails.getConnectionDetailsJson());
        return convertToSharePointConnectionDetails(mediaConnectionDetails, connectionParams, true);
    }

    @Transactional
    public OneDriveConnectionDetailsDto updateOneDriveConnectionDetailsById(long id, OneDriveConnectionDetailsDto oneDriveConnectionDetailsDto) {
        MediaConnectionDetails mediaConnectionDetails = updatedConnectionDetails(id, oneDriveConnectionDetailsDto);
        String connectionParams = decrypt(mediaConnectionDetails.getConnectionDetailsJson());
        return convertToOneDriveConnectionDetails(mediaConnectionDetails, connectionParams, true);
    }


	private List<MicrosoftConnectionDetailsDtoBase> getConnectionDetails(MediaType mediaType, boolean hidePassword, BiFunction<MediaConnectionDetails, String, MicrosoftConnectionDetailsDtoBase> dtoConversionFunc) {
        List<MediaConnectionDetails> mediaConnectionDetailsList = getMediaConnectionDetails(mediaType);
        return mediaConnectionDetailsList.stream()
                .map(connection -> {
                    String connectionParams = decrypt(connection.getConnectionDetailsJson());
                    if (hidePassword) {
                        connectionParams = TextUtils.removePassword(connectionParams);
                    }
                    return dtoConversionFunc.apply(connection, connectionParams);
                })
                .collect(Collectors.toList());
    }


    @Transactional(readOnly = true)
    public List<MicrosoftConnectionDetailsDtoBase> getSharePointConnectionDetails(boolean hidePassword) {
        BiFunction<MediaConnectionDetails, String, MicrosoftConnectionDetailsDtoBase> conversion =
                (connection, connectionParams) -> convertToSharePointConnectionDetails(connection, connectionParams, hidePassword);
        return getConnectionDetails(MediaType.SHARE_POINT, hidePassword, conversion);
    }

    @Transactional(readOnly = true)
    public List<MicrosoftConnectionDetailsDtoBase> getOneDriveConnectionDetails(boolean hidePassword) {
        BiFunction<MediaConnectionDetails, String, MicrosoftConnectionDetailsDtoBase> conversion =
                (connection, connectionParams) -> convertToOneDriveConnectionDetails(connection, connectionParams, hidePassword);
        return getConnectionDetails(MediaType.ONE_DRIVE, hidePassword, conversion);
    }

    @Transactional(readOnly = true)
    public SharePointConnectionDetailsDto getSharePointConnectionDetailsByName(String name, boolean hidePassword) {
        MediaConnectionDetails mediaConnectionDetails = getMediaConnectionDetails(name);
        String connectionParams = decrypt(mediaConnectionDetails.getConnectionDetailsJson());
        return convertToSharePointConnectionDetails(mediaConnectionDetails, connectionParams, hidePassword);
    }

    @Transactional(readOnly = true)
    public boolean isSharePointConnectionValid(long mediaConnectionDetailsId) {
        SharePointConnectionDetailsDto sharePointConnectionDetailsDto =  getSharePointConnectionDetailsById(mediaConnectionDetailsId,false);
           return  sharePointConnectionDetailsDto != null
                   && sharePointConnectionDetailsDto.getSharePointConnectionParametersDto()!=null
                   && !Strings.isNullOrEmpty(sharePointConnectionDetailsDto.getSharePointConnectionParametersDto().getPassword())
                   && !Strings.isNullOrEmpty(sharePointConnectionDetailsDto.getSharePointConnectionParametersDto().getDomain())
                   && !Strings.isNullOrEmpty(sharePointConnectionDetailsDto.getSharePointConnectionParametersDto().getUsername())
                   && !Strings.isNullOrEmpty(sharePointConnectionDetailsDto.getSharePointConnectionParametersDto().getUrl())
                   ;
    }

//    @Transactional
//    public SharePointConnectionDetailsDto setMediaConnectionFromSharePointConnectionDetails(SharePointConnectionDetailsDto sharePointConnectionDetailsDto) {
//        SharePointConnectionParametersDto sharePointConnectionParametersDto = sharePointConnectionDetailsDto.getSharePointConnectionParametersDto();
//        if (sharePointConnectionParametersDto.getSharePointOnline() == null) {
//            String username = sharePointConnectionParametersDto.getUsername();
//            sharePointConnectionParametersDto.setSharePointOnline(username != null && username.endsWith(".onmicrosoft.com"));
//        }
//        String sharePointDetails = ModelDtoConversionUtils.convertSharePointConnectionParamsDtoToJson(sharePointConnectionParametersDto);
//        String encryptedDetails = encrypt(sharePointDetails);
//        sharePointConnectionDetailsDto.setConnectionParametersJson(encryptedDetails);
//        return sharePointConnectionDetailsDto;
//    }

    @Transactional
    public MicrosoftConnectionDetailsDtoBase configureMicrosoftConnectionDetails(MicrosoftConnectionDetailsDtoBase msConnectionDetailsDto) {
        SharePointConnectionParametersDto msConnectionParametersDto = msConnectionDetailsDto.getSharePointConnectionParametersDto();
        if (msConnectionParametersDto.getSharePointOnline() == null) {
            //Automatically calculate value
            String username = msConnectionParametersDto.getUsername();
            msConnectionParametersDto.setSharePointOnline(username != null && username.endsWith(".onmicrosoft.com"));
        }
        String sharePointDetails = convertSharePointConnectionParamsDtoToJson(msConnectionParametersDto);
        String name = msConnectionDetailsDto.getName() != null ? msConnectionDetailsDto.getName() : generateConnectionName(msConnectionParametersDto);
        String encryptedDetails = encrypt(sharePointDetails);

        MediaConnectionDetails connectionDetails = createConnectionDetails(name,
                msConnectionParametersDto.getUsername(),
                msConnectionParametersDto.getUrl(),
                msConnectionDetailsDto.getMediaType(),
                encryptedDetails);
        String connectionParams = decrypt(connectionDetails.getConnectionDetailsJson());

        switch (msConnectionDetailsDto.getMediaType()) {
            case SHARE_POINT:
                return convertToSharePointConnectionDetails(connectionDetails, connectionParams, true);
            case ONE_DRIVE:
                return convertToOneDriveConnectionDetails(connectionDetails, connectionParams, true);

            default:
                throw new UnsupportedOperationException("Unsuppported media-type " + msConnectionDetailsDto.getMediaType());
        }
    }

    @Transactional
    public MIPConnectionDetailsDto createMicrosoftLabelingConnection(MIPConnectionDetailsDto connectionDetailsDto) {
        validateMicrosoftLabelingParameters(connectionDetailsDto);
        try {
            String exConnectionDetails = ModelDtoConversionUtils.getMapper().writeValueAsString(connectionDetailsDto.getMIPConnectionParametersDto());
            if (!isValidJson(exConnectionDetails)) {
                throw new UnsupportedOperationException(messageHandler.getMessage("media-conn.details.invalid"));
            }
            String encryptedDetails = encrypt(exConnectionDetails);

            String name = connectionDetailsDto.getName() != null ? connectionDetailsDto.getName() : String.format("%s_%s", MediaType.MIP, new Date().getTime() / 1000);
            MediaConnectionDetails connectionDetails = createConnectionDetails(name,
                    connectionDetailsDto.getUsername(),
                    connectionDetailsDto.getUrl(),
                    connectionDetailsDto.getMediaType(),
                    encryptedDetails);
            MediaConnectionDetailsDto mediaConnectionDetailsDto = convertMediaConnectionDetails(connectionDetails, exConnectionDetails, true);
            return (MIPConnectionDetailsDto) mediaConnectionDetailsDto;
        } catch (JsonProcessingException e) {
            logger.error("error create microsoft labeling connection {}", connectionDetailsDto, e);
            throw new RuntimeException(e);
        }
    }

    @Transactional
    public Exchange365ConnectionDetailsDto createExchange365Connection(Exchange365ConnectionDetailsDto connectionDetailsDto) {
        validateExchange365Parameters(connectionDetailsDto);
        try {
            String exConnectionDetails = ModelDtoConversionUtils.getMapper().writeValueAsString(connectionDetailsDto.getExchangeConnectionParametersDto());
            if (!isValidJson(exConnectionDetails)) {
                throw new UnsupportedOperationException(messageHandler.getMessage("media-conn.details.invalid"));
            }
            String encryptedDetails = encrypt(exConnectionDetails);

            String name = connectionDetailsDto.getName() != null ? connectionDetailsDto.getName() : String.format("%s_%s", MediaType.EXCHANGE_365, new Date().getTime() / 1000);
            MediaConnectionDetails connectionDetails = createConnectionDetails(name,
                    connectionDetailsDto.getUsername(),
                    connectionDetailsDto.getUrl(),
                    connectionDetailsDto.getMediaType(),
                    encryptedDetails);
            MediaConnectionDetailsDto mediaConnectionDetailsDto = convertMediaConnectionDetails(connectionDetails, exConnectionDetails, true);
            return (Exchange365ConnectionDetailsDto) mediaConnectionDetailsDto;
        } catch (JsonProcessingException e) {
            logger.error("error create exchange 365 connection {}", connectionDetailsDto, e);
            throw new RuntimeException(e);
        }
    }

    private void validateExchange365Parameters(Exchange365ConnectionDetailsDto connectionDetailsDto) {
        Exchange365ConnectionParametersDto params = connectionDetailsDto.getExchangeConnectionParametersDto();
        if (params == null) {
            throw new BadRequestException("application id", null, messageHandler.getMessage("media-conn.exchange-365.application.empty"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(params.getApplicationId()) || Strings.isNullOrEmpty(params.getApplicationId().trim())) {
            throw new BadRequestException("application id", params.getApplicationId(),messageHandler.getMessage("media-conn.exchange-365.application.empty"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(params.getTenantId()) || Strings.isNullOrEmpty(params.getTenantId().trim())) {
            throw new BadRequestException("tenant id", params.getTenantId(),messageHandler.getMessage("media-conn.exchange-365.tenant.empty"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(params.getPassword()) || Strings.isNullOrEmpty(params.getPassword().trim())) {
            throw new BadRequestException("password", params.getPassword(),messageHandler.getMessage("media-conn.exchange-365.password.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void validateMicrosoftLabelingParameters(MIPConnectionDetailsDto connectionDetailsDto) {

        if (Strings.isNullOrEmpty(connectionDetailsDto.getUsername()) || Strings.isNullOrEmpty(connectionDetailsDto.getUsername().trim())) {
            throw new BadRequestException("username", connectionDetailsDto.getUsername(),messageHandler.getMessage("media-conn.microsoft-labeling.username.empty"), BadRequestType.MISSING_FIELD);
        }

        MIPConnectionParametersDto params = connectionDetailsDto.getMIPConnectionParametersDto();
        if (params == null) {
            throw new BadRequestException("client id", null,messageHandler.getMessage("media-conn.microsoft-labeling.client.empty"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(params.getClientId()) || Strings.isNullOrEmpty(params.getClientId().trim())) {
            throw new BadRequestException("client id", params.getClientId(),messageHandler.getMessage("media-conn.microsoft-labeling.client.empty"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(params.getResource()) || Strings.isNullOrEmpty(params.getResource().trim())) {
            throw new BadRequestException("resource", params.getResource(),messageHandler.getMessage("media-conn.microsoft-labeling.resource.empty"), BadRequestType.MISSING_FIELD);
        }

        if (Strings.isNullOrEmpty(params.getPassword()) || Strings.isNullOrEmpty(params.getPassword().trim())) {
            throw new BadRequestException("password", params.getPassword(),messageHandler.getMessage("media-conn.microsoft-labeling.password.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Transactional
    public BoxConnectionDetailsDto createBoxConnection(BoxConnectionDetailsDto connectionDetailsDto){
		String jwt = connectionDetailsDto.getJwt();
		if(!isValidJson(jwt)){
			throw new UnsupportedOperationException(messageHandler.getMessage("media-conn.box.jwt.invalid"));
		}

		String name = connectionDetailsDto.getName() != null ? connectionDetailsDto.getName() : String.format("%s_%s", MediaType.BOX, new Date().getTime()/1000);
		String encryptedDetails = encrypt(jwt);
		MediaConnectionDetails connectionDetails = createConnectionDetails(name,
				connectionDetailsDto.getUsername(),
				connectionDetailsDto.getUrl(),
				connectionDetailsDto.getMediaType(),
				encryptedDetails);
		MediaConnectionDetailsDto mediaConnectionDetailsDto = convertMediaConnectionDetails(connectionDetails, null, true);
		return (BoxConnectionDetailsDto) mediaConnectionDetailsDto;
	}

	@Transactional
	public BoxConnectionDetailsDto updateConnectionDetails(long id, BoxConnectionDetailsDto boxDto) {
		if(boxDto.getJwt() == null) {
			MediaConnectionDetails existingConnection = mediaConnectionDetailsRepository.getOne(id);
			boxDto.setJwt(decrypt( existingConnection.getConnectionDetailsJson()));
		}
		String encryptedJwt = encrypt(boxDto.getJwt());
		MediaConnectionDetails updatedConnectionDetails = updateConnectionDetails(id, boxDto.getName(), boxDto.getUrl(), boxDto.getUsername(), boxDto.getJwt(), encryptedJwt, Optional.ofNullable(boxDto.getUsername()));
		updatedConnectionDetails.setUsername(boxDto.getUsername());
		return (BoxConnectionDetailsDto) convertMediaConnectionDetails(updatedConnectionDetails, boxDto.getJwt(), true);
	}

    @Transactional
    public Exchange365ConnectionDetailsDto updateConnectionDetails(long id, Exchange365ConnectionDetailsDto exchangeDto) {
        validateExchange365Parameters(exchangeDto);
        try {
            String exConnectionDetails = ModelDtoConversionUtils.getMapper().writeValueAsString(exchangeDto.getExchangeConnectionParametersDto());
            String encryptedDetails = encrypt(exConnectionDetails);
            MediaConnectionDetails updatedConnectionDetails = updateConnectionDetails(id, exchangeDto.getName(), exchangeDto.getUrl(), exchangeDto.getUsername(), exConnectionDetails, encryptedDetails, Optional.ofNullable(exchangeDto.getUsername()));
            return (Exchange365ConnectionDetailsDto) convertMediaConnectionDetails(updatedConnectionDetails, exConnectionDetails, true);
        } catch (JsonProcessingException e) {
            logger.error("error update exchange connection {}", exchangeDto, e);
            throw new RuntimeException(e);
        }
    }

    @Transactional
    public MIPConnectionDetailsDto updateConnectionDetails(long id, MIPConnectionDetailsDto labelingDto) {
        validateMicrosoftLabelingParameters(labelingDto);
        try {
            String exConnectionDetails = ModelDtoConversionUtils.getMapper().writeValueAsString(labelingDto.getMIPConnectionParametersDto());
            String encryptedDetails = encrypt(exConnectionDetails);
            MediaConnectionDetails updatedConnectionDetails = updateConnectionDetails(id, labelingDto.getName(), labelingDto.getUrl(), labelingDto.getUsername(), exConnectionDetails, encryptedDetails, Optional.ofNullable(labelingDto.getUsername()));
            return (MIPConnectionDetailsDto) convertMediaConnectionDetails(updatedConnectionDetails, exConnectionDetails, true);
        } catch (JsonProcessingException e) {
            logger.error("error update microsoft labeling connection {}", labelingDto, e);
            throw new RuntimeException(e);
        }
    }

    public Optional<MediaConnectionDetailsDto> createTemporalConnectionDetails(long rootFolderId){
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        if (rootFolder == null){
            return Optional.empty();
        }
        return createTemporalConnectionDetails(rootFolder.getMediaType());
    }

	public static boolean isValidJson(String validatedJson){
		try {
			new JSONObject(validatedJson);
		} catch (JSONException ex) {
			try {
				new JSONArray(validatedJson);
			} catch (JSONException ex1) {
				return false;
			}
		}
		return true;
	}

    private Optional<MediaConnectionDetailsDto> createTemporalConnectionDetails(MediaType mediaType){
        return Optional.of(convertMediaConnectionDetails(
                MediaConnectionDetails.create().setMediaType(mediaType).
                        setPstCacheEnabled(isPstCacheEnabledByDefault(mediaType)),
                null, false));
    }

    private List<MediaConnectionDetails> getMediaConnectionDetails() {
        return mediaConnectionDetailsRepository.findAll();
    }

    @Transactional
    private MediaConnectionDetails updateConnectionDetails(long id, String name, String url, String username, String connectionDetails, String encryptedDetails, Optional<String> userNameOpt) {
        isValidForMediaConnectionCreation(name);
        checkAlreadyExists(name, id);
        isValidUrlForMediaConnectionCreation(url);
        MediaConnectionDetails mediaConnectionDetails = mediaConnectionDetailsRepository.getOne(id);
        mediaConnectionDetails.setName(name);
        if(userNameOpt.isPresent()){
        	mediaConnectionDetails.setUsername(userNameOpt.get());
		}
        mediaConnectionDetails.setConnectionDetailsJson(encryptedDetails);
        mediaConnectionDetails.setUrl(url);
        mediaConnectionDetails.setUsername(username);
        mediaConnectionDetails = mediaConnectionDetailsRepository.save(mediaConnectionDetails);
		eventAuditService.audit(AuditType.MEDIA_CONNECTION_DETAILS, "Update media connection details", AuditAction.UPDATE, MediaConnectionDetails.class, mediaConnectionDetails.getId(), mediaConnectionDetails);
        if(connectionDetailsLoadingCache.getIfPresent(id) != null) {
            connectionDetailsLoadingCache.refresh(id);
        }
        MediaConnectionDetailsDto connectionDto = convertMediaConnectionDetails(mediaConnectionDetails, connectionDetails, false);
        refreshConnectorDetails(connectionDto);
        return mediaConnectionDetails;
    }

    private void refreshConnectorDetails(MediaConnectionDetailsDto connectionDto) {
        connectionDetailsLoadingCache.refresh(connectionDto.getId());
        if (connectionDto.getMediaType() == MediaType.SHARE_POINT || connectionDto.getMediaType() == MediaType.ONE_DRIVE) {
            SharePointConnectionParametersDto msDetails = ((MicrosoftConnectionDetailsDtoBase) connectionDto).getSharePointConnectionParametersDto();
            String encryptedPassword = encrypt(msDetails.getPassword());
            msDetails.setPassword(encryptedPassword);
            connectionDto.setConnectionParametersJson(null);
        } else if (connectionDto.getMediaType() == MediaType.EXCHANGE_365) {
            Exchange365ConnectionParametersDto params = ((Exchange365ConnectionDetailsDto)connectionDto).getExchangeConnectionParametersDto();
            String encryptedPassword = encrypt(params.getPassword());
            params.setPassword(encryptedPassword);
            connectionDto.setConnectionParametersJson(null);
        } else if (connectionDto.getMediaType() == MediaType.BOX) {
            BoxConnectionDetailsDto dto = ((BoxConnectionDetailsDto)connectionDto);
            String encryptedPassword = encrypt(dto.getJwt());
            dto.setJwt(encryptedPassword);
            dto.setConnectionParametersJson(null);
        } else if (connectionDto.getMediaType() == MediaType.MIP) {
            MIPConnectionParametersDto params = ((MIPConnectionDetailsDto)connectionDto).getMIPConnectionParametersDto();
            String encryptedPassword = encrypt(params.getPassword());
            params.setPassword(encryptedPassword);
            connectionDto.setConnectionParametersJson(null);
        }
        mediaProcessorCommService.sendRefreshMediaConnectorDetails(connectionDto);
    }

    public void deleteMediaConnection(long id) {
        MediaConnectionDetails mediaConnectionDetails = mediaConnectionDetailsRepository.getOne(id);
        logger.debug("Delete media connection {}", mediaConnectionDetails);

        if (!docStoreService.getRootFolderIdsByMediaConnectionId(id).isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.delete.root-folders"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }

        mediaConnectionDetailsRepository.deleteById(id);
        if(connectionDetailsLoadingCache.getIfPresent(id) != null) {
            connectionDetailsLoadingCache.refresh(id);
        }
        eventAuditService.audit(AuditType.MEDIA_CONNECTION_DETAILS, "Delete media connection details", AuditAction.DELETE, MediaConnectionDetails.class, mediaConnectionDetails.getId());
    }

    public List<MediaType> getSupportedMediaTypes() {
        return Lists.newArrayList(MediaType.FILE_SHARE, MediaType.SHARE_POINT, MediaType.ONE_DRIVE, MediaType.BOX, MediaType.EXCHANGE_365, MediaType.MIP);
    }

	public String encrypt(String val) {
        if(Strings.isNullOrEmpty(val)) {
            return val;
        }

		String encryptedVal = null;
		try {
			encryptedVal = EncryptionUtils.encryptSymmetric(val, encryptionKey, encryptionAlgorithm);
		}   catch (Exception e) {
			logger.error("Failed to encrypt String using Algorithm [{}]",encryptionAlgorithm, e);
		}
		return "##" + encryptedVal;
	}

	private String decrypt(String encryptedVal) {
		if(Strings.isNullOrEmpty(encryptedVal)) {
			return encryptedVal;
		}
		encryptedVal = encryptedVal.substring(2);
		String decryptedValue = null;
		try {
			decryptedValue = EncryptionUtils.decryptSymmetric(encryptedVal, encryptionKey, encryptionAlgorithm);
		} catch (Exception e) {
			logger.error("Failed to decrypt string", e);
		}
		return decryptedValue;
	}

	public MediaConnectionDetailsDto getMediaConnectionDetailsById(Long mediaConnectionDetailsId, boolean useCache) {
		if(mediaConnectionDetailsId == null)
		{
			return null;
		}
		if (useCache){
			return connectionDetailsLoadingCache.getUnchecked(mediaConnectionDetailsId);
		}
		MediaConnectionDetails connectionDetails = mediaConnectionDetailsRepository.getOne(mediaConnectionDetailsId);
		String connectionParams = decrypt(connectionDetails.getConnectionDetailsJson());
		return convertMediaConnectionDetails(connectionDetails, connectionParams, false);
	}

    private void getMediaConnectionDetailsInTx(final long connectionId,
                                               final MediaConnectionDetailsWrapper details) {
        Runnable r = () -> {
            try {
                details.setDetails(getMediaConnectionDetailsById(connectionId, false));
            } catch (Exception e) {
                throw new RuntimeException("Transaction failed.", e);
            }
        };
        DBTemplateUtils.doInTransaction(r);
    }

    private String generateConnectionName(SharePointConnectionParametersDto sharePointConnectionParametersDto) {
        String url = sharePointConnectionParametersDto.getUrl();
        try {
            URI uri = new URL(url).toURI();
            return uri.getScheme() + ":" + sharePointConnectionParametersDto.getUsername() + "@" + uri.getHost() + uri.getPath();
        }
        catch (MalformedURLException | URISyntaxException e) {
            logger.warn("Illegal URL passed as sharePoint parameters", e);
        }
        return url;
    }

    /**
     * Wrapper for media connection details
     */
    private static class MediaConnectionDetailsWrapper{
        private MediaConnectionDetailsDto details;

        public static MediaConnectionDetailsWrapper create(){
            return new MediaConnectionDetailsWrapper();
        }

        public MediaConnectionDetailsDto getDetails() {
            return details;
        }

        public void setDetails(MediaConnectionDetailsDto details) {
            this.details = details;
        }
    }

    //-------------- Convert media connection details to DTO -------------------------------------------------------
    public static MediaConnectionDetailsDto convertMediaConnectionDetails(MediaConnectionDetails connectionDetails,
                                                                          String connectionParams, boolean hidePassword) {
        switch (connectionDetails.getMediaType()) {
            case BOX:
                return convertToBoxConnectionDetails(connectionDetails, connectionParams, hidePassword);
            case SHARE_POINT:
                return convertToSharePointConnectionDetails(connectionDetails, connectionParams, hidePassword);
            case ONE_DRIVE:
                return convertToOneDriveConnectionDetails(connectionDetails, connectionParams, hidePassword);
            case EXCHANGE_365:
                return convertToExchange365ConnectionDetails(connectionDetails, connectionParams, hidePassword);
            case MIP:
                return convertToMicrosoftLabelingConnectionDetails(connectionDetails, connectionParams, hidePassword);
            case FILE_SHARE:
            default:
                return convertToFileShareConnectionDetails(connectionDetails);
        }
    }

    private static MIPConnectionDetailsDto convertToMicrosoftLabelingConnectionDetails(MediaConnectionDetails connectionDetails,
                                                                                       String connectionParams, boolean hidePassword) {
        MIPConnectionDetailsDto result = new MIPConnectionDetailsDto();
        if (connectionDetails != null) {
            result.setId(connectionDetails.getId());
            result.setName(connectionDetails.getName());
            result.setUrl(connectionDetails.getUrl());
            result.setUsername(connectionDetails.getUsername());
            //result.setConnectionParametersJson(connectionParams);
            if (!Strings.isNullOrEmpty(connectionParams)) {
                try {
                    MIPConnectionParametersDto param = ModelDtoConversionUtils.getMapper().readValue(connectionParams, MIPConnectionParametersDto.class);
                    result.setMIPConnectionParametersDto(param);
                    if (hidePassword) {
                        param.setPassword(null);
                    }
                } catch (IOException e) {
                    logger.error("failed reading microsoft labeling json param {}", connectionParams, e);
                }
            } else {
                result.setMIPConnectionParametersDto(new MIPConnectionParametersDto());
            }
        }
        return result;
    }

    private static Exchange365ConnectionDetailsDto convertToExchange365ConnectionDetails(MediaConnectionDetails connectionDetails,
                                                                                         String connectionParams, boolean hidePassword) {
        Exchange365ConnectionDetailsDto result = new Exchange365ConnectionDetailsDto();
        if (connectionDetails != null) {
            result.setId(connectionDetails.getId());
            result.setName(connectionDetails.getName());
            result.setUrl(connectionDetails.getUrl());
            result.setUsername(connectionDetails.getUsername());
            //result.setConnectionParametersJson(connectionParams);
            if (!Strings.isNullOrEmpty(connectionParams)) {
                try {
                    Exchange365ConnectionParametersDto param = ModelDtoConversionUtils.getMapper().readValue(connectionParams, Exchange365ConnectionParametersDto.class);
                    result.setExchangeConnectionParametersDto(param);
                    if (hidePassword) {
                        param.setPassword(null);
                    }
                } catch (IOException e) {
                    logger.error("failed reading exchange json param {}", connectionParams, e);
                }
            } else {
                result.setExchangeConnectionParametersDto(new Exchange365ConnectionParametersDto());
            }
        }
        return result;
    }

    private static BoxConnectionDetailsDto convertToBoxConnectionDetails(MediaConnectionDetails connectionDetails,
                                                                         String decryptedJwt,
                                                                         boolean hidePassword) {
        BoxConnectionDetailsDto result = new BoxConnectionDetailsDto();
        if (connectionDetails != null) {
            result.setId(connectionDetails.getId());
            result.setName(connectionDetails.getName());
            result.setUrl(connectionDetails.getUrl());
            result.setUsername(connectionDetails.getUsername());
            //noinspection StatementWithEmptyBody
            if(!hidePassword) {
                result.setJwt(decryptedJwt);//currently the only args are the JWT json
                result.setConnectionParametersJson(decryptedJwt);
                result.setPstCacheEnabled(connectionDetails.isPstCacheEnabled());
            }
            else{
                //result.setJwt(connectionDetails.getConnectionDetailsJson());//currently the only args are the JWT json
                //result.setConnectionParametersJson(connectionDetails.getConnectionDetailsJson());
            }
        }
        return result;
    }

    private static FileShareConnectionDetailsDto convertToFileShareConnectionDetails(MediaConnectionDetails connectionDetails) {
        FileShareConnectionDetailsDto result = new FileShareConnectionDetailsDto();
        if (connectionDetails != null) {
            result.setId(connectionDetails.getId());
            result.setName(connectionDetails.getName());
            result.setUrl(connectionDetails.getUrl());
            result.setUsername(connectionDetails.getUsername());
            result.setPstCacheEnabled(connectionDetails.isPstCacheEnabled());
        }
        return result;
    }

    public static SharePointConnectionDetailsDto convertToSharePointConnectionDetails(MediaConnectionDetails connectionDetails, String connectionParams, boolean hidePassword) {
        SharePointConnectionDetailsDto result = new SharePointConnectionDetailsDto();
        setMicrosoftConnectionDetailsDto(connectionDetails, connectionParams, hidePassword, result);
        return result;
    }

    public static OneDriveConnectionDetailsDto convertToOneDriveConnectionDetails(MediaConnectionDetails connectionDetails, String connectionParams, boolean hidePassword) {
        OneDriveConnectionDetailsDto result = new OneDriveConnectionDetailsDto();
        setMicrosoftConnectionDetailsDto(connectionDetails, connectionParams, hidePassword, result);
        return result;
    }

    private static void setMicrosoftConnectionDetailsDto(MediaConnectionDetails connectionDetails, String connectionParams, boolean hidePassword, MicrosoftConnectionDetailsDtoBase dto) {
        dto.setId(connectionDetails.getId());
        dto.setName(connectionDetails.getName());
        if (connectionParams != null) {
            SharePointConnectionParametersDto sharePointConnectionParametersDto = convertJsonToSharePointConnectionDetails(connectionParams);
            if (hidePassword) {
                sharePointConnectionParametersDto.setPassword(null);
            }
            dto.setSharePointConnectionParametersDto(sharePointConnectionParametersDto);
        } else {
            dto.setSharePointConnectionParametersDto(new SharePointConnectionParametersDto());
        }
        dto.setUrl(connectionDetails.getUrl());
        dto.setMediaType(connectionDetails.getMediaType());
        dto.setUsername(connectionDetails.getUsername());
        dto.setPstCacheEnabled(connectionDetails.isPstCacheEnabled());
    }

    private static SharePointConnectionParametersDto convertJsonToSharePointConnectionDetails(@NotNull String sharePointConnectionDetailsJson) {
        if (sharePointConnectionDetailsJson == null) {
            throw new RuntimeException("sharePointConnectionDetailsJson must not be null");
        }
        try {
            return ModelDtoConversionUtils.getMapper().readValue(sharePointConnectionDetailsJson, SharePointConnectionParametersDto.class);
        } catch (IOException e) {
            logger.error("Failed to convert string to sharePoint connection details: {}", sharePointConnectionDetailsJson, e);
            throw new RuntimeException("Failed to convert string to sharePoint connection details", e);
        }
    }

    public static List<MediaConnectionDetailsDto> convertMediaConnectionDetailsList(List<MediaConnectionDetails> list) {
        List<MediaConnectionDetailsDto> result = new ArrayList<>();
        for (MediaConnectionDetails mediaConnectionDetails : list) {
            result.add(convertMediaConnectionDetails(mediaConnectionDetails));
        }

        return result;
    }

    public static MediaConnectionDetailsDto convertMediaConnectionDetails(MediaConnectionDetails mediaConnectionDetails) {
        MediaConnectionDetailsDto result = new MediaConnectionDetailsDto();
        result.setMediaType(mediaConnectionDetails.getMediaType());
        result.setName(mediaConnectionDetails.getName());
        result.setId(mediaConnectionDetails.getId());
        result.setUrl(mediaConnectionDetails.getUrl());
        result.setPstCacheEnabled(mediaConnectionDetails.isPstCacheEnabled());
        result.setUsername(mediaConnectionDetails.getUsername());
        result.setTimestamp(timeSource.currentTimeMillis());
        result.setConnectionParametersJson(mediaConnectionDetails.getConnectionDetailsJson());
        return result;
    }

    public static String convertSharePointConnectionParamsDtoToJson(SharePointConnectionParametersDto sharePointConnectionParametersDto) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(sharePointConnectionParametersDto);
        } catch (JsonProcessingException e) {
            logger.error("Failed to convert sharePoint connection details to JSON", e);
            throw new RuntimeException("Failed to convert sharePoint connection details to JSON", e);
        }
    }

    public static String convertExchangeConnectionParamsDtoToJson(Exchange365ConnectionParametersDto connectionParams) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(connectionParams);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert Exchange 365 connection details to JSON", e);
        }
    }

    public static String convertMicrosoftLabelingConnectionParamsDtoToJson(MIPConnectionParametersDto connectionParams) {
        try {
            return ModelDtoConversionUtils.getMapper().writeValueAsString(connectionParams);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert Microsoft Labeling connection details to JSON", e);
        }
    }

    //------------------------------------------ end of MediaConnectionDetails DTO handling --------------------------------
}
