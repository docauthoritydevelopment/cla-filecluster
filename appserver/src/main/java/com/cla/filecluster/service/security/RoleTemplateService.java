package com.cla.filecluster.service.security;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.security.CompositeRoleDto;
import com.cla.common.domain.dto.security.RoleTemplateDto;
import com.cla.common.domain.dto.security.RoleTemplateType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.security.Role;
import com.cla.filecluster.domain.entity.security.RoleTemplate;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.RoleTemplateRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Role template holds the individual roles connected to a composite role
 * Created by: yael
 * Created on: 11/30/2017
 */
@Service
public class RoleTemplateService {

    @Autowired
    private RoleTemplateRepository roleTemplateRepository;

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private MessageHandler messageHandler;

    private static final Logger logger = LoggerFactory.getLogger(RoleTemplateService.class);

    @Transactional
    public RoleTemplateDto createRoleTemplate(RoleTemplateDto templateDto) {
        validateTemplateNames(templateDto.getDisplayName(), templateDto.getName());
        RoleTemplate template = convertRoleTemplateDto(templateDto, true);
        return convertRoleTemplate(roleTemplateRepository.save(template), true);
    }

    @Transactional(readOnly = true)
    public List<RoleTemplateDto> getRoleTemplates() {
        return convertRoleTemplates(roleTemplateRepository.findNotDeleted(), true);
    }

    public List<CompositeRoleDto> rolesForTemplate(Long id) {
        List<CompositeRoleDto> result = new ArrayList<>();
        result.addAll(bizRoleService.getBizRolesByRoleTemplateId(id));
        result.addAll(functionalRoleService.getFunctionalRoleDtoByRoleTemplateId(id));
        return result;
    }

    private void validateTemplateNames(String displayName, String name) {
        validateDisplayName(displayName);
        validateName(name);
    }

    private void validateDisplayName(String displayName) {
        RoleTemplate roleTemplateByDisplayName = roleTemplateRepository.getRoleTemplateByDisplayNameAll(displayName);
        if (roleTemplateByDisplayName != null) {
            //Another roleTemplate with that displayName already exists
            if (roleTemplateByDisplayName.isDeleted()) {
                roleTemplateByDisplayName.setDisplayName(roleTemplateByDisplayName.getDisplayName()+System.currentTimeMillis());
                roleTemplateRepository.saveAndFlush(roleTemplateByDisplayName);
            } else {
                throw new BadRequestException(messageHandler.getMessage("role-template.display-name.unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void validateName(String name) {
        RoleTemplate roleTemplateByName = roleTemplateRepository.findByNameAll(name);
        if (roleTemplateByName != null) {
            //Another roleTemplate with that Name already exists
            if (roleTemplateByName.isDeleted()) {
                roleTemplateByName.setName(roleTemplateByName.getName()+System.currentTimeMillis());
                roleTemplateRepository.saveAndFlush(roleTemplateByName);
            } else {
                throw new BadRequestException(messageHandler.getMessage("role-template.name.unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void validateTemplateType(RoleTemplateType roleTemplateType, RoleTemplateType dtoTemplateType) {
        if (!roleTemplateType.equals(dtoTemplateType)) {
            throw new BadRequestException(messageHandler.getMessage("role-template.update.type"), BadRequestType.OTHER);
        }
    }

    @Transactional
    public RoleTemplateDto createRoleTemplate(String name, String displayName, String description, Collection<Role> roles, RoleTemplateType type) {
        validateTemplateNames(displayName, name);
        logger.debug("Create role template {}",name);
        RoleTemplate roleTemplate = new RoleTemplate();
        roleTemplate.setName(name);
        roleTemplate.setDisplayName(displayName);
        roleTemplate.setDescription(description);
        roleTemplate.setTemplateType(type);
        roleTemplate.setRoles(new HashSet<>(roles));
        RoleTemplate roleTemplateUpd = roleTemplateRepository.saveAndFlush(roleTemplate);
        return convertRoleTemplate(roleTemplateUpd, true);
    }

    @Transactional
    public void deleteRoleTemplate(long id) {
        if (rolesForTemplate(id).size() > 0) {
            throw new BadRequestException(messageHandler.getMessage("role-template.delete.has-roles"), BadRequestType.UNAUTHORIZED);
        }

        roleTemplateRepository.findById(id).ifPresent(one -> {
            logger.debug("Delete RoleTemplate {} ",one);
            one.setDeleted(true);
            roleTemplateRepository.save(one);
        });
    }

    @Transactional
    public RoleTemplateDto updateRoleTemplate(RoleTemplateDto dto) {
        Optional<RoleTemplate> optionalOne = roleTemplateRepository.findById(dto.getId());
        if (!optionalOne.isPresent()) {
            logger.error("Could not find RoleTemplate id {}.", dto.getId());
            return null;
        }
        RoleTemplate one = optionalOne.get();
        logger.info("Update roleTemplate {}",one);
        validateTemplateType(one.getTemplateType(), dto.getTemplateType());
        if (!dto.getName().equalsIgnoreCase(one.getName())) {
            validateName(dto.getName());
            one.setName(dto.getName());
        }
        if (!dto.getDisplayName().equalsIgnoreCase(one.getDisplayName())) {
            validateDisplayName(dto.getDisplayName());
            one.setDisplayName(dto.getDisplayName());
        }
        one.setDescription(dto.getDescription());
        if (dto.getSystemRoleDtos()!= null && dto.getSystemRoleDtos().size() > 0) {
            one.setRoles(dto.getSystemRoleDtos().stream().map(RoleService::convertRole).collect(Collectors.toSet()));
        }
        return convertRoleTemplate(roleTemplateRepository.save(one), true);
    }

    @Transactional
    public RoleTemplateDto updateRoleTemplate(Long id, String displayName, String description, Collection<Role> roles, RoleTemplateType type) {
        Optional<RoleTemplate> optionalOne = roleTemplateRepository.findById(id);
        if (!optionalOne.isPresent()){
            logger.error("Could not find RoleTemplate id {}.", id);
            return null;
        }
        RoleTemplate one = optionalOne.get();
        logger.info("Update roleTemplate {}",one);
        validateTemplateType(one.getTemplateType(), type);
        if (!displayName.equalsIgnoreCase(one.getDisplayName())) {
            validateDisplayName(displayName);
            one.setDisplayName(displayName);
        }
        one.setDescription(description);
        if (roles!= null && roles.size() > 0) {
            one.setRoles(new HashSet<>(roles));
        }
        return convertRoleTemplate(roleTemplateRepository.save(one), true);
    }

    @Transactional(readOnly = true)
    public RoleTemplateDto getRoleTemplate(Long roleTemplateId) {
        return convertRoleTemplate(roleTemplateRepository.getOneWithRoles(roleTemplateId), true);
    }

    @Transactional(readOnly = true)
    public RoleTemplateDto getRoleTemplateByName(String name) {
        return convertRoleTemplate(roleTemplateRepository.findByName(name), true);
    }

    @Transactional(readOnly = true)
    public List<RoleTemplateDto> getRoleTemplatesByIds(Collection<Long> roleTemplateIds) {
        return convertRoleTemplates(roleTemplateRepository.findByIds(roleTemplateIds), true);
    }

    @Transactional(readOnly = true)
    public Page<RoleTemplateDto> listRoleTemplates(DataSourceRequest dataSourceRequest, RoleTemplateType type) {
        logger.debug("List all role templates ");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<RoleTemplate> roleTemplates;

        if (type == null || type == RoleTemplateType.NONE) {
            roleTemplates = roleTemplateRepository.findNotDeleted(pageRequest);
        } else if (type == RoleTemplateType.BIZROLE) {
            roleTemplates = roleTemplateRepository.findNotDeletedByNeqType(pageRequest, RoleTemplateType.FUNCROLE);
        } else if (type == RoleTemplateType.FUNCROLE) {
            roleTemplates = roleTemplateRepository.findNotDeletedByNeqType(pageRequest, RoleTemplateType.BIZROLE);
        } else {
            throw new BadRequestException(messageHandler.getMessage("role-template.type.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        List<RoleTemplateDto> res = convertRoleTemplates(roleTemplates.getContent(), true);
        return new PageImpl<>(res, pageRequest, roleTemplates.getTotalElements());
    }

    public static List<RoleTemplateDto> convertRoleTemplates(Collection<RoleTemplate> templates, boolean withSystemRoles) {
        List<RoleTemplateDto> result = new ArrayList<>();
        for (RoleTemplate template : templates) {
            result.add(convertRoleTemplate(template, withSystemRoles));
        }
        return result;
    }

    public static RoleTemplateDto convertRoleTemplate(RoleTemplate roleTemplate, boolean withSystemRoles) {
        if (roleTemplate == null) return null;
        RoleTemplateDto templateDto = new RoleTemplateDto();
        templateDto.setDescription(roleTemplate.getDescription());
        templateDto.setId(roleTemplate.getId());
        templateDto.setDisplayName(roleTemplate.getDisplayName());
        templateDto.setDeleted(roleTemplate.isDeleted());
        templateDto.setName(roleTemplate.getName());
        templateDto.setTemplateType(roleTemplate.getTemplateType());
        if (withSystemRoles && roleTemplate.getRoles() != null) {
            templateDto.setSystemRoleDtos(RoleService.convertRolesToSet(roleTemplate.getRoles()));
        }
        return templateDto;
    }

    public static RoleTemplate convertRoleTemplateDto(RoleTemplateDto roleTemplateDto, boolean withSystemRoles) {
        if (roleTemplateDto == null) return null;
        RoleTemplate template = new RoleTemplate();
        template.setDescription(roleTemplateDto.getDescription());
        template.setId(roleTemplateDto.getId());
        template.setDisplayName(roleTemplateDto.getDisplayName());
        template.setName(roleTemplateDto.getName());
        template.setTemplateType(roleTemplateDto.getTemplateType());
        if (withSystemRoles && roleTemplateDto.getSystemRoleDtos() != null) {
            template.setRoles(roleTemplateDto.getSystemRoleDtos().stream().map(RoleService::convertRole).collect(Collectors.toSet()));
        }
        return template;
    }
}
