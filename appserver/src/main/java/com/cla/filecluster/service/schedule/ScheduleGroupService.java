package com.cla.filecluster.service.schedule;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.schedule.CrawlerPhaseBehavior;
import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.schedule.RootFolderSchedule;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.repository.jpa.schedule.RootFolderScheduleRepository;
import com.cla.filecluster.repository.jpa.schedule.ScheduleGroupRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.util.ScheduleUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by uri on 02/05/2016.
 */
@Service
public class ScheduleGroupService {

    public static final String DEFAULT_SCHEDULE_GROUP = "default";

    @Autowired
    private ScheduleGroupRepository scheduleGroupRepository;

    @Autowired
    private RootFolderScheduleRepository rootFolderScheduleRepository;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    private Logger logger = LoggerFactory.getLogger(ScheduleGroupService.class);

    private LoadingCache<Long, ScheduleGroupDto> scheduleGroupsLoadingCache;

    @PostConstruct
    void init() {
        scheduleGroupsLoadingCache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterWrite(15, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, ScheduleGroupDto>() {
                    public ScheduleGroupDto load(@NotNull Long scheduledGroupId) {
                        return dbTemplateUtils.doInTransaction(() -> {
                            ScheduleGroup scheduleGroup = scheduleGroupRepository.findWithRootFolders(scheduledGroupId);
                            return convertScheduleGroup(scheduleGroup);
                        });
                    }
                });
    }

    public ScheduleGroupDto getGroupById(Long scheduledGroupId) {
        if (scheduledGroupId == null) return null;
        return scheduleGroupsLoadingCache.getUnchecked(scheduledGroupId);
    }

    @Transactional(readOnly = true)
    public Page<ScheduleGroup> listAllScheduleGroups(PageRequest pageRequest, String groupNameSearch, List<Long> groupIds) {
        if (!Strings.isNullOrEmpty(groupNameSearch)) {
            return scheduleGroupRepository.findAllJoinRootFolders(pageRequest, groupNameSearch);
        } else if (groupIds != null && !groupIds.isEmpty()) {
            return scheduleGroupRepository.findAllJoinRootFolders(pageRequest, groupIds);
        } else {
            return scheduleGroupRepository.findAllJoinRootFolders(pageRequest);
        }
    }

    @Transactional(readOnly = true)
    public Map<Long, String> getScheduleGroupsNames() {
        List<Object[]> groups = scheduleGroupRepository.getScheduleGroupsNames();
        Map<Long, String> result = new TreeMap<>();
        for (Object[] group : groups) {
            Long id =  ((BigInteger)group[0]).longValue();
            String name = (String)group[1];
            result.put(id, name);
        }
        return result;
    }

    @Transactional(readOnly = true)
    public ScheduleGroup getScheduleGroupByName(String name) {
        return scheduleGroupRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    public ScheduleGroup getScheduleGroupById(Long id) {
        return scheduleGroupRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    public Page<ScheduleGroup> listScheduleGroupsWithRootFolders(String namingSearchTerm, PageRequest pageRequest, String groupNameSearch, List<Long> groupIds) {
        if (!Strings.isNullOrEmpty(groupNameSearch)) {
            return scheduleGroupRepository.listScheduleGroupsWithRootFoldersByName(pageRequest, groupNameSearch);
        } else if (!Strings.isNullOrEmpty(namingSearchTerm)) {
            return scheduleGroupRepository.listScheduleGroupsWithRootFolders(pageRequest, namingSearchTerm);
        } else if (groupIds != null && !groupIds.isEmpty()) {
            return scheduleGroupRepository.listScheduleGroupsWithRootFolders(pageRequest, groupIds);
        } else {
            return scheduleGroupRepository.listScheduleGroupsWithRootFolders(pageRequest);
        }
    }

    @Transactional(readOnly = true)
    public void isValidForScheduleGroupCreation(String name) {
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            throw new BadParameterException(messageHandler.getMessage("scheduled-group.name.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    public void checkAlreadyExists(String name, Long id) {
        ScheduleGroup scheduleGroup = getScheduleGroupByName(name);
        if (scheduleGroup != null && (id == null || !id.equals(scheduleGroup.getId()))) {
            throw new BadParameterException(messageHandler.getMessage("scheduled-group.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    public void validateSchedulingArgs(ScheduleConfigDto scheduleConfigDto) {
        if (scheduleConfigDto != null && scheduleConfigDto.getScheduleType() == null)
            throw new BadParameterException(messageHandler.getMessage("scheduled-group.schedule-type.empty"), BadRequestType.MISSING_FIELD);
        if (scheduleConfigDto != null)
            ScheduleUtils.convertScheduleConfigDtoToCronTrigger(scheduleConfigDto);
    }

    @Transactional
    public synchronized ScheduleGroupDto createScheduleGroup(ScheduleGroupDto dto) {
        logger.debug("Creating Schedule Group with name {}", dto.getName());

        isValidForScheduleGroupCreation(dto.getName());
        checkAlreadyExists(dto.getName(), null);
        ScheduleConfigDto scheduleConfigDto = dto.getScheduleConfigDto();
        if (scheduleConfigDto != null && scheduleConfigDto.getScheduleType() == null) {
            throw new BadParameterException(messageHandler.getMessage("scheduled-group.schedule-type.empty"), BadRequestType.MISSING_FIELD);
        }

        ScheduleGroup scheduleGroup = new ScheduleGroup();
        scheduleGroup.setName(dto.getName());
        scheduleGroup.setSchedulingDescription(dto.getSchedulingDescription());
        scheduleGroup.setAnalyzePhaseBehavior(dto.getAnalyzePhaseBehavior() == null ?
                CrawlerPhaseBehavior.EVERY_ROOT_FOLDER : dto.getAnalyzePhaseBehavior());

        scheduleGroup.setResumeAfterRestart(true);
        if (scheduleConfigDto != null) {
            String cronTriggerString = ScheduleUtils.convertScheduleConfigDtoToCronTrigger(scheduleConfigDto);
            scheduleGroup.setSchedulingJson(ScheduleUtils.convertScheduleConfigDtoToString(scheduleConfigDto));
            scheduleGroup.setCronTriggerString(cronTriggerString);
        }
        if (dto.getActive() != null) {
            scheduleGroup.setActive(dto.getActive());
        }
        if (scheduleGroup.isActive() && StringUtils.isEmpty(scheduleGroup.getCronTriggerString())) {
            logger.info("New Schedule Group {} does not have cronTrigger, will be set as not active.", dto.getName());
        }

        scheduleGroup = scheduleGroupRepository.save(scheduleGroup);

        return convertScheduleGroup(scheduleGroup);
    }

    @Transactional
    public RootFolderSchedule attachRootFolderToScheduleGroup(Long scheduleGroupId, RootFolder rootFolder) {
        ScheduleGroup scheduleGroup = scheduleGroupRepository.findById(scheduleGroupId).orElse(null);
        if (scheduleGroup == null) {
            logger.warn("Failed to find scheduleGroupId {}", scheduleGroupId);
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        logger.debug("attach RootFolder To ScheduleGroup {}", rootFolder, scheduleGroup);
        RootFolderSchedule rootFolderSchedule = new RootFolderSchedule();
        rootFolderSchedule.setRootFolder(rootFolder);
        rootFolderSchedule.setScheduleGroup(scheduleGroup);
        rootFolderSchedule.setNumber(rootFolder.getId()); // Attach to schedule group and assign default order
        logger.debug("attached RootFolder To ScheduleGroup {} using {}", rootFolder, scheduleGroup, rootFolderSchedule);
        return rootFolderScheduleRepository.save(rootFolderSchedule);
    }

    @Transactional
    public void detachRootFolderFromAllScheduleGroups(Long rootFolderId) {
        logger.info("Detach rootFolder {} from all schedule groups", rootFolderId);
        List<RootFolderSchedule> schedules = findRootFolderSchedules(rootFolderId);
        if (schedules.size() > 0) {
            logger.debug("Detaching rootFolder {} from {} schedules", rootFolderId, schedules.size());
            for (RootFolderSchedule schedule : schedules) {
                stopScheduledGroupRunIfNeeded(schedule.getScheduleGroup(), rootFolderId);
            }
            rootFolderScheduleRepository.deleteAll(schedules);
        }
        RootFolder rootFolder = docStoreService.getRootFolderAllState(rootFolderId);
        detachLastRunIfNeeded(rootFolder);
    }

    @Transactional(readOnly = true)
    public List<RootFolderSchedule> findRootFolderSchedules(Long rootFolderId) {
        return rootFolderScheduleRepository.findRootFolderSchedules(rootFolderId);
    }

    @Transactional(readOnly = true)
    public ScheduleGroupDto getRootFolderScheduleGroup(Long rootFolderId, boolean withRootFolders) {
        List<RootFolderSchedule> rootFolderSchedules = findRootFolderSchedules(rootFolderId);
        if (!rootFolderSchedules.isEmpty()) {
            return convertScheduleGroup(rootFolderSchedules.get(0).getScheduleGroup(), withRootFolders);
        }
        return null;
    }

    @Transactional
    public synchronized ScheduleGroupDto updateScheduleGroup(ScheduleGroupDto scheduleGroupDto) {
        Long id = scheduleGroupDto.getId();
        isValidForScheduleGroupCreation(scheduleGroupDto.getName());
        checkAlreadyExists(scheduleGroupDto.getName(), id);
        ScheduleGroup scheduleGroup = scheduleGroupRepository.findWithRootFolders(id);
        ScheduleConfigDto scheduleConfigDto = scheduleGroupDto.getScheduleConfigDto();
        if (scheduleConfigDto != null) {
            String cronTriggerString = ScheduleUtils.convertScheduleConfigDtoToCronTrigger(scheduleConfigDto);
            scheduleGroup.setCronTriggerString(cronTriggerString);
            scheduleGroup.setSchedulingJson(ScheduleUtils.convertScheduleConfigDtoToString(scheduleConfigDto));
        }
        scheduleGroup.setActive(scheduleGroupDto.isActive());
        scheduleGroup.setAnalyzePhaseBehavior(scheduleGroupDto.getAnalyzePhaseBehavior());
        scheduleGroup.setName(scheduleGroupDto.getName());
        scheduleGroup.setSchedulingDescription(scheduleGroupDto.getSchedulingDescription());
        if (scheduleGroup.isActive() && StringUtils.isEmpty(scheduleGroup.getCronTriggerString())) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.active.empty"), BadRequestType.MISSING_FIELD);
        }
        scheduleGroup = scheduleGroupRepository.save(scheduleGroup);

        scheduleGroupsLoadingCache.invalidate(scheduleGroup.getId());

        return convertScheduleGroup(scheduleGroup);
    }

    @Transactional
    public void deleteScheduleGroup(Long id) {
        Optional<ScheduleGroup> scheduleGroup = scheduleGroupRepository.findById(id);
        if (!scheduleGroup.isPresent()) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        List<RootFolder> rfs = rootFolderScheduleRepository.findRootFoldersByScheduleGroup(id);
        if (rfs != null && !rfs.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.delete.root-folders"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }
        scheduleGroupRepository.deleteById(id);
        scheduleGroupsLoadingCache.invalidate(id);
    }

    @Transactional(readOnly = true)
    public ScheduleGroupDto getScheduleGroup(Long scheduleGroupId) {
        return convertScheduleGroup(scheduleGroupRepository.findWithRootFolders(scheduleGroupId));
    }

    @Transactional
    public void detachRootFolderFromScheduleGroup(ScheduleGroupDto scheduleGroupDto, RootFolder rootFolder) {
        List<RootFolderSchedule> schedules = findRootFolderSchedules(rootFolder.getId());
        for (RootFolderSchedule schedule : schedules) {
            if (schedule.getRootFolder().getId().equals(rootFolder.getId())) {
                logger.info("detach root folder {} from schedule group {}", rootFolder, scheduleGroupDto);
                stopScheduledGroupRunIfNeeded(schedule.getScheduleGroup(), rootFolder.getId());
                rootFolderScheduleRepository.deleteById(schedule.getId());
                detachLastRunIfNeeded(rootFolder);
            }
        }
    }

    private void stopScheduledGroupRunIfNeeded(ScheduleGroup sc, Long currentRootFolderId) {
        if (sc.getLastRunId() == null) return;

        crawlRunRepository.findById(sc.getLastRunId())
                .filter(run -> !run.getRunStatus().isFinished())
                .ifPresent(run -> {
                    boolean hasAnotherRFRunning = false;
                    List<RootFolderSchedule> rfd = rootFolderScheduleRepository.findRootFolderSchedulesByScheduleGroup(sc.getId());
                    for (RootFolderSchedule rfsc : rfd) {
                        if (!rfsc.getRootFolder().getId().equals(currentRootFolderId) && rfsc.getRootFolder().getLastRunId() != null) {
                            if (crawlRunRepository.findById(rfsc.getRootFolder().getLastRunId())
                                    .filter(lastRun -> !lastRun.getRunStatus().isFinished())
                                    .isPresent())
                                hasAnotherRFRunning = true;
                            break;
                        }
                    }

                    if (!hasAnotherRFRunning) {
                        fileCrawlerExecutionDetailsService.updateCrawlRunState(run.getId(), RunStatus.STOPPED);
                    }
                });

    }

    private void detachLastRunIfNeeded(RootFolder rootFolder) {
        if (rootFolder.getLastRunId() != null && rootFolder.getLastRunId() > 0) {
            crawlRunRepository.findById(rootFolder.getLastRunId())
                    .filter(run -> !run.getRunStatus().isFinished()
                            && run.getParentRunId() != null && run.getParentRunId() > 0)
                    .ifPresent(run -> {
                        run.setParentRunId(null);
                        crawlRunRepository.save(run);
                    });
        }
    }

    public List<ScheduleGroup> listAllActiveScheduleGroups() {
        return scheduleGroupRepository.findActiveWithRootFolders();
    }

    @Transactional
    public void markScheduleGroupAsMissed(Long groupId) {
        scheduleGroupRepository.findById(groupId)
                .ifPresent(scheduleGroup -> {
                    logger.warn("Schedule group {} was scheduled for run, but another run is currently active. Not Running", scheduleGroup);
                    if (scheduleGroup.getMissedScheduleTimeStamp() == null) {
                        scheduleGroup.setMissedScheduleTimeStamp(System.currentTimeMillis());
                    }
                    scheduleGroupRepository.save(scheduleGroup);
                });
    }

    @Transactional(readOnly = true)
    public List<RootFolderSchedule> listRootFolderSchedulesOnScheduleGroup(Long groupId) {
        return rootFolderScheduleRepository.findRootFolderSchedulesByScheduleGroup(groupId);
    }

    @Transactional(readOnly = true)
    public Page<RootFolder> listScheduleGroupRootFolders(Long scheduleGroupId, PageRequest pageRequest) {
        return rootFolderScheduleRepository.findRootFoldersByScheduleGroup(scheduleGroupId, pageRequest);
    }

    @Transactional(readOnly = true)
    public List<RootFolder> listScheduleGroupRootFolders(Long scheduleGroupId) {
        return rootFolderScheduleRepository.findRootFoldersByScheduleGroup(scheduleGroupId);
    }

    @Transactional(readOnly = true)
    public Page<RootFolder> listScheduleGroupRootFolders(Long scheduleGroupId, String namingSearchTerm, PageRequest pageRequest) {
        return rootFolderScheduleRepository.findLikeNamingByScheduleGroup(scheduleGroupId, namingSearchTerm, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<RootFolder> listScheduleGroupRootFolders(Long scheduleGroupId, String namingSearchTerm,
                                                         List<RunStatus> filterByRunStatus, PageRequest pageRequest) {
        List<RootFolder> list;
        List<String> statuses = filterByRunStatus.stream().map(Enum::name).collect(Collectors.toList());
        long count = rootFolderScheduleRepository.findLikeNamingByScheduleGroupWithStatesNum(scheduleGroupId, namingSearchTerm, statuses);
        if (count > 0) {
            List<BigInteger> listResIds = rootFolderScheduleRepository
                    .findLikeNamingByScheduleGroupWithStates(scheduleGroupId, namingSearchTerm, statuses,
                            pageRequest.getOffset(), pageRequest.getPageSize());
            list = docStoreService.findByIds(listResIds.stream().map(BigInteger::longValue).collect(Collectors.toList()));
        }
        else{
            list = Lists.newArrayList();
        }
        return new PageImpl<>(list, pageRequest, count);
    }

    @Transactional(readOnly = true)
    public List<RootFolderDto> listScheduleGroupRootFolderDtos(Long scheduleGroupId) {
        List<RootFolder> rootFoldersByScheduleGroup = rootFolderScheduleRepository.findRootFoldersByScheduleGroup(scheduleGroupId);
        Long defaultDocStore = docStoreService.getDefaultDocStoreId();
        return DocStoreService.convertRootFoldersToDto(rootFoldersByScheduleGroup, defaultDocStore, false);
    }

    @Transactional
    public void attachRunToScheduleGroup(Long runId, Long scheduleGroupId) {
        logger.debug("Attach run {} to schedule group {}", runId, scheduleGroupId);
        scheduleGroupRepository.findById(scheduleGroupId)
              .ifPresent(one -> {
                  one.setLastRunId(runId);
                  scheduleGroupRepository.save(one);
              });
    }

    @Transactional(readOnly = true)
    public Page<ScheduleGroup> listRootFolderScheduleGroups(Long rootFolderId, PageRequest pageRequest) {
        return scheduleGroupRepository.findRootFolderScheduleGroups(rootFolderId, pageRequest);
    }

    public void fillScheduleGroupDetails(RootFolderSummaryInfo rootFolderSummaryInfo, CrawlRun crawlRun) {
        if (crawlRun.getScheduleGroupId() != null) {
            ScheduleGroupDto scheduleGroup = getScheduleGroup(crawlRun.getScheduleGroupId());
            if (scheduleGroup != null) {
                rootFolderSummaryInfo.getCrawlRunDetailsDto().setScheduleGroupName(scheduleGroup.getName());
            }
            else {
                logger.warn("CrawlRun is connected to a schedule group that was probably deleted ({})", crawlRun.getScheduleGroupId());
            }
        }
    }

    @Transactional(readOnly = true)
    public void fillScheduleGroupDetails(Page<CrawlRunDetailsDto> crawlRunDetailsDtos) {
        Set<Long> scheduleGroupIds = new HashSet<>();
        for (CrawlRunDetailsDto crawlRunDetailsDto : crawlRunDetailsDtos) {
            if (crawlRunDetailsDto.getScheduleGroupId() != null) {
                scheduleGroupIds.add(crawlRunDetailsDto.getScheduleGroupId());
            }
        }
        if (scheduleGroupIds.size() == 0) {
            return;
        }
        List<ScheduleGroup> scheduleGroups = scheduleGroupRepository.findByIds(scheduleGroupIds);
        Map<Long, String> scheduleGroupNameMap = scheduleGroups.stream().
                collect(Collectors.toMap(ScheduleGroup::getId, ScheduleGroup::getName));

        for (CrawlRunDetailsDto crawlRunDetailsDto : crawlRunDetailsDtos) {
            if (crawlRunDetailsDto.getScheduleGroupId() != null) {
                crawlRunDetailsDto.setScheduleGroupName(scheduleGroupNameMap.get(crawlRunDetailsDto.getScheduleGroupId()));
            }
        }
    }

    @Transactional
    public void deleteAllConnections() {
        rootFolderScheduleRepository.deleteAll();
    }

    public static ScheduleGroupDto convertScheduleGroup(ScheduleGroup scheduleGroup) {
        return convertScheduleGroup(scheduleGroup, true);
    }

    public static ScheduleGroupDto convertScheduleGroup(ScheduleGroup scheduleGroup, boolean includeRootfolders) {
        if (scheduleGroup == null) return null;
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setId(scheduleGroup.getId());
        scheduleGroupDto.setAnalyzePhaseBehavior(scheduleGroup.getAnalyzePhaseBehavior());
        scheduleGroupDto.setName(scheduleGroup.getName());
        scheduleGroupDto.setActive(scheduleGroup.isActive());
        scheduleGroupDto.setLastRunId(scheduleGroup.getLastRunId());
        scheduleGroupDto.setCronTriggerString(scheduleGroup.getCronTriggerString());
        scheduleGroupDto.setSchedulingDescription(scheduleGroup.getSchedulingDescription());
        ScheduleConfigDto scheduleConfigDto = ScheduleUtils.convertScheduleConfigDtoFromString(scheduleGroup.getSchedulingJson());
        scheduleGroupDto.setScheduleConfigDto(scheduleConfigDto);

        if (includeRootfolders) {
            List<Long> rootFoldersIds = new ArrayList<>();
            if (scheduleGroup.getRootFolderSchedules() != null) {
                for (RootFolderSchedule rootFolderSchedule : scheduleGroup.getRootFolderSchedules()) {
                    rootFoldersIds.add(rootFolderSchedule.getRootFolder().getId());
                }
            }
            scheduleGroupDto.setRootFolderIds(rootFoldersIds);
        }
        return scheduleGroupDto;
    }

    public static Page<ScheduleGroupSummaryInfoDto> convertScheduleGroupsSummaryInfo(Page<ScheduleGroup> scheduleGroups, PageRequest pageRequest) {
        List<ScheduleGroupSummaryInfoDto> content = new ArrayList<>();
        for (ScheduleGroup scheduleGroup : scheduleGroups.getContent()) {
            content.add(convertScheduleGroupSummaryInfo(scheduleGroup));
        }

        return new PageImpl<>(content, pageRequest, scheduleGroups.getTotalElements());
    }

    public static ScheduleGroupSummaryInfoDto convertScheduleGroupSummaryInfo(ScheduleGroup scheduleGroup) {
        ScheduleGroupSummaryInfoDto scheduleGroupSummaryInfoDto = new ScheduleGroupSummaryInfoDto();
        scheduleGroupSummaryInfoDto.setScheduleGroupDto(convertScheduleGroup(scheduleGroup));
        scheduleGroupSummaryInfoDto.setMissedScheduleTimeStamp(scheduleGroup.getMissedScheduleTimeStamp());
        return scheduleGroupSummaryInfoDto;
    }

    public static Set<ScheduleGroupDto> convertRootFolderSchedulesMinimal(List<RootFolderSchedule> rootFolderSchedules) {
        Set<ScheduleGroupDto> content = new HashSet<>();
        for (RootFolderSchedule rootFolderSchedule : rootFolderSchedules) {
            content.add(convertScheduleGroupMinimal(rootFolderSchedule.getScheduleGroup()));
        }
        return content;
    }

    private static ScheduleGroupDto convertScheduleGroupMinimal(ScheduleGroup scheduleGroup) {
        ScheduleGroupDto result = new ScheduleGroupDto();
        result.setId(scheduleGroup.getId());
        result.setName(scheduleGroup.getName());
        result.setSchedulingDescription(scheduleGroup.getSchedulingDescription());
        result.setActive(scheduleGroup.isActive());
        return result;
    }

    public static Page<ScheduleGroupDto> convertScheduleGroups(Page<ScheduleGroup> scheduleGroups, PageRequest pageRequest) {
        List<ScheduleGroupDto> content = new ArrayList<>();
        for (ScheduleGroup scheduleGroup : scheduleGroups.getContent()) {
            content.add(convertScheduleGroup(scheduleGroup));
        }

        return new PageImpl<>(content, pageRequest, scheduleGroups.getTotalElements());
    }
}
