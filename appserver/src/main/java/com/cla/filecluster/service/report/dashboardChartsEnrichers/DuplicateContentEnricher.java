package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.repository.solr.FacetType;
import com.cla.filecluster.repository.solr.SolrFacetJsonResponseBucket;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component

public class DuplicateContentEnricher implements DefaultTypeDataEnricher {

    @Override
    public void enrichJSONFacetQuery(SolrFacetQueryJson solrFacetQueryJson, FilterDescriptor filter) {
        SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.SORT_NAME.getSolrName());
        innerFacet.setType(FacetType.TERMS);
        innerFacet.setField(CatFileFieldType.SORT_NAME);
        innerFacet.setLimit(1);
        solrFacetQueryJson.addFacet(innerFacet);
        solrFacetQueryJson.setMincount(2);  // show only results when there are at least 2 duplications
    }

    @Override
    public String getEnricherType() {
        return CatFileFieldType.CONTENT_ID.getSolrName();
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        Map<String, String> categoryMap = new HashMap<>();
        dashboardChartDataDto.getSeries().forEach(series -> {
            List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse.getResponseBucket(series.getName())
                    .getBuckets();
            for (SolrFacetJsonResponseBucket bucket : buckets) {
                String oldCategory = bucket.getStringVal();
                SolrFacetJsonResponseBucket innerResult = bucket.getBucketBasedFacets(CatFileFieldType.SORT_NAME.getSolrName());
                List<SolrFacetJsonResponseBucket> innerBuckets = innerResult.getBuckets();
                String newCategory = innerBuckets.get(0).getStringVal();
                categoryMap.put(oldCategory, newCategory);
            }
        });

        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream()
                .map(categoryMap::get)
                .collect(Collectors.toList()));
    }
}
