package com.cla.filecluster.service.audit;

import ch.qos.logback.classic.db.names.DBNameResolver;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.db.DBAppenderBase;
import com.cla.filecluster.domain.dto.AuditDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by oren on 08/10/2017.
 */
public class AuditDBAppender extends DBAppenderBase<ILoggingEvent> {
    final static Logger logger = LoggerFactory.getLogger("audit.event.log");

    public static final int TIME_STAMP_INDEX = 1;
    public static final int USERNAME_INDEX = 2;
    public static final int SUB_SYSTEM_INDEX = 3;
    public static final int MESSAGE_INDEX = 4;
    public static final int ACTION_INDEX = 5;
    public static final int OBJECT_TYPE_INDEX = 6;
    public static final int OBJECT_ID_INDEX =7;
    public static final int ON_OBJECT_TYPE_INDEX = 8;
    public static final int ON_OBJECT_ID_INDEX =9;
    public static final int PARAMS_INDEX =10;

    protected String insertSQL;
    protected static final Method GET_GENERATED_KEYS_METHOD;

    private DBNameResolver dbNameResolver;


    static {
        Method getGeneratedKeysMethod;
        try {
            getGeneratedKeysMethod = PreparedStatement.class.getMethod( "getGeneratedKeys", (Class[]) null);
        } catch (Exception ex) {
            getGeneratedKeysMethod = null;
        }
        GET_GENERATED_KEYS_METHOD = getGeneratedKeysMethod;
    }

    public AuditDBAppender() {

    }

    private String buildInsertSQL(DBNameResolver dbNameResolver) {
        StringBuilder sqlBuilder = new StringBuilder("INSERT INTO ");
        sqlBuilder.append(dbNameResolver.getTableName(AuditTables.AUDIT)).append(" (");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.TIME_STAMP)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.USERNAME)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.SUB_SYSTEM)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.MESSAGE)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.ACTION)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.OBJECT_TYPE)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.OBJECT_ID)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.ON_OBJECT_TYPE)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.ON_OBJECT_ID)).append(", ");
        sqlBuilder.append(dbNameResolver.getColumnName(AuditColumns.PARAMS)).append(") ");
        sqlBuilder.append("VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

        return sqlBuilder.toString();
    }

    @Override
    public void start() {
        super.start();
        if (dbNameResolver == null) {
            dbNameResolver = new AuditDBNameResolver();
        }

        insertSQL = buildInsertSQL(dbNameResolver);
    }

    @Override
    protected void subAppend(ILoggingEvent event, Connection connection, PreparedStatement insertStatement) throws Throwable {
        bindLoggingEventWithInsertStatement(insertStatement, event);
        logger.debug("Audit insert statement: {}", insertStatement);

        int updateCount = -1;
        try {
            updateCount = insertStatement.executeUpdate();
        } catch (Exception e) {
            logger.error("Failed to insert audit record", e);
        }

        logger.debug("updateCount = {}", updateCount);

        if (updateCount != 1) {
            logger.error("Failed to insert audit record");
        }
    }

    @Override
    protected void secondarySubAppend(ILoggingEvent event, Connection connection, long eventId) throws Throwable {

    }

    private void bindLoggingEventWithInsertStatement(PreparedStatement stmt, ILoggingEvent event) throws SQLException {
        Object logdata[] = event.getArgumentArray();
        if (logdata == null || logdata.length == 0 || !(logdata[0] instanceof AuditDto)) {
            throw new IllegalArgumentException("Can't parse audit without data: Are you passing an AuditDto object to logger argument?");
        }

        AuditDto auditDto = (AuditDto) logdata[0];

        stmt.setLong(TIME_STAMP_INDEX, auditDto.getTimestamp());
        stmt.setString(USERNAME_INDEX, auditDto.getUsername());
        stmt.setString(SUB_SYSTEM_INDEX, auditDto.getAuditType().name());
        stmt.setString(MESSAGE_INDEX, auditDto.getMessage());
        stmt.setString(ACTION_INDEX, auditDto.getCrudAction().name());
        stmt.setString(OBJECT_TYPE_INDEX, auditDto.getObjectType());
        stmt.setString(OBJECT_ID_INDEX, auditDto.getObjectId());
        stmt.setString(ON_OBJECT_TYPE_INDEX, auditDto.getOnObjectType());
        stmt.setString(ON_OBJECT_ID_INDEX, auditDto.getOnObjectId());
        stmt.setString(PARAMS_INDEX, auditDto.getParams());
    }

    @Override
    protected Method getGeneratedKeysMethod() {
        return GET_GENERATED_KEYS_METHOD;
    }

    @Override
    protected String getInsertSQL() {
        return insertSQL;
    }
}