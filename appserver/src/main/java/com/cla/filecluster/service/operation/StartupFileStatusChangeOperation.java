package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.service.crawler.analyze.FileGroupingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * upon system startup we change back files statuses so will be handled appropriately
 *
 * Created by: yael
 * Created on: 11/22/2018
 */
@Service
public class StartupFileStatusChangeOperation implements ScheduledOperation {

    @Autowired
    private FileGroupingService fileGroupingService;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            fileGroupingService.changeState(ClaFileState.STARTED_INGESTING, ClaFileState.SCANNED);
            fileGroupingService.changeState(ClaFileState.STARTED_ANALYSING, ClaFileState.INGESTED);
            fileGroupingService.changeContentState(ClaFileState.STARTED_INGESTING, ClaFileState.SCANNED);
            fileGroupingService.changeContentState(ClaFileState.STARTED_ANALYSING, ClaFileState.INGESTED);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.STARTUP_FILE_STATUS_CNG;
    }

    @Override
    public boolean shouldAllowCreateWhileScheduleDown() {
        return true;
    }
}
