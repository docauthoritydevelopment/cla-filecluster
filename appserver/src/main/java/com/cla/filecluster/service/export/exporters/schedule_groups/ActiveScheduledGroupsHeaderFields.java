package com.cla.filecluster.service.export.exporters.schedule_groups;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface ActiveScheduledGroupsHeaderFields {
    String NAME = "Name";
    String PERIOD = "Period";
    String FOLDER_NUM = "Number of folders";
    String STATUS = "Status";
    String NEXT_SCAN = "Next scan";
    String LAST_SCAN = "Last scan";
}
