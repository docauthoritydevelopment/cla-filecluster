package com.cla.filecluster.service.files;


import com.cla.common.domain.dto.filetree.MailboxDto;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class MailboxCache {

    @Autowired
    private MailboxService mailboxService;

    private LoadingCache<Long, Set<String>> internalCache;

    @PostConstruct
    private void init() {
        internalCache = CacheBuilder.newBuilder()
                .maximumSize(500)
                .expireAfterAccess(1, TimeUnit.HOURS)
                .build(new CacheLoader<Long, Set<String>>() {
                    @Override
                    public Set<String> load(Long key) {
                        List<MailboxDto> mailboxList = mailboxService.getByRootFolderId(key);
                        return mailboxList.stream().map(MailboxDto::getUpn).collect(Collectors.toSet());
                    }
                });
    }

    public void invalidate(Long rootFolderId) {
        internalCache.invalidate(rootFolderId);
    }

    public Set<String> getMailBoxes(long rootFolderId) {
        return internalCache.getUnchecked(rootFolderId);
    }
}
