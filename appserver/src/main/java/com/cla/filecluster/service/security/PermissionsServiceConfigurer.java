package com.cla.filecluster.service.security;

import com.cla.filecluster.config.security.PermissionsValidationVoter;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.intercept.aopalliance.MethodSecurityInterceptor;
import org.springframework.security.access.vote.UnanimousBased;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PermissionsServiceConfigurer {

    @Autowired
    private MethodInterceptor methodSecurityInterceptor;

    @Autowired
    private PermissionsService permissionsService;

    @PostConstruct
    public void init() {
        // add da permission voter to spring voters
        // so that its decision is taken into account as well
        ((UnanimousBased)((MethodSecurityInterceptor)methodSecurityInterceptor).getAccessDecisionManager())
                .getDecisionVoters().add(new PermissionsValidationVoter(permissionsService));
        //((AfterInvocationProviderManager)((MethodSecurityInterceptor)methodSecurityInterceptor).getAfterInvocationManager())
    }
}
