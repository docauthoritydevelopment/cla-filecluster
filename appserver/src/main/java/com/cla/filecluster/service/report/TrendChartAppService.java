package com.cla.filecluster.service.report;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.date.DateRangePointType;
import com.cla.common.domain.dto.date.TimePeriod;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.*;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.report.*;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.repository.solr.SolrQueryResponse;
import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.date.DateRangeService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledFuture;

/**
 * Created by uri on 27/07/2016.
 */
@Service
public class TrendChartAppService {

    public static final String NUM_FOUND_ID = "##numFound";
    public static final String FILE_PREFIX = "file:";

    @Autowired
    private TrendChartService trendChartService;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Value("${trendchart.scheduler.cronTriggerString:0 5 * * * *}")
    private String defaultCronTriggerString;

    @Value("${trendchart.scheduler.active:false}")
    private boolean trendChartSchedulerActive;

    @Value("${trendchart.fabricate.maxTotal:5000000}")
    private int fabricateMaxTotal;

    @Value("${trendchart.fabricate.minTotal:500000}")
    private int fabricateMinTotal;

    @Value("${trendchart.fabricate.maxSample:1000000}")
    private int fabricateMaxSample;

    @Value("${trendchart.fabricate.minSample:1000}")
    private int fabricateMinSample;

    @Value("${subprocess:false}")
    private boolean subProcess;

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private TaskScheduler defaultScheduler;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileCatService fileCatService;

    @Value("${trendchart.template.file:}")
    private String trendChartTemplateFile;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private TrendChartRenderService trendChartRenderService;

    @Autowired
    private MessageHandler messageHandler;

    private Random random = new Random();

    private static final String NAME = "Name";
    private static final String SERIES_TYPE = "Series_type";
    private static final String HIDDEN = "Hidden";
    private static final String FILTER_JSON = "Json_filter";
    private static final String FROM_DAYS = "From_days";
    private static final String RESOLUTION = "Resolution";
    private static final String COLLECTOR_NAME = "Collector_name";
    private static final String TRANSFORMATION_SCRIPT = "Transformation_script";
    private static final String VIEW_CONFIGURATION = "View_configuration";

    private final static String[] requiredHeaders = {NAME, SERIES_TYPE, FILTER_JSON, FROM_DAYS, RESOLUTION, COLLECTOR_NAME, TRANSFORMATION_SCRIPT, VIEW_CONFIGURATION};

    private static Logger logger = LoggerFactory.getLogger(TrendChartAppService.class);

    private ScheduledFuture<?> scheduledFuture;

    @Transactional
    @EventListener(ApplicationReadyEvent.class)
    public void startScheduler() {
        if (!trendChartSchedulerActive || subProcess) {
            logger.warn("TrendChart scheduler is not active");
            return;
        }
        TrendChartSchedule firstTrendChartSchedule = trendChartService.findFirstTrendChartSchedule();
        if (firstTrendChartSchedule != null) {
            logger.info("Start trendChart scheduler");
            try {
                startScheduler(firstTrendChartSchedule);
            } catch (RuntimeException e) {
                logger.error("Failed to start schedule group. Unexpected error {}", firstTrendChartSchedule, e);
            }
        }
    }

    @Transactional(readOnly = false)
    @PreDestroy
    public void stopSchedulers() {
        if (scheduledFuture != null) {
            logger.info("Stop trendChart scheduler");
            try {
                scheduledFuture.cancel(true);
            } catch (RuntimeException e) {
                logger.error("Failed to stop schedule. Unexpected error {}", e);
            }
        }
    }

    private void startScheduler(TrendChartSchedule schedule) {

        logger.debug("start TrendChart Schedule {}", schedule);
        TrendChartCollectionJob chartCollectionJob = applicationContext.getBean(TrendChartCollectionJob.class);
        Trigger trigger = new CronTrigger(schedule.getCronTriggerString(), TimeZone.getDefault());
        scheduledFuture = defaultScheduler.schedule(chartCollectionJob, trigger);
    }

    @Transactional
    public TrendChartDto createTrendChart(TrendChartDto newTrendChart) {
        TrendChart trendChart = trendChartService.createTrendChart(newTrendChart);
        logger.debug("Created trendChart {}", trendChart);
        return convertTrendChartToDto(trendChart);
    }

    @Transactional(readOnly = true)
    public Page<TrendChartDto> listTrendCharts(DataSourceRequest dataSourceRequest) {
        List<TrendChart> result = trendChartService.listTrendCharts(false);
        List<TrendChartDto> resultDto = convertTrendCharts(result);
        return new PageImpl<>(resultDto);
    }

    @Transactional(readOnly = true)
    public TrendChartDto getTrendChart(Long id) {
        TrendChart result = trendChartService.getTrendChart(id);
        TrendChartDto resultDto = convertTrendChartToDto(result);
        return resultDto;
    }

    @Transactional
    public TrendChartDto updateTrendChart(TrendChartDto trendChartDto) {
        TrendChart trendChart = trendChartService.updateTrendChart(trendChartDto);
        return convertTrendChartToDto(trendChart);
    }

    public void deleteTrendChart(long id, boolean wait) {
        trendChartService.markTrendChartAsDeleted(id);
        if (wait) {
            trendChartService.deleteTrendChartData(id);
        } else {
            executionManagerService.startAsyncTask(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    trendChartService.deleteTrendChartData(id);
                    return null;
                }
            });
        }
    }

    @Transactional
    public TrendChartDataSummaryDto getTrendChartView(long id, Long startTime, Long endTime, Integer sampleCount, TrendChartResolutionDto tcResolutionDto) {
        TrendChart trendChart = trendChartService.getTrendChart(id);
//        if (startTime != null && endTime != null && viewResolutionMs != null && sampleCount != null &&
//                (endTime-startTime) != ((sampleCount-1)*viewResolutionMs)) {
//            throw new BadHttpRequestException("Bad parameters combination");
//        }
        if (tcResolutionDto.getCount() < 0) {
//            // Allow view resolution set by sample count
//            if (startTime != null && endTime != null && sampleCount != null && sampleCount != 1) {
//                if (sampleCount<1 || endTime<startTime) {
//                    throw new BadHttpRequestException("Illegal parameters values");
//                }
//                viewResolutionMs = (endTime-startTime)/(sampleCount - 1);
//            }
//            else {
//                viewResolutionMs = trendChart.getViewResolutionMs();
//            }
            tcResolutionDto = convertTrendChart(trendChart);
        }
        if (startTime == null) {
            // Allow calc based on other request parameters
            if (endTime != null && tcResolutionDto.getCount() < 0 && sampleCount != null) {
//                endTime = alignEndTime(endTime, tcResolutionDto);
//                startTime = endTime - (sampleCount - 1) * viewResolutionMs;
                startTime = tcResolutionDto.calcPeriodStartMs(endTime, -(sampleCount - 1));
            }
        } else {
//            startTime = alignStartTime(startTime, viewResolutionMs);
        }
        if (endTime == null) {
            // Allow calc based on other request parameters
            if (startTime != null && sampleCount != null) {
//                endTime = startTime + (sampleCount - 1) * viewResolutionMs;
                endTime = tcResolutionDto.calcPeriodStartMs(startTime, (sampleCount - 1));
            }
        } else {
//            endTime = alignEndTime(endTime, viewResolutionMs);
        }
        // If dont have value, get from chart defaults
        if (startTime == null) {
            DateRangePoint viewFrom = trendChart.getViewFrom();
            startTime = DateRangeService.convertDateRangePoint(viewFrom).getAbsoluteTime();
//            startTime = alignStartTime(startTime, viewResolutionMs);
        }
        // If dont have value, get from chart defaults
        if (endTime == null) {
            DateRangePoint viewTo = trendChart.getViewTo();
            if (viewTo != null) {
                endTime = DateRangeService.convertDateRangePoint(viewTo).getAbsoluteTime();
            } else {
                logger.debug("show trendChart until now ({})", System.currentTimeMillis());
                endTime = System.currentTimeMillis();
            }
//            endTime = alignEndTime(endTime, viewResolutionMs);
        }
        if (startTime > endTime) {
            logger.warn("TrendChart request endTime before startTime. {} > {}", startTime, endTime);
            // The graph starts in the future :-( Can happen in tests or due to bad configuration
            startTime = endTime;
        }
        Long trendChartCollectorId = trendChart.getId();
        TrendChart collectorTrendChart = trendChart;
        if (trendChart.getTrendChartCollectorId() != null) {
            logger.debug("TrendChart {} is using data collected by trendChart {}", trendChart, trendChartCollectorId);
            trendChartCollectorId = trendChart.getTrendChartCollectorId();
            collectorTrendChart = trendChartService.getTrendChart(trendChartCollectorId);
        }
        List<TrendChartData> trendChartDataList = trendChartService.getTrendChartData(trendChartCollectorId,
                tcResolutionDto.calcPeriodStartMs(startTime, -1),
                tcResolutionDto.calcPeriodStartMs(endTime, 1));

        if (trendChartDataList == null || trendChartDataList.isEmpty()) {
            //
        } else {
            TrendChartData firstDataPoint = trendChartDataList.get(0);
            if (firstDataPoint.getRequestedTimeStamp() > startTime) {
                startTime = Long.min(endTime, tcResolutionDto.calcPeriodStartMs(firstDataPoint.getRequestedTimeStamp(), 0));
                sampleCount = tcResolutionDto.calcSampleCount(startTime, endTime);
                logger.debug("Data points missing. Adjusting startTime to {} and sampleCount to {}", startTime, sampleCount);
            }
        }

        Map<String, TrendChartSeriesDto> trendChartSeriesDtoMap = trendChartRenderService.renderTrendChartData(trendChart, collectorTrendChart, trendChartDataList, startTime, endTime, tcResolutionDto);
        trendChartRenderService.enrichTrendChartSeriesDtoList(trendChartSeriesDtoMap, trendChart.getSeriesType());
        List<TrendChartSeriesDto> values = new ArrayList<>(trendChartSeriesDtoMap.values());
        TrendChartDataSummaryDto trendChartDataSummaryDto = convertTrendChartDataSummary(trendChart, values, startTime, endTime, tcResolutionDto);
        return trendChartDataSummaryDto;
    }

    public TrendChartResolutionDto convertTrendChart(TrendChart trendChart) {

        TimeResolutionType resolutionType = trendChart.getResolutionType();
        if (resolutionType == null) {
            resolutionType = TimeResolutionType.TR_miliSec;
        }
        Long count = (resolutionType == TimeResolutionType.TR_miliSec) ?
                trendChart.getViewResolutionMs() :
                trendChart.getViewSampleCount();

        long viewOffsetMs = (trendChart.getViewOffset() == null) ? 0 : trendChart.getViewOffset();
        int viewResolutionStep = (trendChart.getViewResolutionStep() == null) ? 1 : trendChart.getViewResolutionStep();
        if (viewResolutionStep == 0) {
            throw new RuntimeException(messageHandler.getMessage("trend-chart.resolution-step.zero", Lists.newArrayList(toString())));
        }

        return new TrendChartResolutionDto(resolutionType, count, viewOffsetMs, viewResolutionStep);
    }

    private Long alignEndTime(Long endTime, final Long viewResolutionMs) {
//        endTime--;
//        endTime = endTime - (endTime % viewResolutionMs) + viewResolutionMs;
        return endTime;
    }

    private Long alignStartTime(Long startTime, final Long viewResolutionMs) {
//        startTime = startTime - (startTime % viewResolutionMs); // We always render a meaningful window
        return startTime;
    }

    @Transactional(readOnly = false)
    public void populateDefaultTrendChartsAndScheduler(User accountAdmin) {
        if (trendChartTemplateFile == null || trendChartTemplateFile.isEmpty()) {
            logger.info("No default trend-charts file defined - ignored");
            return;
        }
        trendChartService.createTrendChartSchedule(defaultCronTriggerString);
        logger.info("load default TrendCharts from {}", trendChartTemplateFile);
        createTrendChartsFromFile(trendChartTemplateFile, accountAdmin);
    }

    private void createTrendChartsFromFile(String trendChartTemplateFile, User accountAdmin) {
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, trendChartTemplateFile, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(trendChartTemplateFile, row -> createTrendChartFromCsvRow(row, headersLocationMap, accountAdmin),
                    null, null);
        } catch (Exception e) {
            logger.error("Failed to read trendCharts file. Got exception: {}", e.getMessage(), e);
        }
    }

    private void createTrendChartFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap, User user) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues[0].startsWith("#")) {
            logger.info("Commented line {} in default trendCharts file. ignored: {}", row.getLineNumber(), row.toString());
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String filter = fieldsValues[headersLocationMap.get(FILTER_JSON)];
        if (filter != null) {
            filter = filter.trim();
        }
        if (filter != null && !filter.isEmpty() && filter.startsWith("[")) {
            TrendChartService.convertFilterDescriptorJsonArray(filter);
        }
        String seriesType = fieldsValues[headersLocationMap.get(SERIES_TYPE)].trim();
        String fromDays = fieldsValues[headersLocationMap.get(FROM_DAYS)].trim();
        String resolutionMs = fieldsValues[headersLocationMap.get(RESOLUTION)].trim();
        String collectorName = fieldsValues[headersLocationMap.get(COLLECTOR_NAME)];
        String transformationScript = fieldsValues[headersLocationMap.get(TRANSFORMATION_SCRIPT)];
        String viewConfiguration = fieldsValues[headersLocationMap.get(VIEW_CONFIGURATION)];
        if (transformationScript != null && transformationScript.startsWith(FILE_PREFIX)) {
            try {
                transformationScript = new String(Files.readAllBytes(Paths.get(transformationScript.substring(FILE_PREFIX.length()))), "UTF8");
            } catch (IOException e) {
                logger.error("Could not open transformation script file '{}' for trendChart {}", transformationScript, name);
                transformationScript = "";
            }
        }
        String hidden = (headersLocationMap.get(HIDDEN) != null) ? fieldsValues[headersLocationMap.get(HIDDEN)].trim() : "false";

        TrendChart trendChart = new TrendChart();
        trendChart.setName(name);
        trendChart.setSortType(SortType.NAME);
        trendChart.setValueType("files");
        trendChart.setFilters(filter);
        trendChart.setHidden(Boolean.valueOf(hidden));
        trendChart.setChartDisplayType(ChartDisplayType.TREND_LINE);
        trendChart.setChartValueFormatType(ChartValueFormatType.NUMERIC);
        trendChart.setSeriesType(seriesType);
        trendChart.setViewResolutionMs(Long.valueOf(resolutionMs));
        trendChart.setOwner(user);
        if (StringUtils.isNotEmpty(transformationScript)) {
            trendChart.setTransformerScript(transformationScript);
        }
        if (StringUtils.isNotEmpty(viewConfiguration)) {
            trendChart.setViewConfiguration(viewConfiguration);
        }
        if (StringUtils.isNotEmpty(collectorName)) {
            TrendChart trendChartCollector = trendChartService.getTrendChartByName(collectorName, true);
            if (trendChartCollector != null) {
                trendChart.setTrendChartCollectorId(trendChartCollector.getId());
            } else {
                logger.warn("Failed to find trendChart collector {} while populating the trendCharts", collectorName);
            }
        }
        DateRangePoint viewFrom = new DateRangePoint();
        viewFrom.setType(DateRangePointType.RELATIVE);
        viewFrom.setRelativePeriod(TimePeriod.DAYS);
        viewFrom.setRelativePeriodAmount(Integer.valueOf(fromDays));
        trendChart.setViewFrom(viewFrom);
        logger.info("Creating trendChart from CSV {}", trendChart);
        trendChartService.saveTrendChart(trendChart);

    }

    @Transactional
    public void collectTrendChartsData(long requestedTimeStamp) {
        List<TrendChart> all = trendChartService.listTrendCharts(false);
        logger.debug("Collect all TrendCharts data ({})", all.size());
        for (TrendChart trendChart : all) {
            try {
                if (trendChart.getTrendChartCollectorId() != null
                        && !Objects.equals(trendChart.getTrendChartCollectorId(), trendChart.getId())) {
                    continue; // This trendChart is using the data from another trendChart
                }
                collectTrendChartData(requestedTimeStamp, trendChart);
            } catch (Exception e) {
                logger.error("Failed to collect trend chart data for chart {}", trendChart, e);
            }

        }
    }

    public void collectTrendChartData(long requestedTimeStamp, TrendChart trendChart) {
        List<TrendChartRecordedValue> recordedValues = generateRecordedValues(trendChart);
        trendChartService.createTrendChartData(recordedValues, requestedTimeStamp, trendChart, true);
    }

    public List<TrendChartRecordedValue> generateRecordedValues(TrendChart trendChart) {
        FilterDescriptor[] filters = trendChartService.getTrendChartFilters(trendChart);
        String seriesType = trendChart.getSeriesType();
        List<TrendChartRecordedValue> recordedValues = new ArrayList<>();
        if (trendChartService.isFilesCollector(seriesType)) {
            //Query file counts and not facet counts
            for (int i = 0; i < filters.length; i++) {
                FilterDescriptor filter = filters[i]; //filter can be null
                SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(filter);
                if (logger.isDebugEnabled()) {
                    logger.debug("Running TC filter {}/{} solrQ={}",
                            Optional.ofNullable(trendChart.getName()).orElse(trendChart.getId().toString()),
                            (filter == null ? "-" : filter.getName()),
                            (solrSpecification == null ? "null" : solrSpecification.toString()));
                }
                SolrQueryResponse solrQueryResponse = fileCatService.runQuery(solrSpecification, new PageRequest(0, 1));
                createTrendChartRecordedValues(solrQueryResponse, recordedValues, i);
            }
        } else {
            CatFileFieldType catFileFieldType = FileDtoFieldConverter.convertToCatFileField(seriesType);
            for (int i = 0; i < filters.length; i++) {
                FilterDescriptor filter = filters[i];
                SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(filter, catFileFieldType);
                solrSpecification.setMinCount(1);
                SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrSpecification, new PageRequest(0, 30));
                createTrendChartFacetRecordedValues(solrFacetQueryResponse, recordedValues, i);
            }
        }
        return recordedValues;
    }

    private void createTrendChartFacetRecordedValues(SolrFacetQueryResponse solrFacetQueryResponse, List<TrendChartRecordedValue> recordedValues, int filterIndex) {
        Double numFound = Double.valueOf(solrFacetQueryResponse.getNumFound());
        TrendChartRecordedValue numFoundRecordedValue = new TrendChartRecordedValue(NUM_FOUND_ID, numFound, filterIndex);
        recordedValues.add(numFoundRecordedValue);
        for (FacetField.Count count : solrFacetQueryResponse.getFirstResult().getValues()) {
            TrendChartRecordedValue trendChartRecordedValue = new TrendChartRecordedValue(count.getName(), Double.valueOf(count.getCount()), filterIndex);
            recordedValues.add(trendChartRecordedValue);
        }
    }

    private void createTrendChartRecordedValues(SolrQueryResponse solrQueryResponse, List<TrendChartRecordedValue> recordedValues, int filterIndex) {
        Double numFound = Double.valueOf(solrQueryResponse.getQueryResponse().getResults().getNumFound());
        TrendChartRecordedValue trendChartRecordedValue = new TrendChartRecordedValue(NUM_FOUND_ID, numFound, filterIndex);
        recordedValues.add(trendChartRecordedValue);
    }

    @Transactional(readOnly = false)
    public void fabricateTrendChartData(long id, int days) {
        fabricateTrendChartData(id, days, LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) * 1000);
    }

    @Transactional(readOnly = false)
    public void fabricateTrendChartData(long id, int days, long endTime) {

        TrendChart trendChart = trendChartService.getTrendChart(id);
        List<TrendChartRecordedValue> recordedValues = generateRecordedValues(trendChart);
        FilterDescriptor[] filters = trendChartService.getTrendChartFilters(trendChart);
        int totalFilterIndex = (-1);
        for (int i = 0; i < filters.length; ++i) {
            if (filters[i] != null && filters[i].getName() != null && filters[i].getName().toLowerCase().startsWith("total")) {
                totalFilterIndex = i;
                break;
            }
        }

        logger.warn("Fabricate trend chart data for trendChart {}", trendChart);
        LocalDateTime endLocalTime = LocalDateTime.ofEpochSecond(endTime / 1000, 0, ZoneOffset.UTC);
        LocalDateTime requestedTime = endLocalTime.minusDays(days);
//        Random random = new Random();
        int z = 0;
        int maxZ = days * 24;
        while (requestedTime.isBefore(endLocalTime)) {
            long requestedTimeStamp = requestedTime.toEpochSecond(ZoneOffset.UTC) * 1000;
            recordedValues = fabricateRecordedValues(recordedValues, z, maxZ, totalFilterIndex);
            trendChartService.createTrendChartData(recordedValues, requestedTimeStamp, trendChart, false);
            requestedTime = requestedTime.plusHours(1);
            z++;
        }
        logger.debug("Fabricated {} values for trend chart {}", z, trendChart);
    }

    private List<TrendChartRecordedValue> fabricateRecordedValues(List<TrendChartRecordedValue> previousRecordedValues, int sampleCount, int totalSamples, int totalFilterIndex) {
        List<TrendChartRecordedValue> result = new ArrayList<>();
        for (int i = 0; i < previousRecordedValues.size(); ++i) {
            TrendChartRecordedValue sampleRecordedValue = previousRecordedValues.get(i);
            int min = (i == totalFilterIndex) ? fabricateMinTotal : (fabricateMinSample + fabricateMinSample * (i + 1) / previousRecordedValues.size() / 5);
            int max = (i == totalFilterIndex) ? fabricateMaxTotal : (fabricateMaxSample + fabricateMaxSample * (i + 1) / previousRecordedValues.size() / 5);
            double value = calculateSampleValue(sampleCount, totalSamples, min, max);
            TrendChartRecordedValue resultValue = new TrendChartRecordedValue(sampleRecordedValue.getId(), value, sampleRecordedValue.getFilterIndex());
            result.add(resultValue);
        }
        return result;
    }

    private double calculateSampleValue(int sampleCount, int totalSamples, int min, int max) {
        return Math.round(min + (max - min) * Math.sqrt(1d * sampleCount / totalSamples));
    }

    //    private List<TrendChartRecordedValue> fabricateRecordedValues(List<TrendChartRecordedValue> previousRecordedValues, int sampleCount, int totalSamples, int totalFilterIndex) {
//        List<TrendChartRecordedValue> result = new ArrayList<>();
//        for (TrendChartRecordedValue sampleRecordedValue : previousRecordedValues) {
//            double previousValue = sampleRecordedValue.getValue();
//            double value = calculateNextValue(previousValue,sampleCount);
//            TrendChartRecordedValue resultValue = new TrendChartRecordedValue(sampleRecordedValue.getId(), value,sampleRecordedValue.getFilterIndex());
//            result.add(resultValue);
//        }
//        return result;
//    }
//
    private double calculateNextValue(double previousValue, int z) {
        double value = previousValue + random.nextInt(2);
        if (z != 0 && z % 15 == 0) {
            value = value - random.nextInt(12);
        }
        return value;
    }

    @Transactional(readOnly = false)
    public void updateTrendChartTransformerScript(Long id, String script) {
        TrendChart trendChart = trendChartService.getTrendChart(id);
        logger.warn("Updating trendChart {} script", trendChart);
        trendChart.setTransformerScript(script);
        trendChartService.saveTrendChart(trendChart);
    }

    public void importCsvFile(byte[] bytes) {
        final User currentUser = userService.getCurrentUserEntity();
        logger.info("Import TrendCharts");

        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, bytes, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(bytes, row -> createTrendChartFromCsvRow(row, headersLocationMap, currentUser), null, null);
        } catch (Exception e) {
            logger.error("Failed to read trendCharts file. Got exception: {}", e.getMessage(), e);
        }
    }

    public List<TrendChartDto> getAllTrendCharts() {
        List<TrendChart> result = trendChartService.listTrendCharts(false);
        List<TrendChartDto> resultDto = convertTrendCharts(result);
        return resultDto;
    }

    public static TrendChartDto convertTrendChartToDto(TrendChart trendChart) {
        TrendChartDto result = new TrendChartDto();
        result.setId(trendChart.getId());
        result.setTrendChartCollectorId(trendChart.getTrendChartCollectorId());
        result.setChartDisplayType(trendChart.getChartDisplayType());
        result.setChartValueFormatType(trendChart.getChartValueFormatType());
        result.setDeleted(trendChart.isDeleted());
        result.setFilters(trendChart.getFilters());
        result.setHidden(trendChart.isHidden());
        result.setName(trendChart.getName());
        result.setOwnerId(trendChart.getOwner().getId());
        result.setSeriesType(trendChart.getSeriesType());
        result.setSortType(trendChart.getSortType());
        result.setValueType(trendChart.getValueType());

        result.setViewConfiguration(trendChart.getViewConfiguration());
        result.setViewFrom(DateRangeService.convertDateRangePoint(trendChart.getViewFrom()));
        result.setViewTo(DateRangeService.convertDateRangePoint(trendChart.getViewTo()));
        if (trendChart.getResolutionType() == null || trendChart.getResolutionType() == TimeResolutionType.TR_miliSec) {
            result.setViewResolutionMs(trendChart.getViewResolutionMs());
        } else {
            result.setViewResolutionCount(trendChart.getViewResolutionCount());
            result.setViewOffset(trendChart.getViewOffset());
            result.setViewResolutionStep(trendChart.getViewResolutionStep());
        }
        result.setViewSampleCount(trendChart.getViewSampleCount());
        return result;
    }

    public static List<TrendChartDto> convertTrendCharts(List<TrendChart> trendCharts) {
        List<TrendChartDto> result = new ArrayList<>();
        for (TrendChart trendChart : trendCharts) {
            result.add(convertTrendChartToDto(trendChart));
        }

        return result;
    }

    public static TrendChartDataSummaryDto convertTrendChartDataSummary(TrendChart trendChart,
                                                                        List<TrendChartSeriesDto> trendChartSeriesDtoList,
                                                                        Long startTime, Long endTime,
                                                                        TrendChartResolutionDto tcResolutionDto) {
        TrendChartDto trendChartDto = convertTrendChartToDto(trendChart);
        int sampleCount = 0;
        if (trendChartSeriesDtoList != null && trendChartSeriesDtoList.size() > 0) {
            sampleCount = trendChartSeriesDtoList.get(0).getValues().length;
        }
        return new TrendChartDataSummaryDto(trendChartDto, trendChartSeriesDtoList, startTime, endTime, tcResolutionDto, sampleCount);
    }
}
