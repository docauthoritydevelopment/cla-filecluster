package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.FileGroupsFieldType;
import com.cla.common.domain.dto.crawler.FileCrawlPhase;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.batch.writer.analyze.MissedMatchHandler;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.service.*;
import com.cla.filecluster.jobmanager.service.ProgressPercentageTracker.Scale;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.naming.GroupNamingService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.cla.common.constants.FileGroupsFieldType.*;
import static com.cla.common.constants.FileGroupsFieldType.ID;

/**
 * Created by uri on 22-Mar-17.
 */
@Service
public class AnalyzeFinalizeService implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(AnalyzeFinalizeService.class);

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private AnalyzerDataService analyzerDataService;

    @Autowired
    private GroupNamingService groupNamingService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private JobCoordinator jobCoordinator;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private MissedMatchHandler missedMatchHandler;

    @Value("${groupNaming.skipTermsThreshold:0.3}")
    private double skipTermsThresholdProp;

    @Value("${analyze.missedMatchesPageSize:5000}")
    private int missedMatchesPageSize;

    @Value("${analyze.missedMatches.parallelProcessing:false}")
    private boolean missedMatchesParallelProcessing;

    @Value("${group.naming.byDirty:true}")
    private boolean groupNamingByDirty;

    @Value("${groupNaming.dbPpageSize:1000}")
    private int groupNamingPageSize;

    @Value("${job-manager.jobs.in-progress-timeout-sec:3600}")
    private int inProgressJobTimeoutSec;

    @Value("${table.missed_file_match.truncate:true}")
    private boolean truncateMissedFileMatchTable;

    private TimeSource timeSource = new TimeSource();

    private AtomicBoolean finalizationInProgress = new AtomicBoolean(false);

    private boolean isSchedulingActive = false;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    @AutoAuthenticate
    @Scheduled(fixedRateString = "${analyze-finalization.tasks-polling-rate-millis:10000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    void processNextTasksGroup() {

        if (!isSchedulingActive) {
            return;
        }

        List<SimpleJob> inProgressJobs = jobManagerService.getJobs(JobType.ANALYZE_FINALIZE, JobState.WAITING);
        if (inProgressJobs.isEmpty()) {
            // avoid calling getTaskCountPerRun (which involves a pricey query) if there are no in-progress jobs
            logger.trace("no waiting analyze fin jobs, exit processing cycle");
            return;
        }

        Map<Long, Long> newTasksCountPerRun =
                jobManagerService.getTaskCountPerRun(TaskType.ANALYZE_FINALIZE, JobState.WAITING, TaskState.NEW);

        if (newTasksCountPerRun.size() == 0) {
            logger.debug("has waiting analyze fin job/s but no new tasks, exit processing cycle");
            return;
        }

        if (finalizationInProgress.compareAndSet(false, true)) {
            try {
                for (Long runId : newTasksCountPerRun.keySet()) {
                    Long taskCount = newTasksCountPerRun.get(runId);
                    if (taskCount > 0L) {
                        List<SimpleTask> newTasks =
                                jobManagerService.getTasksByContext(TaskType.ANALYZE_FINALIZE, TaskState.NEW,
                                        runId, taskCount.intValue());
                        if (newTasks.size() > 1) {
                            logger.warn("Got More than one Analyze-Finalize task for run {}. Running analysis finalization only for the first one", runId);
                        }
                        runAnalyzeFinalize(runId, newTasks.get(0));
                    }
                }
            } finally {
                finalizationInProgress.set(false);
            }
        } else {
            logger.info("Finalize analysis currently in progress");
        }
    }

    public boolean isFinalizationInProgress() {
        return finalizationInProgress.get();
    }

    private void runAnalyzeFinalize(long runContext, SimpleTask analyzeFinTask) {
        Long analyzeFinJobId = jobManagerService.getJobIdCached(runContext, JobType.ANALYZE_FINALIZE);

        try {
            KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runAnalyzeFinalize.updateJobTaskStatus", 183);
            jobManagerService.updateJobState(analyzeFinTask.getJobId(), JobState.IN_PROGRESS, null, null, false);
            jobManagerService.updateTaskStatus(analyzeFinTask.getId(), TaskState.CONSUMING);
            kpiRecording.end();
            kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runAnalyzeFinalize.runFinalizeAnalysisForJob", 186);
            runFinalizeAnalysisForJob(analyzeFinJobId);
            kpiRecording.end();
            if (jobManagerService.isJobActive(analyzeFinJobId)) { //DO not change paused Analyze finalyze to done after already paused
                jobManagerService.updateTaskStatus(analyzeFinTask.getId(), TaskState.DONE);
                jobManagerService.handleFinishJob(analyzeFinJobId, "Finished analyze finalize");
                jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_INITIATED);
            }
        } catch (Exception e) {
            if (jobManagerService.isJobActive(analyzeFinJobId)) { //DO not change paused Analyze finalyze to done after already paused
                jobManagerService.updateTaskStatus(analyzeFinTask.getId(), TaskState.FAILED);
                logger.error("Failed to run finalize analysis", e);
                if (jobManagerService.isJobActive(analyzeFinJobId)) {
                    jobManagerService.handleFailedFinishJob(analyzeFinJobId, "Analyze finalize failed");
                    jobManagerAppService.endRun(runContext, PauseReason.SYSTEM_ERROR);
                }
            } else {
                logger.info("Failed to run finalize analysis - job not active, do nothing", e);
            }
        }
    }

    @AutoAuthenticate
    public void runFinalizeAnalysisForJob(Long analyzeFinJobId) {

        ProgressTracker progressTracker;
        if (analyzeFinJobId == null) {
            progressTracker = new DummyProgressTracker();
        } else {
            progressTracker =
                    new ProgressPercentageTracker(Scale.PER_TEN_THOUSAND, analyzeFinJobId, jobManagerService);
            progressTracker =
                    new ProgressTimestampTracker(progressTracker, inProgressJobTimeoutSec, 10, analyzeFinJobId, jobManagerService);
        }

        if (analyzeFinJobId != null) {
            jobManagerService.setOpMsg(analyzeFinJobId, "Analysis finalization started");
            if (!jobManagerService.isJobActive(analyzeFinJobId)) {
                return;
            }
        }

        final long start = timeSource.currentTimeMillis();

        try {
            long[] analyzeFinJobIds = analyzeFinJobId == null ? new long[0] : new long[]{analyzeFinJobId};
            KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runFinalizeAnalysisForJob.processMissedContentMatches", 224);
            missedMatchHandler.handleAll(analyzeFinJobIds);
            kpiRecording.end();

            if (analyzeFinJobId != null) {
                if (!jobManagerService.isJobActive(analyzeFinJobId)) {
                    return;
                }
                jobManagerService.setOpMsg(analyzeFinJobId, "Updating grouping data...");
            }

            progressTracker.startStageTracking(40);
            kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runFinalizeAnalysisForJob.contentMetadataAnalyzeFinalize", 230);
            filesDataPersistenceService.contentMetadataAnalyzeFinalize(analyzeFinJobId, progressTracker, start);
            kpiRecording.end();

            if (analyzeFinJobId != null && !jobManagerService.isJobActive(analyzeFinJobId)) {
                return;
            }

            kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runFinalizeAnalysisForJob.updateNumOfFilesInGroups", 234);
            updateNumOfFilesInGroups(analyzeFinJobId, progressTracker);
            kpiRecording.end();

            if (analyzeFinJobId != null && !jobManagerService.isJobActive(analyzeFinJobId)) {
                return;
            }

            kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runFinalizeAnalysisForJob.reindexTitles", 238);
            reindexTitles(analyzeFinJobId, progressTracker);            // no more groups names
            kpiRecording.end();

            if (analyzeFinJobId != null && !jobManagerService.isJobActive(analyzeFinJobId)) {
                return;
            }

            kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runFinalizeAnalysisForJob.proposeGroupNamesByPages", 242);
            proposeGroupNamesByPages(skipTermsThresholdProp, analyzeFinJobId, progressTracker, start);
            kpiRecording.end();

            if (analyzeFinJobId != null && !jobManagerService.isJobActive(analyzeFinJobId)) {
                return;
            }

            progressTracker.startStageTracking(20);
            kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "runFinalizeAnalysisForJob.initCategoriesPhase", 247);
            initCategoriesPhase(analyzeFinJobId);
            kpiRecording.end();
            progressTracker.endCurStage();
            progressTracker.endTracking();

            logger.info("Finished finalizeAnalysis within {} sec", timeSource.secondsSince(start));

            jobManagerService.setOpMsg(analyzeFinJobId, JobManagerService.DONE);
            opStateService.checkIfStopRequested();

        } catch (CoordinationLockDeniedException e) {
            logger.info("Analyze finalize did not finish running due to job coordination lock denial:", e);
        }
    }


    public void updateNumOfFilesInGroups(Long jobId, ProgressTracker progressTracker) {
        fileGroupService.getGroupSqlHeavyOperationLock();
        try {
            opStateService.setCurrentPhase(FileCrawlPhase.UPDATE_NUM_OF_FILES_IN_GROUPS);
            jobManagerService.setOpMsg(jobId, "Updating grouping size #1...");
            progressTracker.startStageTracking(63);
            analyzerDataService.updateNumOfFilesInAnalysisGroups(jobId);
            progressTracker.endCurStage();
            jobManagerService.setOpMsg(jobId, "Updating grouping size #2...");
            progressTracker.startStageTracking(63);
            analyzerDataService.updateNumOfFilesInUserGroups();
            progressTracker.endCurStage();
            progressTracker.startStageTracking(63);
            analyzerDataService.updateAssociationsDirtyForGroups(jobId);
            progressTracker.endCurStage();
            jobManagerService.setOpMsg(jobId, "Updating groups samples...");
            progressTracker.startStageTracking(64);
            analyzerDataService.updateSampleFileInGroups(jobId);
            progressTracker.endCurStage();
        } finally {
            fileGroupService.releaseGroupSqlHeavyOperationLock();
        }

    }

    private void reindexTitles(Long jobId, ProgressTracker progressTracker) throws CoordinationLockDeniedException {
        opStateService.setCurrentPhase(FileCrawlPhase.REINDEX_TITLES);
        progressTracker.startStageTracking(10);
        groupNamingService.fullGroupNamingByGroups(false, true, -1, true, jobId, progressTracker);
    }

    /**
     * Apply names to analysis groups and update CatFile core in SOLR
     *
     * @param skipTermsThreshold terms threshold
     * @param jobId              analyze finalization job ID
     * @param progressTracker    used to track progress
     */
    private void proposeGroupNamesByPages(double skipTermsThreshold, Long jobId, ProgressTracker progressTracker, long toDate) throws CoordinationLockDeniedException {
        if (skipTermsThreshold < 0f) {
            skipTermsThreshold = skipTermsThresholdProp;
        }
        opStateService.setCurrentPhase(FileCrawlPhase.GROUP_NAMES);
        if (groupNamingByDirty) {
            KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "proposeGroupNamesByPages.markUpdatedGroupsForRename", 378);
            markUpdatedGroupsForRename(jobId, toDate);
            kpiRecording.end();
        }
        progressTracker.startStageTracking(1100);
        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "proposeGroupNamesByPages.fullGroupNamingByGroups", 383);
        groupNamingService.fullGroupNamingByGroups(false, true, skipTermsThreshold, false, jobId, progressTracker);
        kpiRecording.end();
        long startTime = timeSource.currentTimeMillis();
        logger.info("Starting applying group names to SOLR.");
        progressTracker.startStageTracking(3300);
        kpiRecording = performanceKpiRecorder.startRecording("AnalyzeFinalizeService", "proposeGroupNamesByPages.applyGroupNamesToSolr", 389);
        applyGroupNamesToSolr(jobId, progressTracker, toDate);
        kpiRecording.end();
        logger.info("Finished applying group names to SOLR in {} seconds.", timeSource.secondsSince(startTime));
    }

    private void applyGroupNamesToSolr(Long jobId, ProgressTracker progressTracker, long toDate) {
        int pageCount = 0;

        int count = fileGroupService.countNonDeletedDirtyAnalysisGroups();
        progressTracker.setCurStageEstimatedTotal(count);

        String cursor = "*";
        boolean lastPage;
        do {
            Query query = Query.create().addFilterWithCriteria(
                    Criteria.create(DIRTY, SolrOperator.EQ, true))
                    .addFilterWithCriteria(TYPE, SolrOperator.EQ, GroupType.ANALYSIS_GROUP.toInt())
                    .setRows(groupNamingPageSize).setCursorMark(cursor).addField(ID).addField(GENERATED_NAME)
                    .addSort(FileGroupsFieldType.ID, Sort.Direction.ASC);

            long start = System.currentTimeMillis();
            logger.debug("applyGroupNamesToSolr({}): loading page {} of {} groups", jobId, pageCount, groupNamingPageSize);

            QueryResponse resp = fileGroupService.runQuery(query.build());

            logger.trace("findNonDeletedDirtyAnalysisGroups page {} of {} groups took {} ms", pageCount, resp.getResults().size(), (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();

            cursor = resp.getNextCursorMark();
            if (resp.getResults().size() > 0) {
                resp.getResults().forEach(doc -> {
                    String fq = CatFileFieldType.ANALYSIS_GROUP_ID.getSolrName() + ":" + doc.getFieldValue(ID.getSolrName());
                    fileCatService.updateByQuery(Lists.newArrayList(fq), CatFileFieldType.GROUP_NAME, (String) doc.getFieldValue(GENERATED_NAME.getSolrName()), false);
                });
            }
            logger.trace("updateFieldByQuery page {} of {} groups took {} ms", pageCount, resp.getResults().size(), (System.currentTimeMillis() - start));
            pageCount++;
            progressTracker.incrementProgress(resp.getResults().size());
            lastPage = resp.getResults().size() < groupNamingPageSize;
        } while (!lastPage);
        fileCatService.commitToSolr();

        if (groupNamingByDirty) {
            jobManagerService.setOpMsg(jobId, "Clear group marks...");
            clearGroupsDirty(jobId, false, toDate);
        }
    }

    private void markUpdatedGroupsForRename(Long jobId, long toDate) {
        logger.info("Mark updated groups for potential rename");
        jobManagerService.setOpMsg(jobId, "Identify groups for rename...");
//        groupNamingService.markUpdatedGroupsForNameCalculation();               // TODO: review code
        clearGroupsDirty(jobId, true, toDate);
        jobManagerService.setOpMsg(jobId, "Group naming identification completed");
    }

    public void clearGroupsDirty(Long jobId, final boolean beforeGroupNaming, long toDate) {
        logger.debug("Clear Groups Dirty field (beforeGroupNaming={})", beforeGroupNaming);
        analyzerDataService.clearGroupsDirty(beforeGroupNaming, jobId, toDate);
    }

    private void initCategoriesPhase(Long jobId) {
        logger.info("Init categories phase.");
        long start = timeSource.currentTimeMillis();

        try {
            updateMissingParentsInformation(jobId);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            logger.error("Interrupted", e);
        } catch (CoordinationLockDeniedException e) {
            logger.info("Init categories phase did not run due to job coordination lock denial:", e);
        } catch (RuntimeException e) {
            logger.error("Failed to create Group Categories", e);
            throw e;
        }

        logger.info("initCategoriesPhase - finished. In a total Of {} seconds", timeSource.secondsSince(start));
    }

    private void updateMissingParentsInformation(Long jobId) throws CoordinationLockDeniedException, InterruptedException {
        opStateService.setCurrentPhase(FileCrawlPhase.UPDATE_MISSING_PARENT_INFO);
        jobManagerService.setOpMsg(jobId, "Update all folders parents info");
        try {
            logger.debug("Job #{} requesting job coordination lock for Analyze-Fin updating missing parents info", jobId);
            long[] analyzeFinJobIds = jobId == null ? new long[0] : new long[]{jobId};
            jobCoordinator.requestLock(JobType.ANALYZE_FINALIZE, analyzeFinJobIds);
            logger.debug("Acquired job coordination lock for Analyze-Fin updating missing parents info");
            while (docFolderService.updateFolderParentsInfoPage() > 0) {
                logger.debug("Yielding job coordination lock for Analyze-Fin updating missing parents info");
                jobCoordinator.yieldToWaitingJobs(jobId);
                logger.debug("Got back from yielding job coordination lock for Analyze-Fin updating missing parents info");
            }
        } finally {
            logger.debug("releasing job coordination lock for Analyze-Fin updating missing parents info");
            jobCoordinator.release();
            logger.debug("released job coordination lock for Analyze-Fin updating missing parents info");
        }
        logger.info("Update all folders parents info - finished");
        jobManagerService.setOpMsg(jobId, "Update all folders parents info - finished");
    }
}
