package com.cla.filecluster.service.plugin;

import com.cla.common.domain.dto.messages.action.ActionPluginRequest;

/** an action plugin to perform a specific action on a PluginActionRequest object
 * for example send an email or write to db etc
 * Created by: yael
 * Created on: 1/11/2018
 */
public interface ActionPlugin {
    void handleAction(ActionPluginRequest request);
}
