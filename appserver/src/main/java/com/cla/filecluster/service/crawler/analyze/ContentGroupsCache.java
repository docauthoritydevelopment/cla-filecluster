package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Cache for content grouping status
 * we want to be able to work in bulk when setting group for content
 * this cache saves the content group so we'll know if it's free for assignment or not
 * <p>
 * Created by: yael
 * Created on: 1/10/2019
 */
@Service
public class ContentGroupsCache {

    @Autowired
    private ContentMetadataService contentMetadataService;

    private Map<Long, ContentGroupData> contentToGroup = new HashMap<>();
    private Map<Long, Set<Long>> tasksToContents = new HashMap<>();

    // add contents from several tasks
    public synchronized void addContentsToCache(Map<Long, Long> contentTaskList) {
        if (contentTaskList == null || contentTaskList.isEmpty()) return;

        // check which contents we should fetch data for and add relation between content and task
        List<Long> contentsToFetch = new ArrayList<>();
        contentTaskList.forEach((contentId, taskId) -> {
            addContentToTaskList(Lists.newArrayList(contentId), taskId);

            if (contentToGroup.containsKey(contentId)) {
                contentToGroup.get(contentId).addTask(taskId);
            } else {
                contentsToFetch.add(contentId);
            }
        });

        // fetch contents that were not in cache and add them
        if (!contentsToFetch.isEmpty()) {
            List<SolrContentMetadataEntity> contents = contentMetadataService.findByIds(contentsToFetch);
            contents.forEach(content -> {
                Long contentId = Long.parseLong(content.getId());
                addContentToCache(contentTaskList.get(contentId), contentId, content.getAnalysisGroupId());
            });
        }
    }

    private void addContentToCache(Long taskId, Long contentId, String groupId) {
        ContentGroupData data = new ContentGroupData();
        data.setGroupId(groupId);
        data.addTask(taskId);
        contentToGroup.put(contentId, data);
    }

    private void addContentToTaskList(Collection<Long> contentIds, Long taskId) {
        Set<Long> taskContentList = tasksToContents.getOrDefault(taskId, new HashSet<>());
        taskContentList.addAll(contentIds);
        tasksToContents.put(taskId, taskContentList);
    }


    // add contents from the same task
    public synchronized void addContentsToCache(Collection<Long> contentIds, Long taskId) {
        if (contentIds == null || contentIds.isEmpty()) return;

        // add relation between content and task
        addContentToTaskList(contentIds, taskId);

        // check which contents we should fetch data for
        List<Long> contentsToFetch = new ArrayList<>();
        contentIds.forEach(contentId -> {
            if (contentToGroup.containsKey(contentId)) {
                contentToGroup.get(contentId).addTask(taskId);
            } else {
                contentsToFetch.add(contentId);
            }
        });

        // fetch contents that were not in cache and add them
        if (!contentsToFetch.isEmpty()) {
            List<SolrContentMetadataEntity> contents = contentMetadataService.findByIds(contentsToFetch);
            contents.forEach(content ->
                    addContentToCache(taskId, Long.parseLong(content.getId()), content.getAnalysisGroupId())
            );
        }
    }

    // handle task done and free memory where possible
    public synchronized void handleTasksDone(List<Long> taskIds) {
        if (taskIds == null || taskIds.isEmpty()) return;

        taskIds.forEach(taskId -> {
            Set<Long> taskContentList = tasksToContents.get(taskId);
            if (taskContentList != null) {
                taskContentList.forEach(contentId -> {
                    ContentGroupData data = contentToGroup.get(contentId);
                    data.getRelatedTaskIds().remove(taskId);
                    if (data.getRelatedTaskIds().isEmpty()) {
                        contentToGroup.remove(contentId);
                    }
                });
                tasksToContents.remove(taskId);
            }
        });
    }

    // if content has no group set new group otherwise return false
    public synchronized boolean setContentGroupIfEmpty(Long contentId, String groupId) {
        if (contentToGroup.containsKey(contentId)) {
            ContentGroupData data = contentToGroup.get(contentId);
            if (Strings.isNullOrEmpty(data.getGroupId())) {
                data.setGroupId(groupId);
                return true;
            }
        } else {
            throw new RuntimeException("cant find content " + contentId + " in cache something is wrong!");
        }
        return false;
    }

    public Map<Long, String> getContentToGroup(Collection<Long> contentIds) {
        Map<Long, String> result = Maps.newHashMap();

        // Split by exists in cache and not exists in cache
        Map<Boolean, List<Long>> inCacheIndicationMap = contentIds.stream()
                .collect(Collectors.partitioningBy(contentId -> contentToGroup.containsKey(contentId)));

        addNotInCacheEntriesToResult(result, inCacheIndicationMap.get(false));
        addInCacheEntriesToResult(result, inCacheIndicationMap.get(true));
        return result;
    }

    private void addInCacheEntriesToResult(Map<Long, String> result, List<Long> contentIds) {
        contentIds.stream()
                .forEach(contentId -> result.put(contentId, contentToGroup.get(contentId).groupId));
    }

    private void addNotInCacheEntriesToResult(Map<Long, String> result, List<Long> notInCache) {
        if (notInCache.isEmpty()) {
            return;
        }
        List<Map<SolrCoreSchema, Object>> analysisDataMap = contentMetadataService.findAnalysisDataByContentIds(notInCache);
        if (analysisDataMap != null && !analysisDataMap.isEmpty()) {
            for (Map<SolrCoreSchema, Object> map : analysisDataMap) {
                result.put(Long.valueOf((String) map.get(ContentMetadataFieldType.ID)),
                        (String) map.get(ContentMetadataFieldType.GROUP_ID));
            }
        }
    }

    private class ContentGroupData {
        private String groupId;
        private List<Long> relatedTaskIds = new ArrayList<>();

        String getGroupId() {
            return groupId;
        }

        void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        List<Long> getRelatedTaskIds() {
            return relatedTaskIds;
        }

        void addTask(Long taskId) {
            relatedTaskIds.add(taskId);
        }
    }
}
