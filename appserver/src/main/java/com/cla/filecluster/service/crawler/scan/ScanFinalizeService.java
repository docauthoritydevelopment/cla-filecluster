package com.cla.filecluster.service.crawler.scan;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.event.EventType;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.jobmanager.*;
import com.cla.common.scan.DeletedItemsProcessor;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.progress.ProgressTracker;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.jobmanager.service.ProgressPercentageTracker;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.event.ScanDiffEventProvider;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;

import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class ScanFinalizeService {

    private static final Logger logger = LoggerFactory.getLogger(ScanFinalizeService.class);

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private FileCrawlerExecutionDetailsService runService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private ScanDiffEventProvider scanDiffEventProvider;

    @Value("${scan.sanity.threshold.activation:50}")
    private long sanityThresholdMinimum;

    @Value("${scan.sanity.threshold.minimumPercentage:0.01}")
    private double sanityThresholdPercentage;

    @Value("${scan.dir-listing.enabled:true}")
    private boolean dirListingEnabled;

    @Value("${scan.deleted-fetch.pageSize:500}")
    private int scanDeletedFetchSize;

    @Value("${scan.deleted-items.batch-size:5000}")
    private int deletedItemsBatchSize;

    @Value("${scanner.scanTaskDepth:0}")
    private int scanTaskDepth;

    @Value("${ingester.force.ingest.scanned:true}")
    private boolean forceIngestMappedOnlyFiles;

    private DeletedItemsProcessor deletedItemsProcessor;


    @PostConstruct
    public void init() {
        deletedItemsProcessor = new DeletedItemsProcessor(kvdbRepo);
    }

    public void resumeScanFinJob(long runId, long rootFolderId, long scanFinJobId) {
        SimpleJobDto scanFinJobDto = jobManagerService.getJobAsDto(scanFinJobId);

        if (scanFinJobDto.getState() != JobState.IN_PROGRESS) {
            // make sure the job is IN_PROGRESS before proceeding
            jobManagerService.updateJobState(scanFinJobId, JobState.IN_PROGRESS, null, "Resumed scan finalization", true);
        }
    }

    @AutoAuthenticate
    public void runScanFinalize(long runId, long rootFolderId, Long scanFinalizeJobId, boolean scannedFromScratch, long taskId) {
        JobStateUpdateInfo updateScanFinStateInfo = null;
        try {
            if (scanFinalizeJobId == null) {
                logger.error("runScanFinalize() called with null scanFinalizeJobId");
            }

            ProgressPercentageTracker progressTracker = new ProgressPercentageTracker(scanFinalizeJobId, jobManagerService);
            if (forceIngestMappedOnlyFiles) { //dispatch Ingest Tasks for MappedOnly files
                RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
                jobManagerService.createIngestTasksForMappedOnlyFiles(runId, rootFolder);
            }
            scanFinalize(runId, rootFolderId, scanFinalizeJobId, scannedFromScratch, progressTracker);

            jobManagerService.updateTaskStatus(taskId, TaskState.DONE);

            updateScanFinStateInfo =
                    new JobStateUpdateInfo(scanFinalizeJobId, JobState.DONE, null, "Map finalize finished", true);

            logger.debug("Scan finalize job status: {}", jobManagerService.getJobAsDto(scanFinalizeJobId));
            handleIngestJob(runId, updateScanFinStateInfo);
        } catch (Exception e) {
            logger.error("Failed to run the scan finalize job", e);
            jobManagerService.updateTaskStatus(taskId, TaskState.FAILED);
            jobManagerService.handleFailedFinishJob(scanFinalizeJobId, "Scan finalize failed");
            jobManagerAppService.endRun(runId, PauseReason.SYSTEM_ERROR);
        }
    }

    public void scanFinalize(long runId, long rootFolderId, Long scanFinalizeJobId, boolean scannedFromScratch, ProgressTracker progressTracker) {
        progressTracker.startStageTracking(35);
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
        int scanTaskDepthForRootFolder = rootFolder.getScanTaskDepth() == null ? scanTaskDepth : rootFolder.getScanTaskDepth();

        final MediaType mediaType = rootFolder.getMediaType();
        if (mediaType.equals(MediaType.EXCHANGE_365) && rootFolder.getReingestTimeStampMs() > 0) {
            logger.debug("Handling deletions, reason: media type is {} and it's a full rescan", mediaType);
            jobManagerService.setOpMsg(scanFinalizeJobId, "Handle deletions");
            handleDeletions(runId, rootFolderId, progressTracker, null);
        } else if (scannedFromScratch && !dirListingEnabled && scanTaskDepthForRootFolder == 0) { // only if not multi task
            // Only in case of FileShare or [OD,SP,BOX] with full rescan then handle deletion.
            if (!mediaType.isSupportsNativeChangeLog() || rootFolder.getReingestTimeStampMs() > 0) {
                logger.debug("Handling deletions, reason: media type is {} and scan is {}", mediaType,
                        rootFolder.getReingestTimeStampMs() > 0 ? "Full Rescan" : "Rescan");
                jobManagerService.setOpMsg(scanFinalizeJobId, "Handle deletions");
                handleDeletions(runId, rootFolderId, progressTracker, null);
            }
        } else if (scanTaskDepthForRootFolder > 0) { // if multi task - chk specific tasks relevant
            Long scanJobId = jobManagerService.getJobIdCached(runId, JobType.SCAN);
            List<SimpleTask> tasks = jobManagerService.getTaskByJobIdAndTaskTypeAndStates(scanJobId, TaskType.SCAN_ROOT_FOLDER, TaskState.DONE);
            Map<String, Boolean> paths = new HashMap<>();
            tasks.forEach(t -> { // get all tasks marked for deletion on server side
                String data = t.getJsonData();
                Map<String, Object> scanTaskParams = ModelDtoConversionUtils.jsonToMap(data);
                if (scanTaskParams.containsKey("runDeletePerPath") && scanTaskParams.get("runDeletePerPath").equals("true")) {
                    String p = (t.getPath() == null ? rootFolder.getRealPath() : t.getPath());
                    paths.put(p, Boolean.valueOf((String) scanTaskParams.get("fstLevelOnly")));
                }
            });
            if (paths.size() > 0) {
                logger.debug("Handling deletions, reason: mediaType {}, it's a multi-task scan and {} paths to handle",
                        mediaType, paths.size());
                jobManagerService.setOpMsg(scanFinalizeJobId, "Handle deletions");
                handleDeletions(runId, rootFolderId, progressTracker, paths);
            } else {
                progressTracker.endCurStage();
            }
        } else {
            progressTracker.endCurStage();
        }

        handleContainerFilesWithNoTopId(rootFolderId);

        if (logger.isDebugEnabled()) {
            logger.debug("FolderCacheStats: {}", docFolderService.getDocFolderCacheStats());
        }
        logger.info("Run scan finalize on root folder {} under run {}", rootFolderId, runId);
        jobManagerService.setOpMsg(scanFinalizeJobId, "Count directories under rootFolder");
        progressTracker.startStageTracking(65);
        updateRootFolderFoldersCount(rootFolderId);
        progressTracker.endCurStage();
        jobManagerService.checkIfJobPaused(scanFinalizeJobId);

        progressTracker.endTracking();
    }

    private void handleContainerFilesWithNoTopId(Long rootFolderId) {
        try {
            logger.trace("setting package top ids for files for root folder {}", rootFolderId);
            List<SolrFileEntity> files = fileCatService.findContainerFilesWithNoTopId(rootFolderId);
            if (files != null && !files.isEmpty()) {
                logger.debug("setting package top ids for {} files for root folder {}", files.size(), rootFolderId);
                RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);
                List<SolrInputDocument> fileUpdates = new ArrayList<>();
                files.forEach(f -> {
                    Long containerId = fileCatService.getContainerId(rootFolderId, rf.getRealPath() + f.getFullName(), f.getMediaEntityId());
                    if (containerId > 0) {
                        SolrInputDocument doc = AtomicUpdateBuilder.create()
                                .setId(CatFileFieldType.ID, f.getId())
                                .addModifier(CatFileFieldType.PACKAGE_TOP_FILE_ID, SolrOperator.SET, containerId)
                                .addModifier(CatFileFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                                .build();
                        fileUpdates.add(doc);
                    }
                });
                fileCatService.saveAll(fileUpdates);
            }

        } catch (Exception e) {
            logger.error("error setting package top ids for files for root folder {}", rootFolderId, e);
        }
    }

    private void handleIngestJob(Long runId, JobStateUpdateInfo scanFinStateInfo) {
        CrawlRunDetailsDto crawlRun = runService.getRunDetails(runId);
        Long ingestJobId = jobManagerService.getJobIdCached(runId, JobType.INGEST);

        long ingestTasksCount = ingestJobId == null ? 0 : solrTaskService.getIngestTaskCount(runId);
        if (ingestTasksCount == 0 || !crawlRun.isIngestFiles()) {
            logger.debug("No ingest tasks or job were created for run #{}", runId);

            // DAMAIN-3150: if we had deletions - avoid skipping Ingest-Fin
            final boolean gotDeletions = crawlRun.getDeletedFiles() > 0;
            List<JobStateUpdateInfo> jobStateUpdateInfos = new ArrayList<>();
            jobStateUpdateInfos.add(scanFinStateInfo);

            // skip ingest if exists (no tasks or no job)
            Optional.ofNullable(ingestJobId).ifPresent(jobId -> {
                        logger.debug("no tasks, skipping ingest job {}", ingestJobId);
                        jobStateUpdateInfos.add(
                                new JobStateUpdateInfo(jobId, JobState.DONE, JobCompletionStatus.SKIPPED, "No files to ingest", true));
                    }
            );

            Long ingestFinJobId = jobManagerService.getJobIdCached(runId, JobType.INGEST_FINALIZE);
            if (gotDeletions && ingestFinJobId != null) { // continue to ingest fin to handle deletion compute
                logger.debug("has deletions, start ingest fin job {} for run {}", ingestFinJobId, runId);
                jobStateUpdateInfos.add(
                        new JobStateUpdateInfo(ingestFinJobId, JobState.IN_PROGRESS, null, "Starting ingestion finalization", false));
            } else { // mark all jobs left as skipped - nothing to do
                logger.debug("no deletions or no ingest fin, skip all jobs following ingest for run {} ingest fin {}", runId, ingestFinJobId);
                JobType.getJobTypesFollowing(JobType.INGEST).forEach(jobType ->
                        jobManagerService.getJobIdsByRunAndType(runId, jobType).
                                stream().
                                filter(Objects::nonNull).
                                forEach(jobId -> {
                                        jobStateUpdateInfos.add(
                                                new JobStateUpdateInfo(jobId, JobState.DONE, JobCompletionStatus.SKIPPED, "No files to ingest", true));
                                }));
            }

            jobManagerService.updateJobStates(jobStateUpdateInfos);
            if (!gotDeletions || ingestFinJobId == null) {
                logger.debug("no deletions or no ingest fin, end run {} ingest fin {}", runId, ingestFinJobId);
                jobManagerAppService.endRun(runId, PauseReason.SYSTEM_INITIATED);
            }
        } else {
            if (crawlRun.isIngestFiles()) {
                jobManagerService.updateEstimatedTaskCount(ingestJobId, ingestTasksCount);
                JobStateUpdateInfo updateIngestStateInfo =
                        new JobStateUpdateInfo(ingestJobId, JobState.IN_PROGRESS, null, "Start ingesting", false);
                jobManagerService.updateJobStates(Arrays.asList(scanFinStateInfo, updateIngestStateInfo));
                logger.info("Set INGEST JOB {} state to {}", ingestJobId, JobState.IN_PROGRESS);
                jobManagerService.updateMpJobState(ingestJobId);
            } else {
                jobManagerService.pauseJob(ingestJobId, runId, "Not marked for ingest", true, scanFinStateInfo);
                jobManagerAppService.endRun(runId, PauseReason.SYSTEM_INITIATED);
            }
        }
    }

    /**
     * Update the field foldersCount under the rootFolder.
     * Counts the folders in the database that belong to this particular root folder
     *
     * @param rootFolderId root folder ID
     * @return calculated sub folders count
     */
    private int updateRootFolderFoldersCount(long rootFolderId) {
        int size = docFolderService.countFoldersUnderRootFolder(rootFolderId);
        logger.debug("Update rootFolder {} folder count to {}", rootFolderId, size);
        docStoreService.updateRootFolderFoldersCount(rootFolderId, size);
        return size;
    }


    @Transactional
    public void scanSanityCheck(long rootFolderId, long jobId) {
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        //TODO: implement ... as a minimum check that file/foldeer tables are not empty
        logger.debug("Verify rescan gave sane results on {}", rootFolder);
        jobManagerService.setOpMsg(jobId, "Scan sanity check");
        //fileCrawlerFolderScannerService.verifyTempFilesSanityThresholds(rootFolder);
    }

    private void handleDeletions(long runId, long rootFolderId, ProgressTracker progressTracker, Map<String, Boolean> paths) {
        CrawlRun currentRun = runService.getCrawlRun(runId);
        long startTime = currentRun.getStartTime().getTime();
        String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
        int deletedItemsCount = deletedItemsProcessor.processDeletedItems(runId, startTime, storeName, progressTracker,
                deletedFileIds -> handleDeletedFiles(runId, deletedFileIds),
                docFolderService::markDocFoldersAsDeleted, deletedItemsBatchSize, paths, this::generateScanDiffDeleteEvent);
        logger.info("Marked {} files/folders as deleted under root folder {}", deletedItemsCount, rootFolderId);
    }

    private void generateScanDiffDeleteEvent(String key, FileEntity fileEntity) {
        Long entityId = null;
        if (fileEntity != null) {
            entityId = fileEntity.isFile() ? fileEntity.getFileId() : fileEntity.getFolderId();
        }
        if (entityId != null) {
            scanDiffEventProvider.generateScanDiffEvent(EventType.SCAN_DIFF_DELETED, entityId, key);
        } else {
            logger.warn("Failed to extract entity ID from FileEntity {}", fileEntity);
        }
    }

    private void handleDeletedFiles(long runId, List<Long> deletedFileIds) {
        int deletedFilesCount = deletedFileIds.size();
        if (deletedFilesCount > 0) {
            fileCatService.processFilesDeletion(deletedFileIds);
            runService.incDeletedFiles(runId, deletedFilesCount);
        }
    }

    public boolean verifyTempFilesSanityThresholds(RootFolder rootFolder) {
        // TODO: see if this is still necessary when we finalize deletions handling
//        long oldfilesInRootFolder = claFileService.countFilesUnderRootFolder(rootFolder.getId());
//        long newFilesInRootFolder = crawlTempFileRepository.countFilesUnderRootFolder(rootFolder.getId());
//        String message = "There were " + oldfilesInRootFolder + " files in root folder " + rootFolder + " and now scanned for " + newFilesInRootFolder + " files.";
//        logger.debug(message);
//        if (newFilesInRootFolder == 0 && oldfilesInRootFolder > sanityThresholdMinimum) {
//            logger.warn("Sanity thresholds verification failed (no files) - {}", message);
//            throw new RuntimeException("Sanity thresholds verification failed (no files) - " + message);
//        }
//        if (newFilesInRootFolder > 0 && oldfilesInRootFolder > 0) {
//            double newPercentage = newFilesInRootFolder / Long.valueOf(oldfilesInRootFolder).doubleValue();
//            if (newPercentage < sanityThresholdPercentage) {
//                logger.warn("Sanity thresholds verification failed (percentage) - {}", message);
//                throw new RuntimeException("Sanity thresholds verification failed (percentage) - " + message);
//            }
//        }
        return true;
    }
}
