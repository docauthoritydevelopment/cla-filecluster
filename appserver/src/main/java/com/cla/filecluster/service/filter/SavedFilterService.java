package com.cla.filecluster.service.filter;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.scope.Scope;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.filter.SavedFilter;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.filter.SavedFilterRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.drew.lang.annotations.Nullable;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Map;

/**
 * Created by uri on 08/11/2015.
 */
@Service
public class SavedFilterService {

    @Autowired
    private SavedFilterRepository savedFilterRepository;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private MessageHandler messageHandler;

    private final static Logger logger = LoggerFactory.getLogger(SavedFilterService.class);

    @Value("${filters.template.file:config/saved_filters.csv}")
    private String defaultSavedFiltersFilePath;

    @Autowired
    private CsvReader csvReader;

    private static final String NAME = "Name";
    private static final String RAW_FILTER = "Raw_filter";
    private static final String FILTER_DESCRIPTOR = "Filter_descriptor";

    private final static String[] requiredHeaders = {NAME, RAW_FILTER, FILTER_DESCRIPTOR};

    @Transactional(readOnly = true)
    public SavedFilter getSavedFilterByName(String name) {
        return savedFilterRepository.findByName(name);
    }

    @Transactional
    public void loadDefaultSavedFiltersFile() {
        logger.info("load Default Saved filters from {}", defaultSavedFiltersFilePath);
        createSavedFiltersFromFile(defaultSavedFiltersFilePath);
    }

    private void createSavedFiltersFromFile(String defaultSavedFiltersFilePath) {
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, defaultSavedFiltersFilePath, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(defaultSavedFiltersFilePath, row -> createSavedFilterFromCsvRow(row, headersLocationMap), null, null);
        } catch (Exception e) {
            logger.error("Failed to read saved filters file. Got exception " + e.getMessage(), e);
            throw new RuntimeException("Failed to read saved filters file. Got exception " + e.getMessage(), e);
        }
        if (failedRows != 0) {
            logger.error("Failed to read {} saved filter lines", failedRows);
        }
    }

    private void createSavedFilterFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String raw = fieldsValues[headersLocationMap.get(RAW_FILTER)].trim();
        String desc = fieldsValues[headersLocationMap.get(FILTER_DESCRIPTOR)].trim();
        logger.debug("Create SavedFilter by name {} and raw {} and filter {}", name, raw, desc);
        SavedFilter savedFilter = new SavedFilter();
        savedFilter.setName(name);
        savedFilter.setRawFilter(raw);
        savedFilter.setFilterDescriptor(convertFilterDescriptorJson(desc));
        savedFilterRepository.save(savedFilter);

    }

    @Transactional(readOnly = true)
    public SavedFilter getById(long id) {
        return savedFilterRepository.findById(id).orElse(null);
    }

    @Transactional
    public SavedFilter createSavedFilter(String name, String rawFilter, FilterDescriptor desc, @Nullable User owner, Scope scope) {

        validateActionSchedule(name);

        SavedFilter savedFilter = new SavedFilter();
        savedFilter.setName(name);
        savedFilter.setRawFilter(rawFilter);
        savedFilter.setFilterDescriptor(desc);

        if (owner != null) {
            savedFilter.setOwner(owner);
        }

        BasicScope basicScope = scope == null ? null : scopeService.getOrCreateScope(scope);
        savedFilter.setScope(basicScope);

        validateFilterData(savedFilter);

        SavedFilter createdFilter = savedFilterRepository.save(savedFilter);
        return createdFilter;
    }

    @Transactional(readOnly = true)
    public Page<SavedFilter> listSavedFilters(Pageable pageRequest) {
        return savedFilterRepository.findAll(pageRequest);
    }

    @Transactional
    public void deleteSavedFilter(long id) {
        savedFilterRepository.deleteById(id);
    }

    @Transactional
    public SavedFilter updateSavedFilterData(long id, String name, String rawFilter, FilterDescriptor desc, Scope scope) {
        SavedFilter savedFilter = savedFilterRepository.getOne(id);

        if (!savedFilter.getName().equalsIgnoreCase(name)) {
            validateActionSchedule(name);
        }

        BasicScope basicScope = scope == null ? null : scopeService.getOrCreateScope(scope);
        savedFilter.setScope(basicScope);

        savedFilter.setName(name);
        savedFilter.setRawFilter(rawFilter);
        savedFilter.setFilterDescriptor(desc);
        validateFilterData(savedFilter);
        return savedFilterRepository.save(savedFilter);
    }

    private void validateFilterData(SavedFilter savedFilter) {
        if (savedFilter.getFilterDescriptor() == null) {
            throw new BadRequestException(messageHandler.getMessage("saved-filter.descriptor.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void validateActionSchedule(String name) {
        if (Strings.isNullOrEmpty(name)) {
            throw new BadRequestException(messageHandler.getMessage("saved-filter.name.empty"), BadRequestType.MISSING_FIELD);
        }

        //SavedFilter filter = savedFilterRepository.findByName(name);
        //if (filter != null) {
        //    throw new BadRequestException(messageHandler.getMessage("saved-filter.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        //}
    }

    public static FilterDescriptor convertFilterDescriptorJson(String filterDescriptorJson) {
        if (StringUtils.isEmpty(filterDescriptorJson)) {
            return null;
        }
        try {
            //{"logic":null,"filters":null,"field":"docTypeId","value":1,"operator":{"present":true},"ignoreCase":true}
            return ModelDtoConversionUtils.getMapper().readValue(filterDescriptorJson, FilterDescriptor.class);
        } catch (IOException e) {
            logger.error("Failed to convert filterDescriptorJson (" + filterDescriptorJson + ") to FilterDescriptor (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert filterDescriptorJson to FilterDescriptor (IO Exception): " + e.getMessage());
        }
    }

    public static String convertFilterDescriptorJson(FilterDescriptor filter) {
        if (filter == null) {
            return null;
        }
        try {

            return ModelDtoConversionUtils.getMapper().writeValueAsString(filter);
        } catch (IOException e) {
            logger.error("Failed to convert filterDescriptor (" + filter + ") to FilterDescriptor json (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert filterDescriptor to FilterDescriptor json (IO Exception): " + e.getMessage());
        }
    }
}
