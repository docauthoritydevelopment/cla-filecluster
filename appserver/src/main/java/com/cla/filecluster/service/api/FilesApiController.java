package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDetailsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.group.AttachFilesToGroupDto;
import com.cla.common.domain.dto.group.GroupDetailsDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.NewJoinedGroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.MultiValueListDto;
import com.cla.common.exceptions.NotSupportedException;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.analyze.AnalyzeAppService;
import com.cla.filecluster.service.crawler.analyze.AnalyzeFinalizeService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/file")
public class FilesApiController {
    private static final Logger logger = LoggerFactory.getLogger(FilesApiController.class);

    @Value("${reportPageSize:20}")        // TODO: allow page size config, either in request or as a user config.
    private int defaultPageSize;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private AnalyzeFinalizeService analyzeFinalizeService;

    @Autowired
    private AnalyzeAppService analyzeAppService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private ControllerUtils controllerUtils;

    @RequestMapping(value = "/dn", method = RequestMethod.GET)
    public ResponseEntity<?> findFilesDeNormalizedRest(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        try {
            PageImpl<ClaFileDto> list = filesApplicationService.listFilesDeNormalized(dataSourceRequest);
            if (dataSourceRequest.isExport()) {
                eventAuditService.audit(AuditType.EXPORT,"Discover files export", AuditAction.VIEW,  ClaFileDto.class,null, dataSourceRequest);
            } else {
                eventAuditService.audit(AuditType.QUERY,"Discover files", AuditAction.VIEW,  ClaFileDto.class,null, dataSourceRequest);
            }
            return new ResponseEntity<>(list, HttpStatus.OK);
        }
        catch (Exception e) {
            if (dataSourceRequest.getPageSize() < 5 &&
                    e.getMessage() != null &&
                    e.getMessage().contains("org.apache.solr.search.SyntaxError: Cannot parse 'content")) {
                logger.debug("Bad content search: content-filter={} message={}", dataSourceRequest.getContentFilter(), e.getMessage());
                return new ResponseEntity<>("Bas content search", HttpStatus.BAD_REQUEST);
            }
            throw e;
        }
    }

    // Wrapper for testing
    public PageImpl<ClaFileDto> findFilesDeNormalized(final Map<String, String> params) {
        ResponseEntity<?> res = findFilesDeNormalizedRest(params);
        if (res.getStatusCode() == HttpStatus.OK) {
            return (PageImpl<ClaFileDto>) res.getBody();
        }
        throw new RuntimeException("Bad response " + res.getStatusCode().toString() + ": " + res.getBody().toString());
    }

    // Testing utility
    public int getNoTagsSolrMissMatch() {
        Map<String, String> params = new HashMap<>();
        FilterDescriptor filterDescriptor = new FilterDescriptor("file.tags","*","neq");
        String filterString = JsonDtoConversionUtils.convertFilterDescriptorToJson(filterDescriptor);
        params.put("filter",filterString);
        PageImpl<ClaFileDto> filesDeNormalized = findFilesDeNormalized(params);
        logger.debug("Got back {} files",filesDeNormalized.getContent().size());
        int counter = 0;
        for (ClaFileDto claFileDto : filesDeNormalized.getContent()) {
            if (claFileDto.getFileTagDtos().size() > 0) {
                logger.warn("Unexpected files with tags {} ", claFileDto);
            }
            counter += claFileDto.getFileTagDtos().size();
        }
        return counter;
    }

    @RequestMapping(value = "/dn/count", method = RequestMethod.GET)
    public long countFilesDeNormalized(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "file count export", AuditAction.VIEW, null, null, dataSourceRequest);
        }
        return filesApplicationService.countFilesDeNormalized(dataSourceRequest);
    }

    @RequestMapping(value = "/dn/access/histogram", method = RequestMethod.GET)
    public List<AggregationCountItemDTO<String>> createAccessHistogram(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return filesApplicationService.createAccessHistogram(dataSourceRequest);
    }

    @RequestMapping(value = "/titles/{id}")
    public List<String> getTitles(@PathVariable("id") final long fileId) {
        // Null response if not found
        Long contentId = fileCatService.getClaFileContentId(fileId);
        return filesDataPersistenceService.findProposedTitlesByContentId(contentId);
    }

    @RequestMapping(value = "/compare", method = RequestMethod.GET)
    public MultiValueListDto countFilesComparison(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return filesApplicationService.countFilesComparison(dataSourceRequest);
    }

    @RequestMapping(value = "/types/count", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO> findFileTypeCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return filesApplicationService.findFileTypesCount(dataSourceRequest);
    }

    //http://localhost:8080/api/file/full?page=3
    @RequestMapping(value = "/full")
    public Page<ClaFile> findAllFull(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "0") int pageSize) {
        if (page == 0) {
            logger.warn("Zero page value found in 1-origin service set to 1");
            page = 1;
        }
        if (pageSize == 0) {
            pageSize = defaultPageSize;
        }
        return filesApplicationService.findAllFull(PageRequest.of(page - 1, pageSize));
    }

    //http://localhost:8080/api/file/list?page=3
    @RequestMapping(value = "/list")
    public Page<Object[]> findAllList(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "0") int pageSize) {
        if (page == 0) {
            logger.warn("Zero page value found in 1-origin service set to 1");
            page = 1;
        }
        if (pageSize == 0) {
            pageSize = defaultPageSize;
        }
        return fileCatService.findAllReportListPageOrdered(PageRequest.of(page - 1, pageSize));
    }

    //http://localhost:8080/api/file/full/1234
    @RequestMapping(value = "/full/{id}")
    public ClaFileDetailsDto findFileDetailsById(@PathVariable("id") final long fileId) {
        ClaFileDetailsDto claFileDetailsDto = filesApplicationService.findFileDetailsById(fileId);
        eventAuditService.audit(AuditType.FILE,"View file details", AuditAction.VIEW_INFO, ClaFile.class, fileId);
        return claFileDetailsDto;
    }

    @RequestMapping(value = "/content/{contentId}/files")
    public List<ClaFileDto> listFilesOfSpecificContentId(@PathVariable("contentId") final long contentId) {
        return filesApplicationService.listFilesOfSpecificContentId(contentId);
    }

    //http://localhost:8080/api/file/full/group/26e4698b-3afc-4d29-a013-0b2b6f611a85?page=2
    @RequestMapping(value = "/full/group/{id}")
    public Page<ClaFileDto> findByGroupFull(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "0") int pageSize,
                                         @PathVariable("id") final String groupId) {
        if (page == 0) {
            logger.warn("Zero page value found in 1-origin service set to 1");
            page = 1;
        }
        if (pageSize == 0) {
            pageSize = defaultPageSize;
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setPageSize(pageSize);
        dataSourceRequest.setPage(page - 1);
        return filesApplicationService.listFilesByGroup(dataSourceRequest, groupId, true);
    }

    //http://localhost:8080/api/file/full/group/26e4698b-3afc-4d29-a013-0b2b6f611a85?page=2
    @RequestMapping(value = "/list/group/{id}")
    public Page<ClaFileDto> ListFilesByGroup(@RequestParam final Map params, @PathVariable("id") final String groupId) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        //new PageRequest(page-1, pageSize), groupId
        return filesApplicationService.listFilesByGroup(dataSourceRequest, groupId, false);
    }

    @RequestMapping(value = "/{fileIds}/group/detach", method = RequestMethod.POST)
    @Transactional
    public void detachFileFromGroup(@PathVariable List<Long> fileIds, @RequestParam final Map<String, String> params) {
        List<String> errors = Lists.newArrayList();
        fileIds.forEach(fileId -> {
            SolrFileEntity claFileDto = fileCatService.getFileEntity(fileId);
            String groupId = claFileDto.getUserGroupId();
            if (filesApplicationService.isFileInJoinedGroup(fileId)) {
                try {
                    groupsApplicationService.removeFileFromUserGroup(fileId);
                    eventAuditService.auditDual(AuditType.FILE, "Detach file from user group ", AuditAction.DETACH, SolrFileEntity.class, claFileDto.getFileId(), GroupDto.class, groupId, claFileDto);
                } catch (NotSupportedException e) {
                    errors.add(e.getMessage());
                }
            } else {
                try {
                    filesApplicationService.excludeFileFromGrouping(fileId);
                    eventAuditService.auditDual(AuditType.FILE, "Detach file from  group ", AuditAction.DETACH, SolrFileEntity.class, claFileDto.getFileId(), GroupDto.class, groupId, claFileDto);
                } catch (Exception e) {
                    logger.error("problem detach file {} from  group {}", fileId, groupId, e);
                    errors.add(e.getMessage());
                }
            }
        });

        if (errors.size() > 0) {
            String errStr = errors.stream().collect(Collectors.joining("\n"));
            throw new BadRequestException(messageHandler.getMessage("group.detach.failure", Lists.newArrayList(errStr)), BadRequestType.OPERATION_FAILED);
        }
    }

    @RequestMapping(value = "/{fileId}/group/analyze", method = RequestMethod.POST)
    public void findGroupAndAttachToFile(@PathVariable long fileId, @RequestParam Map<String, String> params) {
        try {
            filesApplicationService.removeFileGroupingOverrides(fileId);
            logger.info("Analyze the file again after removing grouping overrides");
            analyzeAppService.analyzeSingleFile(fileId);
            //TODO 2.0 - do a miniature finalize only on the relevant files
            Runnable task = () ->
                    analyzeFinalizeService.runFinalizeAnalysisForJob(null);
            controllerUtils.runInOtherThreadAsAdmin(task);
        } catch (Exception e) {
            logger.error("error attach to group for file {}",fileId, e);
            throw new BadRequestException(messageHandler.getMessage("operation.failed"), BadRequestType.OPERATION_FAILED);
        }
    }

    @RequestMapping(value = "/group/attach", method = RequestMethod.POST)
    public GroupDetailsDto attachFilesToGroup(@RequestBody AttachFilesToGroupDto attachFilesToGroupDto,
                                              @RequestParam Map<String, String> params) {
        logger.info("Attach files to group {}", attachFilesToGroupDto);
        GroupDetailsDto group = null;
        List<String> analysisGroups = filesApplicationService.createAnalysisGroupsForFiles(attachFilesToGroupDto.getFileIds());
        String targetGroupId = attachFilesToGroupDto.getGroupId();
        if (targetGroupId != null) {
            group = groupsApplicationService.createJoinedGroupIfNeeded(targetGroupId, attachFilesToGroupDto.getGroupName(), analysisGroups);
            eventAuditService.audit(AuditType.GROUP,"Attach files to existing group ", AuditAction.ATTACH, AttachFilesToGroupDto.class,targetGroupId, Pair.of("attachFilesToGroupDto",attachFilesToGroupDto));
        } else {
            NewJoinedGroupDto newJoinedGroupDto = new NewJoinedGroupDto();
            newJoinedGroupDto.setGroupName(attachFilesToGroupDto.getGroupName());
            newJoinedGroupDto.setChildrenGroupIds(analysisGroups);
            group = groupsApplicationService.createJoinedGroup(newJoinedGroupDto);
            eventAuditService.audit(AuditType.GROUP,"Attach files to new created group ", AuditAction.ATTACH, AttachFilesToGroupDto.class, Pair.of("attachFilesToGroupDto",attachFilesToGroupDto));
        }
        return group;
    }

    @RequestMapping(value = "/{fileId}/group/override/{newGroupId}", method = RequestMethod.POST)
    public void overrideFileGroup(@PathVariable long fileId, @PathVariable String newGroupId) {
        try {
            filesApplicationService.overrideFileGroup(fileId, newGroupId);
            eventAuditService.audit(AuditType.GROUP, "Attach file to group ", AuditAction.ATTACH, AttachFilesToGroupDto.class, newGroupId, Pair.of("fileId", fileId));
        } catch (Exception e) {
            logger.error("error override for file {} and group {}",fileId, newGroupId, e);
            throw new BadRequestException(messageHandler.getMessage("operation.failed"), BadRequestType.OPERATION_FAILED);
        }
    }
}
