package com.cla.filecluster.service.syscomponent;

import com.cla.common.domain.dto.components.solr.SolrCollectionDto;
import com.cla.common.domain.dto.components.solr.SolrNodeDto;
import com.cla.common.domain.dto.components.solr.SolrShardDto;
import com.cla.common.domain.dto.components.solr.SolrShardReplicaDto;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrResponse;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.common.util.NamedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by uri on 29-May-17.
 */
@Service
public class SolrComponentsAppService {

    private static final Logger logger = LoggerFactory.getLogger(SolrComponentsAppService.class);

    private SolrClient solrClient;

    @Autowired
    private SolrClientFactory solrClientFactory;

    @PostConstruct
    public void init() {
        solrClient = solrClientFactory.generalSolrClient().getUnderyingClient();
    }

    public List<SolrCollectionDto> listCollections() {
        CollectionAdminRequest listRequest = new CollectionAdminRequest.List();
        List<SolrCollectionDto> result = new ArrayList<>();
        try {
            SolrResponse process = listRequest.process(solrClient);
            List collections = (List) process.getResponse().get("collections");
            for (Object collection : collections) {
                result.add(new SolrCollectionDto(collection.toString()));
            }
            return result;
        } catch (SolrServerException | IOException e) {
            logger.error("Failed to list Solr collections",e);
            throw new RuntimeException("Failed to list Solr collections", e);
        }
    }

    public List<SolrNodeDto> getCollectionLiveNodes(String collection) {
        NamedList clusterStatus = getClusterStatus(collection);
        List<String> liveNodes = (List) clusterStatus.get("live_nodes");
        List<SolrNodeDto> result = Lists.newArrayList();
        for (String liveNode : liveNodes) {
//            logger.debug("Live node {}",liveNode.getClass().getName());
            result.add(new SolrNodeDto(liveNode));
        }
        return result;
    }

    private NamedList getClusterStatus(String collection) {
        NamedList<Object> response = getClusterStatusObject(collection).getResponse();
        return (NamedList) response.get("cluster");
    }

    public List<SolrShardDto> getCollectionShards(String collection) {
        Map collectionDetails = getClusterCollectionDetails(collection);
//        logger.debug("Collection details class {}",collectionDetails.getClass().getName());
        Map shards = (Map) collectionDetails.get("shards");
        List<SolrShardDto> result = Lists.newArrayList();
        for (Object o : shards.entrySet()) {
            String shardName = (String) ((Map.Entry) o).getKey();
            Map shardSettings = (Map) ((Map.Entry) o).getValue();
            SolrShardDto solrShardDto = new SolrShardDto();
            solrShardDto.setName(shardName);
            Object state = shardSettings.get("state");
            solrShardDto.setState(state.toString());
            Map replicas = (Map) shardSettings.get("replicas");
            fillReplicas(solrShardDto, replicas);
            result.add(solrShardDto);
        }
        return result;
    }

    private void fillReplicas(SolrShardDto solrShardDto, Map replicas) {
        for (Object replica : replicas.entrySet()) {
            String baseName = (String) ((Map.Entry) replica).getKey();
            Map value = (Map) ((Map.Entry) replica).getValue();
            SolrShardReplicaDto solrShardReplicaDto = new SolrShardReplicaDto();
            solrShardReplicaDto.setBaseName(baseName);
            solrShardReplicaDto.setCoreName(value.get("core").toString());
            solrShardReplicaDto.setActive(value.get("state").toString());
            solrShardReplicaDto.setLeader(value.get("leader").toString());
            solrShardReplicaDto.setNodeName(value.get("node_name").toString());
            solrShardDto.addReplica(solrShardReplicaDto);
        }
    }

    private Map getClusterCollectionDetails(String collection) {
        NamedList clusterStatus = getClusterStatus(collection);
        NamedList collections = (NamedList) clusterStatus.get("collections");
        return (Map) collections.get(collection);
    }

    private CollectionAdminResponse getClusterStatusObject(String collection) {
        CollectionAdminRequest.ClusterStatus clusterStatus = new CollectionAdminRequest.ClusterStatus();
        clusterStatus.setCollectionName(collection);
        try {
            CollectionAdminResponse response = clusterStatus.process(solrClient);
            for (Map.Entry<String, Object> entry : response.getResponse()) {
                logger.debug(entry.toString());
            }

            return response;
        } catch (SolrServerException | IOException e) {
            logger.error("Failed to get Solr cluster status",e);
            throw new RuntimeException("Failed to get Solr cluster status",e);
        }
    }

    public void replicateShard(String collection, String shard) {
        String asyncId = String.valueOf(new Random().nextLong());
        logger.warn("Replicate Shard {} on collection {} and track using {}",shard,collection,asyncId);
        CollectionAdminRequest.AddReplica addReplica = CollectionAdminRequest.AddReplica.addReplicaToShard(collection, shard);
        addReplica.setAsyncId(asyncId);

        try {
            CollectionAdminResponse process = addReplica.process(solrClient);
            NamedList<Object> response = process.getResponse();
            for (Map.Entry<String, Object> entry : response) {
                logger.info("Response {}:{}",entry.getKey(),entry.getValue());
            }
        }
        catch (SolrServerException | IOException e) {
            logger.error("Failed to Replicate Solr shard",e);
            throw new RuntimeException("Failed to Replicate Solr shard",e);
        }
    }

    public void deleteShardReplica(String collection, String shard, String replica) {
        logger.warn("Delete Shard {} replica {} on collection {}",shard,replica,collection);
        CollectionAdminRequest.DeleteReplica deleteReplica = CollectionAdminRequest.deleteReplica(collection,shard,replica);

        try {
            CollectionAdminResponse process= deleteReplica.process(solrClient);
            NamedList<Object> response = process.getResponse();
            for (Map.Entry<String, Object> entry : response) {
                logger.info("Response {}:{}",entry.getKey(),entry.getValue());
            }

        } catch (SolrServerException | IOException e) {
            logger.error("Failed to Delete Solr shard",e);
            throw new RuntimeException("Failed to Replicate Solr shard",e);
        }
    }

    public void getSolrRequestStatus(String requestId) {
        CollectionAdminRequest.RequestStatus requestStatus = CollectionAdminRequest.requestStatus(requestId);
        try {
            requestStatus.process(solrClient);
        } catch (SolrServerException | IOException e) {
            logger.error("Failed to Get Request Status of {]",requestId,e);
            throw new RuntimeException("Failed to Get Request Status of "+requestId,e);
        }
    }
}
