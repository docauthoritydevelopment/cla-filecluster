package com.cla.filecluster.service.export.serialize;

import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.bizlist.ExtractionSummaryDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


public class BizListExtractionSummarySerializer extends JsonSerializer<List<ExtractionSummaryDto>> {

    private static int maxExtractExampleNumber = 20;

    public static void setMaxExtractExampleNumber(int maxExtractExampleNumber) {
        BizListExtractionSummarySerializer.maxExtractExampleNumber = maxExtractExampleNumber;
    }

    @Override
    public void serialize(List<ExtractionSummaryDto> value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(
                Optional.ofNullable(value)
                        .map((list) ->
                                list.stream()
                                        .map(extraction -> {
                                            String res = extraction.getBizListItemType() + ">" +
                                                    extraction.getBizListName() + "(" + extraction.getCount() + ")";

                                            int min = Math.min(maxExtractExampleNumber, extraction.getSampleExtractions().size());

                                            if (extraction.getBizListItemType().equals(BizListItemType.CLIENTS)) {
                                                res += "[" + String.join(";", extraction.getSampleExtractions()
                                                        .subList(0, min).stream().map(SimpleBizListItemDto::getBusinessId)
                                                        .filter(Objects::nonNull).collect(Collectors.toList())) + "]";
                                            }

                                            return res;
                                        })
                                        .collect(Collectors.joining(";")))
                        .orElse(""));
    }
}
