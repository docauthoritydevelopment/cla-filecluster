package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filesearchpatterns.PatternCategoryDto;
import com.cla.common.domain.dto.filesearchpatterns.RegulationDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.extractor.pattern.RegulationAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by ophir on 10/04/2019.
 */
@RestController
@RequestMapping("/api/regulation")
public class RegulationApiController {

    @Autowired
    private RegulationAppService regulationAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    private Logger logger = LoggerFactory.getLogger(RegulationApiController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<RegulationDto> listRegulations(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return regulationAppService.listRegulations(dataSourceRequest);
    }

    @RequestMapping(value = "full", method = RequestMethod.GET)
    public Page<RegulationDto> listFullRegulations(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return regulationAppService.listRegulations(dataSourceRequest,true);
    }


    @RequestMapping(value = "", method = RequestMethod.PUT)
    public RegulationDto addRegulation(@RequestBody RegulationDto regulationDto) {
        RegulationDto regulation = regulationAppService.createRegulation(regulationDto);
        eventAuditService.audit(AuditType.REGULATIONS, "Create regulation", AuditAction.CREATE, RegulationDto.class, regulation.getId(), regulation);
        return regulation;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public RegulationDto updateRegulation(@RequestBody RegulationDto regulationDto) {
        RegulationDto regulation = regulationAppService.updateRegulation(regulationDto);
        eventAuditService.audit(AuditType.REGULATIONS, "Update regulation", AuditAction.UPDATE, PatternCategoryDto.class, regulation.getId(), regulation);
        return regulation;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    private RegulationDto getRegulation(@PathVariable Integer id) {
        RegulationDto regulationDto = regulationAppService.getRegulationById(id);
        if (regulationDto == null) {
            throw new BadRequestException(messageHandler.getMessage("regulation.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return regulationDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRegulation(@PathVariable int id) {
        RegulationDto regulationDto = getRegulation(id);
        regulationAppService.deleteRegulation(id);
        eventAuditService.audit(AuditType.REGULATIONS,"Delete regulation", AuditAction.DELETE, PatternCategoryDto.class,regulationDto.getId(),regulationDto);
    }

    @RequestMapping(value = "/{id}/enable", method = RequestMethod.POST)
    public RegulationDto enableRegulation(@PathVariable int id) {
        RegulationDto ansRegulationDto = regulationAppService.updateRegulationState(id, true);
        eventAuditService.audit(AuditType.REGULATIONS,"Update regulation", AuditAction.UPDATE, RegulationDto.class,ansRegulationDto.getId(),ansRegulationDto);
        return ansRegulationDto;
    }

    @RequestMapping(value = "/{id}/disable", method = RequestMethod.POST)
    public RegulationDto disableRegulation(@PathVariable int id) {
        RegulationDto ansRegulationDto = regulationAppService.updateRegulationState(id, false);
        eventAuditService.audit(AuditType.REGULATIONS,"Update regulation", AuditAction.UPDATE, RegulationDto.class,ansRegulationDto.getId(),ansRegulationDto);
        return ansRegulationDto;
    }


    @RequestMapping("/filecount")
    public FacetPage<AggregationCountItemDTO<RegulationDto>> getRegulationCount(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover regulations export", AuditAction.VIEW,  RegulationDto.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover regulations", AuditAction.VIEW,  RegulationDto.class,null, dataSourceRequest);
        }
        return regulationAppService.getRegulationsFileCounts(dataSourceRequest);
    }



}
