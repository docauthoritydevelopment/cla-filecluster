package com.cla.filecluster.service.api.reports;

import com.cla.common.domain.dto.report.*;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.report.TrendChartAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 09/11/2015.
 */
@RestController
@RequestMapping("/api/trendchart")
public class TrendChartApiController {
    private Logger logger = LoggerFactory.getLogger(TrendChartApiController.class);

    @Autowired
    private TrendChartAppService trendChartAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(method = RequestMethod.PUT ,value = "")
    public TrendChartDto createTrendChart(@RequestBody(required = true) TrendChartDto newTrendChart) {
        TrendChartDto trendChart = trendChartAppService.createTrendChart(newTrendChart);
        eventAuditService.audit(AuditType.TREND_CHART,"Create trend chart ", AuditAction.CREATE, TrendChartDto.class,newTrendChart);
        return trendChart;
    }

    @RequestMapping(method = RequestMethod.GET ,value = "")
    public Page<TrendChartDto> listTrendCharts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return trendChartAppService.listTrendCharts(dataSourceRequest);
    }

    @RequestMapping(method = RequestMethod.GET ,value = "/{id}")
    public TrendChartDto getTrendChart(@PathVariable long id, @RequestParam final Map params) {
        TrendChartDto chart = trendChartAppService.getTrendChart(id);
        if (chart == null) {
            throw new BadRequestException(messageHandler.getMessage("trend-chart.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return chart;
    }

    @RequestMapping(method = RequestMethod.POST,value = "")
    public TrendChartDto updateTrendChart(@RequestBody(required = true) TrendChartDto trendChartDto) {
        TrendChartDto trendChartDto1 = trendChartAppService.updateTrendChart(trendChartDto);
        eventAuditService.audit(AuditType.TREND_CHART,"Update trend chart ", AuditAction.UPDATE, TrendChartDto.class,trendChartDto);
        return trendChartDto1;
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/{id}")
    public void deleteTrendChart(@PathVariable long id,final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        trendChartAppService.deleteTrendChart(id,dataSourceRequest.isWait());
        eventAuditService.audit(AuditType.TREND_CHART,"Delete trend chart ", AuditAction.DELETE, TrendChartDto.class,id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/collect")
    public void runTrendCharts() {
        trendChartAppService.collectTrendChartsData(System.currentTimeMillis());
    }

    @RequestMapping(method = RequestMethod.GET ,value = "/{id}/data")
    public TrendChartDataSummaryDto getTrendChartView(@PathVariable long id,
                                                      @RequestParam(required = false) Long startTime,
                                                      @RequestParam(required = false) Long endTime,
                                                      @RequestParam(required = false) Long viewResolutionMs,
                                                      @RequestParam(required = false) Integer sampleCount,
                                                      @RequestParam(required = false) TimeResolutionType resolutionType,
                                                      @RequestParam(required = false) Integer viewResolutonCount,
                                                      @RequestParam(required = false) Long viewOffsetMs,
                                                      @RequestParam(required = false) Integer step) {

        if (resolutionType == null || resolutionType == TimeResolutionType.TR_miliSec) {
            resolutionType = TimeResolutionType.TR_miliSec;
            if (viewResolutionMs == null) {
                if (sampleCount != null && startTime != null && endTime != null) {
                    viewResolutionMs = (sampleCount > 1)?((endTime - startTime) / (sampleCount - 1)):0;
                }
                else {
                    viewResolutionMs = -1L; // get from chart
                }
            }
        }
        TrendChartResolutionDto tcResolutionDto = new TrendChartResolutionDto(resolutionType,
                ((resolutionType == TimeResolutionType.TR_miliSec)?viewResolutionMs:Long.valueOf(viewResolutonCount)),
                viewOffsetMs,
                step);
        return trendChartAppService.getTrendChartView(id,startTime,endTime,sampleCount, tcResolutionDto);
    }

    @RequestMapping(value="/import/csv", method=RequestMethod.POST)
    public @ResponseBody List<TrendChartDto> importCsvFile(@RequestParam("file") MultipartFile file){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("trend-chart.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import CSV File");
            byte[] bytes = file.getBytes();
        } catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(),e);
            List<TrendChartDto> res = new ArrayList<>();
        }
        return trendChartAppService.getAllTrendCharts();
    }

    @RequestMapping(method = RequestMethod.GET ,value = "/{id}/data/fabricate")
    public void fabricateTrendChartData(@PathVariable long id, @RequestParam(required = true) int days) {
        trendChartAppService.fabricateTrendChartData(id,days);
    }

}
