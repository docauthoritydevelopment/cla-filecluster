package com.cla.filecluster.service.files;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.MailboxDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.progress.DummyProgressTracker;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.FolderExcludeRule;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.crawler.analyze.AnalyzeFinalizeService;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.operation.DeleteRootFolderOperation;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.progress.ProgressService;
import com.cla.filecluster.service.progress.ProgressType;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Service
public class RootFolderService {

    private final static Logger logger = LoggerFactory.getLogger(RootFolderService.class);

    private static final int MAX_RECORDS_TO_PROCESS = 64000;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private ProgressService progressService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private MailboxService mailboxService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private ControllerUtils controllerUtils;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private AnalyzeFinalizeService analyzeFinalizeService;

    @Value("${app.scanned-root-folder.allow-delete:false}")
    private boolean allowScannedRootFolderDelete;

    @Value("${app.scanned-root-folder.delete-retry:3}")
    private int retryForRootFolderDelete;

    private Map<Long, ReadWriteLock> rootFolderMapLock = new ConcurrentHashMap<>();


    @Transactional
    public RootFolderDto createRootFolder(RootFolderDto rootFolderDto, Long scheduleGroupId) {
        Long defaultDocStoreAsDto = docStoreService.getDefaultDocStoreId();

        licenseService.validateRootFoldersLicense(docStoreService.countRootFolders());

        prepareRootFolderDtoForCreation(rootFolderDto);
        isValidForRootFolderCreation(rootFolderDto);
        validateScheduledGroup(scheduleGroupId);
        validateDepartment(rootFolderDto.getDepartmentId());

        RootFolderDto result = docStoreService.createRootFolder(defaultDocStoreAsDto, rootFolderDto.getMediaType(), rootFolderDto.getPath(),
                rootFolderDto.getRealPath(), rootFolderDto.getMediaEntityId());

        //Update rootFolder metadata
        RootFolder rootFolder = docStoreService.updateRootFolder(result.getId(), rootFolderDto, false);

        rootFolderDto.setPath(result.getPath());

        // TODO: revisit ...
        if (!(MediaType.FILE_SHARE.equals(rootFolder.getMediaType()) || rootFolder.getMediaConnectionDetailsId() != null)) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.media-conn.empty"), BadRequestType.MISSING_FIELD);
        }

        scheduleGroupService.attachRootFolderToScheduleGroup(scheduleGroupId, rootFolder);

        if (!MediaType.FILE_SHARE.equals(rootFolder.getMediaType())) {
            //We need to create the root docFolder as well
            SolrFolderEntity docFolder = docFolderService.createFolderIfNeeded(rootFolder.getId(), rootFolderDto.getPath());
            docFolderService.processParentsInfo(docFolder);
        }
        logger.debug("Created rootFolder {}", rootFolder);
        return DocStoreService.convertRootFolderToDto(rootFolder, defaultDocStoreAsDto, false);
    }

    public void prepareRootFolderDtoForCreation(RootFolderDto rootFolderDto) {
        try {
            if (Strings.isNullOrEmpty(rootFolderDto.getRealPath())) {
                rootFolderDto.setRealPath(rootFolderDto.getPath());
            }
            if (!Strings.isNullOrEmpty(rootFolderDto.getRealPath())) {

                if (rootFolderDto.getMediaType() == null) {
                    throw new BadRequestException("mediaType", null,
                            messageHandler.getMessage("root-folder.media-type.empty"), BadRequestType.MISSING_FIELD);
                }

                rootFolderDto.setRealPath(rootFolderDto.getRealPath().trim());
                rootFolderDto.setPath(rootFolderDto.getRealPath());
                rootFolderDto.setPath(getNormalizePathFromRealPath(rootFolderDto.getRealPath(), rootFolderDto.getMediaType()));
                rootFolderDto.setRealPath(getNormalizedRealPath(rootFolderDto.getRealPath(), rootFolderDto.getMediaType()));
            }
            if (!Strings.isNullOrEmpty(rootFolderDto.getRealPath())) {
                if (rootFolderDto.getRealPath().endsWith("/") || rootFolderDto.getRealPath().endsWith("\\")) {
                    String realPath = rootFolderDto.getRealPath();
                    rootFolderDto.setRealPath(realPath.substring(0, realPath.length() - 1));
                }
            }
        } catch (InvalidPathException e) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.path.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private String getNormalizedRealPath(String realPath, MediaType mediaType) {
        switch (mediaType) {
            case FILE_SHARE:
                return DocFolderUtils.convertToAbsolute(mediaType, realPath);
            case BOX:
                return getNormalizePathFromRealPath(realPath, mediaType);
            default:
                return realPath;
        }
    }

    private String getNormalizePathFromRealPath(String realPath, MediaType mediaType) {
        if (MediaType.FILE_SHARE.equals(mediaType)) {
            if (!(realPath.startsWith("/") || realPath.startsWith(".") || realPath.startsWith("\\") || realPath.contains(":"))) {
                logger.info("User tried to create a root folder {}. Converting to absolute", realPath);
                realPath = MediaType.FILE_SHARE.getPathSeparator() + realPath;
            }
            String normalizePath = FileNamingUtils.normalizeFileSharePath(realPath);
            return FileNamingUtils.convertPathToUniversalString(normalizePath);
        } else {
//            http://ec2-54-200-41-63.us-west-2.compute.amazonaws.com/sites/test/small library
            return FileNamingUtils.convertPathToUniversalString(realPath);
        }
    }

    private boolean hasRfActiveScan(RootFolder rootFolder) {
        if (rootFolder.getLastRunId() != null) {
            CrawlRunDetailsDto runDetails = fileCrawlerExecutionDetailsService.getRunDetails(rootFolder.getLastRunId());
            return runDetails.getRunOutcomeState().isActive();
        }
        return false;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {"MngScanConfig"})
    public boolean deleteRootFolder(long id) {
        RootFolder rootFolder = docStoreService.getRootFolder(id);
        if (rootFolder.getLastRunId() != null) {
            CrawlRunDetailsDto runDetails = fileCrawlerExecutionDetailsService.getRunDetails(rootFolder.getLastRunId());
            if (!allowScannedRootFolderDelete && (runDetails.getTotalProcessedFiles() > 0 || rootFolder.getFoldersCount() > 1 // root folder is also a folder
                    || RunStatus.isProcessing(runDetails.getRunOutcomeState()))) {
                return false;
            }
            if (runDetails.getRunOutcomeState().isActive()) {
                return false;
            }
        }
        logger.warn("Deleting root folder {} with id {}", rootFolder.getPath(), rootFolder.getId());
        ingestErrorService.deleteErrorsByRootFolder(rootFolder);
        if (allowScannedRootFolderDelete) {
            controllerUtils.runInOtherThreadAsAdmin(() -> {
                deleteClaFilesByRootFolder(rootFolder);
                docFolderService.deleteDocFoldersByRootFolder(rootFolder);
            });
        }
        scheduleGroupService.detachRootFolderFromAllScheduleGroups(rootFolder.getId());
        docStoreService.deleteRootFolder(rootFolder);
        logger.info("RootFolder deleted");
        return true;
    }

    private void deleteClaFilesByRootFolder(RootFolder rootFolder) {
        logger.info("Deleting all files that belong to the rootFolder {}", rootFolder);
        fileCatService.deleteByRootFolderId(rootFolder.getId());
    }

    @Transactional(readOnly = true)
    public RootFolderDto getRootFolder(long id) {
        RootFolder rootFolder = docStoreService.getRootFolder(id);
        return rootFolder == null ? null : DocStoreService.convertRootFolderToDto(
                rootFolder, rootFolder.getDocStore().getId(), true);
    }

    private void isValidForRootFolderCreation(RootFolderDto rootFolderDto) {
        isValidForRootFolderCreation(rootFolderDto, null);
    }

    private void isValidForRootFolderCreation(RootFolderDto rootFolderDto, Long currentId) {
        if (rootFolderDto.getMediaType() == null) {
            throw new BadRequestException("mediaType", null,
                    messageHandler.getMessage("root-folder.media-type.empty"), BadRequestType.MISSING_FIELD);
        }

        if (rootFolderDto.getMediaType().equals(MediaType.MIP)) {
            throw new BadRequestException("mediaType", null,
                    messageHandler.getMessage("root-folder.media-type.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        String path = rootFolderDto.getPath();
        String realPath = rootFolderDto.getRealPath();

        if (rootFolderDto.getMediaType().hasPath() &&
                (Strings.isNullOrEmpty(realPath) || Strings.isNullOrEmpty(realPath.trim()))) {
            throw new BadRequestException("path", null, messageHandler.getMessage("root-folder.path.empty"), BadRequestType.MISSING_FIELD);
        }

        if (!Strings.isNullOrEmpty(path)) {
            RootFolder rootFolderByPath = docStoreService.getRootFolderByPath(path);
            if (rootFolderByPath != null && (currentId == null || !currentId.equals(rootFolderByPath.getId()))) {
                logger.info("Can't create root folder by path {} - already exists", rootFolderDto.getPath());
                throw new BadRequestException(messageHandler.getMessage("root-folder.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }

        if (!Strings.isNullOrEmpty(realPath)) {
            List<RootFolder> rfs = docStoreService.findByPathPrefix(realPath, rootFolderDto.getMediaType());
            if (rfs != null && rfs.size() > 0 && (rfs.size() > 1 || currentId == null || !currentId.equals(rfs.get(0).getId()))) {
                throw new BadRequestException("path", realPath,
                        messageHandler.getMessage("root-folder.path.descendant-exists"), BadRequestType.UNSUPPORTED_VALUE);
            }

            List<String> paths = realPath.startsWith("http") ? getPossibleParentFoldersFileStorageService(realPath) : getPossibleParentFoldersFileShare(realPath);
            rfs = !paths.isEmpty() ? docStoreService.findByPaths(paths) : null;
            if (rfs != null && rfs.size() > 0 && (rfs.size() > 1 || currentId == null || !currentId.equals(rfs.get(0).getId()))) {
                throw new BadRequestException("path", realPath,
                        messageHandler.getMessage("root-folder.path.parent-exists"), BadRequestType.UNSUPPORTED_VALUE);
            }
            if (rootFolderDto.getMediaConnectionDetailsId() != null && !isRFDescendantOfConnectionPath(rootFolderDto)) {
                throw new BadRequestException("path", null,
                        messageHandler.getMessage("root-folder.path.media-conn-descendant"), BadRequestType.OTHER);
            }
        }

        if (!rootFolderDto.getMediaType().hasPath() &&
                (Strings.isNullOrEmpty(rootFolderDto.getMailboxGroup()) || Strings.isNullOrEmpty(rootFolderDto.getMailboxGroup().trim()))) {
            throw new BadRequestException("mailbox group", null,
                    messageHandler.getMessage("root-folder.mailbox-group.empty"), BadRequestType.MISSING_FIELD);
        }

        if (!rootFolderDto.getMediaType().hasPath()) {
            if (FileNamingUtils.containsIllegalCharsForPath(rootFolderDto.getMailboxGroup())) {
                throw new BadRequestException("mailbox group", rootFolderDto.getMailboxGroup(),
                        messageHandler.getMessage("root-folder.mailbox-group.invalid"), BadRequestType.UNSUPPORTED_VALUE);
            }

            List<RootFolder> rfs = docStoreService.findByMailboxGroup(rootFolderDto.getMailboxGroup());
            if (rfs != null && rfs.size() > 0 && (rfs.size() > 1 || currentId == null || !currentId.equals(rfs.get(0).getId()))) {
                throw new BadRequestException("mailbox group", rootFolderDto.getMailboxGroup(),
                        messageHandler.getMessage("root-folder.mailbox-group.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    private void validateScheduledGroup(Long scheduleGroupId) {
        if (scheduleGroupId == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.scheduled-group.empty"), BadRequestType.MISSING_FIELD);
        } else if (scheduleGroupService.getGroupById(scheduleGroupId) == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
    }

    private boolean isRFDescendantOfConnectionPath(RootFolderDto rootFolderDto) {
        MediaConnectionDetailsDto connectionDetails =
                mediaConnectionDetailsService.getMediaConnectionDetailsById(rootFolderDto.getMediaConnectionDetailsId());
        String pathSeparator = rootFolderDto.getMediaType().getPathSeparator();
        String connPath = Optional.ofNullable(connectionDetails.getUrl())
                .map(url -> addPathSeparatorIfNeeded(url, pathSeparator))
                .orElse("")
                .toLowerCase();

        String rfPath = rootFolderDto.getRealPath().toLowerCase();
        rfPath = addPathSeparatorIfNeeded(rfPath, pathSeparator);

        return rfPath.startsWith(connPath);
    }

    @NotNull
    private String addPathSeparatorIfNeeded(String path, String pathSeparator) {
        return (path.endsWith(pathSeparator) ? path : path + pathSeparator);
    }

    private List<String> getPossibleParentFoldersFileShare(String path) {
        List<String> paths = new ArrayList<>();
        try {
            Path p = Paths.get(path);
            while (p.getParent() != null) {
                paths.add(p.getParent().normalize().toString());
                p = p.getParent();
            }
        } catch (Exception e) {
            logger.info("failed getting parent paths for new root folder {} {}", path, e.getMessage());
        }
        return paths;
    }

    private static List<String> getPossibleParentFoldersFileStorageService(String url) {
        List<String> paths = new ArrayList<>();
        int pathSlash = url.indexOf("/", url.indexOf("://") + 3);
        if (pathSlash < 0) {
            return paths;
        }

        pathSlash++;
        String prefix = url.substring(0, pathSlash);
        String path = url.substring(pathSlash);
        String[] splitPath = path.split("/");
        StringJoiner strJoin = new StringJoiner("/", prefix, "");
        for (String subFolder : splitPath) {
            strJoin.add(subFolder);
            paths.add(strJoin.toString());
        }
        return paths;
    }

    @Transactional
    public RootFolderDto updateRootFolder(long id, RootFolderDto rootFolderDto) {

        prepareRootFolderDtoForCreation(rootFolderDto);
        isValidForRootFolderCreation(rootFolderDto, id);
        validateFileTypes(rootFolderDto.getFileTypes(), rootFolderDto.getIngestFileTypes());
        validateDepartment(rootFolderDto.getDepartmentId());

        RootFolder rootFolder = docStoreService.getRootFolder(id);

        RunStatus lastRunState = null;
        boolean rfHasFilesOrFolders = false;
        if (rootFolder.getLastRunId() != null) {
            CrawlRunDetailsDto runDetails = fileCrawlerExecutionDetailsService.getRunDetails(rootFolder.getLastRunId());
            rfHasFilesOrFolders = (runDetails.getTotalProcessedFiles() > 0 || rootFolder.getFoldersCount() > 1);
            lastRunState = runDetails.getRunOutcomeState();
        }

        //TODO: this code is an abomination and anti-pattern. someone should fix it when possible
        if (rootFolderDto.getPath() != null) {
            String path = rootFolderDto.getPath();
            String realPath = rootFolderDto.getRealPath();
            if (!path.equalsIgnoreCase(rootFolder.getPath()) || !realPath.equalsIgnoreCase(rootFolder.getRealPath()) ||
                    (rootFolderDto.getMediaType() != null && !rootFolderDto.getMediaType().equals(rootFolder.getMediaType()))) {
                if (rootFolder.getLastRunId() != null) {
                    if (rfHasFilesOrFolders // root folder is also a folder
                            || (lastRunState != null && RunStatus.isProcessing(lastRunState))) {
                        if (!path.equalsIgnoreCase(rootFolder.getPath()) || !realPath.equalsIgnoreCase(rootFolder.getRealPath())) {
                            throw new BadRequestException(messageHandler.getMessage("root-folder.path.change"), BadRequestType.UNSUPPORTED_VALUE);
                        } else {
                            throw new BadRequestException(messageHandler.getMessage("root-folder.media-type.change"), BadRequestType.UNSUPPORTED_VALUE);
                        }
                    }
                }
            }
        }

        rootFolder = docStoreService.updateRootFolder(id, rootFolderDto, rfHasFilesOrFolders);
        return DocStoreService.convertRootFolderToDto(rootFolder, null, false);
    }

    public void validateFileTypes(FileType[] mapFileTypes, FileType[] ingestFileTypes) {
        if (ingestFileTypes != null && ingestFileTypes.length > 0
                && (mapFileTypes == null || mapFileTypes.length == 0)) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.file-type.ingest"), BadRequestType.UNSUPPORTED_VALUE);
        } else if (ingestFileTypes == null || ingestFileTypes.length == 0) {
            return;
        }

        List<FileType> map = Arrays.asList(mapFileTypes);
        List<FileType> ingest = Arrays.asList(ingestFileTypes);

        ingest.forEach(type -> {
            if (!map.contains(type)) {
                throw new BadRequestException(messageHandler.getMessage("root-folder.file-type.ingest"), BadRequestType.UNSUPPORTED_VALUE);
            }
        });
    }

    @Transactional
    public RootFolderDto updateRootFolderActive(long rootFolderId, Boolean state) {
        return docStoreService.updateRootFolderActive(rootFolderId, state);
    }

    @Transactional
    public RootFolderDto updateRootFolderReingest(long rootFolderId, Boolean reingest) {
        return docStoreService.updateRootFolderReingest(rootFolderId, reingest);
    }

    private void validateDepartment(Long departmentId) {
        if (departmentId != null) {
            DepartmentDto dto = departmentService.getByIdCached(departmentId);
            if (dto == null || dto.isDeleted()) {
                throw new BadParameterException(messageHandler.getMessage("department.missing"), BadRequestType.ITEM_NOT_FOUND);
            }
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {"MngScanConfig"})
    public synchronized void createDeleteRootFolderOperation(RootFolderDto rootFolderDto) {
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderDto.getId());
        if (rootFolder == null) {
            throw new BadParameterException(messageHandler.getMessage("root-folder.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (hasRfActiveScan(rootFolder)) {
            throw new BadParameterException(messageHandler.getMessage("root-folder.delete.active-run"), BadRequestType.OPERATION_FAILED);
        }
        markRootFolderAsDeleted(rootFolder.getId());
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.DELETE_ROOT_FOLDER);
        data.setRetryLeft(retryForRootFolderDelete);
        data.setOpData(new HashMap<>());
        data.getOpData().put(DeleteRootFolderOperation.RF_ID_PARAM, String.valueOf(rootFolder.getId()));
        data.setUserOperation(true);
        data.setDescription(messageHandler.getMessage("operation-description.delete-root-folder",
                Lists.newArrayList(rootFolderDto.getIdentifier())));
        scheduledOperationService.createNewOperation(data);
    }

    private void markRootFolderAsDeleted(Long rootFolderId) {
        docStoreService.markRootFolderAsDeleted(rootFolderId);
    }

    @AutoAuthenticate
    public void deleteRootFolder(Long id) {
        RootFolder rootFolder = docStoreService.getRootFolderAllState(id);

        if (rootFolder == null) {
            return;
        }

        String progressId = progressService.createNewProgress(ProgressType.DELETE_ROOT_FOLDER, "deleting scan errors for root folder", 0, null);
        ProgressService.Handler progress = progressService.createHandler(progressId);

        logger.debug("start delete operation for root folder {}", id);

        // handle errors
        ingestErrorService.deleteErrorsByRootFolder(rootFolder);
        scanErrorsService.deleteErrorsByRootFolder(rootFolder.getId());

        progress.inc(10, "deleting files");

        // handle files and contents
        int from = 0;
        List<Triple<Long, Long, String>> fileData;
        do {
            logger.debug("cycle content handing delete operation for root folder {}", id);
            fileData = fileCatService.getFilesUnderRootFolder(id, from, MAX_RECORDS_TO_PROCESS);
            if (fileData != null && !fileData.isEmpty()) {
                logger.debug("delete files for root folder {} cycle from {}", id, from);
                List<Long> fileIds = fileData.stream().map(Triple::getLeft).collect(Collectors.toList());
                List<Long> contentIds = fileData.stream().map(Triple::getMiddle).collect(Collectors.toList());
                Set<String> groupsToFix = fileData.stream().map(Triple::getRight).filter(Objects::nonNull).collect(Collectors.toSet());
                try {
                    logger.debug("Before handling content for deleted files trying to acquire population dirty lock " +
                            "for root folder {}", id);
                    contentMetadataService.acquirePopulationDirtyReadLock();
                    logger.debug("Successfully acquired population dirty lock for root folder {}", id);
                    filesDataPersistenceService.handleContentsForDeletedFiles(fileIds, contentIds);
                } finally {
                    logger.debug("Trying to release population dirty lock for root folder {}", id);
                    contentMetadataService.releasePopulationDirtyReadLock();
                    logger.debug("Successfully released population dirty lock for root folder {}", id);
                }
                if (!groupsToFix.isEmpty()) {
                    fileGroupService.markGroupAsDirtyByIds(groupsToFix);
                }
                from += fileData.size();
            }
        } while (fileData != null && fileData.size() == MAX_RECORDS_TO_PROCESS);

        logger.debug("delete files in delete operation for root folder {}", id);
        fileCatService.deleteByRootFolderId(id);
        fileCatService.commitToSolr();

        progress.inc(20, "deleting folders");

        // handle folders
        logger.debug("delete folders in delete operation for root folder {}", id);
        docFolderService.deleteByRootFolderId(id);
        docFolderService.commit();

        // fix dirty content
        progress.inc(20, "updating contents");
        logger.debug("fix dirty contents for root folder {}", id);
        try {
            logger.debug("Before updating content metadata stats trying to acquire population dirty lock " +
                    "for root folder {}", id);
            contentMetadataService.acquirePopulationDirtyWriteLock();
            logger.debug("Successfully acquired population dirty lock for root folder {}", id);
            filesDataPersistenceService.updateContentMetadataStats(null, new DummyProgressTracker());
        } finally {
            logger.debug("Trying to release population dirty lock for root folder {}", id);
            contentMetadataService.releasePopulationDirtyWriteLock();
            logger.debug("Successfully released population dirty lock for root folder {}", id);
        }

        // fix groups
        progress.inc(20, "updating groups");
        if (fileGroupService.countDirty() > 0) {
            long start = System.currentTimeMillis();
            analyzeFinalizeService.updateNumOfFilesInGroups(null, new DummyProgressTracker());
            analyzeFinalizeService.clearGroupsDirty(null, true, start);
        }

        if (!rootFolder.getMediaType().hasPath()) {
            logger.debug("delete mailboxes for root folder {}", id);
            List<MailboxDto> mailboxes = mailboxService.getByRootFolderId(id);
            if (mailboxes != null && !mailboxes.isEmpty()) {
                mailboxes.forEach(mailboxDto -> mailboxService.deleteMailbox(mailboxDto.getId()));
            }
        }

        progress.inc(20, "detach root folder from scheduled group");

        logger.debug("detach root folder {}", id);
        scheduleGroupService.detachRootFolderFromAllScheduleGroups(id);

        progress.inc(5, "delete root folder");

        logger.debug("delete root folder {}", id);
        docStoreService.deleteRootFolder(rootFolder);

        progress.inc(5, "done");
    }

    public void acquireReadLock(Long rootFolderId) {
        ReadWriteLock lock = rootFolderMapLock.getOrDefault(rootFolderId, new ReentrantReadWriteLock());
        rootFolderMapLock.put(rootFolderId, lock);
        lock.readLock().lock();
    }

    public void releaseReadLock(Long rootFolderId) {
        ReadWriteLock lock = rootFolderMapLock.get(rootFolderId);
        if (lock != null) {
            try {
                lock.readLock().unlock();
            } catch (Exception e) {
                logger.warn("problem releasing read lock for root folder {}", rootFolderId, e);
            }
        }
    }

    public void acquireWriteLock(Long rootFolderId) {
        ReadWriteLock lock = rootFolderMapLock.getOrDefault(rootFolderId, new ReentrantReadWriteLock());
        rootFolderMapLock.put(rootFolderId, lock);
        lock.writeLock().lock();
    }

    public void releaseWriteLock(Long rootFolderId) {
        ReadWriteLock lock = rootFolderMapLock.get(rootFolderId);
        if (lock != null) {
            try {
                lock.writeLock().unlock();
            } catch (Exception e) {
                logger.warn("problem releasing write lock for root folder {}", rootFolderId, e);
            }
        }
    }

    @Transactional
    public boolean equalsCurrentRootFolder(RootFolderDto updateCandidateRootFolder) {
        if (updateCandidateRootFolder == null) {
            return false;
        }
        RootFolder rootFolder = docStoreService.getRootFolder(updateCandidateRootFolder.getId());
        if (rootFolder == null) {
            return false;
        }
        RootFolderDto currentRootFolder = DocStoreService.convertRootFolderToDto(rootFolder, null, false);
        return updateCandidateRootFolder.equals(currentRootFolder);
    }

    @Transactional
    public FolderExcludeRuleDto addGlobalFolderExcludeRule(FolderExcludeRuleDto folderExcludeRuleDto) {
        FolderExcludeRule folderExcludeRule = docStoreService.addGlobalFolderExcludeRule(folderExcludeRuleDto);
        return DocStoreService.convertFolderExcludeRule(folderExcludeRule);
    }

    @Transactional
    public FolderExcludeRuleDto addRootFolderExcludeRule(long rootFolderId, FolderExcludeRuleDto folderExcludeRuleDto) {
        if (folderExcludeRuleDto == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.folder-exclude.empty"), BadRequestType.MISSING_FIELD);
        }
        logger.info("Add rootFolder exclude rule {} to rootFolder ID {}", folderExcludeRuleDto, rootFolderId);
        FolderExcludeRule folderExcludeRule = docStoreService.addFolderExcludeRule(rootFolderId, folderExcludeRuleDto);
        return DocStoreService.convertFolderExcludeRule(folderExcludeRule);
    }

    public Page<FolderExcludeRuleDto> listFolderExcludeRules(DataSourceRequest dataSourceRequest, Long rootFolderId) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<FolderExcludeRule> result;
        if (rootFolderId != null) {
            result = docStoreService.listFolderExcludeRulesForRootFolder(pageRequest, rootFolderId);
        } else {
            result = docStoreService.listFolderExcludeRules(pageRequest);
        }
        return DocStoreService.convertFolderExcludeRules(result, pageRequest);
    }

    @Transactional
    public void deleteFolderExcludeRule(long folderExcludeRuleId) {
        docStoreService.deleteFolderExcludeRule(folderExcludeRuleId);
    }

    @Transactional
    public FolderExcludeRuleDto updateFolderExcludeRule(long id, FolderExcludeRuleDto folderExcludeRuleDto) {
        FolderExcludeRule folderExcludeRule = docStoreService.updateFolderExcludeRule(
                id, folderExcludeRuleDto.isDisabled(), folderExcludeRuleDto.getOperator(), folderExcludeRuleDto.getMatchString());
        return DocStoreService.convertFolderExcludeRule(folderExcludeRule);
    }

    @Transactional(readOnly = true)
    public RootFolderDto findRootFolder(String path, boolean withExcludedFolders) {
        Long defaultDocStore = docStoreService.getDefaultDocStoreId();
        String rootPath = FileNamingUtils.convertPathToUniversalString(path);
        RootFolder rootFolderByPath = docStoreService.getRootFolderByPath(rootPath);
        if (rootFolderByPath == null) {
            logger.info("Couldn't find root folder by path {}", rootPath);
            return null;
        }
        return DocStoreService.convertRootFolderToDto(rootFolderByPath, defaultDocStore, withExcludedFolders);
    }

    @Transactional(readOnly = true)
    public RootFolderDto findRootFolderByMailbox(String mailboxGroup, boolean withExcludedFolders) {
        Long defaultDocStore = docStoreService.getDefaultDocStoreId();

        List<RootFolder> rootFolderByMailbox = docStoreService.findByMailboxGroup(mailboxGroup);
        if (rootFolderByMailbox == null || rootFolderByMailbox.isEmpty()) {
            logger.info("Couldn't find root folder by mailbox group {}", mailboxGroup);
            return null;
        }
        return DocStoreService.convertRootFolderToDto(rootFolderByMailbox.get(0), defaultDocStore, withExcludedFolders);
    }
}