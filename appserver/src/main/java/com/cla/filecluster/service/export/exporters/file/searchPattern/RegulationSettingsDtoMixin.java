package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: ophir
 * Created on: 4/18/2019
 */
public abstract class RegulationSettingsDtoMixin {

    @JsonProperty(TextSearchPatternHeaderFields.NAME)
    private String name;
}
