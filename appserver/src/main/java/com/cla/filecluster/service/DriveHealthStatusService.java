package com.cla.filecluster.service;

import com.cla.common.domain.dto.components.ClaComponentType;
import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.filecluster.domain.entity.monitor.ClaComponent;
import com.cla.filecluster.repository.jpa.monitor.ClaComponentRepository;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.collections.CollectionUtils.isEmpty;


@Service
public class DriveHealthStatusService {
	private static final Logger logger = LoggerFactory.getLogger(DriveHealthStatusService.class);

	@Autowired
	private ClaComponentRepository componentRepository;
	@Autowired
	private SysComponentService sysComponentService;

	private String localhostIp = "";
	private ImmutableList<ClaComponent> allComponents = ImmutableList.of();
	private ClaComponent appServerComp = null;

	private ObjectMapper objectMapper = new ObjectMapper()
											.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
											.setSerializationInclusion(JsonInclude.Include.NON_NULL);

	@PostConstruct
	private void init(){
		try {
			localhostIp = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			logger.warn("Could not retrieve a local host IP. Error:" + e.getMessage());
		}
	}

	private ImmutableList<ClaComponent> getAllComponents() {
		if (isEmpty(allComponents)) {
			refreshComponentsList();
		}
		return allComponents;
	}

	/**
	 * This method logging and analyzing the drive metrics. Based on the analysis,
	 * warning messages are a logged and component state can be set to FAULT.
	 * NOTE: this method is being run in ASYNC mode, because no response is expected. It should be called by queue listener instead of REST controller.
	 * @param driveDto
	 */
	@Async
	public void handleDriveHealth(DriveDto driveDto){
		logger.trace("Measured host drive capacity metrics: " + driveDto);
		String machineId = driveDto.getMachineId();
		if (machineId == null) {
			return;
		}
		Map<String, Integer> bigFolders = sysComponentService.calculateOversizedFolders(driveDto.getFolderSizes());
		bigFolders.entrySet().stream().forEach(e -> logger.warn("Folder threshold has been passed! Folder {} at {} size is {} MB", e.getKey(), machineId, e.getValue()));
		Map<DriveDto.DriveCapacityMetrics, Integer> lowSpaceDrives = sysComponentService.calculateLowSpaceDrives(driveDto.getDriveCapacities());
		lowSpaceDrives.forEach((k,v) -> logger.warn("Free disk space threshold has been passed! Drive {} at {} space usage is {}%", k, machineId, v));
		try {
			String driveData = objectMapper.writeValueAsString(driveDto);
			List<ClaComponent> components = getAllComponents().stream()
					.filter(comp ->
								(comp.getExternalNetAddress() != null && comp.getExternalNetAddress().contains(machineId)) ||
								(comp.getLocalNetAddress() != null && comp.getLocalNetAddress().contains(machineId)) ||
								// local report - match FC or Solr with 'localhost' in its address
								(machineId.equals(localhostIp) && (
										((ClaComponentType.APPSERVER.equals(comp.getClaComponentType()) || ClaComponentType.SOLR_NODE.equals(comp.getClaComponentType())) &&
										 comp.getExternalNetAddress().toLowerCase().contains("localhost"))
										)))
					.collect(Collectors.toList());
			components.forEach(comp -> sysComponentService.updateDriveDataAndState(comp.getId(),null, driveData));
			if (logger.isTraceEnabled()) {
				logger.trace("Update drive health for components: {}",
						components.stream().map(c->c.getComponentType()+"-"+c.getLocalNetAddress()).collect(Collectors.joining(",")));
			}
			if (components.isEmpty()) {
				logger.warn("Cant find component to update drive data, machine id {}", machineId);
			}
		} catch (Exception e) {
			logger.error("Couldn`t update the component status based on Drive Health report " + driveDto.toString(), e );
		}
	}

	@Scheduled(initialDelayString = "30000", fixedRateString = "1800000")
	@ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
	private synchronized void refreshComponentsList(){
		this.allComponents = ImmutableList.copyOf(componentRepository.findAll());
		this.appServerComp = allComponents.stream().filter(c->ClaComponentType.APPSERVER.equals(c.getComponentType())).findAny().orElse(null);
	}
}
