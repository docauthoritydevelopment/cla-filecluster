package com.cla.filecluster.service.extractor;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagAssociationDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.pv.RefinementRuleDto;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.CollectionsUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.SolrFileGroupBuilder;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.*;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.ClaFileByFolderRecursiveSpecification;
import com.cla.filecluster.repository.solr.FacetFunction;
import com.cla.filecluster.repository.solr.FacetType;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.repository.solr.query.*;
import com.cla.filecluster.service.TagRelatedBaseService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.ClaFileGroupOperations;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.RefinementRuleService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FieldStatsInfo;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.util.SimpleOrderedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import static com.cla.common.constants.FileGroupsFieldType.*;

@Service
public class GroupsService extends TagRelatedBaseService {
    private static final Logger logger = LoggerFactory.getLogger(GroupsService.class);

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private RefinementRuleService refinementRuleService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private MessageHandler messageHandler;

    //todo: circular dependency, should resolve in a proper way
    private ClaFileGroupOperations claFileGroupOperations;
    private ContentMetadataService contentMetadataService;

    @Value("${group-service.debug.log-missing-results:true}")
    private boolean logMissingResults;

    @Value("${group-service.joined-groups.delete-zero-num-of-files:false}")
    private boolean deleteUserGroupZeroNumOfFiles;

    @Value("${reports.groups.labels.size:50,500,1000}")
    private String[] groupSizeLabels;

    @Value("${is.toolExecutionMode:false}")
    private boolean isToolExecutionMode;

    // an in-memory cache of all raw analysis groups
    private Set<String> analysisGroupsWithSubGroups = CollectionsUtils.newConcurrentHashSet();

    // an in-memory cache of raw analysis group for a sub group
    private Map<String, String> subGroupToRawAnalysisGroup = new ConcurrentHashMap<>();

    private Lock groupModificationLock = new ReentrantLock();

    private long acquisitionTime = 0;

    private String[] labels;

    private int[] groupSizeRanges;

    private boolean isInitSubGroups = false;

    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    public void aquireGroupModificationLock() {
        groupModificationLock.lock();
        if (logger.isDebugEnabled()) {
            logger.debug("GroupModificationLock aquired");
            acquisitionTime = System.currentTimeMillis();
        }
    }

    public void releaseGroupModificationLock() {
        groupModificationLock.unlock();
        if (logger.isDebugEnabled()) {
            logger.debug("GroupModificationLock released ({} minutes)", ((System.currentTimeMillis() - acquisitionTime)/6000L)/10f );
        }
    }

    private void initSubGroups() {
        while(!isInitSubGroups) {
            try {
                List<FileGroup> raw = findFileGroupsWithRefinementRules();
                if (raw != null && !raw.isEmpty()) {
                    analysisGroupsWithSubGroups.addAll(raw.stream().collect(Collectors.toConcurrentMap(FileGroup::getId, FileGroup::getId)).keySet());
                }

                List<FileGroup> fg = findFileGroupsByType(GroupType.SUB_GROUP);
                if (fg != null && !fg.isEmpty()) {
                    subGroupToRawAnalysisGroup.putAll(fg.stream().collect(Collectors.toConcurrentMap(FileGroup::getId, FileGroup::getRawAnalysisGroupId)));
                }
                isInitSubGroups = true;
            } catch (Exception e) {
                logger.error("failed init sub groups from solr", e);
                try {Thread.sleep(10000L);} catch (Exception er) {}
            }
        }
    }

    @EventListener
    @AutoAuthenticate
    public void handleContextRefresh(ContextRefreshedEvent event) {
        contentMetadataService = applicationContext.getBean(ContentMetadataService.class);
        claFileGroupOperations = applicationContext.getBean(ClaFileGroupOperations.class);
    }

    @PostConstruct
    private void init() {
        if (isToolExecutionMode) {
            logger.info("GroupsService: no initSubGroups() execution");
        } else {
            executorService.submit(() -> initSubGroups());
        }
        groupSizeRanges = new int[groupSizeLabels.length];
        for (int i = 0; i < groupSizeLabels.length; ++i) {
            groupSizeRanges[i] = Integer.parseInt(groupSizeLabels[i]);
        }

        labels = new String[groupSizeLabels.length];
        for (int i = 1; i < groupSizeLabels.length; ++i) {
            String from = groupSizeLabels[i - 1];
            String to = groupSizeLabels[i];
            labels[i - 1] = from + "-" + to;
        }
        labels[groupSizeLabels.length - 1] = groupSizeLabels[groupSizeLabels.length - 1] + "+";
    }

    public String[] getGroupSizeLabels() {
        return labels;
    }

    @Transactional
    public GroupDto addFunctionalRoleToGroup(String groupId, FunctionalRole functionalRole) {
        return addAssociableEntityToGroup(groupId, functionalRole, null, AssociableEntityType.DATA_ROLE);
    }

    @Transactional
    public GroupDto addSimpleBizListItemToGroup(String groupId, SimpleBizListItem simpleBizListItem) {
        return addAssociableEntityToGroup(groupId, simpleBizListItem, null, AssociableEntityType.BUSINESS_LIST_ITEM);
    }

    public GroupDto addAssociableEntityToGroup(String groupId, AssociableEntity associableEntity, BasicScope basicScope, AssociableEntityType associationType) {
        FileGroup fileGroup = fileGroupRepository.getById(groupId).orElse(null);
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(
                fileGroup.getId(), fileGroup.getAssociations(), AssociationType.GROUP);
        if (associations == null) {
            associations = new ArrayList<>();
        }
        //Check if fileTag is already in
        Long scopeId = basicScope == null ? null : basicScope.getId();
        AssociationEntity groupAssociation = associationsService.findGroupAssociation(associations, associableEntity, scopeId);

        if (groupAssociation != null) {
            logger.debug("Group is already associated with tag");
            return null;
        }

        AssociationEntity newAssoc = associationsService.createGroupAssociation(associableEntity, groupId, scopeId, associationType);
        associations.add(newAssoc);
        List<AssociationEntity> docTypes = AssociationsService.getDocTypeAssociation(associations);

        return convertGroupToDto(fileGroup, null, null, true, docTypes, associations);
    }

    public FileGroup copyGroupAssociations(String sourceGroupId, String targetGroupId) {
        List<FileGroup> groups = fileGroupRepository.getByIds(Lists.newArrayList(sourceGroupId, targetGroupId));
        FileGroup sourceGroup = groups.get(0).getId().equals(sourceGroupId) ? groups.get(0) : groups.get(1);
        FileGroup targetGroup = groups.get(0).getId().equals(targetGroupId) ? groups.get(0) : groups.get(1);
        logger.debug("Copy group associations from {} to {}", sourceGroup, targetGroup);

        List<AssociationEntity> associations = copyGroupAssociations(sourceGroup, targetGroup);

        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(targetGroupId);
        builder.setAssociations(targetGroup.getAssociations());
        fileGroupRepository.saveGroupChanges(builder);

        createApplyOperationsForNewGroupAssoc(targetGroupId, associations);

        return targetGroup;
    }

    private AssociationEntity findSimilarIfExists(AssociationRelatedData sourceGroupAssocData,
                                                  AssociationRelatedData targetGroupAssocData,
                                                  AssociationEntity toMatch) {
        if (targetGroupAssocData.getAssociations() == null ||
                targetGroupAssocData.getAssociations().isEmpty() || toMatch == null) {
            return null;
        }

        for (AssociationEntity assoc : targetGroupAssocData.getAssociations()) {
            if (toMatch.similarAssociation(assoc)) {
                return assoc;
            } else if (assoc.getFileTagId() != null && toMatch.getFileTagId() != null) { // chk type single val
                FileTagTypeDto target = ((FileTagDto)AssociationUtils.getEntityFromList(
                        FileTagDto.class, assoc.getFileTagId() ,targetGroupAssocData.getEntities())).getType();
                FileTagTypeDto source = ((FileTagDto)AssociationUtils.getEntityFromList(
                        FileTagDto.class, toMatch.getFileTagId() ,sourceGroupAssocData.getEntities())).getType();
                if (target.getId() == source.getId() && source.isSingleValueTag()) {
                    return assoc;
                }
            }
        }
        return null;
    }

    private void createApplyOperationsForNewGroupAssoc(String groupId, List<AssociationEntity> associations) {
        if (associations != null) {
            associations.forEach(assoc -> associationsService.createGroupApplyOperation(groupId, assoc, SolrFieldOp.ADD, null));
        }
    }

    private List<AssociationEntity> copyGroupAssociations(FileGroup sourceGroup, FileGroup targetGroup) {

        List<AssociationEntity> assocEntityToCopy = new ArrayList<>();
        List<String> associationsToCopy = new ArrayList<>();

        AssociationRelatedData sourceGroupAssocData = associationsService.getAssociationRelatedData(sourceGroup);

        AssociationRelatedData targetGroupAssocData = associationsService.getAssociationRelatedData(targetGroup);

        //------------------------ copy tag associations ---------------------------------------------------------------

        logger.debug("Copy {} group tag associations from {} to {}",
                sourceGroupAssocData.getAssociations().size(), sourceGroup.getId(), targetGroup.getId());

        for (AssociationEntity association : sourceGroupAssocData.getAssociations()) {

            AssociationEntity targetMatch = findSimilarIfExists(sourceGroupAssocData, targetGroupAssocData, association);

            if (targetMatch == null) {
                AssociationEntity toCopy = new AssociationEntity();
                association.copyAssociation(toCopy);
                toCopy.setCreationDate(System.currentTimeMillis());
                associationsToCopy.add(AssociationsService.convertFromAssociationEntity(toCopy));
                assocEntityToCopy.add(toCopy);
            }
        }

        if (!associationsToCopy.isEmpty()) {
            if (targetGroup.getAssociations() == null) {
                targetGroup.setAssociations(new ArrayList<>());
            }
            targetGroup.getAssociations().addAll(associationsToCopy);
        }
        return assocEntityToCopy;
    }

    /**
     * Marks the file tag association as "removed" Only commit of tags to solr will actually remove the association
     *
     * @param groupId group ID
     * @param fileTag file tag
     * @return true if the tag was removed. False if there was nothing to remove
     */
    @Transactional
    public GroupDto markFileTagAssociationAsDeleted(String groupId, FileTag fileTag, BasicScope basicScope) {
        return markGroupAssociationAsDeleted(groupId, fileTag, basicScope, AssociableEntityType.TAG);
    }

    @Transactional
    public GroupDto markSimpleBizListItemAssociationAsDeleted(String groupId, SimpleBizListItem simpleBizListItem) {
        return markGroupAssociationAsDeleted(groupId, simpleBizListItem, null, AssociableEntityType.BUSINESS_LIST_ITEM);
    }

    @Transactional
    public GroupDto markFunctionalRoleAssociationAsDeleted(String groupId, FunctionalRole functionalRole) {
        return markGroupAssociationAsDeleted(groupId, functionalRole, null, AssociableEntityType.DATA_ROLE);
    }

    public GroupDto markGroupAssociationAsDeleted(String groupId, AssociableEntity associableEntity, BasicScope basicScope, AssociableEntityType associationType) {
        FileGroup fileGroup = fileGroupRepository.getById(groupId).orElse(null);
        if (fileGroup == null || fileGroup.getAssociations() == null) {
            return null;
        }
        List<AssociationEntity> groupAssociations = AssociationsService.convertFromAssociationEntity(
                fileGroup.getId(), fileGroup.getAssociations(), AssociationType.GROUP);
        Long scopeId = basicScope == null ? null : basicScope.getId();
        AssociationEntity fileTagAssociation = associationsService.findGroupAssociation(groupAssociations,  associableEntity,  scopeId);

        if (fileTagAssociation == null) {
            logger.debug("No association to deleted at Group {}", groupId);
            return null;
        }

        logger.debug("Remove association {} from Group {}", associableEntity, groupId);

        associationsService.deleteGroupAssociation(groupId, fileTagAssociation, associationType);

        groupAssociations.remove(fileTagAssociation);
        List<AssociationEntity> groupDocTypeAssociations = AssociationsService.getDocTypeAssociation(groupAssociations);
        return convertGroupToDto(fileGroup, null, null, true, groupDocTypeAssociations, groupAssociations);
    }

    public GroupDto markDocTypeAssociationAsDeleted(String groupId, DocType docType, BasicScope basicScope) {
        FileGroup fileGroup = fileGroupRepository.getById(groupId).orElse(null);
        if (fileGroup == null || fileGroup.getAssociations() == null) {
            return null;
        }
        Long scopeId = basicScope == null ? null : basicScope.getId();
        List<AssociationEntity> groupAssociations = AssociationsService.convertFromAssociationEntity(
                fileGroup.getId(), fileGroup.getAssociations(), AssociationType.GROUP);
        AssociationEntity docTypeAssociation = associationsService.getDocTypeAssociation(groupAssociations, docType.getId(), scopeId);
        if (docTypeAssociation == null) {
            logger.debug("No docType {} to remove from Group {}", docType.getId(), groupId);
            return null;
        }

        logger.debug("Remove docType {} from Group {}", docType.getId(), groupId);

        associationsService.deleteGroupAssociation(groupId, docTypeAssociation, AssociableEntityType.DOC_TYPE);

        groupAssociations.remove(docTypeAssociation);
        List<AssociationEntity> groupDocTypeAssociations = AssociationsService.getDocTypeAssociation(groupAssociations);

        return convertGroupToDto(fileGroup, null, null, true,
                groupDocTypeAssociations, groupAssociations);
    }

    /**
     * select g.id, g.generated_name , count(distinct f.id)
     * from cla.cla_files f
     * left join cla.file_groups g on f.file_group_id = g.id
     * left join cla.doc_folder d  on f.doc_folder_id = d.id
     * where d.id = docFolder.id group by g.id;
     *
     * @param docFolder
     * @param dataSourceRequest
     * @return
     */
    @Transactional(readOnly = true)
    public Page<AggregationCountItemDTO<GroupDto>> listUserGroupsWithCountByFolder(DocFolder docFolder, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        return listUserGroupsWithCountByFolderRecursive(docFolder, pageRequest, dataSourceRequest.getFilter(), Collections.singletonList(docFolder.getId()));
    }


    public Map<String, Long> getFileNumForAnalysisGroups(List<String> groupIds) {
        return claFileGroupOperations.getFileNumForFieldByIds(CatFileFieldType.ANALYSIS_GROUP_ID, groupIds);
    }

    public Map<String, Long> getFileNumForUserGroups(Collection<String> groupIds) {
        return claFileGroupOperations.getFileNumForFieldByIds(CatFileFieldType.USER_GROUP_ID, groupIds);
    }

    public Map<String, Long> getFileNumForContent(Collection<Long> contentIds) {
        if (contentIds == null || contentIds.isEmpty()) return new HashMap<>();
        Collection<String> contentIdsStr = contentIds.stream().map(Object::toString).collect(Collectors.toList());
        return claFileGroupOperations.getFileNumForFieldByIds(CatFileFieldType.CONTENT_ID, contentIdsStr);
    }

    public Long getFileNumForAnalysisGroup(String groupId) {
        return claFileGroupOperations.getFileNumForGroup(CatFileFieldType.ANALYSIS_GROUP_ID, groupId);
    }

    @AutoAuthenticate
    public Long getFileNumForUserGroup(String groupId) {
        return claFileGroupOperations.getFileNumForGroup(CatFileFieldType.USER_GROUP_ID, groupId);
    }

    public Page<AggregationCountItemDTO<GroupDto>> listUserGroupsWithCountByFolderRecursive(DocFolder docFolder, PageRequest pageRequest, FilterDescriptor filter, List<Long> folderIds) {
        ClaFileByFolderRecursiveSpecification claFileByFolderRecursiveSpecification = new ClaFileByFolderRecursiveSpecification(filter, docFolder.getId(), docFolder.getPath(), docFolder.getRootFolderId());

        if (folderIds != null && !folderIds.isEmpty()) {
            claFileByFolderRecursiveSpecification.setFolderIds(folderIds);
        }

        Query query = claFileByFolderRecursiveSpecification.toQuery();
        query.addFacetField(CatFileFieldType.USER_GROUP_ID);
        query.setRows(0);
        query.setFacetMinCount(1);
        QueryResponse queryResponse = claFileGroupOperations.runQuery(query.build());
        Map<String, Long> result = new TreeMap<>();
        if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
            FacetField field = queryResponse.getFacetFields().get(0);
            List<FacetField.Count> values = field.getValues();
            for (FacetField.Count c : values) {
                String userGroupId = c.getName();
                long count = c.getCount();
                result.put(userGroupId, count);
            }
        }

        result = CollectionsUtils.getSubMap((TreeMap) result, (int) pageRequest.getOffset(), pageRequest.getPageSize());
        List<FileGroup> groups = findByIds(new ArrayList(result.keySet()));
        List<AggregationCountItemDTO<GroupDto>> content = new ArrayList<>();
        for (FileGroup group : groups) {
            GroupDto groupDto = new GroupDto(group.getId(), group.getName(), group.getNumOfFiles(), group.getFirstMemberId(), null, null,
                    group.getGeneratedName());
            content.add(new AggregationCountItemDTO<>(groupDto, result.get(group.getId())));
        }

        return new PageImpl(content);
    }

    public FileGroup findById(String groupId) {
        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public GroupDto findDtoById(String groupId) {
        FileGroup group = findByIdFull(groupId);
        GroupDto groupDto = null;
        if (group != null) {

            AssociationRelatedData groupsAssociations = associationsService.getAssociationRelatedData(group);
            Collection<AssociationEntity> groupTagAssociations = groupsAssociations.getAssociations();
            Collection<AssociationEntity> groupDocTypes = AssociationsService.getDocTypeAssociation(groupTagAssociations);

            groupDto = convertGroupToDto(group,null,null, true, groupDocTypes,
                    groupTagAssociations, groupsAssociations.getEntities(),null);
        }
        return groupDto;
    }

    public FileGroup findByIdFull(String groupId) {
        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public FileGroup findByIdAssociations(String groupId) {
        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public List<FileGroup> findByIds(Collection<String> ids) {
        if (ids == null || ids.size() == 0) {
            return Lists.newArrayList();
        }
        List<FileGroup> res = fileGroupRepository.getByIds(ids);
        // Handle potential duplication due to OneToMany fetch returned by Hibernate -
        // TODO: consider other solutions for this(e.g. Criteria.DISTINCT_ROOT_ENTITY, or let JPA return SET<>)
        return Lists.newArrayList(Sets.newHashSet(res));
    }

    private List<FileGroup> findFileGroupsWithRefinementRules() {
        return fileGroupRepository.getByQuery(
            Query.create().setRows(SolrFileGroupRepository.MAX_ROWS)
                    .addFilterWithCriteria(
                    Criteria.create(HAS_SUBGROUPS, SolrOperator.EQ, true)));
    }

    private List<FileGroup> findFileGroupsByType(GroupType type) {
        return fileGroupRepository.getByQuery(
                Query.create().setRows(SolrFileGroupRepository.MAX_ROWS)
                        .addFilterWithCriteria(
                        Criteria.create(TYPE, SolrOperator.EQ, type.toInt())));
    }

    public List<GroupDto> findGroupsWithRefinementRules() {
        List<FileGroup> groups = findFileGroupsWithRefinementRules();
        if (groups == null || groups.isEmpty()) {
            return new ArrayList<>();
        }

        AssociationRelatedData associationsData = associationsService.getAssociationRelatedDataGroups(groups);

        Set<GroupDto> groupSet = convertGroupsToDto(groups, null, associationsData.getPerGroupDocTypes(),
                associationsData.getPerGroupAssociations(), associationsData.getEntities());
        return Lists.newArrayList(groupSet);
    }

    public List<FileGroup> findSubGroupsForRefinedGroup(String groupId) {
        return fileGroupRepository.getByQuery(Query.create()
                .addFilterWithCriteria(Criteria.create(RAW_ANALYSIS_GROUP_ID, SolrOperator.EQ, groupId)));
    }


    public Map<Integer, Long> createGroupSizeCountHistogram() {
        Query query = Query.create().setRows(0)
                .addFilterWithCriteria(PARENT_GROUP_ID, SolrOperator.MISSING, null)
                .addFilterWithCriteria(NUM_OF_FILES, SolrOperator.GTE, groupSizeRanges[0])
                .addFacetQuery(Criteria.create(NUM_OF_FILES, SolrOperator.IN_RANGE, Range.create(groupSizeRanges[0], groupSizeRanges[1]).fromOp(SolrOperator.GTE).toOp(SolrOperator.LTE)))
                .addFacetQuery(Criteria.create(NUM_OF_FILES, SolrOperator.IN_RANGE, Range.create(groupSizeRanges[1], groupSizeRanges[2]).fromOp(SolrOperator.GT).toOp(SolrOperator.LTE)))
                .addFacetQuery(Criteria.create(NUM_OF_FILES, SolrOperator.GT, groupSizeRanges[2]))
                ;
        QueryResponse queryResponse = fileGroupRepository.runQuery(query.build());
        Map<Integer, Long> groupSizeHistogram = new LinkedHashMap<>();
        if (queryResponse.getFacetQuery() != null && !queryResponse.getFacetQuery().isEmpty()) {
            Integer identifier = 0;
            for (Map.Entry<String, Integer> entry : queryResponse.getFacetQuery().entrySet()) {
                groupSizeHistogram.put(identifier, entry.getValue() == null ? 0 : entry.getValue().longValue());
                identifier++;
            }
        }

        logger.info("Created group size histogram");
        return groupSizeHistogram;
    }


    public Map<Integer, Long> createGroupSizeFilesHistogram() {
        Map<Integer, Long> groupSizeHistogram = new LinkedHashMap<>();

        Query query = Query.create().setRows(0)
                .addFilterWithCriteria(PARENT_GROUP_ID, SolrOperator.MISSING, null)
                .addFilterWithCriteria(NUM_OF_FILES, SolrOperator.GTE, groupSizeRanges[0]);

        List<SolrFacetQueryJson> jsonFacet = new ArrayList<>();

        ImmutableMap.of(
                groupSizeRanges[0], groupSizeRanges[1],
                groupSizeRanges[1]+1, groupSizeRanges[2],
                groupSizeRanges[2]+1, "*")
                .forEach((from, to) -> {
                    SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(NUM_OF_FILES.getSolrName() + from);
                    innerFacet.setType(FacetType.QUERY);
                    innerFacet.setQueryValueString(Range.create(from, to).toString());
                    innerFacet.setFunc(FacetFunction.SUM);
                    innerFacet.setField(NUM_OF_FILES);
                    jsonFacet.add(innerFacet);
                });

        query.setFacetJsonObjectList(jsonFacet);
        QueryResponse queryResponse = fileGroupRepository.runQuery(query.build());
        SimpleOrderedMap facets = ((SimpleOrderedMap)queryResponse.getResponse().get("facets"));

        for (int i = 0; i < groupSizeRanges.length; ++i) {
            int from = groupSizeRanges[i];
            if (i > 0) {
                from++;
            }
            SimpleOrderedMap f1 = ((SimpleOrderedMap)facets.get(NUM_OF_FILES.getSolrName()+from));
            Double size = f1 == null ? null : (Double)f1.get("numOfFiles_sum");
            groupSizeHistogram.put(i, size == null ? 0 : size.longValue());
        }

        logger.info("Created group size files histogram");
        return groupSizeHistogram;
    }

    public SolrFileGroupEntity createNewAnalysisGroupObject(long contentId) {
        SolrFileGroupBuilder builder = new SolrFileGroupBuilder();
        builder.setFieldsForNewGroup(fileGroupRepository.getNextAvailableId(), GroupType.ANALYSIS_GROUP);
        builder.setFirstMemberContentId(contentId);
        SolrFileGroupEntity fileGroup = builder.build();
        logger.debug("Create New Analysis Group Object {} for content {}", fileGroup.getId(), contentId);
        return fileGroup;
    }

    @Transactional
    public FileGroup createNewAnalysisGroup(SolrFileEntity claFile, String groupName, boolean flush) {
        ContentMetadata contentMetadata = claFile.getContentId() == null ? null : contentMetadataService.getById(FileCatService.getContentId(claFile));
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        if (contentMetadata != null) {
            builder.setFirstMemberContentId(contentMetadata.getId());
            builder.setNumOfFiles((long) contentMetadata.getFileCount());
        } else {
            logger.warn("Failed to find content metadata for file with ID {} and name {}", claFile.getFileId(), claFile.getFullName());
        }
        builder.setName(groupName);
        builder.setFirstMemberId(claFile.getFileId());
        return fileGroupRepository.createGroup(builder, fileGroupRepository.getNextAvailableId(), GroupType.ANALYSIS_GROUP);
    }

    public FileGroup updateAnalysisGroupHasRefinement(String groupId, boolean hasRefinement) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(groupId);
        builder.setHasSubgroups(hasRefinement);
        fileGroupRepository.saveGroupChanges(builder);

        if (hasRefinement) {
            analysisGroupsWithSubGroups.add(groupId);
        } else {
            analysisGroupsWithSubGroups.remove(groupId);
        }

        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public FileGroup updateGroupHasNoFiles(String groupId) {
        fileGroupRepository.updateFieldByQueryMultiFields(new String[]{FIRST_MEMBER_ID.getSolrName(), NUM_OF_FILES.getSolrName(),
                        FIRST_MEMBER_CONTENT_ID.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{null, null, null, String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                ID.filter(groupId), null,
                SolrCommitType.SOFT_NOWAIT);

        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public FileGroup updateGroupFiles(String groupId, Long fileId, Long contentId, long filesNum) {
        fileGroupRepository.updateFieldByQueryMultiFields(new String[]{FIRST_MEMBER_ID.getSolrName(), NUM_OF_FILES.getSolrName(),
                        FIRST_MEMBER_CONTENT_ID.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{fileId == null ? null : String.valueOf(fileId),
                        String.valueOf(filesNum),
                        contentId == null ? null : String.valueOf(contentId),
                        String.valueOf(System.currentTimeMillis())},
                SolrFieldOp.SET.getOpName(),
                ID.filter(groupId), null,
                SolrCommitType.SOFT_NOWAIT);

        return fileGroupRepository.getById(groupId).orElse(null);
    }

    public boolean doesAnalysisGroupHasRefinement(String groupId) {
        checkDataLoaded();
        return analysisGroupsWithSubGroups.contains(groupId);
    }

    public Set<String> getAllAnalysisGroupsWithRefinement() {
        checkDataLoaded();
        return analysisGroupsWithSubGroups;
    }

    public String getRawAnalysisGroupIdIfExist(String groupId) {
        checkDataLoaded();
        return subGroupToRawAnalysisGroup.get(groupId);
    }

    private void checkDataLoaded() {
        if (!isInitSubGroups) {
            throw new RuntimeException("sub groups not loaded to memory - solr issue");
        }
    }

    public FileGroup createNewAnalysisSubGroup(String groupName, String rawAnalysisGroupId, boolean copyAssociations) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setName(groupName);
        builder.setRawAnalysisGroupId(rawAnalysisGroupId);
        FileGroup fg = fileGroupRepository.createGroup(builder, fileGroupRepository.getNextAvailableId(), GroupType.SUB_GROUP);

        subGroupToRawAnalysisGroup.put(fg.getId(), rawAnalysisGroupId);

        if (copyAssociations) {
            copyGroupAssociations(rawAnalysisGroupId, fg.getId());
        }

        return fg;
    }

    public GroupDto createNewUserGroup(String groupName, FileGroup firstGroup) {
        logger.debug("Create new UserGroup {}", groupName);
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setFirstMemberContentId(firstGroup.getFirstMemberContentId());
        builder.setFirstMemberId(firstGroup.getFirstMemberId());
        builder.setName(groupName);
        builder.setGeneratedName(groupName);
        FileGroup saved = fileGroupRepository.createGroup(builder, fileGroupRepository.getNextAvailableId(), GroupType.USER_GROUP);
        return convertGroupToDto(saved, null, null, false, null, null);
    }

    public FileGroup createNewUserGroup(String groupName, SolrFileEntity firstMember) {
        logger.debug("Create new UserGroup {}", groupName);
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setFirstMemberContentId(FileCatService.getContentId(firstMember));
        builder.setFirstMemberId(firstMember.getFileId());
        builder.setName(groupName);
        builder.setGeneratedName(groupName);
        return fileGroupRepository.createGroup(builder, fileGroupRepository.getNextAvailableId(), GroupType.USER_GROUP);
    }

    public FileGroup addSubGroupToUserGroup(String subGroupId, String userGroupId) {
        logger.debug("Add subGroup {} to user group {}", subGroupId, userGroupId);
        groupModificationLock.lock();
        try {
            FileGroup userGroup = fileGroupRepository.getById(userGroupId).orElse(null);
            if (!GroupType.USER_GROUP.equals(userGroup.getGroupType())) {
                throw new RuntimeException(messageHandler.getMessage("group.join.type.failure"));
            }
            FileGroup subGroup = fileGroupRepository.getById(subGroupId).orElse(null);
            if (GroupType.USER_GROUP.equals(subGroup.getGroupType())) {
                return moveGroupsFromUserGroupToUserGroup(subGroup, userGroup);
//                throw new RuntimeException("Can only add analysis groups to groups");
            } else if (GroupType.SUB_GROUP.equals(subGroup.getGroupType())) {
                throw new RuntimeException(messageHandler.getMessage("group.join.type.failure"));
            } else if (subGroup.isHasSubgroups()) {
                throw new RuntimeException(messageHandler.getMessage("group.join.type.failure"));
            }

            List<AssociationEntity> associations = copyGroupAssociations(subGroup, userGroup);

            List<SolrInputDocument> docs = new ArrayList<>();

            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setId(subGroupId);
            builder.setParentGroupId(userGroupId);
            builder.setCountOnLastNaming(-1L); // TODO vladi: this is an intentional misuse of this field
            // in order to temporarily mark subgroups as candidates for grouping
            // will become irrelevant in scale 2
            SolrInputDocument doc = builder.buildAtomicUpdate();
            docs.add(doc);

            builder = SolrFileGroupBuilder.create();
            builder.setId(userGroupId);
            builder.setDeleted(false);
            builder.setNumOfFiles((userGroup.getNumOfFiles() < 0 ? 0 : userGroup.getNumOfFiles()) + subGroup.getNumOfFiles());
            builder.setAssociations(userGroup.getAssociations());
            doc = builder.buildAtomicUpdate();
            docs.add(doc);

            fileGroupRepository.saveAll(docs);
            fileGroupRepository.softCommitNoWaitFlush();

            createApplyOperationsForNewGroupAssoc(userGroup.getId(), associations);

            return fileGroupRepository.getById(userGroupId).orElse(null);
        } finally {
            groupModificationLock.unlock();
        }
    }

    private FileGroup moveGroupsFromUserGroupToUserGroup(FileGroup source, FileGroup target) {
        fileGroupRepository.updateFieldByQueryMultiFields(new String[]{PARENT_GROUP_ID.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{null, target.getId()},
                SolrFieldOp.SET.getOpName(),
                PARENT_GROUP_ID.filter(source.getId()), null,
                SolrCommitType.NONE);


        List<SolrInputDocument> docs = new ArrayList<>();

        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(source.getId());
        builder.setNumOfFiles(0L);
        builder.setDeleted(true);
        builder.setParentGroupId(target.getId());
        builder.setDirty(false);
        docs.add(builder.buildAtomicUpdate());

        builder = SolrFileGroupBuilder.create();
        builder.setId(target.getId());
        builder.setNumOfFiles(target.getNumOfFiles() + source.getNumOfFiles());
        docs.add(builder.buildAtomicUpdate());

        logger.debug("move to user group set source group {} as deleted", source.getId());
        fileGroupRepository.saveAll(docs);
        fileGroupRepository.softCommitNoWaitFlush();

        return fileGroupRepository.getById(target.getId()).orElse(null);
    }

    public List<FileGroup> findSubGroups(String userGroupId) {
        return fileGroupRepository.getByQuery(
                Query.create().addFilterWithCriteria(Criteria.create(PARENT_GROUP_ID, SolrOperator.EQ, userGroupId))
        );
    }

    public void removeSubGroupFromUserGroup(String subGroupId, String userGroupId) {
        groupModificationLock.lock();
        try {

            Optional<FileGroup> userGroupOpt = fileGroupRepository.getById(userGroupId);
            Optional<FileGroup> subGroupOpt = fileGroupRepository.getById(subGroupId);

            if (!userGroupOpt.isPresent() || !subGroupOpt.isPresent()) {
                throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
            }

            if (subGroupOpt.get().getParentGroupId() == null) {
                throw new BadRequestException(messageHandler.getMessage("group.parent.missing"), BadRequestType.UNSUPPORTED_VALUE);
            } else if (!subGroupOpt.get().getParentGroupId().equals(userGroupId)) {
                throw new BadRequestException(messageHandler.getMessage("group.parent.mismatch"), BadRequestType.UNSUPPORTED_VALUE);
            }

            SolrInputDocument doc = new SolrInputDocument();
            doc.setField(ID.getSolrName(), subGroupId);
            SolrFileGroupRepository.addField(doc, PARENT_GROUP_ID, null, SolrFieldOp.SET);
            SolrFileGroupRepository.addField(doc, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
            fileGroupRepository.atomicUpdate(doc);

            if (subGroupOpt.get().getNumOfFiles() > 0) {
                SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
                builder.setId(userGroupId);
                long newUserGroupFileNum = userGroupOpt.get().getNumOfFiles() - subGroupOpt.get().getNumOfFiles();
                builder.setNumOfFiles(newUserGroupFileNum);

                if (newUserGroupFileNum <= 0) {
                    logger.debug("removeSubGroupFromUserGroup: " +
                                    "found userGroup {} with zero file. deleteUserGroupZeroNumOfFiles={}",
                            userGroupId, deleteUserGroupZeroNumOfFiles);
                    if (deleteUserGroupZeroNumOfFiles) {
                        builder.setDeleted(true);
                    }
                }
                fileGroupRepository.saveGroupChanges(builder);
            }

            fileGroupRepository.softCommitNoWaitFlush();
        } finally {
            groupModificationLock.unlock();
        }
    }

    public void deleteSubGroup(FileGroup subGroup) {
        if (subGroup.getGroupType().equals(GroupType.SUB_GROUP)) {
            deleteGroupWithAssociations(subGroup.getId());
            subGroupToRawAnalysisGroup.remove(subGroup.getId());
        }
    }

    public void deleteGroupWithAssociations(String groupId) {
        FileGroup fg = fileGroupRepository.getById(groupId).orElse(null);
        if (fg != null) {
            fileGroupRepository.deleteById(groupId);
        }
    }

    public void deleteUserGroup(String id) {
        groupModificationLock.lock();
        try {
            FileGroup subGroup = fileGroupRepository.getById(id).orElse(null);
            if (subGroup == null) {
                throw new RuntimeException(messageHandler.getMessage("group.missing"));
            }
            if (!GroupType.USER_GROUP.equals(subGroup.getGroupType())) {
                logger.error("Can't delete group {}. Not a user group", subGroup);
                throw new RuntimeException(messageHandler.getMessage("group.delete.type.failure"));
            }

            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setId(id);
            builder.setNumOfFiles(0L);
            builder.setDeleted(true);
            builder.setDirty(false);
            logger.debug("delete user group set group {} as deleted", subGroup.getId());
            fileGroupRepository.saveGroupChanges(builder);
        } finally {
            groupModificationLock.unlock();
        }
    }

    public void applyDocTypeChangeForGroup(String userGroupId, AssociationEntity association, boolean isDeleted) {
        BasicScope basicScope = association.getAssociationScopeId() == null ? null :
                scopeService.getFromCache(association.getAssociationScopeId());
        associationsService.updateDocTypeToCategoryFiles(userGroupId, CatFileFieldType.USER_GROUP_ID,
                association.getDocTypeId(), !isDeleted, basicScope, TagPriority.FILE_GROUP, SolrCommitType.SOFT_NOWAIT);
    }

    public void applyDocTypeChangeForChildGroup(String analysisGroupId, AssociationEntity association, boolean isDeleted) {
        BasicScope basicScope = association.getAssociationScopeId() == null ? null :
                scopeService.getFromCache(association.getAssociationScopeId());
        associationsService.updateDocTypeToCategoryFiles(analysisGroupId, CatFileFieldType.ANALYSIS_GROUP_ID,
                association.getDocTypeId(), !isDeleted, basicScope, TagPriority.FILE_GROUP, SolrCommitType.SOFT_NOWAIT);
    }

    public void overrideGroupNumOfFiles(String userGroupId, long numOfFiles) {
        groupModificationLock.lock();
        try {
            logger.debug("Override group {} numOfFiles to {}", userGroupId, numOfFiles);
            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setId(userGroupId);
            builder.setNumOfFiles(numOfFiles);
            builder.setDirty(true);
            fileGroupRepository.saveGroupChanges(builder);
        } finally {
            groupModificationLock.unlock();
        }
    }

    public long convertGroupFacetResults(FacetField facetField, FacetField facetFieldUnfiltered,
                                         List<AggregationCountItemDTO<GroupDto>> content) {
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set
        List<String> ids = facetField.getValues().stream().map(FacetField.Count::getName).collect(Collectors.toList());
        //Fetch all group ids from the database
        List<FileGroup> groups = findByIds(ids);
        if (groups.size() != ids.size()) {
            if (logger.isDebugEnabled()) {
                String missingIdsStr = "-";
                if (logMissingResults) {
                    List<String> missing = new ArrayList<>(ids);
                    missing.removeAll(groups.stream().map(FileGroup::getId).collect(Collectors.toSet()));
                    missingIdsStr = missing.stream().collect(Collectors.joining(","));
                }
                logger.debug("Missing groups in DB. asked for {} and got only {}. Missing groupIds: {}",
                        ids.size(), groups.size(), missingIdsStr);
            }
        }

        Map<String, Long> unFilteredCount = new HashMap<>();
        if (facetFieldUnfiltered != null) {
            for (FacetField.Count count : facetFieldUnfiltered.getValues()) {
                unFilteredCount.put(count.getName(), count.getCount());
            }
        }

        List<Long> firstMemberIds = groups.stream().map(FileGroup::getFirstMemberId).filter(Objects::nonNull).collect(Collectors.toList());
        Set<Long> rootFolderIds = new HashSet<>();
        Map<Long, SolrFileEntity> firstMembers = new HashMap<>();
        if (firstMemberIds != null && !firstMemberIds.isEmpty()) {
            List<SolrFileEntity> files = claFileGroupOperations.find(Query.create()
                    .setRows(firstMemberIds.size())
                    .addFilterWithCriteria(CatFileFieldType.FILE_ID, SolrOperator.IN, firstMemberIds).build());
            files.forEach(file -> {
                if (firstMembers.containsKey(file.getFileId())) {
                    logger.warn("duplicate file in first members, check solr for issues. file id {}", file.getFileId());
                } else {
                    firstMembers.put(file.getFileId(), file);
                    rootFolderIds.add(file.getRootFolderId());
                }
            });
        }

        List<RootFolder> rootFolders = docStoreService.getRootFoldersCached(rootFolderIds);
        Map<Long, RootFolder> rootFoldersByIds = rootFolders.stream().collect(Collectors.toMap(RootFolder::getId, r -> r));

        List<String> parentGroupIds  = groups.stream().map(FileGroup::getParentGroupId).filter(Objects::nonNull).collect(Collectors.toList());
        List<FileGroup> parentGroups = parentGroupIds.isEmpty() ? new ArrayList<>() : fileGroupRepository.getByIds(parentGroupIds);
        Map<String, FileGroup> parentGroupsByIds = parentGroups.stream().collect(Collectors.toMap(FileGroup::getId, r -> r));

        //Convert database results to a map (we need that to keep consistent order in results)
        Map<String, FileGroup> fileGroupMap = groups.stream().filter(g -> !g.isDeleted()).collect(Collectors.toMap(FileGroup::getId, g -> g));
        AssociationRelatedData associationsData = associationsService.getAssociationRelatedDataGroups(fileGroupMap.values());
        Map<String, List<AssociationEntity>> fileGroupAssociations = associationsData.getPerGroupAssociations();

        logger.debug("convert Facet Field result for {} elements to GroupDto. DB returned {} elements", ids.size(), groups.size());
        long pageAggCount = 0;
        for (FacetField.Count count : facetField.getValues()) {
            FileGroup fileGroup = fileGroupMap.get(count.getName());
            if (fileGroup == null) {
                logger.error("Group {} exists in Solr but not in mySQL num files {}", count.getName(), count.getCount());
                continue;
            }

            SolrFileEntity firstMember = firstMembers.get(fileGroup.getFirstMemberId());
            FileGroup parentGroup = fileGroup.getParentGroupId() == null ? null : parentGroupsByIds.get(fileGroup.getParentGroupId());
            GroupDto groupDto = convertGroupToDto(fileGroup, parentGroup,
                    firstMember, true,
                    AssociationsService.getDocTypeAssociation(fileGroupAssociations.get(fileGroup.getId())),
                    fileGroupAssociations.get(fileGroup.getId()), associationsData.getEntities(),
                    firstMember == null ? null : rootFoldersByIds.get(firstMember.getRootFolderId()));

            refineAssociations(groupDto, null);
            AggregationCountItemDTO<GroupDto> item = new AggregationCountItemDTO<>(groupDto, count.getCount());
            content.add(item);
            pageAggCount += count.getCount();
            if (unFilteredCount.containsKey(fileGroup.getId())) {
                item.setCount2(unFilteredCount.get(fileGroup.getId()));
            }
        }
        return pageAggCount;
    }

    public void userAttachContentToGroup(Long contentId, String fileGroupId) {
        logger.debug("Attach File Content {} to fileGroup {}", contentId, fileGroupId);
        contentMetadataService.setGroupAndHintForContentManual(contentId.toString(), fileGroupId);
    }

    public void refreshGroup(String groupId) {

        Table<String, Long, Long> result = contentMetadataService.getFirstMemberContentsForUserGroups(Collections.singletonList(groupId));

        FileGroup one = fileGroupRepository.getById(groupId).orElse(null);
        if (one == null) {
            throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(groupId);

        if (one.getGroupType().equals(GroupType.USER_GROUP)) {
            long numOfFilesInUserGroup = getFileNumForUserGroup(groupId);
            builder.setNumOfFiles(numOfFilesInUserGroup);
        } else {
            long numOfFilesInAnalysisGroup = getFileNumForAnalysisGroup(groupId);
            builder.setNumOfFiles(numOfFilesInAnalysisGroup);
        }

        if (result != null && !result.isEmpty()) {
            Table.Cell<String, Long, Long> cell = result.cellSet().iterator().next();
            builder.setFirstMemberId(cell.getColumnKey());
            builder.setFirstMemberContentId(cell.getValue());
        }

        fileGroupRepository.saveGroupChanges(builder);
    }

    public void updateGroupFirstMemberContentAndFile(String groupId) {
        Table<String, Long, Long> result = contentMetadataService.getFirstMemberContentsForUserGroups(Collections.singletonList(groupId));

        if (!result.isEmpty()) {
            SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
            builder.setId(groupId);

            Table.Cell<String, Long, Long> cell = result.cellSet().iterator().next();
            builder.setFirstMemberId(cell.getColumnKey());
            builder.setFirstMemberContentId(cell.getValue());

            fileGroupRepository.saveGroupChanges(builder);
        } else {
            logger.debug("cannot locate a member for group {} update empty", groupId);
            updateGroupHasNoFiles(groupId);
        }
    }

    @Transactional
    public void updateGroupName(String groupId, String newGroupName) {
        FileGroup fileGroup = fileGroupRepository.getById(groupId).orElse(null);
        if (fileGroup == null) {
            throw new BadRequestException(messageHandler.getMessage("group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        logger.info("Update group {} name to {}", fileGroup, newGroupName);
        boolean isGroupWithoutRefinementRule = true;
        if (fileGroup.getGroupType().equals(GroupType.SUB_GROUP)) {
            List<RefinementRuleDto> rules = refinementRuleService.getByRawAnalysisGroupId(fileGroup.getRawAnalysisGroupId());
            for (RefinementRuleDto rule : rules) {
                if (groupId.equalsIgnoreCase(rule.getSubgroupId())) {
                    rule.setSubgroupName(newGroupName);
                    refinementRuleService.updateRefinementRule(rule, true);
                    isGroupWithoutRefinementRule = false;
                    break;
                }
            }
        }

        if (isGroupWithoutRefinementRule) { // either not sub group or others sub group
            updateGroupNameInDB(groupId, newGroupName);
        }
    }

    public void updateGroupNameInDB(String groupId, String newGroupName) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(groupId);
        builder.setName(newGroupName);
        fileGroupRepository.saveGroupChanges(builder);
    }

    public List<SolrInputDocument> getFirstMemberQueries(List<String> groupIds, Map<String, Long> firstMembers) {
        return getFirstMemberQueries(groupIds, null, firstMembers);
    }

    private List<SolrInputDocument> getFirstMemberQueries(List<String> groupIds, Map<String, Long> firstMembersContents, Map<String, Long> firstMembersFiles) {
        List<SolrInputDocument> queries = new ArrayList<>();

        groupIds.forEach(gid -> {
            SolrInputDocument doc = new SolrInputDocument();
            doc.setField(ID.getSolrName(), gid);

            Long firstMemberId = firstMembersFiles == null ? null : firstMembersFiles.get(gid);
            Long contentFirstMemberId = firstMembersContents == null ? null : firstMembersContents.get(gid);

            if (firstMemberId != null && contentFirstMemberId != null) {
                SolrFileGroupRepository.addField(doc, FIRST_MEMBER_CONTENT_ID, contentFirstMemberId, SolrFieldOp.SET);
                SolrFileGroupRepository.addField(doc, FIRST_MEMBER_ID, firstMemberId, SolrFieldOp.SET);
                logger.trace("upd first member {} and content {} for group {}", firstMemberId, contentFirstMemberId, gid);
            } else if (firstMemberId != null) {
                SolrFileGroupRepository.addField(doc, FIRST_MEMBER_ID, firstMemberId, SolrFieldOp.SET);
                logger.trace("no first member content found for group {} update file {}", gid, firstMemberId);
            } else if (contentFirstMemberId != null) {
                SolrFileGroupRepository.addField(doc, FIRST_MEMBER_CONTENT_ID, contentFirstMemberId, SolrFieldOp.SET);
                logger.trace("no first member found for group {} update content {}", gid, contentFirstMemberId);
            } else {
                logger.trace("no first member or content found for group {}", gid);
            }

            if (firstMemberId != null || contentFirstMemberId != null) {
                SolrFileGroupRepository.addField(doc, UPDATE_DATE, System.currentTimeMillis(), SolrFieldOp.SET);
                queries.add(doc);
            }
        });

        return queries;
    }

    public void updateAnalysisGroupsCatFileByContents(String newGroupId, Collection<Long> contentIds, boolean commit) {
        claFileGroupOperations.updateAnalysisGroupsCatFileByContents(newGroupId, contentIds, commit);
    }

    public Map<String, Long> getFirstMemberForAnalysisGroups(List<String> groupIds) {
        return claFileGroupOperations.getFirstMemberForAnalysisGroups(groupIds);
    }

    public void updateUserGroupsCatFileByContents(String oldGroupId, String newGroupId, Collection<Long> contentIds, boolean commit) {
        claFileGroupOperations.updateUserGroupsCatFileByContents(oldGroupId, newGroupId, contentIds, commit);
    }

    public void updateUserGroupsCatFileByOldUserGroup(String userGroupId, Collection<String> analysisGroups, boolean commit) {
        claFileGroupOperations.updateUserGroupsCatFileByOldUserGroup(userGroupId, analysisGroups, commit);
    }

    public void updateAnalysisGroupsCatFile(String newGroupId, Collection<String> oldGroupIds, boolean commit) {
        claFileGroupOperations.updateAnalysisGroupsCatFile(newGroupId, oldGroupIds, commit);
    }

    public void updateUserGroupsCatFile(String userGroupId, Collection<String> analysisGroups, boolean commit) {
        claFileGroupOperations.updateUserGroupsCatFile(userGroupId, analysisGroups, commit);
    }

    /**
     * This is a temporary measure, misusing the CountOnLastNaming field
     * in order to indicate that a sub group still has files that need to be
     * assigned to the new user group during groups joining.
     * When all the files are assigned, the indication is cleared using this method.
     * TODO falcon: Becomes irrelevant in scale 2
     *
     * @param subGroupId sub group ID
     */
    public void updateGroupFinishedMarkingFiles(String subGroupId) {
        SolrFileGroupBuilder builder = SolrFileGroupBuilder.create();
        builder.setId(subGroupId);
        builder.setCountOnLastNaming(0L);
        fileGroupRepository.saveGroupChanges(builder);
    }

    public Pair<Long, Long> findNumOfFilesInGroups(int minGroupSize) {
        Query query = Query.create().setRows(0)
                .addFilterWithCriteria(Criteria.create(NUM_OF_FILES, SolrOperator.GTE, minGroupSize))
                .addFilterWithCriteria(Criteria.create(PARENT_GROUP_ID, SolrOperator.MISSING, null))
                .addFilterWithCriteria(Criteria.create(DELETED, SolrOperator.EQ, false))
                .setStatsField(NUM_OF_FILES)
                ;

        QueryResponse resp = fileGroupRepository.runQuery(query.build());
        FieldStatsInfo info = resp.getFieldStatsInfo().get(NUM_OF_FILES.getSolrName());

        if (info == null || info.getCount() == 0) return Pair.of(0L, 0L);
        long groupNum = info.getCount();
        long fileNum = ((Double)info.getSum()).longValue();
        return Pair.of(groupNum, fileNum);
    }

    public int findNumOfGroupsWithDocTypes(long minGroupSize) {
        return (int)associationsService.docTypeAssociationCountForGroups(minGroupSize);
    }

    public GroupDto convertGroupToDto(FileGroup fileGroup, FileGroup parentFileGroup,
                                             SolrFileEntity firstMember, boolean withFileTags,
                                             Collection<AssociationEntity> groupDocTypes,
                                             Collection<AssociationEntity> groupTagAssociations) {
        List<AssociableEntityDto> entities = associationsService.getEntitiesForAssociations(groupTagAssociations);
        return convertGroupToDto(fileGroup, parentFileGroup, firstMember, withFileTags, groupDocTypes, groupTagAssociations, entities, null);
    }

    private GroupDto convertGroupToDto(FileGroup fileGroup,
                                       FileGroup parentFileGroup,
                                       SolrFileEntity firstMember, boolean withFileTags,
                                       Collection<AssociationEntity> groupDocTypes,
                                       Collection<AssociationEntity> groupTagAssociations,
                                       List<AssociableEntityDto> entities,
                                       RootFolder firstMemberRootFolder) {


        if (fileGroup.getFirstMemberId() == null) {
            logger.warn("Group {} with {} files and deleted={} doesn't have a first member",
                    fileGroup.getId(), fileGroup.getNumOfFiles(), fileGroup.isDeleted());
        } else if (firstMember == null) {
            logger.debug("issue getting file member sample {} for group {}", fileGroup.getFirstMemberId(), fileGroup.getId());
        }

        String firstMemberFullName = (firstMember == null ? null : firstMember.getFullName());
        if (firstMemberRootFolder != null && firstMemberRootFolder.getRealPath() != null) {
            firstMemberFullName = firstMemberRootFolder.getRealPath() + firstMemberFullName;
        }
        String firstMemberFullPath = firstMemberFullName == null ? null : DocFolderUtils.extractFolderFromPath(firstMemberFullName);

        GroupDto groupDto = new GroupDto(fileGroup.getId(), fileGroup.getName(), fileGroup.getNumOfFiles(),
                fileGroup.getFirstMemberId(),
                firstMemberFullPath,
                firstMember == null ? null : firstMember.getBaseName(),
                fileGroup.getGeneratedName());

        if (withFileTags) {
            groupDto.setFileTagAssociations(AssociationUtils.collectGroupFileTagAssociations(null, groupTagAssociations, groupDocTypes, entities, false));
            groupDto.setFileTagDtos(groupDto.getFileTagAssociations().stream()
                    .map(FileTagAssociationDto::getFileTagDto)
                    .collect(Collectors.toSet()));
            groupDto.setDocTypeAssociations(AssociationUtils.collectDocTypeAssociations(groupDocTypes, null, FileTagSource.BUSINESS_ID, entities));
            groupDto.setDocTypeDtos(groupDto.getDocTypeAssociations().stream()
                    .map(DocTypeAssociationDto::getDocTypeDto)
                    .collect(Collectors.toSet()));
            groupDto.setAssociatedBizListItems(AssociationUtils.collectAssociatedBizListItemsFromGroup(groupTagAssociations, entities));
            groupDto.setFunctionalRoles(AssociationUtils.collectAssociatedFunctionalRules(groupTagAssociations, entities));
        }

        if (fileGroup.getParentGroupId() != null) {
            if (parentFileGroup != null) {
                if (parentFileGroup.getName() == null) {
                    groupDto.setParentGroupName(parentFileGroup.getGeneratedName());
                } else {
                    groupDto.setParentGroupName(parentFileGroup.getName());
                }
            }
            groupDto.setParentGroupId(fileGroup.getParentGroupId());
        }

        groupDto.setHasSubgroups(fileGroup.isHasSubgroups());
        groupDto.setRawAnalysisGroupId(fileGroup.getRawAnalysisGroupId());
        groupDto.setGroupType(fileGroup.getGroupType());
        groupDto.setDeleted(fileGroup.isDeleted());
        return groupDto;
    }

    public Set<GroupDto> convertGroupsToDto(Collection<FileGroup> subGroups, FileGroup parentGroup, Map<String, List<AssociationEntity>> groupDocTypeAssociations,
                                            Map<String, List<AssociationEntity>> groupTagAssociations, List<AssociableEntityDto> entities) {
        Set<GroupDto> result = new HashSet<>();
        if (subGroups != null) {
            for (FileGroup subGroup : subGroups) {
                result.add(convertGroupToDto(subGroup, parentGroup, null, false,
                        groupDocTypeAssociations == null ? null : groupDocTypeAssociations.get(subGroup.getId()),
                                groupTagAssociations == null ? null : groupTagAssociations.get(subGroup.getId()), entities, null));
            }
        }
        return result;
    }
}
