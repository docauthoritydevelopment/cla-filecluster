package com.cla.filecluster.service.categories;

import com.cla.common.domain.dto.RowDTO;
import com.cla.filecluster.domain.entity.filetype.Extension;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.repository.jpa.fileType.ExtensionRepository;
import com.cla.filecluster.repository.jpa.fileType.FileTypeCategoryRepository;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 *
 */
@Service
public class FileTypeCategoryService {

    private static final Logger logger = LoggerFactory.getLogger(FileTypeCategoryService.class);

    private static final String NAME_FIELD = "Name";
    private final static String FILE_TYPE_CATEGORY_FIELD = "File_type_category_name";
    private final static String[] REQUIRED_EXTENSIONS_HEADERS = {NAME_FIELD, FILE_TYPE_CATEGORY_FIELD};

    @Autowired
    private FileTypeCategoryRepository fileTypeCategoryRepository;

    @Autowired
    private ExtensionRepository extensionRepository;

    @Autowired
    private CsvReader csvReader;

    @Value("${file_type.category.default_file}")
    private String fileTypeCategoriesDefaultsFileName;

    @Transactional(readOnly = true)
    public List<FileTypeCategory> getFileTypeCategoryByPartialName(String fileTypeCategoryNamePart) {
        return fileTypeCategoryRepository.findByNameLike(fileTypeCategoryNamePart);
    }

    @Transactional(readOnly = true)
    public List<FileTypeCategory> getAll() {
        return fileTypeCategoryRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<FileTypeCategory> findByName(String name) {
        return fileTypeCategoryRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    public Optional<FileTypeCategory> findById(Long id) {
        return fileTypeCategoryRepository.findById(id);
    }

    @Transactional
    public void loadDefaultExtensions() {
        if (Strings.isNullOrEmpty(fileTypeCategoriesDefaultsFileName)) {
            logger.info("No default file type categories file defined - ignored");
            return;
        }

        Map<String, Integer> extensionsLocationMap = CsvUtils.handleCsvHeaders(csvReader, fileTypeCategoriesDefaultsFileName, REQUIRED_EXTENSIONS_HEADERS);
        int failedRows = 0;
        try {
            failedRows = csvReader.readAndProcess(fileTypeCategoriesDefaultsFileName,
                    row -> createFileTypeCategoryFromCsvRow(row, extensionsLocationMap), null, null);
        } catch (Exception e) {
            logger.error("Failed to load extensions - problem in csv file: ", e);
        }
        if (failedRows > 0) {
            logger.error("Failed to load {} extensions - problem in csv file", failedRows);
        }
    }

    private void createFileTypeCategoryFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length == 0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String fileTypeCategoryNameStr = fieldsValues[headersLocationMap.get(FILE_TYPE_CATEGORY_FIELD)].trim();

        String name = fieldsValues[headersLocationMap.get(NAME_FIELD)].trim();
        List<Extension> findExt = extensionRepository.findByName(name);
        if (findExt.size() == 0) {
            Extension ext = new Extension();
            ext.setName(name);
            extensionRepository.save(ext);

            List<FileTypeCategory> findCatList = fileTypeCategoryRepository.findByName(fileTypeCategoryNameStr);
            if (findCatList.size() == 0) {
                FileTypeCategory ftc = new FileTypeCategory();
                ftc.setName(fileTypeCategoryNameStr);
                ftc.getExtensions().add(ext);
                fileTypeCategoryRepository.save(ftc);
            } else {
                findCatList.get(0).getExtensions().add(ext);
            }
        } else {
            logger.error("Extension name -'{}' already defined", name);
        }
    }
}
