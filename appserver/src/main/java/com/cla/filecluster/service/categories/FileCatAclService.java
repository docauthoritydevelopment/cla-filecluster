package com.cla.filecluster.service.categories;

import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.AclItemDto;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.SolrRepository;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.security.DomainAccessHandler;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.cla.common.constants.CatFileFieldType.*;

@Service
public class FileCatAclService {

    @Autowired
    private DomainAccessHandler domainAccessHandler;

    @Value("${solr.catFile.expiredUserKey:ExpiredUser}")
    private String expiredUserKey;

    public void updateAcls(FileCatBuilder builder, Collection<AclItemDto> aclItems) {
        if (aclItems == null || aclItems.size() == 0) {
            builder.setAclRead(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
            builder.setAclWrite(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
            return;
        }
        Map<AclType, List<AclItemDto>> aclByType = aclItems.stream().collect(Collectors.groupingBy(AclItemDto::getType));
        for (Map.Entry<AclType, List<AclItemDto>> typeListEntry : aclByType.entrySet()) {

            boolean expiredAdded = false;
            Collection<String> values = new HashSet<>();
            Collection<String> valuesFull = new HashSet<>();
            Collection<String> valuesLower = new HashSet<>();
            for (AclItemDto aclItem : typeListEntry.getValue()) {
                String ent = aclItem.getEntity();
                valuesFull.add(ent);
                if (SolrFileCatRepository.EXPIRED_USER_MATCH_PATTERN.matcher(ent).matches()) {
                    if (!expiredAdded) {
                        values.add(expiredUserKey);
                        expiredAdded = true;
                    }
                } else {
                    ent = domainAccessHandler.getAclInEmailFormat(ent);
                    values.add(ent);
                    valuesLower.add(ent.toLowerCase());
                }
            }

            if (typeListEntry.getKey().equals(AclType.READ_TYPE)) {
                builder.setAclRead(new ArrayList<>(values))
                        .setAclReadFull(new ArrayList<>(valuesFull))
                        .setSearchAclRead(new ArrayList<>(valuesLower));
            } else if (typeListEntry.getKey().equals(AclType.DENY_READ_TYPE)) {
                builder.setAclDenyRead(new ArrayList<>(values))
                        .setAclDenyReadFull(new ArrayList<>(valuesFull))
                        .setSearchAclDenyRead(new ArrayList<>(valuesLower));
            } else if (typeListEntry.getKey().equals(AclType.WRITE_TYPE)) {
                builder.setAclWrite(new ArrayList<>(values))
                        .setAclWriteFull(new ArrayList<>(valuesFull));
            } else if (typeListEntry.getKey().equals(AclType.DENY_WRITE_TYPE)) {
                builder.setAclDenyWrite(new ArrayList<>(values))
                        .setAclDenyWriteFull(new ArrayList<>(valuesFull));
            }
        }

        if (!aclByType.containsKey(AclType.READ_TYPE)) {
            builder.setAclRead(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
        }
        if (!aclByType.containsKey(AclType.WRITE_TYPE)) {
            builder.setAclWrite(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
        }

        builder.setLastMetadataChangeDate(System.currentTimeMillis());
    }

    public void updateAcls(SolrFileEntity fileEntity, Collection<AclItemDto> aclItems) {
        if (aclItems == null || aclItems.size() == 0) {
            fileEntity.setAclRead(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
            fileEntity.setAclWrite(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
            return;
        }
        Map<AclType, List<AclItemDto>> aclByType = aclItems.stream().collect(Collectors.groupingBy(AclItemDto::getType));
        for (Map.Entry<AclType, List<AclItemDto>> typeListEntry : aclByType.entrySet()) {

            boolean expiredAdded = false;
            Collection<String> values = new HashSet<>();
            Collection<String> valuesFull = new HashSet<>();
            Collection<String> valuesLower = new HashSet<>();
            for (AclItemDto aclItem : typeListEntry.getValue()) {
                String ent = aclItem.getEntity();
                valuesFull.add(ent);
                if (SolrFileCatRepository.EXPIRED_USER_MATCH_PATTERN.matcher(ent).matches()) {
                    if (!expiredAdded) {
                        values.add(expiredUserKey);
                        expiredAdded = true;
                    }
                } else {
                    ent = domainAccessHandler.getAclInEmailFormat(ent);
                    values.add(ent);
                    valuesLower.add(ent.toLowerCase());
                }
            }

            if (typeListEntry.getKey().equals(AclType.READ_TYPE)) {
                fileEntity.setAclRead(new ArrayList<>(values));
                fileEntity.setAclReadFull(new ArrayList<>(valuesFull));
                fileEntity.setSearchAclRead(new ArrayList<>(valuesLower));
            } else if (typeListEntry.getKey().equals(AclType.DENY_READ_TYPE)) {
                fileEntity.setAclDenyRead(new ArrayList<>(values));
                fileEntity.setAclDenyReadFull(new ArrayList<>(valuesFull));
                fileEntity.setSearchAclDenyRead(new ArrayList<>(valuesLower));
            } else if (typeListEntry.getKey().equals(AclType.WRITE_TYPE)) {
                fileEntity.setAclWrite(new ArrayList<>(values));
                fileEntity.setAclWriteFull(new ArrayList<>(valuesFull));
            } else if (typeListEntry.getKey().equals(AclType.DENY_WRITE_TYPE)) {
                fileEntity.setAclDenyWrite(new ArrayList<>(values));
                fileEntity.setAclDenyWriteFull(new ArrayList<>(valuesFull));
            }
        }

        if (!aclByType.containsKey(AclType.READ_TYPE)) {
            fileEntity.setAclRead(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
        }
        if (!aclByType.containsKey(AclType.WRITE_TYPE)) {
            fileEntity.setAclWrite(Lists.newArrayList(FileCatUtils.EMPTY_VALUE));
        }

        fileEntity.setLastMetadataChangeDate(System.currentTimeMillis());
    }

    public void updateAcls(SolrInputDocument doc, Collection<AclItemDto> aclItems) {
        updateAcls(doc, aclItems, true);
    }

    public void updateAcls(SolrInputDocument doc, Collection<AclItemDto> aclItems, boolean setFullFields) {
        if (aclItems == null || aclItems.size() == 0) {
            Map<String, Object> fieldModifier = SolrRepository.atomicSetValue(FileCatUtils.EMPTY_VALUE);
            doc.setField(ACL_READ.getSolrName(), fieldModifier);
            fieldModifier = SolrRepository.atomicSetValue(FileCatUtils.EMPTY_VALUE);
            doc.setField(ACL_WRITE.getSolrName(), fieldModifier);
            return;
        }
        Map<AclType, List<AclItemDto>> aclByType = aclItems.stream().collect(Collectors.groupingBy(AclItemDto::getType));
        for (Map.Entry<AclType, List<AclItemDto>> typeListEntry : aclByType.entrySet()) {
            String aclField = FileDtoFieldConverter.convertAclToSolrField(typeListEntry.getKey());
            Map<String, Object> fieldModifier = new HashMap<>(1);
            Map<String, Object> fieldModifierFull = new HashMap<>(1);
            Map<String, Object> fieldModifierLower = new HashMap<>(1);
            Collection<String> values = new HashSet<>();
            Collection<String> valuesFull = new HashSet<>();
            Collection<String> valuesLower = new HashSet<>();
            boolean expiredAdded = false;
            for (AclItemDto aclItem : typeListEntry.getValue()) {
                String ent = aclItem.getEntity();
                valuesFull.add(ent);
                if (SolrFileCatRepository.EXPIRED_USER_MATCH_PATTERN.matcher(ent).matches()) {
                    if (!expiredAdded) {
                        values.add(expiredUserKey);
                        expiredAdded = true;
                    }
                } else {
                    ent = domainAccessHandler.getAclInEmailFormat(ent);
                    values.add(ent);
                    valuesLower.add(ent.toLowerCase());
                }
            }

            fieldModifier.put(SolrFieldOp.SET.getOpName(), values);
            doc.addField(aclField, fieldModifier);

            if (setFullFields) {
                fieldModifierFull.put(SolrFieldOp.SET.getOpName(), valuesFull);
                doc.addField(aclField + "_f", fieldModifierFull);    // Field name for the full ACL list (no expired aggregation)
            }

            // fields created for case insensitive search
            if (typeListEntry.getKey().equals(AclType.READ_TYPE) || typeListEntry.getKey().equals(AclType.DENY_READ_TYPE)) {
                fieldModifierLower.put(SolrFieldOp.SET.getOpName(), valuesLower);
                doc.addField("search_" + aclField, fieldModifierLower);
            }
        }

        if (!aclByType.containsKey(AclType.READ_TYPE)) {
            Map<String, Object> fieldModifier = SolrRepository.atomicSetValue(FileCatUtils.EMPTY_VALUE);
            doc.setField(ACL_READ.getSolrName(), fieldModifier);
        }
        if (!aclByType.containsKey(AclType.WRITE_TYPE)) {
            Map<String, Object> fieldModifier = SolrRepository.atomicSetValue(FileCatUtils.EMPTY_VALUE);
            doc.setField(ACL_WRITE.getSolrName(), fieldModifier);
        }

        if (setFullFields) {
            SolrRepository.setFieldOnce(doc, LAST_METADATA_CNG_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));
        }
    }
}
