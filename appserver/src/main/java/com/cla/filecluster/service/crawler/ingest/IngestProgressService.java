package com.cla.filecluster.service.crawler.ingest;

import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.filecluster.service.AbstractProgressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class IngestProgressService extends AbstractProgressService {

    private final static Logger logger = LoggerFactory.getLogger(IngestProgressService.class);

    @Value("${ingester.opmsg.sample-rate:100}")
    private long ingestOpMsgReportRate;

    private long cmCounter = 0;

    @Override
    protected String topic() {
        return Topics.INGEST;
    }

    @Override
    protected Consumer<Event> eventConsumer() {
        return this::consumeEvent;
    }

    private void consumeEvent(Event event){
        Context context = event.getContext();
        switch (event.getEventType()) {
            case OPERATION_MESSAGE:
                if (ingestOpMsgReportRate > 0 && (++cmCounter % ingestOpMsgReportRate) == 0) {
                    setJobOpMsg(context);
                }
                break;
            default:
                logger.warn("Received unknown event type {} for ingest topic", event.getEventType());
        }
    }
}
