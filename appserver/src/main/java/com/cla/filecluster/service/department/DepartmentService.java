package com.cla.filecluster.service.department;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.security.SystemSettingsDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.repository.jpa.department.DepartmentRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.export.exporters.ScannedItemsHeaderFields;
import com.cla.filecluster.service.security.SystemSettingsDefaultProperties;
import com.cla.filecluster.service.security.SystemSettingsService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by ginat on 14/03/2019.
 */
@Service
public class DepartmentService  {

    private static final String PARENT = "Parent";
    private static final String NAME = "Name";
    private static final String UNIQUE_NAME = "Unique_Name";

    private final static String[] requiredHeaders = {NAME, PARENT, UNIQUE_NAME};

    private Logger logger = LoggerFactory.getLogger(DepartmentService.class);

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private SystemSettingsService systemSettingsService;

    @Value("${department.template.file:}")
    private String defaultDepartmentTemplateFilePath;

    @Value("${matter.template.file:}")
    private String defaultMatterTemplateFilePath;

    @Autowired
    private SystemSettingsDefaultProperties confSystemSettings;

    private LoadingCache<Long, Optional<DepartmentDto>> departmentLoadingCache;

    private Random randomNum = new Random();

    @PostConstruct
    void init() {
        departmentLoadingCache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterWrite(15, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, Optional<DepartmentDto>>() {
                    public Optional<DepartmentDto> load(@NotNull Long departmentId) {
                        return dbTemplateUtils.doInTransaction(() -> {
                            Department dept = departmentRepository.findById(departmentId).orElse(null);
                            return Optional.ofNullable(convertDepartmentToDto(dept));
                        });
                    }
                });
    }

    public DepartmentDto getByIdCached(Long departmentId) {
        if (departmentId == null) return null;
        DepartmentDto dto = departmentLoadingCache.getUnchecked(departmentId).orElse(null);
        return dto == null ? null : new DepartmentDto(dto);
    }

    public boolean isLegalInstallationMode(){
        String installationMode = confSystemSettings.getDefault().get("installation-mode");
        if (installationMode != null) {
            return installationMode.toLowerCase().equals("legal");
        }
        return false;
    }

    public String getLegalInstallationMode(){
        return isLegalInstallationMode() ?
                ScannedItemsHeaderFields.MATTER : ScannedItemsHeaderFields.DEPARTMENT;
    }

    public void loadDefaultDepartmentTemplate() {
        if (isLegalInstallationMode())
        {
            if (defaultMatterTemplateFilePath == null || defaultMatterTemplateFilePath.isEmpty()) {
                logger.info("No default matter template file defined - ignored");
            } else {
                logger.info("Load default maooer template from file {}", defaultMatterTemplateFilePath);
                createDepartmentTemplateFromFile(defaultMatterTemplateFilePath);
            }
        }
        else {
            if (defaultDepartmentTemplateFilePath == null || defaultDepartmentTemplateFilePath.isEmpty()) {
                logger.info("No default department template file defined - ignored");
            } else {
                logger.info("Load default department template from file {}", defaultDepartmentTemplateFilePath);
                createDepartmentTemplateFromFile(defaultDepartmentTemplateFilePath);
            }
        }
    }

    @Transactional
    public void createDepartmentTemplateFromFile(String templateFileName) {
        logger.info("Load department template {} into the database", templateFileName);

        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, templateFileName, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(templateFileName, row -> createDepartmentFromCsvRow(row, headersLocationMap), null, null);
        } catch (Exception e) {
            logger.error("Failed to read department template. Got exception " + e.getMessage(), e);
            throw new RuntimeException("Failed to read department template. Got exception " + e.getMessage(), e);
        }
        if (failedRows != 0) {
            logger.error("Failed to read {} department lines", failedRows);
        }
    }

    private void createDepartmentFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        String parent = fieldsValues[headersLocationMap.get(PARENT)];       // Should point to parent's unique name
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String uniqueName = fieldsValues[headersLocationMap.get(UNIQUE_NAME)];

        if (parent != null) {
            parent = parent.trim();
        }

        if (uniqueName != null) {
            uniqueName = uniqueName.trim();
        }

        logger.trace("Create Department by name {} and parent {}", name, parent);
        Department department = new Department();
        department.setName(name);
        department.setUniqueName(getUniqueName(name, uniqueName));
        if (StringUtils.isNotBlank(parent)) {
            department.setParent(getADepartmentByUniqueName(parent));
        }
        departmentRepository.save(department);
    }

    public Department getADepartmentByUniqueName(String uniqueName) {
        List<Department> res = departmentRepository.findByUniqueName(uniqueName);
        if (res == null || res.isEmpty()) {
            return null;
        }
        return res.get(0);
    }

    public Department getADepartmentByName(String parent) {
        List<Department> res = getDepartmentByName(parent);
        if (res == null || res.isEmpty()) {
            return null;
        }
        return res.get(0);
    }

    public Long parseDepartmentFromPath(String name) {
        Long latestId = null;
        if(!Strings.isNullOrEmpty(name)) {
            StringTokenizer strTkn = new StringTokenizer(name, DepartmentDto.SEPARATOR);

            while (strTkn.hasMoreElements()) {
                String dept = strTkn.nextToken();
                Department department = (latestId == null ?
                        getADepartmentByName(dept) :
                        getDepartmentByNameAndParent(dept, latestId));
                if (department == null) {
                    return null;
                }
                latestId = department.getId();
            }
        }
        return latestId;
    }

    public DepartmentDto createDepartmentFromPath(String name) {
        Long latestId = null;
        if(!Strings.isNullOrEmpty(name)) {
            StringTokenizer strTkn = new StringTokenizer(name, DepartmentDto.SEPARATOR);

            while (strTkn.hasMoreElements()) {
                String dept = strTkn.nextToken();
                Department department = (latestId == null ?
                        getADepartmentByName(dept) :
                        getDepartmentByNameAndParent(dept, latestId));
                if (department == null) {
                    DepartmentDto dep = new DepartmentDto();
                    dep.setName(dept);
                    dep.setParentId(latestId);
                    DepartmentDto dto = createDepartment(dep);
                    latestId = dto.getId();
                } else {
                    latestId = department.getId();
                }
            }
        }
        return (latestId == null ? null : getByIdCached(latestId));
    }

    public void updateDescriptionById(Long departmentId, String description) {
        Department dept = departmentRepository.getOne(departmentId);
        dept.setDescription(description);
        departmentRepository.save(dept);
    }

    public List<Department> getDepartmentByName(String name) {
        List<Department> parentDepartments = departmentRepository.findByName(name);
        return parentDepartments.stream().filter(d -> !d.isDeleted()).collect(Collectors.toList());
    }

    private String getUniqueName(String name, String uniqueName) {
        String result = null;
        if (!Strings.isNullOrEmpty(uniqueName) && !Strings.isNullOrEmpty(uniqueName.trim())) {
            result = uniqueName;
        }
        do {
            if (result != null) {
                List<Department> matches = departmentRepository.findByUniqueName(result);
                if (matches == null || matches.isEmpty()) {
                    return result;
                }
            }
            int randomInt = 1 + randomNum.nextInt(999);
            result =  name + randomInt;

        } while(true);
    }

    @Transactional
    public DepartmentDto createDepartment(DepartmentDto departmentDto) {

        validateDepartmentNameNotEmpty(departmentDto.getName());
        validateDepartmentNameLegal(departmentDto.getName());
        validateDepartmentNameNotExistUnderParent(departmentDto.getId(), departmentDto.getName(), departmentDto.getParentId());

        Department department = new Department(departmentDto.getName());

        department.setUniqueName(getUniqueName(departmentDto.getName(), departmentDto.getUniqueName()));
        department.setDeleted(false);
        if (departmentDto.getParentId() != null && departmentDto.getParentId() > 0) {
            Department parentDepartment = getDepartmentById(departmentDto.getParentId());
            department.setParent(parentDepartment);
        }
        department.setDescription(departmentDto.getDescription());

        logger.debug("Create department {}", department);
        Department newDepartment = departmentRepository.save(department);

        return convertDepartmentToDto(newDepartment);
    }

    @Transactional
    public DepartmentDto cloneDepartment(DepartmentDto departmentDto) {

        DepartmentDto newDepartmentDto = createDepartment(departmentDto);

        for (String subDepartmentName : departmentDto.getChildrenName()) {
            departmentDto = new DepartmentDto(subDepartmentName);
            departmentDto.setParentId(newDepartmentDto.getId());
            createDepartment(departmentDto);
        }

        return newDepartmentDto;
    }

    @Transactional
    public DepartmentDto updateDepartment(Long departmentId, DepartmentDto departmentDto) {
        Department department = getDepartmentById(departmentId);

        validateDepartmentNotExist(department);

        if (departmentDto.getName() != null && !departmentDto.getName().equals(department.getName())) {
            validateDepartmentNameNotEmpty(departmentDto.getName());
            validateDepartmentNameLegal(departmentDto.getName());
            validateDepartmentNameNotExistUnderParent(departmentId, departmentDto.getName(), departmentDto.getParentId());
            department.setName(departmentDto.getName());
        }

        department.setDescription(departmentDto.getDescription());

        logger.debug("Update department {}", department);
        Department updatedDepartment = departmentRepository.save(department);

        departmentLoadingCache.invalidate(departmentId);

        return convertDepartmentToDto(updatedDepartment);
    }

    @Transactional(readOnly = true)
    public Map<Long, String> getDepartmentNames() {
        Map<Long, String> result = new HashMap<>();
        List<Object[]> departments = departmentRepository.getDepartmentsNames();
        if (departments != null && !departments.isEmpty()) {
            departments.forEach(department -> {
                BigInteger id = (BigInteger)department[0];
                String name = (String)department[1];
                result.put(id.longValue(), name);
            });
        }
        return result;
    }

    @Transactional(readOnly = true)
    public Page<DepartmentDto> getDepartments(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Department> departments = departmentRepository.findAllNotDeleted(pageRequest);
        return  convertDepartmentsToDto(pageRequest, departments);
    }

    @Transactional
    public void deleteDepartments(Department department) {
        logger.debug("Delete all sub department of {}", department);
        List<Department> subDepartments = getSubDepartments(department.getId());
        for (Department subDepartment : subDepartments) {
            deleteDepartments(subDepartment);
        }

        logger.debug("Delete department {}", department);
        deleteDepartment(department.getId());
    }

    @Transactional(readOnly = true)
    public DepartmentDto getDepartmentDtoById(Long departmentId) {
        Department department = getDepartmentById(departmentId);
        return convertDepartmentToDto(department);
    }

    @Transactional(readOnly = true)
    public Department getDepartmentById(Long departmentId) {
        return departmentRepository.findById(departmentId).orElse(null);
    }

    @Transactional(readOnly = true)
    public Department getDepartmentByNameAndParent(String departmentName, Long departmentId) {
        if (departmentId == null) {
            return departmentRepository.findByNullParentAndNameNotDeleted(departmentName);
        }
        else {
            return departmentRepository.findByParentAndNameNotDeleted(departmentId, departmentName);
        }
    }

    @Transactional(readOnly = true)
    public List<Department> getSubDepartments(Long departmentId) {
        return departmentRepository.findByParentNotDeleted(departmentId);
    }

    @Transactional(readOnly = true)
    public List<Department> getByPartialName(String departmentNamePart) {
        return departmentRepository.findByNameLike(departmentNamePart);
    }

    private void deleteDepartment(Long departmentId) {
        Department department = getDepartmentById(departmentId);
        validateDepartmentNotExist(department);
        department.setDeleted(true);
        departmentRepository.save(department);
        departmentLoadingCache.invalidate(departmentId);
    }

    private void validateDepartmentNameNotEmpty(String departmentName) {
        if (Strings.isNullOrEmpty(departmentName) || Strings.isNullOrEmpty(departmentName.trim())) {
            throw new BadParameterException(messageHandler.getMessage("department.name.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void validateDepartmentNameLegal(String name) {
        if (name != null && name.contains(DepartmentDto.SEPARATOR)) {
            throw new BadParameterException(messageHandler.getMessage("department.name.invalid-char"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    private void validateDepartmentNameNotExistUnderParent(Long departmentId, String departmentName, Long parentId) {
        Department departmentNameChk = getDepartmentByNameAndParent(departmentName, parentId);

        if (departmentNameChk != null) {
            if (!departmentNameChk.getId().equals(departmentId)) {
                throw new BadParameterException(messageHandler.getMessage("department.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }
    }

    void validateDepartmentNotExist(Department department) {
        if (department == null) {
            throw new BadParameterException(messageHandler.getMessage("department.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
    }

    public DepartmentDto convertDepartmentToDto(Department department) {
        if (department == null) {
            return null;
        }
        DepartmentDto departmentDto = new DepartmentDto(department.getId(), department.getName());
        departmentDto.setUniqueName(department.getUniqueName());
        departmentDto.setDeleted(department.isDeleted());
        departmentDto.setDescription(department.getDescription());
        if (department.getParent() != null) {
            DepartmentDto parent = getByIdCached(department.getParent().getId());
            departmentDto.setParentId(department.getParent().getId());
            departmentDto.setPath(parent.getPath() == null ? parent.getName() : parent.getPath() + DepartmentDto.SEPARATOR + parent.getName());
        }

        return departmentDto;
    }

    private Page<DepartmentDto> convertDepartmentsToDto(PageRequest pageRequest, Page<Department> allDepartments) {
        List<DepartmentDto> content = new ArrayList<>();
        allDepartments.forEach(department -> content.add(convertDepartmentToDto(department)));
        return new PageImpl<>(content, pageRequest, allDepartments.getTotalElements());
    }

    public List<Department> findByIds(List<Long> departmentIds) {
        return departmentRepository.findByIds(departmentIds);
    }

    List<Department> findAll() {
        return departmentRepository.findAll(Sort.by(Sort.Order.by("name")));
    }
}
