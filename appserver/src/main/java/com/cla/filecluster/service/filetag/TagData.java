package com.cla.filecluster.service.filetag;

import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.pv.FileGroup;

import java.util.List;

/**
 * Created by: yael
 * Created on: 1/8/2019
 */
public class TagData {

    public TagData(FileGroup userGroup, FileGroup analysisGroup, SolrFolderEntity folder) {
        this.userGroup = userGroup;
        this.analysisGroup = analysisGroup;
        this.folder = folder;
    }

    private final SolrFolderEntity folder;
    private final FileGroup userGroup;
    private final FileGroup analysisGroup;

    public SolrFolderEntity getFolder() {
        return folder;
    }

    public FileGroup getUserGroup() {
        return userGroup;
    }

    public FileGroup getAnalysisGroup() {
        return analysisGroup;
    }
}
