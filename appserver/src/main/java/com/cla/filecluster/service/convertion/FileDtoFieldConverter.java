package com.cla.filecluster.service.convertion;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static com.cla.common.constants.CatFileFieldType.*;

/**
 * Created by uri on 08/07/2015.
 */
public class FileDtoFieldConverter {

    public static final String BIZLIST_SOLR_PREFIX = "blt.";
    public static final String BIZLIST_ITEM_SOLR_PREFIX = "bli.";
    public static final String FUNCTIONAL_ROLE_SOLR_PREFIX = "frole.";
    public static final String EXTRACTION_RULE_BLI_SOLR_PREFIX = "erb.";
    public static final String EXTRACTION_RULE_SOLR_PREFIX = "er.";
    public static final String DEPARTMENT_SOLR_PREFIX = "dept.";
    public static final String AGGREGATED_SUFFIX = "aggregated";
    public static final String AGG_RULE_PREFIX = "agg_rule_";

    // Order coupled with AclItem.Type definition: READ_TYPE, WRITE_TYPE, DENY_READ_TYPE, DENY_WRITE_TYPE
    private static final CatFileFieldType[] aclFieldNames = new CatFileFieldType[AclType.values().length];
    private static Map<String, DtoFields> lowerCaseDtoFieldMap;

    private final static Pattern commaSeparatorPattern = Pattern.compile(",");
    private static Map<String, DtoFields> alternativeNameDtoFieldMap;
    private static Map<CatFileFieldType, String> catFileFieldToAlternativeMap;
    private static Map<String, CatFileFieldType> catFileNameMap;

    static {
        catFileNameMap = Maps.newHashMap();
        for (CatFileFieldType catFileFieldType : CatFileFieldType.values()) {
            catFileNameMap.put(catFileFieldType.getSolrName(), catFileFieldType);
        }

        alternativeNameDtoFieldMap = new HashMap<>();
        for (DtoFields dtoField : DtoFields.values()) {
            String[] sp = commaSeparatorPattern.split(dtoField.getAlternativeName().toLowerCase());
            Stream.of(sp).forEach(name -> alternativeNameDtoFieldMap.put(name, dtoField));
        }

        catFileFieldToAlternativeMap = new HashMap<CatFileFieldType, String>();
        for (DtoFields dtoField : DtoFields.values()) {
            String[] sp = commaSeparatorPattern.split(dtoField.getAlternativeName());
            catFileFieldToAlternativeMap.put(dtoField.getCatFileFieldType(), sp[0]);
        }

        aclFieldNames[AclType.READ_TYPE.ordinal()] = ACL_READ;
        aclFieldNames[AclType.WRITE_TYPE.ordinal()] = ACL_WRITE;
        aclFieldNames[AclType.DENY_READ_TYPE.ordinal()] = ACL_DENIED_READ;
        aclFieldNames[AclType.DENY_WRITE_TYPE.ordinal()] = ACL_DENIED_WRITE;

        lowerCaseDtoFieldMap = new HashMap<>();
        for (DtoFields dtoField : DtoFields.values()) {
            lowerCaseDtoFieldMap.put(dtoField.name().toLowerCase(), dtoField);
        }
    }


    public enum DtoFields {
        id(CatFileFieldType.ID, "file.id"),
        fileName("baseName", BASE_NAME, "file.fileName"),
        extension("extension", EXTENSION, "file.extension"),
        type(TYPE, "file.type"),
        itemType(ITEM_TYPE, "file.itemType"),
        baseName(BASE_NAME, "file.baseName"),
        fullPath(FULLPATH, CatFoldersFieldType.PATH, "file.fullPath,folder.path"),
        fullName(FULL_NAME_SEARCH, CatFoldersFieldType.NAME, "file.fullName"),
        folderName(FOLDER_NAME,"file.folderName"),
        fsFileSize("fsFileSize", SIZE, "file.fsFileSize"),
        fsLastModified("fsLastModified", true, LAST_MODIFIED, "file.fsLastModified"),
        fsLastAccess("fsLastAccess", true, LAST_ACCESS, "file.fsLastAccess"),
        analysisGroupId(ANALYSIS_GROUP_ID, "analysisGroup.id"),
        groupId(USER_GROUP_ID, "group.id"),                     // group ID should be always similar to the userGroupId, unless the user chose to join groups manually
        userGroupId(USER_GROUP_ID, "userGroup.id"),             // for all practical purposes we should query userGroupId field, thus both groupId and userGroupId are mapped to it
        groupName(GROUP_NAME, "group.name,group.groupName"),
        folderId(FOLDER_ID, CatFoldersFieldType.ID, "folder.id"),
        subFolderId(FOLDER_IDS, "folder.subfolders"),
        ownerId(OWNER, "file.ownerId,owner.user"),
        patternId(MATCHED_PATTERNS_SINGLE, "file.patternId"),
        patternName(MATCHED_PATTERNS_SINGLE_NAME, "file.patternName"),
        patternMultiName(MATCHED_PATTERNS_SINGLE_NAME, "file.patternManyName"),
        fileTypeCategoryName(FILE_TYPE_CATEGORY_NAME, "file.fileTypeCategoryName"),
        patternMultiId(MATCHED_PATTERNS_MULTI, "file.patternMultiId"),
        regulationId(REGULATION_ID, "regulationId"),
        regulationName(REGULATION_NAME, "regulationName"),
        extensionId(EXTENSION, "file.extension"),
        fileTypeCategoryId(FILE_TYPE_CATEGORY_ID, "fileTypeCategoryId"),
        subTreeId(WORKFLOW_SUBTREE_ID, "subTree.id"),
        aclReadId(ACL_READ, "file.aclReadId,aclRead.user"),
        aclWriteId(ACL_WRITE, "file.aclWriteId,aclWrite.user"),
        size(SIZE, "file.size"),
        sizePartition(SIZE_PARTITION, "file.size-partition"),
        tags(TAGS, "file.tags"),
        tagName(TAG_NAME, "file.tagName"),
        workflowData(WORKFLOW_DATA, "workflowData"),
        exactScopedTags(CatFileFieldType.SCOPED_TAGS, "file.scopedTags"),
        deleted(CatFileFieldType.DELETED, "file.deleted"),
        tagType(TAG_TYPE, "file.tagType"),
        tagTypeName(TAG_TYPE_NAME, "file.tagTypeName"),
        tagTypeMismatch(TAG_TYPE_MISMATCH, "file.tagTypeMismatch"),
        docTypeId(DOC_TYPES, "file.docType"),
        subDocTypeId(DOC_TYPE_PARENTS, "file.subDocType"),
        subDocTypeName(DOC_TYPE_PARENTS_NAME, "file.subDocTypeName"),
        extractedText(EXTRACTED_TEXT_IDS, "extractedText.id"),
        extractedEntities(EXTRACTED_ENTITIES_IDS, "extractedEntities.id"),
        bizList(BIZLIST, "file.bizList"),
        extractionRule(EXTRACTION_RULE, "file.extractionRule"),
        bizListItemExtractionId(BIZLIST_ITEM_EXTRACTION, "file.bizListItemExtraction"),
        bizListItemExtractionType(BIZLIST_TYPE, "bizListType,file.bizListItemExtractionType"),
        bizListItemAssociationId(BIZLIST_ITEM_ASSOCIATION, "file.bizListItemAssociation"),
        compoundPolicyAssociationId(COMPOUND_POLICY_ASSOCIATION, "file.compoundPolicyAssociation"),
        bizListItemAssociationType(BIZLIST_ITEM_ASSOCIATION_TYPE, "file.bizListItemAssociationType"),
        rootFolderId(ROOT_FOLDER_ID, CatFoldersFieldType.ROOT_FOLDER_ID, "file.rootFolderId"),
        contentId(CONTENT_ID, "file.contentId"),
        creationDate("creationDate", true, CREATION_DATE, "file.creationDate"),
        appCreationDate("appCreationDate", true, APP_CREATION_DATE, "file.appCreationDate"),
        analyzeHint(ANALYSIS_HINT, "file.analyzeHint"),
        lastModifiedDate("lastModfiedDate", true, LAST_MODIFIED, "file.lastModifiedDate"),
        functionalRoleId(FUNCTIONAL_ROLE_ASSOCIATION, "file.functionalRoleAssociation"),
        functionalRoleName(FUNCTIONAL_ROLE_ASSOCIATION_NAME, "file.functionalRoleAssociationName"),
        rootFolderNameLike(ROOT_FOLDER_NAME_LIKE, "rootFolderNameLike"),
        senderAddress(SENDER_ADDRESS, "file.senderAddress"),
        recipientAddress(RECIPIENTS_ADDRESSES, "file.recipientAddress"),
        senderDomain(SENDER_DOMAIN, "file.senderDomain"),
        recipientDomain(RECIPIENTS_DOMAINS, "file.recipientDomain"),
        noneMail(NO_MAIL_RELATED, "file.noneMail"),
        emailParts(EMAIL_PARTS, "file.email"),
        fileId(FILE_ID,"fileId"),
        itemSubject(ITEM_SUBJECT, "file.itemSubject"),
        departmentId(DEPARTMENT_ID, "file.department"),
        subDepartmentId(DEPARTMENT_PARENTS, "file.subDepartment"),
        subDepartmentName(DEPARTMENT_PARENTS_NAME, "file.subDepartmentName"),
        openAccessRights(OPEN_ACCESS_RIGHTS, "openAccessRights"),
        daLabels(DA_LABELS, "file.daLabels"),
        externalMetadata(EXTERNAL_METADATA, "file.externalMetadata"),
        generalMetadata(GENERAL_METADATA, "file.generalMetadata"),
        activeAssociations(ACTIVE_ASSOCIATIONS, "file.activeAssociations"),
        sharedPermission(SHARED_PERMISSION, "sharedPermission"),
        daMetadata(DA_METADATA, "file.metadata"),
        daMetadataType(DA_METADATA_TYPE, "file.metadataType"),
        expiredUser(EXPIRED_USER, "file.expiredUser"),
        tagTypeAssociation(TAG_TYPE_ASSOCIATION, "file.tagTypeAssociation"),
        mediaType(MEDIA_TYPE, "file.mediaType"),
        unGrouped(UN_GROUPED, "file.unGrouped"),
        genericState(GENERIC_STATE, "file.genericState"),
        specificState(SPECIFIC_STATE, "file.specificState"),
        initialScanDate(INITIAL_SCAN_DATE, "file.initialScanDate"),
        fileTagTypeAssociation(FILE_TAG_TYPE_ASSOC, "file.explicitTagTypeAssociation"),
        fileTagAssociation(FILE_TAG_ASSOC, "file.explicitTagAssociation"),
        ;

        String modelField;

        private boolean dateField;
        private CatFileFieldType catFileFieldType;
        private CatFoldersFieldType catFoldersFieldType;
        private String alternativeName;

        DtoFields(String modelField, CatFileFieldType catFileFieldType, String alternativeName) {
            this.dateField = false;
            this.modelField = modelField;
            this.catFileFieldType = catFileFieldType;
            this.alternativeName = alternativeName;
        }

        DtoFields(String modelField, CatFileFieldType catFileFieldType, CatFoldersFieldType catFoldersFieldType, String alternativeName) {
            this.dateField = false;
            this.modelField = modelField;
            this.catFileFieldType = catFileFieldType;
            this.alternativeName = alternativeName;
            this.catFoldersFieldType = catFoldersFieldType;
        }

        DtoFields(String modelField, boolean dateField, CatFileFieldType catFileFieldType, String alternativeName) {
            this.dateField = dateField;
            this.modelField = modelField;
            this.catFileFieldType = catFileFieldType;
            this.alternativeName = alternativeName;
        }

        DtoFields(String modelField, boolean dateField, CatFileFieldType catFileFieldType, CatFoldersFieldType catFoldersFieldType, String alternativeName) {
            this.dateField = dateField;
            this.modelField = modelField;
            this.catFileFieldType = catFileFieldType;
            this.alternativeName = alternativeName;
            this.catFoldersFieldType = catFoldersFieldType;
        }

        DtoFields(CatFileFieldType catFileFieldType, String alternativeName) {
            this.catFileFieldType = catFileFieldType;
            this.modelField = this.name();
            this.dateField = false;
            this.alternativeName = alternativeName;
        }

        DtoFields(CatFileFieldType catFileFieldType, CatFoldersFieldType catFoldersFieldType, String alternativeName) {
            this.catFileFieldType = catFileFieldType;
            this.modelField = this.name();
            this.dateField = false;
            this.alternativeName = alternativeName;
            this.catFoldersFieldType = catFoldersFieldType;
        }

        public String getModelField() {
            return modelField;
        }

        public boolean isDateField() {
            return dateField;
        }

        public CatFileFieldType getCatFileFieldType() {
            return catFileFieldType;
        }

        public String getAlternativeName() {
            return alternativeName;
        }

        public CatFoldersFieldType getCatFoldersFieldType() {
            return catFoldersFieldType;
        }
    }

    public static CatFileFieldType convertToCatFileField(String dtoField) {
        if (dtoField.equalsIgnoreCase("contentFilter")) {
            //This is not a real DTO field
            return null;
        }
        DtoFields df = FileDtoFieldConverter.getDtoFieldByName(dtoField);
        if (df == null) {
            throw new BadRequestException("Filter field name", dtoField, BadRequestType.UNSUPPORTED_VALUE);
        }
        return FileDtoFieldConverter.getDtoFieldByName(dtoField).getCatFileFieldType();
    }

    public static CatFoldersFieldType convertToCatFolderField(String dtoField) {
        if (dtoField.equalsIgnoreCase("contentFilter")) {
            //This is not a real DTO field
            return null;
        }
        DtoFields df = FileDtoFieldConverter.getDtoFieldByName(dtoField);
        if (df == null) {
            throw new BadRequestException("Filter field name", dtoField, BadRequestType.UNSUPPORTED_VALUE);
        }
        return FileDtoFieldConverter.getDtoFieldByName(dtoField).getCatFoldersFieldType();
    }

    public static String convertDtoField(String dtoField) {
        return FileDtoFieldConverter.getDtoFieldByName(dtoField).getModelField();
    }

    public static CatFileFieldType concvertDtoFieldToCatFileType(String dtoField) {
        return FileDtoFieldConverter.getDtoFieldByName(dtoField).getCatFileFieldType();
    }

    public static boolean isDateField(String dtoField) {
        return FileDtoFieldConverter.getDtoFieldByName(dtoField).isDateField();

    }

    private static DtoFields getDtoFieldByName(String name) {
        if (name.contains(".")) {
            return alternativeNameDtoFieldMap.get(name.toLowerCase());
        }
        return lowerCaseDtoFieldMap.get(name.toLowerCase());
    }

    public static String getAlternativeNameByFieldName(CatFileFieldType catFileFieldType) {
        return catFileFieldToAlternativeMap.get(catFileFieldType);
    }

    public static String convertAclToSolrField(AclType aclType) {
        return aclFieldNames[aclType.ordinal()].getSolrName();
    }

    public static CatFileFieldType convertAclToCatFileField(AclType aclType) {
        return aclFieldNames[aclType.ordinal()];
    }

    public static CatFileFieldType convertSolrNameToCatFileField(String solrName) {
        return catFileNameMap.get(solrName);
    }

    public static String convertAggBizListItemToSolrValue(BizListItemType bizListItemType, Long bizListId, String ruleName) {
        StringBuilder stringBuilder = new StringBuilder(20);
        //Add the bizList term
        stringBuilder.append(BIZLIST_SOLR_PREFIX).append(bizListItemType.ordinal()).append(".").append(bizListId);
        //Add the biztListItem
        stringBuilder.append(" ");

        stringBuilder.append(BIZLIST_ITEM_SOLR_PREFIX).append(bizListItemType.ordinal()).append(".").append(bizListId)
                .append(".").append(AGG_RULE_PREFIX).append(ruleName);

        stringBuilder.append(" ");
        stringBuilder.append(EXTRACTION_RULE_BLI_SOLR_PREFIX).append(bizListItemType.ordinal()).append(".").append(bizListId)
                .append(".").append(ruleName).append(".").append(AGGREGATED_SUFFIX);

        stringBuilder.append(" ");
        stringBuilder.append(EXTRACTION_RULE_SOLR_PREFIX).append(ruleName);

        return stringBuilder.toString();
    }

    public static String convertBizListItemToSolrValue(boolean addBizListTerm, BizListItemType bizListItemType, Long bizListId, String bizListItemId) {
        return convertBizListItemToSolrValue(addBizListTerm, bizListItemType, bizListId, bizListItemId, null);
    }

    public static String convertBizListItemToSolrValue(boolean addBizListTerm, BizListItemType bizListItemType, Long bizListId, String bizListItemId, String ruleName) {
        StringBuilder stringBuilder = new StringBuilder(20);
        //Add the bizList term
        if (addBizListTerm) {
            stringBuilder.append(BIZLIST_SOLR_PREFIX).append(bizListItemType.ordinal()).append(".").append(bizListId);
            //Add the biztListItem
            stringBuilder.append(" ");
        }
        stringBuilder.append(BIZLIST_ITEM_SOLR_PREFIX).append(bizListItemType.ordinal()).append(".").append(bizListId)
                .append(".").append(bizListItemId);
        if (ruleName != null) {
            stringBuilder.append(" ");
            stringBuilder.append(EXTRACTION_RULE_BLI_SOLR_PREFIX)
                    .append(bizListItemType.ordinal()).append(".")
                    .append(ruleName).append(".")
                    .append(bizListItemId);

            stringBuilder.append(" ");
            stringBuilder.append(EXTRACTION_RULE_SOLR_PREFIX).append(ruleName);

        }
        return stringBuilder.toString();
    }

}
