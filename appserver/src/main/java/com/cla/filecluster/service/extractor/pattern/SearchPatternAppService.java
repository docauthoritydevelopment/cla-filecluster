package com.cla.filecluster.service.extractor.pattern;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.filesearchpatterns.*;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.searchpatterns.PatternCategory;
import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.file.TextSearchPatternImplementationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.PatternCategoryRepository;
import com.cla.filecluster.repository.jpa.searchpattern.RegulationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.util.ControllerUtils;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 23/06/2016.
 */
@Service
public class SearchPatternAppService {

    @Autowired
    private ClaFileSearchPatternService claFileSearchPatternService;

    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;


    @Autowired
    private RegulationRepository regulationRepository;


    private static final Logger logger = LoggerFactory.getLogger(SearchPatternAppService.class);

    @Value("${ingester.searchPatternsFile:./config/extraction/searchpatterns.csv}")
    private String searchPatternsFileName;

    // List of sub-categories to be set to active when created
    @Value("${searchPatterns.activeSubCategories:}")
    private String activeSubCategoriesDefault;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private PatternCategoryRepository patternCategoryRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    public static final String NAME = "NAME";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String ACTIVE = "ACTIVE";
    private static final String PATTERN = "PATTERN";
    private static final String CLASS = "CLASS";
    private static final String CATEGORY = "CATEGORY";
    private static final String SUB_CATEGORY = "SUB_CATEGORY";
    private static final String REGULATIONS = "REGULATIONS";

    private final static String[] requiredHeaders = {NAME, DESCRIPTION, ACTIVE, PATTERN, CLASS, CATEGORY, SUB_CATEGORY, REGULATIONS};

    private Set<String> activeSubCategoriesSet;

    @PostConstruct
    void init() {
        String[] sc = activeSubCategoriesDefault.split(",", 30);
        activeSubCategoriesSet = Stream.of(sc).collect(Collectors.toSet());
        logger.info("Init: activeSubCategoriesSet={}", String.join(",", activeSubCategoriesSet));
    }

    private <T> Page<T> splitAllResultsIntoPage(ArrayList<T> allResults, PageRequest pageRequest, String selectItemId, Function<T, String> idExtractor) {
        int toIndex;
        if (selectItemId != null) {
            int index = 0;
            int goIndex = 0;
            for (T item : allResults) {
                if (idExtractor.apply(item).equals(selectItemId)) {
                    index = goIndex;
                    break;
                }
                goIndex++;
            }
            if (index > 0) {
                //calculate page - to find fromIndex and toIndex
                pageRequest = PageRequest.of((index / pageRequest.getPageSize()), pageRequest.getPageSize());
            }
            int fromIndex = (int) pageRequest.getOffset();
            toIndex = Math.min(allResults.size(), fromIndex + pageRequest.getPageSize());
        } else {
            toIndex = Math.min(allResults.size(), (int) pageRequest.getOffset() + pageRequest.getPageSize());
        }
        if (pageRequest.getOffset() > toIndex) {
            return new PageImpl<>(new ArrayList<>());
        }
        return new PageImpl<>(allResults.subList((int) pageRequest.getOffset(), toIndex), pageRequest, allResults.size());
    }

    @Transactional(readOnly = true)
    public Page<TextSearchPatternDto> listSearchPatterns(DataSourceRequest dataSourceRequest, List<Integer> regulationIds) {
        Page<TextSearchPattern> searchPatterns;
        if (dataSourceRequest.getSelectedItemId() != null) {
            PageRequest oPageRequest = dataSourceRequest.createPageRequestWithSort();
            PageRequest newPageRequest = PageRequest.of(0, Integer.MAX_VALUE, oPageRequest.getSort());
            Page<TextSearchPattern> allSearchPatterns = claFileSearchPatternService.listSearchPatterns(newPageRequest,
                    dataSourceRequest.getNamingSearchTerm(), regulationIds);
            searchPatterns = splitAllResultsIntoPage(new ArrayList<>(allSearchPatterns.getContent()), oPageRequest,
                    dataSourceRequest.getSelectedItemId(), this::getTextSearchPatternId);
        } else {
            searchPatterns = claFileSearchPatternService.listSearchPatterns(dataSourceRequest.createPageRequestWithSort(),
                    dataSourceRequest.getNamingSearchTerm(), regulationIds);
        }
        return ClaFileSearchPatternService.convertTextSearchPatternsToDto(searchPatterns);
    }

    @Transactional
    public void populateDefaultSearchPatterns() {
        populateDefaultSearchPatterns(searchPatternsFileName);
    }

    @Transactional
    public void populateDefaultSearchPatterns(final String importFileName) {
        logger.info("populateDefaultSearchPatterns");
        if (StringUtils.isEmpty(importFileName) || !Files.exists(Paths.get(importFileName))) {
            logger.info("Search patterns file '{}' not defined, or doesn't exist", importFileName);
            return;
        }
        logger.info("Load search patterns file {}", importFileName);

        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, importFileName, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(importFileName,
                    row -> createSearchPatternFromCsvRow(row, headersLocationMap), null, null);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load search patterns from file - problem in csv file " + e.getMessage(), e);
        }
        if (failedRows > 0) {
            throw new RuntimeException("Failed to load " + failedRows + " search patterns in file - problem in csv file");
        }
        claFileSearchPatternService.reloadFileSearchPatternsImplementations(false);
    }

    List<Integer> createRegulationsIfNeeded(String[] regulationNames) {
        List<Integer> regulationIds = new ArrayList<>();
        if (regulationNames != null) {
            Arrays.stream(regulationNames).forEach((String regName) -> {
                Regulation reg = regulationRepository.findByName(regName);
                if (reg == null) {
                    reg = new Regulation();
                    reg.setName(regName);
                    reg.setActive(true);
                    reg = regulationRepository.save(reg);
                }
                regulationIds.add(reg.getId());
            });
        }
        return regulationIds;
    }


    PatternCategory createStubPatternCategoryIfNeeded(String categoryName) {
        PatternCategory pc = patternCategoryRepository.findByNameAndNullParentId(categoryName);
        if (pc == null) {
            pc = new PatternCategory();
            pc.setName(categoryName);
            pc.setActive(true);
            pc = patternCategoryRepository.save(pc);
            logger.debug("Create pattern category {}", pc);
        }
        return pc;
    }

    PatternCategory createStubPatternSubCategoryIfNeeded(String sunCategoryName, PatternCategory pc, boolean activeIfCreated) {
        PatternCategory subPc = patternCategoryRepository.findByNameAndParentId(sunCategoryName, pc.getId());
        if (subPc == null) {
            subPc = new PatternCategory();
            subPc.setName(sunCategoryName);
            subPc.setActive(activeIfCreated);
            subPc.setParent(pc);
            subPc = patternCategoryRepository.save(subPc);
            logger.debug("Create pattern subCategory {}", subPc);
        }
        return subPc;
    }


    private void createSearchPatternFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length == 0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String description = Optional.ofNullable(fieldsValues[headersLocationMap.get(DESCRIPTION)]).orElse("").trim();
        Boolean active = Boolean.valueOf(Optional.ofNullable(fieldsValues[headersLocationMap.get(ACTIVE)]).orElse("FALSE").trim());
        String validatorClass = fieldsValues[headersLocationMap.get(CLASS)];

        String pattern = fieldsValues[headersLocationMap.get(PATTERN)];
        if (pattern != null) {
            pattern = pattern.trim();
        }

        if (validatorClass != null && Strings.isNullOrEmpty(pattern)) {
            validatorClass = validatorClass.trim();
            if (!validatorClass.contains(".")) {
                final String packageName = TextSearchPatternDto.class.getPackage().getName();
                validatorClass = packageName + "." + validatorClass;
            }
            //Load from class and extract pattern
            try {
                TextSearchPatternImpl textSearchPatternImpl = TextSearchPatternImplementationRepository.getTextSearchPatternImpl(validatorClass, 0, name, active, null, true);
                pattern = textSearchPatternImpl.getPattern();

            } catch (Exception e) {
                logger.error("Failed to initialize pattern validator class {}", validatorClass, e);
            }
        }
        String categoryName = fieldsValues[headersLocationMap.get(CATEGORY)];
        String subCategoryName = fieldsValues[headersLocationMap.get(SUB_CATEGORY)];
        String regulationsStr = fieldsValues[headersLocationMap.get(REGULATIONS)];
        String[] regulationNames = regulationsStr != null ? regulationsStr.split(";") : null;
        PatternCategory pc = createStubPatternCategoryIfNeeded(categoryName);
        PatternCategory subPc = createStubPatternSubCategoryIfNeeded(subCategoryName, pc, activeSubCategoriesSet.contains(subCategoryName));
        List<Integer> regulationIds = createRegulationsIfNeeded(regulationNames);
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto(name);
        textSearchPatternDto.setDescription(description);
        textSearchPatternDto.setPattern(pattern);
        textSearchPatternDto.setValidatorClass(validatorClass);
        textSearchPatternDto.setRegulationIds(regulationIds);
        textSearchPatternDto.setCategoryId(pc.getId());
        textSearchPatternDto.setCategoryName(pc.getName());
        textSearchPatternDto.setSubCategoryId(subPc.getId());
        textSearchPatternDto.setSubCategoryName(subPc.getName());
        PatternType patternType = validatorClass != null ? PatternType.PREDEFINED : PatternType.REGEX;
        textSearchPatternDto.setPatternType(patternType);
        textSearchPatternDto.setActive(active);
        textSearchPatternDto.setSubCategoryActive(pc.isActive());
        createSearchPattern(textSearchPatternDto);
    }

    @Transactional
    public TextSearchPatternDto createSearchPattern(TextSearchPatternDto textSearchPatternDto) {
        TextSearchPattern searchPattern = claFileSearchPatternService.createSearchPattern(textSearchPatternDto);
        return ClaFileSearchPatternService.convertTextSearchPatternToDto(searchPattern);
    }

    @Transactional
    public TextSearchPatternDto updateSearchPattern(int id, TextSearchPatternDto textSearchPatternDto) {
        textSearchPatternDto.setId(id);
        TextSearchPattern searchPattern = claFileSearchPatternService.updateSearchPattern(textSearchPatternDto);
        return ClaFileSearchPatternService.convertTextSearchPatternToDto(searchPattern);
    }

    @Transactional
    public TextSearchPatternDto updateSearchPatternState(int id, boolean state) {
        TextSearchPattern searchPattern = claFileSearchPatternService.updateSearchPatternState(id, state);
        return ClaFileSearchPatternService.convertTextSearchPatternToDto(searchPattern);
    }

    @Transactional
    public void deleteSearchPattern(int id) {
        claFileSearchPatternService.deleteSearchPattern(id);
    }

    @Transactional
    public TextSearchPatternDto getPattern(int id) {
        TextSearchPattern textSearchPattern = textSearchPatternRepository.getOne(id);
        return ClaFileSearchPatternService.convertTextSearchPatternToDto(textSearchPattern);
    }

    @Transactional
    TextSearchPattern getPatternByName(String name) {
        return textSearchPatternRepository.findByName(name);
    }

    private String getTextSearchPatternDtoId(TextSearchPatternDto item) {
        return String.valueOf(item.getId());
    }

    private String getTextSearchPatternId(TextSearchPattern item) {
        return String.valueOf(item.getId());
    }

    private SolrQueryResponse getFileCountResponse(DataSourceRequest dataSourceRequest, CatFileFieldType facetType, List<String> neededIds) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.MATCHED_PATTERNS_SINGLE.inQuery(neededIds));
        solrFacetSpecification.setMinCount(1);
        return fileCatService.runQuery(solrFacetSpecification, null);
    }


    @SuppressWarnings("unchecked")
    public FacetPage<AggregationCountItemDTO<TextSearchPatternDto>> getTextSearchPatternFileCounts(DataSourceRequest dataSourceRequest, boolean inOnlyMultiMode, boolean withoutValidation) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();

        CatFileFieldType facetType = inOnlyMultiMode ? CatFileFieldType.MATCHED_PATTERNS_MULTI : CatFileFieldType.MATCHED_PATTERNS_SINGLE;
        List<TextSearchPattern> patterns = textSearchPatternRepository.findAll();
        List<String> neededIds = new ArrayList<>();
        HashMap<String, TextSearchPattern> searchPatternByIdMap = new HashMap<>();
        for (TextSearchPattern textSearchPattern : patterns) {
            if (withoutValidation || (textSearchPattern.isActive() && textSearchPattern.getSubCategories().iterator().next().isActive())) {
                searchPatternByIdMap.put(String.valueOf(textSearchPattern.getId()), textSearchPattern);
                neededIds.add(String.valueOf(textSearchPattern.getId()));
            }
        }
        if (neededIds.size() == 0) {
            return new FacetPage<>(new ArrayList<AggregationCountItemDTO<TextSearchPatternDto>>(), dataSourceRequest.createPageRequest());
        }
        SolrQueryResponse solrFacetQueryResponse = getFileCountResponse(dataSourceRequest, facetType, neededIds);
        FacetField fieldResult = solrFacetQueryResponse.getQueryResponse().getFacetField(facetType.getSolrName());

        Map<String, FacetField.Count> fieldResultMapNoFilter = null;
        if (dataSourceRequest.hasFilter() && fieldResult.getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            solrFacetQueryResponse = getFileCountResponse(dataSourceRequest, facetType, SolrSpecificationFactory.getValuesFromFacet(fieldResult));
            FacetField fieldResultNoFilter = solrFacetQueryResponse.getQueryResponse().getFacetField(facetType.getSolrName());
            fieldResultMapNoFilter = fieldResultNoFilter.getValues().stream()
                    .distinct()
                    .collect(Collectors.toMap(FacetField.Count::getName, r -> r));
        }

        List<AggregationCountItemDTO<TextSearchPatternDto>> patternCountList = new ArrayList<>();
        if (fieldResult != null) {
            for (FacetField.Count countObj : fieldResult.getValues()) {
                if (searchPatternByIdMap.containsKey(countObj.getName())) {
                    TextSearchPatternDto textSearchPatternDto =
                            ClaFileSearchPatternService.convertTextSearchPatternToDto(searchPatternByIdMap.get(countObj.getName()));
                    AggregationCountItemDTO<TextSearchPatternDto> item =
                            new AggregationCountItemDTO(textSearchPatternDto, countObj.getCount());
                    patternCountList.add(item);
                    if (fieldResultMapNoFilter != null && fieldResultMapNoFilter.get(countObj.getName()) != null) {
                        item.setCount2(fieldResultMapNoFilter.get(countObj.getName()).getCount());
                    }
                }
            }
        }
        return ControllerUtils.
                getRelevantResultPage(patternCountList, pageRequest,
                        dataSourceRequest.getSelectedItemId(), this::getTextSearchPatternDtoId);
    }


}
