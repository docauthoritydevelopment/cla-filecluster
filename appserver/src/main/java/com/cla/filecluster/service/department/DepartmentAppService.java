package com.cla.filecluster.service.department;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.department.DepartmentAssociationDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.writer.map.MapBatchResultsProcessor;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.TagRelatedBaseService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
@Service
public class DepartmentAppService extends TagRelatedBaseService {

    private Logger logger = LoggerFactory.getLogger(DepartmentAppService.class);

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private MapBatchResultsProcessor mapBatchResultsProcessor;

    public void deleteDepartments(Long departmentId) {
        Department department = departmentService.getDepartmentById(departmentId);

        departmentService.validateDepartmentNotExist(department);

        Optional<String> errCode = isDepartmentOrSubDepartmentAssociated(department);

        if (errCode.isPresent()) {
            throw new BadParameterException(messageHandler.getMessage(errCode.get()), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }

        departmentService.deleteDepartments(department);
    }

    private Optional<String> isDepartmentOrSubDepartmentAssociated(Department department) {

        if (docStoreService.countUnDeletedDepartmentAssociations(department.getId()) > 0) {
            return Optional.of("department.delete.has-root-folders");
        }

        if (associationsService.countUnDeletedDepartmentAssociations(department) > 0) {
            return Optional.of("department.delete.has-associations");
        }

        List<Department> subDepartments = departmentService.getSubDepartments(department.getId());
        for (Department subDepartment : subDepartments) {
            if (isDepartmentOrSubDepartmentAssociated(subDepartment).isPresent()) {
                return Optional.of("department.delete.has-sub-associations");
            }
        }

        return Optional.empty();
    }


    public DocFolderDto removeDepartmentAssociationFromFolder(long departmentId, long docFolderId) {
        DepartmentDto department = departmentService.getByIdCached(departmentId);
        long startOperation = System.currentTimeMillis();
        Set<Pair<Long, Long>> pendingBatchTaskIds = mapBatchResultsProcessor.getPendingBatchTaskIds();
        associationsService.markFolderAssociationAsDeleted(docFolderId, department, null, AssociableEntityType.DEPARTMENT);
        DocFolderDto docFolderDto = docFolderService.getByIdWithAssociations(docFolderId);
        handleRootFolderChangeIfNeeded(docFolderDto, null);

        // if has department from a parent folder - reassign it to files in solr
        if (docFolderDto.getDepartmentDto() != null) {
            DocFolder folder = docFolderService.getById(docFolderId);
            AssociationEntity result = null;
            if (folder.getAssociations() != null) {
                List<AssociationEntity> assocEntities = AssociationsService.convertFromAssociationEntity(docFolderId, folder.getAssociations(), AssociationType.FOLDER);
                for (AssociationEntity assoc : assocEntities) {
                    if (assoc.getDepartmentId() != null) {
                        if (result == null || result.getOriginDepthFromRoot() < assoc.getOriginDepthFromRoot()) {
                            result = assoc;
                        }
                    }
                }
            }
            if (result != null) {
                associationsService.createFolderApplyOperation(docFolderId, result, SolrFieldOp.ADD, startOperation, docFolderDto.getRootFolderId(), AssociableEntityType.DEPARTMENT, pendingBatchTaskIds);
            }
        }
        return docFolderDto;
    }

    @Transactional
    public Pair<DocFolderDto, DepartmentDto> associateFolderWithDepartment(String departmentFullName, String folderPath) {
        Long departmentId = departmentService.parseDepartmentFromPath(departmentFullName);
        if (departmentId == null) {
            throw new BadParameterException(messageHandler.getMessage("department.name.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        Long folderId = docFolderService.locateFolderByFullPath(folderPath);

        DocFolderDto folderDto = associateFolderWithDepartment(departmentId, folderId);
        return Pair.of(folderDto, departmentService.getByIdCached(departmentId));
    }

    @Transactional
    public DocFolderDto associateFolderWithDepartment(long departmentId, long docFolderId) {
        DepartmentDto department = departmentService.getByIdCached(departmentId);
        logger.debug("Associate Folder id {} with department {}", docFolderId, department.getId());
        DocFolderDto docFolderDto = docFolderService.getDtoById(docFolderId);
        docFolderDto = assignDepartmentToDocFolder(department, docFolderDto);
        return docFolderDto;
    }

    private DocFolderDto assignDepartmentToDocFolder(DepartmentDto department, DocFolderDto docFolderDto) {
        List<DepartmentAssociationDto> allFolderAssociations = docFolderDto.getDepartmentAssociationDtos();
        Map<Long, DepartmentAssociationDto> deptAssociations = AssociationUtils.getDepartmentUndeletedExplicitAssociations(allFolderAssociations);

        final boolean sameExplicitDeptAlreadyExists = deptAssociations.containsKey(department.getId());

        if (sameExplicitDeptAlreadyExists) { // In this case we issue a warning and return the existing doc folder
            logger.warn("Department association {} already exists, ignoring request", department);
        } else {
            docFolderService.addExplicitAssociableEntityToFolder(docFolderDto.getId(), department, AssociableEntityType.DEPARTMENT);
            handleRootFolderChangeIfNeeded(docFolderDto, department.getId());
            if (!deptAssociations.isEmpty()) {
                deptAssociations.values().forEach(assoc -> {
                    associationsService.markFolderAssociationAsDeleted(docFolderDto.getId(), assoc.getDepartmentDto(), null, AssociableEntityType.DEPARTMENT);
                });
            }
        }

        // After receiving the correct doc folder dto, refine its associations (by scope & priority)
        final DocFolderDto docFolderDtoToReturn = docFolderService.getByIdWithAssociations(docFolderDto.getId());
        refineAssociations(docFolderDtoToReturn, null);
        return docFolderDtoToReturn;
    }

    private void handleRootFolderChangeIfNeeded(DocFolderDto docFolderDto, Long departmentId) {
        if (docFolderDto.getDepthFromRoot() != null && docFolderDto.getDepthFromRoot() == 0) {
            docStoreService.updateRootFolderDepartment(docFolderDto.getRootFolderId(), departmentId);
        }
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<DepartmentDto>> findDepartmentFileCount(DataSourceRequest dataSourceRequest, boolean returnAllDepartments) {
        logger.debug("list DocType Counts With File Filters");
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        SolrFacetQueryResponse solrFacetQueryResponse = getDepartmentFileCountResult(dataSourceRequest);
        SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
        if (dataSourceRequest.hasFilter()) {
            dataSourceRequest.clearFilters();
            solrFacetQueryRespNoFilter = getDepartmentFileCountResult(dataSourceRequest);
        }
        logger.debug("list file facets by Solr field tags returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        if (returnAllDepartments) {
            return fetchAllDepartmentsAndFillCountData(solrFacetQueryResponse, pageRequest, solrFacetQueryRespNoFilter);
        } else {
            return convertFacetFieldResultToDto(solrFacetQueryResponse, pageRequest,
                    dataSourceRequest.getSelectedItemId(), solrFacetQueryRespNoFilter);
        }
    }

    private SolrFacetQueryResponse getDepartmentFileCountResult(DataSourceRequest dataSourceRequest) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.ACTIVE_ASSOCIATIONS);
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix(DepartmentDto.ID_PREFIX);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrFacetSpecification,
                PageRequest.of(0, ControllerUtils.FACET_MAX_PAGE_SIZE_FILECOUNT));
        return solrFacetQueryResponse;
    }

    private FacetPage<AggregationCountItemDTO<DepartmentDto>> fetchAllDepartmentsAndFillCountData(
            SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, SolrFacetQueryResponse solrFacetQueryRespNoFilter) {
        logger.debug("fetch all docTypes - and fill in counts for the relevant docTypes");
        List<Department> departmentsAll = departmentService.findAll();
        Map<Long, Department> departments = departmentsAll.stream().filter(d -> !d.isDeleted()).collect(Collectors.toMap(
                Department::getId, r -> r, (v1, v2) -> v1, LinkedHashMap::new));
        List<AggregationCountItemDTO<DepartmentDto>> content = new ArrayList<>();
        //Content map is used to update count2 (with children)
        Map<Long, AggregationCountItemDTO<DepartmentDto>> contentMap = new HashMap<>();
        long pageAggCount = 0;

        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        Map<Long, List<FacetField.Count>> facetValuesMap = facetField.getValues().stream()
                .collect(Collectors.groupingBy(v -> AssociationIdUtils.extractDepartmentIdFromSolrIdentifier(v.getName())));

        Map<Long, List<FacetField.Count>> facetValuesMapNoFilter = new HashMap<>();
        if (solrFacetQueryRespNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryRespNoFilter.getFirstResult();
            facetValuesMapNoFilter = facetFieldNoFilter.getValues().stream()
                    .collect(Collectors.groupingBy(v -> AssociationIdUtils.extractDepartmentIdFromSolrIdentifier(v.getName())));
        }

        for (Department department : departments.values()) {
            Long itemCount = 0L;
            if (facetValuesMap.containsKey(department.getId())) {
                List<FacetField.Count> counts = facetValuesMap.get(department.getId());
                itemCount += counts.stream().mapToLong(FacetField.Count::getCount).sum();
            }

            Long itemCount2 = 0L;
            if (facetValuesMapNoFilter.containsKey(department.getId())) {
                List<FacetField.Count> counts = facetValuesMapNoFilter.get(department.getId());
                itemCount2 = counts.stream().mapToLong(FacetField.Count::getCount).sum();
            }

            DepartmentDto departmentDto = departmentService.convertDepartmentToDto(department);
            fillParentIds(departmentDto, departments);
            AggregationCountItemDTO<DepartmentDto> item = contentMap.get(departmentDto.getId());
            if (item == null) {
                item = createItem(departmentDto, itemCount, solrFacetQueryRespNoFilter != null);
                content.add(item);
                contentMap.put(departmentDto.getId(), item);
            } else {
                item.incrementCount(itemCount);
            }
            item.incrementCount2(itemCount2);
            pageAggCount += itemCount;
        }

        fillDepartmentHierarchicalCounters(content, contentMap);
        return new FacetPage<>(content, pageRequest, pageAggCount, solrFacetQueryResponse.getNumFound());
    }

    private AggregationCountItemDTO<DepartmentDto> createItem(DepartmentDto departmentDto, long itemCount, boolean dataFiltered) {
        return dataFiltered ? new AggregationCountItemDTO<>(departmentDto, itemCount, 0, 0, 0)
               : new AggregationCountItemDTO<>(departmentDto, itemCount);
    }

    private String getItemId(DepartmentDto item) {
        return String.valueOf(item.getId());
    }

    private FacetPage<AggregationCountItemDTO<DepartmentDto>> convertFacetFieldResultToDto(
            SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, String selectedItemId,
            SolrFacetQueryResponse solrFacetQueryRespNoFilter) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        List<AggregationCountItemDTO<DepartmentDto>> content = Lists.newArrayList();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        long pageAggCount = 0;
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set
        List<Pair<Long, List<FacetField.Count>>> groupedDepartments = extractGroupedDepartments(facetField);
        List<Long> departmentIds = groupedDepartments.stream().map(Pair::getKey).collect(Collectors.toList());
        List<Department> departments = departmentService.findByIds(departmentIds);
        Map<Long, Department> departmentsMap = departments.stream().filter(d -> !d.isDeleted()).collect(Collectors.toMap(Department::getId, r -> r));

        Map<Long, List<FacetField.Count>> groupedDepartmentsNoFilter = new HashMap<>();
        if (solrFacetQueryRespNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryRespNoFilter.getFirstResult();
            List<Pair<Long, List<FacetField.Count>>> groupedDepartmentsNoFilterList = extractGroupedDepartments(facetFieldNoFilter);
            groupedDepartmentsNoFilterList.forEach(p -> groupedDepartmentsNoFilter.put(p.getKey(), p.getValue()));
        }

        logger.debug("convert Facet Field result for {} elements to DepartmentDto. DB returned {} elements", departmentIds.size(), departments.size());
        for (Pair<Long, List<FacetField.Count>> pair : groupedDepartments) {
            Long departmentId = pair.getKey();
            Department department = departmentsMap.get(departmentId);
            if (department == null) {
                logger.warn("Failed to find department by id {} in the database. Solr and Database are not synced.", departmentId);
                continue;
            }

            long count = pair.getValue().stream().mapToLong(FacetField.Count::getCount).sum();
            DepartmentDto departmentDto = departmentService.convertDepartmentToDto(department);
            AggregationCountItemDTO<DepartmentDto> item = createItem(departmentDto, count, solrFacetQueryRespNoFilter != null);

            if (groupedDepartmentsNoFilter.containsKey(departmentId)) {
                long count2 = groupedDepartmentsNoFilter.get(departmentId).stream().mapToLong(FacetField.Count::getCount).sum();
                item.setCount2(count2);
            }

            content.add(item);
            pageAggCount += count;
        }
        // even though we must go over all facet options to get correct values
        // we should return to ui only what was requested
        return ControllerUtils.getRelevantResultPage(
                content, pageRequest, selectedItemId, this::getItemId, pageAggCount,
                solrFacetQueryResponse.getNumFound());
    }

    private List<Pair<Long, List<FacetField.Count>>> extractGroupedDepartments(FacetField facetField) {
        List<Pair<Long, List<FacetField.Count>>> result = new ArrayList<>(facetField.getValues().size());
        List<FacetField.Count> values = facetField.getValues();
        Map<Long, Integer> nameByIndex = new HashMap<>();
        for (int i = 0; i < values.size(); i++) {
            Long departmentId = AssociationIdUtils.extractDepartmentIdFromSolrIdentifier(values.get(i).getName());
            Integer index = nameByIndex.get(departmentId);
            if (index == null) {
                List<FacetField.Count> counts = new LinkedList<>();
                result.add(Pair.of(departmentId, counts));
                index = result.size() - 1;
                nameByIndex.put(departmentId, index);
            }
            result.get(index).getValue().add(values.get(i));
        }
        return result;
    }

    private void fillParentIds(DepartmentDto departmentDto, Map<Long, Department> departments) {
        List<Long> parentIds = new ArrayList<>();
        Department current = departments.get(departmentDto.getId());
        parentIds.add(departmentDto.getId());
        while (current != null && current.getParent() != null) {
            Long parentId = current.getParent().getId();
            parentIds.add(parentId);
            current = departments.get(parentId);
        }
        departmentDto.setParents(parentIds);
    }

    private void fillDepartmentHierarchicalCounters(List<AggregationCountItemDTO<DepartmentDto>> content,
                                                    Map<Long, AggregationCountItemDTO<DepartmentDto>> contentMap) {
        logger.trace("update tree child sum counters");
        for (AggregationCountItemDTO<DepartmentDto> deptDtoAggregationCountItemDTO : content) {
            long directCount = deptDtoAggregationCountItemDTO.getCount();
            long unfilteredDirect = deptDtoAggregationCountItemDTO.getCount2();
            DepartmentDto item = deptDtoAggregationCountItemDTO.getItem();

            //DepartmentDto all parents
            if (item.getParents() != null) {
                for (Long parentId : item.getParents()) {
                    AggregationCountItemDTO<DepartmentDto> parent = contentMap.get(parentId);
                    if (parent != null) {
                        parent.incrementSubtreeCount(directCount);
                        if (unfilteredDirect > 0) {
                            parent.incrementSubtreeCountUnfiltered(unfilteredDirect);
                        }
                    } else {
                        logger.warn("cant find parent object {} for item {}", parentId, item.getId());
                    }
                }
            }
        }
    }

    public void handleDepartmentAssociationForRootFolder(Long rootFolderId) {
        try {
            RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
            if (rootFolder == null) {
                logger.info("cant find root folder {} probably marked deleted", rootFolderId);
                return;
            }
            DocFolder rfFolder = docFolderService.getFolderOfRootFolder(rootFolder.getId());
            if (rfFolder != null) { // found doc folder of root folder
                List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(
                        rfFolder.getId(), rfFolder.getAssociations(), AssociationType.FOLDER);
                Map<Long, AssociationEntity> deptAssociations = AssociationUtils.getDepartmentUnDeletedExplicitAssociations(associations);
                if (rootFolder.getDepartmentId() != null) { // assign department
                    if (deptAssociations.get(rootFolder.getDepartmentId()) != null) { // has existing association
                        deptAssociations.remove(rootFolder.getDepartmentId());
                        logger.debug("folder {} of root folder {} already associated to department {}",
                                rfFolder.getId(), rootFolder.getId(), rootFolder.getDepartmentId());
                    } else { // create new association
                        DepartmentDto department = departmentService.getByIdCached(rootFolder.getDepartmentId());
                        docFolderService.addExplicitAssociableEntityToFolder(rfFolder.getId(), department, AssociableEntityType.DEPARTMENT);
                        logger.debug("folder {} of root folder {} created association to department {}",
                                rfFolder.getId(), rootFolder.getId(), rootFolder.getDepartmentId());
                    }
                } else { // remove department assignment
                    if (deptAssociations != null) {
                        List<Long> idsHandled = new ArrayList<>();
                        deptAssociations.values().forEach(current -> {
                            List<AssociableEntityDto> entity = associationsService.getEntitiesForAssociations(Lists.newArrayList(current));
                            associationsService.markFolderAssociationAsDeleted(rfFolder.getId(), entity.get(0), current.getAssociationScopeId(), AssociableEntityType.DEPARTMENT);
                            idsHandled.add(current.getDepartmentId());
                            logger.debug("folder {} of root folder {} remove association to department {}",
                                    rfFolder.getId(), rootFolder.getId(), current.getDepartmentId());
                        });
                        idsHandled.forEach(id -> deptAssociations.remove(id));
                    }
                }

                for (AssociationEntity association : deptAssociations.values()) { // there should only be one association for department to the top level
                    // delete associations
                    logger.debug("folder {} of root folder {} delete association to department {}",
                            rfFolder.getId(), rootFolder.getId(), association.getDepartmentId());
                    List<AssociableEntityDto> entity = associationsService.getEntitiesForAssociations(Lists.newArrayList(association));
                    associationsService.markFolderAssociationAsDeleted(rfFolder.getId(), entity.get(0), association.getAssociationScopeId(), AssociableEntityType.DEPARTMENT);
                }
            } else {
                logger.info("currently no doc folder for root folder {}, cant handle dirty department", rootFolder.getId());
            }
        } catch (Exception e) {
            logger.error("failed to handle dirty root folder {}", rootFolderId, e);
        }
    }
}
