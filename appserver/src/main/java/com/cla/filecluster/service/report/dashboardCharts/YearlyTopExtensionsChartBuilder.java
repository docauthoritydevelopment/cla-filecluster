package com.cla.filecluster.service.report.dashboardCharts;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.service.report.DashboardChartDtoBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class YearlyTopExtensionsChartBuilder implements DashboardChartDtoBuilder {

    @Override
    public ChartType getType() {
        return ChartType.YEARLY_TOP_EXTENSIONS;
    }

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;


    public String getAuditMsg(Map<String, String> params) {
        return "View yearly top extensions chart";
    }


    private DashboardChartDataDto parseSolrResponseToDto(SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse
                .getResponseBucket(CatFileFieldType.EXTENSION.getSolrName()).getBuckets();
        ArrayList<DashboardChartSeriesDataDto> seriesList = new ArrayList<>();
        ArrayList<String> categories = new ArrayList<>();
        boolean alreadyFillCategories = false;
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
            String extension = bucket.getStringVal();
            seriesObj.setName(extension);
            seriesObj.setData(new ArrayList<>());
            SolrFacetJsonResponseBucket bucketBasedFacets = bucket.getBucketBasedFacets(CatFileFieldType.CREATION_DATE.getSolrName());
            List<SolrFacetJsonResponseBucket> innerBuckets = bucketBasedFacets.getBuckets();
            for (SolrFacetJsonResponseBucket innerBucket : innerBuckets) {
                Number statFacetValue = innerBucket.getNumberValueByKey(CatFileFieldType.SIZE.getSolrName());
                Double innerSize = statFacetValue == null ? 0d : statFacetValue.doubleValue();
                seriesObj.getData().add(innerSize);
                if (!alreadyFillCategories) {
                    Date rangeDate = innerBucket.getDateVal();
                    categories.add(yearFormatter.format(rangeDate));
                }
            }
            seriesList.add(seriesObj);
            alreadyFillCategories = true;
        }
        return DashboardChartDataDto.builder()
                .categories(categories)
                .series(seriesList)
                .build();
    }

    private SolrFacetQueryJson buildYearlyToExtensionJsonFacet() {
        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(CatFileFieldType.EXTENSION.getSolrName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(CatFileFieldType.EXTENSION);

        SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(CatFileFieldType.SIZE.getSolrName());
        innerFacet.setType(FacetType.SIMPLE);
        innerFacet.setField(CatFileFieldType.SIZE);
        innerFacet.setFunc(FacetFunction.SUM);
        theFacet.setSortField(innerFacet.getName());
        theFacet.addFacet(innerFacet);

        SolrFacetQueryJson innerRangeFacet = new SolrFacetQueryJson(CatFileFieldType.CREATION_DATE.getSolrName());
        innerRangeFacet.setType(FacetType.RANGE);
        innerRangeFacet.setField(CatFileFieldType.CREATION_DATE);
        innerRangeFacet.setStart("NOW/YEAR-10YEAR");
        innerRangeFacet.setEnd("NOW");
        innerRangeFacet.setGap("+1YEAR");

        SolrFacetQueryJson innerSizeFacet = new SolrFacetQueryJson(CatFileFieldType.SIZE.getSolrName());
        innerSizeFacet.setType(FacetType.SIMPLE);
        innerSizeFacet.setField(CatFileFieldType.SIZE);
        innerSizeFacet.setFunc(FacetFunction.SUM);
        innerRangeFacet.addFacet(innerSizeFacet);

        theFacet.addFacet(innerRangeFacet);
        return theFacet;
    }

    public DashboardChartDataDto buildChartDataDto(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                dataSourceRequest);
        solrFacetSpecification.addFacetJsonObject(buildYearlyToExtensionJsonFacet());
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = solrFileCatRepository.runFacetQueryJson(
                solrFacetSpecification);
        return parseSolrResponseToDto(solrFacetQueryJsonResponse);
    }
}
