package com.cla.filecluster.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by uri on 02/11/2016.
 */
@Service
public class GlobalPropertiesService {

    @Autowired
    private ConfigurableEnvironment myEnv;

    private static Logger logger = LoggerFactory.getLogger(GlobalPropertiesService.class);

    Map<String, Object> allProperties = null;

    @PostConstruct
    public void fillProperties() {
        allProperties = getAllProperties();
    }

    public Map<String, Object> getPropertiesStartingWith(String prefix,boolean removePrefix) {
        Map<String, Object> result = new HashMap<>();

        for (Map.Entry<String, Object> entry : allProperties.entrySet()) {
            String key = entry.getKey();

            if (key.startsWith(prefix)) {
                if (removePrefix) {
                    key = key.substring(prefix.length());
                }
                result.put(key, entry.getValue());
            }
        }

        return result;
    }

    public Map<String, Object> getAllProperties() {
        Map<String, Object> result = new HashMap<>();
        myEnv.getPropertySources().forEach(ps -> addAll(result, getAllProperties(ps)));
        return result;
    }

    public static Map<String, Object> getAllProperties(PropertySource<?> aPropSource) {
        Map<String, Object> result = new HashMap<>();

        if (aPropSource instanceof CompositePropertySource) {
            CompositePropertySource cps = (CompositePropertySource) aPropSource;
            cps.getPropertySources().forEach(ps -> addAll(result, getAllProperties(ps)));
            return result;
        }

        if (aPropSource instanceof EnumerablePropertySource<?>) {
            EnumerablePropertySource<?> ps = (EnumerablePropertySource<?>) aPropSource;
            Arrays.asList(ps.getPropertyNames()).forEach(key -> result.put(key, ps.getProperty(key)));
            return result;
        }

        // note: Most descendants of PropertySource are EnumerablePropertySource. There are some
        // few others like JndiPropertySource or StubPropertySource
        logger.debug("Given PropertySource is instanceof " + aPropSource.getClass().getName()
                + " and cannot be iterated");

        return result;

    }

    private static void addAll(Map<String, Object> aBase, Map<String, Object> aToBeAdded) {
        for (Map.Entry<String, Object> entry : aToBeAdded.entrySet()) {
            if (aBase.containsKey(entry.getKey())) {
                continue;
            }

            aBase.put(entry.getKey(), entry.getValue());
        }
    }
}
