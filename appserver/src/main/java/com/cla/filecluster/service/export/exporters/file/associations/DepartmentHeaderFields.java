package com.cla.filecluster.service.export.exporters.file.associations;

/**
 * Created by: yael
 * Created on: 4/1/2019
 */
public interface DepartmentHeaderFields {
    String NAME = "Name";
    String DESCRIPTION = "Description";
    String NUM_OF_FILES = "Number Of Files";
}
