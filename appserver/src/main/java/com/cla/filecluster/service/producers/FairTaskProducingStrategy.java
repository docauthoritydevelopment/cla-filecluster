package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Analyses available scan results and produces groups tasks to be enqueued
 * for processing by the workers.
 * Controls the amount of tasks to enqueue, as well as distribution between different jobs
 * (in order to prevent starvation).
 */
public class FairTaskProducingStrategy extends TaskProducingStrategy {

    public FairTaskProducingStrategy(JobManagerService jobManager, SolrTaskService solrTaskService, TaskProducingStrategyConfig config) {
        super(jobManager, solrTaskService, config);
    }

    // ------------------------ methods --------------------
    @Override
    public Collection<SimpleTask> nextTaskGroup(TaskType taskType, Table<Long, TaskState, Number> taskStatesByType, Integer tasksQueueLimit) {

        // get fair tasks distribution
        Map<Long, Number> tasksDistribution = getTasksDistribution(taskStatesByType, taskType, tasksQueueLimit);

        List<SimpleTask> tasks = Lists.newLinkedList();
        for (Long contextId : tasksDistribution.keySet()){
            Number limit = tasksDistribution.get(contextId);
            if (limit.intValue() > 0) {
                if (taskType.equals(TaskType.INGEST_FILE)) {
                    List<SolrFileEntity> files = solrTaskService.getIngestTasks(contextId, TaskState.NEW, limit.intValue());
                    files.forEach(file -> {
                        SimpleTask task = SolrTaskService.createForIngest(file, jobManager.getJobIdCached(contextId, JobType.INGEST));
                        tasks.add(task);
                    });
                } else {
                    List<SolrContentMetadataEntity> contents = solrTaskService.getAnalysisTasks(contextId, TaskState.NEW, limit.intValue());
                    contents.forEach(content -> {
                        SimpleTask task = SolrTaskService.createForAnalysis(content, jobManager.getJobIdCached(contextId, JobType.ANALYZE));
                        tasks.add(task);
                    });
                }
            }
        }
        return tasks;
    }

    /**
     * Create fair task distribution between all the active contexts,
     * in a way that would prevent starvation of any of them
     * and maintain the queue size under the required limit
     *
     * @return map context ID -> number of tasks that can be enqueued
     */
    private Map<Long, Number> getTasksDistribution(Table<Long, TaskState, Number> taskStatesByType, TaskType taskType, Integer tasksQueueLimit){

        if (tasksQueueLimit == null) {
            tasksQueueLimit = getQueueLimit(taskType);
        }

        Map<Long, Number> newTasksCount = taskStatesByType.column(TaskState.NEW);

        int totalNew = newTasksCount.values().stream().mapToInt(Number::intValue).sum();
        int totalInProgress = taskStatesByType.column(TaskState.ENQUEUED).values().stream().mapToInt(Number::intValue).sum();

        // if total number of enqueued tasks plus total number of new tasks
        // is less than the limit - we can enqueue all the new tasks without passing the limit
        if (newTasksCount.size() == 0 || (totalNew + totalInProgress) <= tasksQueueLimit){
            return newTasksCount;
        }

        // on average - how many tasks can be added per each context ID
        int quotaPerContextId = (tasksQueueLimit - totalInProgress) / newTasksCount.size();
        // the remainder, which can be used later to ensure that the total quota is completely filled
        int borrow = (tasksQueueLimit - totalInProgress) % newTasksCount.size();

        // for each context ID find how many tasks can be added to the queue
        // while maintaining the queue limit and more or less uniform distribution between the contexts
        Map<Long, Number> distribution = Maps.newHashMap();

        // sort the entries by number of available new tasks, smallest first
        // this way the distribution will be most efficient, since the bigger values at the end
        // will be able to "borrow" place in the queue from the smaller numbers in the beginning
        // which are too small to fully fill their quota
        List<Map.Entry<Long, Number>> sortedBySize = newTasksCount.entrySet().stream()
                .sorted(Comparator.comparingInt(f -> f.getValue().intValue()))
                .collect(Collectors.toList());

        for (Map.Entry<Long, Number> entry : sortedBySize){
            Long contextId = entry.getKey();
            Integer count = entry.getValue().intValue();
            if (count >= quotaPerContextId){
                int finalSum = Math.min(count, quotaPerContextId + borrow);
                borrow = Math.max(0, borrow - finalSum);
                distribution.put(contextId, finalSum);
            }
            else{
                distribution.put(contextId, count);
                borrow += quotaPerContextId - count;
            }
        }

        return distribution;
    }

}
