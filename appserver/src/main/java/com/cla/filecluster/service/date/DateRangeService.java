package com.cla.filecluster.service.date;

import com.cla.common.domain.dto.date.*;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.date.DateRangePartition;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.date.DateRangePartitionRepository;
import com.cla.filecluster.repository.jpa.date.SimpleDateRangeItemRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * This service handles the Date range mechanism objects in the database.
 * Both DateRangeItems and DateRangePartitions are handled by this service.
 * Created by uri on 05/06/2016.
 */
@Service
public class DateRangeService {

    @Autowired
    private SimpleDateRangeItemRepository simpleDateRangeItemRepository;

    @Autowired
    private DateRangePartitionRepository dateRangePartitionRepository;

    @Autowired
    private MessageHandler messageHandler;

    private static final Logger logger = LoggerFactory.getLogger(DateRangeService.class);


    @Transactional(readOnly = true)
    public Page<DateRangePartition> listDateRangePartitions(PageRequest pageRequest) {
        return dateRangePartitionRepository.findAll(pageRequest);
    }

    @Transactional
    public DateRangePartition createDateRangePartition(String name, String description, boolean continuousRanges) {
        return createDateRangePartition(name, description, continuousRanges, false);
    }

    @Transactional
    public DateRangePartition createDateRangePartition(String name, String description, boolean continuousRanges, boolean isSystem) {
        DateRangePartition dateRangePartition = new DateRangePartition();
        dateRangePartition.setName(name);
        dateRangePartition.setDescription(description);
        dateRangePartition.setContinuousRanges(continuousRanges);
        dateRangePartition.setSystem(isSystem);
        return dateRangePartitionRepository.save(dateRangePartition);
    }

    @Transactional(readOnly = true)
    public DateRangePartition getDateRangePartitionWithItems(long partitionId) {
        DateRangePartition dateRangePartition = dateRangePartitionRepository.findOneWithItems(partitionId);
        if (dateRangePartition == null) {
            throw new BadRequestException(messageHandler.getMessage("date-range-partition.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return dateRangePartition;
    }

    @Transactional()
    public DateRangePartition updateDateRangePartition(long partitionId, DateRangePartitionDto dateRangePartitionDto) {
        updateDateRangePartitionItems(partitionId, dateRangePartitionDto.getDateRangeItemDtos());
        DateRangePartition dateRangePartition = dateRangePartitionRepository.findOneWithItems(partitionId);
        dateRangePartition.setDescription(dateRangePartitionDto.getDescription());
        dateRangePartition.setName(dateRangePartitionDto.getName());
        dateRangePartitionRepository.save(dateRangePartition);
        return getDateRangePartitionWithItems(partitionId);
    }

    public void updateDateRangePartitionItems(long datePartitionId, List<DateRangeItemDto> dateRangeItemDtos) {
        DateRangePartition dateRangePartition = dateRangePartitionRepository.findOneWithItems(datePartitionId);

        List<DateRangeItemDto> regularRangeItemDtos = Lists.newArrayList();
        Optional<DateRangeItemDto> unavailableRangeItemDto = Optional.empty();

        // split the list to regular and "unavailable" items
        for (DateRangeItemDto dto : dateRangeItemDtos){
            if (dto.getStart().getType() == DateRangePointType.UNAVAILABLE){
                unavailableRangeItemDto = Optional.of(dto);
            }
            else{
                regularRangeItemDtos.add(dto);
            }
        }

        //Sort the points by order from newer to older
        Comparator<DateRangeItemDto> comparator = Comparator.comparingLong(f ->
                calculateAbsoluteTime(f.getStart()));
        dateRangeItemDtos.sort(Collections.reverseOrder(comparator));

        List<SimpleDateRangeItem> simpleDateRangeItems = Lists.newArrayList();

        DateRangePointDto previousPoint = null;
        for (DateRangeItemDto dateRangeItemDto : regularRangeItemDtos) {

            //Connect End to previous start
            dateRangeItemDto.setEnd(
                    (previousPoint == null) ? null :(new DateRangePointDto(previousPoint)));

            SimpleDateRangeItem simpleDateRangeItem;
            if (dateRangeItemDto.getId() != null) {
                simpleDateRangeItem = simpleDateRangeItemRepository.findById(dateRangeItemDto.getId())
                        .map(sdri -> {
                            fillDateRangeItem(dateRangeItemDto, sdri);
                            return sdri;
                        })
                        .orElse(null);
            } else {
                simpleDateRangeItem = createSimpleDateRangeItem(dateRangeItemDto);
            }
            simpleDateRangeItems.add(simpleDateRangeItem);
            previousPoint = dateRangeItemDto.getStart();
        }

        if(unavailableRangeItemDto.isPresent()) {
            DateRangeItemDto unavailableDateRangeItemDto = createUnavailableDateRangeItemDto(unavailableRangeItemDto.get().getName());
            SimpleDateRangeItem unavailableRangeItem = new SimpleDateRangeItem();
            fillDateRangeItem(unavailableDateRangeItemDto, unavailableRangeItem);
            simpleDateRangeItems.add(unavailableRangeItem);
        }

        dateRangePartition.setSimpleDateRangeItems(simpleDateRangeItems);
        dateRangePartition.updateSimpleDateRangeItemsOrder();
        dateRangePartitionRepository.save(dateRangePartition);
    }

    DateRangeItemDto createUnavailableDateRangeItemDto(String name){
        DateRangePointDto startPoint = new DateRangePointDto(TimePeriod.YEARS, 0);
        startPoint.setType(DateRangePointType.UNAVAILABLE);
        DateRangePointDto endPoint = new DateRangePointDto(TimePeriod.YEARS, 1);
        endPoint.setType(DateRangePointType.UNAVAILABLE);
        return new DateRangeItemDto(name, startPoint, endPoint);
    }

    private SimpleDateRangeItem createSimpleDateRangeItem(DateRangeItemDto dateRangeItemDto) {
        SimpleDateRangeItem simpleDateRangeItem = new SimpleDateRangeItem();
        fillDateRangeItem(dateRangeItemDto, simpleDateRangeItem);
        return simpleDateRangeItem;
    }

    private void fillDateRangeItem(DateRangeItemDto dateRangeItemDto, SimpleDateRangeItem simpleDateRangeItem) {
        simpleDateRangeItem.setName(dateRangeItemDto.getName());
        DateRangePointDto startDto = dateRangeItemDto.getStart();
        if (startDto != null) {
            DateRangePoint start = new DateRangePoint();
            fillDateRangePoint(startDto, start);
            simpleDateRangeItem.setStart(start);
        }
        else {
            throw new BadRequestException(messageHandler.getMessage("date-range-partition.start-point.empty"), BadRequestType.MISSING_FIELD);
        }
        DateRangePointDto endDto = dateRangeItemDto.getEnd();
        if (endDto != null) {
            DateRangePoint end = new DateRangePoint();
            fillDateRangePoint(endDto, end);
            simpleDateRangeItem.setEnd(end);
        }
    }

    private void fillDateRangePoint(DateRangePointDto dateRangePointDto, DateRangePoint dateRangePoint) {
        dateRangePoint.setType(dateRangePointDto.getType());
        if (dateRangePointDto.getAbsoluteTime() != null) {
            dateRangePoint.setAbsoluteTime(new Date(dateRangePointDto.getAbsoluteTime()));
        }
        dateRangePoint.setRelativePeriod(dateRangePointDto.getRelativePeriod());
        dateRangePoint.setRelativePeriodAmount(dateRangePointDto.getRelativePeriodAmount());
    }

    @Transactional
    public void deleteDateRangePartition(long partitionId) {
        logger.info("Delete dateRange partition {}", partitionId);
        dateRangePartitionRepository.deleteById(partitionId);
        logger.info("DateRange partition deleted");
    }

    private static long calculateAbsoluteTime(DateRangePoint dateRangePoint) {
        long absoluteTime;

        if (dateRangePoint.getType() == null) {
            return 0L;
        }

        switch (dateRangePoint.getType()) {
            case ABSOLUTE:
                absoluteTime = dateRangePoint.getAbsoluteTime().getTime();
                break;
            case RELATIVE:
                LocalDateTime localDateTime = convertRelativePeriodToAbsoluteTime(dateRangePoint.getRelativePeriod(), dateRangePoint.getRelativePeriodAmount());
                absoluteTime = localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
                break;
            case EVER:
                absoluteTime = TimeUnit.DAYS.toMillis(365);
                break;
            default:
                absoluteTime = 0L;
                break;

        }
        return absoluteTime;
    }

    public static long calculateAbsoluteTime(DateRangePointDto dateRangePoint) {
        long absoluteTime;
        switch (dateRangePoint.getType()) {
            case ABSOLUTE:
                absoluteTime = dateRangePoint.getAbsoluteTime();
                break;
            case RELATIVE:
                LocalDateTime localDateTime = convertRelativePeriodToAbsoluteTime(dateRangePoint.getRelativePeriod(), dateRangePoint.getRelativePeriodAmount());
                absoluteTime = localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
                break;
            case EVER:
                absoluteTime = TimeUnit.DAYS.toMillis(365);
                break;
            default:
                absoluteTime = 0L;
                break;

        }
        return absoluteTime;
    }

    private static LocalDateTime convertRelativePeriodToAbsoluteTime(TimePeriod timePeriod, int periodAmount) {
        LocalDateTime now = LocalDateTime.now();
        switch (timePeriod) {
            case YEARS:
                return now.minusYears(periodAmount);
            case MONTHS:
                return now.minusMonths(periodAmount);
            case DAYS:
                return now.minusDays(periodAmount);
            case HOURS:
                return now.minusHours(periodAmount);
            case WEEKS:
                return now.minusWeeks(periodAmount);
            case MINUTES:
                return now.minusMinutes(periodAmount);
        }
        return now;
    }

    public static DateRangePointDto convertDateRangePoint(DateRangePoint dateRangePoint) {
        if (dateRangePoint == null) {
            return null;
        }
        DateRangePointDto result = new DateRangePointDto();
        long absoluteTime = calculateAbsoluteTime(dateRangePoint);
        result.setAbsoluteTime(absoluteTime);
        result.setRelativePeriod(dateRangePoint.getRelativePeriod());
        result.setRelativePeriodAmount(dateRangePoint.getRelativePeriodAmount());
        result.setType(dateRangePoint.getType());
        return result;
    }
}
