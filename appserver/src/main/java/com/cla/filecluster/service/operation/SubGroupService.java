package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.pv.RefinementRuleDto;
import com.cla.common.domain.dto.pv.RefinementRulePredicate;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.FileDTOSearchResult;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.FileDTOSearchRepository;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.RefinementRuleService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.extractor.entity.ContentSearchTextService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A service to manage files belonging to a raw analysis group, specifically moving files
 * to the correct sub group according to current active rules.
 *
 * Created by: yael
 * Created on: 2/5/2018
 */
@Service
public class SubGroupService {

    static String OTHERS_SUFFIX = " - others";

    private static int CONTENT_GROUP_SIZE_TO_MOVE = 5000;

    @Autowired
    private RefinementRuleService refinementRuleService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private FileDTOSearchRepository fileDTOSearchRepository;

    @Autowired
    private ContentSearchTextService contentSearchTextService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private GroupLockService groupLockService;

    private static final Logger logger = LoggerFactory.getLogger(SubGroupService.class);

    /*
     * Apply current rules to the raw analysis group files by:
     * 1) moving all files back to raw analysis group
     * 2) handling deleted rules and deleting their sub groups
     * 3) re-assessing all contents and re-assigning them to sub groups
     *
     * call all 3 steps functions to complete
     */

    /*
     * apply refinement rules step 1
     * called from @see com.cla.filecluster.service.group.RefinementRulesApplyGroupOperation
     *
     * @param groupId - the raw analysis group
     */
    @AutoAuthenticate
    public boolean firstStepMoveAllBackToParent(String groupId) {
        FileGroup parent = groupsService.findById(groupId);

        // get all existing sub groups
        List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(groupId);
        Map<String, FileGroup> subGroups = subGroupsList.stream().collect(Collectors.toMap(FileGroup::getId, fg -> fg));

        // if files in existing sub groups - move to parent
        logger.debug("going to move all files from sub groups to raw analysis group id {}", groupId);
        moveAllFilesToParent(parent, subGroups);
        return false;
    }

    /*
     * apply refinement rules step 2
     * called from @see com.cla.filecluster.service.group.RefinementRulesApplyGroupOperation
     *
     * @param groupId - the raw analysis group
     */
    @AutoAuthenticate
    public boolean sndStepManageRules(String groupId) {
        FileGroup parent = groupsService.findById(groupId);

        // get all rules
        List<RefinementRuleDto> rules = refinementRuleService.getByRawAnalysisGroupIdAll(groupId);
        if (rules == null || rules.isEmpty()) {
            throw new RuntimeException("Unable to find refinementRules for this group " + groupId);
        }

        // get all existing sub groups
        List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(groupId);
        Map<String, FileGroup> subGroups = subGroupsList.stream().collect(Collectors.toMap(FileGroup::getId, fg -> fg));

        // if there are rules to be deleted - delete them and connected sub groups
        logger.debug("going to delete rules and attached sub groups for raw analysis group id {}", groupId);
        boolean hasInactiveRules = false;
        List<RefinementRuleDto> rulesToRemove = new ArrayList<>();
        for (RefinementRuleDto rule : rules) {
            if (rule.isDeleted()) {
                FileGroup group = subGroups.get(rule.getSubgroupId());
                if (group != null) {
                    groupsService.deleteSubGroup(group);
                    subGroups.remove(rule.getSubgroupId());
                }
                refinementRuleService.deleteRefinementRule(rule.getId());
                rulesToRemove.add(rule);
            } else if (!rule.isActive()) {
                rulesToRemove.add(rule);
                hasInactiveRules = true;
            }
        }
        rules.removeAll(rulesToRemove);

        // parent has children flag update
        logger.debug("going to update subgroup flag for raw analysis group id {}", groupId);
        if (rules.isEmpty()) {
            if (parent.isHasSubgroups()) {
                if (!hasInactiveRules) {
                    updateParentChildrenStatus(groupId, false);
                }
                fileCatService.updateGroupNameOnFiles(parent.getId(), parent.getNameForReport());
                fileCatService.commitToSolr();
                groupsService.refreshGroup(groupId);

                List<FileGroup> fgs = groupsService.findSubGroupsForRefinedGroup(groupId);
                if (fgs != null) {
                    fgs.forEach(fg -> groupsService.deleteGroupWithAssociations(fg.getId()));
                }
            }
            return true;
        }
        if (!parent.isHasSubgroups()) {
            updateParentChildrenStatus(groupId, true);
        }
        return false;
    }

    /*
     * apply refinement rules step 3
     * called from @see com.cla.filecluster.service.group.RefinementRulesApplyGroupOperation
     *
     * @param groupId - the raw analysis group
     */
    @AutoAuthenticate
    public boolean trdStepMoveFilesToSubGroups(String groupId) {
        FileGroup parent = groupsService.findById(groupId);

        // get all rules
        List<RefinementRuleDto> rules = refinementRuleService.getByRawAnalysisGroupIdAll(groupId);
        if (rules == null || rules.isEmpty()) {
            throw new RuntimeException("Unable to find refinementRules for this group " + groupId);
        }

        // get all existing sub groups
        List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(groupId);
        Map<String, FileGroup> subGroups = subGroupsList.stream().collect(Collectors.toMap(FileGroup::getId, fg -> fg));

        // run on all contents in parent group and apply rules to determine to which sub group they belong - and move them there
        logger.debug("going to apply rules for raw analysis group id {}", groupId);
        moveContentsFromRawToSubGroupsByRules(parent, rules, subGroups);

        logger.debug("going to set rules as not dirty for raw analysis group id {}", groupId);
        for (RefinementRuleDto rule : rules) {
            if (rule.isDirty()) {
                refinementRuleService.updateRefinementRuleDirty(rule.getId(), false);
            }
        }

        logger.debug("going to set no files for raw analysis group id {}", groupId);
        setNoFilesInParentGroup(parent);

        return true;
    }

    /**
     * Check if there are any contents in raw analysis group
     * if there are, assign to sub groups according to active rules
     * @param groupId - the raw analysis group to process
     */
    public void moveContentsFromRawToSubGroupsByRules(String groupId) {

        if (!groupLockService.getLockForRawAnalysisGroup(groupId)) {
            logger.info("cant get lock to apply refinement rules to raw analysis group id {}", groupId);
            return;
        }

        try {
            FileGroup group = groupsService.findById(groupId);

            List<RefinementRuleDto> rules = refinementRuleService.getByRawAnalysisGroupIdActive(group.getId());

            if (rules == null || rules.isEmpty()) return;

            List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(group.getId());

            Map<String, FileGroup> subGroups = subGroupsList.stream().collect(Collectors.toMap(FileGroup::getId, fg -> fg));
            moveContentsFromRawToSubGroupsByRules(group, rules, subGroups);
        } catch (Exception e) {
            logger.error("fail to apply refinement rules to raw analysis group id {}", groupId, e);
        } finally {
            groupLockService.releaseLockForRawAnalysisGroup(groupId);
        }
    }

    // Check if there are any contents in raw analysis group if there are, assign to sub groups according to active rules
    private void moveContentsFromRawToSubGroupsByRules(FileGroup parentGroup, List<RefinementRuleDto> rules, Map<String, FileGroup> subGroups) {
        // run on all contents in parent group and apply rules to determine to which sub group they belong
        logger.debug("going to get contents and decide subgroups for raw analysis group id {}", parentGroup.getId());

        while(true) { // get contents in small groups to process to avoid memory issues
            List<ContentMetadata> contents = contentMetadataService.findByAnalysisGroup(parentGroup.getId(), 0, CONTENT_GROUP_SIZE_TO_MOVE);

            if (contents == null || contents.isEmpty()) break;
            Map<String, List<Long>> contentsInGroups = new HashMap<>();

            List<Long> contentIdsList = contents.stream().map(ContentMetadata::getId).collect(Collectors.toList());
            Map<Long, Table<Long, RefinementRulePredicate, Boolean>> evalResults = new HashMap<>();
            for (RefinementRuleDto rule : rules) {
                // evaluate predicates
                List<RefinementRulePredicate> predicates = RefinementRulePredicate.returnAllPredicatesMultiLevel(rule.getPredicate());
                Table<Long, RefinementRulePredicate, Boolean> presults = HashBasedTable.create();
                for (RefinementRulePredicate p : predicates) {
                    evaluatePredicate(p, contentIdsList, presults);
                }
                evalResults.put(rule.getId(), presults);
            }

            // go over contents in page and find relevant sub group by rules
            for (ContentMetadata content : contents) {
                String subGroupId = getFileSubGroup(parentGroup, rules, content.getId(), subGroups, evalResults);
                FileGroup sub = subGroups.get(subGroupId);
                if (sub != null) {
                    List<Long> contentIds = contentsInGroups.get(subGroupId);
                    if (contentIds == null) contentIds = new ArrayList<>();
                    contentIds.add(content.getId());
                    contentsInGroups.put(subGroupId, contentIds);
                } else {
                    logger.warn("failed to find sub group for content {}", content.getId());
                }
            }

            // apply moves to sub groups
            logger.debug("going to move contents to subgroups for raw analysis group id {}", parentGroup.getId());
            Map<Long, ContentMetadata> contentsMap = contents.stream().collect(Collectors.toMap(ContentMetadata::getId, fg -> fg));
            for (Map.Entry<String, List<Long>> entry : contentsInGroups.entrySet()) {
                String subGroupId = entry.getKey();
                FileGroup subGroup = subGroups.get(subGroupId);
                List<Long> contentIds = entry.getValue();
                if (contentIds != null && !contentIds.isEmpty()) {
                    moveContentsAndFilesToSubGroup(parentGroup.getId(), subGroup, contentIds, contentsMap);
                }
            }
        }
    }

    private void updateParentChildrenStatus(String groupId, boolean hasChildren) {
        DBTemplateUtils.doInTransaction(() ->
                groupsService.updateAnalysisGroupHasRefinement(groupId, hasChildren), TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    private FileGroup createSubGroup(String name, String parentGroupId) {
        return groupsService.createNewAnalysisSubGroup(name, parentGroupId, true);
    }

    private FileGroup getOtherSubGroup(FileGroup parent, List<RefinementRuleDto> rules, Map<String, FileGroup> subGroups) {
        List<String> subGroupsWithRules = rules.stream().map(RefinementRuleDto::getSubgroupId).collect(Collectors.toList());
        List<String> existingSubGroups = new ArrayList<>(subGroups.keySet());
        existingSubGroups.removeAll(subGroupsWithRules);
        if (existingSubGroups.isEmpty()) { // create if doesnt exists
            String otherGroupName = (parent.getName() != null ? parent.getName() : parent.getGeneratedName());
            otherGroupName = (otherGroupName == null ? parent.getId() + OTHERS_SUFFIX : otherGroupName + OTHERS_SUFFIX);
            FileGroup fg = createSubGroup(otherGroupName, parent.getId());
            subGroups.put(fg.getId(), fg);
            return fg;
        } else if (existingSubGroups.size() == 1) {
            return subGroups.get(existingSubGroups.get(0));
        } else {
            logger.warn("more groups than expected ????");
            return subGroups.get(existingSubGroups.get(0));
        }
    }

    /**
     * If other sub group was created while the raw analysis group had no name
     * rename it now to match new name
     * @param parent - raw analysis group
     */
    public void renameOthersGroupIfExists(FileGroup parent) {
        List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(parent.getId());
        for (FileGroup sub : subGroupsList) {
            if (sub.getName().equalsIgnoreCase(parent.getId() + OTHERS_SUFFIX)) {
                String otherGroupName = (parent.getName() != null ? parent.getName() : parent.getGeneratedName());
                otherGroupName = (otherGroupName == null ? parent.getId() + OTHERS_SUFFIX : otherGroupName + OTHERS_SUFFIX);
                groupsService.updateGroupName(sub.getId(), otherGroupName);
                break;
            }
        }
    }

    /**
     * Find the correct sub group for a specific content
     * @param parentGroup - raw analysis group id
     * @param contentId - content id to process
     * @return the id of the sub group the content should belong to by the rules
     */
    public String getFileSubGroup(String parentGroup, Long contentId) {

        if (!groupLockService.getLockForRawAnalysisGroup(parentGroup)) {
            logger.info("cant get lock to find sub group (raw {}) for content id {}", parentGroup, contentId);
            return null;
        }

        try {
            FileGroup group = groupsService.findById(parentGroup);

            if (!group.isHasSubgroups()) {
                return parentGroup;
            }

            List<RefinementRuleDto> rules = refinementRuleService.getByRawAnalysisGroupIdActive(parentGroup);

            if (rules == null || rules.isEmpty()) {
                return parentGroup;
            }

            List<FileGroup> subGroupsList = groupsService.findSubGroupsForRefinedGroup(group.getId());
            Map<String, FileGroup> subGroups = subGroupsList.stream().collect(Collectors.toMap(FileGroup::getId, fg -> fg));

            List<Long> contentIdsList = Collections.singletonList(contentId);

            Table<Long, RefinementRulePredicate, Boolean> ruleEvalResults;
            Map<Long, Table<Long, RefinementRulePredicate, Boolean>> evalResults = new HashMap<>();

            for (RefinementRuleDto rule : rules) {
                // evaluate predicates
                List<RefinementRulePredicate> predicates = RefinementRulePredicate.returnAllPredicatesMultiLevel(rule.getPredicate());
                ruleEvalResults = HashBasedTable.create();
                for (RefinementRulePredicate p : predicates) {
                    evaluatePredicate(p, contentIdsList, ruleEvalResults);
                }
                evalResults.put(rule.getId(), ruleEvalResults);
            }

            return getFileSubGroup(group, rules, contentId, subGroups, evalResults);
        } finally {
            groupLockService.releaseLockForRawAnalysisGroup(parentGroup);
        }
    }

    // Find the correct sub group for a specific content
    private String getFileSubGroup(FileGroup parent, List<RefinementRuleDto> sortedRules, Long contentId, Map<String, FileGroup> subGroups,
                                   Map<Long, Table<Long, RefinementRulePredicate, Boolean>> evalResults) {
        for (RefinementRuleDto rule : sortedRules) {

            // evaluate predicates
            Table<Long, RefinementRulePredicate, Boolean> predResults = evalResults.get(rule.getId());
            Map<RefinementRulePredicate, Boolean> results = predResults.row(contentId);

            // test rules and return relevant subgroup
            if (rule.getPredicate().testRule(results)) {
                if (Strings.isNullOrEmpty(rule.getSubgroupId())) {// create sub group if not exists already
                    FileGroup newSubGroup = createSubGroup(rule.getSubgroupName(), parent.getId());
                    refinementRuleService.updateRefinementRuleSubGroup(rule.getId(), newSubGroup.getId());
                    rule.setSubgroupId(newSubGroup.getId());
                    subGroups.put(newSubGroup.getId(), newSubGroup);
                }
                return rule.getSubgroupId();
            }
        }

        // none of the rules apply - return others group
        return getOtherSubGroup(parent, sortedRules, subGroups).getId();
    }

    private void setNoFilesInParentGroup(FileGroup parentGroup) {
        DBTemplateUtils.doInTransaction(() ->
                groupsService.updateGroupHasNoFiles(parentGroup.getId()), TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    // move a bunch of contents and their files from the raw analysis group to a specific sub group
    private void moveContentsAndFilesToSubGroup(String parentGroupId, FileGroup subGroup, List<Long> contentIds, Map<Long, ContentMetadata> contentsMap) {
        DBTemplateUtils.doInTransaction(() -> {
            List<List<Long>> parts = Lists.partition(contentIds, CONTENT_GROUP_SIZE_TO_MOVE);
            for (List<Long> part : parts) {
                contentMetadataService.updateAnalysisGroupIdByContentIds(part, subGroup.getId());
                contentMetadataService.updateUserGroupIdByContentIds(part, subGroup.getId(), parentGroupId);
                groupsService.updateAnalysisGroupsCatFileByContents(subGroup.getId(), part, true);
                groupsService.updateUserGroupsCatFileByContents(parentGroupId, subGroup.getId(), part, true);
                fileCatService.updateGroupNameOnFiles(subGroup.getId(), subGroup.getName());

            }
            long filesNum = groupsService.getFileNumForAnalysisGroup(subGroup.getId());
            // update sub group
            Long firstFileId = contentsMap.get(contentIds.get(0)).getSampleFileId();
            groupsService.updateGroupFiles(subGroup.getId(), firstFileId, contentIds.get(0), filesNum);
        }, TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    // move files and contents from the different sub groups to the raw analysis group
    private void moveAllFilesToParent(FileGroup parent, Map<String, FileGroup> subGroups) {
        if (subGroups.isEmpty()) return; // no sub groups yet - do nothing

        DBTemplateUtils.doInTransaction(() -> {
            for (String sub : subGroups.keySet()) {
                contentMetadataService.changeAnalysisGroupIdAll(parent.getId(), sub);
                contentMetadataService.changeUserGroupIdAll(parent.getId(), sub);
                FileGroup group = groupsService.updateGroupHasNoFiles(sub);
                subGroups.put(sub, group);
            }
            groupsService.updateUserGroupsCatFileByOldUserGroup(parent.getId(), subGroups.keySet(), true);
            groupsService.updateAnalysisGroupsCatFile(parent.getId(), subGroups.keySet(), true);
        }, TransactionDefinition.PROPAGATION_REQUIRES_NEW);
    }

    // evaluate a specific predicate
    private void evaluatePredicate(RefinementRulePredicate p, List<Long> contentIds, Table<Long, RefinementRulePredicate, Boolean> result) {

        String query = "id:";
        if (contentIds.size() > 1) {
            query += contentIds.stream().map(contentId -> "\"" + contentId + "\"")
                    .collect(Collectors.joining(",", "(", ")"));
        } else if (contentIds.size() == 1) {
            query += contentIds.get(0);
        } else {
            return ;
        }

        switch (p.getOperator()) {
            case CONTAINS:
                switch (p.getField()) {
                    case CONTENT:
                        String values = String.join(" or ", p.getValues());
                        String searchContent = contentSearchTextService.convertContentSearchText(values, true);
                        if (searchContent == null) {
                            logger.warn("rule values {} translated into empty search string in solr, cannot search", values);
                        } else {
                            String[] filterQueries = {searchContent, query};
                            String[] fields = {"id"};
                            FileDTOSearchResult resp = fileDTOSearchRepository.find(filterQueries, fields);
                            if (resp != null && resp.getExtractionSolrEntities() != null) {
                                resp.getExtractionSolrEntities().forEach(v -> {
                                    Long contentId = Long.parseLong(v.getId());
                                    result.put(contentId, p, true);
                                });
                            }
                        }
                        return;
                    default:
                        throw new IllegalArgumentException("invalid predicate field");
                }
            default:
                throw new IllegalArgumentException("invalid predicate type");
        }
    }
}
