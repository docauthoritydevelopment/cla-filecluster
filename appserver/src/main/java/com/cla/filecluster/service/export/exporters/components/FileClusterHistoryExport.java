package com.cla.filecluster.service.export.exporters.components;

import com.cla.common.domain.dto.components.AppServerStatus;
import com.cla.common.domain.dto.components.ClaComponentStatusDto;
import com.cla.common.utils.FileSizeUnits;
import com.cla.filecluster.service.export.ExportType;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/17/2019
 */
@Component
public class FileClusterHistoryExport extends ComponentHistoryExport {

    private final static String[] header = {
            TIMESTAMP, INGEST_TASK_REQ, ANALYSE_TASK_REQ, INGEST_THROUGHPUT,
            ANALYSE_THROUGHPUT, DISK_SPACE, MEMORY, CPU};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.FC_HISTORY);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected Map<String, Object> addExtraFields(ClaComponentStatusDto base, Map<String, String> requestParams) {
        Map<String, Object> res = super.addExtraFields(base, requestParams);

        String extraMetrics = base.getExtraMetrics();
        AppServerStatus extra = componentsAppService.getFCStatusFromString(extraMetrics);
        if (extra != null) {
            res.put(INGEST_TASK_REQ, extra.getIngestTaskRequests());
            res.put(ANALYSE_TASK_REQ, extra.getAnalyzedTaskRequests());

            res.put(INGEST_THROUGHPUT, decimalFormatter.format(extra.getIngestRunningThroughput())
                    + "/" + decimalFormatter.format(extra.getAverageIngestThroughput()));
            res.put(ANALYSE_THROUGHPUT, decimalFormatter.format(extra.getAnalyzedRunningThroughput())
                    + "/" + decimalFormatter.format(extra.getAverageAnalyzedThroughput()));

            long memFull = extra.getTotalPhysicalMemorySize() - extra.getFreePhysicalMemorySize();
            int mem = (int)(((float)memFull/extra.getTotalPhysicalMemorySize())*100);
            res.put(MEMORY, FileSizeUnits.BYTE.toGigabytes(memFull)
                    + "GB/" +
                    FileSizeUnits.BYTE.toGigabytes(extra.getTotalPhysicalMemorySize()) +
                  "GB (" + mem + "%)");

            res.put(CPU, decimalFormatter.format(extra.getSystemCpuLoad()*100) + "%");
        }

        return res;
    }
}
