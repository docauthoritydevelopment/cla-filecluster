package com.cla.filecluster.service.export.exporters.security.template;

import com.cla.common.domain.dto.security.RoleTemplateDto;
import com.cla.common.domain.dto.security.RoleTemplateType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.security.RoleTemplateService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.security.template.RoleTemplateHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
@Slf4j
@Component
public class RoleTemplateExporter extends PageCsvExcelExporter<RoleTemplateDto> {

    private final static String[] header = {NAME, DESCRIPTION, ROLES};

    @Autowired
    private RoleTemplateService roleTemplateService;

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(RoleTemplateDto.class, RoleTemplateDtoMixin.class);
    }

    @Override
    protected Iterable<RoleTemplateDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        RoleTemplateType type = RoleTemplateType.valueOf(requestParams.getOrDefault("type", RoleTemplateType.NONE.name()).toUpperCase());
        return roleTemplateService.listRoleTemplates(dataSourceRequest, type);
    }

    @Override
    protected Map<String, Object> addExtraFields(RoleTemplateDto base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();

        if (base.getSystemRoleDtos() != null && !base.getSystemRoleDtos().isEmpty()) {
            List<String> names = new ArrayList<>();
            base.getSystemRoleDtos().forEach(role -> {
                names.add(role.getName());
            });
            String stringConcated = String.join("; ", names);
            res.put(ROLES, stringConcated);
        }

        return res;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.ROLE_TEMPLATE);
    }
}
