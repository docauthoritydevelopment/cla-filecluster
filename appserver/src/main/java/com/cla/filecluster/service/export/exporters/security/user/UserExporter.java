package com.cla.filecluster.service.export.exporters.security.user;

import com.cla.common.domain.dto.security.UserDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.security.UserAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.security.user.UserHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
@Slf4j
@Component
public class UserExporter extends PageCsvExcelExporter<UserDto> {

    private final static String[] header = {NAME, LOCK, USERNAME, OPER_ROLES, DATA_ROLES, STATE, TYPE, EMAIL, CNG_PWD, LDAP_USER};

    @Autowired
    private UserAppService userAppService;

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(UserDto.class, UserDtoMixin.class);
    }

    @Override
    protected Iterable<UserDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return userAppService.getAllUsers(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(UserDto base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();

        if (base.getBizRoleDtos() != null && !base.getBizRoleDtos().isEmpty()) {
            List<String> names = new ArrayList<>();
            base.getBizRoleDtos().forEach(biz -> {
                names.add(biz.getName());
            });
            String stringConcated = String.join("; ", names);
            res.put(OPER_ROLES, stringConcated);
        }

        if (base.getFuncRoleDtos() != null && !base.getFuncRoleDtos().isEmpty()) {
            List<String> names = new ArrayList<>();
            base.getFuncRoleDtos().forEach(func -> {
                names.add(func.getName());
            });
            String stringConcated = String.join("; ", names);
            res.put(DATA_ROLES, stringConcated);
        }

        return res;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.USERS);
    }
}
