package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.Range;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class ContnentMetadataDiagnosticAction implements EntityDiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(ContnentMetadataDiagnosticAction.class);
    private static final String CONTENT_ID_PARAM = "CONTENT ID";

    @Autowired
    private EventBus eventBus;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private FileCatService fileCatService;

    @Value("${diagnostics.content-metadata.page-size:5000}")
    private int pageSize;

    @Value("${diagnostics.content-metadata.start-after-min:90}")
    private int startAfterMin;

    @Value("${diagnostics.content-metadata.event-limit:100}")
    private int cycleEventThreshold;

    @Value("${diagnostics.content-validation.group.enabled:true}")
    private boolean groupValidationEnabled;

    @Value("${diagnostics.content-validation.file-count.enabled:true}")
    private boolean fileCountValidationEnabled;

    @Value("${diagnostics.content-validation.sample-file.enabled:true}")
    private boolean sampleFileValidationEnabled;

    @Value("${diagnostics.content-validation.percentage:100}")
    private String percentageToTestConfig;

    @Value("${diagnostics.content-validation.pickRandomly:true}")
    private boolean partialTestingPickRandomly;

    private AtomicInteger currentEventCounter = new AtomicInteger();

    private Long lastEndRangeRun = null;

    @Override
    public void runDiagnostic(Map<String, String> opData) {

        Long from = opData.containsKey(START_LIMIT) ? Long.parseLong(opData.get(START_LIMIT)) : null;

        Long to = opData.containsKey(END_LIMIT) ? Long.parseLong(opData.get(END_LIMIT)) : null;

        String idsStr = opData.get(SPECIFIC_IDS);

        float percentageTest = Float.parseFloat(opData.getOrDefault(PAGE_PERCENTAGE_TEST, percentageToTestConfig));

        if (currentEventCounter.get() >= cycleEventThreshold) {
            logger.debug("threshold passed for validating content metadata - skip");
            return;
        }

        if (!groupValidationEnabled && !fileCountValidationEnabled && !sampleFileValidationEnabled) {
            logger.debug("all validations are disabled. validating contents - skip");
            return;
        }

        Query query = Query.create().setRows(pageSize)
                .addSort(ContentMetadataFieldType.ID, Sort.Direction.ASC);

        if (from != null && to != null) {
            logger.debug("validating content metadata update date range from {} to {}", from, to);
            query.addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.UPDATE_DATE, SolrOperator.IN_RANGE, Range.create(from, to)));
            lastEndRangeRun = to;
        } else if (idsStr != null) {
            String[] split = idsStr.split(",");
            List<String> ids = Arrays.stream(split)
                    .collect(Collectors.toList());
            logger.debug("validating content metadata for specific ids size {}", ids.size());
            query.addFilterWithCriteria(Criteria.create(ContentMetadataFieldType.ID, SolrOperator.IN, ids));
        } // else no limitation run on entire collection

        String cursor = "*";
        boolean lastPage;
        int pageNum = 1;
        do {

            QueryResponse resp = contentMetadataService.runQuery(query.setCursorMark(cursor).build());

            List<SolrContentMetadataEntity> contents = resp.getBeans(SolrContentMetadataEntity.class);
            cursor = resp.getNextCursorMark();

            int retrievedItemsCount = contents.size();
            if (contents != null && retrievedItemsCount > 0) {
                List<? extends SolrEntity> tempListForSampling = Lists.newArrayList(contents);
                tempListForSampling = getSolrEntitiesSample(tempListForSampling, percentageTest, partialTestingPickRandomly, pageSize);
                contents = (List<SolrContentMetadataEntity>)tempListForSampling;

                Set<Long> contentIds = contents.stream()
                        .map(c -> Long.parseLong(c.getId()))
                        .collect(Collectors.toSet());

                Map<String, Long> fileNumForContent = groupsService.getFileNumForContent(contentIds);

                Set<Long> fileIds = contents.stream()
                        .filter(content -> content.getFileCount() > 0)
                        .map(SolrContentMetadataEntity::getSampleFileId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<SolrFileEntity> files = (fileIds == null || fileIds.isEmpty()) ?
                        new ArrayList<>() : fileCatService.findByFileIds(fileIds);
                Map<Long, SolrFileEntity> fileById = files.stream().collect(Collectors.toMap(SolrFileEntity::getFileId, r -> r));

                Set<String> groupIds = contents.stream()
                        .map(SolrContentMetadataEntity::getUserGroupId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<FileGroup> groups = (groupIds == null || groupIds.isEmpty()) ?
                        new ArrayList<>() : groupsService.findByIds(groupIds);
                Map<String, FileGroup> groupById = groups.stream().collect(Collectors.toMap(FileGroup::getId, r -> r));

                for (SolrContentMetadataEntity content : contents) {
                    validateContent(content, fileNumForContent, fileById, groupById);
                    if (currentEventCounter.get() >= cycleEventThreshold) {
                        logger.debug("threshold passed for validating contents after id {} - skip rest", content.getId());
                        break;
                    }
                }
            }
            logger.debug("check diagnostics page number {} of size {} contents (out of {})", pageNum, contents.size(), retrievedItemsCount);
            pageNum++;
            lastPage = resp.getResults().size() < pageSize;
        } while (!lastPage);
        logger.debug("done validating content metadata");
    }

    public Logger getLogger() {
        return logger;
    }

    private void validateContent(SolrContentMetadataEntity content,
                                 Map<String, Long> fileNumForContent,
                                 Map<Long, SolrFileEntity> fileById,
                                 Map<String, FileGroup> groupById) {
        try {
            List<Event> events = new ArrayList<>();

            if (groupValidationEnabled) {
                validGroup(content, groupById).ifPresent(events::add);
            }

            if (fileCountValidationEnabled) {
                validFileCount(content, fileNumForContent).ifPresent(events::add);
            }

            if (sampleFileValidationEnabled) {
                validSampleFile(content, fileById).ifPresent(events::add);
            }

            currentEventCounter.addAndGet(events.size());
            events.forEach(event -> eventBus.broadcast(Topics.DIAGNOSTICS, event));
        } catch (Exception e) {
            logger.error("problem validating content {}", content.getId() , e);
        }
    }

    private Optional<Event> validSampleFile(SolrContentMetadataEntity content, Map<Long, SolrFileEntity> fileById) {
        if (!content.isPopulationDirty() && content.getFileCount() > 0) {
            Long sampleFileId = content.getSampleFileId();
            SolrFileEntity sampleFile = sampleFileId == null ? null : fileById.get(sampleFileId);
            if (sampleFileId == null || sampleFile == null ||
                    sampleFile.isDeleted() || !sampleFile.getContentId().equals(content.getId())) {
                Event event = Event.builder().withEventType(EventType.CONTENT_METADATA_INVALID_SAMPLE)
                        .withEntry(CONTENT_ID_PARAM, content.getId())
                        .withEntry("SAMPLE_FILE", sampleFileId)
                        .build();
                return Optional.of(event);
            }
        }
        return Optional.empty();
    }

    private Optional<Event> validFileCount(SolrContentMetadataEntity content, Map<String, Long> fileNumForContent) {
        if (!content.isPopulationDirty()) {
            Long fileCount = fileNumForContent.get(content.getId());

            if (content.getFileCount() > 0 && (fileCount == null || fileCount != content.getFileCount())) {
                Event event = Event.builder().withEventType(EventType.CONTENT_METADATA_INVALID_COUNT)
                        .withEntry(CONTENT_ID_PARAM, content.getId())
                        .withEntry("FILE_COUNT", content.getFileCount())
                        .withEntry("REAL_FILE_COUNT", fileCount)
                        .build();
                return Optional.of(event);
            }
        }

        return Optional.empty();
    }

    private Optional<Event> validGroup(SolrContentMetadataEntity content, Map<String, FileGroup> groupById) {
        if (!Strings.isNullOrEmpty(content.getUserGroupId())) {
            if (!groupById.containsKey(content.getUserGroupId()) || groupById.get(content.getUserGroupId()).isDeleted()) {
                Event event = Event.builder().withEventType(EventType.CONTENT_METADATA_INVALID_GROUP)
                        .withEntry(CONTENT_ID_PARAM, content.getId())
                        .withEntry("USER_GROUP_ID", content.getUserGroupId())
                        .build();
                return Optional.of(event);
            }
        }
        return Optional.empty();
    }

    @Override
    public int getStartAfterMin() {
        return startAfterMin;
    }

    @Override
    public Long getLastEndRangeRun() {
        return lastEndRangeRun;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        currentEventCounter.set(0);
        return Lists.newArrayList(addDiagnosticActionTimeParts());
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.CONTENT_METADATA;
    }
}
