package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.schedule.ScheduledOperationMetadataDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by: yael
 * Created on: 3/31/2019
 */
@RestController
@RequestMapping("/api/operations")
public class ScheduledOperationApiController {

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<ScheduledOperationMetadataDto> listScheduledOperations(@RequestParam Map<String,String> params) {
        return scheduledOperationService.getAllPending();
    }

    @RequestMapping(value = "/list/user", method = RequestMethod.GET)
    public List<ScheduledOperationMetadataDto> listUserScheduledOperations(@RequestParam Map<String,String> params) {
        return scheduledOperationService.getAllUserOpPending();
    }

    @RequestMapping(value = "/list/user/current", method = RequestMethod.GET)
    public List<ScheduledOperationMetadataDto> listSpecificUserScheduledOperations(@RequestParam Map<String,String> params) {
        User owner = userService.getCurrentUserEntity();
        if (owner != null) {
            return scheduledOperationService.findAllUserOperations(owner.getId());
        }
        return new ArrayList<>();
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public ScheduledOperationMetadataDto getScheduledOperation(@PathVariable long id) {
        ScheduledOperationMetadataDto operation = scheduledOperationService.getById(id);
        if (operation == null) {
            throw new BadRequestException(messageHandler.getMessage("operation.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return operation;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.POST)
    public void stopDisplayScheduledOperation(@PathVariable long id) {
        ScheduledOperationMetadataDto operation = scheduledOperationService.getById(id);
        if (operation == null) {
            throw new BadRequestException(messageHandler.getMessage("operation.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        validateUserAllowedOperation(operation);
        scheduledOperationService.updateShouldNotDisplay(id);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public void deleteScheduledOperation(@PathVariable long id) {
        ScheduledOperationMetadataDto operation = scheduledOperationService.getById(id);
        if (operation == null) {
            throw new BadRequestException(messageHandler.getMessage("operation.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        validateUserAllowedOperation(operation);
        if (!operation.getOperationType().canBeDeleted()) {
            throw new BadRequestException(messageHandler.getMessage("operation.delete-not-allowed"), BadRequestType.OPERATION_FAILED);
        }
        boolean success = scheduledOperationService.deleteOperation(id);
        if (!success) {
            throw new BadRequestException(messageHandler.getMessage("operation.delete-not-allowed"), BadRequestType.OPERATION_FAILED);
        }
    }

    private void validateUserAllowedOperation(ScheduledOperationMetadataDto operation) {
        User owner = userService.getCurrentUserEntity();
        boolean allowed = true;

        if (owner == null || owner.getId() == null) {
            allowed = false;
        } else if (!userService.isUserAdmin(owner) && operation.getOwnerId() == null) {
            allowed = false;
        } else if (!userService.isUserAdmin(owner) && operation.getOwnerId() != null && !owner.getId().equals(operation.getOwnerId())) {
            allowed = false;
        }

        if (!allowed) {
            throw new BadRequestException(messageHandler.getMessage("operation.user-not-allowed"), BadRequestType.UNAUTHORIZED);
        }
    }
}
