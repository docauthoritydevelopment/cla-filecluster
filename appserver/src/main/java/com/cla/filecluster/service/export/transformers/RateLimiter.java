package com.cla.filecluster.service.export.transformers;

import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.function.Function;

public class RateLimiter<V> implements Transformer<V, V> {

    private final int highTide;
    private final int lowTide;

    public static <T> RateLimiter<T> with(int hightide, int lowtide) {
        return new RateLimiter<>(hightide, lowtide);
    }

    private RateLimiter(int hightide, int lowtide) {
        this.highTide = hightide;
        this.lowTide = lowtide;
    }

    @Override
    public Function<Flux<V>, Publisher<DataBuffer>> asDataBuffer() {
        throw new UnsupportedOperationException("RateLimiter cannot be transformed to a data output stream");
    }

    @Override
    public Publisher<V> apply(Flux<V> inputflux) {
        return inputflux.limitRate(highTide, lowTide);
    }
}
