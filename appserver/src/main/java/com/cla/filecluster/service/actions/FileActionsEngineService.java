package com.cla.filecluster.service.actions;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.common.domain.dto.action.ActionTriggerType;
import com.cla.common.domain.dto.action.DispatchActionRequestDto;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagMinimalDto;
import com.cla.common.domain.dto.filetag.FileTagTypeSettingsDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.filecluster.actions.base.BaseActionParams;
import com.cla.filecluster.actions.base.DocAuthorityAction;
import com.cla.filecluster.actions.base.DocAuthorityActionClassExecutor;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import com.cla.filecluster.domain.entity.actions.ActionExecutionTaskStatus;
import com.cla.filecluster.domain.entity.actions.FileActionDispatch;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.repository.jpa.actions.ActionExecutionStatusRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by uri on 14/02/2016.
 */
@Service
public class FileActionsEngineService {

    public static final int ACTION_PAGE_SIZE = 10000;
    public static final String ID = "id";

    public static final String TARGET_TYPE = "target_type";

    private static Logger logger = LoggerFactory.getLogger(FileActionsEngineService.class);

    @Autowired
    private ActionDefinitionService actionDefinitionService;

    @Autowired
    private ActionExecutionStatusRepository actionExecutionStatusRepository;

    private boolean stopEngineRequestGiven = false;

    public FileActionDispatch createFileActionDispatch(DispatchActionRequestDto request, long actionTriggerId) {
        String url = FileNamingUtils.convertFromUnixToWindows(request.getFullPath()) + request.getBaseName();
        Map<String, String> valueMap = generateReplacementValues(request);
        populateBaseParameterValues(request.getParameters(), valueMap);
        return createFileActionDesired(request.getFileId()
                , url, request.getActionDefinitionId(), request.getParameters(), actionTriggerId);
    }

    private Map<String, String> generateReplacementValues(RootFolder rf, Properties params, ClaFile claFile) {
        Map<String, String> result = new HashMap<>();
        result.put(ID, String.valueOf(claFile.getId()));
        result.put(DocAuthorityAction.FULL_PATH, FileNamingUtils.convertFromUnixToWindows(rf.getRealPath()+claFile.getFullPath()));
        result.put(DocAuthorityAction.BASE_NAME, claFile.getBaseName());
        params.forEach((p, v) -> {
            if (v != null && !((String) v).contains("${")) {
                //static parameters can be used as substitution
                result.put((String) p, (String) v);
            }
        });
        return result;
    }

    private Map<String, String> generateReplacementValues(DispatchActionRequestDto dispatchActionRequestDto) {
        Map<String, String> result = new HashMap<>();
        result.put(ID, String.valueOf(dispatchActionRequestDto.getFileId()));
        result.put(DocAuthorityAction.FULL_PATH, dispatchActionRequestDto.getFullPath());
        result.put(DocAuthorityAction.BASE_NAME, dispatchActionRequestDto.getBaseName());
        dispatchActionRequestDto.getParameters().forEach((p, v) -> {
            if (v != null && !((String) v).contains("${")) {
                //static parameters can be used as substitution
                result.put((String) p, (String) v);
            }
        });
        return result;
    }

    private void populateBaseParameterValues(Properties parameters, Map<String, String> valueMap) {
        StrSubstitutor strSubstitutor = new StrSubstitutor(valueMap, "${", "}");
        for (Map.Entry<Object, Object> objectObjectEntry : parameters.entrySet()) {
            String value = (String) objectObjectEntry.getValue();
            if (value != null && !value.isEmpty() && value.contains("${")) {
                value = strSubstitutor.replace(value);
                parameters.put(objectObjectEntry.getKey(), value);
            }
        }
    }

    private FileActionDispatch createFileActionDesired(long fileId, String url, String actionDefinitionId, Properties parameters, long actionTriggerId) {
        ActionDefinition actionDefinition = actionDefinitionService.findActionDefinition(actionDefinitionId);
        FileActionDispatch fileActionDispatch = new FileActionDispatch();
        fileActionDispatch.setActionDefinition(actionDefinition);
        fileActionDispatch.setClaFileId(fileId);
        fileActionDispatch.setActionTriggerId(actionTriggerId);
        ActionClass actionClass = actionDefinition.getActionClass();
        fileActionDispatch.setActionClass(actionClass);
        String propertiesJson = convertParametersToString(parameters);
        fileActionDispatch.setUrl(url);
        fileActionDispatch.setProperties(propertiesJson);
        return fileActionDispatch;
    }

    @Transactional
    public int runActionClassExecutor(ActionExecutionTaskStatusDto actionExecutionTaskStatusDto,
                                      ActionClass actionClass,
                                      DocAuthorityActionClassExecutor actionClassExecutorInstance,
                                      List<FileActionDispatch> allByActionClass) {
        logger.info("Run action execution engine over action class {} with limit of {}", actionClass, ACTION_PAGE_SIZE);
        int executionCount = 0;
        logger.debug("Executing {} actions", allByActionClass.size());
        Map<Long, Collection<FileActionDispatch>> actionsByFiles = groupActionsByFiles(allByActionClass);
        logger.debug("Executing actions on a total of {} files", actionsByFiles.size());
        for (Map.Entry<Long, Collection<FileActionDispatch>> longCollectionEntry : actionsByFiles.entrySet()) {
            Collection<FileActionDispatch> resolvedActions = resolveRequiredActions(longCollectionEntry.getValue());
            //Execute the actions
            for (FileActionDispatch resolvedAction : resolvedActions) {
                try {
                    executeAction(actionClassExecutorInstance, resolvedAction);
                } catch (RuntimeException e) {
                    logger.warn("Runtime exception while trying to execute action", e);
                    actionExecutionTaskStatusDto.addError(resolvedAction.toString() + ":" + e.getMessage());
                }
            }
            if (stopEngineRequestGiven) {
                logger.info("Stop Engine request was given. Stopping action execution now");
                return 0;
            }
        }
        executionCount = executionCount + allByActionClass.size();

        logger.debug("Executor is applying");
        actionClassExecutorInstance.apply();
        return executionCount;
    }

    public void executeAction(DocAuthorityActionClassExecutor actionClassExecutorInstance, FileActionDispatch resolvedAction) {
        if (actionClassExecutorInstance == null || actionClassExecutorInstance.getClass() == null) {
            throw new RuntimeException("Failed to find action class executor for " + resolvedAction.getActionClass() + "(" + actionClassExecutorInstance + ")");
        }
        Properties properties = convertParametersJson(resolvedAction.getProperties());
        if (properties == null) {
            throw new RuntimeException("Failed to convert action properties: " + resolvedAction.getProperties());
        }

        String javaMethodName = resolvedAction.getActionDefinition().getJavaMethodName();
        BaseActionParams baseActionParams = new BaseActionParams(resolvedAction.getClaFileId(),
                resolvedAction.getUrl(),
                resolvedAction.getActionTriggerId(), resolvedAction.getActionDefinition().getId());
        try {
            Method method = actionClassExecutorInstance.getClass().getMethod(javaMethodName, baseActionParams.getClass(), properties.getClass());
            method.invoke(actionClassExecutorInstance, baseActionParams, properties);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Failed to run resolved action " + resolvedAction.getActionDefinition().getId() + ". Missing method name " + javaMethodName, e);
        } catch (InvocationTargetException e) {
            //This exception wraps another exception by definition (it's the exception's purpose)
            Throwable cause = e.getCause();
            if (cause == null) {
                cause = e;
            }
            logger.warn("Failed to run resolved action {}. Exception: {}", resolvedAction.getActionDefinition().getId(), cause.getMessage());
//            throw new RuntimeException("Failed to run resolved action "+resolvedAction.getActionDefinition().getId()+". Exception: "+cause.getMessage(),cause);
        } catch (Exception e) {
            throw new RuntimeException("Failed to run resolved action " + resolvedAction.getActionDefinition().getId() + ". Exception: " + e.getMessage(), e);
        }
    }

    private Collection<FileActionDispatch> resolveRequiredActions(Collection<FileActionDispatch> value) {
        return value;
    }

    private Map<Long, Collection<FileActionDispatch>> groupActionsByFiles(Collection<FileActionDispatch> allByActionClass) {

        Map<Long, Collection<FileActionDispatch>> result = new HashMap<>();
        for (FileActionDispatch fileActionDispatch : allByActionClass) {
            long fileId = fileActionDispatch.getClaFileId();
            Collection<FileActionDispatch> collection = result.get(fileId);
            if (collection == null) {
                collection = new ArrayList<>();
                result.put(fileId, collection);
            }
            collection.add(fileActionDispatch);
        }

        return result;
    }

    public long createActionTriggerEvent(long triggerTime, ActionTriggerType actionTriggerType,
                                         FilterDescriptor filterDescriptor, String actionId, Properties actionParams,
                                         String entityType, String entityId, Long userId) {
        ActionExecutionTaskStatus actionExecutionTaskStatus = new ActionExecutionTaskStatus();
        actionExecutionTaskStatus.setTriggerTime(triggerTime);
        actionExecutionTaskStatus.setStatus(RunStatus.RUNNING);
        actionExecutionTaskStatus.setType(actionTriggerType);
        actionExecutionTaskStatus.setUserId(userId);
        actionExecutionTaskStatus.setActionId(actionId);
        actionExecutionTaskStatus.setFilterDescriptor(filterDescriptor);
        actionExecutionTaskStatus.setActionParams(actionParams);
        actionExecutionTaskStatus.setEntityId(entityId);
        actionExecutionTaskStatus.setEntityType(entityType);
        ActionExecutionTaskStatus save = actionExecutionStatusRepository.save(actionExecutionTaskStatus);
        return save.getId();
    }

    public void requestStopEngine() {
        this.stopEngineRequestGiven = true;
    }

    public void prepareEngine() {
        this.stopEngineRequestGiven = false;
    }

    /**
     * Extract some more parameters from the file, and add them to the actionParams.
     * The exact list of parameters extracted this way totally depends on the action class
     *
     * @param file
     * @param actionParams
     */
    public void addActionClassParameters(ClaFile file, DocFolder docFolder, RootFolder rf, Properties actionParams,
                                         FileGroup userGroup,
                                         List<AssociationEntity> groupAssociations,
                                         Map<Long, BasicScope> scopes,
                                         List<AssociableEntityDto> fileAssocEntities,
                                         List<AssociationEntity> fileDocTypeAssociation,
                                         List<AssociationEntity> fileTagAssociations,
                                         List<AssociationEntity> folderTagAssociations,
                                         List<AssociationEntity> folderDocTypeAssociation) {
        List<AssociationEntity> groupDocTypeAssociations = AssociationsService.getDocTypeAssociation(groupAssociations);
        addDocTypeToActionParams(actionParams,groupDocTypeAssociations, scopes, fileAssocEntities, fileDocTypeAssociation, folderDocTypeAssociation);
        addFileTagsToActionParams(actionParams, groupAssociations, folderTagAssociations, fileAssocEntities, fileTagAssociations);
        if (rf.getMediaType().hasPath()) {
            actionParams.put(DocAuthorityAction.ROOT_PATH, rf.getRealPath());
            actionParams.put(DocAuthorityAction.RELATIVE_PATH, getRelativePath(file, docFolder, rf));
        } else {
            actionParams.put(DocAuthorityAction.RELATIVE_PATH, file.getFullPath());
        }
        if (FileNamingUtils.isPackageEntry(rf.getRealPath() + file.getFullPath())) {
            actionParams.put(DocAuthorityAction.IS_PACKAGE, "1");
        }
        if (FileNamingUtils.isContainerEntry(file.getFullPath())) {
            actionParams.put(DocAuthorityAction.IS_CONTAINER, "1");
        }
        actionParams.put(DocAuthorityAction.BASE_NAME, file.getBaseName());

        MediaType docFolderMediaType = rf.getMediaType();
        if (docFolderMediaType != MediaType.FILE_SHARE) {
            actionParams.put(DocAuthorityAction.EXTERNAL_MEDIA_URL, Optional.ofNullable( rf.getMediaConnectionDetails().getUrl()).orElse(""));
            actionParams.put(DocAuthorityAction.EXTERNAL_MEDIA_TYPE, rf.getMediaType().name());
        } else if (actionParams.getProperty(TARGET_TYPE) != null) {
                MediaType type = MediaType.valueOf(actionParams.getProperty(TARGET_TYPE));
                actionParams.put(DocAuthorityAction.EXTERNAL_MEDIA_URL, actionParams.getProperty("target"));
                actionParams.put(DocAuthorityAction.EXTERNAL_MEDIA_TYPE, type.name());
        }
    }

    private String getRelativePath(ClaFile file, DocFolder docFolder, RootFolder root) {
        String rootFolderPath = root.getRealPath();
        String docFolderPath = root.getRealPath() + docFolder.getRealPath();
        switch (root.getMediaType()) {
            case ONE_DRIVE:
            case SHARE_POINT:
                if (!docFolderPath.startsWith(root.getRealPath())) {
                    logger.error("path {} doesn't seem to be under root path {}", docFolderPath, rootFolderPath);
                    throw new IllegalArgumentException("Failed to resolve relative path");
                }
                return docFolderPath.substring(rootFolderPath.length());

            case FILE_SHARE:
            default:
                String relPath;
                /*
                 * DAMAIN-9784 StoragePolicy action script does not give correct path for files contained in zip
                 * Don't get internal entry path until an internal ZIP remediation is supported.
                 *
                if (FileNamingUtils.isPackageEntry(rootFolderPath + file.getFullPath())) {
					relPath = Paths.get(FileNamingUtils.getPackageEntryPath(rootFolderPath + file.getFullPath())).toString();
					if (logger.isTraceEnabled()) {
					    logger.trace("isPackageEntry()=true: getPackageEntryPath({})={}, rootFolderPath={}, file.getFullPath()={}  relPath={}",
                                rootFolderPath + file.getFullPath(), FileNamingUtils.getPackageEntryPath(rootFolderPath + file.getFullPath()),
                                rootFolderPath, file.getFullPath(), relPath);
                    }
				}
				else */
				if (FileNamingUtils.isContainerEntry(file.getFullPath())) {
                    relPath = rootFolderPath + file.getFullPath();
                    if (logger.isTraceEnabled()) {
                        logger.trace("isContainerEntry()=true: rootFolderPath={}, file.getFullPath()={} relPath={}", rootFolderPath, file.getFullPath(), relPath);
                    }
                } else {
                    relPath = Paths.get(rootFolderPath).relativize(Paths.get(docFolderPath)).toString();
                    if (logger.isTraceEnabled()) {
                        logger.trace("regular file: rootFolderPath={}, docFolderPath={} relPath={}", rootFolderPath, docFolderPath, relPath);
                    }
                }
                return relPath;
        }
    }

    public void addFileTagsToActionParams(Properties actionParams, List<AssociationEntity> groupAssociations,
                                          List<AssociationEntity> folderTagAssociations, List<AssociableEntityDto> fileAssocEntities,
                                          List<AssociationEntity> fileTagAssociations) {

        Set<FileTagDto> fileTagDtos = AssociationUtils.collectFileTags(true,
                fileAssocEntities, groupAssociations, fileTagAssociations, folderTagAssociations);
        if (fileTagDtos.size() > 0) {
            Collection<FileTagMinimalDto> minimalDtos = FileTagService.convertFileTagsDtoToMinimal(fileTagDtos);
            addTagsToActionParams(actionParams, minimalDtos);
        }
    }

    private void addTagsToActionParams(Properties actionParams, Collection<FileTagMinimalDto> minimalDtos) {
        try {
            if (minimalDtos != null && minimalDtos.size() > 0) {
                if (actionParams.getProperty(DocAuthorityAction.TAGS) != null) {
                    String current = actionParams.getProperty(DocAuthorityAction.TAGS);
                    if (!Strings.isNullOrEmpty(current)) {
                        Collection<FileTagMinimalDto> existing = ModelDtoConversionUtils.getMapper().readValue(current, new TypeReference<Collection<FileTagMinimalDto>>(){});
                        minimalDtos.addAll(existing);
                    }
                }
                actionParams.put(DocAuthorityAction.TAGS, ModelDtoConversionUtils.getMapper().writeValueAsString(minimalDtos));
            }
        } catch (Exception e) {
            logger.error("problem setting tag dtos in action params", e);
        }
    }

    public void addDocTypeToActionParams(Properties actionParams, List<AssociationEntity> groupDocTypeAssociations,
                                         Map<Long, BasicScope> scopes, List<AssociableEntityDto> fileAssocEntities,
                                         List<AssociationEntity> fileDocTypeAssociation, List<AssociationEntity> folderDocTypeAssociation) {

        Set<DocTypeDto> docTypeDtos = AssociationUtils.collectDocTypes(scopes, fileAssocEntities, fileDocTypeAssociation);
        docTypeDtos.addAll(AssociationUtils.collectDocTypes(scopes, fileAssocEntities, folderDocTypeAssociation));
        docTypeDtos.addAll(AssociationUtils.collectDocTypes(scopes, fileAssocEntities, groupDocTypeAssociations));

        Collection<FileTagMinimalDto> minimalDtos = new ArrayList<>();
        if (docTypeDtos.size() > 0) {
            actionParams.put(DocAuthorityAction.DOC_TYPE, docTypeDtos.iterator().next().getName());
            for (DocTypeDto dto : docTypeDtos) {
                if (dto.getFileTagTypeSettings() != null && dto.getFileTagTypeSettings().size() > 0) {
                    for (FileTagTypeSettingsDto tagType : dto.getFileTagTypeSettings()) {
                        if (tagType.getFileTags() != null && tagType.getFileTags().size() > 0) {
                            minimalDtos.addAll(FileTagService.convertFileTagsDtoToMinimal(tagType.getFileTags()));
                        }
                    }
                }
            }
            if (minimalDtos.size() > 0) {
                addTagsToActionParams(actionParams, minimalDtos);
            }
        }
    }

    public void populateBaseParameterValues(RootFolder rf, Properties params, ClaFile claFile, DocFolder folder, FileGroup userGroup, List<AssociationEntity> groupAssociations,
                                            Map<Long, BasicScope> scopes, List<AssociableEntityDto> fileAssocEntities, List<AssociationEntity> fileTagAssociations, List<AssociationEntity> fileDocTypeAssociation,
                                            List<AssociationEntity> folderTagAssociations, List<AssociationEntity> folderDocTypeAssociation) {
        addActionClassParameters(claFile, folder, rf, params, userGroup, groupAssociations, scopes, fileAssocEntities, fileDocTypeAssociation, fileTagAssociations, folderTagAssociations, folderDocTypeAssociation);
        Map<String, String> valueMap = generateReplacementValues(rf, params, claFile);
        populateBaseParameterValues(params, valueMap);
    }

    public static String convertParametersToString(Properties properties) {
        try {
            if (properties == null) {
                return null;
            }
            return ModelDtoConversionUtils.getMapper().writeValueAsString(properties);
        } catch (IOException e) {
            logger.error("Failed to convert Parameters to JSON string (IO Exception): " + e.getMessage(), e);
            throw new RuntimeException("Failed to convert Parameters to JSON string (IO Exception): " + e.getMessage(), e);
        }
    }

    public static Properties convertParametersJson(String parameters) {
        if (StringUtils.isEmpty(parameters)) {
            return new Properties();
        }
        try {
            return ModelDtoConversionUtils.getMapper().readValue(parameters, Properties.class);
        } catch (IOException e) {
            logger.error("Failed to convert paramters string [" + parameters + "] to map", e);
            throw new RuntimeException("Failed to convert paramters string [" + parameters + "] to map", e);
        }
    }
}
