package com.cla.filecluster.service.export.exporters.file.date;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.date.DateRangeItemDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.date.DateRangeAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.date.DateHeaderFields.*;

public abstract class DateExporter extends PageCsvExcelExporter<AggregationCountItemDTO<DateRangeItemDto>> {

    private final static String[] HEADER = {NAME, START_DATE, NUM_OF_FILTERED, NUM_OF_FILES};

    @Autowired
    private DateRangeAppService dateRangeAppService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, DateAggCountDtoMixin.class);
        mapper.addMixIn(DateRangeItemDto.class, DateRangeItemDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<DateRangeItemDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);

        if (requestParams.get("partitionId") == null) {
            throw new BadRequestException(messageHandler.getMessage("date-partition-export.missing-parameter"), BadRequestType.MISSING_FIELD);
        }
        long partitionId = Long.parseLong(requestParams.get("partitionId"));

        FacetPage<AggregationCountItemDTO<DateRangeItemDto>> res = dateRangeAppService.findDateRangeFileCount(getField(), partitionId, dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

    protected abstract CatFileFieldType getField();

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<DateRangeItemDto> base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        result.put(DateHeaderFields.NUM_OF_FILES, base.getCount2() < 0 ? 0 : base.getCount2());
        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }
}
