package com.cla.filecluster.service.files.filetree;

import com.cla.common.constants.CatFoldersFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagSource;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.filetree.DocStoreDto;
import com.cla.common.domain.dto.filetree.PathDescriptorType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.media.Exchange365ItemDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.progress.ProgressTracker;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.SolrFolderBuilder;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetag.AssociationRelatedData;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.exceptions.StoppedByUserException;
import com.cla.filecluster.domain.specification.DocFoldersByPathSpecification;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.solr.SolrCatFolderRepository;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.io.FilenameUtils;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrInputDocument;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class DocFolderService {

    private static final Pattern URL_REGEX = Pattern.compile("^http[s]?://.+");
    private static final int MAX_QUERY_ROWS_NUMBER= 50000;

    private static final Logger logger = LoggerFactory.getLogger(DocFolderService.class);

    @Autowired
    private SolrCatFolderRepository solrFolderRepository;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private MessageHandler messageHandler;

    private LoadingCache<DocFolderCacheKey, DocFolderCacheValue> docFolderIdsCache;

    @Value("${scan.folders.cache.max:5000}")
    private int maximumDocFolderCacheSize;

    private final Object entityCacheLock = new Object();

    private int cacheMisses = 0;

    private Map<DocFolderCacheKey, SolrFolderEntity> docFoldersToBeCreated = new ConcurrentHashMap<>();

    @PostConstruct
    public void initFolderCache() {
        docFolderIdsCache = CacheBuilder.newBuilder()
                .maximumSize(maximumDocFolderCacheSize)
                .build(
                        new CacheLoader<DocFolderCacheKey, DocFolderCacheValue>() {
                            public DocFolderCacheValue load(@NotNull DocFolderCacheKey key) {
                                if (docFoldersToBeCreated.containsKey(key)) {
                                    return new DocFolderCacheValue(docFoldersToBeCreated.get(key));
                                }

                                String realPath = key.getRealPath();
                                Pair<Boolean, SolrFolderEntity> folderIfNeeded = createFoldersCacheHelper(key.getRootFolderId(), realPath);
                                if (folderIfNeeded.getKey()) { // add to be created only if new
                                    addNewFolderToBeCreated(key, folderIfNeeded.getValue());
                                }
                                return new DocFolderCacheValue(folderIfNeeded.getValue());
                            }
                        });
    }

    private void addNewFolderToBeCreated(DocFolderCacheKey key, SolrFolderEntity val) {
        docFoldersToBeCreated.put(key, val);
    }

    public synchronized void createFoldersInSolr() {
        if (docFoldersToBeCreated.isEmpty()) return;

        Map<DocFolderCacheKey, SolrFolderEntity> toCreate = new HashMap<>(docFoldersToBeCreated);

        if (toCreate.isEmpty()) return;

        solrFolderRepository.saveEntities(new ArrayList<>(toCreate.values()));
        solrFolderRepository.softCommitNoWaitFlush();

        toCreate.keySet().forEach(folder -> {
            docFoldersToBeCreated.remove(folder);
        });
    }

    public Optional<SolrFolderEntity> getOneById(long id) {
        return solrFolderRepository.getOneById(id);
    }

    public Optional<SolrFolderEntity> getOneByIdFromCache(long id) {
        List<SolrFolderEntity> values = new ArrayList<>(docFoldersToBeCreated.values());
        return values.stream()
                .filter(folder -> folder.getId() == id).findAny();
    }

    public DocFolder getById(long id) {
        List<SolrFolderEntity> values = new ArrayList<>(docFoldersToBeCreated.values());
        Optional<DocFolder> toBePersisted = values.stream()
                .filter(folder -> folder.getId() == id)
                .map(DocFolderService::getFromEntity)
                .findAny();

        return toBePersisted.orElseGet(() -> solrFolderRepository.getById(id));
    }

    public static DocFolder getFromEntity(SolrFolderEntity entity) {
        return SolrCatFolderRepository.getFromEntity(entity);
    }

    public DocFolderDto getByIdWithAssociations(long id) {
        DocFolder folder = solrFolderRepository.getById(id);
        if (folder == null) return null;
        AssociationRelatedData data = associationsService.getAssociationRelatedData(folder);
        RootFolder rf = docStoreService.getRootFolderCached(folder.getRootFolderId());
        return convertDocFolder(folder, true, true, rf, data.getAssociations(),
                data.getScopes(), data.getEntities());
    }

    public DocFolderDto getDtoById(long id) {
        DocFolder folder = solrFolderRepository.getById(id);
        if (folder == null) return null;
        AssociationRelatedData data = associationsService.getAssociationRelatedData(folder);
        return convertDocFolder(folder, true, true,
                null, data.getAssociations(), data.getScopes(), data.getEntities());
    }

    @Transactional
    public DocFolderCacheValue createDocFolderIfNeededWithCache(Long rootFolderId, String folderUrl) {

        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);
        String folderUrlToAdd = folderUrl;

        if (!Strings.isNullOrEmpty(rf.getRealPath())) {
            if (folderUrl.startsWith(rf.getRealPath())) {
                folderUrlToAdd = folderUrl.substring(rf.getRealPath().length());
            } else if (rf.getRealPath().length() > folderUrl.length() && rf.getRealPath().startsWith(folderUrl)) { // real path ends with slash and otherwise same as folder url
                folderUrlToAdd = "";
                logger.info("doc folder {} for root folder {} special treatment, set url to empty string", folderUrl, rootFolderId);
            }
        }
        folderUrlToAdd = DocFolderUtils.removeTrailingSlash(folderUrlToAdd);
        DocFolderCacheKey docFolderCacheKey = new DocFolderCacheKey(folderUrlToAdd, rootFolderId);
        return docFolderIdsCache.getUnchecked(docFolderCacheKey);
    }

    // TODO: create SOLR plugin for that!!! create all the folder hierarchy in single SOLR request
    private Pair<Boolean, SolrFolderEntity> createFoldersCacheHelper(Long rootFolderId, String realPath) {

        SolrFolderEntity folderByPath = getFolderByPath(rootFolderId, realPath);
        if (folderByPath != null) {
            if (folderByPath.getDeleted()) {
                undeleteFolder(folderByPath);
            }
            return Pair.of(false, folderByPath); // return not new folder
        }

        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);

        DocFolderCacheValue parentFolder = null;
        String universalPath = FileNamingUtils.convertPathToUniversalString(rootFolder.getRealPath() + realPath);
        int depthFromRoot = DocFolderUtils.calculateDepth(universalPath) - rootFolder.getBaseDepth();
        // depth from root should be at least 1 in order to have a meaningful parent
        if (realPath.length() > 0 && depthFromRoot > 0) {
            String parentRealPath = DocFolderUtils.extractFolderFromPath(realPath);
            DocFolderCacheKey parentKey = new DocFolderCacheKey(parentRealPath, rootFolderId);
            // in the following line, if the parent folder does not exist it will be created
            // recursively, so that its own parent folder will be also created if needed and so on
            // the creation of folders is synchronized by means of the LoadingCache, which is thread safe
            parentFolder = docFolderIdsCache.getUnchecked(parentKey);
        }

        return Pair.of(true, createFolder(rootFolder, parentFolder, realPath, true)); // return new folder
    }

    public void undeleteFolder(SolrFolderEntity folder) {
        SolrFolderBuilder builder = SolrFolderBuilder.create();
        builder.setDeleted(false);
        builder.setId(folder.getId());
        SolrInputDocument doc = builder.buildAtomicUpdate();
        solrFolderRepository.atomicUpdate(doc);
        solrFolderRepository.softCommitNoWaitFlush();

        if (folder.getParentFolderId() != null) {
            SolrFolderEntity parent = getOneById(folder.getParentFolderId()).orElse(null);
            if (parent != null && parent.getDeleted()) {
                undeleteFolder(parent);
            }
        }
    }

    public void softCommitNoWaitFlush() {
        solrFolderRepository.softCommitNoWaitFlush();
    }

    public void commit() {
        solrFolderRepository.commit();
    }

    public String getDocFolderCacheStats() {
        return docFolderIdsCache.stats().toString();
    }

    @Transactional
    public SolrFolderEntity createFolderIfNeeded(Long rootFolderId, String realPath) {

        SolrFolderEntity folderByPath = getFolderByPath(rootFolderId, realPath);
        if (folderByPath != null) {
            if (folderByPath.getDeleted()) {
                folderByPath.setDeleted(false);
            }
            return folderByPath;
        }
        RootFolder rootFolder = docStoreService.getRootFolderCached(rootFolderId);
        return createFolder(rootFolder, null, realPath, false);
    }


    /**
     * Create new doc folder in SOLR
     *
     * @param rootFolder        enclosing root folder
     * @param parentFolderCache parent folder if exists
     * @param realPath          full path
     * @return SolrFolderEntity bean
     */
    private SolrFolderEntity createFolder(RootFolder rootFolder, DocFolderCacheValue parentFolderCache, String realPath,
                                          boolean flush) {

        PathDescriptorType pathDescriptorType = null;
        if (rootFolder.getDocStore() != null) {
            pathDescriptorType = rootFolder.getDocStore().getPathDescriptorType();
        }

        Long parentFolderId = parentFolderCache != null ? parentFolderCache.getDocFolderId() : null;
        logger.debug("Creating folder: {}, parentFolderId: {}, rootFolderId: {}, flush: {}", realPath,
                parentFolderId, rootFolder.getId(), flush);

        DocFolder docFolder = new DocFolder();
        fillDocFolderBaseProperties(rootFolder, realPath, docFolder);
        List<String> parentFolderInfo = parentFolderCache != null ? parentFolderCache.getParentsInfo() : null;

        if (parentFolderId != null) {
            docFolder.setParentFolderId(parentFolderId);
        }

        docFolder.setId(solrFolderRepository.getNextAvailableId());

        List<String> associations = null;
        if (parentFolderId != null) {
            associations = inheritParentAssociations(parentFolderId);
            if (associations != null && !associations.isEmpty()) {
                logger.trace("inherit parent association folder {} parent {} associations {}",
                        realPath, parentFolderId, associations);
            } 
        }

        docFolder.setParentsInfo(calculateParentsInfo(docFolder, parentFolderInfo));

        //-----------------------------------------------------

        SolrFolderEntity folderEntity = SolrFolderBuilder.create()
                .setId(docFolder.getId())
                .setPath(realPath)
                .setPathDescriptorType(pathDescriptorType)
                .setRootFolderId(rootFolder.getId())
                .setRootDepth(rootFolder.getBaseDepth())
                .setRootFolderPath(rootFolder.getRealPath())
                .setType(docFolder.getFolderType())
                .setParentInfo(docFolder.getParentsInfo())
                .setParentFolderId(parentFolderId)
                .setAssociations(associations)
                .setCreationDate(System.currentTimeMillis())
                .build();

        if (folderEntity == null) {
            return null;
        }

        return folderEntity;
    }

    private List<String> inheritParentAssociations(Long parentFolderId) {
        DocFolder parentFolder = getById(parentFolderId);
        List<AssociationEntity> association = AssociationsService.convertFromAssociationEntity(parentFolderId, parentFolder.getAssociations(), AssociationType.FOLDER);
        if (association != null && !association.isEmpty()) {
            List<String> result = new ArrayList<>();
            association.
                    forEach(ass -> {
                        ass.setImplicit(true);
                        String res = AssociationsService.convertFromAssociationEntity(ass);
                        if (res != null) {
                            result.add(res);
                        }
                    });
            return result.isEmpty() ? null : result;
        }
        return null;
    }

    @Deprecated
    private boolean fillDocFolderBaseProperties(RootFolder rootFolder, String realPath, DocFolder docFolder) {
        String universalPath = null;
        int absoluteDepth = 0;
        int depthFromRoot = 0;
        String folderName;

        // real path can be null or empty for Exchange 365 root folders, for example (fine for other media type)
        if (rootFolder.getMediaType().hasPath() || !Strings.isNullOrEmpty(realPath)) {
            universalPath = FileNamingUtils.convertPathToUniversalString(realPath);
            String pathForDepth = FileNamingUtils.convertPathToUniversalString(rootFolder.getRealPath() + realPath);
            folderName = DocFolderUtils.extractFolderName(universalPath);
            logger.trace("Create folder realPath={} universal={} folderName={}", realPath, universalPath, folderName);
            absoluteDepth = DocFolderUtils.calculateDepth(pathForDepth);
            int rootDepth = rootFolder.getBaseDepth();
            depthFromRoot = absoluteDepth - rootDepth;
            if (depthFromRoot < 0) {
                logger.error("Can't create a docFolder with depth < 0 ({}, universalPath: {})", depthFromRoot, universalPath);
                return false;
            }
        } else {
            folderName = rootFolder.getNickName();
        }
        docFolder.setRootFolderId(rootFolder.getId());
        docFolder.setPathDescriptorType(rootFolder.getDocStore().getPathDescriptorType());
        docFolder.setDepthFromRoot(depthFromRoot);
        docFolder.setAbsoluteDepth(absoluteDepth);
        docFolder.setRealPath(realPath);
        docFolder.setPath(universalPath);
        docFolder.setName(folderName);
        docFolder.setNumOfSubFolders(null);
        docFolder.setNumOfDirectFiles(0);
        docFolder.updateHashedFolderString();

        if (folderName != null) {
            docFolder.setFolderType(FolderType.getFromName(folderName));
        } else {
            docFolder.setFolderType(FolderType.REGULAR);
        }

        return true;
    }

    public DocFolder getDocFolderByFolderPath(Long rootFolderId, String folderPath) {
        //Root folder is unknown
        return solrFolderRepository.getByPath(rootFolderId, folderPath);
    }

    public void updateFolderSyncState(Long folderId, String syncState) {
        //syncState = syncState.substring(syncState.indexOf("deltatoken=")+11);
        SolrFolderBuilder builder = SolrFolderBuilder.create();
        builder.setSyncState(syncState);
        builder.setId(folderId);
        SolrInputDocument doc = builder.buildAtomicUpdate();
        solrFolderRepository.atomicUpdate(doc);
        solrFolderRepository.softCommitNoWaitFlush();
    }

    @Deprecated
    public DocFolder getFolderByPathFromDb(Long rootFolderId, String realPath) {
        //Use the hashed String for that
        String normalizedPath = FileNamingUtils.convertPathToUniversalString(realPath);
        String hashedFolderString = DocFolder.getHashedFolderString(normalizedPath, rootFolderId);
        List<DocFolder> byHashedFolderString = solrFolderRepository.getByFolderHash(hashedFolderString);
        for (DocFolder docFolder : byHashedFolderString) {
            if (docFolder.getPath().equals(normalizedPath) && docFolder.getRootFolderId().equals(rootFolderId)) {
                return docFolder;
            } else {
                logger.warn("Found a folder with the same hash, but different folders. It may point to a bug (or not) {} != {}", docFolder.getPath(), normalizedPath);
            }
        }
        //No matches
        return null;
    }

    public SolrFolderEntity getFolderByPath(Long rootFolderId, String realPath) {
        //Use the hashed String for that
        String normalizedPath = FileNamingUtils.convertPathToUniversalString(realPath);
        String hashedFolderString = DocFolder.getHashedFolderString(normalizedPath, rootFolderId);
        List<SolrFolderEntity> folders = solrFolderRepository.getByFilterCriterias(
                Criteria.create(CatFoldersFieldType.FOLDER_HASH, SolrOperator.EQ, hashedFolderString));

        for (SolrFolderEntity docFolder : folders) {
            if (docFolder.getRootFolderId().equals(rootFolderId) && docFolder.getPath().equals(normalizedPath)) {
                return docFolder;
            } else {
                logger.warn("Found a folder with the same hash, but different folders. " +
                        "It may point to a bug. {} != {}", docFolder.getPath(), normalizedPath);
            }
        }
        //No matches
        return null;
    }

    public void updateFolderSyncState(Long folderId, Long rootFolderId, String mediaEntityId, String syncState) {
        if (folderId == null) {
            SolrFolderEntity folder = getFolderByMediaEntityIdFromCreated(rootFolderId, mediaEntityId);
            if (folder != null) {
                folder.setSyncState(syncState);
            } else {
                folder = getFolderByMediaEntityIdSolr(rootFolderId, mediaEntityId);
                if (folder != null) {
                    folderId = folder.getId();
                }
            }
        }
        if (folderId != null) {
            updateFolderSyncState(folderId, syncState);
        }
    }

    private SolrFolderEntity getFolderByMediaEntityIdFromCreated(Long rootFolderId, String mediaEntityId) {
        // chk to be created folders
        List<SolrFolderEntity> values = new ArrayList<>(docFoldersToBeCreated.values());
        for (SolrFolderEntity folder : values) {
            if (folder.getRootFolderId().equals(rootFolderId) &&
                    folder.getMediaEntityId() != null && folder.getMediaEntityId().equals(mediaEntityId)) {
                return folder;
            }
        }

        //No matches
        return null;
    }

    private SolrFolderEntity getFolderByMediaEntityIdSolr(Long rootFolderId, String mediaEntityId) {
        List<SolrFolderEntity> folders = solrFolderRepository.getByFilterCriterias(
                Criteria.create(CatFoldersFieldType.MEDIA_ENTITY_ID, SolrOperator.EQ, mediaEntityId));

        for (SolrFolderEntity docFolder : folders) {
            if (docFolder.getRootFolderId().equals(rootFolderId)) {
                return docFolder;
            }
        }
        return null;
    }

    public SolrFolderEntity getFolderByMediaEntityId(Long rootFolderId, String mediaEntityId) {
        // chk to be created folders
        SolrFolderEntity folder = getFolderByMediaEntityIdFromCreated(rootFolderId, mediaEntityId);
        if (folder != null) {
            return folder;
        }

        // search in solr
        return getFolderByMediaEntityIdSolr(rootFolderId, mediaEntityId);
    }

    @Transactional(readOnly = true)
    public Page<DocFolderDto> listAllFolders(Long docStoreId, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = extractPageRequest(dataSourceRequest);
        List<DocFolder> docFolders;
        Long total = 0L;
        Integer maxDepth = dataSourceRequest.getMaxDepth();
        Integer relDepth = dataSourceRequest.getRelDepth();
        if (maxDepth == null && relDepth == null) {
            logger.trace("List all folders in docStore id {}", docStoreId);
            docFolders = solrFolderRepository.getWithLimit((int) pageRequest.getOffset(), pageRequest.getPageSize());
            total = solrFolderRepository.getTotalCount(null);
        } else {
            if (maxDepth == null) {
                maxDepth = relDepth - 1; // This is the same on a full folder list
            }
            logger.trace("List all folders in docStore id {} up to depth {}", docStoreId, maxDepth);
            docFolders = solrFolderRepository.getWithLimit((int) pageRequest.getOffset(), pageRequest.getPageSize(), maxDepth);
            total = solrFolderRepository.getTotalCount(maxDepth);
        }
        return convertDocFolderPage(pageRequest, docFolders, total.intValue());
    }

    @Transactional(readOnly = true)
    public List<DocFolder> listFolders(DocFolder baseFolder, int relMaxDepth) {
        DocFoldersByPathSpecification baseFolderFilter = new DocFoldersByPathSpecification(null, baseFolder);
        logger.trace("List folders from base folder {} up to relative depth of {}", baseFolder.getPath(), relMaxDepth);
        baseFolderFilter.setMaxDepth(baseFolder.getDepthFromRoot() + relMaxDepth);
        Query query = baseFolderFilter.toQuery();
        return solrFolderRepository.findAll(query);
    }

    public List<DocFolder> listFolders(DocFolder baseFolder, int relMinDepth, int relMaxDepth) {
        DocFoldersByPathSpecification baseFolderFilter = new DocFoldersByPathSpecification(null, baseFolder);
        logger.trace("List folders from base folder {} from relative depth of {} up to relative depth of {}", baseFolder.getPath(), relMinDepth, relMaxDepth);
        baseFolderFilter.setMaxDepth(baseFolder.getDepthFromRoot() + relMaxDepth);
        baseFolderFilter.setMinDepth(baseFolder.getDepthFromRoot() + relMinDepth);
        Query query = baseFolderFilter.toQuery();
        query.addSort(CatFoldersFieldType.PATH, Sort.Direction.ASC);
        return solrFolderRepository.findAll(query);
    }

    @Transactional(readOnly = true)
    public Page<DocFolderDto> listFolders(DocFolder baseFolder, DataSourceRequest dataSourceRequest) {
        Integer maxDepth = dataSourceRequest.getMaxDepth();
        Integer relDepth = dataSourceRequest.getRelDepth();
        PageRequest pageRequest = extractPageRequest(dataSourceRequest);

        DocFoldersByPathSpecification baseFolderFilter = new DocFoldersByPathSpecification(dataSourceRequest.getFilter(), baseFolder);
        if (maxDepth != null) {
            logger.trace("List folders from base folder {} up to depth {}", baseFolder.getPath(), maxDepth);
            baseFolderFilter.setMaxDepth(maxDepth);
        } else if (relDepth != null) {
            logger.trace("List folders from base folder {} up to relative depth of {}", baseFolder.getPath(), relDepth);
            baseFolderFilter.setMaxDepth(baseFolder.getDepthFromRoot() + relDepth);

        } else {
            logger.trace("List folders from base folder {}", baseFolder.getPath());
        }

        Query notDeleted = Query.create().addFilterWithCriteria(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false");
        Query query = baseFolderFilter.toQuery().addFilterQuery(notDeleted);

        List<DocFolder> docFolders = solrFolderRepository.getWithLimit((int) pageRequest.getOffset(), pageRequest.getPageSize(), query);
        Long total = solrFolderRepository.getTotalCount(query, CatFoldersFieldType.DELETED, "false");

        return convertDocFolderPage(pageRequest, docFolders, total.intValue());
    }

    private PageRequest extractPageRequest(DataSourceRequest dataSourceRequest) {
        Sort.Order depthOrder = new Sort.Order(Sort.Direction.ASC, "depthFromRoot");
        Sort.Order pathOrder = new Sort.Order(Sort.Direction.ASC, "path");
        Sort sort = new Sort(depthOrder, pathOrder);
        return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(), sort);
    }

    private Page<DocFolderDto> convertDocFolderPage(PageRequest pageRequest, List<DocFolder> docFolders, Integer total) {
        List<DocFolderDto> content = new ArrayList<>();

        if (docFolders != null && !docFolders.isEmpty()) {
            Set<Long> rootFolderIds = docFolders.stream().map(DocFolder::getRootFolderId).filter(Objects::nonNull).collect(Collectors.toSet());
            List<RootFolder> rootFolders = docStoreService.getRootFoldersCached(rootFolderIds);
            Map<Long, RootFolder> rootFoldersById = rootFolders.stream().collect(Collectors.toMap(RootFolder::getId, Function.identity()));
            AssociationRelatedData data = associationsService.getAssociationRelatedData(docFolders);
            docFolders.forEach(docFolder -> {
                        content.add(convertDocFolder(docFolder, true, true,
                                rootFoldersById.get(docFolder.getRootFolderId()),
                                data.getPerEntityAssociations().get(docFolder.getId()), data.getScopes(), data.getEntities()));
                    }
            );
        }
        return new PageImpl<>(content, pageRequest, total);
    }

    @Transactional(readOnly = true)
    public DocFolderDto findParentFolder(long folderId) {
        DocFolder baseFolder = getById(folderId);
        Long parentFolderId = baseFolder.getParentFolderId();
        if (parentFolderId == null) {
            logger.info("User requested parent folder of folder {} but no parent found", baseFolder.getPath());
            return null;
        }
        DocFolder parentFolder = getById(parentFolderId);
        return convertDocFolder(parentFolder, false, false, null, null, null, null);
    }

    @Transactional
    public DocFolderDto assignExplicitDocTypeToFolder(Long folderId, DocTypeDto docType, BasicScope basicScope) {
        DocFolder folder = findById(folderId);
        logger.debug("Assign docType {} to folder {}", docType, folder);
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(folderId, folder.getAssociations(), AssociationType.FOLDER);
        List<AssociationEntity> docTypeAssoc = AssociationsService.getDocTypeAssociation(associations);

        Long docFolderId = folder.getParentFolderId();
        while (docFolderId != null) {
            DocFolder docFolder = findById(docFolderId);
            List<AssociationEntity> parentAssociations = AssociationsService.convertFromAssociationEntity(docFolderId, docFolder.getAssociations(), AssociationType.FOLDER);
            docTypeAssoc.addAll(AssociationsService.getDocTypeAssociation(parentAssociations));
            docFolderId = docFolder.getParentFolderId();
        }

        AssociationEntity folderDocTypeAssociation = associationsService.createFolderAssociation(docType, folder, false, folderId,
                basicScope == null ? null : basicScope.getId(), AssociableEntityType.DOC_TYPE);

        associations.add(folderDocTypeAssociation);
        return convertDocFolder(folder, true, false, null, associations
                , associationsService.getScopesForAssociations(associations), associationsService.getEntitiesForAssociations(associations));
    }

    @Transactional
    public DocFolderDto addExplicitFileTagToFolder(Long folderId, FileTagDto fileTag, BasicScope basicScope) {
        DocFolder folder = findById(folderId);
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(folderId, folder.getAssociations(), AssociationType.FOLDER);
        Set<AssociationEntity> allAssociations = Sets.newHashSet(associations);
        // Case tag is single value, then take all associations also
        // from parent folders to verify no duplicate tags
        if (fileTag.getType().isSingleValueTag()) {
            Long parentFolderId = folder.getParentFolderId();
            while (parentFolderId != null) {
                DocFolder parentFolder = findById(parentFolderId);
                allAssociations.addAll(AssociationsService.convertFromAssociationEntity(parentFolder.getId(), parentFolder.getAssociations(), AssociationType.FOLDER));
                parentFolderId = parentFolder.getParentFolderId();
            }
        }

        Long scopeId = basicScope == null ? null : basicScope.getId();
        for (AssociationEntity association : allAssociations) {
            if (!association.getImplicit() && Objects.equals(association.getFileTagId(), fileTag) &&
                    Objects.equals(association.getAssociationScopeId(), scopeId)) {
                logger.info("Folder or its parent was already explicitly associated with tag {}", fileTag.getId());
                throw new BadRequestException(messageHandler.getMessage("tag.folder.association-exists"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        }

        AssociationEntity association = createTagAssociation(fileTag, folder, basicScope, false, folderId, AssociableEntityType.TAG);
        associations.add(association);
        return convertDocFolder(folder, true, true, null, associations,
                associationsService.getScopesForAssociations(associations), associationsService.getEntitiesForAssociations(associations));
    }

    @Transactional
    public DocFolderDto addExplicitAssociableEntityToFolder(Long folderId, AssociableEntityDto associableEntity, AssociableEntityType associationType) {
        DocFolder folder = getById(folderId);
        List<AssociationEntity> associations = AssociationsService.convertFromAssociationEntity(folderId, folder.getAssociations(), AssociationType.FOLDER);
        for (AssociationEntity association : associations) {
            if (!association.getImplicit() && association.sameEntity(associableEntity)) {
                logger.trace("Folder is already explicitly associated with this entity");
                return null;
            }
        }
        AssociationEntity associationEntity = associationsService.createFolderAssociation(associableEntity, folder, false, folder.getId(), null, associationType);
        associations.add(associationEntity);
        return convertDocFolder(folder, true, true,
                null, associations, associationsService.getScopesForAssociations(associations), associationsService.getEntitiesForAssociations(associations));
    }

    private AssociationEntity createTagAssociation(AssociableEntityDto associableEntity, DocFolder baseFolder,
                                                   BasicScope basicScope, boolean implicit, Long docFolderId, AssociableEntityType associationType) {
        return associationsService.createFolderAssociation(associableEntity, baseFolder, implicit, docFolderId, basicScope == null ? null : basicScope.getId(), associationType);
    }

    public DocFolder findById(Long folderId) {
        Optional<SolrFolderEntity> entity = solrFolderRepository.getOneById(folderId);
        if (entity.isPresent()) {
            return SolrCatFolderRepository.getFromEntity(entity.get());
        }
        return null;
    }

    public List<DocFolder> findByIds(Collection<Long> ids) {
        if (ids == null || ids.size() == 0) {
            return new ArrayList<>();
        }
        List<SolrFolderEntity> entities = solrFolderRepository.find(ids);
        List<DocFolder> res = entities.stream().map(SolrCatFolderRepository::getFromEntity).collect(Collectors.toList());

        // TODO yael get associations and doc types

        // Handle potential duplication due to OneToMany fetch returned by Hibernate -
        // TODO: consider other solutions for this(e.g. Criteria.DISTINCT_ROOT_ENTITY, or let JPA return SET<>)
        return (ids.size() == res.size()) ?
                res :
                new ArrayList<>(new HashSet<>(res));
    }

    private String encodeSingleParentInfo(SolrFolderEntity df) {
        return df.getDepthFromRoot() + "." + df.getId();
    }

    private String encodeSingleParentInfo(DocFolder df) {
        return df.getDepthFromRoot() + "." + df.getId();
    }

    private String encodeSingleParentInfo(Long id, int depthFromRoot) {
        return depthFromRoot + "." + id;
    }

    public List<String> processParentsInfo(SolrFolderEntity docFolder) {
        if (docFolder == null) {
            return Lists.newArrayList();
        }
        List<String> patentsInfoStr = docFolder.getParentsInfo();
        if (patentsInfoStr == null) {
            synchronized (entityCacheLock) {
                patentsInfoStr = calcAndSetParentsInfo(docFolder);
            }
        }
        return patentsInfoStr;
    }


    public void updateFoldersParentsInformation(long rootFolderId, Long jobId, ProgressTracker progressTracker) {
        logger.debug("Update parentsInfo in the folders - start rf={}", rootFolderId);
        try {
            jobManagerService.setOpMsg(jobId, "Update Folder Parents information");
            //First update all folders without parent
            solrFolderRepository.updateFoldersParentsInformationNoParent(rootFolderId);
        } catch (Exception e) {
            //TODO - break run!
            logger.error("Failed to update scan folders parents Information: " + e.getMessage(), e);
            opStateService.addCurrentPhaseError();
        }
        logger.debug("Update parentsInfo in the folders - finished rf={}", rootFolderId);
    }

    // TODO - can be done in SOLR plugin !!!!!!!!!
    private List<String> calcAndSetParentsInfo(SolrFolderEntity folderEntity) {
        // Recursive implementation - use parent data and join info
        cacheMisses++;

        Optional<SolrFolderEntity> parentFolderOp = solrFolderRepository.getOneById(folderEntity.getParentFolderId());
        List<String> parentInfo = new ArrayList<>();
        if (parentFolderOp.isPresent()) {
            parentInfo.addAll(parentFolderOp.get().getParentsInfo());
        }

        String newParentInfo = encodeSingleParentInfo(folderEntity);
        if (!parentInfo.contains(newParentInfo)) {
            parentInfo.add(newParentInfo);
        }

        solrFolderRepository.atomicUpdate(AtomicUpdateBuilder.create()
                .setId(CatFoldersFieldType.ID, folderEntity.getId())
                .addModifier(CatFoldersFieldType.PARENTS_INFO, SolrOperator.SET, parentInfo)
                .addModifier(CatFoldersFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                .build());

        solrFolderRepository.softCommitNoWaitFlush();

        if (logger.isTraceEnabled()) {
            logger.trace("calcAndSetParentsInfo({})={} calc#{}", folderEntity.getId(), parentInfo, cacheMisses);
        }
        return parentInfo;
    }

    @Transactional
    public int updateFolderParentsInfoPage() { //TODO - make this happen in SOLR plugin!!! This method is currently very inefficient!
        Query query = Query.create();
        query.addFilterWithCriteria(CatFoldersFieldType.PARENTS_INFO, SolrOperator.MISSING, "");
        query.addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));
        query.setRows(1000);
        List<SolrFolderEntity> docFolders = solrFolderRepository.find(query.build());

        if (docFolders == null) {
            return 0;
        }
        for (SolrFolderEntity docFolder : docFolders) {
            calcAndSetParentsInfo(docFolder);
        }
        logger.debug("Updated {} folders with parents info", docFolders.size());
        return docFolders.size();
    }

    @Transactional(readOnly = true)
    public int countFoldersUnderRootFolder(Long rootFolderId) {

        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));

        if (rootFolderId != null) {
            Criteria c = Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId);
            query.addFilterWithCriteria(c);
        }

        return solrFolderRepository.getTotalCount(query, CatFoldersFieldType.DELETED, "false").intValue();
    }

    public void deleteByRootFolderId(Long rootFolderId) {
        try {
            String deleteQueryString = CatFoldersFieldType.ROOT_FOLDER_ID.getSolrName() + ":" + rootFolderId;
            logger.trace("Delete by root folder id DocFolders: {}", deleteQueryString);
            solrFolderRepository.deleteByQuery(deleteQueryString);
        } catch (Exception e) {
            throw new RuntimeException("Could not delete DocFolders for root folder (" + rootFolderId + ")", e);
        }
    }

    @Transactional
    public void markDocFolderAsDeleted(Long docFolderId) {
        SolrInputDocument doc = SolrFolderBuilder.create()
                .setId(docFolderId)
                .setDeleted(true)
                .buildAtomicUpdate();
        solrFolderRepository.atomicUpdate(doc);
        solrFolderRepository.softCommitNoWaitFlush();
    }

    @Transactional
    public void markDocFoldersAsDeleted(Collection<Long> docFolderIds) {
        if (docFolderIds.size() > 0) {
            solrFolderRepository.updateFolderByIds(docFolderIds, CatFoldersFieldType.DELETED, Boolean.TRUE, SolrCommitType.SOFT_NOWAIT);
        }
    }

    public void markDocFolderAsNotDeleted(long id) {
        try {
            SolrInputDocument doc = SolrFolderBuilder.create()
                    .setId(id)
                    .setDeleted(false)
                    .buildAtomicUpdate();
            solrFolderRepository.atomicUpdate(doc);
            solrFolderRepository.softCommitNoWaitFlush();
        } catch (Exception e) {
            logger.debug("Exception during markDocFolderAsNotDeleted({}). {}", id, e, e);
        }
    }

    public void updateMailboxFolderProperties(DocFolderCacheValue docFolderCacheVal,
                                              String path, Long rootFolderId, String mailboxUpn) {
        DocFolder docFolder;
        DocFolderCacheKey docFolderCacheKey = new DocFolderCacheKey(path, rootFolderId);
        SolrFolderEntity entity = docFoldersToBeCreated.get(docFolderCacheKey);
        if (entity == null) {
            docFolder = getDocFolderWithRetries(docFolderCacheVal.docFolderId, true);
            if (docFolder != null && !docFolder.getFolderType().equals(FolderType.MAILBOX)) {
                SolrFolderBuilder builder = SolrFolderBuilder.create();
                builder.setType(FolderType.MAILBOX);
                builder.setMailboxUpn(mailboxUpn);
                SolrInputDocument doc = builder.buildAtomicUpdate();
                solrFolderRepository.atomicUpdate(doc);
            }
        } else {
            entity.setType(FolderType.MAILBOX.toInt());
            entity.setMailboxUpn(mailboxUpn);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateDocFolderProperties(Long docFolderId, ClaFilePropertiesDto claFileProp, Long runId,
                                          boolean newFolder, boolean retryIfMissing, long rootFolderId) {

        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);
        String fileName = claFileProp.getFileName();
        String rfRealPath = rf.getRealPath();
        String fileNameToAdd = (!Strings.isNullOrEmpty(rfRealPath) && fileName.startsWith(rfRealPath)) ?
                fileName.substring(rfRealPath.length()) : fileName;
        fileNameToAdd = DocFolderUtils.removeTrailingSlash(fileNameToAdd);

        DocFolder docFolder = null;
        DocFolderCacheKey docFolderCacheKey = new DocFolderCacheKey(fileNameToAdd, rootFolderId);
        SolrFolderEntity entity = docFoldersToBeCreated.get(docFolderCacheKey);
        if (entity == null) {
            docFolder = getDocFolderWithRetries(docFolderId, retryIfMissing);
        } else {
            docFolder = SolrCatFolderRepository.getFromEntity(entity);
        }

        if (docFolder == null) {
            //Workaround!
            docFolder = new DocFolder();
            docFolder.setId(docFolderId);
            fillDocFolderBaseProperties(rf, fileNameToAdd, docFolder);
        }
        String mediaItemId = claFileProp.getMediaItemId();
        String mediaItemIdStr = (!Strings.isNullOrEmpty(rfRealPath) && mediaItemId.startsWith(rfRealPath)) ?
                mediaItemId.substring(rfRealPath.length()) : mediaItemId;
        docFolder.setMediaEntityId(mediaItemIdStr);

        updateFolderAcls(docFolder, claFileProp, newFolder, runId);

        String folderRealPath = fileNameToAdd;
        if (folderRealPath != null && !folderRealPath.equals(docFolder.getRealPath())) {
            logger.warn("path for folder {} change was {} now {}", docFolderId, docFolder.getRealPath(), folderRealPath);
            updateDocFolderPath(docFolderId, folderRealPath, rootFolderId);
        }

        updateAclInSolr(docFolder, claFileProp, mediaItemIdStr, docFolderCacheKey);
    }

    /**
     * Update doc folder ACLs
     *
     * @param docFolder   doc folder
     * @param claFileProp file properties as received from MP or retrieved from DB
     * @param newFolder   true if this is a new folder, false if existing one
     * @param runId       run ID
     */
    private void updateFolderAcls(DocFolder docFolder, ClaFilePropertiesDto claFileProp, boolean newFolder, long runId) {
        if (claFileProp.getAclReadAllowed() != null) {
            docFolder.setAclSignature(calculateAclSignature(claFileProp));
        }
    }

    public void updateFolderAcls(long docFolderId, ClaFilePropertiesDto claFileProp, boolean newFolder, long runId) {
        if (claFileProp.getAclReadAllowed() != null) {
            DocFolder docFolder = solrFolderRepository.getById(docFolderId);
            if (docFolder == null) {
                logger.warn("Failed to updated ACLs on folder with ID {}, folder not found.", docFolderId);
            } else {
                updateFolderAcls(docFolder, claFileProp, newFolder, runId);
                updateAclInSolr(docFolder, claFileProp, null, null);
            }
        }
    }

    private void updateAclInSolr(DocFolder docFolder,
                                 ClaFilePropertiesDto claFileProp,
                                 String mediaItemId, DocFolderCacheKey docFolderCacheKey) {

        SolrFolderEntity entity = null;
        if (docFolderCacheKey != null) {
            entity = docFoldersToBeCreated.get(docFolderCacheKey);
        }

        String mailboxUpn = null;
        if (claFileProp instanceof Exchange365ItemDto) {
            Exchange365ItemDto mailProp = (Exchange365ItemDto) claFileProp;
            mailboxUpn = mailProp.getMailboxUpn();
        }

        if (entity != null) {
            if (mediaItemId != null) {
                entity.setMediaEntityId(mediaItemId);
            }
            entity.setAclSignature(docFolder.getAclSignature());
            entity.setAclDeniedRead(new ArrayList(claFileProp.getAclReadDenied()));
            entity.setAclDeniedWrite(new ArrayList(claFileProp.getAclWriteDenied()));
            entity.setAclRead(new ArrayList(claFileProp.getAclReadAllowed()));
            entity.setAclWrite(new ArrayList(claFileProp.getAclWriteAllowed()));
            if (mailboxUpn != null) {
                entity.setMailboxUpn(mailboxUpn);
            }
        } else {
            SolrFolderBuilder builder = SolrFolderBuilder.create()
                    .setId(docFolder.getId())
                    .setType(docFolder.getFolderType())
                    .setFolderHash(docFolder.getFolderHashed())
                    .setMediaEntityId(mediaItemId)
                    .setAclSignature(docFolder.getAclSignature())
                    .setAllowedReadPrincipalsNames(claFileProp.getAclReadAllowed())
                    .setAllowedWritePrincipalsNames(claFileProp.getAclWriteAllowed())
                    .setDeniedReadPrincipalsNames(claFileProp.getAclReadDenied())
                    .setDeniedWritePrincipalsNames(claFileProp.getAclWriteDenied());

            if (mailboxUpn != null) {
                builder.setMailboxUpn(mailboxUpn);
            }
            SolrInputDocument doc = builder.buildAtomicUpdate();
            solrFolderRepository.atomicUpdate(doc);
        }
    }

    private String calculateAclSignature(ClaFilePropertiesDto claFileProperties) {
        return claFileProperties.calculateAclSignature();
    }

    private DocFolder getDocFolderWithRetries(Long docFolderId, boolean retryIfMissing) {
        DocFolder docFolder = solrFolderRepository.getById(docFolderId);
        if (docFolder == null && retryIfMissing) {
            logger.warn("DocFolder id {} was not found in dataBase - retrying up to 3 times", docFolderId);
            for (int i = 0; i < 3; i++) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new StoppedByUserException(e);
                }
                docFolder = solrFolderRepository.getById(docFolderId);
                if (docFolder != null) {
                    return docFolder;
                }
            }
            logger.warn("DocFolder id {} was not found in dataBase after 3 retries", docFolderId);
        }
        return docFolder;
    }

    public List<DocFolder> getForRootFolderAndMailboxUpn(Long rootFolderId, String mailboxUpn, int offset, int pageSize) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.MAILBOX_UPN, SolrOperator.EQ, mailboxUpn))
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false"));

        return solrFolderRepository.getWithLimit(offset, pageSize, query);
    }

    public List<DocFolder> getForRootFolder(Long rootFolderId, int offset, int pageSize) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId));
        return solrFolderRepository.getWithLimit(offset, pageSize, query);
    }


    public List<SolrFolderEntity> getForRootFolderByDepth(Long rootFolderId, int depth) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.LT, depth))
                .addField(CatFoldersFieldType.ID)
                .addField(CatFoldersFieldType.PARENT_FOLDER_ID)
                .addField(CatFoldersFieldType.NAME)
                .addField(CatFoldersFieldType.REAL_PATH)
                .addField(CatFoldersFieldType.DEPTH_FROM_ROOT)
                .addSort(CatFoldersFieldType.DEPTH_FROM_ROOT, Sort.Direction.DESC);
        query.setRows(MAX_QUERY_ROWS_NUMBER);
        return solrFolderRepository.find(query.build());
    }


    public List<SolrFolderEntity> getByMinAndMaxDepth(int minDepth , int maxDepth) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.GTE, minDepth))
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.LT, maxDepth))
                .addField(CatFoldersFieldType.ID)
                .addField(CatFoldersFieldType.PARENT_FOLDER_ID)
                .addField(CatFoldersFieldType.NAME)
                .addField(CatFoldersFieldType.REAL_PATH)
                .addField(CatFoldersFieldType.DEPTH_FROM_ROOT)
                .addSort(CatFoldersFieldType.DEPTH_FROM_ROOT, Sort.Direction.DESC);
        query.setRows(MAX_QUERY_ROWS_NUMBER);
        return solrFolderRepository.find(query.build());
    }


    public List<DocFolder> getForRootFolderAndDepth(Long rootFolderId, int offset, int pageSize, int depth) {
        Query query = Query.create().
                addFilterWithCriteria(Criteria.create(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFoldersFieldType.DEPTH_FROM_ROOT, SolrOperator.EQ, depth));
        return solrFolderRepository.getWithLimit(offset, pageSize, query);
    }

    public List<SolrFolderEntity> getFolders(Query query) {
        return solrFolderRepository.find(query.build());
    }

    public DocFolder getFolderOfRootFolder(Long rootFolderId) {
        List<DocFolder> folders = getForRootFolderAndDepth(rootFolderId, 0, 30, 0);
        DocFolder rfFolder = null;
        if (folders != null && !folders.isEmpty()) {
            if (folders.size() == 1) {
                rfFolder = folders.get(0);
            } else {
                for (DocFolder f : folders) {
                    if (!f.isDeleted()) {
                        rfFolder = f;
                    }
                }
            }
        }
        return rfFolder;
    }

    @AutoAuthenticate
    public List<Long> getFoldersUnderRootFolder(Long rootFolderDtoId, int from, int count) {
        List<Long> result = new ArrayList<>();
        Query query = Query.create().addFilterWithCriteria(CatFoldersFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderDtoId);
        query.addFilterWithCriteria(CatFoldersFieldType.DELETED, SolrOperator.EQ, "false");
        query.addField(CatFoldersFieldType.ID);
        query.addSort(CatFoldersFieldType.ID, Sort.Direction.ASC);
        query.setStart(from).setRows(count);
        QueryResponse queryResponse = solrFolderRepository.runQuery(query.build());
        queryResponse.getResults().forEach(res -> {
            Long folder = Long.parseLong((String) res.getFieldValue(CatFoldersFieldType.ID.getSolrName()));
            result.add(folder);
        });
        return result;
    }

    @AutoAuthenticate
    public void deleteDocFoldersByRootFolder(RootFolder rootFolder) {
        logger.info("Deleting all DocFolders that belong to the rootFolder {}", rootFolder);
        deleteByRootFolderId(rootFolder.getId());
        solrFolderRepository.softCommitNoWaitFlush();
        logger.info("Finished deleting all docFolders under rootFolder {}", rootFolder);
    }

    public DocFolder updateDocFolderPath(long docFolderId, String newRealPath, long rootFolderId) {

        DocFolder updatedDocFolder = new DocFolder();
        updatedDocFolder.setId(docFolderId);
        updatedDocFolder.setRootFolderId(rootFolderId);
        updatedDocFolder.setRealPath(newRealPath);

        String universalPath = FileNamingUtils.convertPathToUniversalString(newRealPath);
        updatedDocFolder.setPath(universalPath);
        int absoluteDepth = DocFolderUtils.calculateDepth(universalPath);
        updatedDocFolder.setAbsoluteDepth(absoluteDepth);
        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);

        int rootDepth = rf.getBaseDepth();
        int depthFromRoot = absoluteDepth - rootDepth;
        updatedDocFolder.setDepthFromRoot(depthFromRoot);

        DocFolder persistedParent = null;
        Long parentFolderId = null;
        List<String> parentsInfo = null;
        if (depthFromRoot > 0) {
            String parentRealPath = DocFolderUtils.extractFolderFromPath(newRealPath);
            DocFolderCacheKey parentKey = new DocFolderCacheKey(parentRealPath, rootFolderId);
            DocFolderCacheValue parentFolder = docFolderIdsCache.getUnchecked(parentKey);
            parentFolderId = parentFolder.getDocFolderId();
            updatedDocFolder.setParentFolderId(parentFolderId);
            if (docFoldersToBeCreated.containsKey(parentKey)) {
                SolrFolderEntity parentFolderNotYetPersisted = docFoldersToBeCreated.get(parentKey);
                parentsInfo = calculateParentsInfo(
                        docFolderId, depthFromRoot, parentFolderNotYetPersisted.getParentsInfo());
            } else {
                persistedParent = solrFolderRepository.getById(parentFolder.getDocFolderId());
            }
        }
        if (parentsInfo == null) {
            List<String> parentParentsInfo = persistedParent != null ? persistedParent.getParentsInfo() : null;
            parentsInfo = calculateParentsInfo(docFolderId, depthFromRoot, parentParentsInfo);
        }
        updatedDocFolder.setParentsInfo(parentsInfo);

        DocFolderCacheKey key = new DocFolderCacheKey(newRealPath, rootFolderId);
        String folderHash = DocFolder.getHashedFolderString(universalPath, rootFolderId);
        updatedDocFolder.setFolderHashed(folderHash);

        SolrFolderEntity entity = docFoldersToBeCreated.get(key);
        if (entity != null) {
            entity.setPath(universalPath);
            entity.setRealPath(newRealPath);
            entity.setFolderHash(folderHash);
            entity.setParentFolderId(parentFolderId);
            entity.setParentsInfo(parentsInfo);
            entity.setAbsoluteDepth(absoluteDepth);
            entity.setDepthFromRoot(depthFromRoot);
        } else {
            SolrInputDocument doc = SolrFolderBuilder.create()
                    .setId(docFolderId)
                    .setPath(universalPath)
                    .setParentFolderId(parentFolderId)
                    .setParentInfo(parentsInfo)
                    .setFolderHash(folderHash)
                    .setRootDepth(rootDepth)
                    .setRootFolderPath(rf.getRealPath())
                    .buildAtomicUpdate();
            solrFolderRepository.atomicUpdate(doc);
            docFolderIdsCache.invalidate(key);
        }
        return updatedDocFolder;
    }

    private List<String> calculateParentsInfo(DocFolder docFolder, List<String> parentParentsInfo) {
        return calculateParentsInfo(docFolder.getId(), docFolder.getDepthFromRoot(), parentParentsInfo);
    }

    private List<String> calculateParentsInfo(Long folderId, int depthFromRoot, List<String> parentParentsInfo) {
        List<String> childParentInfo = new ArrayList<>();
        if (depthFromRoot > 0 && parentParentsInfo != null) {
            childParentInfo.addAll(parentParentsInfo);
        }
        String newParentInfo = encodeSingleParentInfo(folderId, depthFromRoot);
        if (!childParentInfo.contains(newParentInfo)) {
            childParentInfo.add(newParentInfo);
        }
        return childParentInfo;

    }

    @Deprecated
    public int rebuildDocFoldersPage(RootFolder rootFolder, Pageable pageRequest) {
        List<DocFolder> docFolders = getForRootFolder(rootFolder.getId(), (int) pageRequest.getOffset(), pageRequest.getPageSize());
        if (docFolders.size() == 0) {
            return 0;
        }
        logger.debug("Rebuilding {} docFolders on page {}", docFolders.size(), pageRequest);
        for (DocFolder docFolder : docFolders) {
            fillDocFolderTreeDetails(rootFolder, docFolder);
            SolrInputDocument doc = SolrFolderBuilder.create()
                    .setId(docFolder.getId())
                    .setPath(docFolder.getPath())
                    .buildAtomicUpdate();
            solrFolderRepository.atomicUpdate(doc);
            solrFolderRepository.softCommitNoWaitFlush();
        }
        return docFolders.size();
    }

    @Deprecated
    private void fillDocFolderTreeDetails(RootFolder rootFolder, DocFolder docFolder) {
        String realPath = docFolder.getRealPath();
        if (realPath == null) {
            logger.error("DocFolder {} has no path", docFolder);
            return;
        }
        String fullPath = FileNamingUtils.convertPathToUniversalString(rootFolder.getRealPath() + realPath);
        String universalPath = FileNamingUtils.convertPathToUniversalString(realPath);
        int absoluteDepth = DocFolderUtils.calculateDepth(fullPath);
        int rootDepth = rootFolder.getBaseDepth();
        int depthFromRoot = absoluteDepth - rootDepth;
        if (depthFromRoot < 0) {
            logger.error("DocFolder got depth < 0 (universalPath: {})", fullPath);
            return;
        }
        docFolder.setDepthFromRoot(depthFromRoot);
        docFolder.setAbsoluteDepth(absoluteDepth);
        docFolder.setPath(universalPath);
        docFolder.setParentsInfo(null); // this is malicious!!!!
        docFolder.updateHashedFolderString();
    }

    public Map<Long, Long> getFoldersSubFoldersNumber(Collection<Long> folderIds) {
        if (folderIds == null || folderIds.isEmpty()) return null;
        return solrFolderRepository.getFoldersSubFoldersNumber(folderIds);
    }

    public List<DocFolder> getChildrenDocFolders(List<String> parentsInfo) {
        Query query = Query.create();

        List<Criteria> fq = new ArrayList<>();
        for (String p : parentsInfo) {
            fq.add(Criteria.create(CatFoldersFieldType.PARENTS_INFO, SolrOperator.EQ, p));
        }
        query.addFilterWithCriteria(Criteria.and(fq));
        return solrFolderRepository.findAll(query);
    }

    public List<SolrFolderEntity> find(Set<Long> folderIds) {
        return solrFolderRepository.find(folderIds);
    }

    public List<SolrFolderEntity> getFoldersByClaFiles(List<ClaFile> claFiles) {
        Set<Long> docFolderIds = Sets.newHashSet();
        Set<Long> rootFolderIds = new HashSet<>();
        claFiles.forEach(claFile -> {
            rootFolderIds.add(claFile.getRootFolderId());
            if (claFile.getDocFolderId() != null) {
                docFolderIds.add(claFile.getDocFolderId());
            }
        });

        if (docFolderIds.isEmpty()) {
            logger.info("Root folder ids are are: {} and doc folder ids is empty for files {}", rootFolderIds, claFiles);
            return new ArrayList<>();
        }

        logger.trace("Root folder ids are are: {}", rootFolderIds);
        logger.trace("Doc folder ids are are: {}", docFolderIds);

        Criteria criteria = Criteria.create(CatFoldersFieldType.ID, SolrOperator.IN, docFolderIds);
        logger.debug("SolrQuery is: {}", Query.create().addFilterWithCriteria(criteria).build());

        return solrFolderRepository.getByFilterCriterias(criteria);
    }

    public List<SolrFolderEntity> getFoldersByEntities(List<SolrFileEntity> entities) {
        Set<Long> docFolderIds = Sets.newHashSet();
        Set<Long> rootFolderIds = Sets.newHashSet();
        entities.forEach(file -> {
            rootFolderIds.add(file.getRootFolderId());
            Optional.ofNullable(file.getFolderId())
                    .ifPresent(docFolderIds::add);
        });

        if (docFolderIds.isEmpty()) {
            logger.info("Root folder ids are are: {} and doc folder ids is empty for files {}", rootFolderIds, entities);
            return Lists.newArrayList();
        }

        logger.trace("Root folder ids are are: {}", rootFolderIds);
        logger.trace("Doc folder ids are are: {}", docFolderIds);

        Criteria criteria = Criteria.create(CatFoldersFieldType.ID, SolrOperator.IN, docFolderIds);
        logger.debug("SolrQuery is: {}", Query.create().addFilterWithCriteria(criteria).build());

        return solrFolderRepository.getByFilterCriterias(criteria);
    }


    public static class DocFolderCacheValue implements Serializable {

        private final long docFolderId;

        private final List<String> parentsInfo;        // Performance assist  property to enable list-of-parents ID caching

        private final String mediaId;

        public DocFolderCacheValue(SolrFolderEntity docFolder) {
            this.docFolderId = docFolder.getId();
            this.parentsInfo = docFolder.getParentsInfo();
            this.mediaId = docFolder.getMediaEntityId();
        }

        public DocFolderCacheValue(long docFolderId, List<String> parentsInfo, String mediaId) {
            this.docFolderId = docFolderId;
            this.parentsInfo = parentsInfo;
            this.mediaId = mediaId;
        }

        public long getDocFolderId() {
            return docFolderId;
        }


        public List<String> getParentsInfo() {
            return parentsInfo;
        }

        @Override
        public String toString() {
            return "DocFolderCacheValue{" +
                    "docFolderId=" + docFolderId +
                    ", parentsInfo='" + parentsInfo + '\'' +
                    ", mediaId='" + mediaId + '\'' +
                    '}';
        }
    }

    private class DocFolderCacheKey implements Serializable {

        private final String realPath;
        private final Long rootFolderId;

        DocFolderCacheKey(String realPath, Long rootFolderId) {
            this.realPath = realPath;
            this.rootFolderId = rootFolderId;
        }

        public String getRealPath() {
            return realPath;
        }

        public Long getRootFolderId() {
            return rootFolderId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DocFolderCacheKey that = (DocFolderCacheKey) o;

            return (realPath != null ? realPath.equals(that.realPath) : that.realPath == null)
                    && (rootFolderId != null ? rootFolderId.equals(that.rootFolderId) : that.rootFolderId == null);

        }

        @Override
        public int hashCode() {
            int result = realPath != null ? realPath.hashCode() : 0;
            result = 31 * result + (rootFolderId != null ? rootFolderId.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "DocFolderCacheKey{" +
                    "realPath='" + realPath + '\'' +
                    ", rootFolderId=" + rootFolderId +
                    '}';
        }
    }

    public static DocFolderDto convertDocFolder(DocFolder docFolder, boolean withTags, boolean hideHiddenTags, RootFolder rootFolder,
                                                Collection<AssociationEntity> tagAssociations,
                                                Map<Long, BasicScope> scopes,
                                                List<AssociableEntityDto> folderAssociationEntities) {

        String path = convertDocFolderPath(docFolder.getPath(), docFolder.getPathDescriptorType());
        DocFolderDto docFolderDto = new DocFolderDto(docFolder.getId(), docFolder.getName(), path, docFolder.getNumOfSubFolders());
        docFolderDto.setNumOfDirectFiles(docFolder.getNumOfDirectFiles());
        docFolderDto.setNumOfAllFiles(docFolder.getNumOfAllFiles());
        docFolderDto.setDepthFromRoot(docFolder.getDepthFromRoot());
        docFolderDto.setParentFolderId(docFolder.getParentFolderId());
        docFolderDto.setRealPath(docFolder.getRealPath());

        if (withTags) {
            List<AssociationEntity> docTypeAssociations = AssociationsService.getDocTypeAssociation(tagAssociations);
            docFolderDto.setFileTagAssociations(AssociationUtils.collectFileTagAssociations(scopes, folderAssociationEntities, tagAssociations, docTypeAssociations, hideHiddenTags));
            docFolderDto.setFileTagDtos(AssociationUtils.collectFileTags(tagAssociations, folderAssociationEntities, docFolderDto.getFileTagAssociations(), hideHiddenTags));
            docFolderDto.setDocTypeDtos(AssociationUtils.collectDocTypesDto(AssociationUtils.convertFromAssociationEntity(folderAssociationEntities, scopes, FileTagSource.FOLDER_EXPLICIT, docTypeAssociations)));
            docFolderDto.setAssociatedBizListItems(AssociationUtils.collectAssociatedBizListItemsFromFolder(tagAssociations, folderAssociationEntities));
            docFolderDto.setFunctionalRoles(AssociationUtils.collectAssociatedFunctionalRolesFromFolder(tagAssociations, folderAssociationEntities));
            docFolderDto.setDocTypeAssociations(AssociationUtils.collectDocTypeAssociations(docTypeAssociations, scopes, FileTagSource.FOLDER_EXPLICIT, folderAssociationEntities));
            docFolderDto.setDepartmentAssociationDtos(AssociationUtils.collectDepartmentAssociations(tagAssociations, folderAssociationEntities));
            docFolderDto.setDepartmentDto(AssociationUtils.getDepartmentDto(docFolderDto.getDepartmentAssociationDtos()));
        }

        docFolderDto.setRootFolderId(docFolder.getRootFolderId());
        if (rootFolder != null) {
            docFolderDto.setRootFolderPath(rootFolder.getRealPath());
            String rootFolderPath = rootFolder.getPath();
            if (rootFolderPath != null) {
                docFolderDto.setRootFolderLength(rootFolderPath.length());
            }
            docFolderDto.setMediaType(rootFolder.getMediaType());
            docFolderDto.setMailboxGroup(rootFolder.getMailboxGroup());

            if (!Strings.isNullOrEmpty(rootFolder.getNickName())) {
                docFolderDto.setRootFolderNickName(rootFolder.getNickName());
            } else if (!Strings.isNullOrEmpty(rootFolder.getMailboxGroup())) {
                docFolderDto.setRootFolderNickName(rootFolder.getMailboxGroup());
            }
        }
        docFolderDto.setMediaItemId(docFolder.getMediaEntityId());

        DocStoreDto docStoreDto = new DocStoreDto(0L, "", docFolder.getPathDescriptorType());
        docFolderDto.setDocStore(docStoreDto);
        docFolderDto.setFolderType(docFolder.getFolderType());
        docFolderDto.setMailboxUpn(docFolder.getMailboxUpn());
        docFolderDto.setParentsInfo(docFolder.getParentsInfo());
        return docFolderDto;
    }

    private static String convertDocFolderPath(String path, PathDescriptorType pathDescriptorType) {
        path = Optional.ofNullable(path).orElse("");
        if (URL_REGEX.matcher(path).matches()) {
            return path;
        }
        if (pathDescriptorType == null) {
            return FilenameUtils.separatorsToSystem(path);
        }
        switch (pathDescriptorType) {
            case WINDOWS:
                return FileNamingUtils.convertFromUnixToWindows(path);
            case LINUX:
                return FileNamingUtils.convertFromWindowsToUnix(path);
        }
        return path;
    }

    @Transactional(readOnly = true)
    public Long locateFolderByFullPath(String fullPath) {

        if (Strings.isNullOrEmpty(fullPath)) {
            throw new BadParameterException(messageHandler.getMessage("locate.folder.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        int index = fullPath.indexOf("/") > 0 ? fullPath.indexOf("/") :
                fullPath.indexOf("\\") > 0 ? fullPath.indexOf("\\") : fullPath.length();
        String possibleMailboxGroupName = fullPath.substring(0, index);

        List<String> paths = new ArrayList<>();
        String pathNormalized = DocFolderUtils.convertToAbsolute(MediaType.FILE_SHARE, fullPath);
        pathNormalized = DocFolderUtils.removeTrailingSlash(pathNormalized);
        do {
            paths.add(pathNormalized);
            pathNormalized = DocFolderUtils.removeTrailingSlash(FileNamingUtils.getParentDir(pathNormalized));
        } while (!Strings.isNullOrEmpty(pathNormalized));

        List<RootFolder> rfs = docStoreService.findByPaths(paths);
        List<RootFolder> rfByMailbox = new ArrayList<>();
        if (rfs == null || rfs.isEmpty()) {
            if (!Strings.isNullOrEmpty(possibleMailboxGroupName)) {
                rfByMailbox = docStoreService.findByMailboxGroup(possibleMailboxGroupName);
            }
        }
        if ((rfs == null || rfs.isEmpty()) && (rfByMailbox == null || rfByMailbox.isEmpty())) {
            throw new BadParameterException(messageHandler.getMessage("locate.folder.path-invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        RootFolder rf;
        DocFolder folder;
        String folderPathWithoutRf;
        if (rfs != null && !rfs.isEmpty()) {
            rf = rfs.get(0);
            folderPathWithoutRf = fullPath.substring(rf.getRealPath().length());
        } else {
            rf = rfByMailbox.get(0);
            folderPathWithoutRf = fullPath.substring(possibleMailboxGroupName.length());
        }
        folderPathWithoutRf = FileNamingUtils.convertPathToUniversalString(folderPathWithoutRf);
        folder = getDocFolderByFolderPath(rf.getId(), folderPathWithoutRf);
        if (folder == null) {
            throw new BadParameterException(messageHandler.getMessage("locate.folder.path-invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
        return folder.getId();
    }
}
