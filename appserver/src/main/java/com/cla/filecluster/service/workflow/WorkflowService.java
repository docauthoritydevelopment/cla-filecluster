package com.cla.filecluster.service.workflow;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.workflow.*;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.common.utils.JsonDtoConversionUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.workflow.Workflow;
import com.cla.filecluster.domain.entity.workflow.WorkflowFilterDescriptor;
import com.cla.filecluster.domain.entity.workflow.WorkflowHistory;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.jpa.workflow.WorkflowHistoryRepository;
import com.cla.filecluster.repository.jpa.workflow.WorkflowRepository;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.util.ValidationUtils;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.base.Strings;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * service for workflow management  (CRUD)
 *
 * Created by: yael
 * Created on: 2/21/2018
 */
@Service
public class WorkflowService {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowService.class);

    private static final String FILTER_DESCRIPTOR_UPDATE_ACTION_PATTERN = "Changed files to [action=%s,state=%s] according to filter %s";
    private static final String WORKFLOW_SUMMARY_NUM_OF_FILES_FIELD_NAME = "numOfFilesField";

    @Autowired
    private WorkflowRepository workflowRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private WorkflowHistoryRepository workflowHistoryRepository;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional
    public WorkflowDto createWorkflow(WorkflowDto workflowDto, String comment) {
        validateWorkflow(workflowDto);
        Workflow workflow = convertWorkflowDto(workflowDto);
        WorkflowDto dto = convertWorkflow(workflowRepository.save(workflow));
        saveHistory(dto.getId(), ActionType.CREATE, null, comment);
        return dto;
    }

    private void validateWorkflow(WorkflowDto workflowDto) {
        ValidationUtils.validateStringNotEmpty(workflowDto.getContactInfo(), messageHandler.getMessage("workflow.contact-info.empty"));
        ValidationUtils.validateEnumNotNull(workflowDto.getState(), messageHandler.getMessage("workflow.state.empty"));

        if (workflowDto.getWorkflowType() == null) {
            throw new BadRequestException(messageHandler.getMessage("workflow.type.empty"), BadRequestType.MISSING_FIELD);
        }

        if (workflowDto.getRequestDetails() == null || workflowDto.getRequestDetails().size() == 0) {
            throw new BadRequestException(messageHandler.getMessage("workflow.request-details.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    public List<Long> deleteWorkflowByIds(List<Long> workflowIds) {
        List<Long> failed = new ArrayList<>();
        workflowIds.forEach(id -> {
            try {
                deleteWorkflow(id);
            } catch (Exception e) {
                logger.error("failed delete Workflow", e);
                failed.add(id);
            }
        });
        return failed;
    }

    @Transactional
    public void deleteWorkflow(long id) {
        logger.debug("Delete Workflow {} ", id);
        workflowHistoryRepository.deleteByWorkflow(id);
        workflowRepository.deleteById(id);
    }

    private String objToString(Object obj) {
        return obj == null ? null : obj.toString();
    }

    @Transactional
    public WorkflowDto updateWorkflow(Long workflowId, WorkflowDto workflowDto, String comment) {
        Workflow one = assertWorkflowExistsAndFetch(workflowId);
        logger.debug("Update Workflow, before: {}", one);
        if (workflowDto.getAssignee() != null && !workflowDto.getAssignee().equals(one.getAssignee())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.ASSIGNEE, objToString(one.getAssignee()), objToString(workflowDto.getAssignee()), comment);
            one.setAssignee(workflowDto.getAssignee());
        }
        if (workflowDto.getAssignedWorkgroup() != null && !workflowDto.getAssignedWorkgroup().equals(one.getAssignedWorkgroup())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.ASSIGNEE_WORKGROUP, objToString(one.getAssignedWorkgroup()), objToString(workflowDto.getAssignedWorkgroup()), comment);
            one.setAssignedWorkgroup(workflowDto.getAssignedWorkgroup());
        }
        if (workflowDto.getContactInfo() != null && !workflowDto.getContactInfo().equals(one.getContactInfo())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.CONTACT_INFO, one.getContactInfo(), workflowDto.getContactInfo(), comment);
            one.setContactInfo(workflowDto.getContactInfo());
        }
        if (workflowDto.getCurrentStateDueDate() != null && !workflowDto.getCurrentStateDueDate().equals(one.getCurrentStateDueDate())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.CURRENT_STATE_DUE_DATE, objToString(one.getCurrentStateDueDate()), objToString(workflowDto.getCurrentStateDueDate()), comment);
            one.setCurrentStateDueDate(workflowDto.getCurrentStateDueDate());
        }
        if (workflowDto.getDateClosed() != null && !workflowDto.getDateClosed().equals(one.getDateClosed())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.DATE_CLOSED, objToString(one.getDateClosed()), objToString(workflowDto.getDateClosed()), comment);
            one.setDateClosed(workflowDto.getDateClosed());
        }
        if (workflowDto.getDueDate() != null && !workflowDto.getDueDate().equals(one.getDueDate())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.DUE_DATE, objToString(one.getDueDate()), objToString(workflowDto.getDueDate()), comment);
            one.setDueDate(workflowDto.getDueDate());
        }
        if (workflowDto.getFilters() != null && !workflowDto.getFilters().isEmpty()) {
            logger.debug("Updating filter is via different API, ignoring update update filters for {}", workflowDto);
        }
        if (workflowDto.getPriority() != null && !workflowDto.getPriority().equals(one.getPriority())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.PRIORITY, objToString(one.getPriority()), objToString(workflowDto.getPriority()), comment);
            one.setPriority(workflowDto.getPriority());
        }
        if (workflowDto.getRequestDetails() != null && workflowDto.getRequestDetails().size() > 0 && !workflowDto.getRequestDetails().equals(one.getRequestDetails())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.REQUEST_DETAILS, objToString(one.getRequestDetails()), objToString(workflowDto.getRequestDetails()), comment);
            one.setRequestDetails(workflowDto.getRequestDetails());
        }
        if (workflowDto.getState() != null && !workflowDto.getState().equals(one.getState())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.STATE, objToString(one.getState()), objToString(workflowDto.getState()), comment);
            one.setState(workflowDto.getState());
            one.setDateLastStateChange(System.currentTimeMillis());
        }
        if (workflowDto.getWorkflowType() != null && !workflowDto.getWorkflowType().equals(one.getWorkflowType())) {
            addWorkflowHistoryForUpdate(one.getId(), WorkflowField.TYPE, one.getWorkflowType().name(), workflowDto.getWorkflowType().name(), comment);
            one.setWorkflowType(workflowDto.getWorkflowType());
        }
        logger.debug("Update Workflow, after: {}", one);
        return convertWorkflow(workflowRepository.save(one));
    }

    private void addWorkflowHistoryForUpdate(Long id, WorkflowField field, String before, String after, String comment) {
        ActionType type;
        if (field.equals(WorkflowField.STATE)) {
            type = ActionType.STATE_CHANGE;
        } else {
            type = ActionType.UPDATE_FIELD;
        }
        WorkflowHistoryChangeDescription desc = new WorkflowHistoryChangeDescription();
        desc.setField(field);
        desc.setValueBefore(before);
        desc.setValueAfter(after);
        saveHistory(id, type, desc, comment);
    }

    @Transactional(readOnly = true)
    public WorkflowDto getWorkflow(Long workflowId) {
        return workflowRepository.findById(workflowId)
                .map(WorkflowService::convertWorkflow)
                .orElse(null);
    }

    @Transactional(readOnly = true)
    public List<WorkflowDto> getWorkflowByIds(Collection<Long> workflowIds) {
        return convertWorkflows(workflowRepository.findAllById(workflowIds));
    }

    @Transactional(readOnly = true)
    public Page<WorkflowDto> listWorkflows(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        String workflowTypeStr = params.get("type");
        String workflowStatesStr = params.get("states");
        Page<WorkflowDto> workflowDtoPage;
        if (Strings.isNullOrEmpty(workflowTypeStr) && Strings.isNullOrEmpty(workflowStatesStr)) {
            workflowDtoPage = listWorkflows(dataSourceRequest);
        } else if (Strings.isNullOrEmpty(workflowStatesStr)) {
            WorkflowType workflowType = WorkflowType.valueOf(workflowTypeStr);
            workflowDtoPage = getWorkflowByType(dataSourceRequest, workflowType);
        } else if (Strings.isNullOrEmpty(workflowTypeStr)) {
            List<WorkflowState> workflowStates = extractWorkflowStates(workflowStatesStr);
            workflowDtoPage = getWorkflowByStates(dataSourceRequest, workflowStates);
        } else {
            WorkflowType workflowType = WorkflowType.valueOf(workflowTypeStr);
            List<WorkflowState> workflowStates = extractWorkflowStates(workflowStatesStr);
            workflowDtoPage = getWorkflowByTypeAndState(dataSourceRequest, workflowType, workflowStates);
        }
        enrichWorkflowPageWithSummary(workflowDtoPage.getContent());
        return workflowDtoPage;
    }

    private List<WorkflowState> extractWorkflowStates(String workflowStatesStr) {
        String[] states = workflowStatesStr.split(",");
        return Stream.of(states)
                .map(s -> NumberUtils.isNumber(s) ?
                        WorkflowState.values()[Integer.valueOf(s)] : WorkflowState.valueOf(s)).collect(Collectors.toList());
    }

    private void enrichWorkflowPageWithSummary(List<WorkflowDto> workflowDtos) {
        if (workflowDtos != null && !workflowDtos.isEmpty()) {
            FilterDescriptor filterDescriptor = new FilterDescriptor();
            filterDescriptor.setField(CatFileFieldType.WORKFLOW_DATA.getSolrName());
            filterDescriptor.setOperator("EQ");
            filterDescriptor.setValue(workflowIdsFilter(workflowDtos));
            SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(filterDescriptor, CatFileFieldType.WORKFLOW_DATA);
            SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrSpecification, null);
            FacetField facetField = solrFacetQueryResponse.getFirstResult();
            Table<Long, Object, Long> workflowFacetSummaryTable = buildWorkflowFacetSummaryTable(facetField);
            workflowDtos.forEach(wf -> enrichSingleWorkflow(wf, workflowFacetSummaryTable));
        }
    }

    private String workflowIdsFilter(List<WorkflowDto> workflowDtos) {
        return workflowDtos.stream()
                .map(workflowDto -> String.valueOf(workflowDto.getId()) + FileCatUtils.CATEGORY_SEPARATOR + "*")
                .collect(Collectors.joining(" or ", "(", ")"));
    }

    private Table<Long, Object, Long> buildWorkflowFacetSummaryTable(FacetField facetField) {
        Table<Long, Object, Long> result = HashBasedTable.create();
        for (FacetField.Count count : facetField.getValues()) {
            String wfData = count.getName();
            WorkflowFileData workflowFileData = AssociationIdUtils.extractWorkflowFileDataDto(wfData);
            Long workflowId = AssociationIdUtils.extractWorkflowId(wfData);
            if (count.getCount() > 0) {
                createOrUpdateWorkflowSummaryTable(result, workflowId, WORKFLOW_SUMMARY_NUM_OF_FILES_FIELD_NAME, count.getCount());
                if (workflowFileData.getFileAction() != null) {
                    createOrUpdateWorkflowSummaryTable(result, workflowId, workflowFileData.getFileAction(), count.getCount());
                }
                if (workflowFileData.getFileState() != null) {
                    createOrUpdateWorkflowSummaryTable(result, workflowId, workflowFileData.getFileState(), count.getCount());
                }
            }
        }
        return result;
    }

    private void createOrUpdateWorkflowSummaryTable(Table<Long, Object, Long> result, Long workflowId, Object fieldValue, long count) {
        Long currentCount = result.get(workflowId, fieldValue);
        if (currentCount == null) {
            result.put(workflowId, fieldValue, count);
        } else {
            result.put(workflowId, fieldValue, currentCount + count);
        }
    }


    private String constructWorkflowIdValueByPage(Page<WorkflowDto> workflowsPage) {
        StringBuilder sb = new StringBuilder("(");
        workflowsPage.forEach(wf -> sb
                .append(wf.getId())
                .append(FileCatUtils.CATEGORY_SEPARATOR)
                .append("*")
                .append(" or ")
        );
        return sb.delete(sb.lastIndexOf(" or "), sb.length() - 1)
                .append(")").toString();
    }

    private void enrichSingleWorkflow(WorkflowDto workflowDto, Table<Long, Object, Long> workflowFacetSummaryTable) {
        WorkflowSummaryDto workflowSummaryDto = new WorkflowSummaryDto();

        // Prepare action & state count maps
        Map<FileAction, Long> fileActionCountMap = new HashMap<>();
        Map<FileState, Long> fileStateCountMap = new HashMap<>();
        workflowFacetSummaryTable.row(workflowDto.getId()).forEach((fieldValue, count) -> {
            if (fieldValue instanceof FileAction) {
                fileActionCountMap.put((FileAction) fieldValue, count);
            } else if (fieldValue instanceof FileState) {
                fileStateCountMap.put((FileState) fieldValue, count);
            }
        });

        // Set all workflow summary data
        workflowSummaryDto.setFileActionCount(fileActionCountMap);
        workflowSummaryDto.setFileStateCount(fileStateCountMap);
        Long numOfFiles = workflowFacetSummaryTable.get(workflowDto.getId(), WORKFLOW_SUMMARY_NUM_OF_FILES_FIELD_NAME);
        if (numOfFiles != null) {
            workflowSummaryDto.setNumOfFiles(numOfFiles);
        }

        // Update the dto with it
        workflowDto.setWorkflowSummary(workflowSummaryDto);
    }

    @NotNull
    private Page<WorkflowDto> listWorkflows(DataSourceRequest dataSourceRequest) {
        logger.debug("List all workflow items");
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Workflow> workflows;
        workflows = workflowRepository.findAll(pageRequest);
        List<WorkflowDto> res = convertWorkflows(workflows.getContent());
        return new PageImpl<>(res, pageRequest, workflows.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<WorkflowDto> getWorkflowByType(DataSourceRequest dataSourceRequest, WorkflowType workflowType) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Workflow> workflows = workflowRepository.getByWorkflowType(workflowType, pageRequest);
        List<WorkflowDto> res = convertWorkflows(workflows.getContent());
        return new PageImpl<>(res, pageRequest, workflows.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<WorkflowDto> getWorkflowByStates(DataSourceRequest dataSourceRequest, List<WorkflowState> states) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Workflow> workflows = workflowRepository.getByWorkflowStates(states, pageRequest);
        List<WorkflowDto> res = convertWorkflows(workflows.getContent());
        return new PageImpl<>(res, pageRequest, workflows.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<WorkflowDto> getWorkflowByTypeAndState(DataSourceRequest dataSourceRequest, WorkflowType workflowType, List<WorkflowState> states) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<Workflow> workflows = workflowRepository.getByWorkflowTypeAndStates(workflowType, states, pageRequest);
        List<WorkflowDto> res = convertWorkflows(workflows.getContent());
        return new PageImpl<>(res, pageRequest, workflows.getTotalElements());
    }

    @Transactional(readOnly = true)
    public Page<WorkflowHistoryDto> getWorkflowHistory(DataSourceRequest dataSourceRequest, Long workflowId) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<WorkflowHistory> workflowHistories = workflowHistoryRepository.getByWorkflow(workflowId, pageRequest);
        List<WorkflowHistoryDto> res = convertWorkflowHistories(workflowHistories.getContent());
        return new PageImpl<>(res, pageRequest, workflowHistories.getTotalElements());
    }


    private WorkflowHistoryDto saveHistory(Long workflowId, ActionType type, WorkflowHistoryChangeDescription desc, String comment) {
        WorkflowHistory wh = new WorkflowHistory();
        wh.setWorkflowId(workflowId);
        wh.setActionType(type);
        wh.setChangeDescription(desc);
        wh.setComment(comment);
        String username = userService.getPrincipalUsername();
        wh.setActor(username);
        return convertWorkflowHistory(workflowHistoryRepository.save(wh));
    }

    @Transactional
    public WorkflowFilterDescriptorDto saveWorkflowFilter(long workflowId, WorkflowFilterDescriptorDto workflowFilterDescriptorDto, String comment) {
        Workflow workflow = assertWorkflowExistsAndFetch(workflowId);

        // Try to find existing descriptor
        Optional<WorkflowFilterDescriptor> workflowFilterDescriptorOptional = extractWorkflowFilter(workflow, workflowFilterDescriptorDto);
        WorkflowFilterDescriptor workflowFilterDescriptor;

        // Case present then update workflow only if file data is different
        if (workflowFilterDescriptorOptional.isPresent()) {
            workflowFilterDescriptor = workflowFilterDescriptorOptional.get();
            // If update is the same then do nothing
            if (workflowFilterDescriptor.getWorkflowFileData().equals(workflowFilterDescriptorDto.getWorkflowFileData())) {
                logger.info("{} with {} already exists, do nothing", workflowFilterDescriptor.getFilter(), workflowFilterDescriptor.getWorkflowFileData());
                return convertWorkflowFilterDescriptor(workflowFilterDescriptor);
            }
            // Otherwise save the new file data and add it to history
            else {
                assertValidWorkflowDataForUpdate(workflowFilterDescriptorDto.getWorkflowFileData());
                // Extract existing filter descriptor for history beofre
                WorkflowFilterDescriptorDto oldWorkflowFilterDescriptorDto = convertWorkflowFilterDescriptor(workflowFilterDescriptor);

                // Update and save filter descriptor with new file data
                workflowFilterDescriptor.setWorkflowFileData(workflowFilterDescriptorDto.getWorkflowFileData());
                workflowFilterDescriptor.setDateModified(System.currentTimeMillis());
                workflow = workflowRepository.save(workflow);

                // Extract updated filter descriptor for history after and add history operation
                workflowFilterDescriptorDto = convertWorkflowFilterDescriptor(workflowFilterDescriptor);
                addWorkflowHistoryForUpdate(workflowId, WorkflowField.FILTER,
                        JsonDtoConversionUtils.convertObjToJson(oldWorkflowFilterDescriptorDto),
                        JsonDtoConversionUtils.convertObjToJson(workflowFilterDescriptorDto), comment);
            }
        }
        // Case filter descriptor does not exists then create a new one
        else {
            long now = System.currentTimeMillis();
            workflowFilterDescriptor = new WorkflowFilterDescriptor();
            workflowFilterDescriptor.setFilter(workflowFilterDescriptorDto.getFilter());
            workflowFilterDescriptor.setDateCreated(now);
            workflowFilterDescriptor.setDateModified(now);
            workflowFilterDescriptor.setWorkflowFileData(workflowFilterDescriptorDto.getWorkflowFileData());
            workflow.getFilters().add(workflowFilterDescriptor);
            workflow = workflowRepository.save(workflow);
            workflowFilterDescriptor = extractWorkflowFilter(workflow, workflowFilterDescriptorDto).get();
            workflowFilterDescriptorDto = convertWorkflowFilterDescriptor(workflowFilterDescriptor);
            addWorkflowHistoryForUpdate(workflowId, WorkflowField.FILTER, null,
                    JsonDtoConversionUtils.convertObjToJson(workflowFilterDescriptorDto), comment);
        }

        // If reached here means we need to update all relevant files by filter with given workflow data
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(workflowFilterDescriptorDto.getFilter());
        updateFilesWorkflowDataBySpecification(workflow.getId(), workflowFilterDescriptorDto.getWorkflowFileData(), solrSpecification);

        // Return the updated filter descriptor
        return workflowFilterDescriptorDto;
    }

    private void assertValidWorkflowDataForUpdate(WorkflowFileData workflowFileData) {
        if (workflowFileData.getFileAction() == null && workflowFileData.getFileState() == null) {
            throw new RuntimeException(messageHandler.getMessage("workflow.file.state-and-action.empty"));
        }
    }


    @NotNull
    private Workflow assertWorkflowExistsAndFetch(long workflowId) {
        Workflow workflow = workflowRepository.findById(workflowId).orElse(null);

        // Validate workflow exists
        if (workflow == null) {
            throw new RuntimeException(messageHandler.getMessage("workflow.missing"));
        }
        return workflow;
    }


    private Optional<WorkflowFilterDescriptor> extractWorkflowFilter(Workflow workflow, WorkflowFilterDescriptorDto workflowFilterDescriptorDto) {
        return workflow.getFilters().stream()
                .filter(f -> f.getFilter().equals(workflowFilterDescriptorDto.getFilter()))
                .findFirst();
    }

    @Transactional
    public WorkflowDto clearAllFilters(long workflowId, String comment) {
        Workflow workflow = assertWorkflowExistsAndFetch(workflowId);
        List<WorkflowFilterDescriptor> filters = new ArrayList<>(workflow.getFilters());
        String valueBefore = filters.stream()
                .map(f -> JsonDtoConversionUtils.convertFilterDescriptorToJson(f.getFilter()))
                .collect(Collectors.joining(",", "[", "]"));
        addWorkflowHistoryForUpdate(workflowId, WorkflowField.FILTER, valueBefore, null, comment);
        workflow.getFilters().clear();
        workflow = workflowRepository.save(workflow);
        filters.forEach(f -> removeFilterFromSolr(workflowId, f));
        return convertWorkflow(workflow);
    }

    @Transactional
    public WorkflowFilterDescriptorDto deleteWorkflowFilter(long workflowId, long filterId, String comment) {
        Workflow workflow = assertWorkflowExistsAndFetch(workflowId);

        // Extract matching workflow descriptor
        Optional<WorkflowFilterDescriptor> workflowFilterDescriptorOptional = workflow.getFilters().stream()
                .filter(wfd -> wfd.getId().equals(filterId))
                .findFirst();

        if (workflowFilterDescriptorOptional.isPresent()) { // Case present then remove it from filters and update solr
            WorkflowFilterDescriptor workflowFilterDescriptor = workflowFilterDescriptorOptional.get();
            workflow.getFilters().remove(workflowFilterDescriptor);
            workflow = workflowRepository.save(workflow);
            addWorkflowHistoryForUpdate(workflowId, WorkflowField.FILTER, JsonDtoConversionUtils.convertFilterDescriptorToJson(workflowFilterDescriptor.getFilter()), null, comment);
            removeFilterFromSolr(workflow.getId(), workflowFilterDescriptor);
            return convertWorkflowFilterDescriptor(workflowFilterDescriptor);
        } else { // Do nothing just place a warning
            logger.warn("Did not find filter by id: {}", filterId);
            return null;
        }
    }

    private void removeFilterFromSolr(long workflowId, WorkflowFilterDescriptor workflowFilterDescriptor) {
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(workflowFilterDescriptor.getFilter());
        fileCatService.updateFieldBySolrSpecification(solrSpecification, CatFileFieldType.WORKFLOW_DATA, SolrFieldOp.REMOVE, String.valueOf(workflowId), true);
    }


    @Transactional
    public void updateFilesWorkflowDataByFilter(long workflowId, WorkflowFilterDescriptorDto workflowFilterDescriptorDto, String comment, Map<String, String> additionalParams) {
        assertWorkflowExistsAndFetch(workflowId);
        assertValidWorkflowDataForUpdate(workflowFilterDescriptorDto.getWorkflowFileData());

        // Build solr spec
        Map<String, String> params = new HashMap<>();
        params.put(DataSourceRequest.WORKFLOW_ID, String.valueOf(workflowId));
        params.put(DataSourceRequest.FILTER, JsonDtoConversionUtils.convertFilterDescriptorToJson(workflowFilterDescriptorDto.getFilter()));
        params.putAll(additionalParams);

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);

        // Execute solr update
        updateFilesWorkflowDataBySpecification(workflowId, workflowFilterDescriptorDto.getWorkflowFileData(), solrSpecification);
        addWorkflowHistoryForUpdate(workflowId, WorkflowField.FILE_DATA, null, JsonDtoConversionUtils.convertObjToJson(workflowFilterDescriptorDto), comment);
    }

    public static WorkflowFilterDescriptorDto convertWorkflowFilterDescriptor(WorkflowFilterDescriptor workflowFilterDescriptor) {
        if (workflowFilterDescriptor == null){
            return null;
        }
        WorkflowFilterDescriptorDto workflowFilterDescriptorDto = new WorkflowFilterDescriptorDto();
        workflowFilterDescriptorDto.setId(workflowFilterDescriptor.getId());
        workflowFilterDescriptorDto.setWorkflowFileData(workflowFilterDescriptor.getWorkflowFileData());
        workflowFilterDescriptorDto.setFilter(workflowFilterDescriptor.getFilter());
        workflowFilterDescriptorDto.setDateCreated(workflowFilterDescriptor.getDateCreated());
        workflowFilterDescriptorDto.setDateModified(workflowFilterDescriptor.getDateModified());
        return workflowFilterDescriptorDto;
    }

    public static WorkflowDto convertWorkflow(Workflow workflow) {
        if (workflow == null) return null;
        WorkflowDto result = new WorkflowDto();
        result.setAssignee(workflow.getAssignee());
        result.setAssignedWorkgroup(workflow.getAssignedWorkgroup());
        result.setContactInfo(workflow.getContactInfo());
        result.setFilters(workflow.getFilters().stream()
                .map(WorkflowService::convertWorkflowFilterDescriptor)
                .collect(Collectors.toList()));
        result.setCurrentStateDueDate(workflow.getCurrentStateDueDate());
        result.setDateClosed(workflow.getDateClosed());
        result.setDateLastStateChange(workflow.getDateLastStateChange());
        result.setDueDate(workflow.getDueDate());
        result.setId(workflow.getId());
        result.setPriority(workflow.getPriority());
        result.setRequestDetails(workflow.getRequestDetails());
        result.setState(workflow.getState());
        result.setDateCreated(workflow.getDateCreated());
        result.setDateModified(workflow.getDateModified());
        result.setWorkflowType(workflow.getWorkflowType());
        return result;
    }

    public static List<WorkflowDto> convertWorkflows(Collection<Workflow> workflows) {
        List<WorkflowDto> result = new ArrayList<>();
        for (Workflow workflow : workflows) {
            result.add(convertWorkflow(workflow));
        }
        return result;
    }

    private void updateFilesWorkflowDataBySpecification(long workflowId, WorkflowFileData workflowFileData, SolrSpecification solrSpecification) {
        String workFlowIdentifier = AssociationIdUtils.createWorkFlowIdentifier(workflowId, workflowFileData.getFileAction(), workflowFileData.getFileState());
        fileCatService.updateFieldBySolrSpecification(solrSpecification, CatFileFieldType.WORKFLOW_DATA, SolrFieldOp.SET, workFlowIdentifier, true);
    }

    public Map<WorkflowState, Integer> countWorkflowsByState() {
        List<WorkflowRepository.WorkflowStateCount> workflowStateCounts = workflowRepository.countWorkflowsByState();
        TreeMap<WorkflowState, Integer> result = new TreeMap<>(Comparator.comparingInt(Enum::ordinal));
        workflowStateCounts.forEach(c -> result.put(c.getState(), c.getCount()));
        return result;
    }

    public Map<FileAction, Long> findFileCountByAction(long workflowId, Map<String, String> params) {
        // Place the workflow id in the params to prepare datasource request by this workflow id
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.WORKFLOW_DATA);
        solrSpecification.setFacetPrefix(workflowId + FileCatUtils.CATEGORY_SEPARATOR);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(solrSpecification, null);
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        // Group by file action & sum each action count
        return facetField.getValues().stream()
                .map(this::extractFileActionCountPair).filter(p -> p.getKey() != null)
                .collect(Collectors.groupingBy(p -> p.getKey(), Collectors.summingLong(Pair::getValue)));

    }

    private Pair<FileAction, Long> extractFileActionCountPair(FacetField.Count count) {
        String name = count.getName();
        return Pair.of(AssociationIdUtils.extractWorkflowFileDataDto(name).getFileAction(), count.getCount());
    }

    public static Workflow convertWorkflowDto(WorkflowDto workflowDto) {
        if (workflowDto == null) return null;
        Workflow result = new Workflow();
        result.setAssignee(workflowDto.getAssignee());
        result.setAssignedWorkgroup(workflowDto.getAssignedWorkgroup());
        result.setContactInfo(workflowDto.getContactInfo());
        result.setCurrentStateDueDate(workflowDto.getCurrentStateDueDate());
        result.setDateClosed(workflowDto.getDateClosed());
        result.setDateLastStateChange(workflowDto.getDateLastStateChange());
        result.setDueDate(workflowDto.getDueDate());
        result.setId(workflowDto.getId());
        result.setPriority(workflowDto.getPriority());
        result.setRequestDetails(workflowDto.getRequestDetails());
        result.setState(workflowDto.getState());
        result.setWorkflowType(workflowDto.getWorkflowType());
        return result;
    }

    public static List<WorkflowHistoryDto> convertWorkflowHistories(Collection<WorkflowHistory> workflowHistories) {
        List<WorkflowHistoryDto> result = new ArrayList<>();
        for (WorkflowHistory workflowHistory : workflowHistories) {
            result.add(convertWorkflowHistory(workflowHistory));
        }
        return result;
    }

    public static WorkflowHistoryDto convertWorkflowHistory(WorkflowHistory workflowHistory) {
        if (workflowHistory == null) return null;
        WorkflowHistoryDto result = new WorkflowHistoryDto();
        result.setActionType(workflowHistory.getActionType());
        result.setActor(workflowHistory.getActor());
        result.setDateCreated(workflowHistory.getDateCreated());
        result.setDateModified(workflowHistory.getDateModified());
        result.setId(workflowHistory.getId());
        result.setWorkflowId(workflowHistory.getWorkflowId());
        result.setComment(workflowHistory.getComment());
        if (workflowHistory.getChangeDescription() != null) {
            if (workflowHistory.getChangeDescription().getField() != null) {
                result.setFieldName(workflowHistory.getChangeDescription().getField().getName());
                result.setFieldType(workflowHistory.getChangeDescription().getField().getClassName());
                result.setValueAfter(workflowHistory.getChangeDescription().getValueAfter());
                result.setValueBefore(workflowHistory.getChangeDescription().getValueBefore());
            }
        }
        return result;
    }

}