package com.cla.filecluster.service.export.exporters.security.funcrole;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public interface FunctionalRoleHeaderFields {
    String NAME = "Name";
    String DESCRIPTION = "Description";
    String MNG_USER = "Manager username";
    String LDAP_GROUP = "LDAP group";
    String ROLE_TEMPLATE = "Role template";
}
