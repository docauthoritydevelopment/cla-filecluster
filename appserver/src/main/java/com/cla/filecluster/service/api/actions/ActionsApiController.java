package com.cla.filecluster.service.api.actions;

import com.cla.common.domain.dto.action.ActionDefinitionDetailsDto;
import com.cla.common.domain.dto.action.ActionDefinitionDto;
import com.cla.common.domain.dto.action.ActionExecutionTaskStatusDto;
import com.cla.common.domain.dto.action.DispatchActionRequestDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.actions.ActionExecutorService;
import com.cla.filecluster.service.actions.ActionsAppService;
import com.cla.filecluster.service.actions.ActionsAppService.ActionQueueingResult;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.filter.SavedFilterService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.LocalFileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;
import java.util.zip.ZipOutputStream;

/**
 * Actions api - creating scripts to perform actions on files in the system
 * the scripts can then be ran by the sys admin on the relevant server
 * e.g encrypt/decrypt/change permissions etc
 * <p>
 * Notice, this api used to contain executions for specific files, groups and folders
 * All of this can be done using the filter option which is the only one currently used bu ui
 * To prevent work maintaining unused code, it was removed
 */
@RestController
@RequestMapping("/api/action")
public class ActionsApiController {

    public static final String PARAMETER_PREFIX = "parameter.";

    @Autowired
    private ActionsAppService actionsAppService;

    @Autowired
    private UserService userService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private ActionExecutorService actionExecutorService;

    private Logger logger = LoggerFactory.getLogger(ActionsApiController.class);


    @RequestMapping(value = "/definition", method = RequestMethod.GET)
    public Page<ActionDefinitionDto> listActionDefinitions(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return actionsAppService.listActionDefinitions(dataSourceRequest);
    }

    @RequestMapping("/definition/reload")
    public void reloadDefaultActionConfiguration() {
        actionsAppService.loadDefaultActionConfiguration();
    }

    @RequestMapping(value = "/definition/find", method = RequestMethod.GET)
    public ActionDefinitionDto findAction(@RequestParam(required = false) String name, @RequestParam(required = false) String id) {
        return actionsAppService.findAction(id, name);
    }

    @RequestMapping(value = "/definition/{actionId}/parameters", method = RequestMethod.GET)
    public ActionDefinitionDetailsDto listActionParamters(@PathVariable String actionId) {
        return actionsAppService.listActionParamters(actionId);
    }

    @RequestMapping(value = "/execute/filter", method = RequestMethod.POST)
    public ActionExecutionTaskStatusDto executeActionOnFilter(
            @RequestParam Map<String, String> params, @RequestBody DispatchActionRequestDto dispatchActionRequestDto) {

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        FilterDescriptor filter = dataSourceRequest.getFilter();
        String actionDefinitionId = dispatchActionRequestDto.getActionDefinitionId();
        Properties actionParams = dispatchActionRequestDto.getParameters();
        Long userId = getCurrentUser();
        boolean wait = dataSourceRequest.isWait();

        ActionQueueingResult actionQueueingResult =
                actionsAppService.queueActionOnFilter(filter, actionDefinitionId, actionParams, userId, false);

        actionExecutorService.dispatchAndStartActionExecution(wait, filter, actionParams, actionQueueingResult, userId);

        return getActionTriggerDetails(actionQueueingResult.actionTriggerId);
    }

    private Long getCurrentUser() {
        return userService == null || userService.getCurrentUser() == null ? 0L : userService.getCurrentUser().getId();
    }

    //////////////////// create policy action

    @RequestMapping(value = "/policy/filter/action/{actionId}", method = RequestMethod.POST)
    public long policyActionOnFilter(@PathVariable String actionId, @RequestParam Map<String, String> requestParams) {
        Properties actionParams = extractActionParameters(requestParams);
        String filter = requestParams.get("filter");
        FilterDescriptor filterDescriptor = SavedFilterService.convertFilterDescriptorJson(filter);
        return actionsAppService.queueActionOnFilterAndDispatchPolicy(
                filterDescriptor, actionId, actionParams, getCurrentUser());
    }

    @RequestMapping(value = "/policy/filter", method = RequestMethod.POST)
    public ActionExecutionTaskStatusDto policyActionOnFilter(@RequestParam Map<String, String> params, @RequestBody DispatchActionRequestDto dispatchActionRequestDto) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        long actionTriggerId = actionsAppService.queueActionOnFilterAndDispatchPolicy(
                dataSourceRequest.getFilter(), dispatchActionRequestDto.getActionDefinitionId(),
                dispatchActionRequestDto.getParameters(), getCurrentUser());
        return getActionTriggerDetails(actionTriggerId);
    }

    ///////////////////

    private Properties extractActionParameters(@RequestParam Map<String, String> requestParams) {
        Properties actionParams = new Properties();
        if (requestParams != null) {
            int prefixLength = PARAMETER_PREFIX.length();
            for (Map.Entry<String, String> entry : requestParams.entrySet()) {
                if (entry.getKey().startsWith(PARAMETER_PREFIX) && entry.getValue() != null) {
                    String key = entry.getKey().substring(prefixLength);
                    actionParams.setProperty(key, entry.getValue());
                }
            }
        }
        return actionParams;
    }

    @RequestMapping(value = "/execution/{actionTriggerId}", method = RequestMethod.GET)
    public ActionExecutionTaskStatusDto getActionTriggerDetails(@PathVariable long actionTriggerId) {
        return actionsAppService.getActionExecutionTaskStatus(actionTriggerId);
    }

    @RequestMapping(value = "/execution", method = RequestMethod.GET)
    public Page<ActionExecutionTaskStatusDto> getAllActionTriggers(@RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return actionsAppService.getAllActionTriggers(dataSourceRequest);
    }

    @RequestMapping(value = "/execution/running", method = RequestMethod.GET)
    public List<ActionExecutionTaskStatusDto> getRunningActionTriggers(@RequestParam Map<String, String> params) {
        return actionsAppService.getRunningActionTriggers();
    }

    @RequestMapping(value = "/execution/last", method = RequestMethod.GET)
    public ActionExecutionTaskStatusDto getLastRunningActionExecution(@RequestParam Map<String, String> params) {
        return actionsAppService.getLastRunningActionExecution();
    }

    @RequestMapping(value = "/execution/stop", method = RequestMethod.POST)
    public void stopExecutionEngine() {
        actionsAppService.stopExecutionEngine();
    }

    @RequestMapping(value = "/execution/{actionTriggerId}/files", method = RequestMethod.GET)
    public void downloadActionExecutionTaskFiles(@PathVariable long actionTriggerId, HttpServletResponse response) {
        logger.debug("downloadActionExecutionTaskFiles {}", actionTriggerId);
        try {
            response.setContentType("application/zip");
            // set headers for the response
            final String headerKey = "Content-Disposition";
            String suffix = String.valueOf(actionTriggerId);
            final String headerValue = String.format("attachment; filename=\"%s.%s.zip\"", "execution_", suffix);
            response.setHeader(headerKey, headerValue);
            Stream<Path> actionExecutionReportsFolderPaths = actionsAppService.getActionExecutionReportsFolderPaths(actionTriggerId);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zipOutputStream = new ZipOutputStream(baos);
            actionExecutionReportsFolderPaths.forEach(p -> {
                try {
                    if (!Files.exists(p)) {
                        logger.info("Folder {} doesn't exist. Probably no actions were executed on trigger {}", p, actionTriggerId);
                    } else {
                        addFilesToZipStream(p, zipOutputStream);
                    }
                } catch (IOException e) {
                    logger.info("Error writing action files ({}) to output stream.", p, e);
                    throw new RuntimeException(messageHandler.getMessage("log-fetch.write.error"), e);
                }
            });
            logger.debug("close out stream");
            baos.close();
            logger.debug("close zip stream");
            zipOutputStream.close();
            logger.debug("write response");
            response.setContentLength(baos.size());
            response.getOutputStream().write(baos.toByteArray());
            response.getOutputStream().flush();
        } catch (IOException ex) {
            logger.info("Error writing action files to output stream.", ex);
            throw new RuntimeException(messageHandler.getMessage("log-fetch.write.error"), ex);
        }
    }

    private void addFilesToZipStream(Path p, ZipOutputStream zipOutputStream) throws IOException {
        try (Stream<Path> logFileList = Files.list(p)) {
            logger.debug("zip each script file");
            logFileList.forEach(f -> {
                try {
                    LocalFileUtils.addFileToZip(f, zipOutputStream);
                } catch (IOException e) {
                    logger.error("Failed to add log file " + f + " to zip stream", e);
                }
            });
        }
    }

}
