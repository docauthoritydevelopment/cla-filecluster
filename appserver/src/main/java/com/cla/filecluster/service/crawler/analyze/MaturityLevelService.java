package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.domain.dto.file.FileCollections;
import com.cla.common.domain.dto.histogram.ClaHistogramCollection;
import com.cla.common.domain.dto.histogram.ClaSuperCollection;
import com.cla.common.domain.dto.pv.MaturityClass;
import com.cla.common.domain.dto.pv.MaturityLevel;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.service.files.pv.MaturityHelper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Service
public class MaturityLevelService {
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MaturityLevelService.class);

    public int getPartMaturityLevel(ClaSuperCollection<Long> partPvCollection) {
        return maturityHelper.getPartMaturityLevel(partPvCollection);
    }
    public int getPartMaturityLevel(String pvName, ClaHistogramCollection<Long> claHistogramCollection) {
        return maturityHelper.getPartMaturityLevel(pvName,claHistogramCollection);
    }

    @Value("${maturity.pvGroups:}")
	private String maturityPvGroups;

	@Value("${maturity.low.threshodsByPV:}")
	private String maturityLowThreshodsByPV;

	@Value("${maturity.medium.threshodsByPV:}")
	private String maturityMediumThreshodsByPV;

	@Value("${maturity.model.singleMaturityLevel:false}")		// true = backward compatibility mode
	private boolean maturityModelSingleMaturityLevel;			// Single value per file/part or separate per content and styling / structure

	@Value("${maturity.model.contentPvNames:PV_BodyNgrams,PV_SubBodyNgrams,PV_DocstartNgrams,PV_CellValues,PV_Formulas,PV_ConcreteCellValues,PV_TextNgrams}")
	private String contentPvNames;


	private Map<String, Integer[]> maturityThresholdsMap = new HashMap<>();
	final private Map<String, Integer> maturityPvGroupMap = new HashMap<>();

	public MaturityHelper maturityHelper;


	@PostConstruct
	private void init() {
        logger.debug("Init maturity levels service");
		MaturityHelper.fillMaturityLevelThresholds(maturityThresholdsMap, maturityLowThreshodsByPV, MaturityLevel.ML_Low);
		MaturityHelper.fillMaturityLevelThresholds(maturityThresholdsMap, maturityMediumThreshodsByPV, MaturityLevel.ML_Med);
//		fillMaturityLevelThresholds(maturityHighThreshodsByPV, MaturityLevel.ML_High);
		logger.info("MaturityLevelThresholds: {}",
                ((Stream<String>) maturityThresholdsMap.entrySet().stream().map(e->
				(e.getKey().concat(": ").concat(Arrays.toString(e.getValue()))))).collect(Collectors.joining(" ")));

		MaturityHelper.maturityThreshodsToPvGroups(maturityThresholdsMap, maturityPvGroupMap, maturityPvGroups);

        logger.debug("Maturity levels service initialized");

        maturityHelper = new MaturityHelper(contentPvNames,maturityModelSingleMaturityLevel);
        maturityHelper.setMaturityPvGroupMap(maturityPvGroupMap);
        maturityHelper.setMaturityThresholdsMap(maturityThresholdsMap);
	}

	/*public int getMaturityLevelSingleValue(ClaSuperCollection<Long> partPvCollection) {
		Map<Integer,Integer> pvGroup2LevelMap = new HashMap<>();
		partPvCollection.getCollections().entrySet().stream().filter(e->isMaturityCriteria(e.getKey())).forEach(e->{
			int level = maturityHelper.getPartMaturityLevel(e.getKey(), e.getValue());
			Integer pvtGroup = maturityPvGroupMap.get(e.getKey());
			Integer currGroupLevel = pvGroup2LevelMap.get(pvtGroup);
			// Get max level within group
			if (currGroupLevel==null || currGroupLevel < level) {
				pvGroup2LevelMap.put(pvtGroup, Integer.valueOf(level));
			}
		});
		return pvGroup2LevelMap.values().stream().mapToInt(v->v.intValue()).min().orElse(0);
	}

	public int calcPartMaturityLevelMultiValue(ClaSuperCollection<Long> partPvCollection) {
		Map<Integer,Integer> contentPvGroup2LevelMap = new HashMap<>();
		Map<Integer,Integer> stylePvGroup2LevelMap = new HashMap<>();
		partPvCollection.getCollections().entrySet().stream().filter(e->isMaturityCriteria(e.getKey())).forEach(e->{
			int level = maturityHelper.getPartMaturityLevel(e.getKey(), e.getValue());
			Integer pvtGroup = maturityPvGroupMap.get(e.getKey());
			Map<Integer,Integer> pvGroup2LevelMap = (maturityHelper.pvMaturityClass(e.getKey())==MaturityClass.MC_CONTENT)?contentPvGroup2LevelMap:stylePvGroup2LevelMap;
			Integer currGroupLevel = pvGroup2LevelMap.get(pvtGroup);
			// Get max level within group
			if (currGroupLevel==null || currGroupLevel < level) {
				pvGroup2LevelMap.put(pvtGroup, Integer.valueOf(level));
			}
		});
		int contentMl = contentPvGroup2LevelMap.values().stream().mapToInt(v->v.intValue()).min().orElse(0);
		int styleMl = stylePvGroup2LevelMap.values().stream().mapToInt(v->v.intValue()).min().orElse(0);
		return maturityHelper.maturityClassesToCombinedValue(contentMl, styleMl);
	}
*/
	public int getMaturityLevelValueByCollection(FileCollections fileCollections)
	{
		return maturityHelper.calcFileMaturityLevelValueByCollection(fileCollections);
	}

	public static int maturityClassesCombinedValueToClassValue(int value, MaturityClass mc) {
		return (mc==MaturityClass.MC_CONTENT) ? (value % 10) : (value / 10); 
	}

	public boolean isMaturityCriteria(String key) {
		return maturityThresholdsMap.containsKey(key);
	}
	
	/*public int simParamObjByCombinedMaturityLevelSelector(int combinedMaturityLevel, Object[] vec, PVType pvt) {
		return simParamObjByCombinedMaturityLevelSelector(combinedMaturityLevel, vec, pvt.name());
	}*/
	
	/*public int simParamObjByCombinedMaturityLevelSelector(int combinedMaturityLevel, Object[] vec, String pvName) {
		if (maturityModelSingleMaturityLevel) {
			return (combinedMaturityLevel >= vec.length)  ? 0 : combinedMaturityLevel;
		}
		else {
			MaturityClass mc = maturityHelper.pvMaturityClass(pvName);
			return maturityClassesCombinedValueToClassValue(combinedMaturityLevel, mc);
		}
	}
	
	public int simParamObjByCombinedMaturityLevelSelector(int combinedMaturityLevel, Object[] vec, MaturityClass mc) {
		if (maturityModelSingleMaturityLevel) {
			return (combinedMaturityLevel >= vec.length)  ? 0 : combinedMaturityLevel;
		}
		else {
			final int mlValue = maturityClassesCombinedValueToClassValue(combinedMaturityLevel, mc);
			return (mlValue >= vec.length)  ? 0 : mlValue;
		}
	}*/

	public int workbookMaturityLevel(PvAnalysisData doc1PvAnalysisData) {
		return maturityHelper.workbookMaturityLevel(doc1PvAnalysisData);
	}
}
