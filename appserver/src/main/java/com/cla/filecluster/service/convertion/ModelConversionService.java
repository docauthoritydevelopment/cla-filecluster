package com.cla.filecluster.service.convertion;

import com.cla.common.domain.dto.report.NamedIdentifiableDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.jpa.doctype.DocTypeRepository;
import com.cla.filecluster.repository.jpa.filetag.FileTagRepository;
import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by uri on 31/07/2016.
 */
@Service
public class ModelConversionService {

    @Autowired
    private SolrFileGroupRepository fileGroupRepository;

    @Autowired
    private DocTypeRepository docTypeRepository;

    @Autowired
    private FileTagRepository fileTagRepository;


    @Transactional(readOnly = true)
    public Map<String,NamedIdentifiableDto> findNamesIdsBySolrIds(List<String> solrIds, CatFileFieldType catFileFieldType) {
        Map<String, NamedIdentifiableDto> result = new HashMap<>();
        if (solrIds == null || solrIds.size() == 0) {
            return result;
        }
        switch (catFileFieldType) {
            case USER_GROUP_ID:
                List<FileGroup> byIdsFull = fileGroupRepository.getByIds(solrIds);
                for (FileGroup fileGroup : byIdsFull) {
                    result.put(fileGroup.getId(),new NamedIdentifiableDto(fileGroup.getId(), fileGroup.getNameForReport()));
                }
                break;
            case DOC_TYPES:
                Set<DocType> queryResult = docTypeRepository.findBySolrIdentifiers(solrIds);
                for (DocType docType : queryResult) {
                    result.put(docType.getDocTypeIdentifier(),new NamedIdentifiableDto(docType.getId(),docType.getName()));
                }
                break;
                //TODO - support all model object types
            case TAGS:
                return convertFileTagsFromSolrIds(solrIds);
        }
        return result;
    }

    private Map<String, NamedIdentifiableDto> convertFileTagsFromSolrIds(List<String> solrIds) {
        List<Long> idsToQuery = new ArrayList<>();
        for (String solrId : solrIds) {
            idsToQuery.add(AssociationIdUtils.extractTagIdFromSolrIdentifier(solrId));
        }
        List<FileTag> fileTags = fileTagRepository.findFileTagsById(idsToQuery);
        Map<String, NamedIdentifiableDto> result = new HashMap<>();
        for (FileTag fileTag : fileTags) {
            String tagIdentifier = AssociationIdUtils.createTagIdentifier(fileTag);
            result.put(tagIdentifier,new NamedIdentifiableDto(fileTag.getId(),fileTag.getName()));
        }
        return result;
    }
}
