package com.cla.filecluster.service.files.ingest;

import com.cla.common.domain.dto.FileMetadata;
import com.cla.common.domain.dto.file.AclItemDto;
import com.cla.common.domain.dto.file.AclLevel;
import com.cla.common.domain.dto.file.PstEntryPropertiesDto;
import com.cla.common.domain.dto.mediaproc.OtherFile;
import com.cla.common.domain.dto.mediaproc.excel.ExcelWorkbookMetaData;
import com.cla.common.domain.dto.word.WordfileMetaData;
import com.cla.common.utils.CsvUtils;
import com.cla.common.utils.EmailUtils;
import com.cla.connector.domain.dto.acl.AclAware;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.connector.domain.dto.media.EmailAddress;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.mediaproc.ingest.chain.IngestResultContext;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import java.util.*;

public class ModelIngestUtils {

    public static List<AclItemDto> getAclItems(final Long id, AclAware metadata) {
        final List<AclItemDto> result = Lists.newArrayList();
        if (metadata != null) {
            addAcls(result, metadata.getAclReadAllowed(), id, AclType.READ_TYPE);
            addAcls(result, metadata.getAclWriteAllowed(), id, AclType.WRITE_TYPE);
            addAcls(result, metadata.getAclReadDenied(), id, AclType.DENY_READ_TYPE);
            addAcls(result, metadata.getAclWriteDenied(), id, AclType.DENY_WRITE_TYPE);
        }
        return result;
    }


    public static void fillContentMetaData(ContentMetadata contentMetadata, ExcelWorkbookMetaData excelWorkbookMetaData) {
        fillBasicContentMetaData(contentMetadata, excelWorkbookMetaData);

        contentMetadata.setItemsCountL2(excelWorkbookMetaData.getNumOfFormulas());
        contentMetadata.setItemsCountL3(excelWorkbookMetaData.getNumOfSheets());
        contentMetadata.setItemsCountL1(excelWorkbookMetaData.getNumOfCells());
    }

    public static void fillContentMetaData(ContentMetadata contentMetadata, WordfileMetaData wordfileMetaData) {
        fillBasicContentMetaData(contentMetadata, wordfileMetaData);

        contentMetadata.setItemsCountL2(wordfileMetaData.getNumWords());
        contentMetadata.setItemsCountL3(wordfileMetaData.getNumPages());
        contentMetadata.setItemsCountL1(wordfileMetaData.getNumChars());

    }

    public static void fillContentMetaData(ContentMetadata contentMetadata, OtherFile otherFileMetaData) {
        int textLength = otherFileMetaData.getTextLength();
        if (textLength > 0) {
            contentMetadata.setItemsCountL2(textLength);    // add extracted text size to the file record
        }
    }


    private static void fillBasicContentMetaData(ContentMetadata contentMetadata, FileMetadata fileMetadata) {
        contentMetadata.setFsFileSize(fileMetadata.getFsFileSize());

        contentMetadata.setAuthor(CsvUtils.notNull(fileMetadata.getAuthor()));
        contentMetadata.setCompany(CsvUtils.notNull(fileMetadata.getCompany()));
        contentMetadata.setSubject(CsvUtils.notNull(fileMetadata.getSubject()));
        contentMetadata.setTitle(CsvUtils.notNull(fileMetadata.getTitle()));
        contentMetadata.setKeywords(CsvUtils.notNull(fileMetadata.getKeywords()));
        contentMetadata.setGeneratingApp(CsvUtils.notNull(fileMetadata.getAppNameVer()));
        contentMetadata.setTemplate(CsvUtils.notNull(fileMetadata.getTemplate()));
        contentMetadata.setAppCreationDate(fileMetadata.getCreatDate());
    }

    private static void addAcls(List<AclItemDto> targetList, Collection<String> source, Long id, AclType type){
        CsvUtils.notNull(source).forEach(acl -> targetList.add(new AclItemDto(id, acl, type, AclLevel.document)));
    }

    public static void copyPstFields(PstEntryPropertiesDto pstProps, SolrFileEntity file, boolean parseX500Addresses, boolean extractNonDomainX500CN) {
        copySender(pstProps, file, parseX500Addresses, extractNonDomainX500CN);
        copyRecipients(pstProps, file, parseX500Addresses, extractNonDomainX500CN);
        copySentDate(pstProps, file);
        file.setItemSubject(pstProps.getEmailSubject());
    }

    private static void copySender(PstEntryPropertiesDto pstProps, SolrFileEntity file, boolean parseX500Addresses, boolean extractNonDomainX500CN) {
        EmailAddress sender = pstProps.getSender();
        if (sender != null) {
            file.setSenderName(EmailUtils.getMailName(sender.getName()));
            file.setSenderAddress(EmailUtils.getEmailAddressValue(sender.getAddress(), parseX500Addresses, extractNonDomainX500CN));
            file.setSenderDomain(EmailUtils.extractDomainFromEmailAddress(sender.getAddress(), parseX500Addresses, extractNonDomainX500CN));
            file.setSenderFullAddress(ModelIngestUtils.getEmailJson(sender));
        }
    }

    private static void copyRecipients(PstEntryPropertiesDto pstProps, SolrFileEntity file, boolean parseX500Addresses, boolean extractNonDomainX500CN) {
        List<EmailAddress> recipients = pstProps.getRecipients();
        if (recipients != null && !recipients.isEmpty()) {
            Set<String> recipientsNames = new HashSet<>(recipients.size());
            Set<String> recipientsAddresses = new HashSet<>(recipients.size());
            Set<String> recipientsDomains = new HashSet<>(recipients.size());
            List<String> recipientsFullAddresses = new ArrayList<>(recipients.size());
            recipients.forEach(recipient -> {
                String name = EmailUtils.getRecipientName(recipient);
                if (name != null) {
                    recipientsNames.add(name);
                }
                recipientsAddresses.add(EmailUtils.getEmailAddressValue(recipient.getAddress(), parseX500Addresses, extractNonDomainX500CN));
                recipientsDomains.add(EmailUtils.extractDomainFromEmailAddress(recipient.getAddress(), parseX500Addresses, extractNonDomainX500CN));
                recipientsFullAddresses.add(ModelIngestUtils.getEmailJson(recipient));
            });
            file.setRecipientsNames(new ArrayList<>(recipientsNames));
            file.setRecipientsAddresses(new ArrayList<>(recipientsAddresses));
            file.setRecipientsDomains(new ArrayList<>(recipientsDomains));
            file.setRecipientsFullAddresses(recipientsFullAddresses);
        }
    }

    private static void copySentDate(PstEntryPropertiesDto pstProps, SolrFileEntity file) {
        Date sentDate = pstProps.getSentDate();
        if (sentDate != null) {
            file.setSentDate(sentDate);
        }
    }

    public static String getEmailJson(EmailAddress address) {
        ObjectMapper mapper = ModelDtoConversionUtils.getMapper();
        try {
            return mapper.writeValueAsString(address);
        } catch (Exception e) {
            return null;
        }
    }

    public static EmailAddress getEmailObject(String addressJson) {
        ObjectMapper mapper = ModelDtoConversionUtils.getMapper();
        try {
            return mapper.readValue(addressJson, EmailAddress.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static List<EmailAddress> getEmailObjects(List<String> addressJson) {
        List<EmailAddress> result = new ArrayList<>();
        if (addressJson != null) {
            addressJson.forEach(address -> result.add(getEmailObject(address)));
        }
        return result;
    }

    public static boolean isNonIngestedExcelState(IngestResultContext context) {
        if (!context.isExcel()) {
            return false;
        }
        ExcelWorkbookMetaData metadata = context.getExcelResultMessage().getPayload().getMetadata();
        return (metadata == null || metadata.getNumOfSheets() < 0);
    }
}
