package com.cla.filecluster.service.media;

import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.GoogleDriveConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.google.GoogleMediaConnector;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 *
 * Created by uri on 25/07/2016.
 */
@Service
public class GoogleDriveAppService implements MediaSpecificAppService<GoogleMediaConnector, GoogleDriveConnectionDetailsDto> {

    public String buildAuthorizeUrl(String sessionId) {
        //TODO
        return null;
    }

    public List<ServerResourceDto> listServerFolders(String id) {
        //TODO
        return null;
    }

    public void handleDriveOauthResponse(String code, boolean b) {
        //TODO
    }

    public void fillAuthentication(String state) {
        //TODO
    }

    @Override
    public List<ServerResourceDto> listFoldersUnderPath(MediaConnectionDetailsDto connectionDetails,
                                                        Optional<String> path, Long customerDataCenterId, boolean isTest) {
        //TODO
        return null;
    }
}
