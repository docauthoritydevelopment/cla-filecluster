package com.cla.filecluster.service.api.groups;

import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.service.crawler.analyze.FileGroupingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by uri on 23/11/2016.
 */
@RestController
@RequestMapping("/groups")
public class LegacyGroupingApiController {

    @Autowired
    FileGroupingService fileGroupingService;

    @RequestMapping
    public FileGroup findAnalysisGroupForFile(@RequestParam final long fileId) {
        return fileGroupingService.findAnalysisGroupForFile(fileId);
    }
}
