package com.cla.filecluster.service.export.exporters.components.mixin;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class CustomerDataCenterDtoMixin {

    @JsonProperty(DATA_CENTER)
    private String name;
}
