package com.cla.filecluster.service.actions;

import com.cla.filecluster.domain.entity.actions.ActionDefinition;
import com.cla.filecluster.domain.entity.actions.ActionInstance;
import com.cla.filecluster.repository.jpa.actions.ActionInstanceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Properties;

@Service
public class ActionInstanceService {

    Logger logger = LoggerFactory.getLogger(ActionInstanceService.class);

    @Autowired
    ActionInstanceRepository actionInstanceRepository;

    @Transactional(readOnly = false)
    public ActionInstance createActionInstance(ActionDefinition actionDefinition, String name) {
        ActionInstance actionInstance = new ActionInstance(actionDefinition, name);
        return actionInstanceRepository.save(actionInstance);
    }

    @Transactional(readOnly = false)
    public ActionInstance updateActionInstanceParameters(long actionInstanceId, Properties properties) {
        return actionInstanceRepository.findById(actionInstanceId)
                .map(one -> {
                    String parameters = FileActionsEngineService.convertParametersToString(properties);
                    one.setParameters(parameters);
                    return actionInstanceRepository.save(one);
                })
                .orElse(null);
    }
}
