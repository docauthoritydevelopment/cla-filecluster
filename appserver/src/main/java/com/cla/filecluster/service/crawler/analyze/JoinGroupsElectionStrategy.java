package com.cla.filecluster.service.crawler.analyze;

import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.filetag.FileTagService;

import java.util.Collection;

/**
 * Strategy for electing the target group during groups merging.
 * When several groups are to be merged into one, there is a need to choose which group will
 * remain and receive the members of the other groups.
 */
public interface JoinGroupsElectionStrategy {

    /**
     * Choose the target group
     * @param candidateGroups groups to choose from
     * @return JoinGroupsElectionResult - elected groups, groups to be joined into it and rejected groups, that will not be joined.
     */
    JoinGroupsElectionResult electTargetGroup(Collection<SolrFileGroupEntity> candidateGroups, SolrFileGroupRepository fileGroupRepository, FileTagService fileTagService);

    /**
     * Non-elected groups of size bigger than the threshold will be rejected, i.e. will stay independent.
     * Groups above certain size shouldn't be merged into other groups,
     * since there is a high chance that they are meaningful enough to stand alone.
     *
     * @param rejectSizeThreshold  reject joining groups above this size
     */
    void setRejectSizeThreshold(long rejectSizeThreshold);

    /**
     * Should return true if the strategy requires full group objects
     * with all the referenced objects (tags, func roles etc.)
     * in order to make its decision.
     *
     * @return true if full objects are required
     */
    boolean requiresFullGroupObjects();

}
