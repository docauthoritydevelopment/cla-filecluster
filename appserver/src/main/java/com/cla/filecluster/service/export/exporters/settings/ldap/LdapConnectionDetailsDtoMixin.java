package com.cla.filecluster.service.export.exporters.settings.ldap;

import com.fasterxml.jackson.annotation.JsonProperty;
import static com.cla.filecluster.service.export.exporters.settings.ldap.LdapConnectionHeaderFields.*;

/**
 * Created by: yael
 * Created on: 5/27/2019
 */
public class LdapConnectionDetailsDtoMixin {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(SSL)
    private boolean ssl;

    @JsonProperty(HOST)
    private String host;

    @JsonProperty(PORT)
    private Integer port;

    @JsonProperty(MANAGE_DN)
    private String manageDN;

    @JsonProperty(ACTIVE)
    private boolean enabled = true;

}
