package com.cla.filecluster.service.api;

import com.cla.filecluster.domain.dto.AuditDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * api for getting audit data
 *
 * Created by: yael
 * Created on: 1/30/2018
 */
@SuppressWarnings("unchecked")
@RestController
@RequestMapping("/api/audit")
public class AuditApiController {

    @Autowired
    private EventAuditService eventAuditService;

    @RequestMapping(value = "/actions", method = RequestMethod.GET)
    public List<AuditAction> getAuditActions() {
        return AuditAction.getInUse();
    }

    @RequestMapping(value = "/types", method = RequestMethod.GET)
    public List<AuditType> getAuditTypes() {
        return AuditType.getInUse();
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<AuditDto> getAuditTrail(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        String username = null;
        List<String> crudAction = null;
        List<String> types = null;
        Long fromTime = null;
        Long toTime = null;

        if (!Strings.isNullOrEmpty(params.get("username"))) {
            username = params.get("username");
        }

        if (!Strings.isNullOrEmpty(params.get("auditType"))) {
            String auditTypeStr = params.get("auditType").replace(" ", "");
            if (!auditTypeStr.isEmpty()) {
                types = Arrays.asList(auditTypeStr.split(","));
            }
        }

        if (!Strings.isNullOrEmpty(params.get("action"))) {
            String crudActionStr = params.get("action").replace(" ", "");
            if (!crudActionStr.isEmpty()) {
                crudAction = Arrays.asList(crudActionStr.split(","));
            }
        }

        if (!Strings.isNullOrEmpty(params.get("fromTime"))) {
            fromTime = Long.parseLong(params.get("fromTime"));
        }

        if (!Strings.isNullOrEmpty(params.get("toTime"))) {
            toTime = Long.parseLong(params.get("toTime"));
        }

        return eventAuditService.getAuditTrail(username, crudAction, types, fromTime, toTime, pageRequest);
    }
}
