package com.cla.filecluster.service.license;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.common.domain.dto.license.LicenseImportResultDto;
import com.cla.filecluster.domain.entity.license.DocAuthorityLicense;
import com.cla.filecluster.domain.entity.license.LicenseResource;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by uri on 28/06/2016.
 */
@Service
public class LicenseAppService  {

    public static final int DEMO_LICENSE_LOGIN_EXPIRY_DAYS = 30;
    public static final int DEMO_LICENSE_CRAWLER_EXPIRY_DAYS = 15;
    public static final String DEMO_LICENSE_MAX_FILES = "550000";   // Was 1.2M. Set to 500 + 10% spare

    // Allowed tolerance in local time clock in case new license is created shortly after installation.
    public static final int LICENSE_CREATION_TIME_OFFSET_MILLIS = 2 * 60 * 60 * 1000;

    @Value("${poc.license.rootFolders.max:*}")
    private String demoLicenseMaxRootFolders;

    @Value("${poc.license.serverId.length:13}")
    private int serverIdLength;

    @Autowired
    private LicenseService licenseService;

    private Logger logger = LoggerFactory.getLogger(LicenseAppService.class);

    @Transactional(readOnly = true)
    public DocAuthorityLicenseDto getLastAppliedLicense() {
        DocAuthorityLicense license = licenseService.getLastAppliedLicense();
        if (license == null) {
            return null;
        }
        List<LicenseResource> resourceList = licenseService.getLicenseResources();
        DocAuthorityLicenseDto docAuthorityLicenseDto = LicenseService.convertLicense(license, resourceList);
        docAuthorityLicenseDto.setLoginExpired(isLoginExpired());
        return docAuthorityLicenseDto;
    }

    public LicenseImportResultDto importLicense(String licenseFileContents, final boolean save) {
        logger.info("Load new license ({})",licenseFileContents);
        String[] fileLines = licenseFileContents.split("\n");
        List<String> lines = Arrays.stream(fileLines).filter(f -> f.length() > 0).collect(Collectors.toList());
        if (lines.size() < 2) {
//            throw new BadHttpRequestException("Bad license file. Signature line is missing");
            logger.warn("Bad license data", licenseFileContents);
            return new LicenseImportResultDto(false, "Bad license data");
        }
//        else if (lines.size() > 4 && fileLines[0].length() < 50) {
//            // most likely this is a packed multi-lines license file
//            return importLicensePacked(licenseFileContents, save);
//        }
        String licenseStringBase64 = lines.get(0);
        String signatureString = lines.get(1);
        return importLicense(licenseStringBase64, signatureString, save);
    }

    public LicenseImportResultDto importLicensePacked(String licenseFileContents, final boolean save) {
        logger.info("Load new packed license ({})",licenseFileContents);
        String unpackedLicense = licenseFileContents.replaceAll("\\s","").replaceAll("xYx","\n").replaceAll("YY","Y");
        return importLicense(unpackedLicense, save);
    }

    public LicenseImportResultDto importLicense(final String licenseStringBase64, final String signatureString, final boolean save) {
        if (!licenseService.verifySignature(licenseStringBase64,signatureString)) {
//            DocAuthorityLicenseDto result = getLastAppliedLicense();
            logger.warn("License signature doesn't match - {}", signatureString);
            return new LicenseImportResultDto(false, "License signature doesn't match");
        }
        DocAuthorityLicenseDto docAuthorityLicenseDto = LicenseService.convertLicenseFromBase64(licenseStringBase64);
        if (docAuthorityLicenseDto.getServerId() != null && !docAuthorityLicenseDto.getServerId().isEmpty()) {
            String serverId = licenseService.getServerId();
            if (!docAuthorityLicenseDto.getServerId().equals(serverId)) {
//                DocAuthorityLicenseDto result = getLastAppliedLicense();
                logger.warn("License serverId mismatch. Set for serverId={} != {}. {}", docAuthorityLicenseDto.getServerId(), serverId, docAuthorityLicenseDto.toString());
                return new LicenseImportResultDto(docAuthorityLicenseDto, false, "License serverId mismatch. Set for serverId " + docAuthorityLicenseDto.getServerId());
            }
        }
        if (licenseService.licenseExists(docAuthorityLicenseDto.getId())) {
            logger.warn("License {} was imported before", docAuthorityLicenseDto.toString());
            DocAuthorityLicenseDto result = getLastAppliedLicense();
            return new LicenseImportResultDto(docAuthorityLicenseDto,false,"License was imported before");
        }
        licenseService.importLicense(docAuthorityLicenseDto, save);
        DocAuthorityLicenseDto result = getLastAppliedLicense();
        return new LicenseImportResultDto(save?result:docAuthorityLicenseDto);  // for save return new limitations; otherwise return new license info
    }

    @Transactional
    public void importDefaultLicense() {
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setId(0);
        docAuthorityLicenseDto.setRegisteredTo("- POC -");
        docAuthorityLicenseDto.setIssuer("Auto generated for POC");
        Map<String, String> restrictions = new HashMap<>();
        LocalDateTime localDateTime = LocalDateTime.now();
        docAuthorityLicenseDto.setCreationDate(System.currentTimeMillis() - LICENSE_CREATION_TIME_OFFSET_MILLIS);     // Use same timestamp method as in the licnese gen. tool. (current effectively = UTC time)
        logger.info("Import default license. Creation date: {}", docAuthorityLicenseDto.getCreationDate());
        long loginExpiry = localDateTime.plusDays(DEMO_LICENSE_LOGIN_EXPIRY_DAYS).toEpochSecond(ZoneOffset.UTC) * 1000;
        long crawlerExpiry = localDateTime.plusDays(DEMO_LICENSE_CRAWLER_EXPIRY_DAYS).toEpochSecond(ZoneOffset.UTC) * 1000;
        restrictions.put(LicenseService.CRAWLER_EXPIRY,String.valueOf(crawlerExpiry));
        restrictions.put(LicenseService.LOGIN_EXPIRY,String.valueOf(loginExpiry));
        restrictions.put(LicenseService.FILES_MAX, DEMO_LICENSE_MAX_FILES);
        restrictions.put(LicenseService.ROOTFOLDERS_MAX, demoLicenseMaxRootFolders);
        restrictions.put(LicenseService.SERVER_ID, UUID.randomUUID().toString().substring(0,serverIdLength));

        docAuthorityLicenseDto.setRestrictions(restrictions);
        licenseService.importLicense(docAuthorityLicenseDto, true);
    }

    public Boolean isLoginExpired() {
        return licenseService.isLoginExpired();
    }
}
