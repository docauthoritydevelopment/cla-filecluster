package com.cla.filecluster.service.filetag;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.bizlist.BizListItemType;
import com.cla.common.domain.dto.classification.EvaluateBizListInstructions;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.filetag.FileTagTypeAndTagsDto;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.filetag.*;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.FilterBuildUtils;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.TagRelatedBaseService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.entitylist.BizListItemService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.operation.GroupLockService;
import com.cla.filecluster.service.util.ValidationUtils;
import com.cla.filecluster.util.ControllerUtils;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.hibernate.exception.ConstraintViolationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by uri on 24/08/2015.
 */
@Service("fileTaggingAppService")
public class FileTaggingAppService extends TagRelatedBaseService {

    private static final int CLASSIFICATION_MAX_BIZ_LISTS_TO_VIEW = 100;
    private static final Logger logger = LoggerFactory.getLogger(FileTaggingAppService.class);

    @Value("${fileTagService.systemtags.view.max:100}")
    private int maxSystemTagsToView;

    @Value("${fileTagService.discover-left.filter-tags-view-by-type:true}")
    private boolean filterTagsByTypeIfNeeded;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private BizListItemService bizListItemService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private GroupLockService groupLockService;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private FileTagSettingService fileTagSettingService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private MessageHandler messageHandler;

    private TimeSource timeSource = new TimeSource();


    public ClaFileDto addFileTagToFile(long fileId, FileTagDto fileTag, BasicScope basicScope, SolrCommitType commitType) {
        SolrFileEntity fileEntity = fileCatService.getFileEntity(fileId);
        ClaFile file = fileCatService.getFromEntity(fileEntity);
        List<AssociationEntity> fileTagAssociations = AssociationsService.convertFromAssociationEntity(
                file.getId(), file.getAssociations(), AssociationType.FILE);
        List<AssociationEntity> relevantExplicitTagAssociations = fileTagAssociations == null ? new ArrayList<>() :
                fileTagAssociations.stream()
                        .filter(fta -> explicitAssociationFilter(fta, fileTag.getTypeId(), resolveScopeId(basicScope)))
                        .collect(Collectors.toList());

        if (relevantExplicitTagAssociations.stream()
                .anyMatch(fta -> Objects.equals(fta.getFileTagId(), fileTag.getId()))) {
            logger.warn("File tag {} already exists on claFile {}, ignoring request", fileTag, fileId);
        } else {
            if (fileTag.getType().isSingleValueTag() && !relevantExplicitTagAssociations.isEmpty()) {
                relevantExplicitTagAssociations.stream()
                        .filter(fta -> fileTagService.getFromCache(fta.getFileTagId()).getType().isSingleValueTag())
                        .findFirst()
                        .ifPresent(fta -> associationsService.removeAssociationFromFile(
                                fileEntity, fileTagService.getFromCache(fta.getFileTagId()), fta.getAssociationScopeId(), commitType));
            }
            associationsService.addAssociationToFile(fileTag, file, basicScope, commitType);
        }

        return getFileWithTags(fileId);
    }

    @Transactional
    public ClaFileDto getFileWithTags(long fileId) {
        List<ClaFileDto> res = filesApplicationService.enrichFiles(Lists.newArrayList(fileCatService.getFileEntity(fileId)), null, true);
        return res == null || res.isEmpty() ? null : res.get(0);
    }

    @Transactional
    public void addFileTagsToFile(long fileId, List<FileTagDto> fileTags, SolrCommitType commitType, BasicScope basicScope) {
        fileTags.forEach(ft -> addFileTagToFile(fileId, ft, basicScope, commitType));
    }

    @Transactional
    public ClaFileDto removeFileTagFromFile(long fileId, long fileTagId, BasicScope basicScope, SolrCommitType commitType) {
        FileTagDto fileTag = fileTagService.getFromCache(fileTagId);
        Long scopeId = basicScope == null ? null : basicScope.getId();
        associationsService.removeAssociationFromFile(fileCatService.getFileEntity(fileId), fileTag, scopeId, commitType);
        ClaFileDto claFileDto = FileCatUtils.convertClaFile(fileCatService.getFileObject(fileId));
        refineAssociations(claFileDto, basicScope);
        return claFileDto;
    }

    public GroupDto addFileTagToGroup(String groupId, FileTag fileTag, BasicScope basicScope) {
        if (!groupLockService.getLockForGroup(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("group.operation.in-process"), BadRequestType.GROUP_LOCKED);
        }

        try {
            final FileGroup fileGroup = groupsService.findById(groupId);
            Long scopeId = basicScope == null ? null : basicScope.getId();
            List<AssociationEntity> groupAssociations = AssociationsService.convertFromAssociationEntity(
                    fileGroup.getId(), fileGroup.getAssociations(), AssociationType.GROUP);

            final List<AssociationEntity> relevantExistingExplicitFileGroupAssociations = groupAssociations.stream()
                    .filter(bga -> explicitAssociationFilter(bga, fileTag.getTypeId(), scopeId))
                    .collect(Collectors.toList());


            if (relevantExistingExplicitFileGroupAssociations.stream().anyMatch(bga -> Objects.equals(bga.getFileTagId(), fileTag.getId()))) {
                logger.warn("FileTag {} already added to fileGroup {}, ignoring request", fileTag, groupId);
            } else {
                if (fileTag.getType().isSingleValueTag() && !relevantExistingExplicitFileGroupAssociations.isEmpty()) {
                    relevantExistingExplicitFileGroupAssociations.stream()
                            .filter(bga -> fileTagService.getFromCache(bga.getFileTagId()).getType().isSingleValueTag())
                            .findFirst()
                            .ifPresent(bga -> markGroupAssociationAsDeleted(groupId,
                                    fileTagService.getFileTag(bga.getFileTagId()),
                                    bga.getAssociationScopeId() == null ? null : scopeService.getById(bga.getAssociationScopeId())));
                }
                associationsService.createGroupAssociation(fileTag, groupId, scopeId, AssociableEntityType.TAG);
            }

            GroupDto groupDto = groupsService.findDtoById(groupId);
            logger.debug("Add file tag {} to group {}", fileTag, groupId);
            refineAssociations(groupDto, basicScope);
            return groupDto;
        } finally {
            groupLockService.releaseLockForGroup(groupId);
        }
    }

    /**
     * Choose non deleted, not implicit associations with the same given tag type & scope
     *
     * @param entityAssociation association object
     * @param typeId            tag type ID
     * @param scope             tag scope
     * @return whether this is an explicit association
     */
    private boolean explicitAssociationFilter(EntityAssociation entityAssociation, long typeId, BasicScope scope) {
        return entityAssociation.getFileTag() != null &&
                !entityAssociation.isDeleted() &&
                !entityAssociation.isImplicit() &&
                Objects.equals(entityAssociation.getFileTag().getType().getId(), typeId) &&
                Objects.equals(entityAssociation.getScope(), scope);
    }

    private boolean explicitAssociationFilter(AssociationEntity entityAssociation, long typeId, Long scopeId) {
        return entityAssociation.getFileTagId() != null && (entityAssociation.getImplicit() == null || !entityAssociation.getImplicit()) &&
                Objects.equals(fileTagService.getFromCache(entityAssociation.getFileTagId()).getType().getId(), typeId) &&
                Objects.equals(entityAssociation.getAssociationScopeId(), scopeId);
    }

    @Transactional
    public GroupDto removeFileTagFromGroup(String businessId, FileTag fileTag, BasicScope basicScope) {
        if (!groupLockService.getLockForGroup(businessId)) {
            throw new BadRequestException(messageHandler.getMessage("group.operation.in-process"), BadRequestType.GROUP_LOCKED);
        }

        try {
            final GroupDto groupDto = markGroupAssociationAsDeleted(businessId, fileTag, basicScope);
            if (groupDto != null) {
                refineAssociations(groupDto, basicScope);
            }
            return groupDto;
        } finally {
            groupLockService.releaseLockForGroup(businessId);
        }
    }

    private GroupDto markGroupAssociationAsDeleted(String businessId, FileTag fileTag, BasicScope basicScope) {
        return groupsService.markFileTagAssociationAsDeleted(businessId, fileTag, basicScope);
    }

    @Transactional
    public FileTagDto removeFileTag(long fileTagId) {
        logger.debug("Remove file tag {}", fileTagId);
        validateTagHasNoAssociations(fileTagId);
        List<FileTag> childrenFileTags = fileTagService.getFileTagChildren(fileTagId);
        if (childrenFileTags != null) {
            logger.debug("Remove children file tags");
            for (FileTag childrenFileTag : childrenFileTags) {
                removeFileTag(childrenFileTag.getId());
            }
        }
        fileTagSettingService.deleteByFileTag(fileTagId);
        associationsService.applyDocTypesTagsToSolr();
        return fileTagService.deleteFileTag(fileTagId);
    }

    private void validateTagHasNoAssociations(Long fileTagId) {
        if (associationsService.tagAssociationExists(fileTagId)) {
            throw new BadRequestException(messageHandler.getMessage("file-tag.delete.associations"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }
    }

    @Transactional
    public DocFolderDto addFileTagToFolder(Long folderId, FileTagDto fileTag, BasicScope basicScope) {
        DocFolderDto docFolderDto;
        List<AssociationEntity> folderAssociations = AssociationsService.convertFromAssociationEntity(
                folderId, docFolderService.getById(folderId).getAssociations(), AssociationType.FOLDER);
        final List<AssociationEntity> relevantExistingExplicitFolderTagAssociations =
                folderAssociations.stream()
                        .filter(fa -> explicitAssociationFilter(fa, fileTag.getTypeId(), resolveScopeId(basicScope)))
                        .collect(Collectors.toList());
        List<AssociableEntityDto> entities = associationsService.getEntitiesForAssociations(
                relevantExistingExplicitFolderTagAssociations);


        if (relevantExistingExplicitFolderTagAssociations.stream()
                .anyMatch(fa -> Objects.equals(fa.getFileTagId(), fileTag.getId()))) {
            logger.warn("FileTag {} already exists on folder {}, ignoring request", fileTag, folderId);
            docFolderDto = docFolderService.getDtoById(folderId);
        } else {
            if (fileTag.getType().isSingleValueTag() && !relevantExistingExplicitFolderTagAssociations.isEmpty()) {
                for (AssociationEntity fa : relevantExistingExplicitFolderTagAssociations) {
                    FileTagDto currFileTag = (FileTagDto) AssociationUtils.getEntityFromList(
                            FileTagDto.class, fa.getFileTagId(), entities);
                    if (currFileTag.getType().isSingleValueTag()) {
                        associationsService.markFolderAssociationAsDeleted(folderId, currFileTag, fa.getAssociationScopeId(), AssociableEntityType.TAG);
                        break;
                    }
                }
            }
            docFolderDto = docFolderService.addExplicitFileTagToFolder(folderId, fileTag, basicScope);
        }

        refineAssociations(docFolderDto, basicScope);
        return docFolderDto;
    }

    @Transactional
    public FileTag createFileTagIfNeeded(String name, String description, long fileTagTypeId, FileTagAction fileTagAction) {
        return createFileTagIfNeeded(name, description, fileTagTypeId, fileTagAction, null);
    }

    @Transactional
    public FileTag createFileTagIfNeeded(String name,
                                         String description,
                                         long fileTagTypeId,
                                         FileTagAction fileTagAction,
                                         String bizListId) {
        FileTagType type = fileTagTypeService.getFileTagType(fileTagTypeId);
        FileTag fileTag = fileTagService.getFileTagByName(name, type);
        if (fileTag == null) {
            logger.debug("Create fileTag by name {} and type {}", name, type);
            fileTag = fileTagService.createFileTag(name, description, type, null, bizListId, fileTagAction);
        }
        return fileTag;
    }

    @Transactional
    public DocFolderDto removeFileTagFromFolder(Long folderId, long fileTagId, BasicScope basicScope) {
        logger.debug("Remove file Tag {} from Folder {}", fileTagId, folderId);
        FileTagDto fileTag = fileTagService.getFromCache(fileTagId);
        if (fileTag == null) {
            throw new BadRequestException(messageHandler.getMessage("file-tag.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        associationsService.markFolderAssociationAsDeleted(folderId, fileTag, resolveScopeId(basicScope), AssociableEntityType.TAG);
        DocFolderDto docFolderDto = docFolderService.getByIdWithAssociations(folderId);
        refineAssociations(docFolderDto, basicScope);
        return docFolderDto;
    }

    @Nullable
    private static Long resolveScopeId(BasicScope basicScope) {
        return basicScope == null ? null : basicScope.getId();
    }

    @Transactional(readOnly = true)
    public FileTagDto getFileTagById(long id) {
        return fileTagService.getFileTagDto(id);
    }

    @Transactional
    public FileTagDto createFileTag(String name, String description, FileTagType type, Long docTypeId, FileTagAction fileTagAction) {
        try {
            DocType docType = docTypeId != null ? docTypeService.getDocTypeById(docTypeId) : null;
            FileTag fileTagByName = fileTagService.getFileTagByName(name, type);
            if (fileTagByName != null) {
                logger.info("File tag {} already exists", fileTagByName);
                return FileTagService.convertFileTagToDto(fileTagByName);
            }
            FileTag fileTag = fileTagService.createFileTag(name, description, type, docType, null, fileTagAction);
            return FileTagService.convertFileTagToDto(fileTag);
        } catch (ConstraintViolationException e) {
            logger.error("Constraint violation while trying to create file tag", e);
            throw new BadRequestException(messageHandler.getMessage("file-tag.create.violation"), BadRequestType.OPERATION_FAILED);
        }
    }

    @Transactional
    public FileTagDto createFileTag(String name, String description, FileTagType type, Long docTypeId, String entityId, FileTagAction fileTagAction) {
        nameValidation(name);
        DocType docType = docTypeId != null ? docTypeService.getDocTypeById(docTypeId) : null;
        FileTag fileTagByName = fileTagService.getFileTagByName(name, type);
        if (fileTagByName != null) {
            logger.warn("File tag {} already exists ({}). Can't create file tag by that name", fileTagByName);
            throw new BadRequestException(messageHandler.getMessage("file-tag.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
        FileTag fileTag = fileTagService.createFileTag(name, description, type, docType, entityId, fileTagAction);
        return FileTagService.convertFileTagToDto(fileTag);
    }


    @Transactional
    public FileTagDto createFileTag(String name, String description, FileTagType fileTagType, FileTagAction fileTagAction) {
        FileTag fileTag = fileTagService.createFileTag(name, description, fileTagType, null, null, fileTagAction);
        return FileTagService.convertFileTagToDto(fileTag);
    }

    public FileTagType getFileTagType(String name) {
        logger.debug("Get File Tag Type by name {}", name);
        return fileTagTypeService.getFileTagTypeByName(name);
    }

    @Transactional(readOnly = true)
    public List<FileTagTypeDto> getFileTagTypes(boolean showHiddenTypes, Long userId) {
        List<FileTagType> fileTagTypes = fileTagTypeService.getFileTagTypes(userId);
        Map<Long, Integer> fileTagCounts = fileTagTypeService.findFileTagCounts();
        return FileTagTypeService.convertFileTagTypesToDto(fileTagTypes, fileTagCounts, showHiddenTypes);
    }

    @Transactional(readOnly = true)
    public List<FileTagDto> getFileTags(Long typeId) {
        List<FileTag> result;
        if (typeId == null) {
            result = fileTagService.getFileTags();
        } else {
            result = fileTagService.getFileTagsByType(typeId);
        }
        return FileTagService.convertFileTagsToDto(result);
    }

    public FileTagType getFileTagType(long fileTagTypeId) {
        return fileTagTypeService.getFileTagType(fileTagTypeId);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<FileTagTypeDto>> listTagTypesFileCountWithFileFilter(DataSourceRequest dataSourceRequest, Long scopeId) {
        logger.debug("list Tag types File Count With File Filters");
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        PageRequest pageRequestLarge = PageRequest.of(0, ControllerUtils.FACET_MAX_PAGE_SIZE_FILECOUNT);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.ACTIVE_ASSOCIATIONS);
        Object resolveSolrScopeId = resolveSolrScopeId(scopeId);
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName() + ":" + resolveSolrScopeId + FileCatUtils.CATEGORY_SEPARATOR + "*");
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix(resolveSolrScopeId + FileCatUtils.CATEGORY_SEPARATOR);
        SolrFacetQueryResponse solrFacetQueryResponse = runFacetQuery(
                pageRequestLarge, solrFacetSpecification,
                c -> convertTagType2Value(c.getName()));

        // get total
        solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.ACTIVE_ASSOCIATIONS);
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName() + ":" + resolveSolrScopeId + FileCatUtils.CATEGORY_SEPARATOR + "*");
        solrFacetSpecification.setMinCount(1);
        solrFacetSpecification.setFacetPrefix(resolveSolrScopeId + FileCatUtils.CATEGORY_SEPARATOR);
        SolrFacetQueryResponse totalResp = fileCatService.runFacetQuery(solrFacetSpecification, pageRequestLarge);

        logger.debug("list file facets by Solr field tag types returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());
        return convertFacetFieldTagTypesResultToDto(solrFacetQueryResponse, pageRequest, totalResp, dataSourceRequest.getSelectedItemId());
    }

    private void addHardTagTypeFilterIfNeeded(DataSourceRequest dataSourceRequest, SolrSpecification solrFacetSpecification, Object solrScopeId) {
        List<String> tagTypeFilterList;
        if (dataSourceRequest.getTagTypeFilter() == null) {
            tagTypeFilterList = solrSpecificationFactory.extractFilterByField(dataSourceRequest, CatFileFieldType.TAG_TYPE, KendoFilterConstants.EQ);
        } else {
            tagTypeFilterList = Lists.newArrayList(dataSourceRequest.getTagTypeFilter());
        }
        if (tagTypeFilterList != null && tagTypeFilterList.size() == 1) {
            String tagTypeFilter = tagTypeFilterList.get(0);
            logger.debug("Facet filter tag type {}", tagTypeFilter);
            solrFacetSpecification.setFacetPrefix(solrScopeId + FileCatUtils.CATEGORY_SEPARATOR + tagTypeFilter + FileCatUtils.CATEGORY_SEPARATOR);
            solrFacetSpecification.setAdditionalFilter(
                    CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName() + ":" + solrScopeId + FileCatUtils.CATEGORY_SEPARATOR + tagTypeFilter + FileCatUtils.CATEGORY_SEPARATOR + "*");
        }
    }

    private SolrFacetQueryResponse getFileCountResp(DataSourceRequest dataSourceRequest, PageRequest pageRequest, Long scopeId, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.ACTIVE_ASSOCIATIONS);
        Object resolveSolrScopeId = resolveSolrScopeId(scopeId);
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.ACTIVE_ASSOCIATIONS.getSolrName() + ":" + resolveSolrScopeId + FileCatUtils.CATEGORY_SEPARATOR + "*");
        if (filterTagsByTypeIfNeeded) {
            addHardTagTypeFilterIfNeeded(dataSourceRequest, solrFacetSpecification, resolveSolrScopeId);
        }
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        solrFacetSpecification.setMinCount(1);
        return runFacetQuery(pageRequest != null ? pageRequest : PageRequest.of(0, ControllerUtils.FACET_MAX_PAGE_SIZE_FILECOUNT),
                solrFacetSpecification, c -> AssociationIdUtils.extractTagIdFromScopedSolrIdentifier(c.getName()));
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<FileTagDto>> listTagsFileCountWithFileFilter(DataSourceRequest dataSourceRequest, Long scopeId) {
        try {
            logger.debug("list Tags File Count With File Filters");
            long totalFacetCountTags = fileCatAppService.getTotalFacetCountByPrefix(dataSourceRequest,
                    CatFileFieldType.ACTIVE_ASSOCIATIONS, tagPrefix(scopeId));
            PageRequest pageRequest = dataSourceRequest.createPageRequest();
            SolrFacetQueryResponse solrFacetQueryResponse = getFileCountResp(dataSourceRequest, null, scopeId, null);
            logger.debug("list file facets by Solr field tags returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());

            FacetField facetFieldNoFilter = null;
            if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                    && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                    && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
                dataSourceRequest.clearForUnfilteredForSpecific();
                SolrFacetQueryResponse solrFacetQueryRespNoFilter = getFileCountResp(dataSourceRequest, null, scopeId,
                        CatFileFieldType.ACTIVE_ASSOCIATIONS.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
                facetFieldNoFilter = solrFacetQueryRespNoFilter.getFirstResult();
            }

            FacetPage<AggregationCountItemDTO<FileTagDto>> facetPage = convertFacetFieldResultToDto(
                    solrFacetQueryResponse, facetFieldNoFilter, pageRequest, dataSourceRequest.isShowHidden(),
                    dataSourceRequest.getSelectedItemId());
            facetPage.setTotalElements(totalFacetCountTags);
            return facetPage;
        } catch (RuntimeException e) {
            logger.error("Failed to list tags file counts with filter {}", dataSourceRequest.getFilter(), e);
            throw e;
        }
    }

    @NotNull
    private String tagPrefix(Long scopeId) {
        return resolveSolrScopeId(scopeId) + FileCatUtils.CATEGORY_SEPARATOR;
    }

    @NotNull
    private Object resolveSolrScopeId(Long scopeId) {
        return scopeId == null ? FileCatUtils.GLOBAL_SCOPE_IDENTIFIER : scopeId;
    }

    private String getTypeItemId(FileTagTypeDto item) {
        return String.valueOf(item.getId());
    }

    private FacetPage<AggregationCountItemDTO<FileTagTypeDto>> convertFacetFieldTagTypesResultToDto
            (SolrFacetQueryResponse solrFacetQueryResponse, PageRequest pageRequest, SolrFacetQueryResponse totalResp, String selectedItemId) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        List content = new ArrayList<>();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        Map<Long, AggregationCountItemDTO<FileTagTypeDto>> items = new LinkedHashMap<>();
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set (original values look like type.id
        List<Long> ids = facetField.getValues().stream().map(c -> convertTagType2ValueToId(c.getName())).collect(Collectors.toList());
        //Fetch all group ids from the database
        Map<Long, FileTagType> fileTagTypeMap = fileTagTypeService.getFileTagTypesByIdAsMap(ids);

        logger.debug("convert Facet Field result for {} elements to FileTagTypeDto. DB returned {} elements", ids.size(), fileTagTypeMap.size());
        long pageAggCount = 0;
        for (FacetField.Count count : facetField.getValues()) {
            long tagTypeId = convertTagType2ValueToId(count.getName());
            FileTagType fileTagType = fileTagTypeMap.get(tagTypeId);
            if (fileTagType == null) {
                throw new RuntimeException(messageHandler.getMessage("tag-type.unsync-vals", Lists.newArrayList(tagTypeId, count.getName())));
            }
            if (fileTagType.isHidden()) {
                continue;
            }

            AggregationCountItemDTO<FileTagTypeDto> item = items.get(tagTypeId);
            if (item == null) {
                FileTagTypeDto dto = FileTagTypeService.convertFileTagTypeToDto(fileTagType);
                item = new AggregationCountItemDTO<>(dto, count.getCount());
            } else {
                item.incrementCount(count.getCount());
            }
            items.put(tagTypeId, item);
            pageAggCount += count.getCount();
        }

        long totalAggCount = 0;
        facetField = totalResp == null ? null : totalResp.getFirstResult();
        if (facetField != null) {
            for (FacetField.Count count : facetField.getValues()) {
                totalAggCount += count.getCount();
            }
        } else {
            totalAggCount = Math.max(pageAggCount, solrFacetQueryResponse.getNumFound());
        }

        content.addAll(items.values());
        // even though we must go over all facet options to get correct values
        // we should return to ui only what was requested
        return ControllerUtils.getRelevantResultPage(content, pageRequest, selectedItemId,
                this::getTypeItemId, pageAggCount, totalAggCount);
    }

    private Long convertTagType2ValueToId(String name) {
        String substring = convertTagType2Value(name);
        return Long.valueOf(substring);
    }

    private String convertTagType2Value(String name) {
        @SuppressWarnings("UnstableApiUsage")
        List<String> split = Splitter.on(FileCatUtils.CATEGORY_SEPARATOR).splitToList(name);
        return (split.size() > 1) ? split.get(1) : split.get(0);
    }

    private String getTagItemId(FileTagDto item) {
        return String.valueOf(item.getId());
    }

    private FacetPage<AggregationCountItemDTO<FileTagDto>> convertFacetFieldResultToDto(
            SolrFacetQueryResponse solrFacetQueryResponse, FacetField facetFieldNoFilter, PageRequest pageRequest, boolean showHidden, String selectedItemId) {
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        List content = new ArrayList<>();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }

        List<FacetField.Count> values = facetField.getValues().stream()
                .filter(c -> !c.getName().startsWith(DocTypeDto.ID_PREFIX)) // Do not take doc types tags
                .filter(c -> !c.getName().startsWith(DepartmentDto.ID_PREFIX)) // Do not take department tags
                .collect(Collectors.toList());
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set (original values look like TagTypeId.TagId.Action
        List<Long> ids = values.stream()
                .map(c -> convertSolrValueToId(c.getName()))
                .collect(Collectors.toList());

        Map<String, Long> noFilterValues = facetFieldNoFilter == null ? null : facetFieldNoFilter.getValues().stream()
                .collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount));

        //Fetch all group ids from the database
        Map<Long, FileTag> fileTagMap = fileTagService.getFileTagsByIdAsMap(ids);
        Map<Long, AggregationCountItemDTO<FileTagDto>> aggregatedTagsCountMap = new HashMap<>();
        logger.debug("convert Facet Field result for {} elements to FileTagDto. DB returned {} elements", ids.size(), fileTagMap.size());
        long pageAggCount = 0;
        for (FacetField.Count count : values) {
            long tagId = convertSolrValueToId(count.getName());
            FileTag fileTag = fileTagMap.get(tagId);
            if (fileTag == null) {
                throw new RuntimeException(messageHandler.getMessage("tag.unsync-vals", Lists.newArrayList(tagId, count.getName())));
            }
            if (fileTag.getType().isHidden() && !showHidden) {
                continue; // Hidden tag
            }

            int countFiltered = (int) count.getCount();
            Long countNotFiltered = (noFilterValues == null ? null : noFilterValues.get(count.getName()));

            AggregationCountItemDTO<FileTagDto> item = aggregatedTagsCountMap.get(tagId);
            if (item == null) {
                FileTagDto fileTagDto = FileTagService.convertFileTagToDto(fileTag);
                item = new AggregationCountItemDTO<>(fileTagDto, countFiltered);
                content.add(item);
                if (countNotFiltered != null) {
                    item.setCount2(countNotFiltered);
                }
                aggregatedTagsCountMap.put(tagId, item);
            } else {
                item.incrementCount(countFiltered);
                if (countNotFiltered != null) {
                    item.incrementCount2(countNotFiltered);
                }
            }

            pageAggCount += countFiltered;
        }
        // even though we must go over all facet options to get correct values
        // we should return to ui only what was requested
        return ControllerUtils.getRelevantResultPage(content, pageRequest, selectedItemId,
                this::getTagItemId, pageAggCount, solrFacetQueryResponse.getNumFound());
    }


    private long convertSolrValueToId(String value) {
        try {
            return Long.valueOf(AssociationIdUtils.extractTagIdFromScopedSolrIdentifier(value));
        } catch (RuntimeException e) {
            logger.error("Failed to extract tag id from Solr identfier " + value, e);
            throw e;
        }
    }

    @Transactional()
    public FileTagTypeDto createFileTagType(FileTagTypeDto fileTagTypeDto) {
        validateFileTagTypeName(fileTagTypeDto.getName(), null);

        if (fileTagTypeDto.isSystem()) {
            throw new BadParameterException(messageHandler.getMessage("tag-type.system.forbidden"), BadRequestType.UNAUTHORIZED);
        }

        FileTagType fileTagType = fileTagTypeService.createFileTagType(fileTagTypeDto.getName(), fileTagTypeDto.getDescription(),
                fileTagTypeDto.isSensitive(), fileTagTypeDto.isSystem(), fileTagTypeDto.isHidden(), fileTagTypeDto.isSingleValueTag(), fileTagTypeDto.getScope());
        return FileTagTypeService.convertFileTagTypeToDto(fileTagType);
    }

    private void validateFileTagTypeName(String name, Long id) {
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            throw new BadParameterException(messageHandler.getMessage("tag-type.name.empty"), BadRequestType.MISSING_FIELD);
        }

        if (!ValidationUtils.validateNameLength(name)) {
            throw new BadParameterException(messageHandler.getMessage("tag-type.name.long"), BadRequestType.UNSUPPORTED_VALUE);
        }

        FileTagType fileTagTypeByName = fileTagTypeService.getFileTagTypeByName(name);
        if (fileTagTypeByName != null && (id == null || fileTagTypeByName.getId() != id)) {
            logger.warn("Can't create tag type. Tag type by name {} already exists ({})", name, fileTagTypeByName);
            throw new BadParameterException(messageHandler.getMessage("tag-type.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }
    }

    private void nameValidation(String name) {
        if (Strings.isNullOrEmpty(name) || Strings.isNullOrEmpty(name.trim())) {
            throw new BadParameterException(messageHandler.getMessage("tag.name.empty"), BadRequestType.MISSING_FIELD);
        }

        if (!ValidationUtils.validateNameLength(name)) {
            throw new BadParameterException(messageHandler.getMessage("tag.name.long"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Transactional
    public FileTagDto updateFileTag(long id, FileTagDto fileTagDto) {
        logger.info("Update file tag {}", id);
        nameValidation(fileTagDto.getName());
        FileTag fileTag = fileTagService.getFileTag(id);
        if (fileTagDto.getName() != null && !fileTagDto.getName().equals(fileTag.getName())) {
            fileTag = fileTagService.updateFileTagName(fileTag, fileTagDto.getName());
        }
        if (fileTagDto.getDescription() != null && !fileTagDto.getDescription().equals(fileTag.getDescription())) {
            fileTag = fileTagService.updateFileTagDescription(fileTag, fileTagDto.getDescription());
        }
        if (fileTagDto.getFileTagAction() != null && !fileTagDto.getFileTagAction().equals(fileTag.getAction())) {
            fileTag = fileTagService.updateFileTagAction(fileTag, fileTagDto.getFileTagAction());
        }
        return FileTagService.convertFileTagToDto(fileTag);
    }

    public SolrFacetQueryResponse runFacetQuery(PageRequest pageRequest,
                                                SolrSpecification solrFacetSpecification,
                                                Function<FacetField.Count, String> idExtractor) {
        return fileCatService.runFacetQuery(solrFacetSpecification, idExtractor, pageRequest, null);
    }

    @Transactional(readOnly = true)
    public List<FileTagTypeAndTagsDto> getAllFileTagTypesAndChildren(/* DataSourceRequest dataSourceRequest*/Long userId) {
        // page request is ignored for now
        List<FileTagTypeAndTagsDto> result = new ArrayList<>();

        List<FileTagType> fileTagTypes = fileTagTypeService.getFileTagTypes(userId);
        List<FileTag> fileTagsAll = fileTagService.getFileTags();
        Map<Long, List<FileTag>> fileTagsAllByType = fileTagsAll.stream().collect(Collectors.groupingBy(FileTag::getTypeId));

        for (FileTagType fileTagType : fileTagTypes) {
            if (fileTagType.isHidden()) {
                continue;
            }
            FileTagTypeDto fileTagTypeDto = FileTagTypeService.convertFileTagTypeToDto(fileTagType);
            FileTagTypeAndTagsDto fileTagTypeAndTagsDto = new FileTagTypeAndTagsDto();
            fileTagTypeAndTagsDto.setFileTagTypeDto(fileTagTypeDto);
            List<FileTag> fileTagsByType = fileTagsAllByType.get(fileTagType.getId());
            if (fileTagsByType != null) { // Hide system tags which contain too many tags (hueristic)
                if (fileTagTypeDto.isSystem() && fileTagsByType.size() > maxSystemTagsToView) {
                    fileTagTypeAndTagsDto.setFileTags(Collections.emptyList());
                } else {
                    List<FileTagDto> fileTagDtos = FileTagService.convertFileTagsToDto(fileTagsByType);
                    fileTagTypeAndTagsDto.setFileTags(fileTagDtos);
                }
            } else {
                fileTagTypeAndTagsDto.setFileTags(Collections.emptyList());
            }
            result.add(fileTagTypeAndTagsDto);
        }

        return result;
    }

    @Transactional
    public void evaluateAndTagGroupByExtraction(String groupId, BizListItemType bizListItemType,
                                                EvaluateBizListInstructions instructions) {
        try {
            long start = timeSource.currentTimeMillis();
            SolrFacetQueryResponse solrFacetQueryResponse = getBizListItemsFacetOnGroup(groupId, bizListItemType);
            List<FacetField.Count> solrValues = solrFacetQueryResponse.getFirstResult().getValues();
            FileGroup group = groupsService.findById(groupId);
            if (group == null) {
                logger.error("Invalid group id {} to classify", groupId);
                return;
            }
            long totalTime = timeSource.secondsSince(start);
            logger.debug("Classify group {} of size {} by bizList type {} returned {} extracted bizListItems in {}ms",
                    groupId, group.getNumOfFiles(), bizListItemType, solrValues.size(), totalTime);
            if (solrValues.size() == 0) {
                logger.error("group id {} has no extracted entities", groupId);
                return;
            }
            FacetField.Count firstItem = solrValues.get(0);
            double numOfFiles = Long.valueOf(group.getNumOfFiles()).doubleValue();
            double firstItemPercentage = firstItem.getCount() / numOfFiles;
            if (firstItemPercentage >= instructions.getClassifcationSingleBliMinimumPercentage()
                    && solrValues.size() <= instructions.getClassificationSingleBliMaxOthers()) {
                tryToClassifyAsSingleItemGroup(group, solrValues, firstItem, numOfFiles, instructions);
            } else if (solrValues.size() >= instructions.getClassificationMultiMatchMinBli()) {
                tryToClassifyAsMultiMatchGroup(group, solrValues, numOfFiles, instructions);
            }
        } catch (RuntimeException e) {
            logger.error("Failed to classify group {}", groupId, e);
        }
    }

    private void tryToClassifyAsMultiMatchGroup(FileGroup group, List<FacetField.Count> values,
                                                double numOfFiles,
                                                EvaluateBizListInstructions instructions) {
        logger.debug("Group {} is potentially split across multiple BizListItems", group.getId());
        //None of the others should have more then 10% of the files
        boolean first = true;
        int othersCount = 0;
        for (FacetField.Count value : values) {
            if (first) {
                first = false;
            } else {
                othersCount += value.getCount();
            }
        }
        double othersPercentage = othersCount / numOfFiles;
        if (othersPercentage >= instructions.getClassificationMultiMatchMinOthersPercentage()) {
            logger.debug("Group has multiple BizListItems that total {}%. Classify as multiMatch", othersPercentage);
            long multiMatchTagId = instructions.getMultiMatchTagId();
            FileTag multiMatchTag = fileTagService.getFileTag(multiMatchTagId);
            addFileTagToGroup(group.getId(), multiMatchTag, multiMatchTag.getType().getScope());
        }
    }

    private void tryToClassifyAsSingleItemGroup(FileGroup group, List<FacetField.Count> values,
                                                FacetField.Count firstItem, double numOfFiles, EvaluateBizListInstructions instructions) {
        String bliId = firstItem.getName();
        logger.debug("Group {} has a predominant bizListItem {}", group.getId(), bliId);
        //None of the others should have more then 10% of the files
        //TODO - It's enough to check the second facet value for 10%, the others have lower values.
        boolean first = true;
        for (FacetField.Count value : values) {
            if (first) {
                first = false;
            } else {
                double itemPercentage = value.getCount() / numOfFiles;
                if (itemPercentage > instructions.getClassificationSingleBliMaxOthersPercentage()) {
                    logger.debug("Group has another bizList Item {} with at {}%. Can't classify", value.getName(), itemPercentage);
                    return;
                }
            }
        }
        long singleBliMatchTypeId = instructions.getSingleBliMatchTypeId();
        SimpleBizListItem simpleBizListItem = bizListItemService.getSimpleBizListItem(bliId);
        String bliName = simpleBizListItem == null ? bliId : simpleBizListItem.getEntityId();
        FileTag fileTag;
        try {
            fileTag = createFileTagIfNeeded(bliName, "Group has predominant BizListItem " + bliName, singleBliMatchTypeId, FileTagAction.SUGGESTION);
        } catch (DataIntegrityViolationException e) {
            logger.info("Tag was already created. Fetch instead: " + e.getMessage());
            fileTag = fileTagService.getFileTagByName(bliName, singleBliMatchTypeId);
        }
        addFileTagToGroup(group.getId(), fileTag, fileTag.getType().getScope());
    }

    private SolrFacetQueryResponse getBizListItemsFacetOnGroup(String groupId, BizListItemType bizListItemType) {
        FilterDescriptor groupFilter = new FilterDescriptor(CatFileFieldType.USER_GROUP_ID.getSolrName(), groupId, "eq");
        FilterDescriptor baseFilter = FilterBuildUtils.createAndFilter(groupFilter);
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(baseFilter, CatFileFieldType.BIZLIST_ITEM_EXTRACTION);
        solrFacetSpecification.filterByBizListType(bizListItemType);
        solrFacetSpecification.setMinCount(1);
        return fileCatService.runFacetQuery(solrFacetSpecification, PageRequest.of(0, CLASSIFICATION_MAX_BIZ_LISTS_TO_VIEW));
    }

    @Transactional
    public FileTagTypeDto updateFileTagType(long fileTagTypeId, FileTagTypeDto fileTagTypeDto) {
        validateFileTagTypeName(fileTagTypeDto.getName(), fileTagTypeId);
        FileTagType fileTagType = fileTagTypeService.updateFileTagType(fileTagTypeId, fileTagTypeDto);
        return FileTagTypeService.convertFileTagTypeToDto(fileTagType);
    }

    @Transactional
    public void deleteFileTagType(long fileTagTypeId) {
        fileTagTypeService.deleteFileTagType(fileTagTypeId);
    }

}