package com.cla.filecluster.service.communication;

import com.cla.filecluster.util.template.TemplateContext;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.sun.mail.smtp.SMTPSendFailedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.javamail.ConfigurableMimeFileTypeMap;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Simple async command for sending an email
 */
@Component
@Scope("prototype")
public class SendEmailCommand extends AbstractCommunicationCommand {

    private JavaMailSender mailSender;
    private String plainSubject;
    private TemplateContext subjectTemplate;
    private String[] toEmails;
    private String fromEmail;
    private String replyToEmail;

    @Value("classpath:images/DA_Logo.png")
    private Resource logoImg;

    @Override
    public Boolean execute() {
        try {
            mailSender.send(createMessage());
        } catch (MailAuthenticationException | SMTPSendFailedException authe) {
            logger.warn("Failed to connect, no password specified?");
        } catch (MessagingException e) {
            logger.error("Failed to send email.", e);
            return false;
        }
        return true;
    }

    private MimeMessage createMessage() throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, Charsets.UTF_8.name());


        ConfigurableMimeFileTypeMap fileTypeMap = new ConfigurableMimeFileTypeMap();
        fileTypeMap.setMappings("image/png\t\t\t\tpng");
        fileTypeMap.afterPropertiesSet();
        messageHelper.setFileTypeMap(fileTypeMap);

        messageHelper.setTo(toEmails);
        messageHelper.setFrom(fromEmail);
        messageHelper.setReplyTo(replyToEmail);

        String subjectText;
        if (Strings.isNullOrEmpty(plainSubject)) {
            subjectText = createSubjectFromTemplate();
        } else {
            subjectText = plainSubject;
        }
        messageHelper.setSubject(subjectText);

        String messageBodyText = createBodyFromTemplate();
        messageHelper.setText(messageBodyText, true);

        try {
            messageHelper.addInline("DA_logo.png", logoImg);
        } catch (Exception e) {
            logger.warn("Failed to attach email icon.");
        }

        return message;
    }

    String createBodyFromTemplate() {
        return bodyTemplate.process();
    }

    private String createSubjectFromTemplate() {
        return subjectTemplate.process();
    }

    SendEmailCommand setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
        return this;
    }

    SendEmailCommand setSubjectTemplate(TemplateContext subjectTemplate) {
        this.subjectTemplate = subjectTemplate;
        return this;
    }

    SendEmailCommand setToEmails(String[] toEmails) {
        this.toEmails = toEmails;
        return this;
    }

    SendEmailCommand setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
        return this;
    }


    SendEmailCommand setReplyToEmail(String replyToEmail) {
        this.replyToEmail = replyToEmail;
        return this;
    }

    SendEmailCommand setPlainSubject(String plainSubject) {
        this.plainSubject = plainSubject;
        return this;
    }
}
