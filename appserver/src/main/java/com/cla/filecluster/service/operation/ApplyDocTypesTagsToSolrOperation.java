package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * apply doc type changes to solr operation
 *
 * Created by: yael
 * Created on: 6/4/2018
 */
@Service
public class ApplyDocTypesTagsToSolrOperation implements ScheduledOperation {

    public static String MAX_DOC_TYPES_PARAM = "MAX_DOC_TYPES";

    @Autowired
    private DocTypeAppService docTypeAppService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            int max = Integer.parseInt(opData.get(MAX_DOC_TYPES_PARAM));
            docTypeAppService.applyTagsOnDocTypesToSolr(max);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String max = opData.get(MAX_DOC_TYPES_PARAM);
        if (Strings.isNullOrEmpty(max)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.max-associations.empty"), BadRequestType.MISSING_FIELD);
        }
        Integer.parseInt(max);
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.APPLY_DOC_TYPES_TAGS_TO_SOLR;
    }

    @Override
    public boolean operationTypeUnique() {
        return true;
    }
}
