package com.cla.filecluster.service.export.exporters.security.funcrole;

import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class FunctionalRoleSummaryInfoDtoMixin {

    @JsonUnwrapped
    private FunctionalRoleDto functionalRoleDto;
}
