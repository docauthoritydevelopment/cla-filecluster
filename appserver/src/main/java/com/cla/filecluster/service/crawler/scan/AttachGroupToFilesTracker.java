package com.cla.filecluster.service.crawler.scan;

import com.cla.common.constants.CatFileFieldType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.license.TrackerAppService;
import com.google.common.collect.Maps;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Responsible to track the count of new files that becomes grouped from attach operations
 */
@Component
public class AttachGroupToFilesTracker {

    private static final Logger logger = LoggerFactory.getLogger(AttachGroupToFilesTracker.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private TrackerAppService trackerAppService;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @PostConstruct
    private void init() {
        eventBus.subscribe(Topics.FILE_GROUP_ATTACH, event -> {
            if (event.getEventType() != EventType.SINGLE_FILE_GROUP_ATTACH) { // Case not SINGLE_FILE_GROUP_ATTACH drop it
                if (logger.isTraceEnabled()) {
                    logger.trace("Listening only to {}, Dropping event {}", EventType.SINGLE_FILE_GROUP_ATTACH, event);
                }
                return;
            }

            // Case required keys not exists drop it
            if (!hasEventKeys(event)) {
                logger.trace("Event is missing necessary keys for tracking dropping it, {}", event);
                return;
            }

            AuthenticationHelper.doWithAuthentication(AutoAuthenticate.ADMIN, true,
                    () -> accumulateNewGroupedFiles(event), null);
        });
    }

    private boolean hasEventKeys(Event event) {
        final Context context = event.getContext();
        return context.containsKey(EventKeys.ATTACHED_GROUPED_CONTENT_ID) ||
                (context.containsKey(EventKeys.ATTACHED_GROUPED_FILE_ID) &&
                        context.containsKey(EventKeys.ROOT_FOLDER_ID) &&
                        context.containsKey(EventKeys.FILE_MEDIA_TYPE));

    }

    private void accumulateNewGroupedFiles(Event event) {
        final Context context = event.getContext();

        if (context.containsKey(EventKeys.ATTACHED_GROUPED_FILE_ID)) {
            trackerAppService.incrementLongCount(TrackingKeys.GROUPED_FILES, 1);

            Long rfId = context.get(EventKeys.ROOT_FOLDER_ID, Long.class);
            trackerAppService.incrementLongCount(TrackingKeys.rootFolderNewGroupedFilesCountKey(rfId), 1);

            MediaType mediaType = context.get(EventKeys.FILE_MEDIA_TYPE, MediaType.class);
            trackerAppService.incrementLongCount(TrackingKeys.mediaNewGroupedFilesCountKey(mediaType), 1);
        }
        // TODO: This was probably redundant cause when we attach a file to a  group we know exactly where it is
        // Keeping it just in case we would like to update grouped count by content id
        /*else if (context.containsKey(EventKeys.ATTACHED_GROUPED_CONTENT_ID)) {
            Long contentId = context.get(EventKeys.ATTACHED_GROUPED_CONTENT_ID, Long.class);
            Map<Long, Long> countNewGroupedFilesByRootFolders = countNewGroupedFilesByField(contentId,
                    CatFileFieldType.ROOT_FOLDER_ID, Long::valueOf, 10000);
            logger.debug("Incrementing new grouped files by content id {} for {} root folders", contentId,
                    countNewGroupedFilesByRootFolders.keySet().size());
            countNewGroupedFilesByRootFolders.forEach((rfId, count) ->
                    trackerAppService.incrementLongCount(TrackingKeys.rootFolderNewGroupedFilesCountKey(rfId), count));

            Map<MediaType, Long> countNewGroupedFilesByMediaType = countNewGroupedFilesByField(contentId,
                    CatFileFieldType.MEDIA_TYPE,
                    facetName -> MediaType.valueOf(Integer.valueOf(facetName)),
                    MediaType.values().length);
            logger.debug("Incrementing new grouped files by content id {} for {} media types", contentId,
                    countNewGroupedFilesByMediaType.keySet().size());
            countNewGroupedFilesByMediaType.forEach((mediaType, count) ->
                    trackerAppService.incrementLongCount(TrackingKeys.mediaNewGroupedFilesCountKey(mediaType), count));

            long totalGrouped = countNewGroupedFilesByMediaType.values().stream().mapToLong(value -> value).sum();
            logger.debug("Incrementing total new grouped files by {}", totalGrouped);
            trackerAppService.incrementLongCount(TrackingKeys.GROUPED_FILES,
                    totalGrouped);
        }*/


    }

    /*private <T> Map<T, Long> countNewGroupedFilesByField(Long contentId,
                                                         CatFileFieldType catFileFieldType,
                                                         Function<String, T> conversion,
                                                         int limit) {
        // Build query
        final SolrQuery solrQuery = Query.create()
                .setFacetMinCount(1)
                .setFacetLimit(limit)
                .setRows(0)
                .addFacetField(catFileFieldType)
                .addFilterQuery(Query.create()
                        .addFilterWithCriteria(Criteria.create(CatFileFieldType.CONTENT_ID, SolrOperator.EQ, contentId)))
                .build();

        // Run query
        QueryResponse queryResponse = solrFileCatRepository.runQuery(solrQuery);
        if (queryResponse.getFacetFields() == null || queryResponse.getFacetFields().isEmpty()) {
            logger.warn("Empty count of new grouped files for content id {}!!", catFileFieldType,
                    contentId);
            return Maps.newHashMap();
        }

        // Extract results
        FacetField facetField = queryResponse.getFacetFields().get(0);
        return facetField.getValues().stream()
                .collect(Collectors.toMap(count -> conversion.apply(count.getName()), FacetField.Count::getCount));
    }*/

}
