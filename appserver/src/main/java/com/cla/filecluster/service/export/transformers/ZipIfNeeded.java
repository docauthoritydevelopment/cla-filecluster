package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.progress.ProgressService;
import org.reactivestreams.Publisher;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.List;

public class ZipIfNeeded extends ProgressedTransformer<Resource, Resource> {


    private ZipIfNeeded(String exportId, ProgressService.Handler progress) {
        super(exportId, progress);
    }

    public static ZipIfNeeded with(String exportId, ProgressService.Handler progress) {
        return new ZipIfNeeded(exportId, progress);
    }

    @Override
    protected DataBuffer createDataBuffer(Resource resource) {
        throw new UnsupportedOperationException("ZipIfNeeded cannot be transformed to a data output stream");
    }

    @Override
    public Publisher<Resource> apply(Flux<Resource> inputFlux) {
        return inputFlux.buffer().map(this::zipFiles);
    }

    private Resource zipFiles(List<Resource> files) {
        return null;
    }
}
