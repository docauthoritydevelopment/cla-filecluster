package com.cla.filecluster.service.mcf.internals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.security.GeneralSecurityException;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;



public class Encryptor {
	/** Encryption salt property */
	public static final String saltProperty = "org.apache.manifoldcf.salt";

	public static String encrypt(final String saltValue, final String passCode, final String input) {
		if (input == null)
			return null;
		if (input.length() == 0)
			return input;

		try {
			final ByteArrayOutputStream os = new ByteArrayOutputStream();

			// Write IV as a prefix:
			final byte[] iv = getSecureRandom();
			os.write(iv);
			os.flush();

			final Cipher cipher = getCipher(saltValue, Cipher.ENCRYPT_MODE, passCode, iv);
			final CipherOutputStream cos = new CipherOutputStream(os, cipher);
			final Writer w = new OutputStreamWriter(cos, java.nio.charset.StandardCharsets.UTF_8);
			w.write(input);
			w.flush();
			// These two shouldn't be necessary, but they are.
			cos.flush();
			cos.close();
			final byte[] bytes = os.toByteArray();
			return new Base64().encodeByteArray(bytes);
		} catch (final IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	protected static Cipher getCipher(final int mode, final String passCode,
			final byte[] iv)  {
		return getCipher(saltProperty, mode, passCode, iv);
	}

	protected static Cipher getCipher(final String saltValue, final int mode, final String passCode, final byte[] iv)
			{
		try {
			final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			final KeySpec keySpec = new PBEKeySpec(passCode.toCharArray(), saltValue.getBytes(), 1024, 128);
			final SecretKey secretKey = factory.generateSecret(keySpec);

			final Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			final SecretKeySpec key = new SecretKeySpec(secretKey.getEncoded(), "AES");
			final IvParameterSpec parameterSpec = new IvParameterSpec(iv);
			cipher.init(mode, key, parameterSpec);
			return cipher;
		} catch (final GeneralSecurityException gse) {
			throw new RuntimeException("Could not build a cipher: " + gse.getMessage(), gse);
		}
	}

	protected static byte[] getSecureRandom() {
		final SecureRandom random = new SecureRandom();
		final byte[] iv = new byte[IV_LENGTH];
		random.nextBytes(iv);
		return iv;
	}

	private static String OBFUSCATION_PASSCODE = "NowIsTheTime";
	private static String OBFUSCATION_SALT = "Salty";

	/**
	 * Encode a string in a reversible obfuscation.
	 *
	 * @param input
	 *            is the input string.
	 * @return the output string.
	 */
	public static String obfuscate(final String input) {
		return encrypt(OBFUSCATION_SALT, OBFUSCATION_PASSCODE, input);
	}

	protected static final int IV_LENGTH = 16;

	
}
