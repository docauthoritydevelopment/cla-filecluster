package com.cla.filecluster.service.label;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.label.conf.TagLabelPolicy;
import com.cla.filecluster.label.transformers.TagLabelTransformer;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.repository.solr.query.AtomicUpdateBuilder;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.google.common.base.Strings;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;

import static com.cla.common.constants.CatFileFieldType.*;

/**
 * Created by: yael
 * Created on: 7/11/2019
 */
@Service
public class LabelingAppService {
    private final static Logger logger = LoggerFactory.getLogger(LabelingAppService.class);

    @Autowired
    private LabelingService labelingService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private FileTagService fileTagService;

    @Value("${tag-label.policy.file:}")
    private String tagLabelPolicyFilePath;

    private TagLabelTransformer tagLabelTransformer = new TagLabelTransformer();

    @SuppressWarnings("unused")
    @PostConstruct
    private void init() {
        loadTagPolicyFromFile();
    }

    private void loadTagPolicyFromFile() {
        if (tagLabelPolicyFilePath == null || tagLabelPolicyFilePath.isEmpty()) {
            logger.info("No tag-label policy file defined - ignored");
        } else {
            logger.info("Load tag-label policy from file {}", tagLabelPolicyFilePath);
            loadTagLabelPolicyFromFile(tagLabelPolicyFilePath);
        }
    }

    private void loadTagLabelPolicyFromFile(String labelTransformerFilePath) {
        try {
            File configFile = new File(labelTransformerFilePath);
            TagLabelPolicy config = ModelDtoConversionUtils.getMapper().readValue(configFile, TagLabelPolicy.class);
            if (config != null && config.getTagLabelMappings() != null && !config.getTagLabelMappings().isEmpty()) {
                Map<Long, Long> tagIdToDaLabelId = new HashMap<>();
                try {
                    config.getTagLabelMappings().forEach(tagLabelMapping -> {
                        FileTagType type = fileTagTypeService.getFileTagTypeByName(tagLabelMapping.getTagType());
                        if (type != null) {
                            FileTag tag = fileTagService.getFileTagByName(tagLabelMapping.getTagName(), type);
                            if (tag != null) {
                                LabelDto label = labelingService.getForDaLabel(tagLabelMapping.getDaLabel());
                                if (label != null) {
                                    tagIdToDaLabelId.put(tag.getId(), label.getId());
                                }
                            }
                        }
                    });
                } catch (Exception e) {
                    logger.error("failed loading tag-label policy from file", e);
                }
                tagLabelTransformer.setTagIdToDaLabelId(tagIdToDaLabelId);
            }
        } catch (Exception e) {
            logger.error("failed loading ltag-label policy from file", e);
        }
    }

    public void setLabelOnFile(long fileId, long labelId) {
        SolrFileEntity fileEntity = fileCatService.getFileEntity(fileId);
        if (fileEntity == null) {
            throw new BadParameterException(messageHandler.getMessage("file-action.get.not-found"), BadRequestType.ITEM_NOT_FOUND);
        }

        LabelDto labelDto = labelingService.getLabelByIdCached(labelId).orElse(null);
        if (labelDto == null) {
            throw new BadParameterException(messageHandler.getMessage("label.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        if (fileEntity.getDaLabels() != null && fileEntity.getDaLabels().contains(labelId)) {
            throw new BadParameterException(messageHandler.getMessage("label.exist-on-file"), BadRequestType.ITEM_ALREADY_EXISTS);
        }

        String actionExpectedPrefix = LabelingService.createActionExpected(LabelingVendor.DocAuthority.getName(), String.valueOf(labelId), "");

        if (fileEntity.getActionsExpected() != null) {
            fileEntity.getActionsExpected().forEach(ae -> {
                if (ae.startsWith(actionExpectedPrefix)) {
                    throw new BadParameterException(messageHandler.getMessage("label.exist-on-file"), BadRequestType.ITEM_ALREADY_EXISTS);
                }
            });
        }

        String actionExpected = LabelingService.createActionExpected(LabelingVendor.DocAuthority.getName(), String.valueOf(labelId), "0");

        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
        builder.setId(ID, fileEntity.getId());
        builder.addModifierNotNull(ACTIONS_EXPECTED, SolrOperator.ADD, actionExpected);
        builder.addModifierNotNull(LABEL_PROCESS_DATE, SolrOperator.SET, 0);
        builder.addModifier(UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis());
        List<SolrInputDocument> atomicUpdates = new ArrayList<>();
        atomicUpdates.add(builder.build());
        fileCatService.saveAll(atomicUpdates);
        fileCatService.softCommitNoWaitFlush();
    }

    public void setLabelOnFilesByTags(final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        tagLabelTransformer.getTagIdToDaLabelId().forEach((tagId, labelId) -> {
            try {
                SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
                String[] values = solrSpecificationFactory.convertScopedTagIdToFilterValue(String.valueOf(tagId));
                solrSpecification.setAdditionalFilter(SCOPED_TAGS.filter(values[0]));
                addActionExpectedBySolrSpec(solrSpecification, labelId);
            } catch (Exception e) {
                logger.error("failed setting label {} for tag {} on files filter", labelId, tagId, e);
            }
        });
    }

    private void addActionExpectedBySolrSpec(SolrSpecification solrSpecification, Long labelId) {
        String actionExpected = LabelingService.createActionExpected(LabelingVendor.DocAuthority.getName(), String.valueOf(labelId), "0");
        Integer res = fileCatService.updateFieldBySolrSpecification(solrSpecification, ACTIONS_EXPECTED, SolrFieldOp.ADD, actionExpected, false);
        if (res != null && res > 0) {
            fileCatService.updateFieldBySolrSpecification(solrSpecification, LABEL_PROCESS_DATE, SolrFieldOp.SET, "0", true);
        }
        logger.debug("set label {} on files, {} files updated", labelId, res);
    }

    public Long setLabelOnFiles(final Map<String, String> params) {
        if (!params.containsKey("labelId")) {
            throw new BadParameterException(messageHandler.getMessage("set-label-action.label.empty"), BadRequestType.MISSING_FIELD);
        }

        Long labelId = Long.parseLong(params.get("labelId"));

        LabelDto labelDto = labelingService.getLabelByIdCached(labelId).orElse(null);
        if (labelDto == null) {
            throw new BadParameterException(messageHandler.getMessage("label.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
        addActionExpectedBySolrSpec(solrSpecification, labelId);
        return labelId;
    }

    public FacetPage<AggregationCountItemDTO<LabelDto>> getLabelFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("Get Label {} File Counts With File Filters");
        CatFileFieldType facetType = CatFileFieldType.DA_LABELS;
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
        solrFacetSpecification.setMinCount(1);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
        logger.debug("Get Label File Counts returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryResponseNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            SolrSpecification solrFacetSpecificationNoFilter =
                    solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
            solrFacetSpecificationNoFilter.setMinCount(1);
            solrFacetSpecificationNoFilter.setAdditionalFilter(
                    facetType.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
            solrFacetQueryResponseNoFilter = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecificationNoFilter);
        }

        return convertFacetFieldResultToLabel(
                solrFacetQueryResponse.getFirstResult(), solrFacetQueryResponseNoFilter, solrFacetQueryResponse.getPageRequest());
    }

    public FacetPage<AggregationCountItemDTO<String>> getGeneralMetadataFileCounts(DataSourceRequest dataSourceRequest) {
        logger.debug("Get GeneralMetadata {} File Counts With File Filters");
        CatFileFieldType facetType = CatFileFieldType.GENERAL_METADATA;
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
        solrFacetSpecification.setMinCount(1);
        SolrFacetQueryResponse solrFacetQueryResponse = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecification);
        logger.debug("Get GeneralMetadata File Counts returned {} results", solrFacetQueryResponse.getFirstResult().getValueCount());

        SolrFacetQueryResponse solrFacetQueryResponseNoFilter = null;
        if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearForUnfilteredForSpecific();
            SolrSpecification solrFacetSpecificationNoFilter =
                    solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetType);
            solrFacetSpecificationNoFilter.setMinCount(1);
            solrFacetSpecificationNoFilter.setAdditionalFilter(
                    facetType.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
            solrFacetQueryResponseNoFilter = fileCatService.runFacetQuery(dataSourceRequest, solrFacetSpecificationNoFilter);
        }

        return convertFacetFieldResultForMetadata(
                solrFacetQueryResponse.getFirstResult(), solrFacetQueryResponseNoFilter, solrFacetQueryResponse.getPageRequest());
    }

    private FacetPage<AggregationCountItemDTO<LabelDto>> convertFacetFieldResultToLabel(
            FacetField facetField, SolrFacetQueryResponse solrFacetQueryResponseNoFilter, PageRequest pageRequest) {
        Map<String, AggregationCountItemDTO<LabelDto>> content = new LinkedHashMap<>();
        for (FacetField.Count count : facetField.getValues()) {
            String dtoId = count.getName();
            LabelDto dto;
            if (Strings.isNullOrEmpty(dtoId)) {
                dto = new LabelDto();
            } else {
                dto = labelingService.getLabelByIdCached(Long.parseLong(dtoId)).orElse(new LabelDto());
            }
            AggregationCountItemDTO<LabelDto> item = new AggregationCountItemDTO<>(dto, count.getCount());
            content.put(count.getName(), item);
        }

        if (solrFacetQueryResponseNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryResponseNoFilter.getFirstResult();
            for (FacetField.Count count : facetFieldNoFilter.getValues()) {
                AggregationCountItemDTO<LabelDto> item = content.get(count.getName());
                if (item != null) {
                    item.setCount2(count.getCount());
                }
            }
        }

        return new FacetPage<>(new ArrayList<>(content.values()), pageRequest);
    }

    private FacetPage<AggregationCountItemDTO<String>> convertFacetFieldResultForMetadata(
            FacetField facetField, SolrFacetQueryResponse solrFacetQueryResponseNoFilter, PageRequest pageRequest) {
        Map<String, AggregationCountItemDTO<String>> content = new LinkedHashMap<>();
        for (FacetField.Count count : facetField.getValues()) {
            String generalMetadata = count.getName();
            AggregationCountItemDTO<String> item = new AggregationCountItemDTO<>(generalMetadata, count.getCount());
            content.put(count.getName(), item);
        }

        if (solrFacetQueryResponseNoFilter != null) {
            FacetField facetFieldNoFilter = solrFacetQueryResponseNoFilter.getFirstResult();
            for (FacetField.Count count : facetFieldNoFilter.getValues()) {
                AggregationCountItemDTO<String> item = content.get(count.getName());
                if (item != null) {
                    item.setCount2(count.getCount());
                }
            }
        }
        return new FacetPage<>(new ArrayList<>(content.values()), pageRequest);
    }
}
