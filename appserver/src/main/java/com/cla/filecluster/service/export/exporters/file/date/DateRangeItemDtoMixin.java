package com.cla.filecluster.service.export.exporters.file.date;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class DateRangeItemDtoMixin {

    @JsonProperty(DateHeaderFields.NAME)
    private String name;

    @JsonProperty(DateHeaderFields.START_DATE)
    public abstract String getStartAbsoluteDate();
}
