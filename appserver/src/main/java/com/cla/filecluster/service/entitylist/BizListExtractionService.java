package com.cla.filecluster.service.entitylist;

import com.cla.common.constants.BizListFieldType;
import com.cla.common.domain.dto.extraction.ExtractedEntityDto;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.SolrBizListBuilder;
import com.cla.filecluster.domain.dto.SolrContentEntity;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.bizlist.BizListItemAlias;
import com.cla.filecluster.domain.entity.bizlist.ConsumerBizListItem;
import com.cla.filecluster.domain.entity.bizlist.SimpleBizListItem;
import com.cla.filecluster.domain.entity.extraction.ExtractionRule;
import com.cla.filecluster.repository.solr.SolrBizListRepository;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.extractor.entity.ContentSearchService;
import com.cla.filecluster.service.extractor.entity.ExtractionRulesUtils;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 *
 * Created by uri on 13/01/2016.
 */
@Service
public class BizListExtractionService {

    private static final Logger logger = LoggerFactory.getLogger(BizListExtractionService.class);

    @Autowired
    private SolrBizListRepository solrBizListRepository;

    @Autowired
    private ContentSearchService contentSearchService;

    @Autowired
    private OpStateService opStateService;

    private TimeSource timeSource = new TimeSource();

    public List<ExtractionRule> extractRules(BizList bizList) {
        String rulesFileName = bizList.getRulesFileName();
        logger.debug("Extract rules file {} from bizList {}", bizList.getRulesFileName(), bizList);
        try {

            final List<String> rulesLines = Files.readLines(new File(rulesFileName), Charsets.UTF_8)
                    .stream()
                    .filter(l -> !(l.isEmpty() || l.startsWith("#")))
                    .collect(toList());
            List<ExtractionRule> extractionRules = ExtractionRulesUtils.parseRules(rulesLines);
            logger.info("Found {} active extraction rules:", extractionRules.size());
            extractionRules.forEach(r -> logger.debug("   {}", r.toString()));
            return extractionRules;
        }
        catch (IOException e) {
            logger.error("Failed to extract rules", e);
            throw new RuntimeException("Failed to exract rules", e);
        }

    }

    public ExtractedEntityDto convertBizListItem(SimpleBizListItem simpleBizListItem, BizList bizList, boolean convertAliases) {
        ExtractedEntityDto extractedEntityDto = new ExtractedEntityDto();
        extractedEntityDto.setClearId(simpleBizListItem.getSid());
        extractedEntityDto.setBizListId(simpleBizListItem.getBizList().getId());
        extractedEntityDto.setExtractionSessionId(bizList.getExtractionSessionId());
        extractedEntityDto.setBizListItemType(bizList.getBizListItemType());
        Map<String, Object> fields = new HashMap<>();
        if (simpleBizListItem instanceof ConsumerBizListItem) {
            ConsumerBizListItem consumerBizListItem = (ConsumerBizListItem) simpleBizListItem;
            Map<String, Object> headerValueMap = ModelDtoConversionUtils.jsonToMap(consumerBizListItem.getEncryptedFieldsJson());
            extractedEntityDto.addFields(headerValueMap);
        }
        else {
            if (convertAliases && simpleBizListItem.getBizListItemAliases() != null && !simpleBizListItem.getBizListItemAliases().isEmpty()) {
                List<String> aliases = simpleBizListItem.getBizListItemAliases().stream()
                        .map(BizListItemAlias::getEncryptedAlias)
                        .collect(Collectors.toList());
                fields.put("alias", aliases);
                if (aliases.size() == 0) {
                    logger.warn("No aliases found for bizListItem {}", simpleBizListItem);
                }
            }
        }
        fields.put("name", simpleBizListItem.getName());
        fields.put("id", simpleBizListItem.getEntityId());
        extractedEntityDto.addFields(fields);
        return extractedEntityDto;
    }

    @Transactional
    @AutoAuthenticate
    public void extractBizListItemDto(ExtractedEntityDto extractedEntityDto, ExtractionRule extractionRule, Date filterDate, boolean isFilterDateAvtive) {

        String query = null;
        if (extractionRule != null) {
            query = extractionRule.getQuery();
        }
        try {
            ExtractionRule updatedRule = contentSearchService.convertRule(extractionRule, extractedEntityDto);
            logger.trace("Extracting rule {} ...", updatedRule);
            query = updatedRule.getQuery();
            long start = timeSource.currentTimeMillis();
            final Collection<SolrContentEntity> extractionSolrEntities = contentSearchService.find(query, filterDate, isFilterDateAvtive);
            long duration = timeSource.millisSince(start);
            logger.trace("extractionSolrEntities took {} ms", duration);
            if (!(extractionSolrEntities.isEmpty())) {
                try {
                    logger.trace("Extracting rule {} query is '{}' from itemId {}. Got {} results",
                            updatedRule,
                            updatedRule.getQuery(),
                            extractedEntityDto.getClearId(),
                            extractionSolrEntities.size());
                    saveBizListItemExtractions(extractionSolrEntities, extractedEntityDto, extractionRule);

                }
                catch (Exception e) {
                    logger.error("Error saving extraction for rule {} entity {}, {}.",
                            extractionRule == null ? null : extractionRule.toString(),
                            extractedEntityDto.getClearId(), e.getMessage());
                }
            }
            else {
                logger.trace("Extracting rule {} query is '{}' from itemId {}. No results",
                        updatedRule,
                        updatedRule.getQuery(),
                        extractedEntityDto.getClearId());
            }
        } catch (RuntimeException e) {
            logger.error("Exception while extracting bizListItem {} (query: {})", extractedEntityDto,query, e);
            opStateService.addCurrentPhaseError();
            if (opStateService.isStopOnError()) {
                throw e;
            }
        }
    }

    private synchronized void saveBizListItemExtractions(Collection<SolrContentEntity> extractionSolrEntities, ExtractedEntityDto extractedEntityDto, ExtractionRule extractionRule) {
        List<SolrInputDocument> extractions = new ArrayList<>();
        Map<String, SolrBizListBuilder> buildersToCheck = new HashMap<>();
        List<String> idsToCheck = new ArrayList<>();
        for (final SolrContentEntity extractionSolrEntity : extractionSolrEntities) {
            List<Long> fileIds = contentSearchService.findFileIds(extractionSolrEntity);
            for (Long fileId : fileIds) {
                SolrBizListBuilder builder = createBizListItemExtraction(extractedEntityDto, fileId);
                builder.setRuleName(extractionRule.getName());
                Integer aggMinCount = null;
                if (extractionRule.getType().equals(ExtractionRule.RuleType.AGGREGATED_RULE)) {
                    aggMinCount = (int) extractionRule.getParam();
                    builder.setAggMinCount(aggMinCount);
                } else {
                    builder.setAggMinCount(0);
                }
                String id = solrBizListRepository.calcId(extractionSolrEntity.getId(), fileId, extractedEntityDto.getBizListId(),
                        extractionRule.getName(), extractedEntityDto.getClearId(), extractedEntityDto.getBizListItemType().ordinal());
                builder.setId(id);
                builder.setCounter(1L);

                if (aggMinCount != null && aggMinCount > 0) {
                    idsToCheck.add(id);
                    buildersToCheck.put(id, builder);
                } else {
                    extractions.add(builder.buildAtomicUpdate());
                }
            }
        }

        // check if exists, if does - increment otherwise add to creation list
        if (idsToCheck.size() > 0) {
            Collection<String> idsExisting = solrBizListRepository.findByIds(idsToCheck);
            idsToCheck.forEach(
                    id -> {
                        SolrBizListBuilder builder = buildersToCheck.get(id);
                        if (idsExisting != null && idsExisting.contains(id)) {
                            try {
                                UpdateResponse res = solrBizListRepository.atomicUpdateSingleField(id, BizListFieldType.COUNTER, SolrOperator.INC, 1, false);
                                if (res == null) { // failed after retry doesn't throw exception
                                    extractions.add(builder.buildAtomicUpdate());
                                }
                            } catch (Exception e) {
                                extractions.add(builder.buildAtomicUpdate());
                            }
                        } else {
                            extractions.add(builder.buildAtomicUpdate());
                        }
                    }
            );
        }

        if (extractions.size() > 0) {
            logger.debug("Saving {} extractions for bizListId {}", extractions.size(), extractedEntityDto.getClearId());
            // TODO: Scale up - support large number of extractions by Solr paging + Save extraction results pages in separate transactions
            //          (remove @Transactional from extractBizListItemDto()
            solrBizListRepository.createFromDocuments(extractions);
            solrBizListRepository.commit();
        } else {
            solrBizListRepository.commit();
        }
    }

    private SolrBizListBuilder createBizListItemExtraction(ExtractedEntityDto extractedEntityDto, Long fileId) {
        SolrBizListBuilder builder = SolrBizListBuilder.create();
        builder.setBizListId(extractedEntityDto.getBizListId())
                .setBizListItemSid(extractedEntityDto.getClearId())
                .setBizListItemType(extractedEntityDto.getBizListItemType().ordinal())
                .setFileId(fileId)
                .setDirty(true);
        return builder;
    }

    public void clearPreviousExtraction(long bizListId) {
        solrBizListRepository.deleteForBizList(bizListId);
        solrBizListRepository.commit();
    }
}
