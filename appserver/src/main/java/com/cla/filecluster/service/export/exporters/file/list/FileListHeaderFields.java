package com.cla.filecluster.service.export.exporters.file.list;

import com.cla.filecluster.service.export.exporters.ScannedItemsHeaderFields;

public interface FileListHeaderFields extends ScannedItemsHeaderFields {
    String NAME = "Name";
    String TYPE = "Type";
    String NUM_OF_COPIES = "Copies #";
    String EXTRACTIONS = "Client extractions";
    String ASSOCIATIONS = "Client associations";
    String DOC_TYPES = "DocTypes";
    String ROLES = "Functional roles";
    String TAGS = "Tags";
    String GROUP_NAME = "Group name";
    String SIZE = "Size";
    String OWNER = "Owner";
    String CREATION_DATE = "Creation date";
    String MODIFIED_DATE = "Modified date";
    String ACCESSED_DATE = "Accessed date";
    String PATTERNS = "Patterns";
}
