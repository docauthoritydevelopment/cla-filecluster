package com.cla.filecluster.service.report;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.filecluster.service.report.dashboardChartsEnrichers.DefaultEnricher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DashboardChartFactory {

    @Autowired
    private List<DashboardChartDtoBuilder> chartBuilders;

    @Autowired
    private List<DashboardDataEnricher> enrichers;

    @Autowired
    private List<DashboardChartQueryBuilder> queryBuilders;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    private Map<ChartType, DashboardChartDtoBuilder> myChartsMap = new HashMap<>();
    private Map<String, DashboardDataEnricher> myChartsEnricherMap = new HashMap<>();
    private Map<ChartQueryType, DashboardChartQueryBuilder> myChartsBuilderMap = new HashMap<>();

    @PostConstruct
    public void initMyServiceCache() {
        for (DashboardChartDtoBuilder chartComponent : chartBuilders) {
            myChartsMap.put(chartComponent.getType(), chartComponent);
        }
        for (DashboardDataEnricher chartEnricher : enrichers) {
            myChartsEnricherMap.put(chartEnricher.getEnricherType(), chartEnricher);
        }
        for (DashboardChartQueryBuilder chartBuilder: queryBuilders) {
            myChartsBuilderMap.put(chartBuilder.getQueryBuilderType(), chartBuilder);
        }
    }

    public DashboardChartDtoBuilder getBuilder(ChartType type) {
        DashboardChartDtoBuilder chartComponent = myChartsMap.get(type);
        if (chartComponent == null) throw new RuntimeException("Unknown chart type: " + type);
        return chartComponent;
    }

    public DashboardDataEnricher getEnricher(CatFileFieldType type, String prefix) {
        DashboardDataEnricher ans = myChartsEnricherMap.get(dashboardChartsUtil.getDashboardChartEnricherUniqueName(type, prefix));
        if (ans == null) {
            return  myChartsEnricherMap.get(DefaultEnricher.DEFAULT_ENRICHER_TYPE);
        }
        return ans;
    }

    public DashboardChartQueryBuilder getQueryBuilder(ChartQueryType chartQueryType) {
        return myChartsBuilderMap.get(chartQueryType);
    }
}
