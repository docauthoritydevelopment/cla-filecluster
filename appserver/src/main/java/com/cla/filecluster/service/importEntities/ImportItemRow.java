package com.cla.filecluster.service.importEntities;

import com.cla.common.domain.dto.RowDTO;

/**
 * Created by liora on 27/04/2017.
 */
public abstract class ImportItemRow {
    private RowDTO rowDto;

    private boolean isError;
    private boolean isParsingError;
    private boolean isWarning;
    private String message;

    public RowDTO getRowDto() {
        return rowDto;
    }

    public void setRowDto(RowDTO rowDto) {
        this.rowDto = rowDto;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getError() {
        return isError;
    }

    public void setError(Boolean error) {
        isError = error;
    }

    public boolean isWarning() {
        return isWarning;
    }

    public boolean isParsingError() {
        return isParsingError;
    }

    public void setParsingError(boolean parsingError) {
        isParsingError = parsingError;
    }

    public void setWarning(boolean warning) {
        isWarning = warning;
    }

    @Override
    public String toString() {
        return "ImportItemRow{" +
                ", message='" + message + '\'' +
                ", isError='" + isError + '\'' +
                ", isParsingError='" + isParsingError + '\'' +
                ", isWarning='" + isWarning + '\'' +
                '}';
    }


}
