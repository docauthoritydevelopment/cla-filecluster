package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: ophir
 * Created on: 4/18/2019
 */
public abstract class TextSearchPatternSettingsDtoMixin {

    @JsonProperty(TextSearchPatternHeaderFields.NAME)
    private String name;

    @JsonProperty(TextSearchPatternHeaderFields.DESCRIPTION)
    private String description;

    @JsonProperty(TextSearchPatternHeaderFields.ACTIVE)
    private boolean active;

    @JsonProperty(TextSearchPatternHeaderFields.PATTERN)
    private String pattern;

    @JsonProperty(TextSearchPatternHeaderFields.CLASS)
    private String validatorClass;

    @JsonProperty(TextSearchPatternHeaderFields.CATEGORY)
    private String categoryName;

    @JsonProperty(TextSearchPatternHeaderFields.SUB_CATEGORY)
    private String subCategoryName;

}
