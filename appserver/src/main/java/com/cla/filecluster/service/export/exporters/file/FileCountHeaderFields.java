package com.cla.filecluster.service.export.exporters.file;

public interface FileCountHeaderFields {
    String ID = "Id";
    String NAME = "Name";
    String NUM_OF_FILES = "Number of files";
    String USER = "User";
    String TYPE = "Type";
    String MASK = "Mask";
    String PERMISSION = "Permission";
}
