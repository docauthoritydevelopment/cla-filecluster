package com.cla.filecluster.service.export.exporters.settings.data_center;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public interface CustomerDataCenterHeaderFields {

    String NAME = "Name";
    String DEFAULT = "Default";
    String DESCRIPTION = "Description";
    String MPS = "Media processors";
    String ROOT_FOLDERS = "Root folders";
    String LOCATION = "Location";
    String CREATED = "Created on";
}
