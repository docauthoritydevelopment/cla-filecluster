package com.cla.filecluster.service.export.exporters.settings.doctype;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface DocTypeSettingsHeaderFields {
    String ID = "ID";
    String NAME = "Name";
    String UNIQUE = "UniqueName";
    String PARENT = "ParentID";
}
