package com.cla.filecluster.service.categories;

import com.cla.common.constants.*;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.category.DeepPageImpl;
import com.cla.common.domain.dto.crawler.FileEntity;
import com.cla.common.domain.dto.extraction.SearchPatternCountDto;
import com.cla.common.domain.dto.file.*;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.pst.PstPath;
import com.cla.common.utils.CollectionsUtils;
import com.cla.common.utils.CsvUtils;
import com.cla.common.utils.Triple;
import com.cla.connector.domain.dto.acl.AclInheritanceType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.ItemType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.connector.utils.FileTypeUtils;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.batch.loader.IngestBatchStore;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.converters.FileCatBuilder;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.categorization.FileCategory;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.specification.ClaFileByFolderRecursiveSpecification;
import com.cla.filecluster.domain.specification.ClaFileByUserGroupSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.repository.SequenceIdGenerator;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.query.*;
import com.cla.filecluster.service.extractor.pattern.RegulationsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.files.ingest.ProcessingErrorTypeTagTranslator;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.base.Strings;
import com.google.common.collect.*;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.common.constants.CatFileFieldType.*;
import static com.cla.filecluster.repository.solr.query.SolrOperator.SET;

@Service
public class FileCatService {
    private final static Logger logger = LoggerFactory.getLogger(FileCatService.class);

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private ProcessingErrorTypeTagTranslator processingErrorTypeTagTranslator;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private RegulationsService regulationsService;

    @Autowired
    protected SequenceIdGenerator sequenceIdGenerator;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private FileCatAclService fileCatAclService;

    @Value("${scanner.metadata.removeOwnerQuotes:false}")
    private boolean removeOwnerQuotes;

    @Value("${categories.include.others:false}")
    private boolean includeOthers;

    @Value("${create.CategoryFiles.page.size:1000}")
    private int createCategoryFilesPageSize;

    @Value("${scanner.filesToProcess.excludeExt:}")
    private String[] scanFullProcessExcludeExt;            // set a black list with all file extensions that should be ignored after initial scan.

    @Value("${scanner.filesToProcess.includeExt:}")
    private String[] scanFullProcessIncludeExt;            // mark which files should be fully processed after scan. If empty - all file extensions will be processed;

    @Value("${ingest.file.commit-solr-during:false}")
    private boolean commitSolrDuringIngest;

    @Value("${fileCatService.cat-files-repository.do-optimize:false}")
    private boolean catFilesRepositoryDoOptimize;

    @Value("${fileCatService.update-catfiles.commit:SOFT_NOWAIT}")
    private SolrCommitType updateCatFilesCommitType;

    @Value("${reports.document-file-types:1,2,20}")
    private String[] documentFileTypes;

    @Value("${file.container-cache.evict-time:7200000}")
    private int containerCacheEvict;

    @Value("${ingest.email.parse-x500-addresses:true}")
    private boolean parseX500Addresses;

    @Value("${ingest.email.x500.extract-non-domain-addresses:true}")
    private boolean extractNonDomainX500CN;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private IngestBatchResultsProcessor ingestBatchResultsProcessor;

    @Autowired
    private SolrContentMetadataRepository contentMetadataRepository;

    private Map<String, ContainerData> containerIds = new ConcurrentHashMap<>();

    @PostConstruct
    public void init() {
        logger.info("Setting fileCatBuilder.setParseX500Address({}).setExtractNonDomainX500CN({})", parseX500Addresses ? "true" : "false", extractNonDomainX500CN ? "true" : "false");
    }

    @Scheduled(initialDelayString = "120000", fixedRateString = "${file.container-cache.flush-interval-in-millis:120000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void clearContainerCache() {
        List<String> toRemove = new ArrayList<>();

        containerIds.forEach((key, value) -> {
            if (value.type.equals(FileType.MAIL)) { // email as a container should be saved for very little time in cache
                if (System.currentTimeMillis() - value.dateAdded > (containerCacheEvict / 15)) {
                    toRemove.add(key);
                }
            } else if (System.currentTimeMillis() - value.dateAdded > containerCacheEvict) {
                toRemove.add(key);
            }
        });

        toRemove.forEach(key -> containerIds.remove(key));
    }

    public static Long getContentId(SolrFileEntity entity) {
        if (entity == null) return null;
        return (entity.getContentId() == null ? null : Long.parseLong(entity.getContentId()));
    }

    public static String getBaseNameNoExtention(SolrFileEntity entity) {
        String fn = entity.getBaseName();
        final String ext = entity.getExtension();
        if (fn != null && ext != null) {
            fn = fn.substring(0, Integer.max(0, fn.length() - ext.length() - 1));
        }
        return fn;
    }

    public void commitToSolr() {
        solrFileCatRepository.commit();
    }

    public void optimize() {
        if (catFilesRepositoryDoOptimize) {
            logger.warn("Please notice, fileCatService.cat-files-repository.do-optimize set to true, will start a potentially heavy optimize on CatFiles index");
            solrFileCatRepository.optimize();
        } else {
            logger.debug("fileCatService.cat-files-repository.do-optimize set o false, skipping CatFiles index optimization");
        }
    }

    public Optional<SolrFileEntity> getOne(Long fileId) {
        return solrFileCatRepository.getOneByFilterCriterias(Criteria.create(FILE_ID, SolrOperator.EQ, fileId));
    }

    public List<SolrFileEntity> find(SolrQuery query) {
        return solrFileCatRepository.find(query);
    }

    public Map<Long, String> getIdsForFileIds(Collection<Long> fileIds) {
        Map<Long, String> fileIdsMap = null;
        if (fileIds != null && !fileIds.isEmpty()) {
            List<SolrFileEntity> files = AuthenticationHelper.doWithAuthentication(
                    AutoAuthenticate.ADMIN, false,
                    () -> this.find(Query.create()
                            .addFilterWithCriteria(Criteria.create(FILE_ID, SolrOperator.IN, fileIds))
                            .setRows(fileIds.size())
                            .addField(FILE_ID).addField(ID)
                            .build()
                    ), null);
            fileIdsMap = files.stream()
                    .collect(Collectors.toMap(SolrFileEntity::getFileId, SolrFileEntity::getId, CollectionsUtils::keyCollisionResolver));
        }
        return fileIdsMap;
    }

    public SolrQueryResponse<SolrFileEntity> runQuery(SolrSpecification solrSpecification, PageRequest pageRequest, String selectedItemId) {
        return solrFileCatRepository.runQuery(solrSpecification, pageRequest, selectedItemId);
    }

    /**
     * Get content ID by file ID
     *
     * @param fileId file ID
     * @return content ID
     */
    public Long getContentId(long fileId) {
        return solrFileCatRepository.getContentId(fileId);
    }

    public ClaFile getByFileHashAll(long rootFolderId, String fileName) {
        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);

        String rfRealPath = rf.getRealPath();
        String fileNameNoPrefix = fileName;
        if (rfRealPath != null) {
            fileNameNoPrefix = fileName.startsWith(rfRealPath) ? fileName.substring(rfRealPath.length()) : fileName;
        }

        String fileHashed = ClaFile.getHashedFileName(fileNameNoPrefix);
        List<SolrFileEntity> fileEntities = solrFileCatRepository.getByFilterCriterias(Criteria.create(FILE_NAME_HASHED, SolrOperator.EQ, fileHashed));
        for (final SolrFileEntity claFile : fileEntities) {
            if (fileNameNoPrefix.equals(claFile.getFullName()) && claFile.getRootFolderId().equals(rootFolderId)) {
                return getFromEntity(claFile);
            }
        }
        return null;
    }

    public Long getNextAvailableFileId() {
        return solrFileCatRepository.getNextAvailableFileId();
    }

    public Long getNextAvailableContentId() {
        return contentMetadataRepository.getNextAvailableId();
    }

    public FileCatBuilder getBuilderForCreateUpdateRecord(ClaFile claFile, long rootFolderId, Long docFolderId,
                                                          List<String> parentsInfo, ClaFilePropertiesDto props, Long contentId) {

        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);

        FileCatBuilder fileCatBuilder = FileCatBuilder.create()
                .setClaFilePropertiesDto(props)
                .setRootFolderPath(rf.getRealPath())
                .setDocFolderId(docFolderId)
                .setRootFolderId(rootFolderId)
                .setMediaType(rf.getMediaType())
                .setParentFolderInfo(parentsInfo)
                .setRemoveOwnerQuotes(removeOwnerQuotes)
                .setParseX500Address(parseX500Addresses)
                .setExtractNonDomainX500CN(extractNonDomainX500CN);

        Long fileId;
        if (claFile == null) {
            fileId = solrFileCatRepository.getNextAvailableFileId();
            if (fileId != null && contentId != null) {
                fileCatBuilder.setId(FileCatCompositeId.of(contentId, fileId).toString());
                fileCatBuilder.setContentId(contentId);
                fileCatBuilder.setFileId(fileId);
                fileCatBuilder.setDeleted(false);
            }
            fileCatBuilder.setInitialScanDate(new Date());
        } else {
            fileId = claFile.getId();
            logger.debug("Found file {} in SOLR.", props.getFileName());

            if (claFile.getContentMetadataId() != null) {
                contentId = claFile.getContentMetadataId();
            }

            fileCatBuilder.setFileType(claFile.getType());
            fileCatBuilder.setId(FileCatCompositeId.of(contentId, claFile.getId()).toString());
            fileCatBuilder.setContentId(contentId);
            fileCatBuilder.setFileId(claFile.getId());
            fileCatBuilder.setDeleted(claFile.isDeleted());
        }

        List<String> containerList = claFile == null || claFile.getContainerIds() == null ? new ArrayList<>() : claFile.getContainerIds();
        if (props.getType() != null && props.getType().isContainer()) {
            if (FileNamingUtils.getContainerNumber(props.getFileName()) > 0) {
                populateFromParentContainerList(FileNamingUtils.getFilenamePath(props.getFileName()), rootFolderId, containerList);
            }
            addToContainerList(props.getFileName(), fileId, containerList, props.getType());
        } else if (FileNamingUtils.isContainerEntry(props.getFileName())) {
            ContainerData container = getContainer(rf.getId(), props.getFileName(), props.getMediaItemId());
            if (container != null) {
                fileCatBuilder.setPackageEntityId(container.getFileId());
                if (FileNamingUtils.getContainerNumber(props.getFileName()) > 1) {
                    populateFromParentContainerList(props.getFileName(), rootFolderId, containerList);
                } else {
                    addToContainerListIfNeeded(container.getType() + "." + container.getFileId(), containerList);
                }
            }
        }
        if (props instanceof PstEntryPropertiesDto) {
            PstEntryPropertiesDto pstProps = (PstEntryPropertiesDto) props;
            if (pstProps.getItemType().equals(ItemType.MESSAGE)) {
                addToContainerList(props.getFileName(), fileId, containerList, props.getType());
            } else if (pstProps.getItemType().equals(ItemType.ATTACHMENT)) {
                ContainerData containerData = getContainer(rootFolderId, pstProps.getEmailPath());
                if (containerData != null) {
                    addToContainerListIfNeeded(containerData.getType() + "." + containerData.getFileId(), containerList);
                }
            }
        }

        if (containerList != null && !containerList.isEmpty()) {
            fileCatBuilder.setContainerIds(containerList);
        }

        return fileCatBuilder;
    }

    private static void addToContainerListIfNeeded(String containerIdentifier, List<String> containerList) {
        if (!containerList.contains(containerIdentifier)) {
            containerList.add(containerIdentifier);
        }
    }

    private static void addToContainerListIfNeeded(List<String> newContainerIdentifiers, List<String> containerList) {
        for (String containerIdentifier : newContainerIdentifiers) {
            if (!containerList.contains(containerIdentifier)) {
                containerList.add(containerIdentifier);
            }
        }
    }

    private void populateFromParentContainerList(String fileName, Long rootFolderId, List<String> containerList) {
        String lowestContainerFileName = FileNamingUtils.getLowestLevelContainerPath(fileName);
        ContainerData containerData = getContainer(rootFolderId, lowestContainerFileName);
        if (containerData != null) {
            if (containerData.getContainerIds() != null) {
                addToContainerListIfNeeded(containerData.getContainerIds(), containerList);
            }
            addToContainerListIfNeeded(containerData.getType() + "." + containerData.getFileId(), containerList);
        }
    }

    private ContainerData getContainer(Long rfId, String fileName) {
        ContainerData containerData = containerIds.get(fileName);
        if (containerData != null) {
            return containerData;
        }

        ClaFile file = getByFileHashAll(rfId, fileName);
        if (file != null) {
            ContainerData data = new ContainerData(file.getId(), file.getType(), System.currentTimeMillis(), file.getContainerIds());
            containerIds.put(fileName, data);
            return data;
        }
        return null;
    }

    public Long getContainerId(Long rfId, String fileName, String mediaItemId) {
        ContainerData data = getContainer(rfId, fileName, mediaItemId);
        return (data != null ? data.getFileId() : -1L);
    }

    private ContainerData getContainer(Long rfId, String fileName, String mediaItemId) {
        String container = getContainerPath(fileName, mediaItemId);

        if (container == null) {
            return null;
        }

        ContainerData containerData = containerIds.get(container);
        if (containerData != null) {
            return containerData;
        }

        // populate
        ClaFile file = getByFileHashAll(rfId, container);
        if (file != null) {
            return addToContainerList(container, file.getId(), file.getContainerIds(), file.getType());
        }

        return null;
    }

    private static String getContainerPath(String filename, String mediaItemId) {
        if (FileNamingUtils.isPackageEntry(filename)) {
            return FileNamingUtils.getPackageRootPath(filename);
        } else if (filename.contains(FileTypeUtils.PST_FILE_EXT)) {
            try {
                PstPath pstPath = PstPath.fromString(mediaItemId);
                return pstPath.getPstFilePath().toString();
            } catch (Exception e) {
                logger.trace("getting pst path failed for {}", mediaItemId);
                return null;
            }
        }
        return null;
    }

    private ContainerData addToContainerList(String path, Long id, List<String> containerIds, FileType type) {
        if (type == null) {
            type = FileTypeUtils.getFileType(path);
        }
        ContainerData data = new ContainerData(id, type, System.currentTimeMillis(), containerIds);
        this.containerIds.put(path, data);
        return data;
    }

    public void updateFieldByQuery(String field, String value, SolrFieldOp operation, String query, List<String> filters, boolean commit) {
        solrFileCatRepository.updateFieldByQuery(field, value, operation, query, filters, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    @AutoAuthenticate
    public void deleteByRootFolderId(Long rootFolderId) {
        solrFileCatRepository.deleteByRootFolderId(rootFolderId);
    }

    public Integer updateFieldByQueryMultiFields(String[] fields, String[] values, String operation, String query, List<String> filters, SolrCommitType solrCommitType) {
        return solrFileCatRepository.updateFieldByQueryMultiFields(fields, values, operation, query, filters, solrCommitType);
    }

    public void mergeFilesToSingleContentId(ContentMetadata contentMetadata, List<Long> contentIds) {
        solrFileCatRepository.mergeFilesToSingleContentId(contentMetadata, contentIds);
    }

    public void processFileDeletion(long fileId) {
        solrFileCatRepository.updateFieldByQueryMultiFields(
                new String[]{DELETED.getSolrName(), DELETION_DATE.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{"true", SolrQueryGenerator.now(), String.valueOf(System.currentTimeMillis())},
                SET.getOpName(),
                SolrQueryGenerator.generateFilter(Criteria.create(FILE_ID, SolrOperator.EQ, fileId)),
                null, updateCatFilesCommitType);
    }

    @AutoAuthenticate
    public void processFilesDeletion(Collection<Long> fileIds) {
        if (fileIds == null || fileIds.isEmpty()) return;
        logger.trace("Setting following files as deleted: {}", fileIds);
        solrFileCatRepository.updateFieldByQueryMultiFields(
                new String[]{DELETED.getSolrName(), DELETION_DATE.getSolrName(), UPDATE_DATE.getSolrName()},
                new String[]{"true", SolrQueryGenerator.now(), String.valueOf(System.currentTimeMillis())},
                SET.getOpName(),
                SolrQueryGenerator.generateFilter(Criteria.create(FILE_ID, SolrOperator.IN, fileIds)),
                null, updateCatFilesCommitType);
    }

    public ClaFile useExistingOrCreateRecord(ClaFile claFileByHash,
                                             Long docFolderId,
                                             final String fileName,
                                             final FileType type,
                                             final ClaFilePropertiesDto claFilePropertiesDto,
                                             final long rootFolderId) {
        try {

            if (claFileByHash != null) {//In the future we main fine grain this for multiple processing
                if (claFileByHash.isDeleted()) {
                    return doUndeleteRecord(docFolderId, claFileByHash, claFilePropertiesDto);
                }
                logger.info("The file {} already exists. Skip its scan...", fileName);
                return claFileByHash;
            }
            ClaFile claFileResult = doCreateRecord(docFolderId, fileName, type, claFilePropertiesDto, rootFolderId);
            long fileId = claFileResult.getId();
            logger.trace("Created record for scanned file {} {} - got id {}", fileName, type.toString(), fileId);
            return claFileResult;
        } catch (final Exception e) {
            logger.error("Error while scanning file {} {}.", fileName, type.toString(), e);
        }
        return null;
    }


    private ClaFile doCreateRecord(Long folderId,
                                   final String fileName,
                                   final FileType type,
                                   final ClaFilePropertiesDto claFilePropertiesDto,
                                   final Long rootFolderId) {

        RootFolder rf = docStoreService.getRootFolderCached(rootFolderId);

        String repositoryUrl = rf.getRealPath();
        String fileNameToAdd = fileName;
        String mediaItemId = claFilePropertiesDto.getMediaItemId();
        String mediaItemIdStr = mediaItemId;
        if (!Strings.isNullOrEmpty(repositoryUrl)) {
            fileNameToAdd = fileName.startsWith(repositoryUrl) ? fileName.substring(repositoryUrl.length()) : fileName;
            mediaItemIdStr = mediaItemId.startsWith(repositoryUrl) ? mediaItemId.substring(repositoryUrl.length()) : mediaItemId;
        }

        ClaFile claFile = new ClaFile(fileNameToAdd, type);
        setBasicMetadata(claFile);
        claFile.setInitialScanDate(new Date());
        claFile.setDocFolderId(folderId);
        claFile.setRootFolderId(rootFolderId);
        claFile.setState(ClaFileState.SCANNED);
        fillClaFileAttributes(claFilePropertiesDto, claFile);

        claFile.setMediaEntityId(mediaItemIdStr);
        claFile.setLastMetadataChangeDate(System.currentTimeMillis());
        claFile.setId(0L);
        claFile.setDeleted(false);
        claFile.setContentMetadataId(getNextAvailableContentId());
        return claFile;
    }

    private ClaFile doUndeleteRecord(Long folderId, ClaFile claFile, final ClaFilePropertiesDto claFilePropertiesDto) {
        logger.trace("Undeleting file {} ... state={}", claFile.getFileName(), claFile.getState().name());
        undeleteRecord(FileCatCompositeId.of(claFile.getContentMetadataId(), claFile.getId()).toString(), folderId, claFilePropertiesDto, claFile.getContentMetadataId());

        claFile.setDeleted(false);
        claFile.setInitialScanDate(new Date());
        claFile.setState(ClaFileState.SCANNED);
        claFile.setDeletionDate(null);
        claFile.setDocFolderId(folderId);
        if (claFilePropertiesDto != null) {
            fillClaFileAttributes(claFilePropertiesDto, claFile);
        }
        claFile.setLastMetadataChangeDate(System.currentTimeMillis());
        logger.trace("Changed file id:{} to  {} , state: {}", claFile.getId(), claFile.getFileName(),
                claFile.getState().name());
        return claFile;
    }

    public void undeleteRecord(FileEntity file, ClaFilePropertiesDto properties) {
        if (file == null) {
            return;
        }

        ClaFile claFile = getFileObject(file.getFileId());
        if (claFile == null) {
            logger.error("Failed to undelete file-id {}. File not found in database", file.getFileId());
        } else {
            SolrFolderEntity solrDocFolder = docFolderService.getFolderByPath(claFile.getRootFolderId(), claFile.getFullPath());
            if (solrDocFolder == null) {
                logger.warn("Null solrDocFolder returned from getFolderByPath({}, {}). claFile={}", claFile.getRootFolderId(), claFile.getFullPath(), claFile.toString());
                throw new RuntimeException("Null solrDocFolder in undeleteRecord");     // TODO: consider better handling
            }
            undeleteRecord(
                    FileCatCompositeId.of(claFile.getContentMetadataId(), file.getFileId()).toString(), solrDocFolder.getId(), properties, claFile.getContentMetadataId());
            if (solrDocFolder.getDeleted()) {
                docFolderService.undeleteFolder(solrDocFolder);
                docFolderService.softCommitNoWaitFlush();
            }
        }
    }

    private void fillClaFileAttributes(ClaFilePropertiesDto claFilePropertiesDto, ClaFile claFile) {
        claFile.setFsLastModified(new Date(claFilePropertiesDto.getModTimeMilli()));
        claFile.setFsLastAccess(new Date(claFilePropertiesDto.getAccessTimeMilli()));
        claFile.setCreationDate(new Date(claFilePropertiesDto.getCreationTimeMilli()));
        claFile.setAclSignature(claFilePropertiesDto.getAclSignature());
        claFile.setOwner(CsvUtils.notNull(
                limitLength(claFilePropertiesDto.getOwnerName(), FileNamingUtils.DEFAULT_STR_FIELD_SIZE)));
        claFile.setAclInheritanceType(claFilePropertiesDto.getAclInheritanceType());
        if (removeOwnerQuotes) {
            claFile.setOwner(claFile.getOwner().replaceAll("\\\"", ""));
        }
    }

    private void setBasicMetadata(final ClaFile claFile) {
        claFile.setFullPathProperties(claFile.getFileName());
    }

    private void undeleteRecord(String id, Long docFolderId, ClaFilePropertiesDto props, Long contentId) {
        AtomicUpdateBuilder builder = AtomicUpdateBuilder.create()
                .setId(ID, id)
                .addModifier(DELETED, SET, false)
                .addModifier(INITIAL_SCAN_DATE, SET, new Date())
                .addModifier(PROCESSING_STATE, SET, ClaFileState.SCANNED.name())
                .addModifier(DELETION_DATE, SET, new Date());

        if (docFolderId != null) {
            builder.addModifier(FOLDER_ID, SET, docFolderId);
        }

        builder.addModifier(LAST_MODIFIED, SET, new Date(props.getModTimeMilli()))
                .addModifier(UPDATE_DATE, SET, System.currentTimeMillis())
                .addModifier(ACL_SIGNATURE, SET, props.getAclSignature())
                .addModifier(OWNER, SET, FileCatBuilder.convertOwner(props.getOwnerName(), removeOwnerQuotes))
                .addModifier(ACL_INHERITANCE_TYPE, SET, props.getAclInheritanceType() == null ? null : props.getAclInheritanceType().name());

        SolrInputDocument doc = builder.build();
        solrFileCatRepository.atomicUpdate(doc);

        if (contentId != null) {
            if (contentMetadataRepository.getById(contentId) != null) {
                contentMetadataRepository.saveAll(Collections.singletonList(AtomicUpdateBuilder.create()
                        .setId(ContentMetadataFieldType.ID, contentId)
                        .addModifier(ContentMetadataFieldType.FILE_COUNT, SolrOperator.INC, 1)
                        .addModifier(ContentMetadataFieldType.UPDATE_DATE, SolrOperator.SET, System.currentTimeMillis())
                        .build()));
                contentMetadataRepository.softCommitNoWaitFlush();
            }
        }
        softCommitNoWaitFlush();

        logger.debug("Undeleting file {}.", props.getFileName());
    }

    public void updateByQuery(List<String> filters, CatFileFieldType field, Object value, boolean commit) {
        solrFileCatRepository.updateByQuery(SolrQueryGenerator.ALL_QUERY, filters, field, value, commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    public void saveAndCommit(SolrFileEntity fileEntity) {
        solrFileCatRepository.save(fileEntity);
        softCommitNoWaitFlush();
    }

    public void softCommitNoWaitFlush() {
        solrFileCatRepository.softCommitNoWaitFlush();
    }

    public List<SolrFileEntity> findEmailAttachments(Long emailFileId) {
        Query query = Query.create().addFilterWithCriteria(
                Criteria.create(CatFileFieldType.CONTAINER_IDS, SolrOperator.EQ, FileType.MAIL + "." + emailFileId));
        return solrFileCatRepository.find(query.build());
    }

    public List<SolrFileEntity> findFilesByAnalysisGroupId(Pageable pageRequest, String groupId) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.EQ, groupId))
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        return solrFileCatRepository.find(query.build());
    }

    public Set<Long> findContentsByAnalysisGroupId(Pageable pageRequest, String groupId) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.ANALYSIS_GROUP_ID, SolrOperator.EQ, groupId))
                .addField(CatFileFieldType.CONTENT_ID)
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Set<Long> result = new HashSet<>();
        queryResponse.getResults().forEach(d -> {
            Long id = Long.parseLong((String) d.getFieldValue(CatFileFieldType.CONTENT_ID.getSolrName()));
            result.add(id);
        });
        return result;
    }

    @AutoAuthenticate
    public Set<Long> findContentsByFileIds(Pageable pageRequest, Collection<Long> fileIds) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.IN, fileIds))
                .addField(CatFileFieldType.CONTENT_ID)
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Set<Long> result = new HashSet<>();
        queryResponse.getResults().forEach(d -> {
            Object fieldValue = null;
            Object id = null;
            if (d != null) {
                id = d.getFieldValue(ID.getSolrName());
                fieldValue = d.getFieldValue(CONTENT_ID.getSolrName());
            }
            if (fieldValue != null) {
                result.add(Long.parseLong((String) fieldValue));
            } else {
                logger.debug("Missing content ID on record with ID {}", id == null ? "Unknown" : id);
            }
        });
        return result;
    }

    public Page<ClaFile> findAllFull(PageRequest pageRequest) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .addSort(CatFileFieldType.BASE_NAME, Sort.Direction.ASC)
                .addSort(CatFileFieldType.FULLPATH, Sort.Direction.ASC)
                .addSort(CatFileFieldType.FILE_ID, Sort.Direction.ASC)
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        List<SolrFileEntity> files = solrFileCatRepository.find(query.build());
        return new PageImpl<>(fromEntities(files));
    }

    public List<ClaFile> fromEntities(List<SolrFileEntity> entities) {
        List<ClaFile> result = new ArrayList<>();
        if (entities != null) {
            for (SolrFileEntity entity : entities) {
                result.add(getFromEntity(entity));
            }
        }
        return result;
    }

    public ClaFile getFromEntity(SolrFileEntity entity) {
        if (entity == null) {
            return null;
        }
        ClaFile file = new ClaFile();
        file.setId(entity.getFileId());
        file.setDocFolderId(entity.getFolderId());
        file.setUserGroupId(entity.getUserGroupId());
        file.setAnalysisGroupId(entity.getAnalysisGroupId());
        file.setFileName(entity.getFullName());
        file.setBaseName(entity.getBaseName());
        file.setType(FileType.valueOf(entity.getType()));
        file.setRootFolderId(entity.getRootFolderId());
        file.setState(ClaFileState.valueOf(entity.getState()));
        file.setFullPath(entity.getFullPath());
        file.setOwner(limitLength(entity.getOwner(), FileNamingUtils.DEFAULT_STR_FIELD_SIZE));
        file.setMime(entity.getMime());
        file.setExtension(entity.getExtension());
        file.setFileNameHashed(entity.getFileNameHashed());
        file.setLastMetadataChangeDate(entity.getLastMetadataChangeDate());
        file.setAclSignature(entity.getAclSignature());
        file.setFsLastAccess(entity.getAccessDate());
        file.setFsLastModified(entity.getModificationDate());
        file.setInitialScanDate(entity.getInitialScanDate());
        file.setIngestDate(entity.getIngestDate());
        file.setAnalysisDate(entity.getAnalysisDate());
        file.setDeletionDate(entity.getDeletionDate());
        file.setSizeOnMedia(entity.getSizeOnMedia());
        if (!Strings.isNullOrEmpty(entity.getAclInheritanceType())) {
            file.setAclInheritanceType(AclInheritanceType.valueOf(entity.getAclInheritanceType()));
        }
        file.setCreationDate(entity.getCreationDate());
        file.setDeleted(entity.isDeleted());
        file.setMediaEntityId(entity.getMediaEntityId());
        Long contentId = Strings.isNullOrEmpty(entity.getContentId()) ? null : Long.parseLong(entity.getContentId());
        file.setContentMetadataId(contentId);
        file.setUpdatedOnDB(entity.getUpdatedOnDB());
        file.setCreatedOnDB(entity.getCreatedOnDB());

        file.setSenderAddress(entity.getSenderAddress());
        file.setSenderName(entity.getSenderName());
        file.setSenderDomain(entity.getSenderDomain());
        file.setSenderFullAddress(entity.getSenderFullAddress());
        file.setRecipientsAddresses(entity.getRecipientsAddresses());
        file.setRecipientsNames(entity.getRecipientsNames());
        file.setRecipientsDomains(entity.getRecipientsDomains());
        file.setRecipientsFullAddresses(entity.getRecipientsFullAddresses());
        file.setSentDate(entity.getSentDate());

        if (entity.getItemType() != null) {
            file.setItemType(ItemType.valueOf(entity.getItemType()));
        }
        file.setNumOfAttachments(entity.getNumOfAttachments());
        file.setContainerIds(entity.getContainerIds());
        file.setMailboxUpn(entity.getMailboxUpn());
        file.setItemSubject(entity.getItemSubject());
        file.setMediaType(MediaType.valueOf(entity.getMediaType()));
        file.setAssociations(entity.getAssociations());
        file.setSearchPatternsCounting(resolveSearchPatterns(entity));
        file.setSortName(entity.getSortName());

        file.setDaLabels(entity.getDaLabels());
        file.setExternalMetadata(entity.getExternalMetadata());
        file.setActionsExpected(entity.getActionsExpected());
        file.setRawMetadata(entity.getRawMetadata());
        file.setDaMetadata(entity.getDaMetadata());
        file.setDaMetadataType(entity.getDaMetadataType());
        file.setGeneralMetadata(entity.getGeneralMetadata());
        file.setActionConfirmation(entity.getActionConfirmation());
        file.setLabelProcessDate(entity.getLabelProcessDate());
        return file;
    }

    @Nullable
    private Set<SearchPatternCountDto> resolveSearchPatterns(SolrFileEntity entity) {

        // remove all old patterns from entity so we dont try to parse them
        List<String> extractedPatternsIds;
        if (entity.getExtractedPatternsIds() != null) {
            extractedPatternsIds = new ArrayList<>();
            for (String patternStr : entity.getExtractedPatternsIds()) {
                if (!patternStr.contains("com.cla.filecluster.domain.entity.pv.SearchPatternCount")) {
                    extractedPatternsIds.add(patternStr);
                }
            }
            entity.setExtractedPatternsIds(extractedPatternsIds);
        }

        // try with getting SearchPatternsCounting from the entity. if missing
        // parse the json from the ExtractedPatternsIds field.
        try {
            return Optional.ofNullable(entity.getSearchPatternsCounting())
                    .map(FileCatUtils::convertSearchPatternCounting)
                    .orElse(Optional.ofNullable(entity.getExtractedPatternsIds())
                            .map(list -> list.stream()
                                    .map(FileCatUtils::convertFromJsonSearchPatterns)
                                    .filter(Objects::nonNull)
                                    .flatMap(Set::stream)
                                    .collect(Collectors.toSet())).orElse(null));
        } catch (Exception e) {
            logger.warn("error extracting patterns from entity: {} " +
                            "SearchPatternsCounting: {} ExtractedPatternsIds: {}",
                    entity.getId(), entity.getSearchPatternsCounting(), entity.getExtractedPatternsIds(), e);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public Page<SolrFileEntity> findAllForCatFile(PageRequest pageRequest) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        if (!includeOthers) {
            query.addFilterWithCriteria(Criteria.create(CatFileFieldType.TYPE, SolrOperator.LT, 99));
        }
        List<SolrFileEntity> files = solrFileCatRepository.find(query.build());
        return new PageImpl<>(files);
    }

    public Long getFileNumByStatus(ClaFileState state) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, state.name()))
                .setRows(0)
                .setFacetMinCount(1)
                .addFacetField(CatFileFieldType.PROCESSING_STATE);

        long result = 0L;
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
            FacetField field = queryResponse.getFacetFields().get(0);
            List<FacetField.Count> values = field.getValues();
            for (FacetField.Count c : values) {
                if (c.getName().equalsIgnoreCase(state.name())) {
                    result = c.getCount();
                    break;
                }
            }
        }
        return result;
    }

    public SolrFileEntity getFileEntity(long fileId) {
        return solrFileCatRepository.getById(fileId);
    }

    public ClaFile getFileObject(long fileId) {
        SolrFileEntity entity = getFileEntity(fileId);
        return getFromEntity(entity);
    }

    public Page<Object[]> findAllReportListPageOrdered(PageRequest pageRequest) {
        Page<ClaFile> files = findAllFull(pageRequest);
        List<Object[]> result = new ArrayList<>();
        if (files != null && files.getContent() != null) {
            for (ClaFile file : files) {
                Object[] array = new Object[11];
                array[0] = file.getId();
                array[1] = file.getType();
                array[2] = file.getMime();
                array[3] = file.getFullPath();
                array[4] = file.getBaseName();
                array[5] = file.getSizeOnMedia(); // TODO yael falcon now returns SizeOnMedia instead of fsFileSize
                array[6] = file.getFsLastModified();
                array[7] = file.getCreationDate();
                array[8] = file.getFsLastAccess();
                array[9] = file.getAnalysisGroupId();
                array[10] = file.getUpdatedOnDB();
                result.add(array);
            }
        }
        return new PageImpl<>(result);
    }

    public Long getClaFileContentId(long fileId) {
        SolrFileEntity entity = getFileEntity(fileId);
        String contentId = (entity == null ? null : entity.getContentId());
        return Strings.isNullOrEmpty(contentId) ? null : Long.parseLong(contentId);
    }

    private Page<SolrFileEntity> findFilesByUserGroupId(Pageable pageRequest, String groupId) {
        Query query = Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, groupId))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .setRows(pageRequest.getPageSize())
                .setStart((int) pageRequest.getOffset());
        List<SolrFileEntity> entities = solrFileCatRepository.find(query.build());

        Long total = getTotalCount(query, CatFileFieldType.USER_GROUP_ID, groupId);
        return new PageImpl<>(entities, pageRequest, total);
    }

    public Long getTotalCount(Query query, CatFileFieldType facetField, String facetValueToCount) {
        long total = 0L;
        query.setRows(0);
        query.addFacetField(facetField);
        try {
            QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
            if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
                FacetField field = queryResponse.getFacetFields().get(0);
                List<FacetField.Count> values = field.getValues();
                for (FacetField.Count c : values) {
                    if (facetValueToCount == null) {
                        total += c.getCount();
                    } else if (c.getName().equals(facetValueToCount)) {
                        return c.getCount();
                    }
                }
            }
            return total;
        } catch (Exception e) {
            logger.error("Failed to get file count from Solr", e);
            throw new RuntimeException("Failed to get file count from Solr. query:" + query, e);
        }
    }

    public Page<ClaFile> findByUserGroupIdFull(Pageable pageRequest, String groupId) {
        Page<SolrFileEntity> entities = findFilesByUserGroupId(pageRequest, groupId);

        if (entities == null || entities.getContent().isEmpty()) {
            return new PageImpl<>(Collections.emptyList(), pageRequest, 0);
        }
        List<ClaFile> claFiles = fromEntities(entities.getContent());
        return new PageImpl<>(claFiles, pageRequest, entities.getTotalElements());
    }

    public List<SolrFileEntity> listFilesByFolderRecursive(DocFolder docFolder, Pageable pageRequest, FilterDescriptor baseFilter
            , Integer maxDepth) {
        ClaFileByFolderRecursiveSpecification filterSpecification = new ClaFileByFolderRecursiveSpecification(baseFilter,
                docFolder.getId(), docFolder.getPath(), docFolder.getRootFolderId());
        if (maxDepth != null) {
            List<DocFolder> folders = docFolderService.listFolders(docFolder, maxDepth);
            List<Long> folderIds = folders.stream().map(DocFolder::getId).collect(Collectors.toList());
            filterSpecification.setFolderIds(folderIds);
        }
        Query q = filterSpecification.toQuery();
        q.setRows(pageRequest.getPageSize());
        q.setStart((int) pageRequest.getOffset());
        q.addFilterQuery(Query.create().addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false")));
        return solrFileCatRepository.find(q.build());
    }

    private void updateFilesForMovedFolder(Long folderId, List<String> parentInfo, String folderPath) {
        int start = 0;
        int pageSize = 5000;
        int numFound;
        int total = 0;

        do {
            logger.trace("moving files for moved folder {} start {}", folderId, start);
            Query query = Query.create()
                    .addFilterWithCriteria(Criteria.create(CatFileFieldType.FOLDER_ID, SolrOperator.EQ, folderId))
                    .addField(CatFileFieldType.BASE_NAME)
                    .addField(CatFileFieldType.ID)
                    .addSort(CatFileFieldType.ID, Sort.Direction.ASC)
                    .setRows(pageSize)
                    .setStart(start);
            List<SolrFileEntity> entities = find(query.build());
            numFound = entities.size();
            total += numFound;
            List<SolrInputDocument> entitiesToUpd = new ArrayList<>();

            entities.forEach(file -> {
                String fullName = folderPath + file.getBaseName();
                AtomicUpdateBuilder builder = AtomicUpdateBuilder.create();
                builder.setId(ID, file.getId());
                builder.addModifier(CatFileFieldType.FULL_NAME, SET, fullName);
                builder.addModifier(CatFileFieldType.FULLPATH, SET, folderPath);
                builder.addModifier(CatFileFieldType.FULL_NAME_SEARCH, SET, fullName.toLowerCase());
                builder.addModifier(CatFileFieldType.FOLDER_NAME, SET, FileNamingUtils.getParentNameOnly(fullName.toLowerCase()));
                builder.addModifier(CatFileFieldType.FOLDER_IDS, SET, parentInfo);
                builder.addModifier(CatFileFieldType.FILE_NAME_HASHED, SET, ClaFile.getHashedFileName(fullName));
                builder.addModifier(CatFileFieldType.UPDATE_DATE, SET, System.currentTimeMillis());
                entitiesToUpd.add(builder.build());
            });

            start += pageSize;
            if (!entitiesToUpd.isEmpty()) {
                logger.trace("moving files for moved folder {} num files fixing in cycle {}", folderId, entitiesToUpd.size());
                solrFileCatRepository.saveAll(entitiesToUpd);
            }
        } while (numFound == pageSize);

        if (total > 0) {
            solrFileCatRepository.softCommitNoWaitFlush();
        }
    }

    @AutoAuthenticate
    public int countFilesUnderRootFolder(Long rootFolderDtoId) {
        Query query = Query.create().addFilterWithCriteria(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderDtoId);
        query.addFacetField(CatFileFieldType.ROOT_FOLDER_ID);
        query.addFilterWithCriteria(CatFileFieldType.DELETED, SolrOperator.EQ, "false");
        query.setFacetMinCount(1);
        query.setRows(0);
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        FacetField field = queryResponse.getFacetFields().get(0);
        List<FacetField.Count> values = field.getValues();
        return values == null || values.isEmpty() ? 0 : new Long(values.get(0).getCount()).intValue();
    }

    @AutoAuthenticate
    public List<Triple<Long, Long, String>> getFilesUnderRootFolder(Long rootFolderDtoId, int from, int count) {
        List<Triple<Long, Long, String>> result = new ArrayList<>();
        Query query = Query.create().addFilterWithCriteria(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderDtoId);
        query.addFilterWithCriteria(CatFileFieldType.DELETED, SolrOperator.EQ, "false");
        query.addField(CatFileFieldType.FILE_ID).addField(CatFileFieldType.CONTENT_ID).addField(CatFileFieldType.USER_GROUP_ID);
        query.addSort(CatFileFieldType.FILE_ID, Sort.Direction.ASC);
        query.setStart(from).setRows(count);
        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        queryResponse.getResults().forEach(res -> {
            Long file = (Long) res.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());
            String contentStr = (String) res.getFieldValue(CatFileFieldType.CONTENT_ID.getSolrName());
            Long content = Strings.isNullOrEmpty(contentStr) ? 0 : Long.parseLong(contentStr);
            String group = (String) res.getFieldValue(CatFileFieldType.USER_GROUP_ID.getSolrName());
            result.add(Triple.of(file, content, group));
        });
        return result;
    }

    @AutoAuthenticate
    public List<SolrFileEntity> findContainerFilesWithNoTopId(Long rootFolderDtoId) {
        return solrFileCatRepository.find(
                Query.create()
                        .addFilterWithCriteria(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderDtoId)
                        .addFilterWithCriteria(CatFileFieldType.DELETED, SolrOperator.EQ, "false")
                        .addFilterWithCriteria(CatFileFieldType.PACKAGE_TOP_FILE_ID, SolrOperator.EQ, -1L)
                        .addField(CatFileFieldType.ID)
                        .addField(CatFileFieldType.FULL_NAME)
                        .build());
    }

    public List<SolrFileEntity> findAllForDocFolders(Collection<Long> docFolderIds) {
        if (docFolderIds == null || docFolderIds.isEmpty()) {
            return new ArrayList<>();
        }
        return solrFileCatRepository.find(Query.create().addFilterWithCriteria(CatFileFieldType.FOLDER_ID, SolrOperator.IN, docFolderIds).build());
    }

    public List<SolrFileEntity> findByFileIds(Collection<Long> fileIds) {
        return solrFileCatRepository.findByFileIds(fileIds);
    }

    public List<SolrFileEntity> findByFileIds(Collection<Long> fileIds, List<CatFileFieldType> fields) {
        return solrFileCatRepository.findByFileIds(fileIds, fields);
    }

    public List<ClaFile> findByFilesIds(Collection<Long> fileIds) {
        List<SolrFileEntity> entities = findByFileIds(fileIds);
        return fromEntities(entities);
    }

    public boolean stopAfterScanning(final ClaFile claFile) {
        return Stream.of(scanFullProcessExcludeExt).anyMatch(s -> claFile.getFileName().toLowerCase().endsWith(s))
                || !(scanFullProcessIncludeExt.length == 0
                || Stream.of(scanFullProcessIncludeExt).anyMatch(s -> claFile.getFileName().toLowerCase().endsWith(s)));
    }

    public SolrInputDocument updateCategoryFile(Long taskId, SolrFileEntity file, ContentMetadata contentMetadata,
                                                FileGroup analysisGroup, FileGroup userGroup,
                                                Collection<AclItemDto> aclItemsDtos,
                                                ProcessingErrorType processingErrorType,
                                                IngestBatchStore ingestBatchStore) {

        KpiRecording kpiRecording = performanceKpiRecorder.startRecording("updateCategoryFile",
                "DocFolderUtils.decodeFolderParentsInfo(docFolderService.getById(file.getDocFolderId()))", 869);
        Long docFolderId = file.getFolderId();
        DocFolder docFolderById = ingestBatchStore.getDocFolderById(docFolderId);
        List<String> parentFoldersInfo = docFolderById.getParentsInfo();
        kpiRecording.end();

        try {
            Collection<AclItemDto> aclDtos = aclItemsDtos != null ? Sets.newHashSet(aclItemsDtos) : Sets.newHashSet();

            /*if (AclInheritanceType.FOLDER.equals(file.getAclInheritanceType())) {
                //TODO: we don't actually add inherited ACLs, see bug DAMAIN-2179
                logger.debug("Add " + aclDtos.size() + " ACL items from Folder " + docFolderId);
            }*/

            RootFolder rf = docStoreService.getRootFolderCached(file.getRootFolderId());

            if (file.getSortName() == null) {
                logger.warn("Cannot get SortName for file {}, check why it is not in Solr.", file.getId());
            }

            final SolrInputDocument doc = solrFileCatRepository.toSolrDocument(rf, file, contentMetadata);
            SolrFileCatRepository.setField(doc, CatFileFieldType.UPDATE_DATE, SolrFileCatRepository.atomicSetValue(System.currentTimeMillis()));

            if (ClaFileState.INGESTED.equals(file.getState())) {
                SolrFileCatRepository.setField(doc, CatFileFieldType.INGEST_DATE, System.currentTimeMillis());
            }

            if (processingErrorType != null) {
                FileTag fileTag = processingErrorTypeTagTranslator.getFileTag(processingErrorType);
                if (fileTag != null) {
                    solrFileCatRepository.updateErrorType(doc, fileTag);
                } else {
                    logger.error("Did not find matching tag for processing type {}, not saving error in solr", processingErrorType);
                }
            }
            kpiRecording = performanceKpiRecorder.startRecording("updateCategoryFile",
                    "solrFileCatRepository.updateDocDetails", 892);
            fileCatAclService.updateAcls(doc, aclDtos);
            solrFileCatRepository.setParentFoldersInfo(doc, parentFoldersInfo);
            solrFileCatRepository.addGroupDetails(doc, analysisGroup, userGroup);
            associationsService.associateContentRelatedItems(doc, analysisGroup, userGroup);
            kpiRecording.end();

            kpiRecording = performanceKpiRecorder.startRecording("updateCategoryFile",
                    "associationsService.addTagDataToDocument", 899);
            associationsService.addTagDataToDocument(doc, file, false, ingestBatchStore);
            kpiRecording.end();

            kpiRecording = performanceKpiRecorder.startRecording("updateCategoryFile",
                    "regulationsService.addRegulationsDataToDocument", 1219);

            Set<SearchPatternCountDto> spcDtos = FileCatUtils.convertSearchPatternCounting(file.getSearchPatternsCounting());
            regulationsService.addRegulationsDataToDocument(doc, spcDtos, false);
            kpiRecording.end();


            kpiRecording = performanceKpiRecorder.startRecording("updateCategoryFile",
                    "ingestBatchResultsProcessor.collectFileCatDocument(taskId,doc)", 906);
            logger.debug("updating file {} data", file.getId());
            ingestBatchResultsProcessor.collectFileCatDocument(taskId, doc);
            kpiRecording.end();

            if (commitSolrDuringIngest) {
                solrFileCatRepository.softCommitNoWaitFlush();
            }
            return doc;
        } catch (Exception e) {
            logger.error("Failed to create category file in Solr", e);
            throw new RuntimeException("Failed to create category file in Solr. File-ID:" + file.getId(), e);
        }
    }

    public void updateIngestErrorDataFile(SolrFileEntity file, Collection<AclItemDto> aclItemsDtos,
                                          SolrFileEntity fileEntity, IngestBatchStore ingestBatchStore) {
        try {
            if (aclItemsDtos != null) {
                Collection<AclItemDto> aclDtos = Sets.newHashSet(aclItemsDtos);
                fileCatAclService.updateAcls(fileEntity, aclDtos);
            }
            SolrInputDocument doc = new SolrInputDocument();

            associationsService.addTagDataToDocument(doc, file, false, ingestBatchStore);
            Optional<Set<String>> val = getValueFromDoc(doc, CatFileFieldType.TAGS);
            val.ifPresent(v -> fileEntity.setTags(Lists.newArrayList(v)));

            val = getValueFromDoc(doc, CatFileFieldType.TAGS_2);
            val.ifPresent(v -> fileEntity.setTags2(Lists.newArrayList(v)));

            val = getValueFromDoc(doc, CatFileFieldType.SCOPED_TAGS);
            val.ifPresent(v -> fileEntity.setScopedTags(Lists.newArrayList(v)));

            val = getValueFromDoc(doc, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION);
            val.ifPresent(v -> fileEntity.setDataRoles(Lists.newArrayList(v)));

            val = getValueFromDoc(doc, CatFileFieldType.BIZLIST_ITEM_ASSOCIATION);
            val.ifPresent(v -> fileEntity.setBizListItems(Lists.newArrayList(v)));

            val = getValueFromDoc(doc, CatFileFieldType.DOC_TYPES);
            val.ifPresent(v -> fileEntity.setDocTypes(Lists.newArrayList(v)));

            String dept = doc.containsKey(CatFileFieldType.DEPARTMENT_ID.getSolrName()) ?
                    (String) doc.getField(CatFileFieldType.DEPARTMENT_ID.getSolrName()).getValue() : null;
            if (dept != null) fileEntity.setDepartment(dept);

            val = getValueFromDoc(doc, CatFileFieldType.ACTIVE_ASSOCIATIONS);
            val.ifPresent(v -> fileEntity.setActiveAssociations(Lists.newArrayList(v)));

        } catch (Exception e) {
            logger.error("failed update of document file id {}", file.getId(), e);
        }
    }

    @SuppressWarnings("unchecked")
    private Optional<Set<String>> getValueFromDoc(SolrInputDocument doc, CatFileFieldType field) {
        return doc.containsKey(field.getSolrName()) ?
                Optional.of((Set<String>) doc.getField(field.getSolrName()).getValue()) : Optional.empty();
    }

    @AutoAuthenticate
    public void updateGroupNameOnFiles(String groupId, String newGroupName) {
        try {
            ClaFileByUserGroupSpecification filterSpecification = new ClaFileByUserGroupSpecification(null, groupId);
            List<SolrFileEntity> entities = solrFileCatRepository.find(filterSpecification.toQuery().build());
            logger.debug("Renaming group for {} files in solr", entities.size());
            Iterable<List<SolrFileEntity>> partition = Iterables.partition(entities, 1000);
            for (List<SolrFileEntity> files : partition) {
                executionManagerService.lockFileCatOrThrow("updateGroupNameOnFiles");
                try {
                    solrFileCatRepository.updateGroupName(files, newGroupName);
                } finally {
                    executionManagerService.releaseFileCatLock();
                }
                logger.debug("Finished updating group for {} files", files.size());
            }
            solrFileCatRepository.commit();
        } catch (RuntimeException e) {
            logger.error("Failed to update group name for group id {} to {}", groupId, newGroupName, e);
            throw e;
        }
    }

    public void clearFieldInCatFile(CatFileFieldType catFileFieldType) {
        logger.info("clear FieldInCatFile {}", catFileFieldType);
        //TODO - add parallelism on pages
        String cursorMark;
        String nextCursorMark = "*";
        do {
            cursorMark = nextCursorMark;
            Criteria filterQuery = Criteria.create(catFileFieldType, SolrOperator.PRESENT, null);
            DeepPageImpl<String> compositeIdsPage = solrFileCatRepository
                    .listCompositeIdsWithFilter(filterQuery, createCategoryFilesPageSize, cursorMark);
            nextCursorMark = compositeIdsPage.getCursorMark();

            logger.debug("Clear fileCat field {}: {}/{}", catFileFieldType.getSolrName(),
                    compositeIdsPage.getCursorMark(), compositeIdsPage.getTotalCount());
            clearFieldInCatFile(catFileFieldType, compositeIdsPage);
            solrFileCatRepository.commit();
            if (compositeIdsPage.getContent().size() == 0) {
                logger.warn("Got 0 results - stopping");
                nextCursorMark = cursorMark;
            }

        }
        while (!nextCursorMark.equals(cursorMark));

        logger.info("cleared FieldInCatFile {}", catFileFieldType);

    }

    private void clearFieldInCatFile(CatFileFieldType catFileFieldType, DeepPageImpl<String> compositeIdsPage) {
        List<SolrInputDocument> catFileDocs = new ArrayList<>();
        for (String compositeId : compositeIdsPage.getContent()) {
            SolrInputDocument fileCatDocument = solrFileCatRepository.createClearFieldDocument(compositeId, catFileFieldType);
            catFileDocs.add(fileCatDocument);
        }
        solrFileCatRepository.createCategoryFilesFromDocuments(catFileDocs);
    }

    public void removeRegexFromFieldInCatFile(CatFileFieldType catFileFieldType, String regex, String filter) {
        logger.info("remove regex {} from FieldInCatFile {}", regex, catFileFieldType);
        //TODO - add parallelism on pages
        String cursorMark;
        String nextCursorMark = "*";
        int totalItems = 0;
        do {
            cursorMark = nextCursorMark;
            Criteria filterQuery = Criteria.create(catFileFieldType, SolrOperator.EQ, filter);
            DeepPageImpl<SolrDocument> claFileIdsPage = solrFileCatRepository.listFilesFieldWithFilter(
                    filterQuery, catFileFieldType, createCategoryFilesPageSize, cursorMark);

            nextCursorMark = claFileIdsPage.getCursorMark();

            logger.debug("remove from field [{}]. At cursor [{}] of {}. Next cursor = {}", catFileFieldType.getSolrName(), cursorMark, claFileIdsPage.getTotalCount(), nextCursorMark);
            totalItems += claFileIdsPage.getContent().size();
            removeRegexFromFieldInCatFile(catFileFieldType, regex, claFileIdsPage);
            solrFileCatRepository.commit();
            if (claFileIdsPage.getContent().size() == 0) {
                logger.debug("Got 0 results - exiting loop");
                nextCursorMark = cursorMark;
            }
        }
        while (!nextCursorMark.equals(cursorMark));

        logger.info("finished - removed regex {} from FieldInCatFile {} on {} files", regex, catFileFieldType, totalItems);

    }

    public void removeFromFieldInCatFile(CatFileFieldType catFileFieldType, String value) {
        logger.info("remove value {} from FieldInCatFile {}", value, catFileFieldType);
        //TODO - add parallelism on pages
        String cursorMark;
        String nextCursorMark = "*";
        do {
            cursorMark = nextCursorMark;
            Criteria filterQuery = Criteria.create(catFileFieldType, SolrOperator.PRESENT, null);
            DeepPageImpl<String> compositeIdsPage = solrFileCatRepository.listCompositeIdsWithFilter(filterQuery, createCategoryFilesPageSize, cursorMark);
            nextCursorMark = compositeIdsPage.getCursorMark();

            logger.debug("remove from field [{}]. At cursor [{}] of {}. Next cursor = {}", catFileFieldType.getSolrName(), cursorMark, compositeIdsPage.getTotalCount(), nextCursorMark);
            removeFromFieldInCatFile(catFileFieldType, value, compositeIdsPage);
            solrFileCatRepository.commit();
            if (compositeIdsPage.getContent().size() == 0) {
                logger.warn("Got 0 results - stopping");
                nextCursorMark = cursorMark;
            }
        }
        while (!nextCursorMark.equals(cursorMark));

        logger.info("finished - removed value {} from FieldInCatFile {}", value, catFileFieldType);

    }

    private void removeRegexFromFieldInCatFile(CatFileFieldType catFileFieldType, String regex, DeepPageImpl<SolrDocument> claFilesPage) {
        Pattern pattern = Pattern.compile(regex);
        logger.debug("removeRegexFromFieldInCatFile for {} files", claFilesPage.getContent().size());
        List<SolrInputDocument> catFileDocs = new ArrayList<>();
        int removedCount = 0;
        for (SolrDocument document : claFilesPage.getContent()) {
            @SuppressWarnings("unchecked")
            List<String> values = (List<String>) document.get(catFileFieldType.getSolrName());
            if (values != null) {
                List<String> removeValues = values.stream().filter(f -> pattern.matcher(f).matches()).collect(Collectors.toList());
                if (removeValues.size() > 0) {
                    Map<String, Object> modifier = Maps.newHashMap();
                    modifier.put("remove", removeValues);
                    String compositeId = (String) document.get("id");
                    SolrInputDocument fileCatDocument = solrFileCatRepository.createModifyFieldDocument(compositeId, catFileFieldType, modifier);
                    catFileDocs.add(fileCatDocument);
                    removedCount += removeValues.size();
                }
            }
        }
        logger.debug("Remove {} values from claFile field {}", removedCount, catFileFieldType);
        solrFileCatRepository.createCategoryFilesFromDocuments(catFileDocs);
    }

    private void removeFromFieldInCatFile(CatFileFieldType catFileFieldType, String string, DeepPageImpl<String> compositeIdsPage) {
        logger.debug("removeFromFieldInCatFile for {} files", compositeIdsPage.getContent().size());
        List<SolrInputDocument> catFileDocs = new ArrayList<>();
        for (String compositeId : compositeIdsPage.getContent()) {
            Map<String, Object> modifier = Maps.newHashMap();
            modifier.put("remove", string);
            SolrInputDocument fileCatDocument = solrFileCatRepository.createModifyFieldDocument(compositeId, catFileFieldType, modifier);
            catFileDocs.add(fileCatDocument);
        }
        solrFileCatRepository.createCategoryFilesFromDocuments(catFileDocs);
    }

    public List<Long> findFileIdsByContentId(long contentId) {
        List<SolrFileEntity> files = findFilesByContentId(contentId);
        return files == null || files.isEmpty() ? new ArrayList<>() : files.stream().map(SolrFileEntity::getFileId).collect(Collectors.toList());
    }

    public List<SolrFileEntity> findFilesByContentId(long contentId) {
        Query query = Query.create().addFilterWithCriteria(CatFileFieldType.CONTENT_ID, SolrOperator.EQ, contentId);
        List<SolrFileEntity> files = solrFileCatRepository.find(query.build());
        return files == null ? new ArrayList<>() : files;
    }

    public void deleteByFileIdContentId(Long fileId, Long contentId) {
        solrFileCatRepository.deleteByFileIdContentId(fileId, contentId);
    }

    @AutoAuthenticate
    public void deleteByCompositeIds(List<String> compositeIds) {
        solrFileCatRepository.deleteByCompositeIds(compositeIds);
    }

    public void markAsDeleted(Long fileId) {
        solrFileCatRepository.markAsDeleted(fileId, false);
    }

    public SolrQueryResponse<SolrFileEntity> runQuery(SolrSpecification solrSpecification, PageRequest pageRequest) {
        return solrFileCatRepository.runQuery(solrSpecification, pageRequest);
    }

    public SolrFacetQueryResponse runFacetQuery(SolrSpecification solrFacetSpecification, PageRequest pageRequest) {
        return solrFileCatRepository.runFacetQuery(solrFacetSpecification, pageRequest);
    }

    public SolrFacetQueryResponse runFacetQuery(DataSourceRequest dataSourceRequest, SolrSpecification solrFacetSpecification) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        String selectedItemId = dataSourceRequest.getSelectedItemId();
        solrFacetSpecification.setAdditionalFilter(CatFileFieldType.DELETED.filter("false"));
        return runFacetQuery(solrFacetSpecification, FacetField.Count::getName, pageRequest, selectedItemId);
    }

    public SolrFacetQueryResponse runFacetQuery(SolrSpecification solrFacetSpecification, Function<FacetField.Count, String> idExtractor, PageRequest pageRequest, String selectedItemId) {
        return solrFileCatRepository.runFacetQuery(solrFacetSpecification, idExtractor, pageRequest, selectedItemId);
    }

    public long getFacetNumber(SolrSpecification solrSpecification, CatFileFieldType field) {
        return solrFileCatRepository.getFacetNumber(solrSpecification, field.getSolrName());
    }

    public Integer updateFieldBySolrSpecification(SolrSpecification solrSpecification, CatFileFieldType field, SolrFieldOp operation, String value, boolean commit) {
        return solrFileCatRepository.updateFieldBySolrSpecification(solrSpecification, field,
                 operation,  value,
                commit ? SolrCommitType.SOFT_NOWAIT : SolrCommitType.NONE);
    }

    public SolrDocument findCategoryFile(long fileId) {
        return solrFileCatRepository.findCategoryFile(fileId);
    }

    public void updateAcls(SolrInputDocument doc, Collection<AclItemDto> aclItems) {
        fileCatAclService.updateAcls(doc, aclItems);
    }

    public void updateAcls(FileCatBuilder builder, Collection<AclItemDto> aclItems) {
        fileCatAclService.updateAcls(builder, aclItems);
    }

    public void atomicUpdate(SolrInputDocument doc) {
        solrFileCatRepository.atomicUpdate(doc);
    }

    public QueryResponse runQuery(SolrQuery query) {
        return solrFileCatRepository.runQuery(query);
    }

    public Page<AggregationCountItemDTO<RootFolderDto>> listGroupRootFolders(String groupId, PageRequest pageRequest) {
        Query query = Query.create()
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.USER_GROUP_ID, SolrOperator.EQ, groupId))
                .addFacetField(CatFileFieldType.ROOT_FOLDER_ID)
                .setFacetMinCount(1)
                .setRows(0);
        Map<Long, Long> result = new TreeMap<>();
        QueryResponse queryResponse = runQuery(query.build());
        if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
            FacetField field = queryResponse.getFacetFields().get(0);
            List<FacetField.Count> values = field.getValues();
            for (FacetField.Count c : values) {
                String folderId = c.getName();
                long count = c.getCount();
                result.put(Long.parseLong(folderId), count);
            }
        }

        //noinspection unchecked
        result = CollectionsUtils.getSubMap((TreeMap) result, (int) pageRequest.getOffset(), pageRequest.getPageSize());

        List<AggregationCountItemDTO<RootFolderDto>> content = new ArrayList<>();
        List<RootFolder> folders = docStoreService.getRootFolders(new ArrayList<>(result.keySet()));
        Map<Long, RootFolder> byId = folders.stream().collect(Collectors.toMap(RootFolder::getId, r -> r));

        for (Long rootFolderId : result.keySet()) {
            RootFolder rootFolder = byId.get(rootFolderId);
            RootFolderDto rootFolderDto = DocStoreService.convertRootFolderToDto(rootFolder, rootFolder.getDocStore().getId(), false);
            Long count = result.get(rootFolderId);
            AggregationCountItemDTO<RootFolderDto> aggItem = new AggregationCountItemDTO<>(rootFolderDto, count);
            content.add(aggItem);
        }

        return new PageImpl<>(content);
    }

    public Page<SolrDocument> listFilesWithFilter(PageRequest pageRequest, Criteria filterQuery) {
        return solrFileCatRepository.listFilesWithFilter(pageRequest, filterQuery);
    }

    public void updateUserGroupId(Long fileId, Long contentId, String newGroupId, boolean commit) {
        solrFileCatRepository.updateUserGroupId(fileId, contentId, newGroupId, commit);
    }

    public SolrInputDocument createUpdateGroupDocument(SolrFileEntity file, FileGroup analysisGroup, FileGroup userGroup, ContentMetadata contentMetadata) {
        return solrFileCatRepository.createUpdateGroupDocument(file, analysisGroup, userGroup, contentMetadata);
    }

    public void createCategoryFilesFromDocuments(Collection<SolrInputDocument> documents) {
        solrFileCatRepository.createCategoryFilesFromDocuments(documents);
    }

    Page<SolrDocument> listCategoryFiles(@NotNull FileCategory fileCategory, PageRequest pageRequest) {
        String categoryIdentifier = FileCatUtils.createCategoryIdentifier(fileCategory);
        return solrFileCatRepository.listFilesWithFilter(pageRequest, Criteria.create(ACCOUNT_CATEGORIES, SolrOperator.EQ, categoryIdentifier));
    }

    public SolrPingResponse pingSolr() {
        return solrFileCatRepository.pingSolr();
    }

    @AutoAuthenticate(replace = false)
    public Map<Long, Long> getFoldersDirectFileNumber(Collection<Long> folderIds) {
        if (folderIds == null || folderIds.isEmpty()) return null;
        return solrFileCatRepository.getFoldersDirectFileNumber(folderIds);
    }

    @AutoAuthenticate(replace = false)
    public Map<Long, Long> getFoldersAllFileNumber(Collection<DocFolder> folders) {
        if (folders == null || folders.isEmpty()) return null;
        return solrFileCatRepository.getFoldersAllFileNumber(folders);
    }

    public UpdateResponse saveAll(Collection<SolrInputDocument> atomicUpdates) {
        if (!atomicUpdates.isEmpty()) {
            return solrFileCatRepository.saveAll(atomicUpdates);
        }
        return null;
    }

    public void deleteByIds(List<String> ids) {
        if (ids != null && !ids.isEmpty()) {
            solrFileCatRepository.deleteByIds(ids);
        }
    }

    @AutoAuthenticate
    public void saveEntities(List<SolrFileEntity> entities) {
        solrFileCatRepository.saveAllEntities(entities);
    }

    public Pair<String, Set<Long>> getIngestedContents(String cursor, int count, long rootFolderId) {
        Query query = Query.create().setRows(count).setCursorMark(cursor)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, ClaFileState.INGESTED.name()))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.CONTENT_ID, SolrOperator.PRESENT, null))
                .addFilterWithCriteria(CatFileFieldType.TYPE, SolrOperator.IN, Arrays.asList(documentFileTypes))
                .addField(CatFileFieldType.CONTENT_ID)
                .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Set<Long> result = new HashSet<>();
        queryResponse.getResults().forEach(d -> {
            Long id = Long.parseLong((String) d.getFieldValue(CatFileFieldType.CONTENT_ID.getSolrName()));
            result.add(id);
        });
        return Pair.of(queryResponse.getNextCursorMark(), result);
    }

    public Pair<String ,Map<Long, String>> getScannedFiles(String cursor, int count, long rootFolderId) {
        Query query = Query.create().setRows(count).setCursorMark(cursor)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, ClaFileState.SCANNED.name()))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.TYPE, SolrOperator.IN, FileType.getMeaningfulFileTypes()))
                .addField(CatFileFieldType.FILE_ID)
                .addField(CatFileFieldType.BASE_NAME)
                .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Map<Long, String> result = new HashMap<>();
        queryResponse.getResults().forEach(d -> {
            Long id = (Long) d.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());
            String name = (String) d.getFieldValue(CatFileFieldType.BASE_NAME.getSolrName());
            result.put(id, name);
        });
        return Pair.of(queryResponse.getNextCursorMark(), result);
    }

    /**
     * Searching for files who were scanned only without ingest operation.
     * The search includes all files from previous runs which are in SCANNED state (but excludes current run)
     * it is a fix of bug DAMAIN-5365
     */
    public Pair<String ,Map<Long, String>> getMappedNotIngestedFiles(String cursor, int count, long rootFolderId, Date scanJobStartedDate) {
        Query query = Query.create().setRows(count).setCursorMark(cursor)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, ClaFileState.SCANNED.name()))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.TYPE, SolrOperator.IN, FileType.getMeaningfulFileTypes()))
                .addFilterWithCriteria(Criteria.or(
                        Criteria.create(CatFileFieldType.LAST_RECORD_UPDATE, SolrOperator.LT, scanJobStartedDate),
                        Criteria.create(CatFileFieldType.LAST_RECORD_UPDATE, SolrOperator.MISSING, null)))
                .addField(CatFileFieldType.FILE_ID)
                .addField(CatFileFieldType.BASE_NAME)
                .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Map<Long, String> result = new HashMap<>();
        queryResponse.getResults().forEach(d -> {
            Long id = (Long) d.getFieldValue(CatFileFieldType.FILE_ID.getSolrName());
            String name = (String) d.getFieldValue(CatFileFieldType.BASE_NAME.getSolrName());
            result.put(id, name);
        });
        logger.debug("retrieved {} mapped but un-ingested files for root folder {}", result.size(), rootFolderId);
        return Pair.of(queryResponse.getNextCursorMark(), result);
    }

    /**
     * Searching for files who were scanned only without ingest operation.
     * The search includes all files from previous runs which are in SCANNED state (but excludes current run)
     * it is a fix of bug DAMAIN-5365
     */
    public Pair<String, Set<Long>> getIngestNotAnalyzedContents(String cursor, int count, long rootFolderId, Date scanJobStartedDate) {
        Query query = Query.create().setRows(count).setCursorMark(cursor)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, ClaFileState.INGESTED.name()))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.CONTENT_ID, SolrOperator.PRESENT, null))
                .addFilterWithCriteria(Criteria.or(
                        Criteria.create(CatFileFieldType.INGEST_DATE, SolrOperator.LT, scanJobStartedDate.getTime()),
                        Criteria.create(CatFileFieldType.INGEST_DATE, SolrOperator.MISSING, null)))
                .addFilterWithCriteria(CatFileFieldType.TYPE, SolrOperator.IN, Arrays.asList(documentFileTypes))
                .addField(CatFileFieldType.CONTENT_ID)
                .addSort(CatFileFieldType.ID, Sort.Direction.ASC);

        QueryResponse queryResponse = solrFileCatRepository.runQuery(query.build());
        Set<Long> result = new HashSet<>();
        queryResponse.getResults().forEach(d -> {
            Long id = Long.parseLong((String) d.getFieldValue(CatFileFieldType.CONTENT_ID.getSolrName()));
            result.add(id);
        });
        logger.debug("retrieved {} ingested but un-analyzed files for root folder {}", result.size(), rootFolderId);
        return Pair.of(queryResponse.getNextCursorMark(), result);
    }

    public void handleFolderForFileRename(RootFolder rf, ClaFilePropertiesDto payload, Long fileId) {
        ClaFile file = getFileObject(fileId);
        DocFolder docFolder;
        if (file.isDeleted()) {
            docFolder = DocFolderService.getFromEntity(docFolderService.getFolderByPath(file.getRootFolderId(), file.getFullPath()));
            doUndeleteRecord(docFolder.getId(), file, payload);
        } else {
            docFolder = docFolderService.getById(file.getDocFolderId());
        }

        String fileName = payload.getFileName();
        // Subtract rf path from file name in case rf has a real path
        if (rf.getRealPath() != null && fileName.startsWith(rf.getRealPath())) {
            fileName = fileName.substring(rf.getRealPath().length());
        }

        if (!file.getFullPath().equals(docFolder.getPath())) {
            String folderUrl = DocFolderUtils.extractFolderFromPath(fileName) + "/";
            Long docFolderId = docFolderService.createDocFolderIfNeededWithCache(file.getRootFolderId(), folderUrl).getDocFolderId();
            file.setDocFolderId(docFolderId);
            docFolderService.getById(file.getDocFolderId());
        }
    }

    public void processFileRename(RootFolder rf, ClaFilePropertiesDto payload, Long fileId) {
        ClaFile file = getFileObject(fileId);
        logger.trace("Received rename payload from {} to {} for root folder {} under root folder id: {}, madiaType: {}",
                file.getFileName(), payload.getFileName(), rf.getId(), rf.getMediaType());
        DocFolder docFolder;
        if (file.isDeleted()) {
            logger.trace("File id: {}, path: {}  was deleted", file.getId(), file.getFileName());
            docFolder = DocFolderService.getFromEntity(docFolderService.getFolderByPath(file.getRootFolderId(),
                    file.getFullPath()));
            logger.trace("Fetching File id: {}, path: {}  folder which is id: {}, path: {}", file.getId(),
                    file.getFileName(), docFolder.getId(), docFolder.getPath());
            doUndeleteRecord(docFolder.getId(), file, payload);
        } else {
            logger.trace("File id: {}, path: {}  was not deleted", file.getId(), file.getFileName());
            docFolder = docFolderService.getById(file.getDocFolderId());
            logger.trace("Fetching File id: {}, path: {}  folder which is id: {}, path: {}", file.getId(),
                    file.getFileName(), docFolder.getId(), docFolder.getPath());
        }

        String fileName = payload.getFileName();
        // Subtract rf path from file name in case rf has a real path
        if (rf.getRealPath() != null && fileName.startsWith(rf.getRealPath())) {
            fileName = fileName.substring(rf.getRealPath().length());
        }


        file.setFullPathProperties(fileName);
        file.setFileName(fileName);
        logger.trace("File id: {}, change to path: {}", file.getId(), file.getFileName());
        if (!file.getFullPath().equals(docFolder.getPath())) {
            String folderUrl = DocFolderUtils.extractFolderFromPath(fileName) + "/";
            Long docFolderId = docFolderService.createDocFolderIfNeededWithCache(file.getRootFolderId(), folderUrl).getDocFolderId();
            file.setDocFolderId(docFolderId);
            docFolder = docFolderService.getById(file.getDocFolderId());
            logger.trace("File id:{} update parent folder to id: {}, path: ", docFolder.getId(), docFolder.getId(),
                    docFolder.getPath());
        }
        solrFileCatRepository.updateFilePath(file, docFolder);
    }

    /**
     * Give ClaFile a completeness assessment.
     * ClaFile with missing fields will rank lower,
     * ClaFile in ANALYZED state will rank higher than one in INGEST state etc.
     * Used in collision resolvers
     *
     * @param record ClaFile record
     * @return rank
     */
    public int rankClaFileRecord(ClaFile record) {
        List<String> associations = record.getAssociations();
        ClaFileState state = record.getState();
        return RecordGrade.create()
                .modifyGrade(record.getId() == null, -1000)
                .modifyGrade(record.getRootFolderId() == null, -1000)
                .modifyGrade(record.getDocFolderId() == null, -200)
                .modifyGrade(record.getFullPath() == null, -500)
                .modifyGrade(record.getBaseName() == null, -500)
                .modifyGrade(record.getContentMetadataId() == null, -100)
                .modifyGrade(record.getMediaType() == null, -100)
                .modifyGrade(associations != null && associations.size() > 0, 100)
                .modifyGrade(record.getAnalysisGroupId() != null, 100)
                .modifyGrade(record.getUserGroupId() != null, 100)
                .modifyGrade(state == null, -100)
                .modifyGrade(state == ClaFileState.ERROR || state == ClaFileState.SCANNED_AND_STOPPED, -100)
                .modifyGrade(state == ClaFileState.SCANNED, 10)
                .modifyGrade(state == ClaFileState.INGESTED, 20)
                .modifyGrade(state == ClaFileState.ANALYSED, 30).get();
    }

    public List<SolrDocument> findAllByQuery(SolrQuery query, CatFileFieldType sortField, Sort.Direction direction) {
        return solrFileCatRepository.findAllByQuery(query, sortField, direction);
    }

    public static void addField(SolrInputDocument solrInputDocument, CatFileFieldType type, Object value, SolrFieldOp operation) {
        Map<String, Object> fieldModifier = new HashMap<>(1);
        fieldModifier.put(operation.getOpName(), value);
        solrInputDocument.setField(type.getSolrName(), fieldModifier);
    }

    @AutoAuthenticate
    public void handleFolderMove(Long folderId, String oldPath, String newPath, Long rootFolderId) {
        try {
            // update folder
            DocFolder updatedDocFolder = docFolderService.updateDocFolderPath(folderId, newPath, rootFolderId);

            // update direct files
            updateFilesForMovedFolder(folderId, updatedDocFolder.getParentsInfo(), updatedDocFolder.getPath());

            // child folders
            int numFound;
            int depth = 1;
            do {
                List<DocFolder> children = docFolderService.listFolders(updatedDocFolder, depth, depth);
                if (children != null) {
                    numFound = children.size();
                    children.forEach(child -> {
                        String path = child.getRealPath();
                        path = path.replace(oldPath, newPath);
                        DocFolder childFolder = docFolderService.updateDocFolderPath(child.getId(), path, rootFolderId);
                        updateFilesForMovedFolder(childFolder.getId(), childFolder.getParentsInfo(), childFolder.getPath());
                    });
                    depth++;
                } else {
                    numFound = 0;
                }
            } while (numFound > 0);
        } finally {
            docFolderService.softCommitNoWaitFlush();
        }
    }

    public String limitLength(String original, int lengthLimit) {
        if (original == null) {
            return null;
        }
        return (original.length() < lengthLimit) ? original : original.substring(0, lengthLimit - 1);
    }

    public SolrFacetQueryJsonResponse runFacetQueryJson(SolrSpecification solrFacetSpecification) {
        return solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);
    }

    public Map<String, Long> getFieldTermCountMap(DataSourceRequest dataSourceRequest, CatFileFieldType field,
                                                  String... additionalFilters) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildTermJsonFacetSpecification(
                dataSourceRequest, field);
        Stream.of(additionalFilters).forEach(solrFacetSpecification::setAdditionalFilter);
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = solrFileCatRepository.runFacetQueryJson(
                solrFacetSpecification);
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse.getResponseBucket(field.getSolrName())
                .getBuckets();
        Map<String, Long> resultMap = new HashMap<>();
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            String fieldKey = bucket.getStringVal();
            Long bucketCount = bucket.getLongCountNumber();
            resultMap.put(fieldKey, bucketCount);
        }
        return resultMap;
    }

    public SolrFacetQueryResponse runSolrFacetQuery(DataSourceRequest dataSourceRequest, CatFileFieldType facetField) {
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, facetField);
        solrSpecification.setMinCount(1);
        SolrFacetQueryResponse solrFacetQueryResponse = solrFileCatRepository.runFacetQuery(solrSpecification, null);
        logger.trace("list file facets by Solr field {} returned {} results", facetField, solrFacetQueryResponse);
        return solrFacetQueryResponse;
    }

    public SolrFacetQueryResponse runSolrFacetQuery(FilterDescriptor baseFilter, CatFileFieldType facetField) {
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(baseFilter, facetField);
        solrSpecification.setMinCount(1);
        solrSpecification.setAdditionalFilter(CatFileFieldType.DELETED.filter("false"));
        SolrFacetQueryResponse solrFacetQueryResponse = solrFileCatRepository.runFacetQuery(solrSpecification, null);
        logger.trace("list file facets by Solr field {} returned {} results", facetField, solrFacetQueryResponse);
        return solrFacetQueryResponse;
    }

    public Map<String, Integer> runFacetRangeQuery(SolrSpecification solrSpecification) {
        return solrFileCatRepository.runFacetRangeQuery(solrSpecification);
    }

    public SolrFacetQueryJsonResponse runJsonFacetQuery(DataSourceRequest dataSourceRequest,
                                                        List<SolrFacetQueryJson> facetJsonObjectList,
                                                        String... additionalFilters) {
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                dataSourceRequest, facetJsonObjectList, additionalFilters);
        return solrFileCatRepository.runFacetQueryJson(solrSpecification);
    }

    class ContainerData {
        private Long fileId;
        private FileType type;
        private Long dateAdded;
        private List<String> containerIds;

        ContainerData(Long fileId, FileType type, Long dateAdded, List<String> containerIds) {
            this.fileId = fileId;
            this.type = type;
            this.dateAdded = dateAdded;
            this.containerIds = containerIds;
        }

        public Long getFileId() {
            return fileId;
        }

        public FileType getType() {
            return type;
        }

        public Long getDateAdded() {
            return dateAdded;
        }

        List<String> getContainerIds() {
            return containerIds;
        }
    }


    /**
     * Helper class for ClaFile record grading
     */
    private static class RecordGrade {
        private int grade = 0;

        public static RecordGrade create() {
            return new RecordGrade();
        }

        RecordGrade modifyGrade(boolean condition, int gradeModifier) {
            if (condition) {
                grade = grade + gradeModifier;
            }
            return this;
        }

        public int get() {
            return grade;
        }
    }

    public void acquireFileDeleteWriteLock() {
        solrFileCatRepository.acquireFileDeleteWriteLock();
    }

    public void releaseFileDeleteWriteLock() {
        solrFileCatRepository.releaseFileDeleteWriteLock();
    }
}
