package com.cla.filecluster.service.export.exporters.active_scans;

import com.cla.common.domain.dto.crawler.PhaseDetailsDto;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderRunSummaryInfo;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.active_scans.mixin.*;
import com.cla.filecluster.service.export.exporters.root_folders.mix_in.CustomerDataCenterDtoMixIn;
import com.cla.filecluster.service.files.filetree.ActiveScanQuery;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.WordUtils;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.active_scans.ActiveScansHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
@Slf4j
@Component
public class ActiveScansExporter extends PageCsvExcelExporter<RootFolderRunSummaryInfo> {

    @Autowired
    private ActiveScanQuery activeScanQuery;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    private Map<Long, String> scheduleGroupsNames;

    private DecimalFormat formatter = new DecimalFormat("00");

    private final static String[] header = {PATH, PROCESSED_FILE_TYPES, STATUS, PROGRESS, PHASE,
            PROCESSED_FILES, START_DATE, ELAPSED_TIME, MEDIA_TYPE, NICK_NAME,
            MAPPED_FILE_TYPES, INGESTED_FILE_TYPES, DATA_CENTER, RESCAN, REINGEST, REINGEST_TIME,
            STORE_LOCATION, STORE_PURPOSE, STORE_SECURITY, DESCRIPTION, MEDIA_CONNECTION,
            SCAN_TASK_DEPTH, SCHEDULE_GROUP};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.ACTIVE_SCANS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(RootFolderRunSummaryInfo.class, RootFolderRunSummaryInfoMixin.class)
                .addMixIn(RootFolderSummaryInfo.class, RootFolderSummaryInfoMixIn.class)
                .addMixIn(CrawlRunDetailsDto.class, CrawlRunDetailsDtoMixin.class)
                .addMixIn(CustomerDataCenterDto.class, CustomerDataCenterDtoMixIn.class)
                .addMixIn(RootFolderDto.class, RootFolderDtoMixIn.class)
                .addMixIn(ScheduleGroupDto.class, ScheduleGroupDtoMixin.class);

    }

    @Override
    protected Page<RootFolderRunSummaryInfo> getData(Map<String, String> requestParams) {
        scheduleGroupsNames = scheduleGroupService.getScheduleGroupsNames();
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        List<RootFolderRunSummaryInfo> res = activeScanQuery.run(requestParams, pageRequest);
        return new PageImpl<>(res);
    }

    @Override
    protected Map<String, Object> addExtraFields(RootFolderRunSummaryInfo base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();

        CrawlRunDetailsDto run = base.getRootFolderSummaryInfo() != null ?
                base.getRootFolderSummaryInfo().getCrawlRunDetailsDto() : null;

        if (run != null && run.getScanEndTime() != null) {
            Interval interval =
                    new Interval(run.getScanStartTime(), run.getScanEndTime());
            Period period = interval.toPeriod();

            result.put(ELAPSED_TIME, formatter.format(period.getHours())+":"+
                    formatter.format(period.getMinutes())+":"+formatter.format(period.getSeconds()));
        }

        //WORD files - 0  ,Excel files - 0  ,PDF files - 0  ,Other files - 6
        if (run != null) {
            long word = run.getProcessedWordFiles() == null ? 0 : run.getProcessedWordFiles();
            long excel = run.getProcessedExcelFiles() == null ? 0 : run.getProcessedExcelFiles();
            long pdf = run.getProcessedPdfFiles() == null ? 0 : run.getProcessedPdfFiles();
            long other = run.getProcessedOtherFiles() == null ? 0 : run.getProcessedOtherFiles();
            String processedFilesByType = "WORD files - "+word+"  ,Excel files - "+excel+"  ,PDF files - "+pdf+"  ,Other files - "+other;
            result.put(PROCESSED_FILE_TYPES, processedFilesByType);
        }

        PhaseDetailsDto job = base.getCurrentJob();
        if (job != null) {
            result.put(PHASE, job.getJobType() == null ? JobType.UNKNOWN.getName() : job.getJobType().getName());
            int counter = job.getPhaseCounter() == null ? 0 : job.getPhaseCounter();
            long max = job.getPhaseMaxMark() == null ? 0L : job.getPhaseMaxMark();
            result.put(PROGRESS, counter+"/"+max);
        }

        if (base.getRootFolderSummaryInfo() != null && base.getRootFolderSummaryInfo().getRootFolderDto() != null &&
                base.getRootFolderSummaryInfo().getRootFolderDto().getCustomerDataCenterDto() != null) {
            long id = base.getRootFolderSummaryInfo().getRootFolderDto().getCustomerDataCenterDto().getId();
            result.put(DATA_CENTER, scheduleGroupsNames.get(id));
        }

        String state = null;
        if (base.getRootFolderSummaryInfo().getRunStatus() != null) {
            state = base.getRootFolderSummaryInfo().getRunStatus();
        } else if (base.getRootFolderSummaryInfo().getCrawlRunDetailsDto() != null &&
                base.getRootFolderSummaryInfo().getCrawlRunDetailsDto().getRunOutcomeState() != null) {
            state = FileCrawlerExecutionDetailsService.simplifyRunStatus(
                    base.getRootFolderSummaryInfo().getCrawlRunDetailsDto().getRunOutcomeState(),
                    base.getRootFolderSummaryInfo().getCrawlRunDetailsDto().getPauseReason(),
                    base.getRootFolderSummaryInfo().getCrawlRunDetailsDto().getGracefulStop());
        }
        if (state != null) {
            state = WordUtils.capitalizeFully(state);
            result.put(STATUS, state);
        }

        return result;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
