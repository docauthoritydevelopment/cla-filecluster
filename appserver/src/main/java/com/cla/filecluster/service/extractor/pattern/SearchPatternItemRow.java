package com.cla.filecluster.service.extractor.pattern;

import com.cla.filecluster.service.importEntities.ImportItemRow;

import java.util.Arrays;

/**
 * Created by ophir on 04/21/2019.
 */
public class SearchPatternItemRow  extends ImportItemRow {

    private String name;

    private String pattern;

    private String validatorClass;

    private String description;

    private boolean active;

    private String categoryName;

    private String subCategoryName;

    private String[] regulations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    String getValidatorClass() {
        return validatorClass;
    }

    void setValidatorClass(String validatorClass) {
        this.validatorClass = validatorClass;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    String getCategoryName() {
        return categoryName;
    }

    void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    String getSubCategoryName() {
        return subCategoryName;
    }

    void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String[] getRegulations() {
        return regulations;
    }

    public void setRegulations(String[] regulations) {
        this.regulations = regulations;
    }

    @Override
    public String toString() {
        return "SearchPatternItemRow{" +
                "name='" + name + '\'' +
                ", active='" + active + '\'' +
                ", pattern='" + pattern + '\'' +
                ", validatorClass='" + validatorClass + '\'' +
                ", description='" + description + '\'' +
                ", active='" + active + '\'' +
                ", categoryName='" + categoryName + '\'' +
                ", subCategoryName='" + subCategoryName + '\'' +
                ", regulationStr='" + (regulations != null  ? Arrays.toString(regulations) : "" )+ '\'' +
                super.toString()+
                '}';
    }
}
