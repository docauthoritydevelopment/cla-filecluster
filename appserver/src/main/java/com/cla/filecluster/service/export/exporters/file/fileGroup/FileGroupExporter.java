package com.cla.filecluster.service.export.exporters.file.fileGroup;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.security.CompositeRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.api.groups.GroupsApiController;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.CollectionResolverHelper.resolveCollectionValue;
import static com.cla.filecluster.service.export.exporters.file.fileGroup.FileGroupHeaderFields.*;
import static java.lang.String.format;

@Slf4j
@Component
public class FileGroupExporter extends PageCsvExcelExporter<AggregationCountItemDTO<GroupDto>> {
	private final static String[] HEADER = {NAME, GROUP_SIZE, NUM_OF_FILES, NUM_OF_FILTERED, PERCENT_OF_FILTERED,
			ASSOCIATION, DOC_TYPES, DATA_ROLES, TAGS, SAMPLE_MEMBER};

	@Autowired
	private GroupsApiController groupsApiController;

	@Override
	protected String[] getHeader() {
		return HEADER;
	}

	@Override
	protected void configureMapper(CsvMapper mapper) {
	}

	@Override
	protected Iterable<AggregationCountItemDTO<GroupDto>> getData(Map<String, String> requestParams) {
		FacetPage<AggregationCountItemDTO<GroupDto>> groupsFileCount = groupsApiController.findGroupsFileCount(requestParams);
		return groupsFileCount;
	}

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<GroupDto> base, Map<String, String> requestParams) {
		Map<String, Object> result = new HashMap<>();
		GroupDto item = base.getItem();
		result.put(NAME, item.getGroupName());

		if (!Strings.isNullOrEmpty(item.getFirstMemberName())) {
			result.put(SAMPLE_MEMBER,
					(Strings.isNullOrEmpty(item.getFirstMemberPath()) ? "" : item.getFirstMemberPath()) + item.getFirstMemberName());
		}

		long fileNumUnFiltered = base.getCount2() > 0 ? base.getCount2() : base.getCount();
		result.put(NUM_OF_FILES, fileNumUnFiltered);
		result.put(NUM_OF_FILTERED, base.getCount());
		result.put(PERCENT_OF_FILTERED, (base.getCount()*100)/fileNumUnFiltered);

		result.put(GROUP_SIZE, item.getNumOfFiles());

		result.put(TAGS, resolveCollectionValue(item.getFileTagDtos(),
				t -> format("%s:%s", t.getTypeName(), t.getName()), FileTagDto::isImplicit));

		result.put(DATA_ROLES, resolveCollectionValue(item.getAssociatedFunctionalRoles(),
				CompositeRoleDto::getName, FunctionalRoleDto::getImplicit));

		result.put(DOC_TYPES, resolveCollectionValue(item.getDocTypeDtos(),
				DocTypeDto::getName, DocTypeDto::isImplicit));

		result.put(ASSOCIATION, resolveCollectionValue(item.getAssociatedBizListItems(),
				SimpleBizListItemDto::getName, SimpleBizListItemDto::isImplicit));

		return result;
	}

	@Override
	public PagingMethod getPagingMethod() {
		return PagingMethod.MULTI;
	}

	@Override
	public boolean doesAccept(ExportType exportType, Map<String, String> params) {
		return exportType.equals(ExportType.FILE_GROUP);
	}
}
