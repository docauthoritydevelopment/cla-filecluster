package com.cla.filecluster.service.export.exporters.settings.bizlist.mixin;

import com.cla.common.domain.dto.bizlist.BizListItemState;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.settings.bizlist.BizListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class SimpleBizListItemDtoMixin {

    @JsonProperty(BUSINESS_ID)
    private String businessId;

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(STATE)
    private BizListItemState state;

}
