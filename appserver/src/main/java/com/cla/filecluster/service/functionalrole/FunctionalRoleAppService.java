package com.cla.filecluster.service.functionalrole;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.util.categories.FileCatUtils;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.FacetField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 4/22/2019
 */
@Service
public class FunctionalRoleAppService {

    private static final Logger logger = LoggerFactory.getLogger(FunctionalRoleAppService.class);

    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Transactional
    public FacetPage<AggregationCountItemDTO<FunctionalRoleDto>> listFuncRolesFileCountWithFileFilter(DataSourceRequest dataSourceRequest) {
        try {
            logger.debug("List func roles Association File Counts With File Filters");
            long totalFunctionalRoleDto = fileCatAppService.getTotalFacetCountByPrefix(dataSourceRequest,
                    CatFileFieldType.TAGS_2, FunctionalRoleDto.ID_PREFIX);
            SolrFacetQueryResponse solrFacetQueryResponse = getFacetFileCountResponse(dataSourceRequest, null);
            logger.debug("list file facets by Solr field {} returned {} results", CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION, solrFacetQueryResponse.getFirstResult().getValueCount());

            SolrFacetQueryResponse solrFacetQueryRespNoFilter = null;
            if (dataSourceRequest.hasFilter() && solrFacetQueryResponse.getFirstResult().getValueCount() > 0
                    && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
                dataSourceRequest.clearForUnfilteredForSpecific();
                solrFacetQueryRespNoFilter = getFacetFileCountResponse(dataSourceRequest,
                        CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION.inQuery(SolrSpecificationFactory.getValuesFromFacet(solrFacetQueryResponse)));
            }

            FacetPage<AggregationCountItemDTO<FunctionalRoleDto>> facetPage = convertFacetResultsToFunctionalRolesDto(
                    solrFacetQueryResponse, solrFacetQueryRespNoFilter, solrFacetQueryResponse.getPageRequest());
            facetPage.setTotalElements(totalFunctionalRoleDto);
            return facetPage;
        } catch (RuntimeException e) {
            logger.error("Failed to list func roles file counts with filter {}", dataSourceRequest.getFilter(), e);
            throw e;
        }
    }

    private SolrFacetQueryResponse getFacetFileCountResponse(DataSourceRequest dataSourceRequest, String additionalFilter) {
        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.FUNCTIONAL_ROLE_ASSOCIATION);
        solrFacetSpecification.setMinCount(1);
        if (additionalFilter != null) {
            solrFacetSpecification.setAdditionalFilter(additionalFilter);
        }
        solrFacetSpecification.setFacetPrefix(FileDtoFieldConverter.FUNCTIONAL_ROLE_SOLR_PREFIX);
        return runFacetQuery(dataSourceRequest, solrFacetSpecification);
    }

    public SolrFacetQueryResponse runFacetQuery(DataSourceRequest dataSourceRequest, SolrSpecification solrFacetSpecification) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        String selectedItemId = dataSourceRequest.getSelectedItemId();
        return fileCatService.runFacetQuery(solrFacetSpecification, c -> String.valueOf(convertSolrValueToId(c.getName())), pageRequest, selectedItemId);
    }

    private FacetPage<AggregationCountItemDTO<FunctionalRoleDto>> convertFacetResultsToFunctionalRolesDto(
            SolrFacetQueryResponse solrFacetQueryResponse, SolrFacetQueryResponse solrFacetQueryRespNoFilter, PageRequest pageRequest) {
        List<AggregationCountItemDTO<FunctionalRoleDto>> content = Lists.newArrayList();
        FacetField facetField = solrFacetQueryResponse.getFirstResult();
        if (facetField.getValues().size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        //We need to convert it with the help of mysql
        //First collect the result ids to a Set (original values look like type.id
        List<Long> ids = facetField.getValues().stream().map(c -> convertSolrValueToId(c.getName())).collect(Collectors.toList());
        Set<FunctionalRole> funcRoles = functionalRoleService.getFunctionalRoleByIds(ids);
        Map<Long, FunctionalRoleDto> funRoleDtoByIdMap = funcRoles.stream().filter(fr -> !fr.isDeleted()).collect(
                Collectors.toMap(FunctionalRole::getId, FunctionalRoleService::convertFunctionalRole));

        Map<String, Long> resultsNoFilter = null;
        if (solrFacetQueryRespNoFilter != null) {
            resultsNoFilter = solrFacetQueryRespNoFilter.getFirstResult().getValues().stream()
                    .collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount));
        }

        logger.debug("convert Facet Field result for {} elements to FunctionalRoleDto. DB returned {} elements", ids.size(), funcRoles.size());
        long pageAggCount = 0;
        FunctionalRoleDto functionalRoleDto = null;
        Map<Long, AggregationCountItemDTO<FunctionalRoleDto>> items = new HashMap<>();
        for (FacetField.Count count : facetField.getValues()) {
            long functionalRoleId = convertSolrValueToId(count.getName());
            functionalRoleDto = funRoleDtoByIdMap.get(functionalRoleId);
            if (functionalRoleDto == null) {
                logger.warn("Failed to find functionalRole by id " + functionalRoleId + " (" + count.getName() + ") in the database. Solr and Database are not synced.");
                continue;
            }
            AggregationCountItemDTO<FunctionalRoleDto> item = items.get(functionalRoleId);
            Long countNoFilter = resultsNoFilter == null ? null : resultsNoFilter.get(count.getName());
            if (item == null) {
                item = new AggregationCountItemDTO<>(functionalRoleDto, count.getCount());
                items.put(functionalRoleId, item);
                content.add(item);
                if (countNoFilter != null) {
                    item.setCount2(countNoFilter);
                }
            } else {
                item.incrementCount(count.getCount());
                if (countNoFilter != null) {
                    item.incrementCount2(countNoFilter);
                }
            }
            pageAggCount += count.getCount();
        }
        return new FacetPage<>(content, pageRequest, pageAggCount, solrFacetQueryResponse.getNumFound());
    }

    public ClaFileDto removeFunctionalRoleAssociationFromFile(long functionalRoleId, long fileId) {
        logger.debug("remove functionalRole {} Association From File {}", functionalRoleId, fileId);
        FunctionalRoleDto fr = functionalRoleService.getFromCache(functionalRoleId);
        if (fr != null) {
            associationsService.removeFunctionalRoleFromFile(fileCatService.getFileEntity(fileId), fr, SolrCommitType.SOFT_NOWAIT);
            return FileCatUtils.convertClaFile(fileCatService.getFileObject(fileId));
        }
        return null;
    }

    public ClaFileDto associateFileWithFunctionalRole(long functionalRoleId, long fileId) {
        FunctionalRoleDto fr = functionalRoleService.getFromCache(functionalRoleId);
        if (fr != null) {
            logger.debug("Associate File id {} with functional role {}", fileId, fr);
            return associationsService.associateFunctionalRoleItemWithFile(fileCatService.getFileObject(fileId), fr, SolrCommitType.SOFT_NOWAIT);
        }
        return null;
    }

    public DocFolderDto removeFunctionalRoleAssociationFromFolder(long functionalRoleId, long docFolderId) {
        FunctionalRoleDto fr = functionalRoleService.getFromCache(functionalRoleId);
        if (fr != null) {
            associationsService.markFolderAssociationAsDeleted(docFolderId, fr, null, AssociableEntityType.DATA_ROLE);
            return docFolderService.getByIdWithAssociations(docFolderId);
        }
        return null;
    }

    @Transactional
    public GroupDto associateGroupWithFunctionalRole(long functionalRoleId, String groupId) {
        FunctionalRole fr = functionalRoleService.getDataRoleById(functionalRoleId);
        if (fr != null) {
            return groupsService.addFunctionalRoleToGroup(groupId, fr);
        }
        return null;
    }

    @Transactional
    public GroupDto removeFunctionalRoleAssociationFromGroup(long functionalRoleId, String groupId) {
        FunctionalRole fr = functionalRoleService.getDataRoleById(functionalRoleId);
        if (fr != null) {
            return groupsService.markFunctionalRoleAssociationAsDeleted(groupId, fr);
        }
        return null;
    }

    @Transactional
    public DocFolderDto associateFolderWithFunctionalRole(long functionalRoleId, long docFolderId) {
        logger.debug("Associate Folder id {} with functionalRoleId {}", docFolderId, functionalRoleId);
        FunctionalRoleDto fr = functionalRoleService.getFromCache(functionalRoleId);
        if (fr != null) {
            return docFolderService.addExplicitAssociableEntityToFolder(docFolderId, fr, AssociableEntityType.DATA_ROLE);
        }
        return null;
    }

    private long convertSolrValueToId(String value) {
        try {
            return AssociationIdUtils.extractFunctionalRoleIdFromSolrIdentifier(value);
        } catch (RuntimeException e) {
            logger.error("Failed to extract tag id from Solr identfier " + value, e);
            throw e;
        }
    }

    public void deleteFunctionalRole(long id) {
        if (associationsService.funcRoleAssociationExists(id)) {
            throw new BadRequestException(messageHandler.getMessage("data-role.delete.has-associations"), BadRequestType.ITEM_ATTACHED_TO_ANOTHER);
        }
        functionalRoleService.deleteFunctionalRole(id);
    }
}
