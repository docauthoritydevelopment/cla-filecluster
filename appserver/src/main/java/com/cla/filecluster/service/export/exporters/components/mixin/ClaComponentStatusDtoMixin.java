package com.cla.filecluster.service.export.exporters.components.mixin;

import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/17/2019
 */
public class ClaComponentStatusDtoMixin {

    @JsonProperty(TIMESTAMP)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long timestamp;
}
