package com.cla.filecluster.service.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskRejectedException;

import java.util.concurrent.Callable;

/**
 * This class extends {@code Callable} class, and handles exceptions inside the call() function.
 *
 * While extending this class, user must override {@code execute()} method, which will be called
 * upon execution, surrounded by try-catch.
 *
 * It is a good idea to implement {@code BaseCommand.onError()} and {@code BaseCommand.onRejection()},
 * to specify what to do on error or rejection.
 *
 * Another good idea is to override toString() method, so log will show meaningful message.
 *
 * @param <T> return type of the command
 */
@SuppressWarnings("WeakerAccess")
public abstract class BaseCommand<T> implements Callable<T> {

    protected Logger logger;

    public BaseCommand(){
        logger = LoggerFactory.getLogger(getClass());
    }

    /**
     * This method will be called inside {@code call()} method.
     * @return command result
     */
    public abstract T execute();

    /**
     * internal function to call upon execution, with exception handling.
     * This function must throw an exception, so if using submit() to get {@code Future<T>} object,
     * calling {@code future.get()} will throw the thrown exception (surrounded by {@code ExecutionException}).
     */
    @Override
    public T call(){
        try {
            return doExecute();
        } catch (Exception e){
            onError(e);
            throw e;
        }
    }

    protected T doExecute() {
        return execute();
    }

    /**
     * What to do when {@code execute()} raises an exception.
     * Here we log it. Another implementation can override this method.
     * @param e exception
     */
    public void onError(Exception e){
        logger.error("Task: " + this + " raised exception upon execution.", e);
    }

    /**
     * What to do when this command is rejected.
     * Here we log it. Another implementation can override this method.
     *
     * @param e exception
     */
    public void onRejection(TaskRejectedException e){
        logger.error("Task: " + this + " was rejected.", e);
    }
}
