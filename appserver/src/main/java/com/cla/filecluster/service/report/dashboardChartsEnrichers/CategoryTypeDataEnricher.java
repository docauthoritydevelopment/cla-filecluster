package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.report.DashboardDataEnricher;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


@Component
public interface CategoryTypeDataEnricher extends DashboardDataEnricher {


    default ChartQueryType getQueryType() {
        return ChartQueryType.CATEGORY;
    }

    Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params);

    default void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
    };

    default boolean needToRemoveEmptyValues(){
        return true;
    }

    String getFilterFieldName();
}
