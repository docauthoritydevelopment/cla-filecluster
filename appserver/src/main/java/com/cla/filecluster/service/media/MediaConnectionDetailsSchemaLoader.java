package com.cla.filecluster.service.media;

import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.*;
import com.cla.filecluster.service.importEntities.SchemaData;
import com.cla.filecluster.service.importEntities.SchemaLoader;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 *
 * Created by liora on 21/03/2017.
 */
@Service
public class MediaConnectionDetailsSchemaLoader extends SchemaLoader<MediaConnectionDetailsItemRow,MediaConnectionDetailsDto> {
    private static Logger logger = LoggerFactory.getLogger(MediaConnectionDetailsSchemaLoader.class);

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    private final static String CSV_HEADER_USERNAME = "USERNAME";
    private final static String CSV_HEADER_NAME = "NAME";
    private final static String CSV_HEADER_MEDIA_TYPE = "MEDIA TYPE";
    private final static String CSV_HEADER_URL = "URL";
    private final static String CSV_HEADER_DOMAIN = "DOMAIN";
    private final static String CSV_HEADER_PASSWORD = "PASSWORD";
    private final static String CSV_HEADER_TENANT_ID = "TENANT ID";
    private final static String CSV_HEADER_APP_ID = "APPLICATION ID";
    private final static String CSV_HEADER_CLIENT = "CLIENT ID";
    private final static String CSV_HEADER_RESOURCE = "RESOURCE";

    private final static String[] REQUIRED_HEADERS = {CSV_HEADER_NAME, CSV_HEADER_MEDIA_TYPE};

    public MediaConnectionDetailsSchemaLoader() {
        super(REQUIRED_HEADERS);
    }

    @Override
    public SchemaData<MediaConnectionDetailsItemRow> createSchemaInstance() {
        return new SchemaData<MediaConnectionDetailsItemRow>(){
            @Override
            public MediaConnectionDetailsItemRow createImportItemRowInstance() {
                return new MediaConnectionDetailsItemRow();
            }
        };
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected MediaConnectionDetailsItemRow extractRowSpecific(SchemaData<MediaConnectionDetailsItemRow> schemaData, String[] fieldsValues, ArrayList<String> parsingWarningMessages) {

        MediaConnectionDetailsItemRow itemRow = new MediaConnectionDetailsItemRow();
        itemRow.setUsername(extractValue(schemaData, fieldsValues, CSV_HEADER_USERNAME, parsingWarningMessages));
        itemRow.setName(extractValue(schemaData, fieldsValues, CSV_HEADER_NAME, parsingWarningMessages));
        itemRow.setUrl(extractValue(schemaData, fieldsValues, CSV_HEADER_URL, parsingWarningMessages));
        itemRow.setMediaType(extractValue(schemaData, fieldsValues, CSV_HEADER_MEDIA_TYPE, MediaType.class, parsingWarningMessages));
        itemRow.setDomain(extractValue(schemaData, fieldsValues, CSV_HEADER_DOMAIN, parsingWarningMessages));
        itemRow.setPassword(extractValue(schemaData, fieldsValues, CSV_HEADER_PASSWORD, parsingWarningMessages));
        itemRow.setTenantId(extractValue(schemaData, fieldsValues, CSV_HEADER_TENANT_ID, parsingWarningMessages));
        itemRow.setApplicationId(extractValue(schemaData, fieldsValues, CSV_HEADER_APP_ID, parsingWarningMessages));
        itemRow.setClientId(extractValue(schemaData, fieldsValues, CSV_HEADER_CLIENT, parsingWarningMessages));
        itemRow.setResource(extractValue(schemaData, fieldsValues, CSV_HEADER_RESOURCE, parsingWarningMessages));
        return itemRow;
    }


    @Override
    protected boolean validateItemRow(MediaConnectionDetailsDto mediaConnectionDetailsDto, MediaConnectionDetailsItemRow itemRow,
                                      SchemaData<MediaConnectionDetailsItemRow> schemaData, boolean afterAddToDB, boolean createStubs ) {
        boolean isValid = true;

        if (Strings.isNullOrEmpty(mediaConnectionDetailsDto.getName())){
            schemaData.addErrorRowItem( itemRow, messageHandler.getMessage("media-conn.import.missing-name"));
            return false;
        }

        if (itemRow.getMediaType() == null ) {
            schemaData.addErrorRowItem( itemRow, messageHandler.getMessage("media-conn.import.missing-type"));
            return false;
        }

        if(!afterAddToDB) { //validate if already exists only before added to db
            try {
                mediaConnectionDetailsService.isValidForMediaConnectionCreation(mediaConnectionDetailsDto.getName());
            } catch (RuntimeException e) {
                schemaData.addErrorRowItem( itemRow, messageHandler.getMessage("media-conn.import.missing-name"));
                return false;
            }

            try {
                mediaConnectionDetailsService.checkAlreadyExists(mediaConnectionDetailsDto.getName(), null);
            } catch (RuntimeException e) {
                schemaData.addDupRowItem(itemRow,
                        messageHandler.getMessage("media-conn.import.duplicate", Lists.newArrayList(mediaConnectionDetailsDto.getName())));
                isValid = false;
            }
        }

        if (!itemRow.getMediaType().equals(MediaType.EXCHANGE_365)) {
            if (Strings.isNullOrEmpty(itemRow.getUsername())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-username"));
                isValid = false;
                //continue validating when warnings
            }

            if (Strings.isNullOrEmpty(itemRow.getUrl())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-url"));
                isValid = false;
            }
        }

        if (Strings.isNullOrEmpty(itemRow.getPassword())) {
            schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-password"));
            isValid =  false;
        }

        if (itemRow.getMediaType() == MediaType.SHARE_POINT || itemRow.getMediaType() == MediaType.ONE_DRIVE) {
            if (Strings.isNullOrEmpty(itemRow.getDomain())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-domain"));
                isValid =  false;
            }
        } else if (itemRow.getMediaType().equals(MediaType.EXCHANGE_365)) {
            if (Strings.isNullOrEmpty(itemRow.getTenantId())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-tenant"));
                isValid =  false;
            }
            if (Strings.isNullOrEmpty(itemRow.getApplicationId())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-application"));
                isValid =  false;
            }
        } else if (itemRow.getMediaType().equals(MediaType.MIP)) {
            if (Strings.isNullOrEmpty(itemRow.getClientId())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-client"));
                isValid =  false;
            }
            if (Strings.isNullOrEmpty(itemRow.getResource())) {
                schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("media-conn.import.missing-resource"));
                isValid =  false;
            }
        }
        return isValid;
    }

    @Override
    protected Boolean loadItemRowToRepository(SchemaData<MediaConnectionDetailsItemRow> schemaData, MediaConnectionDetailsItemRow itemRow, boolean updateDuplicates, boolean createStubs) {

        MediaConnectionDetailsDto mediaConnectionDetailsDto;
        MediaConnectionDetailsDto resultMediaConnectionDetailsDto;

        mediaConnectionDetailsDto = createDtoFromRowItem(itemRow);
        if (updateDuplicates) {
            if (mediaConnectionDetailsService.isConnectionDetailsExists(mediaConnectionDetailsDto.getName())) {
                resultMediaConnectionDetailsDto = mediaConnectionDetailsService.updateConnectionDetails(mediaConnectionDetailsDto);
            } else {
                resultMediaConnectionDetailsDto = mediaConnectionDetailsService.createConnectionDetails(mediaConnectionDetailsDto);
            }
        } else {
            resultMediaConnectionDetailsDto = mediaConnectionDetailsService.createConnectionDetails(mediaConnectionDetailsDto);
        }

        if (resultMediaConnectionDetailsDto == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("import.error-insert-row"));
            return false;
        } else {
            return validateItemRow(resultMediaConnectionDetailsDto, itemRow, schemaData, true, createStubs);

        }
    }

    @Override
    protected MediaConnectionDetailsDto createDtoFromRowItem(MediaConnectionDetailsItemRow itemRow) {
        MediaConnectionDetailsDto mediaConnectionDetailsDto;

        MediaType mediaType = itemRow.getMediaType();
        if (mediaType != null && (mediaType.equals(MediaType.ONE_DRIVE) || mediaType.equals(MediaType.SHARE_POINT))) {
            MicrosoftConnectionDetailsDtoBase msConnectionDetailsDto =
                    MediaType.ONE_DRIVE.equals(mediaType)
                            ? new OneDriveConnectionDetailsDto()
                            : new SharePointConnectionDetailsDto();
            SharePointConnectionParametersDto sharePointConnectionParametersDto = new SharePointConnectionParametersDto();
            sharePointConnectionParametersDto.setDomain(itemRow.getDomain());
            sharePointConnectionParametersDto.setPassword(itemRow.getPassword());
            sharePointConnectionParametersDto.setUrl(itemRow.getUrl());
            sharePointConnectionParametersDto.setUsername(itemRow.getUsername());
            msConnectionDetailsDto.setSharePointConnectionParametersDto(sharePointConnectionParametersDto);

            String sharePointDetails =
                    MediaConnectionDetailsService.convertSharePointConnectionParamsDtoToJson(sharePointConnectionParametersDto);
            String encryptedDetails = mediaConnectionDetailsService.encrypt(sharePointDetails);
            msConnectionDetailsDto.setConnectionParametersJson(encryptedDetails);

            mediaConnectionDetailsDto = msConnectionDetailsDto;
        } else if (mediaType != null && mediaType.equals(MediaType.EXCHANGE_365)) {
            Exchange365ConnectionDetailsDto exchange365ConnectionDetailsDto = new Exchange365ConnectionDetailsDto();
            Exchange365ConnectionParametersDto exchangeConnectionParametersDto = new Exchange365ConnectionParametersDto();
            exchangeConnectionParametersDto.setApplicationId(itemRow.getApplicationId());
            exchangeConnectionParametersDto.setTenantId(itemRow.getTenantId());
            exchangeConnectionParametersDto.setPassword(itemRow.getPassword());
            exchange365ConnectionDetailsDto.setExchangeConnectionParametersDto(exchangeConnectionParametersDto);

            String details = MediaConnectionDetailsService.convertExchangeConnectionParamsDtoToJson(exchangeConnectionParametersDto);
            String encryptedDetails = mediaConnectionDetailsService.encrypt(details);
            mediaConnectionDetailsDto = exchange365ConnectionDetailsDto;
            mediaConnectionDetailsDto.setConnectionParametersJson(encryptedDetails);
        } else if (mediaType != null && mediaType.equals(MediaType.MIP)) {
            MIPConnectionDetailsDto MIPConnectionDetailsDto = new MIPConnectionDetailsDto();
            MIPConnectionParametersDto params = new MIPConnectionParametersDto();
            params.setClientId(itemRow.getClientId());
            params.setResource(itemRow.getResource());
            params.setPassword(itemRow.getPassword());
            MIPConnectionDetailsDto.setMIPConnectionParametersDto(params);

            String details = MediaConnectionDetailsService.convertMicrosoftLabelingConnectionParamsDtoToJson(params);
            String encryptedDetails = mediaConnectionDetailsService.encrypt(details);
            mediaConnectionDetailsDto = MIPConnectionDetailsDto;
            mediaConnectionDetailsDto.setConnectionParametersJson(encryptedDetails);
        } else {
            mediaConnectionDetailsDto = new MediaConnectionDetailsDto();
        }
        mediaConnectionDetailsDto.setUsername(itemRow.getUsername());
        mediaConnectionDetailsDto.setMediaType(mediaType);
        mediaConnectionDetailsDto.setName(itemRow.getName());
        mediaConnectionDetailsDto.setUrl(itemRow.getUrl());
        mediaConnectionDetailsDto.setPstCacheEnabled(MediaConnectionDetailsService.isPstCacheEnabledByDefault(mediaType));

        return mediaConnectionDetailsDto;
    }


}
