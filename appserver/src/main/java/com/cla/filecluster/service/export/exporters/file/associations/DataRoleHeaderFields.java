package com.cla.filecluster.service.export.exporters.file.associations;

public interface DataRoleHeaderFields {
    String NAME = "Name";
    String DESCRIPTION = "Description";
    String NUM_OF_FILES = "Number Of Files";
}
