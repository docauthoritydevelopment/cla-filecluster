package com.cla.filecluster.service.export.exporters.file.list_as_files;

public interface AsFilesHeaderFields {
    String TYPE = "Type";
    String FILE_NAME = "File name";
    String PATH = "Path";
    String FILE_SIZE = "File size";
    String CREATION_DATE = "Creation date";
    String MODIFIED_DATE = "Modified date";
    String ACCESSED_DATE = "Accessed date";
    String OWNER = "Owner";
    String GROUP_NAME = "Group name";
}
