package com.cla.filecluster.service.audit;

import ch.qos.logback.classic.db.names.DBNameResolver;

public class AuditDBNameResolver implements DBNameResolver {

	@Override
	public <N extends Enum<?>> String getTableName(N tableName) {
		return tableName.name().toLowerCase();
	}

	@Override
	public <N extends Enum<?>> String getColumnName(N columnName) {
		return columnName.name().toLowerCase();
	}

}
