package com.cla.filecluster.service.export.exporters.user_view;

import com.cla.common.domain.dto.filter.SavedFilterDto;
import com.cla.common.domain.dto.report.DisplayType;
import com.cla.common.domain.dto.report.UserViewDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.report.UserViewDataService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.user_view.UserViewDataHeaderFields.*;


/**
 * Created by: ophir
 * Created on: 7/10/2019
 */
@Slf4j
@Component
public class UserViewDataExporter extends PageCsvExcelExporter<UserViewDataDto> {

    private final static String[] HEADER = {NAME,DISPLAY_TYPE,DISPLAY_DATA,FILTER_NAME,FILTER_DESCRIPTOR,FILTER_RAW,IS_FILTER_GLOBAL, CONTAINER_NAME};

    private static final long DETACHED_CONTAINER_ID = -1;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private UserViewDataService userViewDataService;


    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(UserViewDataDto.class, UserViewDataDtoMixin.class);
    }

    @Override
    protected Page<UserViewDataDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        Page<UserViewDataDto> ans;
        if (!Strings.isNullOrEmpty(requestParams.get("displayType"))) {
            DisplayType type = DisplayType.valueOf(requestParams.get("displayType"));
            if (!Strings.isNullOrEmpty(requestParams.get("withChildData")) && requestParams.get("withChildData").equals("true")) {
                ans = userViewDataService.listUserViewDataWithChildDataByTypeForExport(dataSourceRequest, type);
            }
            else if (!Strings.isNullOrEmpty(requestParams.get("containerId"))) {
                Long containerId = Long.parseLong(requestParams.get("containerId"));
                if (containerId.equals(DETACHED_CONTAINER_ID)) {
                    ans = userViewDataService.listUserViewDataWithoutContainerByType(dataSourceRequest, type);
                }
                else {
                    ans = userViewDataService.listUserViewDataByTypeAndContainerId(dataSourceRequest, type, containerId);
                }
            }
            else {
                ans = userViewDataService.listUserViewDataByType(dataSourceRequest, type);
            }
        }
        else {
            ans = userViewDataService.listUserViewData(dataSourceRequest);
        }
        return ans;
    }

    @Override
    protected Map<String, Object> addExtraFields(UserViewDataDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        SavedFilterDto filterDto =  base.getFilter();
        if (filterDto != null) {
            result.put(FILTER_NAME, filterDto.getName());
            result.put(FILTER_RAW, filterDto.getRawFilter());
            try {
                result.put(FILTER_DESCRIPTOR, objectMapper.writeValueAsString(filterDto.getFilterDescriptor()));
            } catch (JsonProcessingException jpe) {
                throw new RuntimeException(jpe);
            }
        }
        else {
            result.put(FILTER_NAME, "");
            result.put(FILTER_RAW, "");
            result.put(FILTER_DESCRIPTOR, "");
        }
        if (base.getContainerId() != null){
            UserViewDataDto container = this.userViewDataService.getById(base.getContainerId());
            result.put(CONTAINER_NAME, container.getName());
        }
        else {
            result.put(CONTAINER_NAME, "");
        }

        return result;
    }


    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.USER_VIEW_DATA);
    }
}
