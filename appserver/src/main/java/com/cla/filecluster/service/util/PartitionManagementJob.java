package com.cla.filecluster.service.util;

import com.cla.filecluster.repository.jpa.PartitionManagement;
import org.slf4j.Logger;
import java.util.concurrent.TimeUnit;

/**
 * Created by: yael
 * Created on: 7/21/2019
 */
public abstract class PartitionManagementJob {

    private static long ONE_DAY_IN_MILLIS = TimeUnit.DAYS.toMillis(1);

    protected void managePartitions(int daysToAddPartition, int daysToDeletePartition, PartitionManagement repository) {

        long daysToAddPartitionInMillis = daysToAddPartition * ONE_DAY_IN_MILLIS;
        long daysToDeletePartitionInMillis = daysToDeletePartition * ONE_DAY_IN_MILLIS;

        getLogger().debug("===> start managePartitions");
        Long lastPartitionDateInMillis = repository.getLastPartitionDate();
        if (lastPartitionDateInMillis < System.currentTimeMillis() + daysToAddPartitionInMillis) {
            while (lastPartitionDateInMillis + daysToAddPartitionInMillis < System.currentTimeMillis()) {
                lastPartitionDateInMillis += daysToAddPartitionInMillis;
            }
            String lastPartitionName = repository.getLastPartitionName();
            long newPartitionId = (Long.parseLong(lastPartitionName.substring(1)) + 1);
            getLogger().debug("===> create managePartitions " + newPartitionId);
            repository.createPartition(newPartitionId, (lastPartitionDateInMillis + daysToAddPartitionInMillis));
        }

        Long firstPartitionDateInMillis = repository.getFirstPartitionDate();
        if (firstPartitionDateInMillis < System.currentTimeMillis() - daysToDeletePartitionInMillis) {
            String firstPartitionName = repository.getFirstPartitionName();
            Long partitionIdToDel = Long.parseLong(firstPartitionName.substring(1));
            getLogger().debug("===> delete managePartitions " + partitionIdToDel);
            repository.deletePartitionForPartitionId(partitionIdToDel);
        }
        getLogger().debug("===> done managePartitions");
    }

    abstract protected Logger getLogger();
}
