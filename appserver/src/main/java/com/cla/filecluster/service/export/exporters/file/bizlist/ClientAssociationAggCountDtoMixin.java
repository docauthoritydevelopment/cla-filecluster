package com.cla.filecluster.service.export.exporters.file.bizlist;

import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class ClientAssociationAggCountDtoMixin {

    @JsonUnwrapped
    SimpleBizListItemDto item;

    @JsonProperty(BizListHeaderFileds.NUM_OF_FILTERED_FILES)
    private Integer count;
}
