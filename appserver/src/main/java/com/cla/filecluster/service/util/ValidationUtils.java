package com.cla.filecluster.service.util;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by: yael
 * Created on: 3/20/2018
 */
public class ValidationUtils {

    public static int LIMIT_NAME_LENGTH = 255;

    public static boolean validateNameLength(String name) {
        if (name != null && name.length() > LIMIT_NAME_LENGTH) {
            return false;
        }
        return true;
    }

    public static String validateStringParameter(String value, String errorMsgEmpty, String errorMsgInvalid) {
        return validateStringParameter(value, false, errorMsgEmpty, errorMsgInvalid);
    }

    public static void validateStringNotEmpty(String value, String errorMsg) {
        if (Strings.isNullOrEmpty(value) || Strings.isNullOrEmpty(value.trim())) {
            throw new BadRequestException(errorMsg, BadRequestType.MISSING_FIELD);
        }
    }

    public static void validateEnumNotNull(Enum value, String errorMsg) {
        if (value == null) {
            throw new BadRequestException(errorMsg, BadRequestType.MISSING_FIELD);
        }
    }

    public static String validateStringParameter(String value, boolean canBeEmpty, String errorMsgEmpty, String errorMsgInvalid) {

        if (Strings.isNullOrEmpty(value)) {
            if (!canBeEmpty) {
                throw new BadRequestException(errorMsgEmpty, BadRequestType.MISSING_FIELD);
            } else {
                return value;
            }
        }

        if (!canBeEmpty && value.trim().length() == 0) {
            throw new BadRequestException(errorMsgEmpty, BadRequestType.MISSING_FIELD);
        }

        CharSequence from = "<";
        CharSequence to = "&lt;";
        value = value.replace(from, to);

        from = ">";
        to = "&gt;";
        value = value.replace(from, to);

        from = "\"";
        to = "";
        value = value.replace(from, to);

        from = "'";
        to = "";
        value = value.replace(from, to);

        if (!canBeEmpty && value.trim().length() == 0) {
            throw new BadRequestException(errorMsgInvalid, BadRequestType.MISSING_FIELD);
        }

        return value.trim();
    }

    public static boolean isNumeric(final CharSequence cs) {
        if (StringUtils.isEmpty(cs)) {
            return false;
        }
        final int sz = cs.length();

        for (int i = 0; i < sz; i++) {
            if (!Character.isDigit(cs.charAt(i)) &&
                    ((cs.charAt(i) != '-' && cs.charAt(i) != '.') ||
                            (i > 0 && cs.charAt(i) == '-'))) {
                return false;
            }
        }
        return true;
    }
}
