package com.cla.filecluster.service.export.exporters.active_scans;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
public interface JobListHeaderFields {
    String NAME = "Name";
    String STATUS = "Status";
    String DETAILS = "Details";
    String ELAPSED_TIME = "Elapsed time";
    String START_TIME = "Start time";
    String PROGRESS = "Progress";
    String RATE = "Rate";
    String ERRORS = "Errors";
}
