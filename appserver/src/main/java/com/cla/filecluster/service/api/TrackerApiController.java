package com.cla.filecluster.service.api;


import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.license.LicenseTracker;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.scan.TrackingKeys;
import com.cla.filecluster.service.license.TrackerAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.cla.common.domain.dto.security.Authority.Const.GENERAL_ADMIN;

/**
 * API for getting license tracked values
 */
@RestController
@RequestMapping("/api/tracker")
@PreAuthorize("isFullyAuthenticated()")
@Permissions(roles = {GENERAL_ADMIN})
public class TrackerApiController {

    @Autowired
    private TrackerAppService trackerAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @RequestMapping(value = "recover/volumeSize", method = RequestMethod.POST)
    public long recoverTotalProcessedDataVolume() {
        eventAuditService.audit(AuditType.TRACKER, "Recover volume count", AuditAction.RECOVER, LicenseTracker.class,
                TrackingKeys.TOTAL_FILE_VOLUME_KEY);
        return trackerAppService.recoverTotalProcessedDataVolume();
    }

    @RequestMapping(value = "count", method = RequestMethod.GET)
    public long getTotalFilesCount() {
        return trackerAppService.getLongCount(TrackingKeys.TOTAL_FILE_COUNT_KEY);
    }

    @RequestMapping(value = "volume", method = RequestMethod.GET)
    public long getTotalFilesVolume() {
        return trackerAppService.getLongCount(TrackingKeys.TOTAL_FILE_VOLUME_KEY);
    }

    @RequestMapping(value = "rootFolders/{rfId}/count", method = RequestMethod.GET)
    public long getTotalFilesCountByRootFolder(@PathVariable long rfId) {
        return trackerAppService.getLongCount(TrackingKeys.rootFolderCountKey(rfId));
    }

    @RequestMapping(value = "rootFolders/{rfId}/volume", method = RequestMethod.GET)
    public long getTotalFilesVolumeByRootFolder(@PathVariable long rfId) {
        return trackerAppService.getLongCount(TrackingKeys.rootFolderVolumeKey(rfId));
    }


    @RequestMapping(value = "mediaTypes/{mediaType}/count", method = RequestMethod.GET)
    public long getTotalFilesCountByMediaType(@PathVariable MediaType mediaType) {
        return trackerAppService.getLongCount(TrackingKeys.mediaCountKey(mediaType));
    }

    @RequestMapping(value = "mediaTypes/{mediaType}/volume", method = RequestMethod.GET)
    public long getTotalFilesVolumeByMediaType(@PathVariable MediaType mediaType) {
        return trackerAppService.getLongCount(TrackingKeys.mediaVolumeKey(mediaType));
    }

    @RequestMapping(value = "grouped/count", method = RequestMethod.GET)
    public long getTotalGroupedFiles() {
        return trackerAppService.getLongCount(TrackingKeys.GROUPED_FILES);
    }

    @RequestMapping(value = "rootFolders/grouped/{rfId}/count", method = RequestMethod.GET)
    public long getTotalGroupedFilesByRootFolder(@PathVariable long rfId) {
        return trackerAppService.getLongCount(TrackingKeys.rootFolderNewGroupedFilesCountKey(rfId));
    }

    @RequestMapping(value = "mediaTypes/grouped/{mediaType}/count", method = RequestMethod.GET)
    public long getTotalGroupedFilesByMediaType(@PathVariable MediaType mediaType) {
        return trackerAppService.getLongCount(TrackingKeys.mediaNewGroupedFilesCountKey(mediaType));
    }
}
