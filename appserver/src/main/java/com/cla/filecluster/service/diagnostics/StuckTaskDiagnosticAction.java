package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.repository.solr.SolrContentMetadataRepository;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class StuckTaskDiagnosticAction implements DiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(StuckTaskDiagnosticAction.class);

    @Autowired
    private EventBus eventBus;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private SolrContentMetadataRepository solrContentMetadataRepository;

    @Autowired
    private JobManagerService jobManagerService;

    @Value("${diagnostics.stuck-tasks.start-after-min:500}")
    private int startAfterMin;

    @Value("${diagnostics.stuck-tasks.event-limit:50}")
    private int cycleEventThreshold;

    @Value("${diagnostics.stuck-task.ingest.enabled:true}")
    private boolean ingestEnabled;

    @Value("${diagnostics.stuck-task.analysis.enabled:true}")
    private boolean analysisEnabled;


    @Override
    public void runDiagnostic(Map<String, String> opData) {
        if (ingestEnabled) {
            handleIngestTasks();
        } else {
            logger.debug("searching for stuck ingest task is disabled");
        }

        if (analysisEnabled) {
            handleAnalysisTasks();
        } else {
            logger.debug("searching for stuck analysis task is disabled");
        }
    }

    private void handleIngestTasks() {
        logger.debug("searching for stuck ingest tasks");

        List<Long> runIds = jobManagerService.getRunsForRunningJobsByType(JobType.SCAN);
        runIds.addAll(jobManagerService.getRunsForRunningJobsByType(JobType.SCAN_FINALIZE));
        runIds.addAll(jobManagerService.getRunsForRunningJobsByType(JobType.INGEST));

        Query query = Query.create()
                .addFacetField(CatFileFieldType.CURRENT_RUN_ID)
                .setRows(0)
                .setFacetLimit(cycleEventThreshold)
                .setFacetMinCount(1);

        if (runIds != null && !runIds.isEmpty()) {
            query.addFilterWithCriteria(
                    Criteria.create(CatFileFieldType.CURRENT_RUN_ID, SolrOperator.NOT_IN, runIds));
        }

        QueryResponse resp = solrFileCatRepository.runQuery(query.build());

        if (resp != null && resp.getFacetFields() != null && resp.getFacetFields().size() > 0) {
            FacetField facetField = resp.getFacetFields().get(0);
            facetField.getValues().forEach(count -> {
                Event event = Event.builder()
                        .withEventType(EventType.STUCK_TASK)
                        .withEntry("RUN ID", count.getName())
                        .withEntry("TYPE", "INGEST")
                        .withEntry("COUNT", count.getCount())
                        .build();
                eventBus.broadcast(Topics.DIAGNOSTICS, event);
            });
        }

        logger.debug("done searching for stuck ingest tasks");
    }

    private void handleAnalysisTasks() {
        logger.debug("searching for stuck analysis tasks");

        List<Long> runIds = jobManagerService.getRunsForRunningJobsByType(JobType.ANALYZE);
        runIds.addAll(jobManagerService.getRunsForRunningJobsByType(JobType.INGEST));
        runIds.addAll(jobManagerService.getRunsForRunningJobsByType(JobType.INGEST_FINALIZE));
        runIds.addAll(jobManagerService.getRunsForRunningJobsByType(JobType.OPTIMIZE_INDEXES));

        Query query = Query.create()
                .addFacetField(CatFileFieldType.CURRENT_RUN_ID)
                .setRows(0)
                .setFacetLimit(cycleEventThreshold)
                .setFacetMinCount(1);

        if (runIds != null && !runIds.isEmpty()) {
            query.addFilterWithCriteria(
                    Criteria.create(CatFileFieldType.CURRENT_RUN_ID, SolrOperator.NOT_IN, runIds));
        }

        QueryResponse resp = solrContentMetadataRepository.runQuery(query.build());

        if (resp != null && resp.getFacetFields() != null && resp.getFacetFields().size() > 0) {
            FacetField facetField = resp.getFacetFields().get(0);
            facetField.getValues().forEach(count -> {
                Event event = Event.builder()
                        .withEventType(EventType.STUCK_TASK)
                        .withEntry("RUN ID", count.getName())
                        .withEntry("TYPE", "ANALYSIS")
                        .withEntry("COUNT", count.getCount())
                        .build();
                eventBus.broadcast(Topics.DIAGNOSTICS, event);
            });
        }

        logger.debug("done searching for stuck analysis tasks");
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.STUCK_TASKS;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        List<Diagnostic> results = new ArrayList<>();
        results.add(addDiagnosticActionParts());
        return results;
    }

    @Override
    public int getStartAfterMin() {
        return startAfterMin;
    }

}
