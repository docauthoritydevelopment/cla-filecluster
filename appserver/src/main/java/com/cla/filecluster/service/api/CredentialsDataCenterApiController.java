package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filetree.RsaKey;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.filecluster.service.files.filetree.EntityCredentialsService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * CRUD for RSA keys for a data center
 * Created by yael on 11/12/2017.
 */
@RestController
@RequestMapping("/api/sys/customerDataCenter/credentials")
public class CredentialsDataCenterApiController {

    @Autowired
    private EntityCredentialsService entityCredentialsService;

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private EventAuditService eventAuditService;

    Logger logger = LoggerFactory.getLogger(CredentialsDataCenterApiController.class);

    @RequestMapping(value="/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.TEXT_PLAIN_VALUE)
    public RsaKey generateDataCenterNewKeys(@PathVariable Long id, @RequestBody(required=false) String oldPrivateKey) {

        CustomerDataCenterDto customerDataCenter = customerDataCenterService.getCustomerDataCenter(id);
        if (customerDataCenter == null) {
            throw new BadRequestException(messageHandler.getMessage("data-center.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        try {
            RsaKey keys = entityCredentialsService.generateDataCenterNewKeys(customerDataCenter, oldPrivateKey);
            eventAuditService.audit(AuditType.CHANGE_KEYS, "Change keys for data center ", AuditAction.UPDATE, CustomerDataCenterDto.class, customerDataCenter.getId(), customerDataCenter);
            return keys;
        } catch (Exception e) {
            logger.error("Failed to generate new keys for data center:" + id,e);
            throw new BadRequestException(messageHandler.getMessage("data-center.keys-generation.failure",
                    Lists.newArrayList(e.getMessage())), BadRequestType.OPERATION_FAILED);
        }
    }
}
