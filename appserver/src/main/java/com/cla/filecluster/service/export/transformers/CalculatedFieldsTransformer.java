package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.export.ExtraFieldsWrapper;
import com.cla.filecluster.service.progress.ProgressService;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.Map;
import java.util.function.BiFunction;

public class CalculatedFieldsTransformer<V> extends ProgressedTransformer<V, ExtraFieldsWrapper<V>> {

    public static <T> CalculatedFieldsTransformer<T> with(String progressId,
                                                          ProgressService.Handler progress,
                                                          Map<String, String> requestParams,
                                                          BiFunction<T, Map<String, String>, ExtraFieldsWrapper<T>> conversionFunction) {
        return new CalculatedFieldsTransformer<>(progressId, progress, requestParams, conversionFunction);
    }

    private final Map<String, String> requestParams;
    private final BiFunction<V, Map<String, String>, ExtraFieldsWrapper<V>> conversionFunction;

    public CalculatedFieldsTransformer(String progressId,
                                       ProgressService.Handler progress,
                                       Map<String, String> requestParams,
                                       BiFunction<V, Map<String, String>, ExtraFieldsWrapper<V>> conversionFunction) {
        super(progressId, progress);
        this.requestParams = requestParams;
        this.conversionFunction = conversionFunction;
    }

    @Override
    protected DataBuffer createDataBuffer(ExtraFieldsWrapper<V> vExtraFieldsWrapper) {
        throw new UnsupportedOperationException("calculatedFieldsTransformer cannot yet be transformed directly to a data output stream");
    }

    @Override
    public Publisher<ExtraFieldsWrapper<V>> apply(Flux<V> vFlux) {
        return vFlux.map((val) -> conversionFunction.apply(val, requestParams));
    }
}
