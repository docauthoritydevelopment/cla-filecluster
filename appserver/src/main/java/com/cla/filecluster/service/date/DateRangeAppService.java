package com.cla.filecluster.service.date;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.date.*;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.date.DateRangePartition;
import com.cla.filecluster.domain.entity.date.DateRangePoint;
import com.cla.filecluster.domain.entity.date.SimpleDateRangeItem;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This service is responsible for the Date Range configuration and reporting.
 * Created by uri on 05/06/2016.
 */
@Service
public class DateRangeAppService {

    public static final String NAME = "NAME";
    private static final String RELATIVE_PERIOD_AMOUNT = "RELATIVE_PERIOD_AMOUNT";
    private static final String RELATIVE_PERIOD_TYPE = "RELATIVE_PERIOD_TYPE";
    private static final String ABSOLUTE_TIME_STAMP = "ABSOLUTE_TIME_STAMP";

    @Autowired
    private DateRangeService dateRangeService;

    private final static Logger logger = LoggerFactory.getLogger(DateRangeAppService.class);

    @Autowired
    private CsvReader csvReader;

    @Value("${daterange.file:./config/defaultDateRange.csv}")
    private String defaultDateRangeFile;

    @Value("${daterange.file:./config/yearlyDateRange.csv}")
    private String yearlyDateRangeFile;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Transactional()
    public void populateDefaultRangePartitions() {
        populateRangePartitionsByCsvFile("default", defaultDateRangeFile, true);
        populateRangePartitionsByCsvFile("yearly", yearlyDateRangeFile, true);
    }


    private void populateRangePartitionsByCsvFile(String dateRangeName, String dateRangeFileName, boolean isSystem) {
        logger.info("populate Default Date Range Partitions");
        DateRangePartition dateRangePartition = dateRangeService.createDateRangePartition(dateRangeName, dateRangeName, true, isSystem);

        if (dateRangeFileName == null || dateRangeFileName.isEmpty()) {
            logger.info("No dateRange file defined for {} - ignored", dateRangeName);
            return;
        }
        logger.info("Load dateRanges from file {}", dateRangeFileName);
        String[] requiredHeaders = new String[]{NAME, RELATIVE_PERIOD_AMOUNT, RELATIVE_PERIOD_TYPE, ABSOLUTE_TIME_STAMP};
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, dateRangeFileName, requiredHeaders);
        int failedRows;
        try {
            List<DateRangeItemDto> dateRangeItemDtos = new ArrayList<>();
            failedRows = csvReader.readAndProcess(dateRangeFileName,
                    row -> createDateRangeItemDto(dateRangeItemDtos, row, headersLocationMap), null, null);
            updateDateRangePartitionItems(dateRangePartition.getId(), dateRangeItemDtos);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load date range - problem in csv file " + e.getMessage(), e);
        }
        if (failedRows > 0) {
            throw new RuntimeException("Failed to load " + failedRows + " date range - problem in csv file");
        }
    }

    private void createDateRangeItemDto(List<DateRangeItemDto> dateRangeItemDtos, RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length == 0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String relativePeriod = fieldsValues[headersLocationMap.get(RELATIVE_PERIOD_AMOUNT)].trim();
        String relativePeriodType = fieldsValues[headersLocationMap.get(RELATIVE_PERIOD_TYPE)].trim();
        String absoluteTimeStamp = fieldsValues[headersLocationMap.get(ABSOLUTE_TIME_STAMP)].trim();

        DateRangePointDto startPoint;

        if (DateRangePointType.UNAVAILABLE.name().equals(absoluteTimeStamp)) {
            dateRangeItemDtos.add(dateRangeService.createUnavailableDateRangeItemDto(name));
            return;
        } else if (DateRangePointType.EVER.name().equals(absoluteTimeStamp)) {
            startPoint = new DateRangePointDto(TimePeriod.YEARS, 1);
            startPoint.setType(DateRangePointType.EVER);
        } else if (DateRangePointType.RELATIVE.name().equals(absoluteTimeStamp)) {
            TimePeriod timePeriod = TimePeriod.valueOf(relativePeriodType);
            startPoint = new DateRangePointDto(timePeriod, Integer.valueOf(relativePeriod));
        } else {
            // ABSOLUTE
            if (DateRangePointType.UNAVAILABLE.name().equals(absoluteTimeStamp)) {
                startPoint = new DateRangePointDto();
            } else {
                startPoint = new DateRangePointDto(Long.valueOf(absoluteTimeStamp.trim()));
            }
        }
        DateRangeItemDto dateRangeItemDto = new DateRangeItemDto(name, startPoint, null);
        dateRangeItemDtos.add(dateRangeItemDto);
    }

    @Transactional(readOnly = true)
    public Page<DateRangePartitionDto> listDateRangePartitions(DataSourceRequest dataSourceRequest, boolean isFull) {
        logger.debug("List dateRange Partitions");
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<DateRangePartition> dateRangeSets = dateRangeService.listDateRangePartitions(pageRequest);
        return convertDateRangePartitions(dateRangeSets, isFull);
    }

    @Transactional(readOnly = true)
    public DateRangePartitionDto listDateRangePartitionItems(long partitionId) {
        logger.debug("List dateRange Partition {} Items", partitionId);

        DateRangePartition dateRangePartition = dateRangeService.getDateRangePartitionWithItems(partitionId);
        return convertDateRangePartition(dateRangePartition, true);
    }

    @Transactional()
    public DateRangePartitionDto createDateRangePartition(DateRangePartitionDto dateRangePartitionDto) {
        logger.info("Create DateRange partition");
        DateRangePartition dateRangePartition = dateRangeService.createDateRangePartition(dateRangePartitionDto.getName(),
                dateRangePartitionDto.getDescription(),
                dateRangePartitionDto.isContinuousRanges());

        return updateDateRangePartitionItems(dateRangePartition.getId(), dateRangePartitionDto.getDateRangeItemDtos());
    }

    @Transactional()
    public DateRangePartitionDto updateDateRangePartition(long partitionId, DateRangePartitionDto dateRangePartitionDto) {
        logger.info("Update DateRange partition {} ", partitionId);
        DateRangePartition dateRangePartition = dateRangeService.updateDateRangePartition(partitionId, dateRangePartitionDto);
        return convertDateRangePartition(dateRangePartition, true);
    }

    @Transactional(readOnly = true)
    public DateRangePartitionDto getDateRangePartition(long partitionId) {
        DateRangePartition dateRangePartition = dateRangeService.getDateRangePartitionWithItems(partitionId);
        return convertDateRangePartition(dateRangePartition, true);
    }

    @Transactional()
    private DateRangePartitionDto updateDateRangePartitionItems(long partitionId, List<DateRangeItemDto> dateRangeItemDtos) {
        logger.info("Update DateRange partition {} items", partitionId);
        dateRangeService.updateDateRangePartitionItems(partitionId, dateRangeItemDtos);
        DateRangePartition dateRangePartition = dateRangeService.getDateRangePartitionWithItems(partitionId);
        return convertDateRangePartition(dateRangePartition, true);
    }

    @Transactional(readOnly = true)
    public FacetPage<AggregationCountItemDTO<DateRangeItemDto>> findDateRangeFileCount(CatFileFieldType catFileFieldType,
                                                                                       long partitionId,
                                                                                       DataSourceRequest dataSourceRequest) {
        logger.debug("List DateRange partition {} File Count on field {}", partitionId, catFileFieldType);
        DateRangePartition dateRangePartition = dateRangeService.getDateRangePartitionWithItems(partitionId);
        List<SimpleDateRangeItem> simpleDateRangeItems = dateRangePartition.getSimpleDateRangeItems();
        List<DateRangeItemDto> dateRangeItemDtos = convertSimpleDateRangeItems(simpleDateRangeItems, false);
        Map<String, Integer> dateRangePartitionCounts = fileCatAppService.getDateRangePartitionCounts(dataSourceRequest, catFileFieldType, dateRangeItemDtos);
        logger.debug("list file facets by Solr field Group_id returned {} results", dateRangePartitionCounts.size());

        Map<String, Integer> dateRangePartitionCountsNoFilter = new HashMap<>();
        if (dataSourceRequest.hasFilter() && dateRangePartitionCounts.size() > 0
                && solrSpecificationFactory.isShouldGetFacetFileNumNoFilter()) {
            dataSourceRequest.clearFilters();
            dateRangePartitionCountsNoFilter =
                    fileCatAppService.getDateRangePartitionCounts(dataSourceRequest, catFileFieldType, dateRangeItemDtos);
        }
        FacetPage<AggregationCountItemDTO<DateRangeItemDto>> facetPage = convertFacetFieldResultToDto(
                dateRangePartitionCounts, dateRangePartitionCountsNoFilter, dateRangeItemDtos, catFileFieldType);
        facetPage.setTotalElements(dateRangeItemDtos.size());
        return facetPage;
    }


    private FacetPage<AggregationCountItemDTO<DateRangeItemDto>>
    convertFacetFieldResultToDto(Map<String, Integer> datePartitionCounts,
                                 Map<String, Integer> datePartitionCountsNoFilter, List<DateRangeItemDto> dateRangeItemDtos,
                                 CatFileFieldType catFileFieldType) {
        List<AggregationCountItemDTO<DateRangeItemDto>> content = new ArrayList<>();
        PageRequest pageRequest = PageRequest.of(0, dateRangeItemDtos.size());
        if (dateRangeItemDtos.size() == 0) {
            return new FacetPage<>(content, pageRequest);
        }
        logger.debug("convert Facet Field result for {} elements to DateRangeItemDto", datePartitionCounts.keySet().size());
        long pageAggCount = 0;
        for (DateRangeItemDto dateRangeItemDto : dateRangeItemDtos) {
            String solrQuery = dateRangeItemDto.getDateFilter();
            String key = catFileFieldType.getSolrName() + ":" + solrQuery;
            Integer count = datePartitionCounts.get(key);
            if (count == null) {
                count = 0;
            }
            Integer countNoFilter = datePartitionCountsNoFilter.get(key);
            AggregationCountItemDTO<DateRangeItemDto> item = new AggregationCountItemDTO<>(dateRangeItemDto, count);
            if (countNoFilter != null && countNoFilter > 0) {
                item.setCount2(countNoFilter);
            }
            content.add(item);
            pageAggCount += count;
        }
        return new FacetPage<>(content, pageRequest, pageAggCount, pageAggCount);
    }

    @Transactional
    public void deleteDateRangePartition(long partitionId) {
        dateRangeService.deleteDateRangePartition(partitionId);
    }

    public static Page<DateRangePartitionDto> convertDateRangePartitions(Page<DateRangePartition> dateRangeSets, boolean isFull) {
        PageRequest pageRequest = PageRequest.of(dateRangeSets.getNumber(), dateRangeSets.getSize());
        List<DateRangePartitionDto> content = new ArrayList<>();
        for (DateRangePartition dateRangePartition : dateRangeSets.getContent()) {
            content.add(convertDateRangePartition(dateRangePartition, isFull));
        }

        return new PageImpl<>(content, pageRequest, dateRangeSets.getTotalElements());
    }

    public static DateRangePartitionDto convertDateRangePartition(DateRangePartition dateRangePartition, boolean includeItems) {
        DateRangePartitionDto dateRangePartitionDto = new DateRangePartitionDto();
        dateRangePartitionDto.setId(dateRangePartition.getId());
        dateRangePartitionDto.setName(dateRangePartition.getName());
        dateRangePartitionDto.setDescription(dateRangePartition.getDescription());
        dateRangePartitionDto.setSystem(dateRangePartition.isSystem());
        dateRangePartitionDto.setContinuousRanges(dateRangePartition.isContinuousRanges());
        if (includeItems) {
            List<SimpleDateRangeItem> simpleDateRangeItems = dateRangePartition.getSimpleDateRangeItems();
            List<DateRangeItemDto> dateRangeItemDtos = convertSimpleDateRangeItems(simpleDateRangeItems, true);
            dateRangePartitionDto.setDateRangeItemDtos(dateRangeItemDtos);
        }
        return dateRangePartitionDto;
    }

    public static List<DateRangeItemDto> convertSimpleDateRangeItems(List<SimpleDateRangeItem> simpleDateRangeItems, boolean includeDummyPoints) {
        List<DateRangeItemDto> result = new ArrayList<>();
        for (SimpleDateRangeItem simpleDateRangeItem : simpleDateRangeItems) {
            if (!includeDummyPoints && simpleDateRangeItem.getEnd() == null) {
                continue;
            }
            DateRangeItemDto dateRangeItemDto = convertSimpleDateRangeItem(simpleDateRangeItem);
            result.add(dateRangeItemDto);
        }
        return result;
    }

    private static DateRangeItemDto convertSimpleDateRangeItem(SimpleDateRangeItem simpleDateRangeItem) {
        DateRangeItemDto result = new DateRangeItemDto();
        result.setId(simpleDateRangeItem.getId());
        result.setName(simpleDateRangeItem.getName());
        DateRangePoint start = simpleDateRangeItem.getStart();
        DateRangePoint end = simpleDateRangeItem.getEnd();
        if (start != null) {
            result.setStart(DateRangeService.convertDateRangePoint(start));
        }
        if (end != null) {
            result.setEnd(DateRangeService.convertDateRangePoint(end));
        }
        return result;
    }
}
