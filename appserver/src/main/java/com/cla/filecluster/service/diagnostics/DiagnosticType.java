package com.cla.filecluster.service.diagnostics;

public enum DiagnosticType {
    CAT_FILE,
    DOC_FOLDER,
    DUPLICATE_FILES,
    STUCK_TASKS,
    DUPLICATE_FOLDERS,
    CONTENT_METADATA,
    DUPLICATE_CONTENTS,
    FILE_GROUP
    ;
}
