package com.cla.filecluster.service.export.exporters.settings.scheduled_groups.mixin;

import com.cla.common.domain.dto.schedule.CrawlerPhaseBehavior;
import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import java.util.List;

import static com.cla.filecluster.service.export.exporters.settings.scheduled_groups.ScheduledGroupsHeaderFields.*;

public class ScheduleGroupDtoMixIn {

    @JsonProperty(NAME)
    private String name;

    private CrawlerPhaseBehavior analyzePhaseBehavior;

    @JsonProperty(ROOT_FOLDERS)
    private List<Long> rootFolderIds;

    @JsonUnwrapped
    private ScheduleConfigDto scheduleConfigDto;

    @JsonProperty(ACTIVE)
    private Boolean active;

    @JsonProperty(DESCRIPTION)
    private String schedulingDescription;

    @JsonProperty(SCHEDULE_CRON)
    private String cronTriggerString;
}
