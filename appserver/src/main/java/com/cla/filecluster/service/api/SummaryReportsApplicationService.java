package com.cla.filecluster.service.api;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.*;
import com.cla.common.utils.FileSizeUnits;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.SummaryReportConfig;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.exceptions.LicenseExpiredException;
import com.cla.filecluster.domain.specification.FilterBuildUtils;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.cla.common.utils.FileSizeUnits.ROUND_UP;
import static java.util.Optional.ofNullable;

@Service
public class SummaryReportsApplicationService {

    private static Logger logger = LoggerFactory.getLogger(SummaryReportsApplicationService.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private BizListService bizListService;

    @Autowired
    private BizListAppService bizListAppService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private Map<String, Object> fileAccessHistogramConfig;

    @Value("${file.crawler.user:}")
    private String fileCrawlerUser;

    @Value("${reports.min-group-size:50}")
    private int minGroupSize;

    @Value("${reports.show-ungrouped-documents:true}")
    private boolean showUngroupedDocuments;

    @Value("${reports.stats.report-deleted:false}")
    private boolean statsReportDeleted;

    @Value("${reports.group-histogram-category-names:Small Groups,Medium Groups,Large Groups}")
    private String[] groupHistogramCategoryNames;

    @Value("${reports.document-file-types:1,2,20}")
    private String[] documentFileTypes;

    private List<FileType> documentFileTypeNames;

    // All files that are no longer in SCANNED state is considered 'processed'. Use 'NOT' fq.
    private final String PROCESSED_FILES_ONLY_FILTER = generateInFq("-processing_state", ClaFileState.SCANNED);
    private final String ANALYZABLE_FILES_FILTER = generateInFq("processing_state",
            ClaFileState.SCANNED, ClaFileState.STARTED_INGESTING, ClaFileState.INGESTED, ClaFileState.STARTED_ANALYSING, ClaFileState.ANALYSED, ClaFileState.ERROR);

    private static String generateInFq(String prefix, Object... values) {
        return Arrays.stream(values).map(Object::toString).collect(Collectors.joining(" ", prefix + ":(", ")"));
    }

    @PostConstruct
    void init() {
        documentFileTypeNames = new ArrayList<>();
        for (String type : documentFileTypes) {
            documentFileTypeNames.add(FileType.valueOf(Integer.parseInt(type)));
        }
    }

    /**
     * Number of scanned files - all scanned files for an account.
     * Number of files found with sensitive data - count distinct cla files with tag types that are "sensitive"
     * Number of groups found with sensitive data - count groups that have files with tag types that are "sensitive" - filter by threshold
     * Default threshold is 0.
     * Total number of distinct owners - count distinct owners from cla_files
     * Total number of distinct ACL groups for read - count distinct acl_items entities for read type
     * Total number of distinct ACL groups for write - count distinct acl_items entities  for read type
     *
     * @return SummaryReportDto
     */
    @Transactional(readOnly = true)
    @AutoAuthenticate
    public SummaryReportDto fetchScanStatistics() {
        logger.debug("fetch Scan Statistics");
        List<CounterReportDto> counterReportDtos;
        try {
            counterReportDtos = new ArrayList<>(getByStatus());
            counterReportDtos.add(countFilesWithSensitiveData());
            counterReportDtos.add(countFileDistinctOwners());
            counterReportDtos.add(countFileDistinctAclRead());
            counterReportDtos.add(countFileDistinctAclWrite());
            addAggregatedProcessedFilesSize(counterReportDtos);
        } catch (LicenseExpiredException e) {
            logger.warn("Login expired issue " + e.getMessage());
            throw e;
        } catch (RuntimeException e) {
            logger.error("Problem with fetchScanStatistics - exception caught " + e.getMessage(), e);
            throw new RuntimeException(messageHandler.getMessage("stat-fetch.failure", Lists.newArrayList(e.getMessage())), e);
        }
        return new SummaryReportDto(counterReportDtos);
    }

    @Transactional(readOnly = true)
    @AutoAuthenticate(replace = false)
    public SummaryReportDto fetchUserDocumentStatistics() {
        List<CounterReportDto> counterReportDtos = new ArrayList<>();
        try {
            Long totalDocuments = getNonDeletedTotalNumberOfFiles(false, true, false);
            counterReportDtos.add(new CounterReportDto("Total documents", totalDocuments));

            Long documentsWithDocTypes = getNonDeletedTotalNumberOfFiles(true, true, false);
            counterReportDtos.add(new CounterReportDto("Documents with Doc Types", documentsWithDocTypes));

            counterReportDtos.add(new CounterReportDto("Documents with no Doc Types", totalDocuments - documentsWithDocTypes));

            Long noneDocuments = getNonDeletedTotalNumberOfFiles(false, false, false);
            counterReportDtos.add(new CounterReportDto("Non documents", noneDocuments));

        } catch (Exception e) {
            logger.error("Problem with fetchUserDocumentStatistics - exception caught " + e.getMessage(), e);
            throw new RuntimeException(messageHandler.getMessage("stat-fetch.failure", Lists.newArrayList(e.getMessage())), e);
        }
        return new SummaryReportDto(counterReportDtos);
    }

    @Transactional(readOnly = true)
    @AutoAuthenticate(replace = false)
    public SummaryReportDto fetchDocumentStatistics() {
        List<CounterReportDto> counterReportDtos = new ArrayList<>();
        try {
            Pair<Long, Long> groupFiles = groupsService.findNumOfFilesInGroups(minGroupSize);
            counterReportDtos.add(new CounterReportDto("Groups", groupFiles.getKey()));
            counterReportDtos.add(new CounterReportDto("Documents in groups", groupFiles.getValue()));

            Long totalDocuments = getNonDeletedTotalNumberOfFiles(false, true, false);

            Long unGroupedDocuments = -1L;
            if (showUngroupedDocuments && groupFiles.getValue() != null) {
                unGroupedDocuments = totalDocuments - groupFiles.getValue();
            }
            counterReportDtos.add(new CounterReportDto("Ungrouped documents", unGroupedDocuments));

            Long noneDocuments = getNonDeletedTotalNumberOfFiles(false, false, false);
            counterReportDtos.add(new CounterReportDto("Non documents", noneDocuments));

            Long documentsWithDocTypes = getNonDeletedTotalNumberOfFiles(true, true, false);
            counterReportDtos.add(new CounterReportDto("Documents with Doc Types", documentsWithDocTypes));

            counterReportDtos.add(new CounterReportDto("Documents with no Doc Types", totalDocuments - documentsWithDocTypes));

            int groupsWithDocTypes = groupFiles.getKey() > 0 ? groupsService.findNumOfGroupsWithDocTypes(minGroupSize) : 0;
            counterReportDtos.add(new CounterReportDto("Groups with Doc Types", groupsWithDocTypes));

            counterReportDtos.add(new CounterReportDto("Groups with no Doc Types", groupFiles.getKey() - groupsWithDocTypes));

        } catch (Exception e) {
            logger.error("Problem with fetchDocumentStatistics - exception caught " + e.getMessage(), e);
            throw new RuntimeException(messageHandler.getMessage("stat-fetch.failure", Lists.newArrayList(e.getMessage())), e);
        }
        return new SummaryReportDto(counterReportDtos);
    }


    private List<CounterReportDto> getByStatus() {
        HashMap<ClaFileState, Long> statusCount = getFileNumByStatus();
        List<CounterReportDto> counterReportDtos = new ArrayList<>();

        if (statusCount != null && !statusCount.isEmpty()) {
            Long sum = statusCount.entrySet().stream().mapToLong(Map.Entry::getValue).sum() - statusCount.getOrDefault(ClaFileState.DELETED, 0L);
            counterReportDtos.add(new CounterReportDto(ReportItems.SCANNED_FILES, sum));
            counterReportDtos.add(new CounterReportDto(ReportItems.ANALYZED_FILES, statusCount.getOrDefault(ClaFileState.ANALYSED, 0L)));
            counterReportDtos.add(new CounterReportDto(ReportItems.ERROR_FILES, statusCount.getOrDefault(ClaFileState.ERROR, 0L)));
            counterReportDtos.add(new CounterReportDto(ReportItems.INGESTED_FILES, statusCount.getOrDefault(ClaFileState.INGESTED, 0L)));
            counterReportDtos.add(new CounterReportDto(ReportItems.SKIPPED_FILES, statusCount.getOrDefault(ClaFileState.SKIP, 0L)));
        } else {
            counterReportDtos.add(new CounterReportDto(ReportItems.SCANNED_FILES, 0L));
            counterReportDtos.add(new CounterReportDto(ReportItems.ANALYZED_FILES, 0L));
            counterReportDtos.add(new CounterReportDto(ReportItems.ERROR_FILES, 0L));
            counterReportDtos.add(new CounterReportDto(ReportItems.INGESTED_FILES, 0L));
            counterReportDtos.add(new CounterReportDto(ReportItems.SKIPPED_FILES, 0L));
        }
        return counterReportDtos;
    }

    private CounterReportDto countFileDistinctAclRead() {
        return getCounterReport(ReportItems.DISTINCT_ACL_READ, CatFileFieldType.ACL_READ, null);

    }

    private CounterReportDto countFileDistinctAclWrite() {
        return getCounterReport(ReportItems.DISTINCT_ACL_WRITE, CatFileFieldType.ACL_WRITE, null);
    }

    private CounterReportDto countFileDistinctOwners() {
        return getCounterReport(ReportItems.DISTINCT_OWNERS, CatFileFieldType.OWNER, null);
    }

    private CounterReportDto getCounterReport(String label, CatFileFieldType facetField, FilterDescriptor baseFilter) {
        long count;
        if (facetField != null) {
            count = runSolrFacetQueryReturnCount(facetField, baseFilter);
        } else {
            SolrFacetQueryResponse resp = fileCatService.runSolrFacetQuery(baseFilter, null);
            count = resp.getNumFound();
        }
        return new CounterReportDto(label, count);
    }

    private long runSolrFacetQueryReturnCount(CatFileFieldType facetField, FilterDescriptor baseFilter) {
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setPageSize(1);
        dataSourceRequest.setFilter(baseFilter);
        return fileCatAppService.getTotalFacetCount(dataSourceRequest, facetField);
    }

    public void addAggregatedProcessedFilesSize(final List<CounterReportDto> counterReportDtos) {
        Pair<Double, Long> res = createAggregatedFileSize(PROCESSED_FILES_ONLY_FILTER);
        Double aggSize = ofNullable(res.getKey()).orElse(0d);
        Long aggCount = res.getValue();
        long aggSizeMb = FileSizeUnits.BYTE.toMegabytes(aggSize, ROUND_UP);
        CounterReportDto sizeDto = new CounterReportDto(ReportItems.AGG_SIZE, aggSizeMb);
        counterReportDtos.add(sizeDto);
        CounterReportDto counterDto = new CounterReportDto(ReportItems.PROCESSED_FILES_COUNT, ofNullable(aggCount).orElse(0L));
        counterReportDtos.add(counterDto);
        logger.debug("Counted aggregated processed files metrics: {} , {}", counterDto, sizeDto);

        res = createAggregatedFileSize(createFileTypeMeaningfulDataSourceRequest(), ANALYZABLE_FILES_FILTER);
        aggSize = ofNullable(res.getKey()).orElse(0d);
        aggCount = res.getValue();
        aggSizeMb = FileSizeUnits.BYTE.toMegabytes(aggSize, ROUND_UP);
        sizeDto = new CounterReportDto(ReportItems.AGG_SIZE_DOCUMENTS, aggSizeMb);
        counterReportDtos.add(sizeDto);
        counterDto = new CounterReportDto(ReportItems.PROCESSED_FILES_COUNT_DOCUMENTS, ofNullable(aggCount).orElse(0L));
        counterReportDtos.add(counterDto);
        logger.debug("Counted aggregated analyzable files metrics: {} , {}", counterDto, sizeDto);

        Long totalDocumentsWithErrors = getNonDeletedTotalNumberOfFiles(false, true, true);

        counterReportDtos.add(new CounterReportDto(ReportItems.ERROR_DOCUMENTS, totalDocumentsWithErrors));

        // Get stats (count, agg. size) for all files maked as deleted
        res = createAggregatedFileSize(true);
        aggSize = ofNullable(res.getKey()).orElse(0d);
        aggCount = res.getValue();
        aggSizeMb = FileSizeUnits.BYTE.toMegabytes(aggSize, ROUND_UP);
        sizeDto = new CounterReportDto(ReportItems.AGG_SIZE_DELETED, aggSizeMb);
        counterReportDtos.add(sizeDto);
        counterDto = new CounterReportDto(ReportItems.AGG_COUNT_DELETED, ofNullable(aggCount).orElse(0L));
        logger.debug("Counted aggregated deleted files metrics: {} , {}", counterDto, sizeDto);
        // Turned off by default for Castor
        if (statsReportDeleted) {
            counterReportDtos.add(counterDto);
        }
    }

    private CounterReportDto countFilesWithSensitiveData() {
        List<FileTag> fileTags = fileTagService.getSensitiveFileTags();
        logger.debug("{} files tags are sensitive", fileTags.size());
        if (fileTags.size() == 0) {
            return new CounterReportDto(ReportItems.SENSITIVE_DATA_FILES, 0);
        }
        FilterDescriptor baseFilter = FilterBuildUtils.createFileTagsFilterDescriptor(fileTags);
        return getCounterReport(ReportItems.SENSITIVE_DATA_FILES, null, baseFilter);
    }

    /**
     * Folders to Scan - list of root folders
     * <p>
     * Folder scan account - the user that scanned the files
     *
     * @return SummaryReportDto
     */
    @Transactional(readOnly = true)
    public SummaryReportDto fetchScanConfiguration() {
        logger.debug("Fetch Scan configuration");
        List<StringListReportDto> stringListReportDtos = new ArrayList<>();
        stringListReportDtos.add(findFoldersToScan());

        List<StringReportDto> stringReportDtos = new ArrayList<>();
        stringReportDtos.add(findFoldersScanAccount());

        SummaryReportDto summaryReportDto = new SummaryReportDto();
        summaryReportDto.setStringListReportDtos(stringListReportDtos);
        summaryReportDto.setStringReportDtos(stringReportDtos);
        return summaryReportDto;
    }

    /**
     * Entity Management (hashes) -  <List of entity-lists with count and path to imported file.
     *
     * @return SummaryReportDto
     */
    @Transactional(readOnly = true)
    public SummaryReportDto fetchScanCriteria() {
        logger.debug("Fetch Scan criteria");
        MultiValueListDto entityListDetails = findEntityListDetails();

        SummaryReportDto summaryReportDto = new SummaryReportDto();
        summaryReportDto.setMultiValueListDto(entityListDetails);
        return summaryReportDto;
    }

    /**
     * <# groups per group size range>
     * Ranges: <10, 10-50, 50-99, 100-199, 200-499, 500-999, >1000
     *
     * @return isFiles=false - returns groups count per bucket
     * isFiles=true - returns total number of files per each bucket
     */
    @Transactional(readOnly = true)
    public MultiValueListDto createGroupSizeHistogram(boolean isFiles) {
        logger.debug("Create Group size histogram");
        List<CounterReportDto> data = new ArrayList<>();

        Map<Integer, Long> groupSizeHistogram;
        if (isFiles) {
            groupSizeHistogram = groupsService.createGroupSizeFilesHistogram();
        } else {
            groupSizeHistogram = groupsService.createGroupSizeCountHistogram();
        }

        groupSizeHistogram.forEach((index, size) -> {
            String label = groupsService.getGroupSizeLabels()[index];
            data.add(new CounterReportDto(groupHistogramCategoryNames[index], label, size));
        });

        return new MultiValueListDto(ReportItems.GROUP_SIZE, data, MultiValueListType.HISTOGRAM);
    }


    // Same as above but with dataSourceRequest (filter) support
    @Transactional(readOnly = true)
    @AutoAuthenticate(replace = false)
    public MultiValueListDto createFileAccessDateHistogram(DataSourceRequest dataSourceRequest) {
        List<CounterReportDto> data = getCounterReportsByAccessData(dataSourceRequest);
        return new MultiValueListDto(ReportItems.LAST_MODIFY_DATE, data, MultiValueListType.HISTOGRAM);
    }


    @Transactional(readOnly = true)
    @AutoAuthenticate(replace = false)
    public MultiValueListDto createFileTypesHistogram(DataSourceRequest dataSourceRequest) {
        List<CounterReportDto> data = getCounterReports(dataSourceRequest,
                CatFileFieldType.TYPE, count -> {
                    Integer fileType = Integer.valueOf(count.getName());
                    return FileType.valueOf(fileType).name();
                });
        return new MultiValueListDto(ReportItems.FILE_TYPE, data, MultiValueListType.HISTOGRAM);
    }

    @Transactional(readOnly = true)
    public MultiValueListDto createFileExtensionsHistogram(DataSourceRequest dataSourceRequest) {
        List<CounterReportDto> data = getCounterReports(
                dataSourceRequest, CatFileFieldType.EXTENSION, FacetField.Count::getName);
        return new MultiValueListDto(ReportItems.FILE_TYPE, data, MultiValueListType.HISTOGRAM);
    }

    private List<CounterReportDto> getCounterReports(DataSourceRequest dataSourceRequest, CatFileFieldType field,
                                                    Function<FacetField.Count, String> labelResolver) {
        List<CounterReportDto> data = new ArrayList<>();
        SolrFacetQueryResponse resp = fileCatService.runSolrFacetQuery(dataSourceRequest, field);
        FacetField facetField = resp.getFirstResult();
        for (FacetField.Count count : facetField.getValues()) {
            data.add(new CounterReportDto(labelResolver.apply(count), count.getCount()));
        }
        return data;
    }

    public List<CounterReportDto> createExcelStats() {
        DataSourceRequest dsr = DataSourceRequest.build(null);
        FilterDescriptor filter = new FilterDescriptor();
        List<FilterDescriptor> filters = new ArrayList<>();
        filters.add(new FilterDescriptor(FileDtoFieldConverter.DtoFields.deleted.getModelField(), "false", KendoFilterConstants.EQ));
        filters.add(new FilterDescriptor(FileDtoFieldConverter.DtoFields.size.getModelField(), 0, KendoFilterConstants.GT));
        filters.add(new FilterDescriptor(FileDtoFieldConverter.DtoFields.type.getModelField(), FileType.EXCEL, KendoFilterConstants.EQ));
        filter.setFilters(filters);
        dsr.setFilter(filter);
        dsr.setBaseFilterValue(FileType.EXCEL.name());
        return createExcelStats(dsr);
    }

    private List<CounterReportDto> createExcelStats(DataSourceRequest dataSourceRequest) {
        logger.debug("Create Excel Sheets Stats with filterSpec");
        ArrayList<SolrFacetQueryJson> facetJsonObjectList = new ArrayList<>();

        SolrFacetQueryJson stFacet = new SolrFacetQueryJson("st");
        stFacet.setType(FacetType.SIMPLE);
        stFacet.setFunc(FacetFunction.SUM);
        stFacet.setField(CatFileFieldType.ITEM_COUNT_3);
        facetJsonObjectList.add(stFacet);

        SolrFacetQueryJson smFacet = new SolrFacetQueryJson("sm");
        smFacet.setType(FacetType.SIMPLE);
        smFacet.setFunc(FacetFunction.MAX);
        smFacet.setField(CatFileFieldType.ITEM_COUNT_3);
        facetJsonObjectList.add(smFacet);

        SolrFacetQueryJson ctFacet = new SolrFacetQueryJson("ct");
        ctFacet.setType(FacetType.SIMPLE);
        ctFacet.setFunc(FacetFunction.SUM);
        ctFacet.setField(CatFileFieldType.ITEM_COUNT_1);
        facetJsonObjectList.add(ctFacet);

        SolrFacetQueryJson cmFacet = new SolrFacetQueryJson("cm");
        cmFacet.setType(FacetType.SIMPLE);
        cmFacet.setFunc(FacetFunction.MAX);
        cmFacet.setField(CatFileFieldType.ITEM_COUNT_1);
        facetJsonObjectList.add(cmFacet);

        final SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = fileCatService.runJsonFacetQuery(dataSourceRequest,
                facetJsonObjectList);

        SolrFacetJsonResponseBucket solrFacetJsonResponseBucket = solrFacetQueryJsonResponse.topLevelBucket();
        List<CounterReportDto> result = new ArrayList<>();
        long count = solrFacetJsonResponseBucket.getLongCountNumber();
        if (count > 0) {
            long st = (long) (0.5 + solrFacetJsonResponseBucket.getDoubleValueByKey("st"));
            long sm = (long) (0.5 + solrFacetJsonResponseBucket.getDoubleValueByKey("sm"));
            long ct = (long) (0.5 + solrFacetJsonResponseBucket.getDoubleValueByKey("ct"));
            long cm = (long) (0.5 + solrFacetJsonResponseBucket.getDoubleValueByKey("cm"));
            result.add(new CounterReportDto(ReportItems.PROCESSED_EXCEL_FILES_COUNT, count));
            result.add(new CounterReportDto(ReportItems.EXCEL_SHEETS_AVG, (count == 0) ? 0l : (long) (1d * st / count + 0.5d)));
            result.add(new CounterReportDto(ReportItems.EXCEL_SHEETS_MAX, sm));
            result.add(new CounterReportDto(ReportItems.EXCEL_CELLS_AVG, (count == 0) ? 0 : (long) (1d * ct / count + 0.5d)));
            result.add(new CounterReportDto(ReportItems.EXCEL_CELLS_MAX, cm));
        }
        return result;
    }

    private static final List<FileType> processedDocumentTypeFilter = Arrays.asList(FileType.WORD, FileType.EXCEL, FileType.PDF);

    private DataSourceRequest createFileTypeMeaningfulDataSourceRequest() {
        DataSourceRequest dsr = DataSourceRequest.build(null);
        FilterDescriptor hasContent = new FilterDescriptor(FileDtoFieldConverter.DtoFields.contentId.getModelField(),
                "*", KendoFilterConstants.EQ);
        FilterDescriptor processedDocument = new FilterDescriptor(FileDtoFieldConverter.DtoFields.type.getModelField(),
                processedDocumentTypeFilter, KendoFilterConstants.EQ);
        FilterDescriptor joinFilter = new FilterDescriptor();
        joinFilter.setLogic("and");
        joinFilter.getFilters().add(hasContent);
        joinFilter.getFilters().add(processedDocument);
        dsr.setFilter(joinFilter);
        dsr.setBaseFilterValue("AnalysisPotential");
        return dsr;
    }

    public Pair<Double, Long> createAggregatedFileSize(String... additionalFilters) {
        return createAggregatedFileSize(false, additionalFilters);
    }

    public Pair<Double, Long> createAggregatedFileSize(Boolean deleted, String... additionalFilters) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(null);
        dataSourceRequest.setQueryWithoutDeleted(!deleted);
        return createAggregatedFileSize(dataSourceRequest, additionalFilters);
    }

    private Pair<Double, Long> createAggregatedFileSize(DataSourceRequest dataSourceRequest, String... additionalFilters) {
        try {
            logger.debug("Create File Aggregated Size with filterSpec");
            SolrFacetQueryJson jsonFacet = new SolrFacetQueryJson("x");
            jsonFacet.setType(FacetType.SIMPLE);
            jsonFacet.setFunc(FacetFunction.SUM);
            jsonFacet.setField(CatFileFieldType.SIZE);
            SolrFacetQueryJsonResponse response = fileCatService.runJsonFacetQuery(dataSourceRequest,
                    Arrays.asList(jsonFacet), additionalFilters);
            final SolrFacetJsonResponseBucket topLevelBucket = response.topLevelBucket();
            return topLevelBucket.getLongCountNumber() == 0 ? Pair.of(0D, 0L) :
                    Pair.of(topLevelBucket.getDoubleValueByKey("x"), topLevelBucket.getLongCountNumber());
        } catch (Exception e) {
            logger.warn("Exception during runSolrJsonFacetQuery({},{},{}). {}", dataSourceRequest,
                    CatFileFieldType.SIZE, "{x:'sum(%1)'}", e, e);
            return Pair.of(0d, 0L);
        }
    }

    private HashMap<ClaFileState, Long> getFileNumByStatus() {
        Query query = Query.create()
                .setRows(0)
                .addFacetField(CatFileFieldType.PROCESSING_STATE);

        solrSpecificationFactory.addFilterDeletedFiles(query);

        HashMap<ClaFileState, Long> statusCount = new HashMap<>();
        QueryResponse queryResponse = fileCatService.runQuery(query.build());
        if (queryResponse.getFacetFields() != null && !queryResponse.getFacetFields().isEmpty()) {
            FacetField field = queryResponse.getFacetFields().get(0);
            List<FacetField.Count> values = field.getValues();
            for (FacetField.Count c : values) {
                statusCount.put(ClaFileState.valueOf(c.getName()), c.getCount());
            }
        }
        return statusCount;
    }

    private Long getNonDeletedTotalNumberOfFiles(boolean withDoctypes, boolean inclusiveFileTypes, boolean onlyErrors) {
        Query solrQuery = Query.create();
        solrSpecificationFactory.addFilterDeletedFiles(solrQuery);
        if (withDoctypes) {
            solrQuery.addFilterWithCriteria(CatFileFieldType.SCOPED_TAGS, SolrOperator.EQ, "dt.g.*");
        }
        if (inclusiveFileTypes) {
            solrQuery.addFilterWithCriteria(CatFileFieldType.TYPE, SolrOperator.IN, Arrays.asList(documentFileTypes));
        } else {
            solrQuery.addFilterWithCriteria(CatFileFieldType.TYPE, SolrOperator.NOT_IN, Arrays.asList(documentFileTypes));
        }
        if (onlyErrors) {
            solrQuery.addFilterWithCriteria(
                    Criteria.create(CatFileFieldType.PROCESSING_STATE, SolrOperator.EQ, ClaFileState.ERROR));
        }
        solrQuery.setRows(0);
        solrSpecificationFactory.addFilterDeletedFiles(solrQuery);
        QueryResponse result = fileCatService.runQuery(solrQuery.build());
        return result.getResults().getNumFound();
    }

    private List<CounterReportDto> getCounterReportsByAccessData(DataSourceRequest dataSourceRequest) {
        List<CounterReportDto> data = new LinkedList<>();
        CatFileFieldType solrField = (CatFileFieldType) fileAccessHistogramConfig.get(SummaryReportConfig.SOLR_FIELD);
        List<String> queryValues = solrSpecificationFactory.createTimeHistogramQueryValues();
        SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetSpecification(dataSourceRequest, CatFileFieldType.LAST_MODIFIED, queryValues);
        SolrQueryResponse solrQueryResponse = fileCatService.runQuery(solrSpecification, null);
        QueryResponse queryResponse = solrQueryResponse.getQueryResponse();
        Map<String, Integer> facetQuery = queryResponse.getFacetQuery();
        logger.debug("list file facets by Solr field {} returned {} results", solrField, facetQuery.size());
        for (Map.Entry<String, Integer> stringIntegerEntry : facetQuery.entrySet()) {
            String label = stringIntegerEntry.getKey();
            if (stringIntegerEntry.getValue() > 0 || !solrSpecificationFactory.isTimeHistogramValuesToDrop(label)) {
                data.add(new CounterReportDto(label, stringIntegerEntry.getValue()));
            }
        }
        return data;
    }

    private MultiValueListDto findEntityListDetails() {
        List<BizList> bizLists = bizListService.find(true);
        List<CounterReportDto> collect = bizLists.stream().map(this::convertEntityList).collect(Collectors.toList());
        return new MultiValueListDto(ReportItems.ENTITY_MANAGEMENT_DETAILS, collect, MultiValueListType.LIST);
    }

    private CounterReportDto convertEntityList(BizList bizList) {
        if (bizList.getBizListSource() != null) {
            return new CounterReportDto(bizList.getName(), bizList.getBizListSource(), bizList.getBizListSchema().getBizListItemRows().size());
        }
        long count = bizListAppService.getBizListItemCounts(bizList.getId());
        return new CounterReportDto(bizList.getName(), count);
    }

    private StringReportDto findFoldersScanAccount() {
        if (StringUtils.isNotEmpty(fileCrawlerUser)) {
            return new StringReportDto(ReportItems.SCAN_USER_ACCOUNT, fileCrawlerUser);
        }
        return new StringReportDto(ReportItems.SCAN_USER_ACCOUNT, System.getProperty("user.name"));
    }

    private StringListReportDto findFoldersToScan() {
        List<RootFolder> rootFolders = docStoreService.getRootFolders();
        List<String> collect = rootFolders.stream().map(RootFolder::getPath).collect(Collectors.toList());
        logger.debug("Find Folders to Scan found {} folders", collect.size());
        return new StringListReportDto(ReportItems.SCANNED_ROOT_FOLDERS, collect);
    }


}
