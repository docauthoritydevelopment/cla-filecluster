package com.cla.filecluster.service.security;

import java.util.*;
import java.util.stream.Collectors;

import com.cla.common.domain.dto.security.SystemRoleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cla.filecluster.domain.entity.security.Role;
import com.cla.filecluster.repository.jpa.security.RoleRepository;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository roleRepository;


    @Transactional(readOnly = true)
	public List<Role> getRolesWithAuthorities(){
		return roleRepository.findWithAuthorities();
	}


    @Transactional(readOnly = true)
    public List<Role> getRolesByNames(ArrayList<String> rolesList) {
        if (rolesList.size() == 0) {
            return new ArrayList<>();
        }
        return roleRepository.findRolesByName(rolesList);
    }

    public List<Role> getRoles(Set<SystemRoleDto> roles) {
        if (roles == null || roles.size() == 0) {
            return new ArrayList<>();
        }
        List<String> rolesList = roles.stream().map(f -> f.getName()).collect(Collectors.toList());
        return roleRepository.findRolesByName(rolesList);
    }

    @Transactional(readOnly = true)
    public Role getRoleByName(String name) {
        return roleRepository.findRoleByName(name);
    }

    @Transactional(readOnly = true)
    public Role getSystemRole(Integer id) {
        return roleRepository.getOne(id);
    }

    public static Set<SystemRoleDto> convertRolesToSet(Collection<Role> roles) {
        Set<SystemRoleDto> result = new HashSet<>();
        if (roles != null) {
            for (Role role : roles) {
                result.add(convertRole(role));
            }
        }
        return result;
    }

    public static List<SystemRoleDto> convertRoles(Collection<Role> roles) {
        List<SystemRoleDto> result = new ArrayList<>();
        if (roles != null) {
            for (Role role : roles) {
                result.add(convertRole(role));
            }
        }
        return result;
    }

    public static SystemRoleDto convertRole(Role role) {
        SystemRoleDto systemRoleDto = new SystemRoleDto();
        systemRoleDto.setId(role.getId());
        systemRoleDto.setName(role.getName());
        systemRoleDto.setDisplayName(role.getDisplayName());
        systemRoleDto.setDescription(role.getDescription());
        systemRoleDto.getAuthorities().addAll(role.getAuthorities());
        return systemRoleDto;
    }

    public static Role convertRole(SystemRoleDto roleDto) {
        Role role = new Role();
        role.setId(roleDto.getId());
        role.setName(roleDto.getName());
        role.setDisplayName(roleDto.getDisplayName());
        role.setDescription(roleDto.getDescription());
        role.getAuthorities().addAll(roleDto.getAuthorities());
        return role;
    }
}
