package com.cla.filecluster.service.extractor.entity;

import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * creates query for token search in content
 *
 * Created by: yael
 * Created on: 4/24/2018
 */
@Component
public class ContentSearchTextService {

    private static final Logger logger = LoggerFactory.getLogger(ContentSearchTextService.class);

    @Autowired
    private ExtractTokenizer extractTokenizer;

    //	final private static Pattern hashedContentSearchTokenPattern = Pattern.compile("[ \"\\{\\+\\(][^\\s~\\}\\{\"\\+\\(\\)]+");	// replace lower case tokens or tokens surrounded with "" or {}
    final private static Pattern hashedContentSearchTokenPattern = Pattern.compile("[^\\s~\\}\\{\"\\(\\)\\+\\-!][^\\s~\\}\\{\"\\(\\)]+");	// replace lower case tokens or tokens surrounded with "" or {}
    final private static Pattern contentSearchCleannerPattern = Pattern.compile("[\\.,*&\\^#\\?]+");		// remove special characters

    public String convertContentSearchText(String contentSearchText, boolean addFieldName) {
        String hashedContentSearchQuery = getHashedContentSearchQuery(contentSearchText, addFieldName);
        // TODO: Following line should be commented for security purposes
        logger.debug("Content filter: '{}' converted into '{}'", contentSearchText, hashedContentSearchQuery);
        return hashedContentSearchQuery;
    }

    public  String getHashedContentSearchQuery(final String query, boolean addFieldName) {
        String queryText = convertSearchQuery(query);
        if (Strings.isNullOrEmpty(queryText) || Strings.isNullOrEmpty(queryText.trim())) {
            return null;
        }
        return (addFieldName?"content:(":"(")+queryText+")";
    }

    public String convertSearchQuery(String query) {
        String queryText = new String(query);
        queryText = contentSearchCleannerPattern.matcher(query).replaceAll(" ");
        final Map<String, String> replacements = new HashMap<>();
        final Matcher matcher = hashedContentSearchTokenPattern.matcher(" "+queryText);
        while (matcher.find()) {
//			final String replaceStr = new String(matcher.group());
//			final String token = replaceStr.substring(1,replaceStr.length());  // trim first char
            final String token = new String(matcher.group());
            replacements.put(token,extractTokenizer.getHash(token));
        }
        // Don't replace query logical operators
        replacements.remove("AND");
        replacements.remove("OR");
        replacements.remove("NOT");
        // make sure to replace strings from longest to shortest to avoid mis-replacement of abc within abcd.
        final List<String> replacementList = replacements.keySet().stream()
                .sorted((s1, s2)->Integer.compare(s2.length(), s1.length()))
                .collect(Collectors.toList());
        for (final String repl : replacementList) {
            queryText = queryText.replace(repl, replacements.get(repl));
        }
        return queryText;
    }
}
