package com.cla.filecluster.service.crawler;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.utils.PersistableSafeCache;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.jpa.crawler.CrawlRunRepository;
import com.cla.filecluster.repository.solr.SolrFacetQueryResponse;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.cache.CacheLoader;
import org.apache.solr.client.solrj.response.FacetField;
import org.hibernate.StaleStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.PageRequest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by: yael
 * Created on: 5/3/2018
 */
@Service
public class FileCrawlerExecDetailsServiceTxUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileCrawlerExecDetailsServiceTxUtil.class);

    private static final int RETRY_MAX_OPT_LOCK_RUN = 2;

    @Autowired
    private CrawlRunRepository crawlRunRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private DBTemplateUtils dBTemplateUtils;


    private PersistableSafeCache<Long, CrawlRunDetailsDto> runCache;

    private final ExecutorService crawlRunEnrichExecutor = Executors.newCachedThreadPool();

    @SuppressWarnings("unused")
    @PostConstruct
    private void init() {
        runCache = PersistableSafeCache.Builder
                .create(Long.class, CrawlRunDetailsDto.class)
                .maximumSize(200)
                .setLoader(new RunCacheLoader())
                .setPersister((key, value) -> updateRun(value))
                .build();
    }

    /**
     * Periodically persist run statistical data from cache.
     * Not transactional, since the transaction is manual and happens within the persister.
     * This is important, because we need to unlock the locks after the transaction is committed and not before
     */
    @Scheduled(initialDelayString = "${job-manager.persist-jobs-initial-delay-millis:5000}",
            fixedRateString = "${job-manager.persist-jobs-frequency-millis:25000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void persistRunCache() {
        runCache.persistAll();
    }

    public CrawlRunDetailsDto getRunDetails(long runId) {
        return runCache.getForRead(runId);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Retryable(maxAttempts = RETRY_MAX_OPT_LOCK_RUN, value = {StaleStateException.class, ObjectOptimisticLockingFailureException.class})
    public void updateCrawlRunGracefulStop(Long runId) {
        CrawlRun crawlRun = crawlRunRepository.getOne(runId);
        crawlRun.setIngestFiles(false);
        crawlRun.setAnalyzeFiles(false);
        crawlRun.setRunStatus(RunStatus.RUNNING);
        crawlRun.setStateString("Stopping Gracefully");
        crawlRun.setGracefulStop(true);
        crawlRun.setStopTime(new Date(System.currentTimeMillis()));
        crawlRunRepository.save(crawlRun);

        CrawlRunDetailsDto runCached = runCache.lockForUpdate(runId);
        runCached.setIngestFiles(false);
        runCached.setAnalyzeFiles(false);
        runCached.setRunOutcomeState(RunStatus.RUNNING);
        runCached.setGracefulStop(true);
        runCached.setStopTime(crawlRun.getStopTime());
        runCached.setStateString("Stopping Gracefully");
        runCache.unlockUpdate(runId);

        logger.debug("Updated crawl run {} to state to {} for graceful stop", runId, crawlRun.getRunStatus());
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Retryable(maxAttempts = RETRY_MAX_OPT_LOCK_RUN, value = {StaleStateException.class, ObjectOptimisticLockingFailureException.class})
    public void updateCrawlRunState(Long crawlRunId, RunStatus runStatus, PauseReason pauseReason, OpStateService opStateService) {
        crawlRunRepository.findById(crawlRunId)
                .ifPresent(crawlRun -> {
                    logger.debug("Updating crawl run {} state to {}.", crawlRun.getId(), runStatus);
                    crawlRun.setRunStatus(runStatus);
                    if (!runStatus.isActive()) {
                        crawlRun.setStopTime(new Date(System.currentTimeMillis()));
                        opStateService.informRunFinished();
                        if (!crawlRun.getHasChildRuns()) {
                            enrichCrawlRunAsync(crawlRun.getId());
                        }
                    }
                    crawlRun.setPauseReason(pauseReason);

                    if (runStatus.equals(RunStatus.PAUSED) && crawlRun.getTimeFirstPause() == null) {
                        crawlRun.setTimeFirstPause(System.currentTimeMillis());
                    }

                    CrawlRun result = crawlRunRepository.save(crawlRun);
                    CrawlRunDetailsDto runCached = runCache.lockForUpdate(result.getId());
                    runCached.setRunOutcomeState(runStatus);
                    runCached.setStopTime(crawlRun.getStopTime());
                    runCache.unlockUpdate(result.getId());

                    logger.debug("Updated crawl run {} to state to {}", result.getId(), result.getRunStatus());
                });
    }

    private void enrichCrawlRunAsync(Long runId) {
        crawlRunEnrichExecutor.execute(() -> AuthenticationHelper.doWithAuthentication(
                AutoAuthenticate.ADMIN, false, Executors.callable(() -> enrichCrawlRun(runId)), userService));
    }

    private void enrichCrawlRun(Long runId) {
        try {
            enrichRunDetailsFromSolr(runId);
            long scanErrorCount = scanErrorsService.countScanErrorsByRunId(runId);
            long ingestErrorsCount = ingestErrorService.countIngestErrorsByRunId(runId);
            updateScanIngestErrorsCount(runId, scanErrorCount, ingestErrorsCount);
            logger.debug("Enriched crawl run: #{} with scanErrorCount: {} and ingestErrorCount: {}",
                    runId, scanErrorCount, ingestErrorsCount);
        } catch (Exception e) {
            logger.error("Failed to enrich crawl run #{}", runId, e);
        }
    }

    private void enrichRunDetailsFromSolr(long runId) {
        CrawlRunDetailsDto runDto = runCache.lockForUpdate(runId);
        try {
            RootFolderDto rootFolderDto = runDto.getRootFolderDto();
            if (rootFolderDto == null || rootFolderDto.getId() == null) {
                return;
            }
            Long rootFolderId = rootFolderDto.getId();
            FilterDescriptor baseFilter =
                    new FilterDescriptor(FileDtoFieldConverter.DtoFields.rootFolderId.name(), rootFolderId, "eq");
            SolrSpecification solrSpecification =
                    solrSpecificationFactory.buildSolrFacetSpecification(baseFilter, CatFileFieldType.TYPE);
            solrSpecification.setMinCount(1);
            solrSpecification.setAdditionalFilter(CatFileFieldType.DELETED.filter("false"));
            SolrFacetQueryResponse solrFacetQueryResponse =
                    fileCatService.runFacetQuery(solrSpecification, PageRequest.of(0, 10));
            List<FacetField.Count> values = solrFacetQueryResponse.getFacetFields().get(0).getValues();

            // in case we need to remove a type - set to zero before starting
            runDto.setProcessedWordFiles(0L);
            runDto.setProcessedExcelFiles(0L);
            runDto.setProcessedPdfFiles(0L);
            runDto.setProcessedOtherFiles(0L);

            for (FacetField.Count value : values) {
                Integer typeInt = Integer.valueOf(value.getName());
                FileType fileType = FileType.valueOf(typeInt);
                switch (fileType) {
                    case WORD:
                        runDto.setProcessedWordFiles(value.getCount());
                        break;
                    case EXCEL:
                        runDto.setProcessedExcelFiles(value.getCount());
                        break;
                    case PDF:
                        runDto.setProcessedPdfFiles(value.getCount());
                        break;
                    default:
                        Long countOther = runDto.getProcessedOtherFiles();
                        if (countOther == null) {
                            countOther = 0L;
                        }
                        runDto.setProcessedOtherFiles(countOther + value.getCount());
                }
            }
            runDto.setTotalProcessedFiles(solrFacetQueryResponse.getNumFound());
        } catch (Exception e) {
            logger.error("Failed to enrich run details from solr {}", runDto);
        } finally {
            runCache.unlockUpdate(runId);
        }
    }

    private void updateScanIngestErrorsCount(long runId, long scanErrorCount, long ingestErrorsCount) {
        safeUpdateCounter(runId, (CrawlRunDetailsDto runDto) -> {
            runDto.setScanErrors(scanErrorCount);
            runDto.setScanProcessingErrors(ingestErrorsCount);
        });
    }

    void safeUpdateCounter(long runId, SettingUpdater updater) {
        try {
            updater.execute(runCache.lockForUpdate(runId));
        } catch (Exception e) {
            logger.error("Failed to update setting.", e);
        } finally {
            runCache.unlockUpdate(runId);
        }
    }

    interface SettingUpdater {
        void execute(CrawlRunDetailsDto runDto);
    }

    private void updateRun(final CrawlRunDetailsDto runDto) {
        Runnable r = () -> {
            CrawlRun persistentRun = crawlRunRepository.getOne(runDto.getId());
            FileCrawlerExecutionDetailsService.mergeCrawlRun(persistentRun, runDto);
            crawlRunRepository.save(persistentRun);
        };
        dBTemplateUtils.doInTransaction(r);
    }

    private class RunCacheLoader extends CacheLoader<Long, CrawlRunDetailsDto> {
        private TransactionTemplate tmpl;

        private RunCacheLoader() {
            tmpl = new TransactionTemplate(transactionManager);
            tmpl.setReadOnly(true);
        }

        @Override
        public CrawlRunDetailsDto load(Long key) {
            tmpl.setReadOnly(true);
            return tmpl.execute(status -> {
                CrawlRun runEntity = crawlRunRepository.getOne(key);
                return FileCrawlerExecutionDetailsService.convertCrawlRun(runEntity);
            });
        }
    }

    @PreDestroy
    public void destroy() {
        crawlRunEnrichExecutor.shutdown();
    }
}
