package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class DocTypeAggCountDtoMixin {

    @JsonUnwrapped
    DocTypeDto item;

    @JsonProperty(DocTypeHeaderFields.NUM_OF_DIRECT_FILERED)
    private Integer count;
}
