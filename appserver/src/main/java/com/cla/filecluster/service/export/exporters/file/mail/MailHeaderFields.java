package com.cla.filecluster.service.export.exporters.file.mail;

public interface MailHeaderFields {
    String ADDRESS = "Address";
    String DOMAIN = "Domain";
    String NUM_OF_FILES = "Number of files";
}
