package com.cla.filecluster.service.export.exporters.settings.bizlist;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public interface BizListHeaderFields {

    String NAME = "Name";
    String ALIASES = "ALIASES";
    String STATE = "State";
    String BUSINESS_ID = "Business Id";

    String DESCRIPTION = "Description";
    String LAST_FULL_RUN = "Last full run";
    String TYPE = "Type";
    String STATUS = "Status";
    String ACTIVE = "Active";

}
