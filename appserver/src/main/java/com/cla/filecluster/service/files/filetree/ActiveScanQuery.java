package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.crawler.*;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.file.DirectoryExistStatus;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

/**
 * This class serves the new active scan page, the query is build custom to serve it
 * including a count option and a sort option
 * and filtering by specific fields
 * for the root folders requested we will also return all jobs of the current run
 * otherwise we'll just return the active job where relevant
 *
 * Created by: yael
 * Created on: 10/21/2018
 */
@Service
public class ActiveScanQuery {

    private Logger logger = LoggerFactory.getLogger(ActiveScanQuery.class);

    @Autowired
    private DBTemplateUtils dBTemplateUtils;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private SharePermissionService sharePermissionService;

    @Value("${scanner.scanTaskDepth:0}")
    private int scanTaskDepth;

    public enum ActiveScanQueryType {
        FULL,
        COUNT,
        STATE
    }

    private String buildQuery(Map<String, String> params, long offset, int pageSize, ActiveScanQueryType type) {

        String query = "select ";
                if (type.equals(ActiveScanQueryType.COUNT)) {
                    query += "count(distinct rf.id) ";
                } else if (type.equals(ActiveScanQueryType.STATE)) {
                    query += "cr.run_status, cr.pause_reason, count(*) AS cnt ";
                } else {
                    query += "rf.id, rf.nick_name, rf.media_type, rf.is_accessible, rf.is_first_scan, rf.reingest_time_stamp,rf.last_run_id, rf.path, rf.folders_count, " +
                            "rf.customer_data_center_id, rf.real_path, rf.rescan_active, rf.description, rf.file_types, rf.ingest_file_types, rf.scan_task_depth, " +
                            "rf.media_connection_details, rf.store_location, rf.store_purpose, rf.store_security, rf.mailbox_group, rf.department_id, " +
                            "j.id job_id, j.type, j.state, j.completion_stauts, j.start_run_time, j.last_stop_time, j.pause_duration, " +
                            " j.task_retries_count, j.current_task_count, j.estimated_task_count, j.job_state_string, j.failed_count, " +
                            "rfs.schedule_group_id, cr.start_time, cr.stop_time, rf.failure_getting_share_permission, " +
                            "cr.run_status, cr.state_string, cr.pause_reason, cr.total_processed_files, cr.processed_excel_files, " +
                            "cr.processed_pdf_files, cr.processed_word_files, cr.processed_other_files, cr.graceful_stop ";
                }

         query += "from root_folder rf " +
        "left join crawl_runs cr on cr.id = rf.last_run_id " +
        "left join root_folder_schedule rfs on rfs.root_folder_id = rf.id " +
        "left join jm_jobs j on j.run_context = rf.last_run_id and cr.run_status in ('RUNNING','PAUSED') " +
                 "and j.state in ('PENDING', 'WAITING','IN_PROGRESS','PAUSED','READY') " +
        "where rf.deleted = false ";

        if (params.containsKey("scheduleGroup") && !Strings.isNullOrEmpty(params.get("scheduleGroup"))) {
            query += " and rfs.schedule_group_id in (" + params.get("scheduleGroup") + ") ";
        }

        if (params.containsKey("departmentId") && !Strings.isNullOrEmpty(params.get("departmentId"))) {
            String innerQuery = "rf.department_id in (" + params.get("departmentId") + ") ";
            Set<String> deptIds = Sets.newHashSet(params.get("departmentId").split(","));
            if (deptIds.contains("-1")) {
                query += " and (" + innerQuery + " or rf.department_id is null) ";
            } else {
                query += " and " + innerQuery;
            }
        }

        if (params.containsKey("dataCenterId") && !Strings.isNullOrEmpty(params.get("dataCenterId"))) {
            query += " and rf.customer_data_center_id in (" + params.get("dataCenterId") + ") ";
        }

        if (params.containsKey("rootfolderIds") && !Strings.isNullOrEmpty(params.get("rootfolderIds"))) {
            query += " and rf.id in (" + params.get("rootfolderIds") + ") ";
        }

        if (params.containsKey("mediaType") && !Strings.isNullOrEmpty(params.get("mediaType"))) {
            query += " and rf.media_type in ("+ params.get("mediaType") + ") ";
        }

        if (params.containsKey("runStatus") && !Strings.isNullOrEmpty(params.get("runStatus"))) {
            List<String> values = new ArrayList<>(Arrays.asList(params.get("runStatus").split(",")));
            String cond = "";
            boolean middle = false;
            if (values.contains(RunStatus.PAUSED.name()) && values.contains(RootFolderSummaryInfo.SUSPENDED_STATUS)) {
                values.remove(RootFolderSummaryInfo.SUSPENDED_STATUS);
            } else if (values.contains(RunStatus.PAUSED.name()) && !values.contains(RootFolderSummaryInfo.SUSPENDED_STATUS)) {
                cond = "(cr.run_status = '" + RunStatus.PAUSED.name() + "' and cr.pause_reason = 'USER_INITIATED')";
                middle = true;
                values.remove(RunStatus.PAUSED.name());
            } else if (!values.contains(RunStatus.PAUSED.name()) && values.contains(RootFolderSummaryInfo.SUSPENDED_STATUS)) {
                cond = "(cr.run_status = '" + RunStatus.PAUSED.name() + "' and cr.pause_reason != 'USER_INITIATED')";
                middle = true;
                values.remove(RootFolderSummaryInfo.SUSPENDED_STATUS);
            }

            if (values.contains(RootFolderSummaryInfo.OK_STATUS)) {
                values.remove(RootFolderSummaryInfo.OK_STATUS);
                values.add(RunStatus.FINISHED_SUCCESSFULLY.name());
            }

            if (values.contains(RootFolderSummaryInfo.NEW_STATUS)) {
                String part = "rf.last_run_id is null";
                cond = cond.isEmpty() ? part : addInnerCond(cond, part, middle);
                middle = true;
                values.remove(RootFolderSummaryInfo.NEW_STATUS);
            }

            if (!values.isEmpty()) {
                StringBuilder states = new StringBuilder();
                values.forEach(s -> states.append("'").append(s).append("',"));
                String part = "cr.run_status in (" + states.toString().substring(0, states.toString().length()-1) + ")";
                cond = cond.isEmpty() ? part : addInnerCond(cond, part, middle);
            }

            if (!cond.isEmpty()) {
                query += " and (" + cond + ")";
            }
        }

        if (params.containsKey("runPhase") && !Strings.isNullOrEmpty(params.get("runPhase"))) {
            List<String> phases = new ArrayList<>(Arrays.asList(params.get("runPhase").split(",")));
            StringBuilder phaseStrings = new StringBuilder();
            phases.forEach(s -> phaseStrings.append("'").append(s).append("',"));
            query += " and j.type in ("+ phaseStrings.toString().substring(0, phaseStrings.toString().length()-1) + ") ";
        }

        if (params.containsKey("namingSearchTerm") && !Strings.isNullOrEmpty(params.get("namingSearchTerm"))) {
            String namingSearchTerm = params.get("namingSearchTerm").toLowerCase();

            // escape slashes correctly for sql query
            namingSearchTerm = namingSearchTerm.replaceAll("\\\\", "\\\\\\\\\\\\\\\\");

            query += " and (lower(rf.real_path) like '%"+namingSearchTerm+"%' or lower(rf.path) " +
                    "like '%"+namingSearchTerm+"%' or lower(rf.nick_name) like '%"+namingSearchTerm+
                    "%' or lower(rf.mailbox_group) like '%"+namingSearchTerm+
                    "%' or lower(rf.description) like '%"+namingSearchTerm+"%')";
        }

        if (params.containsKey("accessibility") && !Strings.isNullOrEmpty(params.get("accessibility"))) {
            query += " and rf.is_accessible in ("+ params.get("accessibility") + ") ";
        }

        if (params.containsKey("scanningOrder") && !Strings.isNullOrEmpty(params.get("scanningOrder"))) {
            List<String> values = Arrays.asList(params.get("scanningOrder").split(","));
            if (values.size() == 1) {
                if (values.get(0).equals("FIRST_SCAN")) {
                    query += " and rf.is_first_scan = 1 ";
                } else {
                    query += " and rf.is_first_scan = 0 ";
                }
            }
        }

        if (type.equals(ActiveScanQueryType.FULL)) {
            query += " order by ";
            switch (params.getOrDefault("sortField", "")) {
                case "NICKNAME":
                    query += "rf.nick_name ";
                    break;
                case "RUN_STATUS":
                    query += "cr.run_status ";
                    break;
                case "FILE_NUM":
                    query += "cr.total_processed_files ";
                    break;
                case "MEDIA_TYPE":
                    query += "rf.media_type ";
                    break;
                default:
                    query += "rf.id ";
            }
            String dir = params.getOrDefault("sortDesc", "ASC");
            query += dir;

            query += " limit " + offset + ", " + pageSize;
        } else if (type.equals(ActiveScanQueryType.STATE)) {
            query += " GROUP BY cr.run_status, cr.pause_reason ";
        }

        return query;
    }

    private String addInnerCond(String query, String cond, boolean middle) {
        return query + (middle ? " or (" : " and ( ") + cond + ")";
    }

    public int count(Map<String, String> params) {
        String statement = buildQuery(params, 0, 0, ActiveScanQueryType.COUNT);
        List<Integer> result = new ArrayList<>();

        try {
            dBTemplateUtils.doInTransaction(jdbcTemplate -> {
                SqlRowSet res = jdbcTemplate.queryForRowSet(statement);
                if (res.next()) {
                    result.add(res.getInt(1));
                }
            });
            return result.size() == 0 ? 0 : result.get(0);
        } catch (Exception e) {
            logger.error("error getting count from statement {}", statement, e);
            throw e;
        }
    }

    RootFoldersAggregatedSummaryInfo getByState(Map<String, String> params) {

        String statement = buildQuery(params, 0, 0, ActiveScanQueryType.STATE);
        List<Object[]> result = new ArrayList<>();

        try {
            dBTemplateUtils.doInTransaction(jdbcTemplate -> {
                SqlRowSet res = jdbcTemplate.queryForRowSet(statement);
                while (res.next()) {
                    Object[] line = new Object[3];
                    line[0] = res.getString(1);
                    line[1] = res.getString(2);
                    line[2] = res.getInt(3);
                    result.add(line);
                }
            });
            return JobManagerAppService.createRootFoldersAggregatedSummaryInfo(result);
        } catch (Exception e) {
            logger.error("error getting count from statement {}", statement, e);
            throw e;
        }
    }

    @SuppressWarnings("ConstantConditions")
    public List<RootFolderRunSummaryInfo> run(Map<String, String> params, PageRequest pageRequest) {

        String statement = buildQuery(params, pageRequest.getOffset(), pageRequest.getPageSize(), ActiveScanQueryType.FULL);
        Map<Long, RootFolderRunSummaryInfo> result = new LinkedHashMap<>();

        List<String> values = params.get("viewedRootFolders") != null ? Arrays.asList(params.get("viewedRootFolders").split(",")) : new ArrayList<>();
        Set<Long> viewedRootFolders = values.stream().filter(v -> !Strings.isNullOrEmpty(v)).map(Long::parseLong).collect(Collectors.toSet());

        try {
            dBTemplateUtils.doInTransaction(jdbcTemplate -> {
                SqlRowSet res = jdbcTemplate.queryForRowSet(statement);

                ConcurrentMap<Long, PhaseRunningRateDetails> phaseRunningRateDetailsMap = jobManagerService.getPhaseRunningRateDetailsMap();

                while (res.next()) {
                    RootFolderRunSummaryInfo a = new RootFolderRunSummaryInfo();
                    RootFolderDto rootFolderDto = new RootFolderDto();
                    rootFolderDto.setId(res.getLong("id"));
                    rootFolderDto.setNickName(res.getString("nick_name"));

                    rootFolderDto.setReingestTimeStampMs(res.getLong("reingest_time_stamp"));
                    rootFolderDto.setFirstScan(res.getBoolean("is_first_scan"));
                    rootFolderDto.setMediaType(MediaType.valueOf(res.getInt("media_type")));
                    rootFolderDto.setPath(res.getString("path"));
                    rootFolderDto.setRealPath(res.getString("real_path"));
                    rootFolderDto.setNumberOfFoldersFound(res.getInt("folders_count"));
                    rootFolderDto.setRescanActive(res.getBoolean("rescan_active"));
                    rootFolderDto.setDescription(res.getString("description"));
                    rootFolderDto.setMailboxGroup(res.getString("mailbox_group"));
                    rootFolderDto.setRootFolderSharePermissions(
                            sharePermissionService.getRootFolderSharePermissions(rootFolderDto.getId(), rootFolderDto.getMediaType()));
                    rootFolderDto.setShareFolderPermissionError(res.getBoolean("failure_getting_share_permission"));
                    rootFolderDto.setStoreLocation(StoreLocation.valueOf(res.getInt("store_location")));
                    rootFolderDto.setStoreSecurity(StoreSecurity.valueOf(res.getInt("store_security")));
                    rootFolderDto.setStorePurpose(StorePurpose.valueOf(res.getInt("store_purpose")));

                    if (res.getObject("is_accessible") != null) {
                        rootFolderDto.setIsAccessible(DirectoryExistStatus.valueOf(res.getInt("is_accessible")));
                    } else {
                        rootFolderDto.setIsAccessible(DirectoryExistStatus.UNKNOWN);
                    }

                    if (res.getObject("media_connection_details") != null) {
                        Long mediaConnId = res.getLong("media_connection_details");
                        MediaConnectionDetailsDto dto = mediaConnectionDetailsService.getMediaConnectionDetailsById(mediaConnId, true);
                        if (dto != null) {
                            rootFolderDto.setMediaConnectionDetailsId(dto.getId());
                            rootFolderDto.setMediaConnectionName(dto.getName());
                        }
                    }

                    if (res.getObject("department_id") != null) {
                        Long departmentId = res.getLong("department_id");
                        DepartmentDto department = departmentService.getByIdCached(departmentId);
                        rootFolderDto.setDepartmentId(departmentId);
                        rootFolderDto.setDepartmentName(department.getName());
                        a.getRootFolderSummaryInfo().setDepartmentDto(department);
                    }

                    if (res.getObject("scan_task_depth") == null) {
                        rootFolderDto.setScanTaskDepth(scanTaskDepth);
                    } else {
                        rootFolderDto.setScanTaskDepth(res.getInt("scan_task_depth"));
                    }

                    String fileTypes = res.getString("file_types");
                    if (!Strings.isNullOrEmpty(fileTypes)) {
                        rootFolderDto.setFileTypes(splitToFileTypes(fileTypes));
                    }

                    fileTypes = res.getString("ingest_file_types");
                    if (!Strings.isNullOrEmpty(fileTypes)) {
                        rootFolderDto.setIngestFileTypes(splitToFileTypes(fileTypes));
                    }

                    CustomerDataCenterDto cdc = new CustomerDataCenterDto();
                    cdc.setId(res.getLong("customer_data_center_id"));
                    rootFolderDto.setCustomerDataCenterDto(cdc);
                    a.getRootFolderSummaryInfo().setRootFolderDto(rootFolderDto);

                    Long scheduleGroupId = res.getLong("schedule_group_id");
                    if (scheduleGroupId > 0) {
                        ScheduleGroupDto scheduleGroupDto = scheduleGroupService.getGroupById(scheduleGroupId);
                        if (scheduleGroupDto != null) {
                            scheduleGroupDto.setNextPlannedWakeup(scheduleAppService.getNextScheduleTime(scheduleGroupId));
                            a.getRootFolderSummaryInfo().addScheduleGroupDto(scheduleGroupDto);
                        }
                    }

                    CrawlRunDetailsDto run = new CrawlRunDetailsDto();
                    if (res.getObject("run_status") != null) {
                        String status = res.getString("run_status");
                        if (!status.isEmpty()) {
                            run.setRunOutcomeState(RunStatus.valueOf(status));
                        }
                    }

                    if (res.getObject("state_string") != null) {
                        run.setStateString(res.getString("state_string"));
                    }
                    if (res.getObject("pause_reason") != null) {
                        run.setPauseReason(PauseReason.valueOf(res.getString("pause_reason")));
                    }
                    if (res.getObject("graceful_stop") != null) {
                        run.setGracefulStop(res.getBoolean("graceful_stop"));
                    }
                    if (res.getObject("total_processed_files") != null) {
                        run.setTotalProcessedFiles(res.getLong("total_processed_files"));
                    }
                    if (res.getObject("processed_excel_files") != null) {
                        run.setProcessedExcelFiles(res.getLong("processed_excel_files"));
                    }
                    if (res.getObject("processed_pdf_files") != null) {
                        run.setProcessedPdfFiles(res.getLong("processed_pdf_files"));
                    }
                    if (res.getObject("processed_word_files") != null) {
                        run.setProcessedWordFiles(res.getLong("processed_word_files"));
                    }
                    if (res.getObject("processed_other_files") != null) {
                        run.setProcessedOtherFiles(res.getLong("processed_other_files"));
                    }
                    if (res.getObject("start_time") != null) {
                        run.setScanStartTime(res.getDate("start_time").getTime());
                    }
                    if (res.getObject("stop_time") != null) {
                        run.setScanEndTime(res.getDate("stop_time").getTime());
                    }
                    if (res.getObject("last_run_id") != null) {
                        rootFolderDto.setLastRunId(res.getLong("last_run_id"));
                        run.setId(res.getLong("last_run_id"));
                        a.getRootFolderSummaryInfo().setCrawlRunDetailsDto(run);
                    }
                    result.put(rootFolderDto.getId(), a);

                    if (viewedRootFolders.contains(rootFolderDto.getId()) && run.getId() != null) {
                        List<SimpleJobDto> runJobs = jobManagerService.getRunJobs(run.getId());
                        runJobs.forEach(job -> {
                            if (job.getType().equals(JobType.SCAN)) {
                                Long unprocessed = jobManagerService.getExtraScanTaskCountForJob(job.getId());
                                job.setCurrentTaskCount(job.getCurrentTaskCount() + unprocessed.intValue());
                            }
                        });
                        List<PhaseDetailsDto> phaseDetailsDtos = JobManagerService.convertSimpleJobDto(runJobs, jobManagerService.getPhaseRunningRateDetailsMap());
                        a.setJobs(phaseDetailsDtos);
                    }

                    SimpleJobDto job = new SimpleJobDto();
                    if (res.getObject("type") != null) {
                        job.setType(JobType.valueOf(res.getString("type")));
                    }
                    if (res.getObject("state") != null) {
                        job.setState(JobState.valueOf(res.getString("state")));
                    }
                    if (res.getObject("completion_stauts") != null) {
                        job.setCompletionStatus(JobCompletionStatus.valueOf(res.getString("completion_stauts")));
                    }
                    if (res.getObject("job_state_string") != null) {
                        job.setJobStateString(res.getString("job_state_string"));
                    }
                    if (res.getObject("start_run_time") != null) {
                        job.setStartRunTime(res.getLong("start_run_time"));
                    }
                    if (res.getObject("last_stop_time") != null) {
                        job.setLastStopTime(res.getLong("last_stop_time"));
                    }
                    if (res.getObject("task_retries_count") != null) {
                        job.setTotalTaskRetriesCount(res.getLong("task_retries_count"));
                    }
                    if (res.getObject("current_task_count") != null) {
                        job.setCurrentTaskCount(res.getInt("current_task_count"));
                    }
                    if (res.getObject("estimated_task_count") != null) {
                        job.setEstimatedTaskCount(res.getInt("estimated_task_count"));
                    }
                    if (res.getObject("failed_count") != null) {
                        job.setFailedCount(res.getInt("failed_count"));
                    }
                    if (res.getObject("pause_duration") != null) {
                        job.setPauseDuration(res.getLong("pause_duration"));
                    }
                    if (res.getObject("job_id") != null) {
                        job.setId(res.getLong("job_id"));
                        if (job.getType().equals(JobType.SCAN)) {
                            Long unprocessed = jobManagerService.getExtraScanTaskCountForJob(job.getId());
                            job.setCurrentTaskCount(job.getCurrentTaskCount() + unprocessed.intValue());
                        }
                        a.setCurrentJob(JobManagerService.convertRunJobToPhaseDetailsDto(job, phaseRunningRateDetailsMap.get(job.getId())));
                    }
                }
            });
            return new ArrayList<>(result.values());
        } catch (Exception e) {
            logger.error("error getting data from statement {}", statement, e);
            throw e;
        }
    }

    @SuppressWarnings("UnstableApiUsage")
    private FileType[] splitToFileTypes(String fileTypesStr){
        return Splitter.on(",")
                .trimResults()
                .splitToList(fileTypesStr).stream()
                .filter(Objects::nonNull)
                .filter(type -> type.length() > 0)
                .map(type -> FileType.valueOf(type.toUpperCase()))
                .toArray(FileType[]::new);
    }
}
