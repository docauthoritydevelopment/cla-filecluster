package com.cla.filecluster.service.export.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class ToSecondsSerializer extends JsonSerializer<Long> {

    @Override
    public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(Optional.ofNullable(value)
                .map(val -> String.valueOf(TimeUnit.MILLISECONDS.toSeconds(val)))
                .orElse(""));
    }
}
