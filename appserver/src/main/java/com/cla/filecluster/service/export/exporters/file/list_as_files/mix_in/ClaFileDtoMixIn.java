package com.cla.filecluster.service.export.exporters.file.list_as_files.mix_in;

import com.cla.connector.domain.dto.file.FileType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public abstract class ClaFileDtoMixIn {

    @JsonProperty("Path")
    String fileName;

    @JsonProperty("File name")
    String baseName;

    @JsonProperty("Type")
    FileType type;

    @JsonProperty("File size")
    Long fsFileSize;

    @JsonProperty("Modified date")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    Date fsLastModified;

    @JsonProperty("Accessed date")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    Date fsLastAccess;

    @JsonProperty("Creation date")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    Date creationDate;

    @JsonProperty("Group name")
    String groupName;

    @JsonProperty("Owner")
    String owner;
}
