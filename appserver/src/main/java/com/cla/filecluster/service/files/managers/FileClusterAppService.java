package com.cla.filecluster.service.files.managers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.file.FolderType;
import com.cla.common.domain.dto.filetree.MailboxDto;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.PauseReason;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.connector.domain.dto.messages.ScanTaskParameters;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.exceptions.LicenseExpiredException;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.jobmanager.service.TaskManagerService;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.MailboxCache;
import com.cla.filecluster.service.files.MailboxService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.license.LicenseService;
import com.cla.filecluster.util.KeyValueDbUtil;
import com.cla.kvdb.KeyValueDatabaseRepository;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Configuration
public class FileClusterAppService {
    private static final Logger logger = LoggerFactory.getLogger(FileClusterAppService.class);

    private static final int MAX_RECORDS_TO_PROCESS = 10000;

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private KeyValueDatabaseRepository kvdbRepo;

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private JobManagerService jobManagerService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private MailboxService mailboxService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private TaskManagerService taskManagerService;

    @Autowired
    private MailboxCache mailboxCache;

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${mailbox.file-path:./config/}")
    private String mailboxFilePath;

    @AutoAuthenticate
    public void doStartScanSync(CrawlRun run, RootFolderDto rootFolderDto) {
        SimpleJobDto startingJob = null;

        try {
            Long runId = run.getId();
            if (!runId.equals(rootFolderDto.getLastRunId())) {
                logger.error("Failed to start scan. Supplied run is not attached to rootfolder. Run ID {} RootFolder ID {}", runId, rootFolderDto.getId());
                throw new RuntimeException(messageHandler.getMessage("run.not.root-folder.related"));
            }

            List<SimpleJobDto> runJobs = jobManagerService.getRunJobs(runId);

            // look for a SCAN job to start from. If none, look for EXTRACT job
            startingJob = runJobs.
                    stream().
                    filter(j -> j.getType() == JobType.SCAN).
                    findAny().
                    orElseGet(() ->
                            runJobs.
                                    stream().
                                    filter(j -> j.getType() == JobType.EXTRACT).
                                    findAny().
                                    orElseThrow(() -> new IllegalStateException(messageHandler.getMessage("run.no-start-jobs", Lists.newArrayList(runId)))));

            licenseService.validateMaxFilesCount(countScannedFiles());

            if (!rootFolderDto.getMediaType().hasPath()) {
                prepareMailboxListForRootFolder(rootFolderDto);
            }

            logger.info("Start Media Processor Run {} on root folder {}", runId, rootFolderDto);
            dispatchStartingJob(startingJob, rootFolderDto);
        } catch (LicenseExpiredException e) {
            if (startingJob != null) {
                jobManagerService.updateJobState(startingJob.getId(), JobState.DONE, JobCompletionStatus.CANCELLED, messageHandler.getMessage("license.max-file"), true);
            }
            if (run.getId() != null) {
                jobManagerAppService.endRun(run.getId(), PauseReason.LICENSE_EXPIRED);
            }
            throw e;
        } catch (Exception e) {
            if (startingJob != null) {
                jobManagerService.updateJobState(startingJob.getId(), JobState.DONE, JobCompletionStatus.ERROR, e.getMessage(), true);
            }
            if (run.getId() != null) {
                jobManagerAppService.endRun(run.getId(), PauseReason.SYSTEM_ERROR);
            }
            throw e;
        }
    }

    private long countScannedFiles() {
        Query query = Query.create();
        solrSpecificationFactory.addFilterDeletedFiles(query);
        return fileCatService.getTotalCount(query, CatFileFieldType.DELETED, "false");
    }

    private void prepareMailboxListForRootFolder(RootFolderDto rootFolderDto) {
        mailboxCache.invalidate(rootFolderDto.getId());
        List<String> mailboxUpn = new ArrayList<>();
        if (!Strings.isNullOrEmpty(rootFolderDto.getMailboxGroup())) {
            try {
                IOUtils.readLines(new FileInputStream(mailboxFilePath + rootFolderDto.getMailboxGroup() + ".txt"), "UTF8")
                        .stream()
                        .filter(l -> !(l.isEmpty() || l.startsWith("#")))
                        .forEach(l -> mailboxUpn.add(l.trim()));
            } catch (Exception e) {
                logger.error("cant read mailbox file for root folder {}", rootFolderDto.getId(), e);
                throw new RuntimeException(messageHandler.getMessage("run.mailbox-group-file.failure", Lists.newArrayList(rootFolderDto.getId())));
            }
        } else {
            logger.error("mailbox group field for root folder {} is not populated", rootFolderDto.getId());
            throw new RuntimeException(messageHandler.getMessage("run.mailbox-group.missing", Lists.newArrayList(rootFolderDto.getId())));
        }

        // look for current mailboxes in the file records
        List<MailboxDto> rootFolderMailboxes = mailboxService.getByRootFolderId(rootFolderDto.getId());
        List<MailboxDto> rootFolderMailboxesToDelete = new ArrayList<>();
        for (MailboxDto mailbox : rootFolderMailboxes) {
            if (mailboxUpn.contains(mailbox.getUpn())) { // mailbox exists both in file and db
                mailboxUpn.remove(mailbox.getUpn());
            } else {
                rootFolderMailboxesToDelete.add(mailbox); // mailbox no longer in file should be removed
            }
        }

        // delete missing mailboxes - no longer in file
        rootFolderMailboxesToDelete.forEach(mailbox -> mailboxService.deleteMailbox(mailbox.getId()));

        // create doc folder for root folder if missing
        if (mailboxUpn.size() > 0) {
            List<DocFolder> folders = docFolderService.getForRootFolderAndDepth(rootFolderDto.getId(), 0, 10, 0);
            if (folders == null || folders.isEmpty()) {
                docFolderService.createDocFolderIfNeededWithCache(rootFolderDto.getId(), "");
            }
        }

        // create missing mailboxes
        mailboxUpn.forEach(mailbox -> {
            MailboxDto mailboxDto = new MailboxDto();
            mailboxDto.setRootFolderId(rootFolderDto.getId());
            mailboxDto.setUpn(mailbox);
            mailboxService.createMailboxDto(mailboxDto);

            // create doc folder for mailbox if needed
            String mailboxPath = "/" + DocFolderUtils.removeTrailingSlash(rootFolderDto.getMediaType().toNormalizedFolderPath(mailbox));
            DocFolderService.DocFolderCacheValue docFolderCached = docFolderService.createDocFolderIfNeededWithCache(rootFolderDto.getId(), mailboxPath);
            Long folderId = docFolderCached == null ? null : docFolderCached.getDocFolderId();
            if (folderId != null) {
                docFolderService.updateMailboxFolderProperties(docFolderCached, mailboxPath, rootFolderDto.getId(), mailbox);
            }
        });

    }

    private void dispatchStartingJob(SimpleJobDto startingJob, RootFolderDto rootFolderDto) {
        switch (startingJob.getType()) {
            case SCAN:
                startRunFromScan(startingJob, rootFolderDto);
                break;
            case EXTRACT:
                jobManagerService.updateJobState(startingJob.getId(), JobState.READY, null, null, false);
                break;
            default:
                throw new UnsupportedOperationException(messageHandler.getMessage("run.start-job.type.invalid", Lists.newArrayList(startingJob.getType())));
        }
    }

    private void startRunFromScan(SimpleJobDto scanJob, RootFolderDto rootFolderDto) {
        long runId = scanJob.getRunContext();
        Long rootFolderId = rootFolderDto.getId();

        Long scanJobId = scanJob.getId();
        try {
            jobManagerService.setOpMsg(scanJobId, "Scan");
            opStateService.resetScanStartTime();

            if (rootFolderDto.getNumberOfFoldersFound() == null || rootFolderDto.getNumberOfFoldersFound() == 0) {
                logger.info("this is an Initial run");
            } else {
                logger.debug("rerun On RootFolder (runId = {}) ", runId);
                int beforeFileCount = fileCatService.countFilesUnderRootFolder(rootFolderId);
                jobManagerService.updateEstimatedTaskCount(scanJobId, beforeFileCount);

                if (rootFolderDto.isReingest()) {
                    String storeName = KeyValueDbUtil.getServerStoreName(kvdbRepo, rootFolderId);
                    if (!kvdbRepo.getFileEntityDao().storeExists(storeName)) {
                        kvdbRepo.getFileEntityDao().createStore(storeName);
                    }
                }
            }
            jobManagerService.updateJobState(scanJobId, JobState.PENDING, null, "Waiting to be processed", false);
            logger.info("creating scan task for scan job #{}", scanJob.getId());

            // exchange task creation for all mailboxes and all folders (folder-structure and folder's contents)
            // we need to create the tasks here as we cant do it via mp since we only get a change log
            List<SimpleTask> tasksToCreate = new ArrayList<>();
            if (!rootFolderDto.getMediaType().hasPath()) {
                createExchangeRelatedMapTasks(tasksToCreate, rootFolderId, runId, scanJobId);
            }

            // if no exchange tasks created or if not exchange we still need at least one task created to avoif the run getting stuck
            // this shouldnt happen really cause exchange should always have at least one mailbox
            // but since there is no code enforcing this at the moment, we should check
            if (tasksToCreate.isEmpty()) {
                // TODO Itai: The var below is never used but for some mysterious reason the build doesn't pass without saving this var. Figure this out and fix.
                @SuppressWarnings("unused")
                SimpleTask task = jobManagerService.createTaskIfAbsent(scanJob.getItemId(), scanJobId, TaskType.SCAN_ROOT_FOLDER);
            } else {
                taskManagerService.createTasksInBatch(tasksToCreate);
            }
        } catch (RuntimeException e) {
            logger.error("Scan failed. Mark scan job as done", e);
            jobManagerService.handleFailedFinishJob(scanJobId, "Map failed: " + e.getMessage());
            jobManagerAppService.endRun(runId, PauseReason.SYSTEM_ERROR);
            throw e;
        }
    }

    private void createExchangeRelatedMapTasks(List<SimpleTask> tasksToCreate, Long rootFolderId,
                                               long runId, Long scanJobId) {

        // handle mailbox tasks if exists (folder-structure)
        List<MailboxDto> mailboxes = mailboxService.getByRootFolderId(rootFolderId);
        mailboxes.forEach(mailbox -> {
            String params = createExchangeMapTaskParam(mailbox.getUpn(), mailbox.getSyncState());
            SimpleTask task = jobManagerService.createTaskObject(runId, scanJobId, TaskType.SCAN_ROOT_FOLDER, rootFolderId, params);
            tasksToCreate.add(task);
        });
    }

    public void createExchangeRelatedMapTasksForMailbox(Long rootFolderId,
                                                        long runId, Long scanJobId, Boolean firstScan,
                                                        boolean isReingest, String mailboxUpn) {
        if (!isReingest && firstScan != null && !firstScan) { // do on rescan only
            List<SimpleTask> tasksToCreate = new ArrayList<>();
            // handle folder tasks if exists (folder's contents)
            int from = 0;
            List<DocFolder> folders;
            do {
                folders = docFolderService.getForRootFolderAndMailboxUpn(rootFolderId, mailboxUpn, from, MAX_RECORDS_TO_PROCESS);
                if (folders != null && !folders.isEmpty()) {
                    folders.forEach(folder -> {
                        if (folder.getDepthFromRoot() == 0) {
                            // do nothing skip top folder
                        } else if (Strings.isNullOrEmpty(folder.getMailboxUpn())) {
                            logger.warn("Attempt to create task with missing parameters for folder {}.", folder.toString());
                        } else if (folder.getFolderType().equals(FolderType.MAILBOX)) {
                            // do nothing skip mailbox folder
                        } else {
                            String params = createExchangeMapTaskParam(folder.getMailboxUpn(), folder.getSyncState());
                            SimpleTask task = jobManagerService.createTaskObject(runId, scanJobId, TaskType.SCAN_ROOT_FOLDER, rootFolderId, params, folder.getMediaEntityId());
                            tasksToCreate.add(task);
                        }
                    });
                }
                from += MAX_RECORDS_TO_PROCESS;
            } while (folders != null && folders.size() == MAX_RECORDS_TO_PROCESS);

            if (!tasksToCreate.isEmpty()) {
                logger.debug("creating tasks for exchange folders of mailboxUpn {} tasks [{}]", mailboxUpn, tasksToCreate);
                taskManagerService.createTasksInBatch(tasksToCreate);
            }
        }
    }

    private String createExchangeMapTaskParam(String upn, String syncState) {
        Map<String, String> scanTaskParams = new HashMap<>();
        scanTaskParams.put(ScanTaskParameters.MAILBOX_UPN, upn);
        if (syncState != null) {
            scanTaskParams.put(ScanTaskParameters.SYNC_STATE, syncState);
        }
        String params = ModelDtoConversionUtils.mapToJson(scanTaskParams);
        return Strings.nullToEmpty(params).replace("\"", "\\\"");
    }

    public void clearSolrField(CatFileFieldType catFileFieldType) {
        logger.info("Clear Solr field");
        executionManagerService.startAsyncExecution(() -> fileCatService.clearFieldInCatFile(catFileFieldType), "clear_Solr_Field");
    }

    public void removeValueFromSolrField(CatFileFieldType catFileFieldType, String value) {
        logger.info("Remove value from Solr field");
        executionManagerService.startAsyncExecution(() -> fileCatService.removeFromFieldInCatFile(catFileFieldType, value), "remove_value_solr");
    }
}
