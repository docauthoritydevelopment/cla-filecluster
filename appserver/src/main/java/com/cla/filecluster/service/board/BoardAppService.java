package com.cla.filecluster.service.board;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.board.BoardDto;
import com.cla.common.domain.dto.board.BoardEntityType;
import com.cla.common.domain.dto.board.BoardTaskState;
import com.cla.common.domain.dto.board.BoardTaskType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.board.BoardTask;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class BoardAppService {

    @Autowired
    private BoardTaskService boardTaskService;

    @Autowired
    private BoardService boardService;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Value("${board.task-create.limit:10}")
    private int taskCreateLimit;

    @Transactional
    public void deleteBoard(long boardId) {
        boardTaskService.deleteByBoard(boardId);
        boardService.deleteBoard(boardId);
    }

    public void createTasksForFolderFilter(BoardDto boardDto, long ownerId, Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        FacetPage<AggregationCountFolderDto> folderResult;
        if (params != null && params.containsKey("id")) {
            String folderIdString = params.get("id");
            long folderId = Long.valueOf(folderIdString);
            folderResult = fileTreeAppService.findFoldersFileCountTreeNodeChildren(dataSourceRequest, folderId);
        } else {
            folderResult = fileTreeAppService.findFoldersFileCountTreeRoot(dataSourceRequest);
        }

        if (!folderResult.getContent().isEmpty()) {
            List<BoardTask> toCreate = new ArrayList<>();
            List<AggregationCountFolderDto> content = folderResult.getContent();
            int max = Math.min(content.size(), taskCreateLimit);
            for (int i = 0; i < max; ++i) {
                AggregationCountFolderDto item = content.get(i);
                toCreate.add(createBoardTask(boardDto, ownerId, item));
            }
            if (!toCreate.isEmpty()) {
                boardTaskService.createBulk(toCreate);
            }
        }
    }

    private BoardTask createBoardTask(BoardDto boardDto, long ownerId, AggregationCountFolderDto item) {
        BoardTask task = new BoardTask();
        task.setBoardId(boardDto.getId());
        task.setBoardTaskState(BoardTaskState.NEW);
        task.setOwnerId(ownerId);
        task.setAssignee(ownerId);
        task.setBoardEntityType(BoardEntityType.FOLDER);
        task.setEntityId(String.valueOf(item.getItem().getId()));
        task.setBoardTaskType(BoardTaskType.ASSIGN);
        return task;
    }
}
