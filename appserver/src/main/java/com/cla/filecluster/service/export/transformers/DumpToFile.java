package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.progress.ProgressService;
import org.reactivestreams.Publisher;
import org.springframework.core.io.Resource;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

public class DumpToFile extends ProgressedTransformer<DataBuffer, Resource> {

    public static DumpToFile with(String exportId, ProgressService.Handler progress) {
        return new DumpToFile(exportId, progress);
    }

    private DumpToFile(String exportId, ProgressService.Handler progress) {
        super(exportId, progress);
    }

    @Override
    protected DataBuffer createDataBuffer(Resource resource) {
        throw new UnsupportedOperationException("DumpToFile cannot be transformed to a data output stream");
    }

    @Override
    public Publisher<Resource> apply(Flux<DataBuffer> dataBufferFlux) {
        return null;
    }
}
