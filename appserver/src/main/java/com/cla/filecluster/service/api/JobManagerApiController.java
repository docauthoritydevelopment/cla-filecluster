package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.crawler.PhaseDetailsDto;
import com.cla.common.domain.dto.crawler.RootFoldersAggregatedSummaryInfo;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.RunSummaryInfoDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.mediaproc.BizListExtractionManager;
import com.cla.filecluster.mediaproc.StartScanProducer;
import com.cla.filecluster.mediaproc.StrategyType;
import com.cla.filecluster.mediaproc.TaskProducingStrategySelector;
import com.cla.filecluster.mediaproc.connectors.MediaProcessorCommListeners;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.producers.TaskProducingStrategyConfig;
import com.cla.filecluster.util.ControllerUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.security.Authority.Const.GENERAL_ADMIN;
import static com.cla.common.domain.dto.security.Authority.Const.RUN_SCANS;
import static com.cla.common.domain.dto.security.Authority.Const.TECH_SUPPORT;
import static com.cla.filecluster.mediaproc.TaskProducingStrategySelector.EXCLUSIVE_MODE;

@RestController()
@RequestMapping("/api/scan")
public class JobManagerApiController {
    private static final Logger logger = LoggerFactory.getLogger(JobManagerApiController.class);

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private BizListExtractionManager bizListExtractionManager;

    @Autowired
    private StartScanProducer startScanProducer;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private TaskProducingStrategySelector taskProducingStrategySelector;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired(required = false)
    private MediaProcessorCommListeners mediaProcessorCommListeners;

    @Autowired
    private MessageHandler messageHandler;

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/pause/map/process", method={RequestMethod.GET}) // todo yael remove performance chk map
    public String pauseMapProcess() {
        mediaProcessorCommListeners.setAcceptScanResponses(false);
        return "pauseMapProcess";
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/resume/map/process", method={RequestMethod.GET}) // todo yael remove performance chk map
    public String resumeMapProcess() {
        mediaProcessorCommListeners.setAcceptScanResponses(true);
        return "resumeMapProcess";
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/start/rootFolder/{rootFolderId}", method={RequestMethod.POST,RequestMethod.GET})
    public String startScanRootFolder(@PathVariable long rootFolderId, @RequestParam Map<String,String> params) {
        logger.debug("Rerun on root folder {}",rootFolderId);
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        List<String> operationList = startScanProducer.getOperationList(params);
        eventAuditService.audit(AuditType.SCAN,"Start scan rootfolder ", AuditAction.START, RootFolder.class,Long.toString(rootFolderId), params );
        return startScanProducer.startScanRootFolder(rootFolderId,dataSourceRequest.isWait(), operationList, dataSourceRequest.isForceReingest());
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/start/rootFolders", method={RequestMethod.POST,RequestMethod.GET})
    public List<String> startScanRootFolders(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        String rootFolders = params.get("rootFolderIds");
        logger.debug("Rerun on root folder Ids: {}",rootFolders);
        eventAuditService.audit(AuditType.SCAN,"Start scan rootfolders ", AuditAction.START, RootFolder.class,rootFolders, params );
        List<String> operationList = startScanProducer.getOperationList(params);
        List<Long> rootFolderIds = Arrays.stream(rootFolders.split(",")).map(Long::valueOf).collect(Collectors.toList());
        return startScanProducer.startScanRootFolder(rootFolderIds,dataSourceRequest.isWait(),operationList, dataSourceRequest.isForceReingest());
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/analysis/rootFolders", method={RequestMethod.POST,RequestMethod.GET})
    public List<String> startAnalysisRootFolders(@RequestParam Map<String,String> params) {
        String rootFolders = params.get("rootFolderIds");
        logger.debug("Rerun on root folder Ids: {}",rootFolders);
        eventAuditService.audit(AuditType.ANALYSIS,"Start analysis rootfolders ", AuditAction.START, RootFolder.class, rootFolders, params);
        List<Long> rootFolderIds = Arrays.stream(rootFolders.split(",")).map(Long::valueOf).collect(Collectors.toList());
        return startScanProducer.createAnalysisRunForRootFolder(rootFolderIds);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/ingest/rootFolders", method={RequestMethod.POST,RequestMethod.GET})
    public List<String> startIngestRootFolders(@RequestParam Map<String,String> params) {
        String rootFolders = params.get("rootFolderIds");
        logger.debug("Rerun on root folder Ids: {}",rootFolders);
        eventAuditService.audit(AuditType.INGEST,"Start ingest rootfolders ", AuditAction.START, RootFolder.class, rootFolders, params);
        List<Long> rootFolderIds = Arrays.stream(rootFolders.split(",")).map(Long::valueOf).collect(Collectors.toList());
        return startScanProducer.createIngestRunForRootFolder(rootFolderIds);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/start/schedulegroup", method={RequestMethod.POST})
    public List<String> startScheduleGroup(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        String scheduleGroups = params.get("scheduleGroupIds");

        logger.debug("Scan schedule groups rootFolders. Ids: {}",scheduleGroups);
        eventAuditService.audit(AuditType.SCAN,"Start scan schedule groups ", AuditAction.START,
                ScheduleGroup.class, scheduleGroups, params );

        List<String> operationList = startScanProducer.getOperationList(params);
        List<Long> scheduleGroupIds = Arrays.stream(scheduleGroups.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        return startScanProducer.startScanScheduleGroups(scheduleGroupIds, dataSourceRequest.isWait(), operationList, true);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/pause/all", method={RequestMethod.POST,RequestMethod.GET})
    public void pauseAllRuns() {
        eventAuditService.audit(AuditType.SCAN,"Pause all ", AuditAction.PAUSE );
        jobManagerAppService.pauseAllRuns();

    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/pause/run", method={RequestMethod.POST})
    public void pauseRuns(@RequestParam Map<String,String> params) {
        String runs = params.get("runIds");
        logger.debug("Pause run Ids: {}", runs);
        eventAuditService.audit(AuditType.SCAN,"Pause runs ", AuditAction.PAUSE, CrawlRun.class, runs, params);
        List<Long> runIds = Arrays.stream(runs.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        jobManagerAppService.pauseRuns(runIds);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value = "/process/pause/job", method = {RequestMethod.POST, RequestMethod.GET})
    public void pauseJobs(@RequestParam String jobIds, @RequestParam(required = false, defaultValue="true") Boolean force) {
        logger.debug("Pause jobs {}", jobIds);
        eventAuditService.audit(AuditType.SCAN,"Pause jobs ", AuditAction.PAUSE, SimpleJob.class, jobIds,
                Pair.of("force", force));
        List<Long> jobIdsList = Arrays.stream(jobIds.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        jobIdsList.forEach(jobId -> jobManagerAppService.pauseJob(jobId, force));
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value = "/jobs/{jobId}/pause", method = {RequestMethod.POST, RequestMethod.GET})
    public Boolean pauseJob(@PathVariable long jobId, @RequestParam(required = false, defaultValue="true") Boolean force) {
        eventAuditService.audit(AuditType.SCAN,"Pause job ", AuditAction.PAUSE, SimpleJob.class,
                Long.toString(jobId), Pair.of("force", force));
        return jobManagerAppService.pauseJob(jobId, force);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value = "/jobs/{jobId}/resume", method = {RequestMethod.POST, RequestMethod.GET})
    public Boolean resumeJob(@PathVariable long jobId, @RequestParam(required = false, defaultValue="true") Boolean force) {
        eventAuditService.audit(AuditType.SCAN,"Resume job ", AuditAction.RESUME, SimpleJob.class,
                Long.toString(jobId), Pair.of("force", force));
        return jobManagerAppService.resumeJob(jobId, force);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value = "/runs/{runId}/jobTypes/{jobType}/resume", method = {RequestMethod.POST})
    public Boolean resumeRunJobsOfTypes(@PathVariable long runId,
                                        @PathVariable String jobType) {
        eventAuditService.audit(AuditType.SCAN,"Resume jobs of type", AuditAction.RESUME, CrawlRun.class,
                Long.toString(runId), Pair.of("jobType", jobType));
        JobType theJobType = JobType.valueOf(jobType.toUpperCase());
        return jobManagerAppService.resumeJob(runId, theJobType);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/stop/run", method={RequestMethod.POST})
    public void stopRuns(@RequestParam String runIds) {
        logger.debug("Stop run Ids: {}",runIds);
        eventAuditService.audit(AuditType.SCAN,"Stop runs ", AuditAction.STOP, CrawlRun.class,runIds);
        List<Long> runIdsList = Arrays.stream(runIds.split(","))
                .map(Long::valueOf).collect(Collectors.toList());
       jobManagerAppService.stopPausedRuns(runIdsList);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/stop/graceful/run", method={RequestMethod.POST})
    public void stopRunsGraceful(@RequestParam String runIds) {
        logger.debug("Graceful stop run Ids: {}",runIds);
        eventAuditService.audit(AuditType.SCAN,"Graceful stop runs ", AuditAction.STOP_GRACEFUL, CrawlRun.class, runIds);
        List<Long> runIdsList = Arrays.stream(runIds.split(","))
                .map(Long::valueOf).collect(Collectors.toList());
        jobManagerAppService.stopRunsGraceful(runIdsList);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/resume/all", method={RequestMethod.POST, RequestMethod.GET})
    public void resumeAllRuns() {
        eventAuditService.audit(AuditType.SCAN,"Resume all paused runs ", AuditAction.RESUME, CrawlRun.class,null);
        jobManagerAppService.resumeAllRuns();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/resume/run", method={RequestMethod.POST, RequestMethod.GET})
    public void resumeRuns(@RequestParam String runIds) {
        logger.debug("resume runs {}", runIds);
        eventAuditService.audit(AuditType.SCAN,"Resume runs ", AuditAction.RESUME, CrawlRun.class,runIds);
        List<Long> runIdsList = Arrays.stream(runIds.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        jobManagerAppService.resumeRuns(runIdsList);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value = "/process/resume/job", method = {RequestMethod.POST, RequestMethod.GET})
    public void resumeJobs(@RequestParam String jobIds, @RequestParam(required = false, defaultValue="true") Boolean force) {
        // When a specific job is manually resumed - force it
        logger.debug("resume jobs {}", jobIds);
        eventAuditService.audit(AuditType.SCAN,"Resume jobs ", AuditAction.RESUME, SimpleJob.class,
                jobIds, Pair.of("force", force));
        List<Long> jobIdsList = Arrays.stream(jobIds.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        jobIdsList.forEach(jobId -> jobManagerAppService.resumeJob(jobId, force));   // When a specific job is manually resumed - force it
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/start/extract/bizlists", method={RequestMethod.POST, RequestMethod.GET})
    public void extractBizLists() {
        eventAuditService.audit(AuditType.EXTRACT,"Start extract active bizLists", AuditAction.START);
        boolean result = bizListExtractionManager.createExtractRun(true);
        if (!result) {
            throw new BadRequestException(messageHandler.getMessage("biz-list.already-running"), BadRequestType.JOB_ALREADY_STARTED);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/start/extract/bizlist/{bizListId}", method={RequestMethod.POST, RequestMethod.GET})
    public void extractBizList(@PathVariable long bizListId) {
        eventAuditService.audit(AuditType.EXTRACT,"Start extract bizList", AuditAction.START,
                BizList.class, Long.toString(bizListId));
        bizListExtractionManager.startExtractRunForBizListAsync(bizListId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/run/{runId}/job/list",method={RequestMethod.GET})
    public List<PhaseDetailsDto> listRunJobs(@PathVariable long runId, @RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Scan phases export", AuditAction.VIEW,  PhaseDetailsDto.class,null, dataSourceRequest);
        }
        return jobManagerAppService.listRunJobs(runId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT})
    @RequestMapping(value = "/process/enqtonew/run", method = {RequestMethod.POST, RequestMethod.GET})
    public void setEnqTasksToNewForRun(@RequestParam String runIds) {
        List<Long> runIdsList = Arrays.stream(runIds.split(","))
                .map(Long::valueOf).collect(Collectors.toList());
        jobManagerAppService.setEnqTasksToNewForRun(runIdsList);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT})
    @RequestMapping(value = "/process/enqtonew/job", method = {RequestMethod.POST, RequestMethod.GET})
    public void setEnqTasksToNewForJob(@RequestParam String jobIds) {
        List<Long> jobIdsList = Arrays.stream(jobIds.split(","))
                .map(Long::valueOf)
                .collect(Collectors.toList());
        jobManagerAppService.setEnqTasksToNewForJob(jobIdsList);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT, GENERAL_ADMIN})
    @RequestMapping(value="/startanalyze/run/{runId}",method={RequestMethod.GET})
    public void startAnalysisWhileIngestIsRunning(@PathVariable long runId) {
        jobManagerAppService.activateAnalyzeWhileIngestIsRunning(runId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/run/list", method={RequestMethod.GET})
    public Page<CrawlRunDetailsDto> listRuns(@RequestParam  Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);

        List<RunStatus> filterByRunStatus = ControllerUtils.getStatesList(params);

        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Scan history export", AuditAction.VIEW,  CrawlRunDetailsDto.class,null, dataSourceRequest);
        }

        if (filterByRunStatus != null) {
            return jobManagerAppService.listRuns(dataSourceRequest, filterByRunStatus);
        } else {
            return jobManagerAppService.listRuns(dataSourceRequest);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/run/extract/list", method={RequestMethod.GET})
    public Page<CrawlRunDetailsDto> listExtractRuns(@RequestParam  Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        PageRequest pageRequestWithSort = dataSourceRequest.createPageRequestWithSort();
        List<RunStatus> filterByRunStatus = ControllerUtils.getStatesList(params);

        if (filterByRunStatus != null) {
            return fileCrawlerExecutionDetailsService.findAllExtract(pageRequestWithSort, filterByRunStatus);
        } else {
            return fileCrawlerExecutionDetailsService.findAllExtract(pageRequestWithSort);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/extract/run/latest", method={RequestMethod.GET})
    public CrawlRunDetailsDto getExtractLatestRun() {
        return fileCrawlerExecutionDetailsService.getExtractLatestRun();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/active/runs/summary", method={RequestMethod.GET})
    public List<RunSummaryInfoDto> getActivesDetails(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        List<RunSummaryInfoDto> result = jobManagerAppService.getActiveRunsDetails(dataSourceRequest);
        if (result.size() > dataSourceRequest.getPageSize()) {
            result = result.subList(0, dataSourceRequest.getPageSize());
        }
        return result;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={RUN_SCANS})
    @RequestMapping(value="/process/rootfolders/aggregated/summary", method={RequestMethod.GET})
    public RootFoldersAggregatedSummaryInfo getRootFoldersAggregatedSummaryInfo(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return jobManagerAppService.getRootFoldersAggregatedSummaryInfo(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT})
    @RequestMapping(value="/task-strategy/select", method={RequestMethod.PUT})
    public void selectTaskProducingStrategy(@RequestParam StrategyType type){
        taskProducingStrategySelector.setSelectedStrategy(type);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT})
    @RequestMapping(value="/task-strategy/set-param", method={RequestMethod.PUT})
    public void setTaskProducingStrategyParameter(@RequestParam String paramName, @RequestParam String value){
        taskProducingStrategySelector.setParameter(paramName, value);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT, RUN_SCANS})
    @RequestMapping(value="/task-strategy/proc-mode", method={RequestMethod.PUT, RequestMethod.GET})    // Get is temp for interim use until UI is in place
    public JobProcessingMode setTaskProducingStrategyParameter(@RequestParam JobProcessingMode jobProcessingMode){
        if (jobProcessingMode == JobProcessingMode.SIMPLE){
            taskProducingStrategySelector.setSelectedStrategy(StrategyType.SIMPLE);
        }
        else if (jobProcessingMode == JobProcessingMode.FAIR){
            taskProducingStrategySelector.setSelectedStrategy(StrategyType.FAIR);
        }
        else if (jobProcessingMode == JobProcessingMode.INGEST_ONLY){
            taskProducingStrategySelector.setSelectedStrategy(StrategyType.MANUAL_EXCLUSIVE);
            taskProducingStrategySelector.setParameter(EXCLUSIVE_MODE, "INGEST");
        }
        else if (jobProcessingMode == JobProcessingMode.ANALYZE_ONLY){
            taskProducingStrategySelector.setSelectedStrategy(StrategyType.MANUAL_EXCLUSIVE);
            taskProducingStrategySelector.setParameter(EXCLUSIVE_MODE, "ANALYZE");
        }
        else if (jobProcessingMode == JobProcessingMode.AUTO_EXCLUSIVE) {
            taskProducingStrategySelector.setSelectedStrategy(StrategyType.AUTO_EXCLUSIVE);
        }

        return getTaskProducingStrategyParameter();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={TECH_SUPPORT})
    @RequestMapping(value="/task-strategy/get-proc-mode", method={RequestMethod.GET})
    public JobProcessingMode getTaskProducingStrategyParameter(){
        StrategyType selectedStrategyType = taskProducingStrategySelector.getSelectedStrategyType();
        if (selectedStrategyType == StrategyType.SIMPLE){
            return JobProcessingMode.SIMPLE;
        }
        else if (selectedStrategyType == StrategyType.MANUAL_EXCLUSIVE) {
            TaskProducingStrategyConfig config = taskProducingStrategySelector.getConfig();
            if (JobType.INGEST == config.getExclusiveMode()) {
                return JobProcessingMode.INGEST_ONLY;
            } else {
                return JobProcessingMode.ANALYZE_ONLY;
            }
        }
        else if (selectedStrategyType == StrategyType.AUTO_EXCLUSIVE) {
            return JobProcessingMode.AUTO_EXCLUSIVE;
        }
        else{
            return JobProcessingMode.FAIR;
        }
    }

    public enum JobProcessingMode{
        SIMPLE,
        FAIR,
        INGEST_ONLY,
        ANALYZE_ONLY,
        AUTO_EXCLUSIVE
    }


}
