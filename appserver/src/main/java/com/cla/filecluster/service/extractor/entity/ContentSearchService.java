package com.cla.filecluster.service.extractor.entity;

import com.cla.common.constants.ExtractionFieldType;
import com.cla.common.domain.dto.extraction.ExtractedEntityDto;
import com.cla.filecluster.domain.dto.FileDTOSearchResult;
import com.cla.filecluster.domain.dto.SolrContentEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.extraction.AbstractExtraction;
import com.cla.filecluster.domain.entity.extraction.ExtractionRule;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.batch.writer.ingest.IngestBatchResultsProcessor;
import com.cla.filecluster.repository.solr.FileDTOSearchRepository;
import com.cla.filecluster.repository.solr.infra.SolrClientAdapter;
import com.cla.filecluster.repository.solr.infra.SolrClientFactory;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class ContentSearchService {
    private static final Logger logger = LoggerFactory.getLogger(ContentSearchService.class);

    @Value("${searcher.maxResults:10000}")
    private int maxResults;

    @Value("${combinationProximity:100}")
    private String combinationProximity;

    @Value("${tokenReplaceStr:null}")
    private String tokenReplaceStr;

    @Value("${tokenReplacementStr:nullToken}")
    private String tokenReplacementStr;

    private SolrClientAdapter extractionCoreSolrClient;

    @Autowired
    private FileDTOSearchRepository fileDTOSearchRepository;

    @Autowired
    private ContentSearchTextService contentSearchTextService;

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private SolrClientFactory solrClientFactory;

    @Value("${fileDTO.solr.core:Extraction}")
    private String extractionSolrCore;

    @Autowired
    private IngestBatchResultsProcessor ingestBatchResultsProcessor;

    @PostConstruct
    public void init() {
        extractionCoreSolrClient = solrClientFactory.getSolrClient(extractionSolrCore, false);
    }

    public ExtractionRule convertRule(final ExtractionRule rule, ExtractedEntityDto extractedEntityDto) {
        final ExtractionRule convertedRule = new ExtractionRule(rule);
        for (Map.Entry<String, Object> field : extractedEntityDto.getFields().entrySet()) {
            final String fieldHeader = field.getKey();

            String fieldValue = ExtractionRulesUtils.getFieldStringValue(field);
            if (tokenReplaceStr != null && tokenReplacementStr != null && fieldValue != null && fieldValue.equals(tokenReplaceStr)) {
                fieldValue = tokenReplacementStr;
            }
            if (field.getValue() instanceof ArrayList) {
                String query = convertedRule.getQuery();
                query = ExtractionRulesUtils.replaceArrayMacro(query, fieldHeader, (ArrayList) field.getValue());
                convertedRule.setQuery(query);
            }
            if (fieldValue != null) {
                String query = convertedRule.getQuery();
                query = ExtractionRulesUtils.replaceAndOrCombination(query, fieldHeader, fieldValue, extractedEntityDto, combinationProximity);
                query = ExtractionRulesUtils.replaceCombination(query, fieldHeader, fieldValue, extractedEntityDto, combinationProximity);
                query = ExtractionRulesUtils.replaceANDMacro(query, fieldHeader, fieldValue);
                query = ExtractionRulesUtils.replaceORMacro(query, fieldHeader, fieldValue);
                convertedRule.setQuery(query);
            }
        }

        for (final String header : extractedEntityDto.getFields().keySet()) {
            if (convertedRule.getQuery().contains(header)) {
                logger.trace("For rule {} the query {} contains header {} so we will not run the query. row is {}",
                        rule.getName(), convertedRule.getQuery(), header, extractedEntityDto.getFields().values());
                return null;
            }
        }
        return convertedRule;
    }

    public List<SolrContentEntity> find(final String q) {
        return find(q, null, false);
    }

    public List<SolrContentEntity> find(final String q, Date date, boolean isFilterDateAvtive) {
        try {
            final SolrQuery query = new SolrQuery();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            query.setQuery(q);
            query.setRequestHandler("/query");
            query.setRows(maxResults);
            query.setFields(ExtractionFieldType.ID.getSolrName(), ExtractionFieldType.FILE_NAME.getSolrName(), ExtractionFieldType.PART.getSolrName());    // should include fields used later by exportLine()
            if (isFilterDateAvtive && date != null) {
                String dateStr = sdf.format(date);
                query.setFilterQueries(ExtractionFieldType.LAST_RECORD_UPDATE.filter("[" + dateStr + " TO NOW]"));
            }
            final QueryResponse queryResponse = extractionCoreSolrClient.query(query, SolrRequest.METHOD.POST);
            if (queryResponse == null) {
                logger.error("*** Null query response. input={}, query=", q, query.toString());
                return Lists.newArrayList();
            }
            final List<SolrContentEntity> extractionSolrEntities = queryResponse.getBeans(SolrContentEntity.class);
            if (extractionSolrEntities.size() == maxResults) {
                logger.warn("Config check: entity search reached rowLimit searcher.maxResults={}. q={}", maxResults, q);
            }
            return extractionSolrEntities;
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public long findTotalNumOfDocs() {
        try {
            Query q = Query.create().setRows(0); // don't actually request any data
            return extractionCoreSolrClient.query(q.build(), SolrRequest.METHOD.POST).getResults().getNumFound();
        } catch (SolrServerException | IOException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveContentInSolr(Long taskId, ClaFile claFile, ContentMetadata contentMetadata, String content) {
        Long contentID = contentMetadata.getId();
        ingestBatchResultsProcessor.collectContentEntity(taskId, new SolrContentEntity(contentID, claFile.getFileName(),
                claFile.getType().toInt(), content, claFile.getFsLastModified()));
    }

    public void saveContentInSolr(Long taskId, SolrFileEntity file, ContentMetadata contentMetadata, String content) {
        Long contentID = contentMetadata.getId();
        ingestBatchResultsProcessor.collectContentEntity(taskId, new SolrContentEntity(contentID, file.getFullName(),
                file.getType(), content, file.getModificationDate()));
    }

    public void saveContentInSolr(Long taskId, SolrFileEntity file, ContentMetadata contentMetadata, String content, String fullSheetName) {
        Long contentID = contentMetadata.getId();
        ingestBatchResultsProcessor.collectContentEntity(taskId, new SolrContentEntity(contentID, file.getFullName(),
                fullSheetName, file.getType(), content, file.getModificationDate()));
    }

    /*
     * TODO - this function is too "low level" to be exposed as a public function here.
     */
    public FileDTOSearchResult findInSolrExtractionCore(String query, String[] facetArray, String pivotFacetField, String[] filtersArray, Integer rows, String cursorMark) {
        return fileDTOSearchRepository.find(query, facetArray, pivotFacetField, filtersArray, rows, cursorMark);
    }

    public List<Long> findFileIds(SolrContentEntity extractionSolrEntity) {
        long contentId = Long.parseLong(extractionSolrEntity.getId());
        return fileCatService.findFileIdsByContentId(contentId);
    }

    @Deprecated
    public void setGroupToExtraction(final AbstractExtraction abstractExtraction) {
        try {
            SolrFileEntity file = fileCatService.getFileEntity(abstractExtraction.getClaFileId());
            String groupId = file == null ? null : file.getAnalysisGroupId();
            final FileGroup fileGroup = groupId == null ? null : groupsService.findById(groupId);
            if (fileGroup != null) {
                abstractExtraction.setGroupId(fileGroup.getId());
                abstractExtraction.setGroupName(fileGroup.getShortName());    // Set also if null
            } else {
                abstractExtraction.setGroupId("-");
                abstractExtraction.setGroupName("-");
            }
        } catch (final Exception e) {
            logger.error("Unexpected exception while getting group for file {} {}. {}", abstractExtraction.getClaFileId(), abstractExtraction.getClaFileName(), e.getMessage(), e);
            abstractExtraction.setGroupId("-");
            abstractExtraction.setGroupName("-");
        }
    }
}
