package com.cla.filecluster.service.extractor.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

public class CsvParseDateFormatStemming extends CellProcessorAdaptor {	
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CsvParseDateFormatStemming.class);
	
	static List<String> formatVariations = null;
	static int logOnnce = 1;

	public CsvParseDateFormatStemming() {
		super();
	}

	public CsvParseDateFormatStemming(final CellProcessor next) {
		// this constructor allows other processors to be chained after ParseDay
		super(next);
	}

	@Override
	public Object execute(final Object value, final CsvContext context) {
		
		if (formatVariations==null || formatVariations.isEmpty()) {
			if (logOnnce > 0) {
				logOnnce--;
				logger.warn("formatVariations definiton was not set");
			}
			return value.toString();
		}

		validateInputNotNull(value, context);  // throws an Exception if the input is null
		final Object newValue = (new ParseDate(formatVariations.get(0))).execute(value, context);
		if (! (newValue instanceof Date)) {
			throw new SuperCsvCellProcessorException(
			String.format("Expected date object got '%s' of type %s as a day", value, value.getClass().toString()), context, this);
		}
		final Date date = (Date) newValue;
		final String tokens = formatVariations.stream().map(f->(new SimpleDateFormat(f)).format(date)).collect(Collectors.joining(" "));
		return next.execute(tokens, context);
	}

	public static List<String> getFormatVariations() {
		return formatVariations;
	}

	public static void setFormatVariations(final List<String> formatVariations) {
		CsvParseDateFormatStemming.formatVariations = formatVariations;
	}
}