package com.cla.filecluster.service.event;

import com.cla.common.domain.dto.event.DAEvent;
import org.springframework.stereotype.Service;

/**
 * Created by: yael
 * Created on: 1/11/2018
 */
@Service
public class EmptyEventNormalizer implements EventNormalizer<DAEvent> {

    @Override
    public DAEvent normalize(DAEvent eventData) {
        return eventData;
    }
}
