package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.security.WorkGroupDto;
import com.cla.filecluster.domain.entity.security.WorkGroup;
import com.cla.filecluster.repository.jpa.security.WorkGroupRepository;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class WorkGroupService {

    private final static Logger logger = LoggerFactory.getLogger(WorkGroupService.class);

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";

    private final static String[] requiredHeaders = {NAME, DESCRIPTION};

    @Autowired
    private WorkGroupRepository workGroupRepository;

    @Autowired
    private CsvReader csvReader;

    @Value("${workgroup.default.file:./config/defaultWorkgroups.csv}")
    private String workgroupDefaultFile;

    @Transactional
    public void populateDefaultWorkGroups() {
        if (StringUtils.isNotEmpty(workgroupDefaultFile)) {
            logger.info("Populate default workgroups");
            Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, workgroupDefaultFile, requiredHeaders);
            int failedRows = 0;
            try {
                failedRows = csvReader.readAndProcess(workgroupDefaultFile, row -> createWorkgroupFromRow(row, headersLocationMap),
                        null, null);
            } catch (Exception e) {
                logger.error("Failed to read workgroups file.", e);
            }
            if (failedRows > 0) {
                logger.warn("Failed to insert {} workgroups from file.", failedRows);
            }
        } else {
            logger.warn("Missing list of default workgroups - cannot populate default workgroups");
        }
    }

    private void createWorkgroupFromRow(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues[0].startsWith("#")) {
            logger.info("Commented line {} in default businessRoles file. ignored: {}", row.getLineNumber(), row.toString());
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String description = fieldsValues[headersLocationMap.get(DESCRIPTION)].trim();

        WorkGroupDto workGroupDto = new WorkGroupDto();
        workGroupDto.setName(name);
        workGroupDto.setDescription(description);

        createWorkGroup(workGroupDto);
    }

    @Transactional
    public WorkGroupDto createWorkGroup(WorkGroupDto workGroupDto) {
        WorkGroup workGroup = new WorkGroup();
        workGroup.setDescription(workGroupDto.getDescription());
        workGroup.setName(workGroupDto.getName());
        workGroup.setDateCreated(System.currentTimeMillis());
        workGroup.setDateModified(System.currentTimeMillis());
        workGroup = workGroupRepository.save(workGroup);
        return convertWorkGroup(workGroup);
    }

    @Transactional
    public WorkGroupDto updateWorkGroup(Long id, WorkGroupDto workGroupDto) {
        return workGroupRepository.findById(id)
                .map(existingWorkGroup -> {
                    existingWorkGroup.setDateModified(System.currentTimeMillis());
                    existingWorkGroup.setName(workGroupDto.getName());
                    existingWorkGroup.setDescription(workGroupDto.getDescription());
                    return workGroupRepository.save(existingWorkGroup);
                })
                .map(WorkGroupService::convertWorkGroup)
                .orElse(null);
    }

    public WorkGroupDto findWorkGroupByName(String name) {
        WorkGroup workGroupByName = workGroupRepository.findWorkGroupByName(name);
        return workGroupByName == null ? null : convertWorkGroup(workGroupByName);
    }

    public WorkGroupDto getWorkGroupById(long id) {
        return workGroupRepository.findById(id)
                .map(WorkGroupService::convertWorkGroup)
                .orElse(null);
    }

    @Transactional
    public WorkGroupDto deleteWorkGroup(Long id) {
        return workGroupRepository.findById(id)
                .map(workGroup -> {
                    workGroupRepository.deleteById(id);
                    return convertWorkGroup(workGroup);
                })
                .orElse(null);
    }

    public List<WorkGroupDto> getAllWorkGroups() {
        List<WorkGroup> allWorkGroups = workGroupRepository.findAll();
        return allWorkGroups != null ?
                allWorkGroups.stream()
                        .map(WorkGroupService::convertWorkGroup)
                        .collect(Collectors.toList())
                : new ArrayList<>();
    }

    public static WorkGroupDto convertWorkGroup(WorkGroup workGroup) {
        WorkGroupDto workGroupDto = new WorkGroupDto();
        workGroupDto.setDateCreated(workGroup.getDateCreated());
        workGroupDto.setDateModified(workGroup.getDateModified());
        workGroupDto.setDescription(workGroup.getDescription());
        workGroupDto.setName(workGroup.getName());
        workGroupDto.setId(workGroup.getId());
        return workGroupDto;
    }

    public static Set<WorkGroupDto> convertWorkGroups(Set<WorkGroup> workGroups) {
        return workGroups.stream().map(WorkGroupService::convertWorkGroup).collect(Collectors.toSet());
    }
}
