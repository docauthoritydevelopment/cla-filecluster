package com.cla.filecluster.service.export.exporters.file.associations;

public interface FileTagHeaderFields {
    String NAME = "Name";
    String TYPE = "Type";
    String NUM_OF_FILES = "Number of files";
    String DESCRIPTION = "Description";
}
