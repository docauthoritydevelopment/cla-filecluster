package com.cla.filecluster.service.files.managers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.bizlist.ExtractionSummaryDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.extraction.FileHighlightsDto;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.messages.FileContentResultMessage;
import com.cla.common.domain.dto.messages.FileContentType;
import com.cla.filecluster.config.security.FunctionalItemId;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.security.FunctionalItem;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.cla.common.domain.dto.security.Authority.Const.*;

// TODO: remove legacy URL mapping once it is no longer in usage.

@RestController
@RequestMapping(value={"/files","/api/files"})
public class EntityHighlighterApiController {

	private static final Logger logger = LoggerFactory.getLogger(EntityHighlighterApiController.class);

	@Autowired
	private FileCatService fileCatService;

	@Autowired
	private ExtractTokenizer extractTokenizer;

	@Autowired
	private SolrSpecificationFactory solrSpecificationFactory;

	@Autowired
	private FilesApplicationService filesApplicationService;

	@Autowired
	private FileContentService fileContentService;

	@Autowired
	private MessageHandler messageHandler;

	@Autowired
	private SolrQueryBuilder solrQueryBuilder;

	@Value(("${file-content-service.file-get-timeout-min:3}"))
	private int fileGetTimeoutMinutes;

	private final static String errorHtmlFormat = "<html><head><title>Error</title><meta charset=\"UTF-8\"></head>"
			+ "<body><h2>%s</h2>%s" + "</body></html>";

	private static final String filesDelim = "";
	private static final String filesPrefix = "<h2>Matching files with group reference</h2>";
	private static final String filesSuffix = "";
	private static final String fileFormat = "<li>%1$s - <a href=\"%3$s\" target=\"_blank\" >%2$s</a></li>";

	@RequestMapping(value = "/highlightEntityExtractions/{id}")
	@PreAuthorize("isFullyAuthenticated()")
	@Permissions(roles={VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
	public FileHighlightsDto extractionHighlightEntity(@PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) final long fileId,
													   @RequestParam(required = false) Integer itemFullLength , @RequestParam final Map<String,String> params, HttpServletResponse response) {
		if (!fileContentService.userAllowedDownloadFile(fileId)) {
			throw new BadRequestException(messageHandler.getMessage("file.view.unauthorize"), BadRequestType.UNAUTHORIZED);
		}
		DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
		SolrFileEntity categoryFile = fileCatService.getFileEntity(fileId);
		if (categoryFile != null) {
			ClaFileDto claFileDto = new ClaFileDto();
			claFileDto.setId(fileId);
			int maxSamplesPerBizList = 30;
			filesApplicationService.setExtractedEntitiesForFile(claFileDto, categoryFile, maxSamplesPerBizList);
			StringBuilder matchesFilter = new StringBuilder();
			//	matches.forEach(element -> matchesFilter.append("\"").append(element).append("\"").append(" "));//TODO: Excat match doesnt work like this
			List<ExtractionSummaryDto> bizLists = claFileDto.getBizListExtractionSummary();
			List<SimpleBizListItemDto> bizListItems = new ArrayList<>();
			bizLists.forEach(list ->bizListItems.addAll(list.getSampleExtractions()));
			bizListItems.forEach(element -> matchesFilter.append(element.getName()).append(" "));
			//String matchesFilter = Stream.of(matches).map(t -> "\"" + t + "\"").collect(Collectors.joining(" "));
			return listHighlightsInFiles(fileId, itemFullLength == null ? 100 : itemFullLength, dataSourceRequest, matchesFilter.toString(),response);
		}
		return null;
	}

	@RequestMapping(value = "/highlightFilePatterns/{id}")
	@PreAuthorize("isFullyAuthenticated()")
	@Permissions(roles={VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
	public FileHighlightsDto extractionHighlightFilePatterns2(@PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) final long fileId,
															  @RequestParam(required = false) Integer itemFullLength , @RequestParam final Map<String,String> params, HttpServletResponse response) {
		if (!fileContentService.userAllowedDownloadFile(fileId)) {
			throw new BadRequestException(messageHandler.getMessage("file.view.unauthorize"), BadRequestType.UNAUTHORIZED);
		}
		logger.debug("Highlight pattern in file {}",fileId);
		DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
		Set<String> matchesFilter = null;

		try {
			CompletableFuture<FileContentResultMessage> completableFuture = new CompletableFuture<>();
			String requestId = fileContentService.addRequestToLockMap(completableFuture);
			fileContentService.addFileContent(fileId, itemFullLength, dataSourceRequest, null,requestId);

			FileContentResultMessage fileContentResultMessage = completableFuture.get(fileGetTimeoutMinutes, TimeUnit.MINUTES);
			if (fileContentResultMessage.getRequestType().equals(FileContentType.ERROR)) {
				throw new BadRequestException(messageHandler.getMessage("file.access.failure",
						Lists.newArrayList(fileContentResultMessage.getPayload().getContent())), BadRequestType.OPERATION_FAILED);
			} else {
				matchesFilter = fileContentService.getPatternsMatchesSet(fileContentResultMessage);
			}
		} catch(TimeoutException timeoutException) {
			logger.error("timeout exception while waiting for highlight pattern result",timeoutException);
			if(response != null) {
				response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
			}
		} catch (Exception e) {
			logger.error("Exception during extraction Highlight File Patterns", e.getSuppressed(), e);
			throw new BadRequestException(messageHandler.getMessage("file.open.failure"), BadRequestType.OPERATION_FAILED);
		}
		return listHighlightsInFiles(fileId, itemFullLength == null ? 100 : itemFullLength, dataSourceRequest,
				matchesFilter != null ? matchesFilter.toString() : "", response);

	}

	/**
	 * Used by the UI
	 * /api/files/35601/highlight/list
	 * ?filter=^%^7B^%^22field^%^22:^%^22contentFilter^%^22,
	 * ^%^22operator^%^22:^%^22contains^%^22,^%^22value^%^22:^%^22help^%^22^%^7D^
	 * &itemFullLength=100^&page=1^&take=10"
	 *
	 * @param fileId			file ID
	 * @param itemFullLength    full length of the item
	 * @param params			parameters map
	 * @return					file highlights object
	 */
	@RequestMapping(value="{id}/highlight/list", method = RequestMethod.GET)
	@PreAuthorize("isFullyAuthenticated()")
	@Permissions(roles={VIEW_CONTENT, VIEW_PERMITTED_CONTENT})
	public FileHighlightsDto findHighlightsInFiles(@PathVariable("id") @FunctionalItemId(FunctionalItem.FILE) final long fileId,
												   @RequestParam(required = false) Integer itemFullLength , @RequestParam final Map<String,String> params, HttpServletResponse response) {
		if (!fileContentService.userAllowedDownloadFile(fileId)) {
			throw new BadRequestException(messageHandler.getMessage("file.view.unauthorize"), BadRequestType.UNAUTHORIZED);
		}
		logger.debug("Find highlights in files {}",fileId);
		DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
		return listHighlightsInFiles(fileId, itemFullLength, dataSourceRequest,null, response);
	}

	private FileHighlightsDto listHighlightsInFiles(@NotNull long fileId,int itemFullLength, @NotNull DataSourceRequest dataSourceRequest,String additionalTextTokens, HttpServletResponse response) {
		logger.debug("Highlight text in file {}",fileId);
		PageRequest pageRequest = getPageRequest(dataSourceRequest);
		SolrSpecification solrSpecification = createSolrSpecification(fileId, itemFullLength, dataSourceRequest,
				pageRequest.getPageSize());
		if(additionalTextTokens != null) {
			solrSpecification.addToContentFilter(additionalTextTokens);
		}
		solrQueryBuilder.getFilterQueries(solrSpecification);  //content filters from filter are added to contentFilter array
		ProcessTokens(solrSpecification);
		FileHighlightsDto fileHighlightsDto = null;
		try {
			CompletableFuture<FileContentResultMessage> completableFuture = new CompletableFuture<>();
			String requestId = fileContentService.addRequestToLockMap(completableFuture);
			solrSpecification = fileContentService.addFileContentWithSolrSpecification(fileId, itemFullLength, dataSourceRequest, additionalTextTokens, requestId);

			FileContentResultMessage fileContentResultMessage = completableFuture.get(fileGetTimeoutMinutes, TimeUnit.MINUTES);
			if (fileContentResultMessage.getRequestType().equals(FileContentType.ERROR)) {
				throw new BadRequestException(messageHandler.getMessage("file.access.failure",
						Lists.newArrayList(fileContentResultMessage.getPayload().getContent())), BadRequestType.OPERATION_FAILED);
			} else {
				fileHighlightsDto = fileContentService.getHighlightResult(fileContentResultMessage, fileId, itemFullLength, dataSourceRequest, additionalTextTokens, solrSpecification);
			}
		} catch(TimeoutException timeoutException) {
			logger.error("timeout exception while waiting for highlight result",timeoutException);
			if(response != null) {
				response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
			}
		} catch (Exception e) {
			logger.error("Exception during solrFileCatRepository.runQuery. {}", e.getSuppressed(), e);
			throw new BadRequestException(messageHandler.getMessage("file.open.failure"), BadRequestType.OPERATION_FAILED);

		}
		return fileHighlightsDto;
	}

	private void ProcessTokens(SolrSpecification solrSpecification) {
		List<String> contentFilters = solrSpecification.getContentFilters();
		List<String> processedTokens = new ArrayList<>();
		for (String contentToken : contentFilters) {
			//Trying to immitate token analysis chain as defined in solr
			processedTokens.addAll(extractTokenizer.fillTokens(extractTokenizer.filter(contentToken),null));
		}
		solrSpecification.setContentFilters(processedTokens);
	}


	private SolrSpecification createSolrSpecification(@NotNull long fileId,int itemFullLength, @NotNull DataSourceRequest dataSourceRequest,int highlightPageSize) {

		SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrSpecification(dataSourceRequest);
		solrSpecification.setSortField(CatFileFieldType.SORT_NAME.getSolrName());
		solrSpecification.setFileID(fileId);
		solrSpecification.setHighlightItemFullLength(itemFullLength);
		solrSpecification.setHighlightPageSize(highlightPageSize);
		return solrSpecification;
	}

    private PageRequest getPageRequest(DataSourceRequest dataSourceRequest) {
		if (dataSourceRequest.getPageSize() == 0) {
			return null;
		}
		Sort sort;
		if (ArrayUtils.isEmpty(dataSourceRequest.getSort())) {
			//Order the records according to default order TODO - field names should come from Hibernate/JPA and not hardcoded
			Sort.Order baseNameOrder = new Sort.Order(Sort.Direction.ASC,"baseName");
			Sort.Order fileNameOrder= new Sort.Order(Sort.Direction.ASC,"fullPath");
			Sort.Order idOrder = new Sort.Order(Sort.Direction.ASC,"id");
			sort = Sort.by(baseNameOrder,fileNameOrder,idOrder);
		}
		else {
			sort = DataSourceRequest.convertSort(dataSourceRequest, FileDtoFieldConverter::convertDtoField);
		}
		return PageRequest.of(dataSourceRequest.getPage() - 1, dataSourceRequest.getPageSize(),sort);
	}

	private Map<String, List<String>> orderBySheet(final Map<String, List<String>> locations, final String seperator) {
		if (locations == null) {
			return null;
		}

		final Map<String, List<String>> result = new HashMap<>();
		locations.entrySet().forEach(e -> {
			e.getValue().forEach(l -> {
				final String[] split = l.split(seperator);
				if (split.length == 2) {
					List<String> list = result.computeIfAbsent(split[0], k -> Lists.newArrayList());
					list.add(e.getKey() + seperator + split[1]);
				}
			});
		});
		return (result.isEmpty() ? locations : result);
	}
}