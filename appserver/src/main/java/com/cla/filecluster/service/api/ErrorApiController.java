package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.kendo.SortDescriptor;
import com.cla.common.domain.dto.pv.FileProcessingErrorDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.scan.ScanErrorsService;
import com.cla.filecluster.service.files.ingest.IngestErrorService;
import com.cla.filecluster.service.files.managers.OpStateService;
import com.cla.filecluster.service.files.managers.TailerMatchCounter;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("/api")
public class ErrorApiController {

    private static final String ERROR_COUNT = "error";
    private static final String FATAL_COUNT = "fatal";
    private static final String TOTAL_COUNT = "total";
    private static final String RESET_ERROR_OP = "reset";
    private static final String RESET_FATAL_OP = "resetFatal";
    private static final String INC_FATAL_OP = "incFatal";
    private static final String INC_ERROR_OP = "incError";
    private static final String UPDATE_LAST_FATAL_OP = "updateLastFatal";
    private static final String UPDATE_LAST_ERROR_OP = "updateLastError";

    @Autowired
    private OpStateService opStateService;

    @Autowired
    private TailerMatchCounter tailerMatchCounter;

    @Autowired
    private IngestErrorService ingestErrorService;

    @Autowired
    private ScanErrorsService scanErrorsService;

    @Autowired
    private MessageHandler messageHandler;

    @Value(("${api.list-ingest-errors-limit:20000}"))
    private int listIngestErrorsLimit;

    @RequestMapping(value = "/run/logmon/{type}", method = {RequestMethod.GET})
    public Long getTailerMatchCount(@PathVariable String type) {
        if (type == null) {
            throw new BadRequestException(messageHandler.getMessage("match-count.type.empty"), BadRequestType.MISSING_FIELD);
        }

        switch (type) {
            case ERROR_COUNT:
                return tailerMatchCounter.getTailerMatchCount();
            case FATAL_COUNT:
                return tailerMatchCounter.getTailerFatalMatchCount();
            case TOTAL_COUNT:
                return tailerMatchCounter.getTailerTotalMatchCount();
            case RESET_ERROR_OP:
                return tailerMatchCounter.resetTailerErrorMatchCount();
            case RESET_FATAL_OP:
                return tailerMatchCounter.resetTailerFatalMatchCount();
        }

        throw new BadRequestException(messageHandler.getMessage("match-count.type.illegal"), BadRequestType.UNSUPPORTED_VALUE);
    }

    @RequestMapping(value = "/run/logmon/{type}", method = {RequestMethod.POST})
    public void updateMatchCounter(@PathVariable String type, @RequestBody(required = false) String line) {
        if (type == null) {
            throw new BadRequestException(messageHandler.getMessage("match-count.type.empty"), BadRequestType.MISSING_FIELD);
        }

        switch (type) {
            case INC_FATAL_OP:
                tailerMatchCounter.incFatalCount();
                return;
            case INC_ERROR_OP:
                tailerMatchCounter.incErrorCount();
                return;
            case UPDATE_LAST_FATAL_OP:
                if (Strings.isNullOrEmpty(line)) {
                    throw new BadRequestException(messageHandler.getMessage("match-count.line.empty"), BadRequestType.MISSING_FIELD);
                }
                tailerMatchCounter.setTailerLastFatalLine(line);
                return;
            case UPDATE_LAST_ERROR_OP:
                if (Strings.isNullOrEmpty(line)) {
                    throw new BadRequestException(messageHandler.getMessage("match-count.line.empty"), BadRequestType.MISSING_FIELD);
                }
                tailerMatchCounter.setTailerLastErrorLine(line);
                return;
        }

        throw new BadRequestException(messageHandler.getMessage("match-count.type.illegal"), BadRequestType.UNSUPPORTED_VALUE);
    }

    @RequestMapping(value="/ingest/error/types", method = RequestMethod.GET)
    public List<String> getErrorTypes() {
        return ingestErrorService.getErrorTypes();
    }

    @RequestMapping(value = "/ingest/errors/last", method = {RequestMethod.GET})
    public String getLastIngestionError() {
        return opStateService.getLastErrorMsg();
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/ingest/errors", method = {RequestMethod.GET})
    public Page<FileProcessingErrorDto> getIngestionErrors(@RequestParam final Map params) {
        return ingestErrorService.getIngestionErrors(buildPhaseErrorsRequest(params), params);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/ingest/errors/root/{rootFolderId}", method = {RequestMethod.GET})
    public Page<FileProcessingErrorDto> getIngestionErrorsByRootFolder(@PathVariable Long rootFolderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setRootFolderId(rootFolderId);
        return ingestErrorService.getIngestionErrors(dataSourceRequest, params);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/scan/errors", method = {RequestMethod.GET})
    public Page<FileProcessingErrorDto> getScanErrors(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);

        return scanErrorsService.getScanErrors(dataSourceRequest, params);
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/scan/errors/root/{rootFolderId}", method = {RequestMethod.GET})
    public Page<FileProcessingErrorDto> getScanErrorsByRootFolder(@PathVariable Long rootFolderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return scanErrorsService.getScanErrors(dataSourceRequest,params);
    }

    private DataSourceRequest buildPhaseErrorsRequest(Map params) {
        @SuppressWarnings("unchecked")
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.getSort() == null) {
            SortDescriptor sortDescriptor = new SortDescriptor();
            sortDescriptor.setDir(Sort.Direction.DESC.name()); // temporal hard coded sort, until solved in UI
            sortDescriptor.setField("id");
            dataSourceRequest.setSort(new SortDescriptor[]{sortDescriptor});
        }
        if (dataSourceRequest.getPageSize() > listIngestErrorsLimit) {
            dataSourceRequest.setPageSize(listIngestErrorsLimit); // temporal hard stop, until solved in UI
        }
        return dataSourceRequest;
    }
}