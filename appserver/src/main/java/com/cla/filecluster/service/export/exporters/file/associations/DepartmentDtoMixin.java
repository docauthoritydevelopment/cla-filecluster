package com.cla.filecluster.service.export.exporters.file.associations;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DepartmentDtoMixin {

    @JsonProperty(DepartmentHeaderFields.DESCRIPTION)
    private String description;
}
