package com.cla.filecluster.service.export.exporters.settings.department;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.settings.department.DepartmentSettingsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/1/2019
 */
public abstract class DepartmentDtoMixin {

    @JsonProperty(FULL_NAME)
    public abstract String getFullName();

    @JsonProperty(DESCRIPTION)
    private String description;
}
