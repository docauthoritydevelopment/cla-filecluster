package com.cla.filecluster.service.export.exporters.settings.department;

/**
 * Created by: yael
 * Created on: 4/1/2019
 */
public interface DepartmentSettingsHeaderFields {
    String FULL_NAME = "Full_Name";
    String DESCRIPTION = "Description";
}
