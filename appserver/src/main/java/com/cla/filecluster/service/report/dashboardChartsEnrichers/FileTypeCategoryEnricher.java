package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.file.ConstFileTypeCategory;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.filetype.Extension;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.service.categories.FileTypeCategoryService;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class FileTypeCategoryEnricher implements CategoryTypeDataEnricher {

    private static final String FILE_TYPE_CATEGORY_DUMMY_PREFIX = "fileTypeCategory";

    @Autowired
    private FileTypeCategoryService fileTypeCategoryService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.EXTENSION, FILE_TYPE_CATEGORY_DUMMY_PREFIX);
    }

    @Override
    public Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params) {
        HashMap<String, String> ans = new HashMap<>();
        List<FileTypeCategory> fTypeCategories = fileTypeCategoryService.getAll();
        fTypeCategories.forEach((FileTypeCategory fTypeCategory) -> {
            List<String> extNames = fTypeCategory.getExtensions().stream()
                    .map(Extension::getName)
                    .collect(Collectors.toList());
            ans.put(String.valueOf(fTypeCategory.getId()), DashboardChartsUtil.getQueryTypeValueStringAccordingToValuesList(extNames));
        });
        return ans;
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
        List<FileTypeCategory> fTypeCategories = fileTypeCategoryService.getAll();
        Map<Long, FileTypeCategory> fTypeCategoriesMap = fTypeCategories.stream().collect(Collectors.toMap(FileTypeCategory::getId, ftc -> ftc));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return fTypeCategoriesMap.get(Long.parseLong(category)).getName();
        }).collect(Collectors.toList()));
    }

    public String getFilterFieldName() {
        return FileDtoFieldConverter.DtoFields.fileTypeCategoryId.getAlternativeName();
    }


}
