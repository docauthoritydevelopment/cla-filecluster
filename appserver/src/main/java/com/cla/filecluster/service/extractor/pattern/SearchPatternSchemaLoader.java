package com.cla.filecluster.service.extractor.pattern;

import com.cla.common.domain.dto.filesearchpatterns.PatternType;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternDto;
import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternImpl;
import com.cla.filecluster.domain.entity.searchpatterns.PatternCategory;
import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.repository.file.TextSearchPatternImplementationRepository;
import com.cla.filecluster.repository.jpa.searchpattern.TextSearchPatternRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.importEntities.SchemaData;
import com.cla.filecluster.service.importEntities.SchemaLoader;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ophir on 21/04/2019.
 */
@Service
public class SearchPatternSchemaLoader extends SchemaLoader<SearchPatternItemRow,TextSearchPatternDto> {

    @Autowired
    private SearchPatternAppService searchPatternAppService;

    @Autowired
    private PatternCategoryAppService patternCategoryAppService;

    @Autowired
    private RegulationAppService regulationAppService;

    @Autowired
    private TextSearchPatternRepository textSearchPatternRepository;

    @Autowired
    private MessageHandler messageHandler;

    private final static String CSV_HEADER_NAME = "NAME";
    private final static String CSV_HEADER_DESCRIPTION = "DESCRIPTION";
    private final static String CSV_HEADER_ACTIVE = "ACTIVE";
    private final static String CSV_HEADER_PATTERN = "PATTERN";
    private final static String CSV_HEADER_CLASS = "CLASS";
    private final static String CSV_HEADER_CATEGORY = "CATEGORY";
    private final static String CSV_HEADER_SUB_CATEGORY = "SUB_CATEGORY";
    private final static String CSV_HEADER_REGULATIONS = "REGULATIONS";


    private final static String[] REQUIRED_HEADERS = {CSV_HEADER_NAME,CSV_HEADER_DESCRIPTION,CSV_HEADER_ACTIVE,CSV_HEADER_PATTERN,CSV_HEADER_CLASS,CSV_HEADER_CATEGORY,CSV_HEADER_SUB_CATEGORY,CSV_HEADER_REGULATIONS};

    private static final Logger logger = LoggerFactory.getLogger(SearchPatternSchemaLoader.class);

    public SearchPatternSchemaLoader() {
        super(REQUIRED_HEADERS);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public SchemaData<SearchPatternItemRow> createSchemaInstance() {
        return new SchemaData<SearchPatternItemRow>(){
            @Override
            public SearchPatternItemRow createImportItemRowInstance() {
                return new SearchPatternItemRow();
            }
        };
    }

    @Override
    protected SearchPatternItemRow extractRowSpecific(SchemaData<SearchPatternItemRow> schemaData, String[] fieldsValues, ArrayList<String> parsingWarningMessages) {
        SearchPatternItemRow itemRow = new SearchPatternItemRow();
        itemRow.setName(extractValue(schemaData, fieldsValues, CSV_HEADER_NAME, parsingWarningMessages));
        if (itemRow.getName().startsWith("#")) {
            return null;    // Ignore comment
        }
        itemRow.setDescription(extractValue(schemaData, fieldsValues, CSV_HEADER_DESCRIPTION, parsingWarningMessages));
        itemRow.setActive(extractBooleanValue(schemaData, fieldsValues, CSV_HEADER_ACTIVE, parsingWarningMessages));
        itemRow.setPattern(extractValue(schemaData, fieldsValues, CSV_HEADER_PATTERN, parsingWarningMessages));
        itemRow.setValidatorClass(extractValue(schemaData, fieldsValues, CSV_HEADER_CLASS, parsingWarningMessages));
        itemRow.setCategoryName(extractValue(schemaData, fieldsValues, CSV_HEADER_CATEGORY, parsingWarningMessages));
        itemRow.setSubCategoryName(extractValue(schemaData, fieldsValues, CSV_HEADER_SUB_CATEGORY, parsingWarningMessages));
        itemRow.setRegulations(extractMultipleStrings(schemaData, fieldsValues, CSV_HEADER_REGULATIONS, parsingWarningMessages));
        return itemRow;
    }


    @Override
    protected boolean validateItemRow(TextSearchPatternDto textSearchPatternDto, SearchPatternItemRow itemRow,
                                      SchemaData<SearchPatternItemRow> schemaData, boolean afterAddToDB, boolean createStubs) {

        if (Strings.isNullOrEmpty(textSearchPatternDto.getName()) || Strings.isNullOrEmpty(textSearchPatternDto.getName().trim())) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("search-patterns.name.mandatory"));
            return false;
        }

        if (itemRow.getSubCategoryName() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("search-patterns.upload.missing-sub-category"));
            return false;
        }

        if (itemRow.getCategoryName() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("search-patterns.upload.missing-category"));
            return false;
        }

        if (!afterAddToDB) { //validate if already exists only before added to db
            TextSearchPattern tsp = textSearchPatternRepository.findByName(textSearchPatternDto.getName());
            if (tsp != null) {
                schemaData.addDupRowItem(itemRow, messageHandler.getMessage("search-patterns.name.already-exist", Lists.newArrayList(textSearchPatternDto.getName())));
                return false;
            }
        }

        return true;
    }


    @Override
    protected Boolean loadItemRowToRepository(SchemaData<SearchPatternItemRow> schemaData, SearchPatternItemRow itemRow, boolean updateDuplicates, boolean createStubs) {
        if (createStubs) {
            PatternCategory pc = searchPatternAppService.createStubPatternCategoryIfNeeded(itemRow.getCategoryName());
            searchPatternAppService.createStubPatternSubCategoryIfNeeded(itemRow.getSubCategoryName(),pc, false);
            searchPatternAppService.createRegulationsIfNeeded(itemRow.getRegulations());
        }
		TextSearchPatternDto textSearchPatternDto = createDtoFromRowItem(itemRow);
        TextSearchPattern searchPatternByName = searchPatternAppService.getPatternByName(textSearchPatternDto.getName());
        if (searchPatternByName != null){
            textSearchPatternDto.setId(searchPatternByName.getId());
        }
        TextSearchPatternDto resultTextSearchPatternDto = null;
        if (updateDuplicates && searchPatternByName != null ) {
            resultTextSearchPatternDto = searchPatternAppService.updateSearchPattern(textSearchPatternDto.getId(), textSearchPatternDto);
        }
        else if (searchPatternByName == null) {
            resultTextSearchPatternDto = searchPatternAppService.createSearchPattern(textSearchPatternDto);
        }

		if (resultTextSearchPatternDto == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("import.error-insert-row"));
            return false;
        } else {
            return validateItemRow(resultTextSearchPatternDto, itemRow, schemaData, true, createStubs);
        }
    }



    @Override
    protected TextSearchPatternDto createDtoFromRowItem(SearchPatternItemRow itemRow) {
        TextSearchPatternDto textSearchPatternDto = new TextSearchPatternDto();
        textSearchPatternDto.setName(itemRow.getName());
        textSearchPatternDto.setDescription(itemRow.getDescription());
        textSearchPatternDto.setActive(itemRow.isActive());
        String validatorClass = itemRow.getValidatorClass();
        String pattern = itemRow.getPattern();
        if (pattern != null) {
            pattern = pattern.trim();
        }

        if (validatorClass != null && Strings.isNullOrEmpty(pattern)) {
            validatorClass = validatorClass.trim();
            if (!validatorClass.contains(".")) {
                final String packageName = TextSearchPatternDto.class.getPackage().getName();
                validatorClass = packageName + "." + validatorClass;
            }
            //Load from class and extract pattern
            try {
                TextSearchPatternImpl textSearchPatternImpl = TextSearchPatternImplementationRepository.getTextSearchPatternImpl(validatorClass, 0, itemRow.getName(), itemRow.isActive(), null, true);
                pattern = textSearchPatternImpl.getPattern();

            } catch (Exception e) {
                logger.error("Failed to initialize pattern validator class {}",validatorClass,e);
            }
        }
        textSearchPatternDto.setPattern(pattern);
        textSearchPatternDto.setValidatorClass(validatorClass);
        PatternType patternType = itemRow.getValidatorClass() != null ? PatternType.PREDEFINED : PatternType.REGEX;
        textSearchPatternDto.setPatternType(patternType);

        if(!Strings.isNullOrEmpty(itemRow.getCategoryName())) {
            PatternCategory patternCategory = patternCategoryAppService.getPatternCategoryByNameAndParentId(itemRow.getCategoryName(),null);
            if(patternCategory != null) {
                textSearchPatternDto.setCategoryId(patternCategory.getId());
                textSearchPatternDto.setCategoryName(patternCategory.getName());
            }
        }
        if(!Strings.isNullOrEmpty(itemRow.getSubCategoryName()) && textSearchPatternDto.getCategoryId()!= null) {
            PatternCategory patternSubCategory = patternCategoryAppService.getPatternCategoryByNameAndParentId(itemRow.getSubCategoryName(),textSearchPatternDto.getCategoryId());
            if(patternSubCategory != null  && (patternSubCategory.getParent().getId().equals(textSearchPatternDto.getCategoryId()))) {
                textSearchPatternDto.setSubCategoryId(patternSubCategory.getId());
                textSearchPatternDto.setSubCategoryName(patternSubCategory.getName());
                textSearchPatternDto.setSubCategoryActive(patternSubCategory.isActive());
            }
        }

        if(itemRow.getRegulations()!= null) {
            List<Integer> regulationIds = new ArrayList<>();
            Arrays.stream(itemRow.getRegulations()).forEach((String regName) -> {
                Regulation reg = regulationAppService.getRegulationByName(regName);
                if (reg != null) {
                    regulationIds.add(reg.getId());
                }
            });
            if (regulationIds.size() == itemRow.getRegulations().length) {
                textSearchPatternDto.setRegulationIds(regulationIds);
            }
        }

        return textSearchPatternDto;
    }

}
