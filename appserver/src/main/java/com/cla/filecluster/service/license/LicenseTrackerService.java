package com.cla.filecluster.service.license;

import com.cla.filecluster.domain.entity.license.LicenseTracker;
import com.cla.filecluster.repository.jpa.license.LicenseTrackerRepository;
import io.vertx.core.impl.ConcurrentHashSet;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Responsible for setting and getting license trackers with values (Numeric or string)
 * Has a cache layer for serving requests and does async flushing of the trackers to the database
 */
@Service
public class LicenseTrackerService {

    private static final long PERIODIC_FLUSH_INITIAL_DELAY_MILLI = 30 * 1000;
    private static final long PERIODIC_FLUSH_RATE_MILLI = 2 * 60 * 1000;    // Should not be configurable to avoid periodic update cancelation by user

    @Autowired
    private LicenseService licenseService;

    @Autowired
    private LicenseTrackerRepository licenseTrackerRepository;

    private ConcurrentHashMap<String, LicenseTracker> cachedLicenseTrackers = new ConcurrentHashMap<>();
    private ConcurrentHashSet<String> dirtyTrackerIds = new ConcurrentHashSet<>();

    private static final String TRACKER_SIGN_APPENDIX = "fIQmkSQrfSiLqGFtotcy";

    private static Logger logger = LoggerFactory.getLogger(LicenseTrackerService.class);


    @Transactional(readOnly = true)
    @PostConstruct
    private void fillCachedLicenseRestrictions() {
        logger.debug("Load license trackers...");
        List<LicenseTracker> all = licenseTrackerRepository.findAll();

        for (LicenseTracker licenseTracker : all) {
            String signature = generateSignature(licenseTracker);
            if (!licenseTracker.getSignature().equals(signature)) {
                logger.error("Failed to load license tracker - Invalid Signature {}!={}", signature, licenseTracker.getSignature());
            } else {
                cachedLicenseTrackers.put(licenseTracker.getId(), licenseTracker);
            }
        }

        logger.debug("{} license trackers loaded", all.size());
    }

    @PreDestroy
    public void terminate() {
        flushAll(false);
    }

    public String getTrackerValue(String trackerId) {
        return getTrackerValue(trackerId, Function.identity());
    }

    public Long getTrackerLongValue(String trackerId) {
        return getTrackerValue(trackerId, Long::valueOf);
    }

    synchronized
    public Long incrementTrackerLongValue(String trackerId, long value, boolean forceFlush) {
        long tempValue = Optional.ofNullable(getTrackerLongValue(trackerId)).orElse(0L);
        tempValue += value;
        setTrackerLongValue(trackerId, tempValue, forceFlush);
        return tempValue;
    }


    synchronized
    public Double incrementTrackerDoubleValue(String trackerId, double value, boolean forceFlush) {
        double tempValue = Optional.ofNullable(getTrackerDoubleValue(trackerId)).orElse(0D);
        tempValue += value;
        setTrackerDoubleValue(trackerId, tempValue, forceFlush);
        return tempValue;
    }

    public long getTrackerUpdateTime(String trackerId) {
        LicenseTracker lt = cachedLicenseTrackers.get(trackerId);
        return (lt == null) ? 0 : lt.getUpdatedTime();
    }

    @Transactional
    public Long setTrackerLongValue(String trackerId, Long value, boolean forceFlush) {
        return setTrackerValue(trackerId, value.toString(), Long::valueOf, forceFlush);
    }

    @Transactional
    public Double setTrackerDoubleValue(String trackerId, Double value, boolean forceFlush) {
        return setTrackerValue(trackerId, value.toString(), Double::valueOf, forceFlush);
    }

    public String setTrackerValue(String trackerId, String value, boolean forceFlush) {
        return setTrackerValue(trackerId, value, Function.identity(), forceFlush);
    }

    @Transactional
    synchronized
    private <T> T setTrackerValue(String trackerId, String value, Function<String, T> conversion, boolean forceFlush) {
        LicenseTracker newLt = new LicenseTracker(trackerId, value, System.currentTimeMillis());
        LicenseTracker prevVal = cachedLicenseTrackers.put(trackerId, newLt);
        if (prevVal == null || (forceFlush && !prevVal.getValue().equals(value))) {
            // first-setting or forced with val-change
            flushTracker(newLt);
        } else {
            dirtyTrackerIds.add(trackerId);
        }
        if (prevVal == null) {
            return null;
        }
        try {
            return conversion.apply(prevVal.getValue());
        } catch (Exception e) {
            logger.debug("WARN: failed to apply number conversion for tracker {} value={} into long", trackerId, value);
        }
        return null;
    }


    @Transactional
    synchronized
    public boolean flushTracker(String trackerId, boolean forceFlush) {
        if (forceFlush || dirtyTrackerIds.contains(trackerId)) {
            LicenseTracker lt = cachedLicenseTrackers.get(trackerId);
            return flushTracker(lt);
        }
        return true;    // no failure
    }

    @Transactional
    synchronized
    public boolean flushTracker(LicenseTracker tracker) {
        boolean ok = false;
        if (tracker != null) {
            try {
                tracker.setSignature(generateSignature(tracker));
                licenseTrackerRepository.save(tracker);
                logger.debug("Writing license tracker to DB: {}", tracker);
                ok = true;
                dirtyTrackerIds.remove(tracker.getId());        // Maybe best to clear even if save has failed.
            } catch (Exception e) {
                logger.warn("Could not save licenseTracker {}. Error={}", tracker.toString(), e, e);
            }
        }
        return ok;
    }

    @Transactional
    synchronized
    public boolean flushAll(boolean forceFlush) {
        boolean ok = true;
        for (String trackerId : cachedLicenseTrackers.keySet()) {
            ok = ok && flushTracker(trackerId, forceFlush);
        }
        return ok;
    }

    private String generateSignature(LicenseTracker licenseTracker) {
        return generateTrackerSignature(licenseTracker.getId() +
                String.valueOf(licenseTracker.getValue().hashCode()) +
                licenseTracker.getUpdatedTime() +
                licenseService.getServerId());
    }

    private static String generateTrackerSignature(String licensePart) {
        byte[] sha1 = DigestUtils.sha1(licensePart + TRACKER_SIGN_APPENDIX);
        return Base64.encodeBase64String(sha1);
    }

    @Scheduled(initialDelay = PERIODIC_FLUSH_INITIAL_DELAY_MILLI, fixedRate = PERIODIC_FLUSH_RATE_MILLI)
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void periodicFlush() {
        flushAll(false);
    }


    public Double getTrackerDoubleValue(String trackerId) {
        return getTrackerValue(trackerId, Double::valueOf);
    }

    private <T> T getTrackerValue(String trackerId, Function<String, T> conversion) {
        LicenseTracker lt = cachedLicenseTrackers.get(trackerId);
        try {
            return (lt == null) ? null : conversion.apply(lt.getValue());
        } catch (Exception e) {
            logger.debug("WARN: Can not convert tracker {} value={} into long", trackerId, lt.getValue());
        }
        return null;
    }


//    @Scheduled(initialDelay = 65000, fixedRate = 250000)
//    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
//    public void dummyTest() {
//        final String key = "testerDummyKey";
//        Long value = (Long)this.getTrackerLongValue(key);
//        logger.debug("DEB: License tracker {}={}", key, value);
//        Long newVal = (long)(Math.random()*100000f);
//        value = (Long)this.setTrackerLongValue(key, newVal, false);
//        logger.debug("DEB: Setting license tracker {}={}", key, newVal);
//    }

}
