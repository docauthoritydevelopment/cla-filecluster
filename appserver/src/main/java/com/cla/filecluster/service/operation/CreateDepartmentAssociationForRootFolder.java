package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.department.DepartmentAppService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 9/12/2019
 */
@Service
public class CreateDepartmentAssociationForRootFolder implements ScheduledOperation {

    public static String ROOT_FOLDER_ID_PARAM = "ROOT_FOLDER_ID";

    @Autowired
    private DepartmentAppService departmentAppService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            Long rootFolderId = Long.parseLong(opData.get(ROOT_FOLDER_ID_PARAM));
            departmentAppService.handleDepartmentAssociationForRootFolder(rootFolderId);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String max = opData.get(ROOT_FOLDER_ID_PARAM);
        if (Strings.isNullOrEmpty(max)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.root-folder.empty"), BadRequestType.MISSING_FIELD);
        }
        Long.parseLong(max);
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.CREATE_DEPARTMENT_ASSOCIATIONS_FOR_ROOT_FOLDER;
    }

    @Override
    public boolean operationAllowedToRun(ScheduledOperationMetadata operationMetadata) {
        Map<String, String> opData = operationMetadata.getOpData();
        Long rootFolderId = Long.parseLong(opData.get(ROOT_FOLDER_ID_PARAM));
        DocFolder rfFolder = docFolderService.getFolderOfRootFolder(rootFolderId);
        return (rfFolder != null);
    }

    @Override
    public boolean shouldAllowCreateWhileScheduleDown() {
        return true;
    }
}
