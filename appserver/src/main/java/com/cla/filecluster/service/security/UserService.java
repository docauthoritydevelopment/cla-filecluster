package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.security.*;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.CompositeAuthenticationProvider;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.security.*;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.RoleRepository;
import com.cla.filecluster.repository.jpa.security.UserRepository;
import com.cla.filecluster.repository.jpa.security.UserSavedDataRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class UserService {

    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    private static final ThreadLocal<String> CURRENT_USER_DOMAIN_HOLDER = new ThreadLocal<>();

    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    @Autowired
    private AuthenticationProvider authenticationProvider;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BizRoleService bizRoleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserSavedDataRepository userSavedDataRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${ldap.default.role-name:viewer}")
    private String defaultRoleName;

    @Value("${security.login.max-failures:0}")
    private int maxUserFailedLogins;

    @Value("${security.login.auto-unlock.time-sec:0}")
    private long autoUnlockTimeSec;

    @Value("${security.login.auto-unlock.reset-failure-counter:false}")
    private boolean resetLoginFailureCounterOnUnlock;

    @Value("${security.login.failures.users-lock-bypass:}")
    private String[] lockBypassUsers;

    private Set<String> lockBypassUsersSet;

    @Value("${test.users.default.operator:false}")
    private boolean createTestOperatorUser;

    @Value("${test.users.default.roles:admin,operator}")
    private String[] testUserRoles;

    @Value("${accountsAdminProps}")
    private String[] accountsAdminProps;

    @Value("${accountsAdminRoles:admin}")
    private String[] accountsAdminRoles;

    private static final ClaUserDetails systemUser =
            new ClaUserDetails(new UserDto("System", "system.user", "", "system"));

    @PostConstruct
    public void init() {
        lockBypassUsersSet = (lockBypassUsers == null || lockBypassUsers.length == 0) ? Collections.EMPTY_SET :
                Stream.of(lockBypassUsers).collect(Collectors.toSet());
        if (!lockBypassUsersSet.isEmpty()) {
            logger.warn("Lock bypass usernames: {}", lockBypassUsersSet.stream().collect(Collectors.joining(", ")));
        }
        if (autoUnlockTimeSec > 0) {
            logger.warn("Auto user unlock time set to {} seconds. reset failure-counter={}", autoUnlockTimeSec, resetLoginFailureCounterOnUnlock ? "on" : "off");
        }
        ClaUserDetails.setAutoUnlockTimeSec(autoUnlockTimeSec);
        if (maxUserFailedLogins > 0) {
            logger.warn("Max consecutive login failures set to {}", maxUserFailedLogins);
        }
    }

    public UserDto getCurrentUser() {
        Optional<ClaUserDetails> currentUserDetailsOp = getCurrentUserDetails();
        if (currentUserDetailsOp.isPresent()) {
            return currentUserDetailsOp.get().getUser();
        }
        return null;
    }

    public boolean isUserAdmin(User user) {
        if (user.getUserType().equals(UserType.SYSTEM)) {
            return true;
        }

        if (user.getRoleName().equals(accountsAdminRoles)) {
            return true;
        }
        return false;
    }

    public String getPrincipalUsername() {
        return getCurrentUserDetails().orElse(systemUser).getUsername();
    }

    public static Optional<ClaUserDetails> getCurrentUserDetails() {
        if (SecurityContextHolder.getContext() == null ||
                SecurityContextHolder.getContext().getAuthentication() == null ||
                SecurityContextHolder.getContext().getAuthentication().getPrincipal() == null) {
            return Optional.empty();
        }
        final Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof ClaUserDetails) {
            return Optional.of((ClaUserDetails) principal);
        }
        return Optional.empty();
    }


    @Transactional(readOnly = true)
    public List<User> getAllUsers() {
        return userRepository.findAllNotDeleted();
    }

    @Transactional(readOnly = true)
    public Page<User> getAllNonSystemUsers(PageRequest pageRequest) {
        return userRepository.findAllNonSystem(pageRequest);
    }

    @Transactional(readOnly = true)
    public User getUserByIdWithRoles(final long userId, boolean fetchDeleted) {
        if (fetchDeleted) {
            return userRepository.findByIdWithRolesWithDeleted(userId);
        }
        return userRepository.findByIdWithRoles(userId);
    }

    @Transactional(readOnly = true)
    public User getUserById(final long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    @Transactional(readOnly = true)
    public User getUserByName(final String userName) {
        return userRepository.findUserByUsername(userName);
    }

    @Transactional
    public void saveLastLdapForUser(Long userId, Long lastLdapId) {
        User user = getUserById(userId);
        if (user != null) {
            user.setLastLdapFound(lastLdapId);
            userRepository.save(user);
        }
    }

    @Transactional(readOnly = true)
    public UserDto getUserDtoByName(final String userName) {
        return convertUser(userRepository.findUserByUsername(userName), true);
    }

    @Transactional(readOnly = true)
    public UserDto getUserDtoByNameWithTokens(final String userName) {
        return convertUser(userRepository.findUserByUsername(userName), true, true);
    }

    private User createUser(User user, BizRole bizRole) {
        return createUser(user, Collections.singletonList(bizRole));
    }

    private User createUser(User user, List<BizRole> bizRoles) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setLastPasswordChange(new Date());
        if (user.getUserState() == null) {
            user.setUserState(UserState.ACTIVE);
        }
        if (bizRoles != null && !bizRoles.isEmpty()) {
            user.setBizRoles(Sets.newHashSet(bizRoles));
            user.setRoleName(bizRoles.get(0).getName());
        } else {
            user.setRoleName(user.getRoleName() != null ? user.getRoleName() : "user");
        }
//        newUserRoles().forEach(r->user.addRole(r));
//        updateUserRoleConfig(user);
        user = userRepository.save(user);
        logger.debug("Created User {}", user);
        return user;
    }

    public static boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    String validEmail(String email) {
        if (email == null) return null;

        email = email.trim();

        if (email.isEmpty()) {
            return null;
        }

        if (!isValidEmailAddress(email)) {
            throw new BadRequestException(messageHandler.getMessage("user.email.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        return email;
    }

    private void validatePassword(String password) {
        if (Strings.isNullOrEmpty(password) || password.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("user.password.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void validateName(String name) {
        if (Strings.isNullOrEmpty(name) || name.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("user.name.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    private void validateUsername(String username) {
        if (Strings.isNullOrEmpty(username) || username.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("user.user-name.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Transactional
    public User createUserOnCurrentAccount(final UserDto userDto) {
        if (userDto.getId() != null) {
            throw new RuntimeException("Can't create a user with not null id");
        }
        validateName(userDto.getName());
        validatePassword(userDto.getPassword());
        String username = userDto.getUsername();
        validateUsername(username);
        Arrays.stream(LdapDomainDelim.values())
                .map(LdapDomainDelim::toString)
                .filter(username::contains)
                .findAny().ifPresent(ldapDomainDelim -> {
            throw new BadRequestException(messageHandler.getMessage("user.user-name.invalid",
                    Lists.newArrayList(ldapDomainDelim)), BadRequestType.UNSUPPORTED_VALUE);
        });
        userDto.setEmail(validEmail(userDto.getEmail()));

        User user;
        User userByUsername = userRepository.findUserByUsername(username);
        if (userByUsername != null) {
            if (userByUsername.isDeleted()) {
                user = userByUsername;
                user.setDeleted(false);
            } else {
                throw new BadRequestException(messageHandler.getMessage("user.user-name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
            }
        } else {
            user = new User();
        }
        user.setUsername(username);
        user.setName(userDto.getName());
        user.setPassword(userDto.getPassword());
        user.setLastPasswordChange(new Date());
        user.setUserType(UserType.INTERNAL);
        user.setEmail(userDto.getEmail());
        user.setUserState(userDto.getUserState());
        user.setAccountNonExpired(userDto.isAccountNonExpired());
        user.setAccountNonLocked(userDto.isAccountNonLocked());
        user.setCredentialsNonExpired(userDto.isCredentialsNonExpired());
        user.setConsecutiveLoginFailuresCount(userDto.getConsecutiveLoginFailuresCount());
//        UserRole userRole = userDto.getUserRole() == null ? UserRole.USER : userDto.getUserRole();
//        user.setUserRole(userRole);
        return createUser(user, (List<BizRole>) null);
    }


    @Transactional
    public User updateUser(final long id, final UserDto userDto) {
        if (Strings.isNullOrEmpty(userDto.getName()) || userDto.getName().trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("user.name.empty"), BadRequestType.MISSING_FIELD);
        }
        userDto.setEmail(validEmail(userDto.getEmail()));

        User user = userRepository.getOne(id);
//        user.setUsername(userDto.getUsername());
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setUserState(userDto.getUserState());
        user.setUserType(userDto.getUserType());
        user.setAccountNonLocked(userDto.isAccountNonLocked());
        //   user.setAccountNonExpired(userDto.isAccountNonExpired());

//        user.setUserRole(userDto.getUserRole());
//        updateUserRoleConfig(user);
        return userRepository.save(user);
    }

    @Transactional
    public User unlockUser(final long id) {
        logger.debug("Unlock User {}", id);
        User user = userRepository.getOne(id);
        user.setAccountNonLocked(true);
        return userRepository.save(user);
    }

    @Transactional
    public User updateCurrentUser(final UserDto userDto) {

        if (Strings.isNullOrEmpty(userDto.getName()) || userDto.getName().trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("user.name.empty"), BadRequestType.MISSING_FIELD);
        }
        userDto.setEmail(validEmail(userDto.getEmail()));

        User user = userRepository.getOne(getCurrentUser().getId());
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());

        return userRepository.save(user);
    }

    @Transactional
    public void deleteUserById(final long userId) {
        final User accountsAdmin = getAccountsAdmin();
        if (userId == accountsAdmin.getId()) {
            logger.warn("User requested to delete account admin - illegal request");
            throw new BadParameterException(messageHandler.getMessage("user.admin.delete.failure"), BadRequestType.UNAUTHORIZED);
        }
        try {
            userRepository.deleteById(userId);
        } catch (RuntimeException e) {
            logger.error("Failed to delete user {}", userId, e);
            throw e;
        }
    }

    @Transactional
    public UserDto archiveUserById(final long userId) {
        final User accountsAdmin = getAccountsAdmin();
        if (userId == accountsAdmin.getId()) {
            logger.warn("User requested to archive account admin - illegal request");
            throw new BadParameterException(messageHandler.getMessage("user.admin.delete.failure"), BadRequestType.UNAUTHORIZED);
        }
        try {
            User one = userRepository.getOne(userId);
            logger.info("Archive user {}", one);
            one.setDeleted(true);
            one.setUserState(UserState.DISABLED);
            User save = userRepository.save(one);
            return convertUser(save, false);
        } catch (RuntimeException e) {
            logger.error("Failed to delete user {}", userId, e);
            throw e;
        }
    }

    @Transactional
    public void addRoleToUser(final long userId, final int roleId) {
        final User user = userRepository.findById(userId).orElse(null);
        final Role role = roleRepository.findById(roleId).orElse(null);
        if (user != null && role != null) {
            user.addRole(role);
            userRepository.save(user);
        } else {
            logger.warn("Role {} or user {} doesn't exist", role, user);
        }
    }

    @Transactional
    public void removeRoleFromUser(final long userId, final int roleId) {
        final User user = userRepository.findById(userId).orElse(null);
        final Role role = roleRepository.findById(roleId).orElse(null);
        if (user != null && role != null) {
            user.removeRole(role);
            userRepository.save(user);
        } else {
            logger.warn("Role {} or user {} doesn't exist", role, user);
        }
    }

    @Transactional(readOnly = true)
    public User getCurrentUserWithRoles() {
        return userRepository.findByIdWithRoles(getCurrentUser().getId());
    }

    @Transactional
    public void resetUserPassword(long userId, final String newPassword) {
        final String encodedNewPassword = passwordEncoder.encode(newPassword);
        userRepository.changePassword(userId, encodedNewPassword);
    }

    @Transactional
    public void changeCurrentUserPassword(final String newPassword) {
        changeCurrentUserPassword(newPassword, null);
    }

    @Transactional
    public void changeCurrentUserPassword(final String newPassword, final String oldPassword) {
        if (oldPassword != null) {
            final String encodedOldPassword = userRepository.getPasswordById(getCurrentUser().getId());
            if (!passwordEncoder.matches(oldPassword, encodedOldPassword)) {
                logger.warn("Change curr user password for user '{}' old password mismatch.", getCurrentUser().getUsername());
                throw new RuntimeException(messageHandler.getMessage("user.password.wrong"));
            }
        }
        final String encodedNewPassword = passwordEncoder.encode(newPassword);
        userRepository.changePassword(getCurrentUser().getId(), encodedNewPassword);
    }

    @Transactional
    public void addUserSavedData(final String key, String savedData) {
        logger.debug("add User SavedData on key {}", key);
        User currentUser = getCurrentUserEntity();
        addUserSavedData(key, savedData, currentUser);
    }

    @Transactional(readOnly = true)
    public User getCurrentUserEntity() {
        UserDto currentUserDto = getCurrentUser();
        return currentUserDto == null || currentUserDto.getId() == null ? null : getUserById(currentUserDto.getId());
    }

    public boolean isUserSupportAuthority(Authority checkAuthority) {
        User owner = getCurrentUserEntity();
        if (owner == null || owner.getBizRoles() == null) {
            return false;
        }
        return owner.getBizRoles().stream().anyMatch((bizRole)-> {
            return bizRole.getTemplate().getRoles().stream().anyMatch((role) -> {
                return role.getAuthorities().contains(checkAuthority);
            });
        });
    }

    private UserSavedData addUserSavedData(String key, String savedData, User user) {
        List<UserSavedData> byUserAndKey = userSavedDataRepository.findByUserAndKey(user.getUsername(), key);
        if (byUserAndKey.size() > 0) { // Delete previous copies
            logger.debug("Override existing data on key");
            byUserAndKey.get(0).setData(savedData);
            return userSavedDataRepository.save(byUserAndKey.get(0));
        }
        UserSavedData userSavedData = new UserSavedData();
        userSavedData.setUser(user);
        userSavedData.setData(savedData);
        userSavedData.setKey(key);
        return userSavedDataRepository.save(userSavedData);
    }

    @Transactional(readOnly = true)
    public UserSavedData getUserSavedData(final String key) {
        User currentUser = getCurrentUserEntity();
        String username = currentUser.getUsername();
        return getUserSavedData(key, username);
    }

    private UserSavedData getUserSavedData(String key, String username) {
        List<UserSavedData> byUserAndKey = userSavedDataRepository.findByUserAndKey(username, key);
        if (byUserAndKey.size() > 0) {
            return byUserAndKey.get(0);
        }
        return null;
    }

    @Transactional
    public void deleteUserSavedData(final String key, String savedData) {
        User currentUser = getCurrentUserEntity();
        List<UserSavedData> byUserAndKey = userSavedDataRepository.findByUserAndKey(currentUser.getUsername(), key);
        userSavedDataRepository.deleteAll(byUserAndKey);
    }

    @Transactional
    public User createAccountsAdmin() {
        User user1 = new User(accountsAdminProps[0], accountsAdminProps[1], accountsAdminProps[2], accountsAdminProps[3],
                Sets.newHashSet());

        List<BizRole> adminBizRoles = Stream.of(accountsAdminRoles)
                .map(roleStr -> bizRoleService.getBizRoleByName(roleStr)).filter(Objects::nonNull).collect(Collectors.toList());

        BizRole defaultBizRole = bizRoleService.getBizRoleByName(defaultRoleName);
        if (defaultBizRole != null && !adminBizRoles.contains(defaultBizRole)) {
            adminBizRoles.add(defaultBizRole);
        }
        return createUser(user1, adminBizRoles);
    }

    @Transactional
    public void createAccountAdditionalUsers(BizRole bizRole) {
        logger.debug("create Additional Users");
        User user2 = new User("scheduleUser", UserConsts.SCHEDULE_INTERNAL_USER, UserConsts.SCHEDULE_IU_PASSWORD, "schedule");
        user2.setUserType(UserType.SYSTEM);
        createUser(user2, bizRole);

        if (createTestOperatorUser) {
            List<BizRole> testBizRoles = Stream.of(testUserRoles)
                    .map(roleStr -> bizRoleService.getBizRoleByName(roleStr)).filter(Objects::nonNull).collect(Collectors.toList());
            User viewer = new User("operator", "operator", "ZaSxCdFv123", null);
            viewer.setUserType(UserType.INTERNAL);
            createUser(viewer, testBizRoles);
        }
    }

    @Transactional(readOnly = true)
    public User getScheduleUser() {
        return userRepository.findUserByUsername("scheduleInternalUser");
    }

    @Transactional(readOnly = true)
    public UserDto getScheduleUserDto() {
        User user = userRepository.findUserByUsername("scheduleInternalUser");
        return convertUser(user, true);
    }

    @Transactional(readOnly = true)
    public User getAccountsAdmin() {
        return userRepository.findUserByUsername(accountsAdminProps[1]);
    }

    @Transactional(readOnly = true)
    public UserDto getAccountsAdminDto() {
        User adminUser = userRepository.findUserByUsername(accountsAdminProps[1]);
        return convertUser(adminUser, true);
    }


    @Transactional
    public void addSystemRolesToUser(Long userId, Set<Role> newSystemRoles, boolean override) {
        User user = userRepository.findByIdWithRoles(userId);
        logger.debug("Add SystemRoles {} to user {}", newSystemRoles, user);
        Set<Role> roles = user.getRoles();
        if (roles == null || override) {
            roles = new HashSet<>();
        }
        roles.addAll(newSystemRoles);
        user.setRoles(roles);
        userRepository.save(user);
    }

    @Transactional
    public void addBizRolesToUser(long userId, Set<BizRole> newBizRoles, boolean override) {
        User user = userRepository.findByIdWithRoles(userId);
        logger.debug("Add BizRole {} to user {}", newBizRoles, user);
        Set<BizRole> bizRoles = user.getBizRoles();
        if (bizRoles == null || override) {
            bizRoles = new HashSet<>();
        }

        BizRole defaultBizRole = bizRoleService.getBizRoleByName(defaultRoleName);
        if (defaultBizRole != null && !bizRoles.contains(defaultBizRole)) {
            bizRoles.add(defaultBizRole);
        }

        bizRoles.addAll(newBizRoles);
        user.setBizRoles(bizRoles);
        userRepository.save(user);
    }


    @Transactional
    public UserDto addBizRoleToUser(long userId, BizRole bizRole) {
        User user = userRepository.findByIdWithRoles(userId);
        logger.debug("Add BizRole {} to user {}", bizRole, user);
        Set<BizRole> bizRoles = user.getBizRoles();
        if (bizRoles == null) {
            bizRoles = Sets.newHashSet();
        }
        bizRoles.add(bizRole);

        user.setBizRoles(bizRoles);
        User save = userRepository.save(user);
        return convertUser(save, false);
    }

    @Transactional
    public UserDto removeBizRoleFromUser(long userId, BizRole bizRole) {
        User user = userRepository.findByIdWithRoles(userId);
        logger.debug("Remove BizRole {} from user {}", bizRole, user);
        Set<BizRole> bizRoles = user.getBizRoles();
        if (bizRoles == null) {
            bizRoles = new HashSet<>();
        }
        bizRoles.remove(bizRole);
        user.setBizRoles(bizRoles);
        userRepository.save(user);
        return convertUser(user, false);

    }

    @Transactional
    public UserDto addWorkGroupToUser(long userId, WorkGroup workGroup) {
        User user = userRepository.findByIdWithRoles(userId);
        logger.debug("Add WorkGroup {} to user {}", workGroup, user);
        Set<WorkGroup> workGroups = user.getWorkGroups();
        if (workGroups == null) {
            workGroups = Sets.newHashSet();
        }
        workGroups.add(workGroup);
        user.setWorkGroups(workGroups);
        User save = userRepository.save(user);
        return convertUser(save, false);
    }

    @Transactional
    public UserDto removeWorkGroupFromUser(long userId, WorkGroup workGroup) {
        User user = userRepository.findByIdWithRoles(userId);
        logger.debug("Remove WorkGroup {} from user {}", workGroup, user);
        Set<WorkGroup> workGroups = user.getWorkGroups();
        if (workGroups == null) {
            workGroups = new HashSet<>();
        }
        workGroups.remove(workGroup);
        user.setWorkGroups(workGroups);
        userRepository.save(user);
        return convertUser(user, false);
    }

    @Transactional(readOnly = true)
    public Page<User> getCompRoleAssignedUsers(Long compRoleId, PageRequest pageRequest) {
        return userRepository.getCompRoleAssignedUsers(compRoleId, pageRequest);
    }

    public String getCurrentUserDomain() {
        return CURRENT_USER_DOMAIN_HOLDER.get();
    }

    public void setCurrentUserDomain(String currentUserDomain) {
        CURRENT_USER_DOMAIN_HOLDER.set(currentUserDomain);
    }

    @Transactional
    public User createLdapUserIfAbsentInCurrentDomain(String name, String username, Set<CompositeRole> daRoles,
                                                      Set<String> accessTokens) {
        // TODO: add a scheduled job that purges users that didn't login after a month
        String currentUserDomain = CURRENT_USER_DOMAIN_HOLDER.get();
        logger.debug("Retrieved currentUserDomain: {}", currentUserDomain);
        username = concatUsernameWithDomain(username, currentUserDomain);
        logger.debug("Concatenated : {}", currentUserDomain);
        User user = userRepository.findUserByUsername(username);
        if (user == null) {
            user = new User(name, username, null, "operator");
        } else if (!user.isLdapUser()) {
            logger.warn("Ldap user {} already exists as local user. Saving it as ldap user.", username);
            user.setPassword(null);
        }
        if (user.isDeleted()) {
            logger.info("Undeleting LDAP user {} which was previously deleted", username);
            user.setDeleted(false);
            user.setUserState(UserState.ACTIVE);
            eventAuditService.audit(AuditType.USER, "Undelete user", AuditAction.UNDELETE, UserDto.class, user.getId(), user);
        }
        setUserDARoles(user, daRoles);
        user.setLdapUser(true);

        if (logger.isDebugEnabled()) {
            if (accessTokens.stream().filter(at->(at != null && at.contains("\""))).findAny().isPresent()) {
                logger.warn("Ldap user {} has groups with double-quotes in name: {}",
                        username,
                        accessTokens.stream().filter(at->(at != null && at.contains("\""))).collect(Collectors.joining(",")));
            }
        }

        user.setAccessTokens(accessTokens);
        userRepository.save(user);
        return user;
    }

    private String concatUsernameWithDomain(String username, String currentUserDomain) {
        if (currentUserDomain.contains("=")) {
            return currentUserDomain + LdapDomainDelim.BACKSLASH + username;
        } else {
            return username + LdapDomainDelim.AT + currentUserDomain;
        }
    }

    /**
     * Splits the given daRoles to BizRoles and FunctionalRoles and sets those to the given user.
     * <p>
     * Note: this method assumes that any CompositeRole that is not an {@code instanceof} BizRole is a FunctionalRole.
     *
     * @param user    the user to which to set the BizRoles and FunctionalRoles
     * @param daRoles the set of CompositeRoles that contains the BizRoles and FunctionalRoles to set
     */
    private void setUserDARoles(User user, Set<CompositeRole> daRoles) {

        Map<Boolean, List<CompositeRole>> bizAndFuncRoles =
                daRoles.stream().collect(Collectors.partitioningBy(o -> o instanceof BizRole));

        Set<BizRole> bizRoles =
                bizAndFuncRoles.get(true).stream().
                        map(compositeRole -> (BizRole) compositeRole).
                        collect(Collectors.toSet());

        BizRole defaultBizRole = bizRoleService.getBizRoleByName(defaultRoleName);
        if (defaultBizRole != null && !bizRoles.contains(defaultBizRole)) {
            bizRoles.add(defaultBizRole);
        }

        Set<FunctionalRole> funcRoles =
                bizAndFuncRoles.get(false).stream().
                        map(compositeRole -> (FunctionalRole) compositeRole).
                        collect(Collectors.toSet());

        user.setBizRoles(bizRoles);
        logger.trace("The user {} has the following biz-roles: [{}].", user.getUsername(),
                bizRoles.stream().map(BizRole::getName).collect(Collectors.joining(",")));
        user.setFunctionalRoles(funcRoles);
        logger.trace("The user {} has the following functional roles: [{}].", user.getUsername(),
                funcRoles.stream().map(FunctionalRole::getName).collect(Collectors.joining(",")));
    }

    @Transactional
    public void markLoginSuccess() {
        User user = getUserByIdWithRoles(getCurrentUser().getId(), false);
        logger.info("Successful login for user {}. Clearing ConsecutiveLoginFailuresCount (was {})", user.getUsername(), user.getConsecutiveLoginFailuresCount());
        user.setConsecutiveLoginFailuresCount(0);
        user.setLastSuccessfulLogin(new Date());
        if (!user.isAccountNonLocked()) {
            logger.info("Auto user unlock for {}. Last failed login at {}", user.getUsername(), DATE_FORMAT.format(user.getLastFailedLogin()));
            eventAuditService.audit(AuditType.LOGIN, "Auto unlock", AuditAction.AUTO, User.class, user.getId(), user);
            user.setAccountNonLocked(true);
            removeUserFromCache(user.getUsername());
        }
        userRepository.save(user);
    }

    private void removeUserFromCache(String username) {
        ((CompositeAuthenticationProvider) authenticationProvider).removeUserFromAuthenticationCache(username);
    }

    @Transactional
    public boolean markLoginFailure(String principalName, Object principal, Object authenticationDetails) {
        User user = userRepository.findUserByUsername(principalName);
        if (user == null) {
            String userDetails = "";
            if (principal != null && principal instanceof ClaUserDetails) {
                userDetails = ((ClaUserDetails)principal).getUsername();
            }
            logger.info("Failed login for unknown user {}; userDetails: {}; details: {}",
                    principalName, userDetails,
                    (authenticationDetails==null?"null":authenticationDetails.toString()));
            return false;
        }
        user.setConsecutiveLoginFailuresCount(1 + user.getConsecutiveLoginFailuresCount());
        if (!user.isAccountNonLocked() && user.getLastFailedLogin() != null) {
            if (resetLoginFailureCounterOnUnlock && ClaUserDetails.isLastFailureTimeoutPassed(user.getLastFailedLogin().getTime())) {
                logger.info("Reset login failure counter for {}. Last failed login at {}", user.getUsername(), DATE_FORMAT.format(user.getLastFailedLogin()));
                eventAuditService.audit(AuditType.LOGIN, "Reset login failure counter", AuditAction.AUTO, User.class, user.getId(), user.getUsername());
                user.setConsecutiveLoginFailuresCount(0);
            }
        }
        user.setLastFailedLogin(new Date());
        if (maxUserFailedLogins > 0 && user.getConsecutiveLoginFailuresCount() >= maxUserFailedLogins) {
            if (lockBypassUsersSet.contains(principalName)) {
                logger.warn("Failed login for user {}. Account set as lockBypass!!! ConsecutiveLoginFailuresCount={}", user.getUsername(), user.getConsecutiveLoginFailuresCount());
            } else if (user.isAccountNonLocked()) {
                logger.info("Failed login for user {}. Locking user as ConsecutiveLoginFailuresCount={}", user.getUsername(), user.getConsecutiveLoginFailuresCount());
                user.setAccountNonLocked(false);
                removeUserFromCache(user.getUsername());
            } else {
                logger.info("Failed login for locked user {}. ConsecutiveLoginFailuresCount={}", user.getUsername(), user.getConsecutiveLoginFailuresCount());
            }
        } else {
            logger.info("Failed login for user {}. ConsecutiveLoginFailuresCount={}", user.getUsername(), user.getConsecutiveLoginFailuresCount());
        }
        userRepository.save(user);
        return true;
    }

    /**
     * LDAP Domain Delimiter
     * <p>
     * When users login they enter their username with the domain in one string, usernameWithDomain.
     * usernameWithDomain can be in one of two forms:
     * Either "username@dot.delimited.domain" or "OU=orgUnit,DC=domainComp2,DC=domainComp1\ username"
     */
    public enum LdapDomainDelim {
        AT("@"),
        BACKSLASH("\\");

        private final String delim;

        LdapDomainDelim(String delim) {
            this.delim = delim;
        }

        @Override
        public String toString() {
            return delim;
        }

        public static LdapDomainDelim of(String delim) {
            switch (delim) {
                case "@":
                    return AT;
                case "\\":
                    return BACKSLASH;
                default:
                    return null;
            }
        }
    }

    public static UserDto convertUser(User user, boolean includePassword) {
        return convertUser(user, includePassword, false);
    }

    public static UserDto convertUser(User user, boolean includePassword, boolean withAccessTokens) {
        if (user == null) {
            return null;
        }

        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setName(user.getName());
        userDto.setRoleName(user.getRoleName());
        userDto.setUsername(user.getUsername());
        userDto.setUserType(user.getUserType());
        userDto.setEmail(user.getEmail());
        userDto.setSystemRoleDtos(RoleService.convertRolesToSet(user.getRoles()));
        userDto.setLdapUser(user.isLdapUser());
        userDto.setBizRoleDtos(new HashSet<>(BizRoleService.convertBizRoles(user.getBizRoles().stream()
                .filter(b -> !b.isDeleted()).collect(Collectors.toSet()))));
        userDto.setFuncRoleDtos(new HashSet<>(FunctionalRoleService.convertFunctionalRoles(user.getFunctionalRoles().stream()
                .filter(b -> !b.isDeleted()).collect(Collectors.toSet()))));
        userDto.setWorkGroupDtos(new HashSet<>(WorkGroupService.convertWorkGroups(user.getWorkGroups())));

        // userDto.setSystemRoleDtos(sysRoles);

        if (withAccessTokens) {
            userDto.setAccessTokens(user.getAccessTokens());
            if (user.getAccessTokens() != null) {
                userDto.getAccessTokens().addAll(user.getAccessTokens());
            }
        }

        if (user.getLastPasswordChange() != null) {
            userDto.setLastPasswordChangeTimeStamp(user.getLastPasswordChange().getTime());
        }
        userDto.setPasswordMustBeChanged(user.isPasswordMustBeChanged());
        userDto.setLastSuccessfulLogin(user.getLastSuccessfulLogin());
        userDto.setLastFailedLogin(user.getLastFailedLogin());
        userDto.setConsecutiveLoginFailuresCount(user.getConsecutiveLoginFailuresCount());
        userDto.setAccountNonExpired(user.isAccountNonExpired());
        userDto.setAccountNonLocked(user.isAccountNonLocked());
        userDto.setCredentialsNonExpired(user.isCredentialsNonExpired());

        userDto.setUserState(user.getUserState());
        userDto.setDeleted(user.isDeleted());
        if (includePassword) {
            userDto.setPassword(user.getPassword());
        }
//        userDto.setUserRole(user.getUserRole());
        return userDto;
    }

    public static Page<UserDto> convertUsersPage(Page<User> allNonSystemUsers) {
        List<UserDto> content = convertUsers(allNonSystemUsers.getContent());
        Pageable pageable = PageRequest.of(allNonSystemUsers.getNumber(), allNonSystemUsers.getSize());
        return new PageImpl<>(content, pageable, allNonSystemUsers.getTotalElements());
    }

    private static List<UserDto> convertUsers(List<User> allUsers) {
        List<UserDto> result = new ArrayList<>();
        for (User user : allUsers) {
            result.add(convertUser(user, false));
        }

        return result;
    }
}
