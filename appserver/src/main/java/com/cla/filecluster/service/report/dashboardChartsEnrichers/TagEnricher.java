package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.repository.solr.FacetType;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class TagEnricher implements DefaultTypeDataEnricher {

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String convertSolrValueToId(String value) {
        return AssociationIdUtils.extractTagIdFromScopedSolrIdentifier(value);
    }

    @Override
    public void enrichJSONFacetQuery(SolrFacetQueryJson solrFacetQueryJson, FilterDescriptor filter) {
        List<Object> tagTypeFilters = dashboardChartsUtil.getValuesByFieldFromFilter(filter, FileDtoFieldConverter.DtoFields.tagType.getAlternativeName());
        if (tagTypeFilters.size() == 1) {
            String tagTypeFilter = String.valueOf(tagTypeFilters.get(0));
            solrFacetQueryJson.setPrefix(FileTagDto.ID_PREFIX + tagTypeFilter + ".");
        }
    }


    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            FileTagDto fileTagDto = fileTagService.getFromCache(Long.parseLong(convertSolrValueToId(category)));
            return fileTagDto.getType().getName() + "/" + fileTagDto.getName();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.ACTIVE_ASSOCIATIONS, FileTagDto.ID_PREFIX);
    }

}
