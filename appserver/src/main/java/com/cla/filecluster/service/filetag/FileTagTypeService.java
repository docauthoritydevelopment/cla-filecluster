package com.cla.filecluster.service.filetag;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.filetag.FileTagAction;
import com.cla.common.domain.dto.filetag.FileTagTypeDto;
import com.cla.common.domain.dto.scope.Scope;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.filetag.FileTagTypeRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 29/09/2015.
 */
@Service
public class FileTagTypeService {
    private static final Logger logger = LoggerFactory.getLogger(FileTagTypeService.class);

    @Autowired
    private FileTagTypeRepository fileTagTypeRepository;

    @Value("${filetag.type.file:}")
    private String fileTagDefaultsFileName;

    @Value("${filetag.defaultTypeName:User}")
    private String defaultTypeNameTagTypeName;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private MessageHandler messageHandler;

    public static final String NAME = "Name";
    public static final String DESCRIPTION = "Description";
    public static final String SENSITIVE = "Sensitive";
    public static final String SYSTEM = "System";
    public static final String HIDDEN = "Hidden";
    public static final String SINGLE_VALUE_TAG = "SingleValueTag";
    public static final String INITIAL_TAGS = "InitialTags";
    private final static String[] requiredHeaders = {NAME, DESCRIPTION, SENSITIVE};

    @Transactional
    public FileTagType createFileTagType(String name,
                                         String description,
                                         boolean sensitive,
                                         boolean system,
                                         boolean hidden,
                                         boolean singleValueTag,
                                         Scope tagTypeScope) {
        FileTagType fileTagType = new FileTagType();
        fileTagType.setName(name);
        fileTagType.setDescription(description);
        fileTagType.setSensitiveTag(sensitive);
        fileTagType.setSystem(system);
        fileTagType.setHidden(hidden);
        fileTagType.setSingleValueTag(singleValueTag);
        if (tagTypeScope != null) {
            BasicScope scope = scopeService.getOrCreateScope(tagTypeScope);
            fileTagType.setScope(scope);
        }
        return fileTagTypeRepository.save(fileTagType);
    }

    private void createFileTagTypeFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap, User user) {
        String[] fieldsValues = row.getFieldsValues();
        if (fieldsValues == null || fieldsValues.length == 0 || fieldsValues[0].startsWith("#")) {
            return;
        }
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String description = fieldsValues[headersLocationMap.get(DESCRIPTION)].trim();
        String sensitive = fieldsValues[headersLocationMap.get(SENSITIVE)].trim();
        String system = fieldsValues[headersLocationMap.get(SYSTEM)].trim();
        String hidden = fieldsValues[headersLocationMap.get(HIDDEN)].trim();
        String optionalSingleValueTag = CsvUtils.getOptionalFieldValue(row, headersLocationMap, SINGLE_VALUE_TAG);
        boolean singleValueTag = StringUtils.isEmpty(optionalSingleValueTag) ? false : Boolean.valueOf(optionalSingleValueTag);
        String initialTags = fieldsValues[headersLocationMap.get(INITIAL_TAGS)];
        logger.trace("Create Tag-Type by name {} and description {} sensitive {} system {} hidden {}", name, description, sensitive, system, hidden);
        FileTagType newTagType = createFileTagType(name, description, Boolean.valueOf(sensitive), Boolean.valueOf(system), Boolean.valueOf(hidden), singleValueTag, null);
        if (initialTags != null && !initialTags.isEmpty()) {
            Stream.of(initialTags.split(";")).map(String::trim).filter(t -> !t.isEmpty()).forEach(tagName -> {
                logger.trace("Create Tag by name {} (type {})", tagName, newTagType.getName());
                fileTagService.createFileTag(tagName, "", tagName, newTagType, FileTagAction.MANUAL, user.getId());
            });
        }
    }

    @Transactional(readOnly = true)
    public FileTagType getFileTagTypeByName(String name) {
        return fileTagTypeRepository.findTagTypeByName(name);
    }

    @Transactional
    public void loadDefaultTagTypes(User accountsAdmin) {
        if (Strings.isNullOrEmpty(fileTagDefaultsFileName)) {
            logger.info("No default tag types file defined - ignored");
            return;
        }
        logger.info("Load default tag types from file {}", fileTagDefaultsFileName);
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, fileTagDefaultsFileName, requiredHeaders);
        int failedRows = 0;
        try {
            failedRows = csvReader.readAndProcess(fileTagDefaultsFileName, row -> createFileTagTypeFromCsvRow(row, headersLocationMap, accountsAdmin), null, null);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load file tag types - problem in csv file " + e.getMessage(), e);
        }
        if (failedRows > 0) {
            throw new RuntimeException("Failed to load " + failedRows + " file tag types - problem in csv file");
        }
    }

    public String getDefaultTypeNameTagTypeName() {
        return defaultTypeNameTagTypeName;
    }

    @Transactional(readOnly = true)
    public List<FileTagType> getFileTagTypes(Long userId) {
        return fileTagTypeRepository.findAllTagsByUser(userId);
    }

    @Transactional(readOnly = true)
    public List<FileTagType> getFileTagTypes() {
        return fileTagTypeRepository.findTagTypeNotDeleted();
    }

    @Transactional(readOnly = true)
    public FileTagType getFileTagType(long fileTagTypeId) {
        return fileTagTypeRepository.findById(fileTagTypeId).orElse(null);
    }

    @Transactional(readOnly = true)
    public FileTagTypeDto getFileTagTypeDto(long fileTagTypeId) {
        FileTagTypeDto dto = fileTagTypeRepository.findById(fileTagTypeId)
                .map(FileTagTypeService::convertFileTagTypeToDto)
                .orElse(null);

        if (dto != null) {
            List<FileTag> tags = fileTagService.getFileTagsByType(fileTagTypeId);
            dto.setTagItemsCount(tags == null ? 0 : tags.size());
        }

        return dto;
    }

    @Transactional(readOnly = true)
    public Map<Long, FileTagType> getFileTagTypesByIdAsMap(List<Long> ids) {
        List<FileTagType> tagTypesByIds = fileTagTypeRepository.findTagTypesByIds(ids);
        Map<Long, FileTagType> result = new HashMap<>();
        tagTypesByIds.stream().forEach(tt -> result.put(tt.getId(), tt));
        return result;
    }

    @Transactional(readOnly = true)
    public Map<Long, Integer> findFileTagCounts() {
        List<Object[]> tagCountsList = fileTagTypeRepository.findFileTagCounts();
        Map<Long, Integer> result = new HashMap<>();
        for (Object[] objects : tagCountsList) {
            Long fileTagTypeId = (Long) objects[0];
            int value = ((Long) objects[1]).intValue();

            result.put(fileTagTypeId, value);
        }

        return result;
    }

    @Transactional
    public FileTagType updateFileTagType(long fileTagTypeId, FileTagTypeDto fileTagTypeDto) {
        FileTagType one = fileTagTypeRepository.getOne(fileTagTypeId);
        one.setName(fileTagTypeDto.getName());
        one.setDescription(fileTagTypeDto.getDescription());
        one.setSensitiveTag(fileTagTypeDto.isSensitive());
        return fileTagTypeRepository.save(one);
    }

    @Transactional
    public void deleteFileTagType(long fileTagTypeId) {
        FileTagType one = fileTagTypeRepository.getOne(fileTagTypeId);
        if (one == null) {
            throw new BadRequestException(messageHandler.getMessage("file-tag-type.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        List<FileTag> tags = fileTagService.getFileTagsByType(fileTagTypeId);
        if (tags != null && !tags.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("tag-type.delete.has-tags"), BadRequestType.OPERATION_FAILED);
        }

        logger.warn("Delete file tag type {}", one);
        one.setDeleted(true);
        fileTagTypeRepository.save(one);
    }

    private static List<FileTagTypeDto> convertFileTagTypesToDto(List<FileTagType> fileTagTypes, boolean showHiddenTypes) {
        return fileTagTypes.stream()
                .filter(f -> showHiddenTypes || !f.isHidden())
                .map(FileTagTypeService::convertFileTagTypeToDto)
                .collect(Collectors.toList());
    }

    public static List<FileTagTypeDto> convertFileTagTypesToDto(List<FileTagType> fileTagTypes, Map<Long, Integer> fileTagCounts, boolean showHiddenTypes) {
        List<FileTagTypeDto> fileTagTypeDtos = convertFileTagTypesToDto(fileTagTypes, showHiddenTypes);
        for (FileTagTypeDto fileTagTypeDto : fileTagTypeDtos) {
            Integer value = fileTagCounts.get(fileTagTypeDto.getId());
            int iValue = value != null ? value : 0;
            fileTagTypeDto.setTagItemsCount(iValue);
        }
        return fileTagTypeDtos;
    }

    public static FileTagTypeDto convertFileTagTypeToDto(FileTagType type) {
        FileTagTypeDto fileTagTypeDto = new FileTagTypeDto();
        fileTagTypeDto.setId(type.getId());
        fileTagTypeDto.setName(type.getName());
        fileTagTypeDto.setDescription(type.getDescription());
        fileTagTypeDto.setSystem(type.isSystem());
        fileTagTypeDto.setHidden(type.isHidden());
        fileTagTypeDto.setSensitive(type.isSensitiveTag());
        fileTagTypeDto.setSingleValueTag(type.isSingleValueTag());
        fileTagTypeDto.setScope(AssociationUtils.convertBasicScopeToDto(type.getScope()));
        return fileTagTypeDto;
    }
}
