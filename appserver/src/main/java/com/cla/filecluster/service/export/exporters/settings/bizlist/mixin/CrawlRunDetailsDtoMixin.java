package com.cla.filecluster.service.export.exporters.settings.bizlist.mixin;

import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.settings.bizlist.BizListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class CrawlRunDetailsDtoMixin {

    @JsonProperty(LAST_FULL_RUN)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long scanStartTime;
}
