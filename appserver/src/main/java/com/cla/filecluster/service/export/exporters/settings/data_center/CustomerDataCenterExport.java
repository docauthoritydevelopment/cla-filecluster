package com.cla.filecluster.service.export.exporters.settings.data_center;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterSummaryInfo;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.data_center.CustomerDataCenterHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
@Slf4j
@Component
public class CustomerDataCenterExport extends PageCsvExcelExporter<CustomerDataCenterSummaryInfo> {

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    private final static String[] header = {NAME, DEFAULT, DESCRIPTION, MPS, ROOT_FOLDERS, LOCATION, CREATED};

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(CustomerDataCenterSummaryInfo.class, CustomerDataCenterSummaryInfoMixin.class)
                .addMixIn(CustomerDataCenterDto.class, CustomerDataCenterDtoMixin.class);
    }

    @Override
    protected Iterable<CustomerDataCenterSummaryInfo> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return customerDataCenterService.listCustomerDataCentersSummary(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(CustomerDataCenterSummaryInfo base, Map<String, String> requestParams) {
        return null;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DATA_CENTER);
    }
}
