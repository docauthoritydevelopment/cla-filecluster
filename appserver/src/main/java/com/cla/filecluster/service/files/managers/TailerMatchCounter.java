package com.cla.filecluster.service.files.managers;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by uri on 07-May-17.
 */
@Component
public class TailerMatchCounter {


    private AtomicLong tailerTotalMatchCount = new AtomicLong();
    private AtomicLong tailerMatchCount = new AtomicLong();
    private AtomicLong tailerFatalMatchCount = new AtomicLong();
    private String tailerLastFatalLine;
    private String tailerLastErrorLine;

    @ManagedAttribute
    public long getTailerTotalMatchCount() {
        return tailerTotalMatchCount.get();
    }

    @ManagedAttribute
    public long getTailerMatchCount() {
        return tailerMatchCount.get();
    }

    @ManagedAttribute
    public long getTailerFatalMatchCount() {
        return tailerFatalMatchCount.get();
    }

    public String getTailerLastFatalLine() {
        return tailerLastFatalLine;
    }

    public String getTailerLastErrorLine() {
        return tailerLastErrorLine;
    }

    public long resetTailerErrorMatchCount() {
        return tailerMatchCount.getAndSet(0);
    }

    public long resetTailerFatalMatchCount() {
        return tailerFatalMatchCount.getAndSet(0);
    }

    public Long incErrorCount() {
        tailerTotalMatchCount.incrementAndGet();
        return tailerMatchCount.incrementAndGet();
    }

    public Long incFatalCount() {
        tailerTotalMatchCount.incrementAndGet();
        return tailerFatalMatchCount.incrementAndGet();
    }

    public void setTailerLastFatalLine(String tailerLastFatalLine) {
        this.tailerLastFatalLine = tailerLastFatalLine;
    }

    public void setTailerLastErrorLine(String tailerLastErrorLine) {
        this.tailerLastErrorLine = tailerLastErrorLine;
    }
}
