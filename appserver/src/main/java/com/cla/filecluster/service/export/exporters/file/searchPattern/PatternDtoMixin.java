package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.extractor.pattern.SearchPatternAppService.*;


/**
 * Created by: ophir
 * Created on: 2/6/2019
 */
public class PatternDtoMixin {

    @JsonProperty(TextSearchPatternHeaderFields.NAME)
    private String name;

    @JsonProperty(TextSearchPatternHeaderFields.CATEGORY)
    private String categoryName;

    @JsonProperty(TextSearchPatternHeaderFields.SUB_CATEGORY)
    private String subCategoryName;
}
