package com.cla.filecluster.service.communication;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.crawler.FileStateDto;
import com.cla.common.domain.dto.email.MailNotificationConfigurationDto;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.jobmanager.RunSummaryInfoDto;
import com.cla.common.domain.dto.kendo.SortDescriptor;
import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.common.domain.dto.report.CounterReportDto;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.service.api.SummaryReportsApplicationService;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.files.managers.TailerMatchCounter;
import com.cla.filecluster.service.license.LicenseAppService;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by vladi on 3/27/2017.
 */
@Service
public class EmailProgressReportService {

    private final static Logger logger = LoggerFactory.getLogger(EmailProgressReportService.class);

    @Value("${email.notification.body-template:./config/templates/email_progress_report.vm}")
    private String bodyTemplateFileName;

    @Value("${email.notification.from-email-address:support@docauthority.com}")
    private String fromEmailAddress;

    @Value("${email.notification.email-subject:DA Progress report}")
    private String emailSubject;

    @Value("${email.notification.last-hours:24}")
    private int showLastHours;

    @Autowired
    private CommunicationService commService;

    @Autowired
    private EmailConfigurationService emailConfigurationService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private TailerMatchCounter tailerMatchCounter;

    @Autowired
    private SummaryReportsApplicationService summaryReportsApplicationService;

    @Autowired
    private LicenseAppService licenseAppService;

    @Autowired
    private FileCatAppService fileCatAppService;

    @Transactional
    public void testEmail(MailNotificationConfigurationDto mailNotificationConfigurationDto) {
        String addresses = mailNotificationConfigurationDto.getAddresses();
        logger.info("Sending test email to addresses {}.", addresses);
        EmailProperties props = EmailProperties.create()
                .setPlainSubject("test email")
                .setFromEmail("support@docauthority.com")
                .setBodyTemplate("./config/templates/email_test.vm");

        props.setToEmails(Splitter.on(",").splitToList(addresses));

        props.put("formatTools", new FormatTools());

        commService.send(props);
    }

    private EmailProperties createEmailProperties(boolean htmlOnly) {
        String addresses = emailConfigurationService.getAddresses();
        logger.info("Sending scan progress report to email addresses {}.", addresses);

        String emailSubjectString = emailSubject;
        String licenseRegisteredTo = "";
        DocAuthorityLicenseDto license = licenseAppService.getLastAppliedLicense();
        if (license != null) {
            licenseRegisteredTo = license.getRegisteredTo();
            if (!Strings.isNullOrEmpty(licenseRegisteredTo)) {
                emailSubjectString += " - " + licenseRegisteredTo;
                licenseRegisteredTo = "Registered to: " + licenseRegisteredTo;
            }
        }

        EmailProperties props = EmailProperties.create()
                .setPlainSubject(emailSubjectString)
                .setFromEmail(fromEmailAddress)
                .setBodyTemplate(bodyTemplateFileName);

        props.put("licenseRegisteredTo", licenseRegisteredTo);

        props.setToEmails((addresses != null) ? Splitter.on(",").splitToList(addresses) : Collections.emptyList());

        long start = System.currentTimeMillis();
        logger.debug("Started: populateCrawlRuns");
        populateCrawlRuns(props);
        logger.debug("Done: populateCrawlRuns: {} ms", System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        logger.debug("Started: populateCurrentCrawlData");
        populateCurrentCrawlData(props);
        logger.debug("Done: populateCurrentCrawlData: {} ms", System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        logger.debug("Started: populateTailerData");
        populateTailerData(props);
        logger.debug("Done: populateTailerData: {} ms", System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        logger.debug("Started: populateSolrFilesStats");
        populateSolrFilesStats(props);
        logger.debug("Done: populateSolrFilesStats: {} ms", System.currentTimeMillis() - start);

        start = System.currentTimeMillis();
        logger.debug("Started: populateFileStats");
        populateFileStats(props);
        logger.debug("Done: populateFileStats: {} ms", System.currentTimeMillis() - start);

        props.put("htmlOnly", htmlOnly);
        props.put("formatTools", new FormatTools());
        props.put("localTimeNow", ZonedDateTime.now());
        return props;
    }

    @Transactional
//    @Scheduled(cron = "#{@environment.getProperty('email-progress-report.cron', '0 0 8,20 * * *')}")
    @Scheduled(cron = "${email-progress-report.cron:0 0 8,20 * * *}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @AutoAuthenticate
    public void sendEmailReport() {
        if (emailConfigurationService.isSendReportEnabled()) {
            EmailProperties props = createEmailProperties(false);
            commService.send(props);
        }
    }

    @Transactional
    @AutoAuthenticate
    public String createSampleHtml() {
        EmailProperties props = createEmailProperties(true);
        return commService.createSampleHtml(props);
    }


    private void populateCrawlRuns(EmailProperties props) {

        DataSourceRequest request = new DataSourceRequest();

        SortDescriptor sortDescriptor = new SortDescriptor();
        sortDescriptor.setDir("desc");
        sortDescriptor.setField("startTime");
        SortDescriptor[] sort = new SortDescriptor[]{sortDescriptor};
        request.setSort(sort);

        request.setPageSize(10);
        request.setHistoryLastHours(showLastHours);

        Page<CrawlRunDetailsDto> crawlRunHistory = jobManagerAppService.listRuns(request);
        if (crawlRunHistory.getContent().size() < 10) {
            List content = new ArrayList();
            content.addAll(crawlRunHistory.getContent());
            for (int i = crawlRunHistory.getContent().size(); i < 10; ++i) {
                content.add(new CrawlRunDetailsDto());
            }
            crawlRunHistory = new PageImpl<CrawlRunDetailsDto>(content, request.createPageRequestWithSort(), 10);
        }
        props.put("crawlRunsHistory", crawlRunHistory);
    }

    private void populateCurrentCrawlData(EmailProperties props) {
        List<RunSummaryInfoDto> runSummaryInfoList = jobManagerAppService.getActiveRunsDetails(DataSourceRequest.build(null));
        props.put("runSummaryInfoList", runSummaryInfoList);
    }

    private void populateTailerData(EmailProperties props) {
        TailerData tailerData = TailerData.create()
                .setErrorCount(tailerMatchCounter.getTailerMatchCount())
                .setFatalCount(tailerMatchCounter.getTailerFatalMatchCount())
                .setLastErrorLine(tailerMatchCounter.getTailerLastErrorLine())
                .setLastFatalLine(tailerMatchCounter.getTailerLastFatalLine());
        props.put("tailerData", tailerData);
    }

    private void populateSolrFilesStats(EmailProperties props) {
        List<CounterReportDto> stats = summaryReportsApplicationService.createExcelStats();
        summaryReportsApplicationService.addAggregatedProcessedFilesSize(stats);
        props.put("statsCoutners", stats);
    }

    private void populateFileStats(EmailProperties props) {
        List<AggregationCountItemDTO<FileStateDto>> stats = fileCatAppService.countFilesByStateAndType();
        props.put("fileStats", stats);
    }


    @SuppressWarnings("unused")
    public class FormatTools {

        public String epochToDate(Long epoch) {
            if (epoch == null) {
                return "N/A";
            }
            LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(TimeUnit.MILLISECONDS.toSeconds(epoch), 0, ZoneOffset.UTC);
            return localTimeFormat(localDateTime);
        }

        public String epochToDate(Long epoch, String pattern) {
            if (epoch == null) {
                return "N/A";
            }
            LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(TimeUnit.MILLISECONDS.toSeconds(epoch), 0, ZoneOffset.UTC);
            return localTimeFormat(localDateTime, pattern);
        }

        String localTimeFormat(LocalDateTime lt) {
            return localTimeFormat(lt, "yyyy-MM-dd HH:mm");
        }

        public String stringAlternate(String s1, String s2) {
            return (s1 == null) ? s2 : s1;
        }

        String localTimeFormat(LocalDateTime lt, String pattern) {
            if (lt == null) {
                return "N/A";
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            return lt.format(formatter);
        }

        public String localTimeFormat(ZonedDateTime lt) {
            return localTimeFormat(lt, "yyyy-MM-dd HH:mm Z");
        }

        String localTimeFormat(ZonedDateTime lt, String pattern) {
            if (lt == null) {
                return "N/A";
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            return lt.format(formatter);
        }

        public String stringFormat(String fmt, Object obj) {
            return String.format(fmt, obj);
        }

        public String nullToNA(Object canBeNull) {
            if (canBeNull == null) {
                return "N/A";
            }
            if ((canBeNull instanceof String) && ((String) canBeNull).isEmpty()) {
                return "N/A";
            }
            return canBeNull.toString();
        }
    }

    /**
     * DTO for error data retrieved from tailer
     */
    @SuppressWarnings("unused")
    public static class TailerData {
        private long errorCount = 0;
        private long fatalCount = 0;
        private String lastFatalLine;
        private String lastErrorLine;

        public static TailerData create() {
            return new TailerData();
        }

        public long getErrorCount() {
            return errorCount;
        }

        TailerData setErrorCount(long errorCount) {
            this.errorCount = errorCount;
            return this;
        }

        public long getFatalCount() {
            return fatalCount;
        }

        TailerData setFatalCount(long fatalCount) {
            this.fatalCount = fatalCount;
            return this;
        }

        public String getLastFatalLine() {
            return Strings.nullToEmpty(lastFatalLine);
        }

        TailerData setLastFatalLine(String lastFatalLine) {
            this.lastFatalLine = lastFatalLine;
            return this;
        }

        public String getLastErrorLine() {
            return Strings.nullToEmpty(lastErrorLine);
        }

        TailerData setLastErrorLine(String lastErrorLine) {
            this.lastErrorLine = lastErrorLine;
            return this;
        }
    }
}
