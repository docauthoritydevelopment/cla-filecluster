package com.cla.filecluster.service.audit;

public enum AuditColumns {
	EVENT_ID,
	TIME_STAMP,
	USERNAME,
	SUB_SYSTEM,
	MESSAGE,
	ACTION,
	OBJECT_TYPE,
	OBJECT_ID,
	ON_OBJECT_TYPE,
	ON_OBJECT_ID,
	PARAMS
}
