package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.media.MediaConnectionDetailsSchemaLoader;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Controller for media connections management
 * Created by uri on 29/08/2016.
 */
@RestController
@RequestMapping("/api/media")
public class MediaApiController {
    private static Logger logger = LoggerFactory.getLogger(MediaApiController.class);

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private MediaConnectionDetailsSchemaLoader mediaConnectionDetailsSchemaLoader;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/connections")
    public List<MediaConnectionDetailsDto> listConnections(@RequestParam(required = false) MediaType mediaType) {
        if (mediaType != null) {
            return mediaConnectionDetailsService.listConnections(mediaType);
        }
        return mediaConnectionDetailsService.listConnections();
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.DELETE)
    public void deleteMediaConnection(@PathVariable long id) {
        mediaConnectionDetailsService.deleteMediaConnection(id);
    }

    @RequestMapping(value="/import/csv", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto importCsvFile(@RequestParam("file") MultipartFile file, @RequestParam(value ="updateDuplicates",required = false,defaultValue = "false") boolean updateDuplicates){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import connections CSV File. update duplicates set to {}",updateDuplicates);
            byte[] bytes = file.getBytes();
            return mediaConnectionDetailsSchemaLoader.doImport(false,bytes,null,updateDuplicates);
        }
        catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(),e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @RequestMapping(value="/import/csv/validate", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto validateImportCsvFile(@RequestParam("file") MultipartFile file,@RequestParam("maxErrorRowsReport") Integer maxErrorRowsReport){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("validate import media connection CSV File");
            byte[] bytes = file.getBytes();
            return mediaConnectionDetailsSchemaLoader.doImport(true,bytes,maxErrorRowsReport==null?200:maxErrorRowsReport,false);
        }
        catch (Exception e) {
            logger.error("Validate import CSV error:" + e.getMessage(), e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription("Validate import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }


    @RequestMapping(value = "/supported")
    public List<MediaType> getSupportedMediaTypes() {
        return mediaConnectionDetailsService.getSupportedMediaTypes();
    }


}
