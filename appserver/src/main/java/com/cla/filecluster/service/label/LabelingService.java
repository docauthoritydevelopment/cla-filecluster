package com.cla.filecluster.service.label;

import com.cla.common.constants.CatFileFieldType;
import com.cla.connector.domain.dto.file.ClaFilePropertiesDto;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.connector.domain.dto.file.label.NativeLabel;
import com.cla.connector.mediaconnector.labeling.LabelResult;
import com.cla.connector.mediaconnector.labeling.mip.MipLabelResultTyped;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.label.Label;
import com.cla.connector.domain.dto.file.label.LabelData;
import com.cla.connector.domain.dto.file.label.LabelingVendor;
import com.cla.filecluster.label.conf.LabelConfig;
import com.cla.filecluster.label.LabelsResolver;
import com.cla.filecluster.label.resolvers.MipLabelsResolver;
import com.cla.filecluster.label.transformers.Transformer;
import com.cla.filecluster.metadata.MetadataTranslationService;
import com.cla.filecluster.repository.jpa.labeling.LabelsRepository;

import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class LabelingService {

	public final static String SEPARATOR = ".";
	private final static String MIP_PREFIX = "MSIP_Label_";

	private final static Logger logger = LoggerFactory.getLogger(LabelingService.class);

	@Autowired
	private LabelsRepository labelsRepository;

	@Autowired
	private FileCatService fileCatService;

	@Autowired
	private DBTemplateUtils dbTemplateUtils;

	@Autowired
	private List<Transformer> transformersObjs;

	@Autowired
	private MetadataTranslationService metadataTranslationService;

	@Value("${labeling.transformers.file:}")
	private String labelTransformerFilePath;

	@Value("${metadata.mip.type:}")
	private String mipMetadataType;

	private LoadingCache<String, LabelDto> labelLoadingCache;
	private LoadingCache<Long, Optional<LabelDto>> labelByIdLoadingCache;
	private EnumMap<LabelingVendor, LabelsResolver> labelResolvers;
	private EnumMap<LabelingVendor, Transformer> transformers = new EnumMap<>(LabelingVendor.class);

	@SuppressWarnings("unused")
	@PostConstruct
	private void init(){
		labelResolvers = new EnumMap<>(LabelingVendor.class);
		labelResolvers.put(LabelingVendor.MicrosoftInformationProtection, MipLabelsResolver::resolveMipLabels);

		labelLoadingCache =
				CacheBuilder.newBuilder()
						.maximumSize(500)
						.expireAfterAccess(60, TimeUnit.SECONDS)
						.build(new CacheLoader<String, LabelDto>() {
							@Override
							public LabelDto load(@NotNull String labelName) {
								Optional<Label> labelOptional = labelsRepository.findByName(labelName);
								//save the DA labels in the SQL if not exist. Maybe this should be optional?
								return labelOptional.map(LabelingService::labelToDto)
										.orElseGet(()-> labelToDto(labelsRepository.save(new Label().setName(labelName))));
							}
						});

		labelByIdLoadingCache = CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(15, TimeUnit.MINUTES)
				.build(new CacheLoader<Long, Optional<LabelDto>>() {
					public Optional<LabelDto> load(@NotNull Long labelId) {
						return dbTemplateUtils.doInTransaction(() -> {
							Label label = labelsRepository.findById(labelId).orElse(null);
							return Optional.ofNullable(labelToDto(label));
						});
					}
				});

		loadTransformersFromFile();
	}

	private void loadTransformersFromFile() {
		if (labelTransformerFilePath == null || labelTransformerFilePath.isEmpty()) {
			logger.info("No label transformer file defined - ignored");
		} else {
			logger.info("Load label transformers from file {}", labelTransformerFilePath);
			loadLabelTransformersFromFile(labelTransformerFilePath);
		}
	}

	private Transformer getByVendor(LabelingVendor vendor) {
		if (transformersObjs == null) {
			return null;
		}
		for (Transformer transformer : transformersObjs) {
			if (transformer.getVendor().equals(vendor)) {
				return transformer;
			}
		}
		return null;
	}

	private void loadLabelTransformersFromFile(String labelTransformerFilePath) {
		try {
			Set<String> daLabels = new HashSet<>();
			File configFile = new File(labelTransformerFilePath);
			LabelConfig config = ModelDtoConversionUtils.getMapper().readValue(configFile, LabelConfig.class);
			if (config != null && config.getTransformations() != null && !config.getTransformations().isEmpty()) {
				try {
					config.getTransformations().forEach(transformation -> {
						LabelingVendor vendor = LabelingVendor.getByName(transformation.getLabelingVendor());
						Transformer transformerObj = getByVendor(vendor);
						if (transformerObj != null) {
							daLabels.addAll(transformerObj.init(transformation));
							transformers.put(vendor, transformerObj);
						}
					});

					createDaLabelsIfNeeded(daLabels);
				} catch (Exception e) {
					logger.error("failed loading a transformer from file", e);
				}
			}
		} catch (Exception e) {
			logger.error("failed loading label transformers from file", e);
		}
	}

	private void createDaLabelsIfNeeded(Set<String> daLabels) {
		if (daLabels != null && !daLabels.isEmpty()) {
			daLabels.forEach(this::getForDaLabel);
		}
	}

	public Optional<LabelDto> getLabelByIdCached(Long labelId) {
		return labelByIdLoadingCache.getUnchecked(labelId);
	}

	public List<LabelDto> getByIds(List<Long> labelIds) {
        if (labelIds == null || labelIds.isEmpty()) {
            return null;
        }
		List<LabelDto> labels = new ArrayList<>();
		labelIds.forEach(labelId ->
					labelByIdLoadingCache.getUnchecked(labelId).ifPresent(labels::add));

		return labels;
	}

	@Transactional(readOnly = true)
	public Page<LabelDto> listAllDaLabels(PageRequest pageRequest) {
		Page<Label> labels = labelsRepository.findAll(pageRequest);
		return labels == null ? null : convertLabels(labels, pageRequest);
	}

	private static Page<LabelDto> convertLabels(Page<Label> labels, PageRequest pageRequest) {
		List<LabelDto> content = new ArrayList<>();
		for (Label label : labels.getContent()) {
			content.add(labelToDto(label));
		}
		return new PageImpl<>(content, pageRequest, labels.getTotalElements());
	}

	public LabelDto transform(LabelingVendor vendor, String label) {
		Transformer transformer = transformers.get(vendor);
		if (transformer != null) {
			NativeLabel nativeLabel = new NativeLabel();
			nativeLabel.setName(label);
			LabelData data = new LabelData(null, nativeLabel);
			String daLabel = transformer.transformRead(data);
			return getForDaLabel(daLabel);
		}
		return null;
	}

	public LabelDto getForDaLabel(String daLabel) {
		return Strings.isNullOrEmpty(daLabel) ? null : labelLoadingCache.getUnchecked(daLabel);
	}

	private List<LabelData> resolveLabels(Map<String,String> metadata){
		if(metadata == null || metadata.isEmpty()){
			return Lists.newArrayList();
		}
		//find and extract vendor labels
		List<LabelData> labels = labelResolvers.values().stream()
									.map(resolver -> resolver.apply(metadata))
									.flatMap(Collection::stream)
									.collect(Collectors.toList());

		//find and extract da labels based on vendor label
		labels.stream().filter(dl-> transformers.containsKey(dl.getNativeLabel().getVendor()))
				.forEach(ld -> {
					Transformer transformerObj = transformers.get(ld.getNativeLabel().getVendor());
					String daLabel = transformerObj.transformRead(ld);
					LabelDto label = getForDaLabel(daLabel);
					ld.setDaLabel(label);
				});

		return labels;
	}

	public void enrichEntitiesForIngest(SolrFileEntity fileEntity, ClaFilePropertiesDto fileProperties) {
		fileProperties.setLabelData(resolveLabels(fileProperties.getMetadata()));
		searchMissingLabels(fileEntity, fileProperties);
	}

	private void searchMissingLabels(SolrFileEntity fileEntity, ClaFilePropertiesDto fileProperties) {
		if (fileEntity != null) {
			if (fileEntity.getExternalMetadata() != null && !fileEntity.getExternalMetadata().isEmpty()) {
				List<String> externalMetadataToRem = new ArrayList<>();
				List<String> daMetadataToRem = new ArrayList<>();
				List<String> daMetadataTypeToRem = new ArrayList<>();
				fileEntity.getExternalMetadata().forEach(metadata -> {
					try {
						LabelResult labelResult = ModelDtoConversionUtils.getMapper().readValue(metadata, LabelResult.class);
						if (labelResult instanceof MipLabelResultTyped) {
							MipLabelResultTyped mipLabelResult = (MipLabelResultTyped) labelResult;
							String guid = mipLabelResult.getId();

							boolean exists = false;
							if (fileProperties.getLabelData() != null && !fileProperties.getLabelData().isEmpty()) {
								for (LabelData labelData : fileProperties.getLabelData()) {
									if (labelData.getNativeLabel().getVendor().equals(LabelingVendor.MicrosoftInformationProtection)
											&& labelData.getNativeLabel().getNativeId().equals(guid)) {
										exists = true;
										break;
									}
								}
							}

							if (!exists) {
								externalMetadataToRem.add(metadata);
								Map<String, String> md = ImmutableMap.of(mipMetadataType, mipLabelResult.getName());
								Map<String, String> daMetadataMap = metadataTranslationService.translateMetadata(md);
								daMetadataToRem.addAll(metadataTranslationService.mapToListWithSeparator(daMetadataMap));
								daMetadataTypeToRem.addAll(daMetadataMap.keySet());
							}
						}
					} catch (Exception e) {
						logger.error("problem processing metadata {} for file {}", metadata, fileEntity.getId(), e);
					}
				});
				if (!externalMetadataToRem.isEmpty()) {
					fileProperties.setExternalMetadataToRem(externalMetadataToRem);
                    fileEntity.getExternalMetadata().removeAll(externalMetadataToRem);
					fileProperties.setDaMetadataToRem(daMetadataToRem);
					fileEntity.getDaMetadata().removeAll(daMetadataToRem);
					fileProperties.setDaMetadataTypeToRem(daMetadataTypeToRem);
					fileEntity.getDaMetadataType().removeAll(daMetadataTypeToRem);
				}
			}
			if (fileEntity.getDaLabels() != null && !fileEntity.getDaLabels().isEmpty()) {
				List<Long> daLabelIdsToRem = new ArrayList<>();
				fileEntity.getDaLabels().forEach(labelId -> {
					boolean exists = false;
					if (fileProperties.getLabelData() != null && !fileProperties.getLabelData().isEmpty()) {
						for (LabelData labelData : fileProperties.getLabelData()) {
							if (labelData.getDaLabel().getId().equals(labelId)) {
								exists = true;
								break;
							}
						}
					}

					if (!exists) {
						daLabelIdsToRem.add(labelId);
					}
				});
				if (!daLabelIdsToRem.isEmpty()) {
					fileProperties.setDaLabelIdsToRem(daLabelIdsToRem);
                    fileEntity.getDaLabels().removeAll(daLabelIdsToRem);
				}
			}
		}
	}

    public Map<LabelingVendor, String> getNativeIdsForLabel(LabelDto label) {
		Map<LabelingVendor, String> result = new HashMap<>();
		transformersObjs.forEach(transformer -> {
			String labelName = transformer.transformWrite(label);
			result.put(transformer.getVendor(), labelName);
		});
		return result;
	}

	public static String metadataToJson(Map<String, String> metadata) {
		if (metadata == null || metadata.isEmpty()) {
			return null;
		}
		return (new JSONObject(metadata)).toString();
	}

	public static List<String> createActionExpected(List<LabelData> labels) {
		if (labels == null || labels.isEmpty()) {
			return null;
		}

		List<String> result = new ArrayList<>();
		labels.forEach(label -> result.add(
                createActionExpected(label.getNativeLabel().getVendor().getName(),
                        label.getNativeLabel().getNativeId(), "0")));
		return result;
	}

	public static String createActionExpected(String vendor, String nativeId, String retries) {
		return vendor + SEPARATOR + nativeId + SEPARATOR + retries;
	}

	private static LabelDto labelToDto(Label label) {
		if (label == null) {
			return null;
		}
		LabelDto dto = new LabelDto();
		dto.setId(label.getId());
		dto.setName(label.getName());
		dto.setDescription(label.getDescription());
		return dto;
	}

	public static Map<String, String> getNonLabelData(Map<String, String> metadata) {
		if (metadata == null || metadata.isEmpty()) {
			return null;
		}

		Map<String, String> other = new HashMap<>();

		metadata.forEach((k,v) -> {
			if (!k.startsWith(MIP_PREFIX)) {
				other.put(k, v);
			}
		});

		return other;
	}

	public Pair<Long, Long> getLabelingRunStatus() {
		Query query = Query.create()
				.addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
				.addFilterWithCriteria(Criteria.create(CatFileFieldType.ACTIONS_EXPECTED, SolrOperator.PRESENT, null))
				.setRows(0);
		QueryResponse resp = fileCatService.runQuery(query.build());
		long pending = resp.getResults().getNumFound();

		query = Query.create()
				.addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"))
				.addFilterWithCriteria(Criteria.create(CatFileFieldType.ACTIONS_EXPECTED, SolrOperator.MISSING, null))
				.addFilterWithCriteria(Criteria.create(CatFileFieldType.LABEL_PROCESS_DATE, SolrOperator.PRESENT, null))
				.setRows(0);
		resp = fileCatService.runQuery(query.build());
		long handled = resp.getResults().getNumFound();
		return Pair.of(pending, handled);
	}
}
