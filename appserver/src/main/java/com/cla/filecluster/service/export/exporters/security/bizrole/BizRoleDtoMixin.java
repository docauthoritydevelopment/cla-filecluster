package com.cla.filecluster.service.export.exporters.security.bizrole;

import com.cla.common.domain.dto.security.RoleTemplateDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import static com.cla.filecluster.service.export.exporters.security.bizrole.BizRoleHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class BizRoleDtoMixin {

    @JsonProperty(NAME)
    protected String name;

    @JsonProperty(DESCRIPTION)
    protected String description;

    @JsonUnwrapped
    protected RoleTemplateDto template;
}
