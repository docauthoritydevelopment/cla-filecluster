package com.cla.filecluster.service.license;

import com.cla.common.domain.dto.license.DocAuthorityLicenseDto;
import com.cla.filecluster.domain.entity.license.DocAuthorityLicense;
import com.cla.filecluster.domain.entity.license.LicenseResource;
import com.cla.filecluster.domain.exceptions.LicenseExpiredException;
import com.cla.filecluster.repository.jpa.license.DocAuthorityLicenseRepository;
import com.cla.filecluster.repository.jpa.license.LicenseResourceRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by uri on 27/06/2016.
 */
@Service
public class LicenseService {

    public static final String NO_LIMITS = "*";

    @Value("${no-database:false}")
    private boolean noDatabase;

    @Autowired
    private DocAuthorityLicenseRepository docAuthorityLicenseRepository;

    @Autowired
    private LicenseResourceRepository licenseResourceRepository;

    @Autowired
    private MessageHandler messageHandler;

    private ConcurrentHashMap<String, String> cachedLicenseRestrictions = new ConcurrentHashMap<>();

    public static final String SIGN_APPENDIX = "iohT3vISQjOtORHJK3d7";

    public static final String CRAWLER_EXPIRY = "crawlerExpiry";
    public static final String FILES_MAX = "maxFiles";
    public static final String ROOTFOLDERS_MAX = "maxRootFolders";
    public static final String LOGIN_EXPIRY = "loginExpiry";
    public static final String SERVER_ID = "serverId";   // Unique value represents a signed resource and not a limitation

    private static Logger logger = LoggerFactory.getLogger(LicenseService.class);

    @Transactional(readOnly = true)
    public DocAuthorityLicense getLastAppliedLicense() {
        List<DocAuthorityLicense> all = docAuthorityLicenseRepository.findByApplyDate(new PageRequest(0, 1));
        return all.size() > 0 ? all.get(0) : null;
    }

    @Transactional(readOnly = true)
    @PostConstruct
    public void fillCachedLicenseRestrictions() {
        if (!noDatabase) {
            logger.debug("Check license");
            List<LicenseResource> all = licenseResourceRepository.findAll();
            cachedLicenseRestrictions.clear();
            for (LicenseResource licenseResource : all) {
                String signature = generateSignature(licenseResource);
                if (!licenseResource.getSignature().equals(signature)) {
                    logger.error("Failed to load license resource - Invalid Signature {}!={}", signature, licenseResource.getSignature());
                } else {
                    cachedLicenseRestrictions.put(licenseResource.getId(), licenseResource.getValue());
                }
            }
            logger.debug("Check license finished");
        } else {
            logger.info("Check license skipped");
        }
    }

    @Transactional(readOnly = true)
    public List<LicenseResource> getLicenseResources() {
        return licenseResourceRepository.findAll();
    }

    public boolean verifySignature(String licenseStringBase64, String signatureStringBase64) {
        logger.info("Verify license signature {} {}", licenseStringBase64, signatureStringBase64);
        String comparedSignature = generateSignaturePart(licenseStringBase64);
        if (!signatureStringBase64.equals(comparedSignature)) {
            logger.warn("License signature doesn't match license {} != {}", signatureStringBase64, comparedSignature);
//            throw new BadHttpRequestException("License signature doesn't match license "+signatureStringBase64+" != "+comparedSignature);
            return false;
        }
        return true;
    }

    @Transactional
    public boolean importLicense(DocAuthorityLicenseDto docAuthorityLicenseDto, final boolean save) {
        boolean allOk = true;
        logger.info("Importing license {}, save={}", docAuthorityLicenseDto, save ? "true" : "false");
        if (save) {
            DocAuthorityLicense docAuthorityLicense = convertToModelObject(docAuthorityLicenseDto);
            docAuthorityLicense.setSignature(generateSignature(docAuthorityLicense));
            docAuthorityLicenseRepository.save(docAuthorityLicense);
        }
        logger.debug("Importing license resources");
        long creationDate = docAuthorityLicenseDto.getCreationDate();
        for (Map.Entry<String, String> restriction : docAuthorityLicenseDto.getRestrictions().entrySet()) {
            logger.debug("Importing license resource {}", restriction);
            LicenseResource one = licenseResourceRepository.findById(restriction.getKey()).orElse(null);
            String value = validateRestriction(restriction.getKey(), restriction.getValue());
            if (one != null) {
                logger.debug("Overriding existing license resource {}", one);
                if (creationDate > one.getIssueTime()) {
                    if (save) {
                        one.setValue(value);
                    }
                } else {
                    logger.warn("A newer/same license resource was applied already. {}/{}", one.getIssueTime(), creationDate);
                    allOk = false;
                }
            } else {
                if (save) {
                    one = new LicenseResource(restriction.getKey(), value, creationDate);
                }
            }
            if (save) {
                one.setSignature(generateSignature(one));
                licenseResourceRepository.save(one);
            }
        }
        fillCachedLicenseRestrictions();
        return allOk;
    }

    private String validateRestriction(String key, String value) {

        value = (value == null ? null : value.trim());

        if (Strings.isNullOrEmpty(value)) {
            throw new IllegalArgumentException(messageHandler.getMessage("license.restriction.empty", Lists.newArrayList(key)));
        }

        switch (key) {
            case "crawlerExpiry":
            case "loginExpiry":
            case "maxFiles":
            case "maxRootFolders":
                if (!value.equals(NO_LIMITS)) {
                    try {
                        Long.parseLong(value);
                    } catch (NumberFormatException e) {
                        throw new IllegalArgumentException(messageHandler.getMessage("license.restriction.invalid", Lists.newArrayList(key)));
                    }
                }
                break;
            case "serverId":
                break;
            default:
                logger.warn("unknown license restriction {}", key);
        }

        return value;
    }

    private String generateSignature(LicenseResource licenseResource) {
        return generateSignaturePart(licenseResource.getId() + licenseResource.getValue() + licenseResource.getIssueTime());
    }

    public DocAuthorityLicense convertToModelObject(DocAuthorityLicenseDto docAuthorityLicenseDto) {
        DocAuthorityLicense docAuthorityLicense = new DocAuthorityLicense();
        docAuthorityLicense.setApplyDate(System.currentTimeMillis());
        docAuthorityLicense.setCreationDate(docAuthorityLicenseDto.getCreationDate());
        docAuthorityLicense.setId(docAuthorityLicenseDto.getId());
        docAuthorityLicense.setIssuer(docAuthorityLicenseDto.getIssuer());
        docAuthorityLicense.setRegisteredTo(docAuthorityLicenseDto.getRegisteredTo());
        return docAuthorityLicense;
    }

    private String generateSignature(DocAuthorityLicense docAuthorityLicense) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(docAuthorityLicense.getId())
                .append(docAuthorityLicense.getApplyDate())
                .append(docAuthorityLicense.getCreationDate())
                .append(docAuthorityLicense.getIssuer())
                .append(docAuthorityLicense.getRegisteredTo());
        return generateSignaturePart(stringBuilder.toString());
    }

    private static String generateSignaturePart(String licensePart) {
        byte[] sha1 = DigestUtils.sha1(licensePart + SIGN_APPENDIX);
        return Base64.encodeBase64String(sha1);
    }

    @Transactional(readOnly = true)
    public boolean licenseExists(long id) {
        return docAuthorityLicenseRepository.existsById(id);
    }

    public Boolean isLoginExpired() {
        String s = cachedLicenseRestrictions.get(LOGIN_EXPIRY);
        if (s == null) {
            logger.info("NO License loaded. Login is expired by definition");
            return true;
        }
        if (s.equals(NO_LIMITS)) {
            return false;
        }
        Long expireTime = Long.valueOf(s);
        if (System.currentTimeMillis() > expireTime) {
            logger.info("Login expired");
            return true;
        }
        return false;
    }

    public boolean isCrawlerLicenseExpired() {
        String s = cachedLicenseRestrictions.get(CRAWLER_EXPIRY);
        if (s == null) {
            logger.warn("Crawler license not found");
            return true;
        }
        if (s.equals(NO_LIMITS)) {
            return false;
        }
        Long expireTime = Long.valueOf(s);
        if (System.currentTimeMillis() > expireTime) {
            LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(expireTime / 1000, 0, ZoneOffset.UTC);
            logger.debug("Crawler license expired at " + localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE));
            return true;
        }
        return false;
    }

    public void validateCrawlerExpiry() {
        String s = cachedLicenseRestrictions.get(CRAWLER_EXPIRY);
        if (s == null) {
            throw new LicenseExpiredException(messageHandler.getMessage("license.crawler.not-found"));
        }
        if (s.equals(NO_LIMITS)) {
            return;
        }
        Long expireTime = Long.valueOf(s);
        if (System.currentTimeMillis() > expireTime) {
            LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(expireTime / 1000, 0, ZoneOffset.UTC);
            throw new LicenseExpiredException(messageHandler.getMessage("license.crawler.expired", Lists.newArrayList(localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE))));
        }
    }

    public void validateLoginExpiry() {
        String s = cachedLicenseRestrictions.get(LOGIN_EXPIRY);
        if (s == null) {
            throw new LicenseExpiredException(messageHandler.getMessage("license.login.not-found"));
        }
        if (s.equals(NO_LIMITS)) {
            return;
        }
        Long expireTime = Long.valueOf(s);
        if (System.currentTimeMillis() > expireTime) {
            logger.info("Login expired");
            LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(expireTime / 1000, 0, ZoneOffset.UTC);
            throw new LicenseExpiredException(messageHandler.getMessage("license.login.expired", Lists.newArrayList(localDateTime.format(DateTimeFormatter.BASIC_ISO_DATE))));
        }
    }

    public void validateRootFoldersLicense(long currentRootFoldersCount) {
        String s = cachedLicenseRestrictions.get(ROOTFOLDERS_MAX);
        if (s == null) {
            throw new LicenseExpiredException(messageHandler.getMessage("license.root-folder.not-found"));
        }
        if (s.equals(NO_LIMITS)) {
            return;
        }
        Long rootFoldersMax = Long.valueOf(s);
        if (currentRootFoldersCount >= rootFoldersMax) {
            logger.info("RootFolders are at license max ", s);
            throw new LicenseExpiredException(messageHandler.getMessage("license.root-folder.max", Lists.newArrayList(s)));
        }
    }

    public void validateMaxFilesCount(long currentFilesCount) {
        String s = cachedLicenseRestrictions.get(FILES_MAX);
        if (s == null) {
            throw new LicenseExpiredException(messageHandler.getMessage("license.max-file.not-found"));
        }
        if (s.equals(NO_LIMITS)) {
            return;
        }
        Long maxFilesCount = Long.valueOf(s);
        if (currentFilesCount > maxFilesCount) {
            logger.info("Managed files are at license max ", s);
            throw new LicenseExpiredException(messageHandler.getMessage("license.max-file.max", Lists.newArrayList(s)));
        }
    }

    public String getServerId() {
        Optional<String> optionalServerId = licenseResourceRepository.findById(SERVER_ID)
                .map(LicenseResource::getValue);
        if (!optionalServerId.isPresent()) {
            logger.error("Server ID ({}) resource missing.", SERVER_ID);
            return "";
        } else {
            return optionalServerId.get();
        }
    }

    public static DocAuthorityLicenseDto convertLicense(DocAuthorityLicense docAuthorityLicense, List<LicenseResource> resourceList) {
        DocAuthorityLicenseDto docAuthorityLicenseDto = new DocAuthorityLicenseDto();
        docAuthorityLicenseDto.setRegisteredTo(docAuthorityLicense.getRegisteredTo());
        docAuthorityLicenseDto.setIssuer(docAuthorityLicense.getIssuer());
        docAuthorityLicenseDto.setId(docAuthorityLicense.getId());
        docAuthorityLicenseDto.setApplyDate(docAuthorityLicense.getApplyDate());
        docAuthorityLicenseDto.setCreationDate(docAuthorityLicense.getCreationDate());
        Map<String, String> restrictions = docAuthorityLicenseDto.getRestrictions();
        for (LicenseResource licenseResource : resourceList) {
            restrictions.put(licenseResource.getId(), licenseResource.getValue());
        }

        return docAuthorityLicenseDto;
    }

    public static DocAuthorityLicenseDto convertLicenseFromBase64(String licenseStringBase64) {
        byte[] bytes = Base64.decodeBase64(licenseStringBase64);
        String licenseString = new String(bytes);
        try {
            return ModelDtoConversionUtils.getMapper().readValue(licenseString, DocAuthorityLicenseDto.class);
        } catch (IOException e) {
            logger.error("Failed to convert license string [" + licenseString + "] to DTO", e);
            throw new RuntimeException("Failed to convert license string [" + licenseString + "] to DTO", e);
        }
    }
}
