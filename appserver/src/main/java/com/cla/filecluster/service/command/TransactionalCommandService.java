package com.cla.filecluster.service.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Wrapper that provides transactions for Transactional Commands
 */
@Service
@Transactional
public class TransactionalCommandService {

    public <T> T runInTransaction(TransactionalCommand<T> command) {
        return command.execute();
    }

}
