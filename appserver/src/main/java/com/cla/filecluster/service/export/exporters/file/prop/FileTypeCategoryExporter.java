package com.cla.filecluster.service.export.exporters.file.prop;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.FileTypeCategoryDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.file.acl.AclAggCountDtoMixin;
import com.cla.filecluster.service.files.filetree.FileTreeAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

/**
 * Created by: ophir
 * Created on: 27/3/2019
 */
@Slf4j
@Component
public class FileTypeCategoryExporter extends PageCsvExcelExporter<AggregationCountItemDTO<FileTypeCategoryDto>> {

    final static String[] HEADER = {ID, NAME, NUM_OF_FILES};

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, AclAggCountDtoMixin.class);
        mapper.addMixIn(FileTypeCategoryDto.class, FileTypeCategoryDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<FileTypeCategoryDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<FileTypeCategoryDto>> res = fileTreeAppService.getTypeCategoryFileCounts(dataSourceRequest);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<FileTypeCategoryDto> base, Map<String, String> requestParams) {
		return null;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.FILE_TYPE_CATEGORIES);
    }
}
