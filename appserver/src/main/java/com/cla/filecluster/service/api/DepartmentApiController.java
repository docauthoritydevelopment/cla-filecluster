package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.department.*;
import com.cla.filecluster.util.LocalFileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.MNG_DEPARTMENT_CONFIG;

/**
 * Created by ginat on 14/03/2019.
 */
@RestController
@RequestMapping("/api/department")
public class DepartmentApiController {
    private Logger logger = LoggerFactory.getLogger(DepartmentApiController.class);

    @Autowired
    private DepartmentAppService departmentAppService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DepartmentSchemaLoader departmentSchemaLoader;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private EventAuditService eventAuditService;

    @Value("${department.defaultPageSize:1000}")
    private int departmentDefaultPageSize;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<DepartmentDto> getAllDepartments(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(departmentDefaultPageSize);
        return departmentService.getDepartments(dataSourceRequest);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public DepartmentDto getDepartment(@PathVariable Long id) {
        DepartmentDto department = departmentService.getDepartmentDtoById(id);
        if (department == null || department.isDeleted()) {
            throw new BadParameterException(messageHandler.getMessage("department.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return department;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public DepartmentDto addDepartment(@RequestBody DepartmentDto newDepartment) {
        DepartmentDto department = departmentService.createDepartment(newDepartment);
        eventAuditService.audit(AuditType.DEPARTMENT, "Create department", AuditAction.CREATE, DepartmentDto.class, department.getId(), department);
        return department;
    }

    @RequestMapping(value = "/clone", method = RequestMethod.PUT)
    public DepartmentDto cloneDepartment(@RequestBody DepartmentDto newDepartment) {
        DepartmentDto department = departmentService.cloneDepartment(newDepartment);
        eventAuditService.audit(AuditType.DEPARTMENT, "Clone department", AuditAction.CREATE, DepartmentDto.class, department.getId(), department);
        return department;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public DepartmentDto updateDepartment(@PathVariable Long id, @RequestBody DepartmentDto editDepartment) {
        DepartmentDto department = departmentService.updateDepartment(id, editDepartment);
        eventAuditService.audit(AuditType.DEPARTMENT, "Update department", AuditAction.UPDATE, DepartmentDto.class, department.getId(), department);
        return department;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteDepartments(@PathVariable Long id) {
        DepartmentDto department = getDepartment(id);
        departmentAppService.deleteDepartments(id);
        eventAuditService.audit(AuditType.DEPARTMENT, "Delete department", AuditAction.DELETE, DepartmentDto.class, department.getId(), department);
    }

    @RequestMapping(value="/{departmentId}/associate/folder/{docFolderId}", method = RequestMethod.PUT)
    public DocFolderDto associateDepartmentWithDocFolder(@PathVariable long departmentId, @PathVariable long docFolderId) {
        DepartmentDto department = getDepartment(departmentId);
        DocFolderDto docFolderDto = departmentAppService.associateFolderWithDepartment(departmentId, docFolderId);
        if(docFolderDto != null) {
            eventAuditService.auditDual(AuditType.DEPARTMENT, "Assign department to folder", AuditAction.ASSIGN, DepartmentDto.class, department.getId(), DocFolderDto.class, docFolderDto.getId(), docFolderDto);
        }
        return docFolderDto;
    }

    @RequestMapping(value="/{departmentId}/associate/folders", method = RequestMethod.PUT)
    public void associateDepartmentWithDocFolders(@PathVariable long departmentId, @PathVariable String folderIds) {
        String[] folderIdsArray = StringUtils.split(folderIds, ',');
        for (String docFolderId : folderIdsArray) {
            associateDepartmentWithDocFolder(departmentId ,Long.valueOf(docFolderId));
        }
    }

    @RequestMapping(value="/associate/folder", method = RequestMethod.PUT)
    public void associateDepartmentWithDocFolders(@RequestParam String departmentFullName, @RequestParam String folderPath) {
        Pair<DocFolderDto, DepartmentDto> result = departmentAppService.associateFolderWithDepartment(departmentFullName, folderPath);
        if(result != null) {
            eventAuditService.auditDual(AuditType.DEPARTMENT, "Assign department to folder", AuditAction.ASSIGN,
                    DepartmentDto.class, result.getValue().getId(), DocFolderDto.class, result.getKey().getId(), result.getKey());
        }
    }

    @RequestMapping(value="/{departmentId}/associate/folder/{docFolderId}", method = RequestMethod.DELETE)
    public DocFolderDto removeDepartmentAssociationFromDocFolder( @PathVariable long departmentId, @PathVariable long docFolderId) {
        logger.debug("remove department {} Association From Folder {}", departmentId, docFolderId);
        DepartmentDto department = getDepartment(departmentId);
        DocFolderDto docFolderDto = departmentAppService.removeDepartmentAssociationFromFolder(departmentId, docFolderId);
        if(docFolderDto!=null) {
            eventAuditService.auditDual(AuditType.DEPARTMENT, "Unassign department from folder", AuditAction.UNASSIGN, DepartmentDto.class, departmentId, DocFolderDto.class, docFolderId, docFolderDto);
        }
        return  docFolderDto;
    }

    @RequestMapping(value="/{departmentId}/associate/folders", method = RequestMethod.DELETE)
    public void removeDepartmentAssociationFromDocFolders( @PathVariable long departmentId, @PathVariable String folderIds) {
        String[] folderIdsArray = StringUtils.split(folderIds, ',');
        for (String docFolderId : folderIdsArray) {
            removeDepartmentAssociationFromDocFolder(departmentId, Long.valueOf(docFolderId));
        }
    }

    @RequestMapping(value = "/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<DepartmentDto>> findDepartmentFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.setPageSize(departmentDefaultPageSize);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover department export", AuditAction.VIEW, DepartmentDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover department", AuditAction.VIEW, DepartmentDto.class, null, dataSourceRequest);
        }
        FacetPage<AggregationCountItemDTO<DepartmentDto>> res = departmentAppService.findDepartmentFileCount(dataSourceRequest, true);
        res.setPageNumber(1);
        return res;
    }

    @RequestMapping(value = "/assigned/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<DepartmentDto>> findAssignedDepartmentFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY, "Discover department", AuditAction.VIEW, DepartmentDto.class, null, dataSourceRequest);
        return departmentAppService.findDepartmentFileCount(dataSourceRequest, false);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_DEPARTMENT_CONFIG})
    @RequestMapping(value="/import/csv", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto importCsvFile(@RequestParam("file") MultipartFile file,
                                         @RequestParam(value ="updateDuplicates", required = false, defaultValue = "false") boolean updateDuplicates,
                                         @RequestParam(value ="createStubs",required = false,defaultValue = "false") boolean createStubs){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("department.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import department CSV File. update duplicates set to {}",updateDuplicates);
            byte[] bytes = LocalFileUtils.getFileBytesWithoutBOM(file);
            ImportSummaryResultDto result = departmentSchemaLoader.doImport(false, bytes, null, updateDuplicates, createStubs);
            return result;
        } catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(),e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_DEPARTMENT_CONFIG})
    @RequestMapping(value="/import/csv/validate", method=RequestMethod.POST)
    public @ResponseBody ImportSummaryResultDto validateImportCsvFile(@RequestParam("file") MultipartFile file,
                                                                      @RequestParam("maxErrorRowsReport") Integer maxErrorRowsReport,
                                                                      @RequestParam(value ="createStubs", required = false, defaultValue = "false") boolean createStubs){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("department.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }

        try {
            logger.info("validate import department CSV File");
            byte[] bytes = LocalFileUtils.getFileBytesWithoutBOM(file);
            return departmentSchemaLoader.doImport(true, bytes,
                    maxErrorRowsReport == null ? 200 : maxErrorRowsReport,false, createStubs);
        } catch (Exception e) {
            logger.error("Validate import CSV error:" + e.getMessage(), e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription("Validate import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }
}
