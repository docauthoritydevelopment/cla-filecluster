package com.cla.filecluster.service.export.exporters.settings.component;

/**
 * Created by: yael
 * Created on: 5/27/2019
 */
public interface SystemComponentHeaderFields {
    String INSTANCE = "Instance id";
    String STATE = "State";
    String DATA_CENTER = "Data center";
    String LOCATION = "Location";
    String EXTERNAL_ADDR = "External net address";
    String LOCAL_ADDR = "Local net address";
}
