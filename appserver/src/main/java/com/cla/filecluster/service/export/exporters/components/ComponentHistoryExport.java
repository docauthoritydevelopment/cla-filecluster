package com.cla.filecluster.service.export.exporters.components;

import com.cla.common.domain.dto.components.ClaComponentStatusDto;
import com.cla.common.domain.dto.messages.control.DriveDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.components.mixin.ClaComponentStatusDtoMixin;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/17/2019
 */
public abstract class ComponentHistoryExport extends PageCsvExcelExporter<ClaComponentStatusDto> {

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    protected ComponentsAppService componentsAppService;

    protected DecimalFormat decimalFormatter = new DecimalFormat("###.##");

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ClaComponentStatusDto.class, ClaComponentStatusDtoMixin.class);
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    protected Page<ClaComponentStatusDto> getData(Map<String, String> params) {

        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);

        long componentId = 0L;
        if (params.containsKey("componentId")) {
            String dcStr = params.get("componentId");
            if (!Strings.isNullOrEmpty(dcStr)) {
                componentId = Long.parseLong(dcStr);
            }
        }

        Long dcId = 0L;
        if (params.containsKey("dataCenterID")) {
            String dcStr = params.get("dataCenterID");
            if (!Strings.isNullOrEmpty(dcStr)) {
                dcId = Long.parseLong(dcStr);
            }
        }
        long timestamp = System.currentTimeMillis();
        if (params.containsKey("timestamp")) {
            String timestampStr = params.get("timestamp");
            if (!Strings.isNullOrEmpty(timestampStr)) {
                timestamp = Long.parseLong(timestampStr);
            }
        }

        return sysComponentService.listSysComponentsStatus(componentId, timestamp, dcId, dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(ClaComponentStatusDto base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();

        String driveData = base.getDriveData();
        DriveDto driveDto = componentsAppService.getDriveDataFromString(driveData);
        if (driveDto != null && driveDto.getDriveCapacities() != null && !driveDto.getDriveCapacities().isEmpty()) {
            DriveDto.DriveCapacityMetrics drive = driveDto.getDriveCapacities().get(0);
            if (drive != null) {
                long memFull = drive.getTotalSpace() - drive.getFreeSpace();
                int util = (int)(((float)memFull/drive.getTotalSpace())*100);
                res.put(DISK_SPACE, drive.getDriveId() + " "
                        + memFull + "MB/" + drive.getTotalSpace()
                        + "MB (" + util + "%)");
            }
        }
        return res;
    }
}
