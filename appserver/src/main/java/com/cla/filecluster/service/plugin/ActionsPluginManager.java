package com.cla.filecluster.service.plugin;

import com.cla.common.domain.dto.messages.action.ActionPluginRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * exexutor for PluginActionRequest. according to the config in the request,
 * it creates the correct action plugin to handle the request
 * listens on the requests queue and holds a thread pool to handle them
 * Created by: yael
 * Created on: 1/11/2018
 */
@Service
public class ActionsPluginManager {

    private Logger logger = LoggerFactory.getLogger(ActionsPluginManager.class);

    @Autowired
    private BlockingQueue<ActionPluginRequest> actionPluginRequestQueue;

    private List<PluginExecutor> threadList;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${plugin-executor.thread-pool-size:2}")
    private int threadPoolSize;

    @PostConstruct
    void init() {
        logger.debug("starting up threads");
        threadList = new ArrayList<>();
        for (int i = 0; i < threadPoolSize; ++i) {
            PluginExecutor t = new PluginExecutor(actionPluginRequestQueue);
            threadList.add(t);
            t.start();
        }
    }

    @PreDestroy
    void destroy() {
        logger.debug("shutting down threads");
        for (PluginExecutor t : threadList) {
            t.shutdown();
            t.interrupt();
        }
    }

    class PluginExecutor extends Thread {
        private BlockingQueue<ActionPluginRequest> inputQueue;
        private boolean run = true;

        PluginExecutor(BlockingQueue<ActionPluginRequest> inputQueue) {
            this.inputQueue = inputQueue;
        }

        void shutdown() {
            this.run = false;
        }

        @Override
        public void run() {
            while (this.run) {
                try {
                    ActionPluginRequest request = inputQueue.take();
                    logger.debug("handling new request from queue {}", request);
                    ActionPlugin plugin = applicationContext.getBean(request.getConfig().getPluginId(), ActionPlugin.class);
                    plugin.handleAction(request);
                } catch (InterruptedException e) {
                    // do nothing, should check the shutdown flag
                } catch (Exception e) {
                    logger.error("failed action plugin for request", e);
                }
            }
        }
    }
}
