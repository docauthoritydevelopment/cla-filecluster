package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.cla.common.domain.dto.filesearchpatterns.RegulationDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.extractor.pattern.RegulationAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.searchPattern.TextSearchPatternHeaderFields.*;

/**
 * Created by: ophir
 * Created on: 4/18/2019
 */
@Slf4j
@Component
public class RegulationSettingsExporter extends PageCsvExcelExporter<RegulationDto> {

    private final static String[] HEADER = {NAME};

    @Autowired
    private RegulationAppService regulationAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(RegulationDto.class, RegulationSettingsDtoMixin.class);
    }

    @Override
    protected Page<RegulationDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return regulationAppService.listRegulations(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(RegulationDto base, Map<String, String> requestParams) {
        return null;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.REGULATIONS_SETTINGS);
    }
}
