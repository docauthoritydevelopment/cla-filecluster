package com.cla.filecluster.service.export.exporters;

public enum PagingMethod {
    SINGLE, MULTI, MULTI_DEEP
}
