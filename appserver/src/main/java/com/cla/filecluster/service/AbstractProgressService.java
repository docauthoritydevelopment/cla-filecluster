package com.cla.filecluster.service;

import com.cla.eventbus.EventBus;
import com.cla.eventbus.events.Context;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventKeys;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.function.Consumer;

public abstract class AbstractProgressService {

    @Autowired
    protected EventBus eventBus;

    @Autowired
    protected FileCrawlerExecutionDetailsService runService;

    @Autowired
    protected JobManagerService jobManagerService;

    @PostConstruct
    void init() {
        eventBus.subscribe(topic(), eventConsumer());
    }

    protected abstract String topic();

    protected abstract Consumer<Event> eventConsumer();

    public void setJobOpMsg(Context context) {
        Long jobId = context.get(EventKeys.JOB_ID, Long.class);
        String opMsg = context.get(EventKeys.OPERATION_MESSAGE, String.class);
        jobManagerService.setOpMsg(jobId, opMsg);
    }

}
