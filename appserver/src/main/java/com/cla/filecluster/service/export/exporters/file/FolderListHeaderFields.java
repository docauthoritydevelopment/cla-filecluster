package com.cla.filecluster.service.export.exporters.file;

import com.cla.filecluster.service.export.exporters.ScannedItemsHeaderFields;

public interface FolderListHeaderFields extends ScannedItemsHeaderFields {
    String PATH = "Path";
    String ASSOCIATION = "Clients association";
    String DOC_TYPES = "DocTypes";
    String TAGS = "Tags";
    String ROLES = "Functional roles";
    String NUM_OF_DIRECT_FILTERED = "Number of Filtered Direct Files";
    String NUM_OF_DIRECT = "Number of Direct files";
    String NUM_OF_FILES = "Number of All Files";
}
