package com.cla.filecluster.service.extractor;

import java.util.ArrayList;
import java.util.List;

public class GenericSqlReport {
	private String shortName;
	private String title;
	private String sql;
	private List<String> headers;	// report columns in display order
	private List<String> classes;	// column style class names (to match the template)
	private String htmlBaseFilename;
	private int pageSize;
	private String template;

	public GenericSqlReport(final String shortName, final String title, final String sql,
			final List<String> headers, final List<String> classes, final String htmlBaseFilename,
			final int pageSize, final String template) {
		this.shortName = shortName;
		this.title = title;
		this.sql = sql;
		this.headers = headers;
		this.classes = classes;
		this.htmlBaseFilename = htmlBaseFilename;
		this.pageSize = pageSize;
		this.template = template;
	}
	
	public GenericSqlReport(final GenericSqlReport o) {
		this.shortName = new String(o.shortName);
		this.title = new String(o.title);
		this.sql = new String(o.sql);
		this.headers = new ArrayList<>(o.headers);
		this.classes = (o.classes==null)?null:new ArrayList<>(o.classes);
		this.htmlBaseFilename = (o.htmlBaseFilename==null)?null:new String(o.htmlBaseFilename);
		this.pageSize = o.pageSize;
		this.template = (o.template==null)?null:new String(o.template);
	}
	
	public String getShortName() {
		return shortName;
	}
	public void setShortName(final String shortName) {
		this.shortName = shortName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(final String title) {
		this.title = title;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(final String sql) {
		this.sql = sql;
	}
	public List<String> getHeaders() {
		return headers;
	}
	public void setHeaders(final List<String> headers) {
		this.headers = headers;
	}
	public List<String> getClasses() {
		return classes;
	}
	public void setClasses(final List<String> classes) {
		this.classes = classes;
	}
	public String getHtmlBaseFilename() {
		return htmlBaseFilename;
	}
	public void setHtmlBaseFilename(final String htmlBaseFilename) {
		this.htmlBaseFilename = htmlBaseFilename;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(final String template) {
		this.template = template;
	}
}

