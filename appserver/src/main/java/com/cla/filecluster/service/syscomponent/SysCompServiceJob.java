package com.cla.filecluster.service.syscomponent;

import com.cla.filecluster.repository.jpa.monitor.ClaComponentStatusRepository;
import com.cla.filecluster.service.util.PartitionManagementJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * joc to manage components status table partitions
 *
 * Created by: yael
 * Created on: 3/19/2018
 */
@Service
@Profile({ "prod", "win", "dev", "default" })
public class SysCompServiceJob extends PartitionManagementJob {

    private Logger logger = LoggerFactory.getLogger(SysCompServiceJob.class);

    @Value("${components-status.mng-partition-days-add-partition:7}")
    private int daysToAddPartition;

    @Value("${components-status.mng-partition-days-delete-partition:120}")
    private int daysToDeletePartition;

    @Autowired
    private ClaComponentStatusRepository claComponentStatusRepository;

    @PostConstruct
    void init() {
        managePartitions();
    }

    @Scheduled(initialDelayString = "${components-status.mng-partition-initial-delay-millis:10000}",
            fixedRateString = "${components-status.mng-partition-frequency-millis:18000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void managePartitions() {
        managePartitions(daysToAddPartition, daysToDeletePartition, claComponentStatusRepository);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
