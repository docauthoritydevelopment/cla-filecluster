package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterSummaryInfo;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Handle CRUD operations for a data center
 * Created by liora on 29/05/2017.
 */
@RestController
@RequestMapping("/api/sys/customerDataCenter")
public class CustomerDataCenterApiController {

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    private Logger logger = LoggerFactory.getLogger(CustomerDataCenterApiController.class);

    @RequestMapping(value="/list", method = RequestMethod.GET)
    public Page<CustomerDataCenterDto> listCustomerDataCenters(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        //  dataSourceRequest.addDefaultSort("name");
        return customerDataCenterService.listCustomerDataCenters(dataSourceRequest);
    }

    @RequestMapping(value="/names", method = RequestMethod.GET)
    public Map<Long, String> getCustomerDataCentersNames() {
        return customerDataCenterService.getCustomerDataCentersNames();
    }

    @RequestMapping(value="/summary/list", method = RequestMethod.GET)
    public Page<CustomerDataCenterSummaryInfo> listCustomerDataCentersSummary(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return customerDataCenterService.listCustomerDataCentersSummary(dataSourceRequest);
    }

    @RequestMapping(value="/new", method = RequestMethod.PUT)
    public CustomerDataCenterDto createCustomerDataCenter(@RequestBody final CustomerDataCenterDto customerDataCenterDto) {
        CustomerDataCenterDto customerDataCenter = customerDataCenterService.createCustomerDataCenter(customerDataCenterDto);
        eventAuditService.audit(AuditType.CUSTOMER_DATA_CENTER,"Create customer data center", AuditAction.CREATE, CustomerDataCenterDto.class,customerDataCenter.getId(),customerDataCenter);
        return customerDataCenter;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.POST)
    public CustomerDataCenterDto updateCustomerDataCenter(@PathVariable Long id, @RequestBody final CustomerDataCenterDto customerDataCenterDto) {
        customerDataCenterDto.setId(id);
        CustomerDataCenterDto customerDataCenterDto1 = customerDataCenterService.updateCustomerDataCenter(customerDataCenterDto);
        eventAuditService.audit(AuditType.CUSTOMER_DATA_CENTER,"Update customer data center", AuditAction.UPDATE, CustomerDataCenterDto.class,customerDataCenterDto1.getId(),customerDataCenterDto1);
        return customerDataCenterDto1;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public CustomerDataCenterDto getCustomerDataCenter(@PathVariable Long id) {
        CustomerDataCenterDto customerDataCenter = customerDataCenterService.getCustomerDataCenter(id);
        if (customerDataCenter == null) {
            throw new BadRequestException(messageHandler.getMessage("data-center.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return customerDataCenter;
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    public void deleteComponent(@PathVariable Long id) {
        CustomerDataCenterDto customerDataCenter = getCustomerDataCenter(id);
        customerDataCenterService.deleteCustomerDataCenter(id);
        eventAuditService.audit(AuditType.CUSTOMER_DATA_CENTER,"Delete customer data center", AuditAction.DELETE, CustomerDataCenterDto.class, customerDataCenter.getId(),customerDataCenter);
    }

}
