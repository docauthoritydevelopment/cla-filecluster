package com.cla.filecluster.service.audit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * This is a wrapper for the audit logger - that writes to the db
 * so we can unit test the audit service
 * Created by: yael
 * Created on: 11/28/2017
 */
@Service
public class AuditWrapper {

    private final Logger auditLogger = LoggerFactory.getLogger("audit");

    public void audit(Object arg) {
        auditLogger.info("", arg);
    }
}
