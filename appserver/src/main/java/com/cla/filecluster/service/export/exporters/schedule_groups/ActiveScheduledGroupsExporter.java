package com.cla.filecluster.service.export.exporters.schedule_groups;

import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.schedule_groups.mixin.CrawlRunDetailsDtoMixin;
import com.cla.filecluster.service.export.exporters.schedule_groups.mixin.ScheduleGroupDtoMixin;
import com.cla.filecluster.service.export.exporters.schedule_groups.mixin.ScheduleGroupSummaryInfoDtoMixin;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.filecluster.service.export.exporters.schedule_groups.ActiveScheduledGroupsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
@Slf4j
@Component
public class ActiveScheduledGroupsExporter extends PageCsvExcelExporter<ScheduleGroupSummaryInfoDto> {

    @Autowired
    private ScheduleAppService scheduleAppService;

    private final static String[] header = {NAME, PERIOD, FOLDER_NUM, STATUS, NEXT_SCAN, LAST_SCAN};

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.ACTIVE_SCHEDULED_GROUPS);
    }

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(ScheduleGroupSummaryInfoDto.class, ScheduleGroupSummaryInfoDtoMixin.class)
                .addMixIn(ScheduleGroupDto.class, ScheduleGroupDtoMixin.class)
                .addMixIn(CrawlRunDetailsDto.class, CrawlRunDetailsDtoMixin.class);
    }

    @Override
    protected Page<ScheduleGroupSummaryInfoDto> getData(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.addDefaultSort("name");
        String groupNameSearch = params == null ? null : params.get("groupNamingSearchTerm");
        String groupIdsStr = params == null ? null : params.get("scheduleGroupIds");

        List<Long> groupIds = null;
        if (!Strings.isNullOrEmpty(groupIdsStr)) {
            groupIds = Arrays.stream(groupIdsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        }
        return scheduleAppService.listScheduleGroupsSummaryInfo(dataSourceRequest, groupNameSearch, groupIds);
    }

    @Override
    protected Map<String, Object> addExtraFields(ScheduleGroupSummaryInfoDto base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();
        int folders = base.getScheduleGroupDto().getRootFolderIds() == null ? 0 :
                base.getScheduleGroupDto().getRootFolderIds().size();
        res.put(FOLDER_NUM, String.valueOf(folders));
        return res;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
