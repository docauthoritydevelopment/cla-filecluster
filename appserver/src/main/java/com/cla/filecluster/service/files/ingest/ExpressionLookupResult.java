package com.cla.filecluster.service.files.ingest;

import com.cla.common.domain.dto.extraction.SearchPatternCountDto;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * represents a result of pattern search in content
 *
 * Created by: yael
 * Created on: 1/28/2018
 */
public class ExpressionLookupResult {

    private String content;
    private Set<SearchPatternCountDto> searchPatternsCounting = null;

    public ExpressionLookupResult() {
    }

    public ExpressionLookupResult(String content, Set<SearchPatternCountDto> searchPatternsCounting) {
        this.content = content;
        this.searchPatternsCounting = searchPatternsCounting;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<SearchPatternCountDto> getSearchPatternsCounting() {
        return searchPatternsCounting;
    }

    public void setSearchPatternsCounting(Set<SearchPatternCountDto> searchPatternsCounting) {
        this.searchPatternsCounting = searchPatternsCounting;
    }

    @Override
    public String toString() {
        return "ExpressionLookupResult{" +
                "content='" + content + '\'' +
                ", searchPatternsCounting=" + searchPatternsCounting +
                '}';
    }

    public String toExternalString() {
        return "Pattern search results:\n\n" +
                "content='" + content + '\'' +
                "\n\nMatches=" +
                searchPatternsCounting.stream().map(spt->spt.toExternalString()).collect(Collectors.joining("|", "[", "]")) +
                '}';
    }

}
