package com.cla.filecluster.service.operation;

import com.cla.common.domain.dto.schedule.ScheduledOperationMetadataDto;
import com.cla.common.domain.dto.schedule.ScheduledOperationState;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.common.domain.dto.schedule.ScheduledOperationPriority;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.group.ScheduledOperationRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.scheduling.ControlledScheduling;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.util.query.DBTemplateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * We want to be able to recover large operations on groups in case of restart or solr failure
 * so these should run offline in steps to allow recovery and avoid db inconsistency
 * we use a db table to keep track of what should run
 * current supported operations see ScheduledOperationType class
 * <p>
 * Created by: yael
 * Created on: 5/31/2018
 */
@Service
public class ScheduledOperationService implements ControlledScheduling {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledOperationService.class);

    @Autowired
    private ScheduledOperationRepository scheduledOperationRepository;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Autowired
    private DBTemplateUtils dBTemplateUtils;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private UserService userService;

    @Value("${scheduled-operations.page-size:20}")
    private int scheduledOperationPageSize;

    @Value("${scheduled-operations.display-failed-min:360}")
    private int timeToDisplayFailedMin;

    @Autowired
    private List<ScheduledOperation> operations;

    private List<Long> userSolrOperations = new ArrayList<>();
    private List<String> groupsWithOperations = new ArrayList<>();
    private ScheduledOperationMetadata currentOperation = null;
    private AtomicBoolean currentlyRunning = new AtomicBoolean(false);
    private AtomicBoolean isFirstTime = new AtomicBoolean(true);

    private boolean isSchedulingActive = false;
    private long timeToDisplayFailedMillis;

    public boolean isSchedulingActive() {
        return isSchedulingActive;
    }

    public void setSchedulingActive(boolean schedulingActive) {
        isSchedulingActive = schedulingActive;
    }

    public List<String> getGroupsWithOperations() {
        return groupsWithOperations;
    }

    public boolean hasUserSolrOpInQueue() {
        return userSolrOperations.size() > 0;
    }

    public boolean isUserSolrOpRunning() {
        return (currentOperation != null && currentOperation.getOperationType().isSolrApplyOperation() && currentOperation.getUserOperation());
    }

    @PostConstruct
    void initService() {
        timeToDisplayFailedMillis = TimeUnit.MINUTES.toMillis(timeToDisplayFailedMin);
    }

    private synchronized void init() {
        if (isFirstTime.get()) {
            isFirstTime.set(false);
            List<ScheduledOperationMetadata> operations = scheduledOperationRepository.findAll();
            for (ScheduledOperationMetadata op : operations) {
                if (op.getOperationType().isSolrApplyOperation() && op.getUserOperation()) {
                    userSolrOperations.add(op.getId());
                } else if (op.getOperationType().isGroupOperation()) {
                    ScheduledOperation operation = getForType(op.getOperationType());
                    groupsWithOperations.addAll(operation.getRelevantGroupIds(op.getOpData()));
                }
            }

            // Look for files in transition state
            // TODO: This should be revisited as part of Service Refactoring - should do this as a seperated 'set state' service.
            ScheduledOperationMetadata data = new ScheduledOperationMetadata();
            data.setOperationType(ScheduledOperationType.STARTUP_FILE_STATUS_CNG);
            data.setUserOperation(false);
            data.setPriority(ScheduledOperationPriority.HIGH);
            createNewOperation(data);
        }
    }

    @Scheduled(initialDelayString = "${scheduled-operations.initial-delay-millis:20000}",
            fixedDelayString = "${scheduled-operations.frequency-millis:20000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    @AutoAuthenticate
    public void runOperations() {

        if (!isSchedulingActive) {
            return;
        }

        if (!currentlyRunning.compareAndSet(false, true)) {
            return;
        }

        if (isFirstTime.get()) {
            init();
        }

        try {
            long lastChkSolrUp = 0L;
            long lastPendingCheck = System.currentTimeMillis();
            List<ScheduledOperationMetadata> operations = getOperationsToPerform();
            for (ScheduledOperationMetadata data : operations) {
                if (lastChkSolrUp < System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)) {
                    if (!componentsAppService.isSolrUp()) {
                        logger.info("solr seems to be down, try again later");
                        return;
                    }
                    lastChkSolrUp = System.currentTimeMillis();
                }

                if (lastPendingCheck < System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(5)) {
                    lastPendingCheck = System.currentTimeMillis();
                    logger.info("The amount of pending scheduled operations is currently {}", getPendingOperationsNum());
                }

                try {
                    synchronized (this) {
                        if (scheduledOperationRepository.findById(data.getId()).isPresent()) {
                            currentOperation = data;
                        } else {
                            logger.info("skip deleted operation {}", data);
                            continue;
                        }
                    }
                    logger.debug("going to start run scheduled operation {}", currentOperation);
                    boolean wasRan = runOperation(currentOperation);
                    if (wasRan) {
                        logger.debug("finished run scheduled operation {}", currentOperation);
                    } else {
                        logger.debug("currently skipping scheduled operation {}", currentOperation);
                    }
                } catch (GroupLockAcqException e) {
                    logger.info("cant lock group for operation, try again later {}", currentOperation);
                } catch (Exception e) {
                    logger.error("error during scheduled operation {}", currentOperation, e);
                    endOperation(currentOperation, true);
                } finally {
                    synchronized (this) {
                        currentOperation = null;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("scheduled operation run failed", e);
        } finally {
            currentlyRunning.set(false);
        }
    }

    // used to create a new offline operation
    public synchronized void createNewOperation(ScheduledOperationMetadata data) {

        if (data.getOperationType() == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.type.empty"), BadRequestType.MISSING_FIELD);
        }

        if (data.getOperationType().hasParams() && (data.getOpData() == null || data.getOpData().isEmpty())) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.parameters.empty"), BadRequestType.MISSING_FIELD);
        }

        ScheduledOperation operation = getForType(data.getOperationType());
        if (!isSchedulingActive && !operation.shouldAllowCreateWhileScheduleDown()) {
            logger.info("Cannot create scheduled operation when scheduling is not turned on. Metadata: {}", data);
            return;
        }

        if (operation.operationTypeUnique()) {
            List<ScheduledOperationMetadata> byType = scheduledOperationRepository.findAllByType(data.getOperationType());
            if (byType != null && byType.size() > 0) {
                logger.info("Cannot create unique scheduled operation that already exists in db. Metadata: {}", data);
                return;
            }
        }

        operation.validate(data.getOpData());

        List<String> relevantGroupIds = null;
        if (data.getOperationType().isGroupOperation()) {
            relevantGroupIds = operation.getRelevantGroupIds(data.getOpData());
        }

        if (data.getPriority() == null) {
            data.setPriority(ScheduledOperationPriority.LOW);
        }

        if (data.getUserOperation()) {
            User owner = userService.getCurrentUserEntity();
            if (owner != null) {
                data.setOwnerId(owner.getId());
            }
        }

        dBTemplateUtils.doInTransaction(() -> {
            data.setCreateTime(System.currentTimeMillis());
            data.setUpdateTime(System.currentTimeMillis());
            data.setStep(0);
            data.setShouldDisplay(true);
            data.setState(ScheduledOperationState.NEW);
            scheduledOperationRepository.save(data);
        });

        if (data.getOperationType().isSolrApplyOperation() && data.getUserOperation()) {
            userSolrOperations.add(data.getId());
        } else if (data.getOperationType().isGroupOperation() && relevantGroupIds != null) {
            groupsWithOperations.addAll(relevantGroupIds);
        }
    }

    // get the operations waiting to be performed
    private List<ScheduledOperationMetadata> getOperationsToPerform() {
        PageRequest req = PageRequest.of(0, scheduledOperationPageSize);
        Callable<Page<ScheduledOperationMetadata>> task = () -> scheduledOperationRepository.findAll(req);
        Page<ScheduledOperationMetadata> operations = dBTemplateUtils.doInTransaction(task);
        return operations.getContent();
    }

    private long getPendingOperationsNum() {
        Callable<Long> task = () -> scheduledOperationRepository.countPending();
        return dBTemplateUtils.doInTransaction(task);
    }

    // run a single operation
    private boolean runOperation(ScheduledOperationMetadata data) {
        ScheduledOperation operation = getForType(data.getOperationType());
        int step = data.getStep();

        boolean isOperationFinished = false;
        if (operation.operationAllowedToRun(data)) {
            operation.preOperation(data.getOpData());
            try {
                while (step < operation.getTotalStepNum() && !isOperationFinished) {
                    updateOperationStartDateCurrentStep(data, step);
                    logger.debug("run scheduled operation step {} for {}", step, data);
                    isOperationFinished = operation.runStep(step, data.getOpData());
                    ++step;
                }
                logger.debug("end scheduled operation {}", data);
                endOperation(data, false);
            } finally {
                operation.postOperation(data.getOpData());
            }
            return true;
        } else {
            logger.debug("scheduled operation not currently allowed for {}", data);
            return false;
        }
    }

    // delete the record from db when done
    private void endOperation(ScheduledOperationMetadata data, boolean onError) {
        try {
            synchronized (this) {
                if (data.getOperationType().isSolrApplyOperation() && data.getUserOperation()) {
                    userSolrOperations.remove(data.getId());
                } else if (data.getOperationType().isGroupOperation()) {
                    ScheduledOperation operation = getForType(data.getOperationType());
                    groupsWithOperations.removeAll(operation.getRelevantGroupIds(data.getOpData()));
                }
            }

            if (onError && data.getRetryLeft() != null && data.getRetryLeft() > 0) {
                data.setRetryLeft(data.getRetryLeft() - 1);
                data.setState(ScheduledOperationState.ENQUEUED);
                dBTemplateUtils.doInTransaction(() -> scheduledOperationRepository.save(data));
            } else if (onError) {
                data.setRetryLeft(0);
                data.setState(ScheduledOperationState.FAILED);
                dBTemplateUtils.doInTransaction(() -> scheduledOperationRepository.save(data));
            } else {
                dBTemplateUtils.doInTransaction(() -> scheduledOperationRepository.deleteById(data.getId()));
            }
        } catch (Exception e) {
            logger.error("end scheduled operation failed for {}", data, e);
        }
    }

    // update operation state in db between steps
    private void updateOperationStartDateCurrentStep(ScheduledOperationMetadata data, int step) {
        dBTemplateUtils.doInTransaction(() -> scheduledOperationRepository.findById(data.getId())
                .ifPresent(one -> {
                    one.setStartTime(System.currentTimeMillis());
                    one.setStep(step);
                    one.setState(ScheduledOperationState.IN_PROGRESS);
                    one.setOpData(data.getOpData());
                    scheduledOperationRepository.save(one);
                }));
    }

    // get the operation class the handle the specific operation type
    private ScheduledOperation getForType(ScheduledOperationType type) {
        for (ScheduledOperation operation : operations) {
            if (operation.operationTypeHandled().equals(type)) {
                return operation;
            }
        }
        throw new BadRequestException(messageHandler.getMessage("scheduled-operations.operator.missing"), BadRequestType.UNSUPPORTED_VALUE);
    }

    public ScheduledOperationMetadataDto getById(Long id) {
        ScheduledOperationMetadata metadata = scheduledOperationRepository.findById(id).orElse(null);
        return convertScheduledOperationMetadataDto(metadata);
    }

    @Transactional
    public synchronized boolean deleteOperation(Long id) {
        if (currentOperation != null && currentOperation.getId().equals(id)) {
            return false;
        }
        scheduledOperationRepository.deleteById(id);
        return true;
    }

    @Transactional
    public void updateShouldNotDisplay(Long id) {
        ScheduledOperationMetadata metadata = scheduledOperationRepository.getOne(id);
        metadata.setShouldDisplay(false);
        scheduledOperationRepository.save(metadata);
    }

    public List<ScheduledOperationMetadataDto> getAllPending() {
        List<ScheduledOperationMetadata> metadata = scheduledOperationRepository.findAll();
        return convertScheduledOperationMetadataDto(metadata);
    }

    public List<ScheduledOperationMetadataDto> getAllUserOpPending() {
        List<ScheduledOperationMetadata> metadata = scheduledOperationRepository.findAllUserOperations(System.currentTimeMillis() - timeToDisplayFailedMillis);
        return convertScheduledOperationMetadataDto(metadata);
    }

    public List<ScheduledOperationMetadataDto> findAllUserOperations(Long ownerId) {
        List<ScheduledOperationMetadata> metadata = scheduledOperationRepository.findAllUserOperations(ownerId, System.currentTimeMillis() - timeToDisplayFailedMillis);
        return convertScheduledOperationMetadataDto(metadata);
    }

    private static ScheduledOperationMetadataDto convertScheduledOperationMetadataDto(ScheduledOperationMetadata metadata) {
        if (metadata == null) {
            return null;
        }
        ScheduledOperationMetadataDto dto = new ScheduledOperationMetadataDto();
        dto.setId(metadata.getId());
        dto.setCreateTime(metadata.getCreateTime());
        dto.setUpdateTime(metadata.getUpdateTime());
        dto.setOpData(metadata.getOpData());
        dto.setPriority(metadata.getPriority());
        dto.setOperationType(metadata.getOperationType());
        dto.setStartTime(metadata.getStartTime());
        dto.setUserOperation(metadata.getUserOperation());
        dto.setStep(metadata.getStep());
        dto.setRetryLeft(metadata.getRetryLeft());
        dto.setOpCondition(metadata.getOpCondition());
        dto.setOwnerId(metadata.getOwnerId());
        dto.setDescription(metadata.getDescription());
        dto.setState(metadata.getState());
        return dto;
    }

    private static List<ScheduledOperationMetadataDto> convertScheduledOperationMetadataDto(List<ScheduledOperationMetadata> metadata) {
        if (metadata == null) {
            return null;
        }

        return metadata.stream()
                .map(ScheduledOperationService::convertScheduledOperationMetadataDto)
                .collect(Collectors.toList());
    }
}
