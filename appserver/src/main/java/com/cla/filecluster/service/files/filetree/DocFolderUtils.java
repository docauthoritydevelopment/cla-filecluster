package com.cla.filecluster.service.files.filetree;

import com.cla.connector.domain.dto.media.MediaType;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by uri on 23/12/2015.
 */
public class DocFolderUtils {
    private static final Logger logger = LoggerFactory.getLogger(DocFolderUtils.class);

    public final static String ENCODING_SEPARATOR = "x";

    public static List<String> decodeFolderParentsInfo(String parentFoldersInfo) {
        String[] sp = parentFoldersInfo.split(ENCODING_SEPARATOR);
        return Arrays.asList(sp);
    }

    private static final Pattern replacePathCharsPattern = Pattern.compile("[\\\\\\/:'\"]");

    public static String pathToSimpleString(String pathString) {
        if (pathString == null) {
            return null;
        }
        Matcher matcher = replacePathCharsPattern.matcher(pathString);
        return matcher.replaceAll("_");
    }

    public static int calculateDepth(String canonicalPath) {
        int depth = StringUtils.countMatches(canonicalPath, "/") - 1;
        return depth < 0 ? 0 : depth;
    }

    /**
     * Extract the folder name from the file name without using Path
     * @param url
     * @return the name of the folder
     */
    public static String extractFolderFromPath(String url) {
        String updatedUrl = url;
        String originalEnding = null;
        if (url.endsWith("/") || url.endsWith("\\")) {
            //TODO - check if we can remove this entire if clause
            updatedUrl = url.substring(0,url.length()-1);
            originalEnding = url.substring(url.length()-1,url.length());
        }
        String fullPath = FilenameUtils.getFullPath(updatedUrl);
        if (fullPath == null && originalEnding == null) {
            logger.warn("Problem with folder url is null for {} original {}", updatedUrl, url);
            throw new RuntimeException("Problem with folder url is null for "+url);
        }
        if (originalEnding == null) {
            //This was a file that we extracted the folder from
            fullPath = removeTrailingSlash(fullPath);
        }
        return fullPath;
    }

    public static String removeTrailingSlash(String fullPath) {
        if (fullPath == null || fullPath.isEmpty()) {
            return fullPath;
        }
        if (fullPath.endsWith("/") || fullPath.endsWith("\\")) {
            fullPath = fullPath.substring(0,fullPath.length()-1);
        }
        return fullPath;
    }

    public static String removeStartingSlash(String fullPath) {
        if (fullPath == null || fullPath.isEmpty()) {
            return fullPath;
        }
        if (fullPath.startsWith("/") || fullPath.startsWith("\\")) {
            fullPath = fullPath.substring(1);
        }
        return fullPath;
    }

    public static String extractFolderName(String universalPath) {
        //A universal path always ends with a /
        String withoutTrailingSlash = universalPath.substring(0, universalPath.length() - 1);
        return FilenameUtils.getName(withoutTrailingSlash);
    }

    public static String convertToAbsolute(MediaType mediaType, final String sourcePath){
        String result = sourcePath;
        if (MediaType.FILE_SHARE.equals(mediaType)) {
            Path path = Paths.get(sourcePath.trim());
            if (!path.isAbsolute()) {
                result = path.toAbsolutePath().normalize().toString();
            } else {
                result = path.normalize().toString();
            }
        }
        return result;
    }
}
