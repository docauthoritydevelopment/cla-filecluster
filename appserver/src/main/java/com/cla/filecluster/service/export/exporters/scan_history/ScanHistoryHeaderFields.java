package com.cla.filecluster.service.export.exporters.scan_history;

/**
 * Created by: yael
 * Created on: 3/27/2019
 */
public interface ScanHistoryHeaderFields {

    String ID = "Id";
    String NICKNAME = "Nick Name";
    String PATH = "Path";
    String STATE = "State";
    String TRIGGER = "Run trigger";
    String SCHEDULED_GROUP = "Schedule Group";
    String START_TIME = "Start Time";
    String END_TIME = "End Time";
}
