package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 8/8/2018
 */
@Service
public class RemoveDocTypeThatMoved implements ScheduledOperation {

    private static final Logger logger = LoggerFactory.getLogger(RemoveDocTypeThatMoved.class);

    public static String DOC_TYPE_ID_PARAM = "DOC_TYPE_ID";
    public static String DOC_TYPE_IDENTIFIER_PARAM = "DOC_TYPE_IDENTIFIER";

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private DocTypeAppService docTypeAppService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 2;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        String docTypeParam = opData.get(DOC_TYPE_IDENTIFIER_PARAM);
        String docTypeIdParam = opData.get(DOC_TYPE_ID_PARAM);
        Long docTypeId = Long.parseLong(docTypeIdParam);
        if (step == 0) {
            Map<String, DocType> res = docTypeService.getDocTypesByIdentiers(Lists.newArrayList(docTypeParam));
            if (res != null && !res.isEmpty()) {
                logger.debug("a doc type exists with this identifier - do nothing");
                return true;
            }
        } else if (step == 1) {
            docTypeAppService.removeDocTypeIdentifierFromFiles(docTypeId, docTypeParam);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String docTypeParam = opData.get(DOC_TYPE_IDENTIFIER_PARAM);
        if (Strings.isNullOrEmpty(docTypeParam)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.doctype.identifier.empty"), BadRequestType.MISSING_FIELD);
        }
        String docTypeIdParam = opData.get(DOC_TYPE_ID_PARAM);
        if (Strings.isNullOrEmpty(docTypeIdParam)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.doctype.empty"), BadRequestType.MISSING_FIELD);
        }
        try {
            Long.parseLong(docTypeIdParam);
        } catch (Exception e) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.doctype.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.DOC_TYPE_MOVE;
    }
}
