package com.cla.filecluster.service.actions;

/**
 * Created by: yael
 * Created on: 3/12/2018
 */
public interface ActionRunnerInterface extends Runnable {
    void setActionScheduleId(Long actionScheduleId);
}
