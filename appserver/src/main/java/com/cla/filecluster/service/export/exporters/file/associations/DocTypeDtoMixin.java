package com.cla.filecluster.service.export.exporters.file.associations;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class DocTypeDtoMixin {

    @JsonProperty(DocTypeHeaderFields.DESCRIPTION)
    private String description;
}
