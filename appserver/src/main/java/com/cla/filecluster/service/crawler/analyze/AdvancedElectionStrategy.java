package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.domain.dto.group.GroupType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.filetag.FileTagService;

import java.util.Collection;
import java.util.List;

public class AdvancedElectionStrategy implements JoinGroupsElectionStrategy{

    // reject candidate (non-elected) groups with size over this threshold
    // i.e. a group with size over threshold will not be merged into another group
    private long rejectSizeThreshold = Long.MAX_VALUE;

    @Override
    public JoinGroupsElectionResult electTargetGroup(Collection<SolrFileGroupEntity> candidateGroups, SolrFileGroupRepository fileGroupRepository, FileTagService fileTagService) {

        long bestScore = 0;
        SolrFileGroupEntity bestScoreGroup = null;

        JoinGroupsElectionResult result = new JoinGroupsElectionResult();

        for (SolrFileGroupEntity group : candidateGroups) {
            long size = group.getNumOfFiles() == null ? 0 : group.getNumOfFiles();
            size = (size < 0) ? 0 : size;
            List<AssociationEntity> groupAssociation = AssociationsService.convertFromAssociationEntity(
                    group.getId(), group.getAssociations(), AssociationType.GROUP);
            List<AssociationEntity> groupDocTypeAssoc = AssociationsService.getDocTypeAssociation(groupAssociation);
            int docTypeSize = groupDocTypeAssoc.size();
            int funcRoleSize = AssociationUtils.getFunctionalRoles(groupAssociation).size();
            boolean hasSubGroups = group.getHasSubgroups();
            long nonSystemTagsSize = 0;

            if (groupAssociation != null) {
                nonSystemTagsSize = groupAssociation.stream()
                        .filter(assoc -> (assoc.getFileTagId() != null && !fileTagService.getFromCache(assoc.getFileTagId()).getType().isSystem()))
                        .count();

            }
            long score = 0;
            if (docTypeSize > 0 || funcRoleSize > 0 || nonSystemTagsSize > 0 || hasSubGroups) {
                score = 1000;
            }
            score = score * size + size;

            if (score >= bestScore) {
                bestScore = score;
                bestScoreGroup = group;
            }
        }

        result.setElectedGroup(bestScoreGroup);

        for (SolrFileGroupEntity group : candidateGroups) {
            if (!group.equals(bestScoreGroup)){
                if (group.getHasSubgroups() || group.getType().equals(GroupType.SUB_GROUP.toInt())) {
                    result.getRejectedGroups().add(group);
                } else if (group.getNumOfFiles() != null && group.getNumOfFiles() > rejectSizeThreshold){
                    result.getRejectedGroups().add(group);
                }
                else{
                    result.getAcceptedGroups().add(group);
                }
            } else if (group.getType().equals(GroupType.SUB_GROUP.toInt())) { // return the raw analysis group
                result.setElectedGroup(fileGroupRepository.getOneById(group.getRawAnalysisGroupId()).orElse(null));
            }
        }

        return result;
    }

    @Override
    public boolean requiresFullGroupObjects() {
        return true;
    }

    public void setRejectSizeThreshold(long rejectSizeThreshold) {
        this.rejectSizeThreshold = rejectSizeThreshold;
    }
}
