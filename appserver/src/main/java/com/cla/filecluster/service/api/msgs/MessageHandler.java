package com.cla.filecluster.service.api.msgs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class MessageHandler {

    private final MessageSource messageSource;
    private Locale locale;

    @Autowired
    public MessageHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
        locale = Locale.getDefault();
    }

    public String getMessage(String code) {
        return messageSource.getMessage(code, new String[0], locale);
    }

    public String getMessage(String code, List<Object> args) {
        return messageSource.getMessage(code, args.toArray(), locale);
    }
}
