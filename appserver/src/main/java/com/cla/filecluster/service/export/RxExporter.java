package com.cla.filecluster.service.export;


import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;
import java.util.Map;

public interface RxExporter {

    Flux<DataBuffer> createExportFlux(Map<String, String> params, String progressId);

    boolean doesAccept(ExportType exportType, Map<String, String> params);

    ResultType getExportResultType(String fileNameHint);
}
