package com.cla.filecluster.service.api;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * This util class is used to convert from/to Json serialization
 * Created by uri on 08/07/2015.
 */
public final class ModelDtoConversionUtils {

    public static final String CLEAR_UNPRINTABLE_CHARS_REGEX = "[\\x00-\\x1F\\x7F]";

    private final static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
    }

    private static Logger logger = LoggerFactory.getLogger(ModelDtoConversionUtils.class);

    public static String mapToJson(Map<String, String> map) {
        try {
            if (map == null) {
                return null;
            }
            return mapper.writeValueAsString(map);
        } catch (IOException e) {
            logger.error("Failed to convert map to JSON string.", e);
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Object> jsonToMap(String json) {
        if (StringUtils.isEmpty(json)) {
            return new HashMap<>();
        }
        try {
            //noinspection unchecked
            return mapper.readValue(json, Map.class);
        } catch (IOException e) {
            logger.error("Failed to convert JSON [" + json + "] to map", e);
            throw new RuntimeException(e);
        }

    }

    public static List<Long> convertLongStringArray(String fileIdsString) {
        String[] split = StringUtils.split(fileIdsString, ',');
        List<Long> result = new ArrayList<>();
        for (String s : split) {
            result.add(Long.valueOf(s));
        }

        return result;
    }

    public static ObjectMapper getMapper() {
        return mapper;
    }
}
