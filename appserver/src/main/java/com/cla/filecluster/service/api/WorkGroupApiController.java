package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.security.WorkGroupDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.security.WorkGroupService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * api for work group management (CRUD)
 *
 * Created by: yael
 * Created on: 1/15/2018
 */
@RestController
@RequestMapping("/api/workgroup")
public class WorkGroupApiController {

    private final static Logger logger = LoggerFactory.getLogger(WorkGroupApiController.class);

    @Autowired
    private WorkGroupService workGroupService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/new", method = RequestMethod.PUT)
    public WorkGroupDto createWorkGroup(@RequestBody WorkGroupDto workGroupDto) {
        WorkGroupDto workGroupDtoByName = workGroupService.findWorkGroupByName(workGroupDto.getName());
        if (workGroupDtoByName != null) {
            throw new BadRequestException(messageHandler.getMessage("workflow.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }

        workGroupDto = workGroupService.createWorkGroup(workGroupDto);
        eventAuditService.audit(AuditType.WORK_GROUP, "Create work group", AuditAction.CREATE, WorkGroupDto.class, workGroupDto.getId(), workGroupDto);
        return workGroupDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public WorkGroupDto updateWorkGroup(@PathVariable Long id, @RequestBody WorkGroupDto workGroupDto) {
        getWorkGroup(id);
        workGroupDto = workGroupService.updateWorkGroup(id, workGroupDto);
        eventAuditService.audit(AuditType.WORK_GROUP, "Update work group", AuditAction.UPDATE, WorkGroupDto.class, workGroupDto.getId(), workGroupDto);
        return workGroupDto;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteWorkGroup(@PathVariable Long id) {
        getWorkGroup(id);
        try {
            workGroupService.deleteWorkGroup(id);
            eventAuditService.audit(AuditType.WORK_GROUP, "Delete work group", AuditAction.DELETE, WorkGroupDto.class, id);
        } catch (BadRequestException e) {
            throw e;
        } catch (Exception e) {
            throw new BadRequestException(messageHandler.getMessage("work-group.delete.failure", Lists.newArrayList(e.getMessage())), BadRequestType.OPERATION_FAILED);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public WorkGroupDto getWorkGroup(@PathVariable long id) {
        WorkGroupDto workGroupDto = workGroupService.getWorkGroupById(id);
        if (workGroupDto == null) {
            throw new BadRequestException(messageHandler.getMessage("work-group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return workGroupDto;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<WorkGroupDto> getAllWorkGroups() {
        return workGroupService.getAllWorkGroups();
    }
}
