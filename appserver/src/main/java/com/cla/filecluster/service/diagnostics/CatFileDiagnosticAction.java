package com.cla.filecluster.service.diagnostics;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.filetag.FileTagDto;
import com.cla.common.domain.dto.schedule.ScheduledOperationMetadataDto;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.tag.priority.TagPriority;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.connector.utils.FileNamingUtils;
import com.cla.eventbus.EventBus;
import com.cla.eventbus.Topics;
import com.cla.eventbus.events.Event;
import com.cla.eventbus.events.EventType;
import com.cla.filecluster.domain.converters.SolrEntityBuilder;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.diagnostics.Diagnostic;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.repository.solr.SolrFileCatRepository;
import com.cla.filecluster.repository.solr.query.Criteria;
import com.cla.filecluster.repository.solr.query.Query;
import com.cla.filecluster.repository.solr.query.Range;
import com.cla.filecluster.repository.solr.query.SolrOperator;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocFolderUtils;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.operation.ApplyFolderAssociationOnFiles;
import com.cla.filecluster.service.operation.ApplyGroupAssociationOnFiles;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.util.ActiveAssociationUtils;
import com.cla.filecluster.util.categories.FileCatCompositeId;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class CatFileDiagnosticAction implements EntityDiagnosticAction {

    private static final Logger logger = LoggerFactory.getLogger(CatFileDiagnosticAction.class);
    private static final String FILE_ID_PARAM = "FILE ID";

    @Autowired
    private EventBus eventBus;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Value("${diagnostics.cat-files.page-size:5000}")
    private int pageSize;

    @Value("${diagnostics.cat-files.start-after-min:50}")
    private int startAfterMin;

    @Value("${diagnostics.cat-files.event-limit:100}")
    private int cycleEventThreshold;

    @Value("${diagnostics.cla-file-validation.id.enabled:true}")
    private boolean idValidationEnabled;

    @Value("${diagnostics.cla-file-validation.path.enabled:true}")
    private boolean pathValidationEnabled;

    @Value("${diagnostics.cla-file-validation.hash.enabled:true}")
    private boolean hashValidationEnabled;

    @Value("${diagnostics.cla-file-validation.root-folder.enabled:true}")
    private boolean rfValidationEnabled;

    @Value("${diagnostics.cla-file-validation.parent.enabled:true}")
    private boolean parentValidationEnabled;

    @Value("${diagnostics.cla-file-validation.direct-associations.enabled:true}")
    private boolean directAssociationsValidationEnabled;

    @Value("${diagnostics.cla-file-validation.group.enabled:true}")
    private boolean groupValidationEnabled;

    @Value("${diagnostics.cla-file-validation.content.enabled:true}")
    private boolean contentValidationEnabled;

    @Value("${diagnostics.cla-file-validation.acl.enabled:true}")
    private boolean aclValidationEnabled;

    @Value("${diagnostics.cla-file-validation.associations.enabled:true}")
    private boolean associationsValidationEnabled;

    @Value("${diagnostics.cla-file-validation.percentage:100}")
    private String percentageToTestConfig;

    @Value("${diagnostics.cla-file-validation.pickRandomly:true}")
    private boolean partialTestingPickRandomly;

    private Long lastEndRangeRun = null;

    private AtomicInteger currentEventCounter = new AtomicInteger();

    private List<String> activeFileStates =
            Lists.newArrayList(ClaFileState.SCANNED.name(), ClaFileState.INGESTED.name(), ClaFileState.ANALYSED.name());

    @Override
    public void runDiagnostic(Map<String, String> opData) {

        Long from = opData.containsKey(START_LIMIT) ? Long.parseLong(opData.get(START_LIMIT)) : null;

        Long to = opData.containsKey(END_LIMIT) ? Long.parseLong(opData.get(END_LIMIT)) : null;

        Long rootFolderId = opData.containsKey(ROOT_FOLDER_ID) ? Long.parseLong(opData.get(ROOT_FOLDER_ID)) : null;

        String idsStr = opData.get(SPECIFIC_IDS);

        // Integer may not provide sufficient granularity
        float percentageTest = Float.parseFloat(opData.getOrDefault(PAGE_PERCENTAGE_TEST, percentageToTestConfig));

        if (currentEventCounter.get() >= cycleEventThreshold) {
            logger.debug("threshold passed for validating files - skip");
            return;
        }

        if (!idValidationEnabled && !pathValidationEnabled &&
                !hashValidationEnabled && !rfValidationEnabled &&
                !parentValidationEnabled &&
                !directAssociationsValidationEnabled && !groupValidationEnabled &&
                !contentValidationEnabled && !aclValidationEnabled &&
                !associationsValidationEnabled) {
            logger.debug("all validations are disabled. validating files - skip");
            return;
        }

        Query query = Query.create().setRows(pageSize)
                .addSort(CatFileFieldType.ID, Sort.Direction.ASC)
                .addFilterWithCriteria(Criteria.create(CatFileFieldType.DELETED, SolrOperator.EQ, "false"));

        if (from != null && to != null) {
            logger.debug("validating files update date range from {} to {}", from, to);
            query.addFilterWithCriteria(Criteria.create(CatFileFieldType.UPDATE_DATE, SolrOperator.IN_RANGE, Range.create(from, to)));
            lastEndRangeRun = to;
        } else if (rootFolderId != null) {
            logger.debug("validating files for root folder id {}", rootFolderId);
            query.addFilterWithCriteria(Criteria.create(CatFileFieldType.ROOT_FOLDER_ID, SolrOperator.EQ, rootFolderId));
        } else if (idsStr != null) {
            String[] split = idsStr.split(",");
            List<Long> ids = Arrays.stream(split)
                    .map(Long::valueOf)
                    .collect(Collectors.toList());
            logger.debug("validating files for specific ids size {}", ids.size());
            query.addFilterWithCriteria(Criteria.create(CatFileFieldType.FILE_ID, SolrOperator.IN, ids));
        } // else no limitation run on entire collection

        String cursor = "*";
        boolean lastPage;
        int pageNum = 1;
        do {

            QueryResponse resp = solrFileCatRepository.runQuery(query.setCursorMark(cursor).build());

            List<SolrFileEntity> files = resp.getBeans(SolrFileEntity.class);
            cursor = resp.getNextCursorMark();

            int retrievedItemsCount = files.size();
            if (files != null && retrievedItemsCount > 0) {
                List<? extends SolrEntity> tempListForSampling = Lists.newArrayList(files);
                tempListForSampling = getSolrEntitiesSample(tempListForSampling, percentageTest, partialTestingPickRandomly, pageSize);
                files = (List<SolrFileEntity>)tempListForSampling;

                Set<Long> folderIds = files.stream()
                        .map(SolrFileEntity::getFolderId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<DocFolder> folders = (folderIds == null || folderIds.isEmpty() ?
                        new ArrayList<>() : docFolderService.findByIds(folderIds));
                Map<Long, DocFolder> folderById = folders.stream().collect(Collectors.toMap(DocFolder::getId, r -> r));

                Set<String> groupIds = files.stream()
                        .map(SolrFileEntity::getUserGroupId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<FileGroup> groups = (groupIds == null || groupIds.isEmpty()) ?
                        new ArrayList<>() : groupsService.findByIds(groupIds);
                Map<String, FileGroup> groupById = groups.stream().collect(Collectors.toMap(FileGroup::getId, r -> r));

                Set<String> contentIds = files.stream()
                        .map(SolrFileEntity::getContentId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<SolrContentMetadataEntity> contents = contentMetadataService.getByIds(contentIds);
                Map<String, SolrContentMetadataEntity> contentById =
                        contents.stream().collect(Collectors.toMap(SolrContentMetadataEntity::getId, r -> r));

                Set<Long> rootFolderIds = files.stream()
                        .map(SolrFileEntity::getRootFolderId)
                        .filter(Objects::nonNull).collect(Collectors.toSet());
                List<RootFolder> rootFolders = rootFolderIds.isEmpty() ?
                        new ArrayList<>() : docStoreService.getRootFoldersCached(rootFolderIds);
                Map<Long, RootFolder> rootFolderById = rootFolders.stream().collect(Collectors.toMap(RootFolder::getId, r -> r));

                List<ScheduledOperationMetadataDto> pendingOp = scheduledOperationService.getAllPending();
                List<String> folderAssociationInProgress = new ArrayList<>();
                List<String> groupAssociationInProgress = new ArrayList<>();
                for (ScheduledOperationMetadataDto op : pendingOp) {
                    if (ScheduledOperationType.APPLY_FOLDER_ASSOCIATIONS_TO_SOLR.equals(op.getOperationType())) {
                        String folderIdStr = op.getOpData().get(ApplyFolderAssociationOnFiles.DOC_FOLDER_ID_PARAM);
                        if (!Strings.isNullOrEmpty(folderIdStr)) {
                            folderAssociationInProgress.add(folderIdStr);
                        }
                    } else if (ScheduledOperationType.APPLY_GROUP_ASSOCIATIONS_TO_SOLR.equals(op.getOperationType())) {
                        String groupId = op.getOpData().get(ApplyGroupAssociationOnFiles.GROUP_ID_PARAM);
                        if (!Strings.isNullOrEmpty(groupId)) {
                            groupAssociationInProgress.add(groupId);
                        }
                    }
                }

                for (SolrFileEntity file : files) {
                    validateFile(file, rootFolderById, folderById, groupById, contentById, folderAssociationInProgress, groupAssociationInProgress);
                    if (currentEventCounter.get() >= cycleEventThreshold) {
                        logger.debug("threshold passed for validating files after id {} - skip rest", file.getFileId());
                        break;

                    }
                }
            }
            logger.debug("check diagnostics page number {} of size {} files (out of {})", pageNum, files.size(), retrievedItemsCount);
            pageNum++;
            lastPage = resp.getResults().size() < pageSize;
        } while (!lastPage);
        logger.debug("done validating files");
    }

    public Logger getLogger() {
        return logger;
    }

    @Override
    public DiagnosticType getType() {
        return DiagnosticType.CAT_FILE;
    }

    @Override
    public Long getLastEndRangeRun() {
        return lastEndRangeRun;
    }

    @Override
    public List<Diagnostic> createNewDiagnosticActionParts() {
        currentEventCounter.set(0);
        return Lists.newArrayList(addDiagnosticActionTimeParts());
    }

    @Override
    public int getStartAfterMin() {
        return startAfterMin;
    }

    private void validateFile(SolrFileEntity file,
                              Map<Long, RootFolder> rootFolderById,
                              Map<Long, DocFolder> folderById,
                              Map<String, FileGroup> groupById,
                              Map<String, SolrContentMetadataEntity> contentById,
                              List<String> folderAssociationInProgress,
                              List<String> groupAssociationInProgress) {
        try {
            List<Event> events = new ArrayList<>();

            if (idValidationEnabled) {
                validId(file).ifPresent(events::add);
            }

            if (pathValidationEnabled) {
                validPath(file, folderById).ifPresent(events::add);
            }

            if (rfValidationEnabled) {
                validRootFolder(file, rootFolderById).ifPresent(events::add);
            }

            if (hashValidationEnabled) {
                validHash(file).ifPresent(events::add);
            }

            if (parentValidationEnabled) {
                validParentInfo(file, folderById, folderAssociationInProgress).ifPresent(events::add);
            }

            if (associationsValidationEnabled) {
                validAssociations(file).ifPresent(events::add);
            }

            if (groupValidationEnabled) {
                validGroup(file, groupById, groupAssociationInProgress).ifPresent(events::add);
            }

            if (contentValidationEnabled) {
                validContent(file, contentById).ifPresent(events::add);
            }

            if (aclValidationEnabled) {
                validAcl(file).ifPresent(events::add);
            }

            if (directAssociationsValidationEnabled) {
                validFileAssociations(file).ifPresent(events::add);
            }

            currentEventCounter.addAndGet(events.size());
            events.forEach(event -> eventBus.broadcast(Topics.DIAGNOSTICS, event));
        } catch (Exception e) {
            logger.error("problem validating file {}", file.getId() , e);
        }
    }

    private Optional<Event> validId(SolrFileEntity file) {
        String id = file.getId();
        Long fileId = file.getFileId();
        Long contentId = file.getContentId() == null ? null : Long.parseLong(file.getContentId());
        String shouldBeId = FileCatCompositeId.of(contentId, fileId).toString();

        if (!shouldBeId.equals(id)) {
            Event event = Event.builder().withEventType(EventType.CAT_FILE_ID_MISMATCH)
                    .withEntry(FILE_ID_PARAM, fileId)
                    .withEntry("ID", id)
                    .withEntry("CONTENT_ID", contentId)
                    .build();
            return Optional.of(event);
        }
        return Optional.empty();
    }

    private Optional<Event> validRootFolder(SolrFileEntity file, Map<Long, RootFolder> rootFolderById) {
        if (file.getRootFolderId() == null || rootFolderById.get(file.getRootFolderId()) == null) {
            Event event = Event.builder().withEventType(EventType.CAT_FILE_INVALID_ROOT_FOLDER)
                    .withEntry(FILE_ID_PARAM, file.getFileId())
                    .withEntry("ROOT_FOLDER_ID", file.getRootFolderId())
                    .build();
            return Optional.of(event);
        }
        return Optional.empty();
    }

    private Optional<Event> validHash(SolrFileEntity file) {
        Optional<Event> event = Optional.of(Event.builder()
                .withEventType(EventType.CAT_FILE_INVALID_HASH)
                .withEntry(FILE_ID_PARAM, file.getFileId())
                .withEntry("FULL_NAME", file.getFullName())
                .withEntry("HASH", file.getFileNameHashed())
                .build());

        if (file.getFullName() == null) {
            return event;
        }

        String calcHash = SolrEntityBuilder.getHashedString(file.getFullName());

        if (!calcHash.equals(file.getFileNameHashed())) {
            return event;
        }

        return Optional.empty();
    }

    private Optional<Event> validParentInfo(SolrFileEntity file, Map<Long, DocFolder> folderById, List<String> folderAssociationInProgress) {
        Optional<Event> event = Optional.of(Event.builder().
                withEventType(EventType.CAT_FILE_INVALID_PARENT_INFO)
                .withEntry(FILE_ID_PARAM, file.getFileId())
                .withEntry("PARENT_INFO", file.getParentFolderIds())
                .withEntry("FOLDER_ID", file.getFolderId())
                .build());

        if (file.getParentFolderIds() == null) {
            return event;
        }

        if (file.getFolderId() == null) {
            return event;
        }

        DocFolder folder = folderById.get(file.getFolderId());

        if (folder == null || folder.getParentsInfo() == null) {
            return event;
        }

        if (folder.isDeleted() && !file.isDeleted()) {
            return Optional.of(Event.builder().
                    withEventType(EventType.CAT_FILE_DELETED_FOLDER)
                    .withEntry(FILE_ID_PARAM, file.getFileId())
                    .withEntry("FOLDER_ID", file.getFolderId())
                    .build());
        }

        if (file.getParentFolderIds().size() != folder.getParentsInfo().size()) {
            return event;
        }

        for (String folderIdentifier : file.getParentFolderIds()) {
            if (!folder.getParentsInfo().contains(folderIdentifier)) {
                return event;
            }
        }

        if (!folderAssociationInProgress.isEmpty()) {
            for (String folderIdentifier : file.getParentFolderIds()) {
                String folderId = StringUtils.substringAfter(folderIdentifier, ".");
                if (folderAssociationInProgress.contains(folderId)) {
                    return Optional.empty();
                }
            }
        }

        return validFolderAssociations(file, folder);
    }

    private Optional<Event> validFileAssociations(SolrFileEntity file) {
        if (!activeFileStates.contains(file.getState())) {
            return Optional.empty();
        }

        Event.Builder builder = Event.builder().
                withEventType(EventType.CAT_FILE_ASSOCIATION_MISSING)
                .withEntry(FILE_ID_PARAM, file.getFileId());

        if (file.getAssociations() != null) {
            List<AssociationEntity> associations =
                    AssociationsService.convertFromAssociationEntity(file.getId(), file.getAssociations(), AssociationType.FILE);
            List<AssociableEntityDto> associableEntityDtos = associationsService.getEntitiesForAssociations(associations);
            return testAssociations(file, associations, associableEntityDtos, AssociationType.FILE, builder);
        }

        return Optional.empty();
    }

    private Optional<Event> validFolderAssociations(SolrFileEntity file, DocFolder folder) {
        if (!activeFileStates.contains(file.getState())) {
            return Optional.empty();
        }

        Event.Builder builder = Event.builder().
                withEventType(EventType.CAT_FILE_FOLDER_ASSOCIATION_MISSING)
                .withEntry(FILE_ID_PARAM, file.getFileId())
                .withEntry("FOLDER_ID", file.getFolderId())
                ;

        if (folder.getAssociations() != null) {
            List<AssociationEntity> associations =
                    AssociationsService.convertFromAssociationEntity(folder.getId(), folder.getAssociations(), AssociationType.FOLDER);
            List<AssociableEntityDto> associableEntityDtos = associationsService.getEntitiesForAssociations(associations);
            return testAssociations(file, associations, associableEntityDtos, AssociationType.FOLDER, builder);
        }

        return Optional.empty();
    }

    private Optional<Event> testAssociations(SolrFileEntity file, List<AssociationEntity> associations,
                                             List<AssociableEntityDto> associableEntityDtos,
                                             AssociationType type,  Event.Builder builder) {
        for (AssociationEntity association : associations) {
            TagPriority priority = (type.equals(AssociationType.FILE) ? TagPriority.FILE :
                    type.equals(AssociationType.GROUP) ? TagPriority.FILE_GROUP :
                    TagPriority.folder(association.getOriginDepthFromRoot()));

            if (association.getFileTagId() != null) {
                AssociableEntityDto associableEntity = AssociationUtils.getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,associableEntityDtos);
                if (associableEntity == null) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION_TAG_ENTITY", association.getFileTagId())
                            .build());
                }
                String solrKey = AssociationIdUtils.createPriorityScopedTagIdentifier(
                        (FileTagDto)associableEntity, null, priority);
                if (missingFromList(file.getScopedTags(), solrKey)) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION", solrKey)
                            .build());
                }
            } else if (association.getDocTypeId() != null) {
                AssociableEntityDto associableEntity = AssociationUtils.getEntityFromList(
                        DocTypeDto.class, association.getDocTypeId() ,associableEntityDtos);
                if (associableEntity == null) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION_DOCTYPE_ENTITY", association.getDocTypeId())
                            .build());
                }
                String solrKey = AssociationIdUtils.createPriorityScopedDocTypeIdentifier(
                        ((DocTypeDto)associableEntity).getDocTypeIdentifier(), null, priority);
                if (missingFromList(file.getScopedTags(), solrKey)) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION", solrKey)
                            .build());
                }
            } else if (association.getDepartmentId() != null) {
                AssociableEntityDto associableEntity = AssociationUtils.getEntityFromList(
                        DepartmentDto.class, association.getDepartmentId() ,associableEntityDtos);
                if (associableEntity == null) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION_DEPARTMENT_ENTITY", association.getDepartmentId())
                            .build());
                }
                String solrKey = AssociationIdUtils.createDepartmentIdentifier(((DepartmentDto)associableEntity).getId(), priority);
                if (file.getDepartment() == null || !file.getDepartment().equals(solrKey)) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION", solrKey)
                            .build());
                }
            } else if (association.getFunctionalRoleId() != null) {
                AssociableEntityDto associableEntity = AssociationUtils.getEntityFromList(
                        FunctionalRoleDto.class, association.getFunctionalRoleId() ,associableEntityDtos);
                if (associableEntity == null) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION_DATA_ROLE_ENTITY", association.getFunctionalRoleId())
                            .build());
                }
                String solrKey = AssociationIdUtils.createFunctionalRoleIdentfier((FunctionalRoleDto)associableEntity, priority);
                if (missingFromList(file.getDataRoles(), solrKey)) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION", solrKey)
                            .build());
                }
            } else if (association.getSimpleBizListItemSid() != null) {
                AssociableEntityDto associableEntity = AssociationUtils.getEntityFromList(
                        SimpleBizListItemDto.class, association.getSimpleBizListItemSid() ,associableEntityDtos);
                if (associableEntity == null) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION_BIZ_ITEM_ENTITY", association.getSimpleBizListItemSid())
                            .build());
                }
                String solrKey = AssociationIdUtils.createSimpleBizListItemIdentfier((SimpleBizListItemDto)associableEntity, priority);
                if (missingFromList(file.getBizListItems(), solrKey)) {
                    return Optional.of(builder
                            .withEntry("MISSING_ASSOCIATION", solrKey)
                            .build());
                }
            } else {
                return Optional.of(builder
                        .withEntry("INVALID_ASSOCIATION", association)
                        .build());
            }
        }
        return Optional.empty();
    }

    private boolean missingFromList(List<String> list, String key) {
        if (list == null) {
            return true;
        }
        for (String item : list) {
            if (item.contains(key)) {
                return false;
            }
        }
        return true;
    }

    private String normalizePathForCompare(String path) {
        return DocFolderUtils.removeTrailingSlash(DocFolderUtils.removeStartingSlash(FileNamingUtils.convertPathToUniversalString(path)));
    }

    private Optional<Event> validPath(SolrFileEntity file, Map<Long, DocFolder> folderById) {
        Event.Builder event = Event.builder().
                withEventType(EventType.CAT_FILE_INVALID_PATH)
                .withEntry(FILE_ID_PARAM, file.getFileId())
                .withEntry("FOLDER_ID", file.getFolderId())
                .withEntry("FOLDER_NAME", file.getFolderName())
                .withEntry("FULL_NAME", file.getFullName())
                .withEntry("FULL_NAME_SEARCH", file.getFullNameSearch());

        if (file.getFolderId() == null || file.getFullPath() == null) {
            return Optional.of(event.build());
        }

        DocFolder folder = folderById.get(file.getFolderId());

        if (folder == null || folder.getPath() == null) {
            return Optional.of(event.build());
        }

        if (!normalizePathForCompare(file.getFullPath()).equals(normalizePathForCompare(folder.getPath()))) {
            return Optional.of(event.withEntry("FOLDER_ACTUAL_PATH", folder.getPath()).build());
        }

        if (Strings.isNullOrEmpty(file.getFullNameSearch())) {
            return Optional.of(event.build());
        }

        if (file.getFolderName() == null && !Strings.isNullOrEmpty(folder.getName())) {
            return Optional.of(event.withEntry("FOLDER_ACTUAL_NAME", folder.getName()).build());
        } else if (file.getFolderName() != null && !file.getFolderName().equalsIgnoreCase(folder.getName())) {
            return Optional.of(event.withEntry("FOLDER_ACTUAL_NAME", folder.getName()).build());
        }

        return Optional.empty();
    }

    private Optional<Event> validContent(SolrFileEntity file, Map<String, SolrContentMetadataEntity> contentById) {
        if (!file.isDeleted() && !Strings.isNullOrEmpty(file.getContentId())
                && (file.getState().equals(ClaFileState.INGESTED.name()) ||
                file.getState().equals(ClaFileState.ANALYSED.name()))) {
            SolrContentMetadataEntity content = contentById.get(file.getContentId());
            if (content == null || content.getFileCount() == 0) {
                return Optional.of(Event.builder().
                        withEventType(EventType.CAT_FILE_INVALID_CONTENT)
                        .withEntry(FILE_ID_PARAM, file.getFileId())
                        .withEntry("CONTENT_ID", file.getContentId())
                        .withEntry("CONTENT_COUNT", content == null ? 0 : content.getFileCount())
                        .build());
            }
        }
        return Optional.empty();
    }

    private Optional<Event> validGroup(SolrFileEntity file, Map<String, FileGroup> groupById, List<String> groupAssociationInProgress) {
        if (!file.isDeleted() && !Strings.isNullOrEmpty(file.getUserGroupId())) {
            FileGroup group = groupById.get(file.getUserGroupId());
            if (group == null || group.isDeleted()) {
                return Optional.of(Event.builder().
                        withEventType(EventType.CAT_FILE_INVALID_GROUP)
                        .withEntry(FILE_ID_PARAM, file.getFileId())
                        .withEntry("USER_GROUP_ID", file.getUserGroupId())
                        .build());
            }

            if (!activeFileStates.contains(file.getState())) {
                return Optional.empty();
            }

            Event.Builder builder = Event.builder().
                    withEventType(EventType.CAT_FILE_GROUP_ASSOCIATION_MISSING)
                    .withEntry(FILE_ID_PARAM, file.getFileId())
                    .withEntry("USER_GROUP_ID", file.getUserGroupId());

            if (group.getAssociations() != null && !groupAssociationInProgress.contains(file.getUserGroupId())) {
                List<AssociationEntity> associations =
                        AssociationsService.convertFromAssociationEntity(group.getId(), group.getAssociations(), AssociationType.GROUP);
                List<AssociableEntityDto> associableEntityDtos = associationsService.getEntitiesForAssociations(associations);
                return testAssociations(file, associations, associableEntityDtos, AssociationType.GROUP, builder);
            }
        }
        return Optional.empty();
    }

    private Optional<Event> validAssociations(SolrFileEntity file) {
        if (!activeFileStates.contains(file.getState())) {
            return Optional.empty();
        }

        Optional<Event> event = Optional.of(Event.builder().
                withEventType(EventType.CAT_FILE_INVALID_ASSOCIATION)
                .withEntry(FILE_ID_PARAM, file.getFileId())
                .withEntry("SCOPED_TAGS", file.getScopedTags())
                .withEntry("ACTIVE_ASSOCIATIONS", file.getActiveAssociations())
                .withEntry("DEPARTMENT", file.getDepartment())
                .build());

        if (file.getScopedTags() != null) {
            for (String id : file.getScopedTags()) {
                if (id != null) {
                    String val = ActiveAssociationUtils.getActiveAssociationIdentifier(id);
                    if (file.getActiveAssociations() == null || !file.getActiveAssociations().contains(val)) {
                        return event;
                    }
                    if (id.startsWith("dt.") && !id.startsWith("dt.g.")) {
                        return event;
                    }
                    if (id.startsWith("g.")) {
                        int exists =
                                file.getScopedTags().stream().filter(comp -> comp.contains(id) && comp.length() > id.length()).collect(Collectors.toList()).size();
                        if (exists > 0) {
                            return event;
                        }
                    }
                }
            }
        }

        if (file.getDepartment() != null) {
            String val = ActiveAssociationUtils.getActiveAssociationIdentifier(file.getDepartment());
            if (file.getActiveAssociations() == null || !file.getActiveAssociations().contains(val)) {
                return event;
            }
        }

        if (file.getActiveAssociations() != null) {
            Set<String> assocSet = Sets.newHashSet(file.getActiveAssociations());
            if (assocSet.size() < file.getActiveAssociations().size()) {
                return event;
            }
        }

        return Optional.empty();
    }

    private Optional<Event> validAcl(SolrFileEntity file) {

        if (!file.isDeleted() &&
                (file.getState().equals(ClaFileState.INGESTED.name()) ||
                        file.getState().equals(ClaFileState.ANALYSED.name()))) {
            Optional<Event> event = Optional.of(Event.builder().
                    withEventType(EventType.CAT_FILE_INVALID_ACL)
                    .withEntry(FILE_ID_PARAM, file.getFileId())
                    .withEntry("ACL_READ", file.getAclRead())
                    .build());

            if (file.getAclRead() == null || file.getAclRead().isEmpty()) {
                return event;
            } else if (file.getSearchAclRead() == null || file.getSearchAclRead().isEmpty()) {
                return event;
            }
        }
        return Optional.empty();
    }
}
