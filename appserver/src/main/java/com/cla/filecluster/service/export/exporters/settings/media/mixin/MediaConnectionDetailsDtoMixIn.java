package com.cla.filecluster.service.export.exporters.settings.media.mixin;

import com.cla.connector.domain.dto.media.MediaType;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.settings.media.MediaConnectionHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/11/2019
 */
public class MediaConnectionDetailsDtoMixIn {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(USERNAME)
    private String username;

    @JsonProperty(URL)
    private String url;

    @JsonProperty(MEDIA_TYPE)
    private MediaType mediaType;
}
