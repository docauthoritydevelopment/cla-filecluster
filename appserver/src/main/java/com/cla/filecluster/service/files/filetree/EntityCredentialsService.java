package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.filetree.EntityCredentialsDto;
import com.cla.common.domain.dto.filetree.EntityType;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RsaKey;
import com.cla.common.utils.EncryptionUtils;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.entity.filetree.EntityCredentials;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.dataCenter.CustomerDataCenterRepository;
import com.cla.filecluster.repository.jpa.filetree.EntityCredentialsRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * a service to manage credentials for entities (username & password)
 * currently for root folder only
 * Created by yael on 11/9/2017.
 */
@Service
public class EntityCredentialsService {

    @Autowired
    private EntityCredentialsRepository entityCredentialsRepository;

    @Autowired
    private CustomerDataCenterRepository customerDataCenterRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private DocStoreService docStoreService;

    private LoadingCache<Long, List<EntityCredentialsDto>> credentialsCacheForRootFolder;

    private Logger logger = LoggerFactory.getLogger(EntityCredentialsService.class);

    @PostConstruct
    public void init() {
        credentialsCacheForRootFolder = CacheBuilder.newBuilder()
                .maximumSize(50)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<Long, List<EntityCredentialsDto>>() {
                    public List<EntityCredentialsDto> load(Long rootFolderId) {
                       return getEntityCredentialsForRootFolder(rootFolderId);
                    }
                });
    }

    @Transactional(readOnly = true)
    public List<EntityCredentialsDto> getEntityCredentialsForRootFolderCached(Long rootFolderId) {
        return credentialsCacheForRootFolder.getUnchecked(rootFolderId);
    }

    @Transactional(readOnly = true)
    public List<EntityCredentialsDto> getEntityCredentialsForRootFolder(Long rootFolderId) {
        return convertEntityCredentials(entityCredentialsRepository.findForEntity(rootFolderId, EntityType.ROOT_FOLDER.getName()));
    }

    @Transactional(readOnly = true)
    public EntityCredentialsDto getEntityCredentialsById(Long id) {
        return convertEntityCredentialsToDto(entityCredentialsRepository.getOne(id));
    }

    @Transactional
    public EntityCredentialsDto createEntityCredentials(EntityCredentialsDto entityCredentialsDto, RootFolderDto rootFolder) {
        logger.debug("Creating new entity credentials  ",entityCredentialsDto);

        try {
            String key = rootFolder.getCustomerDataCenterDto().getPublicKey();
            if (key != null) {
                String openPassword = entityCredentialsDto.getPassword();
                entityCredentialsDto.setPassword(EncryptionUtils.encryptUsingPublicKeyStr(key, openPassword));
            }
            entityCredentialsDto.setCreatedTimeStampMs(System.currentTimeMillis());
            entityCredentialsDto.setModifiedTimeStampMs(System.currentTimeMillis());

            EntityCredentials newEntityCredentials = convertToEntityCredentials(entityCredentialsDto);
            EntityCredentials entityCredentials = entityCredentialsRepository.save(newEntityCredentials);
            return convertEntityCredentialsToDto(entityCredentials);
        }
        catch (Exception e) {
            logger.error("Failed to create entity credentials {0}",e);
            throw new BadRequestException(messageHandler.getMessage("entity-credentials.create.failure"), BadRequestType.OPERATION_FAILED);
        }
    }

    @Transactional
    public EntityCredentialsDto updateEntityCredentials(EntityCredentialsDto entityCredentialsDto, RootFolderDto rootFolder) {
        logger.debug("Update entity credentials to {}", entityCredentialsDto);
        EntityCredentials one = entityCredentialsRepository.getOne(entityCredentialsDto.getId());

        try {
            if (entityCredentialsDto.isShouldEncryptPassword()) {
                String key = rootFolder.getCustomerDataCenterDto().getPublicKey();
                if (key != null) {
                    String openPassword = entityCredentialsDto.getPassword();
                    entityCredentialsDto.setPassword(EncryptionUtils.encryptUsingPublicKeyStr(key, openPassword));
                }
            }

            entityCredentialsDto.setModifiedTimeStampMs(System.currentTimeMillis());
            EntityCredentials entityCredentials = convertToEntityCredentials(entityCredentialsDto);

            entityCredentials.setId(one.getId());
            EntityCredentials saved = entityCredentialsRepository.save(entityCredentials);
            return convertEntityCredentialsToDto(saved);

        } catch (Exception e) {
            logger.error("Exception while trying to update entity credentials "+entityCredentialsDto.getId()+" details: "+e.getMessage(),e);
            throw new BadRequestException(messageHandler.getMessage("entity-credentials.update.failure"), BadRequestType.OPERATION_FAILED);
        }
    }

    @Transactional
    public void deleteEntityCredentials(Long entityCredentialsId) {
        logger.debug("Delete entity credentials to {}", entityCredentialsId);

        try {
            entityCredentialsRepository.deleteById(entityCredentialsId);
        } catch (RuntimeException e) {
            logger.error("Exception while trying to delete entity credentials "+entityCredentialsId+" details: "+e.getMessage(),e);
            throw e;
        }
    }

    @Transactional
    public RsaKey generateDataCenterNewKeys(CustomerDataCenterDto customerDataCenterDto, String oldPrivateKey) throws Exception {
        // generate new keys
        RsaKey keys = EncryptionUtils.generateNewKeysComplete();
        String publicKey = keys.getPublicKey();

        // if there was a key set already
        if (customerDataCenterDto.getPublicKey() != null) {

            // get data center root folders
            List<RootFolder> rootFolders = docStoreService.getRootFoldersForDataCenter(customerDataCenterDto.getId());
            if (!rootFolders.isEmpty()) {
                List<Long> rootFoldersIdList = rootFolders.stream().map(RootFolder::getId).collect(Collectors.toList());

                // if we received the old key - re-encrypt passwords, otherwise, delete the entries if exists
                if (oldPrivateKey != null && !oldPrivateKey.isEmpty()) {
                    List<EntityCredentials> credentials = entityCredentialsRepository.findForEntities(rootFoldersIdList, EntityType.ROOT_FOLDER.getName());
                    for (EntityCredentials c : credentials) {
                        String openPassword = EncryptionUtils.decryptUsingPrivateKeyStr(oldPrivateKey, c.getPassword());
                        String encrypted = EncryptionUtils.encryptUsingPublicKeyStr(publicKey, openPassword);
                        c.setPassword(encrypted);
                        c.setModifiedTimeStampMs(System.currentTimeMillis());
                        entityCredentialsRepository.save(c);
                    }
                } else {
                    entityCredentialsRepository.deleteForEntityIds(rootFoldersIdList, EntityType.ROOT_FOLDER.getName());
                }
            }
        }

        // update data center
        CustomerDataCenter customerDataCenter = customerDataCenterRepository.getOne(customerDataCenterDto.getId());
        customerDataCenter.setPublicKey(publicKey);
        customerDataCenterRepository.save(customerDataCenter);

        return keys;
    }

    private EntityCredentials convertToEntityCredentials(EntityCredentialsDto entityCredentialsDto) {
        if (entityCredentialsDto != null) {
            EntityCredentials entityCredentials = new EntityCredentials();
            entityCredentials.setId(entityCredentialsDto.getId());
            entityCredentials.setEntityId(entityCredentialsDto.getEntityId());
            entityCredentials.setEntityType(entityCredentialsDto.getEntityType().getName());
            entityCredentials.setUsername(entityCredentialsDto.getUsername());
            entityCredentials.setPassword(entityCredentialsDto.getPassword());
            entityCredentials.setCreatedTimeStampMs(entityCredentialsDto.getCreatedTimeStampMs());
            entityCredentials.setModifiedTimeStampMs(entityCredentialsDto.getModifiedTimeStampMs());
            return entityCredentials;
        }
        return null;
    }

    public static List<EntityCredentialsDto> convertEntityCredentials(List<EntityCredentials> entityCredentials) {
        List<EntityCredentialsDto> dtos = new ArrayList<>();
        for (EntityCredentials item : entityCredentials) {
            dtos.add(convertEntityCredentialsToDto(item));
        }
        return dtos;
    }

    public static EntityCredentialsDto convertEntityCredentialsToDto(EntityCredentials entityCredentials) {
        if (entityCredentials != null) {
            EntityCredentialsDto dto = new EntityCredentialsDto();
            dto.setId(entityCredentials.getId());
            dto.setEntityId(entityCredentials.getEntityId());
            dto.setEntityType(EntityType.valueOf(entityCredentials.getEntityType()));
            dto.setUsername(entityCredentials.getUsername());
            dto.setPassword(entityCredentials.getPassword());
            dto.setCreatedTimeStampMs(entityCredentials.getCreatedTimeStampMs());
            dto.setModifiedTimeStampMs(entityCredentials.getModifiedTimeStampMs());
            return dto;
        }
        return null;
    }
}
