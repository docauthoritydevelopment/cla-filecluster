package com.cla.filecluster.service.export.exporters.settings.department;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.department.DepartmentSettingsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/1/2019
 */
@Slf4j
@Component
public class DepartmentSettingsExporter extends PageCsvExcelExporter<DepartmentDto> {

    @Autowired
    private DepartmentService departmentService;

    private final static String[] Header = {FULL_NAME, DESCRIPTION};

    @PostConstruct
    public void init() {
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DEPARTMENT_SETTINGS);
    }

    @Override
    protected String[] getHeader() {
        return Header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(DepartmentDto.class, DepartmentDtoMixin.class);
    }

    @Override
    protected Page<DepartmentDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return departmentService.getDepartments(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(DepartmentDto base, Map<String, String> requestParams) {
        return null;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }
}
