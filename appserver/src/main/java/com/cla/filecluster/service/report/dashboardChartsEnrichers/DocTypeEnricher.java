package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.doctype.DocTypeService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.stream.Collectors;


@Component
public class DocTypeEnricher implements DefaultTypeDataEnricher {

    @Autowired
    private DocTypeService docTypeService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String convertSolrValueToId(String value) {
        return String.valueOf(AssociationIdUtils.extractDocTypeIdFromSolrIdentifier(value));
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
                docTypeService.getFromCache(Long.parseLong(convertSolrValueToId(category))).getName()).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.ACTIVE_ASSOCIATIONS, DocTypeDto.ID_PREFIX);
    }

}
