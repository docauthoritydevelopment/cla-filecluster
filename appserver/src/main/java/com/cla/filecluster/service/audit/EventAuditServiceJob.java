package com.cla.filecluster.service.audit;

import com.cla.filecluster.repository.jpa.entitylist.DaAuditEventRepository;
import com.cla.filecluster.service.util.PartitionManagementJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Service to run scheduled job to create table partitions
 * separated from EventAuditService so we can avoid running this job in tests
 * Created by yael on 11/16/2017.
 */
@Service
@Profile({ "prod", "win", "dev", "default" })
public class EventAuditServiceJob extends PartitionManagementJob {

    private Logger logger = LoggerFactory.getLogger(EventAuditServiceJob.class);

    @Value("${audit-manager.mng-partition-days-add-partition:7}")
    private int daysToAddPartition;

    @Value("${audit-manager.mng-partition-days-delete-partition:365}")
    private int daysToDeletePartition;

    @Autowired
    private DaAuditEventRepository auditEventRepository;

    @Scheduled(initialDelayString = "${audit-manager.mng-partition-initial-delay-millis:30000}",
            fixedRateString = "${audit-manager.mng-partition-frequency-millis:18000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void managePartitions() {
        managePartitions(daysToAddPartition, daysToDeletePartition, auditEventRepository);
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }
}
