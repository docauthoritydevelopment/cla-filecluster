package com.cla.filecluster.service.chain;

/**
 * Created by: yael
 * Created on: 8/4/2019
 */
public interface ChainContext {
    boolean errorOccurred();
    void setErrorOccurred(boolean errorOccurred);
}
