package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.domain.entity.searchpatterns.Regulation;
import com.cla.filecluster.domain.entity.searchpatterns.TextSearchPattern;
import com.cla.filecluster.repository.jpa.searchpattern.RegulationRepository;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class RegulationEnricher implements CategoryTypeDataEnricher {

    private static final String REGULATION_DUMMY_PREFIX = "regulation";


    @Autowired
    private RegulationRepository regulationRepository;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.MATCHED_PATTERNS_SINGLE, REGULATION_DUMMY_PREFIX);
    }

    @Override
    public Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params) {
        HashMap<String, String> ans = new HashMap<>();
        List<Regulation> regulations = regulationRepository.findAll();
        regulations.forEach((Regulation regulation) -> {
            if (regulation.isActive()) {
                if (regulation.getPatterns() != null) {
                    List<String> patternIds = new ArrayList<>();
                    regulation.getPatterns().forEach((TextSearchPattern pattern) -> {
                        if (pattern.isActive() && pattern.getSubCategories().iterator().next().isActive()) {
                            patternIds.add(pattern.getId().toString());
                        }
                    });
                    if (patternIds.size() > 0) {
                        ans.put(String.valueOf(regulation.getId()), DashboardChartsUtil.getQueryTypeValueStringAccordingToValuesList(patternIds));
                    }
                }
            }
        });
        return ans;
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
        List<Regulation> regulations = regulationRepository.findAll();
        Map<Integer, Regulation> fTypeCategoriesMap = regulations.stream().collect(Collectors.toMap(Regulation::getId, ftc -> ftc));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return fTypeCategoriesMap.get(Integer.parseInt(category)).getName();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getFilterFieldName() {
        return FileDtoFieldConverter.DtoFields.regulationId.getAlternativeName();
    }

}
