package com.cla.filecluster.service.export.exporters.file.associations;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.doctype.DocTypeAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.cla.filecluster.service.export.exporters.file.associations.DocTypeHeaderFields.*;

@Slf4j
@Component
public class DocTypeExporter extends PageCsvExcelExporter<AggregationCountItemDTO<DocTypeDto>> {

    private final static String[] HEADER = {NAME, NUM_OF_DIRECT_FILERED, NUM_OF_ALL_FILTERED, DESCRIPTION};

    @Autowired
    private DocTypeAppService docTypeAppService;

    private LoadingCache<Long, String> docTypeById;

    @PostConstruct
    private void init() {
        docTypeById = CacheBuilder.newBuilder().maximumSize(1000).build(CacheLoader.from(this::getDocTypeById));
    }

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, DocTypeAggCountDtoMixin.class);
        mapper.addMixIn(DocTypeDto.class, DocTypeDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<DocTypeDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<DocTypeDto>> res = docTypeAppService.findDocTypesFileCount(dataSourceRequest, true, null);
        return new PageImpl<>(res.getContent());
    }

	@Override
	protected Map<String, Object> addExtraFields(AggregationCountItemDTO<DocTypeDto> base, Map<String, String> requestParams) {
		Map<String, Object> result = new HashMap<>();
		result.put(NAME, resolveNamePath(base));
        result.put(NUM_OF_ALL_FILTERED, base.getSubtreeCount());
		return result;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    private String resolveNamePath(AggregationCountItemDTO<DocTypeDto> base) {
        docTypeById.put(base.getItem().getId(), base.getItem().getName());
        StringBuilder resultSb = new StringBuilder();
        if (ArrayUtils.isNotEmpty(base.getItem().getParents())) {
            Arrays.stream(base.getItem().getParents()).forEach(parent -> resultSb.append(docTypeById.getUnchecked(parent)).append(DocTypeDto.SEPARATOR));
            resultSb.deleteCharAt(resultSb.length() - 1);
        } else {
            resultSb.append(base.getItem().getName());
        }
        return resultSb.toString();
    }

    private String getDocTypeById(Long id) {
        return Optional.ofNullable(docTypeAppService.getDocTypeById(id))
                .map(DocTypeDto::getName)
                .orElseThrow(() -> new RuntimeException(String.format("Error, cannot resolve docType for id: %d", id)));
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.DOC_TYPE);
    }
}
