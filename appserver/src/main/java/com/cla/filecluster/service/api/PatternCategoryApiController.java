package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.filesearchpatterns.PatternCategoryDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.extractor.pattern.PatternCategoryAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by ophir on 10/04/2019.
 */
@RestController
@RequestMapping("/api/patterncategory")
public class PatternCategoryApiController {
    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private PatternCategoryAppService patternCategoryAppService;
    Logger logger = LoggerFactory.getLogger(PatternCategoryApiController.class);

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<PatternCategoryDto> listPatternsCategories(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return patternCategoryAppService.listPatternsCategories(dataSourceRequest);
    }

    @RequestMapping(value = "/full", method = RequestMethod.GET)
    public Page<PatternCategoryDto> listFullPatternsCategories(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return patternCategoryAppService.listPatternsCategories(dataSourceRequest,true);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public PatternCategoryDto addPatternCategory(@RequestBody PatternCategoryDto patternCategoryDto) {
        PatternCategoryDto category = patternCategoryAppService.createPatternCategory(patternCategoryDto);
        eventAuditService.audit(AuditType.PATTERN_CATEGORIES, "Create pattern category", AuditAction.CREATE, PatternCategoryDto.class, category.getId(), category);
        return category;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public PatternCategoryDto updatePatternCategory(@RequestBody PatternCategoryDto patternCategoryDto) {
        PatternCategoryDto category = patternCategoryAppService.updatePatternCategory(patternCategoryDto);
        eventAuditService.audit(AuditType.PATTERN_CATEGORIES, "Update pattern category", AuditAction.UPDATE, PatternCategoryDto.class, category.getId(), category);
        return category;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PatternCategoryDto getPatternCategory(@PathVariable Integer id) {
        PatternCategoryDto patternCategoryDto = patternCategoryAppService.getPatternCategoryById(id);
        if (patternCategoryDto == null) {
            throw new BadRequestException(messageHandler.getMessage("pattern-categories.not-found"), BadRequestType.ITEM_NOT_FOUND);
        }
        return patternCategoryDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePatternCategory(@PathVariable int id) {
        PatternCategoryDto patternCategoryDto = getPatternCategory(id);
        patternCategoryAppService.deletePatternCategory(id);
        eventAuditService.audit(AuditType.PATTERN_CATEGORIES,"Delete pattern category ", AuditAction.DELETE, PatternCategoryDto.class,patternCategoryDto.getId(),patternCategoryDto);
    }


    @RequestMapping(value = "/{id}/enable", method = RequestMethod.POST)
    public PatternCategoryDto enablePatternCategory(@PathVariable int id) {
        PatternCategoryDto ansPatternCategoryDto = patternCategoryAppService.updatePatternCategoryState(id, true);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Update pattern category", AuditAction.UPDATE, PatternCategoryDto.class,ansPatternCategoryDto.getId(),ansPatternCategoryDto);
        return ansPatternCategoryDto;
    }

    @RequestMapping(value = "/{id}/disable", method = RequestMethod.POST)
    public PatternCategoryDto disablePatternCategory(@PathVariable int id) {
        PatternCategoryDto ansPatternCategoryDto = patternCategoryAppService.updatePatternCategoryState(id, false);
        eventAuditService.audit(AuditType.SEARCH_PATTERN,"Update pattern category", AuditAction.UPDATE, PatternCategoryDto.class,ansPatternCategoryDto.getId(),ansPatternCategoryDto);
        return ansPatternCategoryDto;
    }


}
