package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component
public class RootFolderEnricher implements DefaultTypeDataEnricher {

    @Autowired
    private DocStoreService docStoreService;


    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        List<Long> ids = dashboardChartDataDto.getCategories().stream().map(Long::parseLong).collect(Collectors.toList());
        List<RootFolder> rootFolders = docStoreService.findByIds(ids);
        Map<Long, RootFolder> rootFoldersMap = rootFolders.stream().collect(
                Collectors.toMap(RootFolder::getId, rootFolder -> rootFolder));
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) -> {
            RootFolder rFolder = rootFoldersMap.get(Long.parseLong(category));
            String folderName = rFolder.getNickName();
            if (Strings.isNullOrEmpty(folderName)) {
                folderName = DocStoreService.getRootFolderIdentifier(rFolder);
            }
            return folderName;
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return CatFileFieldType.ROOT_FOLDER_ID.getSolrName();
    }

}
