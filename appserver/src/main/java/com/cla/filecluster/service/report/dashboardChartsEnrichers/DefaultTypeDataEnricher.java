package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartQueryType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.report.DashboardDataEnricher;
import org.springframework.stereotype.Component;

@Component
public interface DefaultTypeDataEnricher extends DashboardDataEnricher {

    default void enrichJSONFacetQuery(SolrFacetQueryJson solrFacetQueryJson, FilterDescriptor filter) {
    }

    default ChartQueryType getQueryType() {
        return ChartQueryType.DEFAULT;
    }

    default void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
    };
}
