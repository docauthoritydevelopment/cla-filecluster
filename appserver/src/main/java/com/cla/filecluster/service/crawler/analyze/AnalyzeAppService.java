package com.cla.filecluster.service.crawler.analyze;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SkipAnalysisReasonType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ClaFileState;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.pv.SimilaritySimulationContext;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileEntity;
import com.cla.filecluster.domain.entity.pv.ContentMetadata;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.kpi.KpiRecording;
import com.cla.filecluster.kpi.PerformanceKpiRecorder;
import com.cla.filecluster.batch.writer.analyze.AnalyzeBatchResultsProcessor;
import com.cla.filecluster.service.categories.FileCatService;
import com.cla.filecluster.service.crawler.analyze.analyzer.ContentAnalyzer;
import com.cla.filecluster.service.crawler.analyze.analyzer.ExcelAnalyzer;
import com.cla.filecluster.service.crawler.analyze.analyzer.WordAnalyzer;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.syscomponent.AppServerStateService;
import com.google.common.collect.Maps;
import com.google.common.hash.Hashing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by uri on 08-May-17.
 */
@Service
public class AnalyzeAppService {

    private static final Logger logger = LoggerFactory.getLogger(AnalyzeAppService.class);

    @Autowired
    private FileCatService fileCatService;

    @Autowired
    private WordAnalyzer wordAnalyzer;

    @Autowired
    private ExcelAnalyzer excelAnalyzer;

    @Autowired
    private JobManagerService jobManager;

    @Autowired
    private SolrTaskService solrTaskService;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private AppServerStateService appServerStateService;

    @Autowired
    private PerformanceKpiRecorder performanceKpiRecorder;

    @Autowired
    private AnalyzeBatchResultsProcessor analyzeBatchResultsProcessor;

    @Value("${scan.file.commit-solr-during:false}")
    private boolean commitFileSolrDuringScan;

    @Value("${fileGrouping.mark-matched-as-analyzed.enabled:true}")
    private boolean markMatchedAsAnalyzed;

    @Value("${fileGrouping.mark-matched-as-done.enabled:false}")
    private boolean markMatchedAsDone;

    @Value("${fileGrouping.mark-matched-as-analyzed.min-threshold:1000}")
    private int markMatchedAsAnalyzedMinThreshold;

    @Value("${fileGrouping.mark-matched-as-analyzed.exclude-percentage:10}")
    private int markMatchedAsAnalyzedExcludePercentage;

    @Value("${fileGrouping.mark-matched-as-analyzed.debug-log-sampling:10}")
    private int markMatchedAsAnalyzedDebugSampling;

    @Value("${fileGrouping.selective-analysis.enabled:false}")
    private boolean selectiveAnalysisEnabled;
    @Value("${fileGrouping.selective-analysis.exclude-percentage.word:100}")
    private int selectiveAnalysisExcludePercentageWord;
    @Value("${fileGrouping.selective-analysis.exclude-percentage.excel:100}")
    private int selectiveAnalysisExcludePercentageExcel;
    @Value("${fileGrouping.selective-analysis.exclude-percentage.pdf:100}")

    private int selectiveAnalysisExcludePercentagePdf;
    private int markMatchedAsAnalyzedExcludePercentageThreshold16bits;
    private int selectiveAnalysisExcludePercentageThreshold16bits_Word;
    private int selectiveAnalysisExcludePercentageThreshold16bits_Excel;
    private int selectiveAnalysisExcludePercentageThreshold16bits_Pdf;

    private int selectiveAnalysisSkippedTaskCounter = 0;

    private int portion;

    private int markMatchedAsAnalyzedTaskCounter = 0;

    @PostConstruct
    private void init() {
        markMatchedAsAnalyzedExcludePercentageThreshold16bits = percentageTo16BitsThreshold(markMatchedAsAnalyzedExcludePercentage);
        selectiveAnalysisExcludePercentageThreshold16bits_Word = percentageTo16BitsThreshold(selectiveAnalysisExcludePercentageWord);
        selectiveAnalysisExcludePercentageThreshold16bits_Excel = percentageTo16BitsThreshold(selectiveAnalysisExcludePercentageExcel);
        selectiveAnalysisExcludePercentageThreshold16bits_Pdf = percentageTo16BitsThreshold(selectiveAnalysisExcludePercentagePdf);
    }

    @Transactional
    public void analyzeSingleFile(long fileId) {
        fileCatService.updateFieldByQueryMultiFields(new String[]{CatFileFieldType.PROCESSING_STATE.getSolrName(), CatFileFieldType.ANALYSIS_HINT.getSolrName()},
                new String[]{ClaFileState.STARTED_ANALYSING.name(), AnalyzeHint.NORMAL.name()},
                SolrFieldOp.SET.getOpName(),
                CatFileFieldType.FILE_ID.getSolrName() + ":" + fileId, null, SolrCommitType.HARD);

        SolrFileEntity claFile = fileCatService.getFileEntity(fileId);
        Long contentId = FileCatService.getContentId(claFile);

        ContentMetadata contentMetadata = contentId == null ? null : contentMetadataService.getById(contentId);

        if (contentMetadata == null) {
            throw new RuntimeException("file " + fileId + " does not have a content, operation cannot proceed");
        }

        ContentMetadataDto contentMetadataDto = ContentMetadataService.convertContentMetaData(contentMetadata);
        contentMetadataDto.setHint(claFile.getFullName());
        analyzeContent(contentMetadataDto, null, Maps.newHashMap());
        logger.debug("Finished analyzing file {}", claFile);
    }

    private void logAndIncMatchedAsAnalyzedTask(Long contentId, Long taskId) {
        markMatchedAsAnalyzedTaskCounter++;
        if (logger.isTraceEnabled()) {
            logger.trace("Ignoring task {} to analyze content {}, already marked as {} in a prior task",
                    contentId, taskId, ClaFileState.ANALYSED);
        } else if (logger.isDebugEnabled() && (markMatchedAsAnalyzedTaskCounter % markMatchedAsAnalyzedDebugSampling) == 0) {
            logger.debug("MarkMatchesAsAnalyzed: {} marked tasks ignored (counter={}). Current item: contentId {}, taskId {}",
                    markMatchedAsAnalyzedDebugSampling, markMatchedAsAnalyzedTaskCounter, contentId, taskId);
        }
    }

    @AutoAuthenticate
    public void analyzeContent(ContentMetadataDto contentMetadataDto, SimpleTask task, Map<Long, ContentMetadataDto> filesContentByIds) {
        try {
            boolean shouldAnalyze = false;
            if (AnalyzeHint.EXCLUDED.equals(contentMetadataDto.getAnalyzeHint())) {
                logger.info("content id {} is marked as excluded from analysis. Skipping", contentMetadataDto.getContentId());
            } else if (contentMetadataDto.getFileCount() == 0) {
                logger.info("Content {} has no files (probably merged). Skipping analysis", contentMetadataDto);
            } else if (contentMetadataDto.getFsFileSize().equals(0L)) {
                logger.warn("Unable to analyze content with size 0. Skipping analysis of {}", contentMetadataDto);
            } else if (task != null && markMatchedAsAnalyzed && contentMetadataDto.getState() == ClaFileState.ANALYSED) {
                if (markMatchedAsDone) {
                    if (analyzeBatchResultsProcessor.isDoneTask(task.getId())) {
                        logAndIncMatchedAsAnalyzedTask(contentMetadataDto.getContentId(), task.getId());
                        return;
                    } else {
                        SolrContentMetadataEntity contEntity = contentMetadataService.findById(contentMetadataDto.getContentId());
                        if (contEntity != null && (contEntity.getCurrentRunId() == null || !contEntity.getCurrentRunId().equals(task.getRunContext()))) {
                            logAndIncMatchedAsAnalyzedTask(contentMetadataDto.getContentId(), task.getId());
                            return;
                        }
                    }
                }
                logAndIncMatchedAsAnalyzedTask(contentMetadataDto.getContentId(), task.getId());
            } else if (task != null && selectiveAnalysisEnabled &&
                    skipAnalysisSelectiveMode(contentMetadataDto.getContentId(), contentMetadataDto.getType())) {
                selectiveAnalysisSkippedTaskCounter++;
                logger.info("SelectiveAnalysis: skip content id {} ({}). count={}", contentMetadataDto.getContentId(), contentMetadataDto.getType().name(), selectiveAnalysisSkippedTaskCounter);
                analyzeBatchResultsProcessor.collectContentFilesToAnalyze(task.getId(), contentMetadataDto.getContentId(), SkipAnalysisReasonType.SKIPPED_SELECTIVELY);
            } else {
                shouldAnalyze = true;
            }

            // Meaning content were not marked as analyzed and not chosen by selective analysis
            if (shouldAnalyze) {
                logger.trace("analyze content {} ", contentMetadataDto);
                final ContentAnalyzer consumer = getConsumer(contentMetadataDto);
                if (consumer != null) {
                    Long taskId = task == null ? null : task.getId();
                    // if appropriate consumer was found
                    KpiRecording kpiRecording = performanceKpiRecorder.startRecording("AnalyzeAppService.analyzeContent", "consumer.analyzeContent", 106);
                    Collection<Long> matchedContentIds = consumer.analyzeContent(taskId, contentMetadataDto);
                    logger.debug("Matches for content {}: {} matches", contentMetadataDto.getContentId(), matchedContentIds == null ? 0 : matchedContentIds.size());
                    kpiRecording.end();
                    createFinalizeJobs(task, contentMetadataDto.getContentId(), matchedContentIds); // does nothing, is empty for now

                    // Case markMatchedAsAnalyzed, keep only markMatchedAsAnalyzedExcludePercentage as not analyzed
                    // All other matchedContentIds mark as analysed
                    if (task != null) {
                        if (markMatchedAsAnalyzed && matchedContentIds != null && matchedContentIds.size() >= markMatchedAsAnalyzedMinThreshold) {
                            long start = System.currentTimeMillis();
                            matchedContentIds = filterMatchedContentToMarkAsAnalysed(contentMetadataDto.getContentId(), matchedContentIds);
                            logger.debug("Time to filter mark as analysed out of {} content ids: took {} ms",
                                    matchedContentIds.size(), (System.currentTimeMillis() - start));
                            start = System.currentTimeMillis();
                            analyzeBatchResultsProcessor.collectContentFilesToAnalyze(taskId, contentMetadataDto.getContentId(), SkipAnalysisReasonType.NOT_SKIPPED);
                            matchedContentIds.forEach(mcId -> analyzeBatchResultsProcessor.collectContentFilesToAnalyze(taskId, mcId, SkipAnalysisReasonType.GROUPED_BY_ANOTHER));
                            matchedContentIds.stream()
                                    .filter(filesContentByIds::containsKey)
                                    .map(filesContentByIds::get)
                                    .forEach(mco -> mco.setState(ClaFileState.ANALYSED));
                            if (markMatchedAsDone && matchedContentIds.size() > 0) {
                                Integer cnged = solrTaskService.endTaskForContents(matchedContentIds);
                                jobManager.incJobCounters(task.getJobId(), (cnged == null ? 0 : cnged), 0);
                                logger.trace("increment job {} matchedContentIds {} changed {}", task.getJobId(), matchedContentIds.size(), cnged);
                            }
                            logger.debug("Time to set {} matched content ids as analysed: took {} ms",
                                    matchedContentIds.size(), (System.currentTimeMillis() - start));
                        } else {
                            analyzeBatchResultsProcessor.collectContentFilesToAnalyze(taskId, contentMetadataDto.getContentId(), SkipAnalysisReasonType.NOT_SKIPPED);
                        }
                    }
                }
            }
            if (task != null) {
                analyzeBatchResultsProcessor.collectDoneTasks(task.getId(), task.getJobId());
                appServerStateService.incAnalyzeTaskFinished();
            }

        } catch (Exception e) {
            logger.error("Failed to analyze content id:{}, file-count:{}. Skip and continue to next content...",
                    contentMetadataDto.getContentId(), contentMetadataDto.getFileCount(), e);
            if (task != null) {
                analyzeBatchResultsProcessor.collectFailedTasks(task.getId(), task.getJobId());
                appServerStateService.incAnalyzeTaskRequestFailure();
            }
        }
    }

    private Collection<Long> filterMatchedContentToMarkAsAnalysed(long contentId, Collection<Long> matchedContentIds) {
        if (matchedContentIds == null || matchedContentIds.isEmpty()) {
            return matchedContentIds;
        }

        List<Long> selectedIdsToMarkAsAnalyzed = matchedContentIds.stream()
                .filter(cId -> skipAnalysisIfMatched(cId))
                .collect(Collectors.toList());
        int portion = selectedIdsToMarkAsAnalyzed.size();
        logger.debug("For content {}, {}% members which is {} out of {} will be marked as analysed",
                contentId,
                (int) (((double) selectedIdsToMarkAsAnalyzed.size() / (double) matchedContentIds.size()) * 100),
                portion,
                matchedContentIds.size());
        return selectedIdsToMarkAsAnalyzed;
    }

    public static int percentageTo16BitsThreshold(int percentage) {
        return percentage * 0xffff / 100;
    }

    public static boolean isAboveHashedPercentage(Long cId, int hashed16bitsThreshold) {
        int last16Bits = longToHashed16Bits(cId);
        return last16Bits > hashed16bitsThreshold;
    }

    public static boolean isAbovePercentage(Long cId, int percentage) {
        return isAboveHashedPercentage(cId, percentageTo16BitsThreshold(percentage));
    }

    private static int longToHashed16Bits(Long cId) {
        int hashed = Hashing.sha256().hashLong(cId).asInt();
        return hashed & 0xffff;
    }

    private boolean skipAnalysisIfMatched(Long cId) {
        return isAboveHashedPercentage(cId, markMatchedAsAnalyzedExcludePercentageThreshold16bits);
    }

    /**
     *  Decide if file should skip analysis or not,
     *  Decision is based on id hash if the value is above the defined thershold.
     *  In case it is above the it should be skipped, other it should be analysed
     * @param cId - the id of the content which will be hashed
     * @param fileType - the type of the file to take the preconfigured threshold to check against
     * @return true if file should be skipped meaning that the file is above the threshold
     */
    private boolean skipAnalysisSelectiveMode(Long cId, FileType fileType) {
        int last16Bits = longToHashed16Bits(cId);
        int threshold = 0;
        switch (fileType) {
            case WORD:
                threshold = selectiveAnalysisExcludePercentageThreshold16bits_Word;
                break;
            case EXCEL:
                threshold = selectiveAnalysisExcludePercentageThreshold16bits_Excel;
                break;
            case PDF:
                threshold = selectiveAnalysisExcludePercentageThreshold16bits_Pdf;
                break;
        }
        return last16Bits > threshold;
    }





    @Transactional
    public Collection<Long> simulateClaFileContentAnalysis(long contentId, SimilaritySimulationContext simulationContext) {
        ContentMetadata contentMetadata = contentMetadataService.getById(contentId);

        if (contentMetadata == null) {
            throw new RuntimeException("content " + contentId + " does not exists, operation cannot proceed");
        }

        ContentMetadataDto contentMetadataDto = ContentMetadataService.convertContentMetaData(contentMetadata);
        Collection<Long> result;
        switch (contentMetadataDto.getType()) {
            case WORD:
                result = wordAnalyzer.analyzeContent(null,contentMetadataDto, simulationContext);
                break;
            case PDF:
                result = wordAnalyzer.analyzeContent(null,contentMetadataDto, simulationContext);
                break;
            case EXCEL:
                // TODO: add result string to Excel analysis
                result = excelAnalyzer.analyzeContent(null,contentMetadataDto, simulationContext);
                break;
            default:
                logger.info("Unable to simulate analysis for type " + contentMetadataDto.getType());
                result = new ArrayList<>();
                break;
        }
        logger.info("Got {} results from simulation", result.size());
        return simulationContext.getSimulationMatchesQueue();
    }

    private ContentAnalyzer getConsumer(ContentMetadataDto contentMetadataDto) {
        switch (contentMetadataDto.getType()) {
            case WORD:
            case PDF:
                return wordAnalyzer;
            case EXCEL:
                return excelAnalyzer;
            case SCANNED_PDF:
                return null;
            default:
                throw new RuntimeException("Unsupported content type " + contentMetadataDto.getType() + " for analysis");
        }
    }

    private void createFinalizeJobs(SimpleTask task, long contentId, Collection<Long> matchedContentIds) {
        //TODO 2.0
    }
}
