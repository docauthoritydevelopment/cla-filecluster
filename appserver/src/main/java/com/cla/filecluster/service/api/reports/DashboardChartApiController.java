package com.cla.filecluster.service.api.reports;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.report.DashboardChartDtoBuilder;
import com.cla.filecluster.service.report.DashboardChartFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping("/api/dashboardchart")
public class DashboardChartApiController {

    @Autowired
    private DashboardChartFactory dashboardChartFactory;

    @Autowired
    private EventAuditService eventAuditService;

    @RequestMapping("/{chartType}/data")
    public DashboardChartDataDto getDashboardChartData(@PathVariable ChartType chartType, @RequestParam final Map<String,String> params) {
        DashboardChartDtoBuilder chartBuilder = dashboardChartFactory.getBuilder(chartType);
        eventAuditService.audit(AuditType.REPORT_WIDGET, chartBuilder.getAuditMsg(params), AuditAction.VIEW, DashboardChartDataDto.class, chartType, params);
        return chartBuilder.buildChartDataDto(params);
    }

}
