package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.connector.domain.dto.file.label.LabelDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.label.LabelingAppService;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.label.LabelingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by: yael
 * Created on: 7/8/2019
 */
@RestController
@RequestMapping("/api/labeling")
public class LabelingApiController {

    @Autowired
    private LabelingAppService labelingAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private LabelingService labelingService;

    @RequestMapping(value="/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<LabelDto>> getLabelFileCount(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover label export", AuditAction.VIEW, String.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover label", AuditAction.VIEW, String.class, null, dataSourceRequest);
        }
        return labelingAppService.getLabelFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/metadata/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountItemDTO<String>> getGeneralMetadataFileCount(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover GeneralMetadata export", AuditAction.VIEW, String.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover GeneralMetadata", AuditAction.VIEW, String.class, null, dataSourceRequest);
        }
        return labelingAppService.getGeneralMetadataFileCounts(dataSourceRequest);
    }

    @RequestMapping(value="/associate/file", method = RequestMethod.PUT)
    public void setLabelOnFile(@RequestParam long fileId, @RequestParam long labelId) {
        labelingAppService.setLabelOnFile(fileId, labelId);
        eventAuditService.audit(AuditType.LABEL, "Set label on file", AuditAction.ASSIGN, Long.class, labelId, Long.class, fileId);
    }

    @RequestMapping(value="/all", method = RequestMethod.GET)
    public Page<LabelDto> listAllDaLabels(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return labelingService.listAllDaLabels(dataSourceRequest.createPageRequest());
    }

    @RequestMapping(value="/set", method = RequestMethod.PUT)
    public void setLabelOnFiles(@RequestParam final Map<String, String> params) {
        Long labelId = labelingAppService.setLabelOnFiles(params);
        eventAuditService.audit(AuditType.LABEL, "Set label on files by filter", AuditAction.ASSIGN, Long.class, labelId, Long.class);
    }

    @RequestMapping(value="/tag/set", method = RequestMethod.PUT)
    public void setLabelOnFilesByTags(@RequestParam final Map<String, String> params) {
        labelingAppService.setLabelOnFilesByTags(params);
        eventAuditService.audit(AuditType.LABEL, "Set label on files by filter and tags", AuditAction.ASSIGN);
    }
}
