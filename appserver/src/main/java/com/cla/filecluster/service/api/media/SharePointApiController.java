package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MicrosoftConnectionDetailsDtoBase;
import com.cla.connector.domain.dto.media.SharePointConnectionDetailsDto;
import com.cla.connector.domain.dto.media.SharePointConnectionParametersDto;
import com.cla.filecluster.service.media.MicrosoftMediaAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 *
 * Created by uri on 07/08/2016.
 */
@RestController
@RequestMapping("/api/media/sharepoint")
public class SharePointApiController extends MicrosoftApiControllerBase {

    private final Function<Long, SharePointConnectionDetailsDto> GET_CONNECTION_DETAILS_FUNC = (id) -> connectionDetailsService.getSharePointConnectionDetailsById(id, false);

    @Autowired
    private MicrosoftMediaAppService microsoftMediaAppService;

    @RequestMapping(value = {"/connections/test/dataCenter/{customerDataCenterId}", "/connections/{connectionId}/test/dataCenter/{customerDataCenterId}"},
            method = RequestMethod.POST)
    public TestResultDto testSharePointConnection(@RequestBody(required = false) SharePointConnectionParametersDto sharePointConnectionParamDto, @PathVariable(required = false) Optional<Long> connectionId, @PathVariable Long customerDataCenterId) {
        return testConnection(sharePointConnectionParamDto, customerDataCenterId, connectionId, new SharePointConnectionDetailsDto(), GET_CONNECTION_DETAILS_FUNC);
    }

    @RequestMapping(value = "/connections/new", method = RequestMethod.POST)
    public MicrosoftConnectionDetailsDtoBase configureSharePointConnectionDetails(@RequestBody SharePointConnectionDetailsDto sharePointConnectionDetailsDto) {
        return connectionDetailsService.configureMicrosoftConnectionDetails(sharePointConnectionDetailsDto);
    }

    @RequestMapping(value = "/server/folders", method = RequestMethod.GET)
    public List<ServerResourceDto> listServerFolders(@RequestParam long connectionId, @RequestParam(required = false) String id, @RequestParam long customerDataCenterId) {
        SharePointConnectionDetailsDto dto = connectionDetailsService.getSharePointConnectionDetailsById(connectionId, true);
        return microsoftMediaAppService.listFoldersUnderPath(dto, Optional.ofNullable(id), customerDataCenterId, false);
    }


    @RequestMapping(value = "/connections/{id}", method = RequestMethod.POST)
    public MicrosoftConnectionDetailsDtoBase updateSharePointConnectionDetails(@PathVariable long id,
                                                                               @RequestBody SharePointConnectionDetailsDto sharePointConnectionDetailsDto) {
        return connectionDetailsService.updateSharePointConnectionDetailsById(id, sharePointConnectionDetailsDto);
    }

    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public List<MicrosoftConnectionDetailsDtoBase> getSharePointConnectionDetails() {
        return connectionDetailsService.getSharePointConnectionDetails(true);
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.GET)
    public MicrosoftConnectionDetailsDtoBase getSharePointConnectionDetails(@PathVariable long id) {
        SharePointConnectionDetailsDto dto = connectionDetailsService.getSharePointConnectionDetailsById(id, true);
        if (dto.getSharePointConnectionParametersDto() != null) {
            dto.getSharePointConnectionParametersDto().setPassword(null);
        }
        return dto;
    }
}
