package com.cla.filecluster.service.event;

import com.cla.common.domain.dto.event.*;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Queue;

/**
 * Used to generate map diff events (new file, cng acl etc)
 * Created by: yael
 * Created on: 1/11/2018
 */
@Service
public class ScanDiffEventProvider extends AbstractEventProvider<DAEvent> implements EventProvider<DAEvent> {

    private Logger logger = LoggerFactory.getLogger(ScanDiffEventProvider.class);

    @Autowired
    private Queue<DAEvent> eventQueue;

    @Autowired
    private EmptyEventNormalizer normalizer;

    @Override
    public EventProviderType getType() {
        return EventProviderType.DOCAUTHORITY;
    }

    @Override
    public EventNormalizer<DAEvent> getEventNormalizer(Class<DAEvent> dataType) {
        return normalizer;
    }

    @Override
    public void send(DAEvent event) {
        logger.trace("Sending {}", event);
        eventQueue.add(event);
    }

    public void generateScanDiffEvent(EventType eventType, Long fileId, String path) {
        generateScanDiffEvent(eventType, fileId, path, Maps.newHashMap());
    }

    public void generateScanDiffEvent(EventType eventType, Long fileId, String path, Map<String, Object> factsMap) {
        try {
            ScanDiffEvent event = new ScanDiffEvent();
            event.setCreated(System.currentTimeMillis());
            event.setReported(System.currentTimeMillis());
            event.setEventType(eventType);
            event.setEventProvider(getType());
            event.setPayload(createPayload(fileId, path));
            event.setFactsMap(factsMap);
            processEvent(event, DAEvent.class);
        } catch (Throwable t) { // we will never want any exception to fail the scan process because of event failure
            logger.error("exception occured during event creation of type {} for file {}", eventType, fileId, t);
        }
    }

    private ScanDiffPayload createPayload(Long fileId, String path) {
        ScanDiffPayload payload = new ScanDiffPayload();
        payload.setFileId(fileId);
        payload.setPath(path);
        return payload;
    }
}
