package com.cla.filecluster.service.export.exporters.settings.component;

import com.cla.common.domain.dto.components.ComponentState;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import static com.cla.filecluster.service.export.exporters.settings.component.SystemComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 5/27/2019
 */
public class ClaComponentDtoMixin {

    @JsonProperty(INSTANCE)
    private String instanceId;

    @JsonProperty(LOCATION)
    private String location;

    @JsonProperty(EXTERNAL_ADDR)
    private String externalNetAddress;

    @JsonProperty(LOCAL_ADDR)
    private String localNetAddress;

    @JsonProperty(STATE)
    private ComponentState state;

    @JsonUnwrapped
    private CustomerDataCenterDto customerDataCenterDto;
}
