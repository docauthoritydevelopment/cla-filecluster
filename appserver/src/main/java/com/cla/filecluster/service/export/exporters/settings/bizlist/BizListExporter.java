package com.cla.filecluster.service.export.exporters.settings.bizlist;

import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.settings.bizlist.mixin.SimpleBizListItemDtoMixin;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.filecluster.service.export.exporters.settings.bizlist.BizListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
@Slf4j
@Component
public class BizListExporter extends PageCsvExcelExporter<SimpleBizListItemDto> {

    @Autowired
    private BizListAppService bizListAppService;

    private final static String[] header = {NAME, ALIASES, STATE, BUSINESS_ID};

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(SimpleBizListItemDto.class, SimpleBizListItemDtoMixin.class);
    }

    @Override
    protected Iterable<SimpleBizListItemDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);

        long bizListId = 0;
        if (requestParams.containsKey("bizListId")) {
            String dcStr = requestParams.get("bizListId");
            if (!Strings.isNullOrEmpty(dcStr)) {
                bizListId = Long.parseLong(dcStr);
            }
        }

        return (Iterable<SimpleBizListItemDto>) bizListAppService.listBizListItems(bizListId, dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(SimpleBizListItemDto base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();

        if (base.getEntityAliases() != null && base.getEntityAliases().size() > 0) {
            List<String> aliases = base.getEntityAliases().stream().map(alias -> alias.getName()).collect(Collectors.toList());
            res.put(ALIASES, StringUtils.join(aliases, ";"));
        }

        return res;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.BIZ_LIST_SETTINGS);
    }
}
