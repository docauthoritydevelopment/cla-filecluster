package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * operation to add child groups to a user group
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
@Service
public class AddToJoinedGroupOperation implements ScheduledOperation {

    public static String USER_GROUP_ID_PARAM = "USER_GROUP_ID";
    public static String CHILD_GROUPS_PARAM = "CHILD_GROUPS";
    public static String UPDATED_CHILD_GROUPS_PARAM = "UPDATED_CHILD_GROUPS";

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 2;
    }

    @Override
    public List<String> getRelevantGroupIds(Map<String, String> opData) {
        List<String> result = new ArrayList<>();
        String groupId = opData.get(USER_GROUP_ID_PARAM);
        result.add(groupId);
        List<String> childrenGroupIds = new ArrayList<>(Arrays.asList(
                opData.get(CHILD_GROUPS_PARAM).split(",")));
        result.addAll(childrenGroupIds);
        return result;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            groupsApplicationService.addGroupsToJoinedGroupUpdateFiles(opData);
        } else if (step == 1) {
            groupsApplicationService.addGroupsToJoinedGroupUpdateParentGroup(opData);
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String groupId = opData.get(USER_GROUP_ID_PARAM);
        if (Strings.isNullOrEmpty(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.empty"), BadRequestType.MISSING_FIELD);
        }
        String childGroups = opData.get(CHILD_GROUPS_PARAM);
        if (Strings.isNullOrEmpty(childGroups)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.child.empty"), BadRequestType.MISSING_FIELD);
        }
        String childGroupsUpd = opData.get(UPDATED_CHILD_GROUPS_PARAM);
        if (Strings.isNullOrEmpty(childGroupsUpd)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.join-child.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.ADD_TO_JOINED_GROUP;
    }
}
