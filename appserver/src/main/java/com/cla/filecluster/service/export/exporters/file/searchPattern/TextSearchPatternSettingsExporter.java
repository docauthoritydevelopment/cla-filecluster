package com.cla.filecluster.service.export.exporters.file.searchPattern;

import com.cla.common.domain.dto.filesearchpatterns.TextSearchPatternDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.extractor.pattern.SearchPatternAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.cla.filecluster.service.export.exporters.file.searchPattern.TextSearchPatternHeaderFields.*;

/**
 * Created by: ophir
 * Created on: 4/18/2019
 */
@Slf4j
@Component
public class TextSearchPatternSettingsExporter extends PageCsvExcelExporter<TextSearchPatternDto> {

    private final static String[] HEADER = {NAME,DESCRIPTION,ACTIVE,PATTERN,CLASS,CATEGORY,SUB_CATEGORY,REGULATIONS};

    @Autowired
    private SearchPatternAppService searchPatternAppService;


    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(TextSearchPatternDto.class, TextSearchPatternSettingsDtoMixin.class);
    }

    @Override
    protected Page<TextSearchPatternDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        List<Integer> regulationIds = null;
        if (!Strings.isNullOrEmpty(requestParams.get("regulationFilter"))) {
            String regulationFilterStr = requestParams.get("regulationFilter").replace(" ", "");
            if (!regulationFilterStr.isEmpty()) {
                regulationIds = Stream.of(regulationFilterStr.split(","))
                        .map(Integer::parseInt)
                        .collect(Collectors.toList());
            }
        }
        return searchPatternAppService.listSearchPatterns(dataSourceRequest,regulationIds);
    }

    @Override
    protected Map<String, Object> addExtraFields(TextSearchPatternDto base, Map<String, String> requestParams) {
        Map<String, Object> result = new HashMap<>();
        result.put(REGULATIONS, getRegulationsStr(base));
        return result;
    }

    private String getRegulationsStr(TextSearchPatternDto textSearchPatternDto) {
        return textSearchPatternDto.getRegulationStr().replaceAll(",",";");
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.TEXT_SEARCH_PATTERNS_SETTINGS);
    }
}
