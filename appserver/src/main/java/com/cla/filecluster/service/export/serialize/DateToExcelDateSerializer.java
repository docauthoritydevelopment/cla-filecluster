package com.cla.filecluster.service.export.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * serializes epoch time to format d-MMM-yy HH:mm.
 * i.e 29-Jan-19 11:28
 */
public class DateToExcelDateSerializer extends JsonSerializer<Date> {
    private static final String DATE_FORMAT_PATTERN = "d-MMM-yy HH:mm";
    private ThreadLocal<SimpleDateFormat> simpleDateFormatThreadLocal = ThreadLocal
            .withInitial(() -> new SimpleDateFormat(DATE_FORMAT_PATTERN));

    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(Optional.ofNullable(value)
                .map(val -> {
                    SimpleDateFormat simpleDateFormat = simpleDateFormatThreadLocal.get();
                    return simpleDateFormat.format(val);
                })
                .orElse(""));
    }
}
