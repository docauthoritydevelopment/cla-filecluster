package com.cla.filecluster.service.api.media;

import com.cla.common.domain.dto.generic.TestResultDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.Exchange365ConnectionDetailsDto;
import com.cla.connector.domain.dto.media.Exchange365ConnectionParametersDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.media.Exchange365MediaAppService;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.google.common.base.Strings;
import org.apache.catalina.connector.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 2/4/2019
 */
@RestController
@RequestMapping("/api/media/exchange365")
public class Exchange365ApiController {

    private final Logger logger = LoggerFactory.getLogger(Exchange365ApiController.class);

    @Autowired
    private MediaConnectionDetailsService connectionDetailsService;

    @Autowired
    private Exchange365MediaAppService exchangeMediaAppService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/connections/new", method = {RequestMethod.PUT, RequestMethod.POST})
    public Exchange365ConnectionDetailsDto newConnection(@RequestBody Exchange365ConnectionDetailsDto connectionDetails){
        Exchange365ConnectionDetailsDto exchangeConnection = connectionDetailsService.createExchange365Connection(connectionDetails);
        exchangeConnection.getExchangeConnectionParametersDto().setPassword(null);
        return exchangeConnection;
    }

    @RequestMapping(value = "/server/folders", method = RequestMethod.GET)
    public List<ServerResourceDto> listServerFolders(@RequestParam long connectionId,
                                                     @RequestParam long customerDataCenterId,
                                                     @RequestParam(required = false, name = "id") String folderId) {
        Exchange365ConnectionDetailsDto connectionDetailsDto = (Exchange365ConnectionDetailsDto) connectionDetailsService.getMediaConnectionDetailsById(connectionId);
        if(connectionDetailsDto == null){
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return exchangeMediaAppService.listFoldersUnderPath(connectionDetailsDto, Optional.ofNullable(folderId), customerDataCenterId, false);
    }

    @RequestMapping(value = "/connections/test/dataCenter/{customerDataCenterId}", method = RequestMethod.POST)
    public TestResultDto testConnection(@RequestBody Exchange365ConnectionDetailsDto exchangeConnectionDetailsDto, @PathVariable long customerDataCenterId){
        try {
            if (exchangeConnectionDetailsDto.getExchangeConnectionParametersDto() == null) {
                exchangeConnectionDetailsDto.setExchangeConnectionParametersDto(new Exchange365ConnectionParametersDto());
            }

            if (exchangeConnectionDetailsDto.getId() != null &&
                    exchangeConnectionDetailsDto.getExchangeConnectionParametersDto().getPassword() == null) {//test new connection
                Exchange365ConnectionDetailsDto exchangeConnectionDetailsDtoExt =
                        (Exchange365ConnectionDetailsDto) connectionDetailsService.getMediaConnectionDetailsById(exchangeConnectionDetailsDto.getId());

                if (exchangeConnectionDetailsDtoExt != null && exchangeConnectionDetailsDtoExt.getExchangeConnectionParametersDto() != null &&
                        exchangeConnectionDetailsDtoExt.getExchangeConnectionParametersDto().getPassword() != null) {
                    exchangeConnectionDetailsDto.getExchangeConnectionParametersDto().setPassword(
                            exchangeConnectionDetailsDtoExt.getExchangeConnectionParametersDto().getPassword()
                    );
                }
            }
            List<ServerResourceDto> serverResourceDtos = exchangeMediaAppService.listFoldersUnderPath(exchangeConnectionDetailsDto, Optional.empty(), customerDataCenterId, true);
            return new TestResultDto(true, serverResourceDtos.get(0).getFullName());
        } catch (Exception e) {
            return new TestResultDto(false, e.getMessage());
        }
    }

    /*
     *	Both following API calls are abomination and should be abolished (as there is already a dedicated media connection service/controller)
     */
    @RequestMapping(value = "/connections", method = RequestMethod.GET)
    public List<Exchange365ConnectionDetailsDto> getConnectionDetails() {
        return connectionDetailsService.getMediaConnectionDetailsDtoByType(MediaType.EXCHANGE_365, true);
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.GET)
    public Exchange365ConnectionDetailsDto getConnectionDetails(@PathVariable long id) {
        MediaConnectionDetailsDto mediaDto = connectionDetailsService.getMediaConnectionDetailsById(id, false);
        if (mediaDto == null) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        } else if (!(mediaDto instanceof Exchange365ConnectionDetailsDto)) {
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        Exchange365ConnectionDetailsDto dto = (Exchange365ConnectionDetailsDto)mediaDto;
        if (dto.getExchangeConnectionParametersDto() != null) {
            dto.getExchangeConnectionParametersDto().setPassword(null);
        }
        return dto;
    }

    @RequestMapping(value = "/connections/{id}", method = RequestMethod.POST)
    public Exchange365ConnectionDetailsDto updateConnectionDetails(@PathVariable long id, @RequestBody Exchange365ConnectionDetailsDto exchangeConnectionDetailsDto) {
        MediaConnectionDetailsDto mediaDto = connectionDetailsService.getMediaConnectionDetailsDtoByConnectionId(id);
        if(mediaDto == null){
            throw new BadRequestException(messageHandler.getMessage("media-conn.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (exchangeConnectionDetailsDto.getExchangeConnectionParametersDto() != null &&
                Strings.isNullOrEmpty(exchangeConnectionDetailsDto.getExchangeConnectionParametersDto().getPassword())) {
            Exchange365ConnectionDetailsDto oldDto = (Exchange365ConnectionDetailsDto)mediaDto;
            exchangeConnectionDetailsDto.getExchangeConnectionParametersDto().setPassword(oldDto.getExchangeConnectionParametersDto().getPassword());
        }
        return connectionDetailsService.updateConnectionDetails(id, exchangeConnectionDetailsDto);
    }

    //region ...legacy methods. Not in use.
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public void login(HttpServletRequest request, HttpServletResponse response) {
        String sessionId = request.getSession().getId();
        String authorizeUrl = buildAuthorizeUrl(sessionId);
        logger.debug(authorizeUrl);
        try {
            if (response instanceof Response) {
                ((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
            }
            response.sendRedirect(authorizeUrl);
        } catch (IOException | RuntimeException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/login/file", method = RequestMethod.GET)
    public void loginToFile(HttpServletRequest request, HttpServletResponse response) {
        String authorizeUrl = buildAuthorizeUrl("file");
        logger.debug(authorizeUrl);
        try {
            if (response instanceof Response) {
                ((Response) response).sendRedirect(authorizeUrl, HttpServletResponse.SC_SEE_OTHER);
            }
            response.sendRedirect(authorizeUrl);
        } catch (IOException | RuntimeException e) {
            logger.error("Failed to send redirect", e);
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/response", method = {RequestMethod.GET, RequestMethod.POST})
    public String exchangeOauthResponse(@QueryParam("code") String code,
                                   @QueryParam("state") String state,
                                   @QueryParam("error") String error,
                                   @QueryParam("errorDescription") String errorDescription) {
        logger.debug("Got exchange Oauth response: {} {} {} {}", code, state, error, errorDescription);
        Long currentUserId = 1L; //TODO: get current user ID
        try {
            if (state.equals("file")) {
                exchangeMediaAppService.handleOauthResponse(currentUserId, code, true);
            } else {
                exchangeMediaAppService.fillAuthentication(state);
                exchangeMediaAppService.handleOauthResponse(currentUserId, code, false);

            }
        } catch (RuntimeException e) {
            logger.error("Failed to handle exchange response", e);
            throw e;
        }
        return "OK";
    }

    private String buildAuthorizeUrl(String sessionId) {
        return exchangeMediaAppService.buildAuthorizeUrl(sessionId);
    }
}
