package com.cla.filecluster.service.files.filetree;

import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.schedule.ScheduleConfigDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleType;
import com.cla.connector.constants.KendoFilterConstants;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.crawler.CrawlRun;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.media.MediaConnectionDetails;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.importEntities.SchemaData;
import com.cla.filecluster.service.importEntities.SchemaLoader;
import com.cla.filecluster.service.media.MediaConnectionDetailsService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class RootfolderSchemaLoader extends SchemaLoader<RootfolderItemRow,RootFolderDto> {
    private static Logger logger = LoggerFactory.getLogger(RootfolderSchemaLoader.class);

    public static final String SHARE_POINT_STUB_DEFAULT_CONNECTION_NAME = "default";

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Autowired
    private MediaConnectionDetailsService mediaConnectionDetailsService;

    @Autowired
    private DepartmentService departmentService;

    private final static String CSV_HEADER_PATH = "PATH";
    private final static String CSV_HEADER_RESCAN = "RESCAN";
    private final static String CSV_HEADER_MEDIA_TYPE = "MEDIA TYPE";
    private final static String CSV_HEADER_DESCRIPTION = "DESCRIPTION";
    private final static String CSV_HEADER_SCHEDULE_GROUP_NAME = "SCHEDULE GROUP NAME";
    private final static String CSV_HEADER_EXTRACT_BIZ_LISTS = "EXTRACT BUSINESS LISTS";
    private final static String CSV_HEADER_NICK_NAME = "NICK NAME";
    private final static String CSV_HEADER_STORE_PURPOSE = "STORE PURPOSE";
    private final static String CSV_HEADER_STORE_SECURITY = "STORE SECURITY";
    private final static String CSV_HEADER_STORE_LOCATION = "STORE LOCATION";
    private final static String CSV_HEADER_MAPPED_FILE_TYPES = "MAPPED FILE TYPES";
    private final static String CSV_HEADER_INGESTED_FILE_TYPES = "INGESTED FILE TYPES";
    private final static String CSV_HEADER_MEDIA_CONNECTION_NAME = "MEDIA CONNECTION NAME";
    private final static String CSV_HEADER_CUSTOMER_DATA_CENTER = "CUSTOMER DATA CENTER";
    private final static String CSV_HEADER_REINGEST = "REINGEST";
    private final static String CSV_HEADER_REINGEST_TIME_SEC = "REINGEST TIME IN SECONDS";
    private final static String CSV_HEADER_SCANNED_LIMIT_COUNT = "SCANNED LIMIT COUNT";
    private final static String CSV_HEADER_MEANINGFUL_FILES_SCANNED_LIMIT_COUNT = "MEANINGFUL FILES SCANNED LIMIT COUNT";
    private final static String CSV_HEADER_SCAN_TASK_DEPTH = "SCAN TASK DEPTH";
    private final static String CSV_HEADER_MAILBOX_GROUP = "MAILBOX GROUP";
    private final static String CSV_HEADER_FROM_DATE_FILTER = "FROM DATE SCAN FILTER";
    private final static String CSV_HEADER_DEPARTMENT_NAME = "DEPARTMENT";
    private final static String CSV_HEADER_MATTER_NAME = "MATTER";
    private final static String CSV_HEADER_EXCLUDED_FOLDERS_RULES = "EXCLUDED FOLDERS RULES";

    private final static String[] REQUIRED_HEADERS = {CSV_HEADER_PATH, CSV_HEADER_MEDIA_TYPE};

    public RootfolderSchemaLoader() {
        super(REQUIRED_HEADERS);
    }

    private Map<Long, String> scheduleGroups;
    private Map<Long, String> dataCenters;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public RootfolderSchemaData createSchemaInstance()
    {
        return new RootfolderSchemaData();
    }

    @Override
    protected void prepareForImport() {
        scheduleGroups = scheduleGroupService.getScheduleGroupsNames();
        dataCenters = customerDataCenterService.getCustomerDataCentersNames();
    }

    @Override
    protected RootfolderItemRow extractRowSpecific(SchemaData<RootfolderItemRow> schemaData, String[] fieldsValues, ArrayList<String> parsingWarningMessages ) {
         RootfolderItemRow itemRow = new RootfolderItemRow();
        itemRow.setRealPath(extractValue(schemaData, fieldsValues, CSV_HEADER_PATH, parsingWarningMessages));
        if (itemRow.getRealPath() != null && itemRow.getRealPath().startsWith("#")) {
            return null;    // Ignore comment
        }
        itemRow.setRescanActive(extractBooleanValue(schemaData, fieldsValues, CSV_HEADER_RESCAN, parsingWarningMessages));

        String nickname = extractValue(schemaData, fieldsValues, CSV_HEADER_NICK_NAME, parsingWarningMessages);
        if (Strings.isNullOrEmpty(nickname) && !Strings.isNullOrEmpty(itemRow.getRealPath())) {
            int start = itemRow.getRealPath().lastIndexOf("\\");
            if (start < 0) {
                start = itemRow.getRealPath().lastIndexOf("/");
            }
            start++;
            nickname = itemRow.getRealPath().substring(start);
        }
        itemRow.setNickName(nickname);

        itemRow.setStoreLocation(extractValue(schemaData, fieldsValues, CSV_HEADER_STORE_LOCATION, StoreLocation.class, parsingWarningMessages));
        itemRow.setStorePurpose(extractValue(schemaData, fieldsValues, CSV_HEADER_STORE_PURPOSE, StorePurpose.class, parsingWarningMessages));
        itemRow.setStoreSecurity(extractValue(schemaData, fieldsValues, CSV_HEADER_STORE_SECURITY, StoreSecurity.class, parsingWarningMessages));
        itemRow.setDescription(extractValue(schemaData, fieldsValues, CSV_HEADER_DESCRIPTION, parsingWarningMessages));
        itemRow.setMediaType(extractValue(schemaData, fieldsValues, CSV_HEADER_MEDIA_TYPE, MediaType.class, parsingWarningMessages));
        itemRow.setExtractBizLists(extractBooleanValue(schemaData, fieldsValues, CSV_HEADER_EXTRACT_BIZ_LISTS, parsingWarningMessages));

        List<FileType> fileTypes = extractMultipleEnums(schemaData, fieldsValues, CSV_HEADER_MAPPED_FILE_TYPES, FileType.class, parsingWarningMessages);
        itemRow.setMappedFileTypes( fileTypes.toArray(new FileType[fileTypes.size()]));

        fileTypes = extractMultipleEnums(schemaData, fieldsValues, CSV_HEADER_INGESTED_FILE_TYPES, FileType.class, parsingWarningMessages);
        itemRow.setIngestedFileTypes( fileTypes != null ? fileTypes.toArray(new FileType[fileTypes.size()]) : itemRow.getMappedFileTypes());

        itemRow.setScheduleGroupName(extractValue(schemaData, fieldsValues, CSV_HEADER_SCHEDULE_GROUP_NAME, parsingWarningMessages));
        itemRow.setMediaConnectionName(extractValue(schemaData, fieldsValues, CSV_HEADER_MEDIA_CONNECTION_NAME, parsingWarningMessages));
        itemRow.setCustomerDataCenterName(extractValue(schemaData, fieldsValues, CSV_HEADER_CUSTOMER_DATA_CENTER, parsingWarningMessages));

        itemRow.setReingest(extractBooleanValue(schemaData, fieldsValues, CSV_HEADER_REINGEST, parsingWarningMessages));
        itemRow.setReingestTimeInSeconds(extractLongValue(schemaData, fieldsValues, CSV_HEADER_REINGEST_TIME_SEC, parsingWarningMessages));

        itemRow.setScannedLimitCount(extractIntegerValue(schemaData, fieldsValues, CSV_HEADER_SCANNED_LIMIT_COUNT, parsingWarningMessages));
        itemRow.setMeaningfulFilesScannedLimitCount(extractIntegerValue(schemaData, fieldsValues, CSV_HEADER_MEANINGFUL_FILES_SCANNED_LIMIT_COUNT, parsingWarningMessages));
        itemRow.setScanTaskDepth(extractIntegerValue(schemaData, fieldsValues, CSV_HEADER_SCAN_TASK_DEPTH, parsingWarningMessages));

        itemRow.setMailboxGroup(extractValue(schemaData, fieldsValues, CSV_HEADER_MAILBOX_GROUP, parsingWarningMessages));
        String departmentName = extractValue(schemaData, fieldsValues, CSV_HEADER_DEPARTMENT_NAME, parsingWarningMessages);
        if (departmentName == null) {
            departmentName = extractValue(schemaData, fieldsValues, CSV_HEADER_MATTER_NAME, parsingWarningMessages);
        }
        itemRow.setDepartmentName(departmentName);
        itemRow.setFromDateScanFilter(extractLongValue(schemaData, fieldsValues, CSV_HEADER_FROM_DATE_FILTER, parsingWarningMessages));

        String excludeFoldersStr = extractValue(schemaData, fieldsValues, CSV_HEADER_EXCLUDED_FOLDERS_RULES, parsingWarningMessages);
        if (!Strings.isNullOrEmpty(excludeFoldersStr)) {
            itemRow.setExcludedFolders(excludeFoldersStr.split(","));
        }

        return itemRow;
    }


    protected boolean validateItemRow(RootFolderDto rootFolderDto, RootfolderItemRow itemRow,
                                    SchemaData<RootfolderItemRow> schemaData, boolean afterAddToDB, boolean createStubs ) {
        boolean isValid = true;

        if (itemRow.getMediaType() == null ) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-media-type"));
            return false;
        }

        if (!itemRow.getMediaType().hasPath()) {
            if (Strings.isNullOrEmpty(rootFolderDto.getMailboxGroup())){
                schemaData.addErrorRowItem( itemRow, messageHandler.getMessage("root-folder.import.missing-mailbox-group"));
                return false;
            }
        } else if (Strings.isNullOrEmpty(rootFolderDto.getPath())){
            schemaData.addErrorRowItem( itemRow, messageHandler.getMessage("root-folder.import.missing-path"));
            return false;
        }

        if(!afterAddToDB) { //validate if already exists - only before added to db
            if (rootFolderDto.getPath() != null && docStoreService.getRootFolderByPath(rootFolderDto.getPath()) != null) {
                schemaData.addDupRowItem(itemRow, messageHandler.getMessage("root-folder.import.duplicate-path", Lists.newArrayList(rootFolderDto.getRealPath())));
                isValid = false;
            } else if (!Strings.isNullOrEmpty(rootFolderDto.getMailboxGroup()) && !itemRow.getMediaType().hasPath()) {
                List<RootFolder> byMailbox =  docStoreService.findByMailboxGroup(rootFolderDto.getMailboxGroup());
                if (byMailbox != null && !byMailbox.isEmpty()) {
                    schemaData.addDupRowItem(itemRow, messageHandler.getMessage("root-folder.import.duplicate-mailbox-group", Lists.newArrayList(rootFolderDto.getMailboxGroup())));
                    isValid = false;
                }
            }
        }

        //Warnings must be at the end and are concatenated

        if (itemRow.getMappedFileTypes() == null || itemRow.getMappedFileTypes().length == 0) {
            schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-map-file-type"));
            isValid = false;
            //continue validating when warnings
        }

        if (itemRow.getIngestedFileTypes() == null || itemRow.getIngestedFileTypes().length == 0) {
            schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-ingest-file-type"));
            isValid = false;
            //continue validating when warnings
        }

        try {
            rootFolderService.validateFileTypes(itemRow.getMappedFileTypes(), itemRow.getIngestedFileTypes());
        } catch (Exception e) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.bad-file-type"));
            isValid = false;
        }

        Long scheduleGroupId = parseScheduleGroupFromItemRow(itemRow);
        if (Strings.isNullOrEmpty(itemRow.getScheduleGroupName()) && scheduleGroupId==null) {
            if(createStubs) {
                if (!afterAddToDB) {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-scheduled-group-default"));
                } else {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.default-scheduled-group"));
                }
            }
            else {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-scheduled-group"));
            }
            isValid = false;
        }
        else if (!Strings.isNullOrEmpty(itemRow.getScheduleGroupName()) && scheduleGroupId == null) {
            if (createStubs) {
                if (!afterAddToDB) {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.create-scheduled-group", Lists.newArrayList(itemRow.getScheduleGroupName())));
                } else {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.scheduled-group-created", Lists.newArrayList(itemRow.getScheduleGroupName())));
                }
            }
            else {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.scheduled-group-not-found", Lists.newArrayList(itemRow.getScheduleGroupName())) );
            }
            isValid = false;
        }

        if (itemRow.getMediaType() == MediaType.MIP) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.invalid-media-type"));
            isValid = false;
        }

        if (itemRow.getMediaType() == MediaType.SHARE_POINT ||
                itemRow.getMediaType() == MediaType.ONE_DRIVE ||
                itemRow.getMediaType() == MediaType.EXCHANGE_365 ||
                itemRow.getMediaType() == MediaType.BOX) {
            if (itemRow.getMediaConnectionName() == null) {
                if (createStubs) {
                    if (!afterAddToDB) {
                        schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-connection-default"));
                    } else {
                        schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.default-connection"));
                    }
                } else {
                    schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-connection"));
                }
                isValid = false;
            } else {
                if (rootFolderDto.getMediaConnectionDetailsId() == null) {
                    if (createStubs) {
                        if (!afterAddToDB) {
                            schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.create-connection", Lists.newArrayList(itemRow.getMediaConnectionName())));
                        } else {
                            schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.connection-created", Lists.newArrayList(itemRow.getMediaConnectionName())));
                        }
                    } else {
                        schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.connection-not-found", Lists.newArrayList(itemRow.getMediaConnectionName())) );
                    }
                    isValid = false;
                }
            }
           if (rootFolderDto.getMediaConnectionDetailsId()!=null) {
               if (rootFolderDto.getMediaType().equals(MediaType.SHARE_POINT)) {
                   boolean sharePointConnectionValid = mediaConnectionDetailsService.isSharePointConnectionValid(rootFolderDto.getMediaConnectionDetailsId());
                   if (!sharePointConnectionValid) {
                       schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.connection-invalid", Lists.newArrayList(itemRow.getMediaConnectionName())));
                       isValid = false;
                   }
               } else {
                   MediaConnectionDetailsDto connDet = mediaConnectionDetailsService.getMediaConnectionDetailsById(rootFolderDto.getMediaConnectionDetailsId(), true);
                   if (connDet == null) {
                       schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.connection-missing", Lists.newArrayList(itemRow.getMediaConnectionName())));
                       isValid = false;
                   }
               }
           }
        }


        if (itemRow.getCustomerDataCenterName() == null) {
            if (createStubs) {
                if (!afterAddToDB) {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-data-center-default"));
                } else {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.default-data-center"));
                }
            }
            else {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.missing-data-center"));
            }
            isValid = false;
        }
        else if (!Strings.isNullOrEmpty(itemRow.getCustomerDataCenterName()) && rootFolderDto.getCustomerDataCenterDto() == null) {
            if (createStubs) {
                if (!afterAddToDB) {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.create-data-center", Lists.newArrayList(itemRow.getCustomerDataCenterName())));
                } else {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.data-center-created", Lists.newArrayList(itemRow.getCustomerDataCenterName())));
                }
            }
            else {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.data-center-not-found", Lists.newArrayList(itemRow.getCustomerDataCenterName())));
            }
            isValid = false;
        }

        if (!Strings.isNullOrEmpty(itemRow.getDepartmentName()) && rootFolderDto.getDepartmentId() == null) {
            if (createStubs) {
                if (!afterAddToDB) {
                    schemaData.addWarningRowItem(itemRow, messageHandler.getMessage("root-folder.import.create-department", Lists.newArrayList(itemRow.getDepartmentName())));
                } else {
                    schemaData.addWarningRowItem(itemRow,  messageHandler.getMessage("root-folder.import.department-created", Lists.newArrayList(itemRow.getDepartmentName())));
                }
            }
            else {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("root-folder.import.department-not-found", Lists.newArrayList(itemRow.getDepartmentName())) );
            }
            isValid = false;
        }

        return isValid;
    }

    @Override
    protected Boolean loadItemRowToRepository(SchemaData<RootfolderItemRow> schemaData, RootfolderItemRow itemRow,
                                              boolean updateDuplicates, boolean createStubs) {

        RootFolderDto newRootFolderDto;
        RootFolderDto resultRootFolderDto;
        newRootFolderDto = createDtoFromRowItem(itemRow);
        Boolean isValid = validateItemRow(newRootFolderDto, itemRow, schemaData, true, createStubs); //isValid counts only rows without warnings and errors
        if(!itemRow.getError()) {

            if (createStubs) {
                createStubMediaConnectionIfNeeded(itemRow, newRootFolderDto);
                createStubCustomerDataCenterIfNeeded(itemRow, newRootFolderDto);
                createScheduleGroupIfNeeded(itemRow);
                createDepartmentIfNeeded(itemRow, newRootFolderDto);
            }
            Long scheduleGroupId = parseScheduleGroupFromItemRow(itemRow);
            if (updateDuplicates) {
                RootFolderDto rootFolderForDuplicate;
                if (Strings.isNullOrEmpty(newRootFolderDto.getPath()) && !Strings.isNullOrEmpty(newRootFolderDto.getMailboxGroup())) {
                    rootFolderForDuplicate = rootFolderService.findRootFolderByMailbox(newRootFolderDto.getMailboxGroup(), false);
                } else {
                    rootFolderForDuplicate = rootFolderService.findRootFolder(newRootFolderDto.getPath(), false);
                }

                if (rootFolderForDuplicate != null) {
                    if (newRootFolderDto.getReingestTimeStampMs() > 0) {
                        if (rootFolderForDuplicate.getLastRunId() != null && rootFolderForDuplicate.getLastRunId() > 0) {
                            CrawlRun cr = fileCrawlerExecutionDetailsService.getCrawlRun(rootFolderForDuplicate.getLastRunId());
                            if (cr.getStartTime().getTime() > newRootFolderDto.getReingestTimeStampMs()) {
                                newRootFolderDto.setReingestOff();
                            }
                        } else {
                            newRootFolderDto.setReingestOff();
                        }
                    }

                    resultRootFolderDto = rootFolderService.updateRootFolder(rootFolderForDuplicate.getId(), newRootFolderDto);
                    if (scheduleGroupId != null) {
                        scheduleAppService.updateScheduleGroupForRootFolder(resultRootFolderDto.getId(), scheduleGroupId);
                    }
                    auditRootFolderUpdate(resultRootFolderDto, scheduleGroupId);
                } else {
                    resultRootFolderDto = rootFolderService.createRootFolder(newRootFolderDto, scheduleGroupId);
                    auditRootFolderCreation(resultRootFolderDto, scheduleGroupId);
                }
            } else {
                resultRootFolderDto = rootFolderService.createRootFolder(newRootFolderDto, scheduleGroupId);
                auditRootFolderCreation(resultRootFolderDto, scheduleGroupId);
            }
            if (resultRootFolderDto == null) {
                schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("import.error-insert-row"));
                return false;
            }

            createFolderExcludeRulesIfNeeded(itemRow, resultRootFolderDto);

            return isValid;
        }
        return false;
    }

    private void createFolderExcludeRulesIfNeeded(RootfolderItemRow itemRow, RootFolderDto resultRootFolderDto) {
        List<Long> rulesFound = new ArrayList<>();
        // get current rules
        List<FolderExcludeRuleDto> currentRules = docStoreService.getFolderExcludeRulesDto(resultRootFolderDto.getId());

        // if rules were set in row
        if (itemRow.getExcludedFolders() != null && itemRow.getExcludedFolders().length > 0) {

            // go over rules from row
            for (String rule : itemRow.getExcludedFolders()) {
                FolderExcludeRuleDto currentRule = null;

                // find existing rule if exists
                if (currentRules != null && !currentRules.isEmpty()) {
                    for (FolderExcludeRuleDto ruleDto : currentRules) {
                        if (ruleDto.getMatchString().equals(rule)) {
                            currentRule = ruleDto;
                            break;
                        }
                    }
                }

                // rule does not exists - create it
                if (currentRule == null) {
                    currentRule = new FolderExcludeRuleDto(rule, KendoFilterConstants.EQ, resultRootFolderDto.getId(), false);
                    FolderExcludeRuleDto folderExcludeRuleDto1 = rootFolderService.addRootFolderExcludeRule(resultRootFolderDto.getId(), currentRule);
                    eventAuditService.auditDual(AuditType.ROOT_FOLDER,"Update root folder excluded folders", AuditAction.UPDATE,
                            RootFolderDto.class,resultRootFolderDto.getId(),FolderExcludeRuleDto.class,folderExcludeRuleDto1.getId(),folderExcludeRuleDto1);
                } else { // mark rule as found
                    rulesFound.add(currentRule.getId());
                }
            }
        }

        // go over existing rules, if not mentioned - disable them
        if (currentRules != null && !currentRules.isEmpty()) {
            currentRules.forEach(rule -> {
                if (!rulesFound.contains(rule.getId()) && !rule.isDisabled()) {
                    rule.setDisabled(true);
                    docStoreService.disableFolderExcludeRule(rule.getId(), true);
                    eventAuditService.auditDual(AuditType.ROOT_FOLDER, "Update root folder excluded folders", AuditAction.UPDATE,
                            RootFolderDto.class, resultRootFolderDto.getId(), FolderExcludeRuleDto.class, rule.getId(), rule);
                }
            });
        }
    }


    private void auditRootFolderCreation(RootFolderDto resultRootFolderDto, Long scheduleGroupId) {
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Create root folder ", AuditAction.IMPORT_ITEM_CREATE, RootFolderDto.class, resultRootFolderDto.getId(), resultRootFolderDto);
        ScheduleGroupDto resultScheduleGroupDto = scheduleGroupService.getRootFolderScheduleGroup(resultRootFolderDto.getId(), false);
        if (resultScheduleGroupDto != null && scheduleGroupId != null) {
            eventAuditService.auditDual(AuditType.ROOT_FOLDER, "Add schedule group for root folder", AuditAction.IMPORT_ITEM_ATTACH, RootFolderDto.class, resultRootFolderDto.getId(), ScheduleGroupDto.class, resultScheduleGroupDto.getId(), resultScheduleGroupDto);
        }
    }

    private void auditRootFolderUpdate(RootFolderDto resultRootFolderDto, Long scheduleGroupId) {
        eventAuditService.audit(AuditType.ROOT_FOLDER,"Update root folder ", AuditAction.IMPORT_ITEM_UPDATE, RootFolderDto.class, resultRootFolderDto.getId(), resultRootFolderDto);
        if(scheduleGroupId != null) {
            ScheduleGroupDto resultScheduleGroupDto = scheduleGroupService.getRootFolderScheduleGroup(resultRootFolderDto.getId(), false);
            if (resultScheduleGroupDto != null) {
                eventAuditService.auditDual(AuditType.ROOT_FOLDER, "Update schedule group for root folder", AuditAction.IMPORT_ITEM_ATTACH, RootFolderDto.class, resultRootFolderDto.getId(), ScheduleGroupDto.class, resultScheduleGroupDto.getId(), resultScheduleGroupDto);
            }
        }
    }

    @Override
    protected RootFolderDto createDtoFromRowItem(RootfolderItemRow itemRow) {
        RootFolderDto rootFolderDto = new RootFolderDto();
        rootFolderDto.setMediaType(itemRow.getMediaType());
        rootFolderDto.setPath(itemRow.getRealPath());
        rootFolderService.prepareRootFolderDtoForCreation(rootFolderDto);

        rootFolderDto.setRescanActive(itemRow.getRescanActive()!=null?itemRow.getRescanActive():false);
        rootFolderDto.setStoreLocation(itemRow.getStoreLocation());
        rootFolderDto.setStorePurpose(itemRow.getStorePurpose());
        rootFolderDto.setStoreSecurity(itemRow.getStoreSecurity());
        rootFolderDto.setDescription(itemRow.getDescription());
        rootFolderDto.setFileTypes(itemRow.getMappedFileTypes());
        rootFolderDto.setIngestFileTypes(itemRow.getIngestedFileTypes());
        rootFolderDto.setNickName(itemRow.getNickName());
        if(itemRow.getReingest() != null && itemRow.getReingest()) {
            if(itemRow.getReingestTimeInSeconds() == null || itemRow.getReingestTimeInSeconds() == 0) {
                rootFolderDto.setReingestOn();
            } else {
                rootFolderDto.setReingestTimeStampMs(itemRow.getReingestTimeInSeconds() * 1000);
            }
        } else {
            rootFolderDto.setReingestOff();
        }
        rootFolderDto.setMediaConnectionName(itemRow.getMediaConnectionName());
        if(!Strings.isNullOrEmpty(itemRow.getMediaConnectionName())) {
            MediaConnectionDetails mediaConnection = mediaConnectionDetailsService.getMediaConnectionDetails(itemRow.getMediaConnectionName());
            if(mediaConnection!=null) {
                rootFolderDto.setMediaConnectionDetailsId(mediaConnection.getId());
            }
        }
        if(!Strings.isNullOrEmpty(itemRow.getCustomerDataCenterName())) {
            CustomerDataCenterDto customerDataCenterDto = customerDataCenterService.getCustomerDataCenter(itemRow.getCustomerDataCenterName());
            if(customerDataCenterDto!=null) {
                rootFolderDto.setCustomerDataCenterDto(customerDataCenterDto);
            }
        }
        if(!Strings.isNullOrEmpty(itemRow.getDepartmentName())) {
            Long departmentId = departmentService.parseDepartmentFromPath(itemRow.getDepartmentName());
            if(departmentId != null) {
                rootFolderDto.setDepartmentId(departmentId);
            }
        }
        rootFolderDto.setScannedFilesCountCap(itemRow.getScannedLimitCount());
        rootFolderDto.setScannedMeaningfulFilesCountCap(itemRow.getMeaningfulFilesScannedLimitCount());
        rootFolderDto.setScanTaskDepth(itemRow.getScanTaskDepth());
        rootFolderDto.setMailboxGroup(itemRow.getMailboxGroup());
        rootFolderDto.setFromDateScanFilter(itemRow.getFromDateScanFilter());

        return rootFolderDto;
    }

    private Long parseScheduleGroupFromItemRow(RootfolderItemRow itemRow) {
        Long scheduleGroupId=null;
        if(itemRow.getScheduleGroupName() != null) {

            if (scheduleGroups != null) {
                for (Map.Entry<Long, String> entry : scheduleGroups.entrySet()) {
                    if (itemRow.getScheduleGroupName().equalsIgnoreCase(entry.getValue())) {
                        scheduleGroupId = entry.getKey();
                        break;
                    }
                }
            } else {
                ScheduleGroup schduleGroup = scheduleGroupService.getScheduleGroupByName(itemRow.getScheduleGroupName());
                if (schduleGroup != null) {
                    scheduleGroupId = schduleGroup.getId();
                }
            }
        }
        return scheduleGroupId;
    }

    private void createStubMediaConnectionIfNeeded(RootfolderItemRow itemRow, RootFolderDto rootFolderDto) {
        if(rootFolderDto.getMediaConnectionDetailsId()==null && rootFolderDto.getMediaType()!=null && !rootFolderDto.getMediaType().equals(MediaType.FILE_SHARE) ) {
            try {
                String connectionName = itemRow.getMediaConnectionName() !=null ? itemRow.getMediaConnectionName() : SHARE_POINT_STUB_DEFAULT_CONNECTION_NAME;
                logger.debug("Create Stub Media Connection details for: {}", connectionName);
                MediaConnectionDetails mediaConnection;
                if (!mediaConnectionDetailsService.isConnectionDetailsExists(connectionName)) {
                    mediaConnection = mediaConnectionDetailsService.createConnectionDetails(connectionName, null, null, rootFolderDto.getMediaType(), null);
                    eventAuditService.audit(AuditType.MEDIA_CONNECTION_DETAILS, "Createmedia connection details", AuditAction.IMPORT_ITEM_CREATE, MediaConnectionDetails.class, mediaConnection.getId(), mediaConnection);
                }
                else {
                    mediaConnection = mediaConnectionDetailsService.getMediaConnectionDetails(connectionName);
                }
                rootFolderDto.setMediaConnectionDetailsId(mediaConnection.getId());
            }
            catch (Exception e) {
                logger.error("Failed to create stub media connection for row ", e);
            }
        }
    }

    private void createStubCustomerDataCenterIfNeeded(RootfolderItemRow itemRow,RootFolderDto rootFolderDto) {
        if(rootFolderDto.getCustomerDataCenterDto() == null && !Strings.isNullOrEmpty(itemRow.getCustomerDataCenterName())) {
            try {
                logger.debug("Create Stub customer data center details for: {}", itemRow.getCustomerDataCenterName());

                boolean shouldCreateDataCenter = false;
                if (dataCenters != null) {
                    if (!dataCenters.values().contains(itemRow.getCustomerDataCenterName())) {
                        shouldCreateDataCenter = true;
                    }
                } else if (!customerDataCenterService.isCustomerDataCenterExists(itemRow.getCustomerDataCenterName())) {
                    shouldCreateDataCenter = true;
                }

                if (shouldCreateDataCenter) {
                    CustomerDataCenterDto customerDataCenterDto = new CustomerDataCenterDto();
                    customerDataCenterDto.setName(itemRow.getCustomerDataCenterName());
                    CustomerDataCenterDto saved = customerDataCenterService.createCustomerDataCenter(customerDataCenterDto);
                    rootFolderDto.setCustomerDataCenterDto(saved);
                    dataCenters.put(saved.getId(), saved.getName());
                    eventAuditService.audit(AuditType.CUSTOMER_DATA_CENTER, "Create customer data center", AuditAction.IMPORT_ITEM_CREATE, CustomerDataCenterDto.class, saved.getId(), saved);
                }
            }
            catch (Exception e) {
                logger.error("Failed to create stub customer data center  for row ", e);
            }
        }
    }

    private void createScheduleGroupIfNeeded(RootfolderItemRow itemRow) {
        Long scheduleGroupId =  parseScheduleGroupFromItemRow(itemRow);
        if( scheduleGroupId == null && !Strings.isNullOrEmpty(itemRow.getScheduleGroupName()) ) {
            try {

                logger.debug("Create Stub schedule group for: {}", itemRow.getScheduleGroupName());

                ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
                scheduleGroupDto.setName(itemRow.getScheduleGroupName());
                scheduleGroupDto.setSchedulingDescription("Created automatically from import");
                ScheduleConfigDto scheduleConfigDto = new ScheduleConfigDto();
                scheduleConfigDto.setEvery(1);
                scheduleConfigDto.setScheduleType(ScheduleType.DAILY);
                scheduleConfigDto.setHour(0);
                scheduleConfigDto.setMinutes(0);
                scheduleGroupDto.setScheduleConfigDto(scheduleConfigDto);
                ScheduleGroupDto saved = scheduleGroupService.createScheduleGroup(scheduleGroupDto);
                eventAuditService.audit(AuditType.SCHEDULE_GROUP, "Create schedule group",
                        AuditAction.IMPORT_ITEM_CREATE, ScheduleGroupDto.class, saved.getId(), saved);
                scheduleGroups.put(saved.getId(), saved.getName());
            }
            catch (Exception e) {
                logger.error("Failed to create stub schedule group for row ", e);
            }
        }
    }

    private void createDepartmentIfNeeded(RootfolderItemRow itemRow, RootFolderDto rootFolderDto) {
        if (rootFolderDto.getDepartmentId() == null && !Strings.isNullOrEmpty(itemRow.getDepartmentName())) {
            Long departmentId = departmentService.parseDepartmentFromPath(itemRow.getDepartmentName());
            if (departmentId == null) {
                try {
                    logger.debug("Create Stub department for: {}", itemRow.getDepartmentName());
                    DepartmentDto department = departmentService.createDepartmentFromPath(itemRow.getDepartmentName());
                    eventAuditService.audit(AuditType.DEPARTMENT, "Create department",
                            AuditAction.IMPORT_ITEM_CREATE, DepartmentDto.class, department.getId(), department);
                    rootFolderDto.setDepartmentId(department.getId());
                } catch (Exception e) {
                    logger.error("Failed to create stub department for row ", e);
                }
            } else {
                rootFolderDto.setDepartmentId(departmentId);
            }
        }
    }
}
