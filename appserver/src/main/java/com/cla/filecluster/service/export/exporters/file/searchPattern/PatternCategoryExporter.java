package com.cla.filecluster.service.export.exporters.file.searchPattern;
import com.cla.common.domain.dto.filesearchpatterns.PatternCategoryDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.extractor.pattern.PatternCategoryAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import static com.cla.filecluster.service.export.exporters.file.searchPattern.TextSearchPatternHeaderFields.*;



/**
 * Created by: ophir
 * Created on: 21/4/2019
 */
@Slf4j
@Component
public class PatternCategoryExporter extends PageCsvExcelExporter<PatternCategoryDto> {

    private final static String[] HEADER = {NAME};

    @Autowired
    private PatternCategoryAppService patternCategoryAppService;


    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
    }

    @Override
    protected Page<PatternCategoryDto> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return patternCategoryAppService.listPatternsCategories(dataSourceRequest);
    }

	@Override
	protected Map<String, Object> addExtraFields(PatternCategoryDto base, Map<String, String> requestParams) {
		Map<String, Object> result = new HashMap<>();
		result.put(NAME, resolveNamePath(base));
		return result;
	}

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    private String resolveNamePath(PatternCategoryDto basePatternCategory) {
        StringBuilder resultPc = new StringBuilder();

        if (basePatternCategory.getParentId() != null) {
            resultPc.append(getPatternCategoryById(basePatternCategory.getParentId())).append("/");
        }
        resultPc.append(basePatternCategory.getName());
        return resultPc.toString();
    }

    private String getPatternCategoryById(Integer id) {
        return Optional.ofNullable(patternCategoryAppService.getPatternCategoryById(id))
                .map(PatternCategoryDto::getName)
                .orElseThrow(() -> new RuntimeException(String.format("Error, cannot resolve pattern category  for id: %d", id)));
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.PATTERN_CATEGORY);
    }
}
