package com.cla.filecluster.service.crawler.pv;

import com.cla.common.constants.ContentMetadataFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.file.AnalyzeHint;
import com.cla.common.domain.dto.file.ContentMetadataDto;
import com.cla.common.domain.dto.pv.ExcelSimilarityParameters;
import com.cla.common.domain.dto.pv.GroupingResult;
import com.cla.filecluster.batch.writer.analyze.AnalyzeBatchResultsProcessor;
import com.cla.filecluster.batch.writer.analyze.MissedMatchHandler;
import com.cla.filecluster.domain.dto.SolrContentMetadataEntity;
import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.domain.entity.pv.MissedFileMatch;
import com.cla.filecluster.domain.entity.pv.PvAnalysisData;
import com.cla.filecluster.repository.solr.SolrAnalysisDataRepository;
import com.cla.filecluster.repository.solr.SolrGrpHelperRepository;
import com.cla.filecluster.service.crawler.analyze.ContentGroupsCache;
import com.cla.filecluster.service.extractor.GroupsService;
import com.cla.filecluster.service.files.ContentMetadataService;
import com.cla.filecluster.service.files.pv.*;
import com.cla.filecluster.service.operation.SubGroupService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class SimilarityHelper implements PvAnalysisDataFetcher, GrpHelperFetcher {

    private static final Logger logger = LoggerFactory.getLogger(SimilarityHelper.class);

    @Value("${fileGrouping.forceMaturityLevelSimilarity:false}")
    private boolean groupingForceMaturityLevelSimilarity;

    @Value("${fileGrouping.excel.minHeadingCountThreshold:4}")
    private int[] excelMinHeadingCountThreshold;
    @Value("${fileGrouping.excel.minStyleCountThreshold:3}")
    private int[] excelMinStyleCountThreshold;
    @Value("${fileGrouping.excel.minLayoutCountThreshold:3}")
    private int[] excelMinLayoutCountThreshold;
    @Value("${fileGrouping.excel.minValueCountThreshold:9}")
    private int[] excelMinValueThreshold;
    @Value("${fileGrouping.excel.minConcreteValueCountThreshold:9}")
    private int[] excelMinConcreteValueThreshold;
    @Value("${fileGrouping.excel.minFormulaCountThreshold:9}")
    private int[] excelMinFormulaThreshold;

    @Value("${fileGrouping.word.similarityContentHighThreshold:0.6,0.8,0.7,0.6}")
    private String[] wordSimilarityContentHighThreshold;

    @Value("${fileGrouping.word.similarityContentLowThreshold:0.05,0.2,0.12,0.05}")
    private String[] wordSimilarityContentLowThreshold;

    @Value("${fileGrouping.word.similarityStyleHighThreshold:0.6,0.75,0.65,0.6}")
    private String[] wordSimilarityStyleHighThreshold;

    @Value("${fileGrouping.word.similarityStyleLowThreshold:0.2,0.4,0.3,0.2}")
    private String[] wordSimilarityStyleLowThreshold;

    @Value("${fileGrouping.word.similarityLowHeadingsThreshold:0.2,0.4,0.3,0.2}")
    private String[] wordSimilarityHeadingsLowThreshold;

    @Value("${fileGrouping.word.similarityHighHeadingsThreshold:0.5,0.7,0.6,0.5}")
    private String[] wordSimilarityHeadingsHighThreshold;

    @Value("${fileGrouping.word.similarityDocstartLowThreshold:0.05,0.08,0.06,0.05}")
    private String[] wordSimilarityDocstartLowThreshold;

    @Value("${fileGrouping.word.minHeadingItems:10}")
    private int[] wordMinHeadingItems;

    @Value("${fileGrouping.word.minPLabelItems:10}")
    private int[] wordMinPLabelItems;

    @Value("${fileGrouping.word.minStyleItems:3}")
    private int[] wordMinStyleItems;

    @Value("${fileGrouping.word.minStyleSequenceItems:1}")
    private int[] wordMinStyleSequenceItems;

    @Value("${fileGrouping.word.minLetterHeadItems:3}")
    private int[] wordMinLetterHeadItems;

    @Value("${fileGrouping.excel.similarityHighContentThreshold:0.5,0.7,0.6,0.5}")
    private String[] excelSimilarityHighContentThreshold;

    @Value("${fileGrouping.excel.similarityHighStyleThreshold:0.8,0.9,0.85,0.8}")
    private String[] excelSimilarityHighStyleThreshold;

    @Value("${fileGrouping.excel.similarityHighLayoutThreshold:0.9,0.95,0.92,0.9}")
    private String[] excelSimilarityHighLayoutThreshold;

    @Value("${fileGrouping.excel.similarityHighHeadingHighSimThreshold:0.8,0.9,0.85,0.8}")
    private String[] excelSimilarityHeadingHighSimThreshold;

    @Value("${fileGrouping.excel.similarityWorkbookAggregatedSimilarityThreshold:0.6,0.8,0.7,0.6}")
    private String[] excelWorkbookAggregatedSimilarityThreshold;

    @Value("${maturity.model.singleMaturityLevel:false}")        // true = backward compatibility mode
    private boolean maturityModelSingleMaturityLevel;            // Single value per file/part or separate per content and styling / structure

    @Value("${maturity.model.contentPvNames:PV_BodyNgrams,PV_SubBodyNgrams,PV_DocstartNgrams,PV_CellValues,PV_Formulas,PV_ConcreteCellValues,PV_TextNgrams}")
    private String maturityContentPvNames;

    @Value("${maturity.low.threshodsByPV:}")
    private String maturityLowThreshodsByPV;

    @Value("${maturity.medium.threshodsByPV:}")
    private String maturityMediumThreshodsByPV;

    @Value("${fileGrouping.refinement-rules-active:true}")
    private boolean refinementRulesActive;

    @Autowired
    private ContentMetadataService contentMetadataService;

    @Autowired
    private SolrAnalysisDataRepository solrAnalysisDataRepository;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private SolrGrpHelperRepository solrGrpHelperRepository;

    @Autowired
    private SubGroupService subGroupService;

    @Autowired
    private AnalyzeBatchResultsProcessor analyzeBatchResultsProcessor;

    @Autowired
    private ContentGroupsCache contentGroupsCache;

    @Autowired
    private MissedMatchHandler missedMatchHandler;

    private int newlyGroupedCounter = 0;
    private AtomicInteger mismatchedPregroupCounter = new AtomicInteger(0);

    private SimilarityCalculator similarityCalculator;


    @PostConstruct
    private void init() {
        SimilarityCalculatorAbstractBuilder similarityCalculatorAbstractBuilder = new SimilarityCalculatorBuilder();
        similarityCalculatorAbstractBuilder.setGroupingForceMaturityLevelSimilarity(groupingForceMaturityLevelSimilarity);
        similarityCalculatorAbstractBuilder.setMaturityContentPvNames(maturityContentPvNames);
        similarityCalculatorAbstractBuilder.setMaturityModelSingleMaturityLevel(maturityModelSingleMaturityLevel);
        similarityCalculatorAbstractBuilder.setMaturityLowThreshodsByPV(maturityLowThreshodsByPV);
        similarityCalculatorAbstractBuilder.setMaturityMediumThreshodsByPV(maturityMediumThreshodsByPV);
        similarityCalculatorAbstractBuilder.setPvAnalysisDataFetcher(this);
        similarityCalculator = similarityCalculatorAbstractBuilder.init();
    }

    @Override
    public UUID getContentGroupIdUUID(long contentId) {
        String fileGroup = getContentGroupId(contentId);
        return getGroupUUID(fileGroup);
    }

    @Override
    public String getContentGroupId(long contentId) {
        return contentMetadataService.getContentGroupId(contentId);
    }


    private UUID getGroupUUID(String fileGroup) {
        return (fileGroup == null) ? null : UUID.fromString(fileGroup);
    }

    public PvAnalysisData getPvAnalysisDataByContentId(final Long docLid) {
        PvAnalysisData pvAnalysisData = solrAnalysisDataRepository.findByContentId(docLid);
        if (pvAnalysisData == null) {
            logger.error("Could not find pvAnalysisData by lid={}", docLid);
        }
        return pvAnalysisData;
    }

    @Override
    public Pair<PvAnalysisData, Integer> getPvAnalysisDataAndSizeByContentId(Long contentId) {
        return Pair.of(null, 0);
    }

    /**
     * @param currentContentGroup UUID of the group, to which the content metadata that is currently being analyzed belongs
     * @param candidateGroupId    simple ID of the group, to which the candidate content metadata belongs
     * @param contentMetadataDto  content metadata DTO
     * @param candidateContentId  ID of similarity candidate content metadata
     * @return grouping result object
     */
    public GroupingResult putContentMetadataInGroup(Long taskId,
                                                    UUID currentContentGroup,
                                                    String candidateGroupId,
                                                    final ContentMetadataDto contentMetadataDto,
                                                    final Long candidateContentId) {

        final Long currentContentId = contentMetadataDto.getContentId();

        // group ID of the content metadata that is being matched to the current content metadata
        final UUID candidateContentGroup = getGroupUUID(candidateGroupId);
        char groupingDeb;

        if (currentContentGroup == null && candidateContentGroup == null) {// new group
            //Create a new group
            final SolrFileGroupEntity fileGroup = groupsService.createNewAnalysisGroupObject(contentMetadataDto.getContentId());
            analyzeBatchResultsProcessor.collectNewFileGroups(taskId, fileGroup);
            final UUID newGroupId = UUID.fromString(fileGroup.getId());
            final boolean ok1 = put(currentContentId, newGroupId, taskId);
            final boolean ok2 = put(candidateContentId, newGroupId, taskId);
            if (!(ok1 && ok2)) {
                saveMissedFileMatch(candidateContentId, currentContentId, MissedFileMatch.Reason.REASON_UPDATE_FAILED);
            }
            return new GroupingResult('+', newGroupId);
        } else if (currentContentGroup == null) { // second!=null
            analyzeBatchResultsProcessor.collectDirtyFileGroups(taskId, candidateContentGroup.toString());
            final boolean ok = put(currentContentId, candidateContentGroup, taskId);
            if (!ok) {
                saveMissedFileMatch(candidateContentId, currentContentId, MissedFileMatch.Reason.REASON_UPDATE_FAILED);
            } else {
                return new GroupingResult('<', candidateContentGroup);
            }
            groupingDeb = '<';
        } else if (candidateContentGroup == null) {// first!=null
            analyzeBatchResultsProcessor.collectDirtyFileGroups(taskId, currentContentGroup.toString());
            final boolean ok = put(candidateContentId, currentContentGroup, taskId);
            if (!ok) {
                saveMissedFileMatch(candidateContentId, currentContentId, MissedFileMatch.Reason.REASON_UPDATE_FAILED);
            }
            groupingDeb = '>';
        } else {    // Both have group set
            if (currentContentGroup.equals(candidateContentGroup)) {    // Both are already on the same group
                groupingDeb = 'v';
            } else {    // Each has its own different group
                saveMissedFileMatch(candidateContentId, currentContentId, MissedFileMatch.Reason.REASON_PRE_MATCHED);
                mismatchedPregroupCounter.incrementAndGet();
                groupingDeb = '*';
            }
        }

        return new GroupingResult(groupingDeb);

    }

    /**
     * Set group for file both in MySQL (content_metadata) and Solr (ContentMetadata)
     *
     * @param contentId the ID of the content being assigned to the group
     * @param groupId   the ID of the group
     * @param taskId    the ID of the analysis task
     * @return true if the update is successful or false otherwise
     */
    private boolean put(final long contentId, final UUID groupId, final long taskId) {
        missedMatchHandler.acquireLock();
        try {
            String groupIdStr = groupId.toString();

            // chk if raw analysis group or if sub group and process using the rules
            if (refinementRulesActive) {
                String rawAnalysisGroupId = groupsService.getRawAnalysisGroupIdIfExist(groupIdStr);
                if (groupsService.doesAnalysisGroupHasRefinement(groupIdStr)) {
                    groupIdStr = subGroupService.getFileSubGroup(groupIdStr, contentId);
                } else if (!Strings.isNullOrEmpty(rawAnalysisGroupId)) {
                    groupIdStr = subGroupService.getFileSubGroup(rawAnalysisGroupId, contentId);
                }

                if (groupIdStr == null) {
                    return false;
                }
            }

            newlyGroupedCounter++;

            boolean updated = contentGroupsCache.setContentGroupIfEmpty(contentId, groupIdStr);
            //If updated == 0 - some other transaction already set a group to this content. Nothing to do here...
            if (updated) {
                analyzeBatchResultsProcessor.collectContentGroupChange(taskId, contentId, groupIdStr);
                // we want to update this ASAP, so that the analysis in Solr can skip those grouped candidates
                // --- update analysis group field in content metadata core
                solrGrpHelperRepository.updateAnalysisGroup(contentId, groupIdStr);
            }
            return updated;
        } finally {
            missedMatchHandler.releaseLock();
        }
    }

    private void saveMissedFileMatch(final Long doc1Id, final Long doc2Id, final MissedFileMatch.Reason reason) {
        analyzeBatchResultsProcessor.collectMissedFileMatches(new MissedFileMatch(doc1Id, doc2Id, reason));
        if (logger.isTraceEnabled()) {
            logger.trace("MissedFileMatch({},{},{})", doc1Id, doc2Id, reason.toString());
        }
    }

    public int getNewlyGroupedCounter() {
        return newlyGroupedCounter;
    }

    public int getMismatchedPregroupCounter() {
        return mismatchedPregroupCounter.get();
    }

    public ExcelSimilarityParameters[] getExcelSimParamsByMaturity() {
        return similarityCalculator.getExcelSimParamsByMaturity();
    }

    public float getWordSimilarityStyleLowThreshold(PvAnalysisData pvAnalysisData) {
        return similarityCalculator.getWordSimilarityStyleLowThreshold(pvAnalysisData);
    }


    public int getWordSimCalc2(final PvAnalysisData doc1PvAnalysisData, final StringBuilder sim2DebInfo, final Long doc2Lid, final PvAnalysisDataPool pvAnalysisDataPool) {
        return similarityCalculator.getWordSimCalc2(doc1PvAnalysisData, sim2DebInfo, doc2Lid, pvAnalysisDataPool);
    }

    @Override
    public List<PvAnalysisData> getPvAnalysisDataByContentIds(Collection<Long> contentIdList) {
        return solrAnalysisDataRepository.findByContentId(contentIdList);
    }

    @Override
    public List<Pair<PvAnalysisData, Integer>> getPvAnalysisDataByContentIdsAndSize(Collection<Long> contentIdList) {
        return Lists.newArrayList();
    }

    @Override
    public void updateStatistics(long time, int size) {

    }


    @Override
    public Map<Long, ContentGroupDetails> getContentGroupByIds(Collection<Long> contentIds) {
        if (contentIds == null || contentIds.size() == 0) {
            return Maps.newHashMap();
        }
        List<Map<SolrCoreSchema, Object>> results = contentMetadataService.findAnalysisDataByContentIds(contentIds);
        if (results == null || results.size() == 0) {
            return Maps.newHashMap();
        }
        Map<Long, ContentGroupDetails> recordMap = new HashMap<>(results.size());
        for (Map<SolrCoreSchema, Object> map : results) {
            recordMap.put(Long.valueOf((String) map.get(ContentMetadataFieldType.ID)),
                    new ContentGroupDetails((String) map.get(ContentMetadataFieldType.GROUP_ID),
                            AnalyzeHint.valueOf((String) map.get(ContentMetadataFieldType.ANALYSIS_HINT))));
        }
        return recordMap;
    }

    @Override
    public ContentGroupDetails getContentGroupDetails(long contentId) {
        Optional<SolrContentMetadataEntity> contentOp = contentMetadataService.getOneById(String.valueOf(contentId));
        if (!contentOp.isPresent()) {
            return null;
        }
        return new ContentGroupDetails(contentOp.get().getAnalysisGroupId(),
                AnalyzeHint.valueOf(contentOp.get().getAnalysisHint()));
    }

    public float workBookAggregatedSimilarityFullMatch(long contentId, Long matchedDocLid, Map<String, Float> sheetsDominanceRatio, Map<String, Float> matchedDocSheetsDominanceRatio, Map<String, String> sheetMatches, PvAnalysisData doc1PvAnalysisData, PvAnalysisData doc2PvAnalysisData, float skippedSheetsDominance) {
        return similarityCalculator.workBookAggregatedSimilarityFullMatch(contentId, matchedDocLid,
                sheetsDominanceRatio, matchedDocSheetsDominanceRatio,
                sheetMatches, doc1PvAnalysisData, doc2PvAnalysisData, skippedSheetsDominance);
    }

    class SimilarityCalculatorBuilder extends SimilarityCalculatorAbstractBuilder {

        public SimilarityCalculatorBuilder() {
            this.wordSimilarityContentHighThreshold = SimilarityHelper.this.wordSimilarityContentHighThreshold;
            this.wordSimilarityStyleLowThreshold = SimilarityHelper.this.wordSimilarityStyleLowThreshold;
            this.wordSimilarityDocstartLowThreshold = SimilarityHelper.this.wordSimilarityDocstartLowThreshold;
            this.wordMinStyleItems = SimilarityHelper.this.wordMinStyleItems;
            this.wordMinLetterHeadItems = SimilarityHelper.this.wordMinLetterHeadItems;
            this.wordMinStyleSequenceItems = SimilarityHelper.this.wordMinStyleSequenceItems;
            this.wordSimilarityContentLowThreshold = SimilarityHelper.this.wordSimilarityContentLowThreshold;
            this.wordSimilarityHeadingsLowThreshold = SimilarityHelper.this.wordSimilarityHeadingsLowThreshold;
            this.wordSimilarityStyleHighThreshold = SimilarityHelper.this.wordSimilarityStyleHighThreshold;
            this.wordSimilarityHeadingsHighThreshold = SimilarityHelper.this.wordSimilarityHeadingsHighThreshold;
            this.wordMinHeadingItems = SimilarityHelper.this.wordMinHeadingItems;
            this.wordMinPLabelItems = SimilarityHelper.this.wordMinPLabelItems;

            this.excelMinHeadingCountThreshold = SimilarityHelper.this.excelMinHeadingCountThreshold;
            this.excelMinLayoutCountThreshold = SimilarityHelper.this.excelMinLayoutCountThreshold;
            this.excelMinFormulaThreshold = SimilarityHelper.this.excelMinFormulaThreshold;
            this.excelMinStyleCountThreshold = SimilarityHelper.this.excelMinStyleCountThreshold;
            this.excelMinConcreteValueThreshold = SimilarityHelper.this.excelMinConcreteValueThreshold;
            this.excelSimilarityHighContentThreshold = SimilarityHelper.this.excelSimilarityHighContentThreshold;
            this.excelMinValueThreshold = SimilarityHelper.this.excelMinValueThreshold;
            this.excelSimilarityHighStyleThreshold = SimilarityHelper.this.excelSimilarityHighStyleThreshold;
            this.excelSimilarityHeadingHighSimThreshold = SimilarityHelper.this.excelSimilarityHeadingHighSimThreshold;
            this.excelSimilarityHighLayoutThreshold = SimilarityHelper.this.excelSimilarityHighLayoutThreshold;
            this.excelWorkbookAggregatedSimilarityThreshold = SimilarityHelper.this.excelWorkbookAggregatedSimilarityThreshold;

        }
    }
}
