package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.connector.utils.TimeSource;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Strategy that allows only one type of tasks (INGEST OR ANALYZE) to be executed at hte same time
 */
public abstract class ExclusiveFairStrategy extends FairTaskProducingStrategy{

    private final static Logger logger = LoggerFactory.getLogger(ExclusiveFairStrategy.class);

    protected TimeSource timeSource = new TimeSource();
    long modeChangeTime = 0;


    ExclusiveFairStrategy(JobManagerService jobManager, SolrTaskService solrTaskService, TaskProducingStrategyConfig config) {
        super(jobManager, solrTaskService, config);
    }

    /**
     * Whether there are tasks that are still in the queue and should be consumed before we can start the next phase
     * @param currentExclusiveMode current mode (INGEST or ANALYZE)
     * @return if true - should wait for tasks of the previous mode to finish
     */
    boolean shouldCooldown(JobType currentExclusiveMode){
        TaskType otherTaskType = taskTypeByMode(otherMode(currentExclusiveMode));
        Table<Long, TaskState, Number> otherTaskStatesByType = otherTaskType.equals(JobType.INGEST) ?
                solrTaskService.getIngestTaskByRunAndStateNum(jobManager.getRunsForRunningJobsByType(JobType.INGEST))
                : solrTaskService.getAnalysisTaskByRunAndStateNum(jobManager.getRunsForRunningJobsByType(JobType.ANALYZE));
        int enqueuedTasksNum = otherTaskStatesByType.column(TaskState.ENQUEUED).values().stream().mapToInt(Number::intValue).sum();
        int consumingTasksNum = otherTaskStatesByType.column(TaskState.CONSUMING).values().stream().mapToInt(Number::intValue).sum();
        boolean shouldCoolDown = enqueuedTasksNum + consumingTasksNum > config.getCooldownTasksThreshold();
        if (shouldCoolDown){
            logger.debug("Cooling down, previous mode: {}, current mode: {}, enqueued tasks: {}, consuming: {}, threshold: {}",
                    otherTaskType.name(), currentExclusiveMode.name(), enqueuedTasksNum, consumingTasksNum, config.getCooldownTasksThreshold());
        }
        return shouldCoolDown;
    }

    private TaskType taskTypeByMode(JobType mode){
        switch(mode){
            case INGEST:
                return TaskType.INGEST_FILE;
            case ANALYZE:
                return TaskType.ANALYZE_CONTENT;
            default:
                throw new RuntimeException("Unsupported mode " + mode);
        }
    }

    private JobType otherMode(JobType currentMode){
        if (JobType.INGEST == currentMode){
            return JobType.ANALYZE;
        }
        else{
            return JobType.INGEST;
        }
    }

    public void setModeChangeTime(long modeChangeTime) {
        this.modeChangeTime = modeChangeTime;
    }

    boolean compatibleMode(JobType jobType, TaskType taskType){
        return taskTypeByMode(jobType) == taskType;
    }
}
