package com.cla.filecluster.service.operation;

import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * apply refinement rules on a group
 *
 * Created by: yael
 * Created on: 5/31/2018
 */
@Service
public class RefinementRulesApplyGroupOperation implements ScheduledOperation {

    private static final Logger logger = LoggerFactory.getLogger(RefinementRulesApplyGroupOperation.class);

    public static String RAW_ANALYSIS_GROUP_PARAM = "RAW_ANALYSIS_GROUP";

    @Autowired
    private SubGroupService subGroupService;

    @Autowired
    private GroupLockService groupLockService;

    @Autowired
    private MessageHandler messageHandler;

    @Override
    public int getTotalStepNum() {
        return 3;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String groupId = opData.get(RAW_ANALYSIS_GROUP_PARAM);
        if (Strings.isNullOrEmpty(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Override
    public List<String> getRelevantGroupIds(Map<String, String> opData) {
        List<String> result = new ArrayList<>();
        String groupId = opData.get(RAW_ANALYSIS_GROUP_PARAM);
        result.add(groupId);
        return result;
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.REFINEMENT_RULE_APPLY;
    }

    @Override
    public void preOperation(Map<String, String> opData) {
        String groupId = opData.get(RAW_ANALYSIS_GROUP_PARAM);

        if (!groupLockService.getLockForRawAnalysisGroup(groupId)) {
            logger.info("cant get lock to apply refinement rules to raw analysis group id {}", groupId);
            throw new GroupLockAcqException();
        }
    }

    @Override
    public void postOperation(Map<String, String> opData) {
        String groupId = opData.get(RAW_ANALYSIS_GROUP_PARAM);
        groupLockService.releaseLockForRawAnalysisGroup(groupId);
    }

    @Override
    @AutoAuthenticate
    public boolean runStep(int step, Map<String, String> opData) {
        String groupId = opData.get(RAW_ANALYSIS_GROUP_PARAM);
        if (step == 0) {
            return subGroupService.firstStepMoveAllBackToParent(groupId);
        } else if (step == 1) {
            return subGroupService.sndStepManageRules(groupId);
        } else if (step == 2) {
            return subGroupService.trdStepMoveFilesToSubGroups(groupId);
        }
        return true;
    }
}
