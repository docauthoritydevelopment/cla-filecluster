package com.cla.filecluster.service;

import org.springframework.data.domain.PageRequest;

/**
 * Created by uri on 08/02/2016.
 */
public class DeepPageRequest extends PageRequest {

    String deepPageMarker;

    public DeepPageRequest(int size) {
        super(0, size);
        this.deepPageMarker = "*";
    }

    public DeepPageRequest(int size, String deepPageMarker) {
        super(0, size);
        this.deepPageMarker = deepPageMarker;
    }

    public String getDeepPageMarker() {
        return deepPageMarker;
    }

    public void setDeepPageMarker(String deepPageMarker) {
        this.deepPageMarker = deepPageMarker;
    }
}
