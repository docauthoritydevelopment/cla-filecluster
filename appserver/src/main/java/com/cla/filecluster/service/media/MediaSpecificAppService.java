package com.cla.filecluster.service.media;

import com.cla.common.domain.dto.messages.FoldersListResultMessage;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.AbstractMediaConnector;
import com.cla.connector.mediaconnector.MediaConnector;
import com.cla.filecluster.service.util.SyncRequestLockMap;

import java.util.List;
import java.util.Optional;

/**
 * App service to support specific type of media, e.g. file server, share point etc.
 * Created by vladi on 2/2/2017.
 */
public interface MediaSpecificAppService<T extends MediaConnector, S extends MediaConnectionDetailsDto> {

    SyncRequestLockMap<FoldersListResultMessage> requestLockMap = new SyncRequestLockMap<>();

    List<ServerResourceDto> listFoldersUnderPath(MediaConnectionDetailsDto connectionDetails,
                                                 Optional<String> path, Long customerDataCenterId, boolean isTest);
}
