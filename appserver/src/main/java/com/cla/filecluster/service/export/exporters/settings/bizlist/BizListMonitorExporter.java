package com.cla.filecluster.service.export.exporters.settings.bizlist;

import com.cla.common.domain.dto.bizlist.BizListDto;
import com.cla.common.domain.dto.bizlist.BizListSummaryInfo;
import com.cla.common.domain.dto.filetree.CrawlRunDetailsDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.service.entitylist.BizListAppService;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.export.exporters.settings.bizlist.mixin.BizListDtoMixin;
import com.cla.filecluster.service.export.exporters.settings.bizlist.mixin.BizListSummaryInfoMixin;
import com.cla.filecluster.service.export.exporters.settings.bizlist.mixin.CrawlRunDetailsDtoMixin;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.settings.bizlist.BizListHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
@Slf4j
@Component
public class BizListMonitorExporter extends PageCsvExcelExporter<BizListSummaryInfo> {

    @Autowired
    private BizListAppService bizListAppService;

    private final static String[] header = {NAME, DESCRIPTION, LAST_FULL_RUN, TYPE, STATUS, ACTIVE};

    @Override
    protected String[] getHeader() {
        return header;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(BizListSummaryInfo.class, BizListSummaryInfoMixin.class)
        .addMixIn(BizListDto.class, BizListDtoMixin.class)
        .addMixIn(CrawlRunDetailsDto.class, CrawlRunDetailsDtoMixin.class);
    }

    @Override
    protected Iterable<BizListSummaryInfo> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        return bizListAppService.listBizListSummary(dataSourceRequest);
    }

    @Override
    protected Map<String, Object> addExtraFields(BizListSummaryInfo base, Map<String, String> requestParams) {
        Map<String, Object> res = new HashMap<>();

        if (base != null && base.getCrawlRunDetailsDto() != null) {
            res.put(STATUS, base.getCrawlRunDetailsDto().getRunOutcomeState().getReadableValue());
        }

        return res;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.MULTI;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.BIZ_LIST_MONITOR);
    }
}
