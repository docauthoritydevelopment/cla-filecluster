package com.cla.filecluster.service.export.exporters.root_folders;

import com.cla.filecluster.service.export.exporters.ScannedItemsHeaderFields;

public interface RootFoldersHeaderFields extends ScannedItemsHeaderFields {
    String PATH = "PATH";
    String MEDIA_TYPE = "MEDIA TYPE";
    String NICK_NAME = "NICK NAME";
    String MAPPED_FILE_TYPES = "MAPPED FILE TYPES";
    String INGESTED_FILE_TYPES = "INGESTED FILE TYPES";
    String DATA_CENTER = "CUSTOMER DATA CENTER";
    String RESCAN = "RESCAN";
    String REINGEST = "REINGEST";
    String REINGEST_TIME = "REINGEST TIME IN SECONDS";
    String STORE_LOCATION = "STORE LOCATION";
    String STORE_PURPOSE = "STORE PURPOSE";
    String STORE_SECURITY = "STORE SECURITY";
    String DESCRIPTION = "DESCRIPTION";
    String MEDIA_CONNECTION_NAME = "MEDIA CONNECTION NAME";
    String SCHEDULE_GROUP_NAME = "SCHEDULE GROUP NAME";
    String EXTRACT_BUSINESS_LISTS = "EXTRACT BUSINESS LISTS";
    String MAILBOX_GROUP = "MAILBOX GROUP";
    String FROM_DATE_SCAN_FILTER = "FROM DATE SCAN FILTER";
    String SCAN_TASK_DEPTH = "SCAN TASK DEPTH";
    String STATUS = "Status";
    String NUM_OF_EXCLUDED_FOLDERS_RULES = "Number of Excluded folders rules";
    String NUM_OF_FOUND_FOLDERS = "Number of found folders";
    String ACCESSIBILITY = "Accessibility";
    String NUM_OF_PROCESSED_FILES = "Number of processed files";
    String PDF_PROCESSED = "Pdf processed";
    String WORD_PROCESSED = "MS-Word processed";
    String EXCEL_PROCESSED = "MS-Excel processed";
    String OTHER_PROCESSED = "Other processed";
    String SCAN_ERRORS = "Scan errors";
    String PROCESSING_ERRORS = "Processing errors";
    String SCANNED_LIMIT_COUNT = "SCANNED LIMIT COUNT";
    String MEANINGFUL_SCANNED_LIMIT_COUNT = "MEANINGFUL FILES SCANNED LIMIT COUNT";
    String EXCLUDED_FOLDERS_RULES = "EXCLUDED FOLDERS RULES";
}
