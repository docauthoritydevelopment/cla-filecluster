package com.cla.filecluster.service.export.exporters.active_scans.mixin;

import com.cla.common.domain.dto.filetree.StoreLocation;
import com.cla.common.domain.dto.filetree.StorePurpose;
import com.cla.common.domain.dto.filetree.StoreSecurity;
import com.cla.connector.domain.dto.file.FileType;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.service.export.serialize.ToSecondsSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.active_scans.ActiveScansHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public abstract class RootFolderDtoMixIn {

    @JsonProperty(PATH)
    private String realPath;

    @JsonProperty(NICK_NAME)
    private String nickName;

    @JsonProperty(DESCRIPTION)
    private String description;

    @JsonProperty(STORE_LOCATION)
    private StoreLocation storeLocation;

    @JsonProperty(STORE_PURPOSE)
    private StorePurpose storePurpose;

    @JsonProperty(STORE_SECURITY)
    private StoreSecurity storeSecurity;

    @JsonProperty(RESCAN)
    private boolean rescanActive;

    @JsonProperty(MAPPED_FILE_TYPES)
    private FileType[] fileTypes;

    @JsonProperty(INGESTED_FILE_TYPES)
    private FileType[] ingestFileTypes;

    @JsonProperty(MEDIA_TYPE)
    private MediaType mediaType;

    @JsonProperty(MEDIA_CONNECTION)
    private String mediaConnectionName;

    @JsonSerialize(using = ToSecondsSerializer.class)
    @JsonProperty(REINGEST_TIME)
    private long reingestTimeStampMs;

    @JsonProperty(REINGEST)
    public abstract boolean isReingest();

    @JsonProperty(SCAN_TASK_DEPTH)
    private Integer scanTaskDepth;
}
