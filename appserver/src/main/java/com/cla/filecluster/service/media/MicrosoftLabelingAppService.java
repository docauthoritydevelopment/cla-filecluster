package com.cla.filecluster.service.media;

import com.cla.common.domain.dto.messages.FoldersListResultMessage;
import com.cla.common.domain.dto.messages.IngestError;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MIPConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.labeling.mip.MipMediaConnector;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by: yael
 * Created on: 7/4/2019
 */
@Service
public class MicrosoftLabelingAppService implements MediaSpecificAppService<MipMediaConnector, MIPConnectionDetailsDto> {

    private final static Logger logger = LoggerFactory.getLogger(MicrosoftLabelingAppService.class);

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private RemoteMediaProcessingService remoteMediaProcessingService;

    @Autowired
    private CustomerDataCenterService customerDataCenterService;

    @Autowired
    private MediaConnectionDetailsService connectionDetailsService;

    public void handleOauthResponse(Long currentUserId, String code, boolean b) {
    }

    public void fillAuthentication(String state) {
    }

    public String buildAuthorizeUrl(String sessionId) {
        return null;
    }

    @Override
    public List<ServerResourceDto> listFoldersUnderPath(MediaConnectionDetailsDto connectionDetails,
                                                        Optional<String> path, Long customerDataCenterId, boolean isTest) {
        CustomerDataCenter customerDataCenterById = customerDataCenterService.getCustomerDataCenterById(customerDataCenterId);
        if( customerDataCenterById == null){
            throw new BadRequestException(messageHandler.getMessage("data-center.missing"), BadRequestType.ITEM_NOT_FOUND );
        }

        MIPConnectionDetailsDto mipConnectionDetailsDto = (MIPConnectionDetailsDto) connectionDetails;
        MIPConnectionDetailsDto newMipConnectionDetailsDto = new MIPConnectionDetailsDto();
        BeanUtils.copyProperties(mipConnectionDetailsDto, newMipConnectionDetailsDto);
        newMipConnectionDetailsDto.setConnectionParametersJson(connectionDetailsService.encrypt(mipConnectionDetailsDto.getConnectionParametersJson()));
        newMipConnectionDetailsDto.getMIPConnectionParametersDto().setPassword(
                connectionDetailsService.encrypt(newMipConnectionDetailsDto.getMIPConnectionParametersDto().getPassword()));

        CompletableFuture<FoldersListResultMessage> syncLockFuture = new CompletableFuture<>();
        String requestId = requestLockMap.addRequestToLockMap(syncLockFuture);
        remoteMediaProcessingService.issueListFoldersRequest(requestId, path.orElse(""), customerDataCenterId, newMipConnectionDetailsDto, isTest);
        try {
            FoldersListResultMessage foldersListResultMessage = syncLockFuture.get(30, TimeUnit.SECONDS);
            if(!foldersListResultMessage.getErrors().isEmpty()){
                IngestError firstError = foldersListResultMessage.getErrors().get(0);
                throw new BadRequestException(messageHandler.getMessage("list-folders.sub-folders-failure",
                        Lists.newArrayList(path.orElse("root"), firstError.getExceptionText())),
                        firstError.getErrorType() == ProcessingErrorType.ERR_NONE ? BadRequestType.ITEM_NOT_FOUND : BadRequestType.OTHER);
            }
            List<ServerResourceDto> serverResourceDtos = foldersListResultMessage.getFoldersList();
            return serverResourceDtos;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            logger.error("error while waiting for folders listing", e);
            throw new BadRequestException(messageHandler.getMessage("list-folders.failure", Lists.newArrayList(e.getMessage())), BadRequestType.OTHER);
        }
    }
}
