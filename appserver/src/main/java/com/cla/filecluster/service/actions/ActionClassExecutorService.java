package com.cla.filecluster.service.actions;

import com.cla.filecluster.actions.base.ActionHelper;
import com.cla.filecluster.actions.base.DocAuthorityActionClassExecutor;
import com.cla.common.domain.dto.action.ActionClass;
import com.cla.filecluster.domain.entity.actions.ActionClassExecutorDefinition;
import com.cla.filecluster.repository.jpa.actions.ActionClassExecutorDefinitionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ActionClassExecutorService {

    static final Logger logger = LoggerFactory.getLogger(ActionClassExecutorService.class);

    @Autowired
    private ActionClassExecutorDefinitionRepository actionClassExecutorDefinitionRepository;

    @Transactional
    public ActionClassExecutorDefinition createActionClassExecutorDefinition(String id, String name, String description, ActionClass actionClass, String javaClassName) {
        logger.debug("Create action class executor {}",id);
        ActionClassExecutorDefinition actionClassExecutorDefinition = new ActionClassExecutorDefinition();
        actionClassExecutorDefinition.setId(id);
        actionClassExecutorDefinition.setName(name);
        actionClassExecutorDefinition.setDescription(description);
        actionClassExecutorDefinition.setActionClass(actionClass);
        actionClassExecutorDefinition.setJavaClassName(javaClassName);
        return actionClassExecutorDefinitionRepository.save(actionClassExecutorDefinition);
    }

    public ActionClassExecutorDefinition getDefaultActionClassExecutorDefinition(ActionClass actionClass) {
        logger.debug("Get Default action class executor for actionClass {}",actionClass);
        List<ActionClassExecutorDefinition> byActionClass = actionClassExecutorDefinitionRepository.findByActionClass(actionClass);
        if (byActionClass.size() >0) {
            return byActionClass.get(0);
        }
        logger.trace("No Action Class Executor for actionClass {}",actionClass);
        return null;
    }

    public DocAuthorityActionClassExecutor createActionClassExecutorInstance(ActionClass actionClass, ActionHelper actionHelper) {
        logger.debug("Create ActionClassExecutor for actionClass {}",actionClass);
        ActionClassExecutorDefinition defaultActionClassExecutorDefinition = getDefaultActionClassExecutorDefinition(actionClass);
        if (defaultActionClassExecutorDefinition == null) {
            return null;
        }
        else {
            String javaClassName = defaultActionClassExecutorDefinition.getJavaClassName();
            logger.debug("Create instance of class {}",javaClassName);
            try {
                Object instance = Class.forName(javaClassName).newInstance();
                DocAuthorityActionClassExecutor castInstance = (DocAuthorityActionClassExecutor) instance;
                logger.debug("Created executor instance - initializing");
                castInstance.init(actionHelper);
                logger.debug("executor instance - initialized successfully");
                return castInstance;
            } catch (Exception e) {
                logger.error("Failed to create class by name "+javaClassName+". Got error: "+e.getMessage(),e);
                throw new RuntimeException("Failed to create class by name "+javaClassName+". Got error: "+e.getMessage(),e);
            }
        }
    }

}
