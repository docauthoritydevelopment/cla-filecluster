package com.cla.filecluster.service.export.retrievers;

import com.google.common.collect.Iterables;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;


@Slf4j
public class DeepPageRetriever<T> extends MultiPageRetriever<T> {

    public DeepPageRetriever(Map<String, String> requestParams, int pageSize, Function<Map<String, String>, Iterable<T>> pageRetriever) {
        super(requestParams, pageSize, pageRetriever);
    }

    @Override
    public Flux<Iterable<T>> createFlux() {
        return Mono.zip(
                ReactiveSecurityContextHolder.getContext(),
                Mono.defer(() -> Mono.just(getPageParams()))
                )
                .flatMap(tuple -> {
                    log.trace("Setting up security context: {}", tuple.getT1().getAuthentication());
                    SecurityContextHolder.getContext().setAuthentication(tuple.getT1().getAuthentication());
                    Iterable<T> iterable = tuple.getT2().map(pageRetriever).orElse(new ArrayList<>());
                    tuple.getT2().ifPresent(this::setParams);
                    return Mono.just(iterable);
                })
                .repeat()
                .takeUntil(Iterables::isEmpty);
    }
}
