package com.cla.filecluster.service.security;

import com.cla.common.domain.dto.security.CompositeRoleDto;
import com.cla.common.domain.dto.security.LdapGroupMappingDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.entity.security.BizRole;
import com.cla.filecluster.domain.entity.security.CompositeRole;
import com.cla.filecluster.domain.entity.security.FunctionalRole;
import com.cla.filecluster.domain.entity.security.LdapGroupMapping;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.security.LdapGroupMappingRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Itai Marko.
 */
@Service
public class LdapGroupMappingService {

    private static final Logger logger = LoggerFactory.getLogger(LdapGroupMappingService.class);

    @Autowired
    private LdapGroupMappingRepository ldapGroupMappingRepository;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional(readOnly = true)
    public Set<CompositeRole> getGroupCompositeRoles(String groupName) {
        LdapGroupMapping ldapGroupMapping = ldapGroupMappingRepository.findByGroupName(groupName);
        if (ldapGroupMapping == null) {
            return null;
        }
        return ldapGroupMapping.getDaRoles();
    }

    @SuppressWarnings("unused") // TODO Itai: Can we delete this unused method?
    @Transactional(readOnly = true)
    public Set<CompositeRoleDto> getGroupCompositeRoles(Set<String> groupNames) {
        if (groupNames == null || groupNames.isEmpty()){
            return Sets.newHashSet();
        }
        Set<LdapGroupMapping> ldapGroupMappings = ldapGroupMappingRepository.findByGroupNames(groupNames);
        if (ldapGroupMappings == null || ldapGroupMappings.isEmpty()) {
            return Sets.newHashSet();
        }
        return ldapGroupMappings.stream()
                .flatMap(mapping -> mapping.getDaRoles().stream())
                .map(LdapGroupMappingService::convertCompositeRole)
                .collect(Collectors.toSet());
    }

    public static CompositeRoleDto convertCompositeRole(CompositeRole daRole) {
        if (daRole instanceof BizRole) {
            return BizRoleService.convertBizRole((BizRole) daRole);
        } else if (daRole instanceof FunctionalRole) {
            return FunctionalRoleService.convertFunctionalRole((FunctionalRole) daRole);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public List<LdapGroupMappingDto> getMappingsByRoleId(Long daRoleId) {
        List<LdapGroupMapping> ldapGroupMappings = ldapGroupMappingRepository.findByDaRoleId(daRoleId);
        if (ldapGroupMappings == null) {
            return null;
        } else {
            return ldapGroupMappings.stream()
                    .map(LdapGroupMappingService::convertLdapGroupMapping)
                    .collect(Collectors.toList());
        }
    }

    @Transactional(readOnly = true)
    public List<LdapGroupMappingDto> getAllGroupMappings() { //TODO Liora : Not good for bizRoles mappings
        List<LdapGroupMapping> ldapGroupMappings = ldapGroupMappingRepository.findAll();
        return ldapGroupMappings.stream()
                .map(LdapGroupMappingService::convertLdapGroupMapping)
                .collect(Collectors.toList());
    }

    @Transactional
    public  List<LdapGroupMappingDto> saveGroupMappingsForRole(CompositeRole funcRole, Set<LdapGroupMappingDto> ldapGroupMappings) {
        deleteAllGroupMappingsForDaRole(funcRole.getId());
        if(ldapGroupMappings.size()>0) {
            return ldapGroupMappings.stream()
                    .map(mapping -> doSaveGroupMapping(mapping, Sets.newHashSet(funcRole)))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    @Transactional
    public void deleteAllGroupMappingsForDaRole(Long daRoleId)
    {
        List<LdapGroupMapping> mappings = ldapGroupMappingRepository.findByDaRoleId(daRoleId);
        if (mappings != null) {
            logger.debug("Delete ldap mappings for daRoleId {} found {} mappings to delete", daRoleId, mappings.size());
            mappings.forEach(mapping -> {
                if (mapping.getDaRoles().size() == 1) {
                    ldapGroupMappingRepository.deleteById(mapping.getId()); //Last item, remove the mapping
                } else {
                    mapping.getDaRoles().removeIf(r -> r.getId().equals(daRoleId));
                    ldapGroupMappingRepository.save(mapping);
                }
            });
        }
    }


    @Transactional
    private LdapGroupMappingDto doSaveGroupMapping(LdapGroupMappingDto mappingDto, Set<? extends CompositeRole> daRoles) {
        logger.debug("Saving mapping for roles {} group", daRoles, mappingDto.getGroupName());
        if(Strings.isNullOrEmpty(mappingDto.getGroupName())) {
            throw new BadRequestException(messageHandler.getMessage("ldap-group-mapping.group-name.empty"), BadRequestType.MISSING_FIELD);
        }
        LdapGroupMapping ldapGroupMapping = ldapGroupMappingRepository.findByGroupName(mappingDto.getGroupName());
        if (ldapGroupMapping == null) {
            ldapGroupMapping = new LdapGroupMapping();
            ldapGroupMapping.setGroupName(mappingDto.getGroupName());
            ldapGroupMapping.setGroupDN(mappingDto.getGroupDN());
        }
        ldapGroupMapping.getDaRoles().addAll(daRoles);
        LdapGroupMapping saved = ldapGroupMappingRepository.save(ldapGroupMapping);
        return convertLdapGroupMapping(saved);
    }

    public static LdapGroupMappingDto convertLdapGroupMapping(LdapGroupMapping ldapGroupMapping) {
        LdapGroupMappingDto ldapGroupMappingDto = new LdapGroupMappingDto();
        ldapGroupMappingDto.setId(ldapGroupMapping.getId());
        ldapGroupMappingDto.setGroupName(ldapGroupMapping.getGroupName());
        ldapGroupMappingDto.setGroupDN(ldapGroupMapping.getGroupDN());
        Set<Long> bizRolesIds = ldapGroupMapping.getDaRoles().stream().map(CompositeRole::getId).collect(Collectors.toSet());
        ldapGroupMappingDto.setDaRolesIds(bizRolesIds);
        return ldapGroupMappingDto;
    }

}
