package com.cla.filecluster.service.crawler.analyze;

import com.cla.filecluster.domain.entity.pv.GroupUnificationHelper;
import com.cla.filecluster.repository.jpa.GroupUnificationHelperRepository;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.ConcurrentMap;


/**
 * Manage file contents that failed (for technical reasons) to be grouped together by the similarity algorithm. <br/>
 * Such contents should either create a new group together (if both ungrouped), join the other content's group <br/>
 * (if first is ungrouped and the second has a group) or force two groups to be merged (if both belong to different groups).<br/>
 * <br/>
 * Created by vladi on 6/25/2017.
 */
@Service
public class MergingGroupsManager {

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
    private GroupUnificationHelperRepository repository;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    // map of merging groups (source group UUID -> target group UUID)
    private ConcurrentMap<String,String> mergingGroups = Maps.newConcurrentMap();

    /**
     * Load all the unprocessed pending merges into the cache
     */
    @PostConstruct
    public void init(){
        Runnable r = () -> {
            List <GroupUnificationHelper> allUnprocessed = repository.findAllUnprocessed();
            allUnprocessed.forEach(entity ->
                    mergingGroups.put(entity.getOrigGroupId(), entity.getNewGroupId()));
        };
        DBTemplateUtils.doInTransactionReadOnly(r);
    }

    @Transactional
    public synchronized void mergeGroups(String sourceGroupUUID, String targetGroupUUID){
        String target = mergingGroups.get(sourceGroupUUID);
        mergingGroups.put(sourceGroupUUID, targetGroupUUID);
    }

    public String getEffectiveGroupUUID(String originalUUID){
        String effectiveUUID = mergingGroups.get(originalUUID);
        return effectiveUUID != null ? effectiveUUID : originalUUID;
    }



}
