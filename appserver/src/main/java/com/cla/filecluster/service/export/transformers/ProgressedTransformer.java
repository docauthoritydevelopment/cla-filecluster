package com.cla.filecluster.service.export.transformers;

import com.cla.filecluster.service.progress.ProgressService;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.function.Function;

public abstract class ProgressedTransformer<V, T> implements Transformer<V, T> {

    private final String exportId;
    private final ProgressService.Handler progress;

    public ProgressedTransformer(String exportId, ProgressService.Handler progress) {
        this.progress = progress;
        this.exportId = exportId;
    }

    public String getExportId() {
        return exportId;
    }

    public ProgressService.Handler getProgress() {
        return progress;
    }

    @Override
    public Function<Flux<V>, Publisher<DataBuffer>> asDataBuffer() {
        return (flux) ->
                flux.compose(this) //calls apply (Transformer extends Function)
                        .map(this::createDataBuffer);
    }

    protected abstract DataBuffer createDataBuffer(T t);
}
