package com.cla.filecluster.service.doctype;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.entity.bizlist.BizList;
import com.cla.filecluster.domain.entity.doctype.DocType;
import com.cla.filecluster.domain.entity.doctype.DocTypeRiskInformation;
import com.cla.filecluster.domain.entity.doctype.DocTypeTemplate;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagSetting;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.doctype.DocTypeRepository;
import com.cla.filecluster.repository.jpa.doctype.DocTypeRiskInformationRepository;
import com.cla.filecluster.repository.jpa.doctype.DocTypeTemplateRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationUtils;
import com.cla.filecluster.service.entitylist.BizListService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTagSettingService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.cla.filecluster.service.operation.RemoveDocTypeThatMoved;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.scope.ScopeService;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Table;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by uri on 19/08/2015.
 */
@Service
public class DocTypeService {

    private static final String PARENT = "Parent";
    public static final String NAME = "Name";
    private static final String UNIQUE_NAME = "Unique_Name";
    private static final String ALTERNATIVE_NAME = "Alternative_Name";
    private static final String RISK_INFORMATION_NAME = "Risk Information Name";
    private static final String ENTITY_LIST_NAME = "Entity List Name";
    private static final String SINGLE_VALUE = "Single Value";
    private static final String ASSIGNED_TAGS = "Assigned Tags";
    private final static String[] requiredHeaders = {NAME, PARENT, RISK_INFORMATION_NAME, ENTITY_LIST_NAME};

    private Logger logger = LoggerFactory.getLogger(DocTypeService.class);

    @Autowired
    private DocTypeRepository docTypeRepository;

    @Autowired
    private DocTypeTemplateRepository docTypeTemplateRepository;

    @Autowired
    private DocTypeRiskInformationRepository docTypeRiskInformationRepository;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @Autowired
    private BizListService bizListService;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private CsvReader csvReader;

    @Value("${doctype.template.file:}")
    private String defaultDocTypeTemplateFilePath;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    private LoadingCache<Long, Optional<DocTypeDto>> docTypeCache = null;

    @PostConstruct
    void init() {
        docTypeCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<Long, Optional<DocTypeDto>>() {
                            public Optional<DocTypeDto> load(Long key) {
                                return dbTemplateUtils.doInTransaction(() -> {
                                    DocType docType = getDocTypeById(key);
                                    return Optional.ofNullable(convertDocTypeToDto(docType, docType.getScope()));
                                });
                            }
                        });

        dbTemplateUtils.doInTransaction(() -> {
            docTypeRepository.findAll().forEach(docType -> {
                docTypeCache.put(docType.getId(), Optional.ofNullable(convertDocTypeToDto(docType, docType.getScope())));
            });
        });
    }

    public DocTypeDto getFromCache(Long docTypeId) {
        DocTypeDto dto = docTypeCache.getUnchecked(docTypeId).orElse(null);
        return dto == null ? null : new DocTypeDto(dto);
    }

    public DocTypeDto getFromCache(Long docTypeId, BasicScope scope) {
        DocTypeDto dto = docTypeCache.getUnchecked(docTypeId).orElse(null);
        dto = (dto == null ? null : new DocTypeDto(dto));
        if (scope != null) {
            dto.setScope(AssociationUtils.convertBasicScopeToDto(scope));
        }
        return dto;
    }

    public void invalidateCache(Long docTypeId) {
        docTypeCache.invalidate(docTypeId);
    }

    @Transactional
    public void loadDefaultDocTypeTemplate() {
        if (defaultDocTypeTemplateFilePath == null || defaultDocTypeTemplateFilePath.isEmpty()) {
            logger.info("No default docType template file defined - ignored");
        } else {
            logger.info("Load default docType template from file {}", defaultDocTypeTemplateFilePath);
            createDocTypeTemplateFromFile(defaultDocTypeTemplateFilePath);
        }
    }

    @Transactional
    public DocTypeTemplate createDocTypeTemplateFromFile(String templateFileName) {
        logger.info("Load DocType template {} into the database", templateFileName);

        final DocTypeTemplate docTypeTemplate = CreateDocTypeTemplateIfNeeded(templateFileName);
        createDefaultRiskInformation();
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, templateFileName, requiredHeaders);
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(templateFileName, row -> createDocTypeFromCsvRow(row, headersLocationMap, docTypeTemplate), null, null);
        } catch (Exception e) {
            logger.error("Failed to read docType template. Got exception " + e.getMessage(), e);
            throw new RuntimeException("Failed to read docType template. Got exception " + e.getMessage(), e);
        }
        if (failedRows != 0) {
            logger.error("Failed to read {} docType lines", failedRows);
        }
        return docTypeTemplate;
    }

    private void createDefaultRiskInformation() {
        ArrayList<String> defaults = Lists.newArrayList("default", "low", "medium", "high");
        defaults.forEach(value -> {
            try {
                DocTypeRiskInformation docTypeRiskInformation = docTypeRiskInformationRepository.findByName(value);
                if (docTypeRiskInformation == null) {
                    docTypeRiskInformation = new DocTypeRiskInformation(value);
                    docTypeRiskInformationRepository.save(docTypeRiskInformation);
                }
            } catch (Exception e) {
                logger.debug("Error creating docTypeRiskInformation {}. Error: {}", value, e.getMessage());
            }
        });
    }

    private DocTypeTemplate CreateDocTypeTemplateIfNeeded(String templateFileName) {
        DocTypeTemplate docTypeTemplate = docTypeTemplateRepository.findByName(templateFileName);
        if (docTypeTemplate != null) {
            return docTypeTemplate;
        }
        docTypeTemplate = new DocTypeTemplate(templateFileName);
        docTypeTemplate = docTypeTemplateRepository.save(docTypeTemplate);
        return docTypeTemplate;
    }


    private void createDocTypeFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap, DocTypeTemplate docTypeTemplate) {
        String[] fieldsValues = row.getFieldsValues();
        String parent = fieldsValues[headersLocationMap.get(PARENT)];
        String name = fieldsValues[headersLocationMap.get(NAME)].trim();
        String uniqueName = CsvUtils.getOptionalFieldValue(row, headersLocationMap, UNIQUE_NAME);
        String alternativeName = CsvUtils.getOptionalFieldValue(row, headersLocationMap, ALTERNATIVE_NAME);
        if (StringUtils.isEmpty(name)) {
            name = uniqueName;
        }
        if (StringUtils.isEmpty(uniqueName)) {
            uniqueName = name;
        }
        String singleValueStr = CsvUtils.getOptionalFieldValue(row, headersLocationMap, SINGLE_VALUE);
        boolean singleValue = StringUtils.isEmpty(singleValueStr) ? true : Boolean.valueOf(singleValueStr);

        logger.trace("Create DocType by name {} unique name {} and parent {}", name, uniqueName, parent);
        DocType docType = new DocType(name, docTypeTemplate);
        docType.setUniqueName(uniqueName);
        docType.setAlternativeName(alternativeName);
        docType.setSingleValue(singleValue);
        docType.setDeleted(false);
        if (StringUtils.isNotBlank(parent)) {
            parent = parent.trim();
            DocType parentDocType = docTypeRepository.findByTemplateIdAndUniqueName(docTypeTemplate.getId(), parent);
            if (parentDocType == null) {
                parentDocType = docTypeRepository.findByTemplateIdAndName(docTypeTemplate.getId(), parent);
            }
            if (parentDocType == null) {
                logger.warn("Failed to find DocType parent {} (template:{}) when adding docType {}", parent, docTypeTemplate.getId(), name);
            }
            docType.setParentDocType(parentDocType);
        }
        DocTypeRiskInformation docTypeRiskInformation = extractDocTypeRiskInformation(row, headersLocationMap);
        docType.setDocTypeRiskInformation(docTypeRiskInformation);
        BizList bizList = extractEntityListInformation(row, headersLocationMap);
        if (bizList != null) {
            docType.setBizList(bizList);
        }
        DocType savedDocType = docTypeRepository.save(docType);
        updateDocTypeIdentifier(savedDocType);

        // Assigned doc type tags if exists
        String assignedTagsStr = CsvUtils.getOptionalFieldValue(row, headersLocationMap, ASSIGNED_TAGS);
        if (StringUtils.isNotBlank(assignedTagsStr)) {
            assignDocTypeTags(row, docType, assignedTagsStr);
        }
    }

    private void assignDocTypeTags(RowDTO row, DocType docType, String assignedTagsStr) {
        String[] tagIdentitiers = assignedTagsStr.split(";");
        if (tagIdentitiers.length == 0) {
            logger.warn("Tag identifiers is wrong, fix format in column {} row num: {}, value: {}", ASSIGNED_TAGS, row.getLineNumber(), assignedTagsStr);
            return;
        }
        Map<Long, List<Long>> tagIdsByTagTypeId = prepareTagSettingsMap(tagIdentitiers);
        if (tagIdsByTagTypeId.isEmpty()) {
            logger.warn("Tag identifiers did not exists in DB, check if correct value in column {} row num: {}, value: {}", ASSIGNED_TAGS, row.getLineNumber(), assignedTagsStr);
            return;
        }
        tagIdsByTagTypeId.forEach((tagTypeId, tagIds) -> {
            saveDocTypeTagSettingsByTagType(docType, tagTypeId, tagIds);
        });
    }

    private Map<Long, List<Long>> prepareTagSettingsMap(String[] tagIdentitiers) {
        List<FileTag> fileTags = fileTagService.findByTagIdentifiers(Arrays.asList(tagIdentitiers));
        if (fileTags == null || fileTags.isEmpty()) {
            return Maps.newHashMap();
        }
        return fileTags.stream()
                .map(ft -> Pair.of(ft.getType().getId(), ft.getId()))
                .collect(Collectors.groupingBy(Pair::getKey,
                        Collectors.mapping(Pair::getValue, Collectors.toList())));
    }

    private void validateName(String name) {
        if (Strings.isNullOrEmpty(name) || name.trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("doctype.name.empty"), BadRequestType.MISSING_FIELD);
        }
    }

    @Transactional
    public DocType createDocType(DocTypeDto docTypeDto) {

        validateName(docTypeDto.getName());

        DocType docType = new DocType(docTypeDto.getName());
        docType.setUniqueName(docTypeDto.getUniqueName());
        if (docType.getUniqueName() == null) {
            docType.setUniqueName(docTypeDto.getName());
        }

        if (Strings.isNullOrEmpty(docType.getUniqueName()) || docType.getUniqueName().trim().isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("doctype.uniquename.empty"), BadRequestType.MISSING_FIELD);
        }

        docType.setAlternativeName(docTypeDto.getAlternativeName());
        docType.setDescription(docTypeDto.getDescription());
        docType.setSingleValue(docTypeDto.isSingleValue());
        docType.setDeleted(false);
        if (docTypeDto.getParentId() != null && docTypeDto.getParentId() > 0) {
            DocType parentDocType = getDocTypeById(docTypeDto.getParentId());
            docType.setParentDocType(parentDocType);
        }

        // Set doc type scope
        if (docTypeDto.getScope() != null) {
            docType.setScope(scopeService.getOrCreateScope(docTypeDto.getScope()));
        }

        List<DocType> existingDocTypes;
        if (docType.getParentDocType() == null){
            existingDocTypes = docTypeRepository.findByNullParentAndName(docType.getName());
        }
        else {
            existingDocTypes = docTypeRepository.findByParentAndName(docType.getParentDocType().getId(), docType.getName());
        }

        if (existingDocTypes.size() > 0){
            throw new BadRequestException(messageHandler.getMessage("doctype.name.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }

        DocType newDocType = docTypeRepository.save(docType);
        //Update docType identifier
        newDocType = updateDocTypeIdentifier(newDocType);
        return newDocType;
    }

    @Transactional
    public DocType updateDocTypeIdentifier(DocType newDocType) {
        DocType parentDocType = newDocType.getParentDocType();
        if (newDocType.getName().equals(newDocType.getUniqueName())) {
            newDocType.setUniqueName(newDocType.getName() + newDocType.getId());
        }
        if (parentDocType != null) {
            newDocType.setDocTypeIdentifier(parentDocType.getDocTypeIdentifier() + DocTypeDto.ID_SEPERATOR + newDocType.getId());
        } else {
            newDocType.setDocTypeIdentifier(DocTypeDto.ID_PREFIX + newDocType.getId());
        }
        logger.trace("updateDocTypeIdentifier: {}, UpdatedUniqueName: {}", newDocType.getDocTypeIdentifier(), newDocType.getUniqueName());
        return docTypeRepository.save(newDocType);
    }

    @Transactional
    public DocType updateDocType(long id, DocTypeDto docTypeDto) {
        validateName(docTypeDto.getName());
        DocType docType = docTypeRepository.getOne(id);
        if (docTypeDto.getName() != null) {
            docType.setName(docTypeDto.getName());
        }
        if (docTypeDto.getDescription() != null) {
            docType.setDescription(docTypeDto.getDescription());
        }
        if (docType.getParentDocType() != null) {
            docType.setParentDocType(docType.getParentDocType());
        }
        DocType save = docTypeRepository.save(docType);
        save = updateDocTypeIdentifier(save);
        docTypeCache.invalidate(save.getId());
        return save;
    }

    private DocTypeRiskInformation extractDocTypeRiskInformation(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        String risk = fieldsValues[headersLocationMap.get(RISK_INFORMATION_NAME)];
        if (StringUtils.isNotBlank(risk)) {
            return docTypeRiskInformationRepository.findByName(risk.trim());
        }
        return docTypeRiskInformationRepository.findByName("default");
    }

    private BizList extractEntityListInformation(RowDTO row, Map<String, Integer> headersLocationMap) {
        String[] fieldsValues = row.getFieldsValues();
        String entityList = fieldsValues[headersLocationMap.get(ENTITY_LIST_NAME)];
        if (StringUtils.isNotBlank(entityList)) {
            return bizListService.findByName(entityList.trim(), false);
        }
        return null;
    }

    @Transactional(readOnly = true)
    public Page<DocType> getAllDocTypes(PageRequest pageRequest) {
        Page<DocType> allByAccount = getDocTypes(pageRequest);
        return allByAccount;
    }

    @Transactional(readOnly = true)
    public Page<DocType> getDocTypes(PageRequest pageRequest) {
        Page<DocType> allByAccount = docTypeRepository.findAllByAccount(pageRequest);
        logger.debug("Get All DocTypes as Dto from page {} pageSize {} returned {} total elements and {} elements",
                pageRequest.getPageNumber(), pageRequest.getPageSize(), allByAccount.getTotalElements(), allByAccount.getNumberOfElements());
        return allByAccount;
    }

    @Transactional(readOnly = true)
    public List<DocType> getDocTypes() {
        List<DocType> allByAccount = docTypeRepository.findAllByAccount();
        logger.debug("Get All DocTypes as Dto returned {} elements",
                allByAccount.size());
        return allByAccount;
    }


    @Transactional(readOnly = true)
    public DocType getDocTypeById(Long docTypeId) {
        return docTypeRepository.getOne(docTypeId);
    }

    @Transactional(readOnly = true)
    public DocType getDocTypeByIdWithTagSettings(Long docTypeId) {
        return docTypeRepository.getByIdWithTagSettings(docTypeId);
    }

    @Transactional(readOnly = true)
    public List<DocType> getChildrenDocTypes(long docTypeId) {
        return docTypeRepository.findByParent(docTypeId);
    }

    @Transactional
    public void deleteDocType(long id) {
        try {
            docTypeRepository.deleteById(id);
            docTypeCache.invalidate(id);
        } catch (RuntimeException e) {
            throw new RuntimeException(messageHandler.getMessage("doctype.delete.failure", Lists.newArrayList(id,  e.getMessage())), e);
        }
    }

    public DocType getDocTypeByName(String docTypeName) {
        return docTypeRepository.findByName(docTypeName);
    }

    public Map<String, DocType> getDocTypesByIdentiers(List<String> docTypeIdentifiers) {
        Set<DocType> queryResult = docTypeRepository.findBySolrIdentifiers(docTypeIdentifiers);
        Map<String, DocType> result = new HashMap<>();
        for (DocType docType : queryResult) {
            result.put(docType.getDocTypeIdentifier(), docType);
        }

        return result;
    }

    @Transactional
    public void createDeleteOldIdentifierOperation(DocType docType, String identifier) {
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.DOC_TYPE_MOVE);
        data.setOpData(new HashMap<>());
        data.getOpData().put(RemoveDocTypeThatMoved.DOC_TYPE_ID_PARAM, String.valueOf(docType.getId()));
        data.getOpData().put(RemoveDocTypeThatMoved.DOC_TYPE_IDENTIFIER_PARAM, identifier);
        data.setUserOperation(true);
        data.setDescription(messageHandler.getMessage("operation-description.move-doc-type",
                Lists.newArrayList(docType.getName())));
        scheduledOperationService.createNewOperation(data);
    }

    @Transactional
    public DocType moveDocType(long id, Long newParentId) {
        DocType docType = docTypeRepository.getOne(id);
        String oldIdentifier = docType.getDocTypeIdentifier();
        if (newParentId != null) {
            DocType parentDocType = docTypeRepository.getOne(newParentId);
            DocTypeDto parentDocTypeDto = convertDocTypeToDtoNoPath(parentDocType, parentDocType.getScope());
            if (Stream.of(parentDocTypeDto.getParents()).anyMatch(i -> i == id)) {
                logger.info("Error: potential loop - can not move DocType {} to reside under sub-child {}", docType, parentDocType);
                throw new BadRequestException(messageHandler.getMessage("doctype.move.circular"), BadRequestType.UNSUPPORTED_VALUE);
            }
            logger.info("Move DocType {} to sit under parent {}", docType, parentDocType);
            docType.setParentDocType(parentDocType);
        } else {
            logger.info("Move DocType {} to sit under root", docType);
            docType.setParentDocType(null);
        }
        updateDocTypeIdentifier(docType);
        DocType result = docTypeRepository.save(docType);

        createDeleteOldIdentifierOperation(docType, oldIdentifier);

        // recursively handle child doc types
        List<DocType> children = docTypeRepository.findByParent(id);
        for (DocType child : children) {
            moveDocType(child.getId(), id);
        }
        docTypeCache.invalidate(result.getId());
        return result;
    }

    @Transactional
    public List<DocTypeDto> assignDocTypeTagsByTagType(Table<Long, Long, List<Long>> docTypeTagSettingTable) {
        return docTypeTagSettingTable.cellSet()
                .stream()
                .map(cell -> saveDocTypeTagSettingsByTagType(getDocTypeById(cell.getRowKey()),
                        cell.getColumnKey(), cell.getValue()))
                .collect(Collectors.toList());
    }

    private DocTypeDto saveDocTypeTagSettingsByTagType(DocType docType, Long tagTypeId, List<Long> tagIds) {
        FileTagType fileTagType = fileTagTypeService.getFileTagType(tagTypeId);
        if (fileTagType.isSingleValueTag()) {
            return replaceSingleValueTag(docType, tagTypeId, tagIds);
        } else {
            return replaceMultiValueTag(docType, tagTypeId, tagIds);
        }
    }

    private DocTypeDto replaceMultiValueTag(DocType docType, long tagTypeId, List<Long> tagIds) {
        Set<FileTagSetting> fileTagSettings = docType.getFileTagSettings();
        List<Long> updatedFileTagIds = new LinkedList<>();
        fileTagSettings.stream().filter(fts -> Objects.equals(fts.getFileTag().getType().getId(), tagTypeId))
                .forEach(fts -> {
                    if (tagIds.contains(fts.getFileTag().getId())) {
                        logger.debug("Assigning multi-value tag {} for doc type {}", fts.getFileTag().getName(), docType.getName());
                        fts.setDeleted(false);
                        updatedFileTagIds.add(fts.getFileTag().getId());
                    } else {
                        logger.debug("Un-assigning multi-value tag {} for doc type {}", fts.getFileTag().getName(), docType.getName());
                        fts.setDeleted(true);
                        fts.setDirty(true);
                    }
                });

        List<Long> newTagIds = new ArrayList(CollectionUtils.subtract(tagIds, updatedFileTagIds));
        if (!newTagIds.isEmpty()) {
            Set<FileTag> fileTagsById = fileTagService.getFileTagsById(newTagIds);
            fileTagsById.forEach(ft -> {
                logger.debug("Assigning multi-value tag {} for doc type {}", ft.getName(), docType.getName());
                fileTagSettings.add(createFileTagSettings(ft, docType));
            });
        }

        docTypeRepository.save(docType);
        docTypeCache.invalidate(docType.getId());
        return convertDocTypeToDto(docType, docType.getScope());
    }

    private DocTypeDto replaceSingleValueTag(DocType docType, Long tagTypeId, List<Long> tagIds) {
        if (tagIds.size() > 1) {
            throw new RuntimeException(messageHandler.getMessage("doctype.replace-tag.multi", Lists.newArrayList(tagIds)));
        }

        // Remove (or unremove if deleted) previous file tag settings of this tag type
        docType.getFileTagSettings().stream()
                .filter(fts -> Objects.equals(fts.getFileTag().getType().getId(), tagTypeId))
                .forEach(fileTagSetting -> {
                    if (tagIds.contains(fileTagSetting.getFileTag().getId())) {
                        logger.debug("Assigning single value tag {} for doc type {}", fileTagSetting.getFileTag().getName(), docType.getName());
                        fileTagSetting.setDeleted(false);
                    } else {
                        logger.debug("Un-assigning single value tag {} for doc type {}", fileTagSetting.getFileTag().getName(), docType.getName());
                        fileTagSetting.setDirty(true);
                        fileTagSetting.setDeleted(true);
                    }
                });

        if (!tagIds.isEmpty()) {
            Long tagId = tagIds.iterator().next();
            if (docType.getFileTagSettings().stream()
                    .noneMatch(fts -> Objects.equals(fts.getFileTag().getId(), tagId))) {
                FileTag fileTag = fileTagService.getFileTag(tagId);
                logger.debug("Assigning tag {} for doc type {}", fileTag.getName(), docType.getName());
                docType.getFileTagSettings().add(createFileTagSettings(fileTag, docType));
            }
        }

        docTypeRepository.save(docType);
        docTypeCache.invalidate(docType.getId());
        return convertDocTypeToDto(docType, docType.getScope());
    }

    private FileTagSetting createFileTagSettings(FileTag fileTag, DocType docType) {
        FileTagSetting fileTagSetting = new FileTagSetting();
        fileTagSetting.setDirty(true);
        fileTagSetting.setDeleted(false);
        fileTagSetting.setDocType(docType);
        fileTagSetting.setFileTag(fileTag);
        return fileTagSetting;
    }

    public void saveDocType(DocType docType) {
        docType = docTypeRepository.save(docType);
        docTypeCache.invalidate(docType.getId());
    }

    public static DocTypeDto convertDocTypeToDtoNoPath(DocType docType, BasicScope scope) {
        if (docType == null) {
            return null;
        }
        DocTypeDto docTypeDto = new DocTypeDto(docType.getId(), docType.getName());
        docTypeDto.setDescription(docType.getDescription());
        docTypeDto.setUniqueName(docType.getUniqueName());
        docTypeDto.setAlternativeName(docType.getAlternativeName());
        docTypeDto.setSingleValue(docType.isSingleValue());
        docTypeDto.setDeleted(docType.isDeleted());
        docTypeDto.setScope(AssociationUtils.convertBasicScopeToDto(scope));
        docTypeDto.setFileTagTypeSettings(FileTagSettingService.convertFileTagTypeSettings(docType.getFileTagSettings()));
        if (docType.getParentDocType() != null) {
            docTypeDto.setParentId(docType.getParentDocType().getId());
        }
        docTypeDto.setDocTypeIdentifier(docType.getDocTypeIdentifier());
        String docTypeIdentifier = docType.getDocTypeIdentifier();
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(docTypeIdentifier)) {
            fillInDocTypeParents(docTypeDto, docTypeIdentifier);
        }
        return docTypeDto;
    }

    public DocTypeDto convertDocTypeToDto(DocType docType, BasicScope scope) {
        DocTypeDto docTypeDto = convertDocTypeToDtoNoPath(docType, scope);

        if (docTypeDto != null && docType.getParentDocType() != null) {
            DocTypeDto parent = getFromCache(docTypeDto.getParentId());
            docTypeDto.setPath(parent.getPath() == null ? parent.getName() : parent.getPath() + DocTypeDto.SEPARATOR + parent.getName());
        }

        return docTypeDto;
    }

    private static void fillInDocTypeParents(DocTypeDto docTypeDto, String docTypeIdentifier) {
        String substring = docTypeIdentifier.substring(DocTypeDto.ID_PREFIX.length());
        String[] split = org.apache.commons.lang3.StringUtils.split(substring, ".");
        Long[] result = new Long[split.length];
        for (int i = 0; i < split.length; i++) {
            result[i] = Long.valueOf(split[i]);
        }
        docTypeDto.setParents(result);
    }

    public List<DocType> findDeleted() {
        return docTypeRepository.findDeleted();
    }

    public List<DocType> findByParentAll(Long id) {
        return docTypeRepository.findByParentAll(id);
    }

    @Transactional(readOnly = true)
    public Page<FileTagSetting> findDirtyFileTagSettings(int associationApplySize) {
        return docTypeRepository.findDirtyFileTagSettings(PageRequest.of(0, associationApplySize));
    }

    @Transactional
    public void deleteFileTagSettings(FileTagSetting fts) {
        DocType docType = fts.getDocType();
        docType.getFileTagSettings().remove(fts);
        saveDocType(docType);
    }
}
