package com.cla.filecluster.service.export.exporters.file.acl;

import com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class FileUserDtoMixin {

    @JsonProperty(FileCountHeaderFields.NAME)
    private String user;
}
