package com.cla.filecluster.service.media;

import com.box.sdk.*;
import com.cla.common.domain.dto.messages.FoldersListResultMessage;
import com.cla.common.domain.dto.messages.IngestError;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.error.ProcessingErrorType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.BoxConnectionDetailsDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.connector.mediaconnector.box.BoxMediaConnector;
import com.cla.filecluster.domain.entity.dataCenter.CustomerDataCenter;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.dataCenter.CustomerDataCenterService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;

/**
 * Box media app service
 * Created by uri on 22/05/2016.
 */
@Service
public class BoxMediaAppService implements MediaSpecificAppService<BoxMediaConnector,BoxConnectionDetailsDto> {

    /**
     * This string is used by MP to identify the LIST_FOLDERS_REQUEST request as connection test. In this case a new temp connection will be made based on the delivered JWT
     */
    public static String BOX_CONNECTION_TEST_LABEL = "##connection-test##";

    private final static Logger logger = LoggerFactory.getLogger(BoxMediaAppService.class);

    @Autowired
    private MessageHandler messageHandler;

	@Autowired
	private RemoteMediaProcessingService remoteMediaProcessingService;

	@Autowired
	private CustomerDataCenterService customerDataCenterService;

	@Autowired
    private MediaConnectionDetailsService connectionDetailsService;

//    public static final String BOX_ACCESS_TOKEN_KEY = "box.access.token";
//    public static final String BOX_REFRESH_TOKEN_KEY = "box.refresh.token";

    private Map<Long,BoxAPIConnection> boxAPIConnectionMap = new ConcurrentHashMap<>();

    @Value("${box.auth.url:https://www.box.com/api/oauth2/authorize?response_type=code}")
    private String boxOAUTHUrl;

    @Value("${box.auth.redirect.url:http://localhost:8080/api/media/box/response}")
    private String redirectUrl;

    @Value("${box.auth.client.id:c7thbtenhm2z70jivuh3i2y5sz50vdmw}")
    private String boxClientId;

    @Value("${box.auth.client.secret:ZXhARAO6B1OnaVC6c9MaYy6Ps6fhcgxX}")
    private String boxSecretId;


    @Value("${box.api.connection.file:./temp/box_connection.txt}")
    private String boxApiConnectionFile;

	public void handleBoxOauthResponse(Long currentUserId, String code, boolean saveBoxResponseToFile) {
        BoxAPIConnection api = new BoxAPIConnection(boxClientId,
                boxSecretId, code);
        String save = api.save();
        if (!saveBoxResponseToFile) {
            setBoxDataToUser(save);
            boxAPIConnectionMap.put(currentUserId, api);
        }
        else {
            try {
                Files.write(Paths.get(boxApiConnectionFile),api.save().getBytes(), StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE);
            } catch (IOException e) {
                logger.error("Failed to write box connection back to file",e);
            }
        }
        addRefreshAutoSaveListener(api, saveBoxResponseToFile);
    }

    private void addRefreshAutoSaveListener(BoxAPIConnection api, boolean saveToFile) {
        api.addListener(new BoxAPIConnectionListener() {
            @Override
            public void onRefresh(BoxAPIConnection api) {
                logger.debug("Box API {} tokens refreshed",api.getClientID());
                if (saveToFile) {
                    try {
                        Files.write(Paths.get(boxApiConnectionFile),api.save().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
                    } catch (IOException e) {
                        logger.error("Failed to write box connection back to file",e);
                    }
                }
                else {
                    setBoxDataToUser(api.save());
                }
            }

            @Override
            public void onError(BoxAPIConnection api, BoxAPIException error) {

            }
        });
    }

    private void setBoxDataToUser(String boxData) {
        //TODO: cannot use persistence here
        // userService.addUserSavedData(BOX_API_OBJECT_KEY, boxData);
    }

    public String buildAuthorizeUrl(String stateId) {
        String sb = boxOAUTHUrl + "&client_id=" + boxClientId +
                "&redirect_uri=" + redirectUrl +
                "&state=" + stateId;
        //TODO: cannot use persistence here
        /* User currentUser = userService.getCurrentUser();
        logger.debug("Binding user {} to state {}",currentUser,stateId);
        boxStateMapper.put(stateId,currentUser); */
        return sb;
    }

    public void fillAuthentication(String state) {
        //TODO: cannot use persistence here
        /*User currentUser = userService.getCurrentUser();
        if (currentUser == null) {
            logger.info("Anonymous user on box OAuth response. Fill from state {}",state);
            User user = boxStateMapper.get(state);
            logger.info("Authenticating as user {} on box",user);
            ClaUserDetails claUserDetails = new ClaUserDetails(user);
            Authentication auth = new UsernamePasswordAuthenticationToken(claUserDetails, claUserDetails.getPassword(), claUserDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);
        } */
    }

    public List<ServerResourceDto> listServerFolders(Long currentUserId, String id, String boxConnectionData) {
        try {
            BoxAPIConnection api = getBoxApiConnection(currentUserId, boxConnectionData);
            List<ServerResourceDto> result = Lists.newArrayList();
            if (id == null) {
                //list root folder folders
                BoxFolder rootFolder = BoxFolder.getRootFolder(api);
                logger.debug("List folders under BOX root folder: {}",rootFolder.getID());
                for (BoxItem.Info itemInfo : rootFolder) {
                    if (itemInfo instanceof BoxFolder.Info) {
                        result.add(new ServerResourceDto(itemInfo.getID(),itemInfo.getName()));
                    }
                }
            }
            else {
                BoxFolder boxFolder = new BoxFolder(api,id);
                logger.debug("List folders under BOX folder {}",boxFolder.getID());
                for (BoxItem.Info itemInfo : boxFolder) {
                    if (itemInfo instanceof BoxFolder.Info) {
                        result.add(new ServerResourceDto(itemInfo.getID(),itemInfo.getName()));
                    }
                }
            }
            return result;
        } catch (BoxAPIException e) {
            logger.error("Failed to list server folders (Box error {}): {}", e.getResponseCode(), e.getResponse(), e);
            throw e;
        }
    }

    private BoxAPIConnection getBoxApiConnection(Long currentUserId, String boxConnectionData) {
        if (boxAPIConnectionMap.containsKey(currentUserId)) {
            return boxAPIConnectionMap.get(currentUserId);
        }
        try {
            boolean saveToFile = false;
            if (boxConnectionData == null) {
                boxConnectionData = readBoxApiFromFile();
                if (boxConnectionData == null) {
                    throw new RuntimeException(messageHandler.getMessage("media-conn.box.unauthorize"));
                }
                else {
                    logger.debug("Loaded box connection data from {}", boxApiConnectionFile);
                    saveToFile = true;
                }
            }

            BoxAPIConnection boxAPIConnection = BoxAPIConnection.restore(boxClientId, boxSecretId, boxConnectionData);
            addRefreshAutoSaveListener(boxAPIConnection, saveToFile);
            boxAPIConnectionMap.put(currentUserId, boxAPIConnection);
            return boxAPIConnection;
        }
        catch (Exception e) {
            logger.error("Failed to create box api connection: ",e.getMessage(),e);
            throw e;
        }

    }

    private String readBoxApiFromFile() {
        if (isBoxApiFileExists()) {
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(boxApiConnectionFile));
                return new String(bytes);
            } catch (IOException e) {
                logger.error("Failed to read box API connection file",e);
            }
        }
        return null;
    }

    public boolean isBoxApiFileExists() {
        return Files.exists(Paths.get(boxApiConnectionFile));
    }



	@Override
	public List<ServerResourceDto> listFoldersUnderPath(MediaConnectionDetailsDto connectionDetails,
                                                        Optional<String> path, Long customerDataCenterId, boolean isTest) {
		CustomerDataCenter customerDataCenterById = customerDataCenterService.getCustomerDataCenterById(customerDataCenterId);
		if( customerDataCenterById == null){
            throw new BadRequestException(messageHandler.getMessage("data-center.missing"), BadRequestType.ITEM_NOT_FOUND );
		}

		BoxConnectionDetailsDto boxConnectionDetailsDto = (BoxConnectionDetailsDto) connectionDetails;
		BoxConnectionDetailsDto newBoxConnectionDetailsDto = new BoxConnectionDetailsDto();
		BeanUtils.copyProperties(boxConnectionDetailsDto, newBoxConnectionDetailsDto);
		newBoxConnectionDetailsDto.setJwt(connectionDetailsService.encrypt(boxConnectionDetailsDto.getJwt()))
				.setConnectionParametersJson(connectionDetailsService.encrypt(boxConnectionDetailsDto.getConnectionParametersJson()));

    	CompletableFuture<FoldersListResultMessage> syncLockFuture = new CompletableFuture<>();
		String requestId = requestLockMap.addRequestToLockMap(syncLockFuture);
		remoteMediaProcessingService.issueListFoldersRequest(requestId, path.orElse(""), customerDataCenterId, newBoxConnectionDetailsDto, isTest);
		try {
			FoldersListResultMessage foldersListResultMessage = syncLockFuture.get(30, TimeUnit.SECONDS);
			if(!foldersListResultMessage.getErrors().isEmpty()){
				IngestError firstError = foldersListResultMessage.getErrors().get(0);
                throw new BadRequestException(messageHandler.getMessage("list-folders.sub-folders-failure",
                        Lists.newArrayList(path.orElse("root"), firstError.getExceptionText())),
                        firstError.getErrorType() == ProcessingErrorType.ERR_NONE ? BadRequestType.ITEM_NOT_FOUND : BadRequestType.OTHER);
			}
			List<ServerResourceDto> serverResourceDtos = foldersListResultMessage.getFoldersList();
			return serverResourceDtos;
		} catch (InterruptedException | ExecutionException |  TimeoutException e) {
			logger.error("error while waiting for folders listing", e);
            throw new BadRequestException(messageHandler.getMessage("list-folders.failure", Lists.newArrayList(e.getMessage())), BadRequestType.OTHER);
		}
	}
}
