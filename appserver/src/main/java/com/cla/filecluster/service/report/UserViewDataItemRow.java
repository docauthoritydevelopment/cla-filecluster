package com.cla.filecluster.service.report;

import com.cla.filecluster.service.importEntities.ImportItemRow;

/**
 * Created by ophir on 04/21/2019.
 */
public class UserViewDataItemRow extends ImportItemRow {

    private String name;

    private String displayType;

    private String displayData;

    private String filterName;

    private String filterDescription;

    private String filterRaw;

    private Boolean filterGlobal;

    private String containerName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String getDisplayType() {
        return displayType;
    }

    void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    String getDisplayData() {
        return displayData;
    }

    void setDisplayData(String displayData) {
        this.displayData = displayData;
    }

    String getFilterName() {
        return filterName;
    }

    void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    String getFilterDescription() {
        return filterDescription;
    }

    void setFilterDescription(String filterDescription) {
        this.filterDescription = filterDescription;
    }

    String getFilterRaw() {
        return filterRaw;
    }

    void setFilterRaw(String filterRaw) {
        this.filterRaw = filterRaw;
    }

    Boolean isFilterGlobal() {
        return filterGlobal;
    }

    void setFilterGlobal(Boolean filterGlobal) {
        this.filterGlobal = filterGlobal;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    @Override
    public String toString() {
        return "SearchPatternItemRow{" +
                "name='" + name + '\'' +
                ", displayType='" + displayType + '\'' +
                ", displayData='" + displayData + '\'' +
                ", filterName='" + filterName + '\'' +
                ", filterDescription='" + filterDescription + '\'' +
                ", filterRaw='" + filterRaw + '\'' +
                ", filterGlobal='" + filterGlobal + '\'' +
                ", containerName='" + containerName + '\'' +
                super.toString()+
                '}';
    }
}
