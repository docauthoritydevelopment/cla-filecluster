package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.common.domain.dto.security.FunctionalRoleSummaryInfoDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleAppService;
import com.cla.filecluster.service.functionalrole.FunctionalRoleService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/functionalrole")
public class FunctionalRoleApiController {

    private final static Logger logger = LoggerFactory.getLogger(FunctionalRoleApiController.class);

    @Autowired
    private FunctionalRoleAppService functionalRoleAppService;
    
    @Autowired
    private FunctionalRoleService functionalRoleService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<FunctionalRoleSummaryInfoDto> listFunctionalRoleSummaryInfoDto(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return functionalRoleService.listAllFunctionRoles(dataSourceRequest);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Set<FunctionalRoleDto> getFunctionalRolesByIds(@RequestParam Map<String,String> params) {
        String funcIdsStr = params.get("funcIds");
        logger.debug("Get func role Ids: {}",funcIdsStr);
        List<Long> funcIds = Arrays.stream(funcIdsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        return functionalRoleService.getFunctionalRoleDtoByIds(funcIds);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public FunctionalRoleSummaryInfoDto createFunctionalRole(@RequestBody FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto) {
        FunctionalRoleSummaryInfoDto functionalRole = functionalRoleService.createFunctionalRole(functionalRoleSummaryInfoDto.getFunctionalRoleDto(), functionalRoleSummaryInfoDto.getLdapGroupMappingDtos());
        eventAuditService.audit(AuditType.DATA_ROLE,"Create data role", AuditAction.CREATE, FunctionalRoleSummaryInfoDto.class,functionalRole.getFunctionalRoleDto().getId(),functionalRoleSummaryInfoDto);
        return functionalRole;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteFunctionalRole(@PathVariable long id) {
        FunctionalRoleDto functionalRole = getFunctionalRole(id);
        functionalRoleAppService.deleteFunctionalRole(id);
        eventAuditService.audit(AuditType.DATA_ROLE,"Delete data role", AuditAction.DELETE, FunctionalRoleDto.class,functionalRole.getId(),functionalRole);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public FunctionalRoleSummaryInfoDto updateFunctionalRole(@PathVariable Long id, @RequestBody FunctionalRoleSummaryInfoDto functionalRoleSummaryInfoDto) {
        FunctionalRoleSummaryInfoDto functionalRole = functionalRoleService.updateFunctionalRole(functionalRoleSummaryInfoDto.getFunctionalRoleDto(), functionalRoleSummaryInfoDto.getLdapGroupMappingDtos());
        eventAuditService.audit(AuditType.DATA_ROLE,"Update data role", AuditAction.UPDATE, FunctionalRoleSummaryInfoDto.class,functionalRole.getFunctionalRoleDto().getId(),functionalRoleSummaryInfoDto);
        return functionalRole;
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public FunctionalRoleDto getFunctionalRole(@PathVariable long id) {
        FunctionalRoleDto functionalRole = functionalRoleService.getFunctionalRoleById(id);
        if (functionalRole == null) {
            throw new BadRequestException(messageHandler.getMessage("data-role.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return functionalRole;
    }

    @RequestMapping(value="/filecount")
    public FacetPage<AggregationCountItemDTO<FunctionalRoleDto>> findFuncRolesFileCount(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Discover functional roles export", AuditAction.VIEW,  FunctionalRoleDto.class,null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY,"Discover functional roles", AuditAction.VIEW,  FunctionalRoleDto.class,null, dataSourceRequest);
        }
        return functionalRoleAppService.listFuncRolesFileCountWithFileFilter(dataSourceRequest);
    }
    @RequestMapping(value="/{functionalRoleId}/associate/file/{fileId}", method = RequestMethod.PUT)
    public ClaFileDto associateFunctionalRoleWithFile(@PathVariable long functionalRoleId, @PathVariable long fileId) {
        FunctionalRoleDto functionalRole = getFunctionalRole(functionalRoleId);
        ClaFileDto claFileDto = functionalRoleAppService.associateFileWithFunctionalRole(functionalRoleId, fileId);
        if(claFileDto!=null) {
            eventAuditService.auditDual(AuditType.DATA_ROLE, "Assign data role to file", AuditAction.ASSIGN, FunctionalRoleDto.class, functionalRole.getId(), ClaFileDto.class, claFileDto.getId(), claFileDto);
        }
        return claFileDto;
    }

    @RequestMapping(value="/{functionalRoleId}/associate/files", method = RequestMethod.PUT)
    public void associateFunctionalRoleWithFiles(@PathVariable long functionalRoleId, @RequestParam String fileIds) {
        String[] fileIdArray = StringUtils.split(fileIds, ',');
        for (String fileId : fileIdArray) {
            associateFunctionalRoleWithFile(functionalRoleId, Long.getLong(fileId));
        }
    }

    @RequestMapping(value="/{functionalRoleId}/associate/file/{fileId}", method = RequestMethod.DELETE)
    public ClaFileDto removeFunctionalRoleAssociationFromFile(@PathVariable long functionalRoleId, @PathVariable long fileId) {
        FunctionalRoleDto functionalRole = getFunctionalRole(functionalRoleId);
        ClaFileDto claFileDto = functionalRoleAppService.removeFunctionalRoleAssociationFromFile(functionalRoleId, fileId);
        if(claFileDto!=null) {
            eventAuditService.auditDual(AuditType.DATA_ROLE, "Unassign data role from file", AuditAction.UNASSIGN, FunctionalRoleDto.class, functionalRole.getId(), ClaFileDto.class, claFileDto.getId(), claFileDto);
        }
        return claFileDto;
    }

    @RequestMapping(value="/{functionalRoleId}/associate/files", method = RequestMethod.DELETE)
    public void removeFunctionalRoleAssociationFromFiles(@PathVariable long functionalRoleId, @RequestParam String fileIds) {
        String[] fileIdArray = StringUtils.split(fileIds, ',');
        for (String fileId : fileIdArray) {
            removeFunctionalRoleAssociationFromFile(functionalRoleId,Long.getLong(fileId));
        }
    }

    @RequestMapping(value="/{functionalRoleId}/associate/folder/{docFolderId}", method = RequestMethod.PUT)
    public DocFolderDto associateFunctionalRoleWithDocFolder(@PathVariable long functionalRoleId, @PathVariable long docFolderId) {
        FunctionalRoleDto functionalRole = getFunctionalRole(functionalRoleId);
        DocFolderDto docFolderDto = functionalRoleAppService.associateFolderWithFunctionalRole(functionalRoleId, docFolderId);
        if(docFolderDto!=null) {
            eventAuditService.auditDual(AuditType.DATA_ROLE, "Assign data role to folder", AuditAction.ASSIGN, FunctionalRoleDto.class, functionalRole.getId(), DocFolderDto.class, docFolderDto.getId(), docFolderDto);
        }
        return docFolderDto;
    }

    @RequestMapping(value="/{functionalRoleId}/associate/folders", method = RequestMethod.PUT)
    public void associateFunctionalRoleWithDocFolders(@PathVariable long functionalRoleId, @PathVariable String folderIds) {
        String[] folderIdsArray = StringUtils.split(folderIds, ',');
        for (String docFolderId : folderIdsArray) {
            associateFunctionalRoleWithDocFolder(functionalRoleId,Long.valueOf(docFolderId));
        }
    }

    @RequestMapping(value="//{functionalRoleId}/associate/folder/{docFolderId}", method = RequestMethod.DELETE)
    public DocFolderDto removeFunctionalRoleAssociationFromDocFolder( @PathVariable long functionalRoleId, @PathVariable long docFolderId) {
        logger.debug("remove FunctionalRole {} Association From Folder {}",functionalRoleId,docFolderId);
        FunctionalRoleDto functionalRole = getFunctionalRole(functionalRoleId);
        DocFolderDto docFolderDto = functionalRoleAppService.removeFunctionalRoleAssociationFromFolder(functionalRoleId, docFolderId);
        if(docFolderDto!=null) {
            eventAuditService.auditDual(AuditType.DATA_ROLE, "Unassign data role from folder", AuditAction.UNASSIGN, FunctionalRoleDto.class, functionalRoleId, DocFolderDto.class, docFolderId, docFolderDto);
        }
        return  docFolderDto;
    }

    @RequestMapping(value="/{functionalRoleId}/associate/folders", method = RequestMethod.DELETE)
    public void removeFunctionalRoleAssociationFromDocFolders( @PathVariable long functionalRoleId, @PathVariable String folderIds) {
        String[] folderIdsArray = StringUtils.split(folderIds, ',');
        for (String docFolderId : folderIdsArray) {
            removeFunctionalRoleAssociationFromDocFolder(functionalRoleId,Long.valueOf(docFolderId));
        }
    }


    @RequestMapping(value="/{functionalRoleId}/associate/group/{groupId}", method = RequestMethod.PUT)
    public GroupDto associateFunctionalRoleWithGroup(@PathVariable long functionalRoleId, @PathVariable String groupId) {
        logger.debug("Associate Group id {} with functionalRoleId {}",groupId,functionalRoleId);
         FunctionalRoleDto functionalRole = getFunctionalRole(functionalRoleId);
        GroupDto groupDto = functionalRoleAppService.associateGroupWithFunctionalRole(functionalRoleId, groupId);
        if(groupDto!=null) {
            eventAuditService.auditDual(AuditType.DATA_ROLE, "Assign data role to group", AuditAction.ASSIGN, FunctionalRoleDto.class, functionalRoleId, GroupDto.class,groupId,groupDto);
        }
        return groupDto;
    }

    @RequestMapping(value="/{functionalRoleId}/associate/groups", method = RequestMethod.PUT)
    public void associateFunctionalRoleWithGroups(@PathVariable long functionalRoleId, @RequestParam String groupIds) {
        String[] groupIdsArray = StringUtils.split(groupIds, ',');
        for (String groupId : groupIdsArray) {
            associateFunctionalRoleWithGroup(functionalRoleId,groupId);
        }
    }

    @RequestMapping(value="/{functionalRoleId}/associate/group/{groupId}", method = RequestMethod.DELETE)
    public GroupDto removeFunctionalRoleAssociationFromGroup(@PathVariable long functionalRoleId, @PathVariable String groupId) {
        logger.debug("remove functionalRole {} Association From Group {}",functionalRoleId,groupId);
        FunctionalRoleDto functionalRole = getFunctionalRole(functionalRoleId);
        GroupDto groupDto = functionalRoleAppService.removeFunctionalRoleAssociationFromGroup(functionalRoleId, groupId);
        if(groupDto!=null) {
            eventAuditService.auditDual(AuditType.DATA_ROLE, "Unassign data role item from group", AuditAction.UNASSIGN, FunctionalRoleDto.class, functionalRoleId, GroupDto.class,groupId,groupDto);
        }
        return groupDto;
    }

    @RequestMapping(value="/{functionalRoleId}/associate/groups", method = RequestMethod.DELETE)
    public void removeFunctionalRoleAssociationFromGroups(@PathVariable long functionalRoleId, @RequestParam String groupIds) {
        String[] groupIdsArray = StringUtils.split(groupIds, ',');
        for (String groupId : groupIdsArray) {
            removeFunctionalRoleAssociationFromGroup(functionalRoleId,groupId);
        }
    }


}
