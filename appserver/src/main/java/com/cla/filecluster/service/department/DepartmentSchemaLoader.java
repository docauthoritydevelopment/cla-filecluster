package com.cla.filecluster.service.department;

import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.importEntities.SchemaData;
import com.cla.filecluster.service.importEntities.SchemaLoader;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
@Service
public class DepartmentSchemaLoader extends SchemaLoader<DepartmentItemRow, DepartmentDto> {

    private static Logger logger = LoggerFactory.getLogger(DepartmentSchemaLoader.class);

    private final static String CSV_HEADER_FULL_NAME = "FULL_NAME";
    private final static String CSV_HEADER_DESCRIPTION = "DESCRIPTION";

    private final static String[] REQUIRED_HEADERS = {CSV_HEADER_FULL_NAME};

    public DepartmentSchemaLoader() {
        super(REQUIRED_HEADERS);
    }

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private EventAuditService eventAuditService;

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    protected DepartmentItemRow extractRowSpecific(SchemaData<DepartmentItemRow> schemaData, String[] fieldsValues, ArrayList<String> parsingWarningMessages) {
        DepartmentItemRow row = new DepartmentItemRow();

        row.setFullName(extractValue(schemaData, fieldsValues, CSV_HEADER_FULL_NAME, parsingWarningMessages));
        row.setDescription(extractValue(schemaData, fieldsValues, CSV_HEADER_DESCRIPTION, parsingWarningMessages));

        return row;
    }

    @Override
    protected boolean validateItemRow(DepartmentDto dto, DepartmentItemRow itemRow, SchemaData<DepartmentItemRow> schemaData, boolean afterAddToDB, boolean createStubs) {

        if (itemRow.getFullName() == null) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("department.import.missing-name"));
            return false;
        }

        if (itemRow.getFullName() != null && StringUtils.countMatches(itemRow.getFullName(), DepartmentDto.SEPARATOR) > 1) {
            schemaData.addErrorRowItem(itemRow, messageHandler.getMessage("department.import.one-sub-level"));
            return false;
        }

        if(!afterAddToDB) { //validate if already exists - only before added to db
            if (itemRow.getFullName() != null && departmentService.parseDepartmentFromPath(itemRow.getFullName()) != null) {
                schemaData.addDupRowItem(itemRow, messageHandler.getMessage("department.import.duplicate", Lists.newArrayList(itemRow.getFullName())));
                return false;
            }
        }

        return true;
    }

    @Override
    protected Boolean loadItemRowToRepository(SchemaData<DepartmentItemRow> schemaData, DepartmentItemRow itemRow, boolean updateDuplicates, boolean createStubs) {
        DepartmentDto newDeptDto = createDtoFromRowItem(itemRow);
        Boolean isValid = validateItemRow(newDeptDto, itemRow, schemaData, true, createStubs);

        if(!itemRow.getError()) {
            Long departmentId = departmentService.parseDepartmentFromPath(itemRow.getFullName());
            if (departmentId == null) {
                DepartmentDto departmentDto = departmentService.createDepartmentFromPath(itemRow.getFullName());
                departmentService.updateDescriptionById(departmentDto.getId(), itemRow.getDescription());
                departmentDto.setDescription(itemRow.getDescription());
                eventAuditService.audit(AuditType.ROOT_FOLDER, "Create department", AuditAction.IMPORT_ITEM_CREATE, DepartmentDto.class, departmentDto.getId(), departmentDto);
            } else if (updateDuplicates) {
                departmentService.updateDescriptionById(departmentId, itemRow.getDescription());
            } else {
                return false;
            }
        }
        return isValid;
    }

    @Override
    protected DepartmentDto createDtoFromRowItem(DepartmentItemRow itemRow) {
        DepartmentDto dto = new DepartmentDto();
        dto.setDescription(itemRow.getDescription());
        dto.setDeleted(false);
        dto.setName(itemRow.getFullName());
        return dto;
    }

    @Override
    public SchemaData<DepartmentItemRow> createSchemaInstance() {
        return new DepartmentSchemaData();
    }
}
