package com.cla.filecluster.service.upgrade;

import com.cla.filecluster.domain.entity.schema.SchemaUpgradeState;
import com.cla.filecluster.repository.jpa.upgrade.SchemaUpgradeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.ToIntBiFunction;

/**
 * Created by shtand on 22/06/2016.
 */
@Service
public class UpgradeSchemaService {

    @Autowired
    SchemaUpgradeRepository schemaUpgradeRepository;

    private static final Logger logger = LoggerFactory.getLogger(UpgradeSchemaService.class);

    @Value("${files.dirtyupdate.batchsize:10000}")
    private int updateClaFilesDirtyBatchSize;

    @Transactional(readOnly = true)
    public SchemaUpgradeState getUpgradeSchemaStatus(long version) {
        List<Object> upgradeStateList = schemaUpgradeRepository.showScriptUpgradeState(version);
        if (upgradeStateList == null || upgradeStateList.size() == 0) {
            return SchemaUpgradeState.NOT_FOUND;
        }
        String upgradeStateString = (String) upgradeStateList.get(0);
        logger.debug("Upgrade script {} has state {}", version, upgradeStateString);
        return SchemaUpgradeState.SUCCESS;
        //TODO - support FAILURE and SKIPPED
    }

    @Transactional(readOnly = false)
    public void markUpgradeSchema(long version, SchemaUpgradeState schemaUpgradeState, String description, long executionTime) {
        if (SchemaUpgradeState.NOT_FOUND.equals(schemaUpgradeState)) {
            return;
        }
        schemaUpgradeRepository.markUpgradeSchema(version, description, executionTime, System.currentTimeMillis(), schemaUpgradeState.toString());
    }



}
