package com.cla.filecluster.service.export.exporters.file;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 2/6/2019
 */
public class AllFilesAggCountDtoMixin {

    @JsonProperty(FileCountHeaderFields.NAME)
    private String item;

    @JsonProperty(FileCountHeaderFields.NUM_OF_FILES)
    private Integer count;
}
