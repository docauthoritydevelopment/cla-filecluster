package com.cla.filecluster.service.export.serialize;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.google.common.base.CaseFormat;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by: yael
 * Created on: 4/4/2019
 */
public class UpperToCamelCaseSerializer  extends JsonSerializer<Object> {
    @Override
    public void serialize(Object value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(Optional.ofNullable(value)
                .map(val -> CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, val.toString()))
                .orElse(""));
    }
}
