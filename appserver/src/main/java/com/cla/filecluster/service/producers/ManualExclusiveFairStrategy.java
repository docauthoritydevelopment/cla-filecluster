package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

import java.util.Collection;

public class ManualExclusiveFairStrategy extends ExclusiveFairStrategy{

    public ManualExclusiveFairStrategy(JobManagerService jobManager, SolrTaskService solrTaskService, TaskProducingStrategyConfig config) {
        super(jobManager, solrTaskService, config);
    }

    public boolean shouldJobTimeout(JobType type) {
        return (type == config.getExclusiveMode()) || (timeSource.secondsSince(modeChangeTime) > config.getTimeoutGraceSec());
    }

    @Override
    public Collection<SimpleTask> nextTaskGroup(TaskType taskType, Table<Long, TaskState, Number> taskStatesByType, Integer tasksQueueLimit) {
        JobType exclusiveMode = config.getExclusiveMode();
        if (compatibleMode(exclusiveMode, taskType) && !shouldCooldown(exclusiveMode)) {
            return super.nextTaskGroup(taskType, taskStatesByType, tasksQueueLimit);
        }
        else{
            return Sets.newHashSet();
        }
    }

}
