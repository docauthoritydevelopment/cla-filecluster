package com.cla.filecluster.service.files.managers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class ExecutionManagerService {

    private Thread executionThread;

    private ReentrantLock catFileOperationMutex = new ReentrantLock();


    private final static Logger logger = LoggerFactory.getLogger(ExecutionManagerService.class);

    @Value("${filecat.lock.wait.ms:50000}")
    private long fileCatLockWaitMs;

    @Value("${ingester.separateProcess.exec:run_ingester.bat}")
    private String ingestionProcessExec;

    private String lockerInfo = "-";
    private String lockerThreadName = "-";

    public Future<Void> startAsyncTask(Callable<Void> callable) {
        return Executors.newSingleThreadExecutor().submit(callable);
    }

    public<T> T runTaskWithTimeout(String taskName, Callable<T> callable, long timeoutMs) {
        Future<T> submit = Executors.newSingleThreadExecutor().submit(callable);
        try {
            return submit.get(timeoutMs,TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            logger.warn("Task {} was interrupted in the middle by the user",taskName,e);
            return null;
        } catch (ExecutionException e) {
            throw new RuntimeException("Task "+taskName+" failed",e);
        } catch (TimeoutException e) {
            logger.warn("Task {} was timed out",taskName);
            return null;
        }
    }

    /**
     * TODO - remove this function. All calls should be with startAsyncTask
     *
     * @param target        runnable
     * @param threadName    thread name
     */
    public void startAsyncExecution(Runnable target, String threadName) {
        executionThread = new Thread(target);
        if (threadName != null) {
            executionThread.setName(threadName);
        }
        executionThread.start();
    }

    public void setAsExecutionThread(Thread thread) {
        this.executionThread = thread;
    }

    public void lockFileCat(String lockerInfo) {
        if (catFileOperationMutex.isLocked()) {
            if (logger.isTraceEnabled()) {
                logger.trace("Thread {} about to lock fileCat info={}. Seems locked by {} info={}", Thread.currentThread().getName(), lockerInfo, this.lockerThreadName, this.lockerInfo);
            }
        } else {
            if (logger.isTraceEnabled()) {
                logger.trace("Thread {} Locks fileCat info={}", Thread.currentThread().getName(), lockerInfo);
            }
        }
        catFileOperationMutex.lock();
        this.lockerInfo = lockerInfo;
        this.lockerThreadName = Thread.currentThread().getName();
    }

    public void releaseFileCatLock() {
        if (logger.isTraceEnabled()) {
            logger.trace("Thread {} Unlocks fileCat. Info={}", Thread.currentThread().getName(), lockerInfo);
        }
        this.lockerInfo = "-";
        this.lockerThreadName = "-";

        try {
            catFileOperationMutex.unlock();
        } catch (IllegalMonitorStateException e) {
            logger.warn("Thread {} FAILED to Unlock fileCat info={}", Thread.currentThread().getName(), lockerInfo, e);
        }
    }

    public void lockFileCatOrThrow(String lockerInfo) {
        lockFileCatOrThrow(fileCatLockWaitMs, lockerInfo);
    }

    public void lockFileCatOrThrow(long timeoutMs, String lockerInfo) {
        try {
            if (catFileOperationMutex.isLocked()) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Thread {} about to lock fileCat info={}. Seems locked by {} info={}",
                            Thread.currentThread().getName(), lockerInfo, this.lockerThreadName, this.lockerInfo);
                }
            } else {
                if (logger.isTraceEnabled()) {
                    logger.trace("Thread {} Locks fileCat info={}", Thread.currentThread().getName(), lockerInfo);
                }
            }

            if (!catFileOperationMutex.tryLock(timeoutMs, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Unable to lock fileCat. Another write operation is currently in progress");
            }
        } catch (InterruptedException e) {
            logger.error("Failed to get a lock on fileCat after {} ms. It is currently locked", timeoutMs);
            throw new RuntimeException("Unable to lock fileCat. Another write operation is currently in progress");
        }
        this.lockerInfo = lockerInfo;
        this.lockerThreadName = Thread.currentThread().getName();
    }

}

