package com.cla.filecluster.service.categories;

import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.bizlist.SimpleBizListItemDto;
import com.cla.common.domain.dto.department.DepartmentAssociationDto;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.doctype.DocTypeAssociationDto;
import com.cla.common.domain.dto.doctype.DocTypeDto;
import com.cla.common.domain.dto.filetag.*;
import com.cla.common.domain.dto.scope.Scope;
import com.cla.common.domain.dto.scope.UserScopeDto;
import com.cla.common.domain.dto.scope.WorkGroupsScopeDto;
import com.cla.common.domain.dto.security.FunctionalRoleDto;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.scope.UserScope;
import com.cla.filecluster.domain.entity.scope.WorkGroupsScope;
import com.cla.filecluster.service.security.UserService;
import com.cla.filecluster.service.security.WorkGroupService;
import com.cla.filecluster.util.ActiveAssociationUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 3/31/2019
 */
public class AssociationUtils {

    private static final String BIZ_LIST_ITEM_ASSOCIATION_OLD = "BizList-Item-Association";
    private static final String BIZ_LIST_ITEM_ASSOCIATION = "Business-List-Item-Association";

    private static Logger logger = LoggerFactory.getLogger(AssociationUtils.class);

    public static Set<Long> getFunctionalRoles(Collection<AssociationEntity> associations) {
        if (associations == null) {
            return new HashSet<>();
        }
        return associations.stream()
                .map(AssociationEntity::getFunctionalRoleId)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }

    public static Map<Long, AssociationEntity> getDepartmentUnDeletedExplicitAssociations(List<AssociationEntity> associations) {
        Map<Long, AssociationEntity> departmentAssociations = new HashMap<>();

        if (associations != null && !associations.isEmpty()) {
            associations.forEach(association -> {
                if (association.getDepartmentId() != null && !association.getImplicit()) {
                    departmentAssociations.put(association.getDepartmentId(), association);
                }
            });
        }

        return departmentAssociations;
    }

    public static Map<Long, DepartmentAssociationDto> getDepartmentUndeletedExplicitAssociations(List<DepartmentAssociationDto> associations) {
        Map<Long, DepartmentAssociationDto> departmentAssociations = new HashMap<>();

        if (associations != null && !associations.isEmpty()) {
            associations.forEach(association -> {
                if (association.getDepartmentDto() != null && !association.isDeleted() && !association.isImplicit()) {
                    departmentAssociations.put(association.getDepartmentDto().getId(), association);
                }
            });
        }

        return departmentAssociations;
    }

    public static Scope convertBasicScopeToDto(BasicScope basicScope) {
        if (basicScope instanceof UserScope) {
            UserScopeDto userScopeDto = new UserScopeDto();
            userScopeDto.setDateCreated(basicScope.getDateCreated());
            userScopeDto.setDateModified(basicScope.getDateModified());
            userScopeDto.setId(basicScope.getId());
            userScopeDto.setScopeBindings(UserService.convertUser(((UserScope) basicScope).getScopeBindings(), false));
            return userScopeDto;
        } else if (basicScope instanceof WorkGroupsScope) {
            WorkGroupsScopeDto workGroupsScope = new WorkGroupsScopeDto();
            workGroupsScope.setDateCreated(basicScope.getDateCreated());
            workGroupsScope.setDateModified(basicScope.getDateModified());
            workGroupsScope.setId(basicScope.getId());
            workGroupsScope.setDescription(((WorkGroupsScope) basicScope).getDescription());
            workGroupsScope.setName(((WorkGroupsScope) basicScope).getName());
            workGroupsScope.setScopeBindings(WorkGroupService.convertWorkGroups(((WorkGroupsScope) basicScope).getScopeBindings()));
            return workGroupsScope;
        }
        return null;
    }

    public static List<FileTagAssociationDto> collectFileTagAssociations(Map<Long, BasicScope> scopes,
                                                                         List<AssociableEntityDto> fileAssociationEntities,
                                                                         Collection<AssociationEntity> fileTagAssociations,
                                                                         Collection<DocTypeAssociationDto> docTypeAssociations,
                                                                         Collection<AssociationEntity> userGroupAssociations,
                                                                         Collection<AssociationEntity> folderAssociations, boolean skipHiddenTags) {
        List<FileTagAssociationDto> fileTagAssociationDtos = Lists.newLinkedList();
        if (fileTagAssociations != null) {
            for (AssociationEntity association : fileTagAssociations) {
                if (association == null || association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
                if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                    continue;
                }
                fileTagAssociationDtos.add(
                        new FileTagAssociationDto(fileTagDto, association.getCreationDate(),
                                FileTagSource.FILE, scopes.get(association.getAssociationScopeId()),
                                null, false, null));
            }
        }
        if (userGroupAssociations != null) {
            for (AssociationEntity association : userGroupAssociations) {
                if (association == null || association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
                fileTagDto = new FileTagDto(fileTagDto);
                fileTagDto.setImplicit(true);
                Scope scope = convertBasicScopeToDto(scopes.get(association.getAssociationScopeId()));
                FileTagAssociationDto fileTagAssociationDto = new FileTagAssociationDto(
                        fileTagDto,
                        association.getCreationDate(),
                        FileTagSource.BUSINESS_ID,
                        scope, null, false, null);
                fileTagAssociationDtos.add(fileTagAssociationDto);
            }
            addFileTagAssociationsByDocTypeDto(docTypeAssociations, fileTagAssociationDtos, null);
        }

        //Set<Long> implicitTagsFound = new HashSet<>(); // Used to collapse implicit tags
        if (folderAssociations != null) {
            for (AssociationEntity association : folderAssociations) {
                if (association == null || association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
                fileTagDto = new FileTagDto(fileTagDto);
                fileTagDto.setImplicit(true);
                FileTagSource fileTagSource = association.getImplicit() ? FileTagSource.FOLDER_IMPLICIT : FileTagSource.FOLDER_EXPLICIT;
                Scope scope = convertBasicScopeToDto(scopes.get(association.getAssociationScopeId()));
                FileTagAssociationDto fileTagAssociationDto = new FileTagAssociationDto(
                        fileTagDto,
                        association.getCreationDate(),
                        fileTagSource,
                        scope, association.getOriginFolderId(), false, association.getOriginDepthFromRoot());
                fileTagAssociationDtos.add(fileTagAssociationDto);
            }
            addFileTagAssociationsByDocTypeDto(docTypeAssociations, fileTagAssociationDtos, null);
        }

        addFileTagAssociationsByDocTypeDto(docTypeAssociations, fileTagAssociationDtos, null);

        return fileTagAssociationDtos;
    }

    public static List<FileTagAssociationDto> collectGroupFileTagAssociations(Map<Long, BasicScope> scopes,
                                                                         Collection<AssociationEntity> groupAssociations,
                                                                         Collection<AssociationEntity> groupDocTypeAssociations,
                                                                         List<AssociableEntityDto> entities,
                                                                         boolean skipHiddenTags) {
        List<FileTagAssociationDto> fileTagAssociationDtos = new ArrayList<>();
        if (groupAssociations == null) return fileTagAssociationDtos;
        for (AssociationEntity association : groupAssociations) {
            if (association == null || association.getFileTagId() == null) {
                continue;
            }
            FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                    FileTagDto.class, association.getFileTagId() ,entities);
            if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                continue;
            }

            fileTagDto = new FileTagDto(fileTagDto);
            fileTagDto.setImplicit(association.getImplicit());
            Scope scope = association.getAssociationScopeId() == null ? null :
                    convertBasicScopeToDto(scopes.get(association.getAssociationScopeId()));
            fileTagAssociationDtos.add(new FileTagAssociationDto(
                    fileTagDto,
                    association.getCreationDate(),
                    FileTagSource.BUSINESS_ID,
                    scope,
                    association.getOriginFolderId(),
                    false,
                    association.getOriginDepthFromRoot()));
        }

        List<DocTypeAssociationDto> docTypeAssociationsItems = convertFromAssociationEntity(entities, scopes, FileTagSource.BUSINESS_ID, groupDocTypeAssociations);
        addFileTagAssociationsByDocTypeDto(docTypeAssociationsItems, fileTagAssociationDtos, null);
        return fileTagAssociationDtos;
    }

    public static List<FileTagAssociationDto> collectFileTagAssociations(Map<Long, BasicScope> scopes,
                                                                         List<AssociableEntityDto> fileAssociationEntities,
                                                                         Collection<AssociationEntity> folderAssociations,
                                                                         Collection<AssociationEntity> docTypeAssociations,
                                                                         boolean skipHiddenTags) {

        List<FileTagAssociationDto> fileTagAssociationDtos = new ArrayList<>();
        if (folderAssociations == null) return fileTagAssociationDtos;
        for (AssociationEntity association : folderAssociations) {
            if (association == null || association.getFileTagId() == null) {
                continue;
            }
            FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                    FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
            if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                continue;
            }

            fileTagDto = new FileTagDto(fileTagDto);
            fileTagDto.setImplicit(association.getImplicit());
            FileTagSource fileTagSource = association.getImplicit() ? FileTagSource.FOLDER_IMPLICIT : FileTagSource.FOLDER_EXPLICIT;
            Scope scope = convertBasicScopeToDto(scopes.get(association.getAssociationScopeId()));
            fileTagAssociationDtos.add(new FileTagAssociationDto(
                    fileTagDto,
                    association.getCreationDate(),
                    fileTagSource,
                    scope,
                    association.getOriginFolderId(),
                    false,
                    association.getOriginDepthFromRoot()));
        }

        List<DocTypeAssociationDto> docTypeAssociationsItems = convertFromAssociationEntity(fileAssociationEntities, scopes, FileTagSource.FILE, docTypeAssociations);
        addFileTagAssociationsByDocTypeDto(docTypeAssociationsItems, fileTagAssociationDtos, null);
        return fileTagAssociationDtos;
    }

    public static AssociableEntityDto getEntityFromList(Class assocType, Object id, List<AssociableEntityDto> associationEntities) {
        if (associationEntities != null) {
            for (AssociableEntityDto entity : associationEntities) {
                if (assocType.isInstance(entity) && entity.isIdMatch(id)) {
                    return entity;
                }
            }
        }
        return null;
    }

    public static Set<FunctionalRoleDto> collectAssociatedFunctionalRulesFromFile(List<AssociableEntityDto> fileAssociationEntities,
                                                                                  Collection<AssociationEntity> fileAssociations,
                                                                                  Collection<AssociationEntity> userGroupAssociations,
                                                                                  Collection<AssociationEntity> folderAssociations) {
        Set<FunctionalRoleDto> result = Sets.newHashSet();
        if (fileAssociations != null) {
            for (AssociationEntity association : fileAssociations) {
                if (association == null || association.getFunctionalRoleId() == null) {
                    continue;
                }
                collectFunctionalRoleAssociation(result, getEntityFromList(
                        FunctionalRoleDto.class, association.getFunctionalRoleId() ,fileAssociationEntities), false);
            }
        }

        if (userGroupAssociations != null) {
            for (AssociationEntity association : userGroupAssociations) {
                if (association == null || association.getFunctionalRoleId() == null) {
                    continue;
                }
                collectFunctionalRoleAssociation(result, getEntityFromList(
                        FunctionalRoleDto.class, association.getFunctionalRoleId() ,fileAssociationEntities), true);
            }
        }
        if (folderAssociations != null) {
            for (AssociationEntity association : folderAssociations) {
                if (association == null || association.getFunctionalRoleId() == null) {
                    continue;
                }
                collectFunctionalRoleAssociation(result, getEntityFromList(
                        FunctionalRoleDto.class, association.getFunctionalRoleId() ,fileAssociationEntities), true);
            }
        }
        return result;
    }

    private static void collectBizListItemAssociation(List<SimpleBizListItemDto> result, AssociableEntityDto associableEntity, Boolean implicit) {
        if (associableEntity instanceof SimpleBizListItemDto) {
            SimpleBizListItemDto e = new SimpleBizListItemDto((SimpleBizListItemDto) associableEntity);
            e.setImplicit(implicit);
            if (result.contains(e) && e.isImplicit()) {
                return;
            }
            if (implicit != null && !implicit) {
                //Replace implicit with explicit
                result.remove(e);
            }
            result.add(e);
        } else if (associableEntity instanceof FileTagDto) {
            FileTagDto fileTag = (FileTagDto) associableEntity;
            if (BIZ_LIST_ITEM_ASSOCIATION_OLD.equals(fileTag.getType().getName()) || BIZ_LIST_ITEM_ASSOCIATION.equals(fileTag.getType().getName())) {
                SimpleBizListItemDto simpleBizListItemDto = convertFileTagToBizListAssociation(fileTag);
                if (simpleBizListItemDto == null) {
                    logger.warn("Bad tag record: tag {}/{} of type {}/{} contains null BizListContext",
                            fileTag.getId(), fileTag.getName(), fileTag.getType().getId(), fileTag.getType().getName());
                } else {
                    simpleBizListItemDto.setImplicit(implicit);
                    result.add(simpleBizListItemDto);
                }
            }
        }
    }

    private static void collectFunctionalRoleAssociation(Collection<FunctionalRoleDto> result, AssociableEntityDto associableEntity, Boolean implicit) {
        if (associableEntity instanceof FunctionalRoleDto && !((FunctionalRoleDto) associableEntity).isDeleted()) {
            FunctionalRoleDto e = new FunctionalRoleDto((FunctionalRoleDto) associableEntity);
            e.setImplicit(implicit);
            if (result.contains(e) && e.getImplicit()) {
                return;
            }
            if (implicit != null && !implicit) {
                //Replace implicit with explicit
                result.remove(e);
            }
            result.add(e);
        }
    }

    public static Set<FunctionalRoleDto> collectAssociatedFunctionalRules(Collection<AssociationEntity> associations,
                                                                          List<AssociableEntityDto> entities) {
        Set<FunctionalRoleDto> functionalRoleDtos = Sets.newHashSet();
        if (associations != null) {
            for (AssociationEntity association : associations) {
                if (association.getFunctionalRoleId() != null) {
                    AssociableEntityDto functionalRoleDto =  getEntityFromList(FunctionalRoleDto.class, association.getFunctionalRoleId(), entities);
                    collectFunctionalRoleAssociation(functionalRoleDtos, functionalRoleDto, association.getImplicit());
                }
            }

        }
        return functionalRoleDtos;
    }

    public static List<SimpleBizListItemDto> collectAssociatedBizListItemsFromGroup(Collection<AssociationEntity> groupAssociations,
                                                                                    List<AssociableEntityDto> groupAssociationEntities) {
        List<SimpleBizListItemDto> result = new ArrayList<>();
        if (groupAssociations == null) return result;
        for (AssociationEntity association : groupAssociations) {
            if (association == null || association.getSimpleBizListItemSid() == null) {
                continue;
            }
            AssociableEntityDto item = getEntityFromList(SimpleBizListItemDto.class, association.getSimpleBizListItemSid(), groupAssociationEntities);
            collectBizListItemAssociation(result, item, association.getImplicit());
        }
        return result;
    }


    private static SimpleBizListItemDto convertFileTagToBizListAssociation(FileTagDto fileTag) {
        String tagName = fileTag.getName();
        if (fileTag.getEntityIdContext() == null) {
            return null;
        }
        Long bizListId = Long.valueOf(fileTag.getEntityIdContext());
        SimpleBizListItemDto bizListItemDto = new SimpleBizListItemDto();
        bizListItemDto.setBizListId(bizListId);
        bizListItemDto.setId(tagName);
        return bizListItemDto;
    }

    public static List<SimpleBizListItemDto> collectAssociatedBizListItemsFromFolder(Collection<AssociationEntity> associations,
                                                                                     List<AssociableEntityDto> folderAssociationEntities) {
        List<SimpleBizListItemDto> result = new ArrayList<>();
        if (associations == null) return result;
        for (AssociationEntity association : associations) {
            if (association == null || association.getSimpleBizListItemSid() == null) {
                continue;
            }
            AssociableEntityDto item = getEntityFromList(SimpleBizListItemDto.class, association.getSimpleBizListItemSid(), folderAssociationEntities);
            collectBizListItemAssociation(result, item, association.getImplicit());
        }
        return result;
    }

    public static Set<FunctionalRoleDto> collectAssociatedFunctionalRolesFromFolder(Collection<AssociationEntity> associations,
                                                                                    List<AssociableEntityDto> folderAssociationEntities) {
        Set<FunctionalRoleDto> result = Sets.newHashSet();
        if (associations == null) return result;
        for (AssociationEntity association : associations) {
            if (association == null || association.getFunctionalRoleId() == null) {
                continue;
            }
            AssociableEntityDto item = getEntityFromList(FunctionalRoleDto.class, association.getFunctionalRoleId(), folderAssociationEntities);
            collectFunctionalRoleAssociation(result, item, association.getImplicit());
        }
        return result;
    }

    public static List<DocTypeAssociationDto> convertFromAssociationEntity(List<AssociableEntityDto> fileAssociationEntities,
                                                                           Map<Long, BasicScope> scopes, FileTagSource source,
                                                                           Collection<AssociationEntity> docTypeAssociations) {
        List<DocTypeAssociationDto> result = new ArrayList<>();
        if (docTypeAssociations != null) {
            for (AssociationEntity fileAssociation : docTypeAssociations) {
                if (fileAssociation == null || fileAssociation.getDocTypeId() == null) {
                    continue;
                }
                DocTypeAssociationDto docTypeAssociation = convertToDocTypeAssociation(fileAssociation,
                        fileAssociationEntities, scopes, source, null);
                result.add(docTypeAssociation);
            }
        }
        return result;
    }

    public static void addFileTagAssociationsByDocTypeDto(Collection<DocTypeAssociationDto> docTypeAssociations,
                                                          List<FileTagAssociationDto> fileTagAssociationDtos,
                                                          Set<FileTagDto> fileTagDtos) {
        if (docTypeAssociations == null){
            return;
        }

        docTypeAssociations.stream()
                .filter(dta -> !dta.isDeleted() && dta.getDocTypeDto().getFileTagTypeSettings() != null)
                // pair up dta with it's associated file tag settings
                .map(dta -> Pair.of(dta, dta.getDocTypeDto().getFileTagTypeSettings()))
                // Filter only un added file tags (not to override explicit tags)
                // According to the amount of docType.fileTags add file tags associations
                .forEach(p -> p.getValue().stream().forEach(ftsp -> {
                    ftsp.getFileTags().stream().forEach(ft -> {
                        // Case file tag already exists ignore it
                        if (fileTagAssociationDtos.stream().anyMatch(fta -> Objects.equals(fta.getFileTagDto().getId(), ft.getId()))) {
                            return;
                        }
                        FileTagDto fileTagDto = new FileTagDto(ft);
                        fileTagDto.setImplicit(true);
                        Long originalFolderId = null;
                        DocTypeAssociationDto docTypeAssociation = p.getKey();

                        Scope scope = docTypeAssociation.getScope();
                        if (docTypeAssociation.getFileTagSource() == FileTagSource.FOLDER_EXPLICIT ||
                                docTypeAssociation.getFileTagSource() == FileTagSource.FOLDER_IMPLICIT) {
                            originalFolderId = docTypeAssociation.getOriginalFolderId();
                        }
                        FileTagAssociationDto fileTagAssociationDto = new FileTagAssociationDto(
                                fileTagDto,
                                docTypeAssociation.getAssociationTimeStamp(),
                                p.getKey().getFileTagSource(),
                                scope, originalFolderId, docTypeAssociation.isDeleted(), docTypeAssociation.getOriginDepthFromRoot());
                        fileTagAssociationDtos.add(fileTagAssociationDto);
                        if (fileTagDtos != null) {
                            fileTagDtos.add(fileTagDto);
                        }
                    });
                }));
    }

    public static List<SimpleBizListItemDto> collectAssociatedBizListItemsFromFile(List<AssociableEntityDto> fileAssociationEntities,
                                                                                   Collection<AssociationEntity> fileTagAssociations,
                                                                                   Collection<AssociationEntity> userGroupAssociations,
                                                                                   Collection<AssociationEntity> folderAssociations) {
        List<SimpleBizListItemDto> result = new ArrayList<>();
        if (fileTagAssociations != null) {
            for (AssociationEntity association : fileTagAssociations) {
                if (association == null || association.getSimpleBizListItemSid() == null) {
                    continue;
                }
                collectBizListItemAssociation(result, getEntityFromList(
                        SimpleBizListItemDto.class, association.getSimpleBizListItemSid() ,fileAssociationEntities), false);
            }
        }
        if (userGroupAssociations != null) {
            for (AssociationEntity association : userGroupAssociations) {
                if (association == null || association.getSimpleBizListItemSid() == null) {
                    continue;
                }
                collectBizListItemAssociation(result, getEntityFromList(
                        SimpleBizListItemDto.class, association.getSimpleBizListItemSid() ,fileAssociationEntities), true);
            }
        }
        if (folderAssociations != null) {
            for (AssociationEntity association : folderAssociations) {
                if (association == null || association.getSimpleBizListItemSid() == null) {
                    continue;
                }
                collectBizListItemAssociation(result, getEntityFromList(
                        SimpleBizListItemDto.class, association.getSimpleBizListItemSid() ,fileAssociationEntities), true);
            }
        }
        return result;
    }

    public static DepartmentDto getDepartmentDto(List<DepartmentAssociationDto> departmentAssociations) {
        DepartmentAssociationDto result = null;
        if (departmentAssociations != null) {
            for (DepartmentAssociationDto assoc : departmentAssociations) {
                if (result == null || result.getOriginDepthFromRoot() < assoc.getOriginDepthFromRoot()) {
                    result = assoc;
                }
            }
        }
        return result == null ? null : result.getDepartmentDto();
    }

    public static List<DepartmentAssociationDto> collectDepartmentAssociations(Collection<AssociationEntity> folderAssociations,
                                                                               List<AssociableEntityDto> fileAssociationEntities) {
        return collectDepartmentAssociations(folderAssociations, fileAssociationEntities, null);
    }

    public static List<DepartmentAssociationDto> collectDepartmentAssociations(Collection<AssociationEntity> folderAssociations,
                                                                               List<AssociableEntityDto> fileAssociationEntities, Boolean forceImplicit) {
        List<DepartmentAssociationDto> departmentAssociationDtos = new ArrayList<>();
        if (folderAssociations == null) return departmentAssociationDtos;
        for (AssociationEntity association : folderAssociations) {
            if (association == null || association.getDepartmentId() == null) {
                continue;
            }
            DepartmentDto departmentDto = (DepartmentDto)getEntityFromList(
                    DepartmentDto.class, association.getDepartmentId() ,fileAssociationEntities);
            departmentDto.setImplicit(forceImplicit != null ? forceImplicit : association.getImplicit());
            departmentAssociationDtos.add(new DepartmentAssociationDto(0L,
                    departmentDto, association.getCreationDate(),
                    association.getOriginFolderId(), association.getOriginDepthFromRoot(),
                    forceImplicit != null ? forceImplicit : association.getImplicit()
            ));
        }
        return departmentAssociationDtos;
    }

    public static Set<FileTagDto> collectFileTags(boolean skipHiddenTags,
                                                  List<AssociableEntityDto> fileAssociationEntities,
                                                  Collection<AssociationEntity> userGroupAssociations,
                                                  Collection<AssociationEntity> fileTagAssociations,
                                                  Collection<AssociationEntity> folderAssociations) {
        Set<FileTagDto> fileTagDtos = Sets.newHashSet();
        if (fileTagAssociations != null) {
            for (AssociationEntity association : fileTagAssociations) {
                if (association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
                if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                    continue;
                }
                fileTagDtos.add(fileTagDto);
            }
        }

        if (userGroupAssociations != null) {
            for (AssociationEntity association : userGroupAssociations) { //claFile.getUserGroup().getNonDeletedAssociations()
                if (association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
                if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                    continue;
                }
                fileTagDto = new FileTagDto(fileTagDto);
                fileTagDto.setImplicit(true);
                fileTagDtos.add(fileTagDto);
            }
        }

        if (folderAssociations != null) {
            for (AssociationEntity association : folderAssociations) {
                if (association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(
                        FileTagDto.class, association.getFileTagId() ,fileAssociationEntities);
                if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                    continue;
                }
                fileTagDto = new FileTagDto(fileTagDto);
                fileTagDto.setImplicit(true);
                fileTagDtos.add(fileTagDto);
            }
        }
        return fileTagDtos;
    }

    public static Set<DocTypeDto> collectDocTypes(Map<Long, BasicScope> scopes,
                                                  List<AssociableEntityDto> fileAssocEntities,
                                                  Collection<AssociationEntity> associations) {
        Set<DocTypeDto> docTypeDtos = new HashSet<>();
        if (associations == null) return docTypeDtos;
        for (AssociationEntity association : associations) {
            if (association == null || association.getDocTypeId() == null) {
                continue;
            }
            DocTypeDto docTypeDto = (DocTypeDto)getEntityFromList(
                    DocTypeDto.class, association.getDocTypeId() ,fileAssocEntities);
            docTypeDto = new DocTypeDto(docTypeDto);
            docTypeDto.setImplicit(false);
            if (!docTypeDto.isImplicit() && docTypeDtos.contains(docTypeDto)) {
                //Replace implicit with explicit
                boolean remove = docTypeDtos.remove(docTypeDto);
                if (!remove) { //Should never happen
                    logger.warn("Failed to remove docType {} from set.", docTypeDto);
                }
            }
            docTypeDtos.add(docTypeDto);
        }
        return docTypeDtos;
    }

    public static Set<DocTypeDto> collectDocTypesDto(Collection<DocTypeAssociationDto> associations) {
        Set<DocTypeDto> docTypeDtos = new HashSet<>();
        if (associations == null) return docTypeDtos;
        for (DocTypeAssociationDto association : associations) {
            if (association == null || association.isDeleted()) {
                continue;
            }
            DocTypeDto docTypeDto = association.getDocTypeDto();
            docTypeDto = new DocTypeDto(docTypeDto);
            docTypeDto.setImplicit(association.isImplicit());
            if (!docTypeDto.isImplicit() && docTypeDtos.contains(docTypeDto)) {
                //Replace implicit with explicit
                boolean remove = docTypeDtos.remove(docTypeDto);
                if (!remove) { //Should never happen
                    logger.warn("Failed to remove docType {} from set.", docTypeDto);
                }
            }
            docTypeDtos.add(docTypeDto);
        }
        return docTypeDtos;
    }

    public static Set<FileTagDto> collectFileTags(Collection<AssociationEntity> folderAssociations,
                                                  List<AssociableEntityDto> folderAssociationEntities,
                                                  List<FileTagAssociationDto> fileAssociations, boolean skipHiddenTags) {
        Set<FileTagDto> fileTagDtos = new HashSet<>();

        if (folderAssociations != null) {
            for (AssociationEntity association : folderAssociations) {
                if (association == null) {
                    continue;
                }
                if (association.getFileTagId() == null) {
                    continue;
                }
                FileTagDto fileTagDto = (FileTagDto)getEntityFromList(FileTagDto.class, association.getFileTagId(), folderAssociationEntities);
                if (isHiddenTag(skipHiddenTags, fileTagDto)) {
                    continue;
                }
                fileTagDto = new FileTagDto(fileTagDto);
                fileTagDto.setImplicit(association.getImplicit());
                if (!association.getImplicit()) {
                    //Replace implicit with explicit
                    fileTagDtos.remove(fileTagDto);
                }
                fileTagDtos.add(fileTagDto);
            }
        }

        if (fileAssociations != null) {
            for (FileTagAssociationDto association : fileAssociations) {
                if (association == null) {
                    continue;
                }
                if (association.getFileTagDto() == null) {
                    continue;
                }
                if (skipHiddenTags && association.getFileTagDto().getType().isHidden()) {
                    continue;
                }
                FileTagDto fileTagDto = association.getFileTagDto();
                fileTagDto = new FileTagDto(fileTagDto);
                fileTagDto.setImplicit(association.isImplicit());
                if (!association.isImplicit()) {
                    //Replace implicit with explicit
                    fileTagDtos.remove(fileTagDto);
                }
                fileTagDtos.add(fileTagDto);
            }
        }

        return fileTagDtos;
    }

    private static boolean isHiddenTag(boolean skipHiddenTags, @NotNull FileTagInterface fileTag) {
        return skipHiddenTags && fileTag.isTypeHidden();
    }

    public static List<DocTypeAssociationDto> collectDocTypeAssociations(Collection<AssociationEntity> docTypeAssociations,
                                                                         Map<Long, BasicScope> scopes, FileTagSource source,
                                                                         List<AssociableEntityDto> items) {
        List<DocTypeAssociationDto> result = new ArrayList<>();
        if (docTypeAssociations == null) return result;

        return AssociationUtils.convertFromAssociationEntity(items, scopes, source, docTypeAssociations);
    }

    public static List<DocTypeAssociationDto> mergeDocTypeAssociations(
            Map<Long, BasicScope> scopes,
            List<AssociableEntityDto> fileAssociationEntities,
            Collection<AssociationEntity> fileDocTypeAssociations,
            Collection<AssociationEntity> folderDocTypeAssociations,
            Collection<AssociationEntity> groupDocTypeAssociations) {
        List<DocTypeAssociationDto> result = new ArrayList<>();
        if (fileDocTypeAssociations != null) {
            for (AssociationEntity fileAssociation : fileDocTypeAssociations) {
                if (fileAssociation == null || fileAssociation.getDocTypeId() == null) {
                    continue;
                }
                DocTypeAssociationDto docTypeAssociation = convertToDocTypeAssociation(fileAssociation,
                        fileAssociationEntities, scopes, FileTagSource.FILE,false);
                result.add(docTypeAssociation);
            }
        }
        if (result.isEmpty() && groupDocTypeAssociations != null) {
            for (AssociationEntity docTypeAssociation : groupDocTypeAssociations) {
                if (docTypeAssociation == null || docTypeAssociation.getDocTypeId() == null) {
                    continue;
                }
                DocTypeAssociationDto docTypeAssociationDto = convertToDocTypeAssociation(docTypeAssociation, fileAssociationEntities,
                        scopes, FileTagSource.BUSINESS_ID, true);
                result.add(docTypeAssociationDto);
            }
        }
        if (result.isEmpty() && folderDocTypeAssociations != null) {
            for (AssociationEntity folderDocTypeAssociation : folderDocTypeAssociations) {
                if (folderDocTypeAssociation == null || folderDocTypeAssociation.getDocTypeId() == null) {
                    continue;
                }
                FileTagSource source = (folderDocTypeAssociation.getImplicit() ?
                        FileTagSource.FOLDER_IMPLICIT : FileTagSource.FOLDER_EXPLICIT);
                DocTypeAssociationDto docTypeAssociation = convertToDocTypeAssociation(folderDocTypeAssociation,
                        fileAssociationEntities, scopes, source,true);
                result.add(docTypeAssociation);
            }
        }
        return result;
    }

    public static DocTypeAssociationDto convertToDocTypeAssociation(AssociationEntity association,
                                                                    List<AssociableEntityDto> entities,
                                                                    Map<Long, BasicScope> scopes,
                                                                    FileTagSource associationSource,
                                                                    Boolean implicitForced) {
        DocTypeDto docType = (DocTypeDto)AssociationUtils.getEntityFromList(
                DocTypeDto.class, association.getDocTypeId() ,entities);
        docType = new DocTypeDto(docType);
        DocTypeAssociationDto docTypeAssociation = new DocTypeAssociationDto();
        docTypeAssociation.setDocTypeDto(docType);
        docType.setImplicit(implicitForced != null ? implicitForced :
                association.getImplicit() == null ? false : association.getImplicit());
        docTypeAssociation.setDeleted(false);
        docTypeAssociation.setAssociationTimeStamp(association.getCreationDate());
        if (association.getAssociationScopeId() != null && scopes != null) {
            docTypeAssociation.setScope(scopes.get(association.getAssociationScopeId()));
        }
        docTypeAssociation.setOriginalFolderId(association.getOriginFolderId());
        docTypeAssociation.setOriginDepthFromRoot(association.getOriginDepthFromRoot());
        docTypeAssociation.setFileTagSource(associationSource);
        return docTypeAssociation;
    }

    public static List<DocTypeAssociationDto> collectDocTypeAssociationsForFile(
            Map<Long, BasicScope> scopes,
            List<AssociableEntityDto> fileAssociationEntities,
            Collection<AssociationEntity> fileDocTypeAssociations,
            Collection<AssociationEntity> folderDocTypeAssociations,
            Collection<AssociationEntity> groupDocTypeAssociations) {
        List<DocTypeAssociationDto> result = new ArrayList<>();
        if (fileDocTypeAssociations != null) {
            for (AssociationEntity fileAssociation : fileDocTypeAssociations) {
                if (fileAssociation == null || fileAssociation.getDocTypeId() == null) {
                    continue;
                }
                DocTypeAssociationDto docTypeAssociation = convertToDocTypeAssociation(fileAssociation,
                        fileAssociationEntities, scopes, FileTagSource.FILE, false);
                docTypeAssociation.setFileTagSource(FileTagSource.FILE);
                result.add(docTypeAssociation);
            }
        }
        if (result.isEmpty() && groupDocTypeAssociations != null) {
            for (AssociationEntity docTypeAssociation : groupDocTypeAssociations) {
                if (docTypeAssociation == null) {
                    continue;
                }
                FileTagSource source = (FileTagSource.BUSINESS_ID);
                DocTypeAssociationDto docTypeAssociationDto = convertToDocTypeAssociation(docTypeAssociation,
                        fileAssociationEntities, scopes, source,true);
                result.add(docTypeAssociationDto);
            }
        }
        if (result.isEmpty() && folderDocTypeAssociations != null) {
            for (AssociationEntity folderDocTypeAssociation : folderDocTypeAssociations) {
                if (folderDocTypeAssociation == null) {
                    continue;
                }
                FileTagSource source = (folderDocTypeAssociation.getImplicit() ?
                        FileTagSource.FOLDER_IMPLICIT : FileTagSource.FOLDER_EXPLICIT);
                DocTypeAssociationDto docTypeAssociation = convertToDocTypeAssociation(folderDocTypeAssociation,
                        fileAssociationEntities, scopes, source,true);
                result.add(docTypeAssociation);
            }
        }
        return result;
    }

    public static void setActiveAssociationValues(Collection<String> ids, Set<String> activeAssociations) {
        if (ids != null && !ids.isEmpty()) {
            ids.forEach(id -> {
                if (id != null) {
                    String val = ActiveAssociationUtils.getActiveAssociationIdentifier(id);
                    activeAssociations.add(val);
                }
            });
        }
    }
}
