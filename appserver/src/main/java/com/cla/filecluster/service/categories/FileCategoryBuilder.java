package com.cla.filecluster.service.categories;

import com.cla.common.domain.dto.category.FileCategoryScope;
import com.cla.common.domain.dto.category.FileCategoryState;
import com.cla.common.domain.dto.category.FileCategoryType;
import com.cla.filecluster.domain.entity.categorization.*;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uri on 23/07/2015.
 */
@Service
public class FileCategoryBuilder {

    @Value("${categories.ungrouped.name:UnGrouped}")
    private String unGroupedCategoryName;

    Logger logger = LoggerFactory.getLogger(FileCategoryBuilder.class);
    /**
     * They must be of the same scope and type
     * @param fileCategoriesToMerge
     * @return
     */
    public FileCategory buildFileCategoryFromFileCategories(String newCategoryName, List<FileCategory> fileCategoriesToMerge) {
        FileCategory firstCategory = fileCategoriesToMerge.get(0);
        FileCategory newCategory = createNewCategory(newCategoryName, firstCategory.getType(), firstCategory.getScope());
        //Set the filters
        switch (firstCategory.getType()) {
            case BUSINESS_ID_FILTER:
                List<CategoryFilter> businessIdFilterList = createBusinessIdFilterList(fileCategoriesToMerge);
                newCategory.setFilters(businessIdFilterList);
                break;
        }
        return newCategory;
    }

    private List<CategoryFilter> createBusinessIdFilterList(List<FileCategory> fileCategoriesToMerge) {
        List<String> groupIds = new ArrayList<>();
        fileCategoriesToMerge.forEach(f -> groupIds.add(f.getFirstFilter().getValue()));
        List<CategoryFilter> categoryFilters = buildMultiGroupFileCategoryFilters(groupIds);
        return categoryFilters;
    }

    private FileCategory createNewCategory(String newCategoryName, FileCategoryType type, FileCategoryScope scope) {
        FileCategory fileCategory = new FileCategory();
        fileCategory.setName(newCategoryName);
        fileCategory.setType(type);
        fileCategory.setScope(scope);
        fileCategory.setState(FileCategoryState.CREATING);
        return fileCategory;
    }

    public boolean isValidGroupForCategory(FileGroup fileGroup) {
        String categoryName = fileGroup.getName() != null ? fileGroup.getName() : fileGroup.getGeneratedName();
        if (categoryName == null) {
            logger.warn("FileGroup id {} has no name - it is not valid group and can't become a category",fileGroup.getId());
            return false;
        }
        return true;
    }
    public FileCategory buildGroupFileCategory(FileGroup fileGroup) {
        FileCategory fileCategory = new FileCategory();
        String categoryName = fileGroup.getName() != null ? fileGroup.getName() : fileGroup.getGeneratedName();
        fileCategory.setName(categoryName);
        fileCategory.setStaticCategory(true);
        fileCategory.setFilters(buildGroupFileCategoryFilters(fileGroup));
        fileCategory.setScope(FileCategoryScope.ACCOUNT);
        fileCategory.setType(FileCategoryType.BUSINESS_ID_FILTER);
        return fileCategory;
    }

    public String getUnGroupedCategoryName() {
        return unGroupedCategoryName;
    }

//    public FileCategory buildTheUnGroupedFileCategory() {
//        FileCategory fileCategory = new FileCategory();
//        fileCategory.setName(unGroupedCategoryName);
//        fileCategory.setStaticCategory(true);
//        CategoryFilter missingValueFilter = new MissingValueCategoryFilter("groupId");
//        fileCategory.setFilters(buildSingleFilterList(missingValueFilter));
//        fileCategory.setScope(FileCategoryScope.ACCOUNT);
//        fileCategory.setType(FileCategoryType.BUSINESS_ID_FILTER);
//        return fileCategory;
//    }

    private List<CategoryFilter> buildSingleFilterList(CategoryFilter categoryFilter) {
        List<CategoryFilter> categoryFilters = new ArrayList<>();
        categoryFilters.add(categoryFilter);
        return categoryFilters;
    }

    private List<CategoryFilter> buildGroupFileCategoryFilters(FileGroup fileGroup) {
        return buildSingleFilterList(new GroupCategoryFilter(fileGroup.getId()));
    }

    private List<CategoryFilter> buildMultiGroupFileCategoryFilters(List<String> groupIds) {
        return buildSingleFilterList(new MultiGroupCategoryFilter(groupIds));
    }

}
