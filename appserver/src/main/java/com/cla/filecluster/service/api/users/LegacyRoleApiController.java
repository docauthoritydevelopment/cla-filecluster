package com.cla.filecluster.service.api.users;

import com.cla.common.domain.dto.security.SystemRoleDto;
import com.cla.filecluster.domain.entity.security.Role;
import com.cla.filecluster.service.security.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by uri on 17/07/2016.
 */
@RestController
@RequestMapping("/roles")
public class LegacyRoleApiController {

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "")
    public List<SystemRoleDto> getRolesWithAuthorities() {
        List<Role> rolesWithAuthorities = roleService.getRolesWithAuthorities();
        return RoleService.convertRoles(rolesWithAuthorities);
    }
}
