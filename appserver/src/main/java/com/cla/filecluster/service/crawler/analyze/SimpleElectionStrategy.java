package com.cla.filecluster.service.crawler.analyze;

import com.cla.filecluster.domain.dto.SolrFileGroupEntity;
import com.cla.filecluster.repository.solr.SolrFileGroupRepository;
import com.cla.filecluster.service.filetag.FileTagService;

import java.util.Collection;
import java.util.stream.Collectors;

public class SimpleElectionStrategy implements JoinGroupsElectionStrategy{

    @Override
    public JoinGroupsElectionResult electTargetGroup(Collection<SolrFileGroupEntity> candidateGroups, SolrFileGroupRepository fileGroupRepository, FileTagService fileTagService) {
        JoinGroupsElectionResult result = new JoinGroupsElectionResult();
        result.setElectedGroup(candidateGroups.stream().findFirst().orElse(null));
        result.getAcceptedGroups().addAll(candidateGroups.stream().skip(1).collect(Collectors.toSet()));
        return result;
    }

    @Override
    public void setRejectSizeThreshold(long rejectSizeThreshold) {
        // not implemented
    }

    @Override
    public boolean requiresFullGroupObjects() {
        return false;
    }
}
