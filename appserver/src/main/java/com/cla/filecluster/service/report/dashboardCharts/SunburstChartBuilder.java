package com.cla.filecluster.service.report.dashboardCharts;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartPointDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.SolrFolderEntity;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.files.filetree.DocFolderService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.report.DashboardChartDtoBuilder;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class SunburstChartBuilder implements DashboardChartDtoBuilder {

    private static Logger logger = LoggerFactory.getLogger(SunburstChartBuilder.class);

    @Override
    public ChartType getType() {
        return ChartType.SUNBURST_CHART;
    }

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private DocStoreService docStoreService;

    private static final String OTHER_CHILD_NAME = "Other";
    private static final int QUERY_LIMIT_ITEMS = Integer.MAX_VALUE;

    public String getAuditMsg(Map<String, String> params) {
        return "View sunburst chart";
    }


    private DashboardChartDataDto parseSolrResponseToDtoWithMinDepth(List<Long> rfList, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse,
                                                                     DashboardChartValueTypeDto valueField, int maxDepth, int minDepth) {
        DashboardChartDataDto ans = new DashboardChartDataDto();
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse
                .getResponseBucket(CatFileFieldType.FOLDER_ID.getSolrName()).getBuckets();

        Map<Long, Double> foundPointIdsAndValues = new HashMap<>();
        Map<Long, Double> foundPointIdsAndLevelValues = new HashMap<>();

        List<DashboardChartPointDataDto> points = new ArrayList<>();
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            Long folderId = bucket.getLongVal();
            Double countValue = dashboardChartsUtil.parseInnerBucketValue(ans, valueField, true, bucket);
            foundPointIdsAndValues.put(folderId, countValue);
            foundPointIdsAndLevelValues.put(folderId, countValue);
        }
        List<SolrFolderEntity> fList =  docFolderService.getByMinAndMaxDepth(minDepth, maxDepth);
        List<DashboardChartPointDataDto> allPoints = new ArrayList<>();
        for (SolrFolderEntity dFolder : fList) {
            Double value = foundPointIdsAndValues.getOrDefault(dFolder.getId(), 0d);
            if (value > 0 && dFolder.getDepthFromRoot() >= minDepth) {
                Long parentFolderId = dFolder.getParentFolderId();
                DashboardChartPointDataDto fPoint = new DashboardChartPointDataDto();
                fPoint.setId(String.valueOf(dFolder.getId()));
                fPoint.setParent(String.valueOf(parentFolderId));
                fPoint.setName(dFolder.getName());
                fPoint.setDescription(dFolder.getRealPath());
                fPoint.setValue(value);
                fPoint.setDirectValue(foundPointIdsAndLevelValues.getOrDefault(dFolder.getId(), 0d));
                fPoint.setDepth(dFolder.getDepthFromRoot());
                allPoints.add(fPoint);
                foundPointIdsAndValues.put(parentFolderId, foundPointIdsAndValues.containsKey(parentFolderId) ?
                        foundPointIdsAndValues.get(parentFolderId) + value : value);


            }
        }
        Map<String, Integer> colorIndexMap = new HashMap<>();
        int colorIndex = 0;
        allPoints.sort(new SortByDepth());
        List<DashboardChartPointDataDto> finalPoints = new ArrayList<>();
        for (int downCount = allPoints.size()-1; downCount >= 0 ; downCount-- ){
            DashboardChartPointDataDto fPoint = allPoints.get(downCount);
            if (fPoint.getDepth().equals(minDepth)){
                fPoint.setColorIndex(colorIndex++);
                fPoint.setParent("");
            }
            else if (colorIndexMap.get(fPoint.getParent()) == null){
                logger.error("Got illegal point id "+ fPoint.getId() + ",ParentId : "+ fPoint.getParent());
                continue;
            }
            else {
                fPoint.setColorIndex(colorIndexMap.get(fPoint.getParent()));
            }

            colorIndexMap.put(fPoint.getId(), fPoint.getColorIndex());
            finalPoints.add(fPoint);
        }
        ans.setPoints(finalPoints);
        return ans;
    }


    private DashboardChartDataDto parseSolrResponseToDto(List<Long> rfList, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse,
                                                         DashboardChartValueTypeDto valueField, int maxDepth) {
        DashboardChartDataDto ans = new DashboardChartDataDto();
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse
                .getResponseBucket(CatFileFieldType.FOLDER_ID.getSolrName()).getBuckets();

        Map<Long, Double> foundPointIdsAndValues = new HashMap<>();
        Map<Long, Double> foundPointIdsAndLevelValues = new HashMap<>();

        List<DashboardChartPointDataDto> points = new ArrayList<>();
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            Long folderId = bucket.getLongVal();
            Double countValue = dashboardChartsUtil.parseInnerBucketValue(ans, valueField, true, bucket);
            foundPointIdsAndValues.put(folderId, countValue);
            foundPointIdsAndLevelValues.put(folderId, countValue);
        }
        List<RootFolder> allRootFolders = docStoreService.getRootFolders();
        int colorIndex = 0;
        for (RootFolder rFolder : allRootFolders) {
            String rfName = Strings.isNullOrEmpty(rFolder.getNickName()) ? ( Strings.isNullOrEmpty(rFolder.getRealPath()) ? rFolder.getMailboxGroup() : rFolder.getRealPath() ) : rFolder.getNickName();
            List<DashboardChartPointDataDto> rfPoints = new ArrayList<>();
            List<SolrFolderEntity> fList = docFolderService.getForRootFolderByDepth(rFolder.getId(), maxDepth);
            for (SolrFolderEntity dFolder : fList) {
                Double value = foundPointIdsAndValues.getOrDefault(dFolder.getId(), 0d);
                if (value > 0 ) {
                    if (dFolder.getParentFolderId() != null) {
                        Long parentFolderId = dFolder.getParentFolderId() != null ? dFolder.getParentFolderId() : dFolder.getRootFolderId();
                        DashboardChartPointDataDto fPoint = new DashboardChartPointDataDto();
                        fPoint.setId(String.valueOf(dFolder.getId()));
                        fPoint.setParent(String.valueOf(parentFolderId));
                        fPoint.setName(dFolder.getName());
                        fPoint.setDescription(dFolder.getRealPath());
                        fPoint.setValue(value);
                        fPoint.setDirectValue(foundPointIdsAndLevelValues.getOrDefault(dFolder.getId(), 0d));
                        fPoint.setDepth(dFolder.getDepthFromRoot());
                        rfPoints.add(fPoint);
                        foundPointIdsAndValues.put(parentFolderId, foundPointIdsAndValues.containsKey(parentFolderId) ?
                                foundPointIdsAndValues.get(parentFolderId) + value : value);
                    }
                    else {
                        DashboardChartPointDataDto point = new DashboardChartPointDataDto();
                        point.setId(String.valueOf(dFolder.getId()));
                        point.setParent("");
                        point.setName(rfName);
                        point.setDescription(rfName);
                        point.setValue(value);
                        point.setDirectValue(foundPointIdsAndLevelValues.getOrDefault(dFolder.getId(),0d));
                        rfPoints.add(point);
                    }

                }
            }
            for (DashboardChartPointDataDto fPoint : rfPoints) {
                fPoint.setRfId(String.valueOf(rFolder.getId()));
                fPoint.setColorIndex(colorIndex);
            }
            points.addAll(rfPoints);
            colorIndex++;
        }
        ans.setPoints(points);
        return ans;
    }


    private List<Long> getRootFoldersIdList(SolrSpecification solrSpecification, DashboardChartValueTypeDto valueField, FilterDescriptor filter) {
        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(CatFileFieldType.ROOT_FOLDER_ID.getSolrName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(CatFileFieldType.ROOT_FOLDER_ID);
        if (valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            theFacet.setSortField(FacetSortType.COUNT.getName());
            theFacet.setSortDirection(valueField.getSortDirection());
        } else {
            dashboardChartsUtil.addInnerFacetsAccordingToValue(valueField, true, theFacet);
        }
        List<String> filterStrList = new ArrayList<>();
        if (filter != null && (filter.getValue() != null || !filter.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filter, solrSpecification));
        }
        if (!filterStrList.isEmpty()) {
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }
        solrSpecification.addFacetJsonObject(theFacet);
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = solrFileCatRepository.runFacetQueryJson(solrSpecification);
        List<SolrFacetJsonResponseBucket> buckets = solrFacetQueryJsonResponse
                .getResponseBucket(CatFileFieldType.ROOT_FOLDER_ID.getSolrName()).getBuckets();

        List<Long> ans = new ArrayList<>();
        for (SolrFacetJsonResponseBucket bucket : buckets) {
            Long rootFolderId = bucket.getLongVal();
            ans.add(rootFolderId);
        }
        return ans;
    }

    private SolrFacetQueryJson buildSunburstJsonFacet(List<Long> rfList, DashboardChartValueTypeDto valueField,
                                                      FilterDescriptor filter, SolrSpecification solrSpecification) {
        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(CatFileFieldType.FOLDER_ID.getSolrName());
        theFacet.setLimit(QUERY_LIMIT_ITEMS);
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(CatFileFieldType.FOLDER_ID);
        if (valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            theFacet.setSortField(FacetSortType.COUNT.getName());
            theFacet.setSortDirection(valueField.getSortDirection());
        } else {
            dashboardChartsUtil.addInnerFacetsAccordingToValue(valueField, true, theFacet);
        }
        List<String> filterStrList = new ArrayList<>();
        if (filter != null && (filter.getValue() != null || !filter.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filter, solrSpecification));
        }
        if (rfList.size() > 0) {
            filterStrList.add(CatFileFieldType.ROOT_FOLDER_ID.inQuery(
                    rfList.stream()
                            .map(Object::toString)
                            .map(DashboardChartsUtil::fixSingleSolrSlashes)
                            .map(DashboardChartsUtil::addDoubleQuoteIfNeeded)
                            .collect(Collectors.toList())));
        }
        if (!filterStrList.isEmpty()) {
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }
        return theFacet;
    }

    private class SortByValue implements Comparator<DashboardChartPointDataDto>
    {
        public int compare(DashboardChartPointDataDto a, DashboardChartPointDataDto b)
        {
            if (a.getValue().equals(b.getValue())) {
                return 0;
            }
            else {
                return (a.getValue() - b.getValue() > 0) ? -1 : 1;
            }
        }
    }

    private class SortByDepth implements Comparator<DashboardChartPointDataDto>
    {
        public int compare(DashboardChartPointDataDto a, DashboardChartPointDataDto b)
        {
            if (a.getDepth().equals(b.getDepth())) {
                return 0;
            }
            else {
                return (a.getDepth() - b.getDepth() > 0) ? -1 : 1;
            }
        }
    }

    private void limitPointChildren(List<DashboardChartPointDataDto> resultList, List<DashboardChartPointDataDto> childList, HashMap<String , List<DashboardChartPointDataDto>> pointsMap, int maxItems, boolean returnOther) {
        if (childList.size() == 0 ) {
            return;
        }
        childList.sort(new SortByValue());
        Double otherValue = 0d ;
        for (int count = 0; count < childList.size() ; count++) {
            if (count < maxItems) {
                resultList.add(childList.get(count));
                if (pointsMap.containsKey(childList.get(count).getId())){
                    limitPointChildren(resultList, pointsMap.get(childList.get(count).getId()), pointsMap, maxItems, returnOther);
                }
            }
            else if (returnOther) {
                otherValue = otherValue + childList.get(count).getValue();
            }
        }
        if (otherValue > 0 && returnOther){
            DashboardChartPointDataDto otherPoint = new DashboardChartPointDataDto();
            otherPoint.setParent(childList.get(0).getParent());
            otherPoint.setDepth(childList.get(0).getDepth());
            otherPoint.setRfId(childList.get(0).getRfId());
            otherPoint.setColorIndex(childList.get(0).getColorIndex());
            otherPoint.setDirectValue(otherValue);
            otherPoint.setValue(otherValue);
            otherPoint.setName(OTHER_CHILD_NAME);
            otherPoint.setDescription(OTHER_CHILD_NAME);
            resultList.add(otherPoint);
        }
    }

    public List<DashboardChartPointDataDto> fixPointsAccordingToMaxChildren(List<DashboardChartPointDataDto> points, int maxItems, boolean returnOther) {
        HashMap<String , List<DashboardChartPointDataDto>> pointsMap = new HashMap<>();
        List<DashboardChartPointDataDto> rootLevelList = new ArrayList<>();
        for (DashboardChartPointDataDto point : points) {
            if (point.getParent() == null || point.getParent().equals("")) {
                rootLevelList.add(point);
            }
            else {
                if (!pointsMap.containsKey(point.getParent())) {
                    pointsMap.put(point.getParent(), new ArrayList<>());
                }
                pointsMap.get(point.getParent()).add(point);
            }
        }
        List<DashboardChartPointDataDto> ansList = new ArrayList<>();
        limitPointChildren(ansList, rootLevelList, pointsMap, maxItems, returnOther);
        return ansList;
    }


    public void updatePercentageValue(List<DashboardChartPointDataDto> points) {
        //Map<Integer, Double> maxValueMap = new HashMap<>();
        Double maxValue = 0d;
        for (DashboardChartPointDataDto fPoint :   points) {
            if (maxValue < fPoint.getDirectValue()) {
                maxValue = fPoint.getDirectValue();
            }
            /* if (!maxValueMap.containsKey(fPoint.getColorIndex())){
                maxValueMap.put(fPoint.getColorIndex(), fPoint.getDirectValue());
            }
            else if (maxValueMap.get(fPoint.getColorIndex()) < fPoint.getDirectValue()) {
                maxValueMap.put(fPoint.getColorIndex(), fPoint.getDirectValue());
            }*/
        };
        for (DashboardChartPointDataDto fPoint :   points) {
            // fPoint.setPercentageValue(100 * fPoint.getDirectValue() / maxValueMap.get(fPoint.getColorIndex()));
            fPoint.setPercentageValue(10 + 90 * fPoint.getDirectValue() / maxValue);
        }
    }


    public DashboardChartDataDto buildChartDataDto(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        DashboardChartValueTypeDto valueField = dashboardChartsUtil.extractChartValueField(params);
        FilterDescriptor filterField = dashboardChartsUtil.extractFilterField(params);
        int maxDepth = dashboardChartsUtil.extractMaxDepth(params);
        int minDepth = dashboardChartsUtil.extractMinDepth(params);
        int maxItems = dashboardChartsUtil.extractMaxChildItems(params);
        boolean returnOther = dashboardChartsUtil.extractReturnOther(params);

        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(dataSourceRequest);
        List<Long> rfList = getRootFoldersIdList(solrFacetSpecification, valueField, filterField);
        if (rfList.size() == 0 ) {
            DashboardChartDataDto ans = new DashboardChartDataDto();
            ans.setPoints(new ArrayList<>());
            return ans;
        }
        solrFacetSpecification.setPageRequest(PageRequest.of(0, Integer.MAX_VALUE));
        solrFacetSpecification.setFacetJsonObjectList(new ArrayList<>());
        solrFacetSpecification.addFacetJsonObject(buildSunburstJsonFacet(rfList, valueField , filterField , solrFacetSpecification));
        SolrFacetQueryJsonResponse solrFacetQueryJsonResponse = solrFileCatRepository.runFacetQueryJson(
                solrFacetSpecification);

        DashboardChartDataDto ans;
        if (minDepth == 0) {
            ans = parseSolrResponseToDto(rfList, solrFacetQueryJsonResponse, valueField, maxDepth);
        }
        else {
            ans = parseSolrResponseToDtoWithMinDepth(rfList, solrFacetQueryJsonResponse, valueField, maxDepth, minDepth);
        }

        ans.setPoints(fixPointsAccordingToMaxChildren(ans.getPoints() , maxItems, returnOther));
        updatePercentageValue(ans.getPoints());
        return ans;
    }
}
