package com.cla.filecluster.service.schedule;

import com.cla.common.domain.dto.RowDTO;
import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.schedule.RescanJob;
import com.cla.filecluster.domain.entity.schedule.RootFolderSchedule;
import com.cla.filecluster.domain.entity.schedule.ScheduleGroup;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.crawler.FileCrawlerExecutionDetailsService;
import com.cla.filecluster.service.files.filetree.DocStoreService;
import com.cla.filecluster.service.util.ScheduleUtils;
import com.cla.filecluster.util.csv.CsvReader;
import com.cla.filecluster.util.csv.CsvUtils;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by uri on 02/05/2016.
 */
@Service
public class ScheduleAppService {

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private DocStoreService docStoreService;

    @Autowired
    private CsvReader csvReader;

    @Autowired
    private TaskScheduler defaultScheduler;

    @Autowired
    private DBTemplateUtils DBTemplateUtils;

    @Autowired
    private MessageHandler messageHandler;

    private Map<Long, ScheduledFuture> activeSchedules = new ConcurrentHashMap<>();

    @Value("${scheduler.active:true}")
    private boolean schedulerActive;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${schedule.groups.file:./config/schedule/defaultScheduleGroups.csv}")
    private String defaultScheduleGroupsFile;

    @Value("${subprocess:false}")
    private boolean subProcess;

    private Logger logger = LoggerFactory.getLogger(ScheduleAppService.class);

    @Autowired
    private FileCrawlerExecutionDetailsService fileCrawlerExecutionDetailsService;

    @Transactional(readOnly = true)
    public Page<ScheduleGroupSummaryInfoDto> listScheduleGroupsSummaryInfo(DataSourceRequest dataSourceRequest, String groupNameSearch, List<Long> groupIds) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<ScheduleGroup> scheduleGroups = scheduleGroupService.listAllScheduleGroups(pageRequest, groupNameSearch, groupIds);
        scheduleGroups = getPageSelected(dataSourceRequest, pageRequest, scheduleGroups);

        return fileCrawlerExecutionDetailsService.convertScheduleGroupsToSummaryInfo(pageRequest, scheduleGroups);
    }

    public ScheduleGroupSummaryInfoDto getScheduleGroupSummaryInfo(Long id) {
        ScheduleGroup group = scheduleGroupService.getScheduleGroupById(id);
        return fileCrawlerExecutionDetailsService.convertScheduleGroupsToSummaryInfo(group);
    }

    @Transactional(readOnly = true)
    public Page<ScheduleGroupSummaryInfoDto> listScheduleGroupsSummaryInfoWithRootFolders(DataSourceRequest dataSourceRequest, String groupNameSearch, List<Long> groupIds) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<ScheduleGroup> scheduleGroups = scheduleGroupService.listScheduleGroupsWithRootFolders(dataSourceRequest.getNamingSearchTerm(), pageRequest, groupNameSearch, groupIds);
        return fileCrawlerExecutionDetailsService.convertScheduleGroupsToSummaryInfo(pageRequest, scheduleGroups);
    }

    private Page<ScheduleGroup> getPageSelected(DataSourceRequest dataSourceRequest, PageRequest pageRequest, Page<ScheduleGroup> scheduleGroups) {
        Page<ScheduleGroup> result;
        if (dataSourceRequest.getSelectedItemId() != null) {
            Long selectedItemId = Long.valueOf(dataSourceRequest.getSelectedItemId());
            logger.debug("listScheduleGroupsSummaryInfo selectedItemId={}", selectedItemId);
            int index = findSelectedItemIndexSG(selectedItemId, scheduleGroups.getContent());
            logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
            if (index > 0) {
                //calculate page
                int fromPage = (index / pageRequest.getPageSize());
                pageRequest = PageRequest.of(fromPage, pageRequest.getPageSize());
            }
            int fromIndex = (int) pageRequest.getOffset();
            int total = scheduleGroups.getContent().size();
            int toIndex = Math.min(total, fromIndex + pageRequest.getPageSize());
            List<ScheduleGroup> content = new ArrayList<>(scheduleGroups.getContent().subList(fromIndex, toIndex));
            result = new PageImpl<>(content, pageRequest, scheduleGroups.getTotalElements());
        } else {
            result = scheduleGroups;
        }
        return result;
    }

    private int findSelectedItemIndexSG(Long selectedItemId, List<ScheduleGroup> all) {
        int i = 0;
        for (ScheduleGroup one : all) {
            if (one.getId().equals(selectedItemId)) {
                return i;
            }
            i++;
        }
        return 0;
    }

    @Transactional(readOnly = true)
    public Page<ScheduleGroupDto> listAllScheduleGroups(DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequestWithSort();
        Page<ScheduleGroup> scheduleGroups = scheduleGroupService.listAllScheduleGroups(pageRequest, null, null);
        return ScheduleGroupService.convertScheduleGroups(scheduleGroups, pageRequest);
    }

    public Map<Long, String> getScheduleGroupsNames() {
        return scheduleGroupService.getScheduleGroupsNames();
    }

    @Transactional
    public ScheduleGroupDto createScheduleGroup(ScheduleGroupDto scheduleGroupDto) {
        ScheduleGroupDto scheduleGroup;
        try {
            scheduleGroup = scheduleGroupService.createScheduleGroup(scheduleGroupDto);
        }
        catch (DataIntegrityViolationException e) {
            logger.warn("Data integrity violation exception while trying to create schedule group {}", scheduleGroupDto, e);
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.not-unique"), BadRequestType.ITEM_ALREADY_EXISTS);
        }

        if (scheduleGroup.isActive()) {
            startScheduleGroup(scheduleGroup);
        }
        return scheduleGroup;
    }

    @Transactional
    public void createDefaultScheduleData() {
        createPresetScheduleGroups();
    }

    private void createPresetScheduleGroups() {
        logger.debug("Create preset schedule groups");
        if (defaultScheduleGroupsFile == null || defaultScheduleGroupsFile.isEmpty()) {
            logger.info("No default schedule groups file defined - ignored");
        }
        logger.info("Load default schedule groups from file {}", defaultScheduleGroupsFile);
        Map<String, Integer> headersLocationMap = CsvUtils.handleCsvHeaders(csvReader, defaultScheduleGroupsFile, new String[]{});
        int failedRows;
        try {
            failedRows = csvReader.readAndProcess(defaultScheduleGroupsFile, row -> createScheduleGroupFromCsvRow(row, headersLocationMap), null, null);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load schedule groups - problem in csv file " + e.getMessage(), e);
        }
        if (failedRows > 0) {
            throw new RuntimeException("Failed to load " + failedRows + " schedule groups - problem in csv file");
        }
    }

    private void createScheduleGroupFromCsvRow(RowDTO row, Map<String, Integer> headersLocationMap) { //TODO: via import
        String[] fieldsValues = row.getFieldsValues();
        //name,active,cronTriggerString,schedulingDescription,schedulingJson
        ScheduleGroupDto scheduleGroupDto = new ScheduleGroupDto();
        scheduleGroupDto.setName(fieldsValues[headersLocationMap.get("name")].trim());
        String active = fieldsValues[headersLocationMap.get("active")].trim();
        String isDefault = fieldsValues[headersLocationMap.get("default")].trim();
        scheduleGroupDto.setActive(Boolean.valueOf(active));
        scheduleGroupDto.setCronTriggerString(fieldsValues[headersLocationMap.get("cronTriggerString")].trim());
        scheduleGroupDto.setSchedulingDescription(fieldsValues[headersLocationMap.get("schedulingDescription")].trim());
        String schedulingJsonFile = fieldsValues[headersLocationMap.get("schedulingJsonFile")].trim();
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(schedulingJsonFile));
            String schedulingJson = new String(bytes);
            scheduleGroupDto.setScheduleConfigDto(ScheduleUtils.convertScheduleConfigDtoFromString(schedulingJson));
        } catch (IOException e) {
            logger.error("Failed to read json schedule file: {}", schedulingJsonFile, e);
        }
        logger.debug("Create Scheduling Group {}", scheduleGroupDto);
        createScheduleGroup(scheduleGroupDto);
    }

    @Transactional
    public ScheduleGroupDto updateScheduleGroup(ScheduleGroupDto scheduleGroupDto) {
        ScheduleGroupDto scheduleGroupBefore = scheduleGroupService.getScheduleGroup(scheduleGroupDto.getId());
        ScheduleGroupDto updatedScheduleGroupDto = scheduleGroupService.updateScheduleGroup(scheduleGroupDto);

        String originalCronString = scheduleGroupBefore.getCronTriggerString();
        String cronTriggerString = updatedScheduleGroupDto.getCronTriggerString();
        boolean groupScheduled = isGroupScheduled(updatedScheduleGroupDto);

        if (updatedScheduleGroupDto.isActive() && groupScheduled && !cronTriggerString.equals(originalCronString)) {
            logger.info("Schedule group {} cronTrigger changed. Refreshing schedule", updatedScheduleGroupDto.getName());
            stopScheduleGroup(updatedScheduleGroupDto,false);
            startScheduleGroup(updatedScheduleGroupDto);
        }
        else
        if (updatedScheduleGroupDto.isActive() && !groupScheduled) {
            logger.info("Schedule group {} changed to active. Start scheduling it.", updatedScheduleGroupDto.getName());
            startScheduleGroup(updatedScheduleGroupDto);
        } else
        if (!updatedScheduleGroupDto.isActive() && groupScheduled) {
            logger.info("Schedule group {} changed to inactive. Stop scheduling it.", updatedScheduleGroupDto.getName());
            stopScheduleGroup(updatedScheduleGroupDto,false);
        }
        return updatedScheduleGroupDto;
    }


    @Transactional
    public void deleteScheduleGroup(Long id) {
        scheduleGroupService.deleteScheduleGroup(id);
    }

    @Transactional
    public ScheduleGroupDto attachRootFolderToSchedulingGroup(Long scheduleGroupId, Long rootFolderId) {
        ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(scheduleGroupId);
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        RootFolderSchedule rootFolderSchedule = scheduleGroupService.attachRootFolderToScheduleGroup(scheduleGroup.getId(), rootFolder);
        return ScheduleGroupService.convertScheduleGroup(rootFolderSchedule.getScheduleGroup());
    }

    @Transactional
    public void detachRootFolderFromSchedulingGroup(Long scheduleGroupId, Long rootFolderId) {
        ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(scheduleGroupId);
        RootFolder rootFolder = docStoreService.getRootFolder(rootFolderId);
        scheduleGroupService.detachRootFolderFromScheduleGroup(scheduleGroup, rootFolder);
    }

    @Transactional
    public void detachRootFolderFromAllSchedulingGroups(Long rootFolderId) {
        scheduleGroupService.detachRootFolderFromAllScheduleGroups(rootFolderId);
    }

    @Transactional
    public ScheduleGroupDto updateScheduleGroupForRootFolder(long rootFolderId, long scheduleGroupId) {
        if (isRootFolderAttachedToScheduleGroup(rootFolderId, scheduleGroupId)) {
            logger.info("Root folder {} is already attached to scheduleGroup {}. Not reattaching", rootFolderId, scheduleGroupId);
            return listScheduleGroup(scheduleGroupId);
        }
        detachRootFolderFromAllSchedulingGroups(rootFolderId);
        return attachRootFolderToSchedulingGroup(scheduleGroupId, rootFolderId);
    }

    @Transactional
    public boolean isRootFolderAttachedToScheduleGroup(Long rootFolderId, Long scheduleGroupId) {
        List<RootFolderSchedule> rootFolderSchedules = scheduleGroupService.findRootFolderSchedules(rootFolderId);
        for (RootFolderSchedule rootFolderSchedule : rootFolderSchedules) {
            if (rootFolderSchedule.getScheduleGroup().getId().equals(scheduleGroupId)) {
                return true;
            }
        }
        return false;
    }


    @PostConstruct
    public void startScheduler() {
        if (!schedulerActive || subProcess) {
            logger.warn("Scheduler is not active.");
            return;
        }
        DBTemplateUtils.doInTransaction(this::startScheduleGroups);
    }

    @Transactional
    private void startScheduleGroups() {
        List<ScheduleGroup> scheduleGroups = scheduleGroupService.listAllActiveScheduleGroups();

        logger.info("Start scheduler. Schedule All active ({}) ScheduleGroups", scheduleGroups.size());
        for (ScheduleGroup scheduleGroup : scheduleGroups) {
            try {
                startScheduleGroup(ScheduleGroupService.convertScheduleGroup(scheduleGroup));
            } catch (RuntimeException e) {
                logger.error("Failed to start schedule group. Unexpected error {}", scheduleGroup, e);
            }
        }
    }

    @Transactional(readOnly = true)
    public boolean isGroupScheduled(Long groupId) {
        ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(groupId);
        return isGroupScheduled(scheduleGroup);
    }

    private boolean isGroupScheduled(ScheduleGroupDto scheduleGroup) {
        return activeSchedules.containsKey(scheduleGroup.getId());
    }

    @Transactional(readOnly = true)
    public void printFutureSchedules() {
        for (Map.Entry<Long, ScheduledFuture> scheduledFutureEntry : activeSchedules.entrySet()) {
            long groupId = scheduledFutureEntry.getKey();
            ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(groupId);
            long delay = scheduledFutureEntry.getValue().getDelay(TimeUnit.SECONDS);
            logger.debug("Schedule group {} will run in {} seconds", scheduleGroup, delay);
        }
    }

    @Transactional
    public void startScheduleGroup(ScheduleGroupDto scheduleGroup) {
        if (isGroupScheduled(scheduleGroup)) {
            logger.warn("Unable to start scheduling group {}. Group already scheduled", scheduleGroup);
            return;
        }
        logger.debug("start Schedule Group {}", scheduleGroup);
        RescanJob rescanJob = applicationContext.getBean(RescanJob.class);
        rescanJob.setGroupId(scheduleGroup.getId());
        Trigger trigger = new CronTrigger(scheduleGroup.getCronTriggerString(), TimeZone.getDefault());
        ScheduledFuture<?> schedule = defaultScheduler.schedule(rescanJob, trigger);
        activeSchedules.put(scheduleGroup.getId(), schedule);
    }

    private void stopScheduleGroup(ScheduleGroupDto scheduleGroup, boolean interruptIfRunning) {
        logger.debug("Stopping Schedule Group {}", scheduleGroup);
        ScheduledFuture scheduledFuture = activeSchedules.get(scheduleGroup.getId());
        scheduledFuture.cancel(interruptIfRunning);
        activeSchedules.remove(scheduleGroup.getId());
        printFutureSchedules();
    }

    @Transactional(readOnly = true)
    public ScheduleGroupDto listScheduleGroup(Long id) {
        return scheduleGroupService.getScheduleGroup(id);
    }

    @Transactional(readOnly = true)
    public Page<ScheduleGroupDto> listRootFolderScheduleGroups(Long rootFolderId, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<ScheduleGroup> result = scheduleGroupService.listRootFolderScheduleGroups(rootFolderId, pageRequest);
        return ScheduleGroupService.convertScheduleGroups(result, pageRequest);
    }


    @Transactional(readOnly = true)
    public Page<RootFolderDto> listScheduleGroupRootFolders(Long id, DataSourceRequest dataSourceRequest) {
        PageRequest pageRequest = dataSourceRequest.createPageRequest();
        Page<RootFolder> rootFolders = scheduleGroupService.listScheduleGroupRootFolders(id, pageRequest);
        return DocStoreService.convertRootFoldersPage(pageRequest, null, rootFolders);
    }

    private int findSelectedItemIndexRF(Long selectedItemId, List<RootFolder> all) {
        int i = 0;
        for (RootFolder one : all) {
            if (one.getId().equals(selectedItemId)) {
                return i;
            }
            i++;
        }
        return i;
    }

    @Transactional(readOnly = true)
    public Page<RootFolderSummaryInfo> listScheduleGroupRootFoldersSummary(Long id, DataSourceRequest dataSourceRequest, List<RunStatus> filterByRunStatus) {
        PageRequest pageRequest;
        Page<RootFolder> rootFolders;

        if (dataSourceRequest.getSelectedItemId() != null) {
            int origPageSize = dataSourceRequest.getPageSize();
            int origPage = dataSourceRequest.getPage();
            dataSourceRequest.setPageSize(50000000); // so we'll get all root folders and can search for the right index
            dataSourceRequest.setPage(1);
            pageRequest = dataSourceRequest.createPageRequestWithSort();
            Page<RootFolder> allRootFolders = scheduleGroupService.listScheduleGroupRootFolders(id, pageRequest);
            Long selectedItemId = Long.valueOf(dataSourceRequest.getSelectedItemId());
            logger.debug("listScheduleGroupRootFoldersSummary selectedItemId={}", selectedItemId);
            int index = findSelectedItemIndexRF(selectedItemId, allRootFolders.getContent());
            logger.debug("Found selectedItemId={} at index={}", selectedItemId, index);
            if (index > 0) {
                //calculate page
                int fromPage = (index / origPageSize);
                pageRequest = PageRequest.of(fromPage ,origPageSize);
            } else {
                dataSourceRequest.setPageSize(origPageSize);
                dataSourceRequest.setPage(origPage);
                pageRequest = dataSourceRequest.createPageRequestWithSort();
            }
            int fromIndex = (int) pageRequest.getOffset();
            int total = allRootFolders.getContent().size();
            int toIndex = Math.min(total, fromIndex + origPageSize);
            List<RootFolder> content = new ArrayList<>(allRootFolders.getContent().subList(fromIndex, toIndex));
            rootFolders = new PageImpl<>(content, pageRequest, total);
        }
        else {
            pageRequest = dataSourceRequest.createPageRequestWithSort();
            Page<RootFolder> allRootFolders;
            if (Strings.isNullOrEmpty(dataSourceRequest.getNamingSearchTerm()) && (filterByRunStatus == null || filterByRunStatus.isEmpty() || filterByRunStatus.size() == RunStatus.numInUseStatuses())) {
                allRootFolders = scheduleGroupService.listScheduleGroupRootFolders(id, pageRequest);
            } else if (filterByRunStatus == null || filterByRunStatus.isEmpty() || filterByRunStatus.size() == RunStatus.numInUseStatuses()) {
                allRootFolders = scheduleGroupService.listScheduleGroupRootFolders(id, dataSourceRequest.getNamingSearchTerm(), pageRequest);
            } else {
                String searchTerm = dataSourceRequest.getNamingSearchTerm() == null ? "%" : dataSourceRequest.getNamingSearchTerm();
                allRootFolders = scheduleGroupService.listScheduleGroupRootFolders(id, searchTerm, filterByRunStatus, pageRequest);
            }
            rootFolders = allRootFolders;
        }

        return fileCrawlerExecutionDetailsService.convertRootFoldersToSummaryInfo(pageRequest, rootFolders, false);
    }

    public Long getNextScheduleTime(Long groupId) {
        ScheduledFuture scheduledFuture = activeSchedules.get(groupId);
        if (scheduledFuture != null) {
            long delay = scheduledFuture.getDelay(TimeUnit.MILLISECONDS);
            return System.currentTimeMillis() + delay;
        }
        return null;
    }

    @Transactional
    public ScheduleGroupDto updateScheduleGroupActive(Long id, Boolean state) {
        if (state == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.state.empty"), BadRequestType.MISSING_FIELD);
        }
        logger.info("Set ScheduleGroup active {} for ScheduleGroup ID {}", (state ? "on" : "off"), id);
        ScheduleGroupDto scheduleGroupDto = scheduleGroupService.getScheduleGroup(id);
        scheduleGroupDto.setActive(state);
        return updateScheduleGroup(scheduleGroupDto);
    }

}
