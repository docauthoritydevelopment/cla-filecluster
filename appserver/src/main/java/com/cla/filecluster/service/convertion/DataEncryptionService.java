package com.cla.filecluster.service.convertion;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 *
 * Created by uri on 12-Apr-17.
 */
@Service
public class DataEncryptionService {

    private static final Logger logger = LoggerFactory.getLogger(DataEncryptionService.class);

    @Value("d0c#Pocs${media.encryption.suffix:328753985628935629386}")
    private String encryptionKey;

    @Value("${media.encryption.algorithm:AES/ECB/PKCS5Padding}")
    private String encryptionAlgorithm;


    public String encrypt(final String valueEnc) {

        String encryptedVal = null;
        final Key key = generateKeyFromString(encryptionKey);
        try {
            final Cipher c = Cipher.getInstance(encryptionAlgorithm);
            c.init(Cipher.ENCRYPT_MODE, key);
            final byte[] encValue = c.doFinal(valueEnc.getBytes());
            encryptedVal = Base64.encodeBase64String(encValue);
        } catch (Exception e) {
            logger.error("Failed to encrypt String using Algorithm [{}] key size [{}]",encryptionAlgorithm,key.getEncoded().length,e);
        }

        return "##" + encryptedVal;
    }

    public String decrypt(String encryptedVal) {
        encryptedVal = encryptedVal.substring(2);
        String decryptedValue = null;
        try {
            final Cipher c = Cipher.getInstance(encryptionAlgorithm);
            final Key key = generateKeyFromString(encryptionKey);
            c.init(Cipher.DECRYPT_MODE, key);
            final byte[] decorVal = Base64.decodeBase64(encryptedVal);
//            final byte[] decorVal = new BASE64Decoder().decodeBuffer(encryptedVal);
            final byte[] decValue = c.doFinal(decorVal);
            decryptedValue = new String(decValue);
        } catch (Exception e) {
            logger.error("Failed to decrypt string", e);
        }

        return decryptedValue;
    }

    private Key generateKeyFromString(final String secKey) {

        final byte[] keyVal = Base64.decodeBase64(secKey);
        final byte[] keyPadded = new byte[16];
        System.arraycopy(keyVal,0,keyPadded,0,Math.min(keyVal.length,16));
        return new SecretKeySpec(keyPadded, "AES");
    }

}
