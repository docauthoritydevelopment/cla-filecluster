package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.utils.AssociationIdUtils;
import com.cla.filecluster.domain.entity.department.Department;
import com.cla.filecluster.repository.solr.SolrFacetQueryJsonResponse;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Component

public class DepartmentEnricher implements DefaultTypeDataEnricher {
    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;


    @Override
    public String convertSolrValueToId(String value) {
        return String.valueOf(AssociationIdUtils.extractDepartmentIdFromSolrIdentifier(value));
    }


    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, SolrFacetQueryJsonResponse solrFacetQueryJsonResponse) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return departmentService.getByIdCached(Long.parseLong(convertSolrValueToId(category))).getName();
        }).collect(Collectors.toList()));
    }

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.ACTIVE_ASSOCIATIONS, DepartmentDto.ID_PREFIX);
    }


}