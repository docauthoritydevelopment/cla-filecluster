package com.cla.filecluster.service.mcf;

import com.cla.common.domain.dto.mcf.mcf_api.*;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfJobObjectDto;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfJobStatusDto;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfJobStatusWrapperDto;
import com.cla.common.domain.dto.mcf.mcf_api.job.McfJobWrapperDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.repository.jpa.filetree.RootFolderRepository;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by uri on 06/07/2016.
 */
@Service
public class McfCommunicationService {

    public static final String COM_CLA_MCF_OUTPUT_FILECLUSTER_FILE_CLUSTER_OUTPUT_CONNECTOR = "com.cla.mcf.output.filecluster.FileClusterOutputConnector";
    public static final String MCF_API_JSON = "/mcf/api/json/";
    public static final String METADATA_TRANSFORMATION_CONNECTION_NAME = "addRootFolderId";
    public static final String FILE_SYSTEM_REPOSITORY = "FileSystem";

    Logger logger = LoggerFactory.getLogger(McfCommunicationService.class);

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    RootFolderRepository rootFolderRepository;

    @Autowired
    private McfDtoGeneratorService mcfDtoGeneratorService;

    @Value("${server.port}")
    private String localServerPort;

    @PostConstruct
    public void initObjectMapper() {
        logger.debug("Init object mapper");
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        mapper.enable(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES);
    }

    public void createTransformationConnectionIfNeeded() throws IOException {
        logger.debug("createTransformationConnectionIfNeeded");
        if (findTransformationConnection(METADATA_TRANSFORMATION_CONNECTION_NAME) == null) {
            McfTransformationConnectionDto dto = mcfDtoGeneratorService.createTransformationConnectionObjectDto(METADATA_TRANSFORMATION_CONNECTION_NAME);
            createTransformationConnection(dto,METADATA_TRANSFORMATION_CONNECTION_NAME);
        }
    }

    private void createTransformationConnection(McfTransformationConnectionDto dto,String name) throws UnsupportedEncodingException, JsonProcessingException {
        try {
            logger.info("Create transformation connection");
            McfTransformationConnectionsDtoList wrapper = new McfTransformationConnectionsDtoList(dto);
            final String body = mapper.writeValueAsString(wrapper);
            logger.debug("Creating the following transformation connection:\n {}", body);
            final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "transformationconnections/" + name);
            addBodyToRequest(body, putRequest);
            sendHttpRequest(putRequest);
        } catch (IOException e) {
            logger.error("Failed to create transformation connection",e);
            throw e;
        }
    }

    public void createOutputConnectorIfNeeded(String username, String password) throws IOException {

        logger.info("Create Output connection if needed (user:{} password:{} serverPort:{})", username,password,localServerPort);
        OutputConnectionObjectDto outputConnection = getOutputConnection(McfDtoGeneratorService.OUTPUT_CONNECTION_NAME);
        if (outputConnection == null) {
            logger.debug("Output connection not defined. Defining");
            Map<String, String> configuration = new HashMap<>();
            configuration.put("USER", username);
            configuration.put("PASSWORD", password);
            configuration.put("CONNECTOR_TYPE", "RAW");
            configuration.put("SERVER_PORT", localServerPort);
            createFileClusterOutputConnection(McfDtoGeneratorService.OUTPUT_CONNECTION_NAME, COM_CLA_MCF_OUTPUT_FILECLUSTER_FILE_CLUSTER_OUTPUT_CONNECTOR,
                    configuration);
        } else {
            logger.debug("Output connection already defined");
            //TODO - make sure it's defined correctly - especially serverPort
            Map<String, McfConfigurationParameterDto[]> configuration = outputConnection.getConfiguration();
            for (Map.Entry<String, McfConfigurationParameterDto[]> configEntry: configuration.entrySet()) {
                McfConfigurationParameterDto[] value = configEntry.getValue();
                String valueString = extractValueStringFromConfigEntry(value);
                logger.debug("Output connection property {}={}",configEntry.getKey(), valueString);
            }

        }
    }

    public String extractValueStringFromConfigEntry(McfConfigurationParameterDto[] value) {
        if (value == null) {
            return "No Value";
        }
        if (value.length == 1) {
            return value[0].get_value_();
        }
        else {
            return "Multiple values";
        }
    }

    public HttpHost getHttpHost() {
        //TODO - this should come out of a db object
        return new HttpHost("localhost", 8345, "http");
    }

    private OutputConnectionObjectDto getOutputConnection(String outputConnectorName) {
        OutputConnectionObjectDto[] outputConnectionObjectDtos = listOutputConnections();
        if (outputConnectionObjectDtos == null) {
            return null;
        }
        return (OutputConnectionObjectDto) findBaseMcfObjectInList(outputConnectorName, outputConnectionObjectDtos);
    }

    private McfBaseObjectDto findBaseMcfObjectInList(String outputConnectorName, McfBaseObjectDto[] baseObjectDtos) {
        if (baseObjectDtos == null) {
            return null;
        }
        for (McfBaseObjectDto outputConnectionObjectDto : baseObjectDtos) {
            if (outputConnectorName.equals(outputConnectionObjectDto.getName())) {
                //Found it
                return outputConnectionObjectDto;
            }
        }
        return null;
    }

    private McfTransformationConnectionDto findTransformationConnection(String name) {
        McfTransformationConnectionDto[] transformationConnectionDtos = listTransformationConnections();
        return (McfTransformationConnectionDto) findBaseMcfObjectInList(name, transformationConnectionDtos);
    }

    private McfTransformationConnectionDto[] listTransformationConnections() {
        String uri = MCF_API_JSON + "transformationconnections";
        McfTransformationConnectionsDtoList list = listMcfObjects(McfTransformationConnectionsDtoList.class, uri);
        return list.getTransformationconnection();
    }

    /**
     * {"outputconnection":{"isnew":"false","name":"FileCluster output","class_name":"com.cla.mcf.output.filecluster.FileClusterOutputConnector","max_connections":"10","description":"","configuration":{"_PARAMETER_":[{"_value_":"mcf","_attribute_name":"USER"},{"_value_":"QAZmcf2daEDC","_attribute_name":"PASSWORD"},{"_value_":"RAW","_attribute_name":"CONNECTOR_TYPE"}]}}}
     *
     * @return
     */
    public OutputConnectionObjectDto[] listOutputConnections() {
        String uri = MCF_API_JSON + "outputconnections";
        OutputConnectionObjectDtoList list = listMcfObjects(OutputConnectionObjectDtoList.class, uri);
        return list.getOutputconnection();
    }

    public <T> T listMcfObjects(Class<T> classType, String uri) {
        final HttpGet getRequest = new HttpGet(uri);
        String result = sendHttpRequest(getRequest);
        try {
            return mapper.readValue(result, classType);
        } catch (final IOException e) {
            logger.error("Failed to list MCF objects on url {} by class {}",uri,classType);
            throw new RuntimeException(e);
        }
    }


    private void createFileClusterOutputConnection(final String outputConnectionName, final String outputConnectionClass, Map<String, String> configuration) throws IOException {
        logger.info("Create FileCluster output connector");
        OutputConnectionObjectDto outputConnectionObjectDto = mcfDtoGeneratorService.createOutputConnectionObjectDto(outputConnectionName, outputConnectionClass, configuration, this);
        OutputConnectionObjectDtoList outputConnectionObjectDtoList = new OutputConnectionObjectDtoList(outputConnectionObjectDto);
        final String outputConnectionString = mapper.writeValueAsString(outputConnectionObjectDtoList);
        logger.debug("Creating the following output connection:\n {}", outputConnectionString);
        String encodedName = URLEncoder.encode(outputConnectionName, "UTF-8");
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "outputconnections/" + encodedName);
        addBodyToRequest(outputConnectionString, putRequest);
        sendHttpRequest(putRequest);
    }

    public boolean isMcfReachable(int timeout) {
        HttpHost httpHost = getHttpHost();
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(httpHost.getAddress(), httpHost.getPort()), timeout);
            socket.close();
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public void createRepositoryConnectionIfNeeded(String repositoryName, MediaType mediaType) throws IOException {
        McfRepositoryConnectionObjectDto existingRepo = listRepositoryConnectionById(repositoryName);
        if (existingRepo == null) {
            String className = convertMediaTypeToClassName(mediaType);
            McfRepositoryConnectionObjectDto newRepository = mcfDtoGeneratorService.creatRepository(repositoryName,className);
            McfRepositoryConnectionObjectWrapperDto wrapperDto= new McfRepositoryConnectionObjectWrapperDto(newRepository);
            final String body = mapper.writeValueAsString(wrapperDto);
            logger.debug("Creating the following repository:\n {}", body);
            final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "repositoryconnections/" + repositoryName);
            addBodyToRequest(body, putRequest);
            sendHttpRequest(putRequest);

        }
    }

    private String convertMediaTypeToClassName(MediaType mediaType) {
        switch (mediaType) {
            case FILE_SHARE:
                return "org.apache.manifoldcf.crawler.connectors.filesystem.FileConnector";
            case BOX:
                throw new RuntimeException("Media type "+mediaType+" is not supported on MCF");
        }
        throw new RuntimeException("Media type "+mediaType+" is not supported on MCF");
    }

    @Transactional(readOnly = false)
    public String createJobForRootFolderIfNeeded(Long rootFolderId) {
        try {
            logger.debug("create Job For RootFolder {} If Needed", rootFolderId);
            RootFolder rootFolder = rootFolderRepository.getOne(rootFolderId);
            String externalCrawlerJobId = rootFolder.getExternalCrawlerJobId();
            if (externalCrawlerJobId != null) {
                McfJobObjectDto mcfJobObjectDto = listJobById(externalCrawlerJobId);
                if (mcfJobObjectDto != null) {
                    logger.debug("RootFolder job {} already exists",externalCrawlerJobId);
                    //TODO - verify job is correctly configured
                }
                else {
                    //Create job anyway
                    logger.warn("RootFolder job {} missing from MCF. Create job for rootFolder {}",externalCrawlerJobId,rootFolderId);
                    externalCrawlerJobId = createRootFolderJob(rootFolder);
                }
            } else {
                String jobName = McfDtoGeneratorService.ROOT_FOLDER_PREFIX + rootFolder.getId();
                logger.debug("Try to find job by name "+jobName);
                McfJobObjectDto jobByName = findJobByName(jobName);
                if (jobByName != null) {
                    logger.info("Found job {} for root Folder Id {}",jobByName,rootFolderId);
                    //TODO - verify job is correctly configured
                    externalCrawlerJobId = jobByName.getId();
                    //This job was not in the database. We don't trust it's history
                    String repositoryName = generateRepositoryName(rootFolder);
                    clearIndexFromRepositoryConnection(repositoryName);
                }
                else {
                    //No job was created for this root folder -
                    logger.debug("Create job for rootFolder {}",rootFolderId);
                    externalCrawlerJobId = createRootFolderJob(rootFolder);
                }
            }
            rootFolder.setExternalCrawlerJobId(externalCrawlerJobId);
            rootFolderRepository.save(rootFolder);
            return externalCrawlerJobId;
        } catch (IOException e) {
            logger.error("Failed to create job for rootFolder {}", rootFolderId, e);
            throw new RuntimeException("Failed to create job for rootFolder " + rootFolderId + ". IO error " + e.getMessage(), e);
        }
    }

    /**
     * java.lang.RuntimeException: com.fasterxml.jackson.databind.JsonMappingException:
     * Can not deserialize instance of java.lang.String out of START_OBJECT token
       at [Source: {"job":[{"id":"1468832908011","description":"rootFolder_1","repository_connection":"FileSystem"
     ,"document_specification":{},"pipelinestage":[{"stage_id":"0","stage_isoutput":"false","stage_connectionname":"addRootFolderId"
     ,"stage_description":"addRootFolderId","stage_specification":{}},{"stage_id":"1","stage_prerequisite":"0"
     ,"stage_isoutput":"true","stage_connectionname":"FileCluster output","stage_description":"1"
     ,"stage_specification":{}}],"start_mode":"manual","run_mode":"scan once","hopcount_mode":"accurate","priority":"5"
     ,"recrawl_interval":"86400000","expiration_interval":"infinite","reseed_interval":"3600000"}
     ,{"id":"1468830417773","description":"temp","repository_connection":"FileSystem"
     ,"document_specification":{"startpoint":{"_value_":"","_attribute_path":"E:\\Workspace\\Data\\Play"
     ,"_attribute_converttouri":"false","include":[{"_value_":"","_attribute_match":"*","_attribute_type":"file"}
     ,{"_value_":"","_attribute_match":"*","_attribute_type":"directory"}]}}
     ,"pipelinestage":[{"stage_id":"0","stage_isoutput":"false","stage_connectionname":"addRootFolderId"
     ,"stage_description":"addRootFolderId","stage_specification":{"expression":{"_value_":"","_attribute_parameter":"rootFolderId","_attribute_value":"1"},"keepAllMetadata":{"_value_":"","_attribute_value":"true"},"filterEmpty":{"_value_":"","_attribute_value":"true"}}},{"stage_id":"1","stage_prerequisite":"0","stage_isoutput":"true","stage_connectionname":"FileCluster output","stage_description":"1","stage_specification":{}}],"start_mode":"manual","run_mode":"scan once","hopcount_mode":"accurate","priority":"5","recrawl_interval":"86400000"
     ,"expiration_interval":"infinite","reseed_interval":"3600000"}]};
     line: 1, column: 96] (through reference chain: com.cla.filecluster.domain.entities.mcf.mcf_api.job.McfJobWrapperDto["job"]->java.util.ArrayList[0]->com.cla.filecluster.domain.entities.mcf.mcf_api.job.McfJobObjectDto["document_specification"])

     * @param jobName
     * @return
     */
    private McfJobObjectDto findJobByName(String jobName) {
        String uri = MCF_API_JSON + "jobs";
        McfJobWrapperDto jobWrapperDto = listMcfObjects(McfJobWrapperDto.class, uri);
        List<McfJobObjectDto> job = jobWrapperDto.getJob();
        if (job == null) {
            return null;
        }
        for (McfJobObjectDto mcfJobObjectDto : job) {
            if (jobName.equalsIgnoreCase(mcfJobObjectDto.getDescription())) {
                return mcfJobObjectDto;
            };
        }
        return null;
    }

    private McfRepositoryConnectionObjectDto listRepositoryConnectionById(String connectionId) {
        String uri = MCF_API_JSON + "repositoryconnections";
        McfRepositoryConnectionObjectWrapperDto wrapperDto= listMcfObjects(McfRepositoryConnectionObjectWrapperDto.class, uri);
        if (wrapperDto.getRepositoryconnection() != null && wrapperDto.getRepositoryconnection().size() >0) {
            for (McfRepositoryConnectionObjectDto dto : wrapperDto.getRepositoryconnection()) {
                if (dto.getName().equalsIgnoreCase(connectionId)) {
                    return dto;
                }
            }
        }
        return null;
    }

    private McfJobObjectDto listJobById(String externalCrawlerJobId) {
        String uri = MCF_API_JSON + "jobs/"+externalCrawlerJobId;
        McfJobWrapperDto jobWrapperDto = listMcfObjects(McfJobWrapperDto.class, uri);
        if (jobWrapperDto.getJob() != null && jobWrapperDto.getJob().size() >0) {
            return jobWrapperDto.getJob().get(0);
        }
        else {
            return null;
        }
    }

    public boolean isOutputConnectionConnected() {
        String outputConnectionStatus = getOutputConnectionStatus(McfDtoGeneratorService.OUTPUT_CONNECTION_NAME);
        if (outputConnectionStatus == null) {
            return false;
        }
        if (outputConnectionStatus.equalsIgnoreCase("Connection working")) {
            return true;
        }
        return false;
    }

        /**
         * {"check_result":"Connection working"}
         * @param connectionName
         */
    public String getOutputConnectionStatus(String connectionName) {
        String encodedName = null;
        try {
            encodedName = URLEncoder.encode(connectionName, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            //Should never happen.
            logger.error("Failed to encode connection name",e);
        }
        String uri = MCF_API_JSON + "status/outputconnections/"+encodedName;
        Map checkStatusMap = listMcfObjects(Map.class, uri);
        String value = (String) checkStatusMap.get("check_result");
        logger.info("Output connection {} status: {}",connectionName,value);
        return value;
    }

    private String createRootFolderJob(final RootFolder rootFolder) throws IOException {
        Long rootFolderId = rootFolder.getId();
        logger.info("Create MCF rootFolder job by for rootFolder {}", rootFolderId);
        if (rootFolder.getRealPath() == null) {
            logger.error("RealPath is missing from rootFolder {}. realPath is required for MCF job creation",rootFolderId);
            throw new RuntimeException("RealPath is missing from rootFolder");
        }
        String repositoryName = generateRepositoryName(rootFolder);
        createRepositoryConnectionIfNeeded(repositoryName,rootFolder.getMediaType());

        Map fileSystemJobSpecification = mcfDtoGeneratorService.createFileSystemJobSpecification(rootFolder.getRealPath());
        McfJobObjectDto newJob = mcfDtoGeneratorService.createRootFolderJobObjectDto(rootFolderId, repositoryName,fileSystemJobSpecification);
        McfJobWrapperDto jobWrapperDto = new McfJobWrapperDto(newJob);

        String jobString = mapper.writeValueAsString(jobWrapperDto);
        logger.debug("Creating the following:\n {}", jobString);
        final HttpEntityEnclosingRequestBase request = new HttpPost(MCF_API_JSON + "jobs");
        addBodyToRequest(jobString, request);
        String result = sendHttpRequest(request);
        //{"job_id":"1468832908011"}
        Map resultDto = mapper.readValue(result, Map.class);
        logger.debug("Created MCF job");
        String job_id = (String) resultDto.get("job_id");
        return job_id;
    }

    private String generateRepositoryName(RootFolder rootFolder) {
        return rootFolder.getId() + "_repository";
    }

    private void addBodyToRequest(String jobString, HttpEntityEnclosingRequestBase request) throws UnsupportedEncodingException {
        final StringEntity se = new StringEntity(jobString);
        request.setEntity(se);
        se.setContentType("application/json");
    }

    private String sendHttpRequest(HttpUriRequest request) {
        URI uri = request.getURI();
        try (CloseableHttpClient httpclient = HttpClientBuilder.create().build()) {
            HttpHost httpHost = getHttpHost();
            final HttpResponse response = httpclient.execute(httpHost, request);
            final int statusCode = response.getStatusLine().getStatusCode();
            final String result = EntityUtils.toString(response.getEntity());
            logger.trace("MCF API response {}", result);
            if (statusCode < 200 || statusCode >= 300) {
                logger.debug("MCF API response {} for request {}", result, uri);
                throw new IOException("Failed uri "+uri+": HTTP error code : " + statusCode);
            }
            return result;
        } catch (IOException e) {
            logger.error("Failed to send HttpRequest to MCF",uri, e);
            throw new RuntimeException("Failed to send HttpRequest to mcf. IO error " + e.getMessage(), e);
        }
        catch (Exception e) {
            logger.error("Failed to send HttpRequest to MCF (uri:{})",uri,e);
            throw new RuntimeException("Failed to send HttpRequest to mcf. Exception " + e.getMessage(), e);
        }
    }

    /**
     * PUT start/<job_id>
     * @param externalCrawlerJobId
     */
    public void startJob(String externalCrawlerJobId) {
        logger.debug("Start MCF job {}",externalCrawlerJobId);
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "start/" + externalCrawlerJobId);
        sendHttpRequest(putRequest);
    }

    public void stopJob(String externalCrawlerJobId) {
        logger.debug("Stop MCF job {}",externalCrawlerJobId);
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "abort/" + externalCrawlerJobId);
        sendHttpRequest(putRequest);

    }

    /**
     * jobstatuses/<job_id>
     * @param externalCrawlerJobId
     */
    public McfJobStatusDto getJobStatus(String externalCrawlerJobId) {
        String uri = MCF_API_JSON + "jobstatuses/" + externalCrawlerJobId;
        McfJobStatusWrapperDto mcfJobStatusWrapperDto = listMcfObjects(McfJobStatusWrapperDto.class, uri);
        List<McfJobStatusDto> jobstatus = mcfJobStatusWrapperDto.getJobstatus();
        if (jobstatus != null && jobstatus.size() > 0) {
            McfJobStatusDto mcfJobStatusDto = jobstatus.get(0);
            logger.debug("get MCF job {} status {}",externalCrawlerJobId,mcfJobStatusDto);
            return mcfJobStatusDto;
        }
        logger.debug("Can't find MCF job {} status",externalCrawlerJobId);
        return null;
    }

    public void verifyOutputConnectionConnected() {
        String outputConnectionStatus = getOutputConnectionStatus(McfDtoGeneratorService.OUTPUT_CONNECTION_NAME);
        if (outputConnectionStatus == null) {
            logger.error("ManifoldCF is not connected to the DocAuthority server. Please check MCF connection details");
            throw new RuntimeException("Unable to start scan. ManifoldCf is not connected");
        }
        if (outputConnectionStatus.equalsIgnoreCase("Connection working")) {
            return;
        }
        logger.error("ManifoldCF is not connected to the DocAuthority server. Please check MCF connection details ({})",outputConnectionStatus);
        throw new RuntimeException("Unable to start scan. ManifoldCf is not connected to DocAuthority ("+outputConnectionStatus+")");
    }

    /**
     * clearversions/<encoded_output_connection_name>
     * Forget previous indexed document versions
     */
    public void clearIndexFromOutputConnection(String outputConnectionName) {
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "clearversions/" + outputConnectionName);
        sendHttpRequest(putRequest);
    }


    /**
     * clearhistory/<encoded_repository_connection_name>
     */
    public void clearIndexFromRepositoryConnection(String respositoryConnectionName) {
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "clearhistory/" + respositoryConnectionName);
        sendHttpRequest(putRequest);
    }

    /**
     * Only works when the job is in continuous scan mode
     * @param jobId
     */
    public void reseedJob(String jobId) {
        logger.warn("Reseed Job {}. All documents will be processed again",jobId);
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "reseed/" + jobId);
        sendHttpRequest(putRequest);
    }

    public void resetOutputConnectorHistory(String outputConnectorName) {
        logger.warn("Reset output connector history. All files will be processed again",outputConnectorName);
        final HttpEntityEnclosingRequestBase putRequest = new HttpPut(MCF_API_JSON + "clearversions/" + outputConnectorName);
        sendHttpRequest(putRequest);
    }

}
