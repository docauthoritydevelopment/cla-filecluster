package com.cla.filecluster.service.progress;

public class ProgressMissingException extends RuntimeException {

    public ProgressMissingException(String message) {
        super(message);
    }

    public ProgressMissingException(String s, Exception e) {
        super(s,e);
    }
}
