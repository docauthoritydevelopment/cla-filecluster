package com.cla.filecluster.service.export.exporters.file.mail;

import com.cla.common.domain.dto.email.MailAddressDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

/**
 * Created by: yael
 * Created on: 2/5/2019
 */
public class MailAggCountDtoMixin {

    @JsonUnwrapped
    private MailAddressDto item;

    @JsonProperty(MailHeaderFields.NUM_OF_FILES)
    private Integer count;
}
