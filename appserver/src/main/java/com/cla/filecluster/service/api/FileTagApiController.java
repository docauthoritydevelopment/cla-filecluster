package com.cla.filecluster.service.api;

import com.cla.common.constants.SolrCommitType;
import com.cla.common.constants.SolrFieldOp;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.filetag.*;
import com.cla.common.domain.dto.filetree.DocFolderDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociableEntityType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.filetag.FileTag;
import com.cla.filecluster.domain.entity.filetag.FileTagType;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.entity.security.User;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.filetag.FileTagService;
import com.cla.filecluster.service.filetag.FileTagTypeService;
import com.cla.filecluster.service.filetag.FileTaggingAppService;
import com.cla.filecluster.service.filter.SavedFilterService;
import com.cla.filecluster.service.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.ASSIGN_TAG_FILES_BATCH;


/**
 * Created by uri on 25/08/2015.
 */
@RestController
@RequestMapping("/api/filetag")
public class FileTagApiController {

    @Autowired
    private FileTaggingAppService fileTaggingAppService;

    @Autowired
    private FileTagTypeService fileTagTypeService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private UserService userService;

    @Autowired
    private FileTagService fileTagService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private MessageHandler messageHandler;

    /**
     * List groups with a file count for each group.
     * The Files in the groups can be filtered by various filters.
     * Only groups with files > 0 after filter are shown.
     * File counts are after filters of course...
     */
    @RequestMapping(value = "/filecount")
    public FacetPage<AggregationCountItemDTO<FileTagDto>> findTagsFileCount(@RequestParam final Map<String, String> params, @RequestParam(required = false) Long scopeId) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover tags export", AuditAction.VIEW, FileTagDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover tags", AuditAction.VIEW, FileTagDto.class, null, dataSourceRequest);
        }
        return fileTaggingAppService.listTagsFileCountWithFileFilter(dataSourceRequest, scopeId);
    }

    @RequestMapping(value = "/type/filecount")
    public FacetPage<AggregationCountItemDTO<FileTagTypeDto>> findTagTypesFileCount(@RequestParam final Map params, @RequestParam(required = false) Long scopeId) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        eventAuditService.audit(AuditType.QUERY, "Discover tagTypes", AuditAction.VIEW, FileTagTypeDto.class, null, dataSourceRequest);
        return fileTaggingAppService.listTagTypesFileCountWithFileFilter(dataSourceRequest, scopeId);
    }


    @RequestMapping(value = "/type/new", method = RequestMethod.PUT)
    public FileTagTypeDto createFileTagType(@RequestBody FileTagTypeDto fileTagTypeDto) {
        FileTagTypeDto fileTagType = fileTaggingAppService.createFileTagType(fileTagTypeDto);
        eventAuditService.audit(AuditType.TAG_TYPE, "Create tagType", AuditAction.CREATE, FileTagTypeDto.class, fileTagType.getId(), fileTagType);
        return fileTagType;
    }

    @RequestMapping(value = "/type/{fileTagTypeId}", method = RequestMethod.POST)
    public FileTagTypeDto updateFileTagType(@PathVariable long fileTagTypeId, @RequestBody FileTagTypeDto fileTagTypeDto) {
        FileTagTypeDto fileTagTypeDto1 = fileTaggingAppService.updateFileTagType(fileTagTypeId, fileTagTypeDto);
        eventAuditService.audit(AuditType.TAG_TYPE, "Update tagType", AuditAction.UPDATE, FileTagTypeDto.class, fileTagTypeDto1.getId(), fileTagTypeDto1);
        return fileTagTypeDto1;
    }

    @RequestMapping(value = "/type/{fileTagTypeId}", method = RequestMethod.DELETE)
    public void deleteFileTagTypeById(@PathVariable long fileTagTypeId) {
        FileTagTypeDto fileTagType = getFileTagType(fileTagTypeId);
        fileTaggingAppService.deleteFileTagType(fileTagTypeId);
        eventAuditService.audit(AuditType.TAG_TYPE, "Delete tagType", AuditAction.DELETE, FileTagTypeDto.class, fileTagTypeId, fileTagType);
    }

    @RequestMapping(value = "/type/{fileTagTypeId}/new", method = RequestMethod.PUT)
    public FileTagDto createFileTag(@PathVariable long fileTagTypeId,
                                    @RequestParam String name,
                                    @RequestParam(required = false) String description,
                                    @RequestParam(required = false) Long docTypeId,
                                    @RequestParam(required = false) String entityId) {
        FileTagType fileTagType = fileTaggingAppService.getFileTagType(fileTagTypeId);
        if (fileTagType.isHidden()) {
            throw new BadRequestException(messageHandler.getMessage("file-tag-type.hidden"), BadRequestType.UNSUPPORTED_VALUE);
        }
        FileTagDto docTypeFileTag = fileTaggingAppService.createFileTag(name, description, fileTagType, docTypeId, entityId, FileTagAction.MANUAL);
        eventAuditService.auditDual(AuditType.TAG_TYPE, "Create file tag item", AuditAction.CREATE_ITEM, FileTagTypeDto.class, fileTagTypeId, FileTagDto.class, docTypeFileTag.getId(), docTypeFileTag);
        return docTypeFileTag;
    }

    /**
     * @param typeId - optional. If not null filters the tags by the specified type
     * @return a list of all the tags in the system, optionally filtered by type
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<FileTagDto> getAllFileTags(@RequestParam Long typeId) {
        return fileTaggingAppService.getFileTags(typeId);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public FileTagDto getFileTagById(@PathVariable long id) {
        return fileTaggingAppService.getFileTagById(id);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public FileTagDto deleteFileTagById(@PathVariable long id) {
        FileTag fileTag = getFileTag(id);
        FileTagDto fileTagDto = fileTaggingAppService.removeFileTag(fileTag.getId());
        eventAuditService.auditDual(AuditType.TAG, "Delete file tag item", AuditAction.DELETE_ITEM, FileTagTypeDto.class, fileTagDto.getType().getId(), FileTagDto.class, fileTagDto.getId(), fileTagDto);
        return fileTagDto;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public FileTagDto updateFileTagById(@PathVariable long id, @RequestBody FileTagDto fileTagDto) {
        FileTagDto fileTagDto1 = fileTaggingAppService.updateFileTag(id, fileTagDto);
        eventAuditService.auditDual(AuditType.TAG_TYPE, "Update file tag item", AuditAction.UPDATE_ITEM, FileTagTypeDto.class, fileTagDto1.getType().getId(), FileTagDto.class, fileTagDto1.getId(), fileTagDto1);
        return fileTagDto1;
    }

    @RequestMapping(value = "/{fileTagId}/file/{fileId}", method = RequestMethod.PUT)
    public ClaFileDto addTagToFile(@PathVariable long fileId, @PathVariable long fileTagId) {
        FileTag fileTag = getFileTag(fileTagId);
        return addTagToFileByScope(fileId, fileTag, fileTag.getType().getScope());
    }

    public ClaFileDto addTagToFileByScope(long fileId, FileTag fileTag, BasicScope basicScope) {
        ClaFileDto claFileDto = fileTaggingAppService.addFileTagToFile(fileId,
                FileTagService.convertFileTagToDto(fileTag), basicScope, SolrCommitType.SOFT_NOWAIT);
        if (claFileDto != null) {
            eventAuditService.auditDual(AuditType.TAG_TYPE, "Assign tag to file", AuditAction.ASSIGN, FileTagDto.class, fileTag.getId(), ClaFileDto.class, claFileDto.getId(), claFileDto);
        }
        return claFileDto;
    }

    @RequestMapping(value = "/{fileTagId}/file/{fileId}", method = RequestMethod.DELETE)
    public ClaFileDto removeTagFromFile(@PathVariable long fileId, @PathVariable long fileTagId) {
        FileTag fileTag = getFileTag(fileTagId);
		ClaFileDto fileWithTags = removeTagFromFileByScope(fileId, fileTag, fileTag.getType().getScope());
		return fileWithTags;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={ASSIGN_TAG_FILES_BATCH})
    @RequestMapping(value = "/{fileTagId}/files", method = RequestMethod.PUT)
    public void addTagToFilesUsingFilter(@PathVariable long fileTagId, @RequestParam Map<String, String> params) {
        FileTagDto fileTag = getFileTagDto(fileTagId);
        String filter = getFilter(params);
        Long scopeId = fileTag.getType().getScope() == null ? null : (Long)fileTag.getType().getScope().getId();
        AssociationEntity entity = AssociationsService.createForFile(fileTag, scopeId);

        User owner = userService.getCurrentUserEntity();
        associationsService.associationOnFilesFilter(entity, SolrFieldOp.ADD, filter, owner == null ? null : owner.getUsername(), AssociableEntityType.TAG);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={ASSIGN_TAG_FILES_BATCH})
    @RequestMapping(value = "/{fileTagId}/files", method = RequestMethod.DELETE)
    public void removeTagToFilesUsingFilter(@PathVariable long fileTagId, @RequestParam Map<String, String> params) {
        FileTagDto fileTag = getFileTagDto(fileTagId);
        String filter = getFilter(params);
        Long scopeId = fileTag.getType().getScope() == null ? null : (Long)fileTag.getType().getScope().getId();
        AssociationEntity entity = AssociationsService.createForFile(fileTag, scopeId);

        User owner = userService.getCurrentUserEntity();
        associationsService.associationOnFilesFilter(entity, SolrFieldOp.REMOVE, filter, owner == null ? null : owner.getUsername(), AssociableEntityType.TAG);
    }

    private String getFilter(Map<String, String> params) {
        DataSourceRequest req = DataSourceRequest.build(params);
        FilterDescriptor filterDescriptor = req.getFilter();
        return SavedFilterService.convertFilterDescriptorJson(filterDescriptor);
    }

    private ClaFileDto removeTagFromFileByScope(long fileId, FileTag fileTag, BasicScope basicScope) {
        ClaFileDto claFileDto = fileTaggingAppService.removeFileTagFromFile(fileId, fileTag.getId(), basicScope, SolrCommitType.SOFT_NOWAIT);
        if (claFileDto != null) {
            eventAuditService.auditDual(AuditType.TAG, "Unassign tag from file", AuditAction.UNASSIGN, FileTagDto.class, fileTag.getId(), ClaFileDto.class, claFileDto.getId(), claFileDto);
        }
        return fileTaggingAppService.getFileWithTags(fileId);
    }

    @RequestMapping(value = "/{fileTagId}/folder/{folderId}", method = RequestMethod.PUT)
    public DocFolderDto addTagToFolder(@PathVariable Long folderId, @PathVariable long fileTagId) {
        FileTag fileTag = getFileTag(fileTagId);
        FileTagDto fileTagDto = FileTagService.convertFileTagToDto(fileTag);
        DocFolderDto docFolderDto = fileTaggingAppService.addFileTagToFolder(folderId, fileTagDto, fileTag.getType().getScope());
        if (docFolderDto != null) {
            eventAuditService.auditDual(AuditType.TAG, "Assign tag to folder", AuditAction.ASSIGN, FileTagDto.class, fileTagId, DocFolderDto.class, docFolderDto.getId(), docFolderDto);
        }
        return docFolderDto;
    }

    @RequestMapping(value = "/{fileTagId}/folder/{folderId}", method = RequestMethod.DELETE)
    public DocFolderDto removeTagFromFolder(@PathVariable Long folderId, @PathVariable long fileTagId) {
        FileTag fileTag = getFileTag(fileTagId);
        DocFolderDto docFolderDto = fileTaggingAppService.removeFileTagFromFolder(folderId, fileTag.getId(), fileTag.getType().getScope());
        if (docFolderDto != null) {
            eventAuditService.auditDual(AuditType.TAG, "Unassign tag to folder", AuditAction.UNASSIGN, FileTagDto.class, fileTagId, DocFolderDto.class, docFolderDto.getId(), docFolderDto);
        }
        return docFolderDto;
    }

    @RequestMapping(value = "/{fileTagId}/bid/{groupId}", method = RequestMethod.PUT)
    public GroupDto addTagToGroup(@PathVariable String groupId, @PathVariable long fileTagId) {
        FileTag fileTag = getFileTag(fileTagId);
        return addTagToGroupByScope(groupId, fileTag, fileTag.getType().getScope());
    }

    private GroupDto addTagToGroupByScope(String groupId, FileTag fileTag, BasicScope basicScope) {
        GroupDto groupDto = fileTaggingAppService.addFileTagToGroup(groupId, fileTag, basicScope);
        if (groupDto != null) {  //Return null if already associated
            eventAuditService.auditDual(AuditType.TAG, "Assign tag to group", AuditAction.ASSIGN, FileTagDto.class, fileTag.getId(), GroupDto.class, groupId, groupDto);
        }
        return groupDto;
    }

    @RequestMapping(value = "/{fileTagId}/bid/{groupId}", method = RequestMethod.DELETE)
    public GroupDto removeTagFromGroup(@PathVariable String groupId, @PathVariable long fileTagId) {
        FileTag fileTag = getFileTag(fileTagId);
        return removeTagFromGroupByScope(groupId, fileTag, fileTag.getType().getScope());
    }

    private GroupDto removeTagFromGroupByScope(@PathVariable String groupId, FileTag fileTag, BasicScope basicScope) {
        GroupDto groupDto = fileTaggingAppService.removeFileTagFromGroup(groupId, fileTag, fileTag.getType().getScope());
        eventAuditService.auditDual(AuditType.TAG, "Unassign tag from group", AuditAction.UNASSIGN, FileTagDto.class, fileTag.getId(), GroupDto.class, groupId, groupDto);
        return groupDto;
    }

    private FileTag getFileTag(long fileTagId) {
        FileTag fileTag = fileTagService.getFileTag(fileTagId);
        if (fileTag == null) {
            throw new BadParameterException(messageHandler.getMessage("file-tag.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return fileTag;
    }

    private FileTagDto getFileTagDto(long fileTagId) {
        FileTagDto fileTag = fileTagService.getFromCache(fileTagId);
        if (fileTag == null) {
            throw new BadParameterException(messageHandler.getMessage("file-tag.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return fileTag;
    }


    @RequestMapping(value = "/type/{fileTagTypeId}", method = RequestMethod.GET)
    public FileTagTypeDto getFileTagType(@PathVariable long fileTagTypeId) {
        FileTagTypeDto fileTagType = fileTagTypeService.getFileTagTypeDto(fileTagTypeId);
        if (fileTagType == null) {
            throw new BadParameterException(messageHandler.getMessage("file-tag-type.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return fileTagType;
    }

    @RequestMapping(value = "/apply/solr", method = RequestMethod.POST)
    public void applyTagsToSolr(@RequestParam(required = false) Boolean wait
            , @RequestParam(required = false, defaultValue = "true") Boolean applyDirtyFiles
            , @RequestParam(required = false, defaultValue = "100000") long maximumAssociationsToProcess) {
        // do nothing
    }

    @RequestMapping(value = "/apply/solr", method = RequestMethod.GET)
    public ApplyToSolrStatusDto getApplyTagsToSolrTaskStatus() {
        ApplyToSolrStatusDto applyToSolrStatusDto = new ApplyToSolrStatusDto();
        applyToSolrStatusDto.setApplyToSolrStatus(ApplyToSolrStatus.DONE);
        return applyToSolrStatusDto;
    }

    @RequestMapping(value = "/type", method = RequestMethod.GET)
    public List<FileTagTypeDto> getAllFileTagTypes() {
        return fileTaggingAppService.getFileTagTypes(false, userService.getCurrentUser().getId());
    }

    @RequestMapping(value = "/type/tree", method = RequestMethod.GET)
    public List<FileTagTypeAndTagsDto> getAllFileTagTypesAndChildren(@RequestParam final Map<String, String> params) {
        // page request is ignored on this request
        return fileTaggingAppService.getAllFileTagTypesAndChildren(userService.getCurrentUser().getId());
    }
}
