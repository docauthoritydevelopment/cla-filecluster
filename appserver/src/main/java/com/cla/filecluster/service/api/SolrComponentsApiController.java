package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.components.solr.SolrCollectionDto;
import com.cla.common.domain.dto.components.solr.SolrNodeDto;
import com.cla.common.domain.dto.components.solr.SolrShardDto;
import com.cla.filecluster.service.syscomponent.SolrComponentsAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by uri on 29-May-17.
 */
@RestController
@RequestMapping("/api/sys/solr")
public class SolrComponentsApiController {

    @Autowired
    SolrComponentsAppService solrComponentsAppService;

    Logger logger = LoggerFactory.getLogger(SolrComponentsApiController.class);

    @RequestMapping(value="/collections/{collection}/shards", method = RequestMethod.GET)
    public List<SolrShardDto> listCollectionShards(@PathVariable String collection) {
        return solrComponentsAppService.getCollectionShards(collection);
    }

    @RequestMapping(value="/collections/{collection}/nodes", method = RequestMethod.GET)
    public List<SolrNodeDto> listCollectionNodes(@PathVariable String collection) {
        return solrComponentsAppService.getCollectionLiveNodes(collection);
    }

    @RequestMapping(value="/collections", method = RequestMethod.GET)
    public List<SolrCollectionDto> listCollections() {
        return solrComponentsAppService.listCollections();
    }


    @RequestMapping(value="/collections/{collection}/shards/replicate/{shard}", method = {RequestMethod.GET, RequestMethod.POST})
    public void replicateShard(@PathVariable String collection, @PathVariable String shard) {
        solrComponentsAppService.replicateShard(collection,shard);
    }

    @RequestMapping(value="/collections/{collection}/shards/{shard}/delete/{replica}", method = {RequestMethod.GET, RequestMethod.POST})
    public void deleteShardReplica(@PathVariable String collection, @PathVariable String shard,@PathVariable String replica) {
        solrComponentsAppService.deleteShardReplica(collection,shard,replica);
    }

    @RequestMapping(value = "/request/{requestId}")
    public void getSolrRequestStatus(@PathVariable String requestId) {
        solrComponentsAppService.getSolrRequestStatus(requestId);
    }
}
