package com.cla.filecluster.service.crawler.pv;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.solr.common.SolrInputDocument;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SolrDocHelper  {

	private final Map<String, Object> singleValueFields = Maps.newHashMap();
	private final Map<String, List<Object>> multiValueFields = Maps.newHashMap();
	
	public Object getValue(String fieldName) {
		return singleValueFields.get(fieldName);
	}

	public Map<String, Object> getSingleValueFields() {
		return singleValueFields;
	}

	public Object setValue(String fieldName, Object value) {
		return singleValueFields.put(fieldName, value);
	}

	public List<Object> getValues(String fieldName) {
		return multiValueFields.get(fieldName);
	}

	private void addValue(String fieldName, Object value) {
		List<Object> oldValues = multiValueFields.computeIfAbsent(fieldName, k -> Lists.newArrayList());
		oldValues.add(value);
	}

	public void addValues(String fieldName, Collection<Object> values) {
		List<Object> oldValues = multiValueFields.computeIfAbsent(fieldName, k -> Lists.newArrayList());
		oldValues.addAll(values);
	}

	public void addListValues(String fieldName, List<String> values) {
		addValue(fieldName, values);
	}
	
	public SolrInputDocument toSolrDoc() {
		final SolrInputDocument res = new SolrInputDocument();
		
		singleValueFields.forEach(res::setField);

		multiValueFields.forEach((key, value) -> {
			Map<String, Object> mapVal = new HashMap<>(1);
			mapVal.put("add", value);
			res.setField(key, mapVal);
		});

		return res;
	}

}
