package com.cla.filecluster.service.progress;

import com.cla.connector.utils.Pair;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProgressService {

    private static final Logger log = LoggerFactory.getLogger(ProgressService.class);

    @Value("${progress_service.max_progresses:1000}")
    private Integer maxProgresses;

    @Value("${progress_service.progress-expiration-time-minutes:1440}")
    private Integer progressExpireTimeMinutes;

    private Cache<String, ProgressBean> progresses;

    @PostConstruct
    public void init() {
        progresses =  CacheBuilder.newBuilder()
                .maximumSize(maxProgresses)
                .expireAfterAccess(progressExpireTimeMinutes, TimeUnit.MINUTES)
                .build();
    }

    @Scheduled(cron = "* */30 * * * *")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    private void progressListCleaner() {
        progresses.cleanUp();
    }

    public String createNewProgress(ProgressType progressType, String initialMessage, int initialPercentage, Pair<String, Object> initialPayload) {
        return createProgress(progressType, ProgressMessage.of(initialPercentage, initialMessage, initialPayload, false))
                .getProgressId();
    }

    public String createNewProgress(ProgressType progressType, ProgressMessage initialProgressMessage) {
        return createProgress(progressType, initialProgressMessage)
                .getProgressId();
    }

    public String createChildProgress(String fatherProgressId, float subProgressWeight, ProgressMessage initialProgressMessage) {
        ProgressBean fatherProgress = getProgressById(fatherProgressId);
        ProgressBean childProgress = createProgress(ProgressType.CHILD, initialProgressMessage);
        fatherProgress.addComposingProgress(childProgress, subProgressWeight);
        return childProgress.getProgressId();
    }

    public String createChildProgress(String fatherProgressId, float subProgressWeight, String initialMessage) {
        return createChildProgress(fatherProgressId, subProgressWeight, ProgressMessage.of(0, initialMessage, null, false));
    }

    public String createChildProgress(String fatherProgressId, String initialMessage) {
        return createChildProgress(fatherProgressId, 1F, ProgressMessage.of(0, initialMessage, null, false));
    }

    public Handler createHandler(String progressId){
        return new Handler(progressId);
    }

    public Handler createChildHandler (String fatherProgressId, float subProgressWeight, ProgressMessage initialProgressMessage) {
        String childProgress = createChildProgress(fatherProgressId, subProgressWeight, initialProgressMessage);
        return createHandler(childProgress);
    }

    public Handler createChildHandler (String fatherProgressId, float subProgressWeight, String initialMessage) {
        return createChildHandler(fatherProgressId,
                subProgressWeight,
                ProgressMessage.of(0, initialMessage, null, false));
    }

    public Handler createChildHandler (String fatherProgressId, String initialMessage) {
        return createChildHandler(fatherProgressId, 1F, ProgressMessage.of(0, initialMessage, null, false));
    }

    public ProgressBean getProgress(String progressId) {
        return ImmutableProgressBean.of(getProgressById(progressId));
    }

    public Set<ProgressBean> getProgresses() {
        return progresses.asMap()
                .values()
                .stream()
                .filter(isCurrentUser())
                .map(ImmutableProgressBean::of)
                .collect(ImmutableSet.toImmutableSet());
    }

    public Set<ProgressBean> getProgresses(Collection<String> progressIds) {
        if (progressIds == null) {
            throw new NullPointerException("getProgresses: supplied parameter can not no be null");
        }

        return progressIds.stream()
                .map(progresses::getIfPresent)
                .filter(Objects::nonNull)
                .filter(isCurrentUser())
                .map(ImmutableProgressBean::of)
                .collect(ImmutableSet.toImmutableSet());
    }

    public Set<ProgressBean> getProgresses(ProgressType progressType) {
        if (progressType == null) {
            throw new NullPointerException("cannot get progresses by type: null");
        }
        return progresses
                .asMap()
                .values()
                .stream()
                .filter(progressBean -> progressType.equals(progressBean.getProgressType()))
                .filter(isCurrentUser())
                .map(ImmutableProgressBean::of)
                .collect(ImmutableSet.toImmutableSet());
    }

    @NotNull
    private Predicate<ProgressBean> isCurrentUser() {
        return progressBean -> SecurityContextHolder.getContext().getAuthentication().getName().equals(progressBean.getOwner());
    }

    public void closeProgress (String progressId) {
        try {
            Set<String> toRemove = Stream.concat(
                    Stream.of(progressId),
                    getProgressById(progressId)
                            .getComposingProgresses()
                            .keySet()
                            .stream()
                            .map(ProgressBean::getProgressId))
                    .collect(Collectors.toSet());
            progresses.invalidateAll(toRemove);
        } catch (Exception e) {
            log.error("fail close progress id {} error {}", progressId, e.getMessage());
        }
    }

    private ProgressBean createProgress(ProgressType progressType, ProgressMessage initialProgressMessage) {
        ProgressBean progressBean = new ProgressBean(progressType, initialProgressMessage);
        progressBean.setOwner(SecurityContextHolder.getContext().getAuthentication().getName());
        progresses.put(progressBean.getProgressId(), progressBean);
        return progressBean;
    }

    public boolean isProgressExists(String progressId) {
        ProgressBean progressBean = progresses.getIfPresent(progressId);
        return progressBean != null;
    }

   private ProgressBean getProgressById(String progressId) {
        ProgressBean progressBean = progresses.getIfPresent(progressId);

        if (progressBean == null) {
            log.info("Could not resolve progress element for id {}", progressId);
            throw new ProgressMissingException(String.format("Could not resolve progress element for id %s", progressId));
        }

        if (!SecurityContextHolder.getContext().getAuthentication().getName().equals(progressBean.getOwner())) {
            log.warn("permission error trying to load progress: {}", progressId);
            throw new RuntimeException(String.format("permission error trying to load progress: %s", progressId));
        }

        return progressBean;
    }


    public class Handler {

        private final String progressId;

        protected Handler(String progressId) {
            this.progressId = progressId;
        }

        public String getProgressId() {
            return progressId;
        }

        public void set(int percentage) {
            set(ProgressMessage.of(percentage, null, null, false));
        }

        public void set(int percentage, String message) {
            set(ProgressMessage.of(percentage, message, null, false));
        }

        public void set(int percentage, String message, Pair<String, Object> payload) {
            set(ProgressMessage.of(percentage, message, payload, false));
        }

        public void set(int percentage, String message, Pair<String, Object> payload, boolean done) {
            set(ProgressMessage.of(percentage, message, payload, done));
        }

        public void set(ProgressMessage progressMessage) {
            ProgressBean progress = getProgressById(progressId);
            progress.setProgressMessage(progressMessage);
        }

        public void inc(int percentage) {
            inc(ProgressMessage.of(percentage, null, null, false));
        }

        public void inc(int percentage, String message) {
            inc(ProgressMessage.of(percentage, message, null, false));
        }

        public void inc(int percentage, String message, Pair<String, Object> payload) {
            inc(ProgressMessage.of(percentage, message, payload, false));
        }

        public void inc(int percentage, String message, Pair<String, Object> payload, boolean done) {
            inc(ProgressMessage.of(percentage, message, payload, done));
        }

        public void inc(ProgressMessage progressMessage) {
            ProgressBean progress = getProgressById(progressId);
            progress.addProgressMessage(progressMessage);
        }

        public void fail(String message, Throwable exception) {
            ProgressBean progress = getProgressById(progressId);
            progress.setFailed(message, exception);

        }

        public boolean isDone() {
            ProgressBean progress = getProgressById(progressId);
            return progress.isDone();
        }

        public void setDone() {
            ProgressBean progress = getProgressById(progressId);
            progress.setDone();
        }
    }
}
