package com.cla.filecluster.service.export.exporters.components.mixin;

import com.cla.common.domain.dto.components.ComponentState;
import com.cla.common.domain.dto.dataCenter.CustomerDataCenterDto;
import com.cla.filecluster.service.export.serialize.LongToExcelDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import static com.cla.filecluster.service.export.exporters.components.ComponentHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/13/2019
 */
public class ClaComponentDtoMixin {

    @JsonProperty(INSTANCE)
    private String instanceId;

    @JsonProperty(NAME)
    private String componentType;

    @JsonProperty(STATE)
    private ComponentState state;

    @JsonProperty(EXTERNAL_ADDRESS)
    private String externalNetAddress;

    @JsonProperty(STATE_CNG_DATE)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long stateChangeDate;

    @JsonProperty(RESPONSE_DATE)
    @JsonSerialize(using = LongToExcelDateSerializer.class)
    private Long lastResponseDate;

    @JsonUnwrapped
    private CustomerDataCenterDto customerDataCenterDto;
}
