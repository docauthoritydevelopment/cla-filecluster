package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.components.*;
import com.cla.common.domain.dto.messages.FileContentResultMessage;
import com.cla.common.domain.dto.messages.FileContentType;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.files.managers.FileContentService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import com.cla.filecluster.service.syscomponent.SysComponentService;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.cla.common.domain.dto.security.Authority.Const.*;

/**
 * Created by liora on 02/03/2017.
 */
@RestController
@RequestMapping("/api/sys/components")
public class SysComponentApiController {

    @Autowired
    private SysComponentService sysComponentService;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Autowired
    private FileContentService fileContentService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private MessageHandler messageHandler;

    @Value(("${file-content-service.file-get-timeout-min:3}"))
    private int fileGetTimeoutMinutes;

    @Value("${mng-agent.port:8091}")
    private int mngAgentPort;

    private Logger logger = LoggerFactory.getLogger(SysComponentApiController.class);


    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Page<ClaComponentDto> listComponents(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        String customerDataCenterId = params.get("customerDataCenterId");
        if (customerDataCenterId != null) {
            return sysComponentService.findSysComponentByCustomerDataCenter(Long.valueOf(customerDataCenterId), ClaComponentType.MEDIA_PROCESSOR, dataSourceRequest);
        }
        return sysComponentService.listAllSysComponents(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/listWithStatus", method = RequestMethod.GET)
    public Page<ClaComponentSummaryDto> listComponentsWithStatus(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        Page<ClaComponentSummaryDto> result = sysComponentService.listComponentsWithStatus(dataSourceRequest, params);

        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Server components export", AuditAction.VIEW,  ClaComponentSummaryDto.class,null, dataSourceRequest);
        }

        return result;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ClaComponentDto getComponent(@PathVariable long id) {
        ClaComponentDto sysComponent = sysComponentService.findComponentById(id);
        if (sysComponent == null) {
            throw new BadRequestException(messageHandler.getMessage("system-component.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return sysComponent;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_SCAN_CONFIG})
    @RequestMapping(value = "/new", method = RequestMethod.PUT)
    public ClaComponentDto createComponent(@RequestBody final ClaComponentDto componentDto) {
        if (ClaComponentType.MEDIA_PROCESSOR.equals(componentDto.getClaComponentType())) {
            componentDto.setState(ComponentState.PENDING_INIT);
            ClaComponentDto sysComponent = sysComponentService.createSysComponent(componentDto);
            eventAuditService.audit(AuditType.MEDIA_PROCESSOR, "Add pending init MEDIA_PROCESSOR ", AuditAction.CREATE, ClaComponentDto.class, componentDto.getId(), componentDto);
            return sysComponent;
        }
        throw new BadRequestException(messageHandler.getMessage("system-component.create.type.invalid"), BadRequestType.UNSUPPORTED_VALUE);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_SCAN_CONFIG})
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ClaComponentDto updateComponent(@PathVariable Long id, @RequestBody final ClaComponentDto componentDto) {
        componentDto.setId(id);
        ClaComponentDto claComponentDto = componentsAppService.updateSysComponent(componentDto);
        eventAuditService.audit(AuditType.MEDIA_PROCESSOR, "Update pending init MEDIA_PROCESSOR ", AuditAction.UPDATE, ClaComponentDto.class, componentDto.getId(), componentDto);
        return claComponentDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_SCAN_CONFIG})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ClaComponentDto deleteComponent(@PathVariable Long id) {
        ClaComponentDto claComponentDto = componentsAppService.deleteSysComponent(id);
        if (claComponentDto != null) {
            eventAuditService.audit(AuditType.MEDIA_PROCESSOR, "Delete MEDIA_PROCESSOR ", AuditAction.DELETE, ClaComponentDto.class, id, claComponentDto);
        }
        return claComponentDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT, MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/{componentId}/status/history", method = RequestMethod.GET)
    public Page<ClaComponentStatusDto> listComponentsStatusHistory(@PathVariable long componentId, @RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);

        Long dcId = 0L;
        if (params.containsKey("dataCenterID")) {
            String dcStr = params.get("dataCenterID");
            if (!Strings.isNullOrEmpty(dcStr)) {
                dcId = Long.parseLong(dcStr);
            }
        }
        long timestamp = System.currentTimeMillis();
        if (params.containsKey("timestamp")) {
            String timestampStr = params.get("timestamp");
            if (!Strings.isNullOrEmpty(timestampStr)) {
                timestamp = Long.parseLong(timestampStr);
            }
        }

        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"system components status export", AuditAction.VIEW,  ClaComponentStatusDto.class,null, dataSourceRequest);
        }

        return sysComponentService.listSysComponentsStatus(componentId, timestamp, dcId, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/fetchMP/{instanceId}", method = RequestMethod.GET)
    public void getMediaProcessorLogs(final HttpServletRequest request, final HttpServletResponse response, @PathVariable Long instanceId) throws IOException {
        if (instanceId == null) {
            throw new BadRequestException(messageHandler.getMessage("system-component.instance.empty"), BadRequestType.MISSING_FIELD);
        }
        ClaComponentDto claComponentDto = sysComponentService.findComponentByInstanceId(ClaComponentType.MEDIA_PROCESSOR, instanceId.toString());
        if (claComponentDto == null) {
            throw new BadRequestException(messageHandler.getMessage("system-component.instance.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        try {
            CompletableFuture<FileContentResultMessage> completableFuture = new CompletableFuture<>();
            String requestId = fileContentService.addRequestToLockMap(completableFuture);
            fileContentService.sendRequestForLogsForMediaProcessor(requestId, instanceId);
            FileContentResultMessage fileContentResultMessage = completableFuture.get(fileGetTimeoutMinutes, TimeUnit.MINUTES);
            if (fileContentResultMessage.getRequestType().equals(FileContentType.ERROR)) {
                response.setStatus(500);
                response.getWriter().append(fileContentResultMessage.getPayload().getContent());
            } else {
                fileContentService.openFile(fileContentResultMessage, request, response, instanceId);
            }
        } catch (TimeoutException e) {
            logger.error("Timeout exception while waiting for download logs from media processor instance {}", instanceId, e);
            response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
        } catch (Exception e) {
            response.setStatus(500);
            response.getWriter().append("Can not get log file from media processor");
            logger.error("Failed to get log file from media processor instance {}", instanceId, e);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/logs/component/{id}", method = RequestMethod.GET)
    public void getComponentLogs(final HttpServletRequest request, final HttpServletResponse response, @PathVariable Long id) throws IOException {
        ClaComponentDto claComponentDto = sysComponentService.findComponentById(id);

        if (claComponentDto == null) {
            throw new BadRequestException(messageHandler.getMessage("system-component.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        // get address
        String address = Strings.isNullOrEmpty(claComponentDto.getExternalNetAddress()) ? claComponentDto.getLocalNetAddress() : claComponentDto.getExternalNetAddress();

        // get one value if there are several
        address = address.indexOf(",") > 0 ? address.substring(0, address.indexOf(",")) : address;

        // remove orig port
        int indAftHttp = address.contains("http:") ? address.indexOf("http:") + "http:".length() : 0;
        address = address.indexOf(":", indAftHttp) > 0 ? address.substring(0, address.indexOf(":", indAftHttp)) : address;

        // add http if needed
        address = (address.startsWith("http") ? address : "http://" + address);

        // create url
        String urlAddress = address + ":" + mngAgentPort + "/management/logs/fetch/active/components";
        BufferedInputStream bis = null;
        try {
            URL url = new URL(urlAddress);
            bis = new BufferedInputStream(url.openStream());
            String fileName = "component-"+id+"-logs.zip";
            fileContentService.openFile(bis, fileName, fileName, null,  request,  response, id, null);
        } catch (Exception e) {
            response.setStatus(500);
            response.getWriter().append("Can not get log file from component id " +id + " in address "+urlAddress);
            logger.error("Failed to get log file from component id {} in address {}", id, urlAddress, e);
        } finally {
            try {bis.close();} catch (Exception e) {}
        }
    }
}
