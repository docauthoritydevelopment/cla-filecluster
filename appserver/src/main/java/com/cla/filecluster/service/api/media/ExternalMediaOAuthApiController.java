package com.cla.filecluster.service.api.media;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by uri on 22/05/2016.
 */
@Controller
@RequestMapping("/api/media")
public class ExternalMediaOAuthApiController {

//    @Autowired
//    BoxMediaAppService boxMediaAppService;

    private final Logger logger = LoggerFactory.getLogger(ExternalMediaOAuthApiController.class);

//    @RequestMapping(value = "/box/login", method = RequestMethod.GET)
//    public String login() {
//        String authorizeUrl = buildBoxAuthorizeUrl();
//        logger.debug(authorizeUrl);
//        return "redirect:"+authorizeUrl;
//    }

//    private String buildBoxAuthorizeUrl() {
//        return boxMediaAppService.buildAuthorizeUrl();
//    }

}
