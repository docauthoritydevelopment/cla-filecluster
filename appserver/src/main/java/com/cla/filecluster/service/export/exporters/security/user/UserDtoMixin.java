package com.cla.filecluster.service.export.exporters.security.user;

import com.cla.common.domain.dto.security.UserState;
import com.cla.common.domain.dto.security.UserType;
import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.security.user.UserHeaderFields.*;

/**
 * Created by: yael
 * Created on: 4/24/2019
 */
public class UserDtoMixin {

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(USERNAME)
    private String username;

    @JsonProperty(STATE)
    private UserState userState;

    @JsonProperty(TYPE)
    private UserType userType;

    @JsonProperty(EMAIL)
    private String email;

    @JsonProperty(CNG_PWD)
    private boolean passwordMustBeChanged;

    @JsonProperty(LDAP_USER)
    private boolean ldapUser;

    @JsonProperty(LOCK)
    private boolean accountNonLocked;
}
