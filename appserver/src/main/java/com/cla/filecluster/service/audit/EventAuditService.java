package com.cla.filecluster.service.audit;

import com.cla.connector.utils.TextUtils;
import com.cla.filecluster.domain.dto.AuditDto;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.audit.DaAuditEvent;
import com.cla.filecluster.repository.jpa.entitylist.DaAuditEventRepository;
import com.cla.filecluster.service.security.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Audit service to write records to the audit table
 * Created by uri on 23/02/2016.
 */
@Service
public class EventAuditService {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private Logger logger = LoggerFactory.getLogger(EventAuditService.class);

    @Autowired
    private AuditWrapper auditWrapper;

    @Autowired
    private UserService userService;

    @Autowired
    private DaAuditEventRepository auditEventRepository;

    @Value("${subprocess:false}")
    private boolean subProcess;

    @PostConstruct
    public void init() {
        if (subProcess) {
            logger.info("SubProcess - Audit service not initialized");
        }
    }

    public Page<AuditDto> getAuditTrail(String username, List<String> crudAction, List<String> types, Long fromTime, Long toTime, PageRequest pageRequest) {
        Page<DaAuditEvent> result;

        if ((types == null || types.isEmpty()) && (crudAction == null || crudAction.isEmpty())) {
            result = auditEventRepository.getAll(
                    username == null ? "%" : username,
                    fromTime == null ? new Long(0) : fromTime,
                    toTime == null ? Long.MAX_VALUE : toTime,
                    pageRequest);
        } else if (types == null || types.isEmpty()) {
            result = auditEventRepository.getAllWithAction(
                    username == null ? "%" : username,
                    crudAction.stream().map(String::toUpperCase).collect(Collectors.toList()),
                    fromTime == null ? new Long(0) : fromTime,
                    toTime == null ? Long.MAX_VALUE : toTime,
                    pageRequest);
        } else if (crudAction == null || crudAction.isEmpty()) {
            result = auditEventRepository.getAllWithType(
                    username == null ? "%" : username,
                    types.stream().map(String::toUpperCase).collect(Collectors.toList()),
                    fromTime == null ? new Long(0) : fromTime,
                    toTime == null ? Long.MAX_VALUE : toTime,
                    pageRequest);
        } else {
            result = auditEventRepository.getAllWithActionAndType(
                    username == null ? "%" : username,
                    types.stream().map(String::toUpperCase).collect(Collectors.toList()),
                    crudAction.stream().map(String::toUpperCase).collect(Collectors.toList()),
                    fromTime == null ? new Long(0) : fromTime,
                    toTime == null ? Long.MAX_VALUE : toTime,
                    pageRequest);
        }

        return convertAuditDtos(result, pageRequest);
    }

    public void audit(AuditType auditType, String message, AuditAction crudAction) {
        audit(auditType, message, crudAction,null, null);
    }

    public void audit(AuditType auditType, String message, AuditAction crudAction, Class objectType, String objectId) {
        auditDual(auditType, message, crudAction, objectType, objectId, null, null);
    }

    public void audit(AuditType auditType, String message, AuditAction crudAction, Class objectType, Object objectId, Object... params) {
        try {
            auditDual(auditType, message, crudAction, objectType, objectId, null, null, params);
        } catch (Exception e) {
            logger.error("Failed to jon objectId for audit record for {} {} {}",auditType,crudAction,e);
        }
    }

    public void auditDual(AuditType auditType, String message, AuditAction crudAction, Class objectType, Object objectId, Class onObjectType, Object onObjectId, Object... params) {
        try {
            String serializedParams = MAPPER.writeValueAsString(params);
            serializedParams = TextUtils.removePassword(serializedParams);
            audit(auditType, message, crudAction,
                    (objectType != null ? objectType.toString() : null), (objectId != null ? objectId.toString() : null),
                    (onObjectType != null ? onObjectType.toString() : null), (onObjectId != null ? onObjectId.toString() : null), serializedParams);
        } catch (Exception e) {
            logger.error("Failed to json params for audit record for {} {} {}",auditType,crudAction,e);
        }
    }

    private void audit(AuditType auditType, String message, AuditAction crudAction, String objectType, String objectId, String onObjectType, String onObjectId, String serializedParams) {
        try {
            String username = userService.getPrincipalUsername();
            long timestamp = System.currentTimeMillis();

            AuditDto auditDto = new AuditDto(0L, username, timestamp, auditType, message, crudAction, objectType, objectId, onObjectType, onObjectId, serializedParams);
            auditWrapper.audit(auditDto);
        } catch (Exception e) {
            logger.error("Failed to audit record for {} {} {}", auditType, crudAction, e);
        }
    }


    private static Page<AuditDto> convertAuditDtos(Page<DaAuditEvent> audits, PageRequest pageRequest) {
        List<DaAuditEvent> entityPageContent = audits.getContent();
        List<AuditDto> dtoPageContent = new ArrayList<>(entityPageContent.size());
        for (DaAuditEvent event : entityPageContent) {
            dtoPageContent.add(convertAudit(event));
        }
        return new PageImpl<>(dtoPageContent, pageRequest, audits.getTotalElements());
    }

    private static AuditDto convertAudit(DaAuditEvent audit) {
        return new AuditDto(
                audit.getId(),
                audit.getUsername(),
                audit.getTimeStampMs(),
                AuditType.valueOf(audit.getSubSystem()),
                audit.getMessage(),
                AuditAction.valueOf(audit.getAction()),
                audit.getObjectType(),
                audit.getObjectId(),
                audit.getOnObjectType(),
                audit.getOnObjectId(), audit.getParams());
    }
}
