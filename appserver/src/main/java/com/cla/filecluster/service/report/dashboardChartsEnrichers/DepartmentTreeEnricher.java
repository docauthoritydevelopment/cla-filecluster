package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.domain.dto.department.DepartmentDto;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.filetype.FileTypeCategory;
import com.cla.filecluster.service.convertion.FileDtoFieldConverter;
import com.cla.filecluster.service.department.DepartmentService;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


@Component
public class DepartmentTreeEnricher implements CategoryTypeDataEnricher {
    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.ACTIVE_ASSOCIATIONS, "deptTree");
    }

    @Override
    public Map<String, String> getCategoryFieldQueryValuesMap(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = new DataSourceRequest();
        dataSourceRequest.setPage(1);
        dataSourceRequest.setPageSize(Integer.MAX_VALUE);
        List<DepartmentDto> allDepartments = departmentService.getDepartments(dataSourceRequest).getContent();
        Map<Long, DepartmentDto> departmentsMap = allDepartments.stream().
                collect(Collectors.toMap(DepartmentDto::getId, Function.identity()));
        Map<String, List<String>> ansMap = new HashMap<>();
        allDepartments.forEach(departmentDto -> {
            DepartmentDto tmpDepartmentDto = departmentDto;
            while (tmpDepartmentDto.getParentId() != null) {
                tmpDepartmentDto = departmentsMap.get(tmpDepartmentDto.getParentId());
            }
            ansMap.putIfAbsent(String.valueOf(tmpDepartmentDto.getId()),new ArrayList<>());
            ansMap.get(String.valueOf(tmpDepartmentDto.getId())).add(DepartmentDto.ID_PREFIX + departmentDto.getId());
        });

        return ansMap.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> DashboardChartsUtil.getQueryTypeValueStringAccordingToValuesList(e.getValue())
                ));
    }

    @Override
    public void enrichChartData(DashboardChartDataDto dashboardChartDataDto, Map<String, String> params) {
        dashboardChartDataDto.setCategories(dashboardChartDataDto.getCategories().stream().map((category) ->
        {
            return departmentService.getByIdCached(Long.parseLong(category)).getName();
        }).collect(Collectors.toList()));
    }


    @Override
    public String getFilterFieldName() {
        return FileDtoFieldConverter.DtoFields.departmentId.getAlternativeName();
    }


}
