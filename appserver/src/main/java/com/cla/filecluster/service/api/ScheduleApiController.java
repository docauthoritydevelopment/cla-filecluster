package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.crawler.RunStatus;
import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.common.domain.dto.filetree.RootFolderSummaryInfo;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupSummaryInfoDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.utils.Pair;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.schedule.ScheduleAppService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.service.schedule.ScheduleSchemaLoader;
import com.cla.filecluster.util.ControllerUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by uri on 02/05/2016.
 */
@RestController
@RequestMapping("/api/schedule")
public class ScheduleApiController {

    public static final String WITH_ROOT_FOLDERS = "withRootFolders";

    @Autowired
    private ScheduleAppService scheduleAppService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private ScheduleSchemaLoader scheduleSchemaLoader;

    @Autowired
    private MessageHandler messageHandler;

    private Logger logger = LoggerFactory.getLogger(ScheduleApiController.class);

    @RequestMapping(value="/groups/summary", method = RequestMethod.GET)
    public Page<ScheduleGroupSummaryInfoDto> listScheduleGroupsSummaryInfo(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.addDefaultSort("name");
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Schedule groups summary Export", AuditAction.VIEW,  ScheduleGroupSummaryInfoDto.class,null, dataSourceRequest);
        }
        String groupNameSearch = params == null ? null : params.get("groupNamingSearchTerm");
        String groupIdsStr = params == null ? null : params.get("scheduleGroupIds");

        List<Long> groupIds = null;
        if (!Strings.isNullOrEmpty(groupIdsStr)) {
            groupIds = Arrays.stream(groupIdsStr.split(",")).map(Long::valueOf).collect(Collectors.toList());
        }

        if (params != null && params.getOrDefault(WITH_ROOT_FOLDERS,"false").equals("true")) {
            return scheduleAppService.listScheduleGroupsSummaryInfoWithRootFolders(dataSourceRequest, groupNameSearch, groupIds);
        }
        return scheduleAppService.listScheduleGroupsSummaryInfo(dataSourceRequest, groupNameSearch, groupIds);
    }

    @RequestMapping(value="/groups/summary/{id}", method = RequestMethod.GET)
    public ScheduleGroupSummaryInfoDto getScheduleGroupSummaryInfo(@PathVariable Long id, @RequestParam final Map<String,String> params) {
        return scheduleAppService.getScheduleGroupSummaryInfo(id);
    }

    @RequestMapping(value="/groups", method = RequestMethod.GET)
    public Page<ScheduleGroupDto> listAllScheduleGroups(@RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.addDefaultSort("name");
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Schedule groups Export", AuditAction.VIEW,  ScheduleGroupDto.class,null, dataSourceRequest);
        }
        return scheduleAppService.listAllScheduleGroups(dataSourceRequest);
    }

    @RequestMapping(value="/names", method = RequestMethod.GET)
    public Map<Long, String> getScheduleGroupsNames() {
        return scheduleGroupService.getScheduleGroupsNames();
    }

    @RequestMapping(value="/groups/byroot/{rootFolderId}", method = RequestMethod.GET)
    public Page<ScheduleGroupDto> listRootFolderScheduleGroups(@PathVariable Long rootFolderId, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return scheduleAppService.listRootFolderScheduleGroups(rootFolderId,dataSourceRequest);
    }

    @RequestMapping(value="/groups/{id}", method = RequestMethod.GET)
    public ScheduleGroupDto listScheduleGroup(@PathVariable Long id) {
        return scheduleAppService.listScheduleGroup(id);
    }

    @RequestMapping(value="/groups/{id}/rootfolders", method = RequestMethod.GET)
    public Page<RootFolderDto> listScheduleGroupRootFolders(@PathVariable Long id, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Root folders export", AuditAction.VIEW,  RootFolderDto.class,null, dataSourceRequest);
        }
        return scheduleAppService.listScheduleGroupRootFolders(id,dataSourceRequest);
    }

    @RequestMapping(value="/groups/{id}/rootfolders/summary", method = RequestMethod.GET)
    public Page<RootFolderSummaryInfo> listScheduleGroupRootFoldersSummary(@PathVariable Long id, @RequestParam final Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        dataSourceRequest.addDefaultSort("number");
        List<RunStatus> filterByRunStatus = ControllerUtils.getStatesList(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT,"Root folders summary export", AuditAction.VIEW,  RootFolderSummaryInfo.class,null, dataSourceRequest);
        }
        return scheduleAppService.listScheduleGroupRootFoldersSummary(id, dataSourceRequest, filterByRunStatus);
    }

    @RequestMapping(value="/groups", method = RequestMethod.PUT)
    public ScheduleGroupDto createScheduleGroup(@RequestBody final ScheduleGroupDto scheduleGroupDto) {
        ScheduleGroupDto scheduleGroup = scheduleAppService.createScheduleGroup(scheduleGroupDto);
        eventAuditService.audit(AuditType.SCHEDULE_GROUP,"Create schedule group", AuditAction.CREATE, ScheduleGroupDto.class,scheduleGroupDto.getId(),scheduleGroup);
        return scheduleGroup;
    }

    @RequestMapping(value="/groups/{id}", method = RequestMethod.POST)
    public ScheduleGroupDto updateScheduleGroup(@PathVariable Long id, @RequestBody final ScheduleGroupDto scheduleGroupDto) {
        scheduleGroupDto.setId(id);
        ScheduleGroupDto scheduleGroupDto1 = scheduleAppService.updateScheduleGroup(scheduleGroupDto);
        eventAuditService.audit(AuditType.SCHEDULE_GROUP,"Update schedule group", AuditAction.UPDATE, ScheduleGroupDto.class,scheduleGroupDto1.getId(),scheduleGroupDto1);
        return scheduleGroupDto1;
    }

    @RequestMapping(value = "/groups/{id}/active", method = RequestMethod.POST)
    public ScheduleGroupDto updateScheduleGroupActive(@PathVariable Long id, @RequestBody Boolean state) {
        ScheduleGroupDto scheduleGroup = getScheduleGroup(id);
        ScheduleGroupDto scheduleGroupDto = scheduleAppService.updateScheduleGroupActive(id, state);
        eventAuditService.audit(AuditType.SCHEDULE_GROUP,"Update schedule group", AuditAction.UPDATE, ScheduleGroupDto.class,scheduleGroupDto.getId(),scheduleGroupDto);
        return scheduleGroupDto;
    }

    @RequestMapping(value="/groups/{id}", method = RequestMethod.DELETE)
    public void deleteScheduleGroup(@PathVariable Long id) {
        ScheduleGroupDto scheduleGroup = getScheduleGroup(id);
        scheduleAppService.deleteScheduleGroup(id);
        eventAuditService.audit(AuditType.SCHEDULE_GROUP,"Delete schedule group", AuditAction.DELETE, ScheduleGroupDto.class, scheduleGroup.getId(),scheduleGroup);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.GET)
    public ScheduleGroupDto getScheduleGroup(@PathVariable Long id) {
        ScheduleGroupDto scheduleGroup = scheduleGroupService.getScheduleGroup(id);
        if (scheduleGroup == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return scheduleGroup;
    }

    @RequestMapping(value="/groups/{scheduleGroupId}/attachment/{rootFolderId}", method = RequestMethod.POST)
    public void attachRootFolderToSchedulingGroup(@PathVariable long scheduleGroupId, @PathVariable long rootFolderId) {
        ScheduleGroupDto scheduleGroup = getScheduleGroup(scheduleGroupId);
        ScheduleGroupDto scheduleGroupDto = scheduleAppService.updateScheduleGroupForRootFolder(rootFolderId, scheduleGroupId);
        eventAuditService.auditDual(AuditType.ROOT_FOLDER, "Update schedule group for root folder", AuditAction.ATTACH, RootFolderDto.class, rootFolderId, ScheduleGroupDto.class, scheduleGroupDto.getId(), scheduleGroupDto);
    }

    @RequestMapping(value="/groups/{scheduleGroupId}/attachment/{rootFolderId}", method = RequestMethod.DELETE)
    public void detachRootFolderFromSchedulingGroup(@PathVariable Long scheduleGroupId, @PathVariable Long rootFolderId) {
        scheduleAppService.detachRootFolderFromSchedulingGroup(scheduleGroupId,rootFolderId);
        eventAuditService.audit(AuditType.ROOT_FOLDER,"Remove schedule group for root folder", AuditAction.DETACH, RootFolderDto.class, rootFolderId, Pair.of("scheduleGroupId",scheduleGroupId));
    }

    @RequestMapping(value="/groups/import/csv", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto importCsvFile(@RequestParam("file") MultipartFile file, @RequestParam(value ="updateDuplicates",required = false,defaultValue = "false") boolean updateDuplicates){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
             logger.info("import connections CSV File. update duplicates set to {}",updateDuplicates);
            byte[] bytes = file.getBytes();
            ImportSummaryResultDto importSummaryResultDto = scheduleSchemaLoader.doImport(false, bytes, null, updateDuplicates);
            eventAuditService.audit(AuditType.SCHEDULE_GROUP,"Import schedule groups", AuditAction.IMPORT, MultipartFile.class,file.getName(), Pair.of("updateDuplicates",updateDuplicates));
            return importSummaryResultDto;
        } catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(),e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @RequestMapping(value="/groups/import/csv/validate", method=RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto validateImportCsvFile(@RequestParam("file") MultipartFile file,@RequestParam("maxErrorRowsReport") Integer maxErrorRowsReport){
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-group.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("validate import schedule CSV File");
            byte[] bytes = file.getBytes();
            return scheduleSchemaLoader.doImport(true,bytes,maxErrorRowsReport==null?200:maxErrorRowsReport,false);
       } catch (Exception e) {
            logger.error("Validate import CSV error:" + e.getMessage(),e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription("Validate import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

}
