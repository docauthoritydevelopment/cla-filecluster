package com.cla.filecluster.service.report.dashboardChartsEnrichers;

import com.cla.common.constants.CatFileFieldType;
import com.cla.filecluster.service.report.DashboardChartsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AccessDatePartitionEnricher extends DatePartitionEnricher{

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    @Override
    public String getEnricherType() {
        return dashboardChartsUtil.getDashboardChartEnricherUniqueName(CatFileFieldType.LAST_ACCESS, DatePartitionEnricher.DATE_PARTITION_DUMMY_PREFIX);
    }

    public String getFilterFieldName(){
        return CatFileFieldType.LAST_ACCESS.getSolrName();
    }

}
