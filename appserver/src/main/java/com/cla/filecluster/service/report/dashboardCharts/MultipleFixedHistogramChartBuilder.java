package com.cla.filecluster.service.report.dashboardCharts;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.ChartType;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.*;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.report.*;
import com.rometools.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class MultipleFixedHistogramChartBuilder implements DashboardChartDtoBuilder {

    private static final int DYNAMIC_QUERY_FILTER_IDX = 0;
    private static final String QUERY_KEY_PREFIX = "query_";
    private static final String OTHER_ITEM_TITLE = "Other";
    private static final String OTHER_ITEM_ID = "OTHER";
    private static final String OTHER_FACET_NAME = "OTHER";

    @Override
    public ChartType getType() {
        return ChartType.MULTIPLE_FIXED_HISTOGRAM;
    }

    @Autowired
    private SolrSpecificationFactory solrSpecificationFactory;

    @Autowired
    private SolrFileCatRepository solrFileCatRepository;


    @Autowired
    private DashboardChartFactory dashboardChartFactory;

    @Autowired
    private DashboardChartsUtil dashboardChartsUtil;

    public String getAuditMsg(Map<String, String> params) {
        CatFileFieldType groupedField = dashboardChartsUtil.extractGroupedField(params);
        String queryPrefix = dashboardChartsUtil.extractPrefixField(params);
        return "Report " +
                dashboardChartsUtil.translateSolrFieldName(groupedField, queryPrefix).toLowerCase();
    }


    public Double parseResponseToOtherValue(List<SolrFacetJsonResponseBucket> buckets, DashboardChartValueTypeDto valueField) {

        for (SolrFacetJsonResponseBucket bucket : buckets) {
            boolean isDeleted = bucket.getBooleanVal();
            if (!isDeleted) {
                SolrFacetJsonResponseBucket categoryBucket = bucket.getBucketBasedFacets(OTHER_FACET_NAME);
                Double countValue;
                if (valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
                    countValue = categoryBucket.getDoubleCountNumber();
                } else {
                    Object statFacetValue = categoryBucket.getObjectValueByKey(valueField.getSolrField().getSolrName());
                    if (statFacetValue == null) {
                        countValue = 0d;
                    } else {
                        countValue = DashboardChartsUtil.getResponseDoubleValueFromObject(
                                statFacetValue);
                    }
                }
                return countValue;

            }
        }
        return 0d;
    }


    public SolrFacetQueryJson buildOtherQueryJsonFacet(SolrSpecification solrSpecification,
                                                       FilterDescriptor filter,
                                                       SolrCoreSchema field,
                                                       DashboardChartValueTypeDto valueField, String queryStr,
                                                       String queryName) {

        SolrFacetQueryJson theFacet = new SolrFacetQueryJson(queryName != null ? queryName : filter.getName());
        theFacet.setType(FacetType.TERMS);
        theFacet.setField(CatFileFieldType.DELETED);
        List<String> filterStrList = new ArrayList<>();
        if (filter != null && (filter.getValue() != null || !filter.getFilters().isEmpty())) {
            filterStrList.addAll(dashboardChartsUtil.getAndQueriesArray(filter, solrSpecification));
        }
        if (!filterStrList.isEmpty()) {
            // this replace is becuase the JSON facet  domain filter need double slash
            filterStrList = filterStrList.stream().map(DashboardChartsUtil::fixSolrSlashes)
                    .map(DashboardChartsUtil::fixDoubleQuoteSlashes)
                    .collect(Collectors.toList());
            theFacet.setFilterList(filterStrList);
        }
        SolrFacetQueryJson catFacet = new SolrFacetQueryJson(OTHER_FACET_NAME);
        catFacet.setType(FacetType.QUERY);
        catFacet.setField(field);
        catFacet.setQueryValueString(queryStr);
        if (!valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(valueField.getSolrField().getSolrName());
            innerFacet.setType(FacetType.SIMPLE);
            innerFacet.setField(valueField.getSolrField());
            innerFacet.setFunc(valueField.getSolrFunction());
            catFacet.addFacet(innerFacet);
        }
        theFacet.addFacet(catFacet);
        return theFacet;
    }

    /**
     * @param params - request query params
     * @return DashboardChartDataDto
     */
    public DashboardChartDataDto buildChartDataDto(Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        FilterDescriptor[] segmentList = dashboardChartsUtil.extractSegmentList(params);
        CatFileFieldType groupedField = dashboardChartsUtil.extractGroupedField(params);
        DashboardChartValueTypeDto valueField = dashboardChartsUtil.extractChartValueField(params);
        String queryPrefix = dashboardChartsUtil.extractPrefixField(params);
        boolean needToReturnOther = dashboardChartsUtil.extractReturnOther(params);
        CatFileFieldType seriesField = dashboardChartsUtil.extractSeriesField(params);
        Integer seriesItemsNumber = dashboardChartsUtil.extractSeriesItemNumber(params);
        String seriesPrefix = dashboardChartsUtil.extractSeriesPrefixField(params);

        FilterDescriptor dynamicFilter = seriesField != null ? DashboardChartsUtil.getUnfilterFilterDescriptor() : segmentList[DYNAMIC_QUERY_FILTER_IDX];
        DashboardChartDataDto resultDashboardData;
        DashboardChartQueryBuilder dashboardChartQueryBuilder;

        DashboardDataEnricher dashboardChartEnricher = dashboardChartFactory.getEnricher(groupedField, queryPrefix);
        dashboardChartQueryBuilder = dashboardChartFactory.getQueryBuilder(dashboardChartEnricher.getQueryType());

        resultDashboardData = new DashboardChartDataDto();
        Object initialGetDataResult = dashboardChartQueryBuilder.setInitialDataToDashboardChartDto(resultDashboardData,
                groupedField, dataSourceRequest, dynamicFilter, queryPrefix, valueField, params, null, dashboardChartEnricher);


        if (seriesField != null ) {
            String additionFilter = needToReturnOther ? null : dashboardChartQueryBuilder.getAdditionalFilterByCategories(initialGetDataResult, resultDashboardData.getCategories(), groupedField, dashboardChartEnricher, params);
            DataSourceRequest seriesDataSourceRequest = DataSourceRequest.build(params);
            seriesDataSourceRequest.setPage(1);
            seriesDataSourceRequest.setPageSize(seriesItemsNumber);
            DashboardDataEnricher seriesChartEnricher = dashboardChartFactory.getEnricher(seriesField, seriesPrefix);
            DashboardChartQueryBuilder seriesQueryBuilder = dashboardChartFactory.getQueryBuilder(seriesChartEnricher.getQueryType());
            segmentList = seriesQueryBuilder.getSeriesQueryResults(seriesField,
                    seriesPrefix,
                    valueField,
                    seriesDataSourceRequest,
                    params,
                    additionFilter,
                    seriesChartEnricher);
            resultDashboardData.getSeries().clear();
        }


        ArrayList<Double> otherResponseList = new ArrayList<>();

        if (resultDashboardData.getSeries().size() < segmentList.length || needToReturnOther) {

            boolean findContentFilter = false;
            for (int index = resultDashboardData.getSeries().size(); index < segmentList.length && !findContentFilter; index++) {
                findContentFilter = segmentList[index].containsContentFilter();
            }

            if (findContentFilter) {
                for (int index = resultDashboardData.getSeries().size(); index < segmentList.length; index++) {

                    SolrSpecification facetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                            dataSourceRequest);

                    SolrFacetQueryJson singleFacetQueryJson = dashboardChartQueryBuilder.
                            buildQueryJsonFacetByFilter(initialGetDataResult, facetSpecification, segmentList[index],
                                    groupedField, queryPrefix, valueField, resultDashboardData.getCategories().toArray(),
                                    QUERY_KEY_PREFIX + index, params, true, null);
                    if (singleFacetQueryJson == null) {
                        createEmptySeries(segmentList[index], resultDashboardData);
                    } else {
                        facetSpecification.addFacetJsonObject(singleFacetQueryJson);
                    }
                    SolrFacetQueryJsonResponse queryResponse = solrFileCatRepository.runFacetQueryJson(facetSpecification);
                    creatSeriesByQueryResponseKey(params, segmentList, valueField, resultDashboardData, dashboardChartQueryBuilder, initialGetDataResult, queryResponse, index);
                }
                if (needToReturnOther && resultDashboardData.getCategories().size() > 0) {
                    for (int index = 0; index < segmentList.length; index++) {
                        SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                                dataSourceRequest);
                        ArrayList<SolrFacetQueryJson> otherFacetLists = new ArrayList<>();
                        String queryStr = dashboardChartQueryBuilder.getOtherQuery(initialGetDataResult, resultDashboardData.getCategories(), params);
                        otherFacetLists.add(buildOtherQueryJsonFacet(solrFacetSpecification,
                                segmentList[index],
                                groupedField,
                                valueField, queryStr,
                                QUERY_KEY_PREFIX + index));
                        solrFacetSpecification.setFacetJsonObjectList(otherFacetLists);
                        SolrFacetQueryJsonResponse otherResponse = solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);
                        otherResponseList.add(parseResponseToOtherValue(otherResponse.getResponseBucket(QUERY_KEY_PREFIX + index).getBuckets(), valueField));
                    }
                }
            } else {
                SolrSpecification mainFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                        dataSourceRequest);

                ArrayList<SolrFacetQueryJson> jsonFacetLists = new ArrayList<>();
                for (int index = resultDashboardData.getSeries().size(); index < segmentList.length; index++) {
                    SolrFacetQueryJson singleFacetQueryJson = dashboardChartQueryBuilder.buildQueryJsonFacetByFilter(initialGetDataResult, mainFacetSpecification, segmentList[index],
                            groupedField, queryPrefix, valueField, resultDashboardData.getCategories().toArray(),
                            QUERY_KEY_PREFIX + index, params, true, null);
                    if (singleFacetQueryJson != null) {
                        jsonFacetLists.add(singleFacetQueryJson);
                    }
                }

                if (jsonFacetLists.size() == 0 && !needToReturnOther) {
                    for (int index = resultDashboardData.getSeries().size(); index < segmentList.length; index++) {
                        createEmptySeries(segmentList[index], resultDashboardData);
                    }
                    return resultDashboardData;
                }

                if (jsonFacetLists.size() > 0) {
                    mainFacetSpecification.setFacetJsonObjectList(jsonFacetLists);
                    SolrFacetQueryJsonResponse queryResponse = solrFileCatRepository.runFacetQueryJson(mainFacetSpecification);
                    for (int index = resultDashboardData.getSeries().size(); index < segmentList.length; index++) {
                        creatSeriesByQueryResponseKey(params, segmentList, valueField, resultDashboardData, dashboardChartQueryBuilder, initialGetDataResult, queryResponse, index);
                    }
                }

                if (needToReturnOther && resultDashboardData.getCategories().size() > 0) {
                    SolrSpecification solrFacetSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(
                            dataSourceRequest);
                    ArrayList<SolrFacetQueryJson> otherFacetLists = new ArrayList<>();
                    String queryStr = dashboardChartQueryBuilder.getOtherQuery(initialGetDataResult, resultDashboardData.getCategories(), params);
                    for (int index = 0; index < segmentList.length; index++) {
                        otherFacetLists.add(buildOtherQueryJsonFacet(solrFacetSpecification,
                                segmentList[index],
                                groupedField,
                                valueField, queryStr,
                                QUERY_KEY_PREFIX + index));
                    }
                    solrFacetSpecification.setFacetJsonObjectList(otherFacetLists);
                    SolrFacetQueryJsonResponse otherResponse = solrFileCatRepository.runFacetQueryJson(solrFacetSpecification);
                    for (int index = 0; index < segmentList.length; index++) {
                        otherResponseList.add(parseResponseToOtherValue(otherResponse.getResponseBucket(QUERY_KEY_PREFIX + index).getBuckets(), valueField));
                    }
                }
            }
        }

        List<String> catIds = new ArrayList<>(resultDashboardData.getCategories());
        catIds = catIds.stream().map((catId) ->  dashboardChartEnricher.convertSolrValueToId(catId)).collect(Collectors.toList());

        resultDashboardData.setCategoryIds(catIds);

        if (!resultDashboardData.getCategories().isEmpty()) {
            dashboardChartQueryBuilder.enrichChartData(initialGetDataResult, resultDashboardData, params, dashboardChartEnricher);
        } else if (Objects.isNull(resultDashboardData.getTotalElements())) {
            resultDashboardData.setTotalElements(0l);
        }

        if (Lists.isNotEmpty(otherResponseList)) {
            resultDashboardData.getCategories().add(OTHER_ITEM_TITLE);
            resultDashboardData.getCategoryIds().add(OTHER_ITEM_ID);
            for (int index = 0; index < resultDashboardData.getSeries().size(); index++) {
                resultDashboardData.getSeries().get(index).getData().add(otherResponseList.get(index));
            }
        }
        return resultDashboardData;
    }


    private void createEmptySeries(FilterDescriptor filterDescriptor, DashboardChartDataDto resultDashboardData) {
        DashboardChartSeriesDataDto seriesObj = new DashboardChartSeriesDataDto();
        seriesObj.setName(dashboardChartsUtil.getNameOfFilter(filterDescriptor));
        resultDashboardData.getSeries().add(seriesObj);
    }

    private void creatSeriesByQueryResponseKey(Map<String, String> params, FilterDescriptor[] segmentList, DashboardChartValueTypeDto valueField, DashboardChartDataDto resultDashboardData, DashboardChartQueryBuilder dashboardChartQueryBuilder, Object initialGetDataResult, SolrFacetQueryJsonResponse queryResponse, int index) {
        SolrFacetJsonResponseBucket bucketBasedFacets = queryResponse.getResponseBucket(
                QUERY_KEY_PREFIX + index);

        if (bucketBasedFacets.isEmpty()) {
            createEmptySeries(segmentList[index], resultDashboardData);
        } else {
            dashboardChartQueryBuilder.parseResponseToDashboardChartDataDto(
                    initialGetDataResult,
                    resultDashboardData,
                    bucketBasedFacets.getBuckets(),
                    dashboardChartsUtil.getNameOfFilter(segmentList[index]),
                    valueField,
                    params, true);
        }
    }
}
