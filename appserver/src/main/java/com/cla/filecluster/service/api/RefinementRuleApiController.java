package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.group.GroupType;
import com.cla.common.domain.dto.pv.RefinementRuleDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.exceptions.BadParameterException;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.operation.GroupLockService;
import com.cla.filecluster.service.operation.ScheduledOperationService;
import com.cla.filecluster.service.operation.RefinementRulesApplyGroupOperation;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.crawler.RefinementRuleService;
import com.cla.filecluster.service.extractor.GroupsService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.cla.common.domain.dto.security.Authority.Const.MNG_GROUPS;

/**
 * Controller for managing refinement rules for raw analysis groups and applying them
 *
 * Created by: yael
 * Created on: 2/5/2018
 */
@RestController
@RequestMapping("/api/refinementrules")
public class RefinementRuleApiController {

    @Autowired
    private RefinementRuleService refinementRuleService;

    @Autowired
    private GroupsService groupsService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private GroupLockService groupLockService;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private ScheduledOperationService scheduledOperationService;

    @RequestMapping(value = "/listGroupsWithRefinementRules", method = RequestMethod.GET)
    public Page<GroupDto> listGroupsWithRefinementRules(@RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        PageRequest req = dataSourceRequest.createPageRequest();
        List<GroupDto> groups = groupsService.findGroupsWithRefinementRules();
        return new PageImpl<>(groups, req, groups.size());
    }

    @RequestMapping(value = "/listRulesForGroup/{id}", method = RequestMethod.GET)
    public Page<RefinementRuleDto> getRulesForGroup(@PathVariable String id, @RequestParam Map<String,String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        PageRequest req = dataSourceRequest.createPageRequest();
        List<RefinementRuleDto> rules = refinementRuleService.getByRawAnalysisGroupId(id);
        return new PageImpl<>(rules, req, rules.size());
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "/applyRulesForGroup/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public void applyRulesForGroup(@PathVariable String id, @RequestParam Map<String,String> params) {
        FileGroup group = validateParentGroup(id);
        ScheduledOperationMetadata data = new ScheduledOperationMetadata();
        data.setOperationType(ScheduledOperationType.REFINEMENT_RULE_APPLY);
        data.setOpData(Collections.singletonMap(RefinementRulesApplyGroupOperation.RAW_ANALYSIS_GROUP_PARAM, id));
        data.setUserOperation(true);

        data.setDescription(messageHandler.getMessage("operation-description.apply-rules",
                Lists.newArrayList(group.getNameForReport())));

        scheduledOperationService.createNewOperation(data);
        eventAuditService.audit(AuditType.REFINEMENT_RULE,"Start refinementRule apply", AuditAction.START, FileGroup.class, id);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "/isApplyRulesForGroupRunning/{id}", method = RequestMethod.GET)
    public boolean isApplyRulesForGroupRunning(@PathVariable String id) {
        return groupLockService.isRawAnalysisGroupLockTaken(id);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "", method = RequestMethod.PUT)
    public RefinementRuleDto createRefinementRule(@RequestBody RefinementRuleDto rule) {
        validateParentGroup(rule);
        RefinementRuleDto refinementRule = refinementRuleService.createRefinementRule(rule);
        eventAuditService.audit(AuditType.REFINEMENT_RULE,"Create refinementRule", AuditAction.CREATE, RefinementRuleDto.class,rule.getId(),rule);
        return refinementRule;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteRefinementRule(@PathVariable long id) {
        RefinementRuleDto refinementRule = refinementRuleService.markRefinementRuleDeleted(id);
        eventAuditService.audit(AuditType.REFINEMENT_RULE,"Delete refinementRule", AuditAction.DELETE, RefinementRuleDto.class, id, refinementRule);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "/delete/{rawAnalysisGroupId}", method = RequestMethod.DELETE)
    public void deleteRefinementRules(@PathVariable String rawAnalysisGroupId) {
        refinementRuleService.markRefinementRulesDeleted(rawAnalysisGroupId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public RefinementRuleDto updateRefinementRule(@PathVariable Long id, @RequestBody RefinementRuleDto rule) {
        validateParentGroup(rule);
        RefinementRuleDto refinementRule = refinementRuleService.updateRefinementRule(rule, true);
        eventAuditService.audit(AuditType.REFINEMENT_RULE,"Update refinementRule", AuditAction.UPDATE, RefinementRuleDto.class,rule.getId(),rule);
        return refinementRule;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "", method = RequestMethod.POST)
    public List<RefinementRuleDto> updateRefinementRules(@RequestBody List<String> phrases, String rawAnalysisGroupId) {
        if (phrases == null || phrases.isEmpty()) {
            throw new BadParameterException(messageHandler.getMessage("refinement-rules.update.phrases.empty"), BadRequestType.MISSING_FIELD);
        }
        if (Strings.isNullOrEmpty(rawAnalysisGroupId)) {
            throw new BadParameterException(messageHandler.getMessage("refinement-rules.raw-analysis-group-id.empty"), BadRequestType.MISSING_FIELD);
        }
        FileGroup fg = validateParentGroup(rawAnalysisGroupId);
        String rawAnalysisGroupName = (fg.getName() != null ? fg.getName() : fg.getGeneratedName());

        return refinementRuleService.updateRefinmentRulesByPhrasesList(phrases, rawAnalysisGroupId, rawAnalysisGroupName);
    }

    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public RefinementRuleDto getRefinementRule(@PathVariable long id) {
        RefinementRuleDto refinementRule = refinementRuleService.getRefinementRule(id);
        if (refinementRule == null) {
            throw new BadParameterException(messageHandler.getMessage("refinement-rule.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return refinementRule;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles={MNG_GROUPS})
    @RequestMapping(value = "/{id}/active", method = RequestMethod.POST)
    public RefinementRuleDto updateScheduleGroupActive(@PathVariable Long id, @RequestBody Boolean state) {
        RefinementRuleDto refinementRule = refinementRuleService.updateRefinementRuleActive(id, state);
        eventAuditService.audit(AuditType.REFINEMENT_RULE,"Update refinementRule", AuditAction.UPDATE, RefinementRuleDto.class,refinementRule.getId(),refinementRule);
        return refinementRule;
    }

    private void validateParentGroup(RefinementRuleDto dto) {
        validateParentGroup(dto.getRawAnalysisGroupId());
    }

    private FileGroup validateParentGroup(String rawAnalysisGroupId) {
        if (StringUtils.isEmpty(rawAnalysisGroupId)) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.parent-group.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
        FileGroup gr = groupsService.findById(rawAnalysisGroupId);
        if (!gr.getGroupType().equals(GroupType.ANALYSIS_GROUP)) {
            throw new BadRequestException(messageHandler.getMessage("refinement-rules.parent-group-type.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
        return gr;
    }
}
