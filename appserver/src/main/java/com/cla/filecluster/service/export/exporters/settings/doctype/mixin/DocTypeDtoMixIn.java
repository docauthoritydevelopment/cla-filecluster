package com.cla.filecluster.service.export.exporters.settings.doctype.mixin;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.settings.doctype.DocTypeSettingsHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/12/2019
 */
public class DocTypeDtoMixIn {

    @JsonProperty(ID)
    private Long id;

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(UNIQUE)
    private String uniqueName;

    @JsonProperty(PARENT)
    private Long parentId;
}
