package com.cla.filecluster.service.department;

import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.filecluster.service.importEntities.SchemaData;

/**
 * Created by: yael
 * Created on: 5/28/2019
 */
public class DepartmentSchemaData extends SchemaData<DepartmentItemRow> {

    @Override
    public DepartmentItemRow createImportItemRowInstance() {
        return new DepartmentItemRow();
    }
}
