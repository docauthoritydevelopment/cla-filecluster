package com.cla.filecluster.service.crawler.pv;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

@Component
public class ExtractTokenizer {
    private final static String SEPERATOR = " ";

    private Map<Long, String> debugHashMapper = null;

    @Value("#{'${removalStrs:999999999}'.split(',')}")
    private List<String> removalStrs;

    //All punctuation ->   "\\p{P}\\p{S}";
    @Value("${tokenizer.ignoreChars:[\\-\\']}")
    private String ignoreChars;
    @Value("${tokenizer.separatorChars:}")
    // no need for , replacement as it is properly handled by the tokenizer.
    private String separatorChars;

    @Value("${entityExtraction.token.minValueLength:3}")
    private int minValueLength;

    private static final boolean regexSearchAndReplace = true;


    /*
     * TODO: Create tokenStreamFactory class to allow generation of multiple tokenStreams per need:
     * 		- Ngrams per language
     * 		- TokenMatcher doc ingestion
     * 		- ToeknMatcher Entity property processing
     */

    /*
     * Entity extraction tokenizer
     */
    private final static Pattern zeroLeadingNumber = Pattern.compile("\\b(0+)([1-9][0-9\\.\\,/]*)\\b");
    private final static Pattern commaPattern = Pattern.compile("([^0-9\\s])[\\.;\\,]");
    private final static Pattern commaZeroSearchPattern = Pattern.compile("(\\b(0+?)([1-9][0-9\\.\\,/]*)\\b)|(([^0-9s])[\\.;\\,])");

    private static String fillTokensExtractorStringFilter(final String input) {
        /*
         * Pre-processing of the input string prior to tokenization:
         * zero leading numbers are trimmed prior to tokenization: "000222,33" -> "222,33"
         * Replace a non-number comma/period/semicolon with a space
         */
        if (regexSearchAndReplace && !commaZeroSearchPattern.matcher(input).find()) {
            return input;
        }
        return zeroLeadingNumber.matcher(commaPattern.matcher(input).replaceAll("$1 ")).replaceAll("$2");
    }

    private static StringReader getFillTokensStringReader(final String input) {
        return new StringReader(fillTokensExtractorStringFilter(input));
    }

    private static Tokenizer getFillTokensTokenizer(final String input) {
        Tokenizer t;
        t = new WhitespaceTokenizer();

        try {
            t.setReader(getFillTokensStringReader(input));
        } catch (Exception e) {
            throw new RuntimeException("Failed to set reader to fillTokensTokenizer",e);
        }

//			tokenizer = new WhitespaceTokenizer(new StringReader(zeroLeadingNumber.matcher(input).replaceAll("$1$2 $2")));
//			tokenizer = new StandardTokenizer(new StringReader(zeroLeadingNumber.matcher(input).replaceAll("$1$2 $2")));
        return t;
    }

    private static TokenStream getFillTokensTokenStream(final Tokenizer t) {
        TokenStream tokenStream;
        tokenStream = new WordDelimiterFilter(t,
                WordDelimiterFilter.GENERATE_WORD_PARTS | WordDelimiterFilter.CATENATE_NUMBERS |/*WordDelimiterFilter.CATENATE_WORDS|*/
                        WordDelimiterFilter.PRESERVE_ORIGINAL | WordDelimiterFilter.STEM_ENGLISH_POSSESSIVE,
                null);
        tokenStream = new LowerCaseFilter(tokenStream);
//			tokenStream = new StopFilter(tokenStream, EnglishAnalyzer.getDefaultStopSet());
        return tokenStream;
    }

    private static TokenStream getNewTokenStream(final String input) {
        final Tokenizer t = getFillTokensTokenizer(input);
        final TokenStream ts = getFillTokensTokenStream(t);
        ts.addAttribute(CharTermAttribute.class);
        return ts;
    }

    public List<Long> getHashList(final String input, final int len) {
        if (input.length() == 0) {
            return null;
        }

        TokenStream tokenStream = null;
        try {
            final List<Long> result = new ArrayList<>();
            StandardTokenizer standardTokenizer = new StandardTokenizer();
            standardTokenizer.setReader(new StringReader(input));
            // tokenStream = new StandardFilter(tokenStream);	// it does nothing starting LUCENE_31
            tokenStream = new LowerCaseFilter(standardTokenizer);
            tokenStream = new StopFilter(tokenStream, EnglishAnalyzer.getDefaultStopSet());
            final CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
            final LinkedList<String> fifo = new LinkedList<>();
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                final String token = charTermAttribute.toString();
                fifo.add(token);
                if (fifo.size() >= len) {
                    final String str = fifo.stream().collect(Collectors.joining(SEPERATOR));
                    result.add(MD5Long(str));
                    fifo.remove();
                }
            }
            // Get residue in case of not enough tokens
            if (result.size() == 0 && fifo.size() > 0) {
                result.add(MD5Long(fifo.stream().collect(Collectors.joining(SEPERATOR))));
            }
            return result;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                tokenStream.close();
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public List<String> getNgrams(final String input, final int len) {
        if (input.length() == 0) {
            return null;
        }

        TokenStream tokenStream = null;
        try {
            final List<String> result = new ArrayList<>();
            StandardTokenizer standardTokenizer = new StandardTokenizer();
            standardTokenizer.setReader(new StringReader(input));
            // tokenStream = new StandardFilter(tokenStream);	// it does nothing starting LUCENE_31
            tokenStream = new LowerCaseFilter(standardTokenizer);
            tokenStream = new StopFilter(tokenStream, EnglishAnalyzer.getDefaultStopSet());
            final CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
            final LinkedList<String> fifo = new LinkedList<>();
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                final String token = charTermAttribute.toString();
                fifo.add(token);
                if (fifo.size() >= len) {
                    result.add(fifo.stream().collect(Collectors.joining(SEPERATOR)));
                    fifo.remove();
                }
            }
            // Get residue in case of not enough tokens
            if (result.size() == 0 && fifo.size() > 0) {
                result.add(fifo.stream().collect(Collectors.joining(SEPERATOR)));
            }
            return result;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                tokenStream.close();
            } catch (final IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public String getHash(final String text) {
        final String filteredText = filter(text);
        if (filteredText == null || filteredText.isEmpty()) {
            return "";
        }
        final List<Long> tokens = getTokens(filteredText);
        final String hashedStr = tokens.stream().map(l -> String.format("%X", l)).collect(joining(" "));
        return hashedStr;
    }

    public String filter(final String input) {
        if (input == null || input.isEmpty()) {
            return null;
        }
        String output = input;
        if (separatorChars != null && !separatorChars.isEmpty()) {
            output = output.replaceAll(separatorChars, " ");        // Separators replaced with space)
        }
        if (ignoreChars != null && !ignoreChars.isEmpty()) {
            output = output.replaceAll(ignoreChars, "");        // Ignored chars to be removed
        }
        for (final String s : removalStrs) {
            output = output.replaceAll(s, " ");
        }
        return (output.isEmpty() ? null : output);
    }

    public long MD5Long(final String text) {
        try {
            final long hash = MD5Long(text.getBytes("UTF-8"/* "iso-8859-1" */));
            if (debugHashMapper != null) {
                debugHashMapper.put(hash, text);
            }
            return hash;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static long MD5Long(final byte[] bytes) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final byte[] md5hash = MD5(bytes);
        // Trim digest into the size of a long (8 bytes). MD5 uniformity should
        // guaranty uniformity within long value.
        final ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(md5hash, 0, 8);
        buffer.flip();// need flip
        return buffer.getLong();
    }

    public static byte[] MD5(final byte[] bytes) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(bytes);
        final byte[] md5hash = md.digest();
        return md5hash;
    }

    public static byte[] MD5(final String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        return MD5(text.getBytes("UTF-8"/* "iso-8859-1" */));
    }

    public List<String> fillTokens(final String input, final Collection<Long> tokens) {
        try (TokenStream tokenStream = getNewTokenStream(input)) {
            /*
             * Tokenizer requirements:
             * 	normalize, remove 's (Jon's -> Jon), remove '.' (I.B.M -> IBM), lowercase, include originals.
             */

            final List<String> deb = new ArrayList<>();

            final CharTermAttribute charTermAttribute = tokenStream.getAttribute(CharTermAttribute.class);
            tokenStream.reset();
            while (tokenStream.incrementToken()) {
                String token = charTermAttribute.toString();
                token = trimToken(token);
                if (token.length() >= minValueLength) {        // remove short tokens here
                    final Long tHash = MD5Long(token);
                    if (tokens != null) {
                        tokens.add(tHash);
                    }
                    deb.add(token);
                    if (debugHashMapper != null) {
                        debugHashMapper.put(tHash, token);
                    }
                    final String hStem = hebrewSimpleStem(token);
                    if (hStem != null) {
                        final Long sHash = MD5Long(hStem);
                        if (tokens != null) {
                            tokens.add(sHash);
                        }
                        deb.add(hStem);
                        if (debugHashMapper != null) {
                            debugHashMapper.put(sHash, hStem);
                        }
                    }
                }
            }
            deb.toString();
            return deb;
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<Long> getTokens(final String input) {
        final List<Long> tokens = new ArrayList<>();
        if (input != null && !input.isEmpty()) {
            fillTokens(input, tokens);
        }
        return tokens;
    }


    private final static String hebPrefixLetterStemming = "\ud791\ud79c\ud7a9\ud795\ud79b\ud97e";    // UTF8 BET LAMED SHIN VAV CHAF MEM

    // private final static String hebPrefixLetterStemming = "בלשוכמ";
    public static String hebrewSimpleStem(final String token) {
        final char c = token.charAt(0);
        return (hebPrefixLetterStemming.indexOf(c) >= 0 && token.length() > 2) ? token.substring(1) : null;
    }

    public static String trimToken(final String token) {
        int first = -1;
        int last = 0;
        final int l = token.length();
        for (int i = 0; i < l; ++i) {
            final char c = token.charAt(i);
            if (Character.isAlphabetic(c) || Character.isDigit(c)) {
                if (first < 0) {
                    first = i;
                }
                last = i;
            }
        }
        return (first >= 0) ? token.substring(first, last + 1) : "";
    }

    public Map<Long, String> getDebugHashMapper() {
        return debugHashMapper;
    }

    public void setDebugHashMapper(final Map<Long, String> debugHashMapper) {
        this.debugHashMapper = debugHashMapper;
    }

}
