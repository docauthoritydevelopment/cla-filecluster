package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.AggregationCountFolderDto;
import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.file.ClaFileDto;
import com.cla.common.domain.dto.file.FileContentDuplicationDto;
import com.cla.common.domain.dto.file.FileExtensionDto;
import com.cla.common.domain.dto.file.FileTypeCategoryDto;
import com.cla.common.domain.dto.filetree.*;
import com.cla.common.domain.dto.generic.ImportSummaryResultDto;
import com.cla.common.domain.dto.group.GroupDto;
import com.cla.common.domain.dto.schedule.ScheduleGroupDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.FolderExcludeRuleDto;
import com.cla.connector.domain.dto.media.MediaType;
import com.cla.connector.domain.dto.file.ServerResourceDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.filecluster.config.security.Permissions;
import com.cla.filecluster.domain.dto.ActiveScanQueryResult;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.domain.entity.audit.AuditType;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.exceptions.EntityAlreadyExistsException;
import com.cla.filecluster.domain.exceptions.LicenseExpiredException;
import com.cla.filecluster.jobmanager.service.JobManagerAppService;
import com.cla.filecluster.service.api.groups.GroupsApplicationService;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.audit.AuditAction;
import com.cla.filecluster.service.audit.EventAuditService;
import com.cla.filecluster.service.categories.FileCatAppService;
import com.cla.filecluster.service.files.FilesApplicationService;
import com.cla.filecluster.service.files.RootFolderService;
import com.cla.filecluster.service.files.filetree.*;
import com.cla.filecluster.service.files.managers.ExecutionManagerService;
import com.cla.filecluster.service.schedule.ScheduleGroupService;
import com.cla.filecluster.util.LocalFileUtils;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.security.Authority.Const.*;

/**
 * Handle rootFolders and docFolders configuration and reports
 * Created by uri on 13/08/2015.
 */

@RestController
@RequestMapping("/api/filetree")
public class FileTreeApiController {

    private static final Logger logger = LoggerFactory.getLogger(FileTreeApiController.class);

    @Autowired
    private FileCatAppService fileCatAppService;

    @Autowired
    private DocFolderService docFolderService;

    @Autowired
    private FilesApplicationService filesApplicationService;

    @Autowired
    private GroupsApplicationService groupsApplicationService;

    @Autowired
    private FileTreeAppService fileTreeAppService;

    @Autowired
    private RootfolderSchemaLoader rootfolderSchemaLoader;

    @Autowired
    private EventAuditService eventAuditService;

    @Autowired
    private ScheduleGroupService scheduleGroupService;

    @Autowired
    private EntityCredentialsService entityCredentialsService;

    @Autowired
    private RootFolderService rootFolderService;

    @Autowired
    private ExecutionManagerService executionManagerService;

    @Autowired
    private JobManagerAppService jobManagerAppService;

    @Autowired
    private MessageHandler messageHandler;

    @Value("${max.page.size.folder:28000}")
    private int maxFolderPageSize;

    @Value("${max.page.size.rootFolder:10000}")
    private int maxRootFolderPageSize;


    @SuppressWarnings("unchecked")
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/folders", method = RequestMethod.GET)
    public Page<DocFolderDto> listFoldersInDefaultDocStore(@RequestParam final Map params) {
        if (params != null && params.containsKey("id")) {
            //Kendo wants a subFolder expansion
            String folderIdString = (String) params.get("id");
            long folderId = Long.valueOf(folderIdString);
            return listFoldersFromSubFolder(folderId, params);
        }
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return fileTreeAppService.listAllFoldersInDefaultDocStore(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/rootfolders", method = RequestMethod.GET)
    public Page<RootFolderDto> listRootFolders(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params, maxRootFolderPageSize);
        return fileTreeAppService.listAllRootFoldersInDefaultDocStore(dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/new", method = RequestMethod.POST)
    public RootFolderDto createRootFolder(@RequestBody RootFolderDto rootFolderDto, @RequestParam(required = false) Long scheduleGroupId) {
        try {
            RootFolderDto rootFolder = rootFolderService.createRootFolder(rootFolderDto, scheduleGroupId);
            eventAuditService.audit(AuditType.ROOT_FOLDER, "Create root folder ", AuditAction.CREATE,
                    RootFolderDto.class, rootFolderDto.getId(), rootFolderDto);
            ScheduleGroupDto scheduleGroupDto = scheduleGroupService.getRootFolderScheduleGroup(rootFolder.getId(), false);
            if (scheduleGroupDto != null && scheduleGroupId != null) {
                eventAuditService.auditDual(AuditType.ROOT_FOLDER, "Add schedule group for root folder",
                        AuditAction.ATTACH, RootFolderDto.class, rootFolderDto.getId(),
                        ScheduleGroupDto.class, scheduleGroupDto.getId(), scheduleGroupDto);
            }
            jobManagerAppService.markStatusChange();
            return rootFolder;
        } catch (LicenseExpiredException e) {
            logger.error("Failed to create root folder - license expired: {}", e.getMessage());
            throw new BadRequestException(messageHandler.getMessage("root-folder.create.license"), BadRequestType.MAX_ROOT_FOLDERS);
        } catch (EntityAlreadyExistsException | BadRequestException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Failed to create rootfolder:" + e.getMessage(), e);
            throw new BadRequestException(messageHandler.getMessage("root-folder.create.failure"), BadRequestType.OPERATION_FAILED);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/import/csv", method = RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto importCsvFile(@RequestParam("file") MultipartFile file,
                                         @RequestParam(value = "updateDuplicates", required = false, defaultValue = "false") boolean updateDuplicates,
                                         @RequestParam(value = "createStubs", required = false, defaultValue = "false") boolean createStubs) {
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }
        try {
            logger.info("import rootfolders CSV File. update duplicates set to {}", updateDuplicates);
            byte[] bytes = LocalFileUtils.getFileBytesWithoutBOM(file);
            ImportSummaryResultDto result = rootfolderSchemaLoader.doImport(false, bytes, null, updateDuplicates, createStubs);
            jobManagerAppService.markStatusChange();
            return result;
        } catch (Exception e) {
            logger.error("Import CSV error:" + e.getMessage(), e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setResultDescription("Import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/import/csv/validate", method = RequestMethod.POST)
    public @ResponseBody
    ImportSummaryResultDto validateImportCsvFile(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("maxErrorRowsReport") Integer maxErrorRowsReport,
                                                 @RequestParam(value = "createStubs", required = false, defaultValue = "false") boolean createStubs) {
        if (file.isEmpty()) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.upload.file-empty"), BadRequestType.UNSUPPORTED_VALUE);
        }

        try {
            logger.info("validate import rootfolders CSV File");
            byte[] bytes = LocalFileUtils.getFileBytesWithoutBOM(file);
            return rootfolderSchemaLoader.doImport(true, bytes,
                    maxErrorRowsReport == null ? 200 : maxErrorRowsReport, false, createStubs);
        } catch (Exception e) {
            logger.error("Validate import CSV error:" + e.getMessage(), e);
            ImportSummaryResultDto importResultDto = new ImportSummaryResultDto();
            importResultDto.setImportFailed(true);
            importResultDto.setResultDescription("Validate import CSV error:" + e.getMessage());
            return importResultDto;
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/rootfolders/summary", method = RequestMethod.GET)
    public Page<RootFolderSummaryInfo> listRootFoldersSummary(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params, maxRootFolderPageSize);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Root folders summary export", AuditAction.VIEW, RootFolderSummaryInfo.class, null, dataSourceRequest);
        }
        return fileTreeAppService.listRootFoldersSummaryInfo(dataSourceRequest, false, false);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS}) // http://localhost:8080/api/filetree/activescan/summary
    @RequestMapping(value = "/activescan/summary", method = RequestMethod.GET)
    public ActiveScanQueryResult activeScanSummary(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params, maxFolderPageSize);
        return fileTreeAppService.activeScanSummaryInfo(dataSourceRequest, params);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/rootfolders/summary/{rootFolderId}", method = RequestMethod.GET)
    public RootFolderSummaryInfo listRootFolderSummary(@PathVariable Long rootFolderId) {
        return fileTreeAppService.getRootFolderSummaryInfo(rootFolderId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{id}", method = RequestMethod.DELETE)
    public void deleteRootFolder(@PathVariable long id) {
        RootFolderDto rootFolder = getRootFolder(id);
        if (rootFolderService.deleteRootFolder(id)) {
            eventAuditService.audit(AuditType.ROOT_FOLDER, "Delete root folder ", AuditAction.DELETE, RootFolderDto.class, rootFolder.getId(), rootFolder);
            jobManagerAppService.markStatusChange();
        } else {
            throw new BadRequestException(messageHandler.getMessage("root-folder.delete.forbidden"), BadRequestType.UNAUTHORIZED);
        }
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{id}/force", method = RequestMethod.DELETE)
    public void deleteRootFolderForce(@PathVariable long id) {
        RootFolderDto rootFolder = getRootFolder(id);
        rootFolderService.createDeleteRootFolderOperation(rootFolder);
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Delete root folder ", AuditAction.DELETE, RootFolderDto.class, rootFolder.getId(), rootFolder);
        jobManagerAppService.markStatusChange();
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/rootfolders/{id}", method = RequestMethod.GET)
    public RootFolderDto getRootFolder(@PathVariable long id) {
        RootFolderDto rootFolder = rootFolderService.getRootFolder(id);
        if (rootFolder == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return rootFolder;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{id}", method = RequestMethod.POST)
    public RootFolderDto updateRootFolder(@PathVariable long id, @RequestBody RootFolderDto rootFolderDto) {
        if (rootFolderService.equalsCurrentRootFolder(rootFolderDto)) {
            logger.info("Current RootFolder {} equals to update DTO. No actual save is needed.", id);
            return rootFolderDto;
        }
        RootFolderDto rootFolderDto1 = rootFolderService.updateRootFolder(id, rootFolderDto);
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Update root folder ", AuditAction.UPDATE, RootFolderDto.class, rootFolderDto1.getId(), rootFolderDto1);
        return rootFolderDto1;

    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{rootFolderId}/active", method = RequestMethod.POST)
    public RootFolderDto updateRootFolderActive(@PathVariable long rootFolderId, @RequestBody Boolean state) {
        RootFolderDto rootFolderDto = rootFolderService.updateRootFolderActive(rootFolderId, state);
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Update root folder ", AuditAction.UPDATE, RootFolderDto.class, rootFolderDto.getId(), rootFolderDto);
        return rootFolderDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{rootFolderId}/reingest", method = RequestMethod.POST)
    public RootFolderDto updateRootFolderReingest(@PathVariable long rootFolderId, @RequestBody Boolean reIngest) {
        RootFolderDto rootFolderDto = rootFolderService.updateRootFolderReingest(rootFolderId, reIngest);
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Update root folder ", AuditAction.UPDATE, RootFolderDto.class, rootFolderDto.getId(), rootFolderDto);
        return rootFolderDto;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/reingest", method = {RequestMethod.GET, RequestMethod.POST})
    public List<RootFolderDto> updateRootFoldersReingest(@RequestParam String rootFolderIds, @RequestParam boolean reIngest) {
        List<RootFolderDto> result = new ArrayList<>();
        if (!Strings.isNullOrEmpty(rootFolderIds)) {
            List<Long> rootFolderIdsList = Arrays.stream(rootFolderIds.split(",")).map(Long::valueOf).collect(Collectors.toList());
            rootFolderIdsList.forEach(rootFolderId -> {
                RootFolderDto rootFolderDto = rootFolderService.updateRootFolderReingest(rootFolderId, reIngest);
                eventAuditService.audit(AuditType.ROOT_FOLDER, "Update root folder ", AuditAction.UPDATE, RootFolderDto.class, rootFolderDto.getId(), rootFolderDto);
                result.add(rootFolderDto);
            });
        }
        return result;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/folders/exclude/new", method = RequestMethod.POST)
    public FolderExcludeRuleDto addGlobalFolderExcludeRule(@RequestBody FolderExcludeRuleDto folderExcludeRuleDto) {
        return rootFolderService.addGlobalFolderExcludeRule(folderExcludeRuleDto);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @SuppressWarnings("UnusedReturnValue")
    @RequestMapping(value = "/rootfolders/{rootFolderId}/exclude/new", method = RequestMethod.POST)
    public FolderExcludeRuleDto addRootFolderExcludeRule(@PathVariable long rootFolderId, @RequestBody FolderExcludeRuleDto folderExcludeRuleDto) {
        RootFolderDto rootFolder = getRootFolder(rootFolderId);
        FolderExcludeRuleDto folderExcludeRuleDto1 = rootFolderService.addRootFolderExcludeRule(rootFolderId, folderExcludeRuleDto);
        eventAuditService.auditDual(AuditType.ROOT_FOLDER, "Update root folder excluded folders", AuditAction.UPDATE,
                RootFolderDto.class, rootFolder.getId(), FolderExcludeRuleDto.class, folderExcludeRuleDto.getId(), folderExcludeRuleDto1);
        return folderExcludeRuleDto1;
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/folders/exclude", method = RequestMethod.GET)
    public Page<FolderExcludeRuleDto> listFolderExcludeRules(@RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        String rootFolderIdString = params.get("rootFolderId");
        Long rootFolderId = rootFolderIdString != null ? Long.valueOf(rootFolderIdString) : null;
        return rootFolderService.listFolderExcludeRules(dataSourceRequest, rootFolderId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG, RUN_SCANS})
    @RequestMapping(value = "/rootfolders/{rootFolderId}/exclude", method = RequestMethod.GET)
    public Page<FolderExcludeRuleDto> listFolderExcludeRulesByRootFolder(@PathVariable long rootFolderId, @RequestParam Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return rootFolderService.listFolderExcludeRules(dataSourceRequest, rootFolderId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{rootFolderId}/exclude/{id}", method = RequestMethod.DELETE)
    public void deleteFolderExcludeRule(@PathVariable long rootFolderId, @PathVariable("id") long folderExcludeRuleId) {
        RootFolderDto rootFolder = getRootFolder(rootFolderId);
        rootFolderService.deleteFolderExcludeRule(folderExcludeRuleId);
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Remove root folder excluded folders", AuditAction.UPDATE, RootFolderDto.class, rootFolder.getId(), FolderExcludeRuleDto.class, folderExcludeRuleId, rootFolder);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/rootfolders/{rootFolderId}/exclude/{id}", method = RequestMethod.POST)
    public FolderExcludeRuleDto updateFolderExcludeRule(@PathVariable long rootFolderId, @PathVariable long id, @RequestBody FolderExcludeRuleDto folderExcludeRuleDto) {
        RootFolderDto rootFolder = getRootFolder(rootFolderId);
        FolderExcludeRuleDto folderExcludeRuleDto1 = rootFolderService.updateFolderExcludeRule(id, folderExcludeRuleDto);
        eventAuditService.audit(AuditType.ROOT_FOLDER, "Update root folder excluded folders", AuditAction.UPDATE, RootFolderDto.class, rootFolder.getId(), FolderExcludeRuleDto.class, folderExcludeRuleDto1.getId(), rootFolder);
        return folderExcludeRuleDto1;
    }

    @RequestMapping(value = "/rootfolders/{id}/credentials", method = RequestMethod.GET)
    public List<EntityCredentialsDto> getRootFolderCredentials(@PathVariable long id) {
        RootFolderDto rootFolder = rootFolderService.getRootFolder(id);
        if (rootFolder == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        return entityCredentialsService.getEntityCredentialsForRootFolder(id);
    }

    @RequestMapping(value = "/rootfolders/{rootFolderId}/credentials", method = RequestMethod.PUT)
    public EntityCredentialsDto createRootFolderCredentials(@PathVariable long rootFolderId, @RequestBody EntityCredentialsDto entityCredentialsDto) {
        RootFolderDto rootFolder = rootFolderService.getRootFolder(rootFolderId);
        if (rootFolder == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        EntityCredentialsDto rootFolderCredentialsDto = entityCredentialsService.createEntityCredentials(entityCredentialsDto, rootFolder);
        eventAuditService.audit(AuditType.ENTITY_CREDENTIAL, "Create credentials for root folder " + rootFolderId, AuditAction.CREATE, EntityCredentialsDto.class, entityCredentialsDto.getId(), rootFolderCredentialsDto);
        return rootFolderCredentialsDto;
    }

    @RequestMapping(value = "/rootfolders/{rootFolderId}/credentials", method = RequestMethod.POST)
    public EntityCredentialsDto updateRootFolderCredentials(@PathVariable long rootFolderId, @RequestBody EntityCredentialsDto entityCredentialsDto) {
        RootFolderDto rootFolder = rootFolderService.getRootFolder(rootFolderId);
        if (rootFolder == null) {
            throw new BadRequestException(messageHandler.getMessage("root-folder.missing"), BadRequestType.ITEM_NOT_FOUND);
        }

        EntityCredentialsDto rootFolderCredentialsDto = entityCredentialsService.updateEntityCredentials(entityCredentialsDto, rootFolder);
        eventAuditService.audit(AuditType.ENTITY_CREDENTIAL, "Update credentials for root folder " + rootFolderId, AuditAction.UPDATE, EntityCredentialsDto.class, entityCredentialsDto.getId(), rootFolderCredentialsDto);
        return rootFolderCredentialsDto;
    }

    @RequestMapping(value = "/rootfolders/{rootFolderId}/credentials/{id}", method = RequestMethod.DELETE)
    public void deleteRootFolderCredentials(@PathVariable long rootFolderId, @PathVariable long id) {

        EntityCredentialsDto entityCredentialsDto = entityCredentialsService.getEntityCredentialsById(id);
        if (entityCredentialsDto == null) {
            throw new BadRequestException(messageHandler.getMessage("entity-credentials.missing"), BadRequestType.ITEM_NOT_FOUND);
        }
        if (entityCredentialsDto.getEntityId() != rootFolderId || entityCredentialsDto.getEntityType() != EntityType.ROOT_FOLDER) {
            throw new BadRequestException(messageHandler.getMessage("entity-credentials.missing-for-root-folder"), BadRequestType.UNSUPPORTED_VALUE);
        }
        entityCredentialsService.deleteEntityCredentials(id);
        eventAuditService.audit(AuditType.ENTITY_CREDENTIAL, "Delete credentials ", AuditAction.DELETE, EntityCredentialsDto.class, entityCredentialsDto.getId(), entityCredentialsDto);
    }

    /**
     * New API - to get the a lot of details about a particular folder.
     * <p>
     * 1. Folder Name
     * 2. number of files directly under folder
     * 3. Number of files under the folder in any level
     * 4. Number of folders under the folder
     *
     * @param id folder ID
     * @return Doc folder DTO
     */
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/folders/{id}", method = RequestMethod.GET)
    public DocFolderDto getFolderDetails(@PathVariable long id) {
        return fileTreeAppService.findDocFolder(id, true);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/folders/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountFolderDto> findFoldersFileCount(@RequestParam final Map<String, String> params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params, maxFolderPageSize);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover folders export", AuditAction.VIEW, DocFolderDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover folders", AuditAction.VIEW, DocFolderDto.class, null, dataSourceRequest);
        }
        return fileTreeAppService.findFoldersFileCount(dataSourceRequest);
    }

    /**
     * Bring the root node or the children of a node by ID.
     *
     * @param params parameters containing folder ID string
     * @return page of AggregationCountFolderDto
     */
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/folders/node/filecount", method = RequestMethod.GET)
    public FacetPage<AggregationCountFolderDto> findFoldersNodeFileCount(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params, maxFolderPageSize);
        if (params != null && params.containsKey("id")) {
            //Kendo wants a subFolder expansion
            String folderIdString = (String) params.get("id");
            long folderId = Long.valueOf(folderIdString);
            eventAuditService.audit(AuditType.QUERY, "Discover folders", AuditAction.VIEW, DocFolderDto.class, folderIdString, dataSourceRequest);
            return fileTreeAppService.findFoldersFileCountTreeNodeChildren(dataSourceRequest, folderId);
        }

        eventAuditService.audit(AuditType.QUERY, "Discover folders", AuditAction.VIEW, DocFolderDto.class, null, dataSourceRequest);
        return fileTreeAppService.findFoldersFileCountTreeRoot(dataSourceRequest);
    }

    @RequestMapping(value = "/folder/locate", method = RequestMethod.GET)
    public Long locateFolderByFullPath(@RequestParam final Map params) {
        String folderPath = (String)params.get("path");
        return docFolderService.locateFolderByFullPath(folderPath);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/folder/{folderId}/folders", method = RequestMethod.GET)
    public Page<DocFolderDto> listFoldersFromSubFolder(@PathVariable long folderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params, maxFolderPageSize);
        return fileTreeAppService.listFoldersFromSubFolder(folderId, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/folder/{folderId}/parent", method = RequestMethod.GET)
    public DocFolderDto getParentFolder(@PathVariable long folderId) {
        DocFolderDto result = docFolderService.findParentFolder(folderId);
        if (result == null) {
            logger.debug("Find parent folder of folderId {}. Folder is root - no parent", folderId);
        } else {
            logger.debug("Find parent folder of folderId {}. Returned folder id {}", folderId, result.getId());
        }
        return result;

    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/folder/{folderId}/files", method = RequestMethod.GET)
    public Page<ClaFileDto> listFilesInFolder(@PathVariable long folderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return filesApplicationService.listFilesByFolder(folderId, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/folder/{folderId}/files/recursive", method = RequestMethod.GET)
    public Page<ClaFileDto> listFilesInFolderRecursive(@PathVariable long folderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return filesApplicationService.listFilesByFolderRecursive(folderId, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/folder/{folderId}/groups", method = RequestMethod.GET)
    public Page<AggregationCountItemDTO<GroupDto>> listGroupDetailsInFolder(@PathVariable long folderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return groupsApplicationService.listGroupsByFolder(folderId, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/folder/{folderId}/groups/recursive", method = RequestMethod.GET)
    public Page<AggregationCountItemDTO<GroupDto>> listGroupDetailsInFolderRecursive(@PathVariable long folderId, @RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        return groupsApplicationService.listGroupsByFolderRecursive(folderId, dataSourceRequest);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {VIEW_FILES, VIEW_PERMITTED_FILES})
    @RequestMapping(value = "/server/folders", method = RequestMethod.GET)
    public List<ServerResourceDto> listServerFolders(@RequestParam(required = false) String id, @RequestParam(value = "connection", required = false) Long connectionId) {
        return fileTreeAppService.listServerFolders(id, connectionId);
    }

    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {MNG_SCAN_CONFIG})
    @RequestMapping(value = "/media/connections", method = RequestMethod.GET)
    public List<MediaConnectionDetailsDto> listMediaConnectionDetailsOptions(MediaType mediaType) {
        return fileTreeAppService.listMediaConnectionDetailsOptions(mediaType);
    }

    @Deprecated
    @PreAuthorize("isFullyAuthenticated()")
    @Permissions(roles = {TECH_SUPPORT})
    @RequestMapping(value = "/rebuild", method = {RequestMethod.GET, RequestMethod.POST})
    public void rebuildFileTree() {
        logger.warn("Rebuilding file tree rest call request");
        executionManagerService.startAsyncTask(() -> {
            logger.warn("Rebuilding file tree");
            fileTreeAppService.rebuildFileTree();
            return null;
        });
    }

    @RequestMapping("/extension/filecount")
    public FacetPage<AggregationCountItemDTO<FileExtensionDto>> getExtensionFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover extensions export", AuditAction.VIEW, FileExtensionDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover extensions", AuditAction.VIEW, FileExtensionDto.class, null, dataSourceRequest);
        }
        return fileCatAppService.getExtensionFileCounts(dataSourceRequest);
    }


    @RequestMapping("/fileTypeCategory/filecount")
    public FacetPage<AggregationCountItemDTO<FileTypeCategoryDto>> getFileTypeCategory(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover file type category export", AuditAction.VIEW, FileTypeCategoryDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover file type category", AuditAction.VIEW, FileTypeCategoryDto.class, null, dataSourceRequest);
        }
        FacetPage<AggregationCountItemDTO<FileTypeCategoryDto>> ans = fileTreeAppService.getTypeCategoryFileCounts(dataSourceRequest);
        return ans;
    }

    @RequestMapping("/content/filecount")
    public FacetPage<AggregationCountItemDTO<FileContentDuplicationDto>> getContentFileCounts(@RequestParam final Map params) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(params);
        if (dataSourceRequest.isExport()) {
            eventAuditService.audit(AuditType.EXPORT, "Discover content export", AuditAction.VIEW, FileContentDuplicationDto.class, null, dataSourceRequest);
        } else {
            eventAuditService.audit(AuditType.QUERY, "Discover content", AuditAction.VIEW, FileContentDuplicationDto.class, null, dataSourceRequest);
        }
        return fileCatAppService.getContentFileCounts(dataSourceRequest);
    }
}