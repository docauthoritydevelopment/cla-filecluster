package com.cla.filecluster.service.api;

import com.cla.common.domain.dto.messages.ContentMetadataRequestMessage;
import com.cla.common.domain.dto.messages.ContentMetadataResponseMessage;
import com.cla.filecluster.service.files.FilesDataPersistenceService;
import com.cla.filecluster.service.syscomponent.ComponentsAppService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * Created by uri on 16-Mar-17.
 */
@RestController
@RequestMapping("/api/content")
public class ContentApiController {
    private final static Logger logger = LoggerFactory.getLogger(ContentApiController.class);

    @Autowired
    private FilesDataPersistenceService filesDataPersistenceService;

    @Autowired
    private ComponentsAppService componentsAppService;

    @Value("${content.check.concurrent-checks-log-threshold:4}")
    private int activeChecksReportThreshold;

    @Value("${content.check.log-sample:10}")
    private int activeChecksReportSample;

    private AtomicLong logSampleCounter = new AtomicLong(0);

    private AtomicLong activeChecks = new AtomicLong(0);;

    @RequestMapping(value="/checkSignature", method = RequestMethod.GET)
    public ContentMetadataResponseMessage checkForExistingContent(@RequestParam String signature, @RequestParam int fileSize, @RequestParam(required=false) String mpId,  @RequestParam long rootFolderId) {
        long activeChecksVal = activeChecks.incrementAndGet();

        if (activeChecksVal > activeChecksReportThreshold) {
            long sampleCounter = logSampleCounter.getAndIncrement();
            if (sampleCounter % activeChecksReportSample == 0) {
                logger.debug("Got {} concurrent content signature checks. ({})", activeChecksVal, sampleCounter);
            }
        }
        try {
            String decodedSignature = URLDecoder.decode(signature, "UTF8");
            ContentMetadataRequestMessage mdRequest = new ContentMetadataRequestMessage(decodedSignature, fileSize, rootFolderId);
            if (mpId != null) {
                componentsAppService.incrementMediaProcessorSignatureChecksCounter(mpId);
            }
            return filesDataPersistenceService.processRequest(mdRequest);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } finally {
            activeChecks.decrementAndGet();
        }
    }

}
