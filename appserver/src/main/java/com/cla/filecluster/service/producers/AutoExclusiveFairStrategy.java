package com.cla.filecluster.service.producers;

import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.config.security.AuthenticationHelper;
import com.cla.filecluster.config.security.AutoAuthenticate;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.domain.entity.TaskType;
import com.cla.filecluster.jobmanager.repository.solr.SolrTaskService;
import com.cla.filecluster.jobmanager.service.JobManagerService;
import com.cla.filecluster.service.crawler.ingest.IngestFinalizationService;
import com.cla.filecluster.service.security.UserService;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.concurrent.Executors;

public class AutoExclusiveFairStrategy extends ExclusiveFairStrategy {

    private final static Logger logger = LoggerFactory.getLogger(AutoExclusiveFairStrategy.class);

    private IngestFinalizationService ingestFinalizationService;
    private UserService userService;
    private JobType currentExclusiveMode;
    private long highThreshold;
    private long lowThreshold;
    private int percentageThreshold;
    private boolean shouldCooldown = false;

    public AutoExclusiveFairStrategy(UserService userService, IngestFinalizationService ingestFinalizationService, JobManagerService jobManager, SolrTaskService solrTaskService, TaskProducingStrategyConfig config) {
        super(jobManager, solrTaskService, config);
        this.userService = userService;
        this.ingestFinalizationService = ingestFinalizationService;
        this.currentExclusiveMode = config.getExclusiveMode();
        this.highThreshold = config.getHighThreshold();
        this.lowThreshold = config.getLowThreshold();
        this.percentageThreshold = config.getPercentageThreshold();
    }

    public boolean shouldJobTimeout(JobType type) {
        return (type == currentExclusiveMode) || (timeSource.secondsSince(modeChangeTime) > config.getTimeoutGraceSec());
    }

    public JobType getCurrentExclusiveMode() {
        return currentExclusiveMode;
    }

    @Override
    public Collection<SimpleTask> nextTaskGroup(TaskType taskType, Table<Long, TaskState, Number> taskStatesByType, Integer tasksQueueLimit) {
        if (decideCurrentMode() || shouldCooldown) {
            shouldCooldown = shouldCooldown(currentExclusiveMode);
        }

        if (compatibleMode(currentExclusiveMode, taskType) && !shouldCooldown) {
            return super.nextTaskGroup(taskType, taskStatesByType, tasksQueueLimit);
        }
        else{
            return Sets.newHashSet();
        }
    }

    private boolean decideCurrentMode() {
        long numAnalyzed = solrTaskService.getAnalysisTaskCount(jobManager.getRunsForRunningJobsByType(JobType.ANALYZE));
        long numIngested = solrTaskService.getIngestTaskCount(jobManager.getRunsForRunningJobsByType(JobType.INGEST));

        long perAnalyzed = numAnalyzed > numIngested ? (100 - (numIngested/numAnalyzed*100)) : 0;
        long perIngested = numIngested > numAnalyzed ? (100 - (numAnalyzed/numIngested*100)) : 0;

        if (numAnalyzed > highThreshold || numIngested == 0 || (numAnalyzed > lowThreshold && perAnalyzed > percentageThreshold)) {
            if (currentExclusiveMode.equals(JobType.INGEST)) {
                logger.debug("optimize indexes before switch to analyze exclusive");
                AuthenticationHelper.doWithAuthentication(AutoAuthenticate.ADMIN, true,
                        Executors.callable(() -> ingestFinalizationService.optimizeIndexesSync()),
                        userService);
            }
            currentExclusiveMode = JobType.ANALYZE;
            logger.debug("switching exclusive mode to analyze");
            setModeChangeTime(timeSource.currentTimeMillis());
            return true;
        } else if (numIngested > highThreshold || numAnalyzed == 0 || (numIngested > lowThreshold && perIngested > percentageThreshold)) {
            currentExclusiveMode = JobType.INGEST;
            logger.debug("switching exclusive mode to ingest");
            setModeChangeTime(timeSource.currentTimeMillis());
            return true;
        }
        logger.trace("don't switch current exclusive mode is {}", currentExclusiveMode);
        return false;
    }
}
