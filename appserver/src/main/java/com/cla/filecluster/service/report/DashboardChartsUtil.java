package com.cla.filecluster.service.report;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCoreSchema;
import com.cla.common.domain.dto.kendo.FilterDescriptor;
import com.cla.common.domain.dto.report.DashboardChartDataDto;
import com.cla.common.domain.dto.report.DashboardChartSeriesDataDto;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.domain.specification.solr.SolrQueryBuilder;
import com.cla.filecluster.domain.specification.solr.SolrSpecification;
import com.cla.filecluster.domain.specification.solr.SolrSpecificationFactory;
import com.cla.filecluster.repository.solr.FacetFunction;
import com.cla.filecluster.repository.solr.FacetType;
import com.cla.filecluster.repository.solr.SolrFacetJsonResponseBucket;
import com.cla.filecluster.repository.solr.SolrFacetQueryJson;
import com.cla.filecluster.repository.solr.charts.DashboardChartValueTypeDto;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Created by: ophir
 * Created on: 9/9/2018
 */
@Service
public class DashboardChartsUtil {

    public static final String FILTER_FIELD = "filterField";
    public static final String COMPARE_TO_TOTAL = "compareToTotal";
    public static final String RETURN_OTHER = "returnOther";
    public static final String GROUPED_FIELD = "groupedField";
    public static final String SERIES_FIELD = "seriesField";
    public static final String SERIES_ITEMS_NUM = "seriesItemsNum";
    public static final String VALUE_FIELD = "valueField";
    public static final String VALUE_DIRECTION = "valueDirection";
    public static final String VALUE_FUNCTION = "valueFunction";
    public static final String QUERY_PREFIX = "queryPrefix";
    public static final String QUERY_SERIES_PREFIX = "seriesFieldPrefix";
    public static final String SEGMENT_LIST = "segmentList";
    public static final String TAKE = "take";
    public static final String PERIOD_TYPE = "periodType";
    public static final String PAGE = "page";
    public static final String MAX_DEPTH = "maxDepth";
    public static final String MIN_DEPTH = "minDepth";
    public static final String MAX_CHILD_ITEMS = "maxChildItems";
    public static final String DATE_PARTITION_ID = "datePartitionId";
    public static final String UNFILTERED_FILTER_NAME = "Unfiltered";

    public static final String TRANSLATION_PARAMETER_PREFIX = "dashboard-charts.title.";
    public static final String TRANSLATION_SIZE_PARAMETER_PREFIX = "dashboard-charts.size-title.";

    public static final String AND_FILTER_LOGIC = "and";
    public static final int DEFAULT_CHART_ITEMS_NUMBER = 10;
    public static final int DEFAULT_SERIES_ITEMS_NUMBER = 5;

    public static final String ACCURATE_COUNT_QUERY_NAME = "ACCURATE_COUNT";

    private Logger logger = LoggerFactory.getLogger(DashboardChartsUtil.class);


    @Value("${solr.facet-count-hll.lower-bound:200}")
    private int lowerBoundFacetCountHll;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    SolrSpecificationFactory solrSpecificationFactory;


    @Autowired
    private SolrQueryBuilder solrQueryBuilder;


    public FilterDescriptor extractFilterField(Map<String, String> params) {
        String fStr = params.get(FILTER_FIELD);
        if (fStr == null) {
            return null;
        }
        try {
            return ModelDtoConversionUtils.getMapper().readValue(fStr, FilterDescriptor.class);
        } catch (IOException e) {
            throw new BadRequestException(FILTER_FIELD, fStr, e, BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    public int extractMaxDepth(Map<String, String> params) {
        if (params.get(MAX_DEPTH) == null) {
            return 10;
        }
        return Integer.parseInt(params.get(MAX_DEPTH));
    }

    public int extractMinDepth(Map<String, String> params) {
        if (params.get(MIN_DEPTH) == null) {
            return 0;
        }
        return Integer.parseInt(params.get(MIN_DEPTH));
    }

    public int extractMaxChildItems(Map<String, String> params) {
        if (params.get(MAX_CHILD_ITEMS) == null) {
            return 10;
        }
        return Integer.parseInt(params.get(MAX_CHILD_ITEMS));
    }

    public Long extractDatePartitionId(Map<String, String> params) {
        if (params.get(DATE_PARTITION_ID) == null) {
            return 1l;
        }
        return Long.parseLong(params.get(DATE_PARTITION_ID));
    }

    public boolean extractCompareToTotal(Map<String, String> params) {
        String compareToTotalStr = params.get(COMPARE_TO_TOTAL);
        if (compareToTotalStr != null) {
            return Boolean.valueOf(compareToTotalStr);
        }
        return false;
    }

    public boolean extractReturnOther(Map<String, String> params) {
        String compareToTotalStr = params.get(RETURN_OTHER);
        if (compareToTotalStr != null) {
            return Boolean.valueOf(compareToTotalStr);
        }
        return false;
    }

    public CatFileFieldType extractGroupedField(Map<String, String> params) {
        String groupedFieldStr = params.get(GROUPED_FIELD);
        CatFileFieldType fieldType = CatFileFieldType.valueOf(groupedFieldStr);
        String translateStr = translateSolrFieldName(fieldType, extractPrefixField(params));
        if (translateStr == null) {
            throw new RuntimeException(this.messageHandler.getMessage("dashboard-charts.illegal-groupedField-value"));
        }
        return fieldType;
    }


    public CatFileFieldType extractSeriesField(Map<String, String> params) {
        String groupedFieldStr = params.get(SERIES_FIELD);
        if (Strings.isNullOrEmpty(groupedFieldStr)) {
            return null;
        }
        CatFileFieldType fieldType = CatFileFieldType.valueOf(groupedFieldStr);
        String translateStr = translateSolrFieldName(fieldType, extractSeriesPrefixField(params));
        if (translateStr == null) {
            throw new RuntimeException(this.messageHandler.getMessage("dashboard-charts.illegal-groupedField-value"));
        }
        return fieldType;
    }

    public DashboardChartValueTypeDto extractChartValueField(Map<String, String> params) {

        String fieldStr = params.get(VALUE_FIELD);
        String directionStr = params.get(VALUE_DIRECTION);
        String functionStr = params.get(VALUE_FUNCTION);

        DashboardChartValueTypeDto ans = new DashboardChartValueTypeDto();
        if (functionStr == null) {
            ans.setSolrFunction(FacetFunction.COUNT);
            return ans;
        } else {
            ans.setSolrFunction(FacetFunction.valueOf(functionStr));
        }

        if (fieldStr != null) {
            //ans.setSolrField(FileDtoFieldConverter.concvertDtoFieldToCatFileType(fieldStr));
            ans.setSolrField(CatFileFieldType.valueOf(fieldStr));
        }
        if (directionStr != null) {
            ans.setSortDirection(Sort.Direction.valueOf(directionStr));
        }
        return ans;
    }


    public String extractPrefixField(Map<String, String> params) {
        return params.get(QUERY_PREFIX);
    }

    public String extractSeriesPrefixField(Map<String, String> params) {
        return params.get(QUERY_SERIES_PREFIX);
    }

    public int extractTakeNumber(Map<String, String> params) {
        if (params.get(TAKE) == null) {
            return DEFAULT_CHART_ITEMS_NUMBER;
        }
        return Integer.parseInt(params.get(TAKE));
    }

    public int extractSeriesItemNumber(Map<String, String> params) {
        if (params.get(SERIES_ITEMS_NUM) == null) {
            return DEFAULT_SERIES_ITEMS_NUMBER;
        }
        return Integer.parseInt(params.get(SERIES_ITEMS_NUM));
    }

    public int extractPageNumber(Map<String, String> params) {
        if (params.get(PAGE) == null) {
            return 1;
        }
        return Integer.parseInt(params.get(PAGE));
    }

    public String extractPeriodType(Map<String, String> params) {
        return params.get(PERIOD_TYPE);
    }

    public FilterDescriptor[] extractSegmentList(Map<String, String> params) {
        String sListStr = params.get(SEGMENT_LIST);
        if (Strings.isNullOrEmpty(sListStr)) {
            return new FilterDescriptor[0];
        }
        try {
            return ModelDtoConversionUtils.getMapper().readValue(sListStr, FilterDescriptor[].class);
        } catch (IOException e) {
            throw new BadRequestException(SEGMENT_LIST, sListStr, e, BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    public String translateSolrFieldName(SolrCoreSchema field, String prefix) {
        return this.messageHandler.getMessage(TRANSLATION_PARAMETER_PREFIX + field.getSolrName() + (prefix != null ? "-" + prefix : ""));
    }

    public String translateSolrSizeFieldName(SolrCoreSchema field, String prefix) {
        return this.messageHandler.getMessage(TRANSLATION_SIZE_PARAMETER_PREFIX + field.getSolrName() + (prefix != null ? "-" + prefix : ""));
    }

    public String getDashboardChartEnricherUniqueName(CatFileFieldType field, String prefix) {
        if (prefix != null) {
            return field.getSolrName() + "_" + prefix;
        }
        return field.getSolrName();
    }


    public List<String> getAndQueriesArray(FilterDescriptor filter, SolrSpecification originalSolrSpecification) {
        ArrayList<String> ans = new ArrayList<>();
        if (!filter.getFilters().isEmpty() && filter.getLogic().toLowerCase().equals(AND_FILTER_LOGIC)) {
            filter.getFilters().forEach((FilterDescriptor innerFilter) -> {
                ans.addAll(getAndQueriesArray(innerFilter, originalSolrSpecification));
            });
        } else {
            /*  I create a workaround because the solrQueryBuilder.generateFilterQuery sometimes instead of returning the
                filter as string it change the additionalFilter of solrSpecification, for future releases need to fix
                this issue at  solrQueryBuilder.generateFilterQuery.
            */
            SolrSpecification solrSpecification = solrSpecificationFactory.buildSolrFacetJsonSpecification(new DataSourceRequest());
            solrSpecification.setAdditionalFilter(new ArrayList<>());
            String filterQuery = solrQueryBuilder.generateFilterQuery(filter, false, solrSpecification);
            if (!Strings.isNullOrEmpty(filterQuery)) {
                ans.add(filterQuery);
            }
            ans.addAll(solrSpecification.getAdditionalFilter());
            originalSolrSpecification.addContentFilters(solrSpecification.getContentFilters());
        }
        return ans;
    }

    public static String fixSolrSlashes(String sourceStr) {
        return sourceStr.replace("\\\\", "\\\\\\\\");
    }

    public static String fixSingleSolrSlashes(String sourceStr) {
        return sourceStr.replace("\\", "\\\\");
    }

    public static String addDoubleQuoteIfNeeded(String sourceStr) {
        if (sourceStr.indexOf(" ") > -1) {
            return "\"" + sourceStr + "\"";
        }
        return sourceStr;
    }

    public static Double getResponseDoubleValueFromObject(Object objValue) {
        if (objValue instanceof Date) {
            return Double.valueOf(String.valueOf(((Date) objValue).getTime()));
        } else {
            return Double.valueOf(String.valueOf(objValue));
        }
    }

    public static String fixDoubleQuoteSlashes(String sourceStr) {
        return sourceStr.replace("\"", "\\\"");
    }

    public static void joinSameCategoryIds(DashboardChartDataDto dashboardChartDataDto, List<String> actualIds) {
        HashMap<String, Integer> seriesLocationIds = new HashMap<>();
        int locationIndex = 0;
        for (int oldIndex = 0; oldIndex < actualIds.size(); oldIndex++) {
            String categoryName = actualIds.get(oldIndex);
            Boolean isIndexCreatedNow = false;
            if (!seriesLocationIds.containsKey(categoryName)) {
                seriesLocationIds.put(categoryName, locationIndex);
                locationIndex++;
                isIndexCreatedNow = true;
            }
            int newIndex = seriesLocationIds.get(categoryName);
            for (DashboardChartSeriesDataDto seriesDto : dashboardChartDataDto.getSeries()) {
                List<Double> dataArray = seriesDto.getData();
                if (isIndexCreatedNow) {
                    dataArray.set(newIndex, dataArray.get(oldIndex));
                } else {
                    dataArray.set(newIndex, dataArray.get(oldIndex) + dataArray.get(newIndex));
                }
            }
        }

        for (DashboardChartSeriesDataDto seriesDto : dashboardChartDataDto.getSeries()) {
            List<Double> dataArray = seriesDto.getData();
            seriesDto.setData(dataArray.subList(0, seriesLocationIds.size()));
        }
    }


    private boolean areAlSeriesAtIndexEmpty(List<DashboardChartSeriesDataDto> serieses, int index) {
        boolean foundData = false;
        for (DashboardChartSeriesDataDto seriesDto : serieses) {
            if (seriesDto.getData().get(index) != null && seriesDto.getData().get(index) > 0) {
                foundData = true;
            }
        }
        return !foundData;
    }

    private void removeCategoryOfDashboardChartDataDtoByIndex(DashboardChartDataDto dashboardChartDataDto, int index) {
        dashboardChartDataDto.getCategories().remove(index);
        for (DashboardChartSeriesDataDto seriesDto : dashboardChartDataDto.getSeries()) {
            seriesDto.getData().remove(index);
        }
    }

    public void removeEmptyCategories(DashboardChartDataDto dashboardChartDataDto) {
        for (int catIndex = dashboardChartDataDto.getCategories().size() - 1; catIndex >= 0; catIndex--) {

            if (areAlSeriesAtIndexEmpty(dashboardChartDataDto.getSeries(), catIndex)) {
                removeCategoryOfDashboardChartDataDtoByIndex(dashboardChartDataDto, catIndex);
            }
        }
    }


    public String getNameOfFilter(FilterDescriptor filterDescriptor) {
        if (filterDescriptor != null) {
            if (filterDescriptor.getName() != null) {
                return filterDescriptor.getName();
            }
            if (filterDescriptor.getFilters() != null) {
                for (FilterDescriptor innerFilter : filterDescriptor.getFilters()) {
                    String innerName = getNameOfFilter(innerFilter);
                    if (innerName != null) {
                        return innerName;
                    }
                }
            }
        }
        return null;
    }

    public static String getQueryTypeValueStringAccordingToValuesList(List<String> queryValues) {
        Stream<String> myStream = queryValues.stream();
        List<String> newFieldsList = myStream.map(s -> "\"" + s + "\"").collect(Collectors.toList());
        return "(" + String.join(" ", newFieldsList) + ")";
    }

    public void addInnerFacetsAccordingToValue(DashboardChartValueTypeDto valueField, boolean getAccurateNumOfBuckets, SolrFacetQueryJson theFacet) {
        theFacet.setSortField(valueField.getSolrField().getSolrName());
        theFacet.setSortDirection(valueField.getSortDirection());
        SolrFacetQueryJson innerFacet = new SolrFacetQueryJson(valueField.getSolrField().getSolrName()); // this is the name of the solr function we use this name for sorting ,
        innerFacet.setType(FacetType.SIMPLE);
        innerFacet.setField(valueField.getSolrField());
        innerFacet.setFunc(valueField.getSolrFunction());
        theFacet.addFacet(innerFacet);
        if (getAccurateNumOfBuckets && (valueField.getSolrFunction().equals(FacetFunction.HLL) || valueField.getSolrFunction().equals(FacetFunction.UNIQUE))) {
            SolrFacetQueryJson countInnerFacet = new SolrFacetQueryJson(DashboardChartsUtil.ACCURATE_COUNT_QUERY_NAME); // this is the name of the solr function we use this name for sorting ,
            countInnerFacet.setType(FacetType.TERMS);
            countInnerFacet.setField(valueField.getSolrField());
            countInnerFacet.setLimit(getLowerBoundFacetCountHll());
            theFacet.addFacet(countInnerFacet);
        }
    }


    public int getLowerBoundFacetCountHll() {
        return lowerBoundFacetCountHll + 1;
    }

    public Double parseInnerBucketValue(DashboardChartDataDto dashboardChartDataDto, DashboardChartValueTypeDto valueField, boolean getAccurateNumOfBuckets, SolrFacetJsonResponseBucket innerBucket) {
        Double countValue = 0d;
        if (valueField.getSolrFunction().equals(FacetFunction.COUNT)) {
            countValue = innerBucket.getDoubleCountNumber();
        } else {
            Object statFacetValue = innerBucket.getObjectValueByKey(valueField.getSolrField().getSolrName());
            if (statFacetValue != null) {
                countValue = DashboardChartsUtil.getResponseDoubleValueFromObject(statFacetValue);
            }
            SolrFacetJsonResponseBucket accurateBucket = innerBucket.getBucketBasedFacets(DashboardChartsUtil.ACCURATE_COUNT_QUERY_NAME);
            if (getAccurateNumOfBuckets) {
                countValue = calculateAccurateCountValue(dashboardChartDataDto, countValue, accurateBucket, valueField);
            }
        }
        return countValue;
    }

    public Double calculateAccurateCountValue(DashboardChartDataDto dashboardChartDataDto, Double countValue, SolrFacetJsonResponseBucket accurateBucket, DashboardChartValueTypeDto valueField) {
        if (accurateBucket != null && countValue > 0) {
            Integer newCountValue = accurateBucket.getBuckets().size();
            if (newCountValue > 0 && newCountValue < getLowerBoundFacetCountHll()) {
                countValue = DashboardChartsUtil.getResponseDoubleValueFromObject(newCountValue);
            } else {
                if (countValue < getLowerBoundFacetCountHll()) {
                    logger.warn("hll total for field {} is {} while exact is {}", valueField.getSolrField(), countValue, newCountValue);
                    countValue = DashboardChartsUtil.getResponseDoubleValueFromObject(getLowerBoundFacetCountHll());
                }
                dashboardChartDataDto.setDataMayNotBeAccurate(true);
            }
        }
        return countValue;
    }


    public List<Object> getValuesByFieldFromFilter(FilterDescriptor filterDescriptor, String field) {
        List<Object> ans = new ArrayList<>();
        if (filterDescriptor != null &&  !Strings.isNullOrEmpty(filterDescriptor.getField()) && filterDescriptor.getField().equals(field)) {
            ans.add(filterDescriptor.getValue());
        }
        if (filterDescriptor.getFilters() != null) {
            for (FilterDescriptor innerFilter : filterDescriptor.getFilters()) {
                ans.addAll(getValuesByFieldFromFilter(innerFilter, field));
            }
        }
        return ans;
    }

    public static  FilterDescriptor getUnfilterFilterDescriptor() {
        FilterDescriptor ans = new FilterDescriptor();
        ans.setName(UNFILTERED_FILTER_NAME);
        return ans;
    }

}
