package com.cla.filecluster.service.export.exporters.file.acl;

import com.cla.common.domain.dto.AggregationCountItemDTO;
import com.cla.common.domain.dto.filetree.RootFolderSharePermissionDto;
import com.cla.connector.domain.dto.acl.AclType;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.dto.facet.FacetPage;
import com.cla.filecluster.service.export.ExportType;
import com.cla.filecluster.service.export.exporters.PageCsvExcelExporter;
import com.cla.filecluster.service.export.exporters.PagingMethod;
import com.cla.filecluster.service.security.SharePermissionAppService;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.cla.filecluster.service.export.exporters.file.FileCountHeaderFields.*;

@Slf4j
@Component
public class SharePermissionAllowReadExporter extends PageCsvExcelExporter<AggregationCountItemDTO<RootFolderSharePermissionDto>> {

    private final static String[] HEADER = {USER, NUM_OF_FILES};

    @Autowired
    private SharePermissionAppService sharePermissionAppService;

    @Override
    protected String[] getHeader() {
        return HEADER;
    }

    @Override
    protected void configureMapper(CsvMapper mapper) {
        mapper.addMixIn(AggregationCountItemDTO.class, SharedPermAggCountDtoMixin.class);
        mapper.addMixIn(RootFolderSharePermissionDto.class, RootFolderSharePermissionDtoMixin.class);
    }

    @Override
    protected Page<AggregationCountItemDTO<RootFolderSharePermissionDto>> getData(Map<String, String> requestParams) {
        DataSourceRequest dataSourceRequest = DataSourceRequest.build(requestParams);
        FacetPage<AggregationCountItemDTO<RootFolderSharePermissionDto>> res =
                sharePermissionAppService.listCountWithFileFilter(dataSourceRequest, AclType.READ_TYPE);
        return new PageImpl<>(res.getContent());
    }

    @Override
    protected Map<String, Object> addExtraFields(AggregationCountItemDTO<RootFolderSharePermissionDto> base, Map<String, String> requestParams) {
        return null;
    }

    @Override
    public PagingMethod getPagingMethod() {
        return PagingMethod.SINGLE;
    }

    @Override
    public boolean doesAccept(ExportType exportType, Map<String, String> params) {
        return exportType.equals(ExportType.SHARED_PERMISSION_ALLOW_READ);
    }

}
