package com.cla.filecluster.service.files.managers;

import com.cla.common.domain.dto.filetree.RootFolderDto;
import com.cla.connector.domain.dto.media.MediaConnectionDetailsDto;
import com.cla.common.domain.dto.messages.FileContentType;
import com.cla.filecluster.domain.entity.filetree.RootFolder;
import com.cla.filecluster.domain.entity.pv.ClaFile;
import com.cla.filecluster.mediaproc.RemoteMediaProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 *
 *
 * Created by liron on 4/9/2017.
 */
@Service
public class RemoteFileContentManager {

    private final static Logger logger = LoggerFactory.getLogger(RemoteFileContentManager.class);

    @Autowired
    private RemoteMediaProcessingService mediaProcessorFileScanService;

    public void doFileContentOperationAsync(String requestId, FileContentType taskType, RootFolder rf,
                                            ClaFile claFile, MediaConnectionDetailsDto mediaConnectionDetailsDto, Set<String> textTokens) {
        mediaProcessorFileScanService.issueFileContentRequest(requestId, taskType, rf, claFile, mediaConnectionDetailsDto,textTokens);
    }

    public void checkIsDirectoryExistsAsync(RootFolderDto rootFolderDto,
                                            MediaConnectionDetailsDto mediaConnectionDetailsDto,
                                            boolean forceAccessibilityCheck) {
        FileContentType requestType = FileContentType.IS_DIRECTORY_EXISTS;
        mediaProcessorFileScanService.issueFileContentRequest(null,requestType,
                rootFolderDto, mediaConnectionDetailsDto,null, forceAccessibilityCheck);
    }

    public void doFileIngestOperationAsync(String requestId, RootFolder rf, ClaFile claFile, boolean getReverseDictionary) {
        mediaProcessorFileScanService.issueFileIngestRequest(requestId, rf, claFile, getReverseDictionary);
    }

    public void doGetMediaProcessorLogsAsync(String requestId, Long instanceId) {
        mediaProcessorFileScanService.issueMediaProcessorLogsRequest(requestId, instanceId);
    }
}
