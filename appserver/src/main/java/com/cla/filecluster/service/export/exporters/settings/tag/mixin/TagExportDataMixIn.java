package com.cla.filecluster.service.export.exporters.settings.tag.mixin;

import com.fasterxml.jackson.annotation.JsonProperty;

import static com.cla.filecluster.service.export.exporters.settings.tag.TagHeaderFields.*;

/**
 * Created by: yael
 * Created on: 3/12/2019
 */
public class TagExportDataMixIn {
    @JsonProperty(TYPE)
    private String type;

    @JsonProperty(NAME)
    private String name;

    @JsonProperty(PROPERTIES)
    private Boolean properties;

    @JsonProperty(TYPE_DESCRIPTION)
    private String typeDescription;

    @JsonProperty(DESCRIPTION)
    private String description;

    @JsonProperty(SINGLE_VAL)
    private Boolean singleValue;
}
