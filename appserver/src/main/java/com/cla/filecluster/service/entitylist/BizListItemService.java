package com.cla.filecluster.service.entitylist;

import com.cla.common.domain.dto.bizlist.*;
import com.cla.filecluster.domain.dto.DataSourceRequest;
import com.cla.filecluster.domain.entity.bizlist.*;
import com.cla.filecluster.domain.entity.filetree.DocFolder;
import com.cla.filecluster.domain.specification.ClientBizListItemSpecification;
import com.cla.filecluster.domain.specification.SimpleBizListItemSpecification;
import com.cla.filecluster.repository.jpa.entitylist.BizListItemAliasRepository;
import com.cla.filecluster.repository.jpa.entitylist.ClientBizListItemRepository;
import com.cla.filecluster.repository.jpa.entitylist.ConsumerBizListItemRepository;
import com.cla.filecluster.repository.jpa.entitylist.SimpleBizListItemRepository;
import com.cla.filecluster.service.api.ModelDtoConversionUtils;
import com.cla.filecluster.service.crawler.pv.ExtractTokenizer;
import com.cla.filecluster.util.query.DBTemplateUtils;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by uri on 31/12/2015.
 */
@Service
public class BizListItemService {
    private final static Logger logger = LoggerFactory.getLogger(BizListItemService.class);

    @Autowired
    private SimpleBizListItemRepository simpleBizListItemRepository;

    @Autowired
    private ClientBizListItemRepository clientBizListItemRepository;

    @Autowired
    private ConsumerBizListItemRepository consumerBizListItemRepository;

    @Autowired
    private BizListItemAliasRepository bizListItemAliasRepository;

    @Autowired
    private BizListService bizListService;

    @Autowired
    private DBTemplateUtils dbTemplateUtils;

    @Autowired
    private ExtractTokenizer extractTokenizer;

    @Value("${bizlist.client.importCsv.clearedSuffix:_CLEARED}")
    private String clientCsvClearedSuffix;

    @Value("${bizlist.client.addNameAsAlias:false}")
    private boolean addNameAsAlias;

    @Value("${bizlist.client.addIdAsAlias:false}")
    private boolean addIdAsAlias;

    @Value("${datapopulator.predefined.bizlist.firstName:FIRST_NAME}")
    private String firstNameField;

    @Value("${datapopulator.predefined.bizlist.lastName:LAST_NAME}")
    private String lastNameField;

    @Value("${bizlist.consumer.encryptAllFields:true}")
    private boolean encryptAllConsumerFields;

    private int debugCounter = 0;

    private LoadingCache<String, Optional<SimpleBizListItemDto>> bizListItemCache = null;

    @PostConstruct
    void init() {
        bizListItemCache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(50, TimeUnit.MINUTES)
                .build(
                        new CacheLoader<String, Optional<SimpleBizListItemDto>>() {
                            public Optional<SimpleBizListItemDto> load(String key) {
                                return dbTemplateUtils.doInTransaction(() -> {
                                    SimpleBizListItem item = getSimpleBizListItemFull(key);
                                    return Optional.ofNullable(convertSimpleBizListItem(item));
                                });
                            }
                        });

        dbTemplateUtils.doInTransaction(() -> {
            simpleBizListItemRepository.findAllWithAliases().forEach(bizListItem -> {
                SimpleBizListItemDto itemDto = convertSimpleBizListItem(bizListItem);
                bizListItemCache.put(bizListItem.getSid(), Optional.ofNullable(itemDto));
            });
        });
    }

    public SimpleBizListItemDto getFromCache(String bizListItemSid) {
        SimpleBizListItemDto dto = bizListItemCache.getUnchecked(bizListItemSid).orElse(null);
        return dto == null ? null : new SimpleBizListItemDto(dto);
    }

    public SimpleBizListItem createSimpleBizListItem(BizList bizList, SimpleBizListItemDto entityDto, boolean printDebugLine) {
        SimpleBizListItem entity = new SimpleBizListItem();
        fillBizListItemFromDto(bizList, entityDto, entity);
        SimpleBizListItem simpleBizListItem = simpleBizListItemRepository.save(entity);
        simpleBizListItem = simpleBizListItemRepository.findWithAliases(simpleBizListItem.getSid());
        simpleBizListItem = createAliases(simpleBizListItem, entityDto.getEntityAliases(), printDebugLine);
        bizListService.updateBizListUpdateDate(bizList);
        return simpleBizListItem;
    }

    private SimpleBizListItem createAliases(SimpleBizListItem simpleBizListItem, List<BizListItemAliasDto> bizListItemAliasDtos, boolean printDebugLine) {
        Set<String> encryptedAliases = extractEncryptedAliases(simpleBizListItem);
        //Create an alias for the bizList name and business id

        ArrayList<BizListItemAlias> newAliases = new ArrayList<>();
        String itemName = null;
        if (addNameAsAlias) {
            itemName = simpleBizListItem.getName();
            BizListItemAlias nameAlias = createAlias(simpleBizListItem, itemName, encryptedAliases, printDebugLine);
            if (nameAlias != null) {
                newAliases.add(nameAlias);
            }
        }
        if (addIdAsAlias) {
            if (itemName == null || !itemName.equals(simpleBizListItem.getEntityId())) {
                BizListItemAlias idAlias = createAlias(simpleBizListItem, simpleBizListItem.getEntityId(), encryptedAliases, printDebugLine);
                if (idAlias != null) {
                    newAliases.add(idAlias);
                }
            }
        }
        if (bizListItemAliasDtos != null) {
            for (BizListItemAliasDto bizListItemAliasDto : bizListItemAliasDtos) {
                String aliasString = bizListItemAliasDto.getName();
                if (StringUtils.isNotEmpty(aliasString)) {
                    BizListItemAlias alias = createAlias(simpleBizListItem, aliasString, encryptedAliases, printDebugLine);
                    if (alias != null) {
                        newAliases.add(alias);
                    }
                }
            }
        }
        if (newAliases.size() > 0) {
            simpleBizListItem.getBizListItemAliases().addAll(newAliases);
//            logger.debug("New aliases were created - refetch item {}",simpleBizListItem.getSid());
//            return simpleBizListItemRepository.findWithAliases(simpleBizListItem.getSid());
        }
        //No changes
        return simpleBizListItem;
    }

    private BizListItemAlias createAlias(SimpleBizListItem simpleBizListItem,
                                         String aliasString,
                                         Set<String> existingEncryptedAliases,
                                         boolean printDebugLine) {
        if (StringUtils.isEmpty(aliasString)) {
            return null;
        }
        String hash = extractTokenizer.getHash(aliasString);
        if (existingEncryptedAliases != null && existingEncryptedAliases.contains(hash)) {
            //Alias already exists
            return null;
        }
        BizListItemAlias alias = new BizListItemAlias();
        alias.setEntity(simpleBizListItem);
        alias.setAlias(aliasString);
        alias.setEncryptedAlias(hash);
        if (printDebugLine) {
            logger.debug("Create alias {} with hash {} for entity sid {}", aliasString, hash, simpleBizListItem.getSid());
        }
        return bizListItemAliasRepository.save(alias);
    }

    private void fillBizListItemFromDto(BizList bizList, SimpleBizListItemDto entityDto, SimpleBizListItem entity) {
        entity.setName(entityDto.getName());
        entity.setBizList(bizList);
        entity.setEntityId(entityDto.getBusinessId());
        entity.setState(entityDto.getState() != null ? entityDto.getState() : BizListItemState.ACTIVE);
        List<BizListItemAlias> bizListItemAliases = new ArrayList<>();
        entity.setBizListItemAliases(bizListItemAliases);
        if (entityDto.getImportId() != null) {
            entity.setImportId(entityDto.getImportId());
        }
        entity.generateSid();
    }

    @Transactional
    public SimpleBizListItem createClientBizListItem(BizList bizList, SimpleBizListItemDto entityDto) {
        ClientBizListItem entity = new ClientBizListItem();
        fillBizListItemFromDto(bizList, entityDto, entity);
        ClientBizListItem clientBizListItem = clientBizListItemRepository.save(entity);
        clientBizListItem = clientBizListItemRepository.findWithAliases(clientBizListItem.getSid());
        SimpleBizListItem simpleBizListItem = createAliases(clientBizListItem, entityDto.getEntityAliases(), true);
        logger.debug("Created Client BizListItem by id {} with {} aliases", clientBizListItem, simpleBizListItem.getBizListItemAliases().size());
        return simpleBizListItem;
    }

    @Transactional(readOnly = true)
    public Page<ClientBizListItem> listClientBizListItems(long bizListId, PageRequest pageRequest, DataSourceRequest dataSourceRequest) {
        if (dataSourceRequest.getFilter() == null) {
            logger.debug("list Client BizListItems for bizList {} using default filtering", bizListId);
            return clientBizListItemRepository.findAllByBizListFiltered(bizListId, pageRequest);
        }
        logger.debug("list Client BizListItems for bizList {} using filter: {}", bizListId, dataSourceRequest.getFilter());
        Specification<ClientBizListItem> spec = new ClientBizListItemSpecification(bizListId, dataSourceRequest.getFilter());
        return clientBizListItemRepository.findAll(spec, pageRequest);
    }

    @Transactional(readOnly = true)
    public Page<SimpleBizListItem> listSimpleBizListItems(long bizListId, PageRequest pageRequest, DataSourceRequest dataSourceRequest) {
        if (dataSourceRequest.getFilter() == null) {
            return simpleBizListItemRepository.findAllByBizListFiltered(bizListId, pageRequest);
        }
        Specification<SimpleBizListItem> spec = new SimpleBizListItemSpecification(bizListId, dataSourceRequest.getFilter());
        return getSimpleBizListItems(pageRequest, spec);
    }

    @Transactional(readOnly = true)
    public Page<SimpleBizListItem> getSimpleBizListItems(Pageable pageRequest, Specification<SimpleBizListItem> spec) {
        return simpleBizListItemRepository.findAll(spec, pageRequest);
    }

    @Transactional(readOnly = true)
    public List<String> getActiveSimpleBizListItemIds(long listId) {
        return simpleBizListItemRepository.getActiveSimpleBizListItemIds(listId);
    }

    @Transactional
    public SimpleBizListItem updateSimpleBizListItem(String sid, SimpleBizListItemDto bizListItemDto) {
        SimpleBizListItem bizListItem = simpleBizListItemRepository.findWithAliases(sid);
        bizListItem.setState(bizListItemDto.getState());
        bizListItem.setEntityId(bizListItemDto.getBusinessId());
        bizListItem.setName(bizListItemDto.getName());
        if (bizListItemDto.getEntityAliases() != null) {
            updateBizListAliases(bizListItem, bizListItemDto.getEntityAliases());
        }
        SimpleBizListItem save = simpleBizListItemRepository.save(bizListItem);
        bizListItemCache.invalidate(save.getSid());
        bizListService.updateBizListUpdateDate(bizListItem.getBizList());
        return simpleBizListItemRepository.findWithAliases(save.getSid());
    }

    private void updateBizListAliases(SimpleBizListItem bizListItem, List<BizListItemAliasDto> entityAliases) {
        logger.debug("update bizList Aliases for bizListItem {}", bizListItem.getSid());
        Set<String> encryptedAliases = extractEncryptedAliases(bizListItem);
        //Remove aliases that are not in the list
        Set<Long> requiredIds = entityAliases.stream().filter(e -> e.getId() != null).map(BizListItemAliasDto::getId).collect(Collectors.toSet());
        for (BizListItemAlias bizListItemAlias : bizListItem.getBizListItemAliases()) {
            if (!requiredIds.contains(bizListItemAlias.getId())) {
                logger.debug("Remove alias {}", bizListItemAlias);
                bizListItemAliasRepository.delete(bizListItemAlias);
            }
        }
        //Add new aliases
        entityAliases.stream().filter(e -> e.getId() == null).forEach(e ->
                createAlias(bizListItem, e.getName(), encryptedAliases, true));
        //Update aliases
        entityAliases.stream().filter(e -> e.getId() != null).forEach(e ->
                updateBizListAlias(bizListItem, e));
    }

    @Transactional(readOnly = true)
    public List<Long> getAliasByBizListId(long bizListId) {
        return bizListItemAliasRepository.getByBizListId(bizListId);
    }

    @Transactional
    public void deleteAliasById(List<Long> ids) {
        bizListItemAliasRepository.deleteByIds(ids);
    }

    private void updateBizListAlias(SimpleBizListItem bizListItem, BizListItemAliasDto updatedAliasDto) {
        Optional<BizListItemAlias> alias = bizListItem.getBizListItemAliases().stream()
                .filter(f -> f.getId().equals(updatedAliasDto.getId()))
                .findFirst();

        if (alias.isPresent()) {
            BizListItemAlias bizListItemAlias = alias.get();
            logger.debug("Update Alias {}", bizListItemAlias);
            bizListItemAlias.setAlias(updatedAliasDto.getName());
            String hash = extractTokenizer.getHash(updatedAliasDto.getName());
            bizListItemAlias.setEncryptedAlias(hash);
            bizListItemAliasRepository.save(bizListItemAlias);
        }
    }

    @Transactional
    public void deleteSimpleBizListItem(String itemId) {
        simpleBizListItemRepository.deleteById(itemId);
        bizListItemCache.invalidate(itemId);
    }

    @Transactional
    public void deleteClientsBizListItem(String itemId) {
        clientBizListItemRepository.deleteById(itemId);
        bizListItemCache.invalidate(itemId);
    }

    @Transactional
    public void createBizListItemFromRow(BizList bizList, BizListItemRow bizListItemRow, ArrayList<String> headers, long importId, BizListSchema bizListSchema) {
        SimpleBizListItemDto entityDto = new SimpleBizListItemDto();
        String businessId = bizListItemRow.getClearId();
        if (isBizListItemExists(businessId, bizList.getId())) {
            bizListSchema.getDuplicateBizListItemRows().add(bizListItemRow);
            return;
        }
        entityDto.setBusinessId(businessId);
        entityDto.setName(bizListItemRow.getClearName());
        entityDto.setImportId(importId);
        entityDto.setState(BizListItemState.PENDING_IMPORT);
        SimpleBizListItem result;
        boolean printDebugLine = debugCounter++ % 30 == 0;
        switch (bizList.getBizListItemType()) {
            case CLIENTS:
                result = createClientBizListItem(bizList, entityDto);
                break;
            case CONSUMERS:
                result = createConsumerBizListItem(bizList, entityDto, bizListItemRow.getFieldValues(), headers);
                break;
            default:
                result = createSimpleBizListItem(bizList, entityDto, printDebugLine);
                break;
        }
        createAliases(bizListItemRow, result, printDebugLine);

    }

    private SimpleBizListItem createConsumerBizListItem(BizList bizList,
                                                        SimpleBizListItemDto entityDto,
                                                        String[] fieldValues,
                                                        ArrayList<String> headers) {
        ConsumerBizListItem entity = new ConsumerBizListItem();
        fillBizListItemFromDto(bizList, entityDto, entity);
        Map<String, String> headerValueMap = createConsumerHeaderValueMap(headers, fieldValues);
        String json = ModelDtoConversionUtils.mapToJson(headerValueMap);
        entity.setEncryptedFieldsJson(json);
        if (headerValueMap.containsKey(firstNameField)) {
            entity.setFirstName(headerValueMap.get(firstNameField));
        }
        if (headerValueMap.containsKey(lastNameField)) {
            entity.setLastName(headerValueMap.get(lastNameField));
        }
        ConsumerBizListItem consumerBizListItem = consumerBizListItemRepository.save(entity);
        logger.trace("Created Consumer BizListItem {} ", consumerBizListItem);
        return consumerBizListItem;
    }

    private Map<String, String> createConsumerHeaderValueMap(ArrayList<String> headers, String[] fieldValues) {
        Map<String, String> headerValueMap = new HashMap<>();
        for (int i = 0; i < headers.size(); i++) {
            String key = headers.get(i);
            String fieldValue = fieldValues[i];
            if (encryptAllConsumerFields || key.endsWith(clientCsvClearedSuffix)) {
                //Cleared header - encrypt first
                String hash = extractTokenizer.getHash(fieldValue);
                String realHeader = key.endsWith(clientCsvClearedSuffix) ?
                        key.substring(0, key.length() - clientCsvClearedSuffix.length()) : key;
                headerValueMap.put(realHeader, hash);
            } else {
                headerValueMap.put(key, fieldValue);
            }
        }
        return headerValueMap;
    }

    private void createAliases(BizListItemRow bizListItemRow, SimpleBizListItem result, boolean printDebugLine) {
        Set<String> encryptedAliases = extractEncryptedAliases(result);
        if (bizListItemRow.getAliasValues() != null) {
            for (String alias : bizListItemRow.getAliasValues()) {
                if (StringUtils.isEmpty(alias)) {
                    continue;
                }
                createAlias(result, alias, encryptedAliases, printDebugLine);
            }
        }
    }

    private Set<String> extractEncryptedAliases(SimpleBizListItem result) {
        return result.getBizListItemAliases().stream().map(BizListItemAlias::getEncryptedAlias).collect(Collectors.toSet());
    }

    public void createBizListItemFromDocFolder(BizList bizList, DocFolder docFolder, long importId, BizListSchema bizListSchema) {
        try {
            String docFolderName = docFolder.getName();
            if (isBizListItemExists(docFolderName, bizList.getId())) {
                bizListSchema.getDuplicateBizListItemRows().add(new BizListItemRow(docFolderName, docFolderName));
                return;
            }
            SimpleBizListItemDto entityDto = new SimpleBizListItemDto();
            entityDto.setBusinessId(docFolderName);
            entityDto.setName(docFolderName);
            entityDto.setImportId(importId);
            entityDto.setState(BizListItemState.PENDING_IMPORT);
            SimpleBizListItem result;
            switch (bizList.getBizListItemType()) {
                case CLIENTS:
                    result = createClientBizListItem(bizList, entityDto);
                    break;
                default:
                    result = createSimpleBizListItem(bizList, entityDto, true);
                    break;
            }
            Set<String> encryptedAliases = extractEncryptedAliases(result);
            bizListSchema.getBizListItemRows().add(new BizListItemRow(docFolderName, docFolderName));
        } catch (Exception e) {
            logger.error("Failed to import BizListItem from docFolder " + docFolder, e);
            bizListSchema.getErrorBizListItemRows().add(new BizListItemRow(docFolder.getName(), docFolder.getName()));
        }
//        createAlias(result, docFolderName, encryptedAliases);
    }

    private boolean isBizListItemExists(String businessId, long bizListId) {
        String sid = SimpleBizListItem.generateSid(bizListId, businessId);
        return simpleBizListItemRepository.findById(sid)
                .map(one -> !BizListItemState.PENDING_IMPORT.equals(one.getState())
                        && !BizListItemState.DELETED.equals(one.getState()))
                .orElse(false);
    }

    public void updateBizListItemsState(List<SimpleBizListItem> content, BizListItemState newState) {
        for (SimpleBizListItem simpleBizListItem : content) {
            simpleBizListItem.setState(newState);
            bizListItemCache.invalidate(simpleBizListItem.getSid());
        }
        simpleBizListItemRepository.saveAll(content);
        List<SimpleBizListItem> distinctBizList = content.stream().distinct().collect(Collectors.toList());
        distinctBizList.forEach(b -> bizListService.updateBizListUpdateDate(b.getBizList()));
        logger.debug("updated bizList. Set {} items state to state={}", content.size(), newState);
    }

    @Transactional(readOnly = true)
    public Map<String, SimpleBizListItem> getSimpleBizListItemsAsMap(Collection<String> sids) {
        Map<String, SimpleBizListItem> result = new HashMap<>();
        if (sids == null || sids.isEmpty()) {
            return result;
        }
        List<SimpleBizListItem> simpleBizListItems = simpleBizListItemRepository.findBySIds(sids);
        for (SimpleBizListItem simpleBizListItem : simpleBizListItems) {
            result.put(simpleBizListItem.getSid(), simpleBizListItem);
        }
        return result;
    }

    @Transactional
    public void deleteSimpleBizListItemsByImportId(BizList bizList, long importId) {
        Set<BizListItemAlias> biz = bizListItemAliasRepository.findAllByImportId(importId);
        List<Long> ids = biz.stream().map(BizListItemAlias::getId).collect(Collectors.toList());
        if (ids != null && !ids.isEmpty()) {
            bizListItemAliasRepository.deleteByIds(ids);
        }
        simpleBizListItemRepository.deleteByImportId(importId);
    }

    @Transactional
    public void deleteClientsBizListItemsByImportId(BizList bizList, long importId) {
        logger.info("delete Clients BizListItems By ImportId {}", importId);
//        Set<BizListItemAlias> allByImportId = bizListItemAliasRepository.findAllByImportId(importId);
//        logger.debug("delete {} Clients BizListItems Aliases",allByImportId.size());
//        bizListItemAliasRepository.delete(allByImportId);
//        logger.debug("delete the Clients BizListItems");
//        clientBizListItemRepository.deleteByImportId(importId);
        List<ClientBizListItem> clients = clientBizListItemRepository.findByImportId(importId);
        logger.info("delete {} Clients BizListItems", clients.size());
        clientBizListItemRepository.deleteAll(clients);
    }

    @Transactional(readOnly = true)
    public long countBizListItems(long bizListId) {
        return simpleBizListItemRepository.countByBizListId(bizListId);
    }

    @Transactional
    public void deleteByBizListItems(long bizListId) {
        simpleBizListItemRepository.deleteByBizListId(bizListId);
    }

    @Transactional(readOnly = true)
    public long countActiveBizListItems(long bizListId) {
        return simpleBizListItemRepository.countActiveByBizListId(bizListId);
    }

    public SimpleBizListItem updateBizListItemStatus(long bizListId, String itemId, BizListItemState bizListItemState) {
        SimpleBizListItem one = simpleBizListItemRepository.findById(itemId).orElse(null);
        one.setState(bizListItemState);
        SimpleBizListItem save = simpleBizListItemRepository.save(one);
        bizListItemCache.invalidate(itemId);
        bizListService.updateBizListUpdateDate(save.getBizList());
        return save;
    }


    @Transactional(readOnly = true)
    public SimpleBizListItem getSimpleBizListItem(String bizListItemId) {
        return simpleBizListItemRepository.findById(bizListItemId).orElse(null);
    }

    @Transactional(readOnly = true)
    public SimpleBizListItem getSimpleBizListItemFull(String bizListItemId) {
        return simpleBizListItemRepository.findWithAliases(bizListItemId);
    }

    @Transactional(readOnly = true)
    public SimpleBizListItemDto getSimpleBizListItemDto(String bizListItemId) {
        return simpleBizListItemRepository.findById(bizListItemId)
                .map(BizListItemService::convertSimpleBizListItem)
                .orElse(null);
    }

    public static SimpleBizListItemDto convertSimpleBizListItem(SimpleBizListItem simpleBizListItem) {
        if (simpleBizListItem == null) {
            return null;
        }
        SimpleBizListItemDto simpleBizListItemDto = new SimpleBizListItemDto();
        fillSimpleBizListItem(simpleBizListItem, simpleBizListItemDto);
        simpleBizListItemDto.setAggregated(false);
        return simpleBizListItemDto;
    }

    private static void fillSimpleBizListItem(SimpleBizListItem simpleBizListItem, SimpleBizListItemDto simpleBizListItemDto) {
        simpleBizListItemDto.setBusinessId(simpleBizListItem.getEntityId());
        simpleBizListItemDto.setName(simpleBizListItem.getName());
        simpleBizListItemDto.setId(simpleBizListItem.getSid());
        simpleBizListItemDto.setState(simpleBizListItem.getState());
        simpleBizListItemDto.setBizListId(simpleBizListItem.getBizList().getId());
        simpleBizListItemDto.setType(simpleBizListItem.getBizList().getBizListItemType());
        simpleBizListItem.setImportId(simpleBizListItem.getImportId());
        fillAliases(simpleBizListItem, simpleBizListItemDto);
        if (simpleBizListItem instanceof ConsumerBizListItem) {
            simpleBizListItemDto.setEncryptedFieldsJson(((ConsumerBizListItem) simpleBizListItem).getEncryptedFieldsJson());
        }
    }

    private static void fillAliases(SimpleBizListItem simpleBizListItem, SimpleBizListItemDto simpleBizListItemDto) {
        List<BizListItemAliasDto> entityAliases = new ArrayList<>();
        if (simpleBizListItem.getBizListItemAliases() != null) {
            for (BizListItemAlias bizListItemAlias : simpleBizListItem.getBizListItemAliases()) {
                entityAliases.add(new BizListItemAliasDto(bizListItemAlias.getId(), bizListItemAlias.getAlias()));
            }
        }
        simpleBizListItemDto.setEntityAliases(entityAliases);
    }

    public static Page<? extends SimpleBizListItemDto> convertBizListItems(BizListItemType bizListItemType,
                                                                           Page<? extends SimpleBizListItem> result,
                                                                           PageRequest pageRequest) {
        switch (bizListItemType) {
            case CLIENTS:
                //noinspection unchecked
                return convertClientBizListItems((Page<ClientBizListItem>) result, pageRequest);
            default:
                return convertSimpleBizListItems(result, pageRequest);
        }
    }

    private static Page<? extends SimpleBizListItemDto> convertClientBizListItems(Page<ClientBizListItem> listPage, PageRequest pageRequest) {
        List<SimpleBizListItemDto> content = new ArrayList<>();
        for (ClientBizListItem bizListItem : listPage.getContent()) {
            content.add(convertClientBizListItem(bizListItem));
        }
        return new PageImpl<>(content, pageRequest, listPage.getTotalElements());
    }

    private static ClientBizListItemDto convertClientBizListItem(ClientBizListItem clientBizListItem) {
        ClientBizListItemDto clientBizListItemDto = new ClientBizListItemDto();
        fillSimpleBizListItem(clientBizListItem, clientBizListItemDto);
        return clientBizListItemDto;
    }

    private static Page<? extends SimpleBizListItemDto> convertSimpleBizListItems(Page<? extends SimpleBizListItem> listPage, PageRequest pageRequest) {
        List<SimpleBizListItemDto> content = new ArrayList<>();
        for (SimpleBizListItem simpleBizListItem : listPage.getContent()) {
            content.add(convertSimpleBizListItem(simpleBizListItem));
        }
        return new PageImpl<>(content, pageRequest, listPage.getTotalElements());
    }

    public static void fillBizListItemDtoDetails(SimpleBizListItemDto simpleBizListItemDto, SimpleBizListItem simpleBizListItem) {
        simpleBizListItemDto.setName(simpleBizListItem.getName());
        simpleBizListItemDto.setBusinessId(simpleBizListItem.getEntityId());
        simpleBizListItemDto.setImportId(simpleBizListItem.getImportId());
        simpleBizListItemDto.setType(simpleBizListItem.getBizList().getBizListItemType());
    }
}
