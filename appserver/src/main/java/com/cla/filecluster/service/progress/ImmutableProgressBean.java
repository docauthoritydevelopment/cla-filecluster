package com.cla.filecluster.service.progress;

import com.cla.connector.utils.Pair;

public final class ImmutableProgressBean extends ProgressBean {

    protected static ImmutableProgressBean of (ProgressBean progressBean) {
        if (progressBean == null) {
            return null;
        }
        return new ImmutableProgressBean(progressBean);
    }

    private ImmutableProgressBean(ProgressBean other) {
        super(other);
    }

    @Override
    public void addComposingProgress(ProgressBean progress, float weight) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");
    }

    @Override
    public void addPayload(String key, Object payload) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");    }

    @Override
    public void addPayload(Pair<String, Object> payload) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");    }

    @Override
    public void addPayload(String key, Object payload, String message) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");    }

    @Override
    public synchronized int addPercentage(int percentage, String message) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");
    }

    @Override
    public synchronized void setPercentage(int percentage, String message) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");
    }

    @Override
    public void addProgressMessage(ProgressMessage progressMessage) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");
    }

    @Override
    public void setProgressMessage(ProgressMessage progressMessage) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");
    }

    @Override
    public void setFailed(Throwable exception) {
        throw new UnsupportedOperationException("This is an immutable progress bean that can't be modified.");
    }
}
