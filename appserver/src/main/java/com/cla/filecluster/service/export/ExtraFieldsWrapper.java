package com.cla.filecluster.service.export;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import java.util.HashMap;
import java.util.Map;

public class ExtraFieldsWrapper<D> {

    @JsonUnwrapped
    private D baseObj;

    private Map<String, Object>  extraFields = new HashMap<>();

    public ExtraFieldsWrapper(D baseObj) {
        this.baseObj = baseObj;
    }

    @JsonAnyGetter
    public Map<String, Object> getExtraFields() {
        return extraFields;
    }

    public void putExtraField(String name, Object value) {
        extraFields.put(name, value);
    }

    public void getExtraField(String name) {
        extraFields.get(name);
    }

    public void putExtraFields(Map<String, Object>  extraFields) {
        this.extraFields.putAll(extraFields);
    }

    public D getBaseObj() {
        return baseObj;
    }
}
