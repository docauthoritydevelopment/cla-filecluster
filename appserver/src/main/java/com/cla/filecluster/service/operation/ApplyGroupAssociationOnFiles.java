package com.cla.filecluster.service.operation;

import com.cla.common.constants.CatFileFieldType;
import com.cla.common.constants.SolrCommitType;
import com.cla.common.domain.dto.AssociableEntityDto;
import com.cla.common.domain.dto.schedule.ScheduledOperationType;
import com.cla.common.tag.priority.TagPriority;
import com.cla.connector.domain.dto.error.BadRequestType;
import com.cla.connector.domain.dto.file.AssociationType;
import com.cla.filecluster.domain.dto.AssociationEntity;
import com.cla.filecluster.domain.entity.operation.ScheduledOperationMetadata;
import com.cla.filecluster.domain.entity.pv.FileGroup;
import com.cla.filecluster.domain.entity.scope.BasicScope;
import com.cla.filecluster.domain.exceptions.BadRequestException;
import com.cla.filecluster.repository.jpa.group.ScheduledOperationRepository;
import com.cla.filecluster.service.api.msgs.MessageHandler;
import com.cla.filecluster.service.categories.AssociationsService;
import com.cla.filecluster.service.files.managers.FileGroupService;
import com.cla.filecluster.service.scope.ScopeService;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by: yael
 * Created on: 8/29/2019
 */
@Service
public class ApplyGroupAssociationOnFiles implements ScheduledOperation {

    private final static Logger logger = LoggerFactory.getLogger(ApplyGroupAssociationOnFiles.class);

    public static String ASSOCIATION_PARAM = "ASSOCIATION_ENTITY";
    public static String GROUP_ID_PARAM = "GROUP_ID";
    public static String ACTION_PARAM = "ACTION_PARAM";

    @Autowired
    private FileGroupService fileGroupService;

    @Autowired
    private AssociationsService associationsService;

    @Autowired
    private ScopeService scopeService;

    @Autowired
    private MessageHandler messageHandler;

    @Autowired
    private ScheduledOperationRepository scheduledOperationRepository;

    @Override
    public int getTotalStepNum() {
        return 1;
    }

    @Override
    public boolean runStep(int step, Map<String, String> opData) {
        if (step == 0) {
            String groupId = opData.get(GROUP_ID_PARAM);
            AssociationEntity entity = AssociationsService.convertFromAssociationEntity(groupId, opData.get(ASSOCIATION_PARAM), AssociationType.GROUP);
            String action = opData.get(ACTION_PARAM);
            boolean addAction = action.equalsIgnoreCase("ADD");

            FileGroup group = fileGroupService.getById(groupId);
            if (group == null) {
                logger.error("cant find group {} for operation {} - skip", groupId, operationTypeHandled());
                return true;
            }

            if (entity.getDocTypeId() != null) {
                BasicScope scope = entity.getAssociationScopeId() == null ? null : scopeService.getFromCache(entity.getAssociationScopeId());
                associationsService.updateDocTypeToCategoryFiles(groupId, CatFileFieldType.USER_GROUP_ID,
                        entity.getDocTypeId(), addAction, scope,
                        TagPriority.FILE_GROUP, SolrCommitType.SOFT_NOWAIT);
            } else {
                List<AssociableEntityDto> entities = associationsService.getEntitiesForAssociations(Lists.newArrayList(entity));
                if (entities.isEmpty()) {
                    logger.error("cant find entity {} for group {} operation {} - skip", entity, groupId, operationTypeHandled());
                    return true;
                }
                associationsService.updateAssociationToCategoryFilesByGroupId(groupId, entities.get(0),
                        entity.getAssociationScopeId(), addAction, SolrCommitType.SOFT_NOWAIT);
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    public void validate(Map<String, String> opData) {
        String groupId = opData.get(GROUP_ID_PARAM);
        if (Strings.isNullOrEmpty(groupId)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.group.empty"), BadRequestType.MISSING_FIELD);
        }

        String json = opData.get(ASSOCIATION_PARAM);
        if (Strings.isNullOrEmpty(json)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.empty"), BadRequestType.MISSING_FIELD);
        }
        if (AssociationsService.convertFromAssociationEntity(groupId, json, AssociationType.GROUP) == null) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.association.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }

        String action = opData.get(ACTION_PARAM);
        if (Strings.isNullOrEmpty(action)) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.action.empty"), BadRequestType.MISSING_FIELD);
        }
        if (!action.equalsIgnoreCase("ADD") && !action.equalsIgnoreCase("REMOVE")) {
            throw new BadRequestException(messageHandler.getMessage("scheduled-operations.action.invalid"), BadRequestType.UNSUPPORTED_VALUE);
        }
    }

    @Override
    public boolean operationAllowedToRun(ScheduledOperationMetadata operationMetadata) {
        Map<String, String> opData = operationMetadata.getOpData();
        // check no other operation on group before running
        String groupId = opData.get(GROUP_ID_PARAM);
        List<ScheduledOperationType> types = Arrays.asList(ScheduledOperationType.values());
        types = types.stream().filter(ScheduledOperationType::isGroupOperation).collect(Collectors.toList());
        List<ScheduledOperationMetadata> opers = scheduledOperationRepository.findAllByTypes(types);
        if (opers != null) {
            for (ScheduledOperationMetadata data : opers) {
                if (data.getOpData().values().contains(groupId)) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public ScheduledOperationType operationTypeHandled() {
        return ScheduledOperationType.APPLY_GROUP_ASSOCIATIONS_TO_SOLR;
    }

    @Override
    public boolean shouldAllowCreateWhileScheduleDown() {
        return true;
    }
}
