package com.cla.filecluster.service.export.exporters.root_folders.mix_in;

import com.cla.filecluster.service.export.exporters.root_folders.RootFoldersHeaderFields;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by: yael
 * Created on: 3/7/2019
 */
public class ScheduleGroupDtoMixin {
    @JsonProperty(RootFoldersHeaderFields.SCHEDULE_GROUP_NAME)
    private String name;
}
