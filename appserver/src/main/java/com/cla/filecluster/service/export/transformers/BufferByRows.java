package com.cla.filecluster.service.export.transformers;

import com.google.common.collect.Iterables;
import org.apache.commons.lang3.tuple.Pair;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public class BufferByRows<V> implements Transformer<Iterable<V>, Iterable<V>> {

    private final int bufferSize;
    private final BinaryOperator<Iterable<V>> accumulator;

    public static <V1> BufferByRows<V1> with(int bufferSize, BinaryOperator<Iterable<V1>> accumulator) {
        return new BufferByRows<>(bufferSize, accumulator);
    }

    public static <V1> BufferByRows<V1> with(int bufferSize) {
        return new BufferByRows<>(bufferSize, Iterables::concat);
    }

    public BufferByRows(int bufferSize, BinaryOperator<Iterable<V>> accumulator) {
        this.bufferSize = bufferSize;
        this.accumulator = accumulator;
    }

    @Override
    public Function<Flux<Iterable<V>>, Publisher<DataBuffer>> asDataBuffer() {
        throw new UnsupportedOperationException("BufferByRows cannot yet be transformed directly to a data output stream");
    }

    @Override
    public Publisher<Iterable<V>> apply(Flux<Iterable<V>> inputFlux) {
        return inputFlux
                // map iterable to pair<size, iterable>
                .map(iterable -> Pair.of(Iterables.size(iterable), iterable))
                // scan - for each input iterable, return a pair of <sum, iterable>
                .scan((pair1, pair2) -> Pair.of(pair1.getLeft() + pair2.getLeft(), pair2.getRight()))
                // buffer until sum reaches the limit (not including the item that exceeds the limit)
                .bufferUntil((pair) -> pair.getLeft() > bufferSize, true)
                // transform List<Pair<sum, Iterable<V>>> to Iterable<V> using the provided accumulator.
                .map(list -> list.stream().map(Pair::getRight).reduce(accumulator).orElse(new ArrayList<V>()));
    }
}
