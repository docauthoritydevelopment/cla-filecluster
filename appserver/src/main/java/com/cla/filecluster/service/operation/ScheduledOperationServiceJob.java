package com.cla.filecluster.service.operation;

import com.cla.filecluster.repository.jpa.group.ScheduledOperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
@Profile({ "prod", "win", "dev", "default" })
public class ScheduledOperationServiceJob {

    private Logger logger = LoggerFactory.getLogger(ScheduledOperationServiceJob.class);

    private static long ONE_DAY_IN_MILLIS = TimeUnit.DAYS.toMillis(1);

    @Autowired
    private ScheduledOperationRepository scheduledOperationRepository;

    @Value("${scheduled-operations.cleanup.days-delete-data:60}")
    private int daysToDeleteData;

    @Scheduled(initialDelayString = "${scheduled-operations.cleanup.initial-delay-millis:30000}",
            fixedRateString = "${scheduled-operations.cleanup.frequency-millis:18000000}")
    @ConditionalOnProperty(value = "is.toolExecutionMode", havingValue = "false", matchIfMissing = true)
    public void deleteObsoleteData() {
        try {
            logger.debug("===> start cleanup");
            long daysToDeleteDataInMillis = daysToDeleteData * ONE_DAY_IN_MILLIS;
            long olderThan = System.currentTimeMillis() - daysToDeleteDataInMillis;
            int deleted = scheduledOperationRepository.deleteByTime(olderThan);
            logger.debug("===> end cleanup removed {} records", deleted);
        } catch (Exception e) {
            logger.error("===> failed cleanup", e);
        }
    }
}
