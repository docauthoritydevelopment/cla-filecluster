package com.cla.filecluster.jobmanager.service;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobStateUpdateInfo;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.common.domain.dto.jobmanager.SimpleJobDto;
import com.cla.filecluster.util.BlockingConsumer;
import com.cla.filecluster.util.ReentrantPriorityLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.cla.common.domain.dto.jobmanager.JobState.DONE;
import static com.cla.common.domain.dto.jobmanager.JobState.PAUSED;

/**
 * Used to coordinate jobs that shouldn't run in parallel, e.g. Ingest-Finalize and Analyze-Finalize.
 *
 * <p>
 * Created by Itai Marko.
 */
@Service
public class JobCoordinator {

    private static final Logger logger = LoggerFactory.getLogger(JobCoordinator.class);

    @Autowired
    private JobManagerService jobManager;

    private ReentrantPriorityLock<JobType> ingestFinAnalyzeFinMutex =
            new ReentrantPriorityLock<>(new IngestFinFirstComparator(), true);

    /**
     * Requests the coordination lock and acquires it if it is not held by another thread.
     * If the lock is held by another thread it will wait for it to be released, in which case the next
     * job to acquire the lock will be determined by the jobType, i.e. some job types have precedence over others.
     *
     * @param jobType the jobType of the requesting job
     */
    public void requestLock(JobType jobType, long... jobIds) throws InterruptedException, CoordinationLockDeniedException {
        logger.debug("Job #{} going into WAITING state before acquiring Ingest-Finalize/Analyze-Finalize coordination lock", jobIds);
        makeJobWaitWhileBlocking(ingestFinAnalyzeFinMutex::lockInterruptibly, jobType, jobIds);
        logger.debug("Job #{} returning to IN_PROGRESS state after acquiring Ingest-Finalize/Analyze-Finalize coordination lock", jobIds);
    }

    /**
     * Releases the coordination lock.
     */
    public void release() {
        ingestFinAnalyzeFinMutex.unlock();
    }

    /**
     * Releases the coordination lock if there are other threads waiting for it.
     * If there are no other threads waiting, the calling thread retains the lock.
     */
    public void yieldToWaitingJobs(long... jobIds) throws InterruptedException, CoordinationLockDeniedException {
        logger.debug("Job #{} going into WAITING state before yielding Ingest-Finalize/Analyze-Finalize coordination lock", jobIds);

        BlockingConsumer<Void> yieldLambda = (Void v) -> ingestFinAnalyzeFinMutex.yield();
        makeJobWaitWhileBlocking(yieldLambda, null, jobIds);

        logger.debug("Job #{} returning to IN_PROGRESS state after acquiring Ingest-Finalize/Analyze-Finalize coordination lock", jobIds);
    }

    /**
     * Wraps the execution of a blocking method with the handling of changing the jobs' state
     * to WAITING before blocking and returning to IN_PROGRESS before returning.
     *
     * @param blockingMethod     the blocking method to wrap
     * @param blockingMethodArg  argument for the blocking method
     * @param jobIds             job ids of which state change should be handled
     * @param <T>                the type of the argument for the blocking method
     * @throws CoordinationLockDeniedException in case all jobs were paused/stopped while blocking
     * @throws InterruptedException if the current thread is interrupted
     */
    private <T> void makeJobWaitWhileBlocking(BlockingConsumer<T> blockingMethod, T blockingMethodArg, long... jobIds)
            throws CoordinationLockDeniedException, InterruptedException {

        if (jobIds.length == 0) {
            // Some flows request the lock out of a job context.
            // In that case there's no need to change any job's state
            blockingMethod.accept(blockingMethodArg);

        } else {
            // Before acquiring the lock, change the jobs that will block to WAITING state
            updateJobsStates(jobIds, JobState.WAITING);

            // Execute the actual blocking method
            blockingMethod.accept(blockingMethodArg);

            // After acquiring the lock, validate that at least one job is still in progress.
            // If none are in progress, release the lock and throw a CoordinationLockDeniedException
            validateJobsState(jobIds);

            // If got here, no exception was thrown. Resume to IN_PROGRESS state before returning
            updateJobsStates(jobIds, JobState.IN_PROGRESS);
        }
    }

    /**
     * @return true iff there other threads waiting for the coordination lock
     */
    public boolean gotWaitingJobs() {
        return ingestFinAnalyzeFinMutex.hasQueuedThreads();
    }

    public boolean isLockHeld() {
        return ingestFinAnalyzeFinMutex.isLocked();
    }

    private void updateJobsStates(long[] jobIds, JobState newJobState) {
        String stateOpMsg;
        switch (newJobState) {
            case WAITING:
                stateOpMsg = "Wait before Ingest-Finalize/Analyze-Finalize. Acquiring coordination lock";
                break;
            case IN_PROGRESS:
                stateOpMsg = "Resume processing after acquiring coordination lock";
                break;
            default:
                throw new IllegalArgumentException("Unsupported JobState");
        }

        List<JobStateUpdateInfo> jobStateUpdateInfos = Arrays.stream(jobIds).
                mapToObj(jobId -> new JobStateUpdateInfo(jobId, newJobState, null, stateOpMsg, false)).
                collect(Collectors.toList());
        jobManager.updateJobStates(jobStateUpdateInfos);
    }

    private void validateJobsState(long[] jobIds) throws CoordinationLockDeniedException {
        long nonPausedJobsNum = Arrays.stream(jobIds).
                mapToObj(jobManager::getJobAsDto).
                filter(jobDto -> jobDto.getState() != PAUSED && jobDto.getState() != DONE).
                map(SimpleJobDto::getId).
                count();

        if (nonPausedJobsNum == 0) {
            // All waiting jobs were paused/stopped
            String jobIdsStr = Arrays.toString(jobIds);
            logger.debug("Jobs with ids: {} were paused/stopped while waiting for Ingest-Finalize/Analyze-Finalize coordination lock", jobIdsStr);
            ingestFinAnalyzeFinMutex.unlock();
            throw new CoordinationLockDeniedException("Jobs with ids: "+jobIdsStr+" have been paused/Stopped while waiting for Ingest-Finalize/Analyze-Finalize coordination lock");
        }
    }

    /**
     * A comparator that puts Ingest-Finalize jobs before all the others
     */
    public static class IngestFinFirstComparator implements Comparator<JobType> {

        @Override
        public int compare(JobType jobType1, JobType jobType2) {
            Objects.requireNonNull(jobType1);
            Objects.requireNonNull(jobType2);


            boolean firstIsIngestFin = jobType1 == JobType.INGEST_FINALIZE;
            boolean secondIsIngestFin = jobType2 == JobType.INGEST_FINALIZE;

            if (firstIsIngestFin && secondIsIngestFin) {
                return 0;
            }
            if (firstIsIngestFin) {
                return -1;
            }
            if (secondIsIngestFin) {
                return 1;
            }
            return 0;
        }
    }
}
