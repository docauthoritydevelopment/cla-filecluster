package com.cla.filecluster.jobmanager.domain.entity;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity {

    @Column(nullable = false, columnDefinition = "datetime(3)", name = "created_ondb")
    protected Date createdOnDB;

    @Column(name = "deleted")
    protected boolean deleted;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreatedOnDB() {
        return createdOnDB;
    }

    @PrePersist
    private void setCreatedOnDB() {
        createdOnDB = new Date();
    }

}
