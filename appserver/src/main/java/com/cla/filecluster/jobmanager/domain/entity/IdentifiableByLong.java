package com.cla.filecluster.jobmanager.domain.entity;

/**
 * Created By Itai Marko
 */
public interface IdentifiableByLong {

    Long getId();
}
