package com.cla.filecluster.jobmanager.repository.jpa;

import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

public interface SimpleTaskRepository extends JpaRepository<SimpleTask, Long> {

    @Modifying
    @Query("UPDATE SimpleTask st SET st.state=:state " +
            " , st.stateUpdateTime=:stateUpdateTime, st.version = st.version + 1 " +
            " WHERE st.id IN (:taskIds) and st.state != 3")
        // prevent done task from change back to another status cause of race condition
    void updateTaskState(@Param("taskIds") Collection<Long> taskId, @Param("state") TaskState taskState, @Param("stateUpdateTime") Long stateUpdateTime);

    @Modifying
    @Query("UPDATE SimpleTask st SET st.state=:stateNew " +
            " , st.stateUpdateTime=:stateUpdateTime, st.version = st.version + 1 " +
            " WHERE st.jobId = :jobId and st.state = :stateOld")
    int updateJobTaskState(@Param("jobId") long jobId, @Param("stateNew") TaskState taskStateNew, @Param("stateOld") TaskState taskStateOld, @Param("stateUpdateTime") Long stateUpdateTime);

    @Modifying
    @Query("UPDATE SimpleTask st SET st.state=:state, st.retries=st.retries+1 " +
            " , st.stateUpdateTime=:stateUpdateTime, st.version = st.version + 1 " +
            "  WHERE st.id IN (:taskIds) and st.state != 3")
        // prevent done task from change back to another status cause of race condition
    void updateTaskStateIncrementRetry(@Param("taskIds") Collection<Long> taskIds, @Param("state") TaskState taskState, @Param("stateUpdateTime") Long stateUpdateTime);

    @Modifying
    @Query("UPDATE SimpleTask st SET st.state=:state, st.retries=st.retries+1 " +
            " , st.stateUpdateTime=:stateUpdateTime, st.version = st.version + 1 " +
            "  WHERE st.partitionId = :partitionId AND st.id IN (:taskIds) " +
            "and st.state != 3")
        // prevent done task from change back to another status cause of race condition
    void updateTaskStateIncrementRetry(@Param("taskIds") Collection<Long> taskIds, @Param("stateUpdateTime") Long stateUpdateTime, @Param("state") TaskState taskState, @Param("partitionId") long partitionId);

    @Query("SELECT COUNT(st) FROM SimpleTask st where st.jobId = :jobId AND " +
            "st.type = :type and st.state != 3")
        // prevent done task from change back to another status cause of race condition
    int getNotDoneTasksForJobByType(@Param("jobId") long jobId, @Param("type") int type);

    @Query("SELECT st FROM SimpleTask st" +
            " WHERE st.jobId=:jobId" +
            " AND st.deleted=false")
    Page<SimpleTask> getAllTasksByJob(@Param("jobId") long jobId, Pageable pageRequest);

    @Query("SELECT count(st) FROM SimpleTask st" +
            " WHERE st.jobId = :jobId" +
            " AND st.path = :path AND st.type = :type")
    long getTaskByJobAndPathAndType(@Param("jobId") long jobId, @Param("path") String path, @Param("type") int type);

    @Query("SELECT st FROM SimpleTask st" +
            " WHERE st.partitionId=:partitionId" +
            " AND st.jobId=:jobId " +
            " AND st.deleted=false")
    Page<SimpleTask> getAllTasksByJob(@Param("jobId") long jobId, @Param("partitionId") long partitionId, Pageable pageRequest);

    @Query("SELECT count(st) FROM SimpleTask st" +
            " WHERE st.jobId=:jobId" +
            " AND st.deleted=false")
    long countNonDeletedTasksByJob(@Param("jobId") long jobId);

    @Query("SELECT st FROM SimpleTask st" +
            " WHERE st.jobId=:jobId " +
            "AND st.state in (:states) " +
            " AND st.deleted=false")
    Page<SimpleTask> getTasksByJobAndStates(@Param("jobId") long jobId, @Param("states") Iterable<TaskState> states, Pageable pageReq);

    @Query("SELECT st FROM SimpleTask st" +
            " WHERE st.partitionId=:partitionId " +
            " AND st.jobId=:jobId " +
            " AND st.state in (:states) " +
            " AND st.deleted=false")
    Page<SimpleTask> getTasksByJobAndStates(@Param("jobId") long jobId, @Param("partitionId") long partitionId, @Param("states") Iterable<TaskState> states, Pageable pageReq);

    @Query("SELECT st.state ,count(*) " +
            "FROM SimpleTask st " +
            " WHERE st.jobId = :jobId" +
            " AND st.deleted = false " +
            " GROUP BY st.state")
    List<Object[]> getTaskStatesByJobId(@Param("jobId") long jobId);

    @Query("SELECT st.state ,count(*) " +
            "FROM SimpleTask st " +
            " WHERE st.partitionId=:partitionId " +
            " AND st.jobId=:jobId " +
            " AND st.deleted=false " +
            " GROUP BY st.state")
    List<Object[]> getTaskStatesByJobId(@Param("jobId") long jobId, @Param("partitionId") long partitionId);

    /*@Query(nativeQuery = true, value = "SELECT st.run_context, st.state, count(*) " +
            "FROM jm_jobs sj JOIN jm_tasks st ON (sj.id = st.job_id) " +
            "WHERE sj.id in (:jobIds) AND " +
            "sj.state in (:jobStates) AND " +
            "st.type = :type AND " +
            "st.deleted = false " +
            "GROUP BY st.run_context, st.state")*/
    @Query(nativeQuery = true, value = "SELECT st.run_context, st.state, st.countSt FROM \n" +
            "(select run_context, state, job_id, count(*) as countSt from jm_tasks\n" +
            "WHERE \n" +
            "deleted = false  AND\n" +
            "type = :type\n" +
            "GROUP BY run_context, state, job_id) st\n" +
            "join (select id from jm_jobs where state in :jobStates AND  id in :jobIds) sj \n" +
            "on st.job_id = sj.id\n")
    List<Object[]> getJobTaskStatesByTypePerRun(@Param("jobIds") Iterable<Long> jobIds, @Param("type") int taskType, @Param("jobStates") Iterable<String> jobStates);

    @Query(nativeQuery = true, value = "SELECT st.job_id, count(*) " +
            "FROM jm_jobs sj JOIN jm_tasks st ON (sj.id = st.job_id) " +
            "WHERE sj.id in (:jobIds) AND " +
            "st.type = :type AND " +
            "st.deleted = false " +
            "GROUP BY st.job_id")
    List<Object[]> getTaskByTypePerJob(@Param("jobIds") Iterable<Long> jobIds, @Param("type") int taskType);

    @Query(nativeQuery = true, value = "SELECT st.job_id, st.state, count(*) " +
            "FROM jm_jobs sj JOIN jm_tasks st ON (sj.id = st.job_id) " +
            "WHERE sj.id in (:jobIds) AND " +
            "sj.state = :jobState AND " +
            "st.type = :type AND " +
            "st.deleted = false " +
            "GROUP BY st.job_id, st.state")
    List<Object[]> getTaskStatesByTypePerJob(@Param("jobIds") Iterable<Long> jobIds, @Param("type") int taskType, @Param("jobState") String jobState);

    @Query(nativeQuery = true, value = "SELECT st.run_context, count(*) " +
            "FROM jm_jobs sj JOIN jm_tasks st ON (sj.id = st.job_id) " +
            "WHERE sj.id in (:jobIds) AND " +
            "sj.state = :jobState AND " +
            "st.type = :type AND " +
            "st.state = :taskState AND " +
            "st.deleted = false " +
            "GROUP BY st.run_context")
    List<Object[]> getTaskCountPerRunByTypeAndState(@Param("jobIds") Iterable<Long> jobIds,
                                                    @Param("type") int taskType, @Param("taskState") int taskState,
                                                    @Param("jobState") String jobState);

    @Query(nativeQuery = true,
            value = "SELECT rf.customer_data_center_id, COUNT(*) " +
                    "FROM jm_tasks st, root_folder rf " +
                    "WHERE st.item_id = rf.id " +
                    "AND st.job_id in (:jobIds) " +
                    "AND rf.customer_data_center_id IN (:dataCenterIds) " +
                    "AND st.type = :type " +
                    "AND st.state IN (:taskStates) " +
                    "AND st.deleted = false " +
                    "GROUP BY rf.customer_data_center_id ")
    List<Object[]> getTaskCountGroupByDataCenterId(@Param("jobIds") Iterable<Long> jobIds,
                                                   @Param("dataCenterIds") Iterable<Long> dataCenterIds,
                                                   @Param("type") int taskType,
                                                   @Param("taskStates") Iterable<Integer> taskStates);

    @Query(nativeQuery = true, value = "SELECT st.* FROM jm_tasks st  USE INDEX (tasks_by_state_idx)\n" +
            "WHERE st.deleted = false\n" +
            "AND st.type = :type\n" +
            "AND st.state = :state\n" +
            "AND st.run_context = :runContext\n")
    List<SimpleTask> getTasksByContext(@Param("type") int type, @Param("state") int state,
                                       @Param("runContext") long contextId, Pageable pageable);

    @Query("SELECT st.id " +
            "FROM SimpleTask st " +
            "WHERE " +
            "st.jobId in (:jobIds) AND " +
            "st.state in (:taskStates) AND " +
            "st.deleted = false")
    List<Long> getTaskIdsByJobsAndStates(@Param("jobIds") Iterable<Long> jobIds, @Param("taskStates") Iterable<TaskState> taskStates);

    @Query("SELECT st " +
            "FROM SimpleTask st " +
            "WHERE st.jobId = :jobId " +
            "AND st.type = :taskType " +
            "AND st.state in (:taskStates) " +
            "AND st.deleted = false")
    List<SimpleTask> getTaskByJobIdAndTaskTypeAndStates(@Param("jobId") long jobId, @Param("taskType") int taskType, @Param("taskStates") Iterable<TaskState> taskStates);

    @Query("SELECT st " +
            "FROM SimpleTask st, SimpleJob sj " +
            "WHERE sj.id = st.jobId " +
            "AND sj.id in (:jobIds) " +
            "AND sj.state in (:jobStates) " +
            "AND st.type = :taskType " +
            "AND st.state = :taskState " +
            "AND st.deleted = false")
    List<SimpleTask> getTasksByTypeAndState(@Param("jobIds") Iterable<Long> jobIds,
                                            @Param("jobStates") Iterable<JobState> jobStates,
                                            @Param("taskType") int type, @Param("taskState") TaskState state);

    @Query(nativeQuery = true, value = "select count(1) from jm_jobs where id = :jobId and state = 'IN_PROGRESS' " +
            "and (select count(1) from jm_tasks where job_id = :jobId and part_id = :partitionId and state in (1,2)) = 0 " +
            "and (select count(1) from jm_tasks where job_id = :jobId and part_id = :partitionId and state = 0) > 0")
    int checkScanJobShouldBePending(@Param("jobId") long jobId, @Param("partitionId") long partitionId);

    @Query(nativeQuery = true, value = "select count(1) from jm_jobs where id = :jobId " +
            "and (select count(1) from jm_tasks where job_id = :jobId and part_id = :partitionId and state in (0,1,2)) = 0 " +
            "and (select count(1) from jm_tasks where job_id = :jobId and part_id = :partitionId and state = 4) > 0")
    int checkScanJobShouldBeFailed(@Param("jobId") long jobId, @Param("partitionId") long partitionId);

    @Query("SELECT st FROM SimpleTask st " +
            "WHERE st.jobId  in (:jobIds) " +
            "AND st.type in (:types) " +
            "AND st.state in (:states) " +
            "AND st.stateUpdateTime < :stateUpdateTime " +
            "AND st.deleted = false ")
    List<SimpleTask> getIdleTasks(@Param("jobIds") Iterable<Long> jobIds,
                                  @Param("types") Iterable<Integer> types,
                                  @Param("states") Iterable<TaskState> states,
                                  @Param("stateUpdateTime") long stateUpdateTime, Pageable pageable);

    @Query("SELECT st FROM SimpleTask st " +
            "WHERE st.runContext in (:runIds) " +
            "AND st.type in (:types) " +
            "AND st.state in (:states) " +
            "AND st.stateUpdateTime < :stateUpdateTime " +
            "AND st.deleted = false ")
    List<SimpleTask> getIdleTasks(@Param("types") Iterable<Integer> types,
                                  @Param("states") Iterable<TaskState> states,
                                  @Param("stateUpdateTime") long stateUpdateTime, @Param("runIds") Iterable<Long> runIds, Pageable pageable);

    @Query("SELECT st.id FROM SimpleTask st " +
            "WHERE st.jobId  in (:jobIds) " +
            "AND st.state in (:states) " +
            "AND st.stateUpdateTime < :stateUpdateTime " +
            "AND st.deleted = false ")
    List<Long> getIdleTaskIds(@Param("jobIds") Iterable<Long> jobIds,
                              @Param("states") Iterable<TaskState> states, @Param("stateUpdateTime") long stateUpdateTime);

    @Transactional
    @Query(nativeQuery = true,
            value = "SHOW CREATE TABLE jm_tasks")
    List<Object[]> showCreateTable();

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "ALTER TABLE jm_tasks ADD PARTITION ( PARTITION p:partitionId VALUES IN (:partitionId))")
    void createPartition(@Param("partitionId") long partitionId);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "ALTER TABLE jm_tasks TRUNCATE PARTITION p:partitionId")
    void truncatePartitionForPartitionId(@Param("partitionId") long partitionId);

    @Modifying
    @Transactional
    @Query(nativeQuery = true,
            value = "ALTER TABLE jm_tasks DROP PARTITION p:partitionId")
    void deletePartitionForPartitionId(@Param("partitionId") long partitionId);

    @Query("SELECT COUNT(st) FROM SimpleTask st" +
            " WHERE st.jobId = :jobId " +
            "AND st.state = :taskState " +
            "AND st.deleted = false ")
    int getJobTasksCountByTaskState(@Param("jobId") long jobId, @Param("taskState") TaskState taskState);

    @Query("SELECT COUNT(st) FROM SimpleTask st" +
            " WHERE st.partitionId=:partitionId " +
            " AND st.jobId = :jobId " +
            " AND st.state = :taskState " +
            " AND st.deleted = false ")
    int getJobTasksCountByTaskState(@Param("jobId") long jobId, @Param("partitionId") long partitionId, @Param("taskState") TaskState taskState);

    @Query(nativeQuery = true,
            value = "SELECT sj.state,COUNT(*) cnt FROM jm_tasks st " +
                    " LEFT JOIN jm_jobs sj ON sj.id = st.job_id " +
                    " WHERE st.part_id=:partitionId " +
                    " AND st.deleted = false " +
                    " GROUP BY sj.state ")
    List<Object[]> getTasksPartitionJobState(@Param("partitionId") long partitionId);

    /*
     *  General DB troubleshooting query
     */
    @Query(nativeQuery = true,
            value = "SHOW ENGINE InnoDB STATUS")
    List<Object[]> showEngineInnoDbStatus();

}
