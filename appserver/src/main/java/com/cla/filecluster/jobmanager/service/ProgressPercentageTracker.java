package com.cla.filecluster.jobmanager.service;

import com.cla.connector.progress.ProgressTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Objects;

import static com.cla.filecluster.jobmanager.service.ProgressPercentageTracker.Scale.*;
import static java.lang.Math.multiplyExact;
import static java.lang.Math.toIntExact;

/**
 * Used to update the percentage progress of a job that comprises multiple execution stages.
 * <p>
 * Before each stage starts, the caller starts stage progress tracking by assigning the stage a weight (in percentage).
 * The Weight represents the stage's proportion in the total work of all of the job's stages.
 * It is the caller's responsibility to have all stages' weights sum up to 100 percent.
 * <p>
 * When updating progress while a stage executes, the ProgressPercentageTracker will accumulate the amount of
 * work done internally and only update when at least 1 percent of the work is achieved.
 * <p>
 *
 * @apiNote This class was first built in percentage terms and then refactored to be able to report progress in
 *          parts of ten thousand (by specifying a {@link Scale} argument
 *          in {@link #ProgressPercentageTracker(Scale, Long, JobManagerService)}).
 *
 * <p>
 * Created by Itai Marko.
 */
public class ProgressPercentageTracker implements ProgressTracker {

    private static final Logger logger = LoggerFactory.getLogger(ProgressPercentageTracker.class);

    private int curStageWeight;
    private long curStageOnePercent ;
    private long curStageProgress;
    private long workFactor;
    private int curStageReportedPercentage;

    private final Long jobId;
    private final JobManagerService jobManagerService;
    private final Scale scale;

    /**
     * Creates a new ProgressPercentageTracker that will report progress in terms of percentage.
     *
     * @param jobId             the id of the job to report progress for
     * @param jobManagerService used to report the progress
     */
    public ProgressPercentageTracker(Long jobId, JobManagerService jobManagerService) {
        this(PERCENT, jobId, jobManagerService);
    }

    /**
     * Creates a new ProgressPercentageTracker that will report progress in the accordance with the given scale.
     * <p>
     * The scale parameter specifies the scale of progress reporting. For instance reporting progress from 0 to 100 is
     * specified by the {@link Scale#PERCENT}. reporting progress from 0 to 10000 is specified by the
     * {@link Scale#PER_TEN_THOUSAND}.
     *
     * @param scale             the scale
     * @param jobId             the id of the job to report progress for
     * @param jobManagerService used to report the progress
     */
    public ProgressPercentageTracker(Scale scale, Long jobId, JobManagerService jobManagerService) {
        Objects.requireNonNull(jobId);
        Objects.requireNonNull(jobManagerService);
        reset();
        this.scale = scale;
        this.jobId = jobId;
        this.jobManagerService = jobManagerService;
        this.jobManagerService.updateJobCounters(jobId, 0, 100, scale.partsInWhole);
    }

    /**
     * Initiates tracking of a stage, assigning it the given weight.
     * Should be called before the execution of each stage.
     * <p>
     * It is the caller's responsibility to have all stages' weights sum up to 100 percent.
     *
     * @param stageWeight  the weight to assign to the stage
     */
    @Override
    public void startStageTracking(int stageWeight) {
        if (stageWeight <= 0 || stageWeight > scale.partsInWhole) {
            throw new IllegalArgumentException(
                    "stageWeight must be between 1 and " + scale.partsInWhole + ". Got " + stageWeight);
        }

        // end previous stage if any
        if (curStageWeight >= 0) {
            endCurStage(true);
        }

        curStageWeight = stageWeight;
        curStageProgress = 0L;
        curStageReportedPercentage = 0;
    }

    /**
     * Sets an estimate of the total work that's going to be done by the current stage.
     * Should be called in the beginning of the execution of each stage and only after a stage was started.
     * <p>
     * The given stageTotalEstimate is used to calculate the amount of work that makes up
     * one percent of the work of all the job's stages.
     *
     * @param stageTotalEstimate  the estimate of the stage's work to be done
     */
    @Override
    public void setCurStageEstimatedTotal(long stageTotalEstimate) {
        if (stageTotalEstimate < 0L) {
            throw new IllegalArgumentException("stageTotalEstimate must be non-negative. Got " + stageTotalEstimate);
        }
        if (curStageWeight < 0) {
            throw new IllegalStateException("Can't set stage total. Stage not started");
        }
        workFactor = calculateFactor(stageTotalEstimate, curStageWeight);
        curStageOnePercent = multiplyExact(stageTotalEstimate, workFactor)/curStageWeight;
    }

    /**
     * Advances the progress of the job by a percentage calculated from the given amount of work.
     * Should be called every time there's and amount of work regarding which progress should be reported.
     * Must be called only after a stage was started and its estimated total was set.
     *
     * @param amount  the amount of work done that should be reported as progress
     */
    @Override
    public boolean incrementProgress(long amount) {
        if (curStageOnePercent < 0L) {
            throw new IllegalStateException("Stage total not set");
        }
        boolean progressReported = false;
        int percentageIncrement = calculatePercentageIncrement(amount);
        if (percentageIncrement > 0) {
            if (curStageReportedPercentage + percentageIncrement <= curStageWeight) {
                jobManagerService.incJobCounters(jobId, percentageIncrement, 0);
                curStageProgress %= curStageOnePercent;
                curStageReportedPercentage += percentageIncrement;
                progressReported = true;
            } else {
                logOverflow(percentageIncrement, amount);
                if (curStageReportedPercentage < curStageWeight) {
                    endCurStage(false);
                    progressReported = true;
                }
            }
        }
        return progressReported;
    }

    private int calculatePercentageIncrement(long amount) {
        long workDelta = multiplyExact(amount, workFactor);
        if (workDelta == 0L) {
            return 0;
        }
        curStageProgress += workDelta;
        return toIntExact(curStageProgress/curStageOnePercent);
    }

    /**
     * Advances the progress of the job to indicate that the current stage is done.
     * <p>
     * After this method was called, a new stage must be started.
     */
    @Override
    public void endCurStage() {
        endCurStage(true);
    }

    private void endCurStage(boolean reset) {
        if (curStageWeight < 0) {
            throw new IllegalStateException("Can't end stage. Stage not started");
        }
        jobManagerService.incJobCounters(jobId, curStageWeight - curStageReportedPercentage, 0);
        curStageProgress = 0L;
        curStageReportedPercentage = curStageWeight;
        if (reset) {
            reset();
        }
    }

    private void reset() {
        curStageWeight = -1;
        curStageOnePercent = -1L;
        workFactor = 1L;
    }

    /**
     * Reports progress of 100% regardless of what the progress was before.
     */
    @Override
    public void endTracking() {
        jobManagerService.updateJobCounters(jobId, scale.partsInWhole, 0);
        reset();
        curStageProgress = 0L;
        curStageReportedPercentage = 0;
    }

    private void logOverflow(int percentageIncrement, long amount) {
        logger.debug("Progress percentage overflow. Percentage increment: {}. Work amount increment: {}",
                percentageIncrement, amount);

        if (logger.isTraceEnabled()) {
            logger.trace("   Current stage weight: {}", curStageWeight);
            logger.trace("   Current stage reported percentage: {}", curStageReportedPercentage);
            logger.trace("   Current stage progress: {}", curStageProgress);
            logger.trace("   Current stage one percent: {}", curStageOnePercent);
            logger.trace("   Work factor: {}", workFactor);
            String stackTrace = Arrays.toString(Thread.currentThread().getStackTrace());
            logger.trace("   Stack trace: {}", stackTrace);
        }
    }

    /**
     * Calculates the factor (a.k.a {@link #workFactor}), by which to multiply work parameters (stageTotalEstimate in
     * {@link #setCurStageEstimatedTotal} and amount in {@link #incrementProgress}).
     * <p>
     * This factor is needed when calculating {@link #curStageOnePercent} in case that the numerator is smaller than the
     * denominator, we need to ensure that the quotient will be positive.
     *
     * @param total     the numerator that may need to be multiplied by the factor, must be positive.
     * @param weight    the denominator, must be positive.
     * @return          the factor by which to multiply the numerator so that it will be greater or equal to the denominator
     */
    private long calculateFactor(long total, int weight) {
        if (total < 0L || weight <= 0) {
            String s = "calculateFactor() called with non-positive arguments. Got total: "+ total + " weight: "+ weight;
            throw new IllegalArgumentException(s);
        }
        if (total == 0L) {
            return 0L;
        }

        long factor = 1L;
        if (total < weight) {
            factor = weight / total;
            factor += (weight % total == 0) ? 0 : 1;
        }
        return factor;
    }

    public enum Scale {
        PERCENT(100),
        PER_TEN_THOUSAND(10000);

        private final int partsInWhole;

        Scale(int partsInWhole) {
            this.partsInWhole = partsInWhole;
        }
    }
}
