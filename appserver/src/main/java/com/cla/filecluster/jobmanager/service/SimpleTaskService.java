package com.cla.filecluster.jobmanager.service;

import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class SimpleTaskService {

    public enum PartitionStatus {
        PARTITION_STATUS_NOT_CONFIGURED,
        PARTITION_STATUS_EXISTS,
        PARTITION_STATUS_NOT_EXISTS
    };

    private Logger logger = LoggerFactory.getLogger(SimpleTaskService.class);

    @Autowired
    private SimpleTaskRepository simpleTaskRepository;

    /**
     * If true, we alrady know and reported that partitions are not configured
     */
    private boolean partitionNotConfigured;
    /*
    ** Low level services tuned to manage MySql partitions for long term self maintenance of the tasks table.
     */

    private PartitionStatus tablePartitionStatus(long partitionId) {
        List<Object[]> sct = simpleTaskRepository.showCreateTable();
        Object co = sct.get(0)[1];
        if (!(co instanceof String)) {
            return PartitionStatus.PARTITION_STATUS_NOT_CONFIGURED;
        }
        String coStr = (String) co;
        if (!coStr.contains("PARTITION p0 ")) {
            return PartitionStatus.PARTITION_STATUS_NOT_CONFIGURED;
        }
        return coStr.contains("PARTITION p" + partitionId + " ") ?
                PartitionStatus.PARTITION_STATUS_EXISTS:
                PartitionStatus.PARTITION_STATUS_NOT_EXISTS;
    }

    private static Pattern partitionMatchPattern = Pattern.compile("PARTITION p(\\d+) VALUES");
    public List<Long> getTaskTablePartitionIds(){
        /*
         * Get list of partitions by parsing MySql create-table statement
         * For example:
         *     CREATE TABLE `jm_tasks` ( ... ) ENGINE=InnoDB DEFAULT CHARSET=utf8
         *     / *!50100 PARTITION BY LIST (`part_id`)
         *     (PARTITION p0 VALUES IN (0) ENGINE = InnoDB,
         *      PARTITION p2493 VALUES IN (2493) ENGINE = InnoDB) * /;
         */
        List<Object[]> res = simpleTaskRepository.showCreateTable();
        List<Long> partitionIds = new ArrayList<>();
        if (!res.isEmpty() && res.get(0).length == 2 && (res.get(0)[1] instanceof String)) {
            String ct = (String) res.get(0)[1];
            Matcher matcher = partitionMatchPattern.matcher(ct);
            while (matcher.find()) {
                String partStr = matcher.group(1);
                try {
                    Long partNum = Long.valueOf(partStr);
                    partitionIds.add(partNum);
                } catch (NumberFormatException e) {}
            }
            if (!partitionIds.remove(0L)) {
                logger.warn("Missing partition #0 in tasks table");
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Found {} partitions in tasks table: {}", partitionIds.size(),
                        partitionIds.stream().map(p->""+p).collect(Collectors.joining(",")));
            }
        } else {
            logger.error("Could not get partition status for tasks table");
        }
        return partitionIds;
    }

    public void createPartitionIfNeeded(long partitionId) {
        if (partitionNotConfigured) {
            return;
        }
        logger.debug("createPartitionIfNeeded({})", partitionId);
        try {
            PartitionStatus ps = tablePartitionStatus(partitionId);
            if (ps == PartitionStatus.PARTITION_STATUS_NOT_CONFIGURED) {
                logger.warn("Partitions not configured for SimpleTasks table");
                partitionNotConfigured = true;
                return;
            }
            if (ps == PartitionStatus.PARTITION_STATUS_NOT_EXISTS) {
                logger.debug("Create Partition for partitionId {}", partitionId);
                simpleTaskRepository.createPartition(partitionId);
            }
        } catch (Exception e) {
            logger.error("Error during createPartition({}). {}", partitionId, e.getMessage(), e);
        }
    }

    public boolean deleteSimpleTaskByPartitionId(long partitionId) {
        logger.debug("deleteSimpleTaskByPartitionId({})", partitionId);
//        createPartitionIfNeeded(partitionId);
        try {
            simpleTaskRepository.truncatePartitionForPartitionId(partitionId);
        } catch (Exception e) {
            logger.error("Error during truncatePartitionForPartitionId({}). {}", partitionId, e.getMessage(), e);
            return false;
        }
        try {
            simpleTaskRepository.deletePartitionForPartitionId(partitionId);
        } catch (Exception e) {
            logger.error("Error during deletePartitionForPartitionId({}). {}", partitionId, e.getMessage(), e);
            return false;
        }
        return true;
    }

}
