package com.cla.filecluster.jobmanager.domain.entity;

import java.util.HashMap;
import java.util.Map;

// This enum is better controlled to optimize the following use-cases:
//      1) Allow DB to hold ordinals rather than enum strings to minimize record type
//      2) Allow code to use strongly typed enum items
//      3) Allow dynamic addition of new values with no impact on deployed systems with active records in the DB during upgrade
public enum TaskType {
    UNKNOWN(0),

    SCAN_ROOT_FOLDER(11),

    SCAN_FINALIZE(22),

    INGEST_FILE(31),

    INGEST_FINALIZE_CONTENT(41),

    ANALYZE_CONTENT(51),
    ANALYZE_FINALIZE(52),

    SOLR_UPDATE_FILE(61),
    SOLR_UPDATE_GROUP(62),
    SOLR_UPDATE_DOC_FOLDER(63),

    EXTRACT_BIZ_LIST_ITEM(71),
    EXTRACT_FINALIZE(72),

    OPTIMIZE_INDEXES(81),

    DUMMY(99);

    private static final Map<Integer, TaskType> intMapping = new HashMap<Integer, TaskType>();
    private final int value;
    private final String name;

    TaskType(final int value) {
        this(value, "_"+value);
    }

    TaskType(final int value, final String name) {
        this.value = value;
        this.name = name;
    }

    static {
        for (final TaskType v : TaskType.values()) {
            intMapping.put(v.value, v);
        }
    }

    public int toInt() {
        return value;
    }

    @Override
    public String toString() {
        return name;
    }

    public static TaskType valueOf(final int value) {
        final TaskType mt = TaskType.intMapping.get(value);
        if (mt == null) {
            throw new RuntimeException("Invalid value: " + value);
        }
        return mt;
    }

}
