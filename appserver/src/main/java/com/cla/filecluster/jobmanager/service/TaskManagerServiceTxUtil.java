package com.cla.filecluster.jobmanager.service;

import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import org.hibernate.StaleStateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * used to create new transactions for status change and allow retry
 *
 * Created by: yael
 * Created on: 5/10/2018
 */
@Service
public class TaskManagerServiceTxUtil {

    private static final Logger logger = LoggerFactory.getLogger(TaskManagerServiceTxUtil.class);

    private static final int RETRY_MAX_OPT_LOCK_TASK = 1;

    @Autowired
    private SimpleTaskRepository simpleTaskRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Retryable(maxAttempts = RETRY_MAX_OPT_LOCK_TASK, value = {StaleStateException.class, ObjectOptimisticLockingFailureException.class})
    public void updateTaskStatus(Long taskId, TaskState taskState, String json) {
        simpleTaskRepository.findById(taskId)
                .ifPresent(task -> {
                    if (TaskState.DONE.equals(task.getState())) {
                        logger.trace("do not update status to {} for task id {} - task already done", taskState, taskId);
                        return;
                    }
                    if (taskState.equals(task.getState())) {
                        logger.trace("do not update status to {} for task id {} - that is already the current status", taskState, taskId);
                        return;
                    }
                    task.setState(taskState);
                    task.setStateUpdateTime();
                    if (json != null) {
                        task.setJsonData(json);
                    }
                    logger.trace("update status to {} for task id {}", taskState, taskId);
                    simpleTaskRepository.save(task);
                });

    }


}
