package com.cla.filecluster.jobmanager.domain.entity;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Table( name = "jm_jobs",
        indexes = {
                @Index(name = "state_idx", columnList = "state"),
                @Index(name = "type_state_idx", columnList = "type,state,deleted"),
                @Index(name = "run_context_idx", columnList = "run_context")
        })
public class SimpleJob extends Job {


    public SimpleJob() {
    }

    public SimpleJob(long partitionId, long runId) {
        this.tasksPartition = partitionId;
        setRunContext(runId);
    }

    @Column(name = "item_id")
    private long itemId;

    @Column(columnDefinition = "char(36)", name = "str_item_id")
    private String strItemId;

    @Column(name = "name")
    private String name;

    // Initiation context for auditing, system analysis. e.g. auto, manual + user-name, etc.
    @Column(name = "initiation_details")
    private String initiationDetails;

    @Column(name = "tasks_part_id")
    private long tasksPartition;

    // when the job status was changed to IN_PROGRESS
    private long startRunTime = 0;

    // how many total milliseconds the job spent in paused mode
    private long pauseDuration = 0;

    // when the job was paused or stopped last time
    private long lastStopTime = 0;

    // total original number of tasks
    private int estimatedTaskCount = 0;

    // how many tasks were done last time we checked
    private int currentTaskCount = 0;

    // how many failures (files/tasks/etc..)
    private Integer failedCount = 0;

    // when the latest done tasks count was taken
    private long lastTaskCountTime = 0;

//     // How long job was in pause state since the latest done tasks count was taken
//    private long pauseDurationSinceLastTaskCountTime = 0;

    private long taskRetriesCount;

    public static final int JOB_STATE_STRING_MAX_SIZE = 4096;
    @Length(max=JOB_STATE_STRING_MAX_SIZE)
    private String jobStateString;

    @Version
    @ColumnDefault(value="'0'")
    private int version = 0;

    private int scannedMeaningfulFilesCount = 0;

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public void setStrItemId(String strItemId) {
        this.strItemId = strItemId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInitiationDetails(String initiationDetails) {
        this.initiationDetails = initiationDetails;
    }

    public void setTasksPartition(long tasksPartition) {
        this.tasksPartition = tasksPartition;
    }

    public long getItemId() {
        return itemId;
    }

    public String getStrItemId() {
        return strItemId;
    }

    public String getName() {
        return name;
    }

    public String getInitiationDetails() {
        return initiationDetails;
    }

    public long getTasksPartition() {
        return tasksPartition;
    }

    public int getEstimatedTaskCount() {
        return estimatedTaskCount;
    }

    public void setEstimatedTaskCount(int estimatedTaskCount) {
        this.estimatedTaskCount = estimatedTaskCount;
    }

    public int getCurrentTaskCount() {
        return currentTaskCount;
    }

    public void setCurrentTaskCount(int currentTaskCount) {
        this.currentTaskCount = currentTaskCount;
    }

    public long getLastTaskCountTime() {
        return lastTaskCountTime;
    }

    public void setLastTaskCountTime(long lastTaskCountTime) {
        this.lastTaskCountTime = lastTaskCountTime;
    }

    public long getStartRunTime() {
        return startRunTime;
    }

    public void setStartRunTime(long startRunTime) {
        this.startRunTime = startRunTime;
    }

    public long getPauseDuration() {
        return pauseDuration;
    }

    public void incrementPauseDuration(long pauseDuration) {
        this.pauseDuration += pauseDuration;
    }

    public void setPauseDuration(long pauseDuration) {
        this.pauseDuration = pauseDuration;
    }

    public long getLastStopTime() {
        return lastStopTime;
    }

    public void setLastStopTime(long lastStopTime) {
        this.lastStopTime = lastStopTime;
    }

    public String getJobStateString() {
        return jobStateString;
    }

    public void setJobStateString(String jobStateString) {
        this.jobStateString = jobStateString;
    }

    public void setFailedCount(Integer failedCount) {
        this.failedCount = failedCount;
    }

    public Integer getFailedCount() {
        return failedCount;
    }

    public long getTaskRetriesCount() {
        return taskRetriesCount;
    }

    public void setTaskRetriesCount(long totalRetriesCount) {
        this.taskRetriesCount = totalRetriesCount;
    }

    public void incrementTaskRetriesCount(long totalRetriesCount) {
        this.taskRetriesCount += taskRetriesCount;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getScannedMeaningfulFilesCount() {
        return scannedMeaningfulFilesCount;
    }

    public void setScannedMeaningfulFilesCount(int scannedMeaningfulFilesCount) {
        this.scannedMeaningfulFilesCount = scannedMeaningfulFilesCount;
    }

    @Override
    public String toString() {

        return "SimpleJob{" +
                super.toString()+
                ", taskCount=" + estimatedTaskCount+
                ", currentTaskCount=" + currentTaskCount+
                ", itemId=" + itemId +
                ", failedCount=" + failedCount +
                ", taskRetriesCount=" + taskRetriesCount +
                ", tasksPartition=" + tasksPartition +
                '}';
    }

}
