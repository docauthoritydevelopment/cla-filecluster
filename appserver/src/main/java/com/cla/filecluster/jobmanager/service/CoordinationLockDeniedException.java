package com.cla.filecluster.jobmanager.service;

/**
 * Thrown by the {@link JobCoordinator} to indicate that a requested lock is denied.
 *
 * <p> This can happen, for example, because the job was paused while it was waiting for the lock.
 *
 * Created by Itai Marko.
 */
public class CoordinationLockDeniedException extends Exception {

    /**
     * {@inheritDoc}
     */
    public CoordinationLockDeniedException(String message) {
        super(message);
    }
}
