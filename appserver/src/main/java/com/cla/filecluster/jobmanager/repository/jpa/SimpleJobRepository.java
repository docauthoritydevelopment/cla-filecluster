package com.cla.filecluster.jobmanager.repository.jpa;

import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;
import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.Collection;
import java.util.List;

public interface SimpleJobRepository extends JpaRepository<SimpleJob, Long> {

    @Query("SELECT COUNT(sj) FROM SimpleJob sj" +
            " WHERE sj.tasksPartition=:partitionId " +
            " AND sj.deleted=false")
    int countNonDeletedByPartitionId(@Param("partitionId") long partitionId);

    @Query("SELECT sj.id FROM SimpleJob sj" +
            " where sj.runContext=:runContext and sj.type = :jobType")
    List<Long> findJobId(@Param("runContext") Long runContext, @Param("jobType") JobType jobType);

    @Query("SELECT sj FROM SimpleJob sj" +
            " where sj.type = :jobType" +
            " and sj.state = :jobState")
    List<SimpleJob> listJobsByTypeAndState(@Param("jobType") JobType jobType, @Param("jobState") JobState jobState);

    @Query("SELECT sj FROM SimpleJob sj" +
            " WHERE sj.type IN ( :jobTypes )" +
            " AND sj.state IN ( :jobStates )" +
            " AND sj.deleted = false")
    List<SimpleJob> listJobsByTypesAndStates(@Param("jobTypes") List<JobType> jobType, @Param("jobStates") Iterable<JobState> jobState);

    @Query("SELECT sj FROM SimpleJob sj" +
            " where sj.type = :jobType" +
            " AND sj.runContext = :runContext " +
            " AND sj.itemId = :itemId")
    List<SimpleJob> listJobsByTypeAndItemId(@Param("runContext") Long runContext, @Param("jobType") JobType jobType, @Param("itemId") Long itemId);

    @Query("SELECT sj FROM SimpleJob sj" +
            " WHERE sj.state in (:states) "+
            " AND sj.runContext IN ( :runContextList ) "+
            " AND sj.deleted=false" )
    List<SimpleJob> findJobsByStates(@Param("states") Iterable<JobState> states , @Param("runContextList") List<Long> runContextList);

    @Query("SELECT sj FROM SimpleJob sj" +
            " WHERE sj.runContext IN ( :runContextList )")
    List<SimpleJob> findJobsByContextList(@Param("runContextList") List<Long> runContextList);

    @Query("SELECT sj FROM SimpleJob sj" +
            " WHERE sj.state = :jobState " +
            " AND sj.completionStatus = :completionStatus " +
            " AND sj.runContext = :runContext " +
            " AND sj.deleted=false")
    List<SimpleJob> findJobsByState(@Param("jobState") JobState jobState, @Param("completionStatus") JobCompletionStatus completionStatus, @Param("runContext") Long runContext);


    @Query("SELECT sj FROM SimpleJob sj" +
            " where sj.runContext = :runContext")
    List<SimpleJob> findRunJobs(@Param("runContext") Long runContext);

    @Query("SELECT sj FROM SimpleJob sj " +
            "WHERE sj.state in (:states) " +
            "AND sj.stateUpdateTime < :stateUpdateTime " +
            "AND sj.lastTaskCountTime < :stateUpdateTime " +
            "AND sj.deleted = false ")
    List<SimpleJob> getIdleJobs(@Param("states") Iterable<JobState> states, @Param("stateUpdateTime") long stateUpdateTime);

    @Query("SELECT sj FROM SimpleJob sj " +
            "WHERE sj.stateUpdateTime > :stateUpdateTime " +
            "AND sj.deleted = false")
    List<SimpleJob> getNewerJobs(@Param("stateUpdateTime") long stateUpdateTime);


    @Query("SELECT sj.id FROM SimpleJob sj " +
            "WHERE sj.state in (:states) " +
            "AND sj.deleted = false ")
    List<Long> findNoneDeletedJobIdsByState(@Param("states") Iterable<JobState> states);

    @Query(nativeQuery = true, value = "select cr.root_folder_id from jm_jobs sj " +
            "join crawl_runs cr on sj.run_context = cr.id " +
            "where sj.`type` = 'INGEST_FINALIZE' and sj.state = 'DONE'")
    Collection<? extends Long> alreadyIngestedFinalizedRootFolders();

    @Query(nativeQuery = true, value = "select sj.run_context from jm_jobs sj " +
            "join crawl_runs cr on sj.run_context = cr.id " +
            "where sj.`type` = :jobType and sj.state = :jobState")
    List<BigInteger> findRunsByJobAndState(@Param("jobType")String jobType, @Param("jobState")String jobState);
}
