package com.cla.filecluster.jobmanager.service;

import com.cla.filecluster.jobmanager.domain.entity.SimpleJob;
import com.cla.filecluster.jobmanager.domain.entity.SimpleTask;
import com.cla.filecluster.jobmanager.domain.entity.TaskState;
import com.cla.filecluster.jobmanager.repository.jpa.SimpleTaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service // used for tests only!!
public class JobManagerAppTaskService {
    private Logger logger = LoggerFactory.getLogger(JobManagerAppTaskService.class);

    @Autowired
    private SimpleTaskRepository simpleTaskRepository;

    @Transactional()
    public Page<SimpleTask> getAllJobTasksPage(SimpleJob job, int start, int pageSize) {
        PageRequest pageReq = PageRequest.of(start/pageSize, pageSize);
        return getAllJobTasksPage(job, pageReq);
    }

    @Transactional()
    public Page<SimpleTask> getAllJobTasksPage(SimpleJob job, PageRequest pageReq) {
        Page<SimpleTask> res = job.getTasksPartition() == 0 ?
                simpleTaskRepository.getAllTasksByJob(job.getId(), pageReq) :                           // TODO: backward compatiblity for the partId = 0 bug. To be later removed
                simpleTaskRepository.getAllTasksByJob(job.getId(), job.getTasksPartition(), pageReq);
        return res;
    }

    @Transactional()
    public Page<SimpleTask> getJobTasksByStatePage(long jobId, long partitionId, Iterable<TaskState> states, int start, int pageSize) {
        PageRequest pageReq = PageRequest.of(start/pageSize, pageSize);
        return getJobTasksByStatePage(jobId, partitionId, states, pageReq);
    }

    private Page<SimpleTask> getJobTasksByStatePage(long jobId, long partitionId, Iterable<TaskState> states, PageRequest pageReq) {
        Page<SimpleTask> res = partitionId == 0 ?
                simpleTaskRepository.getTasksByJobAndStates(jobId, states, pageReq) :
                simpleTaskRepository.getTasksByJobAndStates(jobId, partitionId, states, pageReq);
        return res;
    }

}
