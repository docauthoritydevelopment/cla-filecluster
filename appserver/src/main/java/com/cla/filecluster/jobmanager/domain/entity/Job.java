package com.cla.filecluster.jobmanager.domain.entity;


import com.cla.common.domain.dto.crawler.JobCompletionStatus;
import com.cla.common.domain.dto.jobmanager.JobState;
import com.cla.common.domain.dto.jobmanager.JobType;

import javax.persistence.*;
import java.util.Date;

@MappedSuperclass
public abstract class Job extends BaseEntity {

    @TableGenerator(name = "job-table-generator", allocationSize = 10)
    @GeneratedValue(generator = "job-table-generator", strategy = GenerationType.TABLE)
    @Id
    private Long id;

    /**
     * Job run context, in which the job was triggered
     * e.g. specific manual scan triggered from UI
     * or scheduled scan. Enables to trace a process end-to-end
     */
    @Column(nullable = false, name = "run_context")
    private Long runContext;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "type")
    protected JobType type = JobType.UNKNOWN;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "state")
    protected JobState state = JobState.NEW;

    @Enumerated(EnumType.STRING)
    @Column(name = "resume_state")
    protected JobState resumeState;             // state of the job before it was paused

    @Column(name = "state_update_time")
    private Long stateUpdateTime = 0L;

    @Column(nullable = false, name = "accepting_tasks")
    private boolean acceptingTasks = false;

    @Enumerated(EnumType.STRING)
    @Column(name = "completion_stauts")
    private JobCompletionStatus completionStatus;

    // --------------------------- getters / setters --------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) { // added for ut
        this.id = id;
    }

    public Long getRunContext() {
        return runContext;
    }

    public void setRunContext(Long runContext) {
        this.runContext = runContext;
    }

    public JobCompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    public void setCompletionStatus(JobCompletionStatus completionStatus) {
        this.completionStatus = completionStatus;
    }

    public JobState getState() {
        return state;
    }

    public void setState(JobState state) {
        this.state = state;
    }

    public JobState getResumeState() {
        return resumeState;
    }

    public void setResumeState(JobState resumeState) {
        this.resumeState = resumeState;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public boolean isAcceptingTasks() {
        return acceptingTasks;
    }

    public void setAcceptingTasks(boolean acceptingTasks) {
        this.acceptingTasks = acceptingTasks;
    }

    public Long getStateUpdateTime() {
        return stateUpdateTime;
    }

    public void setStateUpdateTime(Long stateUpdateTime) {
        this.stateUpdateTime = stateUpdateTime;
	}

    @PreUpdate
    public void setStateUpdateTime() {
        this.stateUpdateTime =  (new Date()).getTime();
    }

    @Override
    public String toString() {
        return "Job{" +
                "id=" + id +
                ", runContext=" + runContext +
                ", type=" + type +
                ", state=" + state +
                ", acceptingTasks=" + acceptingTasks +
                ", completionStatus=" + completionStatus +
                '}';
    }
}
